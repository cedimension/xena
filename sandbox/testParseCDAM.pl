use XReport::Storage::IN;

use strict;

use IO::File;

use XReport;
my $cdamfh = XReport::Storage::IN->Open($ARGV[0]);

sub read_block_pure {
  my $f_cdam = shift;
  my $blksz = shift || 23476;
  my $rlen = $f_cdam->read(my $buff, $blksz);
  die "Read Failure - $! - ", ref($buff), " ", length($buff), "\n" unless defined($buff);
  warn "read returned ", length($buff), " bytes buffer - exp.: $rlen\n";
  return $buff;
}

sub recordparser {
    my $self = shift;
    my $data = shift;
    my $lrecl = ($self->{cinfo}->{lrecl} || 80);
    $self->{cinfo}->{maxlrec_detected} = $lrecl;
#    warn "LRECL: $lrecl BUFFER:\n", join("\n", unpack("(H100)*", $data)), "\n", '-' x 100, "\n";
    my $recsarray = [ map { [length($_), $_ ] } map { my $rec = join('', map { my $fld = $_; 
             $fld =~ /^(\xff..)(.*)$/s ? do { my ($f1, $f2) = ($1, $2);
                                    $f2 = '' unless $f2;
                                    my ($c, $l) = unpack("xaC", $f1);
                                    warn "recordparser Suspicious Field detected:", unpack('H*', $f1) unless defined($c);
                                    $c x $l . $f2;
                                } : $fld  } split /(\xff..)/s, $_ );  
                       (exists($self->{recordexit}) ? &{$self->{recordexit}}($self, $rec) : $rec) } 
                                                                                 split(/\xfe/s, $data ) ];
#    $self->{cinfo}->{maxlrec_detected} = $lrecl 
#                   unless $self->{cinfo}->{maxlrec_detected} && $self->{cinfo}->{maxlrec_detected} < $lrecl;
    my $outbuff = join('', map { pack("n/a*", $_->[1]) } @{$recsarray});
    
#    warn "RECARRAY:\n", join("\n", map { 'LL: '.length($_->[1]).' - '.unpack('H*', $_->[1])} @{$recsarray}), "\n";
#    die "STOP HERE ----------";
#    warn "recordparser returning ", scalar(@$recsarray), " records parsed from ", length($data), " bytes: ", 
                                                                                    unpack("H80", $outbuff), "\n";
    return ($outbuff, '', $recsarray);
}

my $readrtn = \&read_block_pure if ref($cdamfh) eq 'XReport::Storage::IN';               
my $lrk = (grep /^lrecl$/i, keys %{$cdamfh->{cinfo}})[0];
my $cdamll = 27998;
$cdamll = $cdamfh->{cinfo}->{$lrk} if $lrk;
warn "CINFO: ", Dumper($cdamfh->{cinfo}), "\n";

my $datahdr = '';
my $ct;

my ($data, $blk) = ('', '');
$main::ffeed = '';

my ( $ofilen, $ofileh, $newjrh );
$main::bytecache = '';

while (!$cdamfh->eof() || ($main::bytecache && length($main::bytecache) > 0)) {
      my $buff = $blk.&$readrtn($cdamfh, 6) unless $cdamfh->eof();
    
      #$buff = &$readrtn($cdamfh) if !$datahdr && $buff =~ /^\xC6\xC4\xC2\x40/;
    
      (my $datal, my $flags, $buff) = unpack('na4a*', $buff);
      warn "going to fill buffer: ", length($buff)
           , " datalen: $datal"
           , " blk: ", length($blk)
           , " blkdata: ", substr($blk, 0, 8)
           , " - ", unpack('H20', $blk), "\n";
      while ( !$cdamfh->eof() && length($buff) < $datal ) {
         warn "filling bufflen: ", length($buff), " datalen: $datal blk: ", length($blk), "\n";
         $buff .= &$readrtn($cdamfh, $datal - length($buff) );
      }
      ($buff, $blk) = unpack("a$datal a*", $buff);
      $blk = '' unless $blk;
      
      my $blen = length($buff) + 6;
      while ( $cdamll > length($blk) + $blen ) {
         last if $cdamfh->eof();
         $blk .= &$readrtn($cdamfh, $cdamll - $blen - length($blk) );
     }
      $blk = substr($buff.$blk, $cdamll - 6) ;# unless ( $blen + length($blk) < $cdamll );
      warn "buff before init: ", unpack("H80", $buff), "\n" unless $ofileh;
      (my $pagsbuff, $buff) = unpack("a$datal a*", $buff);
      warn "CDAMLL: $cdamll bufflen: $blen"
           , " blk: ", length($blk)
           , " data: ", length($data)
           , " pagsbuff: ", length($pagsbuff)
           , " - ", unpack('H80', $pagsbuff)
           , "\n";
  
  if ( $pagsbuff =~ /^\x01\x28..\xE5..\xF0/ ) {
    $newjrh->Close($data) if ( $newjrh && $newjrh->{dfilen} && $newjrh->{outfh} );
    warn "Init new store for spool portion of CDAM file - ", unpack("H80", $pagsbuff), "\n";
    my $hdrl = unpack('n', $pagsbuff); 
    ($datahdr, $pagsbuff) = unpack("a$hdrl a*", $pagsbuff);
    $ofileh = IO::File->new(">$ARGV[0].output".$::ct++);
    $pagsbuff =~ s/^\xfd\xfe// if $main::ffeed eq '';
  }
  die "CDAM Header not recognized " unless $ofileh;
  $pagsbuff =~ s/^\xfd\xfe// unless length($data);
  my $pages = [ split /\xfd\xfe/, $data . $pagsbuff ];
  $data = pop @$pages;
  warn "EOF: ", $cdamfh->eof(), "DATA: ", length($data), " PAGES: ", scalar(@$pages), " PAG1: ", unpack("H80", $data.$pagsbuff),  "\n";
  foreach my $page ( @$pages ) { 
  	my ($outbuff, $str, $recsarray) = recordparser(undef, $page);
    $ofileh->write($outbuff); 
  }
  $pagsbuff = '';
}
$data =~ s/\x00*$//s;
warn "LAST BUFFER:\n", join("\n", unpack("(H160)*", $data)), "\n", '-' x 100, "\n";
my ($outbuff, $str, $recsarray) = recordparser(undef, $data);
$ofileh->write($outbuff); 

$newjrh->Close($data) if ( $newjrh && $newjrh->{dfilen} && $newjrh->{outfh} );
