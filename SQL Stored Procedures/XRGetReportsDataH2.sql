USE [XNCTDH2]
GO

/****** Object:  StoredProcedure [dbo].[XRGetReportsDataH2]    Script Date: 27/05/2019 15:45:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[XRGetReportsDataH2]   
    @JobName varchar(32),   
	@ReportName varchar(64),   
	@JobNumber varchar(16),  
	@Recipient varchar(128), 
    @DataFrom nvarchar(8),
	@DataTo nvarchar(8),
	@Orderby nvarchar(32),
	@User nvarchar(16)  
AS   

DECLARE @tab_results TABLE ([ROW] int, FolderName varchar(128), FolderDescr varchar(256),	JobReport_ID int ,ReportId int,	TimeRef datetime,	UserTimeRef datetime,	UserTimeElab datetime,	XferStartTime datetime,	TotReports int,	TotPages int,	ListOfPages varchar(MAX), JobName varchar(256),	JobReportDescr  varchar(256),	JobNumber  varchar(16),	ReportName varchar(128),	FileRangesVar varchar(64),	ExistTypeMap int,	IsGRANTED int , HasIndexes bit,	Recipient varchar(256),	CheckStatus bit,	LastUsedToCheck varchar(32),	CheckFlag bit,	FromPage int)

DECLARE @SQL NVARCHAR(max);
DECLARE @WHERE  NVARCHAR(max);
DECLARE @CHEKUSER  NVARCHAR(max);
DECLARE @END NVARCHAR(64);
DECLARE @FILTERVALUECONDITION NVARCHAR(128);
DECLARE @ROOT NVARCHAR(16);
DECLARE @HasIndexes NVARCHAR(32);
DECLARE @Pivot_Column  NVARCHAR(max);  
DECLARE @Query  NVARCHAR(max);  
DECLARE @Pages_sum  int = 0;
DECLARE @Report_sum  int = 0;  

SET @WHERE = N' WHERE ';
SET @END = N'';
SET @CHEKUSER = N'';
--SET @FILTERVALUECONDITION = N' AND lr1.reportid <= 2 ';
SET @FILTERVALUECONDITION = N' AND lr1.FilterValue = ''$$$$'' ';
SET @HasIndexes = N' JR.HasIndexes ';


IF ((@Recipient IS NOT NULL) AND (@Recipient <> '%') AND (@Recipient <> '') AND (@Recipient <> '*'))
BEGIN 
        SET @FILTERVALUECONDITION = N' AND lr1.FilterValue = ''' + replace(replace(replace(@Recipient, '*',''), '%',''),'_','')  + ''''
		SET @HasIndexes = N' cast(0 as bit) as HasIndexes ';
END


IF (@User IS NOT NULL)

BEGIN
       select @ROOT = RootNode from tbl_ProfilesFoldersTrees pft join tbl_UsersProfiles up  with (NOLOCK) on pft.ProfileName = up.ProfileName 
		  join    tbl_UserAliases ua  with (NOLOCK) on up.UserName = ua.UserName and  ua.UserAlias = '''+@User+''' and RootNode = 'ROOT' ;
END

IF (@ROOT IS NOT NULL AND @ROOT <> 'ROOT')
BEGIN 
	SET @CHEKUSER = N' and fn1.FolderName in ( select FolderName from tbl_Folders fo inner join tbl_ProfilesFoldersTrees pt with (NOLOCK) on fo.FolderName like pt.RootNode+''%'' inner join tbl_UsersProfiles up with (NOLOCK) 
	on up.ProfileName = pt.ProfileName and rootnode <> ''ROOT'' 
	join tbl_useraliases ua with (NOLOCK) on up.username = ua.username where ua.UserName like '''+@User+''' ) '
END 

SET @JobName = replace(@JobName, '*','%')

SET @SQL = N'SELECT TOP 1000 *, 1 AS FromPage FROM 
	(SELECT ROW_NUMBER() OVER (ORDER BY  '+ @Orderby+', UserTimeElab ASC) AS ROW , B.* FROM (SELECT 
		DISTINCT fn1.FolderName, 
		FolderDescr AS FolderDescr, 
		JR.JobReportId as JobReport_ID, 
		lr1.ReportId As ReportId, 
		JR.XferStartTime As TimeRef, 
		COALESCE(JR.UserTimeRef, JR.XferStartTime) As UserTimeRef, 
		COALESCE(JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime) As UserTimeElab, 
		JR.XferStartTime As XferStartTime,
		1 As TotReports,
		lr1.TotPages As TotPages, 
		CONVERT(varchar(500), pr1.ListOfPages) as ListOfPages,
		--''2118,284,1,2138,77,4,2154,30,1,2117,291,1,2155,233,1,2384,589,4,2139,27,2,2156,15,1,2374,302,2,2140,47,4,2157,13,1,2375,281,2,2116,245,1,2141,57,4,2158,22,1,2376,234,1,3086,553,4,3087,206,1'' as ListOfPages,
		(case  
			when RemoteFileName like  ''%.CC.J%''  then ''CCKE1CC'' 
		    when RemoteFileName like  ''%.M006158A.J%''  then ''CCKE1CC'' 
			when RemoteFileName like  ''%.NC.J%''  then ''CCKE1NC''   
			when 1 = 1 then ''CCKE1EU''
		end ) JobName, 

		'''' as JobReportDescr,
		JR.JobNumber,    
		COALESCE(lrt1.textString, JR.UserRef) AS ReportName,
		JR.FileRangesVar,
		JR.ExistTypeMap,
		1 As IsGRANTED, 
		' + @HasIndexes + ',
        fn1.FolderNameKey AS Recipient, 
		lr1.CheckStatus, 
		lr1.LastUsedToCheck, 
		RN.CheckFlag       
	FROM tbl_Folders fn1 with (NOLOCK)
	INNER JOIN dbo.tbl_FoldersRules  fr1 with (NOLOCK) ON fn1.FolderName = fr1.FolderName  '+ @CHEKUSER + '
	INNER JOIN dbo.tbl_NamedReportsGroups nm1  with (NOLOCK) ON fr1.ReportGroupId = nm1.ReportGroupId
	INNER JOIN tbl_VarSetsValues vr1 with (NOLOCK) ON nm1.ReportRule = vr1.VarSetName
	INNER JOIN tbl_VarSetsValues vv1  with (NOLOCK) ON vv1.VarSetName = nm1.FilterRule   and vv1.VarName = nm1.FilterVar
	INNER JOIN (Select * from tbl_JobReports  with (NOLOCK) where  JobReportName <> ''CDAMFILE'' AND PendingOp <> 12 
	AND (case  
		when RemoteFileName like  ''%.M006158A.J%''  then ''CCKE1CC'' 
		when RemoteFileName like  ''%.CC.J%''  then ''CCKE1CC'' 
		when RemoteFileName like  ''%.NC.J%''  then ''CCKE1NC''   
		when 1 = 1 then ''CCKE1EU''
	end ) LIKE ''' +  @JobName + '%'' ) JR  
	ON JR.JobReportName = vr1.VarValue
	INNER JOIN tbl_LogicalReports lr1  with (NOLOCK) ON lr1.JobReportId = JR.JobReportId and lr1.XferRecipient = nm1.RecipientRule and lr1.FilterValue = vv1.VarValue '+ @FILTERVALUECONDITION + ' 
	INNER JOIN tbl_LogicalReportsRefs lrr1  with (NOLOCK) ON lr1.JobReportId = lrr1.JobReportId AND lrr1.ReportId = lr1.ReportId  and lrr1.FilterVar = vv1.VarName
	INNER JOIN tbl_LogicalReportsTexts lrt1  with (NOLOCK) ON lrr1.textId = lrt1.textId
	INNER JOIN dbo.tbl_ReportNames RN with (NOLOCK)  ON RN.ReportName = lr1.ReportName
	INNER JOIN tbl_PhysicalReports pr1  with (NOLOCK) ON pr1.JobReportId = lr1.JobReportId and pr1.ReportId = lr1.ReportId        
	WHERE JR.Status = 18 and vr1.VarName = '':REPORTNAME'' and vv1.VarName <> '':REPORTNAME''
) B '


-- DATA OBBLIGATORIA
IF (@DataFrom IS NOT NULL AND @DataTo IS NOT NULL)
BEGIN
        SET @WHERE = @WHERE + 'UserTimeRef >= CONVERT(datetime, ''' + @DataFrom + ''' , 120) AND UserTimeRef <= CONVERT(datetime, ''' + @DataTo + ''' , 120)';
END

IF (@ReportName IS NOT NULL)
BEGIN
        SET @WHERE = @WHERE + ' AND ReportName like ''' + @ReportName + '%''';
END

IF (@JobNumber IS NOT NULL)
BEGIN
        SET @WHERE = @WHERE + ' AND JobNumber like ''' + @JobNumber + '%''';
END

IF (@Recipient IS NOT NULL)
BEGIN
        SET @Recipient = replace(@Recipient, '*','%')
		SET @WHERE = @WHERE + ' AND Recipient like ''' + @Recipient + '%'''; 
		 
END

SET @END = ' ) T WHERE ROW > 0 ';

SET @SQL = @SQL + @WHERE + @END;

--print @SQL;

INSERT INTO @tab_results  EXEC sp_executesql @SQL

print N'FATTO'

IF ((@Recipient IS NOT NULL) AND (@Recipient <> '%') AND (@Recipient <> '') AND (@Recipient <> '*'))
BEGIN 

	SELECT @Pivot_Column= COALESCE(@Pivot_Column+',','')+ QUOTENAME (JobReport_ID)+ ','+ ListOfPages FROM  
	@tab_results
	SELECT @Pages_sum=  (@Pages_sum)+ TotPages  FROM  
	@tab_results
	SELECT @Report_sum=  (@Report_sum)+ TotReports  FROM  
	@tab_results

insert into @tab_results 
select top 1 0,'ALL PAGES RESULT' ,'GLOBAL RECIPIENT',JobReport_ID,0, getDate(),getDate(),getDate(),getDate(),@Report_sum,@Pages_sum, replace (replace (@Pivot_Column ,  '[', '') , ']',''),JobName,JobReportDescr,'JOB00000','REPORT OF REPORTS',FileRangesVar,ExistTypeMap,IsGRANTED,HasIndexes,'ALL PAGES RESULT',CheckStatus,LastUsedToCheck,CheckFlag,FromPage
 from @tab_results;
END 

select * from @tab_results order by [ROW]




















GO


