USE [XNCTDMA]
GO

/****** Object:  StoredProcedure [dbo].[XRGetReportsDataALL_AUS_withLastExReport]    Script Date: 11/10/2018 11:43:19 ******/
DROP PROCEDURE [dbo].[XRGetReportsDataALL_AUS_withLastExReport]
GO

/****** Object:  StoredProcedure [dbo].[XRGetReportsDataALL_AUS_withLastExReport]    Script Date: 11/10/2018 11:43:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










CREATE PROCEDURE [dbo].[XRGetReportsDataALL_AUS_withLastExReport]   
    @JobName varchar(32),   
	@ReportName varchar(64),   
	@JobNumber varchar(16),  
	@Recipient varchar(128), 
    @DataFrom varchar(23),
	@DataTo  varchar(23),
	@Orderby nvarchar(32),
	@User nvarchar(100),
	@User2 nvarchar(100),
	@Remark nvarchar(16),
	@FlagLastExecution bit
AS   

DECLARE @SQL NVARCHAR(4000);
DECLARE @WHERE NVARCHAR(4000);
DECLARE @DATAF NVARCHAR(23);
DECLARE @DATAT NVARCHAR(23);
DECLARE @CHEKUSER NVARCHAR(2000);
DECLARE @END NVARCHAR(64);
DECLARE @ROOT NVARCHAR(16);
DECLARE @LIKECOND NVARCHAR(200);
DECLARE @JOBNAMECONDITION NVARCHAR(2000);
DECLARE @LASTEXECCONDITION NVARCHAR(300);

SET @WHERE = ' WHERE 1=1 ';
SET @END = '';
SET @CHEKUSER = '';
SET @DATAF = @DataFrom;
SET @DATAT = @DataTo;
SET @ROOT = NULL;
SET @LASTEXECCONDITION = '';

IF (@User IS NOT NULL)
BEGIN
       select @ROOT = (select TOP 1 RootNode from tbl_ProfilesFoldersTrees pft join tbl_UsersProfiles up  with (NOLOCK) on pft.ProfileName = up.ProfileName 
		  join    tbl_UserAliases ua  with (NOLOCK) on up.UserName = ua.UserName and  ua.UserAlias = @User) ;
END

IF (@ROOT IS NOT NULL AND @ROOT <> 'ROOT')
BEGIN
	set @LIKECOND = N' where ua.UserAlias like '''+@User+'''';
	IF(@User2 IS NOT NULL)
	BEGIN 
		SET @LIKECOND = N' where (ua.UserAlias like '''+@User+''' OR ua.UserAlias like '''+@User2+''' )';
	END
	SET @CHEKUSER = N' and fn1.FolderName in ( select FolderName from tbl_Folders fo inner join tbl_ProfilesFoldersTrees pt with (NOLOCK) on fo.FolderName like pt.RootNode+''%'' inner join tbl_UsersProfiles up with (NOLOCK) 
	on up.ProfileName = pt.ProfileName and rootnode <> ''ROOT'' 
	join tbl_useraliases ua with (NOLOCK) on up.username = ua.username '+ @LIKECOND + ' ) ' ;
END 

SET @JobName = replace(@JobName, '*','%')
SET @ReportName = replace(@ReportName, '*','%')

SET @JOBNAMECONDITION = N' (ComputedJobName LIKE ''' +  @JobName + '%'' ) ';
IF (( (@JobName IS NULL) OR (@JobName = '') OR (@JobName LIKE 'ALL[%]%') ) AND (@ReportName IS NOT NULL ) AND (@ReportName <> '') )
BEGIN 
	SET @JobName = replace(@JobName, 'ALL%','')
	SET @JOBNAMECONDITION = N' (ComputedJobName in (select JobReportNameKey from tbl_Job_ReportMappings where XRReportName LIKE ''' +  @ReportName + '%'' and JobReportNameKey like '''+ @JobName+''')) ';
END 


-- LastExecution = 1
IF (@FlagLastExecution IS NOT NULL AND @FlagLastExecution = 1 )
BEGIN
	SET @LASTEXECCONDITION =  N'INNER JOIN tbl_LastExecLogicalReports lelr with (NOLOCK) ON lelr.JobReportName = vr1.VarValue and lelr.FilterValue =  lr1.FilterValue and lelr.textId =  lrr1.textId and lelr.JobReportId = lr1.JobReportId and lelr.ReportId = lr1.ReportId' + CHAR(10);
END


--ExistTypeMap forced to 2 for MA and BA Environments to show xls icon for all the reporrt. (before the value was JR.ExistTypeMap)
SET @SQL = N'SELECT TOP 1000 *, 1 AS FromPage FROM
(SELECT ROW_NUMBER() OVER (ORDER BY  '+ @Orderby+') AS ROW , B.* FROM (SELECT
DISTINCT fn1.FolderName,
FolderDescr AS FolderDescr,
JR.JobReportId as JobReport_ID,
lr1.ReportId As ReportId,
JR.XferStartTime As TimeRef,
COALESCE(JR.UserTimeRef, JR.XferStartTime) As UserTimeRef,
COALESCE(JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime) As UserTimeElab,
JR.XferStartTime As XferStartTime,
1 As TotReports,
lr1.TotPages As TotPages,
CONVERT(varchar(500), pr1.ListOfPages) as ListOfPages,
substring(JR.jobreportname,1,8)  as JobName,
'''' as JobReportDescr,
JR.JobNumber,
COALESCE(lrt1.textString, JR.UserRef) AS ReportName,
JR.FileRangesVar,
2 as ExistTypeMap,
1 As IsGRANTED,
JR.HasIndexes,
fn1.FolderNameKey AS Recipient,
lr1.CheckStatus,
lr1.LastUsedToCheck,
COALESCE(lr1.Remark, '''') AS Remark,
''campo fake'' AS FakeField,
RN.CheckFlag
FROM tbl_Folders fn1 with (NOLOCK)
INNER JOIN dbo.tbl_FoldersRules  fr1 with (NOLOCK) ON fn1.FolderName = fr1.FolderName  '+ @CHEKUSER + '
INNER JOIN dbo.tbl_NamedReportsGroups nm1  with (NOLOCK) ON fr1.ReportGroupId = nm1.ReportGroupId
INNER JOIN tbl_VarSetsValues vr1 with (NOLOCK) ON nm1.ReportRule = vr1.VarSetName
INNER JOIN tbl_VarSetsValues vv1  with (NOLOCK) ON vv1.VarSetName = nm1.FilterRule   and vv1.VarName = nm1.FilterVar
INNER JOIN (Select * from tbl_JobReports  with (NOLOCK) where  JobReportName <> ''CDAMFILE'' AND PendingOp <> 12
AND ' + @JOBNAMECONDITION + ') JR
ON JR.JobReportName = vr1.VarValue
INNER JOIN tbl_LogicalReports lr1  with (NOLOCK) ON lr1.JobReportId = JR.JobReportId and lr1.XferRecipient = nm1.RecipientRule and lr1.FilterValue = vv1.VarValue
INNER JOIN tbl_LogicalReportsRefs lrr1  with (NOLOCK) ON lr1.JobReportId = lrr1.JobReportId AND lrr1.ReportId = lr1.ReportId  and lrr1.FilterVar = vv1.VarName'
+ CHAR(10) + @LASTEXECCONDITION +
'INNER JOIN tbl_LogicalReportsTexts lrt1  with (NOLOCK) ON lrr1.textId = lrt1.textId
INNER JOIN dbo.tbl_ReportNames RN with (NOLOCK)  ON RN.ReportName = lr1.ReportName
INNER JOIN tbl_PhysicalReports pr1  with (NOLOCK) ON pr1.JobReportId = lr1.JobReportId and pr1.ReportId = lr1.ReportId
WHERE JR.Status = 18 and vr1.VarName = '':REPORTNAME'' and vv1.VarName <> '':REPORTNAME''
) B '



-- DATA OBBLIGATORIA SE FlagLastExecution = 0
IF ((@DataFrom IS NOT NULL) AND (@DataTo IS NOT NULL) AND (@FlagLastExecution <> 1 ) ) 
BEGIN
        
		IF(len(@DataFrom) = 8) 
		BEGIN
			SET @DATAF = substring(@DataFrom, 1,4) + '-' + substring(@DataFrom, 5,2) + '-' + substring(@DataFrom, 7,2) + 'T00:00:00.000';
		END
		IF(len(@DataTo) = 8) 
		BEGIN
			SET @DATAT = substring(@DataTo, 1,4) + '-' + substring(@DataTo, 5,2) + '-' + substring(@DataTo, 7,2) + 'T23:59:59.999';
		END
		SET @WHERE = @WHERE + ' AND CONVERT(varchar, UserTimeRef, 126) >= ''' + @DATAF + ''' AND CONVERT(varchar, UserTimeRef, 126) <= ''' + @DATAT + '''';
	
END


IF (@ReportName IS NOT NULL)  
BEGIN
        SET @WHERE = @WHERE + ' AND ReportName like ''' + @ReportName + '%''';
END


IF (@JobNumber IS NOT NULL)
BEGIN
        SET @WHERE = @WHERE + ' AND JobNumber like ''' + @JobNumber + '%''';
END

IF (@Recipient IS NOT NULL)
BEGIN
        SET @Recipient = replace(@Recipient, '*','%')
		SET @WHERE = @WHERE + ' AND Recipient like ''' + @Recipient + '%''';
END


IF (@Remark IS NOT NULL)
BEGIN
        SET @Remark = replace(@Remark, '*','%')
		SET @WHERE = @WHERE + ' AND Remark like ''' + @Remark + '%''';
END

SET @END = ' ) T WHERE ROW > 0 ';

SET @SQL = @SQL + @WHERE + @END;

--print @SQL;

EXECUTE sp_executesql @SQL









GO

