@REM config SVCS

@SETLOCAL
@set XREPORT_HOME=D:\xena\xreport_home
@set XREPORT_SITECONF=D:\xena\xreport_siteconf
@set XREPORT_USERID=esp\xreport
@set XREPORT_PWD=euriskom

@IF [%1]==[] (
@set sccmd=config
) ELSE (
@set sccmd=%1
)

@call :services %sccmd% _ALL_

@goto :EOF

:services

@call :callsc %1 c0ctlpw003 %2 GTW031 "Jes Gateway Server" demand
@call :callsc %1 c0ctlpw003 %2 LPD031 "LPD Server" demand
@call :callsc %1 c0ctlpw003 %2 FTP031 "FTP Server" demand
@call :callsc %1 c0ctlpw003 %2 INI031 "Work Dispatcher" demand
@call :callsc %1 c0ctlpw003 %2 PDF031 "PDF Maker 01" demand
@call :callsc %1 c0ctlpw003 %2 PDF032 "PDF Maker 02" demand

@goto :EOF

:callsc

@SETLOCAL
@set svcn=XR_%4
@IF [%7]==[] (
@set srvn=XR_%4
) ELSE (
@set srvn=%7
)
@IF [%1]==[stop] (
sc \\%2 stop %svcn%
@goto :EOF
)
@IF [%1]==[config] (
@IF [%srvn%]==[W3SVC] (
@goto :EOF
)
sc \\%2 config %svcn% binPath= "c:\perl\bin\perl.exe -I """%XREPORT_HOME%\perllib""" """%XREPORT_HOME%\bin\xrDaemon.pl""" -HOME """%XREPORT_HOME%""" -SITECONF """%XREPORT_SITECONF%""" -N %srvn%" type= own start= %6 obj= %XREPORT_USERID% password= %XREPORT_PWD% DisplayName= "XReport ""%5"" (%svcn%)"
) 
@IF [%1]==[create] (
@IF [%srvn%]==[W3SVC] (
@goto :EOF
)
sc \\%2 create %svcn% binPath= "c:\perl\bin\perl.exe -I """%XREPORT_HOME%\perllib""" """%XREPORT_HOME%\bin\xrDaemon.pl""" -HOME """%XREPORT_HOME%""" -SITECONF """%XREPORT_SITECONF%""" -N %srvn%" type= own start= %6 obj= %XREPORT_USERID% password= %XREPORT_PWD% DisplayName= "XReport ""%5"" (%svcn%)"
) 
@IF [%1]==[query] (
@echo "query status of %svcn% (%5) on %2 (start= %6)" 
sc \\%2 queryex %svcn% | find /I "STATE"
) ELSE (
@IF [%1]==[recycle] (
sc \\%2 stop %svcn%
)
@IF [%6]==[auto] (
sc \\%2 start %svcn%
) 
@IF [%6]==[demand] (
@IF [%1]==[recycle] (
@echo "leaving as is %svcn% (%5) on %2 (start= %6)" 
) ELSE (
sc \\%2 stop %svcn%
)
)
)
@ENDLOCAL
@goto :EOF

:svc
@echo sc \\%2 %1 %3
@goto :EOF

:excluded

@ENDLOCAL