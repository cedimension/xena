USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_BundlesNames]    Script Date: 06/12/2013 12:14:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_BundlesNames]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_BundlesNames]
GO

USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_BundlesNames]    Script Date: 06/12/2013 12:14:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ARITHABORT ON
GO

CREATE TABLE [dbo].[tbl_BundlesNames](
    [BundleKey]  AS (((([BundleName]+'_')+[BundleCategory])+'_')+[BundleSkel]),
    [BundleName] [varchar](128) NOT NULL,
    [BundleCategory] [varchar](128) NOT NULL,
    [BundleSkel] [varchar](128) NOT NULL,
    [BundleWriter] [varchar](128) NULL,
    [BundleDest] [varchar](128) NULL,
    [BundleDescr] [varchar](128) NULL,
    [BundleUdata] [varchar](128) NULL,
    [BundleUdest] [varchar](128) NULL,
    [BundleForm] [varchar](128) NULL,
    [BundleClass] [varchar](128) NULL,
    [BundlePrtopt] [varchar](128) NULL,
    [BundleOrder] [varchar](128) NULL,
    [InputMember] [varchar](8) NULL,
 CONSTRAINT [PK_tbl_BundlesNames_1] PRIMARY KEY CLUSTERED 
(
    [BundleKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

