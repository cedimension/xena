
@SETLOCAL

@SET _robo_loc=\\EQTRISSTPRT01\xreport_siteconf\install_config\robocopy

@SET _source=\\EQTRISSTPRT01

@SET _dest=\\NASVDM04

@SET _logfile=\\NASVDM04\xreport_work_centro$\robocopy_PRT01_VDM04.log
@set _copyparms=/S /NP /E /XO /LOG+:%_logfile%

echo "ROBOCOPY STARTED" >%_logfile%
@%_robo_loc%\robocopy %_source%\xreport_home %_dest%\xreport_home$ *.* %_copyparms% /XD unused
@%_robo_loc%\robocopy %_source%\xreport_siteconf\bin %_dest%\xreport_siteconf$\bin *.* %_copyparms% /XD unused
@%_robo_loc%\robocopy %_source%\xreport_siteconf\mngrroot %_dest%\xreport_siteconf$\mngrroot *.* %_copyparms% /XD unused
@%_robo_loc%\robocopy %_source%\xreport_siteconf\userlib %_dest%\xreport_siteconf$\userlib *.* %_copyparms% /XD unused
@%_robo_loc%\robocopy %_source%\xreport_siteconf\WebFarmRoot %_dest%\xreport_siteconf$\WebFarmRoot *.* %_copyparms% /XD unused
@%_robo_loc%\robocopy %_source%\xreport_siteconf\webroot %_dest%\xreport_siteconf$\webroot *.* %_copyparms% /XD unused
@%_robo_loc%\robocopy %_source%\xreport_siteconf\xml %_dest%\xreport_siteconf$\xml *.* %_copyparms% /XD unused

@call :copypolo ADDES nord
@call :copypolo ETR nord
@call :copypolo NOMOS nord
@call :copypolo CERIT centro
@call :copypolo SOGEI centro
@call :copypolo SARDEGNA centro
@call :copypolo GERIT sud
@call :copypolo POLIS sud
@call :copypolo SICILIA sud

@call :copyappl2machine manager EQTRISSTPRT02\D$
@call :copyappl2machine manager EQTRISSTPRT03\D$
@call :copyappl2machine manager EQTRISSTPRT04\D$
@call :copyappl2machine manager EQTRISSTPRT05\D$
@call :copyappl2machine manager EQTRISSTPRT06\D$
@call :copyappl2machine manager EQTRISSTPRT07\D$

@goto :EOF

:copypolo

@%_robo_loc%\robocopy %_source%\xreport_work\%1 %_dest%\xreport_work_%2$\%1 *.* %_copyparms% /XF *.log *.ps

:: @%_robo_loc%\robocopy %_source%\xreport_storage\%1\IN\2011\0524 %_dest%\xreport_storage_%2$\%1\IN\2011\0524 *.* %_copyparms% 
:: @%_robo_loc%\robocopy %_source%\xreport_storage\%1\IN\2011\0525 %_dest%\xreport_storage_%2$\%1\IN\2011\0525 *.* %_copyparms% 
:: @%_robo_loc%\robocopy %_source%\xreport_storage\%1\OUT\2011\0524 %_dest%\xreport_storage_%2$\%1\OUT\2011\0524 *.* %_copyparms% 
:: @%_robo_loc%\robocopy %_source%\xreport_storage\%1\OUT\2011\0525 %_dest%\xreport_storage_%2$\%1\OUT\2011\0525 *.* %_copyparms% 
@%_robo_loc%\robocopy %_source%\xreport_storage\%1 %_dest%\xreport_storage_%2$\%1 *.* %_copyparms% 

@%_robo_loc%\robocopy %_source%\xreport_siteconf\%1 %_dest%\xreport_siteconf$\%1 *.* %_copyparms% /XF *.log *.ps

@goto :EOF

:copyappl2machine

@%_robo_loc%\robocopy %_dest%\xreport_home$\WebFarmAppls\%1 \\%2\xreport_home\WebFarmAppls\%1 *.* %_copyparms% 

@goto :EOF

@ENDLOCAL