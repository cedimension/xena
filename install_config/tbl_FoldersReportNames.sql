USE [xreport]
GO

/****** Object:  View [dbo].[tbl_FoldersReportNamesOLD]    Script Date: 08/26/2010 11:40:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  View dbo.tbl_FoldersReportNamesOLD    Script Date: 04/06/2006 22.57.53 ******/
CREATE VIEW [dbo].[tbl_FoldersReportNamesOLD]
AS
SELECT     dbo.tbl_FoldersReportGroups.FolderName, dbo.tbl_ReportsGroups.ReportRule, dbo.tbl_ReportsGroups.FilterVar, dbo.tbl_ReportsGroups.FilterRule, 
                      dbo.tbl_ReportsGroups.RecipientRule, dbo.tbl_FoldersReportGroups.FORBID, dbo.tbl_FoldersReportGroups.RuleId
FROM         dbo.tbl_FoldersReportGroups INNER JOIN
                      dbo.tbl_ReportsGroups ON dbo.tbl_FoldersReportGroups.ReportGroupId = dbo.tbl_ReportsGroups.ReportGroupId


GO

/****** Object:  View [dbo].[tbl_FoldersReportNames]    Script Date: 08/25/2010 08:49:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[tbl_FoldersReportNames]
AS
SELECT     dbo.tbl_FoldersRules.FolderName, dbo.tbl_FoldersRules.FORBID, dbo.tbl_NamedReportsGroups.ReportRule, dbo.tbl_NamedReportsGroups.FilterVar, 
                      dbo.tbl_NamedReportsGroups.FilterRule, dbo.tbl_NamedReportsGroups.RecipientRule, dbo.tbl_FoldersRules.RuleId
FROM         dbo.tbl_FoldersRules INNER JOIN
                      dbo.tbl_NamedReportsGroups ON dbo.tbl_FoldersRules.ReportGroupId = dbo.tbl_NamedReportsGroups.ReportGroupId

GO

/****** Object:  Table [dbo].[tbl_NamedReportsGroups]    Script Date: 08/25/2010 08:48:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_NamedReportsGroups](
    [ReportGroupId] [varchar](1) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[tbl_NamedReportsGroups] ADD [ReportRule] [varchar](32) NOT NULL
ALTER TABLE [dbo].[tbl_NamedReportsGroups] ADD [FilterVar] [varchar](32) NOT NULL
ALTER TABLE [dbo].[tbl_NamedReportsGroups] ADD [FilterRule] [varchar](32) NOT NULL
ALTER TABLE [dbo].[tbl_NamedReportsGroups] ADD [RecipientRule] [varchar](32) NOT NULL
ALTER TABLE [dbo].[tbl_NamedReportsGroups] ADD [GroupIdentity] [int] IDENTITY(1,1) NOT NULL
/****** Object:  Index [PK_tbl_NamedReportsGroups]    Script Date: 08/25/2010 08:48:17 ******/
ALTER TABLE [dbo].[tbl_NamedReportsGroups] ADD  CONSTRAINT [PK_tbl_NamedReportsGroups] PRIMARY KEY CLUSTERED 
(
    [ReportGroupId] ASC,
    [ReportRule] ASC,
    [FilterVar] ASC,
    [FilterRule] ASC,
    [RecipientRule] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[tbl_FoldersRules]    Script Date: 08/25/2010 08:45:29 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[tbl_FoldersRules](
    [FolderName] [varchar](32) NOT NULL,
    [ReportGroupId] [varchar](32) NOT NULL,
    [FORBID] [bit] NOT NULL,
    [RuleId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_tbl_FoldersRules] PRIMARY KEY CLUSTERED 
(
    [FolderName] ASC,
    [ReportGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_FoldersRules] ADD  CONSTRAINT [DF_tbl_FoldersRules_FORBID]  DEFAULT ((0)) FOR [FORBID]
GO

