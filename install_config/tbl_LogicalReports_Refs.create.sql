USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_LogicalReportsRefs]    Script Date: 12/10/2012 14:17:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReportsRefs]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_LogicalReportsRefs]
GO

USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_LogicalReportsRefs]    Script Date: 12/10/2012 14:17:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_LogicalReportsRefs](
	[JobReportId] [int] NOT NULL,
	[ReportId] [int] NOT NULL,
	[textId] [int] NOT NULL,
	[userTimeRef] [datetime] NOT NULL,
	[FilterVar] [varchar](64) NULL,
	[lastUserTocheck] [varchar](64) NULL,
	[lastUserToView] [varchar](64) NULL,
    [Printable] bit NULL,
	[Printed] [datetime] NULL,
 CONSTRAINT [PK_tbl_LogicalReportsRefs] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC,
	[ReportId] ASC
) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

