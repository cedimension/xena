@SETLOCAL

@call :startsvcs ADDES
@call :startsvcs CERIT
@call :startsvcs ETR
@call :startsvcs GERIT
@call :startsvcs NOMOS
@call :startsvcs POLIS
@call :startsvcs SARDEGNA

@goto :EOF

:startsvcs

@sc start XR_%1_GTWL%COMPUTERNAME:~-1%1
@sc start XR_%1_GTWR%COMPUTERNAME:~-1%1
@sc start XR_%1_GTWR%COMPUTERNAME:~-1%2
@sc start XR_%1_PDF%COMPUTERNAME:~-1%1
@sc start XR_%1_PDF%COMPUTERNAME:~-1%2
@sc start XR_%1_INI%COMPUTERNAME:~-1%1

@ENDLOCAL

@goto :EOF

