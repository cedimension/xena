#!/usr/bin/perl -w

package CTDParser;

use strict qw(vars);

use Data::Dumper;
use IO::File;
use File::Basename;
use XML::Simple;
use Digest::SHA1;
use Encode qw(from_to);
use Data::Alias qw(alias copy);

use XReport;
use XReport::METABASE::SQLServer;

use re 'eval';

sub i::logit { warn localtime().' - '.join('', @_)."\n"; }

sub XML::Simple::sorted_keys {
    my ($self, $name, $href) = @_;
    my $hkeys = {map {$_ => $_ } keys %{$href} };
    my @klist = ();
    my $knownkeys = {
          jobreport => [qw(cateList linePos rect exec index report page)]
        , page      => [qw(category bundles holddays contid test vars op)]
        , vars      => [qw(constant unpack concat)]
        , op        => [qw(test bundles indexes getVars)]
        , receiver  => [qw(DEBUG CASE)]
        , CASE      => [qw(MATCH ATTRIB ASSERT)]
        , 'index'   => [qw(table entries vars)]
    }; 

    push @klist, delete($hkeys->{name}) if ( exists($href->{name}) );
    if ( exists($knownkeys->{$name}) ) {
        foreach my $key ( @{$knownkeys->{$name}} ) {
            push @klist, delete($hkeys->{$key}) if ( exists($href->{$key}) );
        }
    }

    push @klist, keys %{$hkeys};
    return (@klist);    
}

sub new {
    my $class = shift;
    my $self = { @_ };
    my $thispath = dirname $0;    
    (@{$self}{qw(filtervars varxlate tree bkupclasses defaultrename)}, my $exceptions, my $uassign) 
                = @{XMLin($thispath.'/CTDconfig.xml',
                        ForceArray => ['exceptions', 'split', 'skip', 'assignuser', 'ASSERT', 'skipre'],
                        KeepRoot => 0, 
                        KeyAttr => ['ctdcol', 'varname', 'name'], 
                        VarAttr => 'ctdcol',
                        ValueAttr => ['ctdjobmask'],
                        ContentKey => '-xrcol'
                        )}{qw(filtervars varxlate tree backupcategories defaultrename exceptions assignuser)};
   $self->{metabase} = XReport::METABASE::SQLServer->new(skiptables => 1);
   
   i::logit("PROCESS CONFIG LOADED - ", Dumper(\{STRUCT => $self, EXCEPTIONS => $exceptions, ASSIGNUSER => $uassign}));
   
   
   if ( exists($self->{tree}->{skip}) && (my $treeskip = delete($self->{tree}->{skip}) ) ) {
      my $skipmask = join('|', map { $_->{rmask}}  @{$treeskip});
      $self->{tree}->{skipmask} = qr/(?:$skipmask)/;
   }
   if ( exists( $exceptions->{forcemissing} ) ) {
      my $skipmask = join('|', map { $_->{jrnmask}}  @{$exceptions->{forcemissing}->{skip}});
      $self->{forcemissing}->{skip} = qr/(?:$skipmask)/;
   }
   if ( exists( $exceptions->{jobnmatch} ) ) {
      my $axx = '';
      my $jobnmask = join('|', map { $_->{jobnmask} 
                                     . "(?\{ \$main::rematch = '$_->{jobnmask}'; })" 
                                   } sort { $b->{jobnmask} cmp $a->{jobnmask} } @{$exceptions->{jobnmatch}->{re}});
      $self->{jobnre} = qr/(?:$jobnmask)/x;
      $jobnmask = join('|', 
           map { $_->{jobnmask} } sort { $b->{jobnmask} cmp $a->{jobnmask} } @{$exceptions->{jobnmatch}->{skipre}});
      $self->{jobnre2skip} = qr/(?:$jobnmask)/x;
   }
   my $code = "sub { my \$recipient = shift;\n";
   if (ref($uassign) eq 'ARRAY' && scalar(@$uassign)) {
     $code .= join("\n", map {
        "if (\$recipient =~ /".$_->{recipient}."/) { return ".$_->{xrcol}."; }"
                    } @$uassign) . "\n";
   }
   $self->{userassign} = eval $code."return \$recipient;\n}";
   
   die "User assign code generation failed - $@\n", Dumper(\{uassign => $uassign, code => $code}), "\n" if $@;
        
    $self->{tables} = $self->{metabase}->readTablesMetaData(
       initrows => 1,
       include => [ qw(JobReportNames ReportNames Os390PrintParameters 
                             VarSetsValues NamedReportsGroups FoldersRules Folders  
                             ProfilesFoldersTrees Profiles UsersProfiles Users UserAliases
                             BundlesNames LogicalReportsTexts) ]);
    
                        
    $self->{filtervarslist} = [ reverse keys %{$self->{filtervars}} ];
    
    die "Print Missions prtmissdir directory not defined" unless $self->{prtmissdir};   
    die "Report  Defin. reportsdir directory not defined" unless $self->{reportsdir};   
    die "Out  Directory outputdir  directory not defined" unless $self->{outputdir};    

    my $ctdtree_fn = $self->{ctdtreefil} || die "ctdtree file name not defined";
    $self->{treefh} = new IO::File("<$ctdtree_fn") || die "Unable to open $ctdtree_fn";
    $self->{treefh}->binmode();

    my $ctdperm_fn = $self->{ctdpermfil} || die "ctdperm file name not defined";
    $self->{permfh} = new IO::File("<$ctdperm_fn") || die "Unable to open $ctdperm_fn";
    $self->{permfh}->binmode();

    opendir( $self->{dirfh}, $self->{reportsdir} ) || die "unable to open Directory \"$self->{reportsdir}\"";
    opendir( $self->{pmdirfh}, $self->{prtmissdir} ) || die "unable to open Directory \"$self->{prtmissdir}\"";
    
    return bless $self, $class;
} 



sub getFoldersList {
=pod

=head1 getFoldersList

partendo da un'arbitrario recipient(folder) (primo parametro) cerca tutti i livelli inferiori
con livello uguale a quello specificato nel secondo parametro di chiamata e restituisce un array
contenente tutti gli hash parent delle entrate corrispondenti al criterio

=cut 
    my $self = shift;
    my ($rootname, $lvl, $varname, $parents) = @_;
    die "Attempt to build $rootname varlist without FilterVar", 
            " - called by ", join('::', (caller())[0,2]), "\n" unless $varname; 
    die "do user lvl* called with not numeric $lvl ", 
            " - called by ", join('::', (caller())[0,2]), "\n" unless $lvl =~ /^\d+$/; 
        
    alias my $root = $self->{recipients}->{$rootname};
    foreach my $user ( keys %{$root->{_chlds}} ) {
        alias my $folder = $self->{recipients}->{$user};
        die "malformed folder: ", Dumper($folder), unless ($folder->{lvl} && $folder->{lvl} =~ /^\d+/ );
        $self->getFoldersList($folder->{name}, $lvl, $varname, $parents) 
               if ( $folder->{lvl} < $lvl && exists($folder->{_chlds}) && scalar(keys %{$folder->{_chlds}}) );
=head2 process recipient synonims

we add an entry to varsetvalues for each synonym defined in the tree 
plus the recipient name itself

=cut
   
        if ($folder->{lvl} && $folder->{lvl} == $lvl) { 
            $parents->{$root->{name}} = $root;
            if ( exists($folder->{filtervals}) && scalar(@{$folder->{filtervals}})) {
               foreach my $varval ( @{$folder->{filtervals}} ) { 
                  next unless $varval;
                  $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
                              {VarSetName => $folder->{name}, VarName => $varname, VarValue => $varval });
               }            	
            }
            $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
                  {VarSetName => $folder->{name}, VarName => $varname, VarValue => $folder->{name} });
                  
            $self->{metabase}->addRows2Table($self->{tables}->{NamedReportsGroups}, 
                  {ReportGroupId => $folder->{name}, ReportRule => $parents->{__firstname__}, 
                                           FilterRule => $folder->{name}, FilterVar => $varname});
            $self->{metabase}->addRows2Table($self->{tables}->{FoldersRules},
                         {FolderName => $folder->{FolderName}, ReportGroupId => $folder->{name}});              
        }
    }
    return $parents;
}

sub getFilterVarName {
    my ($self, $jn) = (shift, shift);
    return undef unless $jn;
    my $filtervar = (grep { $jn =~ m/$self->{filtervars}->{$_}/ } @{$self->{filtervarslist}})[0];
    die "Filtervar not found for $jn" unless $filtervar && $filtervar ne '';
    return  $filtervar;
}

sub xlateCtdColumn {
=pod
=h2 translate column names

CTD index variables are named in such a way that provides some sort of explanation on 
what is expected to type in when searching. Is possible to normailize such names 
using elements ( "varxlate" ) of the xml configuration ("CTDConfig.xml").
e.g:
   <varxlate ctdcol="ctd var name" xrcol="XReport col name">
   
the original variable name is returned if not found in config   

=cut
    my $self = shift;
    alias my $vardescr = shift;
    if ( !exists($self->{varxlate}->{$vardescr}) ) {
        #warn "Index col $vardescr not managed\n";
        return $vardescr;
    }
    return $self->{varxlate}->{$vardescr};
}

sub reprec_R {
=pod

=head2 report definitions Record analyzer routines

=cut

    my ($self, $rline, $memlines) = (shift, shift, shift);

    my ( $jname, $owner, $catname, $var1 ) = unpack("A8 x20 A8 A20 A8", $rline);
    # category name can include special chars that ar not supported by xml - let's get rid of'em
    $catname =~ s/[^\d\w\s\*]/_/g;

    if ( exists($self->{currcat})) {
        my $category = delete($self->{currcat});
        #TODO verify following
#        $self->{cats}
#            ->{$category->{catname}} 
    }
    
    $self->{currcat} = {};
    $self->{defaults} = {};
    @{$self->{currcat}}{qw(jobname owner catname var1 defaults)} 
        = ( $jname, $owner, $catname, $var1, $self->{defaults});
    
    while ( scalar(@$memlines) && $memlines->[0] =~ /^\s*([^RN])(.*)$/ ) {
    	# process records before N and up to next R
        my $result = $self->reprecDispatcher( shift @{$memlines}, $memlines );
        #return undef unless defined($result);
    }
    # at this point we have N|R|eof
    return $self ;
}

sub reprec_F {
=h2 Defaults statement

 defaults specifications contains default copies and recipient 
 for the jobs matched by the "R" record

F01SCARTI

=cut

    my ($self, $rline, $memlines) = (shift, shift, shift);
    @{$self->{defaults}}{qw(copies destuser)} = unpack("A2 x4 A8", $rline);
    return $self;
    
}

sub reprec_N { # ON Statement
=h2 "ON" statement

job attributes selection statement

NCLASS
NDSN
NTRNDSN

=cut

    my ($self, $rline, $memlines) = (shift, shift, shift);
    my $jrn = $self->{currcat}->{jobname};
    $jrn = $self->{currmember} if $jrn =~ /\?/;
    my $jrdefs = {};

    while ( $self->{memlines}->[0] =~ /^X/ ) {
        (my $cont) = ((shift @{$self->{memlines}}) =~ /^.(.+?)\s*$/ )  ;
        $rline =~ s/\s+$//g;
        
        $rline .= ($rline =~ /,$/ ? '' : ',').$cont if $cont;
    }
    $rline =~ s/,\s*$//g;
    if ( $rline =~ /^CLASS.{27}(.+)$/ ) {
        @{$jrdefs}{qw(_C _W _D _F)} = unpack("A8 A8 A8 A4", $1);
    } 
    elsif ( $rline =~ /^(?:DSN|TRNDSN)\s+([^\s].+)\s*$/ ) {
        $jrdefs = { map { /^\s*([^\s]+)\s*$/; $1 =~ /^(?:PGMSTEP|PROCSTEP)/ ? '_S' 
                                            : $1 =~ /^DDNAME/ ? '__' 
                                            : $1 } split /[\=,]/, uc($1) };
    }

    $jrn = join('.', $jrn, grep { $_ } map { $jrdefs->{$_} ? $_.$jrdefs->{$_} : undef } qw(_C _W _D _F _S __));
    my $filtervarname = $self->getFilterVarName($jrn);
    my $jrinfos = { cat => $self->{currcat}, catlist => {},
            JobReportName => $jrn, pages => {},pagcnt => 0, FilterVar => $filtervarname };
    $self->{jobreports}->{$jrn} = $jrinfos unless exists($self->{jobreports}->{$jrn}); 

    alias $self->{currjobreport} = $self->{jobreports}->{$jrn};
    $self->{currjobreport}->{doforce} = 'ROOT';
    die "CURRJR: ", Dumper($self->{currjobreport}), "\n" unless $self->{currjobreport}->{FilterVar};
    @{$self->{currjobreport}}{qw(prtcopies)} = @{$self->{defaults}}{qw(prtcopies)};
    my $result;  
    while ( scalar(@$memlines) && $memlines->[0] =~ /^\s*([^WNRT\s])(.*)$/ ) {
    	my $result = $self->reprecDispatcher(shift @{$memlines}, $memlines);
       return undef unless defined($result);
    }                 
    while ( scalar(@$memlines) && $memlines->[0] =~ /^\s*([^NR\s])(.*)$/ ) {
       my $currpage = $self->reprecDispatcher(shift @{$memlines}, $memlines,$self->createNewPage());
       return undef unless defined($self->pagePostProcess($currpage));
    }
    
    return $self;   
}


sub pagePostProcess {
	my ($self, $currpage) = @_;
    my ($rectid, $rectval, $uref) = map { $_ ? $_ : '' } @{$currpage}{qw(test rectval UserRef)};
    $uref = '__FROMDBDEF__' unless $uref;
    $rectid = $self->{currcat}->{catname}.'.'.$rectid;
    $rectid .= '.'.$uref;
    if ( exists( $self->{currjobreport}->{pages}->{$rectid} )) {
        alias my $opage = $self->{currjobreport}->{pages}->{$rectid};
        if (exists($opage->{bundles}) && exists($currpage->{bundles}) ) {
            $opage->{bundles} = { %{$opage->{bundles}}, %{$currpage->{bundles}} };
        }
        elsif ( exists($currpage->{bundles}) ) {
            $opage->{bundles} = { %{$currpage->{bundles}} };
        }
        alias my $oldops = $opage->{op};
        foreach my $op ( @{$currpage->{op}} ) {
            push @{$oldops}, $op 
               unless grep { $_->{getVars} eq $op->{getVars} 
                   && $_->{test} eq $op->{test} } @{$oldops};
        }
        foreach my $oldix ( 0..$#{$oldops} ) {
           alias my $oldop = $oldops->[$oldix];
           my $op = (grep { $_->{getVars} eq $oldop->{getVars} 
                   && $_->{test} eq $oldop->{test} } @{$currpage->{op}})[0];
           next unless exists($op->{bundles});
           $oldop->{bundles} = {} unless exists($oldop->{bundles});
           $oldop->{bundles} = { %{$oldop->{bundles}}, %{$op->{bundles}}}
        }
    }
    else {
        $currpage->{category} = $self->{currcat}->{catname};
        $self->{currjobreport}->{pagcnt} += 1;
        $currpage->{name} = $self->{currcat}->{catname}.'.'.sprintf('PAG%03d', $self->{currjobreport}->{pagcnt});
        $self->{currjobreport}->{pages}->{$rectid} = $currpage;
    }
    $self->{currjobreport}->{catlist}->{$self->{currcat}->{catname}} = scalar(keys %{$self->{currjobreport}->{catlist}}) * 10;
	return $self->{currjobreport}->{pages}->{$rectid};
}


sub reprec_W { # WHEN STATEMENT
=h2 WHEN Statement

report page content selection

Wlllllssssseeeee   [A|O| ]
Y...........

=cut

    my ($self, $rline, $memlines) = (shift, shift, shift);
    my $currpage = shift;

    my $jrn = $self->{currjobreport}->{JobReportName};
    delete $currpage->{initialdummy};
#    if ( !$currpage || $currpage->{initialdummy} ) {
#       $currpage = { test => '', rectval => '' };
#    }
    my $bool = ''; my $testvals = []; my $ops = [];
=head2 AND/OR

Concatenation parameter. If specified, another WHEN line is
opened. Valid values are:

A (And) � The page is identified only if both WHEN conditions
are true. Use this value when two or more distinct strings on the
report page are required to accomplish the identification.

O (Or) � The page is identified if either of the WHEN conditions
is true. Use this value if the identification strings may reside in
more than one distinct area on the report page.

If both A and O are specified in the same WHEN statement, the
strings concatenated by A are evaluated first.

=cut    
    do {
        die "unexpected eof at line: W$rline\n" if !scalar(@$memlines);
        die "When line \"W\" without match \"Y\" line: W$rline\n" if ( $memlines->[0] !~ /^Y/ && $rline !~ /^\s*$/ );
    
        my $Yline = substr(shift @$memlines, 1) if $memlines->[0] =~ /^Y/;
        my ($match, $oper, $quote, $string, $Ystrlen) = ({rect => '_NULL_'}, '', '', ($currpage->{rectval} ? '__BLANK__' : ''), 0);
        if ( $rline !~ /^\s*$/ ) {
            (@{$match}{qw(froml tol fromc toc)}, $bool, my $contid) = ($rline =~ /^(\d{5})(\d{5})(\d{5})(\d{5})(.)..(.)/ );
            $contid = 'N' unless $contid && $contid eq 'Y';
=head2 contid

N (No) � Every page must be identified. Default if REF NEXT
PAGE is set to N or blank. Pages that do not contain an
identifying string are directed to the default recipient

Y (Yes) � Continue identification to the next pages. Default if
REF NEXT PAGE is set to Y. Unidentified pages are directed to
the �user� identification obtained from the previous pages
under the same ON statement.

=cut
            $match->{rect} = sprintf('%05d%05d%05d%05d', @{$match}{qw(froml tol fromc toc)});
            $bool = '' if $bool eq ' ';
            $currpage->{whenline} = $match->{froml} unless exists($currpage->{whenline});
            $currpage->{contid} = $contid unless exists($currpage->{contid});
        
            $Ystrlen = ($match->{toc} - $match->{fromc} + 1);
            $match->{filterval} = substr($Yline, 0, $Ystrlen ); # =~ s/\s+$//g;
            my $setname = 'VAL'.substr('00000'.$match->{filterval}, -5);

=head2 match string

can be 1 through 50 characters.

The string can contain blanks, single quotes and hex format. When
not enclosed in single quotes, the length of the string to be
compared is measured from the start of the field to the last
non-blank character.
Example:
MYVAL1
' EMP '
x'hhhhhh'
%%vtitle
.[op-comp].{string| {%%var2 | %%system_var}[(<startpos>,<length>)]}

op-comp is the comparison operator. The following operators are
supported:
GT �Greater than
LT � Less than
GE �Greater than or equal to
LE � Less than or equal to
EQ �Equal to
GL � Not equal to
NE � No matching value was found in the range. The line for the next set of DO statements cannot be determined.

=cut

            ($oper, $quote) = ($Yline =~ /^(?:\.(NE|EQ|LT|GT|LE|GE)\.)?(')?([^']+)\2?\s*$/);
            $oper = 'EQ' unless $oper;
            $string = ($quote ? $3 : unpack("A*", $3)) if $3;
            #$string =~ s/\>/\\x3E/g;
            #$string =~ s/\</\\x3C/g;
            $self->{currjobreport}->{rect}->{'R'.$match->{rect}} =  
                            { coord => '('.join(',', map { s/^0+//; $_ } @{$match}{qw(froml fromc tol toc)} ).')' };
        }
        if ( $match->{rect} ne '_NULL_') {
        
           if ((length($string) != $Ystrlen or $match->{froml} != $match->{tol} ) and ($oper =~ /^(?:NE|EQ)$/ )) {

              $currpage->{test} .= '(('.join(',', map { s/^0+//; $_ } @{$match}{qw(froml fromc tol toc)} ).')'
                                . ($oper eq 'EQ' ? "." : "!").$string.")";
           }
           else {
             $currpage->{test} .= '(R'.$match->{rect}.".".($oper || 'EQ').".'$string')";
        }
           $currpage->{test} .= ($bool =~ /^A/i ? '.AND.' : $bool =~ /^O/i ? '.OR.' : '');
        }
    
        elsif ( $currpage->{test} ) {
            die "Empty condition for composite WHEN - LINE: W$rline in $jrn";
        }
        else {
            $currpage->{test} = 1;
        }

        push @{$testvals}, $string if $string;
        if ( $bool && $memlines->[0] =~ /^W(.*)$/ ) {
            $rline = substr(shift @$memlines, $-[1]);
        }
        elsif ( $bool ) {
            die "more WHEN lines expected in $self->{currmember}: for line: W$rline - found: $memlines->[0]\n";
            return undef;
        }
        elsif ( !$bool && $memlines->[0] =~ /^W(.*)$/ ) {
            $bool = 'O';
            $rline = substr(shift @$memlines, $-[1]);
        }
        elsif ( !$oper ) { 
        }

    } while ( $bool) ;
    if ($memlines->[0] =~ /^W/) {
    	i::logit("Unexpected WHEN record in $jrn - LINE: W$rline");
        return undef;
    }
    if ( $currpage->{test} ne '1' ) {
       my $varval = $currpage->{test}; 
       $varval = Digest::SHA1::sha1_base64($varval) if length($varval) > 30;
       push @{$currpage->{setvars}}, {name=> "$self->{currjobreport}->{FilterVar}", constant=> "$varval"};
    }
    $currpage->{rectval} = join("\t", @{$testvals});
    push @{$currpage->{op}}, $ops->[0] if scalar(@{$ops}) > 0; 
    while ( scalar(@$memlines) && $memlines->[0] =~ /^\s*([^WNR\s])(.*)$/ ) { 
        my $result = $self->reprecDispatcher(shift @$memlines, $memlines, $currpage);
        return undef unless defined($result);
    }
    return $currpage;    
}

sub setBundle {
    my $self = shift;
    my ($loc, $pmiss) = (shift, shift);
    $loc->{bundles}->{$pmiss} = $self->{currjobreport}->{prtcopies} 
                  ; #unless $self->{currjobreport}->{prtcopies} && $self->{currjobreport}->{prtcopies} !~ /^\0+$/;

}

sub reprec_P { # PRINT STATEMENT
=head2 PRINT COPIES

=cut

    my ($self, $rline, $memlines) = (shift, shift, shift);
    my $currpage = shift;

      my ($prtcopies, $prtuser) = unpack("A2 x2 A8", $rline);
      $self->{currjobreport}->{prtcopies} = $prtcopies if $prtcopies =~ /^\d+$/;
      $self->{currjobreport}->{prtuser} = $prtuser if $prtuser !~ /^\s*$/ && $prtcopies !~ /^0+$/ ;
      return $self;
      
}

sub addRect {
=head2 XReport rectangle specification insertion

=cut

    my ($self) = (shift);
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};
    
    my ($strline, $strscol, $strecol) = @_;
    my $rectid = sprintf('R%05d%05d%05d%05d', ($strline, $strline, $strscol, $strecol));
    $jobreport->{rect}->{$rectid} =  { coord => '('.join(',', map { s/^0+//; $_ } ($strline, $strscol, $strline, $strecol) ).')' };
    return $rectid;
}

sub solveConstant {
=head2 internal Constant names 

#   EXTWTR
#   FILE
#   JOBNAME
#   USERID

=cut

   my ($self, $varv) = (shift, shift);
   if ( $varv =~ /^\%\%([^\s]+)/ ) {
     my $vs = $1;
     my ($vn, $vp, $vl ) = ( $vs =~ /^([^\(\s]+)(?:\((\d+),(\d+)\))?/ );
     my $vpl = '';
     $vpl = '('.($vp - 1).",$vl)" if ( $vp ); 
     my $varspec = ( $vn eq 'JOBNAME' ? 'JobName'.$vpl
                   : $vn eq 'JOBID' ? 'JobNumber'.$vpl
                   : $vn eq 'FILE' ? 'RemoteFileName'.$vpl
                   : $vn eq 'USER' ? 'XferRecipient'.$vpl
                   : $vn eq 'EXTWTR' ? 'WRITERN'.$vpl
                   : $vn eq 'DDNAME' ? 'DDNAM'.$vpl
                   : $vn.$vpl);
                   
     return { elabvalue => $varspec };
   } 
   else {
     return { constant => $varv };
   }
   return undef; 
}

sub addSetVars {
   my ($self, $currpage) = (shift, shift);
   my ($varn, $strings) = @_;
   
   return 0 unless scalar(@{$strings});
   $currpage->{UserRef} .= join('.', map { values %{$_} } @$strings) if $varn eq 'UserRef'; 
   my $numset = scalar(@{$strings});
   my $setvars = { name => $varn, %{shift @{$strings}} };
   $setvars->{concat} = $strings if scalar(@{$strings});
   push @{$currpage->{setvars}}, $setvars;
   return $numset;
}

sub ctdDONAME {
=h1. DO NAME
DO Name cases
TNAME string
   -  sets report name to the specified string
TNAME * rrrrrssssseeeee
   -  sets report name to the string found at pos 
      in report
TNAME *+*P string|rrrrrssssseeeee|%%varname
   -  sets the report name to this spec concatenated to the prev do name       
TNAME *P+* string|rrrrrssssseeeee|%%varname
   -  sets the report name the prev do name concatenated to this spec       
=cut    

    my ($self, $args) = (shift, shift);
    my ($memlines, $currpage) = @_;
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};

    my $strings = [];
    $currpage->{UserRef} = '';
    while (1) {
      my ($rname, @strp) = unpack( ( $args =~ /.{20}[^\d]\d/ ? 'x A20 (A3)3' 
                                    : $args =~ /.{20}\d/ ? 'A20 (A5)3' : 'A35'), $args);
      my ($concat, $text) = ( $rname =~ /^\*(\+\*P|P\+\*|P\+(?=%%))?(.*)$/ );
      
      if ( $rname !~ /^\*/ ) {
        if ( !exists($jobreport->{JobReportDescr}) && $rname !~ /%%[^\s]+/ ) {
           $jobreport->{JobReportDescr} = $rname;
        }
        elsif ( $rname =~ /%%[^\s]+/ || $jobreport->{JobReportDescr} ne $rname ) {
           push @{$strings}, map { $self->solveConstant($_) } grep( !/^\s*$/, split( /(%%[^\s]+)/, $rname));
        }
      }
      else {
         die "DO NAME * Requested but rect missing in $jobname - RLINE: \"TNAME   $args\" CURRENT PAGE: "
                                   , Dumper($currpage), "\n" if !scalar(@strp);
         my $rectid = sprintf('R%05d%05d%05d%05d', @strp[0,0,1,2]);
         my $strlen = ($strp[2] - $strp[1] + 1);
         $jobreport->{rect}->{$rectid} =  { coord => '('.join(',',map { s/^0+//; $_ } @strp[0,1,0,2] ).')' };
         if ( !$concat ) {
            push @{$strings}, { unpack => "$rectid.A$strlen" };
            push @{$strings}, $self->solveConstant($text) if $text !~ /^\s*$/;
            
         }
         else {
            if ( $concat =~ /^(?:\+\*P|P\+\*)$/ ) {
               if ( !scalar(@{$strings}) && exists($jobreport->{JobReportDescr}) ) {
                  push @{$strings}, { constant => $jobreport->{JobReportDescr} };
               } 
               die "REQUIRED PREV DO NAME missing - CURRENT PAGE: ", Dumper($currpage), "\n" 
                                                               unless scalar(@{$strings});
               my $prev = pop @{$strings};                                                                 
               push @{$strings}, ( $concat eq '+*P' ? ( { unpack => "$rectid.A$strlen" }, $prev )
                                             : ( $prev, { unpack => "$rectid.A$strlen" } )  );
               if ( $text && $text !~ /^\s*$/ ) {
                  $text =~ s/\s+$//g;
                  push @{$strings}, map { $self->solveConstant($_) } grep( !/^\s*$/, split( /(%%[^\s]+)/, $text));
               }
            }
            elsif ( $concat eq 'P+' ) {
               $text =~ s/\s+$//g;
               push @{$strings}, map { $self->solveConstant($_) } grep( !/^\s*$/, split( /(%%[^\s]+)/, $text));
            }
         }                                   
      }
      last unless (scalar(@{$memlines}) && $memlines->[0] =~ /^TNAME/);
      $args = substr(shift @{$memlines}, 8); 
    }
    my $numstr = $self->addSetVars($currpage, 'UserRef', $strings) if scalar(@{$strings});
    
    return $self;
}

sub ctdDOPRINT {
=h1. DO PRINT

TPRINT {mission}

print mission destination

=cut    

    my ($self, $args) = (shift, shift);
    my ($memlines, $currwhen) = @_;
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};
      ( my $pmiss =  unpack("x7 A8", $args)) =~ s/\s+$//g;
        $self->setBundle((exists($currwhen->{op}) ? $currwhen->{op}->[-1] : $currwhen), $pmiss); 
    return $self;
}


=h2 parse ctd coordinates

ctd coordinates could be specified using 3 or 5 digit fields.

=cut
$::posRE = qr/\s*(\d+?)?\*\s+([^\s].+)?/o;


sub ctdDOSET {
=h1 DO SET
   DO SET Format:
   A7  # the string "SET    "
   A8  # varname
   A1  # if = 'Y' the concatenate with next
   A1  # CASE op L: lower U: Upper ' ': no op
   A1  # string type C: Const in next Yrec X: Xpath in next Yrec *: get from pos
   A15 # position within page lllllssssseeeee
=cut

    my ($self, $args) = (shift, shift);
    my ($memlines, $currwhen) = @_;
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};

    my $strings = [];
    my $varn;
    while ( my ($currvar, $concat, $case, $type, $ln, $sc, $ec) = unpack('A8 (A1)3 (A5)3', $args) ) {
        $varn = $currvar unless $varn;
        die "concatenation not supported for different var names in $jobname \"$currvar\" ne \"$varn\"\n" unless $currvar eq $varn; 
        die "SET Type $type Not supported " unless $type =~ /^[CX\*]$/; 
        die "Required \"Y\" line missing for DO SET record\n" 
                                      if $type =~ /^[CX]$/ && $memlines->[0] !~ /^Y/;
        if ( $type =~ /^[CX]$/ ) {
          push @{$strings}, $self->solveConstant(unpack('xA60', shift @$memlines));
        }
        else {
          my $rectid = $self->addRect($ln, $sc, $ec);
          push @{$strings}, {unpack => "$rectid.A".($ec - $sc + 1)};
        }
        last unless $concat && $concat eq 'Y';
        die "No set record to satisfy required concatenation" if $memlines->[0] !~ /^TSET/;
        $args = substr(shift @{$memlines}, 8);
    }
    return $self->addSetVars($currwhen, $varn, $strings);

}

sub buildRectDef {
    my ($self, $orient, @coord) = @_;
    my $rectdef = { name => sprintf('R%05d%05d%05d%05d', @coord)
                  , coord => sprintf('(%d,%d,%d,%d)', @coord[0,2,1,3])
                  };
    $rectdef->{Orient} = $orient if defined($orient);
    return $rectdef;
}

sub parseWebConfig {
    my ($self, $currwhen, $douser, $memlines, $wfn) = @_;
    my $rrule = $douser;
       if ( $douser && -e $wfn ) {
#          warn "RETRIEVING WEB CONFIG $douser from \"$wfn\"\n";
          my $wfh = new IO::File("<$wfn") || die "Unable to open $wfn";
          $wfh->binmode();
          $wfh->read(my $cfgstream, -s $wfn);
          $wfh->close();
          my @lines =  grep { $_ !~ / *do +set +JOBNAME /i } split(/\n/, $cfgstream);
          my @outlines = ();
          $currwhen->{webconfig}->{$douser} = {} unless exists($currwhen->{webconfig}->{$douser});
          alias my $cfg = $currwhen->{webconfig}->{$douser};
          alias my $webwhen = $cfg->{when}->[-1] if exists($cfg->{when});
           
          while ( scalar(@lines) ) {
             my $line = shift @lines;
             $line .= ' '.shift @lines if $line =~ /^ *when *$/i;
             if ( $webwhen 
                && exists($webwhen->{indexes}) 
                && ($line =~ /^ *when +([^ ].*)$/i || !scalar(@lines) ) ) {
                push @{$webwhen->{op}}, { test => 1, getVars => '__IDUMMY__', unpack => '(1,2,1,3).a2', 
                    indexes => join(' ', sort @{delete $webwhen->{indexes}} ) };
             }
             if ( $line =~ /^ *when +([^ ].*)$/i ) {
                push @{$cfg->{when}}, {};
                alias $webwhen = $cfg->{when}->[-1];
                my $whenstr = $1;
                $webwhen = ( $whenstr =~ /^ *True *$/ ? { test => 1 } : 
                  { ( $whenstr =~ /((?:Line|Col|Orient)) +((?:\d+,\d+|\d+))/ig )
                  , ( $whenstr =~ /(String) *= *"([^"]+)"/ ? ( $1, $2 ) : () ) } );
                if ( !exists($webwhen->{test}) ) {
                   push @{$cfg->{rect}}, 
                     $self->buildRectDef((exists($webwhen->{Orient}) ? delete $webwhen->{Orient} : undef)
                           ,( split(/,/, delete $webwhen->{Line}), split(/,/, delete $webwhen->{Col} ) ));
                   if ( exists($webwhen->{String}) ) {
                      $webwhen->{test} = '('.$cfg->{rect}->[-1]->{name}.".EQ.'".$webwhen->{String}."')";
                      delete $webwhen->{String};
                   }        
                }
             }
             elsif ( $line =~ /^ *do +set +\* ([^ ]+) ([^ ].*)$/i ) {
                my ($varnam, $setstr) = ($1, $2);
                my $setvar = { name => $varnam, ( $setstr =~ /((?:Line|Col|Orient)) +((?:\d+,\d+|\d+)) ?/ig ) };
                push @{$cfg->{rect}}, 
                $self->buildRectDef((exists($setvar->{Orient}) ? delete $setvar->{Orient} : undef)
                      ,( $setvar->{Line}, delete $setvar->{Line}, split(/,/, delete $setvar->{Col} ) ) );
                $setvar->{unpack} = $cfg->{rect}->[-1]->{name}.'.A*';      
                push @{$webwhen->{setvars}}, $setvar;
             }
             elsif ( $line =~ /^ *do +name +([^ ].*+)$/i ) {
                my ($setstr) = ($1);
                $setstr =~ s/^"|"$//g;
                my ($fval, @strings) = grep !/^\s*$/, split(/"/, $setstr);
                my @setval = ($fval =~ /^%%/ ? ( elabvalue => substr($fval, 2) ) : ( constant => $fval ) );
                push @{$webwhen->{setvars}}, { name => 'UserRef', (@setval) };
                while ( scalar(@strings) ) {
                    my $val = shift @strings;
                    $webwhen->{setvars}->[-1]->{concat} = [] unless exists($webwhen->{setvars}->[-1]->{concat});
                    push @{$webwhen->{setvars}->[-1]->{concat}}, 
                          ($val =~ /^%%/ ? { elabvalue => substr($val, 2) } : { constant => $val } );
                }
             }
             elsif ( $line =~ /^ *do +dest(?:group)? +"([^"]+)"/i ) {
                my ($recipient) = ($1);
                if ( $douser && exists($self->{recipients}->{$douser})
                     && exists($self->{recipients}->{$douser}->{FolderName}) && exists($webwhen->{test}) ) {
                   $douser = $self->{recipients}->{$douser}->{parent};
                }
             }
             elsif ( $line =~ /^ +do +index "([^"]+)" (?:line +0 col \d{1,3},\d{1,3} orient \d mask "\*" )?(.*)$/i ) {
                my ($vardescr, $loc) = ( $1, $2 );
                (my $varname = $self->xlateCtdColumn($vardescr)) =~ s/ /_/g;
                my ($ln, $eln, $scol, $ecol, $orient) = ($loc =~ /line (\d{1,3})(?:,(\d{1,3}))? col (\d{1,3}),(\d{1,3}) orient (\d)/i) if $loc;
                $eln = $ln unless defined($eln);
                push @{$cfg->{rect}}, $self->buildRectDef($orient, $ln, $eln, $scol, $ecol);
                my $rectid = $cfg->{rect}->[-1]->{name};
                my $strlen = $ecol - $scol + 1;
                push @{$webwhen->{op}}, { test => 1, getVars => $varname, unpack => "$rectid.a$strlen" };
                $self->{indexcols}->{$varname}->{$rectid} = $vardescr;
                my $indexname = $varname;
                $cfg->{indexes}->{$indexname} = [$varname];
                $cfg->{INDEX}->{$indexname} = { name => $indexname, vars => $varname, entries => 'FIRST', table => $indexname };
                push @{$webwhen->{indexes}}, $indexname;
             }
             elsif ( $line =~ /^ *([^ ]+) +([^ ]+)(?: +([^ ].*))?$/i ) {
                my ($stmnt, $verb, $verbstr) = ($1, $2, $3);
                push @{$webwhen->{unparsed}->{$stmnt}}, { name => uc($verb), args => $verbstr }; 
             }
             push @outlines, $line;
          }
          $cfg->{input} = join("\n", @outlines);
       }
       return $rrule;
}

sub ctdDOUSER {

=h1. DO USER statement

Do user record cases:

TUSER string
   - string specifies the recipient name for the report
TUSER *FORCE-string|*FORCE-*        [ rrr(rr)sss(ss)eee(ee) ]
   - string in this case is the tree level name where to start from when seeking synonims
   - the statement is valid ON Statement wide
TUSER [nn]*        [ rrr(rr)sss(ss)eee(ee) ]
   - nn is the level id (10 to nn) where to seek recipients synonim or names found at 
     row rrr(rr) from col sss(ss) to col eee(ee) of report. 
     Optional, if missing level 10 is assumed or any previous FORCE specified
     if coordinates are omitted they must be taken from the "WHEN" record
     Is possible to have multiple specs for a "WHEN" group, in this case only the first not blank
     string has to be considered
TUSER [nn]%%varname
   - 
=cut    
    my $self = shift;
    my $args = shift;

    my ($memlines, $currpage) = @_;
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};
    
#    my ($lvl, $douser, $strpos, $flags, $usrmsk6, $usrmsk, $parentbranch) 
#                                  = (undef, '', {}, '', 'A2 A20 (A5)3 A*', 'x A2 A20 (A3)3 A*', qr/^.*$/);
#    if ( $args =~ /^.{26}\d{6}/ ) { # we have a rectangle specified
#       ($lvl, $douser, @{$strpos}{qw(lnum fromcol tocol)}, $flags) 
#              = unpack(( $args =~ /^.{32}\d/ ? $usrmsk6 : $usrmsk ), $args);
#       die "PARSE FAILS - MEM: $self->{currmember} LVL: $lvl USER: $douser ARGS: \"$args\" POS: "
#                                                                                , Dumper($strpos), "\n"
#          if ( $strpos->{lnum} =~ /[^\d]/ || $strpos->{fromcol} =~ /[^\d]/ || $strpos->{tocol} =~ /[^\d]/ );
#       $lvl = undef if $lvl =~ /^\s*$/;
#       $strpos->{lvl} = $lvl if defined($lvl);
#       $strpos->{len} = $strpos->{tocol} - $strpos->{fromcol} + 1;
#    }
#    elsif ( $args =~ /^.\d/ ) { # a decimal in the 9th char of the card indicates the presence of lvl in any ver
#       ($strpos->{lvl}, $douser, @{$strpos}{qw(dummy dummy dummy)}, $flags) 
#              = unpack(( $args =~ /^ / ? $usrmsk : $usrmsk6 ), $args);
#       delete $strpos->{dummy};       
#       $lvl = $strpos->{lvl};
#    }
#    else {
#       ($douser, $flags) = ($args =~ /^\s*([^\s]+)(?:\s+([^\s].+)|\s*)$/);
#    }
    if ( $args =~ /^\s*\*FORCE-([^\s]+)\s*/ ) {
=head2 FORCE Statement

This option can be used for defining sub-trees. When the user name starts with the
string *FORCE� followed by either a user name or asterisk (*), special handling
occurs. This means that when two or more DO USER statements are used, the search
for the user name is not outside the �branch� of the tree identified by the user name in
the *FORCE statement. If *FORCE� * is specified, a search is performed for user name
in the specified line or column range. After it is identified and a search is performed
for another user under it in the tree (because of another DO USER statement), the
search is limited to children of the identified user. *FORCE� affects all DO USER
statements in the current ON block even if they are defined in another WHEN block.

=cut
       my $forcename = $1;
       die "DO FORCE-* unsupported " if $forcename eq '*';
          $jobreport->{doforce} = $forcename;
    }
    elsif ( $args =~ /^\s*\*([^\s]{1,8})\s*/ ) { 
=head2 generic user name

Begin the
USER NAME with an asterisk, followed by a member name of 1 through 8 characters.
CONTROL-D then reads the list of users from the specified member, in the library
defined by DD statement DAGENUSR. (parm directory in ctdconfig)

=cut
      my $member = $1;
      if ( $self->{parmsdir} && -d  $self->{parmsdir} && -e $self->{parmsdir}."/$member" ){
         my $pfn = $self->{parmsdir}."/$member";
         my $pfh = new IO::File("<$pfn") || die "Unable to open $pfn";
         $pfh->binmode();
         $pfh->read(my $memstream, -s $pfn);
         $pfh->close();
    
         my $plines = [ map { from_to($_, 'cp37', 'latin1'); substr($_, 44, 1) = 'A'; $_ } 
                          unpack("(a80)*", $memstream) ];
         unshift @{$memlines}, @{$plines};
       
         i::logit("do user member $member processed for $jobreport->{JobReportName} - ".scalar(@{$plines})." processed");
      }
    }
    #TODO web config parsing - chk for file $douser.def
    else {
       my ($rtnName, $douser, $currargs);
       if ( $args =~ /^\s*([^\s\*]*\*)\s+([^\s].+)\s*$/ ) {
          ($douser, $currargs) = ($1, $2); 
#          $rtnName = 'ctdDONORMAL';
          $rtnName = 'ctdDOUSERSTAR';
       }
       else {
          $rtnName = 'ctdDOUSERDIRECT';
          my ($fsearch, $fhard, $fsyn, $fconcat);
          ($douser, $currargs, $fsearch, $fhard, $fsyn, $fconcat) = unpack('A22 a15 (a)4', $args);
          $douser =~ s/^\s+//g;
#       	  ($douser,$currargs) = ( $args =~ /^\s*([^\s\*]+)\s+([^\s]+)?\s*$/ );
       }
#       my $rtnName = 'ctdDO'.($args =~ /^\s*([^\s]*)\*/ ? 'NORMAL' : 'DIRECT');
       my $subh = $self->can( $rtnName );
       die "Unable to find DO User handling routine $rtnName - args: $args" unless ref($subh) eq 'CODE';
       return &$subh($self, $douser, $currargs, @_);
    }
    return $self;
    
    push @{$currpage->{dousers}} , $args;
    while(scalar(@$memlines) && @$memlines[0] =~ /^\s*TUSER(.*)/){
        push @{$currpage->{dousers}} , $1;
        shift @$memlines;   
    }
    if (my $subh = $self->can('ctdDOALLUSERS')) {
            return &$subh($self, $args, @_);
    }
    die "DOALLUSER Routine not found - ".Dumper($currpage->{dousers});
}

sub ctdDOUSERDIRECT {
=head2

a specific folder must contain a specific portion of the report
1. the entire jobreport - when the currpage has no test specified (test = 1)
   in this case lrep 0 must be selected 
   (filterrule :: varsetname = _UNDEF_ filtervar :: VarName = $$$$ filtervalue :: VarValue $$$$)
   the report name must be inserted in the list belonging to the folder ( reportrule :: varsetname = foldernamekey )
2. the portion of the report matched by the when condition (test <> 1)
   in this case we must select the lrep that is identified by the FilterValue = test ( check reprec_W )
   (filterrule :: varsetname = foldernamekey filtervar :: VarName = report var name, filtervalue :: varvalue = test )
   the report name must be inserted in the list belonging to the parent of the folder 
   ( reportrule :: varsetname = parent foldernamekey )
   
   for the time being the connection between folders and rules is created here, inserting the needed entries in 
   the list of rules belonging to the folder, joining it to the rule on the foldernamekey 

=cut
    my ($self, $douser, $currargs) = (shift, shift, shift);
    my ( $memlines, $currpage ) = @_;  
    alias my $jobreport = $self->{currjobreport};
    unless ( exists($self->{recipients}->{$douser}) && exists($self->{recipients}->{$douser}->{FolderName})) {
        i::logit("Report recipient $douser requested in $jobreport->{JobReportName} not found in CTDTREE - no tables update");
        return $self;
    } 
    
    my $rgid = $douser; # ReportgroupId always = recipient name (FolderNameKey)
    my ( $rrule, $frule, $fvar, $fval ) 
        = ( $douser, '__UNDEF__', '$$$$', '$$$$' ); #this setting will cover the test=1 case

    if ( $currpage->{rectval} && (( split /\t/, $currpage->{rectval})[0]) ) { 
       $fvar  = $jobreport->{FilterVar};
       $fval  = $currpage->{test}; 
       $fval = Digest::SHA1::sha1_base64($fval) if length($fval) > 30;
       $rrule = $self->{recipients}->{$douser}->{parent};
       if ( $fval eq '__BLANK__' ) {
#TODO explain _BLANK_          
          $rrule .= '__BLANK__';
          $frule = '__BLANK__';
       } 
       else { 
          $frule = $douser;
       }
    }

#TODO this has to be moved in the folder build process
=head3 
  there are just 2 rules for each folder, rr=fnk|fr=_UNDEF_|fv=CUTVAR and rr=parent fnk|fr=fnk|fv=CUTVAR 
  may be they can be created up front for each folder

=cut  

    $self->{metabase}->addRows2Table($self->{tables}->{FoldersRules}, 
           { FolderName => $self->{recipients}->{$douser}->{FolderName}, ReportGroupId => $rgid});
    $self->{metabase}->addRows2Table($self->{tables}->{NamedReportsGroups}, 
           { ReportGroupId => $rgid, ReportRule => $rrule, FilterRule => $frule, FilterVar => $fvar });

    $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
           { VarSetName => $frule, VarName => $fvar, VarValue => $fval });
    $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
           { VarSetName => $rrule, VarName => ':REPORTNAME', VarValue => $jobreport->{JobReportName} });
    return $self;
}



sub ctdDOUSERSTAR {
=head2

portion of the report must be assigned to folders according to the content of specified rectangles in the page

Special handling occurs when two or more DO USER=* statements are used. When
both a child and a parent are identified in DO USER=* statements, the parent receives
a copy of the report only if parameter TYPE is set to U. If parameter TYPE is set to C
(or blank), multiple DO USER statements can be used to point to a Recipient Tree
location.

Example:
Two DO USER=* statements are specified with different LINE/COL references. The
first yields the string BRANCH3 (parent) and the second yields the string FINANCE
(child). If TYPE is set to blank (or C), only user FINANCE (at BRANCH3) receives the
report. CONTROL-V does not generate a copy for the parent (BRANCH3). The first
DO USER statement specifies that the report belongs to the Finance Department of
BRANCH3 and not to the Finance Department of another branch.

=cut
    my ($self, $douser, $actparm, $memlines, $currpage) = @_;
    die "do user undef - rline: $actparm\n" unless $douser;

    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};

    my ($parentbranch, $childlvl) = (qr/^.*$/, undef);
      
    if ( $douser =~ /^(\d+)\*/  ) {
       $childlvl = $1;
    }
    if ( !$childlvl) {
          $childlvl = ( exists($jobreport->{doforce}) ? $self->{recipients}->{$jobreport->{doforce}}->{lvl} : '10');
    }
        
    my ($strline, $strscol, $strecol);
    if ( $actparm =~ /^\d{6,9}[^\d]/) {
       ($strline, $strscol, $strecol) = ($actparm =~ /^(\d{3})?(\d{3})(\d{3})/ );
    }
    elsif ( $actparm =~ /^\d{10,15}[^\d]/ ) { 
       ($strline, $strscol, $strecol) = ($actparm =~ /^(\d{5})?(\d{5})(\d{5})/ );
    }
    if ( exists($currpage->{whenline}) && !($strline && $strline =~ /^\d+$/)) {
       $strline = $currpage->{whenline};
    }
    my $strlen = ($strline ? $strecol - $strscol + 1 : undef); 
    if ($strlen && exists($jobreport->{doforce})) {
       ##warn "processing force for $jobreport->{doforce} recipient - checking $childlvl to distribute $jobreport->{JobReportName}\n";
       my $rectid = sprintf('R%05d%05d%05d%05d', ($strline, $strline, $strscol, $strecol));
       $jobreport->{rect}->{$rectid} =  { coord => '('.join(',', map { s/^0+//; $_ } ($strline, $strscol, $strline, $strecol) ).')' };
       push @{$currpage->{op}}, { test => 1, getVars => $jobreport->{FilterVar}, unpack => "$rectid.a$strlen" };
       $parentbranch = $jobreport->{doforce};
       if ( exists($self->{recipients}->{$parentbranch}) ) {
          $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
                 {VarSetName => $parentbranch, VarName => ':REPORTNAME', VarValue => $jobreport->{JobReportName}});
          my $hfid = join('.', ($parentbranch, $childlvl,$jobreport->{FilterVar}));
          my $parents = {__firstname__ => $parentbranch};
          $self->{hostfolders}->{$hfid} = 
                   $self->getFoldersList($parentbranch, $childlvl, $jobreport->{FilterVar}, $parents )
                                                                   unless exists( $self->{hostfolders}->{$hfid} );
       }
    }
#    elsif (exists($self->{forcemissing}->{skip}) 
#           && $jobreport->{JobReportName} =~ /^$self->{forcemissing}->{skip}/) {
#       (my $recpos) = ($actparm =~ /^\s*(\d+)[^\d]*$/); 
#       #warn "do user $childlvl* (recpos: $recpos - $strlen ) process skipped for $jobreport->{JobReportName} as req by config - no force present\n";
#    }
    else {
       my $parent = $jobreport->{doforce} || '';
       die "dynamic do user without parent branch for $jobreport->{JobReportName} - LVL: $childlvl - strlen: $strlen parent: $parent\n"
              if !$parent;
    }
    return $self;
}

sub ctdDOINDEX {
=h1. DO INDEX



=cut    
    my ($self, $args) = (shift, shift);
    my ($memlines, $currwhen) = @_;
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};
    
    my $rline = 'INDEX  '.$args;
    my $vars = [];
    do { 
            $rline = substr(shift @{$memlines}, 1) unless $rline;
            if ( $rline !~ /^\s*$/ ) {
                my ($lvl, $vardescr, @strpos) = ($rline =~ /^INDEX\s\s[S\s](..)([^\s]+)\s*(\d{5})(\d{5})(\d{5})[^\d]/);
                $lvl = '' if $lvl eq '  ';
                die "subindex definition without main index" if $lvl && !scalar(@{$vars});
                $lvl = (scalar(@{$vars}) && $lvl ? $lvl - 1 : 0);

                my $varname = $self->xlateCtdColumn($vardescr);
                $vars->[$lvl] = $varname;

                my $strlen = ($strpos[0] ? $strpos[2] - $strpos[1] + 1 : undef); 
                die "=========== $self->{currmember} ==========" unless $strpos[0];
                my $rectid = sprintf('R%05d%05d%05d%05d', @strpos[0,0,1,2]);
                $jobreport->{rect}->{$rectid} =  { coord => '('.join(',', map { s/^0+//; $_ } @strpos[0,1,0,2] ).')' };

                push @{$currwhen->{op}}, { test => 1, getVars => $varname, unpack => "$rectid.a$strlen" };

                $self->{indexcols}->{$varname}->{$rectid} = $vardescr;
            }
            $rline = undef;
    } while ( scalar(@{$memlines}) && ($memlines->[0] =~ /^TINDEX/ || $memlines->[0] =~ /^T\s*$/) ) ;

    my $varlist = [grep { $_ } @{$vars}];
    (my $indexname = join('_', @{$varlist})) =~ s/ /_/g;
    $self->{indexes}->{$indexname} = $varlist;
    $jobreport->{INDEX}->{$indexname} = { name => $indexname, vars => join(' ', sort @{$varlist}), entries => 'FIRST', table => $indexname };
    push @{$currwhen->{op}}, { test => 1, getVars => '__IDUMMY__', unpack => '(1,2,1,3).a2', indexes => $indexname };
    return $self;
}

sub getNsetHoldDays {
    my ($self, $args, $currpage) = (shift, shift, shift);
    my ($bmiss, $lblday)  = ($args =~ m/\s*([^\s]+?)([^\s]{3})\s*$/);
    die "BACKUP POLICY $lblday  NOT MANAGED".Dumper($self->{bkupclasses}), "\n" 
                                     unless exists($self->{bkupclasses}->{"C_$lblday"});
    $currpage->{backup} = $self->{bkupclasses}->{"C_$lblday"} 
                if ( !exists($currpage->{backup}) || $currpage->{backup} < $self->{bkupclasses}->{"C_$lblday"}); 

    return $self;
}


sub ctdDOMIGRATE {
=h1. DO MIGRATE



=cut    
    my ($self, $args) = (shift, shift);
    my ($memlines, $currpage) = @_;
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};

    return $self->getNsetHoldDays($args, $currpage);
    return $self;
}

sub ctdDOBACKUP {
=h1. DO BACKUP



=cut    
    my ($self, $args) = (shift, shift);
    my ($memlines, $currpage) = @_;

    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};

    return $self->getNsetHoldDays($args, $currpage);
}

sub reprec_T {
=head2 DO Record dispatcher

=cut
    my ($self, $rline, $memlines) = (shift, shift, shift);
    my $currpage = shift;

    return $self if $rline =~ /^\s*$/;
    my ($action, $args) = unpack('A7 a*', $rline);
    $action =~ s/\s+$//;
    die "UNKNOWN FORMAT for DO Record " unless $action;

    if (my $subh = $self->can('ctdDO'.$action)) {
        return &$subh($self, $args, $memlines, $currpage, @_);
    }

    die "RECORD T$rline non gestito\n";
    
}


sub reprec_C {
    my ($self, $rline, $memlines) = (shift, shift, shift);
    my $currpage = shift;

    my $jobreport = $self->{currjobreport};
      $jobreport->{ddattrs} = join(';', map {
        my ($k, $val) = map { s/\s+$//g; $_ } split /=/, $_;
        $jobreport->{PageDef} = 'P1'.uc($val) if $k eq 'PAGEDEF'; 
        $jobreport->{FormDef} = 'F1'.uc($val) if $k eq 'FORMDEF'; 
        $jobreport->{Chars} = uc($val) if $k eq 'CHARS'; 
        $k.'='.$val;
      } ( $rline =~ /((?:\w+=\([^)]+?\)|\w+=[^(,]*)),?/g ));
    return $self;
}

sub addReport {
    my $self = shift;
    my $currjobreport = shift;
    my $xmlfn = 'NULL.xml';
    if ( !exists($self->{rptconfs}) ) {
       my $xmlhash = { name => '$JobReportName', linePos => 'raw'
#           , xmlgroup => '$$$' 
       };
       my $xml = XML::Simple::XMLout($xmlhash, RootName => 'jobreport', NoEscape => 1);
       my $xmlsha1 = Digest::SHA1::sha1_hex($xml);
       my $outdir = $self->{outputdir}.'/rptconf';
       mkdir $outdir unless -d $outdir;
       $self->{rptconfs}->{$xmlsha1} = $xmlfn;
       my $xmlpath = $outdir.'/'.$xmlfn;
       my $xmlfh = new IO::File(">$xmlpath") 
                    || die "unable to write $self->{currmember} \"$xmlfn\" ($xmlpath) in \"$self->{outputdir}\"- $? - $!";
       $xmlfh->binmode();
       print $xmlfh $xml;        
       $xmlfh->close();
    	
    }
    my $jrn = $currjobreport->{JobReportName};
    i::logit("Adding Report $jrn");
    return $xmlfn if $currjobreport->{JobReportName} eq '__DEFAULTS__';
    
    if ( exists($currjobreport->{pages}) || exists($currjobreport->{backup}) ) {
       my @pages = sort { $a->{name} cmp $b->{name} } values %{delete $currjobreport->{pages}};
       my $xmlhash = { name => '$JobReportName'
                     , linePos => 'raw', page => []
#                     , xmlgroup => substr($currjobreport->{JobReportName}, 0, 3)
#                     , bundles => {}
                     };
#       $xmlhash->{backup} = $currjobreport->{backup};
       my $single = ( scalar(@pages) == 1);
       while ( scalar(@pages) ) {
          my $currpage = shift @pages;
          my $hdays = delete $currpage->{backup} || 0;
          $xmlhash->{holddays} = $hdays if ( $hdays && (!exists($xmlhash->{holddays}) || $xmlhash->{holddays} < $hdays) ) ;
          my $catSfx = '';
          ADD_NOOPS: while(1) {
             if (   $currpage->{test} eq '1'
                && (!exists($currpage->{op}) || !scalar(@{$currpage->{op}}))
                && (!exists($currpage->{fields}) || !scalar(@{$currpage->{fields}})) 
#                && (!exists($currpage->{setvars}) || !scalar(@{$currpage->{setvars}}))
                && (!exists($currpage->{bundles}) || !scalar(keys %{$currpage->{bundles}}))
                ) { 
                 # useless to define a page with no actions defined
                if (!exists($currpage->{setvars}) || !scalar(@{$currpage->{setvars}})
                    || !scalar(grep { $_->{constant} and $_->{name} ne '$$$$' and $_->{constant} ne '$$$$' } 
                                                                                   @{$currpage->{setvars}} )) {
                 # we transfer bundles in the jobreport element but what about categories ? 
#                $xmlhash->{bundles} = { %{$xmlhash->{bundles}}, %{$currpage->{bundles} } }
#                	                                        if ( exists($currpage->{bundles}) ) ;
                   last ADD_NOOPS;
                }
             }
             push @{$xmlhash->{page}}, { };
             alias my $xmlpage = $xmlhash->{page}->[-1];
             $xmlpage->{name} = sprintf('PAGE%03d', scalar(@{$xmlhash->{page}}));
             $xmlpage->{category} = ($currpage->{category} || '_NOCATEG_').$catSfx;
             $xmlpage->{test} = 1;
             $xmlpage->{holddays} = $currpage->{backup} if $currpage->{backup};
             $xmlpage->{test} = $currpage->{test} if $currpage->{test};
             $xmlpage->{contid} = $currpage->{contid} if $currpage->{contid};
             $xmlpage->{bundles} = join(' ', sort map { $_.'/'.$currpage->{bundles}->{$_} } keys %{$currpage->{bundles}} ) 
                                                if $currpage->{bundles};
             if ( exists($currpage->{setvars}) && scalar(@{$currpage->{setvars}}) ) {
                my $attrarray = [ $single ? (grep { $_->{name} ne 'UserRef' } @{$currpage->{setvars}}) 
                                          : @{$currpage->{setvars}} ];
                $xmlpage->{vars} = [grep {!($_->{name} eq "\$\$\$\$" && $_->{constant} eq "\$\$\$\$") } @{$attrarray}] 
                                                                                                 if scalar(@{$attrarray});
                delete $xmlpage->{vars} if (exists($xmlpage->{vars})  && !scalar(@{$xmlpage->{vars}}) );
             }
             if ( exists($currpage->{fields}) && grep /UserRef /, @{$currpage->{fields}}) {
                my $varsarray = $xmlpage->{vars};
                $xmlpage->{vars} = [grep { !($_->{name} eq 'UserRef' && exists($_->{unpack}) ) } @{$varsarray}];
             }
             $xmlpage->{fields} = join(" ", @{$currpage->{fields}})
                  if (exists($currpage->{fields}) && scalar($currpage->{fields}));

             my $FilterVar = '';
            
             last ADD_NOOPS if(!exists($currpage->{op}) || !scalar(@{$currpage->{op}}));
             my $operations = delete $currpage->{op};
             my $dupGetVars = '';
             while (scalar(@{$operations}) ) {
                my $op = shift @{$operations};
#                $op->{test} = 1 if !scalar(@{$operations});
                $xmlpage->{op} = [] unless exists($xmlpage->{op});
                $op->{name} = 'OP'.scalar(@{$xmlpage->{op}});
                if (exists($op->{getVars})) {
                   $FilterVar = $op->{getVars} unless $op->{getVars} eq $FilterVar;
                   $dupGetVars = $FilterVar if (  $op->{test} eq '1' 
                                      && scalar(grep {  $_->{name} eq $FilterVar } @{$xmlpage->{vars}}));
                }
                if ( exists($op->{bundles}) ) {
                   my $opbundles = delete $op->{bundles}; 
                   my $bundlesstr = join(' ', sort map { $_.'/'.$opbundles->{$_} } keys %{$opbundles} );
                   if ( $op->{test} eq '1' && !scalar(@{$operations}) && $bundlesstr ) {
                   	  $xmlpage->{bundles} = $bundlesstr;
                   	  
                   	  $currpage->{bundles} = {} unless exists($currpage->{bundles});
                   	  $currpage->{bundles} = { %{$currpage->{bundles}}, %{$opbundles} }; 
                   }  
                   elsif ( $bundlesstr ) { $op->{bundles} = $bundlesstr; }
                }
                push @{$xmlpage->{op}}, $op ;
             }
             if ( $dupGetVars ) {
             	$xmlpage->{vars} = [ grep { $_->{name} ne $dupGetVars } @{ delete $xmlpage->{vars} } ];
             	delete $xmlpage->{vars} unless scalar(@{$xmlpage->{vars}});
             }
             $catSfx = '_NOOPS';
             $currjobreport->{catlist}->{$currpage->{category}.'_NOOPS'} 
                                  = $currjobreport->{catlist}->{$currpage->{category}} + 1; 
          } #end while 1
       } # end while @pages   
        
       $currjobreport->{HoldDays} = delete $xmlhash->{holddays} if ( exists($xmlhash->{holddays}) && $xmlhash->{holddays} );
       if ( !exists($xmlhash->{page}) ) {
#          $xmlhash->{xmlgroup} = '$$$';
       }
       elsif ( exists($xmlhash->{page}) && scalar(@{$xmlhash->{page}}) == 1 
                    && $xmlhash->{page}->[0]->{test} eq '1' 
                    && !exists($xmlhash->{page}->[0]->{bundles}) 
                    && !exists($xmlhash->{page}->[0]->{vars}) 
                    && !exists($xmlhash->{page}->[0]->{fields}) 
                    && (!exists($xmlhash->{page}->[0]->{op}) 
                        || !scalar(@{$xmlhash->{page}->[0]->{op}})) 
                    ) {
          my $page = delete $xmlhash->{page};
 #         $xmlhash->{xmlgroup} = '$$$';
 #            $xmlhash->{bundles} = $page->{bundles} if exists($page->{bundles});
       }
        
#        if ( exists($xmlhash->{page}) && scalar(@{$xmlhash->{page}}) == 1
#                && !exists($xmlhash->{page}->[0]->{bundles}) 
#                && !exists($xmlhash->{page}->[0]->{vars}) 
#                && !exists($xmlhash->{page}->[0]->{fields}) 
#                && (!exists($xmlhash->{page}->[0]->{op}) 
#                    || !scalar(@{$xmlhash->{page}->[0]->{op}})) ) {
#                    $xmlfn = 'NULL.xml';
#                    #delete $currjobreport->{FilterVar};
#                }           
        else {
            $xmlhash->{'index'} = $currjobreport->{INDEX} if exists($currjobreport->{INDEX}) && scalar(keys %{$currjobreport->{INDEX}});
            $xmlhash->{rect} = $currjobreport->{rect} if exists($currjobreport->{rect}) && scalar(keys %{$currjobreport->{rect}});
            $xmlhash->{cateList} = join('|', sort keys %{$currjobreport->{catlist}}  )
#                 sort {$currjobreport->{catlist}->{$a} <=> $currjobreport->{catlist}->{$b} } keys %{$currjobreport->{catlist}}  )
#                 sort keys { map { $_->{category} => 1 } @{$xmlhash->{page}} } )
                                                       if (exists($xmlhash->{page}) && scalar(@{$xmlhash->{page}}));
        }

        my $xml = XML::Simple::XMLout($xmlhash, RootName => 'jobreport', NoEscape => 1);
        my $xmlsha1 = Digest::SHA1::sha1_hex($xml);

        if ( !exists($self->{rptconfs}->{$xmlsha1}) ) {
#           (my $grpre = qr/$xmlhash->{xmlgroup}/) =~ s/\$/\\\$/g;
#           $self->{rptconfs}->{$xmlsha1} 
#             = sprintf('%s%05d.xml', $xmlhash->{xmlgroup}, scalar( grep { $_ ne 'NULL.xml' && $_ =~ /^$grpre/ } values %{$self->{rptconfs}}));
#          $self->{rptconfs}->{$xmlsha1} = join('.', ($xmlhash->{xmlgroup}, $xmlsha1, 'xml'));
           $self->{rptconfs}->{$xmlsha1} = $currjobreport->{JobReportName}.'.xml';
           my $outdir = $self->{outputdir}.'/rptconf';
           mkdir $outdir unless -d $outdir;
           $xmlfn = $self->{rptconfs}->{$xmlsha1};
           my $xmlpath = $outdir.'/'.$xmlfn;
           my $xmlfh = new IO::File(">$xmlpath") 
                 || die "unable to write $self->{currmember} \"$xmlfn\" ($xmlpath) in \"$self->{outputdir}\"- $? - $!";
           $xmlfh->binmode();
           print $xmlfh $xml;        
           $xmlfh->close();
        }
        else {
           $xmlfn = $self->{rptconfs}->{$xmlsha1};
        }
    }
    
    $currjobreport->{JobReportDescr} = $currjobreport->{JobReportName} unless $currjobreport->{JobReportDescr};
    @{$currjobreport}{qw(ParseFileName CharsPerLine)} = ("*,$xmlfn", 400);
    $self->{metabase}->addRows2Table($self->{tables}->{JobReportNames}, $currjobreport);
    @{$currjobreport}{qw(ReportName ReportDescr)} = @{$currjobreport}{qw(JobReportName JobReportDescr)};
    $self->{metabase}->addRows2Table($self->{tables}->{ReportNames}, $currjobreport);
    if ( ( exists( $currjobreport->{FormDef} ) && $currjobreport->{FormDef} !~ /^\s*|''$/ )
           or ( exists( $currjobreport->{PageDef} ) && $currjobreport->{PageDef} !~ /^\s*|''$/ ) 
           or ( exists( $currjobreport->{Chars} ) && $currjobreport->{Chars} !~ /^\s*|''$/ ) 
           or ( exists( $currjobreport->{PrintControlFile} ) && $currjobreport->{PrintControlFile} !~ /^\s*|''$/ ) 
           ) {
            $self->{metabase}->addRows2Table($self->{tables}->{Os390PrintParameters}, $currjobreport);
#           $afpvars = 1;
        }
    my ($jn, $t1, $q1, $t2, $q2) = (map { $_ ? $_ : '' } 
                $currjobreport->{JobReportName} =~ /^([^\.]+)(?:\._(.)([^\.]+))?(?:\._(.)([^\.]+))?$/);
    $t1 = '.' if $t1 eq '';
    my $flds = { C => 'CLASS', D => 'DEST', F => 'FORMS', S => 'STEPN', W => 'WRTRN', '_' => 'DDNAM'};
    
    my $assert = "\$request->{cinfo}->{JOBNM}";
    $assert .= ".'._$t1'.\$request->{cinfo}->{".$flds->{$t1}."}" if $t1 ne '.';
    $assert .= ".'._$t2'.\$request->{cinfo}->{".$flds->{$t2}."}" if $t2;
    my $defaultassert = $self->{defaultrename}->{ASSERT}->[0]->{reportname};
    return $xmlfn if ($assert eq $defaultassert && $jn !~ /$self->{jobnre2skip}/ );
        
    my $renamematch;
    my $attribute;
    $main::debugjn = $jn;
    
    if ( $jn =~ /$self->{jobnre}/ && $jn !~ /$self->{jobnre2skip}/ ) {
       $renamematch = $main::rematch;
       $attribute = "\$request->{cinfo}->{JOBNM}";
    }
    else {
        if ($t1 ne '.') { 
           $renamematch = "^${jn}${q1}" ;
           $attribute = "\$request->{cinfo}->{JOBNM}.\$request->{cinfo}->{".$flds->{$t1}."}";
           if ( $t2 ) {
                 $attribute .= ".\$request->{cinfo}->{".$flds->{$t2}."}";
                 $renamematch .= "${q2}" ;
           }
        } 
        else {
           $renamematch = "^${jn}";
           $attribute = "\$request->{cinfo}->{JOBNM}";
#           return $xmlfn if ($assert eq $defaultassert && $jn !~ /$self->{jobnre2skip}/ );
        }   
    }
    if ( !exists($main::xrrenlist->{$renamematch.'='.$assert}) ) {
       $main::xrrenlist->{$renamematch.'='.$assert} = 1;
       unshift @{$main::xrrename->{receiver}->{CASE}}, { 
          MATCH => $renamematch, 
          ATTRIB => $attribute, 
          ASSERT => { reportname => $assert, } 
       };
       i::logit("Added ASSERT \"$assert\" when \"$attribute\" MATCHes \"$renamematch\" to rename process");   
    }
    return $xmlfn;
    
}

sub createNewPage {
    my $self = shift;
    my $currpage = { test => '', rectval => '', UserRef => ''
#                   , pages => {}
                   , initialdummy => 1, name => $self->{currcat}->{catname}.'.PAG000' };
#    $currpage->{bundles} = { $self->{currjobreport}->{prtuser} => $self->{currjobreport}->{prtcopies} }
#    	                                                                    if $self->{currjobreport}->{prtuser};
    return $currpage;
}


sub reprecDispatcher {
    my ($self, $record, $memlines) = (shift, shift, shift);
    my $rectype;
    if ( ($rectype, my $recdata) = ($record =~ /^\s*([RDMVIQOSFNPCLWZAYTXUJ0])(.*)/) ) {
        if ( my $subh = $self->can('reprec_'.$rectype) ) { 
                    return &$subh($self, $recdata, $memlines, @_);
        }
    }
    else {  
      die "Record type not handled in $self->{currmember}: ", $record, "\n";
    }
    return $record;
}


sub parseReports {
    my $self = shift;   
    my $dh = $self->{dirfh};
    i::logit("Started parseReports TIMES: ", join('::', times()));

    MEMBER: foreach ( readdir $dh ) {
        
        next MEMBER if $_ =~ /^\.\.?$/;
        my $ctdreps_fn = "$self->{reportsdir}/$_";
        my $mname = $self->{currmember} = basename($ctdreps_fn);
        i::logit("Processing now $mname");
        my $rfh = new IO::File("<$ctdreps_fn") || die "Unable to open $ctdreps_fn - $!\n"; 
        binmode $rfh;
        $rfh->read(my $memstream, -s $ctdreps_fn);
    
        $self->{memlines} = [ map { from_to($_, 'cp37', 'latin1'); $_ } unpack("(a80)*", $memstream) ];
        unless (scalar(@{$self->{memlines}}) && $self->{memlines}->[0] =~ /^\s*R/) {
        	i::logit("First row of $mname not \"R\" - skip to next member");
        	next MEMBER;
        }

        while (scalar(@{$self->{memlines}})) {
           my $currrec = shift @{$self->{memlines}};
        	
           my $result = $self->reprecDispatcher($currrec, $self->{memlines});
           # se un metodo mi restituisce undef devo comunque andare al prossimo record N che me ne pu� definire un altro
           # altrimenti mi perdo tutti quelli di quel membro
           if (!defined($result)) {
           	  delete $self->{currjobreport};
              while (scalar(@{$self->{memlines}}) && $self->{memlines}->[0] !~ /^\s*[RN]/ ) {
                 shift @{$self->{memlines}}
              }
           }
        }
        foreach(sort keys (%{$self->{jobreports}})) {
           $self->addReport(delete $self->{jobreports}->{$_});
        }
            
        $rfh->close();
        $rfh = undef;
    }
    return $self;
}

=pod

=head2 tree definition Record analyzer routines

=cut

sub treerec_S {
=pod

=head2 Synonims record

=cut

        my ($self, $recinfos) = (shift, shift);
        alias my $recipient = $self->{recipient};
        
        if ( my @members = grep !/^(?:TSO-|S|C|\s)/,
              map { ( split( /;/, $_ ) )[0] } ( unpack( "(A20)*", $recinfos ) ) ) {
            push @{$recipient->{filtervals}}, @members;         
        }
    
        if ( grep /^TSO\-/,
              map { ( split( /;/, $_ ) )[0] } ( unpack( "(A20)*", $recinfos ) ) ) {
                $recipient->{toprofile} = 1;
              }
}

sub linkChildParent {
    my $self = shift;
    my ($lvlname, $plvlname) = (shift, shift);
    $self->{recipients}->{$lvlname}->{parent} = $plvlname;
    $self->{recipients}->{$plvlname}->{_chlds}->{$lvlname} = {}; # $recipient;
    $self->{recipients}->{$lvlname}->{FolderName} = "$lvlname";
    return $self unless $self->{recipients}->{$plvlname}->{FolderName};
    $self->{recipients}->{$lvlname}->{ParentFolder} = $self->{recipients}->{$plvlname}->{FolderName};
    $self->{recipients}->{$lvlname}->{FolderName} = $self->{recipients}->{$lvlname}->{ParentFolder}."\\".$lvlname;
    return $self;
} 


sub treerec_U {
=pod

=head2 Recipient Record

=cut

    my ($self, $recinfos) = (shift, shift);
    my ( $lvl, $lvlname, $plvl, $plvlname, $lvldesc ) = unpack( "(A2 A8)2 A*", $recinfos );
    return $self if $lvlname =~ /^.*\*/;
    
    return $self if $plvlname && !exists($self->{recipients}->{$plvlname}); 
    
    if ( exists($self->{tree}->{skipmask}) 
              && ($lvlname =~ /$self->{tree}->{skipmask}/ 
                  || $plvlname =~ /$self->{tree}->{skipmask}/
                  ) ) {
        i::logit("SKIPPING $lvlname ($plvlname) as stated in config");
        return undef;
    }
    $self->{recipients}->{$lvlname} = 
      {name => $lvlname, lvl => $lvl, FolderDescr => $lvldesc, FolderNameDescr => '', address => [] };
    my $currrecipient = $self->{recipient}->{name};
    alias $self->{recipient} = $self->{recipients}->{$lvlname};
    
    $self->linkChildParent($lvlname, ( $plvlname || 'ROOT' ));
    
    if ( exists($self->{tree}->{skipmask}) 
              && ($self->{recipient}->{FolderName} =~ /^.*\\?$self->{tree}->{skipmask}\\/ ) ) {
       my $plvlname = $self->{recipient}->{parent};
       delete $self->{recipients}->{$plvlname}->{_chlds}->{$lvlname} unless $plvlname eq 'ROOT';
       alias $self->{recipient} = $self->{recipients}->{$currrecipient};
       delete $self->{recipients}->{$lvlname};
       return undef;       
    }
    

    return $self;
}

sub treerec_D {
=pod

=head2 Definition Record

=cut

    my ($self, $recinfos) = (shift, shift);
    alias my $recipient = $self->{recipient};
    alias my $branchid = $recipient->{FolderName};
    my @adarray = map { (unpack("A7A*", $_))[1] } grep /\%ADDR\%/, unpack("(A80)*", $recinfos);
    push @{$recipient->{address}}, @adarray;
}

sub traverseTree {
    my $self = shift;
    my $root = shift;
    my $path = shift;
    
    foreach my $childk ( sort keys %{$root} ) {
        
        next unless exists($self->{recipients}->{$childk}) 
                    ;
        if ( $childk =~ /^[^\d]+\d{5}-\d{5}/ ) {
            my $child = delete $self->{recipients}->{$childk};
            alias my $parent = $self->{recipients}->{$child->{parent}};
            delete $parent->{_chlds}->{$childk};
            my @childlist = sort keys %{$child->{_chlds}};
            my $newname = join('.', @childlist[0,-1]);
            while ( scalar(@childlist) ) {
                #warn " " x 8, join(", ", splice(@childlist, 0 , 10)), "\n"
            }
            if ( exists($self->{recipients}->{$newname}) ) {
                my $newchild = delete $self->{recipients}->{$newname};
                $newchild->{_chlds} = {%{$newchild->{_chlds}}, %{$child->{_chlds}}};
                $child = $newchild;
            }

            $self->{recipients}->{$newname} = $child;
            $parent->{_chlds}->{$newname} = {};
            $child->{name} = $newname;
            $child->{FolderDescr} = "Container for folders $newname";
            $childk = $newname; 
                        
        }
        $self->{recipients}->{$childk}->{parent} = $path->[-1];
        $self->{recipients}->{$childk}->{ParentFolder} = join("\\", @$path);
        $self->{recipients}->{$childk}->{FolderName} = join("\\", (@$path,$childk));
        $self->traverseTree( $self->{recipients}->{$childk}->{_chlds},[@$path,$childk]) 
                            if scalar(keys %{$self->{recipients}->{$childk}->{_chlds}});
    }
    
    return $self;
} 

sub processPermRecord {
  my $self = shift;
  my @infos = unpack('(n/a)*', $_[0]);
  $main::numpermrec++;
  i::logit("Processing now  permanent record $main::numpermrec") 
                                 if ( ($main::numpermrec % 100000) == 1 );
  my ($user, $rname, $jname, $nc) = 
          map { $_ =~ s/^\s+|\s+$//g; $_ } (unpack('A12 A59 A91', $infos[0]), unpack('x42 A4',$infos[1]));
  my $attrs = { map { $_ ? uc($_) : '' } (join('', splice(@infos, 2) ) 
                     =~ /(Pagedef|Formdef|Chars|Output)\:(?:\s([^\s]{1,20}))?/ig) };
  $self->{textStrings}->{$rname} = 1;
  return '' if $nc =~ /^DFT$/i; 

  $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
                   { VarSetName => 'PRTC:'.$user, VarName => "$jname/$rname", VarValue => $nc } );
  return '';
}

sub parsePermanent {
=pod

=head2 permanent file parsing routine

=cut
    my $self = shift;   
    my $permfh = $self->{permfh};
    i::logit("Started parsePerm");
    
    my $recll = 133;                                  # usually printouts are fb 133 ;
    $permfh->read(my $hdr, 3);
    my $modeb = 1 if ( $hdr =~ /[\x80\x40](..)/ );   # chk first 3 chars 
    if ( $modeb ) {
       my ($rect, $ll) = unpack('an', $hdr);
       $recll = $ll + 3; # to satisfy next read
     }
    $permfh->read(my $eline, $recll - 3);           # skip first line ( usually company hdr )

    my $recbuff;
    while ( !$permfh->eof() ) {
        my $rect = "\x80";
        if ( $modeb ) {
            $permfh->read(my $rdw, 3);
            ($rect, $recll) = unpack('an', $rdw);
        }
        $permfh->read(my $eline, $recll);
        from_to($eline, 'cp37', 'latin1');
        next if $eline =~ /^(?:\s*|1BMC.*)$/;

        $recbuff .= pack('n/a*',substr($eline, 1)); 
        $recbuff = ( $recbuff =~ /from user: [^\s]/i ? '' 
                     : $self->processPermRecord($recbuff) ) if ( $recbuff =~ /\-{10} end of record \-{10}/i );

        last if ($modeb && $rect eq "\x40");
    }
    
    foreach my $rname ( keys %{$self->{textStrings}} ) {
        delete $self->{textStrings}->{$rname};
#       #warn "Adding Perm data to textTable: TS: $rname\n";
#        $self->{metabase}->addRows2Table($self->{tables}->{LogicalReportsTexts}, { textString => $rname} );
    }
    return $self;
}

sub parseTree {
=pod

=head2 CTDTREE parsing routine

=cut
    my $self = shift;   
    my $treefh = $self->{treefh};
    i::logit("Started parseTree");
    
    $self->{ctdtree} = {};

    TREELINE: while ( !$treefh->eof() ) {
        $treefh->read(my $eline, 80);
        from_to($eline, 'cp37', 'latin1');
        next if $eline =~ /^\s/;
    my ( $typer, $recdata ) = ( $eline =~ /^(.)(.*)$/ );
         next TREELINE if ( $self->{skipuntil_u} && $typer ne 'U' );
         delete $self->{skipuntil_u} if exists( $self->{skipuntil_u} );

        if ( !exists($self->{lasttreetype}) ) {
            @{$self}{qw( lasttreetype lasttreedata )} = ( $typer, $recdata );
            next;
        }
        if ( $self->{lasttreetype} eq $typer ) {
            $self->{lasttreedata} .= " ".$recdata;
            next TREELINE unless $treefh->eof();
        }

        my ( $rectype, $recinfos ) = @{$self}{qw( lasttreetype lasttreedata )};
        @{$self}{qw( lasttreetype lasttreedata )} = ( $typer, $recdata );
        
        if ( my $subh = $self->can('treerec_'.$rectype) ) {
           my $ret = &$subh($self, $recinfos);
           if ( $rectype eq 'U' && !$ret ) {
             $self->{skipuntil_u} = 1;
         @{$self}{qw( lasttreetype lasttreedata )} = ('','') if $typer ne 'U';
             next TREELINE;
           }
        }
    }
        
    $treefh->close();
    $treefh = undef;
    delete $self->{treefh};

    return $self;
    
    foreach my $fname ( keys %{$self->{recipients}} ) {
        my $paname = $self->{recipients}->{$fname}->{parent};
        $self->traverseTree($self->{recipients}->{$fname}->{_chlds}, [$fname]) if $paname && $paname eq 'ROOT';
    }
    return $self;
}

=pod

=head2 Print Mission definitions Record analyzer routines

=cut

sub pmissrec_P {  #Print mission record
=pod

=head2 Print mission record analyzer

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    @{$pmiss}{qw(mission group owner category ttype maxwait mnum)}
        = unpack("A8 A20 A8 A20 A3 A2", $pline);
   
    CFGLINE: while ( scalar(@{$self->{memlines}}) ) {
        last if $self->{memlines}->[0] =~ /^P/;
        
        my $ctdline = shift @{$self->{memlines}};
        die "Not Supported statement in $self->{currmember}: ", $ctdline, "\n" 
                            unless $ctdline =~ /^[CDNXBMVWTA0SIO]/;
        if ($ctdline =~ m/^\s*([^\s])(.*)/ && (my $subh = $self->can('pmissrec_'.$1)) ) {
            my $result = &$subh($self, $2, $self->{memlines}, $pmiss);
            if ( !defined($result) ) { 
               while( scalar(@{$self->{memlines}}) && $self->{memlines}->[0] !~ /^P/ ) { 
                  shift @{$self->{memlines}}; 
               }
               return undef;
            }
        }
    }
    @{$pmiss}{qw(BundleName BundleCategory)} = map { $_ ? $_ : ''} @{$pmiss}{qw(mission category)};
    $pmiss->{ChunkDelivery} = ($pmiss->{free} eq 'C' ? 1 : 0);
    return $self;
}


sub addRecipient {
    my ($self, $pline) = (shift, shift);
    alias my $area = shift;
    my $pattern = unpack("A8", $pline);
    my $varval = '';
    if ( $pattern =~ /[\?\*]/ ) {
        if ( $pattern eq '*' ) {
            $varval = '%';
        }
        else {
            ($varval = $pattern) =~ s/\?/_/g;
            $varval =~ s/\*/\%/g;
        }
    }
    else {
        $varval = $pattern;
    }
    push @{$area}, $varval;
    return $self;
}

sub pmissrec_N {  #include record
=pod

=head2 Include recipient record analyzer

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
#    warn "processing N mission record: ", $pline, "\n";
    $self->{recipients} = {} unless exists($self->{recipients});
    my $pmiss = shift;
    $self->addRecipient($pline, $pmiss->{':BundleIncludePattern'});
        
    return $self;
}

sub pmissrec_W {  # when record
=pod

=head2 When record parser (time schedule)

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    my $t = unpack('a4', $pline);
}

sub pmissrec_X {  #exclude record
=pod

=head2 Exclude recipient record analyzer

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    $self->addRecipient($pline, $pmiss->{':BundleExcludePattern'});
        
    return $self;
}

sub pmissrec_C {  #Overriding Parms
=pod

=head2 Print parameters record

=cut


    my ($self, $pline, $memlines) = (shift, shift, shift);
    return undef unless $pline =~ /^Y/; # ( only Batch = 'Y' )
    my $pmiss = shift;
    @{$pmiss}{qw(batch BundleSkel free timeout BundleDest BundleUdest BundleWriter BundleForm BundleClass BundlePrtopt)}
       = unpack('A A8 A A2 (A8)4 A A8', $pline);

    return $self;
}

sub pmissrec_D {  #Description record
=pod

=head2 Description record analyzer

=cut


    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    @{$pmiss}{qw(BundleDescr BundleUdata)} = unpack("A50 A29", $pline);
    return $self;
}

sub pmissrec_A { #ACIF record
=pod

=head2 ACIF record analyzer

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    $pmiss->{acif} = 'Y' if $pline =~ /^Y/;
}

sub pmissrec_B { #sort record
=pod

=head2 Sort Output record analyzer

=cut


    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    my %SFIELDS = ( '1' => 'FolderName'
        ,'2' => 'JobName'
        ,'3' => 'JobReportName'
        ,'4' => 'Category'
        ,'5' => 'FolderLevel'
        ,'6' => 'FolderName'
        ,'7' => 'Formdef'
        ,'8' => 'Chars'
        ,'9' => 'PrintParameters'
        ,'T' => 'UserTimeRef'
    );

    $pmiss->{BundleOrder} = join(',', ( map { my ($c, $o) = unpack('AA', $_); "$SFIELDS{$c}/".( $o ? $o : 'A') } split(/\,/, $pline)));
    return $self;
}

sub pmissrec_M { # WHEN record (CAL)
=pod

=head2 Calendar record

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    ($pmiss->{retro}, my $dtype, my $dstring) = unpack("A A a*", $pline);
    if ( $dtype !~ /^[CDWM]$/ ) {
        return undef;
    }
    if ( $dtype eq 'C' ) {}
    elsif ( $dtype eq 'D' ) {} 
    elsif ( $dtype eq 'W' ) {} 
    else {} 
    
    return $self;
}

sub pmissrec_V { 
=pod

=head2 Date record (WeekDays)

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    ($pmiss->{shft}, my $stype, my $dtype, my $dstring) = unpack("A A A a*", $pline);
    
    return $self;
}

sub pmissrec_T { 
=pod

=head2 Printer Type record

=cut


    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    @{$pmiss}{qw(BundlePrtype BundlePrdest PRCHUNK BundlePrudest)} = unpack("A20 A8 A6 A8", $pline);
    
    return $self;
}

sub parseMissions {
=pod

=head2 Print mission definitions analyzer

=cut

    
    my $self = shift;   
    my $dh = $self->{pmdirfh};
    i::logit("Started parseMissions - Recipients: ", scalar(keys %{$self->{recipients}}));
    
    MEMBER: foreach  ( readdir $dh ) {

        next MEMBER if $_ =~ /^\./ ;
        my $ctdmiss_fn = "$self->{prtmissdir}/$_";
        my $mname = $self->{currmember} = basename($ctdmiss_fn);

        my $rfh = new IO::File("<$ctdmiss_fn") || die "Unable to open $ctdmiss_fn - $!\n"; 
        binmode $rfh;
        $rfh->read(my $memstream, -s $ctdmiss_fn);
    
        $self->{memlines} = [ map { from_to($_, 'cp37', 'latin1'); $_ } unpack("(a80)*", $memstream) ];
        if (scalar(@{$self->{memlines}}) && $self->{memlines}->[0] =~ /^\s*P/) {
            CONFIGLINE: while ( scalar(@{$self->{memlines}}) ) {
                my $ctdline = shift @{$self->{memlines}};
                die "UnkNown statement in $self->{currmember}: ", $ctdline, "\n" 
                            unless $ctdline =~ /^[PCDNXBMVWTA0SIO]/;
                my $result;
                my $pmiss = { InputMember => $mname };
                if ($ctdline =~ m/^\s*([^\s])(.*)/ && (my $subh = $self->can('pmissrec_'.$1)) ) {
                    $result = &$subh($self, $2, $self->{memlines}, $pmiss);
                }
                else {
                    $result = undef;
                }
                if ( $result ) {
                    my ($incount, $excount) = map { exists($pmiss->{$_}) ? scalar(@{$pmiss->{$_}}) : 0 } 
                                       qw(:BundleIncludePattern :BundleExcludePattern);

                    $self->{metabase}->addRows2Table($self->{tables}->{BundlesNames}, $pmiss);
                    die "PMISSION undef: ", Dumper($pmiss), "\n" if (!defined($pmiss->{BundleName}) || !defined($pmiss->{BundleCategory}) || !defined($pmiss->{BundleSkel}) );
                    $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, map { my $area = $_; map { 
                        { VarSetName => join('_', @{$pmiss}{qw(BundleName BundleCategory BundleSkel)})
                        ,VarName => $area
                        ,VarValue => $_
                        }
                    } @{$pmiss->{$_}} } qw(:BundleIncludePattern :BundleExcludePattern) );
                }
            }
        }
        $rfh->close();
    }
    return $self;
}

sub createFolders {
    my $self = shift;
    i::logit("Started createFolders");
    while ( scalar(keys %{$self->{recipients}}) ) {
        my $FolderId = (keys %{$self->{recipients}})[0];
        my $folder = delete $self->{recipients}->{$FolderId};
        next unless $FolderId ;
        alias my $fname = $folder->{FolderName};
        next unless $fname;
         
        die "no FolderName: fname: $fname fid: $FolderId ", Dumper($folder), "\n" unless $folder->{FolderName};
        if ( $folder->{FolderName} =~ /\\/ ) {
            unless ( exists($folder->{ParentFolder}) && $folder->{ParentFolder} ne '' ) {
                my @lvls = split(/\\/, $folder->{FolderName});
                pop @lvls;
                $folder->{ParentFolder} = join("\\", @lvls)
            }
        }
        else {
            $folder->{ParentFolder} = 'ROOT';
        }
        
        $folder->{FolderAddr} = join("\\n", map { $_ ? $_ : '' } @{$folder->{address}});
        $self->{metabase}->addRows2Table($self->{tables}->{Folders}, $folder);
        if (my $UName = &{$self->{userassign}}($FolderId)) {
            $self->{metabase}->addRows2Table($self->{tables}->{Users}, 
                {UserName => $UName, UserDescr => "User to access $FolderId"});
            $self->{metabase}->addRows2Table($self->{tables}->{UserAliases}, 
                {UserName => $UName, UserAlias => $UName, UserDescr => "User to access $FolderId"});
            $self->{metabase}->addRows2Table($self->{tables}->{UsersProfiles}, 
                {ProfileName => $FolderId, UserName => $UName});
            $self->{metabase}->addRows2Table($self->{tables}->{Profiles}, 
                {ProfileName => $FolderId, ProfileDescr => $folder->{FolderDescr}});
            $self->{metabase}->addRows2Table($self->{tables}->{ProfilesFoldersTrees}, 
                {ProfileName => $FolderId, RootNode => $folder->{FolderName}});
        }
    }

    return $self;
}

sub sqldo {
    my $fh = shift;
    print $fh @_, "\nGO\n";
}

sub writeTables {
    my $self = shift;
    my @tables = @_;
    @tables = keys %{$self->{tables}} unless scalar(@tables);
    my $outdir = $self->{outputdir};
    
    my $sfxtemp = '_temp';
    my $sfxlast = '_last';
    
    foreach my $tableName ( @tables ) {
        if ( !exists($self->{tables}->{$tableName}) ) {
            next;
        }
        my $table = delete $self->{tables}->{$tableName};
        i::logit("Processing now table $tableName - rows: ", (ref($table->{rows}) eq 'ARRAY' ? scalar(@{$table->{rows}}) : scalar(keys %{$table->{rows}})));        
        my $tblfh = new IO::File(">$outdir/tbl_$tableName.sql");
        sqldo($tblfh, $self->{metabase}->preliminarySettings());    
        sqldo($tblfh, $self->{metabase}->rotateTable(table => $table, tblsfx => '_bk'
              , sequence => [ {v2 => 'v1'}, {v3 => 'v2'}, {v4 => 'v3'}, {LAST => 'v4'} ]));
        sqldo($tblfh, $self->{metabase}->SQLCreate(tables => [ $table ], tblsfx => '_bk', nopk => 1));
        sqldo($tblfh, $self->{metabase}->copyTable(table => $table, insfx => '', outsfx => '_bk'));
        sqldo($tblfh, $self->{metabase}->truncateTable(table => $table, tblsfx => ''));
        sqldo($tblfh, $self->{metabase}->copyTable(table => $table, insfx => '_bk', outsfx => '', 
                                                       where => $table->{keep})) if ( exists($table->{keep}) );
        sqldo($tblfh, $self->{metabase}->genInsert(table => $table, tblsfx => ''));  
        sqldo($tblfh, $self->{metabase}->ansiSetting("OFF"));
        $tblfh->close();
        $tblfh = undef;
        
    }   
    
    return $self;

}

1;

package main;
use Data::Dumper;
use XML::Simple;
$| = 1;
my $xx = select STDERR;
$| = 1;
select $xx;

$main::xrrename = {
    name=>"\$JobReportName", language=>"Italian",
    receiver  => { 
        DEBUG => 1,
        CASE  => [ 
#        { MATCH => '.+', ATTRIB => '$request->{cinfo}->{JOBNM}',
#                ASSERT => { reportname => "\$request->{cinfo}->{JOBNM}.'._S'.\$request->{cinfo}->{STEPN}.'.__'.\$request->{cinfo}->{DDNAM}"}
#        }
         ]
    } 
};

my $odir = $ARGV[1];
my $struct = CTDParser->new( reportsdir => $ARGV[0].'/ctdreports'
                            ,prtmissdir => $ARGV[0].'/prtmission'
                            ,ctdtreefil => $ARGV[0].'/ctdtree.bin'
                            ,ctdpermfil => $ARGV[0].'/ctdperm.bin'
                            ,outputdir  => $ARGV[1] )
    ->parseTree()
    ->parseMissions()
    ->parseReports()
    ->createFolders()
    ->parsePermanent()
    ->writeTables()
    ;

#my $wildcases = [ sort { $b->{pfxlen} <=> $a->{pfxlen} } 
#                  grep { $_->{MATCH} =~ /^\(\?\-xism\:/ } @{$main::xrrename->{receiver}->{CASE}}];
#my $wildclist = join('|', map { (my $matchre = $_->{MATCH}) =~ s/\\t/\\\\t/g; $matchre } @{$wildcases} ); 
#$wildclist = qr/^$wildclist/;
#my $normcases = [
#            grep { $_->{MATCH} =~ /$wildclist/ 
#                 || $_->{ASSERT}->[0]->{reportname} ne $struct->{defaultrename}->{ASSERT}->[0]->{reportname} }
#            grep { $_->{MATCH} !~ /^\(\?\-xism\:/ } @{delete $main::xrrename->{receiver}->{CASE}}];
#
#$main::xrrename->{receiver}->{CASE} 
#      = [ map { (my $m = $_->{MATCH}) =~ s/\\([tw])/\\\\$1/g; 
#                {MATCH => $m, ATTRIB => $_->{ATTRIB}, ASSERT => $_->{ASSERT} } } (@{$normcases} 
#        , map { (my $m = $_->{MATCH}) =~ s/\\([tw])/\\\\$1/g; 
#                {MATCH => $m, ATTRIB => $_->{ATTRIB}, ASSERT => $_->{ASSERT} } } 
#          (grep { $_->{MATCH} =~ /^\(\?\-xism\:(?!\\w\+\))/ } @{$wildcases})
#        , map { (my $m = $_->{MATCH}) =~ s/\\([tw])/\\\\$1/g; 
#                {MATCH => $m, ATTRIB => $_->{ATTRIB}, ASSERT => $_->{ASSERT} } } 
#          (grep { $_->{MATCH} =~ /^\(\?\-xism\:(?=\\w\+\))/ } @{$wildcases})
#        , $struct->{defaultrename} ) ];
#
my $cases = [(sort { $b->{MATCH} cmp $a->{MATCH} } @{delete $main::xrrename->{receiver}->{CASE}}), $struct->{defaultrename} ];
$main::xrrename->{receiver}->{CASE} = $cases;
            
(my $a = XML::Simple::XMLout($main::xrrename, RootName => 'jobreport') ) =~ s/\>\s*\<(ASSERT|\/CASE)/\>\<$1/gs;
$a =~ s/\-\&gt\;/\-\>/gs;

open RPTCONF, ">$odir/rptconf/XRRENAME.xml";
binmode RPTCONF;
print RPTCONF $a, "\n";
close RPTCONF;

__END__

