USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_BundlesQ]    Script Date: 06/12/2013 12:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_BundlesQ]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_BundlesQ]
GO

USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_BundlesQ]    Script Date: 06/12/2013 12:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_BundlesQ](
    [BundleName] [varchar](128) NOT NULL,
    [JobReportId] [int] NOT NULL,
    [ReportId] [int] NOT NULL,
    [textId] [int] NOT NULL,
    [ReportTimeRef] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

