USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_JobReportsBtreesBlobs]    Script Date: 01/15/2013 13:24:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsBtreesBlobs]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsBtreesBlobs]
GO

USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_JobReportsBtreesBlobs]    Script Date: 01/15/2013 13:24:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_JobReportsBtreesBlobs](
	[JobReportId] [int] NOT NULL,
	[blob_sha1] [varbinary](40) NOT NULL,
	[btree_id] [int] NOT NULL,
	[blob_data] [varbinary](max) NOT NULL,
	[blob_metadata] [varbinary](max) NULL,
 CONSTRAINT [PK_tbl_JobReportsBtreesBlobs] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC,
	[blob_sha1] ASC,
	[btree_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


