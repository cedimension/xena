USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_LogicalReportsTexts]    Script Date: 12/10/2012 14:17:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReportsTexts]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_LogicalReportsTexts]
GO

USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_LogicalReportsTexts]    Script Date: 12/10/2012 14:17:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_LogicalReportsTexts](
	[textId] [int] IDENTITY(1,1) NOT NULL,
	[textString] [varchar](128) NOT NULL,
 CONSTRAINT [PK_tbl_LogicalReportsTexts] PRIMARY KEY CLUSTERED 
(
	[textId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

