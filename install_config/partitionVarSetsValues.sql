USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_VarSetsValues_temp]    Script Date: 02/12/2013 11:11:07 ******/
BEGIN TRANSACTION
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_VarSetsValues_temp]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_VarSetsValues_temp]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Folders]') AND type in (N'U')) 
SELECT [dbo].[tbl_VarSetsValues].* INTO [dbo].[tbl_VarSetsValues_temp] FROM [dbo].[tbl_VarSetsValues]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_VarSetsValues]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_VarSetsValues]
GO

/****** Object:  Table [dbo].[tbl_VarSetsValues_temp]    Script Date: 02/12/2013 11:11:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

DROP PARTITION FUNCTION PRange_VarSetsValues

GO

CREATE PARTITION FUNCTION PRange_VarSetsValues (varchar(64))
AS RANGE LEFT FOR
VALUES (':BundleExcludePattern'
	   ,':BundleIncludePattern'
	   ,':PMISSREPORT'
	   ,':REPORTNAME'
	)

GO

DROP PARTITION SCHEME PScheme_VarSetsValues

CREATE PARTITION SCHEME PScheme_VarSetsValues
AS PARTITION PRange_VarSetsValues
ALL TO ([PRIMARY]);

GO

CREATE TABLE [dbo].[tbl_VarSetsValues](
	[VarSetName] [varchar](64) NOT NULL,
	[VarName] [varchar](64) NOT NULL,
	[VarValue] [varchar](64) NOT NULL,
 CONSTRAINT [PK_tbl_VarSetsValues] 
 PRIMARY KEY CLUSTERED ( [VarSetName] ASC, [VarName] ASC, [VarValue] ASC )
 WITH (PAD_INDEX  = OFF, 
      STATISTICS_NORECOMPUTE  = OFF, 
      IGNORE_DUP_KEY = OFF, 
      ALLOW_ROW_LOCKS  = ON, 
      ALLOW_PAGE_LOCKS  = ON, 
      FILLFACTOR = 80) ON PScheme_VarSetsValues(VarName)
 ) ON PScheme_VarSetsValues(VarName)
GO

CREATE NONCLUSTERED INDEX [IX_tbl_VarSetsValues]
ON [dbo].[tbl_VarSetsValues] ( [VarSetName] ASC, [VarName] ASC ) INCLUDE ( [VarValue] )
WITH (PAD_INDEX  = OFF
     ,STATISTICS_NORECOMPUTE  = OFF
     ,SORT_IN_TEMPDB = OFF
     ,IGNORE_DUP_KEY = OFF
     ,DROP_EXISTING = OFF
     ,ONLINE = OFF
     ,ALLOW_ROW_LOCKS  = ON
     ,ALLOW_PAGE_LOCKS  = ON
     ,FILLFACTOR = 80) ON PScheme_VarSetsValues(VarName)
GO

SET ANSI_PADDING OFF
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_VarSetsValues_temp]') AND type in (N'U'))
INSERT INTO tbl_VarSetsValues (VarSetName, VarName, VarValue)
SELECT VarSetName, VarName, VarValue from tbl_VarSetsValues_temp
GO
COMMIT

SELECT p.partition_number, fg.name, p.rows
FROM sys.partitions p
    INNER JOIN sys.allocation_units au
    ON au.container_id = p.hobt_id
    INNER JOIN sys.filegroups fg
    ON fg.data_space_id = au.data_space_id
WHERE p.object_id = OBJECT_ID('tbl_VarSetsValues')
