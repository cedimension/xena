/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Folders_temp]') AND type in (N'U')) DROP TABLE [dbo].[tbl_Folders_temp]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Folders]') AND type in (N'U')) SELECT [dbo].[tbl_Folders].* INTO [dbo].[tbl_Folders_temp] FROM [dbo].[tbl_Folders]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Folders]') AND type in (N'U')) DROP TABLE [dbo].[tbl_Folders]

CREATE TABLE dbo.tbl_Folders
	(
	FolderName varchar(64) NOT NULL,
	FolderDescr varchar(128) NOT NULL,
	IsActive bit NOT NULL,
	ParentFolder varchar(64) NOT NULL,
	FolderAddr varchar(500) NULL,
	SpecialInstr varchar(500) NULL,
	FolderNameLevel varchar(2) NULL,
	FolderNameKey varchar(64) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.tbl_Folders SET (LOCK_ESCALATION = TABLE)
GO


IF EXISTS(SELECT * FROM dbo.tbl_Folders_temp)
	 EXEC('INSERT INTO dbo.tbl_Folders (FolderName, FolderDescr, IsActive, ParentFolder, FolderNameLevel, FolderNameKey)
		SELECT FolderName, FolderDescr, IsActive, ParentFolder, FolderNameLevel, FolderNameKey FROM dbo.tbl_Folders_temp WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.tbl_Folders_temp
GO

COMMIT
