#!/usr/bin/perl

use SQL::Translator;
use Data::Dumper;
#use XReport::METABASE::SQLServer;

sub sqldo {
    my $fh = shift;
    print @_, "\nGO\n";
}

my $producer = $ARGV[0] || 'SQLServer';


my $class = 'XReport::METABASE::'.$producer;
eval "require $class";

my $xrMB = $class->new(metabase_loc => "$ENV{XREPORT_HOME}/bin/xrdb_metabase");

my $tblfh; # = new IO::File(">$outdir/tbl_$tableName.sql");
sqldo($tblfh, $xrMB->createDB(name => 'xreport', size => 56,
                            datapath => 'D:\DBDATA\MSSQL10_50.C0CTLPDBH\MSSQL\APPSDB',
                            logpath => 'L:\DBDATA\MSSQL10_50.C0CTLPDBH\MSSQL\APPSDB'));

my $tbls_ref = $xrMB->readTablesMetaData(
           initrows => 1,
           include => 1 );
                             
foreach my $tableName ( keys %$tbls_ref ) {
        my $table = delete $tbls_ref->{$tableName};
        sqldo($tblfh, $xrMB->SQLCreate(tables => [ $table ],
            tblpfx => 'tbl_', 
            tblsfx => '', 
            droptable => 1));
#        sqldo($tblfh, $xrMB->copyTable(table => $table,
#                insfx => '', outsfx => '_temp', where => $table->{keep})) if exists($table->{keep});
        sqldo($tblfh, $xrMB->genInsert(table => $table, tblpfx => 'tbl_', tblsfx => ''));  
}
    
