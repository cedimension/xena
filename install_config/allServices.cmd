
@SETLOCAL
@IF [%1]==[] (
@set sccmd=stop
) ELSE (
@set sccmd=%1
)

@set target=%2

@call :services %sccmd% %target%

@goto :EOF

:services

@call :callsc %1 EQTRISSTPRT01 %2 GTW11 "Jes Gateway Server 21" demand
@call :callsc %1 EQTRISSTPRT01 %2 INI11 "Work Dispatcher 21" demand
@call :callsc %1 EQTRISSTPRT01 %2 PDF11 "PDF Maker 21" auto
@call :callsc %1 EQTRISSTPRT01 %2 PDF12 "PDF Maker 22" auto

@call :callsc %1 EQTRISSTPRT02 %2 GTW21 "Jes Gateway Server 21" demand
@call :callsc %1 EQTRISSTPRT02 %2 INI21 "Work Dispatcher 21" demand
@call :callsc %1 EQTRISSTPRT02 %2 PDF21 "PDF Maker 21" auto
@call :callsc %1 EQTRISSTPRT02 %2 PDF22 "PDF Maker 22" auto

@call :callsc %1 EQTRISSTPRT03 %2 GTW31 "Jes Gateway Server 31" demand
@call :callsc %1 EQTRISSTPRT03 %2 INI31 "Work Dispatcher 31" demand
@call :callsc %1 EQTRISSTPRT03 %2 PDF31 "PDF Maker 31" auto
@call :callsc %1 EQTRISSTPRT03 %2 PDF32 "PDF Maker 32" auto

@call :callsc %1 EQTRISSTPRT04 %2 GTW41 "Jes Gateway Server 41" auto
@call :callsc %1 EQTRISSTPRT04 %2 INI41 "Work Dispatcher 41" auto
@call :callsc %1 EQTRISSTPRT04 %2 PDF41 "PDF Maker 41" auto
@call :callsc %1 EQTRISSTPRT04 %2 PDF42 "PDF Maker 42" demand

@call :callsc %1 EQTRISSTPRT05 %2 GTW51 "Jes Gateway Server 51" auto
@call :callsc %1 EQTRISSTPRT05 %2 INI51 "Work Dispatcher 51" auto
@call :callsc %1 EQTRISSTPRT05 %2 PDF51 "PDF Maker 51" auto
@call :callsc %1 EQTRISSTPRT05 %2 PDF52 "PDF Maker 52" demand

@call :callsc %1 EQTRISSTPRT06 %2 GTW61 "Jes Gateway Server 61" demand
@call :callsc %1 EQTRISSTPRT06 %2 INI61 "Work Dispatcher 61" demand
@call :callsc %1 EQTRISSTPRT06 %2 PDF61 "PDF Maker 61" demand
@call :callsc %1 EQTRISSTPRT06 %2 PDF62 "PDF Maker 62" demand

@call :callsc %1 EQTRISSTPRT07 %2 GTW71 "Jes Gateway Server 71" demand
@call :callsc %1 EQTRISSTPRT07 %2 INI71 "Work Dispatcher 71" demand
@call :callsc %1 EQTRISSTPRT07 %2 PDF71 "PDF Maker 71" demand
@call :callsc %1 EQTRISSTPRT07 %2 PDF72 "PDF Maker 72" demand

@goto :EOF

:callsc

@SETLOCAL
@set svcn=XR_%3_%4

sc \\%2 %1 %svcn%

@ENDLOCAL
@goto :EOF

:svc
@echo sc \\%2 %1 %3
@goto :EOF

:excluded

@ENDLOCAL