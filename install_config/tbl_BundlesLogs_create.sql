USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_BundlesLog]    Script Date: 06/12/2013 12:13:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_BundlesLog]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_BundlesLog]
GO

USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_BundlesLog]    Script Date: 06/12/2013 12:13:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_BundlesLog](
    [BundleId] [int] NOT NULL,
    [JobReportID] [int] NOT NULL,
    [ReportId] [int] NOT NULL,
    [ElabLines] [int] NULL,
    [ElabPages] [int] NULL,
    [ElabStartTime] [int] NULL,
    [ElabEndTime] [int] NULL
) ON [PRIMARY]

GO

