USE [ctdxreport]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ReportFormat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_ReportFormat]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_OutReportFormat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_OutReportFormat]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_IsActive]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_PrintFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_PrintFlag]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ViewOnlineFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_ViewOnlineFlag]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ActiveDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_ActiveDays]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ActiveGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_ActiveGens]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_HoldDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_HoldDays]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_HoldGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_HoldGens]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_StorageClass]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_StorageClass]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_LinesPerPage1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_LinesPerPage1]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_LinesPerPage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_LinesPerPage]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_PageOrient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_PageOrient]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_HasCc]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_HasCc]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_FontSize]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_FontSize]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_FitToPage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_FitToPage]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_CodePage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_CodePage]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_TotReports]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_TotReports]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_TotPages]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_TotPages]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ClusterIndex]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_ClusterIndex]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ParseFileName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_ParseFileName]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_LocalFileId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_LocalFileId]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_TargetLocalPathId_OUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_TargetLocalPathId_OUT]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_WorkType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_WorkType]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_MailTo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_MailTo]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_XrefData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_XrefData]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_CreateId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_CreateId]
END

GO

USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_JobReportNames]    Script Date: 09/20/2012 17:07:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportNames]
GO

USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_JobReportNames]    Script Date: 09/20/2012 17:07:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_JobReportNames](
	[JobReportName] [varchar](32) NOT NULL,
	[JobReportDescr] [varchar](64) NOT NULL,
	[ReportFormat] [tinyint] NOT NULL,
	[ElabFormat] [tinyint] NOT NULL,
	[IsActive] [char](1) NOT NULL,
	[PrintFlag] [char](1) NOT NULL,
	[ViewOnlineFlag] [char](1) NOT NULL,
	[ActiveDays] [smallint] NOT NULL,
	[ActiveGens] [smallint] NOT NULL,
	[HoldDays] [smallint] NOT NULL,
	[HoldGens] [smallint] NOT NULL,
	[StorageClass] [varchar](64) NOT NULL,
	[CharsPerLine] [smallint] NOT NULL,
	[LinesPerPage] [smallint] NOT NULL,
	[PageOrient] [char](1) NOT NULL,
	[HasCc] [bit] NOT NULL,
	[FontSize] [varchar](32) NOT NULL,
	[FitToPage] [bit] NOT NULL,
	[CodePage] [varchar](32) NOT NULL,
	[TotJobReports] [int] NOT NULL,
	[TotPages] [decimal](18, 0) NOT NULL,
	[ClIndexTable] [varchar](64) NOT NULL,
	[ParseFileName] [varchar](64) NOT NULL,
	[TargetLocalPathId_IN] [varchar](32) NOT NULL,
	[TargetLocalPathId_OUT] [varchar](32) NOT NULL,
	[TypeOfWork] [smallint] NOT NULL,
	[WorkClass] [smallint] NULL,
	[MailTo] [varchar](64) NOT NULL,
	[XrefData] [varchar](64) NOT NULL,
	[ModifyKey] [varchar](16) NOT NULL,
	[ModifyTime] [decimal](18, 0) NULL,
 CONSTRAINT [PK_tbl_JobReportNames] PRIMARY KEY CLUSTERED 
(
	[JobReportName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_ReportFormat]  DEFAULT (2) FOR [ReportFormat]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_OutReportFormat]  DEFAULT (1) FOR [ElabFormat]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_IsActive]  DEFAULT (1) FOR [IsActive]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_PrintFlag]  DEFAULT ('Y') FOR [PrintFlag]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_ViewOnlineFlag]  DEFAULT ('Y') FOR [ViewOnlineFlag]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_ActiveDays]  DEFAULT (5) FOR [ActiveDays]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_ActiveGens]  DEFAULT (5) FOR [ActiveGens]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_HoldDays]  DEFAULT (30) FOR [HoldDays]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_HoldGens]  DEFAULT (30) FOR [HoldGens]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_StorageClass]  DEFAULT ('') FOR [StorageClass]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_LinesPerPage1]  DEFAULT (133) FOR [CharsPerLine]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_LinesPerPage]  DEFAULT (70) FOR [LinesPerPage]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_PageOrient]  DEFAULT ('L') FOR [PageOrient]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_HasCc]  DEFAULT (1) FOR [HasCc]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_FontSize]  DEFAULT ('[9.0 8.24]') FOR [FontSize]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_FitToPage]  DEFAULT (0) FOR [FitToPage]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_CodePage]  DEFAULT ('') FOR [CodePage]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_TotReports]  DEFAULT (0) FOR [TotJobReports]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_TotPages]  DEFAULT (0) FOR [TotPages]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_ClusterIndex]  DEFAULT ('') FOR [ClIndexTable]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_ParseFileName]  DEFAULT ('*,NULL.XML') FOR [ParseFileName]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_LocalFileId]  DEFAULT ('L1') FOR [TargetLocalPathId_IN]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_TargetLocalPathId_OUT]  DEFAULT ('') FOR [TargetLocalPathId_OUT]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_WorkType]  DEFAULT (1) FOR [TypeOfWork]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_MailTo]  DEFAULT ('') FOR [MailTo]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_XrefData]  DEFAULT ('') FOR [XrefData]
GO

ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_CreateId]  DEFAULT ('') FOR [ModifyKey]
GO

