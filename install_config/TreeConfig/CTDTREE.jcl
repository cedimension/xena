//CTDA1KY1 JOB (Y10000),CTD,MSGCLASS=K,CLASS=A,                         
//             REGION=0M,                                               
//             SCHENV=Y1BATCH0                                          
/*XEQ   Y1                                                              
//**SCHENV/*JOBPARM SYSAFF=IT2U                                         
/*ROUTE PRINT Y1                                                        
//*%OPC SCAN                                                            
//*     LAVORO NON RIESEGUIBILE                                         
//OUTDEF  OUTPUT  FORMS=0STD,DEFAULT=YES,JESDS=ALL,                     
//        OUTDISP=(WRITE,HOLD)                                          
//*                                                                     
//         JCLLIB  ORDER=GMSYS.CTDA1.IOA.PROCLIB                        
//         INCLUDE MEMBER=IOASETA1                                      
//*                                                                     
//**********************************************************************
//*   ESEGUE COMPRESS PREVENTIVO LIBRERIA GVSYS.CTD.AUTOM.TREE.UNICO   *
//**********************************************************************
//*                                                                     
//STEP03   EXEC PGM=IEBCOPY                                             
//SYSPRINT DD  SYSOUT=*                                                 
//IN       DD  DSN=GVSYS.CTD.AUTOM.TREE.UNICO,DISP=SHR                  
//SYSIN    DD  *                                                        
 COPY INDD=IN,OUTDD=IN                                                  
//*                                                                     
//**********************************************************************
//*                COPIA DA SCARICO NAS C0.....TGTB0001                *
//**********************************************************************
//*                                                                     
//STEP04   EXEC PGM=ICEGENER                                            
//SYSUT1   DD DSN=C0.NAS.B2.TG00BDA0.TGTB0001(0),DISP=SHR               
//SYSUT2   DD DSN=GVSYS.CTD.COPIAC0.TGTB0001,DISP=OLD,                  
//*        DISP=(NEW,CATLG,DELETE),SPACE=(CYL,(10,10),RLSE),            
//         DCB=*.SYSUT1                                                 
//SYSIN    DD  DUMMY                                                    
//SYSUDUMP DD SYSOUT=*                                                  
//SYSPRINT DD SYSOUT=*                                                  
//*                                                                     
//**********************************************************************
//*                ESEGUE CLIST PER CREAZIONE TREONE4C                  
//**********************************************************************
//*                                                                     
//STEP05   EXEC USI0ISPF                                                
//SYSEXEC  DD  DISP=SHR,DSN=SYS3.XSTV.EXECLIB                           
//         DD  DISP=SHR,DSN=SYS3.XSTV.ISRXLIB                           
//*%OPC BEGIN ACTION=NOSCAN                                             
//SYSTSIN   DD  *                                                       
 ISPSTART CMD(%CTDTREC0)                                                
//*%OPC END ACTION=NOSCAN                                               
//*                                                                     
//*                                                                     
//STEP06   EXEC PGM=IDCAMS                                              
//INPUT    DD *                                                         
 BEGIN BUILD AND CHECK TREE                                             
//OUTPUT   DD DSN=GMSYS.CTDY1.CTD.XSTV.DATI(A1SPYTRE),DISP=SHR          
//SYSPRINT DD SYSOUT=*                                                  
//SYSOUT   DD SYSOUT=*                                                  
//SYSIN    DD *                                                        
 REPRO INFILE(INPUT) OUTFILE(OUTPUT)                                   
//*                                                                    
//STOP07   EXEC PGM=WTOOPA,COND=(0,EQ,STEP06)                          
//SYSOUX   DD SYSOUT=*                                                 
//SYSPRINT DD SYSOUT=*,DCB=LRECL=80                                    
//SYSIN    DD *                                                        
*******************************************                            
STEP - STEP06   JOB %OJOBNAME *  ERRORE                                
*******************************************                            
//*                                                                    
//STEP08   EXEC PGM=ICEGENER                                           
//SYSUT1   DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(UNICO),DISP=SHR      
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(HEAD14C),DISP=SHR         
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(TREONE4C),DISP=SHR        
//*        DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(TREPRO4C),DISP=SHR        
//         DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(UPA),DISP=SHR        
//*        DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(UCBP),DISP=SHR            
//         DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(HOLDNEW),DISP=SHR    
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(UCA),DISP=SHR              
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(USINEW),DISP=SHR           
//*        DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(HOLDNEW),DISP=SHR          
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(UBCASA),DISP=SHR           
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(CREDOLD),DISP=SHR          
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(UBMNEW),DISP=SHR           
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(TRADLAB),DISP=SHR          
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(XELION),DISP=SHR           
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(CARTOUBC),DISP=SHR         
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(PIONEER),DISP=SHR          
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(BUMBRIA),DISP=SHR          
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(UGCBANCA),DISP=SHR         
//         DD DSN=GVSYS.CTD.AUTOM.TREE.UNICO(CRCARPI),DISP=SHR          
//         DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEVERO(BANCACO),DISP=SHR     
//         DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEVERO(BANCAPB),DISP=SHR     
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(UPA),DISP=SHR         
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(UCA),DISP=SHR         
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(USINEW),DISP=SHR      
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(HOLDNEW),DISP=SHR     
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(UBCASA),DISP=SHR      
//* NO     DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(BANCAS3),DISP=SHR     
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(CREDOLD),DISP=SHR     
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(UBMNEW),DISP=SHR      
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(TRADLAB),DISP=SHR     
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(XELION),DISP=SHR      
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEMILA(CARTOUBC),DISP=SHR    
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEVERO(PIONEER),DISP=SHR     
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEVERO(BUMBRIA),DISP=SHR     
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEVERO(UGCBANCA),DISP=SHR    
//*        DD DSN=GMSYS.CTDY1.CTD.SOURCE.TREEVERO(CRCARPI),DISP=SHR     
//*        DD DSN=GMSYS.CTDY1.CTD.AUTOM.TREE(C0BODYNW),DISP=SHR         
//*        DD DSN=GMSYS.CTDY1.CTD.AUTOM.TREE(C0FILITU),DISP=SHR         
//         DD DSN=GMSYS.CTDY1.CTD.AUTOM.TREE(BDBODYNW),DISP=SHR         
//         DD DSN=GMSYS.CTDY1.CTD.AUTOM.TREE(BRBODYNW),DISP=SHR         
//*        DD DSN=GMSYS.CTDY1.CTD.AUTOM.TREE(C0BODYOL),DISP=SHR         
//*        DD DSN=GMSYS.CTDY1.CTD.AUTOM.TREE(C0HEAD),DISP=SHR           
//SYSUT2   DD DSN=GMSYS.CTDY1.CTD.TREE.MILANO(CTDA1),DISP=OLD           
//SYSPRINT DD SYSOUT=*                                                  
//SYSOUT   DD SYSOUT=*                                                  
//SYSIN    DD DUMMY                                                     
//*                                                                     
//STOP09   EXEC PGM=WTOOPA,COND=(0,EQ,STEP08)                           
//SYSOUX   DD SYSOUT=*                                                  
//SYSPRINT DD SYSOUT=*,DCB=LRECL=80                                     
//SYSIN    DD *                                                         
*******************************************                             
STEP - STEP08   JOB %OJOBNAME *  ERRORE                                 
*******************************************                             
//*                                                                     
//STEP10   EXEC IOARKSL                                                 
//SPIA      DD DSN=GMSYS.CTDY1.CTD.XSTV.DATI(A1SPYTRE),DISP=SHR         
//DAKSLOUT  DD SYSOUT=*                                                 
  TRACE ON                                                              
  MAXCOMMAND 999999                                                     
  CALLMEM CTDA1KY1                                                      
  END                                                                   
//*                                                                     
//STOP11   EXEC PGM=WTOOPA,COND=(0,EQ,STEP10.KSL)                       
//SYSOUX   DD SYSOUT=*                                                  
//SYSPRINT DD SYSOUT=*,DCB=LRECL=80                                     
//SYSIN    DD *                                                         
*******************************************                             
STEP - STEP10   JOB %OJOBNAME *  ERRORE                                 
*******************************************                             
//*                                                                     
//TEST12   EXEC PGM=IDCAMS                                              
//INPUT    DD DSN=GMSYS.CTDY1.CTD.XSTV.DATI(A1SPYTRE),DISP=SHR          
//OUTPUT   DD DSN=&&TEST12,DISP=(,DELETE,DELETE),                       
//         SPACE=(TRK,(1,1),RLSE),                                      
//         DCB=(LRECL=80,RECFM=FB,BLKSIZE=0)                            
//SYSOUT   DD SYSOUT=*                                                  
//SYSPRINT DD SYSOUT=*                                                  
//SYSIN    DD *                                                         
  REPRO INFILE (INPUT) COUNT (1) OUTFILE (OUTPUT)                       
//*                                                                     
//IFTEST13 IF (TEST12.RC ^= 04) THEN                                    
//*                                                                     
//STOP13   EXEC PGM=WTOOPA                                              
//SYSOUX   DD SYSOUT=*                                                  
//SYSPRINT DD SYSOUT=*,DCB=LRECL=80                                     
//SYSIN    DD *                                                         
*******************************************                             
STEP - STEP13   JOB %OJOBNAME *  ERRORE                                 
*******************************************                             
//*                                                                     
//ENDIF13    ENDIF                                                      
//*                                                                     
//IFTEST14 IF (TEST12.RC = 04) THEN                                     
//*                                                                     
//STEP15   EXEC PGM=ICEGENER                                            
//SYSUT1   DD DSN=GMSYS.CTDA1.CTD.PARM(CTDTREE),DISP=SHR                
//SYSUT2   DD DSN=GMSYS.CTDY1.CTD.TREE.MILANO(BKPCTDA1),DISP=OLD        
//SYSPRINT DD SYSOUT=*                                                  
//SYSOUT   DD SYSOUT=*                                                  
//SYSIN    DD DUMMY                                                     
//*                                                                   
//STOP16   EXEC PGM=WTOOPA,COND=(0,EQ,STEP15)                         
//SYSOUX   DD SYSOUT=*                                                
//SYSPRINT DD SYSOUT=*,DCB=LRECL=80                                   
//SYSIN    DD *                                                       
*******************************************                           
STEP - STEP15   JOB %OJOBNAME *  ERRORE                               
*******************************************                           
//*                                                                   
//STEP20   EXEC PGM=ICEGENER                                          
//SYSUT1   DD DSN=GMSYS.CTDY1.CTD.TREE.MILANO(CTDA1),DISP=OLD         
//SYSUT2   DD DSN=GMSYS.CTDA1.CTD.PARM(CTDTREE),DISP=SHR              
//SYSPRINT DD SYSOUT=*                                                
//SYSOUT   DD SYSOUT=*                                                
//SYSIN    DD DUMMY                                                   
//*                                                                   
//STOP21   EXEC PGM=WTOOPA,COND=(0,EQ,STEP20)                         
//SYSOUX   DD SYSOUT=*                                                
//SYSPRINT DD SYSOUT=*,DCB=LRECL=80                                   
//SYSIN    DD *                                                         
*******************************************                             
STEP - STEP20   JOB %OJOBNAME *  ERRORE                                 
*******************************************                             
//*                                                                     
//STEP30   EXEC PGM=ICEGENER                                            
//SYSPRINT DD SYSOUT=*                                                  
//SYSIN    DD DUMMY                                                     
//SYSUT2   DD SYSOUT=(,INTRDR),DCB=BLKSIZE=80                           
//SYSUT1   DD DATA,DLM=��                                               
/*$VS,'RO IT2U,F CTDA1,LOADTREE'                                        
/*$VS,'RO IT2U,F CTDA1OM,LOADTREE'                                      
/*$VS,'RO IT2U,F CTDA1OM1,LOADTREE'                                     
/*$VS,'RO IT2U,F CTDA1POD,MODASID,LOADTREE'                             
��                                                                      
//*                                                                     
//ENDIF14    ENDIF                                                      
//*                                                                     
//STEP40 EXEC PGM=ICEGENER                                              
//SYSPRINT DD SYSOUT=*                                                  
//SYSOUT   DD SYSOUT=*                                                  
//SYSIN    DD DUMMY                                                     
//SYSUT1  DD DSN=GMSYS.CTDA1.CTD.PARM(CTDTREE),DISP=SHR                 
//SYSUT2  DD DSN=SYS4.CTDX1.CTD.PARM(CTDTREA1),                         
//        SUBSYS=(CSM,'SYSTEM=PLEXIT5X'),DISP=OLD                       
//*                                                                     
//*************************************                                 
//* ASTERISCATO PASSO 14.07.2011 F.T. *                                 
//*************************************                                 
//*                                                                     
//*STEP50 EXEC PGM=ICEGENER                                             
//*SYSPRINT DD SYSOUT=*                                                 
//*SYSOUT   DD SYSOUT=*                                                 
//*SYSIN    DD DUMMY                                                    
//*SYSUT1  DD DSN=GMSYS.CTDA1.CTD.PARM(CTDTREE),DISP=SHR                
//*SYSUT2  DD DSN=GMSYS.CTDZ1.CTD.PARM(CTDTREE),                        
//*        SUBSYS=(CSM,'SYSTEM=PLEXSSAX'),DISP=SHR                      