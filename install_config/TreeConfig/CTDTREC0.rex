/*  Compiled REXX - For objects maintenance use LREX tool  */                   
/************************************************************/                  
/*CREA TREE PER BANCONE DA SAVE TABELLA  TGTB0001          **/                  
/*        CODICI ABEND                                     **/                  
/* EXIT 97   -  ALLOCAZIONI FILE FALLITE                   **/                  
/* EXIT 93   -  SYSPLEX ERRATO                             **/                  
/* EXIT 92   -  ERRORI IN QUADRATURE                       **/                  
/************************************************************/                  
EXIT97 = '97'                                                                   
EXIT93 = '93'                                                                   
EXIT92 = '92'                                                                   
PROFILE NOPREFIX                                                                
ADDRESS ISPEXEC                                                                 
"VGET (ZSYSPLEX) SHARED"                                                        
SELECT                                                                          
  WHEN ZSYSPLEX = 'ITAMIL01' | ,                                                
       ZSYSPLEX = 'ITAVER01'  THEN DO                                           
 /* READ2 = 'US02041.COPIAC0.TGTB0001' */                                       
    READ2 = 'GVSYS.CTD.COPIAC0.TGTB0001'                                        
    WRITE = 'GVSYS.CTD.AUTOM.TREE.UNICO(TREONE4C)'                              
  END                                                                           
  OTHERWISE DO                                                                  
    SAY 'ERRORE SYSPLEX NON PREVISTO'                                           
    SAY ZSYSPLEX                                                                
    ZISPFRC = EXIT93                                                            
    ADDRESS "ISPEXEC " "VPUT (ZISPFRC) SHARED"                                  
    EXIT(EXIT93)                                                                
  END                                                                           
END                                                                             
                                                                                
ADDRESS TSO                                                                     
                                                                                
PARSE VALUE DSNINFO("'"READ2"'") WITH RC INFO                                   
IF RC = 0 THEN DO                                                               
  PARSE VAR INFO DSNAME VOLSER UNIT DSORG RECFM LRECL,                          
    BLKSIZE KEYLEN RKP CREATED EXPIRES REFERENCED,                              
    PASSWORD UPDATED RACFPROF MANAGED DDNAME,                                   
    STATUS NDISP CDISP                                                          
END                                                                             
                                                                                
"ALLOC DDNAME(DTST2) DSN ("DSNAME") SHR"                                        
                                                                                
IF RC ^= 0 THEN DO                                                              
  SAY 'ERRORE ALLOCAZIONE FILE '                                                
  SAY READ2                                                                     
  ZISPFRC = EXIT97                                                              
  ADDRESS "ISPEXEC " "VPUT (ZISPFRC) SHARED"                                    
  EXIT(EXIT97)                                                                  
END                                                                             
                                                                                
ADDRESS MVS "EXECIO * DISKR DTST2 (STEM DATO. FINIS)"                           
"FREE DDNAME(DTST2)"                                                            
                                                                                
IF DATO.0 < 8 THEN DO                                                           
  SAY '                                                '                        
  SAY '   ERRORI IN QUADRATURE                         '                        
  SAY '                                                '                        
  SAY " ERRORE - FLUSSO DA TGTB0001   VUOTO O          "                        
  SAY "           PROBABILMENTE INCOMPLETO             "                        
  SAY "                                                "                        
  ZISPFRC = EXIT92                                                              
  ADDRESS "ISPEXEC " "VPUT (ZISPFRC) SHARED"                                    
  EXIT(EXIT92)                                                                  
END                                                                             
                                                                                
ESCLUSI = '06601 06865 07595 07609 09232 09285 09289'                           
                                                                                
CTR_USER = '0'                                                                  
NEWSTACK                                                                        
DO XX = 1 TO DATO.0                                                             
  CALL INFO                                                                     
                                                                                
  IF POS(USER1,ESCLUSI) <> '0' THEN ITERATE                                     
                                                                                
  LIVELLO = 35                                                                  
  POLO = 'RETAIL'                                                               
  PP = 'RT'                                                                     
  AZIENDA = "UNICREDIT RETAIL BANKING"                                          
                                                                                
                                                                                
  IF STATO98 = '98' THEN POLO =FILCLOSE                                         
  USER4 = PP||USER3                                                             
  POLO = LEFT(POLO,8,' ')                                                       
                                                                                
    CTR_USER = CTR_USER + 1                                                     
    CALL REPO1                                                                  
END                                                                             
                                                                                
CTR_TREE_OUT = QUEUED()                                                         
                                                                                
SAY '                                                   '                       
SAY '    CTDtrec0 -  CREA TREONE4C BANCA C0 - LIVELLO 60 ( RT ) '               
SAY '                U.O. OPEN - FILCLOSE NO UR E NO CE          '              
SAY '                                                   '                       
SAY ' A - NUMERO RECORD INPUT        (TGTB0001)          ' DATO.0               
SAY ' B - NUMERO RECORD TREE OUTPUT  (BDLIV60)           ' CTR_TREE_OUT         
SAY ' C - NUMERO RECIPIENT                               ' CTR_USER             
SAY '                                                   '                       
                                                                                
IF DATO.0 >= CTR_TREE_OUT THEN DO                                               
       SAY '   ERRORI IN QUADRATURE FINALI                '                     
       SAY '                                              '                     
       SAY "            A      NON E' <      DI  B        "                     
       ZISPFRC = EXIT92                                                         
       ADDRESS "ISPEXEC " "VPUT (ZISPFRC) SHARED"                               
       EXIT(EXIT92)                                                             
END                                                                             
                                                                                
CALL SCRIVO                                                                     
EXIT                                                                            
                                                                                
INFO:                                                                           
                                                                                
  USER      = SUBSTR(DATO.XX,31,39)                                             
  DEST      = SUBSTR(DATO.XX,35,35)                                             
  USER1     = STRIP(SUBSTR(DATO.XX,26,5))                                       
  LUNG      = LENGTH(USER1)                                                     
  USER2     = RIGHT(USER1,5,'0')                                                
  USER3     = LEFT(USER2,6,' ')                                                 
  PROV      = SUBSTR(DATO.XX,166,2)                                             
  INDIRIZZO = SUBSTR(DATO.XX,168,42)                                            
  CAP       = SUBSTR(DATO.XX,210,5)                                             
  COMUNE    = SUBSTR(DATO.XX,215,30)                                            
  AMB_PROV  = SUBSTR(DATO.XX,265,1)                                             
  TIPO_DIP  = SUBSTR(DATO.XX,277,2)                                             
  MAIL      = SUBSTR(DATO.XX,319,50)                                            
  STATO98   = SUBSTR(DATO.XX,371,2)                                             
  BANCA     = SUBSTR(DATO.XX,399,2)                                             
  INDIR2    = CAP||COMUNE                                                       
                                                                                
RETURN                                                                          
                                                                                
REPO1:                                                                          
    S='U60'USER4||LIVELLO||POLO||DEST                                           
QUEUE S                                                                         
IF POS('.',MAIL) > 0 THEN DO                                                    
   S ='D%ADDR%='MAIL                                                            
   QUEUE S                                                                      
END                                                                             
S ='D%ADDR%='AZIENDA                                                            
QUEUE S                                                                         
S ='D%ADDR%='DEST                                                               
QUEUE S                                                                         
IF INDIRIZZO ^= '                                          ' THEN DO            
S ='D%ADDR%='INDIRIZZO                                                          
QUEUE S                                                                         
END                                                                             
IF PROV = '  ' THEN                                                             
   PROV1 = '     '                                                              
  ELSE DO                                                                       
   PROV1 = ' ('||PROV||')'                                                      
END                                                                             
IF INDIR2 ^= '                                   ' THEN DO                      
S ='D%ADDR%='CAP || ' ' || COMUNE || PROV1                                      
QUEUE S                                                                         
END                                                                             
/*IF STATO98 ^= '98' THEN DO                                                    
  DO HH = 1 TO 6 - LUNG                                                         
   WW = 'S'                                                                     
   WW = LEFT(WW,HH,0)                                                           
   S=WW||USER1                                                                  
   QUEUE S                                                                      
  END                                                                           
END*/                                                                           
/*IF STATO98 ^= '98' THEN DO */                                                 
  USER5 = USER1 + 0                                                             
  LUNG1 = LENGTH(USER5)                                                         
 DO HH = 1 TO 6 - LUNG1                                                         
  WW = 'S'                                                                      
  S=WW||USER5                                                                   
  QUEUE S                                                                       
  USER5 ='0'||USER5                                                             
 END                                                                            
/*END*/                                                                         
S='C         YYDFT'                                                             
QUEUE S                                                                         
RETURN                                                                          
                                                                                
SCRIVO:                                                                         
                                                                                
QUEUE                                                                           
"ALLOC DDNAME(PRO2) DSN("WRITE")"                                               
                                                                                
 IF RC ^= 0 THEN DO                                                             
   SAY 'ERRORE ALLOCAZIONE FILE '                                               
   SAY WRITE                                                                    
   ZISPFRC = EXIT97                                                             
   ADDRESS "ISPEXEC " "VPUT (ZISPFRC) SHARED"                                   
   EXIT(EXIT97)                                                                 
 END                                                                            
                                                                                
ADDRESS MVS "EXECIO * DISKW PRO2 (FINIS"                                        
"FREE DDNAME(PRO2)"                                                             
DELSTACK                                                                        
RETURN                                                                          
