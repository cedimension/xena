--<ScriptOptions statementTerminator="GO"/>

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE PROCEDURE [dbo].[ce_Insert_tbl_folders]    
  @FolderName  as  varchar(32),    
  @Descr  as  varchar(128),    
  @Inst   as  char(2),    
  @Parent as  varchar(32),
  @Banca as  varchar(32)     
 AS    
 
 DECLARE  @Cousin varchar(10)

--controlli per cosnoscere se parent � Zxxxxxx
BEGIN
   IF EXISTS ( 
      SELECT DISTINCT TG_FIL_SUP
           FROM  [CTDXREPORT].[dbo].tgtb1001
           WHERE     (TG_FIL_SUP = SUBSTRING(@Parent, 3, 5)) 
   )
   BEGIN
      IF NOT EXISTS ( 
          SELECT DISTINCT TG_COD_DIP
           FROM  [CTDXREPORT].[dbo].tgtb1001
           WHERE     (TG_COD_DIP = SUBSTRING(@FolderName, 3, 5)) AND (TG_FIL_SUP = SUBSTRING(@Parent, 3, 5)) 
      )
      BEGIN
           IF NOT EXISTS ( 
               SELECT DISTINCT FOLDERNAME
                 FROM  [CTDXREPORT].[dbo].[tbl_Folders]
                WHERE foldername = 'Z'+RTRIM(cast(convert(int,SUBSTRING(@Parent,3,5)) as char(5)))
           )
		   BEGIN
 	          INSERT INTO [ctdxreport].[dbo].[tbl_Folders]
	              (FolderName, FolderDescr, Istituto, IsActive, HasChilds, HasReports, ParentFolder)	
	          VALUES
		          ('Z'+rtrim(cast(convert(int,SUBSTRING(@Parent,3,5)) as char(5))), ' ', @Inst, 1, 1, 1, @Parent)  
 	       END
 	      SET @Parent = 'Z'+rtrim(cast(convert(int,SUBSTRING(@Parent,3,5)) as char(5)))       
      END
   END
END


	 --Inserisco eventule cugino in tabella tbl_profilesfolderstrees
--BEGIN
--    IF EXISTS ( 
--          SELECT DISTINCT TG_COD_DIP
--           FROM  [CTDXREPORT].[dbo].tgtb1001
--           WHERE     (TG_COD_DIP = SUBSTRING(@FolderName, 3, 5)) 
--    )
--    BEGIN
--	 SET @Cousin = (SELECT DISTINCT TG_FIL_SUP
--           FROM  [CTDXREPORT].[dbo].tgtb1001
--           WHERE     (TG_COD_DIP = SUBSTRING(@FolderName, 3, 5)) )
--	   
--	 IF NOT EXISTS ( 
--	    SELECT DISTINCT PROFILENAME
--	       FROM  [CTDXREPORT].[dbo].[tbl_profileSfolderstrees]
--	          WHERE profilename = @FolderName and substring(rootnode,1,1) = 'Z'
--	    )
--	      BEGIN
--	          INSERT INTO [ctdxreport].[dbo].[tbl_profilesfolderstrees]
--	             (ProfileName, RootNode)	
--	          VALUES
--		         (@FolderName, 'Z'+rtrim(cast(convert(int,@Cousin) as char(5))))  
--	      END
--	    ELSE 
--	    BEGIN
--	         UPDATE [ctdxreport].[dbo].[tbl_profilesfolderstrees]
--	         SET RootNode = 'Z'+rtrim(cast(convert(int,@Cousin) as char(5)))
--	         WHERE ProfileName = @FolderName and substring(rootnode,1,1) = 'Z'
--	    END
--    END	
--END     

--inserisco o updato folder nelle varie tabelle
BEGIN        
	IF EXISTS (    
	   SELECT foldername FROM     
	   tbl_Folders   
	   where FolderName = @FolderName      
	)     
	BEGIN    
	   --PRINT 'Faccio Update.'
	   UPDATE [ctdxreport].[dbo].[tbl_Folders]
	   SET FolderDescr = @descr, ParentFolder = @Parent
	   WHERE FolderName = @FolderName 
		
	END       
	ELSE
	BEGIN  
		--PRINT 'Faccio Insert.'  
		INSERT INTO [ctdxreport].[dbo].[tbl_Folders]
				   (FolderName, FolderDescr, Istituto, IsActive, HasChilds, HasReports, ParentFolder)	
			 VALUES
				   (@FolderName, @Descr, @Inst, 1, 1, 1, @Parent)
		
		INSERT INTO [ctdxreport].[dbo].[tbl_FoldersReportNames]
					   (FolderName, ReportRule, FilterVar, FilterRule, RecipientRule, FORBID)	
			VALUES
					   (@FolderName, 'Fi='+@Banca, @Banca, 'Fo='+@FolderName, ' ', '0')

		INSERT INTO [ctdxreport].[dbo].[tbl_FoldersReportNames]
					   (FolderName, ReportRule, FilterVar, FilterRule, RecipientRule, FORBID)	
				 VALUES
					   (@FolderName, 'Fo='+@FolderName, ' ', ' ', ' ', '0')
        
		IF (substring(@FolderName,1,2) in ('C0', 'BR', 'BD', 'U3', 'U6'))
        BEGIN
		     INSERT INTO [ctdxreport].[dbo].[tbl_VarSetsValues]
				    	   (VarSetName, Varname, VarValue)	
			    	 VALUES
					       ('Fo='+@FolderName, 'FIL_BANCAS3', substring(@FolderName,3,5))
--		     INSERT INTO [ctdxreport].[dbo].[tbl_VarSetsValues]
--				    	   (VarSetName, Varname, VarValue)	
--			    	 VALUES
--					       ('Fo='+@FolderName, 'FIL_BROMA', substring(@FolderName,3,5))
--		     INSERT INTO [ctdxreport].[dbo].[tbl_VarSetsValues]
--				    	   (VarSetName, Varname, VarValue)	
--			    	 VALUES
--					       ('Fo='+@FolderName, 'FIL_BDS', substring(@FolderName,3,5))
        END
        ELSE
        BEGIN
		     INSERT INTO [ctdxreport].[dbo].[tbl_VarSetsValues]
				    	   (VarSetName, Varname, VarValue)	
			    	 VALUES
					       ('Fo='+@FolderName, @Banca, substring(@FolderName,3,5))
        END 
	END
END
GO

SET ANSI_PADDING OFF
GO

