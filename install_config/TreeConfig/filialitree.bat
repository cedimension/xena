


rem la clist insert_tbl_tgtb1001.pl ricrea le informazioni sulla tabella 1001 (quella dei famosi cugini).
rem il VBS filialitree esegue una query host e crea il file per il perl seguente (insert_tbl_folders_vari.pl).
rem la clist insert_tbl_folders_vari.pl xx, dove xx = ambito, non tratta la filiale 9930 ne quelle con parentfolder 0

rem ***************************************************************
rem    bloccato scarico della gerarchia 27 dalla tabella 1001
rem    in attesa che venga ripopolata con dati corretti
rem    al momento si utilizza file XLS inviato da R.Donda con la 
rem    situazione aggiornata
rem ***************************************************************
rem CuginiRetail.vbs
rem


rem perl insert_tbl_tgtb1001.pl
sleep 2
filialitree.vbs C0
rem ************* NON ESEGUIRE BD BR U3 U6 UR **********************
rem sleep 2
rem filialitree.vbs U3
rem sleep 2
rem filialitree.vbs U6
rem sleep 2
rem filialitree.vbs UR
rem sleep 2
rem filialitree.vbs BR
rem sleep 2
rem filialitree.vbs BD
rem ****************************************************************
sleep 2
perl insert_tbl_folders_vari.pl C0
rem ************* NON ESEGUIRE BD BR U3 U6 UR **********************
rem sleep 2
rem perl insert_tbl_folders_vari.pl BR
rem sleep 2
rem perl insert_tbl_folders_vari.pl U3
rem sleep 2
rem perl insert_tbl_folders_vari.pl U6
rem sleep 2
rem perl insert_tbl_folders_vari.pl UR
rem sleep 2
rem perl insert_tbl_folders_vari.pl BD
rem ****************************************************************
