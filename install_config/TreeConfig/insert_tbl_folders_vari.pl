# start command perl insert_tbl_folder_vari.pl $Ambi (c0, u3, u6, br etc)


use lib("$ENV{'XREPORT_HOME'}/perllib");

use strict;

use Time::localtime;
use XReport::DBUtil;

my $Ambi = $ARGV[0];
my $src_dir = "d:\\memowebperl\\Filialitree".$Ambi.".csv";

my $giorno = localtime(time)->mday;
my $mese = localtime(time)->mon + 1;
my $year = localtime(time)->year + 1900;
my $data = "$giorno.$mese.$year"; 

my $fileNameLogError = "D:/memowebperl/errorfiltree_".$Ambi."_".$data."_".time().".log";

my $dbr;

my %poolsBanca = (
  a01 => 'FIL_BANCAS3', 
  a04 => 'FIL_BRA', 
  a05 => 'FIL_FOSSANO',
  a08 => 'FIL_SALUZZO',
  a09 => 'FIL_SAVIGLIA',
  a23 => 'FIL_BROMA',
  a24 => 'FIL_UGIS',
  a27 => 'FIL_UCA',
  a29 => 'FIL_BZIVNO',
  a33 => 'FIL_MVENEZIE',
  a41 => 'FIL_CARTOUBC',
  a45 => 'FIL_UBMNEW',
  a47 => 'FIL_HOLDING',
  a48 => 'FIL_UPA',
  a49 => 'FIL_XELION',
  a52 => 'FIL_BANCAS3', 
  a59 => 'FIL_TRADLAB',
  a60 => 'FIL_BUMBRIA',
  a61 => 'FIL_BANCAS3',
  a62 => 'FIL_BDS',
  a67 => 'FIL_CLARIMA',
  a69 => 'FIL_CRCARPI',
  a71 => 'FIL_BIPOP',
  a86 => 'FIL_BANCAS3',   
  a83 => 'FIL_BANCAS3',      
);


eval{  
	open FH, $src_dir or die "non esiste il file $src_dir $!";
	  
	while (<FH>) {
		#print "AAA $_\n";
		#s/[^0-9 -;\w]//g;
		#$_ =~ s/[^0-9 ;-\w]//g;
		$_ =~ s/[^0-9 ';-\w]//g;
		$_ =~ s/[']/''/g;
		my $go = 1 ;
		#print "bbb  $_\n";
	
		my @parole = split /;+/, $_;
		my $agenzia = $parole[1];
		my $descr = $parole[2];
		my $sup = $parole[3];
		my $torre = $parole[4];
		$go = 0 if $sup == 0;
		$go = 0 if $sup == 9930;
		print "bbb0 $go $sup $torre\n"; 
		
		if ($go) {
			#$torre = 'UR'; 
			$torre = 'C0' if $torre =~ /^(?:S0)$/;
			print "bbb $go $sup $torre\n";
			my $istituto = $parole[0];
			#print "aaa  $istituto\n";
			$istituto = '01' if $istituto =~ /^(?:52|83|86|1)$/;
			my $banca = $poolsBanca{'a'.$istituto};
			
			#print "bbb  $istituto\n";
			
		        #my $folder = $torre.$agenzia;
			
			my $supc = $torre.substr('00000'.$sup,-5);
			my $filiale = $torre.substr('00000'.$agenzia,-5);
			print "bbb $agenzia 'xx' $descr 'yy'  $supc 'rr' $filiale\n";
			
			
			$dbr = dbExecute(                                                                                               
		  		#"tabfil",                                                                                               
		  		"EXEC ".                                                                                              
				" ce_Insert_tbl_folders \@FolderName = '$filiale', \@Descr = '$descr', \@Inst = '$istituto', \@Parent = '$supc' ,\@Banca = '$banca' "                                                                                                                                          
			);
			print " ce_Insert_tbl_folders \@FolderName = '$filiale', \@Descr = '$descr', \@Inst = '$istituto', \@Parent = '$supc' ,\@Banca = '$banca' \n";
		} 
	}
};

if($@){
 open(OUT, ">$fileNameLogError"); binmode(OUT);
 print OUT "$@";
 close(OUT);
} 

for (@ARGV) {
  s/^ +//;s/ +$//;
};


__END__	
