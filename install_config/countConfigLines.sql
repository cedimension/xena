use ctdxreport;
go
select 'tbl_JobReportNames', COUNT(*) from tbl_JobReportNames
union all
select 'tbl_ReportNames', COUNT(*) from tbl_ReportNames
union all
select 'tbl_Os390PrintParameters', COUNT(*) from tbl_Os390PrintParameters
union all
select 'tbl_Folders', COUNT(*) from tbl_Folders
union all
select 'tbl_Profiles', COUNT(*) from tbl_Profiles
union all
select 'tbl_ProfilesFoldersTrees', COUNT(*) from tbl_ProfilesFoldersTrees
union all
select 'tbl_FoldersRules', COUNT(*) from tbl_FoldersRules
union all
select 'tbl_NamedReportsGroups', COUNT(*) from tbl_NamedReportsGroups
union all
select 'tbl_testNRG', COUNT(*) from tbl_testNRG
union all
select 'tbl_VarSetsValues', COUNT(*) from tbl_VarSetsValues
union all
select 'tbl_testVSV', COUNT(*) from tbl_testVSV
union all
select 'tbl_Users (C0)', COUNT(*) from tbl_Users where UserName like 'c0%'
union all
select 'tbl_Users (others)', COUNT(*) from tbl_Users where not UserName like 'c0%'
union all
select 'tbl_UserAliases', COUNT(*) from tbl_UserAliases
union all
select 'tbl_UsersProfiles', COUNT(*) from tbl_UsersProfiles
union all
select 'tbl_BundlesNames', COUNT(*) from tbl_BundlesNames
union all
select 'tbl_LogicalReportsTexts', COUNT(*) from tbl_LogicalReportsTexts

