USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_Bundles]    Script Date: 06/12/2013 12:12:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Bundles]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Bundles]
GO

USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_Bundles]    Script Date: 06/12/2013 12:12:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_Bundles](
    [BundleId] [int] IDENTITY(1,1) NOT NULL,
    [BundleName] [varchar](50) NOT NULL,
    [BundleCategory] [varchar](50) NOT NULL,
    [BundleSkel] [varchar](50) NOT NULL,
    [BundleDest] [varchar](1024) NULL,
    [ElabStartTime] [datetime] NULL,
    [ElabEndTime] [datetime] NULL,
    [InputLines] [bigint] NULL,
    [InputPages] [int] NULL,
    [ElabLines] [bigint] NULL,
    [ElabPages] [int] NULL,
    [Destination] [varchar](max) NULL,
    [XferStartTime] [datetime] NULL,
    [XferEndTime] [datetime] NULL,
    [XferOutBytes] [bigint] NULL,
    [Status] [int] NULL,
    [ElabParameters] [varchar](max) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

