#!perl -w

use strict qw(vars);

use Config;
use File::Copy;
use File::Spec::Functions qw(:ALL);
use Data::Dumper;

use CPAN;

# Set up the directories to store the module packages
CPAN::HandleConfig->load();
#print Dumper($CPAN::Config);

$CPAN::Config->{urllist} = [ 'file://'.$ARGV[0] ];
#print Dumper($CPAN::Config);

CPAN::Shell::setup_output();
while(<DATA>) {
  chomp;
  my ($module, $force) = split /\s/, $_;
  warn "Processing $module\n";
  my $rc = $force ? CPAN::Shell->force('install', $module) 
                  : CPAN::Shell->install($module);
}
close(DATA);

#JPIERCE/Text-FIGlet-2.19.3.tgz
#Win32::Lanman
#Win32::ODBC
#XML::DOM::NamedNodeMap
#XML::DOM::PerlSAX
#XML::DOM::NodeList
#XML::DOM::DOMException
#MIKER/Crypt-CapnMidNite-1.00.tar.gz

__DATA__
Data::Alias
Data::Serializer
Math::Int64
XML::SAX::Expat
XML::Simple
Carp::Clan
Carp::Assert
Crypt::RC4
Spreadsheet::WriteExcel
Spreadsheet::ParseExcel
Convert::EBCDIC
Convert::IBM390
Convert::Binary::C
Convert::Binary::C::Cached
Net::LPR
Crypt::CBC
Crypt::DES
XML::ESISParser
XML::RegExp
XML::DOM
XML::Perl2SAX
XML::SAX2Perl
XML::Writer
XML::Twig
KMACLEOD/libxml-perl-0.08.tar.gz
XML::PatAct::MatchName
XML::PatAct::Amsterdam
XML::PatAct::ToObjects
XML::Handler::CanonXMLWriter
XML::Handler::XMLWriter
XML::Handler::Subs
XML::Handler::Sample
XML::Handler::BuildDOM
OLE::Storage_Lite
MIME::Explode
IO::WrapTie
IO::Stringy
SQL::Translator
Archive::Tar::Stream
Compress::Zlib
Compress::LZW
Date::Calc
Win32::Daemon
Win32::TieRegistry
Win32::Job
Win32::Console
Text::Reform
JPIERCE/Text-FIGlet-2.19.3.tgz  1
Font::TTF
