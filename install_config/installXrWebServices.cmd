@SETLOCAL
@set XREPORT_HOME=E:\unicredit\xena\xreport_home
@set XREPORT_SITECONF=E:\unicredit\xena\xreport_siteconf
@set XRWEBAPP_LOC=E:\unicredit\xena\xrUI
@set XREPORT_USER=.\xreport
@set XREPORT_PSWD=euriskom.1
@SET XRIDPFX=xena
@set APPCMD=@%systemroot%\system32\inetsrv\appcmd.exe
@REM set APPCMD=@echo

@IF [%1]==[] (
@set runtype=config
) ELSE (
@set sccmd=%1
)

@IF [%sccmd%]==[create] (
@set IISCMD=ADD
) ELSE (
@set IISCMD=CONFIG
)
 
@call :%IISCMD%POOL . . . %XREPORT_USER% %XREPORT_PSWD% %XRIDPFX%Pool 
@call :%IISCMD%SITE 777801 %XRIDPFX%WebSvcs 50080 %XREPORT_USER% %XREPORT_PSWD% %XRIDPFX%Pool %XREPORT_HOME%\xrsitiweb\WebServices
@call :%IISCMD%SITE 777802 %XRIDPFX%MgrSvcs 58080 %XREPORT_USER% %XREPORT_PSWD% %XRIDPFX%Pool %XREPORT_HOME%\xrsitiweb\IISWebServ
@call :%IISCMD%SITE 777901 %XRIDPFX%Manager 9090 . . %XRIDPFX%Pool "%XRWEBAPP_LOC%\manager"
@call :%IISCMD%SITE 777001 %XRIDPFX%Portal 9080 %XREPORT_USER% %XREPORT_PSWD% %XRIDPFX%Pool "%XREPORT_SITECONF%\webroot" . noanonymous nonegotiate 
@call :%IISCMD%VDIR 777001 %XRIDPFX%Portal %XRIDPFX%Pool %XRIDPFX%Documents "%XREPORT_HOME%\xrsitiweb\CeUserWeb\wwwroot" %XREPORT_USER% %XREPORT_PSWD% frameUsers.asp
@call :%IISCMD%VDIR 777001 %XRIDPFX%Portal %XRIDPFX%Pool %XRIDPFX%WebGui "%XRWEBAPP_LOC%\WebGui" . . index.html

@GOTO :EOF

:ADDVDIR

@echo Adding Vdir %4 for site %2(%1) path: %5 user: %6 pwd: %7 pool: %3
@SET SITEID=%1
@SHIFT
@IF [%3]==[.] (
%APPCMD% ADD APP /site.name:"%1" /path:/
%APPCMD% ADD VDIR /app.name:"%1/" /path:/
) ELSE (
%APPCMD% ADD APP /site.name:"%1" /path:/%3
%APPCMD% ADD VDIR /app.name:"%1/%3" /path:/
)
@call :CONFIGVDIR %SITEID% %1 %2 %3 %4 %5 %6 %7 %8 %9
@GOTO :EOF

:CONFIGVDIR
@SETLOCAL
@SET SITEID=%1
@SHIFT
@IF [%3]==[.] (
@SET XRVDIR=%1/
@SET XRVAPP=%1/
) ELSE (
@SET XRVDIR=%1/%3/
@SET XRVAPP=%1/%3
)
@echo config Vdir %XRVDIR% for site %1(%SITEID%) path: %4 user: %5 pwd: %6 pool: %2
%APPCMD% SET APP  /app.name:%XRVAPP% /applicationPool:%2
@IF [%5]==[.] (
%APPCMD% SET VDIR /vdir.name:%XRVDIR% /physicalPath:%4
) ELSE (
%APPCMD% SET VDIR /vdir.name:%XRVDIR% /physicalPath:%4 /userName:%5 /password:%6 
)
@IF [%7]==[] (
@SET DDOC=.
) ELSE (
@SET DDOC=%7
)
@IF [%DDOC%]==[.] (
@REM no default document - leave things as is
) ELSE (
%APPCMD% SET CONFIG "%XRVAPP%" /section:defaultDocument "/-files.[@start,value='Default.htm']" /commit:"%XRVAPP%"
%APPCMD% SET CONFIG "%XRVAPP%" /section:defaultDocument "/-files.[@start,value='Default.asp']" /commit:"%XRVAPP%"
%APPCMD% SET CONFIG "%XRVAPP%" /section:defaultDocument "/-files.[@start,value='index.htm']" /commit:"%XRVAPP%"
%APPCMD% SET CONFIG "%XRVAPP%" /section:defaultDocument "/-files.[@start,value='index.html']" /commit:"%XRVAPP%"
%APPCMD% SET CONFIG "%XRVAPP%" /section:defaultDocument "/-files.[@start,value='iisstart.htm']" /commit:"%XRVAPP%"
%APPCMD% SET CONFIG "%XRVAPP%" /section:defaultDocument "/-files.[@start,value='default.aspx']" /commit:"%XRVAPP%"
%APPCMD% SET CONFIG "%XRVAPP%" /section:defaultDocument "/+files.[@start,value='%7']" /commit:"%XRVAPP%"
)
@ENDLOCAL
@GOTO :EOF

:ADDSITE
@echo -----------------------------
@echo Adding site %2(%1)
%APPCMD% DELETE SITE  "%2"
%APPCMD% ADD SITE /name:"%2" /id:%1	
@SET SITEID=%1
@SHIFT
@call :ADDVDIR %SITEID% %1 %5 . %6 %3 %4 %7 %8 %9
@call :CONFIGSITE NOVDIR %1 %2 %3 %4 %5 %6 %7 %8 %9

@GOTO :EOF 

:CONFIGSITE
@echo config site %2(%1) path: %4 user: %5 pwd: %6 pool: %2
%APPCMD% SET SITE "%2" /bindings:http/*:%3: 
@SET SITEID=%1
@SHIFT
@IF [%SITEID%]==[NOVDIR] (
@REM VDIR will be done later - dont do now
) ELSE (
@call :CONFIGVDIR %SITEID% %1 %5 . %6 %3 %4 %7 %8 %9
)
@IF [%8]==[noanonymous] (
%APPCMD% UNLOCK CONFIG "%1" /section:system.webServer/security/authentication/anonymousAuthentication /commit:apphost
%APPCMD% SET CONFIG "%1" /section:anonymousAuthentication /enabled:false /commit:"%1"
)
@IF [%9]==[nonegotiate] (
%APPCMD% UNLOCK CONFIG "%1" /section:system.webServer/security/authentication/windowsAuthentication /commit:apphost
%APPCMD% SET CONFIG "%1" /section:windowsAuthentication /-providers.[value='Negotiate'] /commit:"%1"
)

@GOTO :EOF

:ADDPOOL
%APPCMD% DELETE APPPOOL %6 
%APPCMD% ADD APPPOOL /name:%6  /enable32BitAppOnWin64:true /processModel.IdentityType:SpecificUser
@call :CONFIGPOOL %1 %2 %3 %4 %5 %6

@GOTO :EOF

:CONFIGPOOL
%APPCMD% SET APPPOOL %6 /enable32BitAppOnWin64:true /processModel.IdentityType:SpecificUser /processModel.userName:%4 /processModel.password:%5 /recycling.periodicRestart.time:"04:00:00" /managedRuntimeVersion:v2.0 /managedPipelineMode:Classic

@GOTO :EOF
