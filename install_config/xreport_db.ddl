USE [ctdxreport]
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_WorkQueue_InsertTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_WorkQueue]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_WorkQueue_InsertTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_WorkQueue] DROP CONSTRAINT [DF_tbl_WorkQueue_InsertTime]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Users_UserDescr]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Users]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Users_UserDescr]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Users] DROP CONSTRAINT [DF_tbl_Users_UserDescr]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_UniversalPoolIds_LocalPathId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_UniversalPoolIds]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_UniversalPoolIds_LocalPathId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_UniversalPoolIds] DROP CONSTRAINT [DF_tbl_UniversalPoolIds_LocalPathId]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_UniversalObjectIds_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_UniversalObjectIds]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_UniversalObjectIds_Status]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_UniversalObjectIds] DROP CONSTRAINT [DF_tbl_UniversalObjectIds_Status]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_UniversalObjectIds_XferStartTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_UniversalObjectIds]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_UniversalObjectIds_XferStartTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_UniversalObjectIds] DROP CONSTRAINT [DF_tbl_UniversalObjectIds_XferStartTime]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_UniversalObjectIds_ObjectType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_UniversalObjectIds]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_UniversalObjectIds_ObjectType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_UniversalObjectIds] DROP CONSTRAINT [DF_tbl_UniversalObjectIds_ObjectType]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_UniversalObjectIds_ObjectRef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_UniversalObjectIds]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_UniversalObjectIds_ObjectRef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_UniversalObjectIds] DROP CONSTRAINT [DF_tbl_UniversalObjectIds_ObjectRef]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_TarFiles_TarLogicalPathId_IN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_TarFiles]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_TarFiles_TarLogicalPathId_IN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_TarFiles] DROP CONSTRAINT [DF_tbl_TarFiles_TarLogicalPathId_IN]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_TarFiles_TarLogicalPathId_OUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_TarFiles]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_TarFiles_TarLogicalPathId_OUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_TarFiles] DROP CONSTRAINT [DF_tbl_TarFiles_TarLogicalPathId_OUT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_TarFiles_CreationTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_TarFiles]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_TarFiles_CreationTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_TarFiles] DROP CONSTRAINT [DF_tbl_TarFiles_CreationTime]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_TablesModifyTime_ModifyTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_TablesModifyTime]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_TablesModifyTime_ModifyTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_TablesModifyTime] DROP CONSTRAINT [DF_tbl_TablesModifyTime_ModifyTime]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SpecialPermissions_Enabled]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_SpecialPermissions]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SpecialPermissions_Enabled]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_SpecialPermissions] DROP CONSTRAINT [DF_tbl_SpecialPermissions_Enabled]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Resources_ResGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Resources]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Resources_ResGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Resources] DROP CONSTRAINT [DF_tbl_Resources_ResGroup]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportsGroups_ReportRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportsGroups]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportsGroups_ReportRule]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportsGroups] DROP CONSTRAINT [DF_tbl_ReportsGroups_ReportRule]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportsGroups_FilterVar]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportsGroups]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportsGroups_FilterVar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportsGroups] DROP CONSTRAINT [DF_tbl_ReportsGroups_FilterVar]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportsGroups_FilterRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportsGroups]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportsGroups_FilterRule]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportsGroups] DROP CONSTRAINT [DF_tbl_ReportsGroups_FilterRule]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportsGroups_RecipientRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportsGroups]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportsGroups_RecipientRule]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportsGroups] DROP CONSTRAINT [DF_tbl_ReportsGroups_RecipientRule]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_FilterName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_FilterName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] DROP CONSTRAINT [DF_tbl_SubReportNames_FilterName]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportNames_HasIndexes]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportNames_HasIndexes]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] DROP CONSTRAINT [DF_tbl_ReportNames_HasIndexes]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_ActiveGens]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_ActiveGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] DROP CONSTRAINT [DF_tbl_SubReportNames_ActiveGens]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_ActiveDays]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_ActiveDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] DROP CONSTRAINT [DF_tbl_SubReportNames_ActiveDays]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_HoldGens]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_HoldGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] DROP CONSTRAINT [DF_tbl_SubReportNames_HoldGens]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_HoldDays]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_HoldDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] DROP CONSTRAINT [DF_tbl_SubReportNames_HoldDays]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_MailTo]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_MailTo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] DROP CONSTRAINT [DF_tbl_SubReportNames_MailTo]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportNames_XrefData]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportNames_XrefData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] DROP CONSTRAINT [DF_tbl_ReportNames_XrefData]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportNames_TotReports]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportNames_TotReports]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] DROP CONSTRAINT [DF_tbl_ReportNames_TotReports]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportNames_TotPages]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportNames_TotPages]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] DROP CONSTRAINT [DF_tbl_ReportNames_TotPages]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_ModifyKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_ModifyKey]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] DROP CONSTRAINT [DF_tbl_SubReportNames_ModifyKey]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportNames_CheckFlag]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportNames_CheckFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] DROP CONSTRAINT [DF_tbl_ReportNames_CheckFlag]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesReportNames_LL1_FilterValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames_LL1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesReportNames_LL1_FilterValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesReportNames_LL1] DROP CONSTRAINT [DF_tbl_ProfilesReportNames_LL1_FilterValue]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesReportNames_LL1_RecipientName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames_LL1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesReportNames_LL1_RecipientName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesReportNames_LL1] DROP CONSTRAINT [DF_tbl_ProfilesReportNames_LL1_RecipientName]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesReportNames_ReportRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesReportNames_ReportRule]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesReportNames] DROP CONSTRAINT [DF_tbl_ProfilesReportNames_ReportRule]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesReportNames_FilterVar]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesReportNames_FilterVar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesReportNames] DROP CONSTRAINT [DF_tbl_ProfilesReportNames_FilterVar]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesSubReportNames_FilterValues]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesSubReportNames_FilterValues]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesReportNames] DROP CONSTRAINT [DF_tbl_ProfilesSubReportNames_FilterValues]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesReportNames_XferRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesReportNames_XferRecipient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesReportNames] DROP CONSTRAINT [DF_tbl_ProfilesReportNames_XferRecipient]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesIndexTables_LL1_Filtervalue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesIndexTables_LL1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesIndexTables_LL1_Filtervalue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesIndexTables_LL1] DROP CONSTRAINT [DF_tbl_ProfilesIndexTables_LL1_Filtervalue]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesIndexTables_LL1_RecipientName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesIndexTables_LL1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesIndexTables_LL1_RecipientName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesIndexTables_LL1] DROP CONSTRAINT [DF_tbl_ProfilesIndexTables_LL1_RecipientName]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Profiles_ProfileDescr]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Profiles]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Profiles_ProfileDescr]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Profiles] DROP CONSTRAINT [DF_tbl_Profiles_ProfileDescr]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Profiles_LookAtEverything]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Profiles]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Profiles_LookAtEverything]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Profiles] DROP CONSTRAINT [DF_tbl_Profiles_LookAtEverything]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_PrintersAliases_TypeOfWork]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_PrintersAliases]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_PrintersAliases_TypeOfWork]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_PrintersAliases] DROP CONSTRAINT [DF_tbl_PrintersAliases_TypeOfWork]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_PrintersAliases_Priority]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_PrintersAliases]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_PrintersAliases_Priority]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_PrintersAliases] DROP CONSTRAINT [DF_tbl_PrintersAliases_Priority]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_PageSize]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_PageSize]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] DROP CONSTRAINT [DF_tbl_Os390PrintParameters_PageSize]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_PageOrient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_PageOrient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] DROP CONSTRAINT [DF_tbl_Os390PrintParameters_PageOrient]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_Chars]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_Chars]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] DROP CONSTRAINT [DF_tbl_Os390PrintParameters_Chars]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_cc]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_cc]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] DROP CONSTRAINT [DF_tbl_Os390PrintParameters_cc]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_Fcb]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_Fcb]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] DROP CONSTRAINT [DF_tbl_Os390PrintParameters_Fcb]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_PrintParameters_Trc]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_PrintParameters_Trc]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] DROP CONSTRAINT [DF_tbl_PrintParameters_Trc]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_Formatted]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_Formatted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] DROP CONSTRAINT [DF_tbl_Os390PrintParameters_Formatted]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_t1Fonts]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_t1Fonts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] DROP CONSTRAINT [DF_tbl_Os390PrintParameters_t1Fonts]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_LaserAdjust]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_LaserAdjust]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] DROP CONSTRAINT [DF_tbl_Os390PrintParameters_LaserAdjust]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_FormDef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_FormDef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] DROP CONSTRAINT [DF_tbl_Os390PrintParameters_FormDef]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_PageDef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_PageDef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] DROP CONSTRAINT [DF_tbl_Os390PrintParameters_PageDef]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_ReplaceDef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_ReplaceDef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] DROP CONSTRAINT [DF_tbl_Os390PrintParameters_ReplaceDef]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_PrintControlFile]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_PrintControlFile]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] DROP CONSTRAINT [DF_tbl_Os390PrintParameters_PrintControlFile]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ObjPendingBackups_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ObjPendingBackups]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ObjPendingBackups_Status]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ObjPendingBackups] DROP CONSTRAINT [DF_tbl_ObjPendingBackups_Status]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_NamedReportsGroups_RecipientRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_NamedReportsGroups]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_NamedReportsGroups_RecipientRule]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_NamedReportsGroups] DROP CONSTRAINT [DF_tbl_NamedReportsGroups_RecipientRule]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_HIST_XferRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports_HIST]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_HIST_XferRecipient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports_HIST] DROP CONSTRAINT [DF_tbl_LogicalReports_HIST_XferRecipient]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_TotPages]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_TotPages]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports] DROP CONSTRAINT [DF_tbl_LogicalReports_TotPages]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_XferRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_XferRecipient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports] DROP CONSTRAINT [DF_tbl_LogicalReports_XferRecipient]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_CheckStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_CheckStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports] DROP CONSTRAINT [DF_tbl_LogicalReports_CheckStatus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_HasNotes]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_HasNotes]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports] DROP CONSTRAINT [DF_tbl_LogicalReports_HasNotes]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_LastUsedToView]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_LastUsedToView]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports] DROP CONSTRAINT [DF_tbl_LogicalReports_LastUsedToView]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_LastUsedToCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_LastUsedToCheck]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports] DROP CONSTRAINT [DF_tbl_LogicalReports_LastUsedToCheck]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobSpool_SpoolPathId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobSpool]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobSpool_SpoolPathId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobSpool] DROP CONSTRAINT [DF_tbl_JobSpool_SpoolPathId]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobSpool_ContentType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobSpool]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobSpool_ContentType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobSpool] DROP CONSTRAINT [DF_tbl_JobSpool_ContentType]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobSpool_PrintDuplex]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobSpool]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobSpool_PrintDuplex]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobSpool] DROP CONSTRAINT [DF_tbl_JobSpool_PrintDuplex]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobSpool_RetryCount]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobSpool]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobSpool_RetryCount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobSpool] DROP CONSTRAINT [DF_tbl_JobSpool_RetryCount]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsTarFiles_TarFileNumberIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsTarFiles]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsTarFiles_TarFileNumberIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsTarFiles] DROP CONSTRAINT [DF_tbl_JobReportsTarFiles_TarFileNumberIN]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsTarFiles_TarRecNumberIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsTarFiles]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsTarFiles_TarRecNumberIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsTarFiles] DROP CONSTRAINT [DF_tbl_JobReportsTarFiles_TarRecNumberIN]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsTarFiles_TarFileNumberOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsTarFiles]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsTarFiles_TarFileNumberOUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsTarFiles] DROP CONSTRAINT [DF_tbl_JobReportsTarFiles_TarFileNumberOUT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsTarFiles_TarRecNumberOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsTarFiles]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsTarFiles_TarRecNumberOUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsTarFiles] DROP CONSTRAINT [DF_tbl_JobReportsTarFiles_TarRecNumberOUT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsPendingXfers_TimeInsert]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsPendingXfers]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsPendingXfers_TimeInsert]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsPendingXfers] DROP CONSTRAINT [DF_tbl_JobReportsPendingXfers_TimeInsert]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsLocalFileCache_LocalPath]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsLocalFileCache]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsLocalFileCache_LocalPath]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsLocalFileCache] DROP CONSTRAINT [DF_tbl_JobReportsLocalFileCache_LocalPath]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsLocalFileCache_UsedTimes]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsLocalFileCache]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsLocalFileCache_UsedTimes]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsLocalFileCache] DROP CONSTRAINT [DF_tbl_JobReportsLocalFileCache_UsedTimes]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsLocalFileCache_CreateTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsLocalFileCache]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsLocalFileCache_CreateTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsLocalFileCache] DROP CONSTRAINT [DF_tbl_JobReportsLocalFileCache_CreateTime]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsLocalFileCache_LastUsedTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsLocalFileCache]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsLocalFileCache_LastUsedTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsLocalFileCache] DROP CONSTRAINT [DF_tbl_JobReportsLocalFileCache_LastUsedTime]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabData_ElabFormat]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabData_ElabFormat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabs] DROP CONSTRAINT [DF_tbl_JobReportsElabData_ElabFormat]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabData_FileRangesVar]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabData_FileRangesVar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabs] DROP CONSTRAINT [DF_tbl_JobReportsElabData_FileRangesVar]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabData_isAFP]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabData_isAFP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabs] DROP CONSTRAINT [DF_tbl_JobReportsElabData_isAFP]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabData_isFullAFP]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabData_isFullAFP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabs] DROP CONSTRAINT [DF_tbl_JobReportsElabData_isFullAFP]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabData_UserRef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabData_UserRef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabs] DROP CONSTRAINT [DF_tbl_JobReportsElabData_UserRef]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabData_FileRangeVar]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabData_FileRangeVar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabs] DROP CONSTRAINT [DF_tbl_JobReportsElabData_FileRangeVar]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_ElabFormat]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_ElabFormat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] DROP CONSTRAINT [DF_tbl_JobReportsElabOptions_ElabFormat]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_HasIndexes]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_HasIndexes]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] DROP CONSTRAINT [DF_tbl_JobReportsElabOptions_HasIndexes]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_isAFP]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_isAFP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] DROP CONSTRAINT [DF_tbl_JobReportsElabOptions_isAFP]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_isFullAFP]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_isFullAFP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] DROP CONSTRAINT [DF_tbl_JobReportsElabOptions_isFullAFP]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_FileRangeVar]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_FileRangeVar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] DROP CONSTRAINT [DF_tbl_JobReportsElabOptions_FileRangeVar]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_ExistTypeMap]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_ExistTypeMap]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] DROP CONSTRAINT [DF_tbl_JobReportsElabOptions_ExistTypeMap]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_MaybeTypeMap]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_MaybeTypeMap]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] DROP CONSTRAINT [DF_tbl_JobReportsElabOptions_MaybeTypeMap]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_LocalPathId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_LocalPathId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] DROP CONSTRAINT [DF_tbl_JobReports_HIST_LocalPathId]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_OrigJobReportName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_OrigJobReportName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] DROP CONSTRAINT [DF_tbl_JobReports_HIST_OrigJobReportName]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_TarFileNumberIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_TarFileNumberIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] DROP CONSTRAINT [DF_tbl_JobReports_HIST_TarFileNumberIN]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_TarRecNumberIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_TarRecNumberIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] DROP CONSTRAINT [DF_tbl_JobReports_HIST_TarRecNumberIN]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_TarFileNumberOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_TarFileNumberOUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] DROP CONSTRAINT [DF_tbl_JobReports_HIST_TarFileNumberOUT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_TarRecNumberOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_TarRecNumberOUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] DROP CONSTRAINT [DF_tbl_JobReports_HIST_TarRecNumberOUT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_ViewLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_ViewLevel]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] DROP CONSTRAINT [DF_tbl_JobReports_HIST_ViewLevel]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_MailingStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_MailingStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] DROP CONSTRAINT [DF_tbl_JobReports_HIST_MailingStatus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_PendingOp]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_PendingOp]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] DROP CONSTRAINT [DF_tbl_JobReports_HIST_PendingOp]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_UserRef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_UserRef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] DROP CONSTRAINT [DF_tbl_JobReports_HIST_UserRef]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_LocalPathId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_LocalPathId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_LocalPathId]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_LocalPathId_OUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_LocalPathId_OUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_LocalPathId_OUT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_OrigJobReportName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_OrigJobReportName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_OrigJobReportName]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_TarFileNumberIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_TarFileNumberIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_TarFileNumberIN]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_TarRecNumberIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_TarRecNumberIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_TarRecNumberIN]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_TarFileNumberOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_TarFileNumberOUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_TarFileNumberOUT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_TarRecNumberOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_TarRecNumberOUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_TarRecNumberOUT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HasIndexes]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HasIndexes]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_HasIndexes]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_isAFP]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_isAFP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_isAFP]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_isFullAFP]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_isFullAFP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_isFullAFP]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_ExistTypeMap]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_ExistTypeMap]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_ExistTypeMap]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_MayBeTypeMap]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_MayBeTypeMap]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_MayBeTypeMap]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_RangeVar]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_RangeVar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_RangeVar]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_ViewLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_ViewLevel]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_ViewLevel]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_MailedFlag]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_MailedFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_MailedFlag]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_PendingOp]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_PendingOp]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_PendingOp]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_UserReportRef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_UserReportRef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] DROP CONSTRAINT [DF_tbl_JobReports_UserReportRef]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNamesQre_JobName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNamesQre_JobName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNamesQrexp] DROP CONSTRAINT [DF_tbl_JobReportNamesQre_JobName]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNamesQre_Recipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNamesQre_Recipient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNamesQrexp] DROP CONSTRAINT [DF_tbl_JobReportNamesQre_Recipient]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNamesQre_RemoteHostAddr]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNamesQre_RemoteHostAddr]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNamesQrexp] DROP CONSTRAINT [DF_tbl_JobReportNamesQre_RemoteHostAddr]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNamesQre_RemoteFileName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNamesQre_RemoteFileName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNamesQrexp] DROP CONSTRAINT [DF_tbl_JobReportNamesQre_RemoteFileName]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNamesQre_PageText]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNamesQre_PageText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNamesQrexp] DROP CONSTRAINT [DF_tbl_JobReportNamesQre_PageText]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNamesQre_MaxPageTests]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNamesQre_MaxPageTests]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNamesQrexp] DROP CONSTRAINT [DF_tbl_JobReportNamesQre_MaxPageTests]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_ReportFormat]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ReportFormat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_ReportFormat]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_OutReportFormat]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_OutReportFormat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_OutReportFormat]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_IsActive]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_IsActive]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_PrintFlag]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_PrintFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_PrintFlag]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_ViewOnlineFlag]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ViewOnlineFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_ViewOnlineFlag]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_ActiveDays]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ActiveDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_ActiveDays]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_ActiveGens]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ActiveGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_ActiveGens]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_HoldDays]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_HoldDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_HoldDays]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_HoldGens]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_HoldGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_HoldGens]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_StorageClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_StorageClass]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_StorageClass]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_LinesPerPage1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_LinesPerPage1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_LinesPerPage1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_LinesPerPage]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_LinesPerPage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_LinesPerPage]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_PageOrient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_PageOrient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_PageOrient]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_HasCc]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_HasCc]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_HasCc]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_FontSize]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_FontSize]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_FontSize]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_FitToPage]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_FitToPage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_FitToPage]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_CodePage]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_CodePage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_CodePage]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_TotReports]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_TotReports]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_TotReports]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_TotPages]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_TotPages]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_TotPages]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_ClusterIndex]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ClusterIndex]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_ClusterIndex]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_ParseFileName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ParseFileName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_ParseFileName]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_LocalFileId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_LocalFileId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_LocalFileId]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_TargetLocalPathId_OUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_TargetLocalPathId_OUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_TargetLocalPathId_OUT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_WorkType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_WorkType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_WorkType]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_MailTo]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_MailTo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_MailTo]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_XrefData]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_XrefData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_XrefData]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_CreateId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_CreateId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] DROP CONSTRAINT [DF_tbl_JobReportNames_CreateId]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersRules_FORBID]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersRules]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersRules_FORBID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersRules] DROP CONSTRAINT [DF_tbl_FoldersRules_FORBID]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersSubReportNames_FilterValues]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_old]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersSubReportNames_FilterValues]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportNames_old] DROP CONSTRAINT [DF_tbl_FoldersSubReportNames_FilterValues]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersReportNames_XferRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_old]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersReportNames_XferRecipient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportNames_old] DROP CONSTRAINT [DF_tbl_FoldersReportNames_XferRecipient]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersReportNames_DENY_ACCESS]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_old]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersReportNames_DENY_ACCESS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportNames_old] DROP CONSTRAINT [DF_tbl_FoldersReportNames_DENY_ACCESS]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersReportNames_LL1_FilterValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_LL1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersReportNames_LL1_FilterValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportNames_LL1] DROP CONSTRAINT [DF_tbl_FoldersReportNames_LL1_FilterValue]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersReportNames_LL1_RecipientName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_LL1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersReportNames_LL1_RecipientName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportNames_LL1] DROP CONSTRAINT [DF_tbl_FoldersReportNames_LL1_RecipientName]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Folders_IsActive]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Folders]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Folders_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Folders] DROP CONSTRAINT [DF_tbl_Folders_IsActive]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FilterVarValues_FilterValueDescr]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FilterVarValues]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FilterVarValues_FilterValueDescr]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FilterVarValues] DROP CONSTRAINT [DF_tbl_FilterVarValues_FilterValueDescr]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EmailSentList_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EmailSentList]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EmailSentList_Status]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EmailSentList] DROP CONSTRAINT [DF_tbl_EmailSentList_Status]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EmailSentList_FromField]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EmailSentList]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EmailSentList_FromField]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EmailSentList] DROP CONSTRAINT [DF_tbl_EmailSentList_FromField]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EmailSentList_BodyField]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EmailSentList]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EmailSentList_BodyField]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EmailSentList] DROP CONSTRAINT [DF_tbl_EmailSentList_BodyField]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EMailQueue_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EMailPendingQueue]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EMailQueue_Status]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EMailPendingQueue] DROP CONSTRAINT [DF_tbl_EMailQueue_Status]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EMailPendingQueue_SendNumber]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EMailPendingQueue]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EMailPendingQueue_SendNumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EMailPendingQueue] DROP CONSTRAINT [DF_tbl_EMailPendingQueue_SendNumber]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EMailQueue_FromField]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EMailPendingQueue]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EMailQueue_FromField]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EMailPendingQueue] DROP CONSTRAINT [DF_tbl_EMailQueue_FromField]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EMailQueue_BodyField]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EMailPendingQueue]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EMailQueue_BodyField]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EMailPendingQueue] DROP CONSTRAINT [DF_tbl_EMailQueue_BodyField]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrHoldDays]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrHoldDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] DROP CONSTRAINT [DF_tbl_CTD_XrefData_XrHoldDays]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrHoldGens]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrHoldGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] DROP CONSTRAINT [DF_tbl_CTD_XrefData_XrHoldGens]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrKeepDays]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrKeepDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] DROP CONSTRAINT [DF_tbl_CTD_XrefData_XrKeepDays]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrKeepGens]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrKeepGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] DROP CONSTRAINT [DF_tbl_CTD_XrefData_XrKeepGens]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrProfileList]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrProfileList]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] DROP CONSTRAINT [DF_tbl_CTD_XrefData_XrProfileList]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrFolderList]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrFolderList]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] DROP CONSTRAINT [DF_tbl_CTD_XrefData_XrFolderList]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrMailToList]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrMailToList]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] DROP CONSTRAINT [DF_tbl_CTD_XrefData_XrMailToList]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrParseFile]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrParseFile]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] DROP CONSTRAINT [DF_tbl_CTD_XrefData_XrParseFile]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_ModifyKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_ModifyKey]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] DROP CONSTRAINT [DF_tbl_CTD_XrefData_ModifyKey]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ctd_ReportMappings_XrJobReportName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_ReportMappings]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ctd_ReportMappings_XrJobReportName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_ReportMappings] DROP CONSTRAINT [DF_tbl_ctd_ReportMappings_XrJobReportName]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ctd_ReportMappings_XrJobSuffix]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_ReportMappings]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ctd_ReportMappings_XrJobSuffix]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_ReportMappings] DROP CONSTRAINT [DF_tbl_ctd_ReportMappings_XrJobSuffix]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ctd_ReportMappings_XrParseFileName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_ReportMappings]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ctd_ReportMappings_XrParseFileName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_ReportMappings] DROP CONSTRAINT [DF_tbl_ctd_ReportMappings_XrParseFileName]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ctd_INBUNDLES_XrefData_JobReportid]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES_XrefData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ctd_INBUNDLES_XrefData_JobReportid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_INBUNDLES_XrefData] DROP CONSTRAINT [DF_tbl_ctd_INBUNDLES_XrefData_JobReportid]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ctd_BUNDLES_XrefData_ERROR_MESSAGE]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES_XrefData]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ctd_BUNDLES_XrefData_ERROR_MESSAGE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_INBUNDLES_XrefData] DROP CONSTRAINT [DF_tbl_ctd_BUNDLES_XrefData_ERROR_MESSAGE]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_IN_Bundles_DateTimeString]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_IN_Bundles_DateTimeString]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_INBUNDLES] DROP CONSTRAINT [DF_tbl_CTD_IN_Bundles_DateTimeString]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ctd_INBUNDLES_XferMode]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ctd_INBUNDLES_XferMode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_INBUNDLES] DROP CONSTRAINT [DF_tbl_ctd_INBUNDLES_XferMode]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_IN_Bundles_InsertTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_IN_Bundles_InsertTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_INBUNDLES] DROP CONSTRAINT [DF_tbl_CTD_IN_Bundles_InsertTime]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_AuditLog_OperationTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_AuditLog]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_AuditLog_OperationTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_AuditLog] DROP CONSTRAINT [DF_tbl_AuditLog_OperationTime]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersReportGroups_ReportGroupId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportGroups]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersReportGroups_ReportGroupId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportGroups] DROP CONSTRAINT [DF_tbl_FoldersReportGroups_ReportGroupId]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersReportGroups_FORBID]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportGroups]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersReportGroups_FORBID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportGroups] DROP CONSTRAINT [DF_tbl_FoldersReportGroups_FORBID]
END


End
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbl_FoldersReportGroups_tbl_ReportsGroups]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportGroups]'))
ALTER TABLE [dbo].[tbl_FoldersReportGroups] DROP CONSTRAINT [FK_tbl_FoldersReportGroups_tbl_ReportsGroups]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRExpFoldersReportNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRExpFoldersReportNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRExpFoldersReportNames2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRExpFoldersReportNames2]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_FolderChildFolders]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ce_FolderChildFolders]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_FolderJobReportsOfDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ce_FolderJobReportsOfDate]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_FolderReportNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ce_FolderReportNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_FolderReportNames_HIST]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ce_FolderReportNames_HIST]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_JobReportReports]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ce_JobReportReports]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_ReportNameJobReports]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ce_ReportNameJobReports]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ftb_ExpFoldersReportNames]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ftb_ExpFoldersReportNames]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNamesOLD]'))
DROP VIEW [dbo].[tbl_FoldersReportNamesOLD]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNamesXX]'))
DROP VIEW [dbo].[tbl_FoldersReportNamesXX]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ftb_FiltOfFilterVarValues]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ftb_FiltOfFilterVarValues]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ftb_FiltOfRecipientNames]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ftb_FiltOfRecipientNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ftb_FiltOfReportNames]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ftb_FiltOfReportNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_ReportNameJobReports_HIST]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ce_ReportNameJobReports_HIST]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_UserFoldersTrees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ce_UserFoldersTrees]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_VerifyReportNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ce_VerifyReportNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportGroups]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_FoldersReportGroups]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames]'))
DROP VIEW [dbo].[tbl_FoldersReportNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_VerifyJobReportNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_VerifyJobReportNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_VerifyReportNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_VerifyReportNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRFolderChildFolders]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRFolderChildFolders]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRFolderJobReportsOfDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRFolderJobReportsOfDate]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRFolderJobReportsOfDate_HIST]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRFolderJobReportsOfDate_HIST]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRFolderReportNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRFolderReportNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRFolderReportNames_HIST]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRFolderReportNames_HIST]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_JobReportNamesQrexp]'))
DROP VIEW [dbo].[vw_JobReportNamesQrexp]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_JobReports]'))
DROP VIEW [dbo].[vw_JobReports]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_LogicaReportsCounts]'))
DROP VIEW [dbo].[vw_LogicaReportsCounts]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_Reports]'))
DROP VIEW [dbo].[vw_Reports]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRGetReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRGetReport]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRJobReportFileRanges]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRJobReportFileRanges]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRJobReportReports]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRJobReportReports]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRReportNameJobReports]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRReportNameJobReports]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRReportNameJobReports_HIST]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRReportNameJobReports_HIST]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRUserFoldersTrees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRUserFoldersTrees]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRVerifyJobReportNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRVerifyJobReportNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRVerifyReportNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRVerifyReportNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRReleaseAppLock]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRReleaseAppLock]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FilterValueIsOK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[FilterValueIsOK]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RecipientNameIsOK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[RecipientNameIsOK]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportNameIsOK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ReportNameIsOK]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XferDateDiff]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[XferDateDiff]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[xreport_dbexport]') AND type in (N'U'))
DROP TABLE [dbo].[xreport_dbexport]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[xreport_dbexport1]') AND type in (N'U'))
DROP TABLE [dbo].[xreport_dbexport1]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRGetAppLock]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[XRGetAppLock]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_AuditLog]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_AuditLog]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Backups]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Backups]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_CenteraClips]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_CenteraClips]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ctd_INBUNDLES]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES_XrefData]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ctd_INBUNDLES_XrefData]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ctd_JobNameAlias]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ctd_JobNameAlias]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ctd_ReportMappings]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ctd_ReportMappings]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ctd_XrefReportData]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ElabFormatCodes]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ElabFormatCodes]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_EmailJobReportsStat]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_EmailJobReportsStat]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_EMailPendingQueue]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_EMailPendingQueue]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_EmailSentList]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_EmailSentList]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FilterVarValues]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_FilterVarValues]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Folders]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Folders]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_LL1]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_FoldersReportNames_LL1]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_LL2]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_FoldersReportNames_LL2]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_old]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_FoldersReportNames_old]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersRules]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_FoldersRules]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersTrees]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_FoldersTrees]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersTreesHierarchies]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_FoldersTreesHierarchies]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_IndexTables]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_IndexTables]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobCtlInput]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobCtlInput]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobElabs_ERROR_MESSAGES]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobElabs_ERROR_MESSAGES]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportFormatCodes]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportFormatCodes]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQaliases]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportNamesQaliases]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportNamesQrexp]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReports]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReports_HIST]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsCenteraClipIds]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsCenteraClipIds]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsDiscardedPages]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsDiscardedPages]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsElabOptions]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions_HIST]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsElabOptions_HIST]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsElabs]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsFileRanges]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsFileRanges]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsIndexTables]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsIndexTables]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsLocalFileCache]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsLocalFileCache]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsMixedReportIds]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsMixedReportIds]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsPendingXfers]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsPendingXfers]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsTarFiles]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsTarFiles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsUrls]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsUrls]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsXfers]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsXfers]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobSpool]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobSpool]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_LogicalReports]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports_HIST]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_LogicalReports_HIST]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_NamedReportsGroups]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_NamedReportsGroups]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_NetBackup]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_NetBackup]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_NetBackupAchivesTarFiles]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_NetBackupAchivesTarFiles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_NetBackupArchives]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_NetBackupArchives]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ObjBackups]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ObjBackups]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Objects]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Objects]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ObjPendingBackups]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ObjPendingBackups]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Os390PrintParameters]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Os390ResFiles]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Os390ResFiles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Os390Resources]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Os390Resources]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_PhysicalReports]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_PhysicalReports]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_PhysicalReports_HIST]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_PhysicalReports_HIST]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Printers]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Printers]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_PrintersAliases]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_PrintersAliases]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Profiles]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Profiles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesFoldersTrees]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ProfilesFoldersTrees]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesIndexTables_LL1]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ProfilesIndexTables_LL1]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ProfilesReportNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames_LL1]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ProfilesReportNames_LL1]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames_LL2]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ProfilesReportNames_LL2]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_RecipientNames]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_RecipientNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ReportNames]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ReportNamesFilterValues]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ReportNamesFilterValues]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ReportNamesIndexTables]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ReportNamesIndexTables]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ReportsArchErrors]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ReportsArchErrors]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ReportsGroups]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ReportsGroups]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Resources]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Resources]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_SpecialPermissions]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_SpecialPermissions]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_StatusCodes]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_StatusCodes]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_TablesModifyTime]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_TablesModifyTime]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_TarFiles]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_TarFiles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_TarFilesDir]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_TarFilesDir]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_TypeOfWorkCodes]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_TypeOfWorkCodes]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_UniversalObjectIds]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_UniversalObjectIds]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_UniversalPoolIds]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_UniversalPoolIds]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_UserAliases]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_UserAliases]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Users]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Users]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_UsersLocalSessionCookies]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_UsersLocalSessionCookies]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_UsersProfiles]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_UsersProfiles]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_VarSetsValues]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_VarSetsValues]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_web_activity_websrvc]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_web_activity_websrvc]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_web_activity_wwwroot]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_web_activity_wwwroot]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_WorkQueue]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_WorkQueue]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_XferModeCodes]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_XferModeCodes]
GO
USE [ctdxreport]
GO
CREATE USER [dbo] FOR LOGIN [xreport] WITH DEFAULT_SCHEMA=[dbo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_XferModeCodes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_XferModeCodes](
	[XferMode] [char](1) NOT NULL,
	[XferModeDescr] [varchar](16) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_XferModeCodes] ([XferMode], [XferModeDescr]) VALUES (N'1', N'LPR')
INSERT [dbo].[tbl_XferModeCodes] ([XferMode], [XferModeDescr]) VALUES (N'2', N'PSF')
INSERT [dbo].[tbl_XferModeCodes] ([XferMode], [XferModeDescr]) VALUES (N'3', N'FTPB')
INSERT [dbo].[tbl_XferModeCodes] ([XferMode], [XferModeDescr]) VALUES (N'4', N'FTPC')
INSERT [dbo].[tbl_XferModeCodes] ([XferMode], [XferModeDescr]) VALUES (N'5', N'ASPSF')
INSERT [dbo].[tbl_XferModeCodes] ([XferMode], [XferModeDescr]) VALUES (N'6', N'AFPSTREAM')
INSERT [dbo].[tbl_XferModeCodes] ([XferMode], [XferModeDescr]) VALUES (N'9', N'AS400Spool')
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_WorkQueue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_WorkQueue](
	[ExternalTableName] [varchar](64) NOT NULL,
	[ExternalKey] [int] NOT NULL,
	[TypeOfWork] [smallint] NOT NULL,
	[WorkId] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[InsertTime] [datetime] NULL,
	[WorkClass] [int] NULL,
	[Priority] [smallint] NULL,
	[Status] [smallint] NOT NULL,
	[SrvName] [varchar](32) NULL,
	[SrvParameters] [varchar](4096) NULL,
 CONSTRAINT [PK_tbl_WorkQueue] PRIMARY KEY CLUSTERED 
(
	[ExternalTableName] ASC,
	[ExternalKey] ASC,
	[TypeOfWork] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_web_activity_wwwroot]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_web_activity_wwwroot](
	[JobReportId] [int] NOT NULL,
	[applid] [varchar](50) NULL,
	[pooluser] [varchar](50) NULL,
	[computername] [varchar](50) NULL,
	[reqtime] [datetime] NULL,
	[requser] [varchar](50) NULL,
	[reqaddr] [varchar](50) NULL,
	[folder] [varchar](50) NULL,
	[reqJID] [varchar](50) NULL,
	[reportname] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_web_activity_websrvc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_web_activity_websrvc](
	[JobReportId] [int] NOT NULL,
	[applid] [varchar](50) NULL,
	[pooluser] [varchar](50) NULL,
	[COMPUTERNAME] [varchar](50) NULL,
	[reqstart] [datetime] NULL,
	[reqend] [datetime] NULL,
	[requser] [varchar](50) NULL,
	[reqitems] [smallint] NULL,
	[reqbytes] [varchar](50) NULL,
	[reqtype] [varchar](50) NULL,
	[reqaddr] [varchar](50) NULL,
	[cpuuser] [float] NULL,
	[cpusystem] [float] NULL,
	[reqindex] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_VarSetsValues]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_VarSetsValues](
	[VarSetName] [varchar](32) NOT NULL,
	[VarName] [varchar](32) NOT NULL,
	[VarValue] [varchar](32) NOT NULL,
 CONSTRAINT [PK_tbl_VarSetsValues] PRIMARY KEY CLUSTERED 
(
	[VarSetName] ASC,
	[VarName] ASC,
	[VarValue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_VarSetsValues] ([VarSetName], [VarName], [VarValue]) VALUES (N'TEST', N':REPORTNAME', N'LCC02190_01')
INSERT [dbo].[tbl_VarSetsValues] ([VarSetName], [VarName], [VarValue]) VALUES (N'TEST', N':REPORTNAME', N'LCC02193_01')
INSERT [dbo].[tbl_VarSetsValues] ([VarSetName], [VarName], [VarValue]) VALUES (N'TEST', N':REPORTNAME', N'LCC02193_02')
INSERT [dbo].[tbl_VarSetsValues] ([VarSetName], [VarName], [VarValue]) VALUES (N'TEST', N':REPORTNAME', N'LCC02284_01')
INSERT [dbo].[tbl_VarSetsValues] ([VarSetName], [VarName], [VarValue]) VALUES (N'TEST', N':REPORTNAME', N'LCC04096_01')
INSERT [dbo].[tbl_VarSetsValues] ([VarSetName], [VarName], [VarValue]) VALUES (N'TEST', N':REPORTNAME', N'LCC04096_02')
INSERT [dbo].[tbl_VarSetsValues] ([VarSetName], [VarName], [VarValue]) VALUES (N'TEST', N':REPORTNAME', N'LCC04096_03')
INSERT [dbo].[tbl_VarSetsValues] ([VarSetName], [VarName], [VarValue]) VALUES (N'TEST', N':REPORTNAME', N'LCC04096_04')
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_UsersProfiles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_UsersProfiles](
	[UserName] [varchar](32) NOT NULL,
	[ProfileName] [varchar](32) NOT NULL,
 CONSTRAINT [PK_tbl_UsersProfiles] PRIMARY KEY CLUSTERED 
(
	[UserName] ASC,
	[ProfileName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_UsersProfiles] ([UserName], [ProfileName]) VALUES (N'xreport', N'SUPER')
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_UsersLocalSessionCookies]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_UsersLocalSessionCookies](
	[CookieHexString] [char](32) NOT NULL,
	[CookieExpireTime] [datetime] NOT NULL,
	[UserName] [varchar](32) NOT NULL,
	[AUTH_USER] [varchar](64) NOT NULL,
	[REMOTE_ADDR] [varchar](64) NOT NULL,
 CONSTRAINT [PK_tbl_SessionCookies] PRIMARY KEY CLUSTERED 
(
	[CookieHexString] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_UsersLocalSessionCookies] ([CookieHexString], [CookieExpireTime], [UserName], [AUTH_USER], [REMOTE_ADDR]) VALUES (N'de218db5f6f96737e87c9efec6e7f970', CAST(0x0000A0D300FA5C30 AS DateTime), N'xreport', N'esp\xreport', N'10.254.189.84')
INSERT [dbo].[tbl_UsersLocalSessionCookies] ([CookieHexString], [CookieExpireTime], [UserName], [AUTH_USER], [REMOTE_ADDR]) VALUES (N'ffa98f74f2d9865adf5edd8e4b581fe3', CAST(0x0000A0D300F9B94C AS DateTime), N'xreport', N'ESP\xreport', N'10.248.162.76')
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Users](
	[UserName] [varchar](32) NOT NULL,
	[UserDescr] [varchar](64) NOT NULL,
	[EMailAddr] [varchar](64) NULL,
	[Password] [varchar](16) NULL,
 CONSTRAINT [PK_tbl_Users] PRIMARY KEY CLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_Users] ([UserName], [UserDescr], [EMailAddr], [Password]) VALUES (N'xreport', N'XReport Internal User', NULL, NULL)
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_UserAliases]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_UserAliases](
	[UserAlias] [varchar](32) NOT NULL,
	[UserName] [varchar](32) NOT NULL,
	[UseAlias] [bit] NULL,
	[UserAliasDescr] [varchar](64) NULL,
 CONSTRAINT [PK__tbl_User__1A4394F0C2CB8F63] PRIMARY KEY CLUSTERED 
(
	[UserAlias] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_UserAliases] ([UserAlias], [UserName], [UseAlias], [UserAliasDescr]) VALUES (N'xreport', N'xreport', NULL, N'XReport Internal User')
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_UniversalPoolIds]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_UniversalPoolIds](
	[PoolName] [varchar](32) NOT NULL,
	[PoolId] [int] NOT NULL,
	[LocalPathId] [varchar](16) NOT NULL,
	[MaxObjectId] [int] NOT NULL,
 CONSTRAINT [PK_tbl_UniversalPoolIds] PRIMARY KEY CLUSTERED 
(
	[PoolName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY],
 CONSTRAINT [IX_tbl_UniversalPoolIds_PoolId] UNIQUE NONCLUSTERED 
(
	[PoolId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_UniversalObjectIds]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_UniversalObjectIds](
	[PoolId] [int] NOT NULL,
	[ObjectId] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[InsertTime] [datetime] NOT NULL,
	[ObjectType] [varchar](64) NOT NULL,
	[ObjectRef] [varchar](255) NOT NULL,
	[ObjectURI] [varchar](255) NOT NULL,
	[ObjectLength] [int] NULL,
 CONSTRAINT [PK_tbl_UniversalObjectIds] PRIMARY KEY CLUSTERED 
(
	[ObjectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_TypeOfWorkCodes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_TypeOfWorkCodes](
	[TypeOfWork] [smallint] NOT NULL,
	[TOWsdesc] [char](16) NULL,
	[TOWDescr] [varchar](100) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_TypeOfWorkCodes] ([TypeOfWork], [TOWsdesc], [TOWDescr]) VALUES (1, N'Splitter        ', NULL)
INSERT [dbo].[tbl_TypeOfWorkCodes] ([TypeOfWork], [TOWsdesc], [TOWDescr]) VALUES (2, N'Null            ', NULL)
INSERT [dbo].[tbl_TypeOfWorkCodes] ([TypeOfWork], [TOWsdesc], [TOWDescr]) VALUES (90, N'MODCAConverter  ', NULL)
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_TarFilesDir]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_TarFilesDir](
	[TarStorageClass] [varchar](32) NOT NULL,
	[TarDateRef] [datetime] NOT NULL,
	[TarFileId] [int] NOT NULL,
 CONSTRAINT [PK_tbl_TarFilesDir] PRIMARY KEY CLUSTERED 
(
	[TarStorageClass] ASC,
	[TarDateRef] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_TarFiles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_TarFiles](
	[TarStorageClass] [varchar](32) NOT NULL,
	[TarDateRef] [smalldatetime] NOT NULL,
	[TarFileId] [int] IDENTITY(1,1) NOT NULL,
	[TarLocalPathId_IN] [varchar](32) NOT NULL,
	[TarLocalPathId_OUT] [varchar](32) NOT NULL,
	[TarLocalFileName] [varchar](255) NOT NULL,
	[CreationTime] [datetime] NOT NULL,
	[ModifyTime] [datetime] NULL,
	[BackupTime] [datetime] NULL,
	[BackupXrefId] [decimal](18, 0) NULL,
	[CheckedOffset] [decimal](18, 0) NULL,
	[BackedupOffset] [decimal](18, 0) NULL,
 CONSTRAINT [PK_tbl_TarFiles] PRIMARY KEY CLUSTERED 
(
	[TarFileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_TablesModifyTime]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_TablesModifyTime](
	[TableName] [varchar](64) NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_TablesModifyTime] PRIMARY KEY CLUSTERED 
(
	[TableName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_StatusCodes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_StatusCodes](
	[Status] [smallint] NOT NULL,
	[StatusID] [char](16) NULL,
	[StatusDescr] [varchar](100) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_StatusCodes] ([Status], [StatusID], [StatusDescr]) VALUES (0, N'Accepted        ', N'Accepted')
INSERT [dbo].[tbl_StatusCodes] ([Status], [StatusID], [StatusDescr]) VALUES (1, N'Receiving       ', N'Receiving')
INSERT [dbo].[tbl_StatusCodes] ([Status], [StatusID], [StatusDescr]) VALUES (2, N'Received        ', N'Receive process completed correctly')
INSERT [dbo].[tbl_StatusCodes] ([Status], [StatusID], [StatusDescr]) VALUES (15, N'RecvError       ', N'Receive Error')
INSERT [dbo].[tbl_StatusCodes] ([Status], [StatusID], [StatusDescr]) VALUES (16, N'Queued          ', N'Queued for processing')
INSERT [dbo].[tbl_StatusCodes] ([Status], [StatusID], [StatusDescr]) VALUES (17, N'Processing      ', N'Processing')
INSERT [dbo].[tbl_StatusCodes] ([Status], [StatusID], [StatusDescr]) VALUES (18, N'Completed       ', N'Processing completed correctly')
INSERT [dbo].[tbl_StatusCodes] ([Status], [StatusID], [StatusDescr]) VALUES (31, N'Failed          ', N'Processing failed')
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_SpecialPermissions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_SpecialPermissions](
	[SpecialAttribute] [varchar](32) NOT NULL,
	[ProfileName] [varchar](32) NOT NULL,
	[Enabled] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_SpecialPermissions] PRIMARY KEY CLUSTERED 
(
	[SpecialAttribute] ASC,
	[ProfileName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Resources]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Resources](
	[ResID] [int] IDENTITY(1,1) NOT NULL,
	[ResGroup] [varchar](50) NOT NULL,
	[ResClass] [varchar](50) NOT NULL,
	[ResName] [varchar](255) NOT NULL,
	[XferStartTime] [datetime] NOT NULL,
	[XferEndTime] [datetime] NULL,
	[MD5_hash] [char](16) NOT NULL,
	[ContentType] [varchar](255) NULL,
	[RemoteOrigin] [varchar](255) NULL,
	[ResFileData] [image] NULL,
 CONSTRAINT [PK_tbl_Resources] PRIMARY KEY CLUSTERED 
(
	[ResID] ASC,
	[ResGroup] ASC,
	[ResClass] ASC,
	[ResName] ASC,
	[XferStartTime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ReportsGroups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ReportsGroups](
	[ReportGroupId] [int] IDENTITY(1,1) NOT NULL,
	[ReportRule] [varchar](32) NOT NULL,
	[FilterVar] [varchar](32) NOT NULL,
	[FilterRule] [varchar](32) NOT NULL,
	[RecipientRule] [varchar](32) NOT NULL,
 CONSTRAINT [PK_tbl_ReportsGroups] PRIMARY KEY CLUSTERED 
(
	[ReportRule] ASC,
	[FilterVar] ASC,
	[FilterRule] ASC,
	[RecipientRule] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY],
 CONSTRAINT [IX_tbl_ReportsGroups] UNIQUE NONCLUSTERED 
(
	[ReportGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ReportsArchErrors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ReportsArchErrors](
	[ReportId] [numeric](18, 0) NOT NULL,
	[FileType] [smallint] NULL,
	[LocalFileName] [varchar](255) NOT NULL,
	[Status] [smallint] NULL,
	[NumeroTentativi] [smallint] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ReportNamesIndexTables]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ReportNamesIndexTables](
	[ReportName] [varchar](32) NOT NULL,
	[IndexName] [varchar](32) NOT NULL,
 CONSTRAINT [PK_tbl_JobReportnamesIndexTables] PRIMARY KEY CLUSTERED 
(
	[ReportName] ASC,
	[IndexName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ReportNamesFilterValues]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ReportNamesFilterValues](
	[ReportName] [varchar](32) NOT NULL,
	[FilterValue] [varchar](32) NOT NULL,
	[XferRecipient] [varchar](32) NOT NULL,
 CONSTRAINT [PK_tbl_ReportNamesFilterVarValues] PRIMARY KEY CLUSTERED 
(
	[ReportName] ASC,
	[FilterValue] ASC,
	[XferRecipient] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ReportNames](
	[ReportName] [varchar](32) NOT NULL,
	[ReportDescr] [varchar](64) NOT NULL,
	[FilterVar] [varchar](32) NOT NULL,
	[HasIndex] [varchar](32) NOT NULL,
	[ActiveGens] [int] NOT NULL,
	[ActiveDays] [int] NOT NULL,
	[HoldGens] [int] NOT NULL,
	[HoldDays] [int] NOT NULL,
	[MailTo] [varchar](64) NOT NULL,
	[XrefData] [varchar](64) NULL,
	[TotReports] [decimal](18, 0) NOT NULL,
	[TotPages] [decimal](18, 0) NOT NULL,
	[ModifyKey] [varchar](16) NOT NULL,
	[ModifyTime] [decimal](18, 0) NULL,
	[CheckFlag] [bit] NOT NULL,
	[CodePage] [varchar](64) NULL,
 CONSTRAINT [PK_tbl_ReportNames] PRIMARY KEY CLUSTERED 
(
	[ReportName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_ReportNames] ([ReportName], [ReportDescr], [FilterVar], [HasIndex], [ActiveGens], [ActiveDays], [HoldGens], [HoldDays], [MailTo], [XrefData], [TotReports], [TotPages], [ModifyKey], [ModifyTime], [CheckFlag], [CodePage]) VALUES (N'LCC02190_01', N'', N'', N'', 5, 5, 5, 30, N'30', N'', CAST(0 AS Decimal(18, 0)), CAST(14 AS Decimal(18, 0)), N'2063', NULL, 0, N'2012-08-29 15:17:56.400')
INSERT [dbo].[tbl_ReportNames] ([ReportName], [ReportDescr], [FilterVar], [HasIndex], [ActiveGens], [ActiveDays], [HoldGens], [HoldDays], [MailTo], [XrefData], [TotReports], [TotPages], [ModifyKey], [ModifyTime], [CheckFlag], [CodePage]) VALUES (N'LCC02193_01', N'', N'', N'', 5, 5, 5, 30, N'30', N'', CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'0', NULL, 0, N'2012-08-29 15:17:54.870')
INSERT [dbo].[tbl_ReportNames] ([ReportName], [ReportDescr], [FilterVar], [HasIndex], [ActiveGens], [ActiveDays], [HoldGens], [HoldDays], [MailTo], [XrefData], [TotReports], [TotPages], [ModifyKey], [ModifyTime], [CheckFlag], [CodePage]) VALUES (N'LCC02193_02', N'', N'', N'', 5, 5, 5, 30, N'30', N'', CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'0', NULL, 0, N'2012-08-29 15:17:55.060')
INSERT [dbo].[tbl_ReportNames] ([ReportName], [ReportDescr], [FilterVar], [HasIndex], [ActiveGens], [ActiveDays], [HoldGens], [HoldDays], [MailTo], [XrefData], [TotReports], [TotPages], [ModifyKey], [ModifyTime], [CheckFlag], [CodePage]) VALUES (N'LCC02284_01', N'', N'', N'', 5, 5, 5, 30, N'30', N'', CAST(0 AS Decimal(18, 0)), CAST(1 AS Decimal(18, 0)), N'11412', NULL, 0, N'2012-08-29 15:17:56.543')
INSERT [dbo].[tbl_ReportNames] ([ReportName], [ReportDescr], [FilterVar], [HasIndex], [ActiveGens], [ActiveDays], [HoldGens], [HoldDays], [MailTo], [XrefData], [TotReports], [TotPages], [ModifyKey], [ModifyTime], [CheckFlag], [CodePage]) VALUES (N'LCC04096_01', N'', N'', N'', 5, 5, 5, 30, N'30', N'', CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'0', NULL, 0, N'2012-08-29 15:17:55.213')
INSERT [dbo].[tbl_ReportNames] ([ReportName], [ReportDescr], [FilterVar], [HasIndex], [ActiveGens], [ActiveDays], [HoldGens], [HoldDays], [MailTo], [XrefData], [TotReports], [TotPages], [ModifyKey], [ModifyTime], [CheckFlag], [CodePage]) VALUES (N'LCC04096_02', N'', N'', N'', 5, 5, 5, 30, N'30', N'', CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'0', NULL, 0, N'2012-08-29 15:17:55.890')
INSERT [dbo].[tbl_ReportNames] ([ReportName], [ReportDescr], [FilterVar], [HasIndex], [ActiveGens], [ActiveDays], [HoldGens], [HoldDays], [MailTo], [XrefData], [TotReports], [TotPages], [ModifyKey], [ModifyTime], [CheckFlag], [CodePage]) VALUES (N'LCC04096_03', N'', N'', N'', 5, 5, 5, 30, N'30', N'', CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'0', NULL, 0, N'2012-08-29 15:17:56.057')
INSERT [dbo].[tbl_ReportNames] ([ReportName], [ReportDescr], [FilterVar], [HasIndex], [ActiveGens], [ActiveDays], [HoldGens], [HoldDays], [MailTo], [XrefData], [TotReports], [TotPages], [ModifyKey], [ModifyTime], [CheckFlag], [CodePage]) VALUES (N'LCC04096_04', N'', N'', N'', 5, 5, 5, 30, N'30', N'', CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), N'0', NULL, 0, N'2012-08-29 15:17:56.207')
INSERT [dbo].[tbl_ReportNames] ([ReportName], [ReportDescr], [FilterVar], [HasIndex], [ActiveGens], [ActiveDays], [HoldGens], [HoldDays], [MailTo], [XrefData], [TotReports], [TotPages], [ModifyKey], [ModifyTime], [CheckFlag], [CodePage]) VALUES (N'XRRENAME', N'generic report name', N'', N'', 5, 5, 5, 30, N'30', N'', CAST(0 AS Decimal(18, 0)), CAST(7 AS Decimal(18, 0)), N'180', NULL, 0, N'2012-08-29 11:21:53.890')
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_RecipientNames]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_RecipientNames](
	[RecipientName] [varchar](32) NOT NULL,
	[RecipientDescr] [varchar](64) NOT NULL,
 CONSTRAINT [PK_tbl_RecipientNames] PRIMARY KEY CLUSTERED 
(
	[RecipientName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames_LL2]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ProfilesReportNames_LL2](
	[ProfileName] [varchar](32) NOT NULL,
	[ReportName] [varchar](32) NOT NULL,
	[FilterValue] [varchar](32) NOT NULL,
	[RecipientName] [varchar](32) NOT NULL,
	[FORBID] [bit] NULL,
	[RuleId] [int] NOT NULL,
 CONSTRAINT [PK_tbl_ProfilesReportNames_LL2] PRIMARY KEY CLUSTERED 
(
	[ProfileName] ASC,
	[ReportName] ASC,
	[FilterValue] ASC,
	[RecipientName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames_LL1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ProfilesReportNames_LL1](
	[ProfileName] [varchar](32) NOT NULL,
	[ReportName] [varchar](32) NOT NULL,
	[FilterValue] [varchar](32) NOT NULL,
	[RecipientName] [varchar](32) NOT NULL,
 CONSTRAINT [PK_tbl_ProfilesReportNames_LL1] PRIMARY KEY CLUSTERED 
(
	[ProfileName] ASC,
	[ReportName] ASC,
	[FilterValue] ASC,
	[RecipientName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ProfilesReportNames](
	[ProfileName] [varchar](32) NOT NULL,
	[ReportRule] [varchar](32) NOT NULL,
	[FilterVar] [varchar](32) NOT NULL,
	[FilterRule] [varchar](32) NOT NULL,
	[RecipientRule] [varchar](32) NOT NULL,
	[RuleId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_tbl_ProfilesReportNames] PRIMARY KEY CLUSTERED 
(
	[ProfileName] ASC,
	[ReportRule] ASC,
	[FilterVar] ASC,
	[FilterRule] ASC,
	[RecipientRule] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesIndexTables_LL1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ProfilesIndexTables_LL1](
	[ProfileName] [char](10) NOT NULL,
	[IndexTableName] [char](10) NOT NULL,
	[Filtervalue] [char](10) NOT NULL,
	[RecipientName] [char](10) NOT NULL,
 CONSTRAINT [PK_tbl_ProfilesIndexTables_LL1] PRIMARY KEY CLUSTERED 
(
	[ProfileName] ASC,
	[IndexTableName] ASC,
	[Filtervalue] ASC,
	[RecipientName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesFoldersTrees]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ProfilesFoldersTrees](
	[ProfileName] [varchar](32) NOT NULL,
	[RootNode] [varchar](256) NOT NULL,
 CONSTRAINT [PK_tbl_ProfilesFolders] PRIMARY KEY CLUSTERED 
(
	[ProfileName] ASC,
	[RootNode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_ProfilesFoldersTrees] ([ProfileName], [RootNode]) VALUES (N'SUPER', N'GESTSIST')
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Profiles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Profiles](
	[ProfileName] [varchar](32) NOT NULL,
	[ProfileDescr] [varchar](64) NOT NULL,
	[LookAtEverything] [bit] NULL,
 CONSTRAINT [PK_tbl_Profiles] PRIMARY KEY CLUSTERED 
(
	[ProfileName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_Profiles] ([ProfileName], [ProfileDescr], [LookAtEverything]) VALUES (N'SUPER', N'XReport Superuser Profile', 1)
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_PrintersAliases]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_PrintersAliases](
	[PrintDest] [varchar](255) NOT NULL,
	[PrtName] [varchar](255) NOT NULL,
	[PrtSrvAddress] [varchar](255) NOT NULL,
	[TypeOfWork] [smallint] NOT NULL,
	[WorkClass] [int] NULL,
	[Priority] [smallint] NOT NULL,
 CONSTRAINT [PK_tbl_PrtAliases] PRIMARY KEY CLUSTERED 
(
	[PrintDest] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Printers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Printers](
	[PrtName] [varchar](200) NOT NULL,
	[PrtSrvAddress] [varchar](200) NOT NULL,
	[Caption] [varchar](200) NULL,
	[Comment] [varchar](200) NULL,
	[Description] [varchar](200) NULL,
	[DeviceID] [varchar](200) NULL,
	[DriverName] [varchar](200) NULL,
	[ExtendedPrinterStatus] [varchar](200) NULL,
	[HorizontalResolution] [varchar](200) NULL,
	[InstallDate] [varchar](200) NULL,
	[LanguagesSupported] [varchar](200) NULL,
	[Location] [varchar](200) NULL,
	[MaxSizeSupported] [varchar](200) NULL,
	[PaperSizesSupported] [varchar](200) NULL,
	[PortName] [varchar](200) NULL,
	[PrinterState] [varchar](200) NULL,
	[PrinterStatus] [varchar](200) NULL,
	[Queued] [varchar](200) NULL,
	[ServerName] [varchar](200) NULL,
	[Status] [varchar](200) NULL,
	[StatusInfo] [varchar](200) NULL,
	[SystemName] [varchar](200) NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[PrtName] ASC,
	[PrtSrvAddress] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_PhysicalReports_HIST]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_PhysicalReports_HIST](
	[JobReportId] [int] NOT NULL,
	[ReportId] [smallint] NOT NULL,
	[TotPages] [numeric](18, 0) NOT NULL,
	[ListOfPages] [text] NOT NULL,
 CONSTRAINT [PK_tbl_PhysicalReports_HIST] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC,
	[ReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_PhysicalReports]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_PhysicalReports](
	[JobReportId] [int] NOT NULL,
	[ReportId] [smallint] NOT NULL,
	[TotPages] [numeric](18, 0) NOT NULL,
	[ListOfPages] [text] NOT NULL,
 CONSTRAINT [PK_tbl_PhysicalReports] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC,
	[ReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Os390Resources]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Os390Resources](
	[AfpSubSysId] [tinyint] NOT NULL,
	[ResName] [varchar](17) NOT NULL,
	[MD5_Hash] [char](16) NOT NULL,
	[FileId] [int] NOT NULL,
	[FileOffset] [int] NOT NULL,
 CONSTRAINT [PK_tblOs390Resources2] PRIMARY KEY CLUSTERED 
(
	[AfpSubSysId] ASC,
	[ResName] ASC,
	[MD5_Hash] ASC,
	[FileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Os390ResFiles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Os390ResFiles](
	[FileId] [decimal](18, 0) NOT NULL,
	[UpdMode] [tinyint] NULL,
	[TimeRef] [datetime] NULL,
	[IBMZLIB] [varchar](255) NULL,
 CONSTRAINT [PK_tbl_Os390ResFiles] PRIMARY KEY CLUSTERED 
(
	[FileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Os390PrintParameters](
	[JobReportName] [varchar](16) NOT NULL,
	[PageSize] [varchar](16) NOT NULL,
	[PageOrient] [char](1) NOT NULL,
	[CharsDef] [varchar](16) NOT NULL,
	[cc] [varchar](16) NOT NULL,
	[Fcb] [varchar](16) NOT NULL,
	[Trc] [char](1) NOT NULL,
	[Formatted] [bit] NOT NULL,
	[t1Fonts] [bit] NOT NULL,
	[LaserAdjust] [varchar](32) NOT NULL,
	[FormDef] [varchar](16) NOT NULL,
	[PageDef] [varchar](16) NOT NULL,
	[ReplaceDef] [varchar](16) NOT NULL,
	[PrintControlFile] [varchar](16) NOT NULL,
 CONSTRAINT [PK_Os390PrintParameters] PRIMARY KEY CLUSTERED 
(
	[JobReportName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ObjPendingBackups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ObjPendingBackups](
	[ObjId] [int] NOT NULL,
	[ObjType] [tinyint] NOT NULL,
	[ObjFacet] [tinyint] NOT NULL,
	[Timeref] [datetime] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[FileName] [varchar](255) NOT NULL,
	[StorageClass] [varchar](255) NOT NULL,
 CONSTRAINT [PK_tbl_ObjPendingBackups] PRIMARY KEY CLUSTERED 
(
	[ObjId] ASC,
	[ObjType] ASC,
	[ObjFacet] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Objects]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Objects](
	[ObjectId] [char](6) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[XferStartTime] [datetime] NOT NULL,
	[XferEndTime] [datetime] NULL,
	[MIMETYPE] [smallint] NULL,
	[StorageURI] [varchar](255) NULL,
 CONSTRAINT [PK_tbl_Objects] PRIMARY KEY CLUSTERED 
(
	[ObjectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ObjBackups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ObjBackups](
	[ObjId] [int] NOT NULL,
	[ObjFacet] [tinyint] NOT NULL,
	[BackupListId] [int] NOT NULL,
 CONSTRAINT [PK_tbl_JobReportsBackups] PRIMARY KEY CLUSTERED 
(
	[ObjId] ASC,
	[ObjFacet] ASC,
	[BackupListId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_NetBackupArchives]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_NetBackupArchives](
	[NBUArchiveId] [int] NOT NULL,
	[KeyWord] [varchar](64) NOT NULL,
	[IOType] [char](1) NOT NULL,
	[TimeQueued] [char](10) NULL,
	[TimeStarted] [smalldatetime] NULL,
	[TimeEnded] [char](10) NULL,
	[Status] [smallint] NOT NULL,
	[VirtualDIR] [varchar](1024) NOT NULL,
 CONSTRAINT [PK_tbl_NetBackupArchives] PRIMARY KEY CLUSTERED 
(
	[NBUArchiveId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_NetBackupAchivesTarFiles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_NetBackupAchivesTarFiles](
	[NBUArchiveId] [int] NOT NULL,
	[ObjectType] [smallint] NOT NULL,
	[ObjectId] [int] NOT NULL,
 CONSTRAINT [PK_tbl_NetBackupArchivesJobReports] PRIMARY KEY CLUSTERED 
(
	[NBUArchiveId] ASC,
	[ObjectType] ASC,
	[ObjectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_NetBackup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_NetBackup](
	[data] [datetime] NULL,
	[nroFileGet_fromDb] [decimal](18, 0) NULL,
	[nroFileSpool_3_year] [decimal](18, 0) NULL,
	[nroFileSpool_7_year] [decimal](18, 0) NULL,
	[nroFileSpool_10_year] [decimal](18, 0) NULL,
	[nroFileRepo_Tot] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_NamedReportsGroups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_NamedReportsGroups](
	[ReportGroupId] [varchar](32) NOT NULL,
	[ReportRule] [varchar](32) NOT NULL,
	[FilterVar] [varchar](32) NOT NULL,
	[FilterRule] [varchar](32) NOT NULL,
	[RecipientRule] [varchar](32) NOT NULL,
	[GroupIdentity] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_tbl_NamedReportsGroups] PRIMARY KEY CLUSTERED 
(
	[ReportGroupId] ASC,
	[ReportRule] ASC,
	[FilterVar] ASC,
	[FilterRule] ASC,
	[RecipientRule] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_NamedReportsGroups] ON
INSERT [dbo].[tbl_NamedReportsGroups] ([ReportGroupId], [ReportRule], [FilterVar], [FilterRule], [RecipientRule], [GroupIdentity]) VALUES (N'TEST', N'TEST', N'', N'', N'', 1)
SET IDENTITY_INSERT [dbo].[tbl_NamedReportsGroups] OFF
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports_HIST]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_LogicalReports_HIST](
	[JobReportId] [int] NOT NULL,
	[ReportId] [smallint] NOT NULL,
	[ReportName] [varchar](32) NOT NULL,
	[FilterValue] [varchar](32) NOT NULL,
	[TotPages] [int] NOT NULL,
	[XferDateDiff] [smallint] NOT NULL,
	[XferRecipient] [varchar](32) NOT NULL,
 CONSTRAINT [PK_tbl_LogicalReports_HIST] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC,
	[ReportId] ASC,
	[ReportName] ASC,
	[FilterValue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_LogicalReports](
	[JobReportId] [int] NOT NULL,
	[ReportId] [smallint] NOT NULL,
	[ReportName] [varchar](32) NOT NULL,
	[FilterValue] [varchar](32) NOT NULL,
	[TotPages] [int] NOT NULL,
	[XferDateDiff] [smallint] NOT NULL,
	[XferRecipient] [varchar](32) NOT NULL,
	[CheckStatus] [bit] NOT NULL,
	[HasNotes] [bit] NOT NULL,
	[LastUsedToView] [varchar](32) NOT NULL,
	[LastUsedToCheck] [varchar](32) NOT NULL,
 CONSTRAINT [PK_tbl_LogicalReports] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC,
	[ReportId] ASC,
	[ReportName] ASC,
	[FilterValue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobSpool]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobSpool](
	[JobSpoolId] [int] IDENTITY(1,1) NOT NULL,
	[InsertTime]  AS (getdate()),
	[JobReportId] [int] NOT NULL,
	[SpoolPathId] [varchar](255) NOT NULL,
	[InputFileName] [varchar](255) NULL,
	[LocalFileName] [varchar](255) NOT NULL,
	[ContentType] [varchar](255) NOT NULL,
	[PrintDest] [varchar](255) NOT NULL,
	[PrintDuplex] [smallint] NOT NULL,
	[PrintParameters] [varchar](255) NULL,
	[RemoteHostName] [varchar](255) NULL,
	[RemoteHostAddr] [varchar](255) NULL,
	[RemoteQueueName] [varchar](255) NULL,
	[JobName] [varchar](255) NULL,
	[JobNumber] [varchar](255) NULL,
	[XferDaemon] [varchar](255) NULL,
	[ElabStartTime] [datetime] NULL,
	[ElabEndTime] [datetime] NULL,
	[XferStartTime] [datetime] NULL,
	[XferEndTime] [datetime] NULL,
	[XferRecipient] [varchar](255) NULL,
	[XferMode] [char](1) NULL,
	[XferInBytes] [numeric](19, 0) NULL,
	[XferOutBytes] [numeric](18, 0) NULL,
	[XferId] [varchar](255) NULL,
	[Status] [smallint] NULL,
	[RetryCount] [int] NULL,
	[SrvName] [varchar](255) NULL,
 CONSTRAINT [PK_tbl_JobSpool] PRIMARY KEY CLUSTERED 
(
	[JobSpoolId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsXfers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportsXfers](
	[JobReportId] [char](10) NOT NULL,
	[RemoteHostAddr] [varchar](32) NULL,
	[RemoteFileName] [varchar](255) NULL,
	[JobName] [varchar](16) NULL,
	[JobNumber] [varchar](16) NULL,
	[JobExecutionTime] [datetime] NULL,
	[XferStartTime] [datetime] NULL,
	[XferEndTime] [datetime] NULL,
	[XferRecipient] [varchar](32) NULL,
	[XferMode] [char](1) NULL,
	[XferInBytes] [numeric](19, 0) NULL,
	[XferInLines] [numeric](19, 0) NULL,
	[XferInPages] [numeric](18, 0) NULL,
	[XferOutBytes] [numeric](18, 0) NULL,
	[XferDaemon] [varchar](16) NULL,
	[XferId] [varchar](16) NULL,
 CONSTRAINT [PK_tbl_JobReportsXfers] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsUrls]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportsUrls](
	[OUTFile_Url] [varchar](200) NULL,
	[JobReportId] [int] NULL,
	[INFile_Url] [varchar](200) NULL,
	[INFile_TotalSize] [int] NULL,
	[OUTFile_TotalSize] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsTarFiles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportsTarFiles](
	[JobReportId] [int] NOT NULL,
	[TarFileNumberIN] [smallint] NOT NULL,
	[TarRecNumberIN] [smallint] NOT NULL,
	[TarFileNumberOUT] [smallint] NOT NULL,
	[TarRecNumberOUT] [smallint] NOT NULL,
 CONSTRAINT [PK_tbl_JobReportsTarFiles] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsPendingXfers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportsPendingXfers](
	[JobReportId] [int] NOT NULL,
	[TimeRef] [char](14) NOT NULL,
	[TimeInsert] [datetime] NOT NULL,
	[HostName] [varchar](32) NOT NULL,
	[RemoteFileName] [varchar](255) NOT NULL,
 CONSTRAINT [PK_tbl_JobReportsPendingXfers] PRIMARY KEY CLUSTERED 
(
	[HostName] ASC,
	[RemoteFileName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsMixedReportIds]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportsMixedReportIds](
	[JobReportId] [int] NOT NULL,
	[MD5Hash] [char](32) NOT NULL,
	[ReportId] [smallint] NOT NULL,
	[ListOfReportIds] [text] NOT NULL,
 CONSTRAINT [PK_tbl_JobReportsMixedReportIds] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC,
	[MD5Hash] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsLocalFileCache]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportsLocalFileCache](
	[JobReportId] [int] NOT NULL,
	[LocalPath] [varchar](32) NOT NULL,
	[LocalBasket] [smallint] NOT NULL,
	[UsedTimes] [smallint] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[LastUsedTime] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_LocalFileCache] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsIndexTables]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportsIndexTables](
	[IndexName] [varchar](64) NOT NULL,
	[JobReportId] [int] NOT NULL,
	[TotEntries] [decimal](18, 0) NULL,
	[UserTimeRef] [datetime] NULL,
 CONSTRAINT [PK_tbl_JobReportsIndexTables] PRIMARY KEY CLUSTERED 
(
	[IndexName] ASC,
	[JobReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsFileRanges]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportsFileRanges](
	[JobReportId] [int] NOT NULL,
	[FileId] [smallint] NOT NULL,
	[FromValue] [varchar](64) NOT NULL,
	[ToValue] [varchar](64) NOT NULL,
	[TotPages] [int] NOT NULL,
 CONSTRAINT [PK_tbl_JobReportsFileRanges] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC,
	[FileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportsElabs](
	[JobReportId] [int] NOT NULL,
	[ElabFormat] [smallint] NULL,
	[ElabStartTime] [datetime] NULL,
	[ElabEndTime] [datetime] NULL,
	[ElabOutBytes] [numeric](18, 0) NULL,
	[InputLines] [numeric](18, 0) NULL,
	[InputPages] [numeric](18, 0) NULL,
	[ParsedLines] [numeric](18, 0) NULL,
	[ParsedPages] [numeric](18, 0) NULL,
	[DiscardedLines] [numeric](18, 0) NULL,
	[DiscardedPages] [numeric](18, 0) NULL,
	[FileRangesVar] [varchar](64) NULL,
	[isAFP] [bit] NULL,
	[isFullAFP] [bit] NULL,
	[Status] [smallint] NULL,
	[UserTimeRef] [datetime] NULL,
	[UserTimeElab] [datetime] NULL,
	[UserRef] [varchar](64) NULL,
	[FileRangeVar] [varchar](32) NULL,
	[ElabOptionsXML] [text] NULL,
 CONSTRAINT [PK_tbl_JobReportsElabs] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions_HIST]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportsElabOptions_HIST](
	[JobReportId] [decimal](18, 0) NOT NULL,
	[ElabOptions] [text] NOT NULL,
 CONSTRAINT [PK_tbl_JobReportsElabOptions_HIST] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportsElabOptions](
	[JobReportId] [decimal](18, 0) NOT NULL,
	[ElabFormat] [tinyint] NOT NULL,
	[HasIndexes] [bit] NOT NULL,
	[isAfp] [bit] NOT NULL,
	[isFullAfp] [bit] NOT NULL,
	[FileRangeVar] [varchar](32) NOT NULL,
	[ExistTypeMap] [int] NOT NULL,
	[MaybeTypeMap] [int] NOT NULL,
	[ElabOptions] [text] NOT NULL,
 CONSTRAINT [PK_tbl_JobReportsElabOptions] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsDiscardedPages]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportsDiscardedPages](
	[JobReportId] [int] NOT NULL,
	[ListOfPages] [text] NOT NULL,
 CONSTRAINT [PK_tbl_JobReportsDiscardedPages] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsCenteraClipIds]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportsCenteraClipIds](
	[JobReportId] [int] NOT NULL,
	[PoolRef] [varchar](32) NOT NULL,
	[INFile_ClipId] [varchar](64) NOT NULL,
	[OUTFile_ClipId] [varchar](64) NULL,
 CONSTRAINT [PK_tbl_JobReportsCenteraClipIds] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReports_HIST](
	[JobReportName] [varchar](32) NULL,
	[JobReportId] [int] NOT NULL,
	[LocalPathId] [varchar](64) NULL,
	[LocalFileName] [varchar](255) NOT NULL,
	[OrigJobReportName] [varchar](32) NULL,
	[TarFileNumberIN] [smallint] NULL,
	[TarRecNumberIN] [smallint] NULL,
	[TarFileNumberOUT] [smallint] NULL,
	[TarRecNumberOUT] [smallint] NULL,
	[RemoteHostAddr] [varchar](32) NULL,
	[RemoteFileName] [varchar](255) NULL,
	[JobName] [varchar](16) NULL,
	[JobNumber] [varchar](16) NULL,
	[JobExecutionTime] [datetime] NULL,
	[XferStartTime] [datetime] NULL,
	[XferEndTime] [datetime] NULL,
	[XferRecipient] [varchar](32) NULL,
	[XferMode] [char](1) NULL,
	[XferInBytes] [numeric](19, 0) NULL,
	[XferInLines] [numeric](19, 0) NULL,
	[XferInPages] [numeric](18, 0) NULL,
	[XferOutBytes] [numeric](18, 0) NULL,
	[XferDaemon] [varchar](16) NULL,
	[XferId] [varchar](16) NULL,
	[ElabStartTime] [datetime] NULL,
	[ElabEndTime] [datetime] NULL,
	[ElabOutBytes] [numeric](18, 0) NULL,
	[InputLines] [numeric](18, 0) NULL,
	[InputPages] [numeric](18, 0) NULL,
	[ParsedLines] [numeric](18, 0) NULL,
	[ParsedPages] [numeric](18, 0) NULL,
	[DiscardedLines] [numeric](18, 0) NULL,
	[DiscardedPages] [numeric](18, 0) NULL,
	[Status] [smallint] NULL,
	[ViewLevel] [smallint] NULL,
	[MailingStatus] [smallint] NULL,
	[PendingOp] [smallint] NULL,
	[SrvName] [varchar](16) NULL,
	[UserTimeRef] [datetime] NULL,
	[UserTimeElab] [datetime] NULL,
	[UserRef] [varchar](64) NOT NULL,
	[XrefData] [varchar](64) NULL,
 CONSTRAINT [PK_tbl_JobReports_HIST] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReports](
	[JobReportName] [varchar](32) NULL,
	[JobReportId] [int] IDENTITY(1,1) NOT NULL,
	[DateTime] [char](14) NULL,
	[LocalFileName] [varchar](255) NOT NULL,
	[LocalPathId_IN] [varchar](64) NULL,
	[LocalPathId_OUT] [varchar](64) NULL,
	[OrigJobReportName] [varchar](32) NULL,
	[TarFileNumberIN] [int] NULL,
	[TarRecNumberIN] [smallint] NULL,
	[TarFileNumberOUT] [int] NULL,
	[TarRecNumberOUT] [smallint] NULL,
	[RemoteHostAddr] [varchar](32) NULL,
	[RemoteFileName] [varchar](255) NULL,
	[JobName] [varchar](64) NULL,
	[JobNumber] [varchar](16) NULL,
	[JobExecutionTime] [datetime] NULL,
	[XferStartTime] [datetime] NULL,
	[XferEndTime] [datetime] NULL,
	[XferRecipient] [varchar](32) NULL,
	[XferMode] [char](1) NULL,
	[XferInBytes] [numeric](19, 0) NULL,
	[XferInLines] [numeric](19, 0) NULL,
	[XferInPages] [numeric](18, 0) NULL,
	[XferOutBytes] [numeric](18, 0) NULL,
	[XferDaemon] [varchar](16) NULL,
	[XferId] [varchar](16) NULL,
	[ElabStartTime] [datetime] NULL,
	[ElabEndTime] [datetime] NULL,
	[ElabOutBytes] [numeric](18, 0) NULL,
	[InputLines] [numeric](18, 0) NULL,
	[InputPages] [numeric](18, 0) NULL,
	[ParsedLines] [numeric](18, 0) NULL,
	[ParsedPages] [numeric](18, 0) NULL,
	[DiscardedLines] [numeric](18, 0) NULL,
	[DiscardedPages] [numeric](18, 0) NULL,
	[HasIndexes] [bit] NOT NULL,
	[isAFP] [bit] NOT NULL,
	[isFullAFP] [bit] NOT NULL,
	[ExistTypeMap] [int] NOT NULL,
	[MayBeTypeMap] [int] NOT NULL,
	[FileRangesVar] [varchar](64) NULL,
	[Status] [smallint] NULL,
	[ViewLevel] [smallint] NULL,
	[MailingStatus] [smallint] NULL,
	[PendingOp] [smallint] NULL,
	[SrvName] [varchar](64) NULL,
	[UserTimeRef] [datetime] NULL,
	[UserTimeElab] [datetime] NULL,
	[UserRef] [varchar](64) NOT NULL,
	[XrefData] [varchar](64) NULL,
	[ijrar] [varchar](16) NULL,
	[ojrar] [varchar](16) NULL,
 CONSTRAINT [PK_tbl_JobReports] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportNamesQrexp](
	[fmJobReportName] [varchar](32) NOT NULL,
	[TestOrder] [smallint] NOT NULL,
	[JobName] [varchar](32) NOT NULL,
	[Recipient] [varchar](32) NOT NULL,
	[RemoteHostAddr] [varchar](32) NOT NULL,
	[RemoteFileName] [varchar](255) NOT NULL,
	[PageText] [varchar](255) NOT NULL,
	[MaxPageTests] [smallint] NOT NULL,
	[toJobReportName] [varchar](32) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQaliases]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportNamesQaliases](
	[JobReportName] [varchar](32) NOT NULL,
	[AliasOf] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportNames](
	[JobReportName] [varchar](32) NOT NULL,
	[JobReportDescr] [varchar](64) NOT NULL,
	[ReportFormat] [tinyint] NOT NULL,
	[ElabFormat] [tinyint] NOT NULL,
	[IsActive] [char](1) NOT NULL,
	[PrintFlag] [char](1) NOT NULL,
	[ViewOnlineFlag] [char](1) NOT NULL,
	[ActiveDays] [smallint] NOT NULL,
	[ActiveGens] [smallint] NOT NULL,
	[HoldDays] [smallint] NOT NULL,
	[HoldGens] [smallint] NOT NULL,
	[StorageClass] [varchar](64) NOT NULL,
	[CharsPerLine] [smallint] NOT NULL,
	[LinesPerPage] [smallint] NOT NULL,
	[PageOrient] [char](1) NOT NULL,
	[HasCc] [bit] NOT NULL,
	[FontSize] [varchar](32) NOT NULL,
	[FitToPage] [bit] NOT NULL,
	[CodePage] [varchar](32) NOT NULL,
	[TotJobReports] [int] NOT NULL,
	[TotPages] [decimal](18, 0) NOT NULL,
	[ClIndexTable] [varchar](64) NOT NULL,
	[ParseFileName] [varchar](64) NOT NULL,
	[TargetLocalPathId_IN] [varchar](32) NOT NULL,
	[TargetLocalPathId_OUT] [varchar](32) NOT NULL,
	[TypeOfWork] [smallint] NOT NULL,
	[WorkClass] [smallint] NULL,
	[MailTo] [varchar](64) NOT NULL,
	[XrefData] [varchar](64) NOT NULL,
	[ModifyKey] [varchar](16) NOT NULL,
	[ModifyTime] [decimal](18, 0) NULL,
 CONSTRAINT [PK_tbl_JobReportNames] PRIMARY KEY CLUSTERED 
(
	[JobReportName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_JobReportNames] ([JobReportName], [JobReportDescr], [ReportFormat], [ElabFormat], [IsActive], [PrintFlag], [ViewOnlineFlag], [ActiveDays], [ActiveGens], [HoldDays], [HoldGens], [StorageClass], [CharsPerLine], [LinesPerPage], [PageOrient], [HasCc], [FontSize], [FitToPage], [CodePage], [TotJobReports], [TotPages], [ClIndexTable], [ParseFileName], [TargetLocalPathId_IN], [TargetLocalPathId_OUT], [TypeOfWork], [WorkClass], [MailTo], [XrefData], [ModifyKey], [ModifyTime]) VALUES (N'LCC02190_01', N'', 2, 1, N'1', N'Y', N'Y', 5, 5, 30, 30, N'', 133, 70, N'L', 1, N'[9.0 8.24]', 0, N'', 1, CAST(0 AS Decimal(18, 0)), N'', N'*,NULL.XML', N'L1', N'', 1, NULL, N'', N'', N'', NULL)
INSERT [dbo].[tbl_JobReportNames] ([JobReportName], [JobReportDescr], [ReportFormat], [ElabFormat], [IsActive], [PrintFlag], [ViewOnlineFlag], [ActiveDays], [ActiveGens], [HoldDays], [HoldGens], [StorageClass], [CharsPerLine], [LinesPerPage], [PageOrient], [HasCc], [FontSize], [FitToPage], [CodePage], [TotJobReports], [TotPages], [ClIndexTable], [ParseFileName], [TargetLocalPathId_IN], [TargetLocalPathId_OUT], [TypeOfWork], [WorkClass], [MailTo], [XrefData], [ModifyKey], [ModifyTime]) VALUES (N'LCC02193_01', N'', 2, 1, N'1', N'Y', N'Y', 5, 5, 30, 30, N'', 133, 70, N'L', 1, N'[9.0 8.24]', 0, N'', 0, CAST(0 AS Decimal(18, 0)), N'', N'*,NULL.XML', N'L1', N'', 1, NULL, N'', N'', N'', NULL)
INSERT [dbo].[tbl_JobReportNames] ([JobReportName], [JobReportDescr], [ReportFormat], [ElabFormat], [IsActive], [PrintFlag], [ViewOnlineFlag], [ActiveDays], [ActiveGens], [HoldDays], [HoldGens], [StorageClass], [CharsPerLine], [LinesPerPage], [PageOrient], [HasCc], [FontSize], [FitToPage], [CodePage], [TotJobReports], [TotPages], [ClIndexTable], [ParseFileName], [TargetLocalPathId_IN], [TargetLocalPathId_OUT], [TypeOfWork], [WorkClass], [MailTo], [XrefData], [ModifyKey], [ModifyTime]) VALUES (N'LCC02193_02', N'', 2, 1, N'1', N'Y', N'Y', 5, 5, 30, 30, N'', 133, 70, N'L', 1, N'[9.0 8.24]', 0, N'', 0, CAST(0 AS Decimal(18, 0)), N'', N'*,NULL.XML', N'L1', N'', 1, NULL, N'', N'', N'', NULL)
INSERT [dbo].[tbl_JobReportNames] ([JobReportName], [JobReportDescr], [ReportFormat], [ElabFormat], [IsActive], [PrintFlag], [ViewOnlineFlag], [ActiveDays], [ActiveGens], [HoldDays], [HoldGens], [StorageClass], [CharsPerLine], [LinesPerPage], [PageOrient], [HasCc], [FontSize], [FitToPage], [CodePage], [TotJobReports], [TotPages], [ClIndexTable], [ParseFileName], [TargetLocalPathId_IN], [TargetLocalPathId_OUT], [TypeOfWork], [WorkClass], [MailTo], [XrefData], [ModifyKey], [ModifyTime]) VALUES (N'LCC02284_01', N'', 2, 1, N'1', N'Y', N'Y', 5, 5, 30, 30, N'', 133, 70, N'L', 1, N'[9.0 8.24]', 0, N'', 0, CAST(0 AS Decimal(18, 0)), N'', N'*,NULL.XML', N'L1', N'', 1, NULL, N'', N'', N'', NULL)
INSERT [dbo].[tbl_JobReportNames] ([JobReportName], [JobReportDescr], [ReportFormat], [ElabFormat], [IsActive], [PrintFlag], [ViewOnlineFlag], [ActiveDays], [ActiveGens], [HoldDays], [HoldGens], [StorageClass], [CharsPerLine], [LinesPerPage], [PageOrient], [HasCc], [FontSize], [FitToPage], [CodePage], [TotJobReports], [TotPages], [ClIndexTable], [ParseFileName], [TargetLocalPathId_IN], [TargetLocalPathId_OUT], [TypeOfWork], [WorkClass], [MailTo], [XrefData], [ModifyKey], [ModifyTime]) VALUES (N'LCC04096_01', N'', 2, 1, N'1', N'Y', N'Y', 5, 5, 30, 30, N'', 133, 70, N'L', 1, N'[9.0 8.24]', 0, N'', 1, CAST(0 AS Decimal(18, 0)), N'', N'*,NULL.XML', N'L1', N'', 1, NULL, N'', N'', N'', NULL)
INSERT [dbo].[tbl_JobReportNames] ([JobReportName], [JobReportDescr], [ReportFormat], [ElabFormat], [IsActive], [PrintFlag], [ViewOnlineFlag], [ActiveDays], [ActiveGens], [HoldDays], [HoldGens], [StorageClass], [CharsPerLine], [LinesPerPage], [PageOrient], [HasCc], [FontSize], [FitToPage], [CodePage], [TotJobReports], [TotPages], [ClIndexTable], [ParseFileName], [TargetLocalPathId_IN], [TargetLocalPathId_OUT], [TypeOfWork], [WorkClass], [MailTo], [XrefData], [ModifyKey], [ModifyTime]) VALUES (N'LCC04096_02', N'', 2, 1, N'1', N'Y', N'Y', 5, 5, 30, 30, N'', 133, 70, N'L', 1, N'[9.0 8.24]', 0, N'', 1, CAST(0 AS Decimal(18, 0)), N'', N'*,NULL.XML', N'L1', N'', 1, NULL, N'', N'', N'', NULL)
INSERT [dbo].[tbl_JobReportNames] ([JobReportName], [JobReportDescr], [ReportFormat], [ElabFormat], [IsActive], [PrintFlag], [ViewOnlineFlag], [ActiveDays], [ActiveGens], [HoldDays], [HoldGens], [StorageClass], [CharsPerLine], [LinesPerPage], [PageOrient], [HasCc], [FontSize], [FitToPage], [CodePage], [TotJobReports], [TotPages], [ClIndexTable], [ParseFileName], [TargetLocalPathId_IN], [TargetLocalPathId_OUT], [TypeOfWork], [WorkClass], [MailTo], [XrefData], [ModifyKey], [ModifyTime]) VALUES (N'LCC04096_03', N'', 2, 1, N'1', N'Y', N'Y', 5, 5, 30, 30, N'', 133, 70, N'L', 1, N'[9.0 8.24]', 0, N'', 0, CAST(0 AS Decimal(18, 0)), N'', N'*,NULL.XML', N'L1', N'', 1, NULL, N'', N'', N'', NULL)
INSERT [dbo].[tbl_JobReportNames] ([JobReportName], [JobReportDescr], [ReportFormat], [ElabFormat], [IsActive], [PrintFlag], [ViewOnlineFlag], [ActiveDays], [ActiveGens], [HoldDays], [HoldGens], [StorageClass], [CharsPerLine], [LinesPerPage], [PageOrient], [HasCc], [FontSize], [FitToPage], [CodePage], [TotJobReports], [TotPages], [ClIndexTable], [ParseFileName], [TargetLocalPathId_IN], [TargetLocalPathId_OUT], [TypeOfWork], [WorkClass], [MailTo], [XrefData], [ModifyKey], [ModifyTime]) VALUES (N'LCC04096_04', N'', 2, 1, N'1', N'Y', N'Y', 5, 5, 30, 30, N'', 133, 70, N'L', 1, N'[9.0 8.24]', 0, N'', 1, CAST(0 AS Decimal(18, 0)), N'', N'*,NULL.XML', N'L1', N'', 1, NULL, N'', N'', N'', NULL)
INSERT [dbo].[tbl_JobReportNames] ([JobReportName], [JobReportDescr], [ReportFormat], [ElabFormat], [IsActive], [PrintFlag], [ViewOnlineFlag], [ActiveDays], [ActiveGens], [HoldDays], [HoldGens], [StorageClass], [CharsPerLine], [LinesPerPage], [PageOrient], [HasCc], [FontSize], [FitToPage], [CodePage], [TotJobReports], [TotPages], [ClIndexTable], [ParseFileName], [TargetLocalPathId_IN], [TargetLocalPathId_OUT], [TypeOfWork], [WorkClass], [MailTo], [XrefData], [ModifyKey], [ModifyTime]) VALUES (N'XRRENAME', N'generic report name', 2, 1, N'1', N'Y', N'Y', 5, 5, 30, 30, N'', 133, 70, N'L', 1, N'[9.0 8.24]', 0, N'', 0, CAST(0 AS Decimal(18, 0)), N'', N'*,XRRENAME.XML', N'L1', N'', 1, 100, N'', N'', N'', NULL)
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportFormatCodes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobReportFormatCodes](
	[JobReportFormat] [char](1) NOT NULL,
	[ReportFormatDescr] [varchar](16) NULL,
 CONSTRAINT [PK_tbl_JobReportFormatCodes] PRIMARY KEY CLUSTERED 
(
	[JobReportFormat] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_JobReportFormatCodes] ([JobReportFormat], [ReportFormatDescr]) VALUES (N'1', N'ASCII')
INSERT [dbo].[tbl_JobReportFormatCodes] ([JobReportFormat], [ReportFormatDescr]) VALUES (N'2', N'EBCDIC')
INSERT [dbo].[tbl_JobReportFormatCodes] ([JobReportFormat], [ReportFormatDescr]) VALUES (N'3', N'AFP')
INSERT [dbo].[tbl_JobReportFormatCodes] ([JobReportFormat], [ReportFormatDescr]) VALUES (N'4', N'VIPP')
INSERT [dbo].[tbl_JobReportFormatCodes] ([JobReportFormat], [ReportFormatDescr]) VALUES (N'5', N'XML')
INSERT [dbo].[tbl_JobReportFormatCodes] ([JobReportFormat], [ReportFormatDescr]) VALUES (N'6', N'POSTSCRIPT')
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobElabs_ERROR_MESSAGES]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobElabs_ERROR_MESSAGES](
	[TableName] [varchar](64) NOT NULL,
	[itemid] [int] NOT NULL,
	[Status] [char](10) NOT NULL,
	[ERROR_MESSAGE] [varchar](4096) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobCtlInput]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_JobCtlInput](
	[JobCtlHash] [varchar](64) NOT NULL,
	[JobReportId] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_IndexTables]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_IndexTables](
	[IndexName] [varchar](64) NOT NULL,
	[DatabaseName] [varchar](64) NOT NULL,
 CONSTRAINT [PK_tbl_IndexTables] PRIMARY KEY CLUSTERED 
(
	[IndexName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersTreesHierarchies]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_FoldersTreesHierarchies](
	[FolderTreeHierarchy] [varchar](512) NOT NULL,
	[FolderName] [varchar](32) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[FolderTreeName] [varchar](32) NOT NULL,
 CONSTRAINT [PK_tbl_FoldersTreesHierarchies] PRIMARY KEY CLUSTERED 
(
	[FolderTreeHierarchy] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersTrees]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_FoldersTrees](
	[FolderTreeName] [varchar](64) NOT NULL,
	[FolderTreeDescr] [varchar](64) NOT NULL,
 CONSTRAINT [PK_tbl_FoldersTrees] PRIMARY KEY CLUSTERED 
(
	[FolderTreeName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersRules]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_FoldersRules](
	[FolderName] [varchar](64) NOT NULL,
	[ReportGroupId] [varchar](32) NOT NULL,
	[FORBID] [bit] NOT NULL,
	[RuleId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_tbl_FoldersRules] PRIMARY KEY CLUSTERED 
(
	[FolderName] ASC,
	[ReportGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_FoldersRules] ON
INSERT [dbo].[tbl_FoldersRules] ([FolderName], [ReportGroupId], [FORBID], [RuleId]) VALUES (N'GESTSIST', N'TEST', 0, 1)
SET IDENTITY_INSERT [dbo].[tbl_FoldersRules] OFF
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_old]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_FoldersReportNames_old](
	[FolderName] [varchar](32) NOT NULL,
	[ReportRule] [varchar](32) NOT NULL,
	[FilterVar] [varchar](32) NOT NULL,
	[FilterRule] [varchar](32) NOT NULL,
	[RecipientRule] [varchar](32) NOT NULL,
	[FORBID] [bit] NOT NULL,
	[RuleId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_tbl_FoldersReportNames] PRIMARY KEY CLUSTERED 
(
	[FolderName] ASC,
	[ReportRule] ASC,
	[FilterVar] ASC,
	[FilterRule] ASC,
	[RecipientRule] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_LL2]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_FoldersReportNames_LL2](
	[FolderName] [varchar](32) NOT NULL,
	[ReportName] [varchar](32) NOT NULL,
	[FilterValue] [varchar](32) NOT NULL,
	[RecipientName] [varchar](32) NOT NULL,
	[FORBID] [bit] NOT NULL,
	[RuleId] [int] NOT NULL,
 CONSTRAINT [PK_tbl_FoldersReportNames_LL2] PRIMARY KEY CLUSTERED 
(
	[FolderName] ASC,
	[ReportName] ASC,
	[FilterValue] ASC,
	[RecipientName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_LL1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_FoldersReportNames_LL1](
	[FolderName] [varchar](32) NOT NULL,
	[ReportName] [varchar](32) NOT NULL,
	[FilterValue] [varchar](32) NOT NULL,
	[RecipientName] [varchar](32) NOT NULL,
 CONSTRAINT [PK_tbl_FoldersReportNames_LL1] PRIMARY KEY CLUSTERED 
(
	[FolderName] ASC,
	[ReportName] ASC,
	[FilterValue] ASC,
	[RecipientName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Folders]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Folders](
	[FolderName] [varchar](64) NOT NULL,
	[FolderDescr] [varchar](128) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ParentFolder] [varchar](64) NOT NULL,
	[FolderAddr] [varchar](500) NULL,
	[SpecialInstr] [varchar](500) NULL,
	[FolderNameLevel] [varchar](2) NULL,
	[FolderNameKey] [varchar](32) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_Folders] ([FolderName], [FolderDescr], [IsActive], [ParentFolder], [FolderAddr], [SpecialInstr], [FolderNameLevel], [FolderNameKey]) VALUES (N'GESTSIST', N'GESTIONE PRODOTTO CONTROL-D', 1, N'', N'', NULL, NULL, NULL)
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FilterVarValues]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_FilterVarValues](
	[FilterVar] [varchar](32) NOT NULL,
	[FilterValue] [varchar](32) NOT NULL,
	[FilterDescr] [varchar](64) NOT NULL,
 CONSTRAINT [PK_tbl_FilterVarValues] PRIMARY KEY CLUSTERED 
(
	[FilterVar] ASC,
	[FilterValue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_EmailSentList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_EmailSentList](
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[SmtpServer] [varchar](128) NULL,
	[TotBytes] [int] NULL,
	[Status] [tinyint] NOT NULL,
	[SendNumber] [smallint] NOT NULL,
	[TotRetries] [smallint] NOT NULL,
	[MailClass] [varchar](32) NULL,
	[FromField] [varchar](64) NULL,
	[FakeFromField] [varchar](64) NULL,
	[ToField] [varchar](64) NULL,
	[FakeToField] [varchar](64) NULL,
	[SubjectField] [varchar](64) NULL,
	[BodyField] [varchar](4096) NULL,
	[ExitArgs] [varchar](1024) NULL,
	[EmailerArgs] [varchar](1024) NULL,
	[FileAttachments] [varchar](1024) NULL,
	[JobReportId] [int] NOT NULL,
	[FromPage] [int] NOT NULL,
	[ForPages] [smallint] NULL,
 CONSTRAINT [PK_tbl_EmailSentList] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC,
	[FromPage] ASC,
	[SendNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_EMailPendingQueue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_EMailPendingQueue](
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[SmtpServer] [varchar](128) NULL,
	[TotBytes] [int] NULL,
	[Status] [tinyint] NULL,
	[SendNumber] [smallint] NULL,
	[TotRetries] [smallint] NULL,
	[MailClass] [varchar](32) NULL,
	[FromField] [varchar](64) NULL,
	[FakeFromField] [varchar](64) NULL,
	[ToField] [varchar](64) NULL,
	[FakeToField] [varchar](64) NULL,
	[SubjectField] [varchar](64) NULL,
	[BodyField] [varchar](4096) NULL,
	[ExitArgs] [varchar](1024) NULL,
	[EmailerArgs] [varchar](1024) NULL,
	[FileAttachments] [varchar](1024) NULL,
	[JobReportId] [int] NOT NULL,
	[FromPage] [int] NOT NULL,
	[ForPages] [smallint] NOT NULL,
 CONSTRAINT [PK_tbl_EMailPendingQueue] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC,
	[FromPage] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_EmailJobReportsStat]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_EmailJobReportsStat](
	[JobReportId] [int] NOT NULL,
	[TotEmails] [int] NOT NULL,
	[SentEmails] [int] NOT NULL,
	[PendingEmails] [int] NOT NULL,
 CONSTRAINT [PK_tbl_EmailsJobReports] PRIMARY KEY CLUSTERED 
(
	[JobReportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ElabFormatCodes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ElabFormatCodes](
	[ElabFormat] [tinyint] NULL,
	[ElabFormatDescr] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_ElabFormatCodes] ([ElabFormat], [ElabFormatDescr]) VALUES (1, N'PDF')
INSERT [dbo].[tbl_ElabFormatCodes] ([ElabFormat], [ElabFormatDescr]) VALUES (2, N'RAWDATA')
INSERT [dbo].[tbl_ElabFormatCodes] ([ElabFormat], [ElabFormatDescr]) VALUES (3, N'QMF')
INSERT [dbo].[tbl_ElabFormatCodes] ([ElabFormat], [ElabFormatDescr]) VALUES (4, N'CSV')
INSERT [dbo].[tbl_ElabFormatCodes] ([ElabFormat], [ElabFormatDescr]) VALUES (101, N'AFPRESOURCES')
INSERT [dbo].[tbl_ElabFormatCodes] ([ElabFormat], [ElabFormatDescr]) VALUES (102, N'BUNDLE')
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ctd_XrefReportData](
	[CtdJobName] [char](8) NOT NULL,
	[CtdReportName] [char](20) NOT NULL,
	[XrExternalKey] [decimal](18, 0) NULL,
	[XrJobReportName] [varchar](16) NOT NULL,
	[XrJobReportDescr] [varchar](64) NOT NULL,
	[XrReportName] [varchar](16) NOT NULL,
	[XrReportDescr] [varchar](64) NOT NULL,
	[XrHoldDays] [char](16) NOT NULL,
	[XrHoldGens] [char](16) NOT NULL,
	[XrKeepDays] [char](16) NOT NULL,
	[XrKeepGens] [char](16) NOT NULL,
	[XrProfileList] [varchar](2048) NOT NULL,
	[XrFolderList] [varchar](2048) NOT NULL,
	[XrMailToList] [varchar](2048) NOT NULL,
	[XrParseFileName] [varchar](256) NOT NULL,
	[ModifyKey] [varchar](16) NULL,
	[ModifyTime] [decimal](18, 0) NULL,
 CONSTRAINT [PK_tbl_CTD_XrefReportData] PRIMARY KEY CLUSTERED 
(
	[XrReportName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ctd_ReportMappings]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ctd_ReportMappings](
	[CtdJobMask] [varchar](16) NOT NULL,
	[CtdReportName] [char](20) NOT NULL,
	[XrJobReportName] [varchar](16) NOT NULL,
	[XrJobSuffix] [smallint] NOT NULL,
	[XrParseFileName] [varchar](64) NOT NULL,
 CONSTRAINT [PK_tbl_ctd_ReportMappings] PRIMARY KEY CLUSTERED 
(
	[CtdJobMask] ASC,
	[CtdReportName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ctd_JobNameAlias]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ctd_JobNameAlias](
	[CtdJobName] [char](8) NOT NULL,
	[CtdJobAlias] [char](8) NOT NULL,
 CONSTRAINT [PK_tbl_ctd_JobNameAlias] PRIMARY KEY CLUSTERED 
(
	[CtdJobName] ASC,
	[CtdJobAlias] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES_XrefData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ctd_INBUNDLES_XrefData](
	[BUNDLEID] [int] NOT NULL,
	[PROGR] [smallint] NOT NULL,
	[CtdReportName] [char](20) NOT NULL,
	[JobName] [char](8) NOT NULL,
	[JobNumber] [int] NOT NULL,
	[DateTimeRef] [char](14) NOT NULL,
	[InputLines] [int] NOT NULL,
	[InputPages] [int] NOT NULL,
	[ExpInputPages] [int] NOT NULL,
	[ExpInputLines] [int] NOT NULL,
	[JobReportName] [varchar](32) NOT NULL,
	[JobReportid] [int] NOT NULL,
	[ERROR_MESSAGE] [varchar](2048) NOT NULL,
 CONSTRAINT [PK_tbl_ctd_BUNDLES_XrefData] PRIMARY KEY CLUSTERED 
(
	[BUNDLEID] ASC,
	[PROGR] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ctd_INBUNDLES](
	[BUNDLEID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[Host] [varchar](16) NOT NULL,
	[Dir] [varchar](255) NOT NULL,
	[FileName] [varchar](255) NOT NULL,
	[Status] [smallint] NOT NULL,
	[LocalFileName] [varchar](255) NOT NULL,
	[DateTimeString] [char](16) NOT NULL,
	[HostAddr] [varchar](32) NOT NULL,
	[SrvName] [varchar](16) NOT NULL,
	[XferMode] [tinyint] NULL,
	[InsertTime] [datetime] NOT NULL,
	[UnbundleTimeStart] [datetime] NULL,
	[UnbundleTimeEnd] [datetime] NULL,
 CONSTRAINT [PK_tbl_CTD_IN_Bundles] PRIMARY KEY CLUSTERED 
(
	[Host] ASC,
	[Dir] ASC,
	[FileName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [IX_tbl_InBundles_BUNDLEID] UNIQUE NONCLUSTERED 
(
	[BUNDLEID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_CenteraClips]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_CenteraClips](
	[CentClipId] [char](65) NOT NULL,
	[XrClipId] [int] NOT NULL,
	[Name] [varchar](32) NOT NULL,
	[TotTags] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Backups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Backups](
	[BkId] [int] NOT NULL,
	[BkStartTime] [datetime] NULL,
	[BkEndTime] [datetime] NULL,
	[BkKeyword] [varchar](32) NULL,
 CONSTRAINT [PK_tbl_Backups] PRIMARY KEY CLUSTERED 
(
	[BkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_AuditLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_AuditLog](
	[Operation] [varchar](64) NOT NULL,
	[OperationTime] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRGetAppLock]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRGetAppLock    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE PROCEDURE [dbo].[XRGetAppLock]
(
  @Resource As nvarchar(255),
  @LockMode As nvarchar(32),
  @LockOwner As nvarchar(32),
  @LockTimeout As int
)

AS

BEGIN
  DECLARE @Result int
  
  EXEC @Result = sp_getapplock 
   @Resource = @Resource, 
   @LockMode = @LockMode, 
   @LockOwner = @LockOwner, 
   @LockTimeout = @LockTimeout

  SELECT @Result
END

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[xreport_dbexport1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[xreport_dbexport1](
	[JobReportName] [varchar](50) NULL,
	[JobReportId] [varchar](50) NULL,
	[DateTime] [varchar](50) NULL,
	[LocalFileName] [varchar](64) NULL,
	[LocalPathId_IN] [varchar](50) NULL,
	[LocalPathId_OUT] [varchar](50) NULL,
	[OrigJobReportName] [varchar](50) NULL,
	[TarFileNumberIN] [varchar](50) NULL,
	[TarRecNumberIN] [varchar](50) NULL,
	[TarFileNumberOUT] [varchar](50) NULL,
	[TarRecNumberOUT] [varchar](50) NULL,
	[RemoteHostAddr] [varchar](50) NULL,
	[RemoteFileName] [varchar](50) NULL,
	[JobName] [varchar](50) NULL,
	[JobNumber] [varchar](50) NULL,
	[JobExecutionTime] [varchar](50) NULL,
	[XferStartTime] [varchar](50) NULL,
	[XferEndTime] [varchar](50) NULL,
	[XferRecipient] [varchar](50) NULL,
	[XferMode] [varchar](50) NULL,
	[XferInBytes] [varchar](50) NULL,
	[XferInLines] [varchar](50) NULL,
	[XferInPages] [varchar](50) NULL,
	[XferOutBytes] [varchar](50) NULL,
	[XferDaemon] [varchar](50) NULL,
	[XferId] [varchar](50) NULL,
	[ElabStartTime] [varchar](50) NULL,
	[ElabEndTime] [varchar](50) NULL,
	[ElabOutBytes] [varchar](50) NULL,
	[InputLines] [varchar](50) NULL,
	[InputPages] [varchar](50) NULL,
	[ParsedLines] [varchar](50) NULL,
	[ParsedPages] [varchar](50) NULL,
	[DiscardedLines] [varchar](50) NULL,
	[DiscardedPages] [varchar](50) NULL,
	[HasIndexes] [varchar](50) NULL,
	[isAFP] [varchar](50) NULL,
	[isFullAFP] [varchar](50) NULL,
	[ExistTypeMap] [varchar](50) NULL,
	[MayBeTypeMap] [varchar](50) NULL,
	[FileRangesVar] [varchar](50) NULL,
	[Status] [varchar](50) NULL,
	[ViewLevel] [varchar](50) NULL,
	[MailingStatus] [varchar](50) NULL,
	[PendingOp] [varchar](50) NULL,
	[SrvName] [varchar](50) NULL,
	[UserTimeRef] [varchar](50) NULL,
	[UserTimeElab] [varchar](50) NULL,
	[UserRef] [varchar](50) NULL,
	[XrefData] [varchar](50) NULL,
	[ijrar] [varchar](50) NULL,
	[ojrar] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[xreport_dbexport]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[xreport_dbexport](
	[JobReportName] [varchar](50) NULL,
	[JobReportId] [varchar](50) NULL,
	[DateTime] [varchar](50) NULL,
	[LocalFileName] [varchar](64) NULL,
	[LocalPathId_IN] [varchar](50) NULL,
	[LocalPathId_OUT] [varchar](50) NULL,
	[OrigJobReportName] [varchar](50) NULL,
	[TarFileNumberIN] [varchar](50) NULL,
	[TarRecNumberIN] [varchar](50) NULL,
	[TarFileNumberOUT] [varchar](50) NULL,
	[TarRecNumberOUT] [varchar](50) NULL,
	[RemoteHostAddr] [varchar](50) NULL,
	[RemoteFileName] [varchar](50) NULL,
	[JobName] [varchar](50) NULL,
	[JobNumber] [varchar](50) NULL,
	[JobExecutionTime] [varchar](50) NULL,
	[XferStartTime] [varchar](50) NULL,
	[XferEndTime] [varchar](50) NULL,
	[XferRecipient] [varchar](50) NULL,
	[XferMode] [varchar](50) NULL,
	[XferInBytes] [varchar](50) NULL,
	[XferInLines] [varchar](50) NULL,
	[XferInPages] [varchar](50) NULL,
	[XferOutBytes] [varchar](50) NULL,
	[XferDaemon] [varchar](50) NULL,
	[XferId] [varchar](50) NULL,
	[ElabStartTime] [varchar](50) NULL,
	[ElabEndTime] [varchar](50) NULL,
	[ElabOutBytes] [varchar](50) NULL,
	[InputLines] [varchar](50) NULL,
	[InputPages] [varchar](50) NULL,
	[ParsedLines] [varchar](50) NULL,
	[ParsedPages] [varchar](50) NULL,
	[DiscardedLines] [varchar](50) NULL,
	[DiscardedPages] [varchar](50) NULL,
	[HasIndexes] [varchar](50) NULL,
	[isAFP] [varchar](50) NULL,
	[isFullAFP] [varchar](50) NULL,
	[ExistTypeMap] [varchar](50) NULL,
	[MayBeTypeMap] [varchar](50) NULL,
	[FileRangesVar] [varchar](50) NULL,
	[Status] [varchar](50) NULL,
	[ViewLevel] [varchar](50) NULL,
	[MailingStatus] [varchar](50) NULL,
	[PendingOp] [varchar](50) NULL,
	[SrvName] [varchar](50) NULL,
	[UserTimeRef] [varchar](50) NULL,
	[UserTimeElab] [varchar](50) NULL,
	[UserRef] [varchar](50) NULL,
	[XrefData] [varchar](50) NULL,
	[ijrar] [varchar](50) NULL,
	[ojrar] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XferDateDiff]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
/****** Object:  User Defined Function dbo.XferDateDiff    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  User Defined Function dbo.XferDateDiff    Script Date: 2002/07/01 5:48:54 PM ******/

CREATE FUNCTION [dbo].[XferDateDiff]
(
  @XferStartTime datetime 
)
RETURNS int

AS

BEGIN
  RETURN ( DATEDIFF(day, ''2000/1/1'', @XferStartTime) )
END


' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportNameIsOK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
/****** Object:  User Defined Function dbo.ReportNameIsOK    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  User Defined Function dbo.ReportNameIsOK    Script Date: 2002/07/01 5:48:54 PM ******/
CREATE FUNCTION [dbo].[ReportNameIsOK]
(
  @ReportName As varchar(32),
  @RuleFilter As varchar(32)
)
RETURNS varchar(32)

AS

BEGIN
  DECLARE @rc as bit 
  SET @rc = 0
  
  IF @RuleFilter = '''' BEGIN
   SET @rc = 1
  END
  
  ELSE IF @RuleFilter LIKE ''[=]:%'' BEGIN
   IF @ReportName = SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
   SET @rc = 1
  END
  
  ELSE IF @RuleFilter LIKE ''[%]:%'' BEGIN
   IF @ReportName LIKE SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
   SET @rc = 1
  END
  
  ELSE IF @RuleFilter LIKE ''[#]:%'' BEGIN
   IF @ReportName IN (
        SELECT VarValue  FROM vw_VarSetsValues
		WHERE 
		  VarSetName = SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
		  AND VarName = '':REPORTNAME''
      )
   SET @rc = 1
  END

  RETURN @rc
END



' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RecipientNameIsOK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
/****** Object:  User Defined Function dbo.RecipientNameIsOK    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  User Defined Function dbo.RecipientNameIsOK    Script Date: 2002/07/01 5:48:54 PM ******/
CREATE FUNCTION [dbo].[RecipientNameIsOK]
(
  @RecipientName As varchar(32),
  @RuleFilter As varchar(32)
)
RETURNS varchar(32)

AS

BEGIN
  DECLARE @rc as bit 
  SET @rc = 0
  
  IF @RuleFilter = '''' BEGIN
   SET @rc = 1
  END
  
  ELSE IF @RuleFilter LIKE ''[=]:%'' BEGIN
   IF @RecipientName = SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
   SET @rc = 1
  END
  
  ELSE IF @RuleFilter LIKE ''[%]:%'' BEGIN
   IF @RecipientName LIKE SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
   SET @rc = 1
  END
  
  ELSE IF @RuleFilter LIKE ''[#]:%'' BEGIN
   IF @RecipientName IN (
      SELECT VarValue  FROM vw_VarSetsValues
	  WHERE 
		VarSetName = SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
		AND VarName = '':RECIPIENTNAME''
      )
   SET @rc = 1
  END

  RETURN @rc
END



' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FilterValueIsOK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
/****** Object:  User Defined Function dbo.FilterValueIsOK    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  User Defined Function dbo.FilterValueIsOK    Script Date: 2002/07/01 5:48:54 PM ******/
CREATE FUNCTION [dbo].[FilterValueIsOK]
(
  @FilterValue As varchar(32),
  @RuleFilterVar AS varchar(32),
  @RuleFilter As varchar(32)
)
RETURNS varchar(32)

AS

BEGIN
  DECLARE @rc as bit 
  SET @rc = 0
  
  IF @RuleFilter = '''' BEGIN
   SET @rc = 1
  END
  
  ELSE IF @RuleFilter LIKE ''[=]:%'' BEGIN
   IF @FilterValue = SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
   SET @rc = 1
  END
  
  ELSE IF @RuleFilter LIKE ''[%]:%'' BEGIN
   IF @FilterValue LIKE SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
   SET @rc = 1
  END
  
  ELSE IF @RuleFilter LIKE ''[#]:%'' BEGIN
   IF @FilterValue IN (
        SELECT VarValue  FROM vw_VarSetsValues
		WHERE 
		  VarSetName = SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
		  AND VarName = @RuleFilterVar
      )
   SET @rc = 1
  END

  RETURN @rc
END



' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRReleaseAppLock]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRReleaseAppLock    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE PROCEDURE [dbo].[XRReleaseAppLock]
(
  @Resource As nvarchar(255),
  @LockOwner As nvarchar(32)
)

AS

BEGIN
  DECLARE @Result int
  
  EXEC @Result = sp_releaseapplock 
   @Resource = @Resource, 
   @LockOwner = @LockOwner

  SELECT @Result
END

' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRVerifyReportNames]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRVerifyReportNames    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE PROCEDURE [dbo].[XRVerifyReportNames]
 AS
UPDATE tbl_ReportNames
SET
  TotReports = ( 
    SELECT COUNT(*) FROM tbl_LogicalReports
    WHERE 
      ReportName = tbl_ReportNames.ReportName
  ) ,
  TotPages = CASE
    WHEN EXISTS(
	  SELECT ReportName 
	  from tbl_LogicalReports 
	  WHERE ReportName = tbl_ReportNames.ReportName 
	) 
	THEN
	(
      SELECT SUM(b.TotPages) from tbl_LogicalReports a, tbl_PhysicalReports b
      WHERE 
        a.ReportName = tbl_ReportNames.ReportName
	    AND b.JobReportId = a.JobReportId AND b.ReportId = a.ReportId
	)
	ELSE 
	(
	  0
	)
  END

' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRVerifyJobReportNames]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRVerifyJobReportNames    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE PROCEDURE [dbo].[XRVerifyJobReportNames]
 AS
UPDATE tbl_JobReportNames
SET
  TotJobReports = ( 
    SELECT COUNT(*) FROM tbl_JobReports
    WHERE 
      JobReportName = tbl_JobReportNames.JobReportName
  ) ,
  TotPages = CASE 
    WHEN EXISTS(
	  SELECT JobReportName FROM tbl_JobReports 
	  where 
	    JobReportName = tbl_JobReportNames.JobReportName
	    AND InputPages IS NOT NULL
	) THEN
	(
      SELECT SUM(InputPages) FROM tbl_JobReports
      WHERE 
        JobReportName = tbl_JobReportNames.JobReportName
	    AND InputPages IS NOT NULL
	)
	ELSE
	(
	  0
	)
  END

' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRUserFoldersTrees]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRUserFoldersTrees    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE PROCEDURE [dbo].[XRUserFoldersTrees] 
(
 @UserName varchar(32)
)
 
AS
 
SELECT DISTINCT FolderName, FolderDescr, IsActive, 
  HasChilds = CASE
    WHEN EXISTS(
      SELECT b.FolderName FROM tbl_Folders b WHERE b.ParentFolder = a.FolderName 
    ) 
    THEN 1 ELSE 0
  END
  ,
  HasReports = CASE
    WHEN EXISTS(
      SELECT b.FolderName FROM tbl_FoldersReportNames_LL1 b WHERE b.FolderName = a.FolderName
    )
    THEN 1 ELSE 0
  END
FROM tbl_Folders a

WHERE FolderName IN (
  SELECT RootNode From tbl_ProfilesFoldersTrees
  WHERE ProfileName IN (
    SELECT ProfileName FROM tbl_UsersProfiles
    WHERE UserName = @UserName
  )
)

ORDER BY FolderName
' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRReportNameJobReports_HIST]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRReportNameJobReports_HIST    Script Date: 9/8/2004 3:27:20 PM ******/

CREATE PROCEDURE [dbo].[XRReportNameJobReports_HIST]
  @FolderName varchar(32),
  @ReportName varchar(32)

 AS
SELECT
  d.JobReportId, MIN(c.ReportId) As ReportId,
  CONVERT(varchar, d.XferStartTime, 120) As TimeRef,
  CONVERT(varchar, d.UserTimeRef  , 103) As UserTimeRef, 
  CONVERT(varchar, d.UserTimeElab , 103) As UserTimeElab,
  (
    CONVERT(varchar, d.XferStartTime, 103) + 
    SUBSTRING(CONVERT(varchar, d.XferStartTime, 121),11,6)
  )
  As XferStartTime,
  d.JobName, d.JobNumber, d.UserRef, 
  COUNT(*) As TotReports, SUM(c.TotPages) As TotPages

FROM 
  tbl_Folders a
  
  INNER JOIN tbl_FoldersReportNames_LL1 b 
   ON b.FolderName = a.FolderName 
	
  INNER JOIN tbl_LogicalReports_HIST c 
   ON c.ReportName = b.ReportName
   AND c.FilterValue = b.FilterValue
   AND c.XferRecipient = b.RecipientName
	
  INNER JOIN tbl_JobReports_HIST d 
   ON d.JobReportId = c.JobReportid
  
WHERE 
  a.FolderName = @FolderName
  AND b.ReportName = @ReportName

GROUP BY
  d.JobReportId, d.XferStartTime, d.UserTimeRef, d.UserTimeElab, d.JobName, d.JobNumber, d.UserRef
  
ORDER BY TimeRef DESC
' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRReportNameJobReports]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRReportNameJobReports    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE PROCEDURE [dbo].[XRReportNameJobReports]
  @FolderName as  varchar(32),
  @ReportName as  varchar(32),
  @UserName   as  varchar(32)

 AS
SELECT
  d.JobReportId, MIN(c.ReportId) As ReportId,
  CONVERT(varchar, d.XferStartTime, 120) As TimeRef,
  CONVERT(varchar, d.UserTimeRef  , 103) As UserTimeRef, 
  CONVERT(varchar, d.UserTimeElab , 103) As UserTimeElab,
  (
    CONVERT(varchar, d.XferStartTime, 103) + 
    SUBSTRING(CONVERT(varchar, d.XferStartTime, 121),11,6)
  )
  As XferStartTime,
  COUNT(*) As TotReports, SUM(c.TotPages) As TotPages,
  d.JobName, d.JobNumber, d.UserRef, d.FileRangesVar, d.ExistTypeMap,

  CASE
    WHEN NOT EXISTS (
      SELECT ReportName FROM tbl_ProfilesReportNames_LL1
      WHERE ReportName = @ReportName
    )
    THEN 1

    WHEN EXISTS (
      SELECT ReportName FROM tbl_ProfilesReportNames_LL1
      WHERE ReportName = @ReportName
      AND ProfileName IN (
        SELECT ProfileName FROM tbl_UsersProfiles
        WHERE UserName = @UserName
      )
    )
    THEN 1

    ELSE 0
  END

  As isGRANTED

FROM 
  tbl_Folders a
  
  INNER JOIN tbl_FoldersReportNames_LL1 b 
   ON b.FolderName = a.FolderName 
	
  INNER JOIN tbl_LogicalReports c 
   ON c.ReportName = b.ReportName
   AND c.FilterValue = b.FilterValue
   AND c.XferRecipient = b.RecipientName
	
  INNER JOIN tbl_JobReports d 
   ON d.JobReportId = c.JobReportid
  
WHERE 
  a.FolderName = @FolderName
  AND b.ReportName = @ReportName

GROUP BY
  d.JobReportId, d.XferStartTime, d.UserTimeRef, d.UserTimeElab, d.JobName, d.JobNumber, d.UserRef, d.FileRangesVar, d.ExistTypeMap
  
ORDER BY TimeRef DESC
' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRJobReportReports]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRJobReportReports    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE PROCEDURE [dbo].[XRJobReportReports]
  @FolderName  as varchar(32),
  @ReportName  as varchar(32),
  @JobReportId as int,
  @UserName    as varchar(32) = ''''

 AS

SELECT
  @JobReportid As JobReportId, c.ReportId, c.FilterValue, d.FilterVar, c.TotPages,

  CASE
    WHEN NOT EXISTS (
      SELECT ReportName FROM tbl_ProfilesReportNames_LL1
      WHERE ReportName = @ReportName
    )
    THEN 1

    WHEN EXISTS (
      SELECT ReportName FROM tbl_ProfilesReportNames_LL1
      WHERE ReportName = @ReportName AND FilterValue = c.FilterValue
      AND ProfileName IN (
        SELECT ProfileName FROM tbl_UsersProfiles
        WHERE UserName = @UserName
      )
    )
    THEN 1

    ELSE 0
  END

  As isGRANTED

FROM 
  tbl_Folders a

  INNER JOIN tbl_FoldersReportNames_LL1 b 
   ON b.FolderName = a.FolderName 
  
  INNER JOIN tbl_LogicalReports c 
   ON c.ReportName = b.ReportName
   AND c.FilterValue = b.FilterValue
   AND c.XferRecipient = b.RecipientName

  INNER JOIN tbl_ReportNames d
   ON d.ReportName = c.ReportName

WHERE 
  a.FolderName = @FolderName
  AND b.ReportName = @ReportName
  AND c.JobReportId = @JobReportId

ORDER BY c.FilterValue
' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRJobReportFileRanges]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRJobReportFileRanges    Script Date: 9/8/2004 3:27:20 PM ******/
/****** Object:  Stored Procedure dbo.XRJobReportFileRanges    Script Date: 2002/07/01 5:48:53 PM ******/

CREATE PROCEDURE [dbo].[XRJobReportFileRanges]
  @FolderName varchar(32),
  @ReportName varchar(32),
  @JobReportId int

 AS

SELECT
  @JobReportid As JobReportId, e.FileId, d.FileRangesVar, e.FromValue, e.ToValue, e.TotPages

FROM 
  tbl_Folders a

  INNER JOIN tbl_FoldersReportNames_LL1 b 
   ON b.FolderName = a.FolderName 
  
  INNER JOIN tbl_LogicalReports c 
   ON c.ReportName = b.ReportName
   AND c.FilterValue = b.FilterValue
   AND c.XferRecipient = b.RecipientName

  INNER JOIN tbl_JobReports d
   ON d.JobReportId = c.JobReportId

  INNER JOIN tbl_JobReportsFileRanges e
   ON e.JobReportId = d.JobReportId

WHERE 
  b.FilterValue = ''''
  AND a.FolderName = @FolderName
  AND b.ReportName = @ReportName 
  AND c.JobReportId = @JobReportId

ORDER BY e.FileId
' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRGetReport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRGetReport    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  Stored Procedure dbo.XRGetReport    Script Date: 2002/07/01 5:48:53 PM ******/

CREATE PROCEDURE [dbo].[XRGetReport] 
 @JobReportId int,
 @ReportId int,
 @UserName as varchar(16)

 AS
 
SELECT a.JobReportId, a.ReportId, b.JobReportName, b.LocalPathId_OUT, b.LocalFileName
from 
  tbl_LogicalReports a 
  INNER JOIN tbl_JobReports b ON a.JobReportId = b.JobReportId
WHERE
  a.JobReportId = @JobReportId 
  AND a.ReportId = @ReportId

' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_Reports]'))
EXEC dbo.sp_executesql @statement = N'
/****** Object:  View dbo.vw_Reports    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE VIEW [dbo].[vw_Reports]
AS
SELECT     dbo.tbl_LogicalReports.JobReportId, dbo.tbl_LogicalReports.ReportId, dbo.tbl_LogicalReports.ReportName, dbo.tbl_LogicalReports.FilterValue, 
                      dbo.tbl_LogicalReports.TotPages, dbo.tbl_PhysicalReports.ListOfPages
FROM         dbo.tbl_LogicalReports INNER JOIN
                      dbo.tbl_PhysicalReports ON dbo.tbl_LogicalReports.JobReportId = dbo.tbl_PhysicalReports.JobReportId AND 
                      dbo.tbl_LogicalReports.ReportId = dbo.tbl_PhysicalReports.ReportId

'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_LogicaReportsCounts]'))
EXEC dbo.sp_executesql @statement = N'
/****** Object:  View dbo.vw_LogicaReportsCounts    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  View dbo.vw_LogicalReportsCounts    Script Date: 2002/07/01 5:48:52 PM ******/
CREATE VIEW [dbo].[vw_LogicaReportsCounts]
AS
SELECT     ReportName, FilterValue, XferRecipient, COUNT(*) AS TotReports, SUM(TotPages) AS TotPages
FROM         dbo.tbl_LogicalReports
GROUP BY ReportName, FilterValue, XferRecipient


'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_JobReports]'))
EXEC dbo.sp_executesql @statement = N'
/****** Object:  View dbo.vw_JobReports    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE VIEW [dbo].[vw_JobReports]
AS
SELECT     dbo.tbl_JobReports.*
FROM         dbo.tbl_JobReportsTarFiles RIGHT OUTER JOIN
                      dbo.tbl_JobReports ON dbo.tbl_JobReportsTarFiles.JobReportId = dbo.tbl_JobReports.JobReportId

'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_JobReportNamesQrexp]'))
EXEC dbo.sp_executesql @statement = N'
/****** Object:  View dbo.vw_JobReportNamesQrexp    Script Date: 9/8/2004 3:27:20 PM ******/
/****** Object:  View dbo.vw_JobReportNamesQre    Script Date: 2002/07/01 5:48:53 PM *****
*/
CREATE VIEW [dbo].[vw_JobReportNamesQrexp]
AS
SELECT     a.JobReportName AS fmJobReportName, b.TestOrder, b.JobName, b.Recipient, b.RemoteHostAddr, b.RemoteFileName, b.PageText, b.MaxPageTests, 
                      b.toJobReportName
FROM         dbo.tbl_JobReportNamesQaliases a INNER JOIN
                      dbo.tbl_JobReportNamesQrexp b ON a.AliasOf = b.fmJobReportName

'
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRFolderReportNames_HIST]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRFolderReportNames_HIST    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  Stored Procedure dbo.XRFolderReportNames_HIST    Script Date: 2002/07/01 5:48:53 PM ******/

CREATE PROCEDURE [dbo].[XRFolderReportNames_HIST]
  @FolderName varchar(32)
AS
 
SELECT DISTINCT c.ReportName, c.ReportDescr

FROM 
  tbl_Folders a
  INNER JOIN tbl_FoldersReportNames_LL1 b ON a.FolderName = b.FolderName 
  INNER JOIN tbl_ReportNames c ON b.ReportName = c.ReportName
  
WHERE 
  a.FolderName = @FolderName
  AND EXISTS (
    SELECT JobReportId FROM tbl_LogicalReports_HIST j
	WHERE 
	 j.ReportName = b.ReportName
	 AND j.FilterValue = b.FilterValue
	 AND j.XferRecipient = b.RecipientName
  )
    
ORDER BY c.ReportName

' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRFolderReportNames]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRFolderReportNames    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE PROCEDURE [dbo].[XRFolderReportNames]
  @FolderName As varchar(32),
  @UserName   As varchar(32)
AS
 
SELECT DISTINCT c.ReportName, c.ReportDescr, c.HasIndex,

  CASE
    WHEN NOT EXISTS (
      SELECT ReportName FROM tbl_ProfilesReportNames_LL1
      WHERE ReportName = b.ReportName
    )
    THEN 1

    WHEN EXISTS (
      SELECT ReportName FROM tbl_ProfilesReportNames_LL1
      WHERE ReportName = b.ReportName
      AND ProfileName IN (
        SELECT ProfileName FROM tbl_UsersProfiles
        WHERE UserName = @UserName
      )
    )
    THEN 1

    ELSE 0
  END

  As isGRANTED


FROM 
  tbl_Folders a
  INNER JOIN tbl_FoldersReportNames_LL1 b ON a.FolderName = b.FolderName 
  INNER JOIN tbl_ReportNames c ON b.ReportName = c.ReportName

    
WHERE 
  a.FolderName = @FolderName
 
  AND EXISTS (
    SELECT JobReportId FROM tbl_LogicalReports j
	WHERE 
	 j.ReportName = b.ReportName
	 AND j.FilterValue = b.FilterValue
	 AND j.XferRecipient = b.RecipientName
  )
    
ORDER BY c.ReportName
' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRFolderJobReportsOfDate_HIST]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRFolderJobReportsOfDate_HIST    Script Date: 9/8/2004 3:27:20 PM ******/

CREATE PROCEDURE [dbo].[XRFolderJobReportsOfDate_HIST]
  @FolderName as varchar(32),
  @UserTimeRef as datetime,
  @ReportName as varchar(32) = ''%''

 AS
BEGIN

DECLARE @XferDateDiff  smallint
SET @XferDateDiff = DATEDIFF(day, ''2000/1/1'',  @UserTimeRef)
  
SELECT 
  c.JobReportId, c.ReportId, e.JobName, e.JobNumber, e.UserRef,
  CONVERT(varchar, e.XferStartTime, 120) TimeRef,
  CONVERT(varchar, e.UserTimeRef  , 103) UserTimeRef, 
  CONVERT(varchar, e.UserTimeElab , 103) UserTimeElab,
  (
    CONVERT(varchar, e.XferStartTime, 103) + 
    SUBSTRING(CONVERT(varchar, e.XferStartTime, 121),11,6)
  )
  As XferStartTime,
  d.ReportName, d.ReportDescr,
  COUNT(*) As TotReports, SUM(c.TotPages) As TotPages

FROM 
  tbl_Folders a
  
  INNER JOIN tbl_FoldersReportNames_LL1 b 
    ON a.FolderName = b.FolderName
	
  INNER JOIN tbl_LogicalReports_HIST c 
    ON b.ReportName = c.ReportName 
	AND b.FilterValue = c.FilterValue 
	AND b.RecipientName = c.XferRecipient

  INNER JOIN tbl_ReportNames d
    ON d.ReportName = c.ReportName

  INNER JOIN tbl_JobReports_HIST e 
    ON e.JobReportId = c.JobReportId
  
WHERE 
  a.FolderName = @FolderName
  AND c.XferDateDiff = @XferDateDiff
 

GROUP BY
  c.JobReportId, c.ReportId, e.JobName, e.JobNumber, e.UserRef, 
  e.UserTimeRef, e.UserTimeElab, e.XferStartTime,
  d.ReportName, d.ReportDescr

ORDER BY d.ReportName, TimeRef DESC

END
' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRFolderJobReportsOfDate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRFolderJobReportsOfDate    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE PROCEDURE [dbo].[XRFolderJobReportsOfDate]
  @FolderName  as  varchar(32),
  @UserTimeRef as  datetime,
  @ReportName  as  varchar(32) = ''%'',
  @UserName    as  varchar(32) = ''''
 AS
BEGIN

DECLARE @XferDateDiff  smallint
SET @XferDateDiff = DATEDIFF(day, ''2000/1/1'',  @UserTimeRef)
  
SELECT 
  c.JobReportId, c.ReportId, e.JobName, e.JobNumber, 
  CONVERT(varchar, e.XferStartTime, 120) TimeRef,
  CONVERT(varchar, e.UserTimeRef  , 103) UserTimeRef, 
  CONVERT(varchar, e.UserTimeElab , 103) UserTimeElab,
  (
    CONVERT(varchar, e.XferStartTime, 103) + 
    SUBSTRING(CONVERT(varchar, e.XferStartTime, 121),11,6)
  )
  As XferStartTime,
  COUNT(*) As TotReports, SUM(c.TotPages) As TotPages,
  d.ReportName, d.ReportDescr, e.UserRef, e.FileRangesVar, e.ExistTypeMap,

  CASE
    WHEN NOT EXISTS (
      SELECT ReportName FROM tbl_ProfilesReportNames_LL1
      WHERE ReportName = d.ReportName
    )
    THEN 1

    WHEN EXISTS (
      SELECT ReportName FROM tbl_ProfilesReportNames_LL1
      WHERE ReportName = d.ReportName
      AND ProfileName IN (
        SELECT ProfileName FROM tbl_UsersProfiles
        WHERE UserName = @UserName
      )
    )
    THEN 1

    WHEN EXISTS (
      SELECT ProfileName FROM tbl_Profiles
      WHERE LookAtEverything = 1
      AND ProfileName IN (
        SELECT ProfileName FROM tbl_UsersProfiles
        WHERE UserName = @UserName
      )
    )
    THEN 1

    ELSE 0
  END

  As isGRANTED

FROM 
  tbl_Folders a
  
  INNER JOIN tbl_FoldersReportNames_LL1 b 
    ON a.FolderName = b.FolderName
	
  INNER JOIN tbl_LogicalReports c 
    ON b.ReportName = c.ReportName 
	AND b.FilterValue = c.FilterValue 
	AND b.RecipientName = c.XferRecipient

  INNER JOIN tbl_ReportNames d
    ON d.ReportName = c.ReportName

  INNER JOIN tbl_JobReports e 
    ON e.JobReportId = c.JobReportId
  
WHERE 
  a.FolderName = @FolderName
  AND c.XferDateDiff = @XferDateDiff
 

GROUP BY
  d.ReportName, d.ReportDescr, c.JobReportId, c.ReportId, e.XferStartTime, e.UserTimeRef, 
  e.UserTimeElab, e.JobName, e.JobNumber,  e.UserRef, e.FileRangesVar,
  e.ExistTypeMap

ORDER BY d.ReportName, TimeRef DESC

END
' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRFolderChildFolders]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRFolderChildFolders    Script Date: 9/8/2004 3:27:20 PM ******/

CREATE PROCEDURE [dbo].[XRFolderChildFolders]
  @FolderName As varchar(32),
  @UserName  As varchar(32)

AS

SELECT 
  FolderName, FolderDescr, IsActive, 
  HasChilds = CASE
    WHEN EXISTS(
      SELECT b.FolderName FROM tbl_Folders b WHERE b.ParentFolder = a.FolderName 
    ) 
    THEN 1 ELSE 0
  END
  ,
  HasReports = CASE
    WHEN EXISTS(
      SELECT b.FolderName FROM tbl_FoldersReportNames_LL1 b WHERE b.FolderName = a.FolderName
    )
    THEN 1 ELSE 0
  END
 
FROM tbl_Folders a
WHERE ParentFolder = @FolderName
' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_VerifyReportNames]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.sp_VerifyReportNames    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  Stored Procedure dbo.XRVerifyReportNames    Script Date: 2002/07/01 5:48:53 PM ******/
CREATE PROCEDURE [dbo].[sp_VerifyReportNames]
 AS
UPDATE tbl_ReportNames
SET
  TotReports = ( 
    SELECT COUNT(*) FROM tbl_LogicalReports
    WHERE 
      ReportName = tbl_ReportNames.ReportName
  ) ,
  TotPages = CASE
    WHEN EXISTS(
	  SELECT ReportName 
	  from tbl_Logicalreports 
	  WHERE ReportName = tbl_ReportNames.ReportName 
	) 
	THEN
	(
      SELECT SUM(b.TotPages) from tbl_LogicalReports a, tbl_PhysicalReports b
      WHERE 
        a.ReportName = tbl_ReportNames.ReportName
	    AND b.JobReportId = a.JobReportId AND b.ReportId = a.ReportId
	)
	ELSE 
	(
	  0
	)
  END


' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_VerifyJobReportNames]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.sp_VerifyJobReportNames    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  Stored Procedure dbo.XRVerifyJobReportNames    Script Date: 2002/07/01 5:48:54 PM ******/
CREATE PROCEDURE [dbo].[sp_VerifyJobReportNames]
 AS
UPDATE tbl_JobReportNames
SET
  TotJobReports = ( 
    SELECT COUNT(*) FROM tbl_JobReports
    WHERE 
      JobReportName = tbl_JobReportNames.JobReportName
  ) ,
  TotPages = CASE 
    WHEN EXISTS(
	  SELECT JobReportName FROM tbl_JobReports 
	  where 
	    JobReportName = tbl_JobReportNames.JobReportName
	    AND InputPages IS NOT NULL
	) THEN
	(
      SELECT SUM(InputPages) FROM tbl_JobReports
      WHERE 
        JobReportName = tbl_JobReportNames.JobReportName
	    AND InputPages IS NOT NULL
	)
	ELSE
	(
	  0
	)
  END


' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames]'))
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[tbl_FoldersReportNames]
AS
SELECT     dbo.tbl_FoldersRules.FolderName, dbo.tbl_FoldersRules.FORBID, dbo.tbl_NamedReportsGroups.ReportRule, dbo.tbl_NamedReportsGroups.FilterVar, 
                      dbo.tbl_NamedReportsGroups.FilterRule, dbo.tbl_NamedReportsGroups.RecipientRule, dbo.tbl_FoldersRules.RuleId
FROM         dbo.tbl_FoldersRules INNER JOIN
                      dbo.tbl_NamedReportsGroups ON dbo.tbl_FoldersRules.ReportGroupId = dbo.tbl_NamedReportsGroups.ReportGroupId

'
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportGroups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_FoldersReportGroups](
	[FolderName] [varchar](32) NOT NULL,
	[ReportGroupId] [int] NOT NULL,
	[FORBID] [bit] NOT NULL,
	[RuleId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_tbl_FoldersReportGroups] PRIMARY KEY CLUSTERED 
(
	[FolderName] ASC,
	[ReportGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_VerifyReportNames]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.ce_VerifyReportNames    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE PROCEDURE [dbo].[ce_VerifyReportNames]
 AS
UPDATE tbl_ReportNames
SET
  TotReports = ( 
    SELECT COUNT(*) FROM tbl_LogicalReports
    WHERE 
      ReportName = tbl_ReportNames.ReportName
  ) ,
  TotPages = CASE
    WHEN EXISTS(
	  SELECT ReportName 
	  from tbl_LogicalReports 
	  WHERE ReportName = tbl_ReportNames.ReportName 
	) 
	THEN
	(
      SELECT SUM(b.TotPages) from tbl_LogicalReports a, tbl_PhysicalReports b
      WHERE 
        a.ReportName = tbl_ReportNames.ReportName
	    AND b.JobReportId = a.JobReportId AND b.ReportId = a.ReportId
	)
	ELSE 
	(
	  0
	)
  END

' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_UserFoldersTrees]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.ce_UserFoldersTrees    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE PROCEDURE [dbo].[ce_UserFoldersTrees] 
(
 @UserName varchar(32)
)
 
AS
 
SELECT DISTINCT FolderName, FolderDescr, IsActive, 
  HasChilds = CASE
    WHEN EXISTS(
      SELECT b.FolderName FROM tbl_Folders b WHERE b.ParentFolder = a.FolderName 
    ) 
    THEN 1 ELSE 0
  END
  ,
  HasReports = CASE
    WHEN EXISTS(
      SELECT b.FolderName FROM tbl_FoldersReportNames_LL1 b WHERE b.FolderName = a.FolderName
    )
    THEN 1 ELSE 0
  END
FROM tbl_Folders a

WHERE FolderName IN (
  SELECT RootNode From tbl_ProfilesFoldersTrees
  WHERE ProfileName IN (
    SELECT ProfileName FROM tbl_UsersProfiles
    WHERE UserName = @UserName
  )
)

ORDER BY FolderName
' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_ReportNameJobReports_HIST]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.ce_ReportNameJobReports_HIST    Script Date: 9/8/2004 3:27:20 PM ******/

CREATE PROCEDURE [dbo].[ce_ReportNameJobReports_HIST]
  @FolderName varchar(32),
  @ReportName varchar(32)

 AS
SELECT
  d.JobReportId, MIN(c.ReportId) As ReportId,
  CONVERT(varchar, d.XferStartTime, 120) As TimeRef,
  CONVERT(varchar, d.UserTimeRef  , 103) As UserTimeRef, 
  CONVERT(varchar, d.UserTimeElab , 103) As UserTimeElab,
  (
    CONVERT(varchar, d.XferStartTime, 103) + 
    SUBSTRING(CONVERT(varchar, d.XferStartTime, 121),11,6)
  )
  As XferStartTime,
  d.JobName, d.JobNumber, d.UserRef, 
  COUNT(*) As TotReports, SUM(c.TotPages) As TotPages

FROM 
  tbl_Folders a
  
  INNER JOIN tbl_FoldersReportNames_LL1 b 
   ON b.FolderName = a.FolderName 
	
  INNER JOIN tbl_LogicalReports_HIST c 
   ON c.ReportName = b.ReportName
   AND c.FilterValue = b.FilterValue
   AND c.XferRecipient = b.RecipientName
	
  INNER JOIN tbl_JobReports_HIST d 
   ON d.JobReportId = c.JobReportid
  
WHERE 
  a.FolderName = @FolderName
  AND b.ReportName = @ReportName

GROUP BY
  d.JobReportId, d.XferStartTime, d.UserTimeRef, d.UserTimeElab, d.JobName, d.JobNumber, d.UserRef
  
ORDER BY TimeRef DESC
' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ftb_FiltOfReportNames]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
/****** Object:  User Defined Function dbo.ftb_FiltOfReportNames    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  User Defined Function dbo.ftb_FiltOfReportNames    Script Date: 2002/07/01 5:48:54 PM ******/

CREATE FUNCTION [dbo].[ftb_FiltOfReportNames]
(
  @RuleFilter As varchar(32),
  @ReportFilterVar As varchar(32)
)

RETURNS @ReportNamesList TABLE
(
  ReportName varchar(32)
)

AS

BEGIN
  IF @RuleFilter LIKE ''[=]:%'' BEGIN
    INSERT @ReportNamesList
      SELECT ReportName from tbl_ReportNamesKK
	  WHERE
	    FilterVar = @ReportFilterVar
        AND ReportName = SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
    RETURN 
  END
  
  ELSE IF @RuleFilter LIKE ''[%]:%'' BEGIN
    INSERT @ReportNamesList
      SELECT ReportName from tbl_ReportNamesLL
      WHERE 
	    FilterVar = @ReportFilterVar
	    AND ReportName LIKE SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
    RETURN 
  END
  
  ELSE IF @RuleFilter LIKE ''[#]:%'' BEGIN
    INSERT @ReportNamesList
	  SELECT ReportName from tbl_ReportNames
      WHERE 
	    FilterVar = @ReportFilterVar
		AND ReportName IN (
          SELECT VarValue  FROM tbl_VarSetsValues
	      WHERE 
	        VarSetName = SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
	        AND VarName = '':REPORTNAME''
	    )
    RETURN 
  END

  RETURN
END






' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ftb_FiltOfRecipientNames]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
/****** Object:  User Defined Function dbo.ftb_FiltOfRecipientNames    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  User Defined Function dbo.ftb_FiltOfRecipientNames    Script Date: 2002/07/01 5:48:54 PM ******/

CREATE FUNCTION [dbo].[ftb_FiltOfRecipientNames]
(
  @RuleFilter As varchar(32)
)

RETURNS @RecipientNamesList TABLE
(
  RecipientName    varchar(32)
)

AS

BEGIN
  IF @RuleFilter = '''' BEGIN
    INSERT @RecipientNamesList
	  SELECT ''''
	RETURN
  END
  
  ELSE IF @RuleFilter LIKE ''[=]:%'' BEGIN
    INSERT @RecipientNamesList
      SELECT RecipientName from tbl_RecipientNames
      WHERE RecipientName = SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
    RETURN 
  END
  
  ELSE IF @RuleFilter LIKE ''[%]:%'' BEGIN
    INSERT @RecipientNamesList
      SELECT RecipientName from tbl_RecipientNames
      WHERE RecipientName LIKE SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
    RETURN 
  END
  
  ELSE IF @RuleFilter LIKE ''[#]:%'' BEGIN
    INSERT @RecipientNamesList
      SELECT VarValue  FROM tbl_VarSetsValues
	  WHERE 
	    VarSetName = SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
		AND VarName = '':RECIPIENTNAME''
    RETURN 
  END

  RETURN
END




' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ftb_FiltOfFilterVarValues]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
/****** Object:  User Defined Function dbo.ftb_FiltOfFilterVarValues    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  User Defined Function dbo.ftb_FiltOfFilterVarValues    Script Date: 2002/07/01 5:48:54 PM ******/

CREATE FUNCTION [dbo].[ftb_FiltOfFilterVarValues]
(
  @RuleFilterVar As varchar(32),
  @RuleFilter As varchar(32)
)

RETURNS @FilterValuesList TABLE
(
  FilterValue varchar(32)
)

AS

BEGIN
  
  IF @RuleFilterVar = '''' BEGIN
    INSERT @FilterValuesList 
	  SELECT ''''
    RETURN 
  END
  
  IF @RuleFilter = '''' BEGIN
    INSERT @FilterValuesList
      SELECT FilterValue from tbl_FilterVarValues 
	  WHERE 
	    FilterVar = @RuleFilterVar
    RETURN 
  END
  
  ELSE IF @RuleFilter LIKE ''[=]:%'' BEGIN
    INSERT @FilterValuesList
      SELECT SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
    RETURN 
  END
  
  ELSE IF @RuleFilter LIKE ''[%]:%'' BEGIN
    INSERT @FilterValuesList
      SELECT FilterValue from tbl_FilterVarValues 
	  WHERE 
	    FilterVar = @RuleFilterVar
        AND FilterValue LIKE SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
    RETURN 
  END
  
  ELSE IF @RuleFilter LIKE ''[#]:%'' BEGIN
    INSERT @FilterValuesList
      SELECT VarValue  FROM tbl_VarSetsValues
	  WHERE 
	    VarSetName = SUBSTRING(@RuleFilter,3,len(@RuleFilter)-2)
	    AND VarName = @RuleFilterVar
    RETURN 
  END

  RETURN
END



' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNamesXX]'))
EXEC dbo.sp_executesql @statement = N'
/****** Object:  View dbo.tbl_FoldersReportNames    Script Date: 04/06/2006 22.57.53 ******/
CREATE VIEW [dbo].[tbl_FoldersReportNamesXX]
AS
SELECT     dbo.tbl_FoldersReportGroups.FolderName, dbo.tbl_ReportsGroups.ReportRule, dbo.tbl_ReportsGroups.FilterVar, dbo.tbl_ReportsGroups.FilterRule, 
                      dbo.tbl_ReportsGroups.RecipientRule, dbo.tbl_FoldersReportGroups.FORBID, dbo.tbl_FoldersReportGroups.RuleId
FROM         dbo.tbl_FoldersReportGroups INNER JOIN
                      dbo.tbl_ReportsGroups ON dbo.tbl_FoldersReportGroups.ReportGroupId = dbo.tbl_ReportsGroups.ReportGroupId

'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNamesOLD]'))
EXEC dbo.sp_executesql @statement = N'

/****** Object:  View dbo.tbl_FoldersReportNamesOLD    Script Date: 04/06/2006 22.57.53 ******/
CREATE VIEW [dbo].[tbl_FoldersReportNamesOLD]
AS
SELECT     dbo.tbl_FoldersReportGroups.FolderName, dbo.tbl_ReportsGroups.ReportRule, dbo.tbl_ReportsGroups.FilterVar, dbo.tbl_ReportsGroups.FilterRule, 
                      dbo.tbl_ReportsGroups.RecipientRule, dbo.tbl_FoldersReportGroups.FORBID, dbo.tbl_FoldersReportGroups.RuleId
FROM         dbo.tbl_FoldersReportGroups INNER JOIN
                      dbo.tbl_ReportsGroups ON dbo.tbl_FoldersReportGroups.ReportGroupId = dbo.tbl_ReportsGroups.ReportGroupId


'
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ftb_ExpFoldersReportNames]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
/****** Object:  User Defined Function dbo.ftb_ExpFoldersReportNames    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  User Defined Function dbo.ftb_ExpFoldersReportNames    Script Date: 2002/07/01 5:48:54 PM ******/
CREATE FUNCTION [dbo].[ftb_ExpFoldersReportNames]
(
  @ReportRule varchar(32),
  @FilterVar varchar(32),
  @FilterRule varchar(32), 
  @RecipientRule varchar(32)
)

RETURNS @FoldersReportNames_LL1 TABLE
(
  ReportName varchar(32),
  FilterValue varchar(32),
  RecipientName varchar(32)
)

AS

BEGIN

  INSERT @FoldersReportNames_LL1
    SELECT  a.ReportName, b.FilterValue, c.RecipientName
  
    FROM  
      ftb_FiltOfReportNames(@ReportRule, @FilterVar) a 
      CROSS JOIN ftb_FiltOfFilterVarValues(@FilterVar, @FilterRule) b 
      CROSS JOIN ftb_FiltOfRecipientNames(@RecipientRule) c	  
  RETURN

END


' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_ReportNameJobReports]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ce_ReportNameJobReports]
  @FolderName as  varchar(32),
  @ReportName as  varchar(32),
  @UserName   as  varchar(32)

 AS
SELECT
  d.JobReportId, MIN(c.ReportId) As ReportId,
  CONVERT(varchar, d.XferStartTime, 120) As TimeRef,
  CONVERT(varchar, d.UserTimeRef  , 103) As UserTimeRef, 
  CONVERT(varchar, d.UserTimeElab , 103) As UserTimeElab,
  (
    CONVERT(varchar, d.XferStartTime, 103) + 
    SUBSTRING(CONVERT(varchar, d.XferStartTime, 121),11,6)
  )
  As XferStartTime,
  COUNT(*) As TotReports, SUM(c.TotPages) As TotPages,
  d.JobName, d.JobNumber, d.UserRef, d.FileRangesVar, d.ExistTypeMap, 1 As IsGRANTED

FROM 
  tbl_Folders a
  
  INNER JOIN tbl_FoldersReportNames b 
   ON b.FolderName = a.FolderName 

  INNER JOIN tbl_VarSetsValues v
   ON v.VarSetName = b.ReportRule
	
  INNER JOIN tbl_LogicalReports c 
   ON c.ReportName = v.VarValue
	
  INNER JOIN tbl_JobReports d 
   ON d.JobReportId = c.JobReportid
  
WHERE 
  a.FolderName = @FolderName
  AND c.ReportName = @ReportName
  AND v.VarName = '':REPORTNAME''
  AND (
     b.FilterVar = ''''
     OR c.FilterValue IN (
       SELECT VarValue FROM tbl_VarSetsValues
       WHERE VarsetName = b.FilterRule AND VarName = b.FilterVar
     )
  )

GROUP BY
  d.JobReportId, d.XferStartTime, d.UserTimeRef, d.UserTimeElab, d.JobName, d.JobNumber, d.UserRef, d.FileRangesVar, d.ExistTypeMap
  
ORDER BY TimeRef DESC' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_JobReportReports]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.ce_JobReportReports    Script Date: 9/8/2004 3:27:20 PM ******/
CREATE PROCEDURE [dbo].[ce_JobReportReports]
  @FolderName  as varchar(32),
  @ReportName  as varchar(32),
  @JobReportId as int,
  @UserName    as varchar(32) = ''''

 AS

SELECT
  @JobReportid As JobReportId, c.ReportId, c.FilterValue, d.FilterVar, c.TotPages, 1 As IsGRANTED

FROM 
  tbl_Folders a

  INNER JOIN tbl_FoldersReportNames b ON a.FolderName = b.FolderName 
  INNER JOIN tbl_VarSetsValues v ON b.ReportRule = v.VarSetName
  
  INNER JOIN tbl_LogicalReports c 
   ON c.ReportName = v.VarValue
   AND (
     b.FilterVar = ''''
    /*** OR v.VarValue IN ( ***/
     OR c.FilterValue IN (
       SELECT VarValue FROM tbl_VarSetsValues
       WHERE VarsetName = b.FilterRule AND VarName = b.FilterVar
     )
   )
   AND c.XferRecipient = b.RecipientRule

  INNER JOIN tbl_ReportNames d
   ON d.ReportName = c.ReportName

WHERE 
  a.FolderName = @FolderName
  AND c.ReportName = @ReportName
  AND c.JobReportId = @JobReportId

ORDER BY c.FilterValue' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_FolderReportNames_HIST]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.ce_FolderReportNames_HIST    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  Stored Procedure dbo.ce_FolderReportNames_HIST    Script Date: 2002/07/01 5:48:53 PM ******/

CREATE PROCEDURE [dbo].[ce_FolderReportNames_HIST]
  @FolderName As varchar(32),
  @UserName   As varchar(32)
AS
 
SELECT DISTINCT c.ReportName, c.ReportDescr, c.HasIndex, 1 As IsGRANTED

FROM 
  tbl_Folders a
  INNER JOIN tbl_FoldersReportNames b ON a.FolderName = b.FolderName 
  INNER JOIN tbl_VarSetsValues v ON b.ReportRule = v.VarSetName
  INNER JOIN tbl_ReportNames c ON v.VarValue = c.ReportName

    
WHERE 
  a.FolderName = @FolderName
 
  AND EXISTS (
    SELECT JobReportId FROM tbl_LogicalReports_HIST j
	WHERE j.ReportName = c.ReportName
    AND v.VarName = '':REPORTNAME''
    AND (
       b.FilterVar = ''''
       OR j.FilterValue IN (
         SELECT VarValue FROM tbl_VarSetsValues
         WHERE VarsetName = b.FilterRule AND VarName = b.FilterVar
       )
    )
	AND b.RecipientRule = j.XferRecipient
  )
    
ORDER BY c.ReportName

' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_FolderReportNames]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.ce_FolderReportNames    Script Date: 9/8/2004 3:27:20 PM ******/

CREATE PROCEDURE [dbo].[ce_FolderReportNames]
  @FolderName As varchar(32),
  @UserName   As varchar(32)
AS
 
SELECT DISTINCT c.ReportName, c.ReportDescr, c.HasIndex, 1 As IsGRANTED

FROM 
  tbl_Folders a
  INNER JOIN tbl_FoldersReportNames b ON a.FolderName = b.FolderName 
  INNER JOIN tbl_VarSetsValues v ON b.ReportRule = v.VarSetName
  INNER JOIN tbl_ReportNames c ON v.VarValue = c.ReportName

    
WHERE 
  a.FolderName = @FolderName
 
  AND EXISTS (
    SELECT JobReportId FROM tbl_LogicalReports j
	WHERE j.ReportName = c.ReportName
    AND v.VarName = '':REPORTNAME''
    AND (
       b.FilterVar = ''''
       OR j.FilterValue IN (
         SELECT VarValue FROM tbl_VarSetsValues
         WHERE VarsetName = b.FilterRule AND VarName = b.FilterVar
       )
    )
	AND b.RecipientRule = j.XferRecipient
  )
    
ORDER BY c.ReportName
' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_FolderJobReportsOfDate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.ce_FolderJobReportsOfDate    Script Date: 9/8/2004 3:27:20 PM ******/

CREATE PROCEDURE [dbo].[ce_FolderJobReportsOfDate]
  @FolderName  as  varchar(32),
  @UserTimeRef as  datetime,
  @ReportName  as  varchar(32) = ''%'',
  @UserName    as  varchar(32) = ''''
 AS
BEGIN

DECLARE @XferDateDiff  smallint
SET @XferDateDiff = DATEDIFF(day, ''2000/1/1'',  @UserTimeRef)
  
SELECT 
  c.JobReportId, c.ReportId, e.JobName, e.JobNumber, 
  CONVERT(varchar, e.XferStartTime, 120) TimeRef,
  CONVERT(varchar, e.UserTimeRef  , 103) UserTimeRef, 
  CONVERT(varchar, e.UserTimeElab , 103) UserTimeElab,
  (
    CONVERT(varchar, e.XferStartTime, 103) + 
    SUBSTRING(CONVERT(varchar, e.XferStartTime, 121),11,6)
  )
  As XferStartTime,
  COUNT(*) As TotReports, SUM(c.TotPages) As TotPages,
  d.ReportName, d.ReportDescr, e.UserRef, e.FileRangesVar, e.ExistTypeMap, 1 As IsGRANTED

FROM 
  tbl_Folders a
  
  INNER JOIN tbl_FoldersReportNames b 
    ON a.FolderName = b.FolderName

  INNER JOIN tbl_VarSetsValues v 
    ON v.VarSetName = b.ReportRule
	
  INNER JOIN tbl_LogicalReports c 
    ON c.ReportName = v.VarValue 

  INNER JOIN tbl_ReportNames d
    ON d.ReportName = c.ReportName

  INNER JOIN tbl_JobReports e 
    ON e.JobReportId = c.JobReportId
  
WHERE 
  a.FolderName = @FolderName
  AND v.VarName = '':REPORTNAME''
  AND (
       b.FilterVar = ''''
       OR c.FilterValue IN (
         SELECT VarValue FROM tbl_VarSetsValues
         WHERE VarsetName = b.FilterRule AND VarName = b.FilterVar
       )
    )
  AND b.RecipientRule = c.XferRecipient
  AND c.XferDateDiff = @XferDateDiff
 

GROUP BY
  d.ReportName, d.ReportDescr, c.JobReportId, c.ReportId, e.XferStartTime, e.UserTimeRef, 
  e.UserTimeElab, e.JobName, e.JobNumber,  e.UserRef, e.FileRangesVar,
  e.ExistTypeMap

ORDER BY d.ReportName, TimeRef DESC

END
' 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ce_FolderChildFolders]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/****** Object:  Stored Procedure dbo.ce_FolderChildFolders    Script Date: 9/8/2004 3:27:20 PM ******/

CREATE PROCEDURE [dbo].[ce_FolderChildFolders]
  @FolderName As varchar(32),
  @UserName  As varchar(32)

AS

SELECT 
  FolderName, FolderDescr, IsActive, 

  CASE
    WHEN EXISTS(
      SELECT b.FolderName FROM tbl_Folders b WHERE b.ParentFolder = a.FolderName 
    ) 
    THEN 1 ELSE 0
  END
  As HasChilds,

  CASE
    WHEN EXISTS(
      SELECT b.FolderName FROM tbl_FoldersReportNames b 
      INNER JOIN tbl_VarSetsValues c on b.ReportRule = c.VarSetName
      WHERE b.FolderName = a.FolderName AND c.VarName = '':REPORTNAME''
    )
    THEN 1 ELSE 0
  END
  As HasReports
 
FROM tbl_Folders a WHERE ParentFolder = @FolderName

' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRExpFoldersReportNames2]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRExpFoldersReportNames2    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  Stored Procedure dbo.XRExpFoldersReportNames2    Script Date: 2002/07/01 5:48:53 PM ******/
CREATE PROCEDURE [dbo].[XRExpFoldersReportNames2]
(
  @RuleId int
)

AS

BEGIN
  DECLARE @FolderName varchar(32)
  DECLARE @ReportRule varchar(32)
  DECLARE @FilterVar varchar(32) 
  DECLARE @FilterRule varchar(32) 
  DECLARE @RecipientRule varchar(32)
  DECLARE @FORBID bit

  SELECT 
    @FolderName    = FolderName,
    @ReportRule    = ReportRule, 
	@FilterVar     = FilterVar, 
	@FilterRule    = FilterRule, 
	@RecipientRule = RecipientRule,
	@FORBID        = FORBID
  FROM tbl_FoldersReportNames WHERE RuleId = @RuleId 

  DELETE FROM tbl_FoldersReportNames_LL2 WHERE RuleId = @RuleId 
   
  INSERT INTO tbl_FoldersReportNames_LL2
  SELECT  @FolderName, a.ReportName, b.FilterValue, c.RecipientName, @FORBID, @RuleId
  FROM  
    ftb_FiltOfReportNames(@ReportRule, @FilterVar) a 
    CROSS JOIN ftb_FiltOfFilterVarValues(@FilterVar, @FilterRule) b 
    CROSS JOIN ftb_FiltOfRecipientNames(@RecipientRule) c	  

  RETURN SELECT COUNT(*) FROM tbl_FoldersReportNames_LL2 WHERE RuleId = @RuleId
END

' 
END
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[XRExpFoldersReportNames]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/****** Object:  Stored Procedure dbo.XRExpFoldersReportNames    Script Date: 9/8/2004 3:27:20 PM ******/

/****** Object:  Stored Procedure dbo.XRExpFoldersReportNames    Script Date: 2002/07/01 5:48:53 PM ******/
CREATE PROCEDURE [dbo].[XRExpFoldersReportNames]
(
  @FolderNameMask varchar(32) = ''%''
)

AS

BEGIN
  DECLARE @RuleId int
  
  IF @FolderNameMask = '''' BEGIN
    SET @FolderNameMask = ''%''
  END

  DECLARE FolderReportNames CURSOR LOCAL FORWARD_ONLY
  FOR
  SELECT RuleId FROM tbl_FoldersReportNames WHERE FolderName LIKE @FolderNameMask 

  OPEN FolderReportNames

  FETCH NEXT FROM FolderReportnames INTO @RuleId
  WHILE @@FETCH_STATUS = 0
  BEGIN
    EXECUTE XRExpFoldersReportNames2 @RuleId
    FETCH NEXT FROM FolderReportnames INTO @RuleId
  END
  
  CLOSE FolderReportNames
  
  DEALLOCATE FolderReportNames

  IF @FolderNameMask = ''%'' TRUNCATE TABLE tbl_FoldersReportNames_LL1
  ELSE DELETE FROM tbl_FoldersReportNames_LL1 WHERE FolderName LIKE @FolderNameMask

  INSERT INTO tbl_FoldersReportNames_LL1 (FolderName, ReportName, FilterValue, RecipientName)
  SELECT DISTINCT FolderName, ReportName, FilterValue, RecipientName
  FROM tbl_FoldersReportNames_LL2 t1
  WHERE 
    FORBID = 0 
    AND FolderName LIKE @FolderNameMask
    AND NOT EXISTS (
	  SELECT FolderName FROM tbl_FoldersReportNames_LL2 t2
	  WHERE 
	    t2.FORBID = 1
	    AND t1.FolderName = t2.FolderName
		AND t1.ReportName = t2.ReportName
		AND t1.FilterValue = t2.FilterValue
		AND t1.RecipientName = t2.RecipientName
    )
END

' 
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_WorkQueue_InsertTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_WorkQueue]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_WorkQueue_InsertTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_WorkQueue] ADD  CONSTRAINT [DF_tbl_WorkQueue_InsertTime]  DEFAULT (getdate()) FOR [InsertTime]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Users_UserDescr]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Users_UserDescr]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Users] ADD  CONSTRAINT [DF_tbl_Users_UserDescr]  DEFAULT ('') FOR [UserDescr]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_UniversalPoolIds_LocalPathId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_UniversalPoolIds]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_UniversalPoolIds_LocalPathId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_UniversalPoolIds] ADD  CONSTRAINT [DF_tbl_UniversalPoolIds_LocalPathId]  DEFAULT ('') FOR [LocalPathId]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_UniversalObjectIds_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_UniversalObjectIds]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_UniversalObjectIds_Status]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_UniversalObjectIds] ADD  CONSTRAINT [DF_tbl_UniversalObjectIds_Status]  DEFAULT ((0)) FOR [Status]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_UniversalObjectIds_XferStartTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_UniversalObjectIds]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_UniversalObjectIds_XferStartTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_UniversalObjectIds] ADD  CONSTRAINT [DF_tbl_UniversalObjectIds_XferStartTime]  DEFAULT (getdate()) FOR [InsertTime]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_UniversalObjectIds_ObjectType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_UniversalObjectIds]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_UniversalObjectIds_ObjectType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_UniversalObjectIds] ADD  CONSTRAINT [DF_tbl_UniversalObjectIds_ObjectType]  DEFAULT ('') FOR [ObjectType]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_UniversalObjectIds_ObjectRef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_UniversalObjectIds]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_UniversalObjectIds_ObjectRef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_UniversalObjectIds] ADD  CONSTRAINT [DF_tbl_UniversalObjectIds_ObjectRef]  DEFAULT ('') FOR [ObjectRef]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_TarFiles_TarLogicalPathId_IN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_TarFiles]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_TarFiles_TarLogicalPathId_IN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_TarFiles] ADD  CONSTRAINT [DF_tbl_TarFiles_TarLogicalPathId_IN]  DEFAULT ('') FOR [TarLocalPathId_IN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_TarFiles_TarLogicalPathId_OUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_TarFiles]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_TarFiles_TarLogicalPathId_OUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_TarFiles] ADD  CONSTRAINT [DF_tbl_TarFiles_TarLogicalPathId_OUT]  DEFAULT ('') FOR [TarLocalPathId_OUT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_TarFiles_CreationTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_TarFiles]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_TarFiles_CreationTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_TarFiles] ADD  CONSTRAINT [DF_tbl_TarFiles_CreationTime]  DEFAULT (getdate()) FOR [CreationTime]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_TablesModifyTime_ModifyTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_TablesModifyTime]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_TablesModifyTime_ModifyTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_TablesModifyTime] ADD  CONSTRAINT [DF_tbl_TablesModifyTime_ModifyTime]  DEFAULT (getdate()) FOR [ModifyTime]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SpecialPermissions_Enabled]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_SpecialPermissions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SpecialPermissions_Enabled]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_SpecialPermissions] ADD  CONSTRAINT [DF_tbl_SpecialPermissions_Enabled]  DEFAULT ((1)) FOR [Enabled]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Resources_ResGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Resources]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Resources_ResGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Resources] ADD  CONSTRAINT [DF_tbl_Resources_ResGroup]  DEFAULT ('') FOR [ResGroup]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportsGroups_ReportRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportsGroups]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportsGroups_ReportRule]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportsGroups] ADD  CONSTRAINT [DF_tbl_ReportsGroups_ReportRule]  DEFAULT ('') FOR [ReportRule]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportsGroups_FilterVar]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportsGroups]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportsGroups_FilterVar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportsGroups] ADD  CONSTRAINT [DF_tbl_ReportsGroups_FilterVar]  DEFAULT ('') FOR [FilterVar]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportsGroups_FilterRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportsGroups]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportsGroups_FilterRule]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportsGroups] ADD  CONSTRAINT [DF_tbl_ReportsGroups_FilterRule]  DEFAULT ('') FOR [FilterRule]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportsGroups_RecipientRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportsGroups]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportsGroups_RecipientRule]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportsGroups] ADD  CONSTRAINT [DF_tbl_ReportsGroups_RecipientRule]  DEFAULT ('') FOR [RecipientRule]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_FilterName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_FilterName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] ADD  CONSTRAINT [DF_tbl_SubReportNames_FilterName]  DEFAULT ('') FOR [FilterVar]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportNames_HasIndexes]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportNames_HasIndexes]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] ADD  CONSTRAINT [DF_tbl_ReportNames_HasIndexes]  DEFAULT ('') FOR [HasIndex]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_ActiveGens]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_ActiveGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] ADD  CONSTRAINT [DF_tbl_SubReportNames_ActiveGens]  DEFAULT ((5)) FOR [ActiveGens]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_ActiveDays]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_ActiveDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] ADD  CONSTRAINT [DF_tbl_SubReportNames_ActiveDays]  DEFAULT ((5)) FOR [ActiveDays]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_HoldGens]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_HoldGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] ADD  CONSTRAINT [DF_tbl_SubReportNames_HoldGens]  DEFAULT ((30)) FOR [HoldGens]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_HoldDays]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_HoldDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] ADD  CONSTRAINT [DF_tbl_SubReportNames_HoldDays]  DEFAULT ((30)) FOR [HoldDays]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_MailTo]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_MailTo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] ADD  CONSTRAINT [DF_tbl_SubReportNames_MailTo]  DEFAULT ('') FOR [MailTo]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportNames_XrefData]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportNames_XrefData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] ADD  CONSTRAINT [DF_tbl_ReportNames_XrefData]  DEFAULT ('') FOR [XrefData]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportNames_TotReports]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportNames_TotReports]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] ADD  CONSTRAINT [DF_tbl_ReportNames_TotReports]  DEFAULT ((0)) FOR [TotReports]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportNames_TotPages]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportNames_TotPages]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] ADD  CONSTRAINT [DF_tbl_ReportNames_TotPages]  DEFAULT ((0)) FOR [TotPages]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_SubReportNames_ModifyKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_SubReportNames_ModifyKey]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] ADD  CONSTRAINT [DF_tbl_SubReportNames_ModifyKey]  DEFAULT ('') FOR [ModifyKey]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ReportNames_CheckFlag]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ReportNames_CheckFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ReportNames] ADD  CONSTRAINT [DF_tbl_ReportNames_CheckFlag]  DEFAULT ((0)) FOR [CheckFlag]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesReportNames_LL1_FilterValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames_LL1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesReportNames_LL1_FilterValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesReportNames_LL1] ADD  CONSTRAINT [DF_tbl_ProfilesReportNames_LL1_FilterValue]  DEFAULT ('') FOR [FilterValue]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesReportNames_LL1_RecipientName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames_LL1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesReportNames_LL1_RecipientName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesReportNames_LL1] ADD  CONSTRAINT [DF_tbl_ProfilesReportNames_LL1_RecipientName]  DEFAULT ('') FOR [RecipientName]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesReportNames_ReportRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesReportNames_ReportRule]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesReportNames] ADD  CONSTRAINT [DF_tbl_ProfilesReportNames_ReportRule]  DEFAULT ('') FOR [ReportRule]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesReportNames_FilterVar]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesReportNames_FilterVar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesReportNames] ADD  CONSTRAINT [DF_tbl_ProfilesReportNames_FilterVar]  DEFAULT ('') FOR [FilterVar]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesSubReportNames_FilterValues]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesSubReportNames_FilterValues]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesReportNames] ADD  CONSTRAINT [DF_tbl_ProfilesSubReportNames_FilterValues]  DEFAULT ('') FOR [FilterRule]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesReportNames_XferRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesReportNames_XferRecipient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesReportNames] ADD  CONSTRAINT [DF_tbl_ProfilesReportNames_XferRecipient]  DEFAULT ('') FOR [RecipientRule]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesIndexTables_LL1_Filtervalue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesIndexTables_LL1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesIndexTables_LL1_Filtervalue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesIndexTables_LL1] ADD  CONSTRAINT [DF_tbl_ProfilesIndexTables_LL1_Filtervalue]  DEFAULT ('('')') FOR [Filtervalue]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ProfilesIndexTables_LL1_RecipientName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ProfilesIndexTables_LL1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ProfilesIndexTables_LL1_RecipientName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ProfilesIndexTables_LL1] ADD  CONSTRAINT [DF_tbl_ProfilesIndexTables_LL1_RecipientName]  DEFAULT ('') FOR [RecipientName]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Profiles_ProfileDescr]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Profiles]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Profiles_ProfileDescr]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Profiles] ADD  CONSTRAINT [DF_tbl_Profiles_ProfileDescr]  DEFAULT ('') FOR [ProfileDescr]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Profiles_LookAtEverything]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Profiles]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Profiles_LookAtEverything]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Profiles] ADD  CONSTRAINT [DF_tbl_Profiles_LookAtEverything]  DEFAULT ((0)) FOR [LookAtEverything]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_PrintersAliases_TypeOfWork]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_PrintersAliases]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_PrintersAliases_TypeOfWork]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_PrintersAliases] ADD  CONSTRAINT [DF_tbl_PrintersAliases_TypeOfWork]  DEFAULT ((515)) FOR [TypeOfWork]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_PrintersAliases_Priority]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_PrintersAliases]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_PrintersAliases_Priority]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_PrintersAliases] ADD  CONSTRAINT [DF_tbl_PrintersAliases_Priority]  DEFAULT ((1)) FOR [Priority]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_PageSize]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_PageSize]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] ADD  CONSTRAINT [DF_tbl_Os390PrintParameters_PageSize]  DEFAULT ('-') FOR [PageSize]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_PageOrient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_PageOrient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] ADD  CONSTRAINT [DF_tbl_Os390PrintParameters_PageOrient]  DEFAULT ('P') FOR [PageOrient]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_Chars]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_Chars]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] ADD  CONSTRAINT [DF_tbl_Os390PrintParameters_Chars]  DEFAULT ('-') FOR [CharsDef]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_cc]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_cc]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] ADD  CONSTRAINT [DF_tbl_Os390PrintParameters_cc]  DEFAULT ('-') FOR [cc]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_Fcb]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_Fcb]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] ADD  CONSTRAINT [DF_tbl_Os390PrintParameters_Fcb]  DEFAULT ('-') FOR [Fcb]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_PrintParameters_Trc]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_PrintParameters_Trc]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] ADD  CONSTRAINT [DF_tbl_PrintParameters_Trc]  DEFAULT ('-') FOR [Trc]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_Formatted]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_Formatted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] ADD  CONSTRAINT [DF_tbl_Os390PrintParameters_Formatted]  DEFAULT ((1)) FOR [Formatted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_t1Fonts]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_t1Fonts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] ADD  CONSTRAINT [DF_tbl_Os390PrintParameters_t1Fonts]  DEFAULT ((1)) FOR [t1Fonts]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_LaserAdjust]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_LaserAdjust]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] ADD  CONSTRAINT [DF_tbl_Os390PrintParameters_LaserAdjust]  DEFAULT ('[0 0]') FOR [LaserAdjust]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_FormDef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_FormDef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] ADD  CONSTRAINT [DF_tbl_Os390PrintParameters_FormDef]  DEFAULT ('') FOR [FormDef]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_PageDef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_PageDef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] ADD  CONSTRAINT [DF_tbl_Os390PrintParameters_PageDef]  DEFAULT ('') FOR [PageDef]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_ReplaceDef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_ReplaceDef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] ADD  CONSTRAINT [DF_tbl_Os390PrintParameters_ReplaceDef]  DEFAULT ('') FOR [ReplaceDef]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Os390PrintParameters_PrintControlFile]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Os390PrintParameters]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Os390PrintParameters_PrintControlFile]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Os390PrintParameters] ADD  CONSTRAINT [DF_tbl_Os390PrintParameters_PrintControlFile]  DEFAULT ('') FOR [PrintControlFile]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ObjPendingBackups_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ObjPendingBackups]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ObjPendingBackups_Status]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ObjPendingBackups] ADD  CONSTRAINT [DF_tbl_ObjPendingBackups_Status]  DEFAULT ((0)) FOR [Status]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_NamedReportsGroups_RecipientRule]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_NamedReportsGroups]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_NamedReportsGroups_RecipientRule]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_NamedReportsGroups] ADD  CONSTRAINT [DF_tbl_NamedReportsGroups_RecipientRule]  DEFAULT ('') FOR [RecipientRule]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_HIST_XferRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports_HIST]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_HIST_XferRecipient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports_HIST] ADD  CONSTRAINT [DF_tbl_LogicalReports_HIST_XferRecipient]  DEFAULT ('') FOR [XferRecipient]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_TotPages]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_TotPages]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports] ADD  CONSTRAINT [DF_tbl_LogicalReports_TotPages]  DEFAULT ((0)) FOR [TotPages]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_XferRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_XferRecipient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports] ADD  CONSTRAINT [DF_tbl_LogicalReports_XferRecipient]  DEFAULT ('') FOR [XferRecipient]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_CheckStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_CheckStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports] ADD  CONSTRAINT [DF_tbl_LogicalReports_CheckStatus]  DEFAULT ((0)) FOR [CheckStatus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_HasNotes]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_HasNotes]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports] ADD  CONSTRAINT [DF_tbl_LogicalReports_HasNotes]  DEFAULT ((0)) FOR [HasNotes]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_LastUsedToView]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_LastUsedToView]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports] ADD  CONSTRAINT [DF_tbl_LogicalReports_LastUsedToView]  DEFAULT ('') FOR [LastUsedToView]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_LogicalReports_LastUsedToCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_LogicalReports_LastUsedToCheck]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_LogicalReports] ADD  CONSTRAINT [DF_tbl_LogicalReports_LastUsedToCheck]  DEFAULT ('') FOR [LastUsedToCheck]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobSpool_SpoolPathId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobSpool]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobSpool_SpoolPathId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobSpool] ADD  CONSTRAINT [DF_tbl_JobSpool_SpoolPathId]  DEFAULT ('L1') FOR [SpoolPathId]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobSpool_ContentType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobSpool]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobSpool_ContentType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobSpool] ADD  CONSTRAINT [DF_tbl_JobSpool_ContentType]  DEFAULT ('application/pdf') FOR [ContentType]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobSpool_PrintDuplex]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobSpool]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobSpool_PrintDuplex]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobSpool] ADD  CONSTRAINT [DF_tbl_JobSpool_PrintDuplex]  DEFAULT ((0)) FOR [PrintDuplex]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobSpool_RetryCount]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobSpool]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobSpool_RetryCount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobSpool] ADD  CONSTRAINT [DF_tbl_JobSpool_RetryCount]  DEFAULT ((0)) FOR [RetryCount]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsTarFiles_TarFileNumberIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsTarFiles]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsTarFiles_TarFileNumberIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsTarFiles] ADD  CONSTRAINT [DF_tbl_JobReportsTarFiles_TarFileNumberIN]  DEFAULT ((0)) FOR [TarFileNumberIN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsTarFiles_TarRecNumberIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsTarFiles]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsTarFiles_TarRecNumberIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsTarFiles] ADD  CONSTRAINT [DF_tbl_JobReportsTarFiles_TarRecNumberIN]  DEFAULT ((0)) FOR [TarRecNumberIN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsTarFiles_TarFileNumberOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsTarFiles]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsTarFiles_TarFileNumberOUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsTarFiles] ADD  CONSTRAINT [DF_tbl_JobReportsTarFiles_TarFileNumberOUT]  DEFAULT ((0)) FOR [TarFileNumberOUT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsTarFiles_TarRecNumberOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsTarFiles]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsTarFiles_TarRecNumberOUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsTarFiles] ADD  CONSTRAINT [DF_tbl_JobReportsTarFiles_TarRecNumberOUT]  DEFAULT ((0)) FOR [TarRecNumberOUT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsPendingXfers_TimeInsert]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsPendingXfers]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsPendingXfers_TimeInsert]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsPendingXfers] ADD  CONSTRAINT [DF_tbl_JobReportsPendingXfers_TimeInsert]  DEFAULT (getdate()) FOR [TimeInsert]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsLocalFileCache_LocalPath]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsLocalFileCache]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsLocalFileCache_LocalPath]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsLocalFileCache] ADD  CONSTRAINT [DF_tbl_JobReportsLocalFileCache_LocalPath]  DEFAULT ('') FOR [LocalPath]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsLocalFileCache_UsedTimes]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsLocalFileCache]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsLocalFileCache_UsedTimes]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsLocalFileCache] ADD  CONSTRAINT [DF_tbl_JobReportsLocalFileCache_UsedTimes]  DEFAULT ((0)) FOR [UsedTimes]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsLocalFileCache_CreateTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsLocalFileCache]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsLocalFileCache_CreateTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsLocalFileCache] ADD  CONSTRAINT [DF_tbl_JobReportsLocalFileCache_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsLocalFileCache_LastUsedTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsLocalFileCache]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsLocalFileCache_LastUsedTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsLocalFileCache] ADD  CONSTRAINT [DF_tbl_JobReportsLocalFileCache_LastUsedTime]  DEFAULT (getdate()) FOR [LastUsedTime]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabData_ElabFormat]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabData_ElabFormat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabs] ADD  CONSTRAINT [DF_tbl_JobReportsElabData_ElabFormat]  DEFAULT ((1)) FOR [ElabFormat]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabData_FileRangesVar]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabData_FileRangesVar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabs] ADD  CONSTRAINT [DF_tbl_JobReportsElabData_FileRangesVar]  DEFAULT ('') FOR [FileRangesVar]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabData_isAFP]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabData_isAFP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabs] ADD  CONSTRAINT [DF_tbl_JobReportsElabData_isAFP]  DEFAULT ((0)) FOR [isAFP]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabData_isFullAFP]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabData_isFullAFP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabs] ADD  CONSTRAINT [DF_tbl_JobReportsElabData_isFullAFP]  DEFAULT ((0)) FOR [isFullAFP]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabData_UserRef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabData_UserRef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabs] ADD  CONSTRAINT [DF_tbl_JobReportsElabData_UserRef]  DEFAULT ('') FOR [UserRef]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabData_FileRangeVar]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabs]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabData_FileRangeVar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabs] ADD  CONSTRAINT [DF_tbl_JobReportsElabData_FileRangeVar]  DEFAULT ('') FOR [FileRangeVar]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_ElabFormat]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_ElabFormat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] ADD  CONSTRAINT [DF_tbl_JobReportsElabOptions_ElabFormat]  DEFAULT ((1)) FOR [ElabFormat]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_HasIndexes]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_HasIndexes]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] ADD  CONSTRAINT [DF_tbl_JobReportsElabOptions_HasIndexes]  DEFAULT ((0)) FOR [HasIndexes]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_isAFP]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_isAFP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] ADD  CONSTRAINT [DF_tbl_JobReportsElabOptions_isAFP]  DEFAULT ((0)) FOR [isAfp]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_isFullAFP]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_isFullAFP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] ADD  CONSTRAINT [DF_tbl_JobReportsElabOptions_isFullAFP]  DEFAULT ((0)) FOR [isFullAfp]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_FileRangeVar]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_FileRangeVar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] ADD  CONSTRAINT [DF_tbl_JobReportsElabOptions_FileRangeVar]  DEFAULT ('') FOR [FileRangeVar]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_ExistTypeMap]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_ExistTypeMap]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] ADD  CONSTRAINT [DF_tbl_JobReportsElabOptions_ExistTypeMap]  DEFAULT ((0)) FOR [ExistTypeMap]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportsElabOptions_MaybeTypeMap]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsElabOptions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportsElabOptions_MaybeTypeMap]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportsElabOptions] ADD  CONSTRAINT [DF_tbl_JobReportsElabOptions_MaybeTypeMap]  DEFAULT ((0)) FOR [MaybeTypeMap]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_LocalPathId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_LocalPathId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] ADD  CONSTRAINT [DF_tbl_JobReports_HIST_LocalPathId]  DEFAULT ('') FOR [LocalPathId]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_OrigJobReportName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_OrigJobReportName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] ADD  CONSTRAINT [DF_tbl_JobReports_HIST_OrigJobReportName]  DEFAULT ('') FOR [OrigJobReportName]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_TarFileNumberIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_TarFileNumberIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] ADD  CONSTRAINT [DF_tbl_JobReports_HIST_TarFileNumberIN]  DEFAULT ((0)) FOR [TarFileNumberIN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_TarRecNumberIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_TarRecNumberIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] ADD  CONSTRAINT [DF_tbl_JobReports_HIST_TarRecNumberIN]  DEFAULT ((0)) FOR [TarRecNumberIN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_TarFileNumberOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_TarFileNumberOUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] ADD  CONSTRAINT [DF_tbl_JobReports_HIST_TarFileNumberOUT]  DEFAULT ((0)) FOR [TarFileNumberOUT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_TarRecNumberOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_TarRecNumberOUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] ADD  CONSTRAINT [DF_tbl_JobReports_HIST_TarRecNumberOUT]  DEFAULT ((0)) FOR [TarRecNumberOUT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_ViewLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_ViewLevel]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] ADD  CONSTRAINT [DF_tbl_JobReports_HIST_ViewLevel]  DEFAULT ((0)) FOR [ViewLevel]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_MailingStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_MailingStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] ADD  CONSTRAINT [DF_tbl_JobReports_HIST_MailingStatus]  DEFAULT ((0)) FOR [MailingStatus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_PendingOp]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_PendingOp]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] ADD  CONSTRAINT [DF_tbl_JobReports_HIST_PendingOp]  DEFAULT ((0)) FOR [PendingOp]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HIST_UserRef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports_HIST]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HIST_UserRef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports_HIST] ADD  CONSTRAINT [DF_tbl_JobReports_HIST_UserRef]  DEFAULT ('') FOR [UserRef]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_LocalPathId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_LocalPathId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_LocalPathId]  DEFAULT ('') FOR [LocalPathId_IN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_LocalPathId_OUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_LocalPathId_OUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_LocalPathId_OUT]  DEFAULT ('') FOR [LocalPathId_OUT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_OrigJobReportName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_OrigJobReportName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_OrigJobReportName]  DEFAULT ('') FOR [OrigJobReportName]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_TarFileNumberIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_TarFileNumberIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_TarFileNumberIN]  DEFAULT ((0)) FOR [TarFileNumberIN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_TarRecNumberIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_TarRecNumberIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_TarRecNumberIN]  DEFAULT ((0)) FOR [TarRecNumberIN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_TarFileNumberOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_TarFileNumberOUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_TarFileNumberOUT]  DEFAULT ((0)) FOR [TarFileNumberOUT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_TarRecNumberOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_TarRecNumberOUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_TarRecNumberOUT]  DEFAULT ((0)) FOR [TarRecNumberOUT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_HasIndexes]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_HasIndexes]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_HasIndexes]  DEFAULT ((0)) FOR [HasIndexes]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_isAFP]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_isAFP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_isAFP]  DEFAULT ((0)) FOR [isAFP]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_isFullAFP]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_isFullAFP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_isFullAFP]  DEFAULT ((0)) FOR [isFullAFP]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_ExistTypeMap]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_ExistTypeMap]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_ExistTypeMap]  DEFAULT ((0)) FOR [ExistTypeMap]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_MayBeTypeMap]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_MayBeTypeMap]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_MayBeTypeMap]  DEFAULT ((0)) FOR [MayBeTypeMap]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_RangeVar]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_RangeVar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_RangeVar]  DEFAULT ('') FOR [FileRangesVar]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_ViewLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_ViewLevel]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_ViewLevel]  DEFAULT ((0)) FOR [ViewLevel]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_MailedFlag]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_MailedFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_MailedFlag]  DEFAULT ((0)) FOR [MailingStatus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_PendingOp]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_PendingOp]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_PendingOp]  DEFAULT ((0)) FOR [PendingOp]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReports_UserReportRef]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReports]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReports_UserReportRef]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReports] ADD  CONSTRAINT [DF_tbl_JobReports_UserReportRef]  DEFAULT ('') FOR [UserRef]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNamesQre_JobName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNamesQre_JobName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNamesQrexp] ADD  CONSTRAINT [DF_tbl_JobReportNamesQre_JobName]  DEFAULT ('') FOR [JobName]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNamesQre_Recipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNamesQre_Recipient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNamesQrexp] ADD  CONSTRAINT [DF_tbl_JobReportNamesQre_Recipient]  DEFAULT ('') FOR [Recipient]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNamesQre_RemoteHostAddr]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNamesQre_RemoteHostAddr]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNamesQrexp] ADD  CONSTRAINT [DF_tbl_JobReportNamesQre_RemoteHostAddr]  DEFAULT ('') FOR [RemoteHostAddr]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNamesQre_RemoteFileName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNamesQre_RemoteFileName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNamesQrexp] ADD  CONSTRAINT [DF_tbl_JobReportNamesQre_RemoteFileName]  DEFAULT ('') FOR [RemoteFileName]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNamesQre_PageText]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNamesQre_PageText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNamesQrexp] ADD  CONSTRAINT [DF_tbl_JobReportNamesQre_PageText]  DEFAULT ('') FOR [PageText]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNamesQre_MaxPageTests]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNamesQrexp]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNamesQre_MaxPageTests]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNamesQrexp] ADD  CONSTRAINT [DF_tbl_JobReportNamesQre_MaxPageTests]  DEFAULT ((1)) FOR [MaxPageTests]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_ReportFormat]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ReportFormat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_ReportFormat]  DEFAULT ((2)) FOR [ReportFormat]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_OutReportFormat]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_OutReportFormat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_OutReportFormat]  DEFAULT ((1)) FOR [ElabFormat]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_IsActive]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_IsActive]  DEFAULT ((1)) FOR [IsActive]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_PrintFlag]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_PrintFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_PrintFlag]  DEFAULT ('Y') FOR [PrintFlag]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_ViewOnlineFlag]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ViewOnlineFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_ViewOnlineFlag]  DEFAULT ('Y') FOR [ViewOnlineFlag]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_ActiveDays]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ActiveDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_ActiveDays]  DEFAULT ((5)) FOR [ActiveDays]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_ActiveGens]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ActiveGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_ActiveGens]  DEFAULT ((5)) FOR [ActiveGens]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_HoldDays]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_HoldDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_HoldDays]  DEFAULT ((30)) FOR [HoldDays]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_HoldGens]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_HoldGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_HoldGens]  DEFAULT ((30)) FOR [HoldGens]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_StorageClass]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_StorageClass]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_StorageClass]  DEFAULT ('') FOR [StorageClass]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_LinesPerPage1]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_LinesPerPage1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_LinesPerPage1]  DEFAULT ((133)) FOR [CharsPerLine]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_LinesPerPage]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_LinesPerPage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_LinesPerPage]  DEFAULT ((70)) FOR [LinesPerPage]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_PageOrient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_PageOrient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_PageOrient]  DEFAULT ('L') FOR [PageOrient]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_HasCc]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_HasCc]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_HasCc]  DEFAULT ((1)) FOR [HasCc]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_FontSize]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_FontSize]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_FontSize]  DEFAULT ('[9.0 8.24]') FOR [FontSize]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_FitToPage]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_FitToPage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_FitToPage]  DEFAULT ((0)) FOR [FitToPage]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_CodePage]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_CodePage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_CodePage]  DEFAULT ('') FOR [CodePage]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_TotReports]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_TotReports]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_TotReports]  DEFAULT ((0)) FOR [TotJobReports]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_TotPages]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_TotPages]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_TotPages]  DEFAULT ((0)) FOR [TotPages]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_ClusterIndex]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ClusterIndex]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_ClusterIndex]  DEFAULT ('') FOR [ClIndexTable]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_ParseFileName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_ParseFileName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_ParseFileName]  DEFAULT ('*,NULL.XML') FOR [ParseFileName]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_LocalFileId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_LocalFileId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_LocalFileId]  DEFAULT ('L1') FOR [TargetLocalPathId_IN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_TargetLocalPathId_OUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_TargetLocalPathId_OUT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_TargetLocalPathId_OUT]  DEFAULT ('') FOR [TargetLocalPathId_OUT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_WorkType]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_WorkType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_WorkType]  DEFAULT ((1)) FOR [TypeOfWork]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_MailTo]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_MailTo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_MailTo]  DEFAULT ('') FOR [MailTo]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_XrefData]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_XrefData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_XrefData]  DEFAULT ('') FOR [XrefData]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_JobReportNames_CreateId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_JobReportNames]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_JobReportNames_CreateId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_JobReportNames] ADD  CONSTRAINT [DF_tbl_JobReportNames_CreateId]  DEFAULT ('') FOR [ModifyKey]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersRules_FORBID]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersRules]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersRules_FORBID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersRules] ADD  CONSTRAINT [DF_tbl_FoldersRules_FORBID]  DEFAULT ((0)) FOR [FORBID]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersSubReportNames_FilterValues]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_old]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersSubReportNames_FilterValues]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportNames_old] ADD  CONSTRAINT [DF_tbl_FoldersSubReportNames_FilterValues]  DEFAULT ('') FOR [FilterRule]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersReportNames_XferRecipient]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_old]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersReportNames_XferRecipient]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportNames_old] ADD  CONSTRAINT [DF_tbl_FoldersReportNames_XferRecipient]  DEFAULT ('') FOR [RecipientRule]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersReportNames_DENY_ACCESS]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_old]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersReportNames_DENY_ACCESS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportNames_old] ADD  CONSTRAINT [DF_tbl_FoldersReportNames_DENY_ACCESS]  DEFAULT ((0)) FOR [FORBID]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersReportNames_LL1_FilterValue]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_LL1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersReportNames_LL1_FilterValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportNames_LL1] ADD  CONSTRAINT [DF_tbl_FoldersReportNames_LL1_FilterValue]  DEFAULT ('') FOR [FilterValue]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersReportNames_LL1_RecipientName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportNames_LL1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersReportNames_LL1_RecipientName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportNames_LL1] ADD  CONSTRAINT [DF_tbl_FoldersReportNames_LL1_RecipientName]  DEFAULT ('') FOR [RecipientName]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_Folders_IsActive]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_Folders]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_Folders_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_Folders] ADD  CONSTRAINT [DF_tbl_Folders_IsActive]  DEFAULT ((1)) FOR [IsActive]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FilterVarValues_FilterValueDescr]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FilterVarValues]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FilterVarValues_FilterValueDescr]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FilterVarValues] ADD  CONSTRAINT [DF_tbl_FilterVarValues_FilterValueDescr]  DEFAULT ('') FOR [FilterDescr]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EmailSentList_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EmailSentList]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EmailSentList_Status]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EmailSentList] ADD  CONSTRAINT [DF_tbl_EmailSentList_Status]  DEFAULT ((0)) FOR [Status]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EmailSentList_FromField]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EmailSentList]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EmailSentList_FromField]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EmailSentList] ADD  CONSTRAINT [DF_tbl_EmailSentList_FromField]  DEFAULT ('') FOR [FromField]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EmailSentList_BodyField]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EmailSentList]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EmailSentList_BodyField]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EmailSentList] ADD  CONSTRAINT [DF_tbl_EmailSentList_BodyField]  DEFAULT ('') FOR [BodyField]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EMailQueue_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EMailPendingQueue]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EMailQueue_Status]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EMailPendingQueue] ADD  CONSTRAINT [DF_tbl_EMailQueue_Status]  DEFAULT ((0)) FOR [Status]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EMailPendingQueue_SendNumber]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EMailPendingQueue]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EMailPendingQueue_SendNumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EMailPendingQueue] ADD  CONSTRAINT [DF_tbl_EMailPendingQueue_SendNumber]  DEFAULT ((0)) FOR [SendNumber]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EMailQueue_FromField]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EMailPendingQueue]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EMailQueue_FromField]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EMailPendingQueue] ADD  CONSTRAINT [DF_tbl_EMailQueue_FromField]  DEFAULT ('') FOR [FromField]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_EMailQueue_BodyField]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_EMailPendingQueue]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_EMailQueue_BodyField]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_EMailPendingQueue] ADD  CONSTRAINT [DF_tbl_EMailQueue_BodyField]  DEFAULT ('') FOR [BodyField]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrHoldDays]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrHoldDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] ADD  CONSTRAINT [DF_tbl_CTD_XrefData_XrHoldDays]  DEFAULT ((0)) FOR [XrHoldDays]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrHoldGens]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrHoldGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] ADD  CONSTRAINT [DF_tbl_CTD_XrefData_XrHoldGens]  DEFAULT ((0)) FOR [XrHoldGens]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrKeepDays]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrKeepDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] ADD  CONSTRAINT [DF_tbl_CTD_XrefData_XrKeepDays]  DEFAULT ((0)) FOR [XrKeepDays]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrKeepGens]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrKeepGens]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] ADD  CONSTRAINT [DF_tbl_CTD_XrefData_XrKeepGens]  DEFAULT ((0)) FOR [XrKeepGens]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrProfileList]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrProfileList]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] ADD  CONSTRAINT [DF_tbl_CTD_XrefData_XrProfileList]  DEFAULT ('') FOR [XrProfileList]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrFolderList]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrFolderList]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] ADD  CONSTRAINT [DF_tbl_CTD_XrefData_XrFolderList]  DEFAULT ('') FOR [XrFolderList]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrMailToList]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrMailToList]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] ADD  CONSTRAINT [DF_tbl_CTD_XrefData_XrMailToList]  DEFAULT ('') FOR [XrMailToList]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_XrParseFile]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_XrParseFile]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] ADD  CONSTRAINT [DF_tbl_CTD_XrefData_XrParseFile]  DEFAULT ('') FOR [XrParseFileName]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_XrefData_ModifyKey]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_XrefReportData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_XrefData_ModifyKey]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_XrefReportData] ADD  CONSTRAINT [DF_tbl_CTD_XrefData_ModifyKey]  DEFAULT (NULL) FOR [ModifyKey]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ctd_ReportMappings_XrJobReportName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_ReportMappings]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ctd_ReportMappings_XrJobReportName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_ReportMappings] ADD  CONSTRAINT [DF_tbl_ctd_ReportMappings_XrJobReportName]  DEFAULT ('') FOR [XrJobReportName]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ctd_ReportMappings_XrJobSuffix]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_ReportMappings]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ctd_ReportMappings_XrJobSuffix]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_ReportMappings] ADD  CONSTRAINT [DF_tbl_ctd_ReportMappings_XrJobSuffix]  DEFAULT ((0)) FOR [XrJobSuffix]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ctd_ReportMappings_XrParseFileName]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_ReportMappings]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ctd_ReportMappings_XrParseFileName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_ReportMappings] ADD  CONSTRAINT [DF_tbl_ctd_ReportMappings_XrParseFileName]  DEFAULT ('') FOR [XrParseFileName]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ctd_INBUNDLES_XrefData_JobReportid]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES_XrefData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ctd_INBUNDLES_XrefData_JobReportid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_INBUNDLES_XrefData] ADD  CONSTRAINT [DF_tbl_ctd_INBUNDLES_XrefData_JobReportid]  DEFAULT ((-1)) FOR [JobReportid]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ctd_BUNDLES_XrefData_ERROR_MESSAGE]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES_XrefData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ctd_BUNDLES_XrefData_ERROR_MESSAGE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_INBUNDLES_XrefData] ADD  CONSTRAINT [DF_tbl_ctd_BUNDLES_XrefData_ERROR_MESSAGE]  DEFAULT ('') FOR [ERROR_MESSAGE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_IN_Bundles_DateTimeString]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_IN_Bundles_DateTimeString]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_INBUNDLES] ADD  CONSTRAINT [DF_tbl_CTD_IN_Bundles_DateTimeString]  DEFAULT ('') FOR [DateTimeString]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_ctd_INBUNDLES_XferMode]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_ctd_INBUNDLES_XferMode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_INBUNDLES] ADD  CONSTRAINT [DF_tbl_ctd_INBUNDLES_XferMode]  DEFAULT ((4)) FOR [XferMode]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_CTD_IN_Bundles_InsertTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_ctd_INBUNDLES]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_CTD_IN_Bundles_InsertTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_ctd_INBUNDLES] ADD  CONSTRAINT [DF_tbl_CTD_IN_Bundles_InsertTime]  DEFAULT (getdate()) FOR [InsertTime]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_AuditLog_OperationTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_AuditLog]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_AuditLog_OperationTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_AuditLog] ADD  CONSTRAINT [DF_tbl_AuditLog_OperationTime]  DEFAULT (getdate()) FOR [OperationTime]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersReportGroups_ReportGroupId]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportGroups]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersReportGroups_ReportGroupId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportGroups] ADD  CONSTRAINT [DF_tbl_FoldersReportGroups_ReportGroupId]  DEFAULT ((0)) FOR [ReportGroupId]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_tbl_FoldersReportGroups_FORBID]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportGroups]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tbl_FoldersReportGroups_FORBID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tbl_FoldersReportGroups] ADD  CONSTRAINT [DF_tbl_FoldersReportGroups_FORBID]  DEFAULT ((0)) FOR [FORBID]
END


End
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbl_FoldersReportGroups_tbl_ReportsGroups]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportGroups]'))
ALTER TABLE [dbo].[tbl_FoldersReportGroups]  WITH CHECK ADD  CONSTRAINT [FK_tbl_FoldersReportGroups_tbl_ReportsGroups] FOREIGN KEY([ReportGroupId])
REFERENCES [dbo].[tbl_ReportsGroups] ([ReportGroupId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbl_FoldersReportGroups_tbl_ReportsGroups]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_FoldersReportGroups]'))
ALTER TABLE [dbo].[tbl_FoldersReportGroups] CHECK CONSTRAINT [FK_tbl_FoldersReportGroups_tbl_ReportsGroups]
GO