USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_JobReportsBtrees]    Script Date: 01/15/2013 15:11:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_JobReportsBtrees]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_JobReportsBtrees]
GO

USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_JobReportsBtrees]    Script Date: 01/15/2013 15:11:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_JobReportsBtrees](
	[JobReportId] [int] NOT NULL,
	[btree_id] [int] NOT NULL,
	[btree_io] [char](1) NOT NULL,
	[btree_name] [varchar](64) NOT NULL,
	[btree_meta_data] [varbinary](max) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

