//COPYS0  EXEC PGM=IEBCOPY                                              00000100
//SYSPRINT DD  SYSOUT=*                                                 00000200
//SYSUT1   DD  DISP=SHR,DSN=XREPORT.R010.AUTHLOAD                       00000300
//SYSUT2   DD  DISP=(,PASS),DCB=XREPORT.R010.AUTHLOAD,                  00000400
//         UNIT=SYSDA,SPACE=(TRK,(30,30,30))                            00000500
//SYSIN    DD  DUMMY                                                    00000600
//COPYS1  EXEC PGM=IEBCOPY                                              00000700
//SYSPRINT DD  SYSOUT=*                                                 00000800
//FROM     DD  DISP=OLD,DSN=*.COPYS0.SYSUT2                             00000900
//TO       DD  DISP=SHR,DSN=XREPORT.R010.AUTHLOAD                       00001000
//SYSIN    DD  *                                                        00001100
 COPY OUTDD=TO,INDD=FROM                                                00001200
 S M=((XRCLIENT,XRCLIEN$,R))                                            00001300
//ZAPSBUF EXEC PGM=AMASPZAP,REGION=2M                                   00001400
//SYSPRINT DD  SYSOUT=*                                                 00002000
//SYSLIB   DD  DISP=SHR,DSN=XREPORT.R010.AUTHLOAD                       00003000
//SYSIN    DD  *                                                        00004000
    NAME XRCLIENT XRCLIENT                                              00005000
    VER 0850 45E0B954                                                   00006001
    REP 0850 A7E88000                                                   00007001
    VER 0854 D20395A091F0                                               00008001
    REP 0854 50E095A00700                                               00009001
    VER 086E 45E0B954                                                   00009101
    REP 086E A7E88000                                                   00009201
    VER 0872 D20393C891F0                                               00009301
    REP 0872 50E093C80700                                               00009401
//*                                                                     00010000
//COPY1   EXEC PGM=IEBCOPY                                              00011000
//SYSPRINT DD  SYSOUT=*                                                 00012000
//SYSUT1   DD  DISP=SHR,DSN=XREPORT.R010.AUTHLOAD                       00013000
//SYSUT2   DD  DISP=(,PASS),DCB=XREPORT.R010.AUTHLOAD,                  00014000
//         UNIT=SYSDA,SPACE=(TRK,(30,30,30))                            00014100
//SYSIN    DD  DUMMY                                                    00014200
//COPY    EXEC PGM=IEBCOPY                                              00014300
//SYSPRINT DD  SYSOUT=*                                                 00014400
//FROM     DD  DISP=OLD,DSN=*.COPY1.SYSUT2                              00014500
//TO       DD  DISP=SHR,DSN=XREPORT.R010.AUTHLOAD                       00014600
//SYSIN    DD  *                                                        00014700
 COPY OUTDD=TO,INDD=FROM                                                00014800
 S M=((XRCLIENT,XRCL9516,R))                                            00014900
 COPY OUTDD=TO,INDD=FROM                                                00015000
 S M=((XRCLIENT,XRCL9517,R))                                            00016000
 COPY OUTDD=TO,INDD=FROM                                                00017000
 S M=((XRCLIENT,XRCL9518,R))                                            00018000
 COPY OUTDD=TO,INDD=FROM                                                00019000
 S M=((XRCLIENT,XRCL9519,R))                                            00019100
 COPY OUTDD=TO,INDD=FROM                                                00019200
 S M=((XRCLIENT,XRCL9520,R))                                            00019300
 COPY OUTDD=TO,INDD=FROM                                                00019400
 S M=((XRCLIENT,XRCL9521,R))                                            00019500
 COPY OUTDD=TO,INDD=FROM                                                00019600
 S M=((XRCLIENT,XRCL9522,R))                                            00019700
 COPY OUTDD=TO,INDD=FROM                                                00019800
 S M=((XRCLIENT,XRCL9523,R))                                            00019900
//LIST1   EXEC PGM=AMASPZAP,REGION=2M                                   00020000
//SYSPRINT DD  SYSOUT=*                                                 00030000
//SYSLIB   DD  DISP=SHR,DSN=XREPORT.R010.AUTHLOAD                       00040000
//SYSIN    DD  *                                                        00050000
    NAME XRCL9516 XRCLIENT                                              00060000
    VER 0024 252B                                                       00070000
    REP 0024 252C                                                       00080000
    NAME XRCL9517 XRCLIENT                                              00090000
    VER 0024 252B                                                       00100000
    REP 0024 252D                                                       00110000
    NAME XRCL9518 XRCLIENT                                              00120000
    VER 0024 252B                                                       00130000
    REP 0024 252E                                                       00140000
    NAME XRCL9519 XRCLIENT                                              00150000
    VER 0024 252B                                                       00160000
    REP 0024 252F                                                       00170000
    NAME XRCL9520 XRCLIENT                                              00180000
    VER 0024 252B                                                       00190000
    REP 0024 2530                                                       00200000
    NAME XRCL9521 XRCLIENT                                              00210000
    VER 0024 252B                                                       00220000
    REP 0024 2531                                                       00230000
    NAME XRCL9522 XRCLIENT                                              00240000
    VER 0024 252B                                                       00250000
    REP 0024 2532                                                       00260000
    NAME XRCL9523 XRCLIENT                                              00270000
    VER 0024 252B                                                       00280000
    REP 0024 2533                                                       00290000
