USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_LogicalReportNotes]    Script Date: 06/10/2013 13:13:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LogicalReportNotes]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_LogicalReportNotes]
GO

USE [ctdxreport]
GO

/****** Object:  Table [dbo].[tbl_LogicalReportNotes]    Script Date: 06/10/2013 13:13:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tbl_LogicalReportNotes](
    [JobReportId] [int] NOT NULL,
    [ReportId] [smallint] NOT NULL,
    [NoteId] [int] NOT NULL,
    [IsActive] [bit] NOT NULL,
    [LastUpdateTime] [datetime] NOT NULL,
    [AUTH_USER] [varchar](64) NOT NULL,
    [textString] [varchar](max) NULL,
 CONSTRAINT [PK_tbl_LogicalReportNotes] PRIMARY KEY CLUSTERED 
(
    [JobReportId] ASC,
    [ReportId] ASC,
    [NoteId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

