DROP TABLE [ctdxreport].[dbo].[tbl_ProfilesFoldersTrees_OLD]
GO

SELECT *
INTO  [ctdxreport].[dbo].[tbl_ProfilesFoldersTrees_OLD]
FROM  [ctdxreport].[dbo].[tbl_ProfilesFoldersTrees]
GO

TRUNCATE TABLE [ctdxreport].[dbo].[tbl_ProfilesFoldersTrees]
GO

INSERT INTO [ctdxreport].[dbo].[tbl_ProfilesFoldersTrees] ([ProfileName] ,[RootNode])
SELECT [ProfileName], [RootNode] FROM [ctdxreport].[dbo].[tbl_ProfilesFoldersTrees_temp]
GO

DROP TABLE [ctdxreport].[dbo].[tbl_BundlesNames_OLD]
GO

SELECT *
INTO  [ctdxreport].[dbo].[tbl_BundlesNames_OLD]
FROM  [ctdxreport].[dbo].[tbl_BundlesNames]
GO

TRUNCATE TABLE [ctdxreport].[dbo].[tbl_BundlesNames]
GO

INSERT INTO [ctdxreport].[dbo].[tbl_BundlesNames] ([BundleName]
           ,[BundleDescr]
           ,[BundleUdata]
           ,[BundleSkel]
           ,[BundleDest]
           ,[BundleUdest]
           ,[BundleWriter]
           ,[BundleForm]
           ,[BundleClass]
           ,[BundlePrtopt]
           ,[BundleOrder])
SELECT [BundleName]
           ,[BundleDescr]
           ,[BundleUdata]
           ,[BundleSkel]
           ,[BundleDest]
           ,[BundleUdest]
           ,[BundleWriter]
           ,[BundleForm]
           ,[BundleClass]
           ,[BundlePrtopt]
           ,[BundleOrder] FROM [ctdxreport].[dbo].[tbl_BundlesNames_temp]
GO

DROP TABLE [ctdxreport].[dbo].[tbl_Folders_OLD]
GO

SELECT *
INTO  [ctdxreport].[dbo].[tbl_Folders_OLD]
FROM  [ctdxreport].[dbo].[tbl_Folders]
GO

TRUNCATE TABLE [ctdxreport].[dbo].[tbl_Folders]
GO

INSERT INTO [ctdxreport].[dbo].[tbl_Folders] ([FolderName]
           ,[FolderDescr]
           ,[IsActive]
           ,[ParentFolder]
           ,[FolderAddr]
           ,[SpecialInstr])
SELECT [FolderName]
           ,[FolderDescr]
           ,[IsActive]
           ,[ParentFolder]
           ,[FolderAddr]
           ,[SpecialInstr] FROM [ctdxreport].[dbo].[tbl_Folders_temp]
GO

DROP TABLE [ctdxreport].[dbo].[tbl_FoldersRules_OLD]
GO

SELECT *
INTO  [ctdxreport].[dbo].[tbl_FoldersRules_OLD]
FROM  [ctdxreport].[dbo].[tbl_FoldersRules]
GO

TRUNCATE TABLE [ctdxreport].[dbo].[tbl_FoldersRules]
GO

INSERT INTO [ctdxreport].[dbo].[tbl_FoldersRules] ([FolderName]
           ,[ReportGroupId]
           ,[FORBID])
SELECT DISTINCT [FolderName]
           ,[ReportGroupId]
           ,[FORBID] FROM [ctdxreport].[dbo].[tbl_FoldersRules_temp]
GO

DROP TABLE [ctdxreport].[dbo].[tbl_NamedReportsGroups_OLD]
GO

SELECT *
INTO  [ctdxreport].[dbo].[tbl_NamedReportsGroups_OLD]
FROM  [ctdxreport].[dbo].[tbl_testNRG]
GO

TRUNCATE TABLE [ctdxreport].[dbo].[tbl_testNRG]
GO

INSERT INTO [ctdxreport].[dbo].[tbl_testNRG] ([ReportGroupId]
           ,[ReportRule]
           ,[FilterVar]
           ,[FilterRule]
           ,[RecipientRule])
SELECT DISTINCT [ReportGroupId]
           ,[ReportRule]
           ,[FilterVar]
           ,[FilterRule]
           ,[RecipientRule] FROM [ctdxreport].[dbo].[tbl_NamedReportsGroups_temp]
GO


DROP TABLE [ctdxreport].[dbo].[tbl_VarSetsValues_OLD]
GO

SELECT *
INTO  [ctdxreport].[dbo].[tbl_VarSetsValues_OLD]
FROM  [ctdxreport].[dbo].[tbl_testVSV]
GO

TRUNCATE TABLE [ctdxreport].[dbo].[tbl_testVSV]
GO

INSERT INTO [ctdxreport].[dbo].[tbl_testVSV] ([VarSetName]
           ,[VarName]
           ,[VarValue])
SELECT DISTINCT [VarSetName]
           ,[VarName]
           ,[VarValue] FROM [ctdxreport].[dbo].[tbl_VarSetsValues_temp]
GO

