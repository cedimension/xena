﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewTextFile.aspx.cs" meta:resourcekey="ViewTextFile" UICulture="auto" Culture="auto" Inherits="XReportSearch.ViewTextFile"  %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <title></title>
    <script >var multiHead = '<%=GetLocalResourceObject("Note").ToString() %>';
        var multiCanc = '<%=GetLocalResourceObject("Canc").ToString() %>';
        var multiSave = '<%=GetLocalResourceObject("Save").ToString() %>';
        var multiCreate = '<%=GetLocalResourceObject("Create").ToString() %>';

    </script>
    <script src="js/jquery-1.12.4.js"></script>
    <script src="js/ui/1.12.1/jquery-ui.js"></script>
    <script src="js/autosize.min.js"></script>

    <script src="js/variables.js"></script>

    <script src="js/accepted.js"></script>
    <script src="js/cancelled.js"></script>
    <script src="js/pending.js"></script>
    <script src="js/unconfirmed.js"></script>
    <script src="js/notes.js"></script>

    <script src="js/main.js"></script>
    
    <!--<link href="~/App_Themes/XReportSearchStyle.css?4" rel="stylesheet" type="text/css" />-->
    <link href="~/App_Themes/layout.css" rel="stylesheet" type="text/css" />
</head>
<body onload="">
    <form id="form1" runat="server">
        <!-- Static Draggable Image -->
        <!-- ASP.NET server image control -->
        <div class="headerLeftSmall">
            
        </div>
        <div class="headerContainer" style="width:100%;height:70px;background-color:#52A201;">
           
            <button type="button" title='<%=GetLocalResourceObject("Accepted").ToString() %>' class="accepted" style="vertical-align:middle; margin-left:30%;display:none;"><img src="Images/check.gif" style="width:24px;height:24px"/></button>
            <button type="button" title='<%=GetLocalResourceObject("Cancelled").ToString() %>' class="cancelled" style="vertical-align:middle;display:none;"><img src="Images/delete.gif" style="width:24px;height:24px"/></button>
            <button type="button" title='<%=GetLocalResourceObject("Pending").ToString() %>' class="pending"  style="vertical-align:middle;display:none;"><img src="Images/hourglass.gif" style="width:24px;height:24px"/></button>
            <button type="button" title='<%=GetLocalResourceObject("Unconfirmed").ToString() %>' class="unconfirmed"  style="vertical-align:middle;display:none;"><img src="Images/question_mark.gif" style="width:24px;height:24px"/></button>
            <button type="button" title='<%=GetLocalResourceObject("Save").ToString() %>' class="save" style="vertical-align:middle;display:none;margin-left:15px"><img src="Images/save.gif" style="width:24px;height:24px" /></button>
            <button type="button" title='<%=GetLocalResourceObject("Exit").ToString() %>' class="exit" style="vertical-align:middle;display:none;"><img src="Images/exit.gif" style="width:24px;height:24px" /></button>

            <button type="button" id="notes" style="float: right;margin:0.25% 0.5%">
                    <img src="Images/notes.gif" style="vertical-align:middle;width:30px;height:30px" />
                    <span style="font-size:inherit"><%=GetLocalResourceObject("Mode").ToString() %></span>
              
            </button>
        </div>

        <div class="sticky" style="width:auto;height:245px;display:none">
            <div style="margin:3px;">
                <button type="button" title='<%=GetLocalResourceObject("Accepted").ToString() %>' class="accepted" ><img src="Images/check.gif"  style="width:24px;height:24px"/></button>
            </div>
            <div style="margin:3px">
                <button type="button" title='<%=GetLocalResourceObject("Cancelled").ToString() %>' class="cancelled"><img src="Images/delete.gif"  style="width:24px;height:24px"/></button>
            </div>
            <div style="margin:3px">
                <button type="button" title='<%=GetLocalResourceObject("Pending").ToString() %>'  class="pending" ><img src="Images/hourglass.gif"  style="width:24px;height:24px"/></button>
            </div>
            <div style="margin:3px">
                <button type="button" title='<%=GetLocalResourceObject("Unconfirmed").ToString() %>' class="unconfirmed" ><img src="Images/question_mark.gif"  style="width:24px;height:24px"/></button>   
            </div>
            <div style="margin-top: 12px; margin-bottom: 3px; margin-left: 3px; margin-right: 3px">
                <button type="button" title='<%=GetLocalResourceObject("Save").ToString() %>' class="save" ><img src="Images/save.gif"  style="width:24px;height:24px" /></button>
            </div>
            <div style="margin: 3px 3px">
                <button type="button" title='<%=GetLocalResourceObject("Exit").ToString() %>' class="exit" ><img src="Images/exit.gif"  style="width:24px;height:24px" /></button>
            </div>
        </div>

        <div id="outer" style="width:100%">
            <div class="headerContainerTitleInner" id="user" style="float:right;height:22px;color:white;font-family:Verdana;font-size:11px;font-weight:bold"  value="<%=((XReportSearch.Business.User)Session["User"]).Username.ToString()%>" >Ambiente : <%=((XReportSearch.Business.User)Session["User"]).getGlobalEnv().ToString()%> User : <%=((XReportSearch.Business.User)Session["User"]).Username.ToString()%>      <%=Session["ViewParameters3"].ToString()%> </div>
            <!--<div id="loading"><img style='float:left;align-content:initial;visibility:visible;height:22px;' src='/Images/loadind.gif' alt='Loading...' /></div>-->
            <div style="display:table;margin: 0 auto;">
                <a href="#" id="bigprev" class="previous round" >&#8249;&#8249;</a>
                <a href="#" id="prev" class="previous round" >&#8249;</a>
                <input  type="text" id="pages" value="1" maxlength="<%=Session["ViewParameters1"].ToString().Split(';')[4].Length %>" size="1" /><%=Session["ViewParameters1"].ToString().Split(';')[4] %><a href="#" id="next" class="next round" >&#8250;</a><a href="#" id="bignext" class="next round" >&#8250;&#8250;</a>
            </div>
            <input  type="hidden" id="allpages" value ="<%=Session["ViewParameters1"].ToString().Split(';')[4]%>" />
            <div id="inner" class="text" style="float:left;height:auto;width:1225pt;border:1px solid black;font-family:'Lucida Console', 'Lucida Sans';">
            <textarea id="divtext" cols="55" rows="5" style="width:100%;height:100%;resize:vertical;" readonly="readonly"></textarea>
            </div>
        </div>
       
        <ul class='custom-menu'>
            <li data-action="remove"><%=GetLocalResourceObject("Removed").ToString() %></li>
            <li data-action="esc"><%=GetLocalResourceObject("Esc").ToString() %></li>
        </ul>

        <ul class='edit-menu'>
            <li data-action="remove"><%=GetLocalResourceObject("Removed").ToString() %></li>
            <li data-action="edit"><%=GetLocalResourceObject("Edit").ToString() %></li>
            <li data-action="esc"><%=GetLocalResourceObject("Exit").ToString() %></li>
        </ul>

        <ul class='comment-menu'>
            <li data-action="add"><%=GetLocalResourceObject("AddN").ToString() %></li>
            <li data-action="esc"><%=GetLocalResourceObject("Exit").ToString() %></li>
        </ul>
        <div class='confirm-menu'>
            <div class="header-conf">
                <label><%=GetLocalResourceObject("ServiceM").ToString() %></label>
            </div>
            <br />
            <div  style="margin-bottom:auto;vertical-align:middle;">
                <label class="messlabel"></label>
            </div>
            <div style="margin-bottom:auto;vertical-align:bottom;position:absolute;bottom:0;left: 50%;">
                <input type="button" id="cancmess" style="width:85px;height:25px;font-size:12px;margin-bottom:auto;transform: translate(-50%, -50%)" value="OK"/>
            </div>
        </div>
        <input  type="hidden" id="myUrl" value ="<%=ResolveUrl("~")%>" />
    </form>
</body>
</html>
