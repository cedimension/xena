﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Configuration;
using System.Web.Services.Protocols;
using XReportSearch.Utils;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;
using PlainElastic.Net.Queries;
using XReportSearch.Business;
using PlainElastic.Net;
using PlainElastic.Net.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.Script.Services;
using Microsoft.Web.Administration;

namespace XReportSearch
{
    public partial class SearchHistoryIndex : System.Web.UI.Page
    {
        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog log_ES = LogManager.GetLogger("ES_Log");
        #endregion

        #region Main Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.Validators != null && Page.Validators.Count > 0)
                ScriptManager.RegisterOnSubmitStatement(this, Page.GetType(), "", "fnOnUpdateValidators()");
            try
            {
                if (!Page.IsPostBack)
                {
                    LoadIndici();
                    //setta i due valori nell' indice
                    if (Navigation.ViewParameters != null)
                        ChooseIndici();
                    LoadDynamicControls();
                }
                else
                {
                    //Bind Grid unnecessary cause only problems with columns mapping (checked)
                    OnlyBindGridView();
                    //BindGridView();
                    LoadDynamicControls();
                }
            }
            catch (Exception Ex)
            {
                log.Error("Failed in loading ES indexes: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
            }
        }
        #endregion

        #region Private Methods
        private void LoadHistoryReports()
        {
            if (Navigation.Search != null)
            {
                string query = "";
                query = Navigation.Search.BuildQueryManual(this.ddl_type.SelectedValue.ToString(), int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]));
                string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
                int hostPort = int.Parse(ConfigurationManager.AppSettings["ElasticSearchHost_Port"]);
                var connection = new ElasticConnection(host, hostPort);
                string result = connection.Post(Commands.Search(Navigation.CurrentIndex, Navigation.CurrentType), query);
                
                Navigation.GridSource = CreateTableFromJSON(result);          
                BindGridView();
            }
        }

        private DataTable CreateTableFromJSON(string result)
        {
            JToken jt = JToken.Parse(result);
            string formattedRes = jt.ToString(Newtonsoft.Json.Formatting.Indented);
            log_ES.Debug("QUERY STRING LoadHistoryReports: " + formattedRes);
            var serializer = new JsonNetSerializer();
            RootObject root = serializer.Deserialize<RootObject>(result);
            Navigation.TotRowsEs = root.hits.total;
            if (root.hits.hits.Count > 0)
            {
                Hit h = root.hits.hits[0];
                
                DataTable dt = new DataTable();
                dt.Columns.Add("_id");
                dt.Columns.Add("_index");
                dt.Columns.Add("_score");
                dt.Columns.Add("_type");

                foreach (var key in ((IDictionary<string, object>)h._source).Keys)
                {
                    if (key.EndsWith("#"))
                        dt.Columns.Add(key.TrimEnd('#') + "_");
                    else
                        dt.Columns.Add(key);
                }

                foreach (Hit h1 in root.hits.hits)
                {
                    DataRow dr = dt.NewRow();
                    IDictionary<string, object> dic = (IDictionary<string, object>)h1._source;
                    foreach (KeyValuePair<string, object> o in dic)
                    {
                        if (o.Key.EndsWith("#"))
                            dr[o.Key.TrimEnd('#') + "_"] = o.Value;
                        else
                            dr[o.Key] = o.Value;
                    }
                    dr["_id"] = h1._id;
                    dr["_index"] = h1._index;
                    dr["_score"] = h1._score;
                    dr["_type"] = h1._type;
                    dt.Rows.Add(dr);
                }
                log_ES.Debug("COUNT RESULTSET LoadHistoryReports: " + root.hits.hits.Count);
                if (root.hits.total > 1000)
                {
                    this.lb_Popup.Text = "More than 1000 results found.";
                    this.ModalPopupExtender2.Show();
                }
                return dt;
            }
            else
                return null;
        }
        private bool isES_IndexChanged()
        {
            return Navigation.CurrentIndex != this.ddl_indice.SelectedValue || Navigation.CurrentType != this.ddl_type.SelectedValue;
        }

        private void ChooseIndici()
        {
            if (Navigation.ViewParameters != null)
                BindValues(ref this.ddl_indice, ref this.ddl_type, Navigation.ViewParameters);
        }

        private void LoadIndici()
        {
            if (Navigation.Indici == null)
                GetIndici();
            BindCombo(ref this.ddl_indice, Navigation.Indici);
        }

        private void GetIndici()
        {           
                string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
                int hostPort = int.Parse(ConfigurationManager.AppSettings["ElasticSearchHost_Port"]);
                var connection = new ElasticConnection(host, hostPort);
                string types = connection.Get(Commands.GetMapping("_all"));
                JToken jt = JToken.Parse(types);
                string formattedRes = jt.ToString(Newtonsoft.Json.Formatting.Indented);
                log_ES.Debug("JSON STRING GetIndici: " + formattedRes);
                var serializer = new JsonNetSerializer();
                IDictionary<string, IDictionary<string, object>> entryJ1 = serializer.Deserialize<IDictionary<string, IDictionary<string, object>>>(types);
                Navigation.Indici = entryJ1;
                log_ES.Debug("JSON STRING GetIndici: " + formattedRes);
        }

        private void OnlyBindGridView()
        {
            
            this.gv_search.DataSource = Navigation.GridSource;
            this.gv_search.PageSize = int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
            if (Navigation.CurrentGridPageES != -1)
                this.gv_search.PageIndex = Navigation.CurrentGridPageES;
            this.gv_search.RowCommand += new GridViewCommandEventHandler(gv_search_OnRowCommand);
            //this.gv_search.DataSource = Navigation.GridSource;
            if (Navigation.GridSource != null)
            {
                double pages = (double)Navigation.GridSource.Rows.Count / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                //double pages = (double)Navigation.TotRowsEs / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                int numPages = (int)Math.Ceiling(pages);
                this.gv_search.PagerSettings.FirstPageText = "1";
                this.gv_search.PagerSettings.LastPageText = "" + numPages + "";
                //this.gv_search.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                //this.gv_search.PagerSettings.PageButtonCount = numPages;
                this.gv_search.PagerSettings.Visible = true;
            }
            this.gv_search.DataBind();
        }

        /// <summary>
        /// void method for binding dataSource to Layout Html , in presence of indexes create dinamically Html columns to show data coherently
        /// </summary>
        private void BindGridView()
        {
            ///check Navigation.GridSource or gv_search.DataSource to see if contains fields with sharps
            ///could be move instruction this.gv_search.DataSource = Navigation.GridSource; before ?????
            /// yes we can check grid columns if cointains special characther #
            /// Utility.checkSpecial('#', this.gv_search)
            this.gv_search.DataSource = Navigation.GridSource;
            ///
            if (this.ddl_indice.SelectedIndex > 0 && this.ddl_type.SelectedIndex > 0)
            {
                IDictionary<string, object> dictionary = Navigation.Indici[this.ddl_indice.SelectedValue];
                JObject listaTipi = (JObject)dictionary["mappings"];          
                List<BoundField> fields = Utility.BuildGrid(listaTipi, this.ddl_type.SelectedValue, this.gv_search);
                int width = 0;
                //BoundField f = fields.Find(delegate(BoundField bf) { return bf.HeaderText == "Id"; });
                BoundField f = Utility.checkId("_id", this.gv_search);
                if (f == null && fields.Count > 0)
                    fields.Add(Utility.BuildBoundField("_id"));
                if (fields.Count > 0)
                    width = 65 / fields.Count;
                //check if add special fields 
                bool tocheck = Utility.checkSpecial("#", this.gv_search);
                //added before line 
                foreach (BoundField field in fields)
                {
                    if (field.HeaderText == "Report Name")
                    {
                        field.HeaderStyle.Width = Unit.Percentage(20);
                        field.ItemStyle.Width = Unit.Percentage(20);
                    }
                    else
                    {
                        field.HeaderStyle.Width = Unit.Percentage(width);
                        field.ItemStyle.Width = Unit.Percentage(width);
                    }
                    //added this if -->e.g. to manage incoerent index in elastic search (not all numeric fields have duplicate columns named with #)
                    if (!field.DataField.EndsWith("_") || (field.DataField.EndsWith("_") && tocheck))
                        this.gv_search.Columns.Add(field);
                }
            }


            this.gv_search.PageSize = int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
            if (Navigation.CurrentGridPageES != -1)
                this.gv_search.PageIndex = Navigation.CurrentGridPageES;
            this.gv_search.RowCommand += new GridViewCommandEventHandler(gv_search_OnRowCommand);
            //this.gv_search.DataSource = Navigation.GridSource;
            if (Navigation.GridSource != null)
            {
                double pages = (double)Navigation.GridSource.Rows.Count / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                //double pages = (double)Navigation.TotRowsEs / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                int numPages = (int)Math.Ceiling(pages);
                this.gv_search.PagerSettings.FirstPageText = "1";
                this.gv_search.PagerSettings.LastPageText = "" + numPages + "";
                //this.gv_search.PagerSettings.Mode = PagerButtons.NumericFirstLast;
                //this.gv_search.PagerSettings.PageButtonCount = numPages;
                this.gv_search.PagerSettings.Visible = true;
            }
            this.gv_search.DataBind();
        }

        private void LoadDynamicControls()
        {
            if (Navigation.DynamicFormControls != null)
            {
                foreach (HtmlTableRow row in Navigation.DynamicFormControls)
                {
                    this.ControlsTable.Rows.Remove(row);
                    row.Dispose();
                }
            }
            if (this.ddl_indice.SelectedIndex != -1 && this.ddl_type.SelectedIndex != -1)
            {
                IDictionary<string, object> dictionary = Navigation.Indici[Navigation.CurrentIndex];
                JObject listaTipi = (JObject)dictionary["mappings"];
                Navigation.Search = new Business.Search();
                Navigation.Search.fields = new List<Field>();

                //history_fields_to_block=job_name;report_name
                //makes read_only the fields job_name and report_name
                string history_fields_to_block = (ConfigurationManager.AppSettings["history_fields_to_block"] == null ? "" : ConfigurationManager.AppSettings["history_fields_to_block"]);

                List<HtmlTableRow> rows = Utility.BuildForm(listaTipi, this.ddl_type.SelectedValue, ref this.gv_search, history_fields_to_block);
                foreach (HtmlTableRow row in rows)
                    this.ControlsTable.Rows.Insert(this.ControlsTable.Rows.Count, row);
                Navigation.DynamicFormControls = rows;
            }
        }

        private void BindValues(ref AjaxControlToolkit.ComboBox comboBox, ref AjaxControlToolkit.ComboBox comboBoxTipi, string IndexValues)
        {
            if (IndexValues != "")
            {
                string[] valori = IndexValues.Split('-');
                string index = valori[0];
                string controld = valori[valori.Length - 1];
                string subindexes = IndexValues.Substring(index.Length + 1, (IndexValues.Length - index.Length - controld.Length - 2));
                comboBox.SelectedValue = index.ToLower() + "_" + controld.ToLower();
                Navigation.CurrentIndex = index.ToLower() + "_" + controld.ToLower();
                SetComboTipi();
                comboBoxTipi.SelectedValue = subindexes.Replace(",", "_");
                Navigation.CurrentType = comboBoxTipi.SelectedValue;
            }
        }

        private void SetComboTipi()
        {
            if (this.ddl_indice.SelectedIndex > 0)
            {
                Navigation.CurrentIndex = this.ddl_indice.SelectedValue;
                IDictionary<string, object> dictionary = Navigation.Indici[Navigation.CurrentIndex];
                JObject listaTipi = (JObject)dictionary["mappings"];
                BindComboTipi(ref this.ddl_type, listaTipi.Properties());
            }
        }

        private void BindCombo(ref AjaxControlToolkit.ComboBox comboBox, IDictionary<string, IDictionary<string, object>> dictionary)
        {
            dictionary = dictionary.OrderBy(x => x.Key.Substring(x.Key.Length - 2, 2)).
                ThenBy(x => x.Key).ToDictionary(x => x.Key, y => y.Value);
            comboBox.DataSource = dictionary;
            comboBox.DataValueField = "Key";
            comboBox.DataTextField = "Key";
            comboBox.DataBind();
            comboBox.Items.Insert(0, new ListItem("---Selezionare---", "0"));
        }

        private void BindComboTipi(ref AjaxControlToolkit.ComboBox comboBox, IEnumerable<JProperty> source)
        {
            source = source.OrderBy(x => x.Name);
            comboBox.DataSource = source;
            comboBox.DataValueField = "Name";
            comboBox.DataTextField = "Name";
            comboBox.DataBind();
            comboBox.Items.Insert(0, new ListItem("---Selezionare---", "0"));
        }

        private void ClearGrid()
        {
            this.gv_search.Columns.Clear();
            Navigation.CurrentGridPageES = 0;
            this.gv_search.DataSource = null;
            this.gv_search.DataBind();
            this.gv_search.Visible = false;
        }

        private void AddImageButton(GridViewRow row)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                ImageButton b = new ImageButton();
                b.ID = "b_doc";
                b.Attributes.Add("runat", "server");
                b.CommandName = "ViewFile";
                b.ImageUrl = "~/Images/List.png";
                b.ToolTip = "View File";
                b.Width = new Unit(20, UnitType.Pixel);
                b.Height = new Unit(20, UnitType.Pixel);
                b.Visible = true;
                TableCell cell = new TableCell();
                cell.Width = new Unit(5, UnitType.Percentage);
                cell.Controls.Add(b);
                row.Cells.AddAt(row.Cells.Count, cell);
            }
            else if (row.RowType == DataControlRowType.Header)
            {
                TableCell cell = new TableCell();
                cell.Width = new Unit(5, UnitType.Percentage);
                row.Cells.AddAt(row.Cells.Count, cell);
            }
        }

        private void SetAmbiente()
        {
            string controld = Navigation.CurrentIndex.Substring(this.ddl_indice.SelectedValue.Length - 2, 2);
            //this.lb_Ambiente.Text = "Ambiente:" + controld.ToUpper();
        }
        #endregion

        #region Events
        protected void gv_search_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Navigation.CurrentGridPageES = e.NewPageIndex;
            BindGridView();
        }

        protected void buttonElasticSearch_Click(object sender, EventArgs e)
        {
            this.ClearGrid();
            LoadHistoryReports();
            Navigation.Search = null;
            this.gv_search.Visible = true;
            Navigation.CurrentGridPageES = -1;
        }

        protected void ddl_indice_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetComboTipi();
            SetAmbiente();
            this.ClearGrid();
        }

        protected void ddl_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddl_type.SelectedIndex > 0)
            {
                Navigation.CurrentType = this.ddl_type.SelectedValue;
                this.ClearGrid();
            }
        }

        protected void gv_search_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            AddImageButton(e.Row);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView dr = (DataRowView)e.Row.DataItem;
                ImageButton b_pdf = (ImageButton)e.Row.FindControl("b_doc");
                string controld = this.ddl_indice.SelectedValue.Substring(this.ddl_indice.SelectedValue.Length - 2, 2);
                if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.BPM.ToString())
                    b_pdf.CommandArgument = dr["lotto_id"] + ";" + dr["_id"];
                else
                    b_pdf.CommandArgument = controld + ";" + dr["_id"] + ";" + dr["from_rba"] + ";" + dr["last_rba"] + ";" + dr["tot_pages"];
            }
        }

        protected void gv_search_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewFile")
            {
                Navigation.ViewParameters = e.CommandArgument.ToString();
                ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFileHistory.aspx');", true);
            }
        }
        #endregion

        #region WebMethods
        [WebMethod]
        public static string[] GetCompletionList(string prefixText, int count, string contextKey)
        {
            string query = Utility.BuildQueryAutocomplete(contextKey, prefixText);
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int hostPort = int.Parse(ConfigurationManager.AppSettings["ElasticSearchHost_Port"]);
            var connection = new ElasticConnection(host, hostPort);
            string result = connection.Post(Commands.Search(Navigation.CurrentIndex, Navigation.CurrentType), query);
            JToken jt = JToken.Parse(query);
            string formattedRes = jt.ToString(Newtonsoft.Json.Formatting.Indented);
            log_ES.Debug("QUERY STRING GetCompletionList (for autocomplete): " + formattedRes);
            jt = JToken.Parse(result);
            formattedRes = jt.ToString(Newtonsoft.Json.Formatting.Indented);
            log_ES.Debug("RES STRING GetCompletionList (for autocomplete): " + formattedRes);
            var serializer = new JsonNetSerializer();
            RootObject root = serializer.Deserialize<RootObject>(result);
            List<string> list = new List<string>();
            foreach (BucketLeaf t in root.aggregations.suggest.buckets)
                list.Add(t.key);
            return list.ToArray();
        }
        #endregion
    }
}