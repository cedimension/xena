﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using XReportSearch.Utils;
using System.Web.Services.Protocols;
using log4net;
using XReportSearch.Business;
using System.Text;
using System.Net;

namespace XReportSearch.Authentication
{
    public partial class Login : System.Web.UI.Page
    {
        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Costants
        public const String AUTH_SUCCESS = "SUCCESS";
        public const String ROOT = "ROOT";
        #endregion

        #region Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ConfigurationManager.AppSettings["DEBUG"].ToString() == "1")
            {
                if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.VITTORIA.ToString() || ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.GENERALI.ToString())
                {
                    Navigation.User = new User("xreport", "", "xreport", "", "");
                    Navigation.User.IsRoot = true;
                    Response.Redirect("~/StartPage.aspx");
                }
                else
                {
                    Navigation.User = new User("xreport", "SUPER", "X1NADM", "C0", "X1NADM");
                    Response.Redirect("~/StartPage.aspx");
                }
            }
            if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.VITTORIA.ToString() || ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.GENERALI.ToString())
                CheckAuthentication();
            else
            {
                this.butt.Visible = false;
                this.lb_err.Visible = true;
                this.lb_error.Visible = true;
                this.lb_err.Text = "Authentication failed.";
                this.lb_error.Text = "User not authorized.";
            }
           
        }

        private void CheckAuthentication()
        {
            if (Navigation.User != null)
            {
                Response.Redirect("~/StartPage.aspx");
            }
            else
            {
                XReportWebIface.DocumentData Data;
                string user = Request.ServerVariables["AUTH_USER"];
                string[] authUser = user.Split('\\');
                if (authUser.Length > 1)
                    user = authUser[1];
                log.Debug("AUTH USER: " + user + " waiting for authentication.");
                if (ConfigurationManager.AppSettings["DEBUG"].ToString() == "1")
                    user = "XREPORT";
                Data = BuildDocumentData(user);
                if (Authenticated(ref Data, user))
                {
                    log.Debug("AUTH USER: " + user + " authenticated");
                    Response.Redirect("~/StartPage.aspx");
                }
                else
                {
                    Navigation.User = null;
                    this.lb_err.Text = "Authentication failed.";
                    this.lb_error.Text = "User " + user + " is not defined in XReport.";
                    this.lb_err.Visible = true;
                    this.lb_error.Visible = true;
                    this.butt.Visible = false;
                    log.Debug("AUTH FAILED: User is not defined in XReport.");
                }
            }
        }

        private XReportWebIface.DocumentData BuildDocumentData(string user)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column UserName = new XReportWebIface.column();
                UserName.colname = "UserName";
                UserName.Value = user;
                XReportWebIface.column AuthMethod = new XReportWebIface.column();
                AuthMethod.colname = "AuthMethod";
                AuthMethod.Value = "";
                XReportWebIface.column ApplName = new XReportWebIface.column();
                ApplName.colname = "ApplName";
                ApplName.Value = ConfigurationManager.AppSettings["ApplicationName"].ToString();
                XReportWebIface.column Password = new XReportWebIface.column();
                Password.colname = "Password";
                Password.Value = "";

                Entry.Columns = new XReportWebIface.column[4] { UserName, AuthMethod, ApplName, Password };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "Authentication";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                throw SoapEx;
            }
            catch (WebException WebEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + WebEx.Message);
                throw WebEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                throw Ex;
            }
        }

        private bool Authenticated(ref XReportWebIface.DocumentData Data, string user)
        {
            try
            {
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface();
                XReportWebIFace.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                Data = XReportWebIFace.getUserConfig(Data);
                if (Data.IndexEntries != null && Data.IndexEntries.Length > 0 && Data.IndexEntries[0].Columns != null)
                {
                    XReportWebIface.column actionMessage = Data.IndexEntries[0].Columns.ToList().Find(delegate(XReportWebIface.column c) { return c.colname == "ActionMessage"; });
                    XReportWebIface.column isRoot = Data.IndexEntries[0].Columns.ToList().Find(delegate(XReportWebIface.column c) { return c.colname == "IsRoot"; });
                    if (actionMessage != null && actionMessage.Value.ToString().ToUpper() == AUTH_SUCCESS)
                    {
                        Navigation.User = new User(user, "", user, "", "");
                        if (isRoot != null && isRoot.Value.ToString().ToUpper() == ROOT)
                            Navigation.User.IsRoot = true;
                        else
                            Navigation.User.IsRoot = false;
                        return true;
                    }
                }
                return false;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Authenticated() Failed: " + SoapEx.Message);
                throw SoapEx;
            }
            catch (WebException WebEx)
            {
                log.Error("Authenticated() Failed: " + WebEx.Message);
                throw WebEx;
            }
            catch (Exception Ex)
            {
                log.Error("Authenticated() Failed: " + Ex.Message);
                throw Ex;
            }
        }

        protected void butt_Click(object sender, EventArgs e)
        {
            Encoding enc = new UTF8Encoding(true, true);
            Byte[] b = enc.GetBytes(ConfigurationManager.AppSettings["PGE_BACKURL"]);
            string s = enc.GetString(b);
            string url = "http://ufr-uj.collaudo.usinet.it/XA-PGE-PF/private/pgeStartApp.do?applicationToStart=token&pgeMenuId=start&pgeuidname=user&tokenString=token&pgeAppCode=X1N&writeInfo=y&backUrl=" + s;
            log.Debug("URL: " + url);
            ClientScript.RegisterStartupScript(this.Page.GetType(), "", "window.open('" + url + "');", true);
        }
        #endregion
    }
}