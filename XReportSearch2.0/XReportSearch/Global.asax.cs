﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using log4net;
using XReportSearch.Utils;

namespace XReportSearch
{
    public class Global : System.Web.HttpApplication
    {
        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog log_ES = LogManager.GetLogger("ES_Log");
        #endregion

        #region Main Events
        protected void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Debug("********************************************* APPLICATION STARTED *********************************************");
            Server.ClearError();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            string url = "null";
            string servername = "null";
            try {
                url = HttpContext.Current.Request.Url.AbsoluteUri;
                servername = System.Environment.MachineName;
            }
            catch { }

            //log.Debug("********************************************* SESSION STARTED - SESSION ID " + Session.SessionID + " *********************************************");
            log.Debug("********************************************* SESSION STARTED - SESSION ID " + Session.SessionID + " ******** " + url + " ************* " + servername+ " ************************");
            Session.Timeout = 240;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (ex != null)
            {
                if (XReportSearch.Utils.Navigation.Error != null)
                    if (ex != null)
                        XReportSearch.Utils.Navigation.Error = ex.Message + " - " + ex.InnerException + " - " + Request.Url.ToString();
                Server.ClearError();
                if (Navigation.User != null)
                {
                    log.Debug("Global.asax Error: " + XReportSearch.Utils.Navigation.Error);
                }
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {
            log.Debug("********************************************* SESSION END - SESSION ID " + Session.SessionID + " *********************************************");
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
        #endregion
    }
}