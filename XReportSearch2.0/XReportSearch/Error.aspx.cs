﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace XReportSearch
{
    public partial class Error : System.Web.UI.Page
    {
        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            string forceCloseSession = Request.Params["forceCloseSession"];
            if (forceCloseSession != null )
            {
                string username = "N/D";
                if (XReportSearch.Utils.Navigation.User != null)
                {
                    username = XReportSearch.Utils.Navigation.User.Username;
                }
                Session.Clear();
                log.Info("------------------Session deleted for user: " + username);
                string js = "javascript: window.close();";
                ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", js, true);
                return;
            }


            log.Info("Error.aspx: " + XReportSearch.Utils.Navigation.Error);
            this.err.Text = XReportSearch.Utils.Navigation.Error;

            string disableReload = Request.Params["disableReload"];
            if (disableReload != null && (disableReload == "YES" || disableReload == "Y"))
            {
                b_refresh.Visible = false;
                b_refresh.Enabled = false;
            }
            string enableClose = Request.Params["enableClose"];
            if (enableClose != null && (enableClose == "YES" || enableClose == "Y"))
            {
                b_close.Visible = true;
                b_close.Enabled = true;
            }
            else
            {
                b_close.Visible = false;
                b_close.Enabled = false;
            }

        }

        protected void b_refresh_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/StartPage.aspx");
        }

        protected void b_close_Click(object sender, EventArgs e)
        {
            Session.Clear();
            string js = "javascript: window.close();";
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", js, true);
        }

    }
}