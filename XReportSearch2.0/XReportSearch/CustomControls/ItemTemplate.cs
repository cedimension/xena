﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XReportSearch.Business;
using System.ComponentModel;

namespace CustomControls
{
    public class ItemTemplate : ITemplate
    {
        private ListItemType myListItemType;
        private string name;

        public ItemTemplate()
        {

        }

        public ItemTemplate(ListItemType Item, string name)
        {
            myListItemType = Item;
            this.name = name;
        }

        void ITemplate.InstantiateIn(System.Web.UI.Control container) 
        {
            if (myListItemType == ListItemType.Item)
            {
                Label data = new Label();
                data.ID = name;
                container.Controls.Add(data);
            }
        }
    }
}