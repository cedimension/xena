﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CustomControls
{
    public class CeTreeNode : TreeNode
    {
        protected override void RenderPreText(HtmlTextWriter writer)
        {

            writer.Write("<div >");
            base.RenderPreText(writer);
        }

        public CeTreeNode()
        {
            //this.Text = label;
            //this.Value = value;
            //base.TreeNode();

        }

        public CeTreeNode(string label)
        {
            this.Text = label;
            //this.Value = value;
            
        }

        protected override void RenderPostText(HtmlTextWriter writer)
        {
            writer.Write("</div>");
            base.RenderPostText(writer);
        }

    }

}