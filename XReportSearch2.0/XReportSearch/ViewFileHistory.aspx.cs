﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Configuration;
using System.Web.Services.Protocols;
using XReportSearch.Utils;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;

namespace XReportSearch
{
    public partial class ViewFileHistory : System.Web.UI.Page
    {
        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.BPM.ToString())
                LoadFileBPM();
            else
                LoadFileHistory();
        }

        private void LoadFileBPM()
        {
            if (Navigation.ViewParameters != null && Navigation.ViewParameters != "")
            {
                string[] pars = Navigation.ViewParameters.Split(';');
                string url = ConfigurationManager.AppSettings["DocDirectory"] + "/" + pars[0] + "/" + pars[1] + ".pdf";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                MemoryStream ms = new MemoryStream();
                response.GetResponseStream().CopyTo(ms);

                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "text/html";
                Response.AddHeader("Content-Disposition", url);
                Response.BinaryWrite(ms.ToArray());
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                //Response.End();
                ms.Dispose();
            }
        }

        private void LoadFileHistory()
        {
            try
            {
                string parUrl = "";
                if (Navigation.ViewParameters != null && Navigation.ViewParameters != "")
                {
                    string[] pars = Navigation.ViewParameters.Split(';');
                    parUrl = "?" + pars[0] + "/" + pars[1] + "/" + pars[2] + "/" + pars[3] + "/" + pars[4];

                    string cgiURL = ConfigurationManager.AppSettings["UrlFastCGI"] + parUrl;
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(cgiURL);
                    request.Method = "GET";
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    
                    MemoryStream ms = new MemoryStream();
                    response.GetResponseStream().CopyTo(ms);

                    Response.ClearHeaders();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.ContentType = "text/html";
                    Response.AddHeader("Content-Disposition", cgiURL);
                    Response.BinaryWrite(ms.ToArray());
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    ms.Dispose();
                }
            }
            catch (Exception ex)
            {
                log.Error("LoadFileHistory() Failed: " + ex.Message);
                Navigation.Error = ex.Message;
                this.l_error.Visible = true;
                this.l_error.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                //throw ex;
            }
        }

        public void MakeWebRequestAsync()
        {
            try
            {
                if (Navigation.RequestState != null)
                    Navigation.RequestState.AbortRequest();
                if (Navigation.ViewParameters != null && Navigation.ViewParameters != "")
                {
                    string[] pars = Navigation.ViewParameters.Split(';');
                    string parUrl = "?" + pars[0] + "/" + pars[1] + "/" + pars[2] + "/" + pars[3] + "/" + pars[4];
                    string cgiURL = ConfigurationManager.AppSettings["UrlFastCGI"] + parUrl;

                    RequestState state = new RequestState();
                    state.AsyncCall(cgiURL);
                    Navigation.RequestState = state;
                    while (true)
                    {
                        if (state.stream != "")
                        {
                            Response.ClearHeaders();
                            Response.ClearContent();
                            Response.Buffer = true;
                            Response.ContentType = "text/html";
                            Response.AddHeader("Content-Disposition", cgiURL);
                            Response.Write(state.stream);
                            break;
                        }
                    }
                    Navigation.RequestState = null;
                }
            }
            catch (Exception ex)
            {
                log.Error("MakeWebRequestAsync() Failed: " + ex.Message);
                Navigation.Error = ex.Message;
                this.l_error.Visible = true;
                this.l_error.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                //throw ex;
            }
        }
        #endregion
    }
}