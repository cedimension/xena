﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Configuration;
using System.Web.Services.Protocols;
using XReportSearch.Utils;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;
using PlainElastic.Net.Queries;
using XReportSearch.Business;
using PlainElastic.Net;
using PlainElastic.Net.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Services;
using Microsoft.Web.Administration;
using XReportSearch.XReportWebIface;
using System.ComponentModel;

namespace XReportSearch
{
    public partial class SearchHistory : System.Web.UI.UserControl
    {
        #region Constants
        private string SORT_DESCENDING = "DESC";
        private string SORT_ASCENDING = "ASC";
        #endregion

        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Main Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Page.Validators != null && Page.Validators.Count > 0)
                {
                    ScriptManager.RegisterOnSubmitStatement(this, Page.GetType(), "", "fnOnUpdateValidatorsH()");
                }
                this.sh_l_prof.Text = ConfigurationManager.AppSettings["ProfileLbText"];
                Navigation.TableSourceH = null;

                this.sh_tb_jobname2.Enabled = true;
                if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.UNICREDIT.ToString())
                {
                    //this.sh_tb_jobname2.Text = Navigation.User.Torre;
                    this.sh_tb_jobname2.Text = "";
                    this.sh_tb_jobname2.Visible = false;
                    this.sh_tb_jobname2.Enabled = false;
                    this.sh_tb_jobname.Width = this.sh_tb_jobnumber.Width;


                        this.sh_tb_prof.Enabled = true;
                    //if (Navigation.User.CodAuthor.Equals("X1NADM", StringComparison.OrdinalIgnoreCase) || Navigation.User.CodAuthor.Equals("X1NOPR", StringComparison.OrdinalIgnoreCase))
                    //{
                    //    //this.sh_tb_jobname2.Enabled = true;
                    //    this.sh_tb_prof.Enabled = true;
                    //}
                    //else
                    //{
                    //    //this.sh_tb_jobname2.Enabled = false;
                    //    this.sh_l_prof.Visible = false;
                    //    this.sh_tb_prof.Enabled = false;
                    //    this.sh_tb_prof.Visible = false;
                    //}
                }
                if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.VITTORIA.ToString() || ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.GENERALI.ToString())
                {
                    if (Navigation.User.IsRoot)
                    {
                        this.sh_l_specUser.Visible = true;
                        this.sh_tb_specUser.Visible = true;
                        this.sh_l_ambiente.Visible = false;
                        this.sh_ddl_ambiente.Visible = false;
                    }
                }
            }
            else
            {
                if (this.sh_hiddenIndex.Value == "1")
                    this.CreateLinkButtons();
            }
        }
        #endregion

        #region Private Methods
        private XReportWebIface.DocumentData BuildDocumentData(string RepName, string JRName, string JRName2, string JobNumber, string date, string date2, string recipient, string ambiente)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column ReportName = new XReportWebIface.column();
                ReportName.colname = "ReportName";
                if (RepName != null && RepName != "")
                    ReportName.Value = RepName + "%";
                else
                    ReportName.Value = RepName;
                XReportWebIface.column JobName = new XReportWebIface.column();
                JobName.colname = "JobName";
                if ((JRName != null && JRName != "") || (JRName2 != null && JRName2 != ""))
                    JobName.Value = JRName + "%" + JRName2;
                else
                    JobName.Value = "";
                XReportWebIface.column JobNum = new XReportWebIface.column();
                JobNum.colname = "JobNumber";
                if (JobNumber != null && JobNumber != "")
                    JobNum.Value = JobNumber + "%";
                else
                    JobNum.Value = JobNumber;
                XReportWebIface.column XferStartTime = new XReportWebIface.column();
                XferStartTime.colname = "UserTimeRef";
                XferStartTime.operation = "date";
                DateTime dt = DateTime.ParseExact(date, "dd/MM/yyyy", null);
                XferStartTime.Min = dt.ToString("yyyyMMdd");
                dt = DateTime.ParseExact(date2, "dd/MM/yyyy", null);
                XferStartTime.Max = dt.ToString("yyyyMMdd");

                XReportWebIface.column User = new XReportWebIface.column();
                User.colname = "User";
                if (this.sh_tb_specUser.Text != "")
                    User.Value = this.sh_tb_specUser.Text;
                else if (Navigation.User.Profilo != null && Navigation.User.Profilo != "")
                    User.Value = Navigation.User.Profilo;
                else
                    User.Value = "";
                XReportWebIface.column Recipient = new XReportWebIface.column();
                Recipient.colname = "Recipient";
                Recipient.Value = recipient;
                if (recipient != null && recipient != "")
                    Recipient.Value = recipient + "%";
                else
                    Recipient.Value = "";
                XReportWebIface.column Ambiente = new XReportWebIface.column();
                Ambiente.colname = "Ambiente";
                Ambiente.Value = ambiente;
                XReportWebIface.column CurrentRow = new XReportWebIface.column();
                CurrentRow.colname = "CurrentRow";
                CurrentRow.Value = "0";

                Entry.Columns = new XReportWebIface.column[8] { ReportName, JobName, XferStartTime, JobNum, User, Ambiente, CurrentRow, Recipient };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.sh_lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.sh_ModalPopupExtender2.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.sh_lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.sh_ModalPopupExtender2.Show();
                return null;
                //throw Ex;
            }
        }

        private XReportWebIface.DocumentData BuildDocumentDataPDF(string JRID, string FromPag)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column JobRepID = new XReportWebIface.column();
                JobRepID.colname = "JobReportId";
                JobRepID.Value = JRID;
                XReportWebIface.column FromPage = new XReportWebIface.column();
                FromPage.colname = "FromPage";
                FromPage.Value = FromPag;

                Entry.Columns = new XReportWebIface.column[2] { JobRepID, FromPage };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.sh_lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.sh_ModalPopupExtender2.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.sh_lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.sh_ModalPopupExtender2.Show();
                return null;
                //throw Ex;
            }
        }

        private XReportWebIface.DocumentData BuildDocumentDataNotes(string JobRepID, string RepID, string version, string UpdUser, string text, Utility.Operation operation)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column JobReportID = new XReportWebIface.column();
                JobReportID.colname = "JobReportID";
                JobReportID.Value = JobRepID;
                XReportWebIface.column ReportID = new XReportWebIface.column();
                ReportID.colname = "ReportID";
                ReportID.Value = RepID;
                XReportWebIface.column Version = new XReportWebIface.column();
                Version.colname = "Version";
                Version.Value = version;
                XReportWebIface.column UpdateUser = new XReportWebIface.column();
                UpdateUser.colname = "UpdateUser";
                UpdateUser.Value = UpdUser;
                XReportWebIface.column Text = new XReportWebIface.column();
                Text.colname = "Text";
                Text.Value = text;

                Entry.Columns = new XReportWebIface.column[5] { JobReportID, ReportID, Version, UpdateUser, Text };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.sh_lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.sh_ModalPopupExtender2.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.sh_lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.sh_ModalPopupExtender2.Show();
                return null;
                //throw Ex;
            }
        }

        private XReportWebIface.DocumentData BuildDocumentDataCheck(string JRID, string ReportID, string CheckUser)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column JobRepID = new XReportWebIface.column();
                JobRepID.colname = "JobReportID";
                JobRepID.Value = JRID;
                XReportWebIface.column RepID = new XReportWebIface.column();
                RepID.colname = "ReportID";
                RepID.Value = ReportID;
                XReportWebIface.column User = new XReportWebIface.column();
                User.colname = "CheckUser";
                User.Value = CheckUser;

                Entry.Columns = new XReportWebIface.column[3] { JobRepID, RepID, User };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.sh_lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.sh_ModalPopupExtender2.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.sh_lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.sh_ModalPopupExtender2.Show();
                return null;
                //throw Ex;
            }
        }

        public void LoadReports(string RepName, string JRName, string JRName2, string JobNumber, string date, string date2, string recipient, string ambiente)
        {
            try
            {
                log.Debug("LoadReports() Started");
                XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                //XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface();
                //XReportWebIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.DocumentData Data = BuildDocumentData(RepName, JRName, JRName2, JobNumber, date, date2, recipient, ambiente);
                XReportWebIface.Timeout = 600000;
                Data = XReportWebIface.getHistoryReportsData(Data);
                Navigation.TableSourceH = Utility.IndexEntriesToTable(Data.IndexEntries);
                BindGridView();
                if (Navigation.TableSourceH != null && Navigation.TableSourceH.Rows.Count >= 1000)
                {
                    this.sh_lb_Popup.Text = "More than 1000 results found.";
                    this.sh_ModalPopupExtender2.Show();
                }
                log.Debug("LoadReports() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadReports() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.sh_lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.sh_ModalPopupExtender2.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadReports() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.sh_lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.sh_ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        private void LoadPDF(string JRID, string FromPage)
        {
            try
            {
                log.Debug("LoadPDF() Started");
                //XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface();
                //XReportWebIFace.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                XReportWebIFace.Timeout = 600000;
                XReportWebIface.DocumentData Data = BuildDocumentDataPDF(JRID, FromPage);
                Data = XReportWebIFace.getJobReportPdf(Data);
                Navigation.ViewParameters = Data.FileName;
                log.Debug("LoadPDF() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadPDF() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.sh_lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.sh_ModalPopupExtender2.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadPDF() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.sh_lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.sh_ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        private void BindGridView()
        {
            int numPages = 0;
            if (Navigation.TableSourceH != null)
            {
                double pages = (double)Navigation.TableSourceH.Rows.Count / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                numPages = (int)Math.Ceiling(pages);
            }

            this.sh_gv_searchH.PageSize = int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
            if (Navigation.CurrentGridPageH != -1)
                this.sh_gv_searchH.PageIndex = Navigation.CurrentGridPageH;
            this.sh_gv_searchH.DataSource = Navigation.TableSourceH;
            this.sh_gv_searchH.Visible = true;
            this.sh_gv_searchH.PagerSettings.FirstPageText = "1";
            this.sh_gv_searchH.PagerSettings.LastPageText = "" + numPages + "";
            this.sh_gv_searchH.DataBind();
        }

        private void CreateLinkButtons()
        {
            string[] indexes = Navigation.ViewParameters.Split(';');
            for (int i = 0; i < indexes.Length - 1; i++)
            {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell = new HtmlTableCell();
                System.Web.UI.WebControls.LinkButton btn = new System.Web.UI.WebControls.LinkButton();
                btn.ID = "sh_btnLink_" + i;
                btn.Text = indexes[i];
                btn.CssClass = "linkButton";
                btn.Click += new EventHandler(sh_btn_Click);
                cell.Controls.Add(btn);
                row.Cells.Add(cell);
                this.sh_TablePopup.Rows.Add(row);
            }
        }

        private void ClearLinkButton()
        {
            this.sh_hiddenIndex.Value = "0";
            int[] rowInd = new int[this.sh_TablePopup.Rows.Count];
            for (int i = 2; i < this.sh_TablePopup.Rows.Count - 1; i++)
                rowInd[i] = i;
            for (int i = this.sh_TablePopup.Rows.Count - 1; i > 2; i--)
            {
                HtmlTableRow row = this.sh_TablePopup.Rows[i];
                this.sh_TablePopup.Rows.Remove(row);
                row.Controls.Clear();
                row.Dispose();
            }
        }
        #endregion

        #region Events

        protected void grid_Sorting(object sender, GridViewSortEventArgs e)
        {
            Navigation.CurrentSortExpressionH = e.SortExpression;
            if (Navigation.CurrentSortDirectionH == null)
                Navigation.CurrentSortDirectionH = SORT_DESCENDING;

            if (Navigation.CurrentSortDirectionH.ToString() == SORT_ASCENDING)
                Navigation.CurrentSortDirectionH = SORT_DESCENDING;
            else
                Navigation.CurrentSortDirectionH = SORT_ASCENDING;
            BindGridView();
        }

        protected void sh_buttonSearch_Click(object sender, EventArgs e)
        {
            Navigation.CurrentGridPageH = 0;
            LoadReports(this.sh_tb_name.Text, this.sh_tb_jobname.Text, this.sh_tb_jobname2.Text, this.sh_tb_jobnumber.Text, this.sh_tb_data1.Text, this.sh_tb_data2.Text, this.sh_tb_prof.Text, this.sh_ddl_ambiente.SelectedValue);
        }

        protected void sh_gv_searchH_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Navigation.CurrentGridPageH = e.NewPageIndex;
            BindGridView();
        }

        protected void sh_gv_searchH_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView view = (DataRowView)e.Row.DataItem;

                ImageButton b_pdf = (ImageButton)e.Row.FindControl("sh_b_pdf");
                ImageButton b_index = (ImageButton)e.Row.FindControl("sh_b_index");
                string restored = view["Restored"].ToString();
                string indexed = view["Indexed"].ToString();
                string recipient = view["Recipient"].ToString();
                string controld = this.sh_ddl_ambiente.SelectedValue.ToString();
                string restore_id = view["restore_id"].ToString();
                string firstRBA = view["firstRBA"].ToString();
                string last_rba = view["lastRBA"].ToString();
                string tot_pages = view["totpages2"].ToString();
                string time_elab = view["UserTimeElab"].ToString();
                b_pdf.CommandArgument = controld + ";" + restore_id + "/1.1" + ";" + firstRBA + ";" + last_rba + ";" + tot_pages;
                if (restored == "0" || restored == "")
                {
                    b_pdf.ImageUrl = "~/Images/refresh.ico";
                    b_pdf.ToolTip = "Not Yet Restored";
                    b_pdf.Enabled = false;
                    b_index.Enabled = false;
                }
                else if (recipient == "")
                {
                    b_pdf.Visible = false;
                }
                if (indexed == "")
                {
                    b_index.ImageUrl = "";
                    b_index.ToolTip = "";
                    b_index.Enabled = false;
                    b_index.Visible = false;
                }
                else
                {
                    string jobName = view["JobName"].ToString();
                    string reportName = view["ReportName"].ToString();
                    b_index.CommandArgument = indexed + "/" + jobName + "," + reportName + "," + time_elab;
                }
            }
        }

        protected void sh_gv_searchH_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewFile")
            {
                Navigation.ViewParameters = e.CommandArgument.ToString();
                ((StartPage)this.Page).OpenViewFileHistory();
            }
            else if (e.CommandName == "Index")
            {
                string[] indexes = e.CommandArgument.ToString().Split('/');
                Navigation.ViewParameters2 = indexes[1];
                if (indexes.Length == 2)
                {
                    Navigation.ViewParameters = indexes[0] + "-" + this.sh_ddl_ambiente.SelectedValue;
                    this.sh_myFrame.Attributes.Add("src", "SearchHistoryIndex.aspx");
                    this.sh_programmaticModalPopup.Show();
                }
                else if (indexes.Length > 2)
                {
                    Navigation.ViewParameters = e.CommandArgument.ToString();
                    CreateLinkButtons();
                    this.sh_hiddenIndex.Value = "1";
                    this.sh_cmdExt.Show();
                }
            }
            else if (e.CommandName == "Check")
            {
            }
        }

        protected void sh_btn_Click(object sender, EventArgs e)
        {
            ClearLinkButton();
            this.sh_cmdExt.Hide();
            Navigation.ViewParameters = ((LinkButton)sender).Text + "-" + this.sh_ddl_ambiente.SelectedValue;
            this.sh_myFrame.Attributes.Add("src", "SearchHistoryIndex.aspx");
            this.sh_programmaticModalPopup.Show();
        }

        protected void sh_siPopup_Click(object sender, EventArgs e)
        {
            this.sh_ModalPopupExtender2.Hide();
        }

        protected void sh_b_yes_Click(object sender, EventArgs e)
        {

        }

        protected void sh_b_close_Click(object sender, EventArgs e)
        {
            this.sh_programmaticModalPopup.Hide();
        }

        protected void sh_b_close2_Click(object sender, EventArgs e)
        {
            ClearLinkButton();
            this.sh_cmdExt.Hide();
        }

        protected void sh_b_close3_Click(object sender, EventArgs e)
        {

        }

        protected void sh_currPopup_Click(object sender, EventArgs e)
        {

        }

        protected void sh_allPopup_Click(object sender, EventArgs e)
        {

        }

        protected void Resize(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "resizeH();", true);
            if (this.sp_txtHidData.Value != "")
                this.sh_programmaticPopup.Height = new Unit(double.Parse(this.sp_txtHidData.Value) - 100, UnitType.Pixel);
            if (this.sp_txtHidData2.Value != "")
                this.sh_programmaticPopup.Width = new Unit(double.Parse(this.sp_txtHidData2.Value) - 100, UnitType.Pixel);
        }
        #endregion
    }
}