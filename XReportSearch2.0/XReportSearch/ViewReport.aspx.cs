﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;
using System.Web.UI;
using System.Web.UI.WebControls;
using XReportSearch.Utils;
using XReportSearch.Business;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using System.Globalization;

namespace XReportSearch
{
    public partial class ViewReport : System.Web.UI.Page
    //public partial class ViewReport : System.Web.UI.UserControl
    {


        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog log_ES = LogManager.GetLogger("ES_Log");
        #endregion

        #region Constants
        private const string AUSTRIAN_ENV = "BA";
        #endregion

        #region Methods

        private bool isDebugActive = true;

        private DataTable globalTableSource;
        private int globalCurrentGridPage;

        
        //private bool isDebugActive = false;

        protected void Page_Load(object sender, EventArgs e)
        { 
            if (Page.IsPostBack)
                return;
            //b_close.Visible = true;
            //b_close.Enabled = true;  
            try
            { 

                if( 
                    (Request.Params["twr"] != null)
                    && (!Request.Params["twr"].Equals(""))
                    && (Request.Params["effective_twr"] != null)
                    && (!Request.Params["effective_twr"].Equals(""))
                    && (Request.Params["key"] != null)
                    && (!Request.Params["key"].Equals("")) 
                    )

                //if (
                //    (Request.Params["jobreportid"] != null)
                //    && (!Request.Params["jobreportid"].Equals(""))
                //    && (Request.Params["reportid"] != null)
                //    && (!Request.Params["reportid"].Equals(""))
                //    && (Request.Params["pages"] != null)
                //    && (!Request.Params["pages"].Equals(""))
                //    && (Request.Params["frompage"] != null)
                //    && (!Request.Params["frompage"].Equals(""))
                //    && (Request.Params["totpages"] != null)
                //    && (!Request.Params["totpages"].Equals(""))
                //    && (Request.Params["format"] != null)
                //    && (!Request.Params["format"].Equals(""))
                //    && (Request.Params["reportname"] != null)
                //    && (!Request.Params["reportname"].Equals(""))
                //    )
                {
                    string jobreportid ="";
                    string reportid = "";
                    string pages = "";
                    string frompage = "";
                    string totpages = "";
                    string format = "";
                    string reportname = "";
                    
                    string key = Request.Params["key"];
                    //string decodedkeyvalue = HttpUtility.UrlDecode(key);
                    //string[] trueparameters = decryptedvalue.Split('&');
                    string convertedvalue = key.Replace(" ", "+"); 
                    if( ! Signature.ValidateToken(convertedvalue) )
                    {
                        throw new Exception("Link wrong or expired.");
                    }
                    string decryptedvalue = Signature.GetToken(convertedvalue).Value;

                    //throw new Exception("FORZATURA");
                    log.Info("List Parameters decrypted " + decryptedvalue);
                    string[] trueparameters = decryptedvalue.Split('&'); 
                    foreach(string element in trueparameters)
                    {
                        string[] keyvalues = element.Split('=');
                        if ((keyvalues != null) && (keyvalues.Length == 2))
                        {
                            switch (keyvalues[0])
                            {
                                case "jobreportid":
                                    jobreportid = keyvalues[1];
                                    break;
                                case "reportid":
                                    reportid = keyvalues[1];
                                    break;
                                case "pages":
                                    pages = keyvalues[1];
                                    break;
                                case "frompage":
                                    frompage = keyvalues[1];
                                    break;
                                case "totpages":
                                    totpages = keyvalues[1];
                                    break;
                                case "format":
                                    format = keyvalues[1];
                                    break;
                                case "reportname":
                                    reportname = keyvalues[1];
                                    break;
                            }  
                        }
                    }
                    log.Info("List Parameters separated " + jobreportid + " " + reportid + " " + pages + " " + pages + " " + totpages + " " + format+" "+reportname);
                    SetUser();
                    
                    
                    LoadReports_SP(jobreportid, reportid);

                    if ((globalTableSource != null) && (globalTableSource.Rows.Count > 0))
                    {
                        DataRow view = globalTableSource.Rows[0];
                        //string ListOfPages = view["ListOfPages"].ToString();
                        //string jid = view["JobReport_ID"].ToString();
                        //string id = view["ReportId"].ToString();
                        //string name = view["ReportName"].ToString();
                        //string pages = view["ListOfPages"].ToString();
                        //string fromPage = view["FromPage"].ToString();
                        //string typeMap = view["ExistTypeMap"].ToString();
                        string userTimeRef = view["UserTimeRef"].ToString();
                        string xferStartTime = view["XferStartTime"].ToString();
                        //string reportName = view["ReportName"].ToString();
                        //string totPages = view["TotPages"].ToString();
                        //string remark = view["Remark"].ToString();
                        int totPages = Int32.Parse(view["TotPages"].ToString());
                        //bool checkFlag = false;
                        //bool checkStatus = false;
                        //bool hasIndex = false;
                        //if (view["CheckFlag"] != null && view["CheckFlag"].ToString() != "")
                        //    checkFlag = bool.Parse(view["CheckFlag"].ToString());//Utility.ConvertStringToBool(view["CheckFlag"].ToString());
                        //if (view["CheckStatus"] != null && view["CheckStatus"].ToString() != "")
                        //    checkStatus = bool.Parse(view["CheckStatus"].ToString()); //Utility.ConvertStringToBool(view["CheckStatus"].ToString());
                        //if (view["HasIndexes"] != null && view["HasIndexes"].ToString() != "")
                        //    hasIndex = bool.Parse(view["HasIndexes"].ToString());
                        //string lastUserToCheck = view["LastUsedToCheck"].ToString();
                        string jobName    = view["JobName"].ToString();
                        string jobNum     = view["JobNumber"].ToString();
                        string folderName = view["FolderName"].ToString();


                        //string allArgs = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + reportName + ";" + userTimeRef + ";" + xferStartTime + ";" + totPages;
                        string allArgs = jobreportid + ";" + reportid + ";" + pages + ";" + frompage + ";" + reportname + ";" + userTimeRef + ";" + xferStartTime + ";" + totPages;
                        string[] allArgAr = allArgs.Split(';'); int numPages = Int32.Parse(allArgAr.Last());
                        List<String> args = allArgAr.ToList();
                        args.RemoveRange(4, 4); String.Join(";", args.ToArray());
                        //Navigation.ViewParameters = String.Join(";", args.ToArray()); //e.CommandArgument.ToString();
                        //Navigation.ViewParameters2 = format;
                        //Navigation.LogParameters = new string[] { jobreportid, reportid, pages, frompage, reportname, userTimeRef, xferStartTime, jobName, jobNum, folderName };
                        //download filename  
                        //Navigation.ViewParameters3 = jobName + "_" + jobreportid + "_" + reportid + "_" + jobNum + "_" + (DateTime.Now).ToString("yyyyMMddHHmmssffff");
                        //log.Info("Call ViewFile with ViewParameter " + Navigation.ViewParameters + "---" + Navigation.ViewParameters2);

                        //download file 
                        // CompleteRequestAndRedirect("~/ViewFile.aspx", false); 

                        string encr_equal = "#&##&#"; 
                        string encr_and = "&#&&#&"; 
                        string encr_log = "&####&";

                        string parameters = "";
                        parameters = "viep1" + encr_equal + String.Join(";", args.ToArray()) + encr_and
                            + "viep2" + encr_equal + format + encr_and
                            + "username" + encr_equal + Navigation.User.Username + encr_and
                            + "codauthor" + encr_equal + Navigation.User.CodAuthor + encr_and                             
                            //"username" + encr_equal + Navigation.User.Username + encr_and
                            + "session" + encr_equal + Session.SessionID + encr_and
                            + "viep3" + encr_equal + jobName + "_" + jobreportid + "_" + reportid + "_" + jobNum + "_" + (DateTime.Now).ToString("yyyyMMddHHmmssffff") + encr_and
                            + "viewlog" + encr_equal + String.Join(encr_log, new string[] { jobreportid, reportid, pages, frompage, reportname, userTimeRef, xferStartTime, jobName, jobNum, folderName, totpages } )
                            ;



                        string encryptedParams = Signature.CreateToken(parameters, EXP_TIME_SECONDS);
                        //pass krypted value
                        //((StartPage)this.Page).OpenViewFile(encryptedvalue);

                        
                        //CompleteRequestAndRedirect("~/ViewFile.aspx", false);
                        CompleteRequestAndRedirect("~/ViewFile.aspx?key=" + encryptedParams + "&void=void", false);
                        
                    }
                    else
                    {
                        //messaggio di errore: utente non autorizzato
                        log.Info("Page_Load Err " + "User not authorized to view the report.");
                        throw new Exception("User not authorized to view the report");
                        //Navigation.Error = "An error occurred on server loading page because :" + "User not authorized to view the report";
                        ////HttpContext.Current.Application.set
                        //CompleteRequestAndRedirect("~/Error.aspx?disableReload=Y&enableClose=Y");
                    }
                }
                else
                {
                    //messaggio di errore: utente non autorizzato
                    log.Info("Page_Load Err: " + "Some parameters not found in request.");
                    throw new Exception("Some parameters not found in request");
                    //Navigation.Error = "An error occurred on server loading page because :" + "Parameters JobReportID and ReportID not found in request";
                    ////HttpContext.Current.Application.set
                    //CompleteRequestAndRedirect("~/Error.aspx?disableReload=Y&enableClose=Y");
                }

            }
            catch (Exception Ex)
            {
                log.Error("Failed to load the report: " + Ex.Message);
                Navigation.Error = Ex.Message;
                CompleteRequestAndRedirect("~/Error.aspx?disableReload=Y&enableClose=Y");

            }

            //Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "closePage", "window.onunload = CloseWindow();"); 
            //ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.open('close.html', '_self', null);", true);
            //CompleteRequestAndRedirect("~/close.html");
        }

        private void CompleteRequestAndRedirect(string p1, bool p2)
        {
            //throw new NotImplementedException();
            Context.ApplicationInstance.CompleteRequest();
            Response.Redirect(p1, p2);
        }
        private void CompleteRequestAndRedirect(string p1)
        {
            //throw new NotImplementedException();
            CompleteRequestAndRedirect(p1, false); 
        }

         

        private void TestPGE()
        {
            printOnlyOnDebug("TestPGE()");
            string Branch = "";
            string Organization_Unit = ""; 

            PGE_WS.ApplicationSecurityCheckService PGE_WS = null;
            try
            {
                string matricola = "";
                string token = "";
                string twr = "";
                string effective_twr = "";
                if (Request.Params["user"] != null && 
                    (!Request.Params["user"].Equals("")) &&
                    Request.Params["token"] != null &&
                    (!Request.Params["token"].Equals("")) &&
                    Request.Params["twr"] != null &&
                    (!Request.Params["twr"].Equals("")) &&
                    Request.Params["effective_twr"] != null &&
                    (!Request.Params["effective_twr"].Equals("")) 
                    )
                {
                    log.Info("CHIAMATA DA XHP");
                    matricola = Request.Params["user"];
                    token = Request.Params["token"];
                    twr = Request.Params["twr"];
                    effective_twr = Request.Params["effective_twr"];


                    User user = new User(matricola, token);

                    PGE_WS = new PGE_WS.ApplicationSecurityCheckService();
                    PGE_WS.Url = "http://" + twr + "/" + ConfigurationManager.AppSettings["PGE_WS"];

                    //TESTSAN
                    //if (matricola.TrimEnd().Equals("EE24544",StringComparison.OrdinalIgnoreCase) )
                    //{
                    //    isDebugActive = true;
                    //    if( Request.Params["test"] != null  )
                    //    { 
                    //        PGE_WS.Url = "http://" + "ufr-uj.collaudo.usinet.it" + "/" + ConfigurationManager.AppSettings["PGE_WS"];
                    //    }
                    //}


                    string appCode = ConfigurationManager.AppSettings["ApplicationCode"];

                    PGE_WS.PGEabdb2 resp = PGE_WS.retrievePGEAuthority(matricola, token, appCode);


                    log.Info("CHIAMATO PGE WEBSERVICE - WS URL: " + PGE_WS.Url
                        + " - USER: " + matricola
                        + " - TWR: " + twr
                        + " - EFFECTIVE_TWR: " + effective_twr
                        + " - TOKEN: " + token
                        );

                    //TEST - PG_COD_ABI: 02008 - 
                    //    PG_COD_AUTHOR: X1NBAS     - 
                    //        PG_COD_CAB: 01600 - 
                    //            PG_COD_ERRORE:  -
                    //                PG_COD_IST: 01 - 
                    //                    PG_COD_MATRICOLA_FITT: 63013 - PG_COD_SERVER:         - 
                    //PG_COD_SET_CONT: 00200 - PG_COD_SOC:   - 
                    //    PG_COD_SPORT_CONT: 00200 - PG_COD_TERM_LU0: PEE24544 - 
                    //        PG_COD_TERM_SERV_SIF: FEE24544 - PG_COD_TERM_VTAM: $EE24544 - PG_CODISA_INQ: XX - PG_COGNOME_OPER: AIELLO          - 
                    //            PG_DESCR_DIP: MILANO CORDUSIO                      - 
                    //                PG_DOM_OPRT: AA1S3 - PG_FIL_INQ: 00001 - PG_NOME_OPER: SANTINO    - PG_RUOLO:          - 
                    //                    PG_TORRE: C0 - PG_UFFICIO_INQ: US93048    - PG_USERID: EE24544  - PG_CAB_CIN: 00006 - PG_CAB_FCOMM: 15122 - PG_CAB_FORO: ROMA                           - PG_CAB_LUOGO: MILANO                                  


                    //TESTSAN

                    //if (matricola.TrimEnd().Equals("EE24544", StringComparison.OrdinalIgnoreCase))
                    


                    if ("1".Equals(ConfigurationManager.AppSettings["DEBUG_PGE"]))
                    {
                        //resp.PG_COD_AUTHOR = "X1N6UVE"; //forzatura di test TESTSAN
                        //resp.PG_TORRE = "BA"; //forzatura di test TESTSAN
                        //resp.PG_UFFICIO_INQ = "00200"; //forzatura di test TESTSAN
                        //resp.PG_COD_SPORT_CONT = "00200"; //forzatura di test TESTSAN 
                        resp.PG_COD_AUTHOR = ConfigurationManager.AppSettings["DEBUG_PGE_COD_AUTHOR"]; //forzatura di test TESTSAN
                        resp.PG_TORRE = ConfigurationManager.AppSettings["DEBUG_PGE_TORRE"]; //forzatura di test TESTSAN
                        resp.PG_UFFICIO_INQ = ConfigurationManager.AppSettings["DEBUG_PGE_UFFICIO_INQ"]; //forzatura di test TESTSAN
                        resp.PG_COD_SPORT_CONT = ConfigurationManager.AppSettings["DEBUG_COD_SPORT_CONT"]; //forzatura di test TESTSAN 
                    }

                    if (resp.PG_COD_AUTHOR != null &&
                        resp.PG_BANCA != null &&
                        resp.PG_COD_IST != null &&
                        resp.PG_RUOLO != null &&
                        resp.PG_TORRE != null &&
                        resp.PG_UFFICIO_INQ != null &&
                        resp.PG_FIL_INQ != null)
                    {
                        log.Info("PGE AUTH SUCCEDED: "
                        + " - User: " + user.Username
                        + " - Torre: " + effective_twr
                        + " - PG_COD_AUTHOR: " + resp.PG_COD_AUTHOR
                        + " - PG_BANCA: " + resp.PG_BANCA
                        + " - PG_COD_IST: " + resp.PG_COD_IST
                        + " - PG_RUOLO: " + resp.PG_RUOLO
                        + " - PG_TORRE: " + resp.PG_TORRE
                        + " - PG_UFFICIO_INQ: " + resp.PG_UFFICIO_INQ
                        + " - PG_FIL_INQ: " + resp.PG_FIL_INQ
                        + " - PG_COD_SPORT_CONT : " + resp.PG_COD_SPORT_CONT
                        );
                    }
                    if ((resp.PG_ERRORE != null) && (!resp.PG_ERRORE.codiceErr.Equals("00")))
                    {
                        log.Info("PGE AUTH ERROR: "
                        + " - User: " + user.Username
                        + " - Torre: " + effective_twr
                        + " - PG_ERRORE.codiceErr: " + resp.PG_ERRORE.codiceErr
                        + " - PG_ERRORE.descError: " + resp.PG_ERRORE.descError);
                    }

                    if (resp.PG_UFFICIO_INQ != null)
                        Branch = resp.PG_COD_SPORT_CONT;
                    if (resp.PG_COD_SPORT_CONT != null)
                        Organization_Unit = resp.PG_UFFICIO_INQ;


                    if (resp.PG_ERRORE != null && resp.PG_ERRORE.codiceErr != "" && resp.PG_ERRORE.codiceErr != "00")
                    {
                        log.Info("Autenticazione non riuscita " + resp.PG_ERRORE.codiceErr + " - " + resp.PG_ERRORE.descError);

                        if (isSkipPGEuser(matricola, 1, Branch, Organization_Unit))
                        {
                            printOnlyOnDebug("Skipped PGE checks for the user: " + matricola + " " + user.CodAuthor + " " + user.AltProfilo + " " + user.getRealAltProfilo); 
                            return;
                        }
                        throw new Exception("Autenticazione non riuscita " + resp.PG_ERRORE.codiceErr + " - " + resp.PG_ERRORE.descError);
                    }
                    else
                    {

                        if (isSkipPGEuser(matricola, 1, Branch, Organization_Unit))
                        {
                            printOnlyOnDebug("Skipped PGE 1(GO LIVE Man.)checks for the user: " + matricola + " " + user.CodAuthor + " " + user.AltProfilo + " " + user.getRealAltProfilo); 
                            return;
                        }
                        //if (resp.PG_TORRE != null && resp.PG_TORRE != "")
                        //    user.Torre = resp.PG_TORRE.TrimEnd(); 
                        user.SportCont = resp.PG_COD_SPORT_CONT.TrimEnd();
                        user.OrganizationUnit = resp.PG_UFFICIO_INQ.TrimEnd();

                        user.TorrePG = resp.PG_TORRE.TrimEnd();

                        if (effective_twr != null && effective_twr != "")
                            user.Torre = effective_twr;

                        //TESTSAN
                        //string profilo = Request.Params["profilo"];
                        //if (profilo != null && profilo != "" && matricola != null && matricola.TrimEnd().Equals("EE24544", StringComparison.OrdinalIgnoreCase))
                        //{
                        //    resp.PG_COD_AUTHOR = profilo;
                        //    log.Info("Profile changed in: " + profilo);
                        //}



                        if (resp.PG_COD_AUTHOR != null && resp.PG_COD_AUTHOR != "")
                            user.CodAuthor = resp.PG_COD_AUTHOR.TrimEnd();
                        if (resp.PG_COD_AUTHOR.TrimEnd().Equals("X1NBAS", StringComparison.OrdinalIgnoreCase)
                            || resp.PG_COD_AUTHOR.TrimEnd().Equals("X1NOPR", StringComparison.OrdinalIgnoreCase)
                            || resp.PG_TORRE.Equals(AUSTRIAN_ENV))
                            //user.ProfiluserORRE.TrimEnd() + resp.PG_COD_SPORT_CONT.TrimEnd();
                            //user.Profilo = resp.PG_TORRE.TrimEnd() + resp.PG_UFFICIO_INQ.TrimEnd().Substring(2);
                            user.Profilo = resp.PG_TORRE.TrimEnd() + resp.PG_COD_SPORT_CONT.TrimEnd();
                        else
                            user.Profilo = resp.PG_COD_AUTHOR.TrimEnd();
                        Navigation.User = user;
                        //Navigation.Twr = twr;
                        log.Info("PGE AUTH SUCCEDED: "
                        + " - User: " + user.Username
                        + " - Profilo: " + user.Profilo
                        + " - CodAuthor: " + user.CodAuthor
                        + " - Banca: " + resp.PG_BANCA
                        + " - Istituto: " + resp.PG_COD_IST
                        + " - Ruolo: " + resp.PG_RUOLO
                        + " - TorrePG: " + resp.PG_TORRE
                        + " - Torre: " + user.Torre
                        + " - Ufficio: " + resp.PG_UFFICIO_INQ
                        + " - Filiale: " + resp.PG_FIL_INQ
                        );

                        printOnlyOnDebug("PG_COD_ABI: " + resp.PG_COD_ABI
                                         + " - PG_COD_AUTHOR: " + resp.PG_COD_AUTHOR
                                         + " - PG_COD_CAB: " + resp.PG_COD_CAB
                                         + " - PG_COD_ERRORE: " + resp.PG_COD_ERRORE
                                         + " - PG_COD_IST: " + resp.PG_COD_IST
                                         + " - PG_COD_MATRICOLA_FITT: " + resp.PG_COD_MATRICOLA_FITT
                                         + " - PG_COD_SERVER: " + resp.PG_COD_SERVER
                                         + " - PG_COD_SET_CONT: " + resp.PG_COD_SET_CONT
                                         + " - PG_COD_SOC: " + resp.PG_COD_SOC
                                         + " - PG_COD_SPORT_CONT: " + resp.PG_COD_SPORT_CONT
                                         + " - PG_COD_TERM_LU0: " + resp.PG_COD_TERM_LU0
                                         + " - PG_COD_TERM_SERV_SIF: " + resp.PG_COD_TERM_SERV_SIF
                                         + " - PG_COD_TERM_VTAM: " + resp.PG_COD_TERM_VTAM
                                         + " - PG_CODISA_INQ: " + resp.PG_CODISA_INQ
                                         + " - PG_COGNOME_OPER: " + resp.PG_COGNOME_OPER
                                         + " - PG_DESCR_DIP: " + resp.PG_DESCR_DIP
                                         + " - PG_DOM_OPRT: " + resp.PG_DOM_OPRT
                                         + " - PG_FIL_INQ: " + resp.PG_FIL_INQ
                                         + " - PG_NOME_OPER: " + resp.PG_NOME_OPER
                                         + " - PG_RUOLO: " + resp.PG_RUOLO
                                         + " - PG_TORRE: " + resp.PG_TORRE
                                         + " - PG_UFFICIO_INQ: " + resp.PG_UFFICIO_INQ
                                         + " - PG_USERID: " + resp.PG_USERID
                                         + " - PG_CAB_CIN: " + resp.PG_CAB_CIN
                                         + " - PG_CAB_FCOMM: " + resp.PG_CAB_FCOMM
                                         + " - PG_CAB_FORO: " + resp.PG_CAB_FORO
                                         + " - PG_CAB_LUOGO: " + resp.PG_CAB_LUOGO
                            );
                    }
                }
                else
                {
                    log.Info("----- USER LOGGED: Context" + HttpContext.Current.User.Identity.Name + "-old logon-" + Request.ServerVariables["LOGON_USER"] + "-----AUTH " + Request.ServerVariables["AUTH_USER"]);

                    

                    //string[] skipUsers = ConfigurationManager.AppSettings["SKIP_PGE"].Split(',');
                    //bool found = false;
                    //printOnlyOnDebug(" ConfigurationManager.AppSettings['SKIP_PGE']=" +  ConfigurationManager.AppSettings["SKIP_PGE"]);
                    //foreach (string item in skipUsers ?? new string[0])
                    //{
                    //    printOnlyOnDebug("item=" + item);
                    //    string[] parms = item.Split('=');
                    //    printOnlyOnDebug("parms[0]=" + parms[0]);
                    //    if (parms[0].ToUpper().Equals(user.ToUpper()))
                    //    {
                    //        Navigation.User = new User(user, "", (parms.Length > 1 ? parms[1] : "X1NADM"), "", (parms.Length > 1 ? parms[1] : "X1NADM"), "N/A",  (parms.Length > 1 ? parms[1] : "N/A"));
                    //        found = true;
                    //        printOnlyOnDebug("found = true");
                    //        log.Info("AUTH USER SKIPPING PGE: " + user);
                    //        break;
                    //    }
                    //}
                    //if (!found)

                    string user2 = (Navigation.User == null ? "" : Navigation.User.Username);

                    if ("".Equals(user2))
                    {
                        System.Security.Principal.WindowsPrincipal p = System.Threading.Thread.CurrentPrincipal as System.Security.Principal.WindowsPrincipal;
                        user2 = HttpContext.Current.User.Identity.Name.ToString();
                    }
                    if ("".Equals(user2))
                        user2 = HttpContext.Current.User.Identity.Name.ToString();
                    if ("".Equals(user2))
                        user2 = Request.ServerVariables["LOGON_USER"]; //Finding with name   
                    if ("".Equals(user2))
                        user2 = Request.ServerVariables["AUTH_USER"]; //Finding with name   
                    if ("".Equals(user2))
                        user2 = Request.ServerVariables[5]; //Finding with index   
                    string[] authUser = user2.Split('\\');
                    if (authUser.Length > 1)
                        user2 = authUser[1];

                    if (!isSkipPGEuser(user2, 0, Branch, Organization_Unit))
                    {
                        printOnlyOnDebug("found = false");
                        //CompleteRequestAndRedirect("resp~/Authentication/Login.aspx");
                        throw new Exception("Session timeout or wrong authentication. Please try again to launch the application from Applications menu of your Branch.");
                    }
                    else
                    {
                        printOnlyOnDebug("Skipped PGE checks for the user: " + user2);
                    }
                }
            }
            catch (Exception Ex)
            {
                log.Error("TestPGE() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                if (PGE_WS != null)
                {
                    Navigation.Error = Ex.Message + "\n Authentication Web Service url " + PGE_WS.Url + " \n could be currently unavailable";
                }
                throw new Exception(Navigation.Error); 
            }
        }


        private void printOnlyOnDebug(string line)
        {
            if (this.isDebugActive)
                log.Debug("TEST - " + line);

        }

        protected bool isSkipPGEuser(String user, int type, String Branch, String Organization_Unit)
        {
            if (ConfigurationManager.AppSettings["SKIP_PGE"] == null)
                return false;
            string[] skipUsers = ConfigurationManager.AppSettings["SKIP_PGE"].Split(',');
            //printOnlyOnDebug(" ConfigurationManager.AppSettings['SKIP_PGE']=" + ConfigurationManager.AppSettings["SKIP_PGE"]);
            foreach (string item in skipUsers ?? new string[0])
            {
                //printOnlyOnDebug("item=" + item);
                string[] parms = item.Split('=');
                //printOnlyOnDebug("parms[0]=" + parms[0]);
                if (parms[0].ToUpper().Equals(user.ToUpper()))
                {
                    Navigation.User = new User(user, "", (parms.Length > 1 ? parms[1] : "X1NADM"), "", (parms.Length > 1 ? parms[1] : "X1NADM"), (parms.Length > 2 ? parms[2] : (Branch.Equals("") ? "N/A" : Branch)), (parms.Length > 3 ? parms[3] : (Organization_Unit.Equals("") ? "N/A" : Organization_Unit)));
                    printOnlyOnDebug("AUTH USER SKIPPING PGE: " + user);
                    return true;
                }
            }
            printOnlyOnDebug("USER NOT FOUND IN SKIPPING PGE LIST: " + user);
            // The appsett variable SKIP_PGE_4ALL is used to force the profile for all the users. The focrced profile is the value of the variable 
            // but only if that one is one of the profile name available

            if (type == 1 && ConfigurationManager.AppSettings["SKIP_PGE_4ALL"] != null)
            {
                String skip_profile = ConfigurationManager.AppSettings["SKIP_PGE_4ALL"].ToString().ToUpper();
                if (skip_profile.Equals("X1NADM") ||
                    skip_profile.Equals("X1NOPR") ||
                    skip_profile.Equals("X1NBAS"))
                {
                    Navigation.User = new User(user, "", skip_profile, "", skip_profile, (Branch.Equals("") ? "N/A" : Branch), Organization_Unit.Equals("") ? "N/A" : Organization_Unit);
                    log.Info("AUTH USER SKIP_PGE_4ALL: " + user);
                    return true;
                }
                else
                {
                    log.Info("AUTH USER SKIP_PGE_4ALL: an error occured: the value of SKIP_PGE_4ALL (" + skip_profile + ") is not a valid profilename.");
                    return false;
                }
            }
            log.Info("PARAMETER SKIP_PGE_4ALL NOT FOUND.");
            return false;
        }
  
        private void SetUser()
        {
            printOnlyOnDebug("SetUser()");

            if ("1".Equals(ConfigurationManager.AppSettings["DEBUG"]))
            //if ((ConfigurationManager.AppSettings["DEBUG"] != null) && (ConfigurationManager.AppSettings["DEBUG"].ToString().Equals("1")) )
            {
                this.isDebugActive = true;
                printOnlyOnDebug("SetUser() if debug");
                string DEBUG_Username = (ConfigurationManager.AppSettings["DEBUG_Username"] != null ? ConfigurationManager.AppSettings["DEBUG_Username"] : "xreport");
                string DEBUG_TokenString = (ConfigurationManager.AppSettings["DEBUG_TokenString"] != null ? ConfigurationManager.AppSettings["DEBUG_TokenString"] : "");
                string DEBUG_Profilo = (ConfigurationManager.AppSettings["DEBUG_Profilo"] != null ? ConfigurationManager.AppSettings["DEBUG_Profilo"] : "X1NBAS");
                string DEBUG_Torre = (ConfigurationManager.AppSettings["DEBUG_Torre"] != null ? ConfigurationManager.AppSettings["DEBUG_Torre"] : "");
                string DEBUG_CodAuthor = (ConfigurationManager.AppSettings["DEBUG_CodAuthor"] != null ? ConfigurationManager.AppSettings["DEBUG_CodAuthor"] : "X1NBAS");
                string DEBUG_SportCont = (ConfigurationManager.AppSettings["DEBUG_SportCont"] != null ? ConfigurationManager.AppSettings["DEBUG_SportCont"] : "N/A");
                string DEBUG_OrganizationUnit = (ConfigurationManager.AppSettings["DEBUG_OrganizationUnit"] != null ? ConfigurationManager.AppSettings["DEBUG_OrganizationUnit"] : "N/A");
                //Navigation.Twr = (ConfigurationManager.AppSettings["DEBUG_twr"] != null ? ConfigurationManager.AppSettings["DEBUG_SportCont"] : "N/A");
                Navigation.User = new User(DEBUG_Username, DEBUG_TokenString, DEBUG_Profilo, DEBUG_Torre, DEBUG_CodAuthor, DEBUG_SportCont, DEBUG_OrganizationUnit);

            }
            else
            {
                TestPGE();
            }

            //    string matricola = "";
            //    string token1 = "";
            //    string twr = "";
            //    string effective_twr = ""; 
            //    if (Request.Params["token"] != null && Request.Params["token"] != null &&
            //        Request.Params["user"] != "" && Request.Params["token"] != "" &&
            //        Request.Params["twr"] != "" && Request.Params["effective_twr"] != "")
            //    {
            //        token1 = Request.Params["token"];

            //        this.lb_token.Text = "***** TOKEN OK *****";
            //    }
            //    else
            //    {
            //        this.lb_token.Text = "***** TOKEN NOT OK *****";

            //    }


            //    if (false)
            //    {
            //        string token = Request.QueryString["Token"];
            //        if (Signature.ValidateToken(token))
            //        {
            //            Token t = Signature.GetToken(token);
            //            this.lb_token.Text = "***** TOKEN OK *****";
            //            string[] pars = t.Value.Split(';');
            //            LoadPDF(pars[0], pars[1], pars[2], pars[3]);
            //        }
            //        else
            //        {
            //            this.lb_token.Text = "***** TOKEN NOT OK *****";
            //        } 
        }

        public void LoadReports_SP(string JobReportid, string Reportid)
        {
            string user = "";
            string user2 = "";
            try
            {
                //user = Navigation.User.Profilo;
                //user2 = string.Join("' OR ua.UserAlias like  '", new string[] { (Navigation.User.AltProfilo == null ? "" : string.Join("' OR ua.UserAlias like  '", Navigation.User.AltProfilo)), Navigation.User.Username });
                if (Navigation.User.Profilo != null && Navigation.User.Profilo != "")
                {
                    Navigation.User.AltProfilo = (new string[] { "START" });
                    if (Navigation.User.isAustrian())
                    {
                        if (Navigation.User.getRealAltProfilo == null || ("START".Equals(string.Join(" , ", Navigation.User.getRealAltProfilo)) || "".Equals(string.Join(" , ", Navigation.User.getRealAltProfilo))))
                        {
                            user = Navigation.User.Profilo;
                            user2 = string.Join("' OR ua.UserAlias like  '", new string[] { string.Join("' OR ua.UserAlias like  '", Navigation.User.AltProfilo), Navigation.User.Username });
                        }
                        else
                        {
                            user = string.Join("' OR ua.UserAlias like  '", Navigation.User.getRealAltProfilo);
                            user2 = "";
                        }
                    }
                    else
                    {
                        user = Navigation.User.Profilo;
                        user2 = Navigation.User.OrganizationUnit.Contains("N/A") ? "" : Navigation.User.TorrePG + Navigation.User.OrganizationUnit.TrimEnd().Substring(2);
                    }

                }

                //ResetBind();
                string startSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture); string logParms = "";
                SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["SQL_CONNECTION"]);
                conn.Open();
                DataSet ds = new DataSet("Reports");
                string storedName = (ConfigurationManager.AppSettings["storedName"] != null ? ConfigurationManager.AppSettings["storedName"] : "XRGetReportsDataALL_AUS_withLastExReportJR");
                string StoredFields = (ConfigurationManager.AppSettings["StoredFields"] != null ? ConfigurationManager.AppSettings["StoredFields"] :
                    "JobName;ReportName;JobNumber;Recipient;DataFrom;DataTo;Orderby;User;User2;Remark;FlagLastExecution;JobReportid;Reportid");


                string[] StoredFieldsList = StoredFields.Split(';');

                SqlCommand sqlComm = new SqlCommand(storedName, conn);
                //DateTime dt;
                for (int i = 0; i < StoredFieldsList.Length; i++)
                {
                    //XRGetReportsDataALL_AUS_withLastExReportJR '%',null, null,null,null,null,'Recipient desc','X1N6UVE',null,null,1,'189093','2'
                    switch (StoredFieldsList[i])
                    {
                        case "JobName":
                            sqlComm.Parameters.AddWithValue("@JobName", "%");
                            logParms += "JobName == %::";
                            break;
                        case "ReportName":
                            sqlComm.Parameters.AddWithValue("@ReportName", DBNull.Value);
                            break;
                        case "JobNumber":
                            sqlComm.Parameters.AddWithValue("@JobNumber", DBNull.Value);
                            break;
                        case "Recipient":
                            sqlComm.Parameters.AddWithValue("@Recipient", DBNull.Value);
                            break;
                        case "DataFrom":
                            sqlComm.Parameters.AddWithValue("@DataFrom", DBNull.Value);
                            break;
                        case "DataTo":
                            sqlComm.Parameters.AddWithValue("@DataTo", DBNull.Value);
                            break;
                        case "Orderby":
                            sqlComm.Parameters.AddWithValue("@Orderby", "Recipient desc");
                            logParms += "Orderby == Recipient desc::";
                            break;
                        case "User":
                            if (user != null && user != "")
                            {
                                sqlComm.Parameters.AddWithValue("@User", user);
                                logParms += "User == " + user + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@User", DBNull.Value);
                            break;
                        case "User2":
                            if (user2 != null && user2 != "")
                            {
                                sqlComm.Parameters.AddWithValue("@User2", user2);
                                logParms += "User2 == " + user2 + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@User2", DBNull.Value);
                            break;
                        case "Remark":
                            sqlComm.Parameters.AddWithValue("@Remark", DBNull.Value);
                            logParms += "Remark == " + "null" + "::";
                            break;
                        case "FlagLastExecution":
                            sqlComm.Parameters.AddWithValue("@FlagLastExecution", DBNull.Value);
                            break;
                        case "JobReportid":
                            sqlComm.Parameters.AddWithValue("@JobReportid", JobReportid);
                            logParms += "JobReportid == " + JobReportid + "::";
                            break;
                        case "Reportid":
                            sqlComm.Parameters.AddWithValue("@Reportid", Reportid);
                            logParms += "Reportid == " + Reportid + "::";
                            break;
                        default:
                            log.Error("Failed in parameter StoredFields");
                            throw new Exception("Failed in parameter StoredFields - field " + StoredFieldsList[i] + " not managed");
                    }
                }

                logParms.Remove(logParms.Length - 2, 2);

                log.Info("XRGetReportsData_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + logParms + " 80 - 127.0.0.1 Mozilla/4.0+(compatible;+MSIE+6.0;+MS+Web+Services+Client+Protocol+4.0.30319.1) 200 0 0 ");


                sqlComm.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm; sqlComm.CommandTimeout = 180; //time out a 180 secondi 
                da.Fill(ds);


                DataTable mytable = ds.Tables[0];


                //if (!mytable.Columns.Contains("Remark"))
                //{
                //    mytable.Columns.Add("Remark");
                //    //test
                //    foreach (DataRow row in mytable.Rows)
                //    {
                //        //need to set value to NewColumn column
                //        row["Remark"] = "0123456789123456";   // or set it to some other value
                //    }
                //}

                globalTableSource = mytable; int rowSize = mytable.Rows.Count;
                //globalTableSource = ds.Tables[0]; int rowSize = ds.Tables[0].Rows.Count;
                conn.Close();
                Response.AppendToLog("XRGetReportsData_Connection_CLOSE");
                log.Debug("XRGetReportsData_Connection_CLOSE");
                string endSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                globalCurrentGridPage = 0;
                Response.AppendToLog("BindGridView_ended");
                log.Debug("BindGridView_ended");
                logParms += "ResSize == " + rowSize;
                log.Info("XRGetReportsData_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSP + "_" + logParms + " 80 - 127.0.0.1 Mozilla/4.0+(compatible;+MSIE+6.0;+MS+Web+Services+Client+Protocol+4.0.30319.1) 200 0 0 " + rowSize);
                Response.AppendToLog("XRGetReportsData_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSP + "_" + logParms);
                //log.Debug("LoadReports_SP() Finished");
            }
            catch (Exception Ex)
            {
                Navigation.Error = Ex.Message;
            }
        }

        //public void OpenViewFile()
        //{
        //    ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFile.aspx');", true);
        //}

        //public void OpenViewFile(string encryptedParams)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFile.aspx?key=" + encryptedParams + "&void=void');", true);
        //}




        public void OpenViewTxtFile()
        {
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewTextFile.aspx');", true);
        }

        public string EXP_TIME_SECONDS = (
        (
            (ConfigurationManager.AppSettings["EXPIRATION_TIME_SECONDS"] != null) &&
            (!ConfigurationManager.AppSettings["EXPIRATION_TIME_SECONDS"].Equals(""))
        ) ? ConfigurationManager.AppSettings["EXPIRATION_TIME_SECONDS"]
            : "86400"
    );
         


        #endregion

        protected void b_close_Click(object sender, EventArgs e)
        { 
            Session.Clear();
            string js = "javascript: window.close();";
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", js, true);
        }
    }
}