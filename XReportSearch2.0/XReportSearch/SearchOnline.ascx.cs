﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Configuration;
using System.Web.Services.Protocols;
using XReportSearch.Utils;
using CustomControls;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;
using PlainElastic.Net.Queries;
using XReportSearch.Business;
using PlainElastic.Net;
using PlainElastic.Net.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Services;
using Microsoft.Web.Administration;
using XReportSearch.XReportWebIface;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
//using System.Threading.Tasks;
//using System.Windows.Forms;

namespace XReportSearch
{
    public partial class SearchOnline : System.Web.UI.UserControl
    {
        #region Constants
        private string SORT_DESCENDING = "DESC";
        private string SORT_ASCENDING = "ASC";
        private string CHECK_ROOT = "select RootNode from tbl_ProfilesFoldersTrees pft join tbl_UsersProfiles up  with (NOLOCK) on pft.ProfileName = up.ProfileName  " +
                                    "join    tbl_UserAliases ua  with (NOLOCK) on up.UserName = ua.UserName and  ua.UserAlias = '#{User}#' and RootNode = 'ROOT'";
        private string OPTION_FOLDER_USER = " and fn1.FolderName = ( select FolderName from tbl_Folders fo inner join tbl_ProfilesFoldersTrees pt with (NOLOCK) on fo.FolderName like pt.RootNode+'%' inner join tbl_UsersProfiles up with (NOLOCK) \n" +
                                            " on up.ProfileName = pt.ProfileName and rootnode <> 'ROOT' \n" +
                                            " join tbl_useraliases ua with (NOLOCK) on up.username = ua.username #{wherecond}#  )";
        //" where ua.UserName =  '#{User}#' )";
        private string SELECT_SEARCH = " select top 1000 *, 1 AS FromPage from (SELECT ROW_NUMBER() OVER (ORDER BY #{OrderBy}#) AS ROW , B.* from (select  \n" +
                       "distinct  \n" +
                       "fn1.FolderName, FolderDescr AS FolderDescr, JR.JobReportId as JobReport_ID, lr1.ReportId As ReportId  , JR.XferStartTime As TimeRef  , COALESCE(lrr1.UserTimeRef, JR.UserTimeRef, JR.XferStartTime) As UserTimeRef, COALESCE(JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime) As UserTimeElab  , (CONVERT(varchar, JR.XferStartTime, 103) + SUBSTRING(CONVERT(varchar, JR.XferStartTime, 121),11,6)) As XferStartTime  ,    1 As TotReports  ,lr1.TotPages As TotPages  , CONVERT(varchar(500), pr1.ListOfPages) as ListOfPages, \n" +
                       "   substring(JR.jobreportname,1,8)  as JobName, --JR.JobName  , \n" +
                       "  '' as JobReportDescr, \n" +
                       "   JR.JobNumber  ,    COALESCE(lrt1.textString, JR.UserRef) AS ReportName   ,JR.FileRangesVar       ,JR.ExistTypeMap  ,1 As IsGRANTED, JR.HasIndexes, \n" +
                       "   --CASE WHEN charindex('\',reverse(fn1.FolderName)) > 0 THEN (right(fn1.FolderName,    charindex('\',reverse(fn1.FolderName))-(1))) ELSE fn1.FolderName END \n" +
                       "   fn1.FolderNameKey \n" +
                       "   AS Recipient, lr1.CheckStatus, lr1.LastUsedToCheck, RN.CheckFlag       from tbl_Folders fn1 with (NOLOCK) \n" +
                       "INNER JOIN dbo.tbl_FoldersRules  fr1 with (NOLOCK) ON fn1.FolderName = fr1.FolderName \n" +
                       " #{OPTION_FOLDER_USER}#" +
                       " \n" +
                       "INNER JOIN dbo.tbl_NamedReportsGroups nm1  with (NOLOCK) ON fr1.ReportGroupId = nm1.ReportGroupId \n" +
                       "INNER JOIN tbl_VarSetsValues vr1 with (NOLOCK) ON nm1.ReportRule = vr1.VarSetName \n" +
                       "INNER JOIN tbl_VarSetsValues vv1  with (NOLOCK) ON vv1.VarSetName = nm1.FilterRule   and vv1.VarName = nm1.FilterVar \n" +
                       "--INNER join tbl_JobReportNames JRN WITH (READUNCOMMITTED)  ON  jrn.JobReportName like vr1.VarValue \n" +
                       "INNER JOIN (Select * from tbl_JobReports  with (NOLOCK) where  JobReportName <> 'CDAMFILE' AND PendingOp <> 12 #{JobName}# ) JR \n" +
                       "--INNER JOIN (Select * from tbl_JobReports where  JobReportName <> 'CDAMFILE' AND PendingOp <> 12 and substring(jobreportname,1,8) LIKE  'GQI8M0C0%' ) JR \n" +
                       "--like introdotta per Visibilita uffici direzionali richiesta da Morello \n" +
                       " ON JR.JobReportName = vr1.VarValue \n" +
                       " \n" +
                       "--ON JR.JobReportName =JRN.JobReportName \n" +
                       "INNER JOIN tbl_LogicalReports lr1  with (NOLOCK) ON lr1.JobReportId =     JR.JobReportId and lr1.XferRecipient = nm1.RecipientRule       and        lr1.FilterValue = vv1.VarValue \n" +
                       " \n" +
                       "INNER JOIN tbl_LogicalReportsRefs lrr1  with (NOLOCK)      ON lr1.JobReportId = lrr1.JobReportId AND lrr1.ReportId = lr1.ReportId  and lrr1.FilterVar = vv1.VarName \n" +
                       "INNER JOIN tbl_LogicalReportsTexts lrt1  with (NOLOCK) ON lrr1.textId = lrt1.textId \n" +
                       "INNER JOIN dbo.tbl_ReportNames RN with (NOLOCK)  ON RN.ReportName = lr1.ReportName \n" +
                       "INNER JOIN tbl_PhysicalReports pr1  with (NOLOCK) ON pr1.JobReportId = lr1.JobReportId and pr1.ReportId = lr1.ReportId        where JR.Status = 18 and vr1.VarName = ':REPORTNAME' and vv1.VarName <> ':REPORTNAME' \n" +
                       " \n" +
                       " \n" +
                       "   --GROUP BY fn1.FolderName, lr1.ReportName, JR.JobReportId, lr1.ReportID, XferStartTime, COALESCE(lrr1.UserTimeRef, JR.UserTimeRef, JR.XferStartTime), convert(varchar(500), pr1.L \n" +
                       "--istOfPages) , JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime, JR.UserTimeRef,UserTimeElab ,JobName, JobNumber, lrt1.textString, JR.UserRef, FileRangesVar, existTypeMap, \n" +
                       " -- FolderDescr, lr1.CheckStatus, lr1.LastUsedToCheck, RN.CheckFlag \n" +
                       ") B \n" +
                       "      WHERE  Convert (varchar ,UserTimeRef ,112) >=  #{DataFrom}# AND  Convert (varchar ,UserTimeRef ,112) <=  #{DataTo}#  #{ReportName}# #{JobNumber}# #{Recipient}# ) T WHERE ROW > 0 ";

        string MAXN_OF_SAVED_SEARCH = ((ConfigurationManager.AppSettings["MAXN_OF_SAVED_SEARCH"] != null) && (!ConfigurationManager.AppSettings["MAXN_OF_SAVED_SEARCH"].Equals("")))
                                                ? ConfigurationManager.AppSettings["MAXN_OF_SAVED_SEARCH"] : "20";

        string EXP_TIME_SECONDS = (
                (
                    (ConfigurationManager.AppSettings["EXPIRATION_TIME_SECONDS"] != null) &&
                    (!ConfigurationManager.AppSettings["EXPIRATION_TIME_SECONDS"].Equals(""))
                ) ? ConfigurationManager.AppSettings["EXPIRATION_TIME_SECONDS"]
                    : "86400"
            );

        #endregion

        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog log_ES = LogManager.GetLogger("ES_Log");
        #endregion

        #region Main Methods
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!Page.IsPostBack)
                {

                    this.tb_data1.Text = String.Format(CultureInfo.InvariantCulture, "{0:dd/MM/yyyy}", System.DateTime.Today);
                    this.tb_data2.Text = String.Format(CultureInfo.InvariantCulture, "{0:dd/MM/yyyy}", System.DateTime.Today);
                        
                    if (Request.Cookies["userInfo"] != null)
                    {
                        System.Collections.Specialized.NameValueCollection userCookie = Request.Cookies["userInfo"].Values;
                        if (Navigation.User.Username.Equals(userCookie["userName"]))
                        {
                           this.tb_jobname.Text = userCookie["JobReportName"];
                           this.tb_name.Text  = userCookie["ReportName"];
                           this.tb_advisor.Text  = userCookie["Advisor"];
                           this.tb_data1.Text  = userCookie["DataFrom"] ;
                           this.tb_data2.Text  = userCookie["DataTo"];
                           this.tb_filterRemark.Text  = userCookie["Remark"];
                           this.tb_prof.Text  = userCookie["Profile"];
                           this.ld_multidate.Text  = userCookie["Period"];
                           this.tb_hour1.Text  = userCookie["TimeFrom"] ;
                           this.tb_hour2.Text = userCookie["TimeTo"];
                           if (null!= userCookie["CheckLast"] && !"".Equals(userCookie["CheckLast"]))
                           {
                               this.CheckBoxLastReport.Checked = Boolean.Parse(userCookie["CheckLast"]);
                               manageLast();
                           }
                        }
                        //Label1.Text = Server.HtmlEncode(Request.Cookies["userName"].Value);
                    }
                    //

                    if (Page.Validators != null && Page.Validators.Count > 0)
                    {
                        ScriptManager.RegisterOnSubmitStatement(this, Page.GetType(), "", "fnOnUpdateValidators()");
                    }

                    if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.GENERALI.ToString())
                    {
                        this.gv_search.Columns[4].Visible = false;
                        this.ddl_group.Visible = true;
                        this.lb_group.Visible = true;
                        LoadUserGroups();
                    }
                    //this.lprof.Text = ConfigurationManager.AppSettings["ProfileLbText"];
                    //this.tb_data1.Text = String.Format(CultureInfo.InvariantCulture, "{0:dd/MM/yyyy}", System.DateTime.Today);
                    //this.tb_data2.Text = String.Format(CultureInfo.InvariantCulture, "{0:dd/MM/yyyy}", System.DateTime.Today);
                    if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.UNICREDIT.ToString())
                    {
                        //this.tb_jobname2.Text = Navigation.User.Torre;
                        this.tb_jobname2.Text = "";
                        this.tb_jobname2.Visible = false;
                        this.tb_jobname2.Enabled = false;
                        this.tb_jobname.Width = this.tb_jobnumber.Width;
                        //hide_column_in_gv_search("ReportName", this.gv_search.Columns);
                        this.gv_search.Columns[5].Visible = false;

                        //this.tb_jobname2.Enabled = true;
                        this.tb_prof.Enabled = true;
                        this.tb_jobname.Attributes.Add("autocomplete", "off");
                        //this.tc_jobname.Controls.Add(this.tb_jobname);
                        //this.tc_jobname.Controls.Add(this.tb_jobname2);
                        //this.tc_jobname.Controls.Add(Utility.CreateAutoComplete("tb_jobname", "tb_jobname"));

                        //10/05/2018 
                        this.tr_prof.Visible = true;
                        this.tb_prof.Enabled = true;
                        this.tb_prof.Visible = true;
                        this.lprof.Visible = true;
                        log.Debug("Before Remark" + ConfigurationManager.AppSettings["ManageRemark"]);
                        //if (ConfigurationManager.AppSettings["ManageRemark"] == null || ConfigurationManager.AppSettings["ManageRemark"].ToString().Equals("0"))
                        //{
                        //    this.lbfilterRemark.Visible = false;
                        //    this.tb_filterRemark.Visible = false;
                        //    this.lbadvisor.Visible = false;
                        //    this.tb_advisor.Visible = false;
                        //}

                        log.Debug("After Remark" + ConfigurationManager.AppSettings["ManageRemark"]);
                        //10/05/2018 
                        //if (Navigation.User.CodAuthor.Equals("X1NADM", StringComparison.OrdinalIgnoreCase) 
                        //    || Navigation.User.CodAuthor.Equals("X1NOPR", StringComparison.OrdinalIgnoreCase))
                        //{
                        //    //this.tb_jobname2.Enabled = true;
                        //    this.tr_prof.Visible = true;
                        //    this.tb_prof.Enabled = true;
                        //    this.tb_prof.Visible = true;
                        //    this.l_prof.Visible = true;
                        //}
                        //else
                        //{
                        //    //this.tb_jobname2.Enabled = false;
                        //    this.l_prof.Visible = false;
                        //    this.tb_prof.Enabled = false;
                        //    this.tb_prof.Visible = false;
                        //    this.tr_prof.Visible = false; 
                        //}
                        if (Navigation.User.isAustrian())
                        {
                            System.Collections.ArrayList nodes = new System.Collections.ArrayList();
                            TreeNode topNode = new CustomControls.CeTreeNode("START");
                            DataTable mytable;
                            if ("X1N1UV".Equals(Navigation.User.CodAuthor))
                            {
                                //profilo 1 select solo sotto ramo BACA con FolderNameKey = BA+Filiale
                                // profile = Navigation.User.Profile = resp.PG_TORRE.TrimEnd() + resp.PG_COD_SPORT_CONT.TrimEnd();
                                mytable = LoadFolders(Navigation.User.Profilo, 0);
                                LoadNodes(mytable, topNode);
                                mytable = LoadFoldersByUser(Navigation.User.Username);
                                LoadSecondaryNodes(mytable, topNode);
                            }
                            else if ("X1N2UV".Equals(Navigation.User.CodAuthor))
                            {
                                //profilo 2 select sotto ramo BACA e sotto ramo BACAHR con FolderName key BA+Filiale  e BA+Filiale +"L"
                                //mytable = LoadFolders("BA02021L", 0);

                                //LoadNodes(LoadFolders(Navigation.User.Profilo+"L", 0), topNode);
                                log.Debug("profile two " + Navigation.User.isAustrian());
                                foreach (string pro in Navigation.User.AltProfilo)
                                    LoadNodes(LoadFolders(pro, 0), topNode);
                                LoadNodes(LoadFolders(Navigation.User.Profilo, 0), topNode);
                                mytable = LoadFoldersByUser(Navigation.User.Username);
                                LoadSecondaryNodes(mytable, topNode);

                            }

                            else if ("X1N3UV".Equals(Navigation.User.CodAuthor) || "X1N5UVE".Equals(Navigation.User.CodAuthor))
                            {
                                //profilo 3 select sotto ramo BACA con FolderNameKey 'BACA' e sotto ramo BACAHR con 
                                //LoadNodes(LoadFolders(Navigation.User.Profilo + "L", 0), topNode);
                                //LoadNodes(LoadFolders("BACA", 1), topNode);
                                foreach (string pro in Navigation.User.AltProfilo)
                                    LoadNodes(LoadFolders(pro, 0), topNode);
                                mytable = LoadFoldersByUser(Navigation.User.Username);
                                LoadSecondaryNodes(mytable, topNode);

                            }
                            //    //profilo 4 e 6 select con folderNameKey = BACA e FolderNameKey = 'BACAHR'
                            else
                            {
                                foreach (string pro in Navigation.User.AltProfilo)
                                    LoadNodes(LoadFolders(pro, 0), topNode);

                                mytable = LoadFoldersByUser(Navigation.User.Username);
                                LoadSecondaryNodes(mytable, topNode);
                            }
                            topNode.Selected = true;
                            log.Debug("profile two " + topNode.ValuePath);
                            this.generalTree.Nodes.Add(topNode);
                            setNavigateUrl(this.generalTree);
                            if (!this.txtHidData.Value.Equals(""))
                                SelectTreeView(generalTree, this.txtHidData.Value);

                            this.CustomValidator_AtLeastOneContact.Enabled = false;
                            this.CustomValidator_AtLeast3CharsInJobname.Enabled = false;
                            this.RetrieveSavedSearch();
                        }
                        else {
                            this.lbfilterRemark.Visible = false;
                            this.tb_filterRemark.Visible = false;
                            this.lbadvisor.Visible = false;
                            this.tb_advisor.Visible = false;
                            this.generalTree.Visible = false;
                            this.CSavedSearch.Visible = false;
                        }

                    }
                    if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.VITTORIA.ToString() || ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.GENERALI.ToString())
                    {
                        if (Navigation.User.IsRoot)
                        {
                            this.l_specUser.Visible = true;
                            this.tb_specUser.Visible = true;
                        }
                        this.CustomValidator_AtLeastOneContact.Enabled = false;
                        this.CustomValidator_AtLeast3CharsInJobname.Enabled = false;
                    }

                    // Create a list of parts.
                    List<TextBox> tbList = new List<TextBox>();
                    tbList.Add(this.tb_specUser);
                    //tbList.Add(this.tb_jobname);
                    //tbList.Add(this.tb_jobname2);
                    tbList.Add(this.tb_name);
                    tbList.Add(this.tb_jobnumber);
                    tbList.Add(this.tb_prof);
                    tbList.Add(this.tb_data1);
                    tbList.Add(this.tb_data2);
                    tbList.Add(this.tb_hour1);
                    tbList.Add(this.tb_hour2);
                    foreach (TextBox tb in tbList)
                    {
                        tb.Attributes.Add("onkeypress", "button_click('" + tb.ClientID + "','" + this.b_assign.ClientID + "')");
                    }
                    //this.pShow.Attributes.Add("onkeypress", "button_click(this,'" + this.b_assign.ClientID + "')"); 

                }
                else
                {
                    TreeNode node = this.generalTree.SelectedNode;
                    setCookies();
                    //if (!this.txtHidData.Value.Equals(""))
                    //SelectTreeView(generalTree, this.txtHidData.Value);

                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "scrollTreeToSelected", " " +
                    //    "var Data = window[\"<%=generalTree.ClientID%>\" + \"_Data\"]; " +
                    //    //"alert(\"inside\" + window[\"<%=generalTree.ClientID%>\"+ \"_Data\"] + \"opp \" + document.getElementById(\"<%=generalTree.ClientID%>\")); " +
                    //    "if (typeof Data !== 'undefined' && Data.selectedNodeID != null && Data.selectedNodeID.value != \"\") { " +
                    //     "   var name = Data.selectedNodeID.value; " +
                    //     "   var selectedNode = document.getElementById(name); " +
                    //     "   alert(\"nome \" + selectedNode); " +
                    //     "   if (selectedNode) { selectedNode.scrollTop; } }", true);

                    if (this.hiddenIndex.Value == "1")
                        this.CreateLinkButtons();
                }
            }
            catch (Exception ex)
            {
                log.Error("Page_Load Err " + ex.Message);
                //string js = "alert(\" " + ex.Message + "\nPlease close this window and reopen the application.\"); window.opener.focus(); window.close();";
                //printOnlyOnDebug("Page_Load Err string js:" + js);
                //Session.Clear();
                //ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", js, true);
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "myscr", js, true);
                Navigation.Error = "An error occurred on server loading page because :" + ex.Message;
                //HttpContext.Current.Application.set
                Context.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/Error.aspx?disableReload=Y&enableClose=Y");
            }
        }
        #endregion

        #region Private Methods

        string selectedPath = string.Empty;//selectedPath stores the seleted value path
        private bool SelectTreeView(TreeView treeView, string selectStr)
        {
            //treeView.Focus();

            foreach (TreeNode item in treeView.Nodes)
            {
                checkTree(item, selectStr);
                if(selectedPath != "")
                    break;
            }

            if (selectedPath != "")
            {
                treeView.FindNode(selectedPath).Select();//here is the point
                return true;
            }
            return false;
        }

        private void checkTree(TreeNode treenode, string selectstr)
        {
            if (treenode.Text == selectstr)
            {
                selectedPath = treenode.ValuePath;
                return;
            }
            else
            {
                foreach (TreeNode item in treenode.ChildNodes)
                {
                    checkTree(item, selectstr);
                    if (selectedPath != "")
                        break;
                }
            }
        }

        private void setCookies() {
            //### cookies management
            HttpCookie aCookie = new HttpCookie("userInfo");
            aCookie.Values["userName"] = Navigation.User.Username;
            aCookie.Values["lastVisit"] = DateTime.Now.ToString();
            aCookie.Values["JobReportName"] = this.tb_jobname.Text;
            aCookie.Values["ReportName"] = this.tb_name.Text;
            aCookie.Values["Advisor"] = this.tb_advisor.Text;
            aCookie.Values["DataFrom"] = this.tb_data1.Text;
            aCookie.Values["DataTo"] = this.tb_data2.Text;
            aCookie.Values["Remark"] = this.tb_filterRemark.Text;
            aCookie.Values["Profile"] = this.tb_prof.Text;
            aCookie.Values["Period"] = this.ld_multidate.Text;
            aCookie.Values["TimeFrom"] = this.tb_hour1.Text;
            aCookie.Values["TimeTo"] = this.tb_hour2.Text;
            aCookie.Values["CheckLast"] = this.CheckBoxLastReport.Checked.ToString();
            aCookie.Expires = DateTime.Now.AddDays(3);
            Response.Cookies.Add(aCookie);

        }

        private XReportWebIface.DocumentData BuildDocumentData(string RepName, string JRName, string JRName2, string JobNumber, string date, string date2, string recipient, string ambiente)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportSearch.XReportWebIface.IndexEntry();
                XReportWebIface.column ReportName = new XReportWebIface.column();
                ReportName.colname = "ReportName";
                if (RepName != null && RepName != "")
                {
                    if (ConfigurationManager.AppSettings["SearchLike"].ToString() == "0")
                        ReportName.Value = RepName;
                    else
                        ReportName.Value = RepName + "%";
                }
                else
                    ReportName.Value = RepName;
                XReportWebIface.column JobName = new XReportWebIface.column();
                JobName.colname = "JobName";
                if ((JRName != null && JRName != "") || (JRName2 != null && JRName2 != ""))
                    JobName.Value = JRName + "%" + JRName2;
                else
                    JobName.Value = "";
                XReportWebIface.column JobNum = new XReportWebIface.column();
                JobNum.colname = "JobNumber";
                if (JobNumber != null && JobNumber != "")
                    JobNum.Value = JobNumber + "%";
                else
                    JobNum.Value = JobNumber;

                XReportWebIface.column XferStartTime = new XReportWebIface.column();
                XferStartTime.colname = "UserTimeRef";
                XferStartTime.operation = "date";
                DateTime dt = DateTime.ParseExact(date, "dd/MM/yyyy", null);
                XferStartTime.Min = dt.ToString("yyyyMMdd");
                dt = DateTime.ParseExact(date2, "dd/MM/yyyy", null);
                XferStartTime.Max = dt.ToString("yyyyMMdd");

                XReportWebIface.column User = new XReportWebIface.column();
                User.colname = "User";
                if (this.tb_specUser.Text != "")
                    User.Value = this.tb_specUser.Text;
                else if (Navigation.User.Profilo != null && Navigation.User.Profilo != "")
                    User.Value = Navigation.User.Profilo;
                else
                    User.Value = "";
                XReportWebIface.column Recipient = new XReportWebIface.column();
                Recipient.colname = "Recipient";
                Recipient.Value = recipient;
                if (recipient != null && recipient != "")
                    Recipient.Value = recipient + "%";
                else
                    Recipient.Value = "";
                XReportWebIface.column Ambiente = new XReportWebIface.column();
                Ambiente.colname = "Ambiente";
                Ambiente.Value = ambiente;
                XReportWebIface.column CurrentRow = new XReportWebIface.column();
                CurrentRow.colname = "CurrentRow";
                CurrentRow.Value = "0";

                XReportWebIface.column FilterRoot = new XReportWebIface.column();
                FilterRoot.colname = "FilterRoot";
                if (this.ddl_group.SelectedValue.ToString() == "-1")
                    FilterRoot.Value = "%";
                else
                    FilterRoot.Value = this.ddl_group.SelectedValue.ToString();

                Entry.Columns = new XReportWebIface.column[8] { ReportName, JobName, XferStartTime, JobNum, User, Ambiente, CurrentRow, Recipient };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                if (Navigation.CurrentSortExpression != null && Navigation.CurrentSortExpression != "")
                    Data.ORDERBY = Navigation.CurrentSortExpression + " " + Navigation.CurrentSortDirection;
                else
                    Data.ORDERBY = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                return null;
                //throw Ex;
            }
        }

        private XReportWebIface.DocumentData BuildDocumentDataPDF(string JRID, string FromPag)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column JobRepID = new XReportWebIface.column();
                JobRepID.colname = "JobReportId";
                JobRepID.Value = JRID;
                XReportWebIface.column FromPage = new XReportWebIface.column();
                FromPage.colname = "FromPage";
                FromPage.Value = FromPag;

                Entry.Columns = new XReportWebIface.column[2] { JobRepID, FromPage };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                return null;
                //throw Ex;
            }
        }

        private XReportWebIface.DocumentData BuildDocumentDataNotes(string JobRepID, string RepID, string version, string UpdUser, string text, string type, string foldername, int isRemark, Utility.Operation operation)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column JobReportID = new XReportWebIface.column();
                JobReportID.colname = "JobReportID";
                JobReportID.Value = JobRepID;
                XReportWebIface.column ReportID = new XReportWebIface.column();
                ReportID.colname = "ReportID";
                ReportID.Value = RepID;
                XReportWebIface.column Version = new XReportWebIface.column();
                Version.colname = "Version";
                Version.Value = version;
                XReportWebIface.column UpdateUser = new XReportWebIface.column();
                UpdateUser.colname = "UpdateUser";
                UpdateUser.Value = UpdUser;
                XReportWebIface.column Text = new XReportWebIface.column();
                Text.colname = "Text";
                Text.Value = text;
                XReportWebIface.column IsRemark = new XReportWebIface.column();
                IsRemark.colname = "IsRemark";
                IsRemark.Value = isRemark.ToString();

                XReportWebIface.column FolderName = new XReportWebIface.column();
                FolderName.colname = "FolderName";
                FolderName.Value = foldername;
                XReportWebIface.column Type = new XReportWebIface.column();
                Type.colname = "Type";
                Type.Value = type;

                Entry.Columns = new XReportWebIface.column[8] { JobReportID, ReportID, Version, UpdateUser, Text, IsRemark, FolderName, Type };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentDataNotes(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentDataNotes(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                return null;
                //throw Ex;
            }
        }

        private XReportWebIface.DocumentData BuildDocumentDataCheck(string JRID, string ReportID, string CheckUser)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column JobRepID = new XReportWebIface.column();
                JobRepID.colname = "JobReportID";
                JobRepID.Value = JRID;
                XReportWebIface.column RepID = new XReportWebIface.column();
                RepID.colname = "ReportID";
                RepID.Value = ReportID;
                XReportWebIface.column User = new XReportWebIface.column();
                User.colname = "CheckUser";
                User.Value = CheckUser;

                Entry.Columns = new XReportWebIface.column[3] { JobRepID, RepID, User };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentDataCheck(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentDataCheck(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                return null;
                //throw Ex;
            }
        }

        private void setNavigateUrl(CeTreeView genTree)
        {
            int i = 0;
            foreach (TreeNode t in genTree.Nodes)
            {
                t.NavigateUrl = "javascript:TreeView_SelectNode(" + genTree.ClientID + "_Data,document.getElementById('" + genTree.ClientID + "t" + i + "'),'" + genTree.ClientID + "t" + i + "');simulate_click();";
                setNavigateUrlForChildren(t, ref i, genTree.ClientID);
            }
        
        }

        private void setNavigateUrlForChildren(TreeNode t, ref int i, string id)
        {
            i++;
            foreach (TreeNode child in t.ChildNodes)
            {
                child.NavigateUrl = "javascript:TreeView_SelectNode(" + id + "_Data, document.getElementById('" + id + "t" + i + "') ,'" + id + "t" + i + "');simulate_click();";
                setNavigateUrlForChildren(child, ref i, id); //getElementById('"++ "')
            }
        }
        
        private void LoadNodes(DataTable mytable, TreeNode   topNode){
            foreach (DataRow dr in mytable.Rows) {
                    TreeNode node = new CeTreeNode(dr["FolderNameKey"].ToString());
                    node.Value = dr["FolderNameKey"].ToString();
                    node.ToolTip = dr["FolderDescr"].ToString();
                    node.ImageUrl = "~/Images/folder-closed-2.jpg";
                    
                    //node.NavigateUrl = "javascript:simulate_click(event);";
                    //node.NavigateUrl = "javascript:void(0);";
                    node.SelectAction = TreeNodeSelectAction.Select; // selectexpand togheter create problems with javascript submitting form
                    if (dr["HasChilds"].ToString().Equals("1"))
                        node.ChildNodes.Add(new TreeNode("FAKE"));
                    topNode.ChildNodes.Add(node);
                    node.CollapseAll();
            }
        }

        private static bool SearchNode(DataRow dr, TreeNode topNode)
        {
            foreach (TreeNode tn in topNode.ChildNodes)
            {
                    if (dr["FolderName"].ToString().Contains(tn.Value)) {
                        return true;
                    }
                    
                }
            return false;
        }
        /*
         * Load other notes only if topnode doesn't contains the node itself or a parent node
         * 
        */

        private void LoadSecondaryNodes(DataTable mytable, TreeNode topNode)
        {
            foreach (DataRow dr in mytable.Rows)
            {

                if (!SearchNode(dr, topNode)) {
                    TreeNode node = new CeTreeNode(dr["FolderNameKey"].ToString());
                    node.Value = dr["FolderNameKey"].ToString();
                    node.ToolTip = dr["FolderDescr"].ToString();
                    node.ImageUrl = "~/Images/folder-closed-2.jpg";
                    //node.NavigateUrl = "javascript:simulate_click(event);";
                    //node.NavigateUrl = "javascript:void(0);";
                    node.SelectAction = TreeNodeSelectAction.Select; // selectexpand togheter create problems with javascript submitting form
                    if (dr["HasChilds"].ToString().Equals("1"))
                        node.ChildNodes.Add(new TreeNode("FAKE"));
                    topNode.ChildNodes.Add(node);
                    node.CollapseAll();
                }
            }
        }

        public DataTable LoadFolders(string FolderNameParent, int type)
        {
            try
            {
                string startSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture); string logParms = "";
                SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["SQL_CONNECTION"]);
                conn.Open();
                DataSet ds = new DataSet("Folders");
                string storedName = "XRFolderTreeChild";
                
                SqlCommand sqlComm = new SqlCommand(storedName, conn);
                
                sqlComm.Parameters.AddWithValue("@FolderNameKey", FolderNameParent);
                logParms += "FolderNameParent == " + FolderNameParent+"::";
                sqlComm.Parameters.AddWithValue("@type", type);
                logParms += "type == " + type + "::";
                //logParms.Remove(logParms.Length - 2, 2);
                sqlComm.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm; sqlComm.CommandTimeout = 120; //time out a 120 secondi 
                da.Fill(ds);
                
                DataTable mytable = ds.Tables[0];
                int rowSize = mytable.Rows.Count;
                conn.Close();
                Response.AppendToLog("XRFolderTreeChild_Close");
                log.Debug("XRFolderTreeChild_Close");
                string endSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                
                logParms += "ResSize == " + rowSize;
                log.Info("XRFolderTreeChild_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSP + "_" + logParms + " 80 - 127.0.0.1 Mozilla/4.0+(compatible;+MSIE+6.0;+MS+Web+Services+Client+Protocol+4.0.30319.1) 200 0 0 " + rowSize);
                Response.AppendToLog("XRFolderTreeChild_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSP + "_" + logParms);
                return mytable;
               
                //log.Debug("LoadReports_SP() Finished");
            }
            catch (Exception Ex)
            {
                log.Error("LoadFoders() Failed: " + Ex.Message + " " + FolderNameParent);
                Response.AppendToLog("XRFolderTreeChild_Failed" + Ex.Message + " "+ FolderNameParent);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                return null;
                //throw Ex;
            }
        }

        public DataTable LoadFoldersByUser(string User)
        {
            try
            {
                string startSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture); string logParms = "";
                SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["SQL_CONNECTION"]);
                conn.Open();
                DataSet ds = new DataSet("Folders");
                string storedName = "XRFoldersOfUser";

                SqlCommand sqlComm = new SqlCommand(storedName, conn);
                
                sqlComm.Parameters.AddWithValue("@UserName", User);

                logParms += "UserName == " + User + "::";
                //logParms.Remove(logParms.Length - 2, 2);
                sqlComm.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm; sqlComm.CommandTimeout = 120; //time out a 120 secondi 
                da.Fill(ds);

                DataTable mytable = ds.Tables[0];
                int rowSize = mytable.Rows.Count;
                conn.Close();
                Response.AppendToLog("XRFoldersOfUser_Close");
                log.Debug("XRFoldersOfUser_Close");
                string endSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);

                logParms += "ResSize == " + rowSize;
                log.Info("XRFoldersOfUser_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSP + "_" + logParms + " 80 - 127.0.0.1 Mozilla/4.0+(compatible;+MSIE+6.0;+MS+Web+Services+Client+Protocol+4.0.30319.1) 200 0 0 " + rowSize);
                Response.AppendToLog("XRFoldersOfUser_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSP + "_" + logParms);
                return mytable;

                //log.Info("LoadReports_SP() Finished");
            }
            catch (Exception Ex)
            {
                log.Error("XRFoldersOfUser Failed: " + Ex.Message + " " + User);
                Response.AppendToLog("XRFoldersOfUser_Failed" + Ex.Message + " " + User);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                return null;
                //throw Ex;
            }
        }

        private string[] RetrieveSavedSearch()
        {
            
            string[] filterList = null;
            List<ListItem> ls = new List<ListItem>(); Dictionary<string, string> curr ;
            ls.Add(new ListItem(""));
            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["SQL_CONNECTION"]))
            {
                //SqlDataAdapter da = new SqlDataAdapter("SELECT ImgXmlFile, ChangeMode FROM [tbl_LogicalRep_TextNotes] WHERE JobReportId = @jobReportid, ReportId = @reportid,Numpage= @numpage", conn);
                //SqlCommandBuilder cd = new SqlCommandBuilder(da);
                //DataSet ds = new DataSet();
                //da.Fill(ds);
                string user = Navigation.User.Username;
                if (Navigation.SavedSearch == null)
                    Navigation.SavedSearch = new Dictionary<string, IDictionary<string, string>>();
                using (var sqlComm = new SqlCommand("select top " + MAXN_OF_SAVED_SEARCH + " * from [tbl_UserSavedFilters] WHERE UserName = @userName order by InsDateTime desc", conn))
                {
                    conn.Open();
                    sqlComm.Parameters.AddWithValue("@userName", user);
                   
                    sqlComm.CommandTimeout = 120; //time out a 120 secondi 

                    SqlDataReader result = sqlComm.ExecuteReader(); int i = 1; //from 1 because added in ls default empty value
                    while (result.Read())
                    {
                        //this.CSavedSearch.Items.Add(result["FilterName"].ToString());
                        //JobName	ReportName	JobNumber	AdvisorCode	FolderName	Remark	Path
                        ls.Add(new ListItem(result["FilterName"].ToString()));
                        curr = new Dictionary<string, string>();
                        string[] StoredFieldsList = "JobName;ReportName;JobNumber;AdvisorCode;FolderName;Remark;Path".Split(';');
                        foreach(string value in StoredFieldsList){
                            curr.Add(value, result[value].ToString());
                        }
                        Navigation.SavedSearch[ls[i].Text] = curr;
                        i++;
                    }
                }
                this.CSavedSearch.Items.Clear();
                this.CSavedSearch.Items.AddRange(ls.ToArray());
                this.CSavedSearch.SelectedIndex = -1;
                
            }
            return filterList;
        }

        private void deleteSavedSearch(string filterName)
        {

            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["SQL_CONNECTION"]))
            {
                string user = Navigation.User.Username;
                string startSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture); string logParms = "";
                conn.Open();
                int retvalue = 0;
                const string storedDeletedSql = @"DELETE FROM [tbl_UserSavedFilters] where UserName = @UserName and  filterName = @filterName ";
                using (SqlTransaction sqlTrans = conn.BeginTransaction())
                {
                    try
                    {
                        using (SqlCommand sqlComm = new SqlCommand(storedDeletedSql, conn))
                        {
                            sqlComm.Transaction = sqlTrans;
                            sqlComm.Parameters.AddWithValue("@userName", user);
                            logParms += "User == " + user;
                            sqlComm.Parameters.AddWithValue("@filterName", filterName);
                            logParms += "FilterName == " + user;
                            sqlComm.CommandTimeout = 120; //time out a 120 secondi 
                            retvalue = sqlComm.ExecuteNonQuery();
                        }
                        sqlTrans.Commit();
                    }
                    catch(Exception ex)
                    {
                        log.Error("SaveToDB() Failed: " + ex.Message + " User" + user + "FilterName" + filterName);
                        sqlTrans.Rollback();
                        throw new Exception("Saved Filter Delete failed because ", ex);
                    }
                }
                conn.Close();
                log.Debug("deleteSavedSearch" + " User" + user + "FilterName" + filterName);
                string endSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                log.Info("DeleteSavedSearch_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSP + "_" + logParms + " 80 - 127.0.0.1 Mozilla/4.0+(compatible;+MSIE+6.0;+MS+Web+Services+Client+Protocol+4.0.30319.1) 200 0 0 ");
            }
        }

        private bool updateUserList(string FilterName, string JRName, string RepName, string JobNumber, string AdvisorCode, string FolderName, string Remark, string Path)
                
        {   
            string user = Navigation.User.Username;
            try
            {
        
                string startSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture); string logParms = "";
                SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["SQL_CONNECTION"]);
                conn.Open();
                //string storedName = "SaveToDB";const int NowRowsAffected = 0;
                int retvalue = 0; 
                
                string storedUpdateSql = @"BEGIN IF NOT EXISTS (select 1 from [tbl_UserSavedFilters] where UserName = @UserName and  filterName = @filterName ) " +
                     "BEGIN IF ((select count(*) from  [tbl_UserSavedFilters] where UserName = @UserName) <= " + MAXN_OF_SAVED_SEARCH + " )" +
                    " INSERT INTO [tbl_UserSavedFilters] (UserName, FilterName, JobName, ReportName, JobNumber, AdvisorCode, FolderName, Remark, [Path], InsDateTime ) VALUES ( @userName ,@filterName , @jobName, @reportName , @jobNumber, @advisorCode , @folderName, @remark, @path , getDate())  " +
                    "  END 	" +
                    " ELSE UPDATE [tbl_UserSavedFilters] set InsDateTime = getDate() , JobName = @jobName, ReportName = @ReportName, JobNumber = @JobNumber, AdvisorCode = @advisorCode , FolderName = @folderName, Remark = @remark, [Path] =  @path where UserName = @UserName and  filterName = @filterName END";
                //const string storedIntsertedSql = @"INSERT INTO tbl_LogicalRep_TextNotes (JobReportId, ReportId,Numpage,FolderPath,[User],ImgXmlFile) VALUES(@jobReportid, @reportid, @numpage,@FolderPath, @user, @Xml)";
                using (SqlTransaction sqlTrans = conn.BeginTransaction())
                {
                    try
                    {
                        using (SqlCommand sqlComm = new SqlCommand(storedUpdateSql, conn))
                        {
                            sqlComm.Transaction = sqlTrans;

                            if (user != null && user != "")
                            {
                                sqlComm.Parameters.AddWithValue("@userName", user);
                                logParms += "UserName == " + user + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@userName", DBNull.Value);
                            
                            if (FilterName != null && FilterName != "")
                            {
                                sqlComm.Parameters.AddWithValue("@filterName", FilterName);
                                logParms += "FilterName == " + FilterName + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@filterName", DBNull.Value);

                            if (JRName != null && JRName != "")
                            {
                                sqlComm.Parameters.AddWithValue("@jobName", JRName);
                                logParms += "JobName == " + JRName + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@jobName", DBNull.Value);

                            if (RepName != null && RepName != "")
                            {
                                sqlComm.Parameters.AddWithValue("@reportName", RepName);
                                logParms += "ReportName == " + RepName + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@reportName", DBNull.Value);
                            
                            if (JobNumber != null && JobNumber != "")
                            {
                                sqlComm.Parameters.AddWithValue("@jobNumber", JobNumber);
                                logParms += "JobNumber == " + JobNumber + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@jobNumber", DBNull.Value);

                            if (AdvisorCode != null && AdvisorCode != "")
                            {
                                sqlComm.Parameters.AddWithValue("@advisorCode", AdvisorCode);
                                logParms += "AdvisorCode == " + AdvisorCode + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@advisorCode", DBNull.Value);
                            if (FolderName != null && FolderName != "")
                            {
                                sqlComm.Parameters.AddWithValue("@folderName", FolderName);
                                logParms += "AdvisorCode == " + FolderName + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@folderName", DBNull.Value);
                            
                            if (Remark != null && Remark != "")
                            {
                                sqlComm.Parameters.AddWithValue("@remark", Remark);
                                logParms += "Remark == " + Remark + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@remark", DBNull.Value);
                            
                            if (Path != null && Path != "")
                            {
                                sqlComm.Parameters.AddWithValue("@path", Path);
                                logParms += "Path == " + Path + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@path", DBNull.Value);
                            sqlComm.CommandTimeout = 120; //time out a 120 secondi 
                            retvalue = sqlComm.ExecuteNonQuery();
                        }
                        sqlTrans.Commit();
                        
                    }
                    
                    catch (Exception Ex)
                    {
                        log.Error("updateUserList() Failed: " + Ex.Message);
                        sqlTrans.Rollback();
                        throw Ex;
                    }
                }
                conn.Close();
                log.Debug("updateUserList Succefful " + logParms);
                string endSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                //logParms += "ResSize == " + rowSize;
                log.Info("updateUserList_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSP + "_" + logParms + " 80 - 127.0.0.1 Mozilla/4.0+(compatible;+MSIE+6.0;+MS+Web+Services+Client+Protocol+4.0.30319.1) 200 0 0 ");
                if(retvalue == -1){
                        throw new Exception(" WARNING : <br/> Max Number ("+ MAXN_OF_SAVED_SEARCH +") of Saved Searched Reached <br/> Delete others before saving again  ");
                }
                return true;
                //log.Info("LoadReports_SP() Finished");
            }
            catch (Exception Ex)
            {

                log.Error("updateUserList() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                //return false;
                //return null;
                throw Ex;
            }
        }

        public void LoadReports(string RepName, string JRName, string JRName2, string JobNumber, string date, string date2, string hour, string hour2, string recipient, string ambiente, string remark, bool flagLastExecution)
        {
            if (Utility.QuerySearchType.SP.ToString().Equals(ConfigurationManager.AppSettings["SearchArch"].ToString()))
                LoadReports_SP(RepName, JRName, JRName2, JobNumber, date, date2, hour, hour2, recipient, ambiente, remark, flagLastExecution);
            else if (Utility.QuerySearchType.SQL.ToString().Equals(ConfigurationManager.AppSettings["SearchArch"].ToString()))
                LoadReports_SQL(RepName, JRName, JRName2, JobNumber, date, date2, recipient, ambiente);
            else if (Utility.QuerySearchType.WS.ToString().Equals(ConfigurationManager.AppSettings["SearchArch"].ToString()))
                LoadReports_WS(RepName, JRName, JRName2, JobNumber, date, date2, recipient, ambiente);

        }


        //#Version: 1.0
        //#Date: 2017-09-13 12:30:14
        //#Fields: date time s-ip cs-method cs-uri-stem cs-uri-query s-port cs-username c-ip cs(User-Agent) sc-status sc-substatus sc-win32-status time-taken
        //2017-09-13 12:30:14 127.0.0.1 POST /XReportWebIface/xrIISWebServ.asp wsdlgetReportsData_start_at_2017-09-13+14:30:10.152_-ID_0-_Times(us::sy::cus::csy):_0.109::0.0630000000000002::0::0_C059434_2017-09-13+14:30:14.105_JobName+==+KAg30EA3%::User+==+X1NOPR::UserTimeRef+==+20170912||20170913 60080 - 127.0.0.1 Mozilla/4.0+(compatible;+MSIE+6.0;+MS+Web+Services+Client+Protocol+4.0.30319.1) 200 0 0 8062
        public void LoadReports_SP(string RepName, string JRName, string JRName2, string JobNumber, string date, string date2, string hour, string hour2, string recipient, string ambiente, string remark, bool flagLastExecution)
        {
            try
            {
                string user; string user2;
                if (this.tb_specUser.Text != "")
                {
                    user = this.tb_specUser.Text;
                    user2 = "";
                }
                else if (Navigation.User.Profilo != null && Navigation.User.Profilo != "")
                {
                    //TODO se profilo è ADM 
                    // if() ConfigurationManager.AppSettings["SKIP_PGE_4ALL"].ToString().ToUpper() != null 
                    // perchè  se è skippato  sulla stored deve andarci solo Profile ADM ma dovrebbe skippare comunque
                    //user2 = ""
                    if (Navigation.User.isAustrian()) {
                        if (Navigation.User.getRealAltProfilo == null || ("START".Equals(string.Join(" , ", Navigation.User.getRealAltProfilo)) || "".Equals(string.Join(" , ", Navigation.User.getRealAltProfilo))))
                        {
                            user = Navigation.User.Profilo;
                            user2 = string.Join("' OR ua.UserAlias like '", new string[] {string.Join( "' OR ua.UserAlias like '", Navigation.User.AltProfilo), Navigation.User.Username} );
                        }
                        else {
                            //user = Navigation.User.getRealAltProfilo;
                            //user = string.Join("' OR ua.UserAlias like  '", new string[] {string.Join( "' OR ua.UserAlias like  '", Navigation.User.getRealAltProfilo), Navigation.User.Username} );
                            user =  string.Join("' OR ua.UserAlias like '", Navigation.User.getRealAltProfilo);
                            user2 = "";
                        }
                    }
                    else
                    {
                        user = Navigation.User.Profilo;
                        user2 = Navigation.User.OrganizationUnit.Contains("N/A") ? "" : Navigation.User.TorrePG + Navigation.User.OrganizationUnit.TrimEnd().Substring(2);
                    }
                    
                }
                else
                {
                    user = "";
                    user2 = "";
                }

                ResetBind();
                string startSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture); string logParms = "";
                SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["SQL_CONNECTION"]);
                conn.Open();
                DataSet ds = new DataSet("Reports");
                string storedName = (ConfigurationManager.AppSettings["StoredName"] != null ? ConfigurationManager.AppSettings["StoredName"] : "XRGetReportsData");
                string StoredFields = (ConfigurationManager.AppSettings["StoredFields"] != null ? ConfigurationManager.AppSettings["StoredFields"] :
                    "JobName;ReportName;JobNumber;Recipient;DataFrom;DataTo;Orderby;User;Remark;FlagLastExecution;JobReportid;Reportid"); 

                string[] StoredFieldsList = StoredFields.Split(';');

                SqlCommand sqlComm = new SqlCommand(storedName, conn);
                DateTime dt;
                for (int i = 0; i < StoredFieldsList.Length; i++)
                {
                    switch (StoredFieldsList[i])
                    {
                        case "JobName":
                            if ((JRName != null && JRName != "") || (JRName2 != null && JRName2 != ""))
                            {
                                sqlComm.Parameters.AddWithValue("@JobName", JRName + "%" + JRName2);
                                logParms += "JobName == " + JRName + "%" + JRName2 + "::";
                            }
                            else {
                                sqlComm.Parameters.AddWithValue("@JobName",  "%" );
                                logParms += "JobName == %::";
                            
                            }
                            break;
                        case "ReportName":
                            if (RepName != null && RepName != "")
                            {
                                sqlComm.Parameters.AddWithValue("@ReportName", RepName);
                                logParms += "ReportName == " + RepName + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@ReportName", DBNull.Value);
                            break;
                        case "JobNumber":
                            if (JobNumber != null && JobNumber != "")
                            {
                                Match match = Regex.Match(JobNumber, @"^(?:(?!JOB).*)$", RegexOptions.IgnoreCase);
                                if (match.Success)
                                    JobNumber = "%" + JobNumber;
                                sqlComm.Parameters.AddWithValue("@JobNumber", JobNumber);
                                logParms += "JobNumber == " + JobNumber + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@JobNumber", DBNull.Value);
                            break;
                        case "Recipient":
                            if (recipient != null && recipient != "")
                            {
                                Match match = Regex.Match(recipient, @"([^A-Za-z]{2}).*$", RegexOptions.IgnoreCase);
                                if (match.Success)
                                {
                                    string key = match.Groups[1].Value;
                                    recipient = "%" + recipient;

                                }
                                sqlComm.Parameters.AddWithValue("@Recipient", recipient);
                                logParms += "Recipient == " + recipient + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@Recipient", DBNull.Value);
                            break;
                        case "DataFrom":
                            if (!(hour.Equals("") || hour.Equals("__:__"))) {hour += ":00.000";} else {hour = "00:00:00.000";};
                            //dt = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dt = DateTime.ParseExact(date + " " + hour, "dd/MM/yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                            //sqlComm.Parameters.AddWithValue("@DataFrom", dt.ToString("yyyyMMdd"));
                            sqlComm.Parameters.AddWithValue("@DataFrom", dt.ToString("yyyy-MM-dd") + "T" + hour);
                            logParms += "UserTimeRef == " + dt.ToString("yyyyMMdd") + "||";
                            break;
                        case "DataTo":
                            if (!(hour2.Equals("") || hour2.Equals("__:__"))) {hour2 += ":59.999";} else {hour2 = "23:59:59.999";};
                            //dt = DateTime.ParseExact(date2, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dt = DateTime.ParseExact(date2+" "+hour2, "dd/MM/yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                            //sqlComm.Parameters.AddWithValue("@DataTo", dt.ToString("yyyyMMdd"));
                            sqlComm.Parameters.AddWithValue("@DataTo", dt.ToString("yyyy-MM-dd") + "T" + hour2);
                            logParms += dt.ToString("yyyyMMdd") + "::";
                            break;
                        case "Orderby":
                            if (Navigation.CurrentSortExpression != null && Navigation.CurrentSortExpression != "")
                            {
                                sqlComm.Parameters.AddWithValue("@Orderby", Navigation.CurrentSortExpression + " " + Navigation.CurrentSortDirection);
                                logParms += "Orderby == " + Navigation.CurrentSortExpression + "+" + Navigation.CurrentSortDirection + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@Orderby", DBNull.Value);
                            break;
                        case "User":
                            if (user != null && user != "")
                            {
                                sqlComm.Parameters.AddWithValue("@User", user);
                                logParms += "User == " + user + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@User", DBNull.Value);
                            break;
                        case "User2":
                            if (user2 != null && user2 != "")
                            {
                                sqlComm.Parameters.AddWithValue("@User2", user2);
                                logParms += "User2 == " + user2 + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@User2", DBNull.Value);
                            break;
                        case "Remark":
                            if (remark != null && remark != "")
                            {
                                sqlComm.Parameters.AddWithValue("@Remark", remark);
                                logParms += "Remark == " + remark + "::";
                            }
                            else
                                sqlComm.Parameters.AddWithValue("@Remark", DBNull.Value);
                            break;
                        
                        case "FlagLastExecution":
                                sqlComm.Parameters.AddWithValue("@FlagLastExecution", (flagLastExecution ? "1" : "0"));
                                logParms += "FlagLastExecution == " + (flagLastExecution ? "1" : "0") + "::";
                                break;    
                        case "JobReportid":
                                sqlComm.Parameters.AddWithValue("@JobReportid", DBNull.Value);
                            break;
                        case "Reportid":
                                sqlComm.Parameters.AddWithValue("@Reportid", DBNull.Value);
                            break;
                        default:
                            log.Error("Failed in parameter StoredFields");
                            throw new Exception("Failed in parameter StoredFields - field " + StoredFieldsList[i] + " not managed");
                    }

                }

                //if (RepName != null && RepName != ""){
                //    sqlComm.Parameters.AddWithValue("@ReportName", RepName);
                //    logParms += "ReportName == "+RepName+"::";
                //}
                //else
                //    sqlComm.Parameters.AddWithValue("@ReportName", DBNull.Value);
                //if ((JRName != null && JRName != "") || (JRName2 != null && JRName2 != "")){
                //    sqlComm.Parameters.AddWithValue("@JobName", JRName + "%" + JRName2);
                //        logParms += "JobName == "+JRName + "%" + JRName2+"::";
                //}
                //else
                //    sqlComm.Parameters.AddWithValue("@JobName", DBNull.Value);
                //if (JobNumber != null && JobNumber != ""){
                //    Match match = Regex.Match(recipient, @"^(?:(?!JOB).)*$", RegexOptions.IgnoreCase);
                //    if (match.Success)
                //        JobNumber = "%" + JobNumber;
                //    sqlComm.Parameters.AddWithValue("@JobNumber", JobNumber);
                //        logParms += "JobNumber == "+JobNumber+"::";
                //}
                //else
                //    sqlComm.Parameters.AddWithValue("@JobNumber", DBNull.Value);
                //if (recipient != null && recipient != ""){
                //    Match match = Regex.Match(recipient, @"([^A-Za-z]{2}).*$",RegexOptions.IgnoreCase);
                //    if (match.Success) {
                //        string key = match.Groups[1].Value;
                //        recipient = "%" + recipient;

                //    }
                //    sqlComm.Parameters.AddWithValue("@Recipient", recipient);
                //    logParms += "Recipient == "+recipient+"::";
                //}
                //else
                //    sqlComm.Parameters.AddWithValue("@Recipient", DBNull.Value);
                //if (user != null && user != ""){
                //    sqlComm.Parameters.AddWithValue("@User", user);
                //    logParms += "User == "+user+"::";
                //}
                //else
                //    sqlComm.Parameters.AddWithValue("@User", DBNull.Value);
                //if (user2 != null && user2 != "")
                //{
                //    sqlComm.Parameters.AddWithValue("@User2", user2);
                //    logParms += "User2 == " + user2 + "::";
                //}
                //else
                //    sqlComm.Parameters.AddWithValue("@User2", DBNull.Value);
                //DateTime dt = DateTime.ParseExact(date, "dd/MM/yyyy", null);
                //sqlComm.Parameters.AddWithValue("@DataFrom", dt.ToString("yyyyMMdd"));
                //logParms += "UserTimeRef == "+dt.ToString("yyyyMMdd")+"||";

                //dt = DateTime.ParseExact(date2, "dd/MM/yyyy", null);
                //sqlComm.Parameters.AddWithValue("@DataTo", dt.ToString("yyyyMMdd"));
                //logParms += dt.ToString("yyyyMMdd")+"::";

                //if (Navigation.CurrentSortExpression != null && Navigation.CurrentSortExpression != ""){
                //    sqlComm.Parameters.AddWithValue("@Orderby", Navigation.CurrentSortExpression + " " + Navigation.CurrentSortDirection);
                //    logParms += "Orderby == "+Navigation.CurrentSortExpression + "+" + Navigation.CurrentSortDirection+"::";
                //}
                //else
                //    sqlComm.Parameters.AddWithValue("@Orderby", DBNull.Value);
				//
                logParms.Remove(logParms.Length - 2, 2);
                sqlComm.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm; sqlComm.CommandTimeout = 180; //time out a 180 secondi 
                da.Fill(ds);

                
                DataTable mytable = ds.Tables[0];
                
                if(!mytable.Columns.Contains("Remark"))
                {
                    mytable.Columns.Add("Remark"); 
                    //test
                    foreach (DataRow row in mytable.Rows)
                    { 
                        //need to set value to NewColumn column
                        row["Remark"] = "0123456789123456";   // or set it to some other value
                    }
                }
                
                Navigation.TableSource = mytable; int rowSize = mytable.Rows.Count;
                //Navigation.TableSource = ds.Tables[0]; int rowSize = ds.Tables[0].Rows.Count;
                conn.Close();
                Response.AppendToLog("XRGetReportsData_Connection_CLOSE");
                log.Debug("XRGetReportsData_Connection_CLOSE");
                string endSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                Navigation.CurrentGridPage = 0;
                BindGridView();
                Response.AppendToLog("BindGridView_ended");
                log.Debug("BindGridView_ended");
                if (Navigation.TableSource != null && rowSize >= 1000)
                {
                    this.lb_Popup.Text = GetLocalResourceObject("warn.moreres").ToString();
                    this.ModalPopupExtender2.Show();
                }
                logParms += "ResSize == " + rowSize;
                log.Info("XRGetReportsData_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSP + "_" + logParms + " 80 - 127.0.0.1 Mozilla/4.0+(compatible;+MSIE+6.0;+MS+Web+Services+Client+Protocol+4.0.30319.1) 200 0 0 " + rowSize);
                Response.AppendToLog("XRGetReportsData_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSP + "_" + logParms);
                //log.Info("LoadReports_SP() Finished");
            }
            catch (Exception Ex)
            {
                log.Error("LoadReports_SP() Failed: " + Ex.Message + " " + RepName + " " + JRName + " " + JRName2 + " " + JobNumber + " " + date + " " + date2 + " " + recipient + " " + ambiente);
                Response.AppendToLog("XRGetReportsData_Failed" + Ex.Message + " " + RepName + " " + JRName + " " + JRName2 + " " + JobNumber + " " + date + " " + date2 + " " + recipient + " " + ambiente);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        public void LoadReports_SQL(string RepName, string JRName, string JRName2, string JobNumber, string date, string date2, string recipient, string ambiente)
        {
            try
            {
                log.Info("LoadReports_SQL Started");
                string user; string user2;
                if (this.tb_specUser.Text != "")
                {
                    user = this.tb_specUser.Text;
                    user2 = "";
                }
                else if (Navigation.User.Profilo != null && Navigation.User.Profilo != "")
                {
                    //TODO se profilo è ADM 
                    // if() ConfigurationManager.AppSettings["SKIP_PGE_4ALL"].ToString().ToUpper() != null 
                    // perchè  se è skippato  sulla stored deve andarci solo Profile ADM ma dovrebbe skippare comunque
                    //user2 = ""
                    user = Navigation.User.Profilo;
                    user2 = Navigation.User.OrganizationUnit.Contains("N/A") ? "" : Navigation.User.TorrePG + Navigation.User.OrganizationUnit.TrimEnd().Substring(2);
                }
                else
                {
                    user = "";
                    user2 = "";
                }
                ResetBind();
                SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["SQL_CONNECTION"]);
                SqlDataAdapter da = new SqlDataAdapter(CHECK_ROOT.Replace("#{User}#", user), conn);
                SqlCommandBuilder cd = new SqlCommandBuilder(da);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0 && "ROOT".Equals(ds.Tables[0].Rows[0]["RootNode"]))
                {
                    user = null;
                }

                string sql_query = SELECT_SEARCH; string logParms = "";

                if (RepName != null && RepName != "")
                {
                    sql_query = sql_query.Replace("#{ReportName}#", "AND REPORTNAME like '" + RepName + "%'");
                    logParms += "ReportName == " + RepName + "::";
                }
                else
                    sql_query = sql_query.Replace("#{ReportName}#", "");
                if ((JRName != null && JRName != "") || (JRName2 != null && JRName2 != ""))
                {
                    sql_query = sql_query.Replace("#{JobName}#", "AND ComputedJobName like '" + JRName + "%" + JRName2 + "'");
                    logParms += "JobName == " + JRName + "%" + JRName2 + "::";
                }
                else
                    sql_query = sql_query.Replace("#{JobName}#", "");

                if (JobNumber != null && JobNumber != "")
                {
                    sql_query = sql_query.Replace("#{JobNumber}#", "JobNumber like '" + JobNumber + "%'");
                    logParms += "JobNumber == " + JobNumber + "::";
                }
                else
                    sql_query = sql_query.Replace("#{JobNumber}#", "");

                if (recipient != null && recipient != "")
                {
                    sql_query = sql_query.Replace("#{Recipient}#", "AND Recipient like '" + recipient + "%'");
                    logParms += "Recipient == " + recipient + "::";
                }
                else
                    sql_query = sql_query.Replace("#{Recipient}#", "");

                if (user != null && user != "")
                {
                    string wherecond = (user2 != null && user2 != "") ? "  where (ua.UserName like '" + user + "' OR ua.UserName like '" + user2 + "' ) " : " where ua.UserName like '" + user + "' ";
                    sql_query = sql_query.Replace("#{OPTION_FOLDER_USER}#", OPTION_FOLDER_USER.Replace("#{wherecond}#", wherecond));
                    logParms += "User == " + user + "::";
                }
                else
                {
                    sql_query = sql_query.Replace("#{OPTION_FOLDER_USER}#", "");
                    logParms += "User == " + Navigation.User.Profilo + "::";
                }
                DateTime dt = DateTime.ParseExact(date, "dd/MM/yyyy", null);
                sql_query = sql_query.Replace("#{DataFrom}#", dt.ToString("yyyyMMdd"));
                logParms += "UserTimeRef == " + dt.ToString("yyyyMMdd") + "||";

                dt = DateTime.ParseExact(date2, "dd/MM/yyyy", null);
                sql_query = sql_query.Replace("#{DataTo}#", dt.ToString("yyyyMMdd"));
                logParms += dt.ToString("yyyyMMdd") + "::";

                if (Navigation.CurrentSortExpression != null && Navigation.CurrentSortExpression != "")
                {
                    sql_query = sql_query.Replace("#{OrderBy}#", Navigation.CurrentSortExpression + " " + Navigation.CurrentSortDirection);
                    logParms += "Orderby == " + Navigation.CurrentSortExpression + "+" + Navigation.CurrentSortDirection + "::";
                }
                else
                    sql_query = sql_query.Replace("#{OrderBy}#", "XFerStartTime DESC");
                logParms += "Orderby == XFerStartTime DESC::";


                logParms.Remove(logParms.Length - 2, 2);
                string startSQL = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                conn.Open();

                da = new SqlDataAdapter(sql_query, conn);
                cd = new SqlCommandBuilder(da);
                ds = new DataSet();
                da.Fill(ds);
                Navigation.TableSource = ds.Tables[0]; int rowSize = ds.Tables[0].Rows.Count;
                conn.Close();
                string endSQL = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                Navigation.CurrentGridPage = 0;
                BindGridView();
                if (Navigation.TableSource != null && rowSize >= 1000)
                {
                    this.lb_Popup.Text = GetLocalResourceObject("warn.moreres").ToString();
                    this.ModalPopupExtender2.Show();
                }
                log.Info("GetReportsDataSQL_Start_at_" + startSQL + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSQL + "_" + logParms + " 80 - 127.0.0.1 Mozilla/4.0+(compatible;+MSIE+6.0;+MS+Web+Services+Client+Protocol+4.0.30319.1) 200 0 0 " + rowSize);
                Response.AppendToLog("GetReportsDataSQL_Start_at_" + startSQL + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSQL + "_" + logParms);
                log.Debug("Finished--sql_query");
            }

            catch (Exception Ex)
            {
                log.Error("LoadReports_SQL Failed: " + Ex.Message);
                Response.AppendToLog("GetReportsDataSQL_Failed" + Ex.Message + " " + RepName + " " + JRName + " " + JRName2 + " " + JobNumber + " " + date + " " + date2 + " " + recipient + " " + ambiente);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        public void LoadReports_WS(string RepName, string JRName, string JRName2, string JobNumber, string date, string date2, string recipient, string ambiente)
        {
            try
            {
                log.Debug("LoadReports_WS Started");
                log.Info("Method Start: " + DateTime.Now);
                ResetBind();
                //XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface(ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                ////XReportWebIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                //AuthenticSoap authHeader = new AuthenticSoap();
                ////authHeader.userid = this.authenticatedUser;
                //authHeader.userid = Navigation.User.Username;
                ////authHeader.userPwd = "xxxx";
                //XReportWebIface.authenticationValue = authHeader;
                XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);

                XReportWebIface.DocumentData Data = BuildDocumentData(RepName, JRName, JRName2, JobNumber, date, date2, recipient, ambiente);
                XReportWebIface.Timeout = 600000;
                //log.Debug("WS Start: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                string startWS = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                Data = XReportWebIface.getReportsData(Data);
                log.Debug("WS Duration :  LoadReports_WSStart" + startWS + "_End_" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture));
                log.Debug("Fill Start: " + DateTime.Now);
                Navigation.TableSource = Utility.IndexEntriesToTable(Data.IndexEntries);
                log.Debug("Fill End: " + DateTime.Now);
                Navigation.CurrentGridPage = 0;
                BindGridView();
                if (Navigation.TableSource != null && Navigation.TableSource.Rows.Count >= 1000)
                {
                    this.lb_Popup.Text = GetLocalResourceObject("warn.moreres").ToString();
                    this.ModalPopupExtender2.Show();
                }
                log.Info("LoadReports_WS Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadReports_WS Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadReports_WS Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        private void LoadPDF(string JRID, string FromPage)
        {
            try
            {
                log.Info("LoadPDF() Started");
                //XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface();
                //XReportWebIFace.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                XReportWebIFace.Timeout = 600000;
                XReportWebIface.DocumentData Data = BuildDocumentDataPDF(JRID, FromPage);
                Data = XReportWebIFace.getJobReportPdf(Data);
                Navigation.ViewParameters = Data.FileName;
                log.Info("LoadPDF() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadPDF() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadPDF() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        private void LoadNotes()
        {
            try
            {
                log.Info("LoadNotes() Started");
                //XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface();
                //XReportWebIFace.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                Encoding iso_8859 = Encoding.GetEncoding("iso-8859-1");
                
                XReportWebIFace.Timeout = 600000;
                //XReportWebIFace.RequestEncoding = utf_8;
                XReportWebIFace.RequestEncoding = iso_8859;
                string[] args = Navigation.ViewParameters.ToString().Split(';');
                //b_note.CommandArgument = jid + ";" + id + ";" + "G" + ";" + folderName;
                XReportWebIface.DocumentData Data = BuildDocumentDataNotes(args[0], args[1], "", "", "", args[2], args[3], 0, Utility.Operation.GET);

                this.Response.ContentEncoding = iso_8859; 
                Data = XReportWebIFace.GetNotes(Data);
                
                //Encoding
                Navigation.TableNotes = Utility.IndexEntriesToTable(Data.IndexEntries, iso_8859); 
                //Navigation.TableNotes = Utility.IndexEntriesToTable(Data.IndexEntries);
                if (Navigation.TableNotes != null)
                {
                    this.dv_notes.PagerSettings.PageButtonCount = Navigation.TableNotes.Rows.Count;
                    this.dv_notes.PagerSettings.FirstPageText = "1";
                    this.dv_notes.PagerSettings.LastPageText = "" + Navigation.TableNotes.Rows.Count + "";
                    this.dv_notes.PageIndex = Navigation.CurrentNoteIndex;
                    this.dv_notes.ChangeMode(DetailsViewMode.ReadOnly);
                    this.dv_notes.DataSource = Navigation.TableNotes;
                    this.dv_notes.DataBind();
                }
                else
                {
                    dv_notes.ChangeMode(DetailsViewMode.Insert);
                }
                log.Info("LoadNotes() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadNotes() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadNotes() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        private void LoadIntervals(string[] parms, int numPages)
        {
            try
            {
                log.Info("LoadIntervals() Started");

                DataTable dt = Utility.CreateTable(numPages, Int32.Parse(parms[0]), Int32.Parse(parms[1]), parms[2], parms[4], parms[5], parms[6], parms[7], parms[8], parms[9], Navigation.CurrSortIntDirection);
                //this.rangepages_grid.PageIndex = 1;
                Navigation.CurrGridPageInterv = 0;
                Navigation.GridIntervals = dt;
                BindGridIntervals();
               
                log.Info("LoadIntervals() Finished");
            }
            //catch (SoapException SoapEx)
            //{
            //    log.Error("LoadIntervals() Failed: " + SoapEx.Message);
            //    Navigation.Error = SoapEx.Message;
            //    this.lb_Popup.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
            //    this.ModalPopupExtender2.Show();
            //    //throw SoapEx;
            //}
            catch (Exception Ex)
            {
                log.Error("LoadIntervals() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        private void UpdateNote(string version, string UpdUser, string text, Utility.Operation operation)
        {
            try
            {
                log.Info("UpdateNote() Started");
                //XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface();
                //XReportWebIFace.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                XReportWebIFace.Timeout = 600000;
                Encoding iso_8859 = Encoding.GetEncoding("iso-8859-1");
                Encoding utf_8 = Encoding.UTF8;
                XReportWebIFace.RequestEncoding = utf_8;
                string[] args = Navigation.ViewParameters.ToString().Split(';');
                //XReportWebIface.DocumentData Data = BuildDocumentDataNotes(args[0], args[1], version, UpdUser, text, 0, operation);
                //b_note.CommandArgument = jid + ";" + id + ";" + "G" + ";" + folderName;
                XReportWebIface.DocumentData Data = BuildDocumentDataNotes(args[0], args[1], version, UpdUser, text, args[2], args[3], 0, operation);
                switch (operation)
                {
                    case Utility.Operation.INSERT:
                        Data = XReportWebIFace.InsertNote(Data);
                        break;
                    case Utility.Operation.UPDATE:
                        Data = XReportWebIFace.UpdateNote(Data);
                        break;
                    case Utility.Operation.DELETE:
                        Data = XReportWebIFace.DeleteNote(Data);
                        break;
                    default:
                        break;
                }
                log.Info("UpdateNote() Finished");
                if (Data.IndexEntries[0].Columns[0].colname == "ActionMessage" && Data.IndexEntries[0].Columns[0].Value.ToLower() != "success")
                {
                    this.lb_Popup.Text = Data.IndexEntries[0].Columns[0].Value;
                    this.ModalPopupExtender2.Show();
                }
            }
            catch (SoapException SoapEx)
            {
                log.Error("UpdateNote() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("UpdateNote() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        private void UpdateRemark(string jrID, string repID, string text, string type, string folderNameKey)
        {
            try
            {
                log.Info("UpdateRemark() Started");
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                XReportWebIFace.Timeout = 600000;
                string[] args = Navigation.ViewParameters.ToString().Split(';');


                XReportWebIface.DocumentData Data = BuildDocumentDataNotes(jrID, repID, "1", Navigation.User.Username, text, type, folderNameKey, 1, Utility.Operation.UPDATE);
                Data = XReportWebIFace.UpdateNote(Data);
                log.Info("UpdateRemark() Finished");
                if (Data.IndexEntries[0].Columns[0].colname == "ActionMessage" && Data.IndexEntries[0].Columns[0].Value.ToLower() != "success")
                {
                    this.lb_Popup.Text = Data.IndexEntries[0].Columns[0].Value;
                    this.ModalPopupExtender2.Show();
                }
            }
            catch (SoapException SoapEx)
            {
                log.Error("UpdateRemark() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("UpdateRemark() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        private void CheckReport(Utility.Operation operation)
        {
            string logs = "";
            try
            {
                log.Info("CheckReport() Started");
                //XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface();
                //XReportWebIFace.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                XReportWebIFace.Timeout = 600000;
                logs += "Parameters \n";
                string[] args = Navigation.ViewParameters.ToString().Split(';');
                logs += "Par: " + Navigation.ViewParameters.ToString() + "\n";
                if (Navigation.User == null)
                    logs += "Now user is null!!! \n";
                XReportWebIface.DocumentData Data = BuildDocumentDataCheck(args[0], args[1], Navigation.User.Username);
                logs += "Before OP Navigation.User.Username\n";
                switch (operation)
                {
                    case Utility.Operation.CHECK_REPORT:
                        logs += "Updating user to check\n";
                        Data = XReportWebIFace.CheckReport(Data);
                        logs += "Updated user to check\n";
                        break;
                    case Utility.Operation.UPDATE_USER_TO_VIEW:
                        logs += "Updating user to view\n";
                        Data = XReportWebIFace.UpdateLastUserToView(Data);
                        logs += "Updated user to view\n";
                        break;
                    default:
                        break;
                }
                log.Info("CheckReport() Finished");
                if (Data.IndexEntries[0].Columns[0].colname == "ActionMessage" && Data.IndexEntries[0].Columns[0].Value.ToLower() != "success")
                {
                    this.lb_Popup.Text = Data.IndexEntries[0].Columns[0].Value;
                    this.ModalPopupExtender2.Show();
                }
            }
            catch (SoapException SoapEx)
            {
                log.Error("CheckReport() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message + " - " + SoapEx.InnerException + " - " + SoapEx.Actor + " - " + SoapEx.Detail + " - " + SoapEx.StackTrace;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error + "<br/>" + logs;
                this.ModalPopupExtender2.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("CheckReport() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message + " - " + Ex.InnerException + " - " + Ex.StackTrace;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error + "<br/>" + logs;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        private void CountReports(string RepName, string JRName, string JRName2, string JobNumber, string date, string date2, string recipient, string ambiente)
        {
            try
            {
                log.Info("CountReports() Started");
                //XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface();
                //XReportWebIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                XReportWebIface.DocumentData Data = BuildDocumentData(RepName, JRName, JRName2, JobNumber, date, date2, recipient, ambiente);
                XReportWebIFace.Timeout = 600000;
                if (Data.IndexEntries[0].Columns[0].colname == "ActionMessage" && Data.IndexEntries[0].Columns[0].Value.ToLower().StartsWith(Utility.WS_NO_DATA))
                    Navigation.TotReports = -1;
                if (Data.IndexEntries != null && Data.IndexEntries.Length > 1)
                {
                    Navigation.TotReports = int.Parse(Data.IndexEntries[1].Columns[0].Value);
                }
                log.Info("CountReports() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("CountReports() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("CountReports() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        public void LoadUserGroups()
        {
            try
            {
                log.Info("LoadUserGroups() Started");
                //XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface();
                //XReportWebIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column User = new XReportWebIface.column();
                User.colname = "UserName";
                User.Value = Navigation.User.Username;
                Entry.Columns = new XReportWebIface.column[1] { User };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                XReportWebIFace.Timeout = 600000;
                if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.VITTORIA.ToString())
                    Data = null;
                this.ddl_group.DataSource = Utility.IndexEntriesToTable(Data.IndexEntries);
                this.ddl_group.DataTextField = "Group";
                this.ddl_group.DataValueField = "RootNode";
                this.ddl_group.DataBind();
                this.ddl_group.Items.Insert(0, new ListItem("-----", "-1"));
                this.ddl_group.SelectedIndex = -1;
                log.Info("LoadUserGroups() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadUserGroups() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadUserGroups() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        private void ResetBind()
        {
            log.Debug("Init");
            int numPages = 0;
            this.gv_search.DataSource = null;
            this.gv_search.PagerSettings.FirstPageText = "1";
            this.gv_search.PagerSettings.LastPageText = "" + numPages + "";
            this.gv_search.DataBind();
            log.Debug("Bind Grid Finished");
        }

        private void BindGridIntervals()
        {
            log.Debug("Bind Grid Intervals");
            int numPages = 0;
            if (Navigation.GridIntervals != null)
            {
                double pages = (double)Navigation.GridIntervals.Rows.Count / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                numPages = (int)Math.Ceiling(pages);
            }
            this.rangepages_grid.PageSize = int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
            if (Navigation.CurrGridPageInterv != -1)
                this.rangepages_grid.PageIndex = Navigation.CurrGridPageInterv;
            log.Debug("Table PageInt index: " + Navigation.CurrGridPageInterv);
            this.rangepages_grid.DataSource = Navigation.GridIntervals;
            this.rangepages_grid.PagerSettings.FirstPageText = "1";
            this.rangepages_grid.PagerSettings.LastPageText = "" + numPages + "";
            //System.Web.UI.WebControls.SortDirection dirSortInterv;
            //if (Navigation.CurrSortIntDirection == "DESC")
            //    dirSortInterv = System.Web.UI.WebControls.SortDirection.Descending;
            //else
            //    dirSortInterv = System.Web.UI.WebControls.SortDirection.Ascending;
            //this.rangepages_grid.Sort(this.rangepages_grid.Columns[3].SortExpression, dirSortInterv);
            this.rangepages_grid.DataBind();
            this.UpdatePanel3.Update();
            log.Debug("Bind Grid Intervals Finished");

        }

        private void BindGridView()
        {
            log.Debug("Bind Grid");
            int numPages = 0;
            if (Navigation.TableSource != null)
            {
                log.Debug("Table not empty");
                double pages = (double)Navigation.TableSource.Rows.Count / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                numPages = (int)Math.Ceiling(pages);
            }
            this.gv_search.PageSize = int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
            if (Navigation.CurrentGridPage != -1)
                this.gv_search.PageIndex = Navigation.CurrentGridPage;
            log.Debug("Table index: " + Navigation.CurrentGridPage);
            this.gv_search.DataSource = Navigation.TableSource;
            this.gv_search.PagerSettings.FirstPageText = "1";
            this.gv_search.PagerSettings.LastPageText = "" + numPages + "";
            this.gv_search.DataBind();
            this.UpdatePanel6.Update();
            log.Debug("Bind Grid Finished");
        }

        private void CreateLinkButtons()
        {
            string[] indexes = Navigation.ViewParameters.Split(';');
            for (int i = 0; i < indexes.Length - 1; i++)
            {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell = new HtmlTableCell();
                System.Web.UI.WebControls.LinkButton btn = new System.Web.UI.WebControls.LinkButton();
                btn.ID = "btnLink_" + i;
                btn.Text = indexes[i];
                btn.CssClass = "linkButton";
                btn.Click += new EventHandler(btn_Click);
                cell.Controls.Add(btn);
                row.Cells.Add(cell);
                this.TablePopup.Rows.Add(row);
            }
        }

        private void ClearLinkButton()
        {
            this.hiddenIndex.Value = "0";
            int[] rowInd = new int[this.TablePopup.Rows.Count];
            for (int i = 2; i < this.TablePopup.Rows.Count - 1; i++)
                rowInd[i] = i;
            for (int i = this.TablePopup.Rows.Count - 1; i > 2; i--)
            {
                HtmlTableRow row = this.TablePopup.Rows[i];
                this.TablePopup.Rows.Remove(row);
                row.Controls.Clear();
                row.Dispose();
            }
        }

        private void AddSortImage(GridViewRow headerRow, int column, string sortDirection)
        {
            if (column == -1)
                return;
            Image sortImage = new Image();
            if (SORT_ASCENDING == sortDirection)
            {
                sortImage.ImageUrl = "~/Images/sort_asc.gif";
                sortImage.AlternateText = "Ascending order";
            }
            else
            {
                sortImage.ImageUrl = "~/Images/sort_desc.gif";
                sortImage.AlternateText = "Descending order";
            }
            headerRow.Cells[column].Controls.Add(sortImage);
        }

        private int GetSortColumnIndex(GridView gridView, String sortExpression)
        {
            if (gridView == null)
                return -1;
            foreach (DataControlField field in gridView.Columns)
            {
                if (field.SortExpression == sortExpression)
                {
                    return gridView.Columns.IndexOf(field);
                }
            }
            return -1;
        }

        protected void ld_multidateOnTextChanged(object sender, EventArgs e)
        {
           
        }
        

        #endregion

        #region Events
        protected void buttonSearch_Click(object sender, EventArgs e)
        {
            Navigation.CurrentSortExpression = "Recipient";
            Navigation.CurrentSortDirection = SORT_ASCENDING;
            string tb_name = this.tb_name.Text; string tb_jobname = this.tb_jobname.Text; 
            string data1 = ""; string data2 = ""; 
            this.UpdatePanel4.Update();
            if (!this.ld_multidate.Text.Equals("0"))
            {
                DateTime today = DateTime.Today;
                data2 = today.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture);
                int numdays = Int32.Parse(this.ld_multidate.Text);
                data1 = DateTime.Today.AddDays(-numdays).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            else {
                data1 = this.tb_data1.Text;
                data2 = this.tb_data2.Text;
            }
            
            if(this.generalTree!= null)
            {
                Navigation.User.AltProfilo = (new string[] {this.generalTree.SelectedNode.Value});
                //this.sp_lb_Tree.Text = GetLocalResourceObject("tree").ToString() + ": " + "START";
                this.UpdatePanel5.Update();
                if (!this.CustomValidator1.IsValid)
                {
                    Navigation.Error = CustomValidator1.ErrorMessage;
                    this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                    this.ModalPopupExtender2.Show();
                    return;
                }
                if (!this.tb_advisor.Text.Equals(""))
                {
                    tb_name = this.tb_advisor.Text;
                    tb_jobname = "ALL%" + tb_jobname; //stored procedure recognize special PATTERN ALL% and manage to search in another way
                }
            }
            //this.tb_name.Text.Trim();
            //this.tb_jobnumber.Text.Trim();
            //this.tb_jobname.Text.Trim();
            //this.tb_prof.Text.Trim();
            Response.AppendToLog("buttonSearch_Click--date--" + this.tb_data1.Text +"-"+ this.tb_data2.Text);
            log.Info("buttonSearch_Click--date--" + this.tb_data1.Text + "-" + this.tb_data2.Text);
            LoadReports(tb_name, tb_jobname, this.tb_jobname2.Text, this.tb_jobnumber.Text, data1, data2, this.tb_hour1.Text, this.tb_hour2.Text, this.tb_prof.Text, "", this.tb_filterRemark.Text, this.CheckBoxLastReport.Checked);
        }

        protected void gv_search_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Navigation.CurrentGridPage = e.NewPageIndex;
            BindGridView();
        }

        protected void gv_search_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRowView view = (DataRowView)e.Row.DataItem;
                    string jid = view["JobReport_ID"].ToString();
                    string id = view["ReportId"].ToString();
                    string name = view["JobName"].ToString();
                    string pages = view["ListOfPages"].ToString();
                    string fromPage = view["FromPage"].ToString();
                    string typeMap = view["ExistTypeMap"].ToString();
                    string userTimeRef = view["UserTimeRef"].ToString();
                    string xferStartTime = view["XferStartTime"].ToString();
                    string reportName = view["ReportName"].ToString();
                    string totPages = view["TotPages"].ToString();
                    string remark = view["Remark"].ToString();
                    string JobNum = view["JobNumber"].ToString();
                    string folderName = view["FolderName"].ToString();
                    string folderNameKey = folderName.Split('\\').Last();


                    
                    string isReportFormat = view["isRightRF"].ToString();

                    if (ConfigurationManager.AppSettings["ManageRemark"] == null || ConfigurationManager.AppSettings["ManageRemark"].Equals("0"))
                        hide_column_in_gv_search("Remark");

                    //int totPages = Int32.Parse(view["TotPages"].ToString());
                    bool checkFlag = false; bool checkStatus = false; bool hasIndex = false; bool rightReportFormat = false;
                    if (view["CheckFlag"] != null && view["CheckFlag"].ToString() != "")
                        checkFlag = bool.Parse(view["CheckFlag"].ToString());//Utility.ConvertStringToBool(view["CheckFlag"].ToString());
                    if (view["CheckStatus"] != null && view["CheckStatus"].ToString() != "")
                        checkStatus = bool.Parse(view["CheckStatus"].ToString()); //Utility.ConvertStringToBool(view["CheckStatus"].ToString());
                    if (view["HasIndexes"] != null && view["HasIndexes"].ToString() != "")
                        hasIndex = bool.Parse(view["HasIndexes"].ToString());
                    if (null != view["isRightRF"] &&  ""!= view["isRightRF"].ToString())
                        rightReportFormat = Utility.ConvertStringToBool(view["isRightRF"].ToString());
                    string lastUserToCheck = view["LastUsedToCheck"].ToString();
                    ImageButton b_pdf = (ImageButton)e.Row.FindControl("b_pdf");
                    ImageButton b_txt = (ImageButton)e.Row.FindControl("b_txt");
                    if (!rightReportFormat)
                    {
                        b_pdf.ToolTip = "View PDF - " + jid;
                        b_pdf.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + reportName + ";" + userTimeRef + ";" + xferStartTime + ";" + name + ";" + JobNum + ";" + folderName + ";" + totPages;
                        b_txt.ToolTip = "View TXT - " + jid;
                        //b_txt.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage;
                        b_txt.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + reportName + ";" + userTimeRef + ";" + xferStartTime + ";" + name + ";" + JobNum + ";" + folderName + ";" + totPages;
                        b_pdf.Visible = true;
                        b_pdf.Enabled = true;
                        b_txt.Visible = true;
                        b_txt.Enabled = true;
                    }
                    else {
                        b_pdf.Visible = false;
                        b_pdf.Enabled = false;
                        b_txt.Visible = false;
                        b_txt.Enabled = false;
                    }
                    
                    ImageButton b_note = (ImageButton)e.Row.FindControl("b_note");
                    b_note.CommandArgument = jid + ";" + id + ";" + "G" + ";" + folderNameKey;
                    ImageButton b_rem = (ImageButton)e.Row.FindControl("b_rem");
                    b_rem.CommandArgument = jid + ";" + id + ";" + "R" + ";" + folderNameKey + ";" + remark+";"+e.Row.RowIndex;
                    ImageButton b_check = (ImageButton)e.Row.FindControl("b_check");
                    ImageButton b_xls = (ImageButton)e.Row.FindControl("b_xls");
                    b_xls.ToolTip = "View XLS"+(typeMap == "3" ? "X" : "")+" - " + jid;
                    //b_xls.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage;
                    b_xls.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + reportName + ";" + userTimeRef + ";" + xferStartTime + ";" + name + ";" + JobNum + ";" + folderName + ";" + totPages;

                    ImageButton b_send_email = (ImageButton)e.Row.FindControl("b_send_email");
                    b_send_email.ToolTip = GetLocalResourceObject("messToolEm").ToString() + jid;
                    b_send_email.CommandArgument = b_send_email.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + totPages + ";" + "PDF" + ";" + reportName;
                    ImageButton b_save_file = (ImageButton)e.Row.FindControl("b_save_file");
                    b_save_file.ToolTip = GetLocalResourceObject("messToolChoice").ToString() + jid;
                    //b_save_file.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + reportName + ";" + userTimeRef + ";" + xferStartTime + ";" + name + ";" + JobNum + ";" + folderName + ";" + totPages;
                    //save file will have one more arguments 
                    if (rightReportFormat)
                    {
                        //only excel
                        b_save_file.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + reportName + ";" + userTimeRef + ";" + xferStartTime + ";" + name + ";" + JobNum + ";" + folderName + ";1;"+ totPages;
                    }
                    else {
                        b_save_file.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + reportName + ";" + userTimeRef + ";" + xferStartTime + ";" + name + ";" + JobNum + ";" + folderName + ";0;" + totPages;
                    }
                    if (typeMap == "2" || typeMap == "3")
                    {
                        b_xls.Visible = true;
                        b_xls.Enabled = true;
                        show_column_in_gv_search("XLS");
                    }
                    
                    else
                    {
                        b_xls.Visible = false;
                        b_xls.Enabled = false;
                    }
                    if (checkFlag && checkStatus)
                    {
                        b_check.ImageUrl = "~/Images/checked.png";
                        b_check.CommandName = "Checked";
                        b_check.ToolTip = "Checked by " + lastUserToCheck;
                        b_check.Enabled = false;
                    }
                    else if (checkFlag && !checkStatus)
                    {
                        show_column_in_gv_search("CHK");
                        b_check.ImageUrl = "~/Images/toCheck.png";
                        b_check.CommandName = "Check";
                        b_check.ToolTip = "Check Report";
                        b_check.CommandArgument = jid + ";" + id;
                        b_check.Enabled = true;
                    }
                    else if (!checkFlag)
                    {
                        b_check.Visible = false;
                        b_check.Enabled = false;
                    }
                    ImageButton b_index = (ImageButton)e.Row.FindControl("b_index");
                    if (!hasIndex)
                    {
                        b_index.ImageUrl = "";
                        b_index.ToolTip = "";
                        b_index.Enabled = false;
                        b_index.Visible = false;
                    }
                    else
                    {
                        show_column_in_gv_search("IDX");
                        string recipient = view["Recipient"].ToString();
                        //string folderName = view["FolderName"].ToString();
                        string report_name = view["ReportName"].ToString();
                        //b_index.CommandArgument = jid + ";" + recipient + ";" + folderName + ";" + Navigation.User.Username;
                        b_index.CommandArgument = jid + ";" + recipient + ";" + folderName + ";" + Navigation.User.Username + ";" + report_name;
                        b_index.ToolTip = "View INDEX - " + jid;
                    }
                }
                else if (e.Row.RowType == DataControlRowType.Header)
                {

                    hide_column_in_gv_search("IDX");
                    hide_column_in_gv_search("XLS");
                    hide_column_in_gv_search("CHK");

                    int column = GetSortColumnIndex(this.gv_search, Navigation.CurrentSortExpression);
                    if (column != -1)
                        AddSortImage(e.Row, column, Navigation.CurrentSortDirection.ToString());
                    //hide headers PDF, XLS....
                    for (int i = 0; i < e.Row.Cells.Count; i++)
                    {
                        if ((e.Row.Cells[i].Text.Equals("PDF"))
                            || (e.Row.Cells[i].Text.Equals("TXT"))
                            || (e.Row.Cells[i].Text.Equals("IDX"))
                            || (e.Row.Cells[i].Text.Equals("NOTE"))
                            || (e.Row.Cells[i].Text.Equals("CHK"))
                            || (e.Row.Cells[i].Text.Equals("XLS"))
                            )
                        {
                            e.Row.Cells[i].Text = "";
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                log.Error("Failed to bind search grid: " + Ex.Message);
                Navigation.Error = Ex.Message;
                Context.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/Error.aspx?disableReload=Y&enableClose=Y");

            }
        }

        protected void hide_column_in_gv_search(String colname, DataControlFieldCollection columns)
        {
            (//(DataControlField)gv_search.Columns
                (DataControlField)columns
                    .Cast<DataControlField>()
                    .Where(fld => fld.HeaderText == colname)
                    .SingleOrDefault()).Visible = false;
        }

        protected void hide_column_in_gv_search(String colname)
        {
            ((DataControlField)gv_search.Columns
                    .Cast<DataControlField>()
                    .Where(fld => fld.HeaderText == colname)
                    .SingleOrDefault()).Visible = false;
        }
        protected void show_column_in_gv_search(String colname)
        {
            ((DataControlField)gv_search.Columns
                    .Cast<DataControlField>()
                    .Where(fld => fld.HeaderText == colname)
                    .SingleOrDefault()).Visible = true;
        }

        //   protected void gv_search_RowCreated(object sender, GridViewRowEventArgs e)
        //   {
        //       ((DataControlField)gv_search.Columns
        //               .Cast<DataControlField>()
        //               .Where(fld => fld.HeaderText == "PDF")
        //               .SingleOrDefault()).Visible = false;
        //   }

        protected void gv_search_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ViewFile")
                {
                    string allArgs = e.CommandArgument.ToString();
                    string[] allArgAr = allArgs.Split(';'); int numPages = Int32.Parse(allArgAr.Last());
                    if (numPages > 2500)
                    {
                        this.LoadIntervals(allArgAr, numPages);
                        this.ModalPopupExtender4.Show();
                    }
                    else
                    {
                        string encryptedvalue = createKey(allArgAr, "PDF");
                        ((StartPage)this.Page).OpenViewFile(encryptedvalue);
                    }

                }
                else if (e.CommandName == "ViewFileTxt")
                {
                    string allArgs = e.CommandArgument.ToString();
                    string[] allArgAr = allArgs.Split(';'); int numPages = Int32.Parse(allArgAr.Last());
                    if (numPages > 2500)
                    {
                        this.LoadIntervals(allArgAr, numPages);
                        this.ModalPopupExtender4.Show();
                    }
                    else
                    {
                        List<String> args = allArgAr.ToList();
                        Navigation.ViewParameters3 = String.Join("", ((List<String>)args.GetRange(4, 1)).ToArray());
                        //args.RemoveRange(4, 4);
                        //args.RemoveRange(4, 6);
                        args.RemoveRange(4, 7);
                        Navigation.ViewParameters1 = String.Join(";", args.ToArray()) + ";" + numPages;
                        //Navigation.ViewParameters = String.Join(";", args.ToArray()) + ";" + numPages; //e.CommandArgument.ToString();
                        Navigation.ViewParameters2 = "TXT";
                        Navigation.CurrentCulture =  CultureInfo.CurrentUICulture.Name;
                        //Navigation.CurrentCulture =  ((StartPage)this.Page);
                        Navigation.LogParameters = allArgAr;
                        ((StartPage)this.Page).OpenViewTxtFile();
                        
                    }
                }
                //else if (e.CommandName == "ViewCommTxt")
                //{
                //    string allArgs = e.CommandArgument.ToString();
                //    string[] allArgAr = allArgs.Split(';'); int numPages = Int32.Parse(allArgAr.Last());
                //    if (numPages > 2500)
                //    {
                //        this.LoadIntervals(allArgAr, numPages);
                //        this.ModalPopupExtender4.Show();
                //    }
                //    else
                //    {
                //        List<String> args = allArgAr.ToList();
                //        Navigation.ViewParameters3 = String.Join("", ((List<String>)args.GetRange(4, 1)).ToArray());
                //        args.RemoveRange(4, 4);
                //        Navigation.ViewParameters = String.Join(";", args.ToArray()) + ";" + numPages; //e.CommandArgument.ToString();
                //        Navigation.ViewParameters2 = "TXT";
                //        Navigation.CurrentCulture = CultureInfo.CurrentUICulture.Name;
                //        //Navigation.CurrentCulture =  ((StartPage)this.Page);
                //        //((StartPage)this.Page).OpenViewFile();
                //        ((StartPage)this.Page).OpenViewTxtFile();
                //    }
                //}
                else if (e.CommandName == "ViewFileXls")
                {
                    ////Navigation.ViewParameters = e.CommandArgument.ToString();
                    string allArgs = e.CommandArgument.ToString();
                    string[] allArgAr = allArgs.Split(';'); 
                    //int numPages = Int32.Parse(allArgAr.Last());
                    //List<String> args = allArgAr.ToList();
                    ////args.RemoveRange(4, 4);
                    ////args.RemoveRange(4, 6);
                    //args.RemoveRange(4, 7);
                    //Navigation.ViewParameters = String.Join(";", args.ToArray()) + ";" + numPages; //e.CommandArgument.ToString();
                    //Navigation.ViewParameters2 = "XLS";
                    //Navigation.ViewParameters3 = null;
                    //Navigation.LogParameters = allArgAr;
                    //((StartPage)this.Page).OpenViewFile();
                    string encryptedvalue = createKey(allArgAr, "XLS");
                    ((StartPage)this.Page).OpenViewFile(encryptedvalue);   
                }
                else if (e.CommandName == "ViewFileXlsx")
                {
                    string allArgs = e.CommandArgument.ToString();
                    string[] allArgAr = allArgs.Split(';');
                    string encryptedvalue = createKey(allArgAr, "XLSX");
                    ((StartPage)this.Page).OpenViewFile(encryptedvalue);
                }
                else if (e.CommandName == "Index")
                {
                    Navigation.ViewParameters = e.CommandArgument.ToString();
                    if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.GENERALI.ToString())
                        this.myFrame.Attributes.Add("src", "Indexes.aspx");
                    else if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.UNICREDIT.ToString())
                        this.myFrame.Attributes.Add("src", "Indexes4Uni.aspx");
                    this.programmaticModalPopup.Show();
                }
                else if (e.CommandName == "Note")
                {
                    Navigation.ViewParameters = e.CommandArgument.ToString();
                    Navigation.ViewParameters2 = "NOTE";
                    this.LoadNotes();
                    this.UpdatePanel2.Update();
                    this.notes_ext.Show();
                }
                else if (e.CommandName == "Check")
                {
                    Navigation.ViewParameters2 = "CHK";
                    Navigation.ViewParameters = e.CommandArgument.ToString();
                    this.ModalPopupExtender3.Show();
                }
                else if (e.CommandName == "UpdateRemark")
                {
                    this.tb_Remark.Text = e.CommandArgument.ToString().Split(';')[2];
                    this.lh_remark.Text = e.CommandArgument.ToString();
                    this.mpe_remark.Show();
                }
                else if (e.CommandName == "SaveFile" || e.CommandName == "SaveXFile")
                {
                    string allArgs = e.CommandArgument.ToString();
                    string[] allArgAr = allArgs.Split(';'); int numPages = Int32.Parse(allArgAr.Last());
                    if (numPages > 2500)
                    {
                        this.LoadIntervals(allArgAr, numPages);
                        this.ModalPopupExtender4.Show();
                    }
                    else
                    {
                        //this. = e.CommandArgument.ToString().Split(';')[2];
                        List < String > args = allArgAr.ToList();
                        this.tbx_download.Text = String.Join("", ((List<String>)args.GetRange(4, 1)).ToArray()).Replace(' ', '_');
                        //if("1".Equals(String.Join("", ((List<String>)args.GetRange(10, 1)).ToArray()))){
                        //    this.r_downtype3.Checked = true;
                        //    this.r_downtype2.Visible = false;
                        //    this.r_downtype1.Visible = false;
                        //}else{
                        //    this.r_downtype3.Visible = true;
                        //    this.r_downtype2.Visible = true;
                        //    this.r_downtype1.Visible = true;
                        //}
                        if ("1".Equals(String.Join("", ((List<String>)args.GetRange(10, 1)).ToArray())))
                        {
                            this.r_downtype4.Visible = false;
                            this.r_downtype3.Checked = true;
                            this.r_downtype2.Visible = false;
                            this.r_downtype1.Visible = false;
                        }
                        else
                        {
                            object cosa = e.CommandSource;
                            if (e.CommandName == "SaveXFile")
                            {
                                this.r_downtype4.Visible = true;
                                this.r_downtype3.Visible = false;
                            }
                            else
                            {
                                this.r_downtype4.Visible = false;
                                this.r_downtype3.Visible = true;
                            }

                            //this.r_downtype3.Visible = true;
                            this.r_downtype2.Visible = true;
                            this.r_downtype1.Visible = true;
                        }
                        this.lh_download.Text = e.CommandArgument.ToString();
                        this.UpdatePanel7.Update();
                        Navigation.LogParameters = allArgAr;
                        this.mpe_downloadchoice.Show();
                    }
                }
                else if (e.CommandName == "SendEmail")
                {
                    //e.CommandArgument.ToString().Split(';')[2];
                    //Sign tne parameters
                    //this.lh_remark.Text = e.CommandArgument.ToString();

                    //<--http://ufr-uj.collaudo.usinet.it/XA-PGE-PF/private/pgeStartApp.do?applicationToStart=token&pgeMenuId=start&pgeuidname=user&tokenString=token&pgeAppCode=X1N&writeInfo=y&backUrl=--><!--http://localhost:53190/ViewReport.aspx&timeout=180000&twr=ufr-uj.collaudo.usinet.it&effective_twr=[var_tower]&JobReportID=189093&ReportID=2-->
                    string allArgs = e.CommandArgument.ToString();
                    string[] allArgAr = allArgs.Split(';');
                    //b_mail.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + totPages + ";" + "PDF" + ";" + reportName;
                    if (allArgAr.Length < 6)
                    {
                        throw (new Exception("few parameters"));
                    }
                    //SendMail(allArgAr);
                    SendMail(allArgAr[0], allArgAr[1], allArgAr[2], allArgAr[3], allArgAr[4], allArgAr[5], allArgAr[6]);
                }
            }
            catch (Exception Ex)
            {
                log.Error("Failed to call rowcommand on grid : " + Ex.Message);
                Navigation.Error = Ex.Message;
                Context.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/Error.aspx?disableReload=Y&enableClose=Y");

            }
        }

        private string createKey(string[] allArgAr, string format)
        { 
            List<String> args = allArgAr.ToList();
            int numPages = Int32.Parse(allArgAr.Last());
            //args.RemoveRange(4, 4); String.Join(";", args.ToArray());
            //args.RemoveRange(4, 6); String.Join(";", args.ToArray());
            args.RemoveRange(4, 7); String.Join(";", args.ToArray());
            //krypt all parameters and return krypted value 
            string encr_equal = "#&##&#"; string encr_and = "&#&&#&"; string encr_log = "&####&";
            //Navigation.ViewParameters = String.Join(";", args.ToArray()); //e.CommandArgument.ToString();
            //Navigation.ViewParameters2 = "PDF";
            //Navigation.ViewParameters3 = null;
            //Navigation.LogParameters = allArgAr;
            string parameters = "viep1" + encr_equal + String.Join(";", args.ToArray()) + ";" + numPages + encr_and +
                "viep2" + encr_equal + format + encr_and +
                "username" + encr_equal + Navigation.User.Username + encr_and +
                "codauthor" + encr_equal + Navigation.User.CodAuthor + encr_and +
                "session" + encr_equal + Session.SessionID + encr_and +
                "viep3" + encr_equal + "null" + encr_and +
                "viewlog" + encr_equal + String.Join(encr_log, allArgAr);
            string encryptedvalue = Signature.CreateToken(parameters, EXP_TIME_SECONDS);

            //this.CheckReport(Utility.Operation.UPDATE_USER_TO_VIEW);
            //((StartPage)this.Page).OpenViewFile();
            //pass krypted value
            return encryptedvalue;
        }
        protected void btnsavesearch_Click(object sender, EventArgs e)
        {

            if (this.tbx_savesearch.Text != "")
            {

                try
                {
                    this.updateUserList(this.tbx_savesearch.Text, this.tb_jobname.Text, this.tb_name.Text, this.tb_jobnumber.Text, this.tb_advisor.Text, this.tb_prof.Text, this.tb_filterRemark.Text, this.generalTree.SelectedNode.Text);
                    this.mpe_savesearch.Hide();
                    this.RetrieveSavedSearch();
                    this.UpdatePanel1.Update();
                }
                catch (Exception Ex)
                {
                    log.Error("Failed to save preferred searchs: " + Ex.Message);
                    Navigation.Error = Ex.Message;
                    this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                    this.mpe_savesearch.Hide();
                    this.UpdatePanel9.Update();
                    this.ModalPopupExtender2.Show();
                    //throw Ex;
                }
            }
            else
            {
                this.lb_Popup.Text = "WARNING: <br/>"+GetLocalResourceObject("warn.notspecified").ToString();
                this.mpe_savesearch.Hide();
                this.UpdatePanel9.Update();
                this.ModalPopupExtender2.Show();

            }
        }

        protected void b_deleteSearch_Click(object sender, EventArgs e) {
            if (!"".Equals(this.CSavedSearch.SelectedValue))
            {
                this.l_delsaved.Text = GetLocalResourceObject("delmessfilt").ToString() + "<br/> <br/>" + this.CSavedSearch.SelectedValue;
                //this.l_delsaved.Text = "DELETING SAVED SEARCH <br/> <br/>" + this.CSavedSearch.SelectedValue;
                this.b_confdel.Visible = true;
                this.UpdatePanel10.Update();
                this.mpe_delsearch.Show();
            }
            else
            {
                this.l_delsaved.Text = GetLocalResourceObject("warn.warnnodel").ToString() + "<br/> <br/>" + this.CSavedSearch.SelectedValue;
                //this.l_delsaved.Text = "NO SELECTED VALUE TO DELETE <br/> <br/>" + this.CSavedSearch.SelectedValue;
                this.b_confdel.Visible = false;
                this.UpdatePanel10.Update();
                this.mpe_delsearch.Show();
            }
            
        }

        protected void buttonFilter_Click(object sender, EventArgs e)
        {
            this.tbx_savesearch.Text = "";
            this.UpdatePanel8.Update();
            this.mpe_savesearch.Show();
        }

        protected void btndownload_Click(object sender, EventArgs e)
        {
            string encr_equal = "#&##&#"; string encr_and = "&#&&#&"; string encr_log = "&####&";
            string filename = this.tbx_download.Text.ToString();
            string allArgs = this.lh_download.Text;
            string[] allArgAr = allArgs.Split(';'); int numPages = Int32.Parse(allArgAr.Last());
            List<String> args = allArgAr.ToList();
            string parameters = "";
            //call to this function can come from two point
            //one with all parameters coming from principals search page contaning also reportname etc..
            //second from range_pages management with only pages parameters
            if (args.Count > 5)
            {
                //args.RemoveRange(4, 4); 
                //args.RemoveRange(4, 6); 
                args.RemoveRange(4, 7); 
            }
            if(this.r_downtype1.Checked)
            //if (filename.EndsWith("pdf", StringComparison.InvariantCultureIgnoreCase))
            {
                //Navigation.ViewParameters = String.Join(";", args.ToArray()); 
                //Navigation.ViewParameters2 = "PDF";
                parameters =  "viep1" + encr_equal + String.Join(";", args.ToArray()) + encr_and 
                            + "viep2" + encr_equal + "PDF" + encr_and 
                            + "username" + encr_equal + Navigation.User.Username + encr_and 
                            + "codauthor" + encr_equal + Navigation.User.CodAuthor + encr_and 
                            + "viep3" + encr_equal + filename + encr_and 
                            + "session" + encr_equal + Session.SessionID + encr_and 
                            + "viewlog" + encr_equal + String.Join(encr_log, allArgAr);
            }
            else if (this.r_downtype2.Checked)
            //else if (filename.EndsWith("txt", StringComparison.InvariantCultureIgnoreCase))
            {
                //Navigation.ViewParameters = String.Join(";", args.ToArray()) + ";" + numPages; //e.CommandArgument.ToString();
                //Navigation.ViewParameters2 = "TXT";
                parameters =  "viep1" + encr_equal + String.Join(";", args.ToArray()) + ";" + numPages + encr_and 
                            + "viep2" + encr_equal + "TXT" + encr_and
                            + "username" + encr_equal + Navigation.User.Username + encr_and
                            + "codauthor" + encr_equal + Navigation.User.CodAuthor + encr_and 
                            + "viep3" + encr_equal + filename + encr_and 
                            + "session" + encr_equal + Session.SessionID + encr_and 
                            + "viewlog" + encr_equal + String.Join(encr_log, allArgAr);
                
            }
            else if (this.r_downtype3.Checked || this.r_downtype4.Checked)
            {
                //Navigation.ViewParameters = String.Join(";", args.ToArray()) + ";" + numPages;
                //Navigation.ViewParameters2 = "XLS";
                parameters = "viep1" + encr_equal + String.Join(";", args.ToArray()) + ";" + numPages + encr_and +
                    "username" + encr_equal + Navigation.User.Username + encr_and +
                    "codauthor" + encr_equal + Navigation.User.CodAuthor + encr_and + 
                    "session" + encr_equal + Session.SessionID + encr_and +
                    "viep2" + encr_equal + (this.r_downtype4.Checked ? "XLSX" : "XLS") + encr_and + 
                    "viep3" + encr_equal + filename + encr_and + 
                    "viewlog" + encr_equal + String.Join(encr_log, allArgAr);
            }
            //Navigation.ViewParameters3 = filename;
            //((StartPage)this.Page).OpenViewFile();
            string encryptedvalue = Signature.CreateToken(parameters, EXP_TIME_SECONDS);
            //pass krypted value
            ((StartPage)this.Page).OpenViewFile(encryptedvalue);
            this.mpe_downloadchoice.Hide();
        }

        protected void btn_Click(object sender, EventArgs e)
        {

        }

        protected void siPopup_Click(object sender, EventArgs e)
        {
            this.ModalPopupExtender2.Hide();
        }

        protected void b_yes_Click(object sender, EventArgs e)
        {
            this.CheckReport(Utility.Operation.CHECK_REPORT);
            this.ModalPopupExtender3.Hide();
            LoadReports(this.tb_name.Text, this.tb_jobname.Text, this.tb_jobname2.Text, this.tb_jobnumber.Text, this.tb_data1.Text, this.tb_data2.Text, this.tb_hour1.Text, this.tb_hour2.Text, this.tb_prof.Text, "", this.tb_filterRemark.Text, this.CheckBoxLastReport.Checked);
        }

        protected void b_close_Click(object sender, EventArgs e)
        {
            this.myFrame.Attributes.Remove("src");
            this.programmaticModalPopup.Hide();
        }

        protected void b_close2_Click(object sender, EventArgs e)
        {
            ClearLinkButton();
            this.cmdExt.Hide();
        }

        protected void b_close3_Click(object sender, EventArgs e)
        {
            Navigation.CurrentNoteIndex = 0;
            Navigation.TableNotes = null;
            this.dv_notes.DataSource = Navigation.TableNotes;
            this.dv_notes.DataBind();
            this.notes_ext.Hide();
        }

        protected void b_close4_Click(object sender, EventArgs e)
        {
            Navigation.CurrGridPageInterv = 0;
            Navigation.TableNotes = null;
            this.rangepages_grid.DataSource = null;
            this.rangepages_grid.DataBind();
            this.ModalPopupExtender4.Hide();
        }

        protected void b_downloadchoiceClose_Click(object sender, EventArgs e) {
            this.mpe_downloadchoice.Hide();
        }

        protected void b_saveSearchClose_Click(object sender, EventArgs e)
        {
            //this.UpdatePanel8.Update();
            this.mpe_savesearch.Hide();
        }

        protected void conf_del_Click(object sender, EventArgs e)
        {
            if ("" != this.CSavedSearch.SelectedValue)
            {
                try
                {
                    deleteSavedSearch(this.CSavedSearch.SelectedValue);
                }
                catch (Exception ex)
                {
                    this.lb_Popup.Text = "WARNING: <br/> Saved Search parameters not deleted <br/>" + ex.Message;
                    this.mpe_delsearch.Hide();
                    this.UpdatePanel9.Update();
                    this.ModalPopupExtender2.Show();
                }
                this.RetrieveSavedSearch();
                this.UpdatePanel1.Update();
            }
        }

        protected void b_delsaved_Click(object sender, EventArgs e)
        {
            this.mpe_delsearch.Hide();
        }
        
        protected void dv_notes_DataBound(object sender, EventArgs e)
        {
            DetailsView detView = (DetailsView)sender;
            switch (dv_notes.CurrentMode)
            {
                case DetailsViewMode.Edit:
                    this.dv_notes.Fields[7].Visible = false;
                    this.dv_notes.Fields[8].Visible = false;
                    break;
                case DetailsViewMode.Insert:
                    this.dv_notes.Fields[6].Visible = false;
                    this.dv_notes.Fields[8].Visible = false;
                    ((Label)dv_notes.FindControl("tb_JRID2")).Text = Navigation.ViewParameters.Split(';')[0];
                    ((Label)dv_notes.FindControl("tb_repID2")).Text = Navigation.ViewParameters.Split(';')[1];
                    break;
                case DetailsViewMode.ReadOnly:
                    this.dv_notes.Fields[6].Visible = true;
                    this.dv_notes.Fields[7].Visible = true;
                    this.dv_notes.Fields[8].Visible = true;
                    break;
                default:
                    break;
            }
        }

        protected void dv_notes_PageIndexChanging(object sender, DetailsViewPageEventArgs e)
        {
            this.dv_notes.PageIndex = e.NewPageIndex;
            Navigation.CurrentNoteIndex = e.NewPageIndex;
            this.dv_notes.DataSource = Navigation.TableNotes;
            this.dv_notes.DataBind();
        }

        protected void dv_notes_ModeChanging(object sender, DetailsViewModeEventArgs e)
        {
            switch (e.NewMode)
            {
                case DetailsViewMode.Edit:
                    dv_notes.ChangeMode(DetailsViewMode.Edit);
                    this.dv_notes.DataSource = Navigation.TableNotes;
                    this.dv_notes.DataBind();
                    break;
                case DetailsViewMode.ReadOnly:
                    dv_notes.ChangeMode(DetailsViewMode.ReadOnly);
                    this.dv_notes.DataSource = Navigation.TableNotes;
                    this.dv_notes.DataBind();
                    break;
                case DetailsViewMode.Insert:
                    dv_notes.ChangeMode(DetailsViewMode.Insert);
                    break;
            }
        }

        protected void dv_notes_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            try
            {
                this.UpdateNote("", Navigation.User.Username, ((TextBox)dv_notes.FindControl("tb_text2")).Text, Utility.Operation.INSERT);
                dv_notes.ChangeMode(DetailsViewMode.ReadOnly);
                if (Navigation.TableNotes != null)
                    Navigation.CurrentNoteIndex = Navigation.TableNotes.Rows.Count;
                else
                    Navigation.CurrentNoteIndex = 1;
                this.LoadNotes();
            }
            catch (Exception Ex)
            {
                log.Error("Failed to insert notes: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        protected void dv_notes_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            try
            {
                this.UpdateNote(((Label)dv_notes.FindControl("tb_ver2")).Text, Navigation.User.Username, ((TextBox)dv_notes.FindControl("tb_text2")).Text, Utility.Operation.UPDATE);
                dv_notes.ChangeMode(DetailsViewMode.ReadOnly);
                this.LoadNotes();
            }
            catch (Exception Ex)
            {
                log.Error("Failed to update notes: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        protected void dv_notes_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
        {
            this.ConfDel_ext.Show();
        }

        protected void currPopup_Click(object sender, EventArgs e)
        {
            try
            {
                this.UpdateNote(((Label)dv_notes.FindControl("tb_ver1")).Text, Navigation.User.Username, "", Utility.Operation.DELETE);
                dv_notes.ChangeMode(DetailsViewMode.ReadOnly);
                Navigation.CurrentNoteIndex = 0;
                this.ConfDel_ext.Hide();
                this.LoadNotes();
                if (Navigation.TableNotes == null)
                    this.notes_ext.Hide();
                else
                    this.notes_ext.Show();
            }
            catch (Exception Ex)
            {
                log.Error("Failed to update notes: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        protected void allPopup_Click(object sender, EventArgs e)
        {
            try
            {
                this.UpdateNote("-1", Navigation.User.Username, "", Utility.Operation.DELETE);
                dv_notes.ChangeMode(DetailsViewMode.ReadOnly);
                Navigation.CurrentNoteIndex = 0;
                Navigation.TableNotes = null;
                this.dv_notes.DataSource = Navigation.TableNotes;
                this.dv_notes.DataBind();
                this.ConfDel_ext.Hide();
                this.notes_ext.Hide();
            }
            catch (Exception Ex)
            {
                log.Error("Failed to update notes: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
                //throw Ex;
            }
        }

        protected void b_saveRemark_Click(object sender, EventArgs e)
        {
            //b_note.CommandArgument = jid + ";" + id + ";" + "G" + ";" + folderNameKey;
            this.UpdateRemark(this.lh_remark.Text.Split(';')[0], this.lh_remark.Text.Split(';')[1], this.tb_Remark.Text, this.lh_remark.Text.Split(';')[2], this.lh_remark.Text.Split(';')[3]);
            this.mpe_remark.Hide();
            DataTable dt = Navigation.TableSource;
            int rowid = Int32.Parse(this.lh_remark.Text.Split(';')[5]);
            rowid = rowid + ((Navigation.CurrentGridPage) * int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]));
            dt.Rows[rowid]["Remark"] = this.tb_Remark.Text;
            Navigation.TableSource = dt;
            this.tb_Remark.Text = "";
            //string data1 = ""; string data2 = ""; string tb_name = this.tb_name.Text; string tb_jobname = this.tb_jobname.Text; 
            //if (!this.ld_multidate.Text.Equals("0"))
            //{
            //    DateTime today = DateTime.Today;
            //    data2 = today.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            //    int numdays = Int32.Parse(this.ld_multidate.Text);
            //    data1 = DateTime.Today.AddDays(-numdays).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            //}
            //else
            //{
            //    data1 = this.tb_data1.Text;
            //    data2 = this.tb_data2.Text;
            //}
            //if (this.generalTree != null)
            //{
            //    if (!this.CustomValidator1.IsValid)
            //    {
            //        Navigation.Error = CustomValidator1.ErrorMessage;
            //        this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
            //        this.ModalPopupExtender2.Show();
            //        return;
            //    }
            //    if (!this.tb_advisor.Text.Equals(""))
            //    {
            //        tb_name = this.tb_advisor.Text;
            //        tb_jobname = "ALL%" + tb_jobname; //stored procedure recognize special PATTERN ALL% and manage to search in another way
            //    }
            //}
            //int currentpag = Navigation.CurrentGridPage;
            //LoadReports(tb_name, tb_jobname, this.tb_jobname2.Text, this.tb_jobnumber.Text, data1, data2, this.tb_hour1.Text, this.tb_hour2.Text, this.tb_prof.Text, "", this.tb_filterRemark.Text, this.CheckBoxLastReport.Checked);
            //int maxPage = Int32.Parse(this.gv_search.PagerSettings.LastPageText);
            //if (currentpag <= maxPage) {
            //    Navigation.CurrentGridPage = currentpag;    
            //}
            BindGridView();
        }

        protected void gv_search_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (Navigation.CurrentSortDirection == null)
                Navigation.CurrentSortDirection = SORT_DESCENDING;
            if (Navigation.CurrentSortExpression == e.SortExpression)
            {
                if (Navigation.CurrentSortDirection.ToString() == SORT_ASCENDING)
                    Navigation.CurrentSortDirection = SORT_DESCENDING;
                else
                    Navigation.CurrentSortDirection = SORT_ASCENDING;
            }
            else
                Navigation.CurrentSortDirection = SORT_DESCENDING;
            Navigation.CurrentSortExpression = e.SortExpression;

            string data1 = ""; string data2 = ""; string tb_name = this.tb_name.Text; string tb_jobname = this.tb_jobname.Text; 
            if (!this.ld_multidate.Text.Equals("0"))
            {
                DateTime today = DateTime.Today;
                data2 = today.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                int numdays = Int32.Parse(this.ld_multidate.Text);
                data1 = DateTime.Today.AddDays(-numdays).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                data1 = this.tb_data1.Text;
                data2 = this.tb_data2.Text;
            }
            if (this.generalTree != null)
            {
                if (!this.CustomValidator1.IsValid)
                {
                    Navigation.Error = CustomValidator1.ErrorMessage;
                    this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                    this.ModalPopupExtender2.Show();
                    return;
                }
                if (!this.tb_advisor.Text.Equals(""))
                {
                    tb_name = this.tb_advisor.Text;
                    tb_jobname = "ALL%" + tb_jobname; //stored procedure recognize special PATTERN ALL% and manage to search in another way
                }
            }
            LoadReports(tb_name, tb_jobname, this.tb_jobname2.Text, this.tb_jobnumber.Text, data1, data2, this.tb_hour1.Text, this.tb_hour2.Text, this.tb_prof.Text, "", this.tb_filterRemark.Text, this.CheckBoxLastReport.Checked);

        }

        protected void rangepage_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView view = (DataRowView)e.Row.DataItem;
                string jid = view["JobReportID"].ToString(); //attenzione
                string id = view["ReportId"].ToString();
                string name = view["JobName"].ToString();
                string pages = view["ListOfPages"].ToString();
                string fromPage = view["FromPage"].ToString();
                string forPages = view["ForPages"].ToString();
                //string typeMap = view["ExistTypeMap"].ToString();
                string userTimeRef = view["UserTimeRef"].ToString();
                string xferStartTime = view["XferStartTime"].ToString();
                string reportName = view["ReportName"].ToString();
                string totPages = view["TotPages"].ToString();
                string JobNum = view["JobNumber"].ToString();
                string folderName = view["FolderName"].ToString();



                //int totPages = Int32.Parse(view["TotPages"].ToString());
                //bool checkFlag = false;
                //bool checkStatus = false;
                //bool hasIndex = false;
                //if (view["CheckFlag"] != null && view["CheckFlag"].ToString() != "")
                //    checkFlag = bool.Parse(view["CheckFlag"].ToString());//Utility.ConvertStringToBool(view["CheckFlag"].ToString());
                //if (view["CheckStatus"] != null && view["CheckStatus"].ToString() != "")
                //    checkStatus = bool.Parse(view["CheckStatus"].ToString()); //Utility.ConvertStringToBool(view["CheckStatus"].ToString());
                //if (view["HasIndexes"] != null && view["HasIndexes"].ToString() != "")
                //    hasIndex = bool.Parse(view["HasIndexes"].ToString());
                //string lastUserToCheck = view["LastUsedToCheck"].ToString();
                ImageButton b_pdf = (ImageButton)e.Row.FindControl("b_ran_pdf");
                b_pdf.ToolTip = "View PDF - " + jid;
                //b_pdf.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + name + ";" + JobNum + ";" + folderName + ";" + forPages;
                b_pdf.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + reportName + ";" + userTimeRef + ";" + xferStartTime + ";" + name + ";" + JobNum + ";" + folderName + ";" + totPages;
                ImageButton b_txt = (ImageButton)e.Row.FindControl("b_ran_txt");
                b_txt.ToolTip = "View TXT - " + jid;
                //takes always all  pages and nota only the interval of range pages (to change and return to interval need js to be modified!!!)
                b_txt.CommandArgument = jid + ";" + id + ";1," + totPages + ";1;" + reportName + ";" + userTimeRef + ";" + xferStartTime + ";" + name + ";" + JobNum + ";" + folderName + ";" + totPages;
                //b_txt.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + reportName + ";" + userTimeRef + ";" + xferStartTime + ";" + name + ";" + JobNum + ";" + folderName + ";" + totPages;
                
                //b_txt.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + reportName + ";" + name + ";" + JobNum + ";" + folderName + ";" + totPages;
                //b_txt.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + forPages;
                ImageButton b_send_email = (ImageButton)e.Row.FindControl("b_ran_sendm");
                b_send_email.ToolTip = GetLocalResourceObject("messToolEm").ToString() + jid;
                b_send_email.CommandArgument = b_send_email.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + totPages + ";" + "PDF" + ";" + reportName;
                ImageButton b_save_file = (ImageButton)e.Row.FindControl("b_ran_savef");
                b_save_file.ToolTip = GetLocalResourceObject("messToolChoice").ToString() + jid;
                b_save_file.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + reportName + ";" + userTimeRef + ";" + xferStartTime + ";" + name + ";" + JobNum + ";" + folderName + ";0;" + totPages;
                //b_save_file.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + name + ";" + JobNum + ";" + totPages;

            }
        }

        protected void rangepage_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Navigation.CurrGridPageInterv = e.NewPageIndex;
            this.BindGridIntervals();
        }

        protected void rangepage_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            string allArgs = e.CommandArgument.ToString();
            string[] allArgAr = allArgs.Split(';');
            List<String> args = allArgAr.ToList();
            try
            {
                //args.RemoveAt(allArgAr.Length - 1);
                //args.RemoveAt(allArgAr.Length - 3);
                args.RemoveAt(allArgAr.Length - 4);
            }
            catch (Exception ex)
            {
                log.Error("Failed to remove element: " + ex.Message);
            }
            
            if (e.CommandName == "ViewFile")
            {
                int numPages = Int32.Parse(allArgAr.Last());
                //Navigation.ViewParameters = String.Join(";", args.ToArray()) + ";" + numPages;
                //Navigation.ViewParameters2 = "PDF";
                //Navigation.ViewParameters3 = null;
                string encr_equal = "#&##&#"; string encr_and = "&#&&#&"; string encr_log = "&####&";
                
                string parameters = "viep1" + encr_equal + String.Join(";", args.ToArray()) + encr_and +
                    "viep2" + encr_equal + "PDF" + encr_and +
                    "username" + encr_equal + Navigation.User.Username + encr_and +
                    "codauthor" + encr_equal + Navigation.User.CodAuthor + encr_and +                    
                    "session" + encr_equal + Session.SessionID + encr_and +
                    "viep3" + encr_equal + "null" + encr_and + 
                    "viewlog" + encr_equal + String.Join(encr_log, allArgAr);
                string encryptedvalue = Signature.CreateToken(parameters, EXP_TIME_SECONDS);
                //pass krypted value
                ((StartPage)this.Page).OpenViewFile(encryptedvalue);
                //Navigation.LogParameters = allArgs.Split(';');
                //((StartPage)this.Page).OpenViewFile();

            }
            else if (e.CommandName == "ViewFileTxt")
            {
                int numPages = Int32.Parse(allArgAr.Last());
                Navigation.ViewParameters = String.Join(";", args.ToArray()) + ";" + numPages;
                Navigation.ViewParameters2 = "TXT";
                Navigation.ViewParameters3 = null;
                Navigation.ViewParameters3 = String.Join("", ((List<String>)args.GetRange(4, 1)).ToArray());
                //args.RemoveRange(4, 1);
                //args.RemoveRange(4, 3);
                args.RemoveRange(4, 5);
                Navigation.ViewParameters1 = String.Join(";", args.ToArray()) + ";" + numPages;
                Navigation.CurrentCulture = CultureInfo.CurrentUICulture.Name;
                Navigation.LogParameters = allArgs.Split(';');
                ((StartPage)this.Page).OpenViewTxtFile();
            }
            else if (e.CommandName == "SaveFile" || e.CommandName == "SaveXFile")
            {
                this.tbx_download.Text = String.Join("", ((List<String>)args.GetRange(4, 1)).ToArray()).Replace(' ', '_');
                if ("1".Equals(String.Join("", ((List<String>)args.GetRange(10, 1)).ToArray())))
                {
                    this.r_downtype4.Visible = false;
                    this.r_downtype3.Checked = true;
                    this.r_downtype2.Visible = false;
                    this.r_downtype1.Visible = false;
                }
                else
                {
                    if (e.CommandName == "SaveXFile")
                    {
                        this.r_downtype4.Visible = true;
                        this.r_downtype3.Visible = false;
                    }
                    else {
                        this.r_downtype4.Visible = false;
                        this.r_downtype3.Visible = true;
                    }
                    
                    //this.r_downtype3.Visible = true;
                    this.r_downtype2.Visible = true;
                    this.r_downtype1.Visible = true;
                }
                this.lh_download.Text = e.CommandArgument.ToString();
                this.UpdatePanel7.Update();
                Navigation.LogParameters = allArgAr;
                this.mpe_downloadchoice.Show();

            }
             else if (e.CommandName == "SendEmail")
            {
                //b_mail.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage + ";" + totPages + ";" + "PDF" + ";" + reportName;
                if (allArgAr.Length < 6)
                {
                    throw (new Exception("few parameters"));
                }
                //int numPages = Int32.Parse(allArgAr[4]);

                //string parameters = "&amp;format=PDF";
                //int i = 0;
                //foreach (string p in allArgAr)
                //{
                //    parameters += "&amp;parameter" + ++i + "=" + p;
                //}
                //SendMail(allArgAr);
                SendMail(allArgAr[0],allArgAr[1],allArgAr[2],allArgAr[3],allArgAr[4],allArgAr[5],allArgAr[6] );


            }
        }
        
        //private void SendMail(string[] allArgAr)
        private void SendMail(string jobreportid, string reportid, string pages, string frompage, string totpages, string format,  string reportname)
        { 
            
            //string parameters = "jobreportid=" + allArgAr[0];
            //parameters += "&reportid=" + allArgAr[1];
            //parameters += "&pages=" + allArgAr[2];
            //parameters += "&frompage=" + allArgAr[3];
            //parameters += "&totpages=" + allArgAr[4];
            //parameters += "&format=" + allArgAr[5];
            //parameters += "&reportname=" + allArgAr[6];
            string parameters = "jobreportid=" + jobreportid;
            parameters += "&reportid=" + reportid;
            parameters += "&pages=" + pages;
            parameters += "&frompage=" + frompage;
            parameters += "&totpages=" + totpages;
            parameters += "&reportname=" + reportname;

            //string PDFparameters = parameters += "&format=PDF";
            //string TXTparameters = parameters += "&format=TXT";
            //string CSVparameters = parameters += "&format=CSV";
            parameters += "&format=" + format;
            string expiration_time_seconds =
            (
                (
                    (ConfigurationManager.AppSettings["EXPIRATION_TIME_SECONDS"] != null) &&
                    (!ConfigurationManager.AppSettings["EXPIRATION_TIME_SECONDS"].Equals(""))
                ) ? ConfigurationManager.AppSettings["EXPIRATION_TIME_SECONDS"]
                    : "86400"
            );


            string encryptedvalue = Signature.CreateToken(parameters, expiration_time_seconds);


            //string PDFencryptedvalue = Signature.CreateToken(PDFparameters);
            //string TXTencryptedvalue = Signature.CreateToken(TXTparameters);
            //string CSVencryptedvalue = Signature.CreateToken(CSVparameters);
            parameters = "&key=" + encryptedvalue;
            //PDFparameters = "&key=" + PDFencryptedvalue;
            //TXTparameters = "&key=" + TXTencryptedvalue;
            //CSVparameters = "&key=" + CSVencryptedvalue;

                string twr = (
                    (Request.Params["twr"] != null && Request.Params["twr"] != "") ? Request.Params["twr"] :
                    (ConfigurationManager.AppSettings["PGE_TWR"] != null && ConfigurationManager.AppSettings["PGE_TWR"] != "") ? ConfigurationManager.AppSettings["PGE_TWR"] :
                    "ufr-uj.collaudo.usinet.it");
            //string currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            //currentUrl = currentUrl.Substring(0, currentUrl.LastIndexOf("/"));
			if((ConfigurationManager.AppSettings["APPLICATION_URL"] == null) || (ConfigurationManager.AppSettings["APPLICATION_URL"].Equals("")))
			{
				throw (new Exception("Error - parameter APPLICATION_URL not found in configuration or not correct."));
			}

			string currentUrl = ConfigurationManager.AppSettings["APPLICATION_URL"];


            string link4report = "http://" + twr + "/XA-PGE-PF/private/pgeStartApp.do?applicationToStart=token&pgeMenuId=start&pgeuidname=user&tokenString=token&pgeAppCode=X1N&writeInfo=y&backUrl=" + currentUrl + "/ViewReport.aspx&timeout=180000&twr=" + twr + "&effective_twr=[var_tower]" + parameters+"&void=void";
            //string PDFlink4report = "http://" + twr + "/XA-PGE-PF/private/pgeStartApp.do?applicationToStart=token&pgeMenuId=start&pgeuidname=user&tokenString=token&pgeAppCode=X1N&writeInfo=y&backUrl=" + currentUrl + "/ViewReport.aspx&timeout=180000&twr=" + twr + "&effective_twr=[var_tower]" + PDFparameters;
            //string TXTlink4report = "http://" + twr + "/XA-PGE-PF/private/pgeStartApp.do?applicationToStart=token&pgeMenuId=start&pgeuidname=user&tokenString=token&pgeAppCode=X1N&writeInfo=y&backUrl=" + currentUrl + "/ViewReport.aspx&timeout=180000&twr=" + twr + "&effective_twr=[var_tower]" + TXTparameters;
            //string CSVlink4report = "http://" + twr + "/XA-PGE-PF/private/pgeStartApp.do?applicationToStart=token&pgeMenuId=start&pgeuidname=user&tokenString=token&pgeAppCode=X1N&writeInfo=y&backUrl=" + currentUrl + "/ViewReport.aspx&timeout=180000&twr=" + twr + "&effective_twr=[var_tower]" + CSVparameters;

                //string command = "mailto:?subject=XENA%20Report&body=This%20mail%20contains%20the%20following%20report:%0A%0Ahttp://ufr-uj.collaudo.usinet.it/XA-PGE-PF/private/pgeStartApp.do%3FapplicationToStart=token%26pgeMenuId=start%26pgeuidname=user%26tokenString=token%26pgeAppCode=X1N%26writeInfo=y%26backUrl=http://localhost:53190/ViewReport.aspx%26timeout=180000%26twr=ufr-uj.collaudo.usinet.it%26effective_twr=[var_tower]%26JobReportID=189093%26ReportID=2%0A%0AGenarated by XENA Application.";
            //string command = "mailto:?subject=XENA%20Report&body=This%20mail%20contains%20the%20following%20report:%0A%0A" + HttpUtility.UrlEncode(link4report) + "%0A%0AGenarated by XENA Application.";
            string command = "mailto:?subject=XENA%20Report&body=The%20requested%20file%20is%20now%20available.%0APlease%20paste%20and%20copy%20the%20following%20link%20in%20the%20address-bar%20of%20your%20browser. Before%20to%20open%20the%20link%20check%20to%20be%20logged%20in%20the%20Application%20Menu%20page%20of%20your%20branch%20(PGE%20home)." +
                                                                                                                  "%0A%0A" + HttpUtility.UrlEncode(link4report) +
                                                                                                                  "%0A%0AGenarated by XENA Application.";
            //string command = "mailto:?subject=XENA%20Report&body=The%20requested%20files%20are%20now%20available.%0APlease%20paste%20and%20copy%20the%20following%20links%20in%20the%20address-bar%20of%20your%20browser.Before%20to%20open%20each%20link%20check%20to%20be%20logged%20in%20the%20Application%20Menu%20page%20of%20your%20branch%20(PGE%20home)." +
            //                                                                                                      "%0A(PDF format):%0A" + HttpUtility.UrlEncode(PDFlink4report) +
            //                                                                                                      "%0A(TXT format):%0A" + HttpUtility.UrlEncode(TXTlink4report) +
            //                                                                                                      "%0A(CSV format):%0A" + HttpUtility.UrlEncode(CSVlink4report) +
            //                                                                                                      "%0A%0AGenarated by XENA Application.";
            //The%20requested%20files%20are%20now%20available.%0APlease%20paste%20and%20copy%20the%20following%20links%20in%20the%20address-bar%20of%20your%20browser.Before%20to%20open%20each%20link%20check%20to%20be%20logged%20in%20the%20Application%20Menu%20page%20of%20your%20branch%20(PGE%20home).

                ((StartPage)this.Page).OpenMail(command);
              
            //try
            //{
            //    Microsoft.Office.Interop.Outlook.Application oApp = new Microsoft.Office.Interop.Outlook.Application();
            //    Microsoft.Office.Interop.Outlook.MailItem oMsg = (Microsoft.Office.Interop.Outlook.MailItem)oApp.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olMailItem);

            //    oMsg.Subject = "XENA Report";
            //    oMsg.BodyFormat = Microsoft.Office.Interop.Outlook.OlBodyFormat.olFormatHTML;
            //    oMsg.BCC = "";
            //    oMsg.To = "";

            //    string body = ""
            //                + "<html>"
            //                + "<head></head>"
            //                + "<body>"
            //                + "<p><span>The requested file is available clicking </span>"
            //                + "<span>"
            //                + "<a href=\"" + link4report + "\">here</a>"
            //                + "</span></p>"
            //                + "<p><span>Before clicking check to be logged in the Application Menu page of your branch (PGE home)</span></p>"
            //                + "<p><span>&nbsp;</span></p>"
            //                + "<p><span>Genarated by XENA Application.<o:p></o:p></span></p>"
            //                + "</div>"
            //                + "</body>"
            //                + "</html>"
            //        ;

            //    oMsg.HTMLBody = body;
            //    //oMsg.Attachments.Add(Convert.ToString(@"/my_location_virtual_path/myfile.txt"), Microsoft.Office.Interop.Outlook.OlAttachmentType.olByValue, Type.Missing, Type.Missing);

            //    oMsg.Display(true); //In order to displ   

            //}
            //catch
            //{ 
            //    //string command = "mailto:?subject=XENA%20Report&body=This%20mail%20contains%20the%20following%20report:%0A%0Ahttp://ufr-uj.collaudo.usinet.it/XA-PGE-PF/private/pgeStartApp.do%3FapplicationToStart=token%26pgeMenuId=start%26pgeuidname=user%26tokenString=token%26pgeAppCode=X1N%26writeInfo=y%26backUrl=http://localhost:53190/ViewReport.aspx%26timeout=180000%26twr=ufr-uj.collaudo.usinet.it%26effective_twr=[var_tower]%26JobReportID=189093%26ReportID=2%0A%0AGenarated by XENA Application.";
            //    string command = "mailto:?subject=XENA%20Report&body=This%20mail%20contains%20the%20following%20report:%0A%0A" + link4report.Replace("?", "%3F").Replace("&", "%26").Replace("&", "%26") + "%0A%0AGenarated by XENA Application.";
            //    Process.Start(command); 
            //}

        }

        protected void rangepage_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (Navigation.CurrSortIntDirection == null)
                Navigation.CurrSortIntDirection = SORT_ASCENDING;
            if ("Range_Pages" == e.SortExpression)
            {
                if (Navigation.CurrSortIntDirection.ToString() == SORT_ASCENDING)
                    Navigation.CurrSortIntDirection = SORT_DESCENDING;
                else
                    Navigation.CurrSortIntDirection = SORT_ASCENDING;
            }
            else
                Navigation.CurrSortIntDirection = SORT_DESCENDING;
            //Navigation.CurrentSortExpression = e.SortExpression;
            BindGridIntervals();
            //LoadIntervals(allArgAr , numPages);
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime d1;
            DateTime d2;
            if (DateTime.TryParse(this.tb_data1.Text, out d1)
             && DateTime.TryParse(this.tb_data2.Text, out d2))
                args.IsValid = d1 < d2;
            else
                args.IsValid = false;
        }

        protected void ddl_group_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddl_group_CSavedSearch(object sender, EventArgs e)
        {
            //retrieve all value from table and insert into form
            //JobName;ReportName;JobNumber;AdvisorCode;FolderName;Remark;Path

            try
            {
                if (Navigation.SavedSearch != null)
                {
                    if ("" != this.CSavedSearch.SelectedValue)
                    {
                        IDictionary<string, string> values = Navigation.SavedSearch[this.CSavedSearch.SelectedValue];
                        foreach (string value in values.Keys)
                        {
                            switch (value)
                            {
                                case "JobName":
                                    this.tb_jobname.Text = values[value];
                                    break;
                                case "ReportName":
                                    this.tb_name.Text = values[value];
                                    break;
                                case "JobNumber":
                                    this.tb_jobnumber.Text = values[value];
                                    break;
                                case "AdvisorCode":
                                    this.tb_advisor.Text = values[value];
                                    break;
                                case "FolderName":
                                    this.tb_prof.Text = values[value];
                                    break;
                                case "Remark":
                                    this.tb_Remark.Text = values[value];
                                    break;
                                case "Path":
                                    this.txtHidData.Value = values[value];
                                    if (!SelectTreeView(generalTree, this.txtHidData.Value))
                                    {
                                        throw new Exception("WARNING : <br/> Not found tree folder [" + values[value] + "] saved for search <br/> "
                                            + this.CSavedSearch.SelectedValue + "<br/> Open the tree or check your authorization ");
                                    };
                                    break;
                            }
                        }
                    }
                    this.UpdatePanel4.Update();
                    this.UpdatePanel1.Update();
                }
                else {
                    throw new Exception("Session timeout close and reload page");
                }
            }
            catch (Exception ex)
            {
                this.lb_Popup.Text = ex.Message;
                this.UpdatePanel9.Update();
                this.ModalPopupExtender2.Show();
            }
        }

        protected void tb_specUser_TextChanged(object sender, EventArgs e)
        {
            this.tb_specUser.Text = this.tb_specUser.Text.Trim();
        }

        protected void tb_jobname_TextChanged(object sender, EventArgs e)
        {
            this.tb_jobname.Text = this.tb_jobname.Text.Trim();
            //Regex rgx = new Regex("[^a-zA-Z0-9*?%_]");
            //this.tb_jobname.Text = rgx.Replace(this.tb_jobname.Text, "");
        }

        protected void tb_jobname2_TextChanged(object sender, EventArgs e)
        {
            this.tb_jobname2.Text = this.tb_jobname2.Text.Trim();
            //Regex rgx = new Regex("[^a-zA-Z0-9*?%_]");
            //this.tb_jobname2.Text = rgx.Replace(this.tb_jobname2.Text, "");
        }

        protected void tb_name_TextChanged(object sender, EventArgs e)
        {
            this.tb_name.Text = this.tb_name.Text.Trim();
        }

        protected void tb_jobnumber_TextChanged(object sender, EventArgs e)
        {
            this.tb_jobnumber.Text = this.tb_jobnumber.Text.Trim();
        }

        protected void tb_prof_TextChanged(object sender, EventArgs e)
        {
            this.tb_prof.Text = this.tb_prof.Text.Trim();
        }


        protected void tb_advisor_TextChanged(object sender, EventArgs e)
        {
            this.tb_advisor.Text = tb_advisor.Text.Trim();
        }

        protected void tb_data1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void tb_data2_TextChanged(object sender, EventArgs e)
        {

        }

        protected void rangepages_grid_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void populateChild(object sender, TreeNodeEventArgs e)
        {

            if (!e.Node.Text.Equals("START"))
            {
                e.Node.ChildNodes.Clear();
                //CallNodesSelector(e);
                DataTable mytable = LoadFolders(e.Node.Value, 1);
                if (mytable != null)
                {
                    foreach (DataRow dr in mytable.Rows)
                    {
                        TreeNode node = new CeTreeNode(dr["FolderNameKey"].ToString());
                        node.Value = dr["FolderNameKey"].ToString();
                        node.ToolTip = dr["FolderDescr"].ToString();
                        node.ImageUrl = "~/Images/folder-closed-2.jpg";
                        //node.NavigateUrl = "javascript:simulate_click(event);";
                        //node.NavigateUrl = "javascript:void(0);";
                        node.SelectAction = TreeNodeSelectAction.Select; // selectexpand togheter create problems with javascript submitting form 
                        if (dr["HasChilds"].ToString().Equals("1"))
                            node.ChildNodes.Add(new TreeNode("FAKE"));
                        e.Node.ChildNodes.Add(node);

                    }
                    //e.Node.ExpandAll(); if active became recursive
                    this.txtHidData.Value = e.Node.Value;
                }
            }
            setNavigateUrl(this.generalTree); // set navigation url inside each node to trigger only client script an not post_back

        }

        protected void generalTree_SelectedNodeChanged(object sender, EventArgs e)
        {
            //this.tb_prof.Text = ((TreeNodeEventArgs)e).Node.Text;
            //this.tb_prof.Text = this.generalTree.SelectedNode.Value;
            Navigation.User.AltProfilo = (new string[] {this.generalTree.SelectedNode.Value});
            this.UpdatePanel5.Update();
            if (!this.CustomValidator1.IsValid)
            {
                Navigation.Error = CustomValidator1.ErrorMessage;
                this.lb_Popup.Text = GetLocalResourceObject("err.generic").ToString() + " <br/>" + Navigation.Error;
                this.ModalPopupExtender2.Show();
            }
            //this.generalTree.Focus();
            //this.generalTree.FindNode().
            Navigation.CurrentSortExpression = "Recipient";
            Navigation.CurrentSortDirection = SORT_ASCENDING;
            string data1 = ""; string data2 = "";
            if (!this.ld_multidate.Text.Equals("0"))
            {
                DateTime today = DateTime.Today;
                data2 = today.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                int numdays = Int32.Parse(this.ld_multidate.Text);
                data1 = DateTime.Today.AddDays(-numdays).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                data1 = this.tb_data1.Text;
                data2 = this.tb_data2.Text;
            }
            this.txtHidData.Value = this.generalTree.SelectedNode.Value;
            LoadReports(this.tb_name.Text, this.tb_jobname.Text, this.tb_jobname2.Text, this.tb_jobnumber.Text, data1, data2, this.tb_hour1.Text, this.tb_hour2.Text, this.tb_prof.Text, "", this.tb_filterRemark.Text,this.CheckBoxLastReport.Checked);
            
        }

        protected void generalTree_PreRender(object sender, EventArgs e)
        {
            //if (!this.txtHidData.Value.Equals(""))
            //    SelectTreeView(generalTree, this.txtHidData.Value);

        }

        protected void CheckBoxLastReport_CheckedChanged(object sender, EventArgs e)
        {
            manageLast();
            //bool statusOfDateControls = true;
            //if (this.CheckBoxLastReport.Checked)
            //{
            //    statusOfDateControls = false;
            //}
            //this.tb_data1.Enabled = statusOfDateControls;
            //this.tb_data2.Enabled = statusOfDateControls;
            //this.b_data1.Enabled = statusOfDateControls;
            //this.b_data2.Enabled = statusOfDateControls;

            //this.ld_multidate.Enabled = statusOfDateControls;
            //this.tb_hour1.Enabled = statusOfDateControls;
            //this.tb_hour2.Enabled = statusOfDateControls;


        }

        private void manageLast() {
            bool statusOfDateControls = true;
            if (this.CheckBoxLastReport.Checked)
            {
                statusOfDateControls = false;
            }
            this.tb_data1.Enabled = statusOfDateControls;
            this.tb_data2.Enabled = statusOfDateControls;
            this.b_data1.Enabled = statusOfDateControls;
            this.b_data2.Enabled = statusOfDateControls;

            this.ld_multidate.Enabled = statusOfDateControls;
            this.tb_hour1.Enabled = statusOfDateControls;
            this.tb_hour2.Enabled = statusOfDateControls;
        
        }
        //protected void getSecondValue(object sender, EventArgs e) {
        //    log.Debug("ecco sono dentro");

        //}
        #endregion

        //#region WebMethods
        //[WebMethod]

        //public static string[] GetInnerCompletionList2(string prefixText, int count, string contextKey)
        //{
        //    string query = Utility.BuildQueryAutocomplete(contextKey, prefixText);
        //    string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
        //    int hostPort = int.Parse(ConfigurationManager.AppSettings["ElasticSearchHost_Port"]);
        //    var connection = new ElasticConnection(host, hostPort);
        //    string result = connection.Post(Commands.Search(Navigation.CurrentIndex, Navigation.CurrentType), query);
        //    JToken jt = JToken.Parse(query);
        //    string formattedRes = jt.ToString(Newtonsoft.Json.Formatting.Indented);
        //    log_ES.Debug("QUERY STRING GetCompletionList (for autocomplete): " + formattedRes);
        //    jt = JToken.Parse(result);
        //    formattedRes = jt.ToString(Newtonsoft.Json.Formatting.Indented);
        //    log_ES.Debug("RES STRING GetCompletionList (for autocomplete): " + formattedRes);
        //    var serializer = new JsonNetSerializer();
        //    RootObject root = serializer.Deserialize<RootObject>(result);
        //    List<string> list = new List<string>();
        //    foreach (BucketLeaf t in root.aggregations.suggest.buckets)
        //        list.Add(t.key);
        //    return list.ToArray();
        //}
        //#endregion
    }
}