﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainPage.aspx.cs" Inherits="XReportSearch.MainPage"
    Theme="XReportSearchTheme" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/SearchOnline.ascx" TagPrefix="ced" TagName="search" %>
<%@ Register Src="~/SearchHistory.ascx" TagPrefix="cedHS" TagName="searchHS" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
    <title>XReport Search</title>
    <link href="~/App_Themes/XReportSearchAjaxControls.css?2" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/XReportSearchStyle.css?4" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/Menu.css?4" rel="stylesheet" type="text/css" />
</head>
<body onload="resize()" onresize="resize()" onclick="resize()" onbeforeprint="resize()">
    <script language="javascript" type="text/javascript">

    

        function resize() {
            var frame = document.getElementById("MainDiv");
            var page = document.getElementById("panelTable");
            var S1 = document.getElementById("S1");
            var page2 = document.getElementById("panelTable2");
            var windowheight = document.documentElement.clientHeight;
            var windowwidth = document.documentElement.clientWidth;
            if (frame != null && frame.style.height != null) {
                frame.style.height = windowheight + "px";
            }
            if (document.getElementById("txtHidData") != null) {
                document.getElementById("txtHidData").value = windowheight;
            }
            if (document.getElementById("txtHidData2") != null) {
                document.getElementById("txtHidData2").value = windowwidth;
            }
            if (frame != null && frame.style.height != null) {
                frame.style.height = (windowheight - 200) + "px";
            }
            if (page != null && page.style.height != null) {
                page.style.height = (windowheight - 300) + "px";
            }
            if (page2 != null && page2.style.height != null) {
                page2.style.height = (windowheight - 102) + "px";
            }

            if (windowwidth < 800) {
                frame.style.fontSize = "0.4em";
            }
            else if (windowwidth > 2000) {
                frame.style.fontSize = "2em";
            }
            else {
                frame.style.fontSize = "0.7em";
            }
        }
    </script>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"
            AsyncPostBackTimeout="600" />
        <script language="javascript" type="text/javascript">
            function pageLoad() {
                if ($find('ModalProgress') != null)
                    $find('ModalProgress').add_showing(onshowing);
                resize();
            }

            function onshowing() {
                if ($find('ModalProgress') != null) {
                    var windowwidth = document.documentElement.clientWidth;
                    var windowheight = document.documentElement.clientHeight;
                    $find('ModalProgress').set_X((parseInt(windowwidth) / 2) - 100);
                    $find('ModalProgress').set_Y((parseInt(windowheight) / 2) - 100);
                }
            }

            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

            function beginRequest() {
                if ($find('ModalProgress') != null)
                    $find('ModalProgress').show();
            }

            function endRequest() {
                if ($find('ModalProgress') != null)
                    $find('ModalProgress').hide();
            }

            var xPos, yPos;
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(BeginRequestHandler);
            prm.add_endRequest(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                if ($get('divCustomGrid') != null) {
                    xPos = $get('divCustomGrid').scrollLeft;
                    yPos = $get('divCustomGrid').scrollTop;
                }
            }

            function EndRequestHandler(sender, args) {
                if ($get('divCustomGrid') != null) {
                    $get('divCustomGrid').scrollLeft = xPos;
                    $get('divCustomGrid').scrollTop = yPos;
                }
            }

            function hideF() {
                var b_Tab1 = document.getElementById('b_Tab1');
                var b_Tab2 = document.getElementById('b_Tab2');
                var panel = document.getElementById('panelTable');
                var panelH = document.getElementById('panelTable2');
                b_Tab1.className = 'buttonMenu';
                b_Tab2.className = 'buttonMenuSelected';
                panel.style.display = 'none';
                panelH.style.display = 'block';
            }

            function hideFQ() {
                var b_Tab1 = document.getElementById('b_Tab1');
                var b_Tab2 = document.getElementById('b_Tab2');
                var panel = document.getElementById('panelTable');
                var panelH = document.getElementById('panelTable2');
                b_Tab1.className = 'buttonMenuSelected';
                b_Tab2.className = 'buttonMenu';
                panelH.style.display = 'none';
                panel.style.display = 'block';
            }

        </script>

        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div id="MainDiv" class="main">
                    <div class="headerContainer" id="headerDiv">
                        <div class="headerLeft">
                            <div class="headerTitleLeft">
                                <asp:Button Text="Online" BorderStyle="None" ID="b_Tab1" runat="server" OnClick="b_Tab1_Click"
                                    CssClass="buttonMenuSelected" />
                                <asp:Button Text="History" BorderStyle="None" ID="b_Tab2" runat="server" OnClick="b_Tab2_Click"
                                    CssClass="buttonMenu" />
                            </div>
                        </div>
                        <div class="headerRight">
                        </div>
                    </div>
                    <div class="headerContainerTitleInner" id="Div1">
                        <div class="headerTitleLeftInner">
                            <asp:Label ID="lb_User" runat="server" SkinID="LabelFilter" Width="500px"></asp:Label>
                            <asp:Label ID="Label6" Width="30px" runat="server"></asp:Label>
                            <asp:Label ID="lb_Ambiente" runat="server" SkinID="LabelFilter"></asp:Label>
                            <asp:Button Text="" BorderStyle="None" ID="Button1" runat="server" Visible="false" />
                            <asp:Button Text="" BorderStyle="None" ID="Button2" runat="server" Visible="false" />
                        </div>
                    </div>

                    <asp:Panel runat="server" ID="panelTable" CssClass="page" ScrollBars="None" >
                    <!--<div id="panelTable" class="page" style="overflow: hidden">-->
                        <ced:search runat="server" ID="S1" EnableTheming="true"></ced:search>
                        <!--<iframe runat="server" id="frame" height="100%" width="100%" visible="true"></iframe>-->
                        </asp:Panel>
                    <!--</div>-->
                    <!--<div id="panelTable2" class="page" style="overflow: hidden">-->
                        <asp:Panel runat="server" ID="panelTable2" CssClass="page" ScrollBars="None" Visible="true">
                        <!--<iframe runat="server" id="frameH" height="100%" width="100%" visible="true" clientidmode="Static"></iframe>-->
                        <cedHS:searchHS runat="server" ID="S2" EnableTheming="true"></cedHS:searchHS>
                        </asp:Panel>
                    <!--</div>-->

                </div>
                <asp:UpdateProgress ID="UpdateProgress" runat="server" DisplayAfter="0">
                    <ProgressTemplate>
                        <asp:Panel ID="panelUpdateProgress" runat="server">
                            <div>
                                <img id="Img1" src="~/Images/loader.gif" alt="" runat="server" />
                            </div>
                        </asp:Panel>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <ajax:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
                    BackgroundCssClass="modalProgress" PopupControlID="panelUpdateProgress" BehaviorID="ModalProgress">
                </ajax:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
        <input type="hidden" clientidmode="Static" id="txtHidData" runat="server" />
        <input type="hidden" clientidmode="Static" id="txtHidData2" runat="server" />
    </form>
</body>
</html>
