﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Configuration;
using System.Web.Services;
using System.Web.Services.Protocols;
using XReportSearch.Utils;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;
using XReportSearch.Business;
using XReportSearch.XReportWebIface;


namespace XReportSearch
{
    public partial class Indexes4Uni : System.Web.UI.Page
    {
        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog log_ES = LogManager.GetLogger("ES_Log");
        private const string SCRIPT_DOFOCUS = @"window.setTimeout('DoFocus()', 1);
            function DoFocus()
            {
                try { document.getElementById('REQUEST_LASTFOCUS').focus(); } catch (ex) {}
            }";
        #endregion

        #region Main Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    Navigation.CombosSources = null;
                    Navigation.CombosValues = null;
                    Navigation.DynamicFormControls = null;
                    LoadIndexes();
                    HookOnFocus(this.Page as Control);
                }
                else
                {
                    if (Navigation.DynamicFormControls != null)
                    {
                        log.Info("Creating dynamic controls");
                        LoadDynamicControls();
                    }
                    if (GetPostBackControlId() == "")
                        log.Info("Creating null");

                }
            }
            catch (Exception ex)
            {
                Navigation.Error = "An error occurred:" + ex.Message;
                //HttpContext.Current.Application.set
                Response.Redirect("~/Error.aspx?disableReload=Y&enableClose=N");
            }



            ScriptManager.RegisterStartupScript(this, typeof(Indexes4Uni), "ScriptDoFocus", SCRIPT_DOFOCUS.Replace("REQUEST_LASTFOCUS", Request["__LASTFOCUS"]), true);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Gets the ID of the post back control.
        /// 
        /// See: http://geekswithblogs.net/mahesh/archive/2006/06/27/83264.aspx
        /// </summary>
        /// <param name = "page">The page.</param>
        /// <returns></returns>
        private string GetPostBackControlId()
        {

            Control control = null;
            // first we will check the "__EVENTTARGET" because if post back made by the controls
            // which used "_doPostBack" function also available in Request.Form collection.
            string controlName = Page.Request.Params["__EVENTTARGET"];
            string controlArg = Page.Request.Params["__EVENTARGUMENT"];
            if (!String.IsNullOrEmpty(controlName))
            {
                control = Page.FindControl(controlName);
            }
            else
            {
                // if __EVENTTARGET is null, the control is a button type and we need to
                // iterate over the form collection to find it

                // ReSharper disable TooWideLocalVariableScope
                string controlId;
                Control foundControl;
                // ReSharper restore TooWideLocalVariableScope

                foreach (string ctl in Page.Request.Form)
                {
                    // handle ImageButton they having an additional "quasi-property" 
                    // in their Id which identifies mouse x and y coordinates
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        controlId = ctl.Substring(0, ctl.Length - 2);
                        foundControl = Page.FindControl(controlId);
                    }
                    else
                    {
                        foundControl = Page.FindControl(ctl);
                    }

                    if (!(foundControl is IButtonControl)) continue;

                    control = foundControl;
                    break;
                }
            }

            return control == null ? String.Empty : control.ID;
        }

        private void LoadIndexes()
        {
            try
            {
                log.Info("LoadIndexes() Started");
                //XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface();
                //XReportWebIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                XReportWebIface.DocumentData Data = BuildDocumentData1("", "", "", "");
                XReportWebIFace.Timeout = 60000;
                Navigation.IndexesData = XReportWebIFace.getIndexes(Data);
                Navigation.IndexesData.DISTINCT = "SUM";
                bool next = BindIndexCombo();
                LoadIndexStruct();
                log.Info("LoadIndexes() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadIndexes() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadIndexes() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private void LoadIndexStruct()
        {
            try
            {
                log.Info("LoadIndexStruct() Started");
                //XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface();
                //XReportWebIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                XReportWebIFace.Timeout = 60000;
                XReportWebIface.DocumentData Data = null;
                Data = XReportWebIFace.GetIndexStruct(Navigation.IndexesData);
                if (Data.IndexEntries != null)
                {
                    Navigation.Fields = Utility.Fields2Table(Data.IndexEntries[0].Columns[0].Value);
                    FillDictionaries(Navigation.Fields);
                    if (Navigation.IndexExampleHashtable == null)
                    {
                        Navigation.IndexExampleHashtable = new System.Collections.Hashtable();
                        Navigation.IndexSuggestionHash = new System.Collections.Hashtable();
                    }
                    else
                    {
                        Navigation.IndexExampleHashtable.Clear();
                        Navigation.IndexSuggestionHash.Clear();
                    }
                    foreach (DataRow dr in Navigation.Fields.Rows)
                    {
                        if (dr["Primary"].ToString() == "1")
                        {
                            Data = BuildDocumentData1(dr["Field"].ToString(), "", "", "");
                            Data.IndexName = Navigation.IndexesData.IndexName;
                            IEnumerable<string> listValues = XReportWebIFace.GetIndexExample(Data).IndexEntries.Skip(1).Select(x => x.Columns[0].Value);
                            string IndexExampleListString = string.Join("\n", listValues.Take(200));
                            Navigation.IndexExampleHashtable.Add(dr["Field"].ToString(), IndexExampleListString);

                            Navigation.IndexSuggestionHash.Add(dr["Field"].ToString(), new List<String>(listValues));
                            //break;
                        }
                    }
                    LoadDynamicControls();
                }
                Navigation.IndexesData.IndexEntries[0].Columns = null;
                log.Info("LoadIndexStruct() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadIndexStruct() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadIndexStruct() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private void LoadIndex()
        {
            try
            {
                log.Info("LoadIndex Started");
                //XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface();
                //XReportWebIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                XReportWebIFace.Timeout = 60000;
                bool primarySet = false;
                string retValue = null; string retCol = null;
                foreach (DataRow dr in Navigation.Fields.Rows)
                {
                    string colname = dr["Field"].ToString();
                    XReportWebIface.column col = null;
                    if (Navigation.IndexesData.IndexEntries[0].Columns != null)
                        col = Array.Find(Navigation.IndexesData.IndexEntries[0].Columns, delegate(XReportWebIface.column c) { return c.colname == colname; });
                    if (col != null)
                    {
                        //if (dr["Primary"].ToString() == "1")
                        //{
                        //    if (dr["Value"].ToString() != "")
                        //        primarySet = true;
                        //    col.Value = dr["Value"].ToString();
                        //    col.Max = dr["Value"].ToString();
                        //    col.Min = dr["Value"].ToString();
                        //}
                        //else
                        //{
                        //    col.Value = "%" + dr["Value"].ToString() + "%";
                        //    col.Max = "%" + dr["Value"].ToString() + "%";
                        //    col.Min = "LIKE";
                        //}
                        if (dr["Value"].ToString() != "")
                        {
                            if (dr["Primary"].ToString() == "1")
                                primarySet = true;
                            col.Value = dr["Value"].ToString();
                            col.Max = dr["Value"].ToString();
                            //if (col.Value.EndsWith("*"))
                            //    col.Value = col.Value.Remove(col.Value.LastIndexOf("\u2605", 0,1))+"%";
                            //if (col.Max.EndsWith("*"))
                            //    col.Max = col.Max.Remove(col.Max.LastIndexOf("*", 0))+"%";
                        }
                        else
                        {
                            col.Value = "%";
                            col.Max = "%";

                        }
                        col.Min = "LIKE";
                    }
                    else
                    {
                        col = new XReportWebIface.column();
                        col.colname = dr["Field"].ToString();
                        //if (dr["Primary"].ToString() == "1")
                        //{
                        //    if (dr["Value"].ToString() != "")
                        //        primarySet = true;
                        //    col.Value = dr["Value"].ToString();
                        //    col.Max = dr["Value"].ToString();
                        //    col.Min = dr["Value"].ToString();
                        //}
                        //else
                        //{
                        //    col.Value = "%" + dr["Value"].ToString() + "%";
                        //    col.Max = "%" + dr["Value"].ToString() + "%";
                        //    col.Min = "LIKE";
                        //}
                        if (dr["Value"].ToString() != "")
                        {
                            if (dr["Primary"].ToString() == "1")
                                primarySet = true;
                            col.Value = dr["Value"].ToString();
                            col.Max = dr["Value"].ToString();
                            //if (col.Value.EndsWith("*"))
                            //    col.Value = col.Value.Remove(col.Value.LastIndexOf("\u2605", 0)) + "%";
                            //if (col.Max.EndsWith("*"))
                            //    col.Max = col.Max.Remove(col.Max.LastIndexOf("*", 0)) + "%";

                        }
                        else
                        {
                            col.Value = "%";
                            col.Max = "%";

                        }
                        col.Min = "LIKE";

                        List<XReportWebIface.column> columns = new List<XReportWebIface.column>();
                        if (Navigation.IndexesData.IndexEntries[0].Columns != null)
                            columns = Navigation.IndexesData.IndexEntries[0].Columns.ToList();
                        columns.Add(col);
                        Navigation.IndexesData.IndexEntries[0].Columns = columns.ToArray();
                        Navigation.IndexesData.IndexEntries[0].JobReportId = Navigation.ViewParameters.Split(';')[0];
                    }
                }
                //if (primarySet)
                //{
                if (Navigation.IndexesData.IndexEntries[0].Columns[0].colname != null)
                    Navigation.IndexesData.ORDERBY = Navigation.IndexesData.IndexEntries[0].Columns[0].colname;
                Navigation.IndexesDataLast = XReportWebIFace.getIndexesUnicredit(Navigation.IndexesData);
                Navigation.IndexesData.ORDERBY = "";
                if (Navigation.IndexesData.DISTINCT == "SUM")
                {
                    if (Navigation.IndexesDataLast.IndexEntries != null && Navigation.IndexesDataLast.IndexEntries[0].Columns[0].Value != null && int.Parse(Navigation.IndexesDataLast.IndexEntries[0].Columns[0].Value) > 0)
                    {
                        this.lb_doc.Text = Navigation.IndexesDataLast.IndexEntries[0].Columns[0].Value + " PAGES RETURNED";
                        this.lb_doc.Visible = true;
                        this.lb_doc.Enabled = true;
                        this.t_doc.Visible = true;
                        this.t_doc.Enabled = true;
                        this.t_doc.ToolTip = "View TXT";
                        this.t_pdf.Visible = true;
                        this.t_pdf.Enabled = true;
                        this.t_pdf.ToolTip = "View PDF";
                        this.l_doc.Visible = false;
                        this.l_doc.Width = new Unit(0, UnitType.Pixel);
                    }
                    else
                    {
                        this.l_doc.Text = "NO PAGES RETURNED";
                        this.lb_doc.Visible = false;
                        this.lb_doc.Enabled = false;
                        this.t_doc.Visible = false;
                        this.l_doc.Visible = true;
                        this.t_pdf.Visible = false;
                        this.l_doc.Width = new Unit(200, UnitType.Pixel);
                    }
                }
                //}
                //else
                //{
                //    this.l_doc.Text = "INSERT PRIMARY INDEX";
                //    this.lb_doc.Visible = false;
                //    this.lb_doc.Enabled = false;
                //    this.l_doc.Visible = true;
                //    this.l_doc.Width = new Unit(200, UnitType.Pixel);
                //}
                if (Navigation.IndexesDataLast.IndexEntries != null && Navigation.IndexesDataLast.IndexEntries[0].Columns != null && Navigation.IndexesDataLast.IndexEntries[0].Columns.Count() > 1 && Navigation.IndexesDataLast.IndexEntries[0].Columns[1].Value != "")
                {
                    retValue = Navigation.IndexesDataLast.IndexEntries[0].Columns[1].Value;
                    retCol = Navigation.IndexesDataLast.IndexEntries[0].Columns[1].colname;

                }
                else if (Navigation.IndexesData.IndexEntries != null && Navigation.IndexesData.IndexEntries[0].Columns != null && Navigation.IndexesData.IndexEntries[0].Columns.Count() > 1 && Navigation.IndexesData.IndexEntries[0].Columns[1].colname != "")
                {
                    retValue = Navigation.IndexesData.IndexEntries[0].Columns[1].Value;
                    retCol = Navigation.IndexesData.IndexEntries[0].Columns[1].colname;
                }
                if (Navigation.IndexesData.IndexEntries != null && Navigation.IndexesData.IndexEntries[0].Columns.Count() > 1 && Navigation.IndexesData.IndexEntries[0].Columns[0].Value != null
                    && Navigation.IndexesData.IndexEntries[0].Columns[0].Value != "" && (retValue != null)
                    && Navigation.IndexesData.DISTINCT != "*")
                {

                    //XReportWebIface.DocumentData Data = BuildDocumentData1(Navigation.IndexesData.IndexEntries[0].Columns[1].colname, Navigation.IndexesData.IndexEntries[0].Columns[0].colname, Navigation.IndexesData.IndexEntries[0].Columns[0].Value);
                    //Data.IndexName = Navigation.IndexesData.IndexName;
                    IEnumerable<string> listValues = getFilteredExample(Navigation.IndexesData.IndexEntries[0].Columns[1].colname, Navigation.IndexesData.IndexEntries[0].Columns[0].colname, retValue, Navigation.IndexesData.IndexEntries[0].Columns[1].Value);
                    //Navigation.IndexExampleHashtable.Add(dr["Field"].ToString(), IndexExampleListString);
                    Navigation.IndexSuggestionHash.Remove(Navigation.IndexesData.IndexEntries[0].Columns[1].colname);
                    Navigation.IndexSuggestionHash.Add(Navigation.IndexesData.IndexEntries[0].Columns[1].colname, new List<String>(listValues));
                    string IndexExampleListString = string.Join("\n", listValues.Take(200));
                    ((TextBox)Page.FindControl(Navigation.IndexesData.IndexEntries[0].Columns[1].colname)).ToolTip = "Example\n\n" + IndexExampleListString;
                    //Navigation.IndexExampleHashtable.Add(dr["Field"].ToString(), IndexExampleListString);
                    //((TextBox)Page.FindControl(Navigation.IndexesData.IndexEntries[0].Columns[1].colname)).ToolTip = "Example\n\n" + (string)Navigation.IndexSuggestionHash[Navigation.IndexesData.IndexEntries[0].Columns[1].colname];
                    //Array.Find(Navigation.IndexesData.IndexEntries[0].Columns, delegate(XReportWebIface.column c) { return c.colname == retCol; });
                    //Valori de reinserire nelle TextBox altrimenti si perdono nelle chiamate successive ai web-services 
                    if (Navigation.IndexesData.IndexEntries[0].Columns[0].colname == retCol)
                    {
                        Navigation.IndexesData.IndexEntries[0].Columns[0].Value = String.Copy(retValue);
                        ((TextBox)Page.FindControl(Navigation.IndexesData.IndexEntries[0].Columns[0].colname)).Text = String.Copy(retValue);

                    }
                    else if (Navigation.IndexesData.IndexEntries[0].Columns[1].colname == retCol)
                    {
                        Navigation.IndexesData.IndexEntries[0].Columns[1].Value = String.Copy(retValue);
                        ((TextBox)Page.FindControl(Navigation.IndexesData.IndexEntries[0].Columns[1].colname)).Text = String.Copy(retValue);
                    }
                }

                log.Info("LoadIndex Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadIndex Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                Navigation.IndexesData.DISTINCT = "SUM";
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadIndex Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                Navigation.IndexesData.DISTINCT = "SUM";
                throw Ex;
            }
        }

        private void LoadIndexValues(bool first)
        {
            try
            {
                log.Info("LoadIndexValues() Started");
                //XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface();
                //XReportWebIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                XReportWebIFace.Timeout = 60000;
                XReportWebIface.DocumentData Data = null;
                Data = XReportWebIFace.getIndexes(Navigation.IndexesData);
                log.Info("LoadIndexValues() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadIndexValues() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadIndexValues() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private bool BindIndexCombo()
        {
            DataTable dt = Utility.IndexEntriesToTable2(Navigation.IndexesData.IndexEntries);
            if (dt.Rows.Count > 1)
            {
                this.ddl_index.Visible = true;
                this.ddl_index.DataSource = dt;
                this.ddl_index.DataTextField = "IndexName";
                this.ddl_index.DataValueField = "IndexName";
                this.ddl_index.DataBind();
                this.ddl_index.Items.Insert(0, new ListItem("---Selezionare---", "-1"));
                this.ddl_index.SelectedIndex = -1;
                if (this.ddl_index.Items.Count == 2)
                {
                    this.ddl_index.SelectedIndex = 1;
                    Navigation.IndexesData.IndexName = this.ddl_index.SelectedItem.Text;
                    this.ddl_index.Enabled = false;
                    return true;
                }
                else
                    return false;
            }
            else
            {
                this.l_index.Visible = true;
                this.l_index.Text = dt.Rows[0]["IndexName"].ToString();
                Navigation.IndexesData.IndexName = this.l_index.Text;
                return true;
            }
        }


        private static IEnumerable<string> getFilteredExample(string contextKey, string columnWhere, string prefixText, string whereVal)
        {
            try
            {
                //XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface();
                //XReportWebIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                XReportWebIFace.Timeout = 60000;
                //XReportWebIface.AuthenticationValue;
                XReportWebIface.DocumentData Data = BuildDocumentData1(contextKey, columnWhere, prefixText, whereVal);
                Data.IndexName = Navigation.IndexesData.IndexName;
                IEnumerable<string> listValues = XReportWebIFace.GetIndexExample(Data).IndexEntries.Skip(1).Select(x => x.Columns[0].Value);
                if (listValues != null)
                    return listValues;
                else
                    return null;
            }

            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in GetIndexExample(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in GetIndexExample(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }

        }

        private static XReportWebIface.DocumentData BuildDocumentData1(string top, string columnWhere, string filVal, string whereVal)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();

                Entry.JobReportId = Navigation.ViewParameters.Split(';')[0];
                Entry.Columns = new XReportWebIface.column[0] { };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.TOP = top;

                if (filVal != "")
                {
                    //XReportWebIface.IndexEntry Entry2 = new XReportWebIface.IndexEntry();
                    List<XReportWebIface.column> columns = new List<XReportWebIface.column>();
                    //Entry2.Columns = new XReportWebIface.column[1] { new XReportWebIface.column()};
                    XReportWebIface.column col = new XReportWebIface.column();
                    col.colname = top;
                    if (whereVal != "")
                    {
                        XReportWebIface.column col1 = new XReportWebIface.column();
                        col1.colname = top;
                        col1.Value = whereVal;
                        col.colname = columnWhere;
                        columns.Add(col1);
                    }
                    else if (columnWhere != "")
                    {
                        col.colname = columnWhere;
                    }
                    col.Value = filVal;
                    columns.Add(col);
                    //Entry2.Columns = columns.ToArray();
                    Entry.Columns = columns.ToArray();
                    //Data.IndexEntries = new XReportWebIface.IndexEntry[2] { Entry, Entry2 };
                }
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };
                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private void FillDictionaries(DataTable source)
        {
            if (Navigation.CombosValues == null)
                Navigation.CombosValues = new Dictionary<string, string>();
            foreach (DataRow dr in source.Rows)
            {
                if (!Navigation.CombosValues.ContainsKey(dr["Field"].ToString()))
                    Navigation.CombosValues.Add(dr["Field"].ToString(), "");
            }
        }


        #endregion

        #region Dynamic Controls

        private void CreateDynamicControl(string name, string type, string primary)
        {
            List<HtmlTableRow> rows = this.BuildControl(name, type, primary);
            foreach (HtmlTableRow row in rows)
            {
                this.ControlsTable.Rows.Insert(this.ControlsTable.Rows.Count, row);
                if (Navigation.DynamicFormControls == null)
                    Navigation.DynamicFormControls = new List<HtmlTableRow>();
                Navigation.DynamicFormControls.Add(row);
            }
        }

        private void LoadDynamicControls()
        {
            if (Navigation.DynamicFormControls != null)
            {
                foreach (HtmlTableRow row in Navigation.DynamicFormControls)
                {
                    this.ControlsTable.Rows.Remove(row);
                    row.Dispose();
                }
                Navigation.DynamicFormControls.Clear();
            }

            foreach (DataRow dr in Navigation.Fields.Rows)
            {
                CreateDynamicControl(dr["Field"].ToString(), dr["Type"].ToString(), dr["Primary"].ToString());
                log.Info("Dynamic controls created");
            }
        }

        private List<HtmlTableRow> BuildControl(string name, string type, string primary)
        {
            List<HtmlTableRow> rows = new List<HtmlTableRow>();
            HtmlTableRow row = new HtmlTableRow();
            row.Attributes.Add("class", "data");
            HtmlTableCell cellControl = new HtmlTableCell();
            HtmlTableCell cellControlLabel = new HtmlTableCell();
            cellControl.Width = "300px";
             


            switch (type.ToLower())
            {
                case "135":
                    cellControlLabel.Controls.Add(Utility.CreateLabel(name, name));
                    CreateDate(ref cellControl, name, primary);
                    break;
                case "200":
                    cellControlLabel.Controls.Add(Utility.CreateLabel(name, name));
                    //cellControl.Controls.Add(CreateTextBox(name, false, primary));
                    TextBox tb = CreateTextBox(name, false, primary);
                    tb.Attributes.Add("autocomplete", "off");
                    cellControl.Controls.Add(tb);

 
                    if (primary.Equals("1"))
                    {
                        cellControl.Controls.Add(CreateAutoComplete("primary_" + name, name, "GetCompletionListMemory"));
                    }
                    else
                    {
                        cellControl.Controls.Add(CreateAutoComplete("primary_" + name, name, "GetCompletionListSecMemory"));
                    } 
                    break;
                case "button":
                    ImageButton b = CreateButton();
                    cellControl.Controls.Add(b);
                    cellControl.Align = "right";
                    break;
                default:
                    break;
            }
            row.Cells.Add(cellControlLabel);
            row.Cells.Add(cellControl);
            rows.Insert(0, row);
            return rows;
        }

        public static AjaxControlToolkit.AutoCompleteExtender CreateAutoComplete(string key, string name, string method)
        {
            AjaxControlToolkit.AutoCompleteExtender ace = new AjaxControlToolkit.AutoCompleteExtender();
            ace.ID = name + "_ace";
            ace.TargetControlID = name;
            ace.ServiceMethod = method; // "GetCompletionListMemory";
            ace.MinimumPrefixLength = 1;
            ace.CompletionSetCount = 10;
            ace.EnableCaching = false;
            ace.CompletionInterval = 100;
            ace.FirstRowSelected = false;

            ace.CompletionListCssClass = "completionList";
            ace.CompletionListHighlightedItemCssClass = "itemHighlighted";
            ace.CompletionListItemCssClass = "listItem";
            ace.ContextKey = name;
            return ace;
        }

        private ImageButton CreateButton()
        {
            ImageButton b = new ImageButton();
            b.ID = "b_doc";
            b.Attributes.Add("runat", "server");
            b.CommandName = "ViewFile";
            b.ImageUrl = "~/Images/icona_pdf.png";
            b.ToolTip = "View File";
            b.Click += new ImageClickEventHandler(b_doc_Click);
            b.Width = new Unit(20, UnitType.Pixel);
            b.Height = new Unit(20, UnitType.Pixel);
            b.Visible = true;
            return b;
        }

        private TextBox CreateTextBox(string name, bool isDate, string primary)
        {
            TextBox tb = new TextBox();
            tb.ID = name;
            tb.ViewStateMode = System.Web.UI.ViewStateMode.Enabled;
            if (isDate)
                if (primary == "1")
                    tb.SkinID = "TextboxDateMan";
                else
                    tb.SkinID = "TextboxDate";
            else
                if (primary == "1")
                    tb.SkinID = "TextboxMediumMan";
                else
                    tb.SkinID = "TextboxMedium";
            //if (primary == "1")
            //    tb.ToolTip = "Example\n\n" + Navigation.IndexExample;
            if (Navigation.IndexExampleHashtable.ContainsKey(tb.ID))
            {
                tb.ToolTip = "Example\n\n" + (string)Navigation.IndexExampleHashtable[tb.ID];
            }
            tb.AutoPostBack = true;

            tb.TextChanged += new EventHandler(TextBoxIndex_TextChanged);
            return tb;
        }

        private void CreateDate(ref HtmlTableCell cellControl, string name, string primary)
        {
            TextBox tb = CreateTextBox(name, true, primary);
            cellControl.Controls.Add(tb);
            Label lb = new Label();
            lb.Width = new Unit(6, UnitType.Pixel);
            cellControl.Controls.Add(lb);
            ImageButton cal = Utility.CreateCalendarButton(name);
            cellControl.Controls.Add(cal);
            cellControl.Controls.Add(Utility.CreateCalendar(name, cal.ID));
            AjaxControlToolkit.MaskedEditExtender mask = Utility.CreateDataMask(name);
            cellControl.Controls.Add(mask);
            cellControl.Controls.Add(Utility.CreateDataValidator(name, mask.ID));
        }

        private void HookOnFocus(Control CurrentControl)
        {
            if ((CurrentControl is TextBox) ||
                (CurrentControl is DropDownList) ||
                (CurrentControl is ListBox) ||
                (CurrentControl is Button))
                //adds a script which saves active control on receiving focus 
                //in the hidden field __LASTFOCUS.
                (CurrentControl as WebControl).Attributes.Add("onfocus", "try{document.getElementById('__LASTFOCUS').value=this.id} catch(e) {}");
            //checks if the control has children and apply recursion
            if (CurrentControl.HasControls())
                foreach (Control CurrentChildControl in CurrentControl.Controls)
                    HookOnFocus(CurrentChildControl);
        }
        #endregion

        #region Events

        protected void ddl_index_SelectedIndexChanged(object sender, EventArgs e)
        {
            Navigation.IndexesData.IndexName = this.ddl_index.SelectedItem.Text;
            this.ddl_index.Enabled = false;
        }

        protected void TextBoxIndex_TextChanged(object sender, System.EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            bool callIndex = false;
            if (tb.Text != "__/__/____")
            {
                foreach (DataRow dr in Navigation.Fields.Rows)
                {
                    if (tb.ID == dr["Field"].ToString())
                    {
                        if (!dr["Value"].Equals(tb.Text))
                        {
                            dr["Value"] = tb.Text;
                            callIndex = true;
                        }
                        break;
                    }
                }
                if (callIndex)
                    LoadIndex();
            }
        }

        protected void b_doc_Click(object sender, ImageClickEventArgs e)
        {
            string listPag = "";
            string jrid = "";
            Navigation.IndexesData.DISTINCT = "*";
            LoadIndex();
            foreach (XReportWebIface.IndexEntry entry in Navigation.IndexesDataLast.IndexEntries)
            {
                jrid = entry.JobReportId;
                listPag += entry.FromPage + "," + entry.ForPages + ",";
            }
            if (listPag != "")
            {
                listPag = jrid + "," + listPag;
                listPag = listPag.Remove(listPag.Length - 1);
            }
            Navigation.ViewParameters = jrid + ";" + listPag;
            Navigation.IndexesData.DISTINCT = "SUM";
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFile.aspx?" + SHA.GenTimeSHA256String() + "');", true);
        }

        protected void lb_doc_Click(object sender, EventArgs e)
        {
            string listPag = "";
            string jrid = "";
            Navigation.IndexesData.DISTINCT = "*";
            Navigation.IndexesData.TOP = "-1";
            LoadIndex();
            foreach (XReportWebIface.IndexEntry entry in Navigation.IndexesDataLast.IndexEntries)
            {
                jrid = entry.JobReportId;
                listPag += entry.FromPage + "," + entry.ForPages + ",";
            }
            if (listPag != "")
            {
                listPag = jrid + "," + listPag;
                listPag = listPag.Remove(listPag.Length - 1);
            }
            Navigation.ViewParameters2 = "PDF";
            Navigation.ViewParameters = jrid + ";" + listPag;
            Navigation.IndexesData.DISTINCT = "SUM";
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFile.aspx?" + SHA.GenTimeSHA256String() + "');", true);
        }

        protected void t_doc_Click(object sender, EventArgs e)
        {
            string listPag = "";
            string jrid = ""; string rid = "";
            Navigation.IndexesData.DISTINCT = "*";
            Navigation.IndexesData.TOP = "-1";
            Navigation.IndexesData.ORDERBY = "FromPage, ForPages";
            LoadIndex();
            int lastFromP = 0; int lastForP = 0; int totPages = 0;
            foreach (XReportWebIface.IndexEntry entry in Navigation.IndexesDataLast.IndexEntries)
            {
                jrid = entry.JobReportId;
                rid = "0";
                if (lastFromP == 0)
                    lastFromP = Convert.ToInt32(entry.FromPage);
                if (Convert.ToInt32(entry.FromPage) != lastFromP + lastForP)
                {
                    listPag += lastFromP + "," + lastForP + ",";
                    lastFromP = Convert.ToInt32(entry.FromPage); lastForP = Convert.ToInt32(entry.ForPages);
                }
                else
                {
                    lastForP += Convert.ToInt32(entry.ForPages);

                }

                //listPag += entry.FromPage + "," + entry.ForPages + ",";
            }
            totPages = lastForP;
            listPag += lastFromP + "," + lastForP + ",";
            if (listPag != "")
            {
                //listPag = jrid + "," + listPag;
                listPag = listPag.Remove(listPag.Length - 1);
                Navigation.ViewParameters = jrid + ";" + rid + ";" + listPag + ";" + lastFromP + ";" + totPages;
            }
            Navigation.ViewParameters2 = "TXT";
            Navigation.IndexesData.DISTINCT = "SUM";
            Navigation.IndexesData.ORDERBY = "";
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFile.aspx?" + SHA.GenTimeSHA256String() + "');", true);
        }
        #endregion

        #region WebMethods
        [WebMethod]
        public static string[] GetCompletionListMemory(string prefixText, int count, string contextKey)
        {

            List<string> suggList = null;
            if (Navigation.IndexSuggestionHash.ContainsKey(contextKey))
            {
                suggList = ((List<string>)Navigation.IndexSuggestionHash[contextKey]).Where(p => (p.StartsWith(prefixText))).ToList();
                //suggList.Count();
            }


            if (prefixText.Length > 3 || (prefixText.Length > 1 && suggList != null && suggList.Count() < 100))
            {
                if (Navigation.IndexSuggestionHash.ContainsKey(prefixText + contextKey))
                {
                    suggList = ((List<string>)Navigation.IndexSuggestionHash[prefixText + contextKey]).Where(p => (p.StartsWith(prefixText))).ToList();
                    return suggList.ToArray();
                }
                else
                {

                    IEnumerable<string> listValues = getFilteredExample(contextKey, "", prefixText, "");
                    if (listValues != null)
                    {
                        Navigation.IndexSuggestionHash.Add(prefixText + contextKey, new List<String>(listValues));
                        suggList = ((List<string>)Navigation.IndexSuggestionHash[prefixText + contextKey])
                            //.Where(p => (p.StartsWith(prefixText)))
                            .ToList();
                        return suggList.ToArray();
                    }
                    else
                    {
                        return null;
                    }

                }
            }

            //if (Navigation.IndexSuggestionHash.ContainsKey(contextKey))
            //{
            //    List<string> suggList = ((List<string>)Navigation.IndexSuggestionHash[contextKey]).Where(p => (p.StartsWith(prefixText))).ToList();
            //    return suggList.ToArray();
            //}
            if (suggList != null)
            {
                return suggList.ToArray();
            }
            else
            {
                return null;
            }
        }
        [WebMethod]
        public static string[] GetCompletionListSecMemory(string prefixText, int count, string contextKey)
        {

            List<string> suggList = null;
            if (Navigation.IndexSuggestionHash.ContainsKey(contextKey))
            {
                suggList = ((List<string>)Navigation.IndexSuggestionHash[contextKey]).Where(p => (p.StartsWith(prefixText))).ToList();
                //suggList.Count();
            }


            if (prefixText.Length > 3 || (prefixText.Length > 1 && suggList != null && suggList.Count() < 100))
            {
                if (Navigation.IndexSuggestionHash.ContainsKey(prefixText + contextKey))
                {
                    suggList = ((List<string>)Navigation.IndexSuggestionHash[prefixText + contextKey]).Where(p => (p.StartsWith(prefixText))).ToList();
                    return suggList.ToArray();
                }
                else
                {
                    IEnumerable<string> listValues;
                    if (Navigation.IndexesData.IndexEntries[0].Columns.Count() > 1 && Navigation.IndexesData.IndexEntries[0].Columns[0].Value != null && Navigation.IndexesData.IndexEntries[0].Columns[0].Value != ""
                    && Navigation.IndexesData.IndexEntries[0].Columns[1].colname != null && Navigation.IndexesData.IndexEntries[0].Columns[1].colname != ""

                    )
                    {
                        listValues = getFilteredExample(contextKey, Navigation.IndexesData.IndexEntries[0].Columns[0].colname, Navigation.IndexesData.IndexEntries[0].Columns[0].Value, prefixText);
                    }
                    else
                    {
                        listValues = getFilteredExample(contextKey, "", prefixText, "");

                    }
                    if (listValues != null)
                    {
                        Navigation.IndexSuggestionHash.Add(prefixText + contextKey, new List<String>(listValues));
                        suggList = ((List<string>)Navigation.IndexSuggestionHash[prefixText + contextKey])
                            //.Where(p => (p.StartsWith(prefixText)))
                            .ToList();
                        return suggList.ToArray();
                    }
                    else
                    {
                        return null;
                    }

                }
            }

            //if (Navigation.IndexSuggestionHash.ContainsKey(contextKey))
            //{
            //    List<string> suggList = ((List<string>)Navigation.IndexSuggestionHash[contextKey]).Where(p => (p.StartsWith(prefixText))).ToList();
            //    return suggList.ToArray();
            //}
            if (suggList != null)
            {
                return suggList.ToArray();
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}