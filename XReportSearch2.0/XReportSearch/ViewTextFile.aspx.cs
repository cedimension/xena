﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Configuration;
using System.Web.Services.Protocols;
using XReportSearch.Utils;
using System.Data;
using System.Globalization;
using System.Resources;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Xml;
using System.Web.Services;
using System.Text;
using System.Data.SqlClient;

namespace XReportSearch
{
    public partial class ViewTextFile : System.Web.UI.Page
    {
        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Methods
        public static string _noteswarninguser {get;set;}
        public static string _checknotsucc { get; set; }
        public static string _sesstimeout { get; set; }
        public static string _rightsaved { get; set; }
        public static string _wrongsaved { get; set; }


        protected override void InitializeCulture()
        {
            if (Navigation.CurrentCulture != null)
            {
                UICulture = Navigation.CurrentCulture;
                Culture = Navigation.CurrentCulture;
            }
            base.InitializeCulture();

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            _noteswarninguser = GetLocalResourceObject("noteswarninguser").ToString();
            _checknotsucc =  GetLocalResourceObject("checknotsucc").ToString();
            _sesstimeout = GetLocalResourceObject("checknotsucc").ToString();
            _rightsaved = GetLocalResourceObject("rightsaved").ToString();
            _wrongsaved = GetLocalResourceObject("wrongsaved").ToString();
        }
        #endregion

        [Serializable]
        public class Item
        {
            public String type { get; set; }
            public String name { get; set; }
            public int page { get; set; }
            public String user { get; set; }
            public String date { get; set; }
            public List<int> coordinates { get; set; }
            public String msgName { get; set; }
            public String message { get; set; }
        }

        [Serializable]
        public class NotesResponse
        {
            public String[] report { get; set; }
            public List<Item> notes { get; set; }
        }

        private static void createNode(string itemType, string name, string page, string user,
                                            string date, string x, string y, string nName, string note, XmlWriter writer)
        {
            writer.WriteStartElement(itemType);
            writer.WriteStartElement("Name");
            writer.WriteString(name);
            writer.WriteEndElement();
            writer.WriteStartElement("Page");
            writer.WriteString(page);
            writer.WriteEndElement();
            writer.WriteStartElement("User");
            writer.WriteString(user);
            writer.WriteEndElement();
            writer.WriteStartElement("Date");
            writer.WriteString(date);
            writer.WriteEndElement();
            writer.WriteStartElement("Coordinates");
            writer.WriteStartElement("Xpos");
            writer.WriteString(x);
            writer.WriteEndElement();
            writer.WriteStartElement("Ypos");
            writer.WriteString(y);
            writer.WriteEndElement();
            writer.WriteEndElement();

            if (itemType == "note")
            {
                writer.WriteStartElement("NoteName");
                writer.WriteString(nName);
                writer.WriteEndElement();
                writer.WriteStartElement("Note");
                writer.WriteString(note);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        private static string CheckMode(string JobReportID, string ReportID, string numpage)
        {
            const int minutetocheck = 5;
            string cUser = null;
            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["SQL_CONNECTION"]))
            {
                //SqlDataAdapter da = new SqlDataAdapter("SELECT ImgXmlFile, ChangeMode FROM [tbl_LogicalRep_TextNotes] WHERE JobReportId = @jobReportid, ReportId = @reportid,Numpage= @numpage", conn);
                //SqlCommandBuilder cd = new SqlCommandBuilder(da);
                //DataSet ds = new DataSet();
                //da.Fill(ds);
                using (var sqlComm = new SqlCommand("SELECT  [ChangeMode], [ChangeUser],[ChangeTime], [UpdDateTime] FROM [tbl_LogicalRep_TextNotes] WHERE JobReportId = @jobReportid and ReportId = @reportid and Numpage= @numpage and (dateadd( mi, -" + minutetocheck + ", getDate()) < ChangeTime or dateadd( mi, -" + minutetocheck + ", getDate()) < UpdDateTime)", conn))
                {                                                         
                    conn.Open();                                        
                    sqlComm.Parameters.AddWithValue("@jobReportid", JobReportID);
                    sqlComm.Parameters.AddWithValue("@reportid", ReportID);
                    sqlComm.Parameters.AddWithValue("@numpage", numpage);
                    sqlComm.CommandTimeout = 120; //time out a 120 secondi 

                    SqlDataReader result = sqlComm.ExecuteReader();
                    while (result.Read())
                    {
                        object temp ;
                        string cMode = result["ChangeMode"].ToString();
                        cUser = result["ChangeUser"].ToString();
                        DateTime chgTime = (DBNull.Value != (temp = result["ChangeTime"]) ? (DateTime)result["ChangeTime"] : new DateTime());
                        DateTime updTime = (DBNull.Value != (temp = result["UpdDateTime"]) ? (DateTime)result["UpdDateTime"] : new DateTime());
                    }
                }
            }
            return cUser;
        }

        private static bool updateChangeMode(string JobReportID, string ReportID, string numpage)
        {
                try
                {
                    string user = Navigation.User.Username;
                    string startSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture); string logParms = "";
                    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["SQL_CONNECTION"]);
                    conn.Open();
                    //string storedName = "SaveToDB";const int NowRowsAffected = 0;
                    int retvalue = 0;
                    const string storedUpdateSql = @"BEGIN IF NOT EXISTS (select 1 from tbl_LogicalRep_TextNotes where JobReportid = @jobReportid and  ReportId = @reportid and  Numpage = @numpage ) " +
                        " INSERT INTO  tbl_LogicalRep_TextNotes (JobReportId, ReportId, Numpage, FolderPath, [User],  ImgXmlFile, ChangeMode, ChangeUser, ChangeTime ) VALUES ( @jobReportid ,@reportid , @numpage , '' , @chUser,Convert(varbinary,''), 1, @chUser , getDate())  " +
                        " ELSE UPDATE tbl_LogicalRep_TextNotes set ChangeTime = getDate() , ChangeUser = @chUser , ChangeMode = 1 where JobReportId = @jobReportid and  ReportId = @reportid and  Numpage = @numpage END";
                    //const string storedIntsertedSql = @"INSERT INTO tbl_LogicalRep_TextNotes (JobReportId, ReportId,Numpage,FolderPath,[User],ImgXmlFile) VALUES(@jobReportid, @reportid, @numpage,@FolderPath, @user, @Xml)";
                    using (SqlTransaction sqlTrans = conn.BeginTransaction())
                    {
                        try
                        {
                            using (SqlCommand sqlComm = new SqlCommand(storedUpdateSql, conn))
                            {
                                sqlComm.Transaction = sqlTrans;
                                sqlComm.Parameters.AddWithValue("@chUser", user);
                                logParms += "user == " + user;
                                sqlComm.Parameters.AddWithValue("@jobReportid", JobReportID);
                                logParms += "JobReportID == " + JobReportID;
                                sqlComm.Parameters.AddWithValue("@reportid", ReportID);
                                logParms += "ReportID == " + ReportID;
                                sqlComm.Parameters.AddWithValue("@numpage", numpage);
                                logParms += "numpage == " + numpage.ToString();
                                sqlComm.CommandTimeout = 120; //time out a 120 secondi 
                                retvalue = sqlComm.ExecuteNonQuery();
                            }
                            sqlTrans.Commit();
                        }
                        catch (Exception Ex)
                        {
                            log.Error("updateChangeMode() Failed: " + Ex.Message + " JobReportid" + JobReportID);
                            sqlTrans.Rollback();
                        }
                    }
                    conn.Close();
                    log.Debug("updateChangeMode Succefful JobReportid" + JobReportID);
                    string endSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                    //logParms += "ResSize == " + rowSize;
                    log.Debug("updateChangeMode_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSP + "_" + logParms + " 80 - 127.0.0.1 Mozilla/4.0+(compatible;+MSIE+6.0;+MS+Web+Services+Client+Protocol+4.0.30319.1) 200 0 0 ");
                    return true;
                    //log.Info("LoadReports_SP() Finished");
                }
                catch (Exception Ex)
                {

                    log.Error("updateChangeMode() Failed: " + Ex.Message + " " + JobReportID);
                    Navigation.Error = Ex.Message;
                    return false;
                    //return null;
                    //throw Ex;
                }
        }

        private static byte[] GetXmlData(string JobReportID, string ReportID, string numpage)
        {

            byte[] xmlImg = null;
            using (var conn = new SqlConnection(ConfigurationManager.AppSettings["SQL_CONNECTION"]))
            {
                //SqlDataAdapter da = new SqlDataAdapter("SELECT ImgXmlFile, ChangeMode FROM [tbl_LogicalRep_TextNotes] WHERE JobReportId = @jobReportid, ReportId = @reportid,Numpage= @numpage", conn);
                //SqlCommandBuilder cd = new SqlCommandBuilder(da);
                //DataSet ds = new DataSet();
                //da.Fill(ds);
                using (var sqlComm = new SqlCommand("SELECT ImgXmlFile, ChangeMode FROM [tbl_LogicalRep_TextNotes] WHERE JobReportId = @jobReportid and ReportId = @reportid and Numpage= @numpage", conn))
                {
                    conn.Open();
                    sqlComm.Parameters.AddWithValue("@jobReportid", JobReportID);

                    sqlComm.Parameters.AddWithValue("@reportid", ReportID);

                    sqlComm.Parameters.AddWithValue("@numpage", numpage);

                    sqlComm.CommandTimeout = 120; //time out a 120 secondi 

                    SqlDataReader result = sqlComm.ExecuteReader();
                    while (result.Read())
                    {
                        xmlImg = (byte[])result["ImgXmlFile"];
                        string cMode = result["ChangeMode"].ToString();
                    }
                }
            }
            return xmlImg;
        }

        private static List<Item> getReportItems(string JobReportID, string ReportID, string numpage)
        {
            List<Item> items = new List<Item>();
            XmlDocument finaldoc = new XmlDocument();
            byte[] bytes = GetXmlData(JobReportID, ReportID, numpage);
            if (bytes != null && bytes.LongLength > 0) { 
                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                
                //System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                //string decoded = new string(encoding.GetChars(bytes));
                //using (XmlReader reader = new XmlTextReader(new System.IO.StringReader(decoded)))

                /**reading directly from stream let step forward problem with encoding of stream object in C#; remain only Econding of Xml itself( in this case UTF-8) **/
                MemoryStream ms = new MemoryStream(bytes);
                using(XmlReader reader = new XmlTextReader(ms))
                {
                    List<int> coordinates = new List<int>();
                    while (reader.Read())
                    {
                        if (reader.IsStartElement())
                        {
                            switch (reader.Name.ToString())
                            {
                                case "Name":
                                    items[items.Count - 1].name = reader.ReadString();
                                    break;

                                case "Page":
                                    items[items.Count - 1].page = Int32.Parse(reader.ReadString());
                                    break;

                                case "User":
                                    items[items.Count - 1].user = reader.ReadString();
                                    break;

                                case "Date":
                                    items[items.Count - 1].date = reader.ReadString();
                                    break;

                                case "Xpos":
                                    if (items[items.Count - 1].coordinates == null)
                                    {
                                        items[items.Count - 1].coordinates = new List<int>();
                                        items[items.Count - 1].coordinates.Add(Int32.Parse(reader.ReadString()));
                                    }
                                    break;

                                case "Ypos":
                                    if (items[items.Count - 1].coordinates != null)
                                    {
                                        items[items.Count - 1].coordinates.Add(Int32.Parse(reader.ReadString()));
                                    }
                                    break;

                                case "NoteName":
                                    items[items.Count - 1].msgName = reader.ReadString();
                                    break;

                                case "Note":
                                    items[items.Count - 1].message = reader.ReadString();
                                    break;

                                case "note":
                                    items.Add(new Item());
                                    items[items.Count - 1].type = "note";
                                    break;

                                case "accepted":
                                    items.Add(new Item());
                                    items[items.Count - 1].type = "accepted";
                                    break;

                                case "cancelled":
                                    items.Add(new Item());
                                    items[items.Count - 1].type = "cancelled";
                                    break;

                                case "pending":
                                    items.Add(new Item());
                                    items[items.Count - 1].type = "pending";
                                    break;

                                case "unconfirmed":
                                    items.Add(new Item());
                                    items[items.Count - 1].type = "unconfirmed";
                                    break;
                            }
                        }
                    }
                }
            }
            return items;
        }

        [WebMethod(EnableSession=true)]
        public static string[] check(String numpage)
        {
            log.Info("check("+numpage+") Started");
            string currUser = Navigation.User.Username;
            if (Navigation.ViewParameters1 != null && Navigation.ViewParameters1 != ""){

                try
                {
                    string[] pars = Navigation.ViewParameters1.Split(';');
                    string JobReportID = pars[0]; string ReportID = pars[1]; string Pages = pars[2]; string FromPage = pars[3]; string totPages = pars[4];
                    int pagenum = Int32.Parse(numpage);
                    string user = CheckMode(JobReportID, ReportID, numpage);
                    updateChangeMode(JobReportID, ReportID, numpage);
                    if (user != null && !user.Equals(currUser))
                    {
                        return new string[] { string.Format(_noteswarninguser.ToString(),user) };
                        
                    }
                    else { return new string[] { "" }; }
                }
                catch (Exception Ex)
                {
                    log.Error("check() Failed: " + Ex.Message);
                    return new string[] { _checknotsucc };
                }
            }else{
                return new string[] { _sesstimeout };
                }
        }
                

        [WebMethod(EnableSession=true)]
        public static NotesResponse LoadReport(String numpage)
        {
            log.Info("LoadReport("+numpage+") Started");
            List<Item> xmlItems ; NotesResponse res ;
            if (Navigation.ViewParameters1 != null && Navigation.ViewParameters1 != ""){

                try{  
                    string[] pars = Navigation.ViewParameters1.Split(';');
                    string JobReportID = pars[0]; string ReportID = pars[1]; string Pages = pars[2]; string FromPage = pars[3]; string totPages = pars[4];
                    int pagenum = Int32.Parse(numpage);

                    
                    //XReportWebIface.xreportwebiface docIface = new XReportWebIface.xreportwebiface();
                    //docIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                    XReportWebIface.xreportwebiface docIface = new XReportWebIface.xreportwebiface(Navigation.User.Username, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                    docIface.Timeout = 600000;
                    Encoding iso_8859 = Encoding.GetEncoding("iso-8859-1");
                    Encoding utf_8 = Encoding.UTF8;
                    Encoding unicode = Encoding.Unicode;
                    docIface.RequestEncoding = iso_8859;

                    XReportWebIface.DocumentData Data;
                    
                    int rangePages = Int32.Parse(ConfigurationManager.AppSettings["RangePages"]); int totP =  Int32.Parse(totPages);
                    //add the page user is visualizing
                    //string newPages =  (Int32.Parse(Pages.Split(',')[0]) + pagenum-1).ToString() + ",1";
                    //FromPage = (Int32.Parse(FromPage) + pagenum-1).ToString(); 
                    //Data = ViewFile.BuildDocumentData(JobReportID, ReportID, Pages, FromPage);
                    //###########
                    int forpages = 100; int beforeRange = -1;
                    ///new implementation
                    if (pagenum > 100) {
                        forpages = 200;
                        beforeRange = -101;
                    }
                    //end new implementation
                    if (totP < (forpages + (pagenum + beforeRange + 1))) //instead of pagenum becomed (pagenum + beforeRange+1)
                    {
                        forpages = (totP - (pagenum + beforeRange)); //instead of pagenum +1  becomed (pagenum + beforeRange+1)
                    }
                    string newPages = (Int32.Parse(Pages.Split(',')[0]) + pagenum + beforeRange).ToString() + "," + forpages;
                    FromPage = (Int32.Parse(FromPage) + pagenum + beforeRange).ToString();
                    //###########  ---###***---###***---###***---###***
                    ////here begin cache management 
                    // if pagenum > 100 
                    //  forpages = 200
                    //  if( totP< totpages+ pagenum) 
                    //      forpages = (totp-pagenum+1) 
                    //    newPages = 
                    //     Frompage = 
                    //
                    string current_page;
                    if (Navigation.pageText == null)
                        Navigation.pageText = new Dictionary<Tuple<string, string, int>, string>();

                    if (Navigation.pageText.Count() > 2500) {
                        Navigation.pageText.Clear();
                    }
                    Tuple.Create(JobReportID, ReportID, pagenum);
                    if (!(Navigation.pageText.ContainsKey(Tuple.Create(JobReportID, ReportID,pagenum))))
                    {
                        //call web-service
                        //frompage = parametro  and fropages calcolato
                        Data = ViewFile.BuildDocumentData(JobReportID, ReportID, newPages, FromPage, "---###***---###***---###***---###***");
                        Data = docIface.getReportEntryTxt(Data);
                        //Encoding iso_8859 = Encoding.GetEncoding("iso-8859-1");

                        byte[] b_unicode = Encoding.Convert(iso_8859, unicode, Data.DocumentBody);
                        //byte[] b_utf8 = Encoding.Convert(iso_8859, utf_8, Data.DocumentBody);
                        string[] pages = unicode.GetString(b_unicode).Split(new string[] { "---###***---###***---###***---###***" }, StringSplitOptions.RemoveEmptyEntries);
                        for (int p = 0; p < forpages; p++)
                        {
                            if (!(Navigation.pageText.ContainsKey(Tuple.Create(JobReportID, ReportID, (pagenum + beforeRange+1) + p))))  
                                Navigation.pageText.Add(Tuple.Create(JobReportID, ReportID, (pagenum + beforeRange + 1) + p), pages[p]); //instead of pagenum becomed (pagenum + beforeRange+1)
                        }
                    }
                    current_page = Navigation.pageText[Tuple.Create(JobReportID, ReportID, pagenum)];

                    //here end chache management

                    //current_page = unicode.GetString(b_unicode);
                    //current_page = pages[0];
                    xmlItems = getReportItems(JobReportID, ReportID,numpage);
                    //Encoding iso_8859 = Encoding.GetEncoding("iso-8859-1");
                    //Encoding utf_8 = Encoding.UTF8;
                    //byte[] b_utf8 = Encoding.Convert(iso_8859, utf_8, Data.DocumentBody);
                    string[] lines = new string[] {  utf_8.GetString(Encoding.Convert(unicode, utf_8, unicode.GetBytes(current_page))) };
                    
                    res= new NotesResponse();
                    res.report = lines;
                    
                    res.notes = xmlItems;
                    log.Info("LoadReport() user:" + Navigation.User.Username + ";profile: " + Navigation.User.CodAuthor + ";report_name:" + Navigation.LogParameters[4] + ";report_data:" + Navigation.LogParameters[6] + ";job_name:" + Navigation.LogParameters[7] + ";job_number:" + Navigation.LogParameters[8] + ";folder_path:" + Navigation.LogParameters[9]);
                    
                    return res;
                    //HttpContext.Current.ApplicationInstance.CompleteRequest();
                    
                }
                catch (SoapException SoapEx)
                {
                    log.Error("LoadTxt() Failed: " + SoapEx.Message);
                    res = new NotesResponse();
                    res.report = new string[] { " Failed to load txt report " + SoapEx.Message };
                    return res;
                    
                }
                catch (Exception Ex)
                {
                    log.Error("LoadTxt() Failed: " + Ex.Message);
                    res = new NotesResponse();
                    res.report = new string[] {" Failed to load txt report " + Ex.Message};
                    return res;
                    //Navigation.Error = Ex.Message;
                    //HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
               }else{
                   res =  new NotesResponse();
                    //return new string[] {"empty session"};
                   res.report = new string[] { " session timed out" };
                   return res;
                }
        }

       

        [WebMethod(EnableSession = true)]
        public static List<Item> LoadFile(string numpage)
        {
            List<Item> items = new List<Item>();
            if (Navigation.ViewParameters1 != null && Navigation.ViewParameters1 != "")
            {
                string[] pars = Navigation.ViewParameters1.Split(';');
                string JobReportID = pars[0]; string ReportID = pars[1]; string Pages = pars[2]; string FromPage = pars[3]; string totPages = pars[4];
                XmlDocument finaldoc = new XmlDocument();
                byte[] bytes = GetXmlData(JobReportID, ReportID, numpage);
                //Select sui dati della image
                //string provaXml = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>" +
                //                    "<Items>" +
                //                      "<accepted>" +
                //                        "<Name>accepted1</Name>" +
                //                        "<Page>1</Page>" +
                //                        "<User>none</User>" +
                //                        "<Date>3/8/2018, 19:13</Date>" +
                //                        "<Coordinates>" +
                //                          "<Xpos>195</Xpos>" +
                //                         " <Ypos>39</Ypos>" +
                //                        "</Coordinates>" +
                //                      "</accepted></Items>";
                ////List<Item> items = new List<Item>();
                //System.Text.UTF32Encoding encoding = new System.Text.UTF32Encoding();
                //byte[] bytes = encoding.GetBytes(provaXml);
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                string decoded = new string(encoding.GetChars(bytes));
                //finaldoc.LoadXml(provaRicreato);

                //using (XmlTextReader reader = new XmlTextReader(new System.IO.StringReader(provaRicreato)))
                using (XmlReader reader = new XmlTextReader(new System.IO.StringReader(decoded)))
                {
                    List<int> coordinates = new List<int>();
                    while (reader.Read())
                    {
                        if (reader.IsStartElement())
                        {
                            switch (reader.Name.ToString())
                            {
                                case "Name":
                                    items[items.Count - 1].name = reader.ReadString();
                                    break;

                                case "Page":
                                    items[items.Count - 1].page = Int32.Parse(reader.ReadString());
                                    break;

                                case "User":
                                    items[items.Count - 1].user = reader.ReadString();
                                    break;

                                case "Date":
                                    items[items.Count - 1].date = reader.ReadString();
                                    break;

                                case "Xpos":
                                    if (items[items.Count - 1].coordinates == null)
                                    {
                                        items[items.Count - 1].coordinates = new List<int>();
                                        items[items.Count - 1].coordinates.Add(Int32.Parse(reader.ReadString()));
                                    }
                                    break;

                                case "Ypos":
                                    if (items[items.Count - 1].coordinates != null)
                                    {
                                        items[items.Count - 1].coordinates.Add(Int32.Parse(reader.ReadString()));
                                    }
                                    break;

                                case "NoteName":
                                    items[items.Count - 1].msgName = reader.ReadString();
                                    break;

                                case "Note":
                                    items[items.Count - 1].message = reader.ReadString();
                                    break;

                                case "note":
                                    items.Add(new Item());
                                    items[items.Count - 1].type = "note";
                                    break;

                                case "accepted":
                                    items.Add(new Item());
                                    items[items.Count - 1].type = "accepted";
                                    break;

                                case "cancelled":
                                    items.Add(new Item());
                                    items[items.Count - 1].type = "cancelled";
                                    break;

                                case "pending":
                                    items.Add(new Item());
                                    items[items.Count - 1].type = "pending";
                                    break;

                                case "unconfirmed":
                                    items.Add(new Item());
                                    items[items.Count - 1].type = "unconfirmed";
                                    break;
                            }
                        }
                    }
                }
                return items;
            }
            else {
                return items;
            }
        }

        [WebMethod(EnableSession = true)]
        public static string[] WriteToDb(List<Item> items, int numpage)
        {
            if (Navigation.ViewParameters1 != null && Navigation.ViewParameters1 != "")
            {
                MemoryStream stream = new MemoryStream();
                XmlWriterSettings settings = new XmlWriterSettings();
                //settings.Encoding = Encoding.ASCII;
                settings.Encoding = Encoding.UTF8;
                XmlWriter writer = XmlWriter.Create(stream,settings);
                //int numpage2 = 1;
                //MemoryStream stream = new MemoryStream();
                ////StreamWriter swriter = new StreamWriter(stream);
                //XmlWriter writer = new XmlWriter.Create(stream);
                //XmlTextWriter writer = new XmlTextWriter(@"C:\Users\Giuseppe\Desktop\test.xml", System.Text.Encoding.UTF8);
                if (items.Count() > 0) { 
                    writer.WriteStartDocument(true);
                    //writer.Formatting = Formatting.Indented;
                    //writer.Indentation = 2;
                    writer.WriteStartElement("Items");
                    for (int i = 0; i < items.Count; i++)
                    {
                        createNode(items[i].type, items[i].name, items[i].page.ToString(), items[i].user, items[i].date,
                            items[i].coordinates[0].ToString(), items[i].coordinates[1].ToString(), items[i].msgName, items[i].message, writer);
                    }

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                    writer.Flush();
                }
                stream.Position = 0;
                //StreamReader streamReader = new StreamReader(stream);

                //Response.Write(streamReader.ReadToEnd());
                byte[] tosave = stream.ToArray();
                writer.Close();
                stream.Close();

                string[] pars = Navigation.ViewParameters1.Split(';');
                string JobReportID = pars[0]; string ReportID = pars[1]; string Pages = pars[2]; string FromPage = pars[3]; string totPages = pars[4];
                try
                {

                    string startSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture); string logParms = "";
                    SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["SQL_CONNECTION"]);
                    conn.Open();
                    DataSet ds = new DataSet("Folders");
                    //string storedName = "SaveToDB";const int NowRowsAffected = 0;
                    int retvalue = 0;
                    const string storedDeletedSql = @"DELETE FROM tbl_LogicalRep_TextNotes where JobReportId = @jobReportid and  ReportId = @reportid and  Numpage = @numpage ";
                    const string storedIntsertedSql = @"INSERT INTO tbl_LogicalRep_TextNotes (JobReportId, ReportId,Numpage,FolderPath,[User],ChangeUser, ImgXmlFile) VALUES(@jobReportid, @reportid, @numpage,@FolderPath, @user, @user, @Xml)";
                    using (SqlTransaction sqlTrans = conn.BeginTransaction())
                        {   
                        try
                            {
                        
                            using (SqlCommand sqlComm = new SqlCommand(storedDeletedSql, conn))
                            {
                                //ero qui
                                sqlComm.Transaction = sqlTrans;
                                sqlComm.Parameters.AddWithValue("@jobReportid", JobReportID);
                                logParms += "JobReportID == " + JobReportID;
                                sqlComm.Parameters.AddWithValue("@reportid", ReportID);
                                logParms += "ReportID == " + ReportID;
                                sqlComm.Parameters.AddWithValue("@numpage", numpage);
                                logParms += "numpage == " + numpage.ToString();
                                sqlComm.CommandTimeout = 120; //time out a 120 secondi 
                                retvalue = sqlComm.ExecuteNonQuery();
                                sqlComm.Parameters.Clear();
                                sqlComm.CreateParameter();

                                sqlComm.Parameters.AddWithValue("@jobReportid", JobReportID);
                                logParms += "JobReportID == " + JobReportID;
                                sqlComm.Parameters.AddWithValue("@reportid", ReportID);
                                logParms += "ReportID == " + ReportID;
                                sqlComm.Parameters.AddWithValue("@numpage", numpage);
                                logParms += "numpage == " + numpage.ToString();
                                sqlComm.Parameters.AddWithValue("@user", Navigation.User.Username.ToUpperInvariant());
                                logParms += "user == " + Navigation.User.Username.ToUpperInvariant();
                                sqlComm.Parameters.AddWithValue("@FolderPath","PROVA/PROVA");
                                logParms += "folderpath == " + Navigation.User.Username.ToUpperInvariant();
                                sqlComm.Parameters.Add("@Xml", SqlDbType.VarBinary, -1).Value = tosave;
                                logParms += "xml == " + Navigation.User.Username.ToUpperInvariant();
                                //sqlComm.Parameters.Add("@updDateTime", SqlDbType.DateTime).Value = DateTime.UtcNow;
                                //logParms += "updDateTime == " + DateTime.UtcNow;
                                sqlComm.CommandText = storedIntsertedSql;
                                retvalue = sqlComm.ExecuteNonQuery();
                                //logParms.Remove(logParms.Length - 2, 2);
                                //sqlComm.CommandType = CommandType.StoredProcedure;
                            }
                            sqlTrans.Commit();
                        }catch (Exception Ex)
                        {
                            log.Error("SaveToDB() Failed: " + Ex.Message + " JobReportid" + JobReportID);
                            sqlTrans.Rollback();
                        } 
                        }
                        
                    
                   
                    conn.Close();
                    log.Debug("SaveToDB");
                    string endSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);

                    //logParms += "ResSize == " + rowSize;
                    log.Info("SaveToDB_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + Navigation.User.Username + "_" + endSP + "_" + logParms + " 80 - 127.0.0.1 Mozilla/4.0+(compatible;+MSIE+6.0;+MS+Web+Services+Client+Protocol+4.0.30319.1) 200 0 0 ");
                    string returnMsg = "";
                    if(retvalue.Equals(1)){
                        returnMsg = _rightsaved + numpage;
                    }else{
                        returnMsg = _wrongsaved + numpage;
                    }
                    
                    return new String[] { returnMsg };
                    //log.Debug("LoadReports_SP() Finished");
                }
                catch (Exception Ex)
                {
                    
                    log.Error("SaveToDB() Failed: " + Ex.Message + " " + JobReportID);
                    Navigation.Error = Ex.Message;
                    return new string[] { "LoadTxt() Failed: " + Ex.Message };
                    //return null;
                    //throw Ex;
                }
            }else
            {
                return new string[] { "empty session" };
            }
        }

        [System.Web.Services.WebMethod]
        
        public static void WriteToFileStream(List<Item> items)
        {

            MemoryStream stream = new MemoryStream();
            XmlWriter writer = XmlWriter.Create(stream);

            //MemoryStream stream = new MemoryStream();
            ////StreamWriter swriter = new StreamWriter(stream);
            //XmlWriter writer = new XmlWriter.Create(stream);
            //XmlTextWriter writer = new XmlTextWriter(@"C:\Users\Giuseppe\Desktop\test.xml", System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            //writer.Formatting = Formatting.Indented;
            //writer.Indentation = 2;
            writer.WriteStartElement("Items");
            for (int i = 0; i < items.Count; i++)
            {
                createNode(items[i].type, items[i].name, items[i].page.ToString(), items[i].user, items[i].date,
                    items[i].coordinates[0].ToString(), items[i].coordinates[1].ToString(), items[i].msgName, items[i].message, writer);
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Flush();
            stream.Position = 0;
            //StreamReader streamReader = new StreamReader(stream);

            //Response.Write(streamReader.ReadToEnd());
            byte[] tosave = stream.ToArray();
            writer.Close();
            stream.Close();
        }


        [System.Web.Services .WebMethod]
        public static void WriteToFileOld(string[] lines)
        {
            using (System.IO.StreamWriter file =
                            new System.IO.StreamWriter(@"C:\Users\EE08247\Desktop\WriteLines.txt"))
            {
                foreach (string line in lines)
                {
                    // If the line doesn't contain the word 'Second', write the line to the file.
                    if (!line.Contains("Second"))
                    {
                        file.WriteLine(line);
                    }
                }
            }
        }

        [WebMethod]
        public static void WriteToFile(List<Item> items)
        {
            XmlTextWriter writer = new XmlTextWriter(@"C:\Users\EE08247\Desktop\test.xml", System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;
            writer.WriteStartElement("Items");
            for (int i = 0; i < items.Count; i++)
            {
                createNode(items[i].type, items[i].name, items[i].page.ToString(), items[i].user, items[i].date,
                    items[i].coordinates[0].ToString(), items[i].coordinates[1].ToString(), items[i].msgName, items[i].message, writer);
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
        }
        [System.Web.Services.WebMethod]
        public static string[] ClearSessionData()
        {
            if (Navigation.ViewParameters1 != null && Navigation.ViewParameters1 != "")
            {

                try
                {
                    string[] pars = Navigation.ViewParameters1.Split(';');
                    string JobReportID = pars[0]; string ReportID = pars[1]; string Pages = pars[2]; string FromPage = pars[3]; string totPages = pars[4];
                    Navigation.pageText.Clear();
                    return new string[] { "session cleared" };
                }
                catch(Exception ex) {
                    return new string[] { ex.Message + " clearing session" };
                }
            }else
            {
                return new string[] { "session timeout" };
            }

        }
        

        [System.Web.Services.WebMethod]
        public static string[] LoadFile2()
        {
            string username = Navigation.User.Username;
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\EE08247\Desktop\WriteLines.txt");
            return lines;
        }

        [System.Web.Services.WebMethod]
        public static List<Item> LoadFileFS()
        {
            List<Item> items = new List<Item>();

            using (XmlReader reader = XmlReader.Create(@"C:\Users\EE08247\Desktop\test.xml"))
            {
                List<int> coordinates = new List<int>();
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name.ToString())
                        {
                            case "Name":
                                items[items.Count - 1].name = reader.ReadString();
                                break;

                            case "Page":
                                items[items.Count - 1].page = Int32.Parse(reader.ReadString());
                                break;

                            case "User":
                                items[items.Count - 1].user = reader.ReadString();
                                break;

                            case "Date":
                                items[items.Count - 1].date = reader.ReadString();
                                break;

                            case "Xpos":
                                if (items[items.Count - 1].coordinates == null)
                                {
                                    items[items.Count - 1].coordinates = new List<int>();
                                    items[items.Count - 1].coordinates.Add(Int32.Parse(reader.ReadString()));
                                }
                                break;

                            case "Ypos":
                                if (items[items.Count - 1].coordinates != null)
                                {
                                    items[items.Count - 1].coordinates.Add(Int32.Parse(reader.ReadString()));
                                }
                                break;

                            case "NoteName":
                                items[items.Count - 1].msgName = reader.ReadString();
                                break;

                            case "Note":
                                items[items.Count - 1].message = reader.ReadString();
                                break;

                            case "note":
                                items.Add(new Item());
                                items[items.Count - 1].type = "note";
                                break;

                            case "accepted":
                                items.Add(new Item());
                                items[items.Count - 1].type = "accepted";
                                break;

                            case "cancelled":
                                items.Add(new Item());
                                items[items.Count - 1].type = "cancelled";
                                break;

                            case "pending":
                                items.Add(new Item());
                                items[items.Count - 1].type = "pending";
                                break;

                            case "unconfirmed":
                                items.Add(new Item());
                                items[items.Count - 1].type = "unconfirmed";
                                break;
                        }
                    }
                }
            }

            return items;
        }
    }
}

   
       

