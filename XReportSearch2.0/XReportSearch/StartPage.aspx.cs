﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XReportSearch.Utils;
using XReportSearch.Business;
using System.Configuration;
using log4net;
using System.Web.Services.Protocols;
using System.Net;
using Microsoft.Web.Administration;
using System.Web.Services;
using PlainElastic.Net;
using PlainElastic.Net.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Globalization;
using System.Resources;
using System.ServiceModel.Channels;
using System.ServiceModel;

namespace XReportSearch
{
    public partial class StartPage : System.Web.UI.Page
    {

        #region Log
        private static readonly ILog log_ES = LogManager.GetLogger("ES_Log");
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Constants
        private const string AUSTRIAN_ENV = "BA";
        #endregion

        #region Methods

       protected override void InitializeCulture()
        {
            if (Request.Form["ddlLanguage"] != null)
            {
                UICulture = Request.Form["ddlLanguage"];
                Culture = Request.Form["ddlLanguage"];
            }
            //else {
            //    string browCulture = System.Threading.Thread.CurrentThread.CurrentUICulture.ToString();
            //    UICulture = browCulture;
            //    Culture = browCulture;
            //}
            base.InitializeCulture();
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                log.Info("*** SESSION_ID: " + Session.SessionID + " * IS_NEW: " + Session.IsNewSession.ToString() + " * " + Session["SessionID"] + " IS_COOKIELESS: " + Session.IsCookieless.ToString() + " * COOKIE_MODE: " + Session.CookieMode.ToString());
                Session["SessID"] = Session.SessionID;
                this.sp_hidden3.DataBind();
                SetUser();
                if (Request.Form["ddlLanguage"] == null) {
                    string browCulture = System.Threading.Thread.CurrentThread.CurrentUICulture.ToString();
                    this.ddlLanguage.Text = browCulture;
               }
                

                string ambiente;
                if (ConfigurationManager.AppSettings["ENV"] != null)
                {
                    ambiente = ConfigurationManager.AppSettings["ENV"].ToString().TrimEnd().ToUpper();
                }
                else
                {
                    ambiente = Request.Url.AbsoluteUri.Substring(9, 2).ToUpper();
                }
                log.Info("*** User set in session - User: " + Navigation.User + " ambiente: " + ambiente);

                if (!Page.IsPostBack && Navigation.User != null)
                {

                    //for log to be analyzed as W3SVC log analogy
                    log.Info("#Fields: date time cs-level cs-method cs-uri-stem cs-uri-query s-port cs-username c-ip cs(User-Agent) sc-status sc-substatus sc-win32-status time-taken");
                    log.Debug("Page_Load '!Page.IsPostBack && Navigation.User != null'");
                    this.sp_panelTable2.Attributes.Add("style", "display:none");
                    string tabs = ConfigurationManager.AppSettings["ExcludedTabs"];
                    if (tabs != null && tabs != "")
                    {
                        List<MenuItem> items = new List<MenuItem>();
                        string[] values = tabs.Split(',');
                        foreach (string v in values)
                        {
                            if (v == "0")
                                this.sp_b_Tab1.Visible = false;
                            else if (v == "1")
                                this.sp_b_Tab2.Visible = false;
                        }
                    }
                    //PG_UFFICIO_INQ
                    //list of value of UFFICIO_INQ authorized to view hyhistory reports
                    log.Debug("Page_Load '!Page.IsPostBack CodAut" + Navigation.User.CodAuthor + " Og Navigation.User.OrganizationUnit " + Navigation.User.OrganizationUnit);
                    Navigation.User.CodAuthor = Navigation.User.CodAuthor.TrimEnd();
                    Navigation.User.OrganizationUnit = Navigation.User.OrganizationUnit.TrimEnd();

                    if ((!Navigation.User.CodAuthor.Equals("X1NADM", StringComparison.OrdinalIgnoreCase)) && (this.sp_b_Tab2.Visible == true))
                    {
                        string historyAuthList = ConfigurationManager.AppSettings["HistoryAuthList"];
                        if (historyAuthList != null && (!historyAuthList.TrimEnd().Equals("")))
                        {
                            log.Debug("Page_Load '!Page.IsPostBack inside authHistList");
                            bool found = false;
                            string[] values = historyAuthList.TrimEnd().Split(',');
                            foreach (string v in values)
                            {
                                if (v.Trim().Equals(Navigation.User.OrganizationUnit, StringComparison.OrdinalIgnoreCase))
                                {
                                    log.Info("UFFICIO_INQ of User(" + Navigation.User.Username + ") found in historyAuthList:" + Navigation.User.OrganizationUnit);
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                            {
                                log.Info("UFFICIO_INQ of User(" + Navigation.User.Username + ") not found in historyAuthList(" + Navigation.User.OrganizationUnit + "):(" + historyAuthList + ")");
                                this.sp_b_Tab2.Visible = false;
                            }

                        }

                    }
                    
                    if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.UNICREDIT.ToString())
                    {
                        //this.sp_lb_Ambiente.Text = "Environment: " + ambiente; 
                        this.sp_lb_Ambiente.Text = GetLocalResourceObject("env").ToString()+ ": "+ ambiente;
                        //this.sp_lb_User.Text = "User: " + Navigation.User.Username;
                        this.sp_lb_User.Text = GetLocalResourceObject("user").ToString() + ": " + Navigation.User.Username;
                        //this.sp_lb_Profilo.Text = "Profile: " + Navigation.User.Profilo;
                        //this.sp_lb_Profilo.Text = "Profile: " + Navigation.User.CodAuthor;
                        this.sp_lb_Profilo.Text = GetLocalResourceObject("profile").ToString() + ": " + Navigation.User.CodAuthor;
                        //this.sp_lb_Profilo2.Text = "Branch: " + Navigation.User.SportCont;
                        this.sp_lb_Profilo2.Text = GetLocalResourceObject("branch").ToString() + ": " + Navigation.User.SportCont;
                        //this.sp_lb_OrganizationUnit.Text = "Organization Unit: " + Navigation.User.OrganizationUnit;
                        this.sp_lb_OrganizationUnit.Text = GetLocalResourceObject("orgunit").ToString() + ": " + Navigation.User.OrganizationUnit;
                        //<asp:Label ID="Label4" Width="20px" runat="server"></asp:Label>
                        //<asp:Label ID="sp_lb_Tree" runat="server" SkinID="LabelAuto"></asp:Label>
                        //insert into aspx page this   
                        //this.sp_lb_Tree.Text = GetLocalResourceObject("tree").ToString() + ": " + "START";
                        if (Navigation.User.isAustrian()) {
                            string currenturl = Request.Url.AbsoluteUri;
                            //behind a load balancer AbsoluteUri return balancer call uri ; in this mode we reconstruct original client uri
                            HttpRequestMessageProperty httpRequestProperty = null;
                            object propertyValue = null;
                            string[] headers = HttpContext.Current.Request.Headers.AllKeys;
                            if (HttpContext.Current.Request.Headers["Host"] != null)
                            {
                                currenturl = (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]!= null ? "https://" : "http://") + HttpContext.Current.Request.Headers["Host"];
                            }
                            //if (OperationContext.Current.IncomingMessageProperties.TryGetValue(HttpRequestMessageProperty.Name, out propertyValue))
                            //{
                            //    httpRequestProperty = (HttpRequestMessageProperty)propertyValue;
                            //    currenturl = httpRequestProperty.Headers["Host"];
                            //}
                            log.Info("currenturl : " + currenturl);
                            //this.linkmanual.NavigateUrl = currenturl.Substring(0,currenturl.IndexOf('/',10))+'/'+"Xena_usermanual.pdf";
                            this.linkmanual.NavigateUrl = currenturl + '/' + "Xena_usermanual.pdf";
                            this.linkmanual.ToolTip = GetLocalResourceObject("downmanual").ToString();
                            
                        }
                    }
                    log.Debug("Page_Load '!Page.IsPostBack after Label");
                    if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.VITTORIA.ToString() || ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.GENERALI.ToString())
                    {
                        this.sp_lb_User.Text = "User: " + Navigation.User.Username;
                    }
                }
                if (Navigation.User == null)
                {
                    log.Debug("Page_Load 'Navigation.User == null'");
                    Session.Clear();
                    string js = "alert(\"Session ended: errors could occur. Please close this window and reopen the application.\"); window.opener.focus(); window.close();";
                    ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", js, true);
                    //Navigation.Error = "Session ended: errors could occur. Please close this window and reopen the application";
                    //Response.Redirect("~/Error.aspx?disableReload=Y&enableClose=Y"); 
                }
            }
            catch (Exception ex)
            {
                log.Info("Page_Load Err " + ex.Message);
                //string js = "alert(\" " + ex.Message + "\nPlease close this window and reopen the application.\"); window.opener.focus(); window.close();";
                //log.Debug("Page_Load Err string js:" + js);
                //Session.Clear();
                //ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", js, true);
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "myscr", js, true);
                Navigation.Error = "An error occurred:" + ex.Message;
                //HttpContext.Current.Application.set
                Response.Redirect("~/Error.aspx?disableReload=Y&enableClose=Y");
            }
        }

        private void SetUser()
        {
            log.Info("SetUser()");
            if (ConfigurationManager.AppSettings["DEBUG"].ToString() == "1")
            {
                
                log.Info("SetUser() in debug mode");
                if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.VITTORIA.ToString() || ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.GENERALI.ToString())
                {
                    Navigation.User = new User("xreport", "", "xreport", "", "");
                    Navigation.User.IsRoot = true;
                }
                else
                {
                    //Navigation.User = new User("xreport", "", "X1NADM", "", "X1NADM", "N/A", "N/A"); 
                    string DEBUG_Username = (ConfigurationManager.AppSettings["DEBUG_Username"] != null ? ConfigurationManager.AppSettings["DEBUG_Username"] : "xreport");
                    string DEBUG_TokenString = (ConfigurationManager.AppSettings["DEBUG_TokenString"] != null ? ConfigurationManager.AppSettings["DEBUG_TokenString"] : "");
                    string DEBUG_Profilo = (ConfigurationManager.AppSettings["DEBUG_Profilo"] != null ? ConfigurationManager.AppSettings["DEBUG_Profilo"] : "X1NBAS");
                    string DEBUG_Torre = (ConfigurationManager.AppSettings["DEBUG_Torre"] != null ? ConfigurationManager.AppSettings["DEBUG_Torre"] : "");
                    string DEBUG_CodAuthor = (ConfigurationManager.AppSettings["DEBUG_CodAuthor"] != null ? ConfigurationManager.AppSettings["DEBUG_CodAuthor"] : "X1NBAS");
                    string DEBUG_SportCont = (ConfigurationManager.AppSettings["DEBUG_SportCont"] != null ? ConfigurationManager.AppSettings["DEBUG_SportCont"] : "N/A");
                    string DEBUG_OrganizationUnit = (ConfigurationManager.AppSettings["DEBUG_OrganizationUnit"] != null ? ConfigurationManager.AppSettings["DEBUG_OrganizationUnit"] : "N/A");
                    Navigation.User = new User(DEBUG_Username, DEBUG_TokenString, DEBUG_Profilo, DEBUG_Torre, DEBUG_CodAuthor, DEBUG_SportCont, DEBUG_OrganizationUnit);

                }


            }
            else
            {
                if (Navigation.User == null)
                {
                    log.Debug("SetUser() with User == null beginning check permission");
                    if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.VITTORIA.ToString() || ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.GENERALI.ToString())
                        Response.Redirect("~/Authentication/Login.aspx");
                    else if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.UNICREDIT.ToString())
                    {
                        try
                        {
                            TestPGE();
                        }
                        catch (Exception ex)
                        {
                            log.Error("Set User Err " + ex.Message);
                            throw ex;
                        }
                    }
                }
                else
                {
                    log.Debug("SetUser() - Navigation.User not null");


                    //TESTSAN
                    //if (Navigation.User.Profilo != null)
                    //    log.Debug("SetUser() - Navigation.User.Profilo:" + Navigation.User.Profilo);
                    //if (Request.Params["profilo"] != null && Request.Params["profilo"] != "")
                    //    log.Debug("SetUser() - Request.Params[\"profilo\"]:" + Request.Params["profilo"]);
                    //
                    //if (Navigation.User.Username.TrimEnd().Equals("EE24544", StringComparison.OrdinalIgnoreCase))
                    //{
                    //    if (Navigation.User.Profilo != null
                    //        && Request.Params["profilo"] != null
                    //        && !Navigation.User.Profilo.TrimEnd().Equals(Request.Params["profilo"],StringComparison.OrdinalIgnoreCase))
                    //    {
                    //        foreach(string parameter in Request.Params)
                    //        {
                    //            log.Debug("Request.Params["+parameter+"]: " + Request.Params[parameter]);
                    //        }
                    //        log.Debug("Navigation.User set to null");
                    //        Navigation.User = null;
                    //        TestPGE();
                    //    }
                    //}
                    //
                }
            }
        }

        private void TestPGE()
        {
            log.Debug("TestPGE()");
            String Branch = "";
            String Organization_Unit = "";
            PGE_WS.ApplicationSecurityCheckService PGE_WS = null;
            try
            {
                string matricola = "";
                string token = "";
                string twr = "";
                string effective_twr = "";
                if (Request.Params["user"] != null && Request.Params["token"] != null &&
                    Request.Params["twr"] != null && Request.Params["effective_twr"] != null &&
                    Request.Params["user"] != "" && Request.Params["token"] != "" &&
                    Request.Params["twr"] != "" && Request.Params["effective_twr"] != "")
                {
                    log.Info("CHIAMATA DA XHP");
                    matricola = Request.Params["user"];
                    token = Request.Params["token"];
                    twr = Request.Params["twr"];
                    effective_twr = Request.Params["effective_twr"];


                    User user = new User(matricola, token);

                    PGE_WS = new PGE_WS.ApplicationSecurityCheckService();
                    PGE_WS.Url = "http://" + twr + "/" + ConfigurationManager.AppSettings["PGE_WS"];

                    //TESTSAN
                    //if (matricola.TrimEnd().Equals("EE24544",StringComparison.OrdinalIgnoreCase) )
                    //{
                    //    isDebugActive = true;
                    //    if( Request.Params["test"] != null  )
                    //    {
                    //        PGE_WS.Url = "http://" + "sportelloc0.intranet.unicredit.it" + "/" + ConfigurationManager.AppSettings["PGE_WS"];
                    //    }
                    //}


                    string appCode = ConfigurationManager.AppSettings["ApplicationCode"];
                     log.Info("CALLING PGE WEBSERVICE - WS URL: " + PGE_WS.Url+ "with "
                          + " - USER: " + matricola
                        + " - TWR: " + twr
                        + "- APPCODE: "+appCode);

                    PGE_WS.PGEabdb2 resp = PGE_WS.retrievePGEAuthority(matricola, token, appCode);


                    log.Info("CHIAMATO PGE WEBSERVICE - WS URL: " + PGE_WS.Url
                        + " - USER: " + matricola
                        + " - TWR: " + twr
                        + " - EFFECTIVE_TWR: " + effective_twr
                        + " - TOKEN: " + token
                        );

                    if (resp.PG_COD_AUTHOR != null &&
                        resp.PG_BANCA != null &&
                        resp.PG_COD_IST != null &&
                        resp.PG_RUOLO != null &&
                        resp.PG_TORRE != null &&
                        resp.PG_UFFICIO_INQ != null &&
                        resp.PG_FIL_INQ != null)
                    {
                        log.Info("PGE AUTH SUCCEDED: "
                        + " - User: " + user.Username
                        + " - Torre: " + effective_twr
                        + " - PG_COD_AUTHOR: " + resp.PG_COD_AUTHOR
                        + " - PG_BANCA: " + resp.PG_BANCA
                        + " - PG_COD_IST: " + resp.PG_COD_IST
                        + " - PG_RUOLO: " + resp.PG_RUOLO
                        + " - PG_TORRE: " + resp.PG_TORRE
                        + " - PG_UFFICIO_INQ: " + resp.PG_UFFICIO_INQ
                        + " - PG_FIL_INQ: " + resp.PG_FIL_INQ
                        + " - PG_COD_SPORT_CONT : " + resp.PG_COD_SPORT_CONT
                        );
                    }
                    if ((resp.PG_ERRORE != null) && (!resp.PG_ERRORE.codiceErr.Equals("00")))
                    {
                        log.Info("PGE AUTH ERROR: "
                        + " - User: " + user.Username
                        + " - Torre: " + effective_twr
                        + " - PG_ERRORE.codiceErr: " + resp.PG_ERRORE.codiceErr
                        + " - PG_ERRORE.descError: " + resp.PG_ERRORE.descError);
                    }

                    if (resp.PG_UFFICIO_INQ != null)
                        Branch = resp.PG_COD_SPORT_CONT;
                    if (resp.PG_COD_SPORT_CONT != null)
                        Organization_Unit = resp.PG_UFFICIO_INQ;


                    if (resp.PG_ERRORE != null && resp.PG_ERRORE.codiceErr != "" && resp.PG_ERRORE.codiceErr != "00")
                    {
                        log.Info("Autenticazione non riuscita " + resp.PG_ERRORE.codiceErr + " - " + resp.PG_ERRORE.descError);

                        if (isSkipPGEuser(matricola, 1, Branch, Organization_Unit))
                        {
                            log.Debug("Skipped PGE checks for the user: " + matricola);
                            return;
                        }
                        throw new Exception("Autenticazione non riuscita " + resp.PG_ERRORE.codiceErr + " - " + resp.PG_ERRORE.descError);
                    }
                    else
                    {

                        if (isSkipPGEuser(matricola, 1, Branch, Organization_Unit))
                        {
                            log.Debug("Skipped PGE 1(GO LIVE Man.)checks for the user: " + matricola);
                            return;
                        }
                        //if (resp.PG_TORRE != null && resp.PG_TORRE != "")
                        //    user.Torre = resp.PG_TORRE.TrimEnd(); 
                        user.SportCont = resp.PG_COD_SPORT_CONT.TrimEnd();
                        user.OrganizationUnit = resp.PG_UFFICIO_INQ.TrimEnd();
                        user.TorrePG = resp.PG_TORRE.TrimEnd();

                        if (effective_twr != null && effective_twr != "")
                            user.Torre = effective_twr;

                        //TESTSAN
                        //string profilo = Request.Params["profilo"];
                        //if (profilo != null && profilo != "" && matricola != null && matricola.TrimEnd().Equals("EE24544", StringComparison.OrdinalIgnoreCase))
                        //{
                        //    resp.PG_COD_AUTHOR = profilo;
                        //    log.Info("Profile changed in: " + profilo);
                        //}



                        if (resp.PG_COD_AUTHOR != null && resp.PG_COD_AUTHOR != "")
                            user.CodAuthor = resp.PG_COD_AUTHOR.TrimEnd();
                        if (resp.PG_COD_AUTHOR.TrimEnd().Equals("X1NBAS", StringComparison.OrdinalIgnoreCase)
                            || resp.PG_COD_AUTHOR.TrimEnd().Equals("X1NOPR", StringComparison.OrdinalIgnoreCase)
                            || resp.PG_TORRE.Equals(AUSTRIAN_ENV))
                            //user.ProfiluserORRE.TrimEnd() + resp.PG_COD_SPORT_CONT.TrimEnd();
                            //user.Profilo = resp.PG_TORRE.TrimEnd() + resp.PG_UFFICIO_INQ.TrimEnd().Substring(2);
                            user.Profilo = resp.PG_TORRE.TrimEnd() + resp.PG_COD_SPORT_CONT.TrimEnd();
                        else
                            user.Profilo = resp.PG_COD_AUTHOR.TrimEnd();
                        Navigation.User = user;
                        log.Info("PGE AUTH SUCCEDED: "
                        + " - User: " + user.Username
                        + " - Profilo: " + resp.PG_COD_AUTHOR
                        + " - Banca: " + resp.PG_BANCA
                        + " - Istituto: " + resp.PG_COD_IST
                        + " - Ruolo: " + resp.PG_RUOLO
                        + " - TorrePG: " + resp.PG_TORRE
                        + " - Torre: " + user.Torre
                        + " - Ufficio: " + resp.PG_UFFICIO_INQ
                        + " - Filiale: " + resp.PG_FIL_INQ
                        );

                        log.Debug("PG_COD_ABI: " + resp.PG_COD_ABI
                                         + " - PG_COD_AUTHOR: " + resp.PG_COD_AUTHOR
                                         + " - PG_COD_CAB: " + resp.PG_COD_CAB
                                         + " - PG_COD_ERRORE: " + resp.PG_COD_ERRORE
                                         + " - PG_COD_IST: " + resp.PG_COD_IST
                                         + " - PG_COD_MATRICOLA_FITT: " + resp.PG_COD_MATRICOLA_FITT
                                         + " - PG_COD_SERVER: " + resp.PG_COD_SERVER
                                         + " - PG_COD_SET_CONT: " + resp.PG_COD_SET_CONT
                                         + " - PG_COD_SOC: " + resp.PG_COD_SOC
                                         + " - PG_COD_SPORT_CONT: " + resp.PG_COD_SPORT_CONT
                                         + " - PG_COD_TERM_LU0: " + resp.PG_COD_TERM_LU0
                                         + " - PG_COD_TERM_SERV_SIF: " + resp.PG_COD_TERM_SERV_SIF
                                         + " - PG_COD_TERM_VTAM: " + resp.PG_COD_TERM_VTAM
                                         + " - PG_CODISA_INQ: " + resp.PG_CODISA_INQ
                                         + " - PG_COGNOME_OPER: " + resp.PG_COGNOME_OPER
                                         + " - PG_DESCR_DIP: " + resp.PG_DESCR_DIP
                                         + " - PG_DOM_OPRT: " + resp.PG_DOM_OPRT
                                         + " - PG_FIL_INQ: " + resp.PG_FIL_INQ
                                         + " - PG_NOME_OPER: " + resp.PG_NOME_OPER
                                         + " - PG_RUOLO: " + resp.PG_RUOLO
                                         + " - PG_TORRE: " + resp.PG_TORRE
                                         + " - PG_UFFICIO_INQ: " + resp.PG_UFFICIO_INQ
                                         + " - PG_USERID: " + resp.PG_USERID
                                         + " - PG_CAB_CIN: " + resp.PG_CAB_CIN
                                         + " - PG_CAB_FCOMM: " + resp.PG_CAB_FCOMM
                                         + " - PG_CAB_FORO: " + resp.PG_CAB_FORO
                                         + " - PG_CAB_LUOGO: " + resp.PG_CAB_LUOGO
                            );
                    }
                }
                else
                {
                    log.Info("----- USER LOGGED: Context" + HttpContext.Current.User.Identity.Name + "-old logon-" + Request.ServerVariables["LOGON_USER"] + "-----AUTH " + Request.ServerVariables["AUTH_USER"]);
                    //string user = Request.ServerVariables["LOGON_USER"];
                    //string user = Request.ServerVariables["AUTH_USER"];
                    string user = HttpContext.Current.User.Identity.Name;
                    string[] authUser = user.Split('\\');
                    if (authUser.Length > 1)
                        user = authUser[1];

                    //string[] skipUsers = ConfigurationManager.AppSettings["SKIP_PGE"].Split(',');
                    //bool found = false;
                    //log.Debug(" ConfigurationManager.AppSettings['SKIP_PGE']=" +  ConfigurationManager.AppSettings["SKIP_PGE"]);
                    //foreach (string item in skipUsers ?? new string[0])
                    //{
                    //    log.Debug("item=" + item);
                    //    string[] parms = item.Split('=');
                    //    log.Debug("parms[0]=" + parms[0]);
                    //    if (parms[0].ToUpper().Equals(user.ToUpper()))
                    //    {
                    //        Navigation.User = new User(user, "", (parms.Length > 1 ? parms[1] : "X1NADM"), "", (parms.Length > 1 ? parms[1] : "X1NADM"), "N/A",  (parms.Length > 1 ? parms[1] : "N/A"));
                    //        found = true;
                    //        log.Debug("found = true");
                    //        log.Info("AUTH USER SKIPPING PGE: " + user);
                    //        break;
                    //    }
                    //}
                    //if (!found)
                    if (!isSkipPGEuser(user, 0, Branch, Organization_Unit))
                    {
                        log.Debug("found = false");
                        //Response.Redirect("resp~/Authentication/Login.aspx");
                        throw new Exception("Session timeout or wrong authentication. Please try again to launch the application from Applications menu of your Branch.");
                    }
                    else
                    {
                        log.Debug("Skipped PGE checks for the user: " + user);
                    }
                }
            }
            catch (Exception Ex)
            {
                log.Error("TestPGE() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                if (PGE_WS != null)
                {
                    Navigation.Error = Ex.Message + "\n Authentication Web Service url " + PGE_WS.Url + " \n could be currently unavailable";
                    throw new Exception(Navigation.Error);
                }
                throw Ex;
            }
        }

        /**
         *  method to manage skip user from web.config file parameters SKIP_PGE and SKIP_PGE_4ALL
         *  ATTENTION if type is 0 only SKIP_PGE is checked
        **/
        protected bool isSkipPGEuser(String user, int type, String Branch, String Organization_Unit)
        {

            string[] skipUsers = ConfigurationManager.AppSettings["SKIP_PGE"].Split(',');
            //log.Debug(" ConfigurationManager.AppSettings['SKIP_PGE']=" + ConfigurationManager.AppSettings["SKIP_PGE"]);
            foreach (string item in skipUsers ?? new string[0])
            {
                //log.Debug("item=" + item);
                string[] parms = item.Split('=');
                //log.Debug("parms[0]=" + parms[0]);
                if (parms[0].ToUpper().Equals(user.ToUpper()))
                {
                    log.Info("creating User profile:" + (parms.Length > 1 ? parms[1] : "X1NADM")+" and filiale" );
                    Navigation.User = new User(user, "", (parms.Length > 4 ? parms[4] : "X1NADM"), null, (parms.Length > 1 ? parms[1] : "X1NADM"), (parms.Length > 2 ? parms[2] : (Branch.Equals("") ? "N/A" : Branch)), (parms.Length > 3 ? parms[3] : (Organization_Unit.Equals("") ? "N/A" : Organization_Unit)));
                    log.Debug("AUTH USER SKIPPING PGE: " + user);
                    return true;
                }
            }
            log.Debug("USER NOT FOUND IN SKIPPING PGE LIST: " + user);
            // The appsett variable SKIP_PGE_4ALL is used to force the profile for all the users. The focrced profile is the value of the variable 
            // but only if that one is one of the profile name available

            if (type == 1 && ConfigurationManager.AppSettings["SKIP_PGE_4ALL"] != null)
            {
                String skip_profile = ConfigurationManager.AppSettings["SKIP_PGE_4ALL"].ToString().ToUpper();
                if (skip_profile.Equals("X1NADM") ||
                    skip_profile.Equals("X1NOPR") ||
                    skip_profile.Equals("X1NBAS"))
                {
                    Navigation.User = new User(user, "", skip_profile, "", skip_profile, (Branch.Equals("") ? "N/A" : Branch), Organization_Unit.Equals("") ? "N/A" : Organization_Unit);
                    log.Info("AUTH USER SKIP_PGE_4ALL: " + user);
                    return true;
                }
                else
                {
                    log.Info("AUTH USER SKIP_PGE_4ALL: an error occured: the value of SKIP_PGE_4ALL (" + skip_profile + ") is not a valid profilename.");
                    return false;
                }
            }
            log.Info("PARAMETER SKIP_PGE_4ALL NOT FOUND.");
            return false;
        }


        //protected void Page_Unload(object sender, EventArgs e){
        //    //todo 
        //    log.Debug("Chiamata a Page_Unload. - User: " + Navigation.User.Username + " Page.IsPostBack:" + Page.IsPostBack);
        //    if (Navigation.User != null)
        //        log.Debug("Navigation.User != null");
        //    if (!Page.IsPostBack)
        //        Session.Clear();
        //    if (Navigation.User == null)
        //        log.Debug("Navigation.User == null");
        //}

        //~StartPage()
        //{
        //    log.Debug("Chiamata a distruttore. - ");
        //    if(Navigation.User != null)
        //        log.Debug("User: " + Navigation.User.Username);
        //    if (Page != null)
        //        log.Debug("Page.IsPostBack:" + Page.IsPostBack);
        //    Session.Clear();
        //    if (Navigation.User == null)
        //        log.Debug("Navigation.User == null");

        //}



        protected void sp_b_Tab1_Click(object sender, EventArgs e)
        {
            this.sp_panelTable.Visible = true;
            this.sp_panelTable2.Visible = false;
            this.sp_b_Tab2.CssClass = "buttonMenu";
            this.sp_b_Tab1.CssClass = "buttonMenuSelected";
        }

        protected void sp_b_Tab2_Click(object sender, EventArgs e)
        {
            this.sp_panelTable.Visible = false;
            this.sp_panelTable2.Visible = true;
            this.sp_b_Tab1.CssClass = "buttonMenu";
            this.sp_b_Tab2.CssClass = "buttonMenuSelected";
        }

        public void OpenMail(string urlmail)
        {
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "mailto", "parent.location='" + urlmail + "'", true);
            //ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFile.aspx');", true);
        }

        public void OpenViewFile()
        {
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFile.aspx');", true);
        }

        public void OpenViewFile(string encryptedParams)
        {
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFile.aspx?key="+encryptedParams+"&void=void');", true);
        }

        public void OpenViewTxtFile()
        {
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "TextFile", "window.open('ViewTextFile.aspx', '_textfilewindow');", true);
        }
        
        public void OpenViewFileHistory()
        {
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFileHistory.aspx');", true);
        }

        #endregion
        #region WebMethods
        [WebMethod]
        public static string[] GetInnerCompletionList2(string prefixText, int count, string contextKey)
        {
            string otherKey = ""; string otherValue = "";
            if (contextKey.Contains("jobname"))
            {
                if (contextKey.Split('|').Length > 1)
                    otherValue = contextKey.Split('|')[1];
                contextKey = "job_name";
                otherKey = "report_name";
            }
            else if (contextKey.Contains("tb_name"))
            {
                if (contextKey.Split('|').Length > 1)
                    otherValue = contextKey.Split('|')[1];
                contextKey = "report_name";
                otherKey = "job_name";
            }
            //this.tb_jobname;
            List<string> listPre = new List<string>();
            listPre.Add(contextKey); listPre.Add(prefixText.ToUpper()); listPre.Add(otherKey); listPre.Add(otherValue.ToUpper());
            string query = Utility.BuildQueryMultipleAutocomplete(contextKey, listPre.ToArray());
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            string env = ConfigurationManager.AppSettings["ENV"];
            if (env != null)
                env = env.ToLower();
            else
                env = "a1";
            log.Debug(query);
            int hostPort = int.Parse(ConfigurationManager.AppSettings["ElasticSearchHost_Port"]);
            var connection = new ElasticConnection(host, hostPort);
            //string result = connection.Post(Commands.Search(Navigation.CurrentIndex, Navigation.CurrentType), query);
            string result = connection.Post(Commands.Search("jobn_repn_" + env, "jobn_repn"), query);
            JToken jt = JToken.Parse(query);
            string formattedRes = jt.ToString(Newtonsoft.Json.Formatting.Indented);
            //log_ES.Debug(" autocomplete : " + formattedRes);
            jt = JToken.Parse(result);
            formattedRes = jt.ToString(Newtonsoft.Json.Formatting.Indented);
            var serializer = new JsonNetSerializer();
            RootObject root = serializer.Deserialize<RootObject>(result);
            List<string> list = new List<string>();
            foreach (BucketLeaf t in root.aggregations.suggest.buckets)
                list.Add(t.key);
            return list.ToArray();
        }
        [WebMethod]
        public static string[] GetInnerCompletionList3(string prefixText, int count, string contextKey)
        {
            string otherKey = ""; string otherValue = "";
            if (contextKey.Contains("jobname"))
            {
                if (contextKey.Split('|').Length > 1)
                    otherValue = contextKey.Split('|')[1];
                contextKey = "job_name";
                otherKey = "report_name";
            }
            else if (contextKey.Contains("tb_name"))
            {
                if (contextKey.Split('|').Length > 1)
                    otherValue = contextKey.Split('|')[1];
                contextKey = "report_name";
                otherKey = "job_name";
            }
            //this.tb_jobname;
            List<string> listPre = new List<string>();
            listPre.Add(contextKey); listPre.Add(prefixText.ToUpper()); listPre.Add(otherKey); listPre.Add(otherValue.ToUpper());
            string query = Utility.BuildQueryMultipleAutocomplete(contextKey, listPre.ToArray());
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            string env = ConfigurationManager.AppSettings["ENV"];
            if (env != null)
                env = env.ToLower();
            else
                env = "a1";
            log.Debug(query);
            int hostPort = int.Parse(ConfigurationManager.AppSettings["ElasticSearchHost_Port"]);
            var connection = new ElasticConnection(host, hostPort);
            //string result = connection.Post(Commands.Search(Navigation.CurrentIndex, Navigation.CurrentType), query);
            string result = connection.Post(Commands.Search("jobn_repn_" + env, "jobn_repn"), query);
            JToken jt = JToken.Parse(query);
            string formattedRes = jt.ToString(Newtonsoft.Json.Formatting.Indented);
            //log_ES.Debug(" autocomplete : " + formattedRes);
            jt = JToken.Parse(result);
            formattedRes = jt.ToString(Newtonsoft.Json.Formatting.Indented);
            var serializer = new JsonNetSerializer();
            RootObject root = serializer.Deserialize<RootObject>(result);
            List<string> list = new List<string>();
            foreach (BucketLeaf t in root.aggregations.suggest.buckets) {
                if (t.key.Length > 4){
                    if (!list.Contains(t.key.Substring(0, 4)))
                        list.Add(t.key.Substring(0, 4));
                }
                else
                    if (!list.Contains(t.key))
                        list.Add(t.key);
            }
            return list.ToArray();
        }
        [WebMethod]
        public static string getSecondValue(string name)
        {
            log.Debug("ecco sono dentro");
            return HttpContext.Current.Session.SessionID;
            //+ "--The Current Time is: " + DateTime.Now.ToString()
            //return null;

        }
        #endregion

        protected void ddlLanguage_TextChanged(object sender, EventArgs e)
        {
            string ambiente = "";
            //this.linkmanual.CssClass = "headerTitleRightInner";
            this.linkmanual.Visible = true;
            this.linkmanual.ToolTip = GetLocalResourceObject("downmanual").ToString();
            this.sp_man_img.Visible = true;
            this.linkmanual.Text = "<IMG width=15 height=15 id=sp_man_img alt=\"Scarica manuale utente\" src=\"Images/icon_manual.png\">";
            if (ConfigurationManager.AppSettings["ENV"] != null)
            {
                ambiente = ConfigurationManager.AppSettings["ENV"].ToString().TrimEnd().ToUpper();
            }
            else
            {
                ambiente = Request.Url.AbsoluteUri.Substring(9, 2).ToUpper();
            }
            //this.sp_lb_Ambiente.Text = "Environment: " + ambiente; 
            this.sp_lb_Ambiente.Text = GetLocalResourceObject("env").ToString() + ": " + ambiente;
            //this.sp_lb_User.Text = "User: " + Navigation.User.Username;
            this.sp_lb_User.Text = GetLocalResourceObject("user").ToString() + ": " + Navigation.User.Username;
            //this.sp_lb_Profilo.Text = "Profile: " + Navigation.User.Profilo;
            //this.sp_lb_Profilo.Text = "Profile: " + Navigation.User.CodAuthor;
            this.sp_lb_Profilo.Text = GetLocalResourceObject("profile").ToString() + ": " + Navigation.User.CodAuthor;
            //this.sp_lb_Profilo2.Text = "Branch: " + Navigation.User.SportCont;
            this.sp_lb_Profilo2.Text = GetLocalResourceObject("branch").ToString() + ": " + Navigation.User.SportCont;
            //this.sp_lb_OrganizationUnit.Text = "Organization Unit: " + Navigation.User.OrganizationUnit;
            this.sp_lb_OrganizationUnit.Text = GetLocalResourceObject("orgunit").ToString() + ": " + Navigation.User.OrganizationUnit;
            //this.sp_lb_Tree.Text = GetLocalResourceObject("orgunit").ToString() + ": " + "START";
            this.sp_UpdatePanel1.Update();
            
        }
    }
}