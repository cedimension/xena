﻿<%@ Import Namespace="XReportSearch.Business" %>
<%@ Register Namespace="CustomControls" TagPrefix="cedim" Assembly="XReportSearch" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchHistory.aspx.cs"
    Inherits="XReportSearch.SearchHistory" Theme="XReportSearchTheme" EnableViewState="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
    <title>XReport History Search</title>
    <link href="~/App_Themes/XReportSearchAjaxControls.css?1" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/XReportSearchStyle.css?2" rel="stylesheet" type="text/css" />
</head>
<body onload="resize()" onresize="resize()" onclick="resize()" onbeforeprint="resize()">
    <script language="javascript" type="text/javascript">

        function fnOnUpdateValidators() {
            var count = 0;
            for (var i = 0; i < Page_Validators.length; i++) {
                var val = Page_Validators[i];
                var ctrl = document.getElementById(val.controltovalidate);
                if (ctrl != null && ctrl.style != null) {
                    if (!val.isvalid) {
                        ctrl.style.borderColor = "Red";
                        ctrl.className = "comboboxError";
                        count++;
                    }
                    else {
                        ctrl.style.borderColor = "#518012";
                        ctrl.className = "combobox";
                    }
                }
            }
        }

        function resize() {
            var frame = document.getElementById("MainDiv");
            var page = document.getElementById("PageDiv2");
            var grid = document.getElementById("divCustomGrid");
            var gridView = document.getElementById("gv_search");
            var windowheight = document.documentElement.clientHeight;
            var windowwidth = document.documentElement.clientWidth;
            frame.style.height = windowheight + "px";
            if (document.getElementById("txtHidData") != null) {
                document.getElementById("txtHidData").value = windowheight;
            }
            if (document.getElementById("txtHidData2") != null) {
                document.getElementById("txtHidData2").value = windowwidth;
            }
            frame.style.height = windowheight + "px";
            if (page.style.height != null && windowheight > 0) {
                page.style.height = (windowheight - 4) + "px";
            }
            if (grid != null && windowheight > 0) {
                grid.style.height = (windowheight - 61) + "px";
                if (windowwidth < 800) {
                    page.style.fontSize = "0.5em";
                }
                else if (windowwidth > 2000) {
                    page.style.fontSize = "2em";
                }
                else {
                    page.style.fontSize = "0.8em";
                }
            }
        }
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" EnablePartialRendering="true"
        EnablePageMethods="true"  AsyncPostBackTimeout="600">
    </asp:ScriptManager>
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').add_showing(onshowing);
            resize();
        }

        function onshowing() {
            if ($find('ModalProgress') != null) {
                var windowwidth = document.documentElement.clientWidth;
                var windowheight = document.documentElement.clientHeight;
                $find('ModalProgress').set_X((parseInt(windowwidth) / 2) - 20);
                $find('ModalProgress').set_Y((parseInt(windowheight) / 2) - 20);
            }
        }

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

        function beginRequest() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').show();
        }

        function endRequest() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').hide();
        }

        function TextChanged(controlName) {
            var tb_To;
            if (controlName.indexOf('_From') !== -1)
                tb_To = document.getElementById(controlName.replace("_From", "_To"));
            var tb_From = document.getElementById(controlName);
            if (tb_From.value.length > 0 && tb_To != null)
                tb_To.value = tb_From.value;
        }

        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            if ($get('divCustomGrid') != null) {
                xPos = $get('divCustomGrid').scrollLeft;
                yPos = $get('divCustomGrid').scrollTop;
            }
        }
        function EndRequestHandler(sender, args) {
            if ($get('divCustomGrid') != null) {
                $get('divCustomGrid').scrollLeft = xPos;
                $get('divCustomGrid').scrollTop = yPos;
            }
        }
    </script>
    <input type="hidden" clientidmode="Static" id="txtHidData" runat="server" />
    <input type="hidden" clientidmode="Static" id="txtHidData2" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div id="MainDiv" class="main">
               
                <asp:Panel runat="server" ID="panelTable" class="page" ScrollBars="Auto">
                    <table class="page" id="PageDiv">
                        <tr id="PageDiv2">
                            <td class="pageLeft">
                                <table id="ControlsTable" runat="server">
                                    <tr class="data">
                                        <td>
                                        </td>
                                        <td class="buttons">
                                            <asp:Button ID="Button2" CausesValidation="true" runat="server" Text="Search" Visible="true"
                                                CssClass="buttonBig" OnClick="buttonElasticSearch_Click" />
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="Label5" runat="server" Text="Index" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <ajax:ComboBox ID="ddl_indice" runat="server" AutoPostBack="True" RenderMode="Block"
                                                DropDownStyle="DropDown" Visible="true" CaseSensitive="False" CssClass="comboboxDoc"
                                                onkeydown="OnKeyDownCombo()" OnSelectedIndexChanged="ddl_indice_SelectedIndexChanged">
                                            </ajax:ComboBox>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="Label6" runat="server" Text="Type" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <ajax:ComboBox ID="ddl_type" runat="server" AutoPostBack="True" RenderMode="Block"
                                                DropDownStyle="DropDown" Visible="true" CaseSensitive="False" CssClass="comboboxDoc"
                                                onkeydown="OnKeyDownCombo()" OnSelectedIndexChanged="ddl_type_SelectedIndexChanged">
                                            </ajax:ComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="border-bottom: 1px solid White; height: 3px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="height: 3px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="pageRight">
                                <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
                                    <ContentTemplate>
                                        <cedim:CeGridView ID="gv_search" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                            AllowPaging="true" PagerStyle-CssClass="pager" HeaderStyle-ForeColor="White"
                                            HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" HeaderStyle-Height="25px"
                                            RowStyle-Height="25px" Visible="false" Font-Name="Verdana" SkinID="Tables2" Width="100%"
                                            Height="100%" OnRowDataBound="gv_search_OnRowDataBound" OnPageIndexChanging="gv_search_PageIndexChanging"
                                            OnRowCommand="gv_search_OnRowCommand" EmptyDataRowStyle-Font-Bold="true" EmptyDataText="No Data"
                                            EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="#518012">
                                            <Columns>
                                            </Columns>
                                        </cedim:CeGridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <asp:UpdateProgress ID="UpdateProgress" runat="server" DisplayAfter="200">
                <ProgressTemplate>
                    <asp:Panel ID="panelUpdateProgress" runat="server">
                        <div>
                            <img src="~/Images/loader.gif" alt="" runat="server" />
                        </div>
                    </asp:Panel>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <ajax:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
                BackgroundCssClass="modalProgress" PopupControlID="panelUpdateProgress" BehaviorID="ModalProgress">
            </ajax:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
