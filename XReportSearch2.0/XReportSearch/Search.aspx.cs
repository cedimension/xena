﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Configuration;
using System.Web.Services.Protocols;
using XReportSearch.Utils;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;
using PlainElastic.Net.Queries;
using XReportSearch.Business;
using PlainElastic.Net;
using PlainElastic.Net.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Services;
using Microsoft.Web.Administration;

namespace XReportSearch
{
    public partial class Search : System.Web.UI.Page
    {
        #region Constants
        private string SORT_DESCENDING = "DESC";
        private string SORT_ASCENDING = "ASC";
        #endregion

        #region Log
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Main Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ConfigurationManager.AppSettings["DEBUG"].ToString() == "1")
                Navigation.User = new User("xreport", "SUPER", "C0X1NADM", "C0", "X1NADM");
            else
                TestPGE();
            if (Navigation.User == null)
            {
                Navigation.Error = "Session ended. Please reload the application.";
                Response.Redirect("~/Error.aspx");
            }
            if (Page.Validators != null && Page.Validators.Count > 0)
            {
                ScriptManager.RegisterOnSubmitStatement(this, Page.GetType(), "", "fnOnUpdateValidators()");
            }
            if (!Page.IsPostBack)
            {
                string tabs = ConfigurationManager.AppSettings["ExcludedTabs"];
                if (tabs != null && tabs != "")
                {
                    List<MenuItem> items = new List<MenuItem>();
                    string[] values = tabs.Split(',');
                    foreach (string v in values)
                        foreach (MenuItem i in this.nav.Items)
                            if (v == i.Value)
                                items.Add(i);
                    foreach (MenuItem i in items)
                        this.nav.Items.Remove(i);
                }
                this.l_prof.Text = ConfigurationManager.AppSettings["ProfileLbText"];
                //this.nav.Items[0].Selected = true;
                //switch (this.nav.SelectedItem.Value)
                //{
                //    case "1":
                //        Navigation.SearchType = Utility.Searchtype.Online;
                //        break;
                //    case "2":
                //        Navigation.SearchType = Utility.Searchtype.History;
                //        break;
                //}
                Navigation.TableSource = null;
                if (Navigation.SearchType == Utility.Searchtype.Online)
                {
                    this.nav.Items[0].Selected = true;
                    this.gv_search.Visible = true;
                    this.gv_searchH.Visible = false;
                    this.l_ambiente.Visible = false;
                    this.ddl_ambiente.Visible = false;
                }
                else if (Navigation.SearchType == Utility.Searchtype.History)
                {
                    this.nav.Items[1].Selected = true;
                    this.gv_search.Visible = false;
                    this.gv_searchH.Visible = true;
                    this.l_ambiente.Visible = true;
                    this.ddl_ambiente.Visible = true;
                }
                if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.UNICREDIT.ToString())
                {
                    this.lb_User.Text = "User: " + Navigation.User.Username + " - Profile: " + Navigation.User.Profilo;
                    this.tb_jobname2.Text = Navigation.User.Torre;
                    //this.tb_prof.Text = Navigation.User.Profilo;
                    if (Navigation.User.CodAuthor.Equals("X1NADM") || Navigation.User.CodAuthor.Equals("X1NOPER"))
                    {
                        this.tb_jobname2.Enabled = true;
                        this.tb_prof.Enabled = true;
                    }
                    else
                    {
                        //this.tb_jobname2.Enabled = false;
                        this.l_prof.Visible = false;
                        this.tb_prof.Enabled = false;
                        this.tb_prof.Visible = false;
                    }
                }
                else
                {
                    this.lb_User.Text = "User: " + Navigation.User.Username;
                }
            }
            else
            {
                if (this.hiddenIndex.Value == "1")
                    this.CreateLinkButtons();
            }
        }
        #endregion

        #region Private Methods
        private XReportWebIface.DocumentData BuildDocumentData(string RepName, string JRName, string JRName2, string JobNumber, string date, string date2, string recipient, string ambiente)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column ReportName = new XReportWebIface.column();
                ReportName.colname = "ReportName";
                if (RepName != null && RepName != "")
                    ReportName.Value = RepName + "%";
                else
                    ReportName.Value = RepName;
                XReportWebIface.column JobName = new XReportWebIface.column();
                JobName.colname = "JobName";
                if ((JRName != null && JRName != "") || (JRName2 != null && JRName2 != ""))
                    JobName.Value = JRName + "%" + JRName2;
                else
                    JobName.Value = "";
                XReportWebIface.column JobNum = new XReportWebIface.column();
                JobNum.colname = "JobNumber";
                if (JobNumber != null && JobNumber != "")
                    JobNum.Value = JobNumber + "%";
                else
                    JobNum.Value = JobNumber;
                XReportWebIface.column XferStartTime = new XReportWebIface.column();
                XferStartTime.colname = "UserTimeRef";
                XferStartTime.operation = "date";
                XferStartTime.Min = Utility.ParseDate(date, "{0:yyyyMMdd}");
                XferStartTime.Max = Utility.ParseDate(date2, "{0:yyyyMMdd}");
                XReportWebIface.column User = new XReportWebIface.column();
                User.colname = "User";
                if (Navigation.User.Profilo != null && Navigation.User.Profilo != "")
                    User.Value = Navigation.User.Profilo;
                else
                    User.Value = "";
                XReportWebIface.column Recipient = new XReportWebIface.column();
                Recipient.colname = "Recipient";
                Recipient.Value = recipient;
                if (recipient != null && recipient != "")
                    Recipient.Value = recipient + "%";
                else
                    Recipient.Value = "";
                XReportWebIface.column Ambiente = new XReportWebIface.column();
                Ambiente.colname = "Ambiente";
                Ambiente.Value = ambiente;
                XReportWebIface.column CurrentRow = new XReportWebIface.column();
                CurrentRow.colname = "CurrentRow";
                CurrentRow.Value = "0";

                Entry.Columns = new XReportWebIface.column[8] { ReportName, JobName, XferStartTime, JobNum, User, Ambiente, CurrentRow, Recipient };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private XReportWebIface.DocumentData BuildDocumentDataPDF(string JRID, string FromPag)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column JobRepID = new XReportWebIface.column();
                JobRepID.colname = "JobReportId";
                JobRepID.Value = JRID;
                XReportWebIface.column FromPage = new XReportWebIface.column();
                FromPage.colname = "FromPage";
                FromPage.Value = FromPag;

                Entry.Columns = new XReportWebIface.column[2] { JobRepID, FromPage };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private XReportWebIface.DocumentData BuildDocumentDataNotes(string JobRepID, string RepID, string version, string UpdUser, string text, Utility.Operation operation)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column JobReportID = new XReportWebIface.column();
                JobReportID.colname = "JobReportID";
                JobReportID.Value = JobRepID;
                XReportWebIface.column ReportID = new XReportWebIface.column();
                ReportID.colname = "ReportID";
                ReportID.Value = RepID;
                XReportWebIface.column Version = new XReportWebIface.column();
                Version.colname = "Version";
                Version.Value = version;
                XReportWebIface.column UpdateUser = new XReportWebIface.column();
                UpdateUser.colname = "UpdateUser";
                UpdateUser.Value = UpdUser;
                XReportWebIface.column Text = new XReportWebIface.column();
                Text.colname = "Text";
                Text.Value = text;

                Entry.Columns = new XReportWebIface.column[5] { JobReportID, ReportID, Version, UpdateUser, Text };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private static XReportWebIface.DocumentData BuildDocumentDataCheck(string JRID, string ReportID, string CheckUser)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column JobRepID = new XReportWebIface.column();
                JobRepID.colname = "JobReportID";
                JobRepID.Value = JRID;
                XReportWebIface.column RepID = new XReportWebIface.column();
                RepID.colname = "ReportID";
                RepID.Value = ReportID;
                XReportWebIface.column User = new XReportWebIface.column();
                User.colname = "CheckUser";
                User.Value = CheckUser;

                Entry.Columns = new XReportWebIface.column[3] { JobRepID, RepID, User };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        public void LoadReports(string RepName, string JRName, string JRName2, string JobNumber, string date, string date2, string recipient, string ambiente)
        {
            try
            {
                log.Debug("LoadReports() Started");
                XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface();
                XReportWebIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.DocumentData Data = BuildDocumentData(RepName, JRName, JRName2, JobNumber, date, date2, recipient, ambiente);
                XReportWebIface.Timeout = 600000;
                if (Navigation.SearchType == Utility.Searchtype.Online)
                    Data = XReportWebIface.getReportsData(Data);
                else if (Navigation.SearchType == Utility.Searchtype.History)
                    Data = XReportWebIface.getHistoryReportsData(Data);
                Navigation.TableSource = Utility.IndexEntriesToTable(Data.IndexEntries);
                BindGridView();
                if (Navigation.TableSource != null && Navigation.TableSource.Rows.Count >= 1000)
                {
                    this.lb_Popup.Text = "More than 1000 results found.";
                    this.ModalPopupExtender2.Show();
                }
                log.Debug("LoadReports() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadReports() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadReports() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private void LoadPDF(string JRID, string FromPage)
        {
            try
            {
                log.Debug("LoadPDF() Started");
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface();
                XReportWebIFace.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIFace.Timeout = 600000;
                XReportWebIface.DocumentData Data = BuildDocumentDataPDF(JRID, FromPage);
                Data = XReportWebIFace.getJobReportPdf(Data);
                Navigation.ViewParameters = Data.FileName;
                ClientScript.RegisterStartupScript(this.Page.GetType(), "", "window.open('ViewFile.aspx');", true);
                log.Debug("LoadPDF() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadPDF() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadPDF() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private void LoadNotes()
        {
            try
            {
                log.Debug("LoadNotes() Started");
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface();
                XReportWebIFace.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIFace.Timeout = 600000;
                string[] args = Navigation.ViewParameters.ToString().Split(';');
                XReportWebIface.DocumentData Data = BuildDocumentDataNotes(args[0], args[1], "", "", "", Utility.Operation.GET);
                Data = XReportWebIFace.GetNotes(Data);
                Navigation.TableNotes = Utility.IndexEntriesToTable(Data.IndexEntries);
                if (Navigation.TableNotes != null)
                {
                    this.dv_notes.PagerSettings.PageButtonCount = Navigation.TableNotes.Rows.Count;
                    this.dv_notes.PagerSettings.FirstPageText = "1";
                    this.dv_notes.PagerSettings.LastPageText = "" + Navigation.TableNotes.Rows.Count + "";
                    this.dv_notes.PageIndex = Navigation.CurrentNoteIndex;
                    this.dv_notes.ChangeMode(DetailsViewMode.ReadOnly);
                    this.dv_notes.DataSource = Navigation.TableNotes;                   
                    this.dv_notes.DataBind();
                }
                else
                {
                    dv_notes.ChangeMode(DetailsViewMode.Insert);
                }
                log.Debug("LoadNotes() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadNotes() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadNotes() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private void UpdateNote(string version, string UpdUser, string text, Utility.Operation operation)
        {
            try
            {
                log.Debug("UpdateNote() Started");
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface();
                XReportWebIFace.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIFace.Timeout = 600000;
                string[] args = Navigation.ViewParameters.ToString().Split(';');
                XReportWebIface.DocumentData Data = BuildDocumentDataNotes(args[0], args[1], version, UpdUser, text, operation);
                switch (operation)
                {
                    case Utility.Operation.INSERT:
                        Data = XReportWebIFace.InsertNote(Data);
                        break;
                    case Utility.Operation.UPDATE:
                        Data = XReportWebIFace.UpdateNote(Data);
                        break;
                    case Utility.Operation.DELETE:
                        Data = XReportWebIFace.DeleteNote(Data);
                        break;
                    default:
                        break;
                }
                log.Debug("UpdateNote() Finished");
                if (Data.IndexEntries[0].Columns[0].colname == "ActionMessage" && Data.IndexEntries[0].Columns[0].Value.ToLower() != "success")
                {
                    this.lb_Popup.Text = Data.IndexEntries[0].Columns[0].Value;
                    this.ModalPopupExtender2.Show();
                }
            }
            catch (SoapException SoapEx)
            {
                log.Error("UpdateNote() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("UpdateNote() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private void CheckReport(Utility.Operation operation)
        {
            try
            {
                log.Debug("CheckReport() Started");
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface();
                XReportWebIFace.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIFace.Timeout = 600000;
                string[] args = Navigation.ViewParameters.ToString().Split(';');
                XReportWebIface.DocumentData Data = BuildDocumentDataCheck(args[0], args[1], Navigation.User.Username);
                switch (operation)
                {
                    case Utility.Operation.CHECK_REPORT:
                        Data = XReportWebIFace.CheckReport(Data);
                        break;
                    case Utility.Operation.UPDATE_USER_TO_VIEW:
                        Data = XReportWebIFace.UpdateLastUserToView(Data);
                        break;
                    default:
                        break;
                }  
                log.Debug("CheckReport() Finished");
                if (Data.IndexEntries[0].Columns[0].colname == "ActionMessage" && Data.IndexEntries[0].Columns[0].Value.ToLower() != "success")
                {
                    this.lb_Popup.Text = Data.IndexEntries[0].Columns[0].Value;
                    this.ModalPopupExtender2.Show();
                }
            }
            catch (SoapException SoapEx)
            {
                log.Error("CheckReport() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("CheckReport() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private void CountReports(string RepName, string JRName, string JRName2, string JobNumber, string date, string date2, string recipient, string ambiente)
        {
            try
            {
                log.Debug("CountReports() Started");
                XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface();
                XReportWebIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.DocumentData Data = BuildDocumentData(RepName, JRName, JRName2, JobNumber, date, date2, recipient, ambiente);
                XReportWebIface.Timeout = 600000;
                if (Data.IndexEntries[0].Columns[0].colname == "ActionMessage" && Data.IndexEntries[0].Columns[0].Value.ToLower().StartsWith(Utility.WS_NO_DATA))
                    Navigation.TotReports = -1;
                if (Data.IndexEntries != null && Data.IndexEntries.Length > 1)
                {
                    Navigation.TotReports = int.Parse(Data.IndexEntries[1].Columns[0].Value);
                }
                log.Debug("CountReports() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("CountReports() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("CountReports() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private void BindGridView()
        {
            int numPages = 0;
            if (Navigation.TableSource != null)
            {
                double pages = (double)Navigation.TableSource.Rows.Count / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                //double pages = (double)Navigation.TotReports / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                numPages = (int)Math.Ceiling(pages);
            }
            if (Navigation.SearchType == Utility.Searchtype.Online)
            {
                this.gv_search.PageSize = int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                if (Navigation.CurrentGridPage != -1)
                    this.gv_search.PageIndex = Navigation.CurrentGridPage;
                //    this.gv_search.PageIndex = Utility.GetRealPage(Navigation.CurrentGridPage);
                this.gv_search.DataSource = Navigation.TableSource;
                this.gv_search.Visible = true;
                this.gv_search.PagerSettings.FirstPageText = "1";
                this.gv_search.PagerSettings.LastPageText = "" + numPages + "";
                this.gv_search.DataBind();
            }
            else if (Navigation.SearchType == Utility.Searchtype.History)
            {
                this.gv_searchH.PageSize = int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                if (Navigation.CurrentGridPage != -1)
                    this.gv_searchH.PageIndex = Navigation.CurrentGridPage;
                this.gv_searchH.DataSource = Navigation.TableSource;
                this.gv_searchH.Visible = true;
                this.gv_searchH.PagerSettings.FirstPageText = "1";
                this.gv_searchH.PagerSettings.LastPageText = "" + numPages + "";
                this.gv_searchH.DataBind();
            }
        }

        private void TestPGE()
        {
            try
            {
                string matricola = "";
                string token = "";
                if (Request.Params["user"] != null && Request.Params["token"] != null)
                {
                    log.Debug("BACKURL CHIAMATA");
                    matricola = Request.Params["user"];
                    token = Request.Params["token"];
                    log.Debug("USER: " + matricola);
                    log.Debug("TOKEN: " + token);
                    User user = new User(matricola, token);
                    PGE_WS.ApplicationSecurityCheckService PGE_WS = new PGE_WS.ApplicationSecurityCheckService();
                    PGE_WS.Url = ConfigurationManager.AppSettings["PGE_WS"];
                    string appCode = ConfigurationManager.AppSettings["ApplicationCode"];

                    PGE_WS.PGEabdb2 resp = PGE_WS.retrievePGEAuthority(matricola, token, appCode);
                    if (resp.PG_ERRORE != null && resp.PG_ERRORE.codiceErr != "" && resp.PG_ERRORE.codiceErr != "00")
                    {
                        log.Debug("Autenticazione non riuscita " + resp.PG_ERRORE.codiceErr + " - " + resp.PG_ERRORE.descError);
                    }
                    else
                    {
                        if (resp.PG_TORRE != null && resp.PG_TORRE != "")
                            user.Torre = resp.PG_TORRE.TrimEnd();
                        if (resp.PG_COD_AUTHOR != null && resp.PG_COD_AUTHOR != "")
                            user.CodAuthor = resp.PG_COD_AUTHOR.TrimEnd();
                        if (resp.PG_COD_AUTHOR == "X1NBAS")
                            user.Profilo = resp.PG_TORRE.TrimEnd() + resp.PG_FIL_INQ.TrimEnd();
                        else
                            user.Profilo = resp.PG_TORRE.TrimEnd() + resp.PG_COD_AUTHOR.TrimEnd();
                        Navigation.User = user;
                        log.Debug("PGE AUTH SUCCEDED: ");
                        log.Debug("User: " + user.Username);
                        log.Debug("Profilo: " + resp.PG_COD_AUTHOR);
                        log.Debug("Banca: " + resp.PG_BANCA);
                        log.Debug("Istituto: " + resp.PG_COD_IST);
                        log.Debug("Ruolo: " + resp.PG_RUOLO);
                        log.Debug("Torre: " + resp.PG_TORRE);
                        log.Debug("Ufficio: " + resp.PG_UFFICIO_INQ);
                        log.Debug("Filiale: " + resp.PG_FIL_INQ);
                    }
                }
            }
            catch (Exception Ex)
            {
                log.Error("TestPGE() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private void CreateLinkButtons()
        {
            string[] indexes = Navigation.ViewParameters.Split(';');
            for (int i = 0; i < indexes.Length-1; i++)
            {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell = new HtmlTableCell();
                System.Web.UI.WebControls.LinkButton btn = new System.Web.UI.WebControls.LinkButton();
                btn.ID = "btnLink_" + i;
                btn.Text = indexes[i];
                btn.CssClass = "linkButton";
                btn.Click += new EventHandler(btn_Click);
                cell.Controls.Add(btn);
                row.Cells.Add(cell);
                this.TablePopup.Rows.Add(row);
            }
        }

        private void ClearLinkButton()
        {
            this.hiddenIndex.Value = "0";
            int[] rowInd = new int[this.TablePopup.Rows.Count];
            for (int i = 2; i < this.TablePopup.Rows.Count - 1; i++)
                rowInd[i] = i;
            for (int i = this.TablePopup.Rows.Count - 1; i > 2; i--)
            {
                HtmlTableRow row = this.TablePopup.Rows[i];
                this.TablePopup.Rows.Remove(row);
                row.Controls.Clear();
                row.Dispose();
            }
        }
        #endregion

        #region Events
        protected void Item_Click(object sender, MenuEventArgs e)
        {
            switch (e.Item.Value.ToString())
            {
                case "1":
                    Navigation.SearchType = Utility.Searchtype.Online;
                    Response.Redirect("Search.aspx");
                    break;
                case "2":
                    Navigation.SearchType = Utility.Searchtype.History;
                    Response.Redirect("Search.aspx");
                    break;
            }
        }

        protected void grid_Sorting(object sender, GridViewSortEventArgs e)
        {
            Navigation.CurrentSortExpression = e.SortExpression;
            if (Navigation.CurrentSortDirection == null)
                Navigation.CurrentSortDirection = SORT_DESCENDING;

            if (Navigation.CurrentSortDirection.ToString() == SORT_ASCENDING)
                Navigation.CurrentSortDirection = SORT_DESCENDING;
            else
                Navigation.CurrentSortDirection = SORT_ASCENDING;
            BindGridView();
        }

        protected void buttonSearch_Click(object sender, EventArgs e)
        {
            Navigation.CurrentGridPage = 0;
            if (Navigation.User == null)
                Navigation.User = new User("xreport", "SUPER", "C0X1NADM", "C0", "X1NADM");
            if (Navigation.SearchType == Utility.Searchtype.Online)
            {
                //CountReports(this.tb_name.Text, this.tb_jobname.Text, this.tb_jobname2.Text, this.tb_jobnumber.Text, this.tb_data1.Text, this.tb_data2.Text, this.tb_prof.Text, "");
                LoadReports(this.tb_name.Text, this.tb_jobname.Text, this.tb_jobname2.Text, this.tb_jobnumber.Text, this.tb_data1.Text, this.tb_data2.Text, this.tb_prof.Text, "");
            }
            else if (Navigation.SearchType == Utility.Searchtype.History)
                LoadReports(this.tb_name.Text, this.tb_jobname.Text, this.tb_jobname2.Text, this.tb_jobnumber.Text, this.tb_data1.Text, this.tb_data2.Text, this.tb_prof.Text, this.ddl_ambiente.SelectedValue);
        }

        protected void gv_search_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Navigation.CurrentGridPage = e.NewPageIndex;
            BindGridView();
        }

        protected void gv_search_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView view = (DataRowView)e.Row.DataItem;
                if (Navigation.SearchType == Utility.Searchtype.Online)
                {
                    string jid = view["JobReport_ID"].ToString();
                    string id = view["ReportId"].ToString();
                    string name = view["ReportName"].ToString();
                    string pages = view["ListOfPages"].ToString();
                    string fromPage = view["FromPage"].ToString();
                    bool checkFlag = false;
                    bool checkStatus = false;
                    if (view["CheckFlag"] != null && view["CheckFlag"].ToString() != "")
                        checkFlag = Utility.ConvertStringToBool(view["CheckFlag"].ToString());
                    if (view["CheckStatus"] != null && view["CheckStatus"].ToString() != "")
                        checkStatus = Utility.ConvertStringToBool(view["CheckStatus"].ToString());
                    string lastUserToCheck = view["LastUsedToCheck"].ToString();
                    ImageButton b_pdf = (ImageButton)e.Row.FindControl("b_pdf");
                    b_pdf.ToolTip = "View PDF - " + jid;
                    b_pdf.CommandArgument = jid + ";" + id + ";" + pages + ";" + fromPage;
                    ImageButton b_note = (ImageButton)e.Row.FindControl("b_note");
                    b_note.CommandArgument = jid + ";" + id;
                    ImageButton b_check = (ImageButton)e.Row.FindControl("b_check");   
                    if (checkFlag && checkStatus)
                    {
                        b_check.ImageUrl = "~/Images/checked.png";
                        b_check.CommandName = "Checked";
                        b_check.ToolTip = "Checked by " + lastUserToCheck;
                        b_check.Enabled = false;
                    }
                    else if (checkFlag && !checkStatus)
                    {
                        b_check.ImageUrl = "~/Images/toCheck.png";
                        b_check.CommandName = "Check";
                        b_check.ToolTip = "Check Report";
                        b_check.CommandArgument = jid + ";" + id;
                        b_check.Enabled = true;
                    }
                    else if (!checkFlag)
                    {
                        b_check.Visible = false;
                        b_check.Enabled = false;
                    }
                }
                else if (Navigation.SearchType == Utility.Searchtype.History)
                {
                    ImageButton b_pdf = (ImageButton)e.Row.FindControl("b_pdf");
                    ImageButton b_index = (ImageButton)e.Row.FindControl("b_index");
                    string restored = view["Restored"].ToString();
                    string indexed = view["Indexed"].ToString();
                    string recipient = view["Recipient"].ToString();
                    string controld = this.ddl_ambiente.SelectedValue.ToString();
                    string restore_id = view["restore_id"].ToString();
                    string firstRBA = view["firstRBA"].ToString();
                    string last_rba = view["lastRBA"].ToString();
                    string tot_pages = view["totpages2"].ToString();
                    b_pdf.CommandArgument = controld + ";" + restore_id + "/1.1" + ";" + firstRBA + ";" + last_rba + ";" + tot_pages;
                    if (restored == "0" || restored == "")
                    {
                        b_pdf.ImageUrl = "~/Images/refresh.ico";
                        b_pdf.ToolTip = "Not Yet Restored";
                        b_pdf.Enabled = false;
                        b_index.Enabled = false;
                    }
                    else if (recipient == "")
                    {
                        b_pdf.Visible = false;
                    }
                    if (indexed == "")
                    {
                        b_index.ImageUrl = "";
                        b_index.ToolTip = "";
                        b_index.Enabled = false;
                        b_index.Visible = false;
                    }
                    else
                    {
                        string jobName = view["JobName"].ToString();
                        string reportName = view["ReportName"].ToString();
                        b_index.CommandArgument = indexed + "/" + jobName + "," + reportName;               
                    }
                }
            }

            //else if (e.Row.RowType == DataControlRowType.Pager)
            //{
            //    LiteralControl control = new LiteralControl(); //new litearl control 
            //    if (Navigation.SearchType == Utility.Searchtype.Online)
            //    {
            //        control.Text = "Page " + (this.gv_search.PageIndex + 1) + " of " + this.gv_search.PageCount; // add text 
            //    }
            //    else if (Navigation.SearchType == Utility.Searchtype.History)
            //    {
            //        control.Text = "Page " + (this.gv_searchH.PageIndex + 1) + " of " + this.gv_searchH.PageCount; // add text 
            //    }
            //    Table table = e.Row.Cells[0].Controls[0] as Table;  // get the pager table 
            //    TableCell newCell = new TableCell(); //Create new cell 
            //    newCell.Controls.Add(control);  //Add contol 
            //    table.Rows[0].Cells.AddAt(0, newCell); //add cell 
            //} 
        }

        protected void gv_search_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewFile")
            {
                Navigation.ViewParameters = e.CommandArgument.ToString();
                if (Navigation.SearchType == Utility.Searchtype.Online)
                {
                    this.CheckReport(Utility.Operation.UPDATE_USER_TO_VIEW);
                    ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFile.aspx');", true);
                }
                else if (Navigation.SearchType == Utility.Searchtype.History)
                {
                    ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFileHistory.aspx');", true);
                }
            }
            else if (e.CommandName == "Index")
            {
                string[] indexes = e.CommandArgument.ToString().Split('/');
                Navigation.ViewParameters2 = indexes[1];
                if (indexes.Length == 2)
                {
                    Navigation.ViewParameters = indexes[0] + "-" + this.ddl_ambiente.SelectedValue;
                    this.myFrame.Attributes.Add("src", "SearchHistory.aspx");
                    this.programmaticModalPopup.Show();
                }
                else if (indexes.Length > 2)
                {
                    Navigation.ViewParameters = e.CommandArgument.ToString();
                    CreateLinkButtons();
                    this.hiddenIndex.Value = "1";
                    this.cmdExt.Show();
                }
            }
            else if (e.CommandName == "Note")
            {
                Navigation.ViewParameters = e.CommandArgument.ToString();
                this.LoadNotes();
                this.notes_ext.Show();
            }
            else if (e.CommandName == "Check")
            {
                Navigation.ViewParameters = e.CommandArgument.ToString();
                this.ModalPopupExtender3.Show();
            }
        }

        protected void btn_Click(object sender, EventArgs e)
        {
            ClearLinkButton();
            this.cmdExt.Hide();
            Navigation.ViewParameters = ((LinkButton)sender).Text + "-" + this.ddl_ambiente.SelectedValue;
            this.myFrame.Attributes.Add("src", "SearchHistory.aspx");
            this.programmaticModalPopup.Show();
        }

        protected void siPopup_Click(object sender, EventArgs e)
        {
            this.ModalPopupExtender2.Hide();
        }

        protected void b_yes_Click(object sender, EventArgs e)
        {
            this.CheckReport(Utility.Operation.CHECK_REPORT);
            this.ModalPopupExtender3.Hide();
            LoadReports(this.tb_name.Text, this.tb_jobname.Text, this.tb_jobname2.Text, this.tb_jobnumber.Text, this.tb_data1.Text, this.tb_data2.Text, this.tb_prof.Text, ""); 
        }

        protected void b_close_Click(object sender, EventArgs e)
        {
            this.programmaticModalPopup.Hide();
        }

        protected void b_close2_Click(object sender, EventArgs e)
        {
            ClearLinkButton();
            this.cmdExt.Hide();
        }

        protected void b_close3_Click(object sender, EventArgs e)
        {
            Navigation.CurrentNoteIndex = 0;
            Navigation.TableNotes = null;
            this.dv_notes.DataSource = Navigation.TableNotes;
            this.dv_notes.DataBind();
            this.notes_ext.Hide();
        }

        protected void dv_notes_DataBound(object sender, EventArgs e)
        {
            DetailsView detView = (DetailsView)sender;
            switch (dv_notes.CurrentMode)
            {
                case DetailsViewMode.Edit:
                    this.dv_notes.Fields[7].Visible = false;
                    this.dv_notes.Fields[8].Visible = false;
                    break;
                case DetailsViewMode.Insert:
                    this.dv_notes.Fields[6].Visible = false;
                    this.dv_notes.Fields[8].Visible = false;
                    ((Label)dv_notes.FindControl("tb_JRID2")).Text = Navigation.ViewParameters.Split(';')[0];
                    ((Label)dv_notes.FindControl("tb_repID2")).Text = Navigation.ViewParameters.Split(';')[1];
                    break;
                case DetailsViewMode.ReadOnly:
                    this.dv_notes.Fields[6].Visible = true;
                    this.dv_notes.Fields[7].Visible = true;
                    this.dv_notes.Fields[8].Visible = true;
                    break;
                default:
                    break;
            }
        }

        protected void dv_notes_PageIndexChanging(object sender, DetailsViewPageEventArgs e)
        {
            this.dv_notes.PageIndex = e.NewPageIndex;
            Navigation.CurrentNoteIndex = e.NewPageIndex;
            this.dv_notes.DataSource = Navigation.TableNotes;
            this.dv_notes.DataBind();
        }

        protected void dv_notes_ModeChanging(object sender, DetailsViewModeEventArgs e)
        {
            switch (e.NewMode)
            {
                case DetailsViewMode.Edit:
                    dv_notes.ChangeMode(DetailsViewMode.Edit);
                    this.dv_notes.DataSource = Navigation.TableNotes;
                    this.dv_notes.DataBind();
                    break;
                case DetailsViewMode.ReadOnly:
                    dv_notes.ChangeMode(DetailsViewMode.ReadOnly);
                    this.dv_notes.DataSource = Navigation.TableNotes;
                    this.dv_notes.DataBind();
                    break;
                case DetailsViewMode.Insert:
                    dv_notes.ChangeMode(DetailsViewMode.Insert);
                    break;
            }
        }

        protected void dv_notes_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            try
            {
                this.UpdateNote("", Navigation.User.Username, ((TextBox)dv_notes.FindControl("tb_text2")).Text, Utility.Operation.INSERT);
                dv_notes.ChangeMode(DetailsViewMode.ReadOnly);
                if (Navigation.TableNotes != null)
                    Navigation.CurrentNoteIndex = Navigation.TableNotes.Rows.Count;
                else
                    Navigation.CurrentNoteIndex = 1;
                this.LoadNotes();              
            }
            catch (Exception Ex)
            {
                log.Error("Failed to insert notes: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        protected void dv_notes_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            try
            {
                this.UpdateNote(((Label)dv_notes.FindControl("tb_ver2")).Text, Navigation.User.Username, ((TextBox)dv_notes.FindControl("tb_text2")).Text, Utility.Operation.UPDATE);
                dv_notes.ChangeMode(DetailsViewMode.ReadOnly);
                this.LoadNotes();
            }
            catch (Exception Ex)
            {
                log.Error("Failed to update notes: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        protected void dv_notes_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
        {
            this.ConfDel_ext.Show();
        }

        protected void currPopup_Click(object sender, EventArgs e)
        {
            try
            {
                this.UpdateNote(((Label)dv_notes.FindControl("tb_ver1")).Text, Navigation.User.Username, "", Utility.Operation.DELETE);
                dv_notes.ChangeMode(DetailsViewMode.ReadOnly);
                Navigation.CurrentNoteIndex = 0;
                this.ConfDel_ext.Hide();
                this.LoadNotes();
                if (Navigation.TableNotes == null)
                    this.notes_ext.Hide();
                else
                    this.notes_ext.Show();
            }
            catch (Exception Ex)
            {
                log.Error("Failed to update notes: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        protected void allPopup_Click(object sender, EventArgs e)
        {
            try
            {
                this.UpdateNote("-1", Navigation.User.Username, "", Utility.Operation.DELETE);
                dv_notes.ChangeMode(DetailsViewMode.ReadOnly);
                Navigation.CurrentNoteIndex = 0;
                Navigation.TableNotes = null;
                this.dv_notes.DataSource = Navigation.TableNotes;
                this.dv_notes.DataBind();
                this.ConfDel_ext.Hide();
                this.notes_ext.Hide();
            }
            catch (Exception Ex)
            {
                log.Error("Failed to update notes: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }
        #endregion

        #region OLD

        //protected void gv_search_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Pager)
        //    {
        //        GridViewRow gvr = e.Row;
        //        LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //        lb = (LinkButton)gvr.Cells[0].FindControl("p1");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //        lb = (LinkButton)gvr.Cells[0].FindControl("p2");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //        lb = (LinkButton)gvr.Cells[0].FindControl("p4");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //        lb = (LinkButton)gvr.Cells[0].FindControl("p5");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //        lb = (LinkButton)gvr.Cells[0].FindControl("p6");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //        lb = (LinkButton)gvr.Cells[0].FindControl("LinkButton1");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //        lb = (LinkButton)gvr.Cells[0].FindControl("LinkButton4");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //    }
        //}

        //void lb_Command2(object sender, CommandEventArgs e)
        //{
        //    int range1 = Utility.GetPageRange(Navigation.CurrentGridPage);
        //    int range2 = Utility.GetPageRange(Convert.ToInt32(e.CommandArgument));
        //    Navigation.CurrentGridPage = Convert.ToInt32(e.CommandArgument) - 1;
        //    if (range1 != range2)
        //        LoadReports(this.tb_name.Text, this.tb_jobname.Text, this.tb_jobname2.Text, this.tb_jobnumber.Text, this.tb_data1.Text, this.tb_data2.Text, this.tb_prof.Text, "");
        //    else
        //        BindGridView();
        //}

        //protected void gv_search_DataBound2(object sender, EventArgs e)
        //{
        //    int numPages = 0;
        //    if (Navigation.TableSource != null)
        //    {
        //        //double pages = (double)Navigation.TableSource.Rows.Count / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);               
        //        double pages = (double)Navigation.TotReports / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
        //        numPages = (int)Math.Ceiling(pages);
        //        int range = Utility.GetPageRange(Navigation.CurrentGridPage + 1);

        //        GridViewRow gvrow = this.gv_search.BottomPagerRow;
        //        LinkButton lnk = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //        lnk.Text = "" + numPages + "";
        //        lnk.CommandArgument = "" + numPages + "";
        //        lnk = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //        lnk.CommandArgument = "" + 1 + "";
        //        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        //        lblcurrentpage.Text = Convert.ToString(Navigation.CurrentGridPage + 1);

        //        //int[] page = new int[7];
        //        //page[0] = this.gv_search.PageIndex - 2;
        //        //page[1] = this.gv_search.PageIndex - 1;
        //        //page[2] = this.gv_search.PageIndex;
        //        //page[3] = this.gv_search.PageIndex + 1;
        //        //page[4] = this.gv_search.PageIndex + 2;
        //        //page[5] = this.gv_search.PageIndex + 3;
        //        //page[6] = this.gv_search.PageIndex + 4;
        //        int[] pageText = new int[7];
        //        pageText[0] = Navigation.CurrentGridPage - 2;
        //        pageText[1] = Navigation.CurrentGridPage - 1;
        //        pageText[2] = Navigation.CurrentGridPage;
        //        pageText[3] = Navigation.CurrentGridPage + 1;
        //        pageText[4] = Navigation.CurrentGridPage + 2;
        //        pageText[5] = Navigation.CurrentGridPage + 3;
        //        pageText[6] = Navigation.CurrentGridPage + 4;
        //        for (int i = 0; i < 7; i++)
        //        {
        //            if (i != 3)
        //            {
        //                if (pageText[i] < 1)//|| page[i] > numPages)
        //                {
        //                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //                    lnkbtn.Visible = false;
        //                }
        //                else
        //                {
        //                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //                    lnkbtn.Text = Convert.ToString(pageText[i]);
        //                    lnkbtn.CommandName = "PageNo";
        //                    lnkbtn.CommandArgument = Convert.ToString(pageText[i]);
        //                }
        //            }
        //        }

        //        //if (this.gv_search.PageIndex == 0)
        //        //{
        //        //    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //        //    lnkbtn.Visible = false;
        //        //}
        //        //if (this.gv_search.PageIndex == this.gv_search.PageCount - 1)
        //        //{
        //        //    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //        //    lnkbtn.Visible = false;
        //        //}
        //        //if (this.gv_search.PageIndex > this.gv_search.PageCount - 6)
        //        //{
        //        //    Label lbmore = (Label)gvrow.Cells[0].FindControl("nmore");
        //        //    lbmore.Visible = false;
        //        //}
        //        //if (this.gv_search.PageIndex > this.gv_search.PageCount - 5)
        //        //{
        //        //    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //        //    lnkbtn.Visible = false;
        //        //}
        //        //if (this.gv_search.PageIndex < 5)
        //        //{
        //        //    Label lbmore = (Label)gvrow.Cells[0].FindControl("pmore");
        //        //    lbmore.Visible = false;
        //        //}
        //        //if (this.gv_search.PageIndex < 4)
        //        //{
        //        //    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //        //    lnkbtn.Visible = false;
        //        //}

        //        if (Navigation.CurrentGridPage == 1)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //            lnkbtn.Visible = false;
        //        }
        //        if (Navigation.CurrentGridPage == numPages - 1)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //            lnkbtn.Visible = false;
        //        }
        //        if (Navigation.CurrentGridPage > numPages - 6)
        //        {
        //            Label lbmore = (Label)gvrow.Cells[0].FindControl("nmore");
        //            lbmore.Visible = false;
        //        }
        //        if (Navigation.CurrentGridPage > numPages - 5)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //            lnkbtn.Visible = false;
        //        }
        //        if (Navigation.CurrentGridPage < 5)
        //        {
        //            Label lbmore = (Label)gvrow.Cells[0].FindControl("pmore");
        //            lbmore.Visible = false;
        //        }
        //        if (Navigation.CurrentGridPage < 4)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //            lnkbtn.Visible = false;
        //        }
        //    }
        //}

        //void lb_Command(object sender, CommandEventArgs e)
        //{
        //    Navigation.CurrentGridPage = Convert.ToInt32(e.CommandArgument) - 1;
        //    BindGridView();
        //}

        //protected void gv_search_DataBound(object sender, EventArgs e)
        //{
        //    int numPages = 0;
        //    if (Navigation.TableSource != null && Navigation.TableSource.Rows.Count > 0)
        //    {
        //        double pages = (double)Navigation.TableSource.Rows.Count / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
        //        numPages = (int)Math.Ceiling(pages);

        //        GridViewRow gvrow = this.gv_search.BottomPagerRow;
        //        LinkButton lnk = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //        lnk.Text = "" + numPages + "";
        //        lnk.CommandArgument = numPages.ToString();
        //        lnk = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //        lnk.CommandArgument = "1";
        //        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        //        lblcurrentpage.Text = Convert.ToString(this.gv_search.PageIndex + 1);
        //        int[] page = new int[7];
        //        page[0] = this.gv_search.PageIndex - 2;
        //        page[1] = this.gv_search.PageIndex - 1;
        //        page[2] = this.gv_search.PageIndex;
        //        page[3] = this.gv_search.PageIndex + 1;
        //        page[4] = this.gv_search.PageIndex + 2;
        //        page[5] = this.gv_search.PageIndex + 3;
        //        page[6] = this.gv_search.PageIndex + 4;
        //        for (int i = 0; i < 7; i++)
        //        {
        //            if (i != 3)
        //            {
        //                if (page[i] < 1 || page[i] > this.gv_search.PageCount)
        //                {
        //                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //                    lnkbtn.Visible = false;
        //                }
        //                else
        //                {
        //                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //                    lnkbtn.Text = Convert.ToString(page[i]);
        //                    lnkbtn.CommandName = "PageNo";
        //                    lnkbtn.CommandArgument = lnkbtn.Text;
        //                }
        //            }
        //        }
        //        if (this.gv_search.PageIndex == 0)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //            lnkbtn.Visible = false;
        //        }
        //        if (this.gv_search.PageIndex == this.gv_search.PageCount - 1)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //            lnkbtn.Visible = false;
        //        }
        //        if (this.gv_search.PageIndex > this.gv_search.PageCount - 6)
        //        {
        //            Label lbmore = (Label)gvrow.Cells[0].FindControl("nmore");
        //            lbmore.Visible = false;
        //        }
        //        if (this.gv_search.PageIndex > this.gv_search.PageCount - 5)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //            lnkbtn.Visible = false;
        //        }
        //        if (this.gv_search.PageIndex < 5)
        //        {
        //            Label lbmore = (Label)gvrow.Cells[0].FindControl("pmore");
        //            lbmore.Visible = false;
        //        }
        //        if (this.gv_search.PageIndex < 4)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //            lnkbtn.Visible = false;
        //        }
        //    }
        //}

        //protected void gv_searchH_DataBound(object sender, EventArgs e)
        //{
        //    int numPages = 0;
        //    if (Navigation.TableSource != null && Navigation.TableSource.Rows.Count > 0)
        //    {
        //        double pages = (double)Navigation.TableSource.Rows.Count / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
        //        numPages = (int)Math.Ceiling(pages);

        //        GridViewRow gvrow = this.gv_searchH.BottomPagerRow;
        //        LinkButton lnk = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //        lnk.Text = "" + numPages + "";
        //        lnk.CommandArgument = numPages.ToString();
        //        lnk = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //        lnk.CommandArgument = "1";
        //        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        //        lblcurrentpage.Text = Convert.ToString(this.gv_searchH.PageIndex + 1);
        //        int[] page = new int[7];
        //        page[0] = this.gv_searchH.PageIndex - 2;
        //        page[1] = this.gv_searchH.PageIndex - 1;
        //        page[2] = this.gv_searchH.PageIndex;
        //        page[3] = this.gv_searchH.PageIndex + 1;
        //        page[4] = this.gv_searchH.PageIndex + 2;
        //        page[5] = this.gv_searchH.PageIndex + 3;
        //        page[6] = this.gv_searchH.PageIndex + 4;
        //        for (int i = 0; i < 7; i++)
        //        {
        //            if (i != 3)
        //            {
        //                if (page[i] < 1 || page[i] > this.gv_searchH.PageCount)
        //                {
        //                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //                    lnkbtn.Visible = false;
        //                }
        //                else
        //                {
        //                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //                    lnkbtn.Text = Convert.ToString(page[i]);
        //                    lnkbtn.CommandName = "PageNo";
        //                    lnkbtn.CommandArgument = lnkbtn.Text;
        //                }
        //            }
        //        }
        //        if (this.gv_searchH.PageIndex == 0)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //            lnkbtn.Visible = false;
        //        }
        //        if (this.gv_searchH.PageIndex == this.gv_searchH.PageCount - 1)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //            lnkbtn.Visible = false;
        //        }
        //        if (this.gv_searchH.PageIndex > this.gv_searchH.PageCount - 6)
        //        {
        //            Label lbmore = (Label)gvrow.Cells[0].FindControl("nmore");
        //            lbmore.Visible = false;
        //        }
        //        if (this.gv_searchH.PageIndex > this.gv_searchH.PageCount - 5)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //            lnkbtn.Visible = false;
        //        }
        //        if (this.gv_searchH.PageIndex < 5)
        //        {
        //            Label lbmore = (Label)gvrow.Cells[0].FindControl("pmore");
        //            lbmore.Visible = false;
        //        }
        //        if (this.gv_searchH.PageIndex < 4)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //            lnkbtn.Visible = false;
        //        }
        //    }
        //}
        #endregion
    }
}