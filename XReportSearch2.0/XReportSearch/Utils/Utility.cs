﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services.Protocols;
using log4net;
using System.Configuration;
using System.Data;
using XReportSearch.Business;
using XReportSearch.XReportWebIface;
using Newtonsoft.Json.Linq;
using System.Web.UI.HtmlControls;
using System.Reflection;
using System.Globalization;
using CustomControls;
using PlainElastic.Net.Queries;
using System.Text;

namespace XReportSearch.Utils
{
    public class Utility
    {
        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Const
        public static string JOB_NAME = "job_name";
        public static string REPORT_NAME = "report_name";
        public static string WS_NO_DATA = "no data found";
        #endregion

        #region Enum
        public enum Searchtype
        {
            ElasticSearch = 1,
            History = 2,
            Online = 3,
        };

        public enum ControlD
        {
            A1,
            A3,
            A7,
            A8,
        };

        public enum ApplicationType
        {
            BPM,
            UNICREDIT,
            VITTORIA,
            GENERALI
        };

        public enum QuerySearchType
        {
            WS,
            SP,
            SQL
        };

        public enum Operation
        {
            INSERT,
            UPDATE,
            DELETE,
            GET,
            CHECK_REPORT,
            UPDATE_USER_TO_VIEW
        };

        #endregion

        #region Public Methods

        public static void ChangeLogFileName(string appenderName, string newFilename)
        {
            var rootRepository = log4net.LogManager.GetRepository();
            foreach (var appender in rootRepository.GetAppenders())
            {
                if (appender.Name.Equals(appenderName) && appender is log4net.Appender.FileAppender)
                {
                    var fileAppender = appender as log4net.Appender.FileAppender;
                    fileAppender.File = newFilename;
                    fileAppender.ActivateOptions();
                }
            }
        }

        public static DataTable IndexEntriesToTable(IndexEntry[] Entries)
        {
            DataTable dt = new DataTable();
            if (Entries != null)
            {
                if (Entries[0].Columns[0].colname == "ActionMessage" && Entries[0].Columns[0].Value.ToLower().StartsWith(WS_NO_DATA))
                    return null;
                if (Entries.Length > 1)
                {
                    DataColumn dc;
                    DataRow dr;
                    dt.Columns.Add(new DataColumn("JobReportID"));
                    dt.Columns.Add(new DataColumn("FromPage"));
                    dt.Columns.Add(new DataColumn("ForPages"));
                    if (Entries[1].Columns != null)
                    {
                        foreach (column col in Entries[1].Columns)
                        {
                            dc = new DataColumn(col.colname);
                            dt.Columns.Add(dc);
                        }
                    }
                    foreach (IndexEntry entry in Entries)
                    {
                        if (entry.Columns != null && entry.Columns[0].colname != "ActionMessage")
                        {
                            dr = dt.NewRow();
                            foreach (column col in entry.Columns)
                            {
                                dr[col.colname] = col.Value;
                            }
                            dr["JobReportID"] = entry.JobReportId;
                            dr["FromPage"] = entry.FromPage;
                            dr["ForPages"] = entry.ForPages;
                            dt.Rows.Add(dr);
                        }
                    }
                }
            }
            return dt;
        }

        public static DataTable IndexEntriesToTable(IndexEntry[] Entries, Encoding inputEncoding)
        {
            
            DataTable dt = new DataTable();
            if (Entries != null)
            {
                if (Entries[0].Columns[0].colname == "ActionMessage" && Entries[0].Columns[0].Value.ToLower().StartsWith(WS_NO_DATA))
                    return null;
                if (Entries.Length > 1)
                {
                    DataColumn dc;
                    DataRow dr;
                    dt.Columns.Add(new DataColumn("JobReportID"));
                    dt.Columns.Add(new DataColumn("FromPage"));
                    dt.Columns.Add(new DataColumn("ForPages"));
                    if (Entries[1].Columns != null)
                    {
                        foreach (column col in Entries[1].Columns)
                        {
                            dc = new DataColumn(col.colname);
                            dt.Columns.Add(dc);
                        }
                    }
                    foreach (IndexEntry entry in Entries)
                    {
                        if (entry.Columns != null && entry.Columns[0].colname != "ActionMessage")
                        {
                            dr = dt.NewRow();
                            foreach (column col in entry.Columns)
                            {
                                 
                                if(col.colname.Equals("textString"))
                                { 
                                    byte[] outputBytes = Convert.FromBase64String(col.Value);  
                                    string msg = inputEncoding.GetString(outputBytes); 
                                dr[col.colname] = msg;
                                }
                                else
                                {
                                    dr[col.colname] = col.Value;
                                }

                                
                            }
                            dr["JobReportID"] = entry.JobReportId;
                            dr["FromPage"] = entry.FromPage;
                            dr["ForPages"] = entry.ForPages;
                            dt.Rows.Add(dr);
                        }
                    }
                }
            }
            return dt;
        }

        public static DataTable CreateTable(int totPages, int jobReportid, int reportid, string lop, string reportName, string userTimeRef, string xferStartTime,string jobname, string jobnum,string foldername, string sortDir)
        {
            if (totPages == 0)
            {
                return null;
            }
            /*
            int rangePages = Int32.Parse(ConfigurationManager.AppSettings["RangePages"]);

            DataTable dt = new DataTable();
            DataRow dr;
            dt.Columns.Add(new DataColumn("Range_Pages"));
            dt.Columns.Add(new DataColumn("JobReportID"));
            dt.Columns.Add(new DataColumn("ReportId"));
            dt.Columns.Add(new DataColumn("ListOfPages"));
            dt.Columns.Add(new DataColumn("FromPage"));
            dt.Columns.Add(new DataColumn("ForPages"));
            dt.Columns.Add(new DataColumn("ReportName"));
            dt.Columns.Add(new DataColumn("UserTimeRef"));
            dt.Columns.Add(new DataColumn("XferStartTime"));
            dt.Columns.Add(new DataColumn("TotPages"));

            int fromPage = 1;
            int forPages = rangePages;
            int toPage = fromPage + forPages - 1;
            while (fromPage <= totPages)
            {
                dr = dt.NewRow();
                dr["Range_Pages"] = "Page " + fromPage + " to " + toPage;
                dr["JobReportID"] = jobReportid;
                dr["ReportId"] = reportid;
                dr["ListOfPages"] = "" + fromPage + "," + forPages;
                dr["FromPage"] = fromPage;
                dr["ForPages"] = forPages;
                dr["ReportName"] = reportName;
                dr["UserTimeRef"] = userTimeRef;
                dr["XferStartTime"] = xferStartTime;
                dr["TotPages"] = totPages;
                dt.Rows.Add(dr);
                //}
                fromPage = toPage + 1;
                forPages = rangePages;
                toPage = fromPage + forPages - 1;
                if (toPage > totPages)
                {
                    toPage = totPages;
                    forPages = totPages - fromPage + 1;
                }
            } 
            */
            int[] partArray = Array.ConvertAll(lop.Split(','), int.Parse).ToList<int>().ToArray();
            List<int> partList = Array.ConvertAll(lop.Split(','), int.Parse).ToList<int>();
            DataTable dt = new DataTable();
            int rangePages = Int32.Parse(ConfigurationManager.AppSettings["RangePages"]);
            int entries = (totPages / rangePages);
            int lastseg = (totPages % rangePages);
            if (entries > 0)
            {
             
                    DataRow dr;
                    dt.Columns.Add(new DataColumn("Range_Pages"));
                    dt.Columns.Add(new DataColumn("JobReportID"));
                    dt.Columns.Add(new DataColumn("ReportId"));
                    dt.Columns.Add(new DataColumn("ListOfPages"));
                    dt.Columns.Add(new DataColumn("FromPage"));
                    dt.Columns.Add(new DataColumn("ForPages"));
                    dt.Columns.Add(new DataColumn("ReportName"));
                    dt.Columns.Add(new DataColumn("UserTimeRef"));
                    dt.Columns.Add(new DataColumn("XferStartTime"));
                    dt.Columns.Add(new DataColumn("JobName"));
                    dt.Columns.Add(new DataColumn("JobNumber"));
                    dt.Columns.Add(new DataColumn("FolderName"));
                    dt.Columns.Add(new DataColumn("TotPages"));
                    dr = dt.NewRow();
                    dr["Range_Pages"] = "All Pages (attention! file could be too big)  ";
                    dr["JobReportID"] = jobReportid;
                    dr["ReportId"] = reportid;
                    dr["ListOfPages"] = lop;
                    dr["FromPage"] = 1;  // dr["FromPage"] = 1 + ((i - 1) * rangePages);
                    dr["ForPages"] = totPages;
                    dr["ReportName"] = reportName;
                    dr["UserTimeRef"] = userTimeRef;
                    dr["XferStartTime"] = xferStartTime;
                    dr["JobName"] = jobname;
                    dr["JobNumber"] = jobnum;
                    dr["FolderName"] = foldername;
                    dr["TotPages"] = totPages;
                    dt.Rows.Add(dr);
                    int sumPart = 1; int k = 0; int nextFrom = 0; int nextFor = 0;
                    for (int i = 1; i  <= entries; i++ )
                    {
                        //sumPart += partList.ElementAt(k + 1);// k += 2;
                        if (partList.ElementAt(k + 1) > (i * rangePages) || sumPart > (i * rangePages))
                        {
                            //if (nextFrom == 0) { nextFrom = (1 + ((i - 1) * rangePages)); nextFor = rangePages; }
                            if (nextFrom == 0) { nextFrom = partList.ElementAt(k); nextFor = rangePages; }
                            dr = dt.NewRow();
                            dr["Range_Pages"] = "Page " + (1 + ((i - 1) * rangePages) + " to " + (i * rangePages));
                            dr["JobReportID"] = jobReportid;
                            dr["ReportId"] = reportid;
                            dr["ListOfPages"] = "" + nextFrom + "," + nextFor;
                            dr["FromPage"] = nextFrom;  // dr["FromPage"] = 1 + ((i - 1) * rangePages);
                            dr["ForPages"] = rangePages;
                            dr["ReportName"] = reportName;
                            dr["UserTimeRef"] = userTimeRef;
                            dr["XferStartTime"] = xferStartTime;
                            dr["JobName"] = jobname;
                            dr["JobNumber"] = jobnum;
                            dr["FolderName"] = foldername;
                            dr["TotPages"] = totPages;
                            dt.Rows.Add(dr);
                            sumPart += nextFor;
                            //nextFrom = 1 + (i * rangePages); nextFor = rangePages;
                            nextFrom = partList.ElementAt(k) + (i * rangePages); nextFor = rangePages;
                        }
                        else {

                            dr = dt.NewRow();
                            dr["Range_Pages"] = "Page " + (1 + ((i - 1) * rangePages) + " to " + (i * rangePages));
                            dr["JobReportID"] = jobReportid;
                            dr["ReportId"] = reportid;
                            dr["ListOfPages"] = "";
                            if (nextFrom > 1) dr["ListOfPages"] += nextFrom + "," + nextFor;
                            dr["FromPage"] = nextFrom; // dr["FromPage"] = 1 + ((i - 1) * rangePages);
                            dr["ForPages"] = nextFor;
                            dr["ReportName"] = reportName;
                            dr["UserTimeRef"] = userTimeRef;
                            dr["XferStartTime"] = xferStartTime;
                            dr["JobName"] = jobname;
                            dr["JobNumber"] = jobnum;
                            dr["FolderName"] = foldername;
                            dr["TotPages"] = totPages;
                            
                            while(((1 + ((i - 1) * rangePages)) <= sumPart) && (sumPart <= (i * rangePages))){
                                nextFrom = partList.ElementAt(k); 
                                nextFor = partList.ElementAt(k + 1); sumPart += nextFor;
                                if ((sumPart -1 ) < (i * rangePages))
                                {
                                    dr["ListOfPages"] += "" + partList.ElementAt(k) + "," + partList.ElementAt(k + 1);
                                    if (sumPart != (i * rangePages)) dr["ListOfPages"] += ",";
                                }
                                k += 2;
                            }
                            dr["ListOfPages"] += "" + nextFrom + "," + (nextFor - ((sumPart - 1) - i * rangePages));
                            nextFrom = nextFrom + (sumPart - i * rangePages) ;
                            nextFor = ((sumPart - 1) - i * rangePages);
                            dt.Rows.Add(dr);
                        }
                    }
                    if (lastseg > 0) {
                        dr = dt.NewRow();
                        if (nextFor > lastseg) { nextFor = lastseg; sumPart += lastseg; }
                        dr["Range_Pages"] = "Page " + (1 + (entries * rangePages)) + " to " + ((entries * rangePages) + lastseg);
                        dr["JobReportID"] = jobReportid;
                        dr["ReportId"] = reportid;
                        dr["ListOfPages"] = "" + nextFrom + "," + nextFor;
                        dr["FromPage"] = 1 + (entries * rangePages);
                        dr["ForPages"] = lastseg;
                        dr["ReportName"] = reportName;
                        dr["UserTimeRef"] = userTimeRef;
                        dr["XferStartTime"] = xferStartTime;
                        dr["JobName"] = jobname;
                        dr["JobNumber"] = jobnum;
                        dr["FolderName"] = foldername;
                        dr["TotPages"] = totPages;
                        while (sumPart <= (entries * rangePages) + lastseg)
                        {
                            
                            dr["ListOfPages"] += "," + partList.ElementAt(k) + "," + partList.ElementAt(k + 1);
                            nextFrom = partList.ElementAt(k); sumPart += partList.ElementAt(k + 1);
                            k += 2;
                        }
                        dt.Rows.Add(dr);    
                    }
            }
            return dt;
        }

        public static DataTable IndexEntriesToTable2(IndexEntry[] Entries)
        {
            DataTable dt = new DataTable();
            if (Entries != null)
            {
                if (Entries.Length > 0)
                {
                    DataColumn dc;
                    DataRow dr;
                    dt.Columns.Add(new DataColumn("JobReportID"));
                    dt.Columns.Add(new DataColumn("FromPage"));
                    dt.Columns.Add(new DataColumn("ForPages"));
                    if (Entries[0].Columns != null)
                    {
                        foreach (column col in Entries[0].Columns)
                        {
                            dc = new DataColumn(col.colname);
                            dt.Columns.Add(dc);
                        }
                    }
                    foreach (IndexEntry entry in Entries)
                    {
                        if (entry.Columns != null && entry.Columns[0].colname != "ActionMessage")
                        {
                            dr = dt.NewRow();
                            foreach (column col in entry.Columns)
                            {
                                dr[col.colname] = col.Value;
                            }
                            dr["JobReportID"] = entry.JobReportId;
                            dr["FromPage"] = entry.FromPage;
                            dr["ForPages"] = entry.ForPages;
                            dt.Rows.Add(dr);
                        }
                    }
                }
            }
            return dt;
        }

        public static DataTable Fields2Table(string fields)
        {
            DataTable dt = new DataTable();
            string[] cols = fields.Split(',');
            DataColumn dc;
            DataRow dr;
            dc = new DataColumn("Field");
            dt.Columns.Add(dc);
            dc = new DataColumn("Type");
            dt.Columns.Add(dc);
            dc = new DataColumn("Value");
            dt.Columns.Add(dc);
            dc = new DataColumn("Primary");
            dt.Columns.Add(dc);
            for (int i = 0; i < cols.Length; i++)
            {
                string[] field = cols[i].Split('|');
                dr = dt.NewRow();
                dr["Field"] = field[0];
                dr["Type"] = field[1];
                if (i == 0)
                    dr["Primary"] = "1";
                else
                    dr["Primary"] = "0";
                dt.Rows.Add(dr);
            }
            return dt;
        }

        
        public static List<HtmlTableRow> BuildControl(string typeControl, string name)
        {
            return BuildControl(typeControl, name, "");
        }

        public static List<HtmlTableRow> BuildControl(string typeControl, string name, string elements_to_block)
        {
            List<HtmlTableRow> rows = new List<HtmlTableRow>();
            HtmlTableRow row = new HtmlTableRow();
            row.Attributes.Add("class", "data");
            HtmlTableCell cell = new HtmlTableCell();
            HtmlTableCell cellControl = new HtmlTableCell();
            HtmlTableRow rowTo;
            HtmlTableCell cellTo1;
            HtmlTableCell cellTo2;
            Field f = new Field(name);
            Navigation.Search.fields.Add(f);
            if (name.EndsWith("#"))
            {
                name = name.TrimEnd('#') + "_";
                f.Name = name;
            }

            switch (typeControl.ToLower())
            {
                case "string":
                    cell.Controls.Add(Utility.CreateLabel(name, name));
                    row.Cells.Add(cell);
                    TextBox tb = Utility.CreateTextBox(name, false, name);
                    Boolean block = false;
                    foreach (string element in elements_to_block.Split(';'))
                    {
                        if (name.Equals(element, StringComparison.OrdinalIgnoreCase))
                        {
                            block = true;
                            break;
                        }
                    }
                    //if (name.Equals("job_name"))
                    //    tb.ReadOnly = true;
                    

                    cellControl.Controls.Add(tb);
                    tb.Attributes.Add("autocomplete", "off");
                    if(block)
                    {
                        tb.ReadOnly = true;
                        tb.Enabled = false;
                    }
                    else
                        cellControl.Controls.Add(CreateAutoComplete(REPORT_NAME, name));
                    break;
                case "date":
                    cell.Controls.Add(Utility.CreateLabel(name + "_From", name + " From"));
                    row.Cells.Add(cell);
                    rowTo = new HtmlTableRow();
                    rowTo.Attributes.Add("class", "data");
                    cellTo1 = new HtmlTableCell();
                    cellTo1.Controls.Add(Utility.CreateLabel(name + "_To", name + " To"));
                    rowTo.Cells.Add(cellTo1);
                    cellTo2 = new HtmlTableCell();
                    CreateDate(ref cellControl, name + "_From", name);
                    CreateDate(ref cellTo2, name + "_To", name);
                    rowTo.Cells.Add(cellTo2);
                    rows.Add(rowTo);
                    f.IsDate = true;
                    break;
                case "integer":
                case "long":
                    cell.Controls.Add(Utility.CreateLabel(name + "_From", name + " From"));
                    row.Cells.Add(cell);
                    cellControl.Controls.Add(Utility.CreateTextBox(name + "_From", false, name));
                    cellControl.Controls.Add(Utility.CreateNumericMask(name + "_From"));
                    rowTo = new HtmlTableRow();
                    rowTo.Attributes.Add("class", "data");
                    cellTo1 = new HtmlTableCell();
                    cellTo1.Controls.Add(Utility.CreateLabel(name + "_To", name + " To"));
                    rowTo.Cells.Add(cellTo1);
                    cellTo2 = new HtmlTableCell();
                    cellTo2.Controls.Add(Utility.CreateTextBox(name + "_To", false, name));
                    cellTo2.Controls.Add(Utility.CreateNumericMask(name + "_To"));
                    rowTo.Cells.Add(cellTo2);
                    rows.Add(rowTo);
                    f.IsNumeric = true;
                    break;
                case "double":
                    cell.Controls.Add(Utility.CreateLabel(name + "_From", name + " From"));
                    row.Cells.Add(cell);
                    cellControl.Controls.Add(Utility.CreateTextBox(name + "_From", false, name));
                    cellControl.Controls.Add(Utility.CreateNumericFloatMask(name + "_From"));
                    rowTo = new HtmlTableRow();
                    rowTo.Attributes.Add("class", "data");
                    cellTo1 = new HtmlTableCell();
                    cellTo1.Controls.Add(Utility.CreateLabel(name + "_To", name + " To"));
                    rowTo.Cells.Add(cellTo1);
                    cellTo2 = new HtmlTableCell();
                    cellTo2.Controls.Add(Utility.CreateTextBox(name + "_To", false, name));
                    cellTo2.Controls.Add(Utility.CreateNumericFloatMask(name + "_To"));
                    rowTo.Cells.Add(cellTo2);
                    rows.Add(rowTo);
                    f.IsNumeric = true;
                    break;
                default:
                    break;
            }
            row.Cells.Add(cellControl);
            rows.Insert(0, row);
            return rows;
        }

        
        public static List<HtmlTableRow> BuildForm(JObject listaTipi, string sottoIndice, ref CeGridView grid)
        { 
            return BuildForm(listaTipi, sottoIndice, ref grid, "");
        }

        public static List<HtmlTableRow> BuildForm(JObject listaTipi, string sottoIndice, ref CeGridView grid, string elements_to_block)
        {
            
            List<HtmlTableRow> rows = new List<HtmlTableRow>();
            foreach (JProperty tipo in listaTipi.Properties())
            {
                if (tipo.Name == sottoIndice)
                {
                    JObject root = JObject.Parse(tipo.Value.ToString());
                    JToken items = root["properties"];
                    JToken token = items.First;
                    while (token != null)
                    {
                        System.Diagnostics.Debug.WriteLine(((JProperty)token).Name.ToString() + " : " + ((JProperty)token).Value.ToString() + "<br />");
                        JToken subToken = JObject.Parse(((JProperty)token).Value.ToString());
                        string type = (string)subToken.SelectToken("type");
                        string index = (string)subToken.SelectToken("index");
                        if (((JProperty)token).Name.ToString() == "index_name" || (index != null && index.ToLower() == "no") ||
                            (((JProperty)token).Name.ToString() == "last_page" || ((JProperty)token).Name.ToString() == "last_rba" ||
                            ((JProperty)token).Name.ToString() == "from_rba" || ((JProperty)token).Name.ToString() == "from_page" ||
                            ((JProperty)token).Name.ToString() == "tot_pages"))
                        {
                            token = token.Next;
                        }
                        else
                        {
                            List<HtmlTableRow> rowsControls = BuildControl(type, ((JProperty)token).Name.ToString(), elements_to_block);                            
                            foreach (HtmlTableRow row in rowsControls)
                            {
                                if (((JProperty)token).Name.ToString() == "job_name" || ((JProperty)token).Name.ToString() == "report_name")
                                    rows.Insert(0, row);
                                else
                                    rows.Add(row);
                            }
                            token = token.Next;
                        }
                    }
                }
            }
            return rows;
        }

        public static bool checkSpecial(string spchar, GridView grid) {

            BoundField field = grid.Columns.OfType<BoundField>().Where(f => f.DataField.EndsWith(spchar)).FirstOrDefault();
            return field != null ? true : false;
        
        }

        public static BoundField checkId(string idString, GridView grid)
        {

            BoundField field = grid.Columns.OfType<BoundField>().Where(f => f.DataField.Equals(idString)).FirstOrDefault();
            return field;
        
        }

        public static List<BoundField> BuildGrid(JObject listaTipi, string sottoIndice, GridView grid)
        {
            List<BoundField> fields = new List<BoundField>();
            foreach (JProperty tipo in listaTipi.Properties())
            {
                if (tipo.Name == sottoIndice)
                {
                    JObject root = JObject.Parse(tipo.Value.ToString());
                    JToken items = root["properties"];
                    JToken token = items.First;
                    while (token != null)
                    {
                        System.Diagnostics.Debug.WriteLine(((JProperty)token).Name.ToString() + " : " + ((JProperty)token).Value.ToString() + "<br />");
                        JToken subToken = JObject.Parse(((JProperty)token).Value.ToString());
                        string type = (string)subToken.SelectToken("type");
                        string index = (string)subToken.SelectToken("index");
                        if ((((JProperty)token).Name.ToString() == "index_name" || index != null && index.ToLower() == "no") && ((JProperty)token).Name.ToString() != "tot_pages")
                        {
                            token = token.Next;
                        }
                        else
                        {
                            BoundField field = grid.Columns.OfType<BoundField>().Where(f => f.DataField == comparableString(((JProperty)token).Name.ToString())).FirstOrDefault();
                            if (field == null)
                            {
                                field = Utility.BuildBoundField(((JProperty)token).Name.ToString());
                                fields.Add(field);
                            }
                            token = token.Next;
                        }
                    }
                }
            }
            return fields;
        }

        public static TemplateField BuildButtonColumn()
        {
            TemplateField tf = new TemplateField();
            tf.HeaderStyle.Width = new Unit(5, UnitType.Percentage);
            tf.ItemStyle.Width = new Unit(5, UnitType.Percentage);
            tf.ItemTemplate = new ButtonTemplate();
            tf.HeaderText = "pdf";
            return tf;
        }

        public static string ParseDate(string date, string format)
        {
            DateTime dateTime = DateTime.Parse(date);
            return String.Format(format, dateTime);
        }

        public static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        static public string DecodeFrom64(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
            return returnValue;
        }

        public static string BuildQueryAutocompleteOLD(string field, string prefix)
        {
            string elasticQuery = new QueryBuilder<Search>()
                .Query(q => q
                     .Bool(b => b
                        .Must(m => m
                            .Text(text => text
                                .Field(field)
                                .Query(prefix.ToUpper())
                                .Type(TextQueryType.phrase_prefix))
                        )
                    )
                )
                .Facets(facets => facets
                    .Terms(terms => terms
                        .FacetName("suggest")
                        .Size(100)
                        .Field(field)
                    )
                )
                .Size(0)
                .BuildBeautified();
            log.Debug("ELASTIC SEARCH QUERY: " + elasticQuery);
            return elasticQuery;
        }

        public static string BuildQueryAutocomplete(string field, string prefix)
        {
            string elasticQuery =  
                "{ " + 
                    "\"query\": { " + 
                        "\"bool\": { " + 
                            "\"must\": [ " + 
                                "{ " + 
                                    "\"prefix\": { " +
                                        "\"" + field + "\": \"" + prefix + "\" " + 
                                    "} " + 
                                "} " + 
                            "] " + 
                        "} " + 
                    "}, " + 
                    "\"aggregations\": { " + 
                        "\"suggest\": { " + 
                            "\"terms\": { " + 
                                "\"size\": 100, " + 
                                "\"field\": \"" + field + "\" " + 
                            "} " + 
                        "} " + 
                    "}, " + 
                    "\"size\": 0 " + 
                "} ";
            log.Debug("ELASTIC SEARCH QUERY: " + elasticQuery);
            return elasticQuery;
        }

        public static string BuildQueryMultipleAutocomplete(string field, string[] arrayPrefix)
        {
            string elasticQuery =
                "{ " +
                    "\"query\": { " +
                        "\"bool\": { " +
                            "\"must\": [ " ;
                            for (int j=0; j< arrayPrefix.Length; j=j+2){    
                            elasticQuery += 
                                "{ " +
                                    "\"prefix\": { " +
                                        "\"" + arrayPrefix[j] + "\": \"" + arrayPrefix[j+1] + "\" " +
                                    "} " +
                                "} ,";
                            };
                            elasticQuery= elasticQuery.Remove(elasticQuery.Length - 1);//Substring(-1);
                            elasticQuery +=  "] " +
                        "} " +
                    "}, " +
                    "\"aggregations\": { " +
                        "\"suggest\": { " +
                            "\"terms\": { " +
                                "\"size\": 100, " +
                                "\"field\": \"" + field + "\" " +
                            "} " +
                        "} " +
                    "}, " +
                    "\"size\": 0 " +
                "} ";
            //log.Debug("ELASTIC SEARCH QUERY: " + elasticQuery);
            return elasticQuery;
        }

        public static int GetPageRange(int page)
        {
            double range = ((double)page * (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"])) / 1000;
            if ((int)Math.Ceiling(range) == 0)
                return 1;
            else
                return (int)Math.Ceiling(range);
        }

        public static int GetRealPage(int page)
        {
            if (page <= 40)
                return page;
            else
                return (page % 40);
        }

        public static bool ConvertStringToBool(string value)
        {
            if (value == "1")
            {
                return true;
            }
            else if (value == "0")
            {
                return false;
            }
            else
            {
                throw new FormatException("The string is not a recognized as a valid boolean value.");
            }
        }

        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        #endregion

        #region Dynamic Controls Methods
        public static void CreateDate(ref HtmlTableCell cellControl, string name, string origName)
        {
            TextBox tb = Utility.CreateTextBox(name, true, origName);
            cellControl.Controls.Add(tb);
            Label lb = new Label();
            lb.Width = new Unit(6, UnitType.Pixel);
            cellControl.Controls.Add(lb);
            ImageButton cal = Utility.CreateCalendarButton(name);
            cellControl.Controls.Add(cal);
            cellControl.Controls.Add(Utility.CreateCalendar(name, cal.ID));
            AjaxControlToolkit.MaskedEditExtender mask = Utility.CreateDataMask(name);
            cellControl.Controls.Add(mask);
            cellControl.Controls.Add(Utility.CreateDataValidator(name, mask.ID));
        }

        public static Label CreateLabel(string name, string text)
        {
            Label lb = new Label();
            lb.ID = name + "_l";
            lb.Text = FirstCharToUpper(text.Replace("_", " "));
            lb.SkinID = "LabelFilter";
            lb.EnableViewState = false;
            return lb;
        }

        public static TextBox CreateTextBox(string name, bool isDate, string origName)
        {
            TextBox tb = new TextBox();
            tb.ID = name;
            if (isDate)
                tb.SkinID = "TextboxDate";
            else
                tb.SkinID = "TextboxSmall";
            tb.EnableViewState = false;
            if (tb.ID.Contains("_From"))
                tb.Attributes.Add("onchange", "TextChanged('" + tb.ClientID + "')");
            string[] val = Navigation.ViewParameters2.Split(';');
            string[] values = val[0].Split(',');
            if (name == "job_name")
            {
                tb.Text = values[0];
                Field field = Navigation.Search.fields.Find(f => f.Name == origName);
                field.Value = tb.Text;
            }
            else if (name == "report_name")
            {
                tb.Text = values[1];
                Field field = Navigation.Search.fields.Find(f => f.Name == origName);
                field.Value = tb.Text;
            }
            else if (name == "date_elab_From")
            {
                tb.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Parse(values[2].ToString()));
                Field field = Navigation.Search.fields.Find(f => f.Name == origName);
                field.Value_from = tb.Text;
            }
            else if (name == "date_elab_To")
            {
                tb.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Parse(values[2].ToString()));
                Field field = Navigation.Search.fields.Find(f => f.Name == origName);
                field.Value_to = tb.Text;
            }
            tb.TextChanged += new EventHandler(TextBox_TextChanged);
            return tb;
        }

        public static ImageButton CreateCalendarButton(string name)
        {
            ImageButton cal = new ImageButton();
            cal.ID = name + "calButton";
            cal.ImageUrl = "~/Images/calendario.png";
            cal.ImageAlign = ImageAlign.Top;
            cal.EnableViewState = false;
            cal.CausesValidation = false;
            return cal;
        }

        public static AjaxControlToolkit.CalendarExtender CreateCalendar(string name, string calendarName)
        {
            AjaxControlToolkit.CalendarExtender calendar = new AjaxControlToolkit.CalendarExtender();
            calendar.ID = name + "_calendar";
            calendar.TargetControlID = name;
            calendar.EnableViewState = false;
            calendar.Format = "dd/MM/yyyy";
            calendar.CssClass = "calendar";
            calendar.PopupButtonID = calendarName;
            calendar.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomRight;
            return calendar;
        }

        public static AjaxControlToolkit.FilteredTextBoxExtender CreateNumericMask(string name)
        {
            AjaxControlToolkit.FilteredTextBoxExtender ext = new AjaxControlToolkit.FilteredTextBoxExtender();
            ext.ID = name + "_numeric";
            ext.TargetControlID = name;
            ext.EnableViewState = false;
            ext.FilterType = AjaxControlToolkit.FilterTypes.Numbers;
            return ext;
        }

        private static AjaxControlToolkit.FilteredTextBoxExtender CreateNumericFloatMask(string name)
        {
            AjaxControlToolkit.FilteredTextBoxExtender ext = new AjaxControlToolkit.FilteredTextBoxExtender();
            ext.ID = name + "_numericFloat";
            ext.TargetControlID = name;
            ext.EnableViewState = false;
            ext.FilterType = AjaxControlToolkit.FilterTypes.Custom;
            ext.ValidChars = "0123456789,";
            return ext;
        }

        public static AjaxControlToolkit.MaskedEditExtender CreateDataMask(string name)
        {
            AjaxControlToolkit.MaskedEditExtender ext = new AjaxControlToolkit.MaskedEditExtender();
            ext.ID = name + "_data";
            ext.TargetControlID = name;
            ext.CultureName = "it-IT";
            ext.EnableViewState = false;
            ext.MaskType = AjaxControlToolkit.MaskedEditType.Date;
            ext.Mask = "99/99/9999";
            return ext;
        }

        public static AjaxControlToolkit.MaskedEditValidator CreateDataValidator(string name, string extender)
        {
            AjaxControlToolkit.MaskedEditValidator val = new AjaxControlToolkit.MaskedEditValidator();
            val.ID = name + "_datavalidator";
            val.ControlExtender = extender;
            val.ControlToValidate = name;
            return val;
        }

        public static AjaxControlToolkit.AutoCompleteExtender CreateAutoComplete(string key, string name)
        {
            AjaxControlToolkit.AutoCompleteExtender ace = new AjaxControlToolkit.AutoCompleteExtender();
            ace.ID = name + "_ace";
            ace.TargetControlID = name;
            ace.ServiceMethod = "GetCompletionList";
            ace.MinimumPrefixLength = 1;
            ace.CompletionSetCount = 10;
            ace.EnableCaching = false;
            ace.CompletionInterval = 100;
            ace.FirstRowSelected = false;

            ace.CompletionListCssClass = "completionList";
            ace.CompletionListHighlightedItemCssClass = "itemHighlighted";
            ace.CompletionListItemCssClass = "listItem";
            ace.ContextKey = name;
            return ace;
        }

        private static AjaxControlToolkit.AccordionPane CreateAccordionPane(string name)
        {
            AjaxControlToolkit.AccordionPane acc = new AjaxControlToolkit.AccordionPane();
            acc.ID = name + "_Pane";
            Label header = new Label();
            header.Text = FirstCharToUpper(name);
            acc.HeaderContainer.Controls.Add(header);
            return acc;
        }

        private static HtmlTable CreateHtmlTable(string name)
        {
            HtmlTable table = new HtmlTable();
            table.ID = name;
            return table;
        }

        private static HtmlTableRow CreateHtmlTableRow(string css)
        {
            HtmlTableRow row = new HtmlTableRow();
            row.Attributes.Add("class", css);
            return row;
        }

        private static HtmlTableCell CreateHtmlTableCell()
        {
            HtmlTableCell cell = new HtmlTableCell();
            return cell;
        }

        private static string comparableString(string name)
        {
            
            name.Replace("_", " ");
            if (name.EndsWith("#"))
                name = name.TrimEnd('#') + "_";
            return name;
        }

        public static BoundField BuildBoundField(string name)
        {
            BoundField field = new BoundField();
            string header = FirstCharToUpper(name.Replace("_", " "));
            if (name.EndsWith("#"))
                name = name.TrimEnd('#') + "_";
            field.FooterText = name;
            field.HeaderText = FirstCharToUpper(header);
            field.DataField = name;
            return field;
        }

        private static string FirstCharToUpper(string input)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input.ToLower());
        }
        #endregion

        #region Events
        private static void TextBox_TextChanged(object sender, System.EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            string from = "_From";
            string to = "_To";
            string name = "";
            Field field = null;
            if (tb.ID.EndsWith(from))
            {
                int ix = tb.ID.LastIndexOf(from);
                name = tb.ID.Substring(0, ix) + tb.ID.Substring(ix + from.Length);
                field = Navigation.Search.fields.Find(f => f.Name == name);
                field.Value_from = tb.Text;

            }
            else if (tb.ID.EndsWith(to))
            {
                int ix = tb.ID.LastIndexOf(to);
                name = tb.ID.Substring(0, ix) + tb.ID.Substring(ix + to.Length);
                field = Navigation.Search.fields.Find(f => f.Name == name);
                field.Value_to = tb.Text;
            }
            else
            { 
                name = tb.ID;
                field = Navigation.Search.fields.Find(f => f.Name == name);
                field.Value = tb.Text;
            }
        }
        #endregion
    }

    public class ButtonTemplate : ITemplate
    {
        public void InstantiateIn(System.Web.UI.Control container)
        {
            ImageButton b = new ImageButton();
            b.ID = "b_doc";
            b.Attributes.Add("runat", "server");
            b.CommandName = "ViewFile";
            b.ImageUrl = "~/Images/List.png";
            b.ToolTip = "View File";
            b.Width = new Unit(20, UnitType.Pixel);
            b.Height = new Unit(20, UnitType.Pixel);
            b.Visible = true;
            container.Controls.Add(b);
        }
    }
}