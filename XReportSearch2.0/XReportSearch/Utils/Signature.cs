﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace XReportSearch.Utils
{
    public class Signature
    {
        private static string _PrivateKey = "<RSAKeyValue><Modulus>xSVMBvgXtguOB6HrJN2yDuFzn+66Pz0CejIkwre9Ym6ePfsXs2floh7tmtWUbVo57R7Z26+Qam1HZ+lVFM/jOSsOCdMlFk33D1YHWvNdxd/UZXraBU6kPu+MjUx/9Jk95J28phLfwdcV8vkInwYpGXutBFIIJYB/1RlRG0yTaEU=</Modulus><Exponent>AQAB</Exponent><P>0ZqmmtYlbldD8bVNYWsvLlnnoSvSiWyhCiVBV8E6JRYvpWj+jPMEP3Pcp/2WG27C5kv7bJewTC2AzMCwhyr+Hw==</P><Q>8Miuq6SqpNJCoTyuENUfpewYPeLvHeCFTe8oYdimfjhiBCc9Gv6Hbrk8M1ZXGAFCIMHV2+27U+M1Br6MIWwFGw==</Q><DP>pxYkJ5v8HLid5gmEFNuPseBjYDyByaD5ww+txhm3kybbdn17Jbr3sOlYheYFifrCjTkWxRsqo59GZHJahnXEOw==</DP><DQ>j4aAH8467Z5rmyLJez0e7U9rL9CKyhtgOpFZV+HFdK11N5aQJPwdjJGb4doQdj2hxlbkfIEbzag8nnWMoAt8iw==</DQ><InverseQ>haKxVgLT7Eo9f/mr4QOA1RB8IZ2ZDNgzb/j2dV3lOzHXlEJzZ1TWyas3IX5jeptPM+BAweNIF4aopYgh0IRPaw==</InverseQ><D>LONDHksayv5yhlZdvfUkd7Lpqr6mdYOkrsIAEL3ZRzI8oYwsZ1L+vB0iqKB8vTFQAyeFHzooPbNIEObV0nlLGPCh/Z4RnxAAgGU61pYFaXeeIpCIhYzyjczfM+pcrKrQpSinfEuAlaBW9GLTI7IBJiEvpmVB+KSbaJlzIahg7Xk=</D></RSAKeyValue>";

        public static string CreateToken(string value)
        {
            string expiration_time_seconds = "86400"; //1 day 
            return CreateToken(value, expiration_time_seconds);
        }


        public static string CreateToken(string value, string expiration_time_seconds)
        {
            int expiration_time_seconds_i = Int32.Parse(expiration_time_seconds);
            var token = new Token(value, DateTime.Now.AddSeconds(expiration_time_seconds_i));
            return token.GetTokenString(_PrivateKey);
        } 



        public static bool ValidateToken(string tokenString)
        {
            var token = Token.FromTokenString(tokenString, _PrivateKey);
            if (token != null)
                return true;
            else
                return false;
        }

        public static Token GetToken(string tokenString)
        {
            var token = Token.FromTokenString(tokenString, _PrivateKey);
            return token;
        }

        private static string GetPrivateKey()
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                return rsa.ToXmlString(true);
            }
        }
    }
}