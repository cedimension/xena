﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.HtmlControls;
using XReportSearch.Business;
using System.Net;
using System.IO;
using System.Web.UI.WebControls;
using System.Collections;

namespace XReportSearch.Utils
{
    public static class Navigation
    {
        #region Session Properties
        public static User User
        {
            get { return (User)GetSessionValue("User", null); }
            set { SetSessionValue("User", value); }
        }

        public static string CurrentSortDirection
        {
            get { return (String)GetSessionValue("CurrentSortDirection", null); }
            set { SetSessionValue("CurrentSortDirection", value); }
        }

        public static string CurrSortIntDirection
        {
            get { return (String)GetSessionValue("CurrSortIntDirection", null); }
            set { SetSessionValue("CurrSortIntDirection", value); }
        }

        public static string CurrentSortExpression
        {
            get { return (String)GetSessionValue("CurrentSortExpression", null); }
            set { SetSessionValue("CurrentSortExpression", value); }
        }

        public static int CurrentGridPage
        {
            get { return (int)GetSessionValue("CurrentGridPage", -1); }
            set { SetSessionValue("CurrentGridPage", value); }
        }

        public static string CurrentSortDirectionH
        {
            get { return (String)GetSessionValue("CurrentSortDirectionH", null); }
            set { SetSessionValue("CurrentSortDirectionH", value); }
        }

        public static string CurrentSortExpressionH
        {
            get { return (String)GetSessionValue("CurrentSortExpressionH", null); }
            set { SetSessionValue("CurrentSortExpressionH", value); }
        }

        public static int CurrentGridPageH
        {
            get { return (int)GetSessionValue("CurrentGridPageH", -1); }
            set { SetSessionValue("CurrentGridPageH", value); }
        }

        public static int CurrentGridPageES
        {
            get { return (int)GetSessionValue("CurrentGridPageES", -1); }
            set { SetSessionValue("CurrentGridPageES", value); }
        }
        // to manage elastic pagination
        public static int TotRowsEs
        {
            get { return (int)GetSessionValue("TotRowsEs", -1); }
            set { SetSessionValue("TotRowsEs", value); }
        }

        public static int CurrentNoteIndex
        {
            get { return (int)GetSessionValue("CurrentNoteIndex", 0); }
            set { SetSessionValue("CurrentNoteIndex", value); }
        }

        public static string CurrentCulture
        {
            get { return (string)GetSessionValue("CurrentCulture", 0); }
            set { SetSessionValue("CurrentCulture", value); }
        }

        public static string Error
        {
            get { return (string)GetSessionValue("Error", ""); }
            set { SetSessionValue("Error", value); }
        }

        public static string[] LogParameters
        {
            get { return (string[])GetSessionValue("LogParameters", ""); }
            set { SetSessionValue("LogParameters", value); }
        }

        public static string ViewParameters
        {
            get { return (string)GetSessionValue("ViewParameters", ""); }
            set { SetSessionValue("ViewParameters", value); }
        }

        public static string ViewParameters1
        {
            get { return (string)GetSessionValue("ViewParameters1", null); }
            //get { return (string)GetSessionValue("ViewParameters1", ""); }
            set { SetSessionValue("ViewParameters1", value); }
        }

        //public static IDictionary<Tuple<string,string>, string> ViewParameters1
        //{
        //    get { return (IDictionary<Tuple<string, string>, string>)GetSessionValue("ViewParameters1", null); }
        //    set { SetSessionValue("ViewParameters1", value); }
        //}

        public static string ViewParameters2
        {
            //get { return (string)GetSessionValue("ViewParameters2", ""); }
            get { return (string)GetSessionValue("ViewParameters2", null); }
            set { SetSessionValue("ViewParameters2", value); }
        }

        public static string ViewParameters3
        {
            //get { return (string)GetSessionValue("ViewParameters3", ""); }
            get { return (string)GetSessionValue("ViewParameters3", null); }
            set { SetSessionValue("ViewParameters3", value); }
        }

        public static IDictionary<string, IDictionary<string, object>> Indici
        {
            get { return (IDictionary<string, IDictionary<string, object>>)GetSessionValue("Indici", null); }
            set { SetSessionValue("Indici", value); }
        }

        public static IDictionary<Tuple<string,string,int>, string> pageText
        {
            get { return (IDictionary<Tuple<string, string, int>, string>)GetSessionValue("pageText", null); }
            set { SetSessionValue("pageText", value); }
        }

        public static IDictionary<string, IDictionary<string, string>> SavedSearch
        {
            get { return (IDictionary<string, IDictionary<string, string>>)GetSessionValue("SavedSearch", null); }
            set { SetSessionValue("SavedSearch", value); }
        }

        public static  List<HtmlTableRow> DynamicFormControls
        {
            get { return (List<HtmlTableRow>)GetSessionValue("DynamicFormControls", null); }
            set { SetSessionValue("DynamicFormControls", value); }
        }

        public static DataTable Fields
        {
            get { return (DataTable)GetSessionValue("Fields", null); }
            set {
                if (value != null)
                    ((DataTable)value).TableName = "Fields";
                SetSessionValue("Fields", value); 
            }
        }

        public static DataTable TableSource
        {
            get { return (DataTable)GetSessionValue("TableSource", null); }
            set {
                if (value != null)
                    ((DataTable)value).TableName = "TableSource";
                SetSessionValue("TableSource", value); 
            }
        }

        public static DataTable TableSourceH
        {
            get { return (DataTable)GetSessionValue("TableSourceH", null); }
            set { 
                if (value != null) ((DataTable)value).TableName = "TableSourceH"; 
                SetSessionValue("TableSourceH", value); }
        }

        public static List<DataTable> TablesSources
        {
            get { return (List<DataTable>)GetSessionValue("TablesSources", null); }
            set { SetSessionValue("TablesSources", value); }
        }

        public static DataTable TableNotes
        {
            get { return (DataTable)GetSessionValue("TableNotes", null); }
            set { SetSessionValue("TableNotes", value); }
        }

        public static List<Hit> GridSource2
        {
            get { return (List<Hit>)GetSessionValue("GridSource2", null); }
            set { SetSessionValue("GridSource2", value); }
        }

        public static DataTable GridSource
        {
            get { return (DataTable)GetSessionValue("GridSource", null); }
            set { SetSessionValue("GridSource", value); }
        }


        public static Business.Search Search
        {
            get { return (Business.Search)GetSessionValue("Search", null); }
            set { SetSessionValue("Search", value); }
        }

        public static string CurrentIndex
        {
            get { return (string)GetSessionValue("CurrentIndex", null); }
            set { SetSessionValue("CurrentIndex", value); }
        }

        public static int CurrGridPageInterv 
        {
            get { return (int)GetSessionValue("CurrGridPageInterv", null); }
            set { SetSessionValue("CurrGridPageInterv", value); }
        }

        public static string CurrentType
        {
            get { return (string)GetSessionValue("CurrentType", null); }
            set { SetSessionValue("CurrentType", value); }
        }

        public static RequestState RequestState
        {
            get { return (RequestState)GetSessionValue("RequestState", null); }
            set { SetSessionValue("RequestState", value); }
        }

        public static Utility.Searchtype SearchType
        {
            get { return (Utility.Searchtype)GetSessionValue("SearchType", Utility.Searchtype.Online); }
            set { SetSessionValue("SearchType", value); }
        }

        public static int TotReports
        {
            get { return (int)GetSessionValue("TotReports", 0); }
            set { SetSessionValue("TotReports", value); }
        }

        public static int CurrentRange
        {
            get { return (int)GetSessionValue("CurrentRange", 0); }
            set { SetSessionValue("CurrentRange", value); }
        }

        public static XReportWebIface.DocumentData IndexesData
        {
            get { return (XReportWebIface.DocumentData)GetSessionValue("IndexesData", null); }
            set { SetSessionValue("IndexesData", value); }
        }

        public static XReportWebIface.DocumentData IndexesDataLast
        {
            get { return (XReportWebIface.DocumentData)GetSessionValue("IndexesDataLast", null); }
            set { SetSessionValue("IndexesDataLast", value); }
        }

        public static Dictionary<string, DataTable> CombosSources
        {
            get { return (Dictionary<string, DataTable>)GetSessionValue("CombosSources", null); }
            set { SetSessionValue("CombosSources", value); }
        }

        public static Dictionary<string, string> CombosValues
        {
            get { return (Dictionary<string, string>)GetSessionValue("CombosValues", null); }
            set { SetSessionValue("CombosValues", value); }
        }

        public static string IndexExample
        {
            get { return (string)GetSessionValue("IndexExample", ""); }
            set { SetSessionValue("IndexExample", value); }

        }

        public static List<string> IndexSuggestion
        {
            get { return (List<string>)GetSessionValue("IndexSuggestion", null); }
            set { SetSessionValue("IndexSuggestion", value); }

        }

        public static Hashtable IndexSuggestionHash
        {
            get { return (Hashtable)GetSessionValue("IndexSuggestion", null); }
            set { SetSessionValue("IndexSuggestion", value); }

        }
        
        public static Hashtable IndexExampleHashtable
        {
            get { return (Hashtable)GetSessionValue("IndexExampleHashtable", null); }
            set { SetSessionValue("IndexExampleHashtable", value); }
        }
        #endregion

        #region Methods
        private static void SetSessionValue(string SessionName, object value)
        {
            if (HttpContext.Current.Session != null) 
                HttpContext.Current.Session[SessionName] = value;
        }

        private static object GetSessionValue(string SessionName, object defaultvalue)
        {
            object res = defaultvalue;
            if (HttpContext.Current.Session != null) 
                res = HttpContext.Current.Session[SessionName] != null ? HttpContext.Current.Session[SessionName] : defaultvalue;
            return res;
        }

        private static void SetApplicationValue(string ApplicationNameObj , object value){
            if (HttpContext.Current.Application != null && HttpContext.Current.Application[ApplicationNameObj] != null)
                HttpContext.Current.Application[ApplicationNameObj] = value;
        }

        private static object GetApplicationValue(string ApplicationNameObj, object defaultvalue)
        {
            object res = defaultvalue;
            if (HttpContext.Current.Session != null)
                res = HttpContext.Current.Application[ApplicationNameObj] != null ? HttpContext.Current.Application[ApplicationNameObj] : defaultvalue;
            return res;
        }
        
        #endregion

        public static DataTable GridIntervals
        {
            get { return (DataTable)GetSessionValue("GridIntervals", null); }
            set
            {
                if (value != null)
                    ((DataTable)value).TableName = "GridIntervals";
                SetSessionValue("GridIntervals", value);
            }
        }
    }
}