﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;

namespace XReportSearch
{
    [System.Xml.Serialization.XmlTypeAttribute("userId", Namespace = "http://cereport.org/WebService")]
    [System.Xml.Serialization.XmlRootAttribute("Authentication", Namespace = "http://cereport.org/WebService", IsNullable = false)]
    public class AuthenticSoap : SoapHeader
    {
        
        public string userid;

    }
}