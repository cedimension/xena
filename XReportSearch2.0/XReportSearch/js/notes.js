
function enableInsertNewComments() {

	$(".comment-menu li").click(function () {

		// This is the triggered action name
		switch ($(this).attr("data-action")) {

			// A case for each action. Your actions here
			case "add": {

				var xBox = commentX + 75;
				var yBox = commentY + 75;
				var box = "<div class=\"dialbox\" id=\"dialogBox\" style=\"width:255px; height:324px;border:1px solid #CCC;background-color:lightblue;position: absolute;top: " + yBox + "px;left: " + xBox + "px;\">" +
								 "<div style=\"height:2px;\"></div>" +
								 "<label for=\"nameDialog\" style=\"margin-left:4px\">Name:</label>" +
								 "<div style=\"align-content:center\"><textarea id=\"nameDialog\" cols=\"40\" rows=\"1\" style=\"width:240px; height:20px;display: block;margin : 4px 4px;font-size:14px; resize: none;\"></textarea></div>" +
								 "<label for=\"dialogText\" style=\"margin-left:4px\">"+ multiHead +":</label>" +
								 "<div style=\"align-content:center\"><textarea id=\"dialogText\" cols=\"40\" rows=\"5\" style=\"width:240px; height:210px;display: block;margin : 4px 4px;font-size:14px; resize: none;\"></textarea></div>" +
								 "<div>" +
									"<input type=\"button\" id=\"storeComment\" style=\"float: right;width:55px;height:25px;font-size:12px;margin:4px 4px\" value=\""+multiSave+"\"/></div>" +
									 "<input type=\"button\" id=\"canc\" style=\"float: right;width:55px;height:25px;font-size:12px;margin:4px 4px\" value=\""+multiCanc+"\"/>" +
								"</div>";

				$("#inner").append(box);

				$('#storeComment').click(function () {

					var noteName = $('#nameDialog').val();
					var note = $('#dialogText').val();
					$('#dialogBox').remove();
					
					addCommentPointer(undefined, undefined, undefined, undefined, undefined, noteName, note);
				});

				$('#canc').click(function () {

					$('#dialogBox').remove();
				});
				break;
			}
			case "esc": break;
		}

		// Hide it AFTER the action was triggered
		$(".comment-menu").hide(100);
	});

	$("#inner").bind("contextmenu", function (event) {

		// Avoid the real one
		event.preventDefault();
		commentX = event.offsetX;
		commentY = event.offsetY;

		// Show commentmenu
		var val = $(event.target).parents("#inner").context.id;
		if (val.indexOf("accepted") === -1 && val.indexOf("cancelled") === -1 &&
		   val.indexOf("pending") === -1 && val.indexOf("unconfirmed") && val.indexOf("comment") === -1) {
			$(".comment-menu").finish().toggle(100).
	   
			// In the right position (the mouse)
			css({
				top: event.pageY + "px",
				left: event.pageX + "px"
			});
		}
	});


	$("#inner").bind("mousedown", function (e) {
		var val = $(e.target).parents("#inner").context.id;

		if (!$(e.target).parents(".comment-menu").length > 0) {

			// Hide it
			$(".custom-menu").hide(100);
		}

	});
}

function addCommentPointer(item, page, x, y, datetime, msgName, message) {

	var date = new Date();
	var datetime = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + ", " + date.getHours() + ":" + date.getMinutes();
	var newItem = "comment" + $commentCounter;

	if (item == undefined) {
		var newItem = "comment" + $commentCounter;
		$commentCounter = $commentCounter + 1;

		var parentPos = $('#inner').offset();
		var XPOS = Math.round(commentX) + parentPos.left - 4;
		var YPOS = Math.round(commentY) + parentPos.top - 8;

		$('#inner').append(
			"<img src=\"" + myUrl + "Images/point.gif\" style=\"width:24px;height:24px;position:absolute;left:"
				+ XPOS + "px;top:" + YPOS + "px\" id=\"" + newItem + "\" + title=\"" + "name:" + msgName + "\n\n" + message + "\"\/>");

		comments.push({
			'type': 'note',
			'name': newItem,
			'page': $("#pages").val(),
			'user': $("#user").attr('value'),
			'date': datetime,
			'coordinates': [Math.round(commentX), Math.round(commentY)],
			'msgName': msgName,
			'message': message
		});
	}
	else {

		if (x == undefined) {
			$("#" + item).attr('title', "name:" + msgName + "\n\n" + message);

			var index = searchItem(comments, item);
			comments[index].msgName = msgName;
			comments[index].message = message;
			comments[index].date = datetime;
		}
		else {
			var newItem = "comment" + $commentCounter;
			$commentCounter = $commentCounter + 1;

			var parentPos = $('#inner').offset();

			var XPOS = x + parentPos.left - 4;
			var YPOS = y + parentPos.top - 8;

			$('#inner').append(
				"<img src=\"" + myUrl + "Images/point.gif\" style=\"width:24px;height:24px;position:absolute;left:"
					+ XPOS + "px;top:" + YPOS  + "px\" id=\"" + newItem + "\"; + title=\"" + "name:" + msgName + "\n\n" + message + "\"\"\/>");

			comments.push({
				'type': 'note',
				'name': newItem,
				'page': page,
				'user': $("#user").attr('value'),
				'date': datetime,
				'coordinates': [x, y],
				'msgName' : msgName,
				'message': message
			});
		}
	}
 
	commentX = undefined;
	commentY = undefined;

	$(".edit-menu li").click(function () {
		if (newItem === $itemSelected) {
			// This is the triggered action name
			switch ($(this).attr("data-action")) {

				// A case for each action. Your actions here
				case "remove": {
					var index = searchItem(comments, newItem);

					comments.splice(index, 1);
					$("#" + newItem).remove();

					break;
				}

				case "edit": {
					var index = searchItem(comments, newItem);
					var xpos = comments[index].coordinates[0] + 75;
					var ypos = comments[index].coordinates[1] + 75;
					var box = "<div class=\"dialbox\" id=\"dialogBox\" style=\"width:255px; height:324px;border:1px solid #CCC;background-color:lightblue;position: absolute;top: " + ypos + "px;left: " + xpos + "px;\">" +
								"<div style=\"height:2px;\"></div>" +
								"<label for=\"nameDialog\" style=\"margin-left:4px\">Name:</label>" +
								"<div style=\"align-content:center\"><textarea id=\"nameDialog\" cols=\"40\" rows=\"1\" style=\"width:240px; height:20px;display: block;margin : 4px 4px;font-size:14px; resize: none;\">" + comments[index].msgName + "</textarea></div>" +
								"<label for=\"dialogText\" style=\"margin-left:4px\">" + multiHead + ":</label>" +
								"<div style=\"align-content:center\"><textarea id=\"dialogText\" cols=\"40\" rows=\"5\" style=\"width:240px; height:210px;display: block;margin : 4px 4px;font-size:14px; resize: none;\">" + comments[index].message + "</textarea></div>" +
								"<div>" +
								"<input type=\"button\" id=\"storeComment\" style=\"float: right;width:55px;height:25px;font-size:12px;margin:4px 4px\" value=\"" + multiSave + "\"/></div>" +
									"<input type=\"button\" id=\"canc\" style=\"float: right;width:55px;height:25px;font-size:12px;margin:4px 4px\" value=\""+multiCanc+"\"/>" +
							 "</div>";
					$("#inner").append(box);

					$('#storeComment').click(function () {

						var noteName = $('#nameDialog').val();
						var note = $('#dialogText').val();
						$('#dialogBox').remove();
						
						addCommentPointer(newItem, undefined, undefined, undefined, undefined, noteName, note);
					});

					$('#canc').click(function () {

						$('#dialogBox').remove();
					});
				}

				case "esc": break;
			}

			$itemSelected = undefined;
		}
		// Hide it AFTER the action was triggered
		$(".edit-menu").hide(100);
	});

	if (x == undefined) {
		$("#" + newItem).bind("contextmenu", function (event) {

			// Avoid the real one
			event.preventDefault();

			commentX = event.offsetX;
			commentY = event.offsetY;

			// Show contextmenu
			$(".edit-menu").finish().toggle(100).

			// In the right position (the mouse)
			css({
				top: event.pageY + "px",
				left: event.pageX + "px"
			});
		});
	}

	$("#" + newItem).bind("mousedown", function (e) {
		$itemSelected = newItem;
		// If the clicked element is not the menu
		if (!$(e.target).parents(".edit-menu").length > 0) {

			// Hide it
			$(".edit-menu").hide(100);
		}
	});
}
