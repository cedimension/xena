var myUrl = undefined;
var itemsAccepted = [];
var itemsCancelled = [];
var itemsPending = [];
var itemsUnconfirmed = [];

var comments = [];
var commentX = undefined;
var commentY = undefined;

var $acceptedCounter = 1;
var $cancelledCounter = 1;
var $pendingCounter = 1;
var $unconfirmedCounter = 1;
var $commentCounter = 1;
var $itemSelected = undefined;

var notesEnabled = false;

var origitemsAccepted = [];
var origitemsCancelled = [];
var origitemsPending = [];
var origitemsUnconfirmed = [];
    
var origcomments = [];
var origcommentX = undefined;
var origcommentY = undefined;
    
var $origacceptedCounter = 1;
var $origcancelledCounter = 1;
var $origpendingCounter = 1;
var $origunconfirmedCounter = 1;
var $origcommentCounter = 1;

function resetVariables() {

    itemsAccepted = [];
    itemsCancelled = [];
    itemsPending = [];
    itemsUnconfirmed = [];

    comments = [];
    commentX = undefined;
    commentY = undefined;

    $acceptedCounter = 1;
    $cancelledCounter = 1;
    $pendingCounter = 1;
    $unconfirmedCounter = 1;
    $commentCounter = 1;
    $itemSelected = undefined;

    notesEnabled = false;

    $("#inner").html('<textarea id="divtext" cols="55" rows="5" style="width:100%;height:100%;resize:vertical;" readonly="readonly"></textarea>');
}

function copytoOriginal() {
    origitemsAccepted = JSON.parse(JSON.stringify(itemsAccepted));
    origitemsCancelled = JSON.parse(JSON.stringify(itemsCancelled));
    origitemsPending = JSON.parse(JSON.stringify(itemsPending));
    origitemsUnconfirmed = JSON.parse(JSON.stringify(itemsUnconfirmed));

    origcomments = JSON.parse(JSON.stringify(comments));
    if (commentX !== undefined) {
        origcommentX = JSON.parse(JSON.stringify(commentX));
    }
    if (commentY !== undefined) {
        origcommentY = JSON.parse(JSON.stringify(commentY));
    }
    $origacceptedCounter = JSON.parse(JSON.stringify($acceptedCounter));
    $origcancelledCounter = JSON.parse(JSON.stringify($cancelledCounter));
    $origpendingCounter = JSON.parse(JSON.stringify($pendingCounter));
    $origunconfirmedCounter = JSON.parse(JSON.stringify($unconfirmedCounter));
    $origcommentCounter = JSON.parse(JSON.stringify($commentCounter));
}

function resetToOriginal() {
        //recreate all items deleted loadAcceptedItems etc ..
        //items added/modified during page management will be erased and recreated
        var parentPos = $("#inner").offset();
        for (var i = 0; i < itemsAccepted.length; i++) {
            var ind = searchItem(origitemsAccepted, itemsAccepted[i].name);
            $("#" + itemsAccepted[i].name).remove();
        }
        for (var i = 0; i < itemsCancelled.length; i++) {
            var ind = searchItem(origitemsCancelled, itemsCancelled[i].name);
            $("#" + itemsCancelled[i].name).remove();
          
        }
        for (var i = 0; i < itemsPending.length; i++) {
            var ind = searchItem(origitemsPending, itemsPending[i].name);
            $("#" + itemsPending[i].name).remove();
            
        }
        for (var i = 0; i < itemsUnconfirmed.length; i++) {
            var ind = searchItem(origitemsUnconfirmed, itemsUnconfirmed[i].name);
            $("#" + itemsUnconfirmed[i].name).remove();
           
        }
        for (var i = 0; i < comments.length; i++) {
            $("#" + comments[i].name).remove();
        }
        itemsAccepted = [];
        itemsCancelled = [];
        itemsPending = [];
        itemsUnconfirmed = [];
        comments = [];
        $('.dialbox').remove();
        for (var i = 0; i < origitemsAccepted.length; i++) {
            var ind = searchItem(itemsAccepted, origitemsAccepted[i].name);
           
            loadAcceptedItems(origitemsAccepted[i].name, origitemsAccepted[i].date, parseInt(origitemsAccepted[i].coordinates[0]) + parentPos.left, parseInt(origitemsAccepted[i].coordinates[1]) + parentPos.top);
        }
        for (var i = 0; i < origitemsCancelled.length; i++) {
            
            loadCancelledItems(origitemsCancelled[i].name, origitemsCancelled[i].date, parseInt(origitemsCancelled[i].coordinates[0]) + parentPos.left, parseInt(origitemsCancelled[i].coordinates[1]) + parentPos.top);
        }
        for (var i = 0; i < origitemsPending.length; i++) {
                       
            loadPendingItems(origitemsPending[i].name, origitemsPending[i].date, parseInt(origitemsPending[i].coordinates[0]) + parentPos.left, parseInt(origitemsPending[i].coordinates[1]) + parentPos.top);
        }
        for (var i = 0; i < origitemsUnconfirmed.length; i++) {
            
            loadUnconfirmedItems(origitemsUnconfirmed[i].name, origitemsUnconfirmed[i].date, parseInt(origitemsUnconfirmed[i].coordinates[0]) + parentPos.left, parseInt(origitemsUnconfirmed[i].coordinates[1]) + parentPos.top);
        }
        for (var i = 0; i < origcomments.length; i++) {
            addCommentPointer(origcomments[i].name, origcomments[i].page, parseInt(origcomments[i].coordinates[0]) + parentPos.left, parseInt(origcomments[i].coordinates[1]) + parentPos.left, origcomments[i].date, origcomments[i].msgName, origcomments[i].message);
        }
        
        commentX = origcommentX;
        commentY = origcommentY;
        $acceptedCounter = $origacceptedCounter;
        $cancelledCounter = $origcancelledCounter;
        $pendingCounter = $origpendingCounter;
        $unconfirmedCounter = $origunconfirmedCounter;
        $commentCounter = $origcommentCounter;

}


function copyFromOriginal() {
    
    //bisogna confrontare gli id per capire cosa cancellare (#id).remove() C'� un metodo pi� furbo ? 
    //Cancellare tutti gli items presenti e ricaricare quelli original !!!!!
    itemsAccepted = origitemsAccepted;
    itemsCancelled = origitemsCancelled;
    itemsPending = origitemsPending;
    itemsUnconfirmed = origitemsUnconfirmed;
     
    comments = origcomments;
    commentX = origcommentX;
    commentY = origcommentY;
     
    $acceptedCounter = $origacceptedCounter;
    $cancelledCounter = $origcancelledCounter;
    $pendingCounter = $origpendingCounter;
    $unconfirmedCounter = $origunconfirmedCounter;
    $commentCounter = $origcommentCounter;
}