
 function setDraggableAccepptedItems(newItem) {
          
     var attribute = $("#" + newItem).data('uiDraggable');
     console.log("----------->" + attribute + " " + newItem);
	if (attribute === undefined) {
		$("#" + newItem).draggable({
			drag: function (event, ui) {

				var parentPos = $("#inner").offset();

				var $newPosX = Math.round(ui.offset.left - parentPos.left);
				var $newPosY = Math.round(ui.offset.top - parentPos.top);

				var date = new Date();
				var datetime = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + ", " + date.getHours() + ":" + date.getMinutes();

				var val = {
					'type': 'accepted',
					'name': newItem,
					'page': $("#pages").val(),
					'user': $("#user").attr('value'),
					'date': datetime,
					'coordinates': [$newPosX, $newPosY],
					'msgName' : 'none',
					'message': ''
				}

				$("#" + newItem).attr('title', multiCreate + " " + datetime + " by " + val.user);

				var pos = searchItem(itemsAccepted, newItem);
				if (pos != -1) {

				    itemsAccepted[pos].coordinates[0] = $newPosX;
				    itemsAccepted[pos].coordinates[1] =  $newPosY;
					itemsAccepted[pos].date = datetime;
				}
				else {
					itemsAccepted.push(val);
				}
			},
			containment: "#inner"
		});
	}
	else {
		$("#" + newItem).draggable('enable');
	}
}

function loadAcceptedItems(user, date, xpos, ypos) {
	var newItem = "accepted" + $acceptedCounter;
	$acceptedCounter = $acceptedCounter + 1;
	var datetime = undefined;

	var toStoreX = -1;
	var toStoreY = -1;
	if (xpos == undefined) {

		var x = 0;
		var parentPos = $('.sticky').offset();
		if (parentPos.left === 0) {
			parentPos = $('#inner').offset();
			x = parentPos.left + 5;
			toStoreX = x;
		}
		else {
			var innerOffset = $('#inner').offset();
			x = parentPos.left - parentPos.left / 2;
			toStoreX = (x - innerOffset.left);
		}
		var y = parentPos.top + 5;

		toStoreY = y;
		$('#inner').append(
		 "<img src=\""+myUrl+"Images/check.gif\" style=\"width:34px;height:24px;position:absolute;left:" + x + "px;top:" + y + "px\" id=\"" + newItem + "\"\/>");

		var date = new Date();
		datetime = date.getDay() + "/" + date.getMonth() + "/" + date.getFullYear() + ", " + date.getHours() + ":" + date.getMinutes();
	}
	else {
		$('#inner').append(
		   "<img src=\"" + myUrl + "Images/check.gif\" style=\"width:34px;height:24px;position:absolute;left:" + xpos + "px;top:" + ypos + "px\" id=\"" + newItem + "\"\/>");

		datetime = date;
		$("#" + newItem).attr('title', multiCreate + " " + datetime + " by " + user);

		toStoreX = xpos;
		toStoreY = ypos;
	}

	var parentPos = $('#inner').offset();

	itemsAccepted.push({
		'type': 'accepted',
		'name': newItem,
		'page': $("#pages").val(),
		'user': $("#user").attr('value'),
		'date': datetime,
		'coordinates': [toStoreX - parentPos.left, toStoreY - parentPos.top],
		'msgName' : '',
		'message': ''
	});

	if (xpos === undefined) {
		setDraggableAccepptedItems(newItem);
		enableRigthButton(newItem);
	}
	// If the menu element is clicked
	$(".custom-menu li").click(function () {
		if (newItem === $itemSelected) {
			// This is the triggered action name
			switch ($(this).attr("data-action")) {

				// A case for each action. Your actions here
				case "remove": {
					var index = searchItem(itemsAccepted, newItem);
					itemsAccepted.splice(index, 1);
					$("#" + newItem).remove();
					break;
				}
				case "esc": break;
			}

			$itemSelected = undefined;
		}
		// Hide it AFTER the action was triggered
		$(".custom-menu").hide(100);
	});

	$("#" + newItem).bind("mousedown", function (e) {
		$itemSelected = newItem;
		// If the clicked element is not the menu
		if (!$(e.target).parents(".custom-menu").length > 0) {
			// Hide it
			$(".custom-menu").hide(100);
		}
	});
}