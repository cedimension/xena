function blockResources() {
    $("#prev").off('click');
    $('#next').off('click');
    $("#bigprev").off('click');
    $('#bignext').off('click');
    $("#notes").prop("disabled", true);
   
}

function releaseResources() {
    $("#prev").on('click', function (e) { e.stopPropagation(); e.preventDefault(); prevPage(1); });
    $('#next').on('click', function (e) { e.stopPropagation(); e.preventDefault(); nextPage(1);});
    $("#bigprev").on('click', function (e) { e.stopPropagation(); e.preventDefault(); prevPage(10);});
    $('#bignext').on('click', function (e) { e.stopPropagation(); e.preventDefault(); nextPage(10) });
    $("#notes").prop("disabled", false);
}


function checkConcurrent(numpage) {
    $.ajax({
        type: 'POST',
        url: myUrl + "/ViewTextFile.aspx/check",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        async: false,
        data: JSON.stringify({ numpage: numpage }),
        //contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
        beforeSend: function (jqXHR) {
            jqXHR.overrideMimeType('application/x-www-form-urlencoded;charset=utf-8')
        },
        success: function (data) {
            if(data.d != "")
                showMess("" + data.d);
        }
    });


}

function clearsession() {
    
    $.ajax({
        type: 'POST',
        url: myUrl + 'ViewTextFile.aspx/ClearSessionData',
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        async: false,
        //contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
        beforeSend: function (jqXHR) {
            jqXHR.overrideMimeType('application/x-www-form-urlencoded;charset=utf-8')
        },
        success: function (data) {
            //alert(data.d);
        }
    });
}

function managePage(numpage) {
    $("#inner").html("<img style='align-content:center' src='"+myUrl+"Images/loading.gif' alt='Loading...' />");
    //var loading = $('#loading');
    //loading.show();
    setTimeout($.ajax({
        type: 'POST',
        url: myUrl+'ViewTextFile.aspx/LoadReport',
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        async : true,
        //contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
        beforeSend: function (jqXHR) {
            jqXHR.overrideMimeType('application/x-www-form-urlencoded;charset=utf-8')
        },
        data: JSON.stringify({ numpage: numpage}),
        success: function (data) {
            //data.setHeader("Content-Type", "application/text;charset=ISO-8859-1");
            $("#inner").html('<textarea id="divtext" cols="55" rows="5" style="width:100%;height:100%;resize:vertical;" readonly="readonly"></textarea>');
            //loading.hide();
            $("#divtext").val(data.d.report);
            //alert(" " + data.d.report);
            autosize($("#divtext"));
            if (data.d.notes != null) {
                loadImages(data.d.notes);
                copytoOriginal();
            }
            releaseResources();
            //$("#prev").removeattr('disabled');
            //$("#prev").removeattr('disabled');
            
        },
        failure: function (response) {
            //alert(response.d);
            releaseResources();
        }

    }),10);


}

function showMess(mess) {
    $(".confirm-menu").show();
    $(".messlabel").html(mess);
}

function loadPage() {
            //$("#prev").off('click');
            //$('#next').off('click');
            //$("#next").prop("disabled", true);
            //$("#notes").prop("disabled", true);
            blockResources()
            managePage("1");
            //$("#notes").prop("disabled", false);
            //$("#next").prop("disabled", false);
}


function nextPage(numpag) {
    //var numpag = e.data.numpage;
    var ss = parseInt($("#pages").val()) + numpag;
	if (ss <= parseInt($("#allpages").val())) {
	    $("#pages").val(ss);
	    resetVariables();
	    $(".confirm-menu").hide();
	    blockResources()
	    //$("#prev").off('click');
	    //$('#next').off('click');
	    //$("#bigprev").off('click');
	    //$('#bignext').off('click');
	    //$("#notes").prop("disabled", true);
	    managePage(ss);
            
	    //$("#prev").on('click', function () { prevPage(1) });
	    //$('#next').on('click', function () { nextPage(1) });
	    //$("#bigprev").on('click', function () { prevPage(10) });
	    //$('#bignext').on('click', function () { nextPage(10) });
	    //$("#notes").prop("disabled", false);
	    
	}
}

function prevPage(numpag) {
    
    if ($("#pages").val() > numpag) {
        var ss = parseInt($("#pages").val()) - numpag;
		$("#pages").val(ss);
		resetVariables();
		$(".confirm-menu").hide();
		blockResources()
        //$("#prev").off('click');
		//$('#next').off('click');
		//$("#bigprev").off('click');
		//$('#bignext').off('click');
		//$("#notes").prop("disabled", true);
		managePage(ss);
		//$("#prev").on('click', function () { prevPage(1) });
		//$('#next').on('click', function () { nextPage(1) });
		//$("#bigprev").on('click', function () { prevPage(10) });
		//$('#bignext').on('click', function () { nextPage(10) });
		//$("#notes").prop("disabled", false);
		
	}
}

function WriteToFile(items) {
    //alert("saving page number - "+$("#pages").val()+" - ");
    $.ajax({
        type: "POST",
        url: myUrl + 'ViewTextFile.aspx/WriteToDb',
        dataType: "json",
        data: JSON.stringify({ items: items, numpage: parseInt($("#pages").val())}),
        //data: JSON.stringify({ items: items }),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            showMess("" + data.d);
        },
        failure: function (response) {
				alert(response.d);
			}
		});
	}

function LoadFile() {
	$.ajax({
		type: "POST",
		url: myUrl + 'ViewTextFile.aspx/LoadFile',
		contentType: "application/json; charset=ISO-8859-1",
		dataType: "json",
		data: JSON.stringify({numpage : $("#pages").val()}),
		success: function (response) {
			loadImages(response.d)
		},
		failure: function (response) {
			alert("44" + response.d);
		}
	});
}

function loadImages(items) {

	var parentPos = $("#inner").offset();
	for (var i = 0; i < items.length; i++)
	{
		if (items[i].type == "accepted") {
			loadAcceptedItems(items[i].user, items[i].date, parseInt(items[i].coordinates[0]) + parentPos.left, parseInt(items[i].coordinates[1]) + parentPos.top);
		}
		else if (items[i].type == "cancelled") {
			loadCancelledItems(items[i].user, items[i].date, parseInt(items[i].coordinates[0]) + parentPos.left, parseInt(items[i].coordinates[1]) + parentPos.top);
		}
		else if (items[i].type == "pending") {
			loadPendingItems(items[i].user, items[i].date, parseInt(items[i].coordinates[0]) + parentPos.left, parseInt(items[i].coordinates[1]) + parentPos.top);
		}
		else if (items[i].type == "unconfirmed") {
			loadUnconfirmedItems(items[i].user, items[i].date, parseInt(items[i].coordinates[0]) + parentPos.left, parseInt(items[i].coordinates[1]) + parentPos.top);
		}
		else if (items[i].type == "note") {
			addCommentPointer(items[i].name, items[i].page,parseInt(items[i].coordinates[0]) + parentPos.left, parseInt(items[i].coordinates[1]) + parentPos.left, items[i].date, items[i].msgName, items[i].message);
		}
	}
	copytoOriginal();
}

function searchItem(items, item) {
	for (var i = 0; i < items.length; i++) {
		if (items[i].name === item) {
			return i;
		}
	}

	return -1;
}

function enableRigthButtonComment(newItem) {
	$("#" + newItem).bind("contextmenu", function (event) {

		// Avoid the real one
		event.preventDefault();

		commentX = event.offsetX;
		commentY = event.offsetY;

		// Show contextmenu
		$(".edit-menu").finish().toggle(100).

		// In the right position (the mouse)
		css({
			top: event.pageY + "px",
			left: event.pageX + "px"
		});
	});
}

function enableRigthButton(newItem) {
	var attribute = $("#" + newItem).data('contextmenu');
	if (attribute == undefined) {
		$("#" + newItem).bind("contextmenu", function (event) {

			// Avoid the real one
			event.preventDefault();

			commentX = event.offsetX;
			commentY = event.offsetY;

			// Show contextmenu
			$(".custom-menu").finish().toggle(100).

			// In the right position (the mouse)
			css({
				top: event.pageY + "px",
				left: event.pageX + "px"
			});
		});
	}
	else {

		$("#" + newItem).bind('contextmenu');
	}   
}

$(function () {
	
	$(document).ready(function () {
	    myUrl = $("#myUrl").val();
	    $(window).scroll(function () {
			if (notesEnabled) {
				var sticky = $('.sticky'),
					scroll = $(window).scrollTop();
				if (scroll >= 20) {
					sticky.addClass('fixed');
					sticky.show();
				}
				else {
					sticky.hide();
					sticky.removeClass('fixed');
				}
			}
		});
        
		enableInsertNewComments();
		$("#inner").unbind("contextmenu");
		clearsession();
		$("#next").click(function (e) {
		    e.stopPropagation();
		    e.preventDefault();
		    nextPage(1);
		});
		$("#prev").click(function (e) {
		    e.stopPropagation();
		    e.preventDefault();
		    prevPage(1);

		});
		$("#bigprev").on('click', function (e) {
		    e.stopPropagation();
		    e.preventDefault();
		    prevPage(10)
		});
		$('#bignext').on('click', function (e) {
		    e.stopPropagation();
		    e.preventDefault();
		    nextPage(10)
		});
		loadPage();
		 //$("#lesen").click(function () {
         //           alert("Ajax");
         //           $.ajax({
         //               type: 'POST',
         //               //url: "<%=Page.ResolveUrl("~/ViewTextFile.aspx/LoadReport")%>",
         //               url: "/ViewTextFile.aspx/LoadReport",
         //               dataType: 'json',
         //               contentType : 'application/json; charset=utf-8',
         //               //async: 'true',
         //               data: JSON.stringify({ numpage: "1" }),
         //               success: function (data) {
         //                   alert("ritorno"+data.d);
         //                   $(".text").html(data.d);
         //               },
         //           failure: function (data) {
         //                   alert(data.d);
         //               }
         //         });
         //});

        //$("#lesen").click(function () {
        //    PageMethods.LoadReport(OnSuccess);
        //});

		//$("#lesen").click(function () {
		//    $("#inner").html("<img style='align-content:center'src='/Images/loading.gif' alt='Loading...' />");
		//    $.ajax({
		//                type: 'POST',
		//			    url: "/ViewTextFile.aspx/LoadReport",
		//				dataType: "json",
		//			    contentType: "application/json;charset=ISO-8859-1",
		//				//contentType: "application/x-www-form-urlencoded;charset=ISO-8859-1",
		//				beforeSend : function (jqXHR){
		//				    jqXHR.overrideMimeType('application/x-www-form-urlencoded;charset=ISO-8859-1')
		//				},
		//				data: JSON.stringify({ numpage: "1" }),
		//				success: function (data) {
		//				    //data.setHeader("Content-Type", "application/text;charset=ISO-8859-1");
		//				    $("#inner").html('<textarea id="divtext" cols="55" rows="5" style="width:100%;height:100%;resize:vertical;" readonly="readonly"></textarea>');
		//				    alert("data" + '<div>' + data.d + '</div>');
		//					$("#divtext").val(data.d);
		//					autosize($("#divtext"));
		//		        }
		//	        });
		//});

		$(".save").click(function () {

			var r = confirm("Are you sure you want to save changes?");
			if (r == false) {
				return;
			}
			var items = [];
			for (var i = 0; i < itemsAccepted.length; i++) {
				items.push(itemsAccepted[i]);
				$("#" + itemsAccepted[i].name).draggable('disable');
				$("#" + itemsAccepted[i].name).unbind('contextmenu');
			}

			for (var i = 0; i < itemsCancelled.length; i++) {
				items.push(itemsCancelled[i]);
				$("#" + itemsCancelled[i].name).draggable('disable');
				$("#" + itemsCancelled[i].name).unbind('contextmenu');
				//console.log(itemsCancelled[i].date + " __ " + itemsCancelled[i].name);
			}

			for (var i = 0; i < itemsPending.length; i++) {
				items.push(itemsPending[i]);
				$("#" + itemsPending[i].name).draggable('disable');
				$("#" + itemsPending[i].name).unbind('contextmenu');
			}

			for (var i = 0; i < itemsUnconfirmed.length; i++) {
				items.push(itemsUnconfirmed[i]);
				$("#" + itemsUnconfirmed[i].name).draggable('disable');
				$("#" + itemsUnconfirmed[i].name).unbind('contextmenu');
			}

			for (var i = 0; i < comments.length; i++) {
				items.push(comments[i]);
				$("#" + comments[i].name).unbind('contextmenu');
			}

			console.log(items.length);

			for (var i = 0; i < items.length; i++) {
			    console.log("my object: %o", items[i]);
			}
			
			WriteToFile(items);
			copytoOriginal();

			$("#prev").on('click', function (e) { e.stopPropagation(); e.preventDefault(); prevPage(1); });
			$('#next').on('click', function (e) { e.stopPropagation(); e.preventDefault(); nextPage(1); });
			$("#bigprev").on('click', function (e) { e.stopPropagation(); e.preventDefault(); prevPage(10); });
			$('#bignext').on('click', function (e) { e.stopPropagation(); e.preventDefault(); nextPage(10); });

			$('.accepted').hide();
			$('.cancelled').hide();
			$('.pending').hide();
			$('.unconfirmed').hide();
			$('.save').hide();
			$('.exit').hide();
			$(".confirm-menu").hide();

			$("#inner").unbind("contextmenu");
			notesEnabled = false;

			$("#notes").prop("disabled", false);
			$("input[type=text]").removeAttr('disabled');
			$("#divtext").css({ 'background-color': '#ffffff' });
			$("#divtext").css('background-image', 'none');
		});

		//$("#load").click(function () {
		//	LoadFile();
		//});

		$(".exit").click(function () {

		    resetToOriginal();
		    //for (var i = 0; i < itemsAccepted.length; i++) {
			//	$("#" + itemsAccepted[i].name).draggable('disable');
			//	$("#" + itemsAccepted[i].name).unbind('contextmenu');
			//}

			//for (var i = 0; i < itemsCancelled.length; i++) {
			//	 $("#" + itemsCancelled[i].name).draggable('disable');
			//	$("#" + itemsCancelled[i].name).unbind('contextmenu');
			//}

			//for (var i = 0; i < itemsPending.length; i++) {
			//	$("#" + itemsPending[i].name).draggable('disable');
			//	$("#" + itemsPending[i].name).unbind('contextmenu');
			//}

			//for (var i = 0; i < itemsUnconfirmed.length; i++) {
			//	$("#" + itemsUnconfirmed[i].name).draggable('disable');
			//	$("#" + itemsUnconfirmed[i].name).unbind('contextmenu');
			//}

			for (var i = 0; i < comments.length; i++) {

				$("#" + comments[i].name).unbind('contextmenu');
			}

			$("#prev").on('click', function (e) { e.stopPropagation(); e.preventDefault(); prevPage(1);});
			$('#next').on('click', function (e) { e.stopPropagation(); e.preventDefault(); nextPage(1);});
			$("#bigprev").on('click', function (e) { e.stopPropagation(); e.preventDefault(); prevPage(10); });
			$('#bignext').on('click', function (e) { e.stopPropagation(); e.preventDefault(); nextPage(10); });

			$('.accepted').hide();
			$('.cancelled').hide();
			$('.pending').hide();
			$('.unconfirmed').hide();
			$('.save').hide();
			$('.exit').hide();
			$(".confirm-menu").hide();
			
			$("#inner").unbind("contextmenu");
			notesEnabled = false;

			$("#notes").prop("disabled", false);
			$("input[type=text]").removeAttr('disabled');
			$("#divtext").css({ 'background-color': '#ffffff' });
			$("#divtext").css('background-image', 'none');
		});

		$("#notes").click(function () {
		    $(".confirm-menu").hide();
		    checkConcurrent($("#pages").val());
			$("#divtext").css('background-color', "#FFCC99");
			$("#notes").prop("disabled",true);
			$('.accepted').show();
			$('.cancelled').show();
			$('.pending').show();
			$('.unconfirmed').show();
			$('.save').show();
			$('.exit').show();
			
			$("input[type=text]").attr('disabled', 'disabled');
			$("#inner").bind("contextmenu", function (event) {

				// Avoid the real one
				event.preventDefault();
				commentX = event.offsetX;
				commentY = event.offsetY;

				// Show commentmenu
				var val = $(event.target).parents("#inner").context.id;
				if (val.indexOf("accepted") === -1 && val.indexOf("cancelled") === -1 &&
				   val.indexOf("pending") === -1 && val.indexOf("unconfirmed") && val.indexOf("comment") === -1) {
					$(".comment-menu").finish().toggle(100).
	   
					// In the right position (the mouse)
					css({
						top: event.pageY + "px",
						left: event.pageX + "px"
					});
				}
			});

			$("#prev").off('click'); 
			$('#next').off('click');
			$("#bigprev").off('click');
			$('#bignext').off('click');
			for (var i = 0; i < itemsAccepted.length; i++) {
				setDraggableAccepptedItems(itemsAccepted[i].name);
				enableRigthButton(itemsAccepted[i].name);
			}

			for (var i = 0; i < itemsPending.length; i++) {
				setDraggablesPendingItems(itemsPending[i].name);
				enableRigthButton(itemsPending[i].name);
			}

			for (var i = 0; i < itemsCancelled.length; i++) {
				setDraggablesCancelledItems(itemsCancelled[i].name);
				enableRigthButton(itemsCancelled[i].name);
			}

			for (var i = 0; i < itemsUnconfirmed.length; i++) {
				setDraggablesUnconfirmedItems(itemsUnconfirmed[i].name);
				enableRigthButton(itemsUnconfirmed[i].name);
			}

			for (var i = 0; i < comments.length; i++) {
				enableRigthButtonComment(comments[i].name);
			}
		    //to do check or insert check mode on db with user
            //alert the current user another person is changing same logical report
			notesEnabled = true;
		});

		$(".accepted").click(function () {
			loadAcceptedItems();
		});

		$(".cancelled").click(function () {
			loadCancelledItems();
		});

		$(".pending").click(function () {
			loadPendingItems();
		});

		$(".unconfirmed").click(function () {
			loadUnconfirmedItems();
		});
		$("#cancmess").click(function () {
		    $(".confirm-menu").hide();
		});
		
		$("#pages").on('keyup', function (e) {
		    var ss = parseInt($("#pages").val());
		    if(ss > parseInt($("#allpages").val())){
		        $("#pages").val($("#allpages").val());
		    }
		    if (ss < 1) {
		        $("#pages").val(1);
		    }
		});
		//$("#pages").click(function (e) {
		//    e.stopPropagation();
		//    e.preventDefault();
		//    alert("submittin");
		//    managePage();
	    //});
		$(document).ajaxSend(function (event, request, settings) {
		    $('#loading').show();
		});

		$(document).ajaxComplete(function (event, request, settings) {
		    $('#loading').hide();
		});

		$(document).on('submit', '#form1', function (e) {
		    e.preventDefault();
		    e.stopPropagation();
		    var ss = parseInt($("#pages").val());
		    if (ss > 0 && ss <= parseInt($("#allpages").val())) {
		        $("#pages").val(ss);
		        resetVariables();
		        blockResources();
		        //$("#prev").off('click');
		        //$('#next').off('click');
		        //$("#bigprev").off('click');
		        //$('#bignext').off('click');
		        //$("#notes").prop("disabled", true);
		        managePage(ss);
		        //$("#prev").on('click', function () { prevPage(1) });
		        //$('#next').on('click', function () { nextPage(1) });
		        //$("#bigprev").on('click', function () { prevPage(10) });
		        //$('#bignext').on('click', function () { nextPage(10) });
		        //$("#notes").prop("disabled", false);
		    } else {
		        alert("pages out of range");
		    }
		});
		 
	});
});