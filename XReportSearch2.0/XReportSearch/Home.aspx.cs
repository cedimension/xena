﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;
using XReportSearch.Business;
using System.Configuration;
using XReportSearch.Utils;
using log4net;

namespace XReportSearch
{
    public partial class Home : System.Web.UI.Page
    {
        #region Log
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {  
            try
            {
                string matricola = "";
                string token = "";
                if (Request.Params["user"] != null && Request.Params["token"] != null)
                {
                    log.Debug("BACKURL CHIAMATA");
                    matricola = Request.Params["user"];
                    token = Request.Params["token"];
                    log.Debug("USER: " + matricola);
                    log.Debug("TOKEN: " + token);
                    User user = new User(matricola, token);
                    PGE_WS.ApplicationSecurityCheckService PGE_WS = new PGE_WS.ApplicationSecurityCheckService();
                    PGE_WS.Url = ConfigurationManager.AppSettings["PGE_WS"];
                    string appCode = ConfigurationManager.AppSettings["ApplicationCode"];

                    PGE_WS.PGEabdb2 resp = PGE_WS.retrievePGEAuthority(matricola, token, appCode);
                    if (resp.PG_ERRORE != null && resp.PG_ERRORE.codiceErr != "" && resp.PG_ERRORE.codiceErr != "00")
                    {
                        log.Debug("Autenticazione non riuscita " + resp.PG_ERRORE.codiceErr + " - " + resp.PG_ERRORE.descError);
                    }
                    else
                    {
                        if (resp.PG_TORRE != null && resp.PG_TORRE != "")
                            user.Torre = resp.PG_TORRE;
                        Navigation.User = user;
                        log.Debug(user.ToString());
                    }
                }
            }
            catch (Exception Ex)
            {
                log.Error("LoadReports() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
            if (Navigation.User != null)
            {
                this.lb_user.Text = "PGE User: " + Navigation.User.Username + " - Torre: " + Navigation.User.Torre;
                if (Navigation.User.TokenString != "")
                    this.lb_user.Text += " - Token: ottenuto e verificato";
            }
        }

        protected void b_elastic_Click(object sender, EventArgs e)
        {
            Navigation.SearchType = Utility.Searchtype.ElasticSearch;
            Response.Redirect("~/SearchHistory.aspx");
            //string uri = "http://localhost:58069/restservice/getaperson/1/35";
            //HttpWebRequest req = WebRequest.Create(uri) as HttpWebRequest;
            //req.KeepAlive = false;
            //req.Method = "Get";

            //HttpWebResponse resp = req.GetResponse() as HttpWebResponse;


            //Encoding enc = System.Text.Encoding.GetEncoding(1252);
            //StreamReader loResponseStream = new StreamReader(resp.GetResponseStream(), enc);

            //string Response = loResponseStream.ReadToEnd();

            //loResponseStream.Close();
            //resp.Close();
            //System.Diagnostics.Debug.WriteLine(Response);
        }

        protected void b_online_Click(object sender, EventArgs e)
        {
            Navigation.SearchType = Utility.Searchtype.Online;
            Response.Redirect("~/Search.aspx");
        }

        protected void b_history_Click(object sender, EventArgs e)
        {
            Navigation.SearchType = Utility.Searchtype.History;
            Response.Redirect("~/Search.aspx");
        }
        #endregion
    }
}