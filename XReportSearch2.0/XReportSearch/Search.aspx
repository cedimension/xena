﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="XReportSearch.Search"
    Theme="XReportSearchTheme" %>

<%@ Register Namespace="CustomControls" TagPrefix="cedim" Assembly="XReportSearch" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
    <title>XReport Search</title>
    <link href="~/App_Themes/XReportSearchAjaxControls.css?2" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/XReportSearchStyle.css?2" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/Menu.css?4" rel="stylesheet" type="text/css" />
</head>
<body onload="resize()" onresize="resize()" onclick="resize()" onbeforeprint="resize()">
    <script language="javascript" type="text/javascript">
        function AtLeastOneContact_ClientValidate(source, args) {
            if (document.getElementById("<%= tb_jobname.ClientID %>").value == "" &&
                document.getElementById("<%= tb_jobname2.ClientID %>").value == "" &&
                document.getElementById("<%= tb_jobnumber.ClientID %>").value == "" &&
                document.getElementById("<%= tb_name.ClientID %>").value == "") {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }

        function fnOnUpdateValidators() {
            var count = 0;
            for (var i = 0; i < Page_Validators.length; i++) {
                var val = Page_Validators[i];
                var ctrl = document.getElementById(val.controltovalidate);
                if (ctrl != null && ctrl.style != null) {
                    if (!val.isvalid) {
                        ctrl.style.borderColor = "Red";
                        ctrl.className = "comboboxError";
                        count++;
                    }
                    else {
                        ctrl.style.borderColor = "#518012";
                        ctrl.className = "combobox";
                    }
                }
            }
        }

        function resize() {
            var frame = document.getElementById("MainDiv");
            var page = document.getElementById("PageDiv2");
            var pageRight = document.getElementById("PageRight");
            var grid = document.getElementById("divCustomGrid");
            var tbGrid = document.getElementById("tbOutGrid");
            var windowheight = document.documentElement.clientHeight;
            var windowwidth = document.documentElement.clientWidth;
            var popup = document.getElementById("myPopup");
            frame.style.height = windowheight + "px";
            if (document.getElementById("txtHidData") != null) {
                document.getElementById("txtHidData").value = windowheight;
            }
            if (document.getElementById("txtHidData2") != null) {
                document.getElementById("txtHidData2").value = windowwidth;
            }
            frame.style.height = windowheight + "px";
            popup.style.height = windowheight - 100 + "px";
            popup.style.width = windowwidth - 40 + "px";
            if (page.style.height != null) {
                page.style.height = (windowheight - 97) + "px";
            }
            if (grid != null) {
                grid.style.height = (windowheight - 160) + "px";
                if (windowwidth < 800) {
                    pageRight.style.fontSize = "0.5em";
                }
                else if (windowwidth > 2000) {
                    pageRight.style.fontSize = "2em";
                }
                else {
                    pageRight.style.fontSize = "0.8em";
                }
            }
        }
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"
        AsyncPostBackTimeout="600" />
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').add_showing(onshowing);
            resize();
        }

        function onshowing() {
            if ($find('ModalProgress') != null) {
                var windowwidth = document.documentElement.clientWidth;
                var windowheight = document.documentElement.clientHeight;
                $find('ModalProgress').set_X((parseInt(windowwidth) / 2) - 100);
                $find('ModalProgress').set_Y((parseInt(windowheight) / 2) - 100);
            }
        }

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

        function beginRequest() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').show();
        }

        function endRequest() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').hide();
        }

        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            if ($get('divCustomGrid') != null) {
                xPos = $get('divCustomGrid').scrollLeft;
                yPos = $get('divCustomGrid').scrollTop;
            }
        }
        function EndRequestHandler(sender, args) {
            if ($get('divCustomGrid') != null) {
                $get('divCustomGrid').scrollLeft = xPos;
                $get('divCustomGrid').scrollTop = yPos;
            }
        }
    </script>
    <input type="hidden" clientidmode="Static" id="txtHidData" runat="server" />
    <input type="hidden" clientidmode="Static" id="txtHidData2" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div id="MainDiv" class="main">
                <div class="headerContainer" id="headerDiv">
                    <div class="headerLeft">
                        <div class="headerTitleLeft">
                            <asp:Menu ID="nav" runat="server" Orientation="Horizontal" OnMenuItemClick="Item_Click">
                                <StaticSelectedStyle BackColor="#518012" ForeColor="White" Font-Bold="true" />
                                <StaticMenuItemStyle BackColor="#e5f6d1" ForeColor="#518012" Font-Bold="true" HorizontalPadding="5px"
                                    VerticalPadding="1px" />
                                <Items>
                                    <asp:MenuItem Text="Online" Value="1" Selected="true" />
                                    <asp:MenuItem Text="History" Value="2" />
                                </Items>
                            </asp:Menu>
                        </div>
                    </div>
                    <div class="headerRight">
                    </div>
                </div>
                <div class="headerContainerTitleInner" id="Div1">
                    <div class="headerTitleLeftInner">
                        <asp:Label ID="lb_User" runat="server" SkinID="LabelFilter" Width="500px"></asp:Label>
                        <asp:Label ID="Label6" Width="30px" runat="server"></asp:Label>
                        <asp:Label ID="lb_Ambiente" runat="server" SkinID="LabelFilter"></asp:Label>
                    </div>
                </div>
                <asp:Panel runat="server" ID="panelTable" class="page">
                    <table class="page" id="PageDiv">
                        <tr id="PageDiv2">
                            <td class="pageLeft">
                                <table>
                                    <tr class="data">
                                        <td>
                                        </td>
                                        <td class="buttons">
                                            <asp:Button ID="b_assign" CausesValidation="true" runat="server" Text="Search" Visible="true"
                                                CssClass="buttonBig" OnClick="buttonSearch_Click" />
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="l_ambiente" runat="server" Text="Environment" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <ajax:ComboBox ID="ddl_ambiente" runat="server" AutoPostBack="False" RenderMode="Block"
                                                DropDownStyle="DropDown" Visible="false" CaseSensitive="False" CssClass="comboboxDoc">
                                                <asp:ListItem Text="A1" Value="A1"></asp:ListItem>
                                                <asp:ListItem Text="A2" Value="A2"></asp:ListItem>
                                                <asp:ListItem Text="A3" Value="A3"></asp:ListItem>
                                                <asp:ListItem Text="A4" Value="A4"></asp:ListItem>
                                                <asp:ListItem Text="A6" Value="A6"></asp:ListItem>
                                                <asp:ListItem Text="A7" Value="A7"></asp:ListItem>
                                                <asp:ListItem Text="A8" Value="A8"></asp:ListItem>
                                                <asp:ListItem Text="A9" Value="A9"></asp:ListItem>
                                            </ajax:ComboBox>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="lb_jobname" runat="server" Text="Job Name" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_jobname" runat="server" SkinID="TextboxSmallLeft"></asp:TextBox>
                                            <asp:TextBox ID="tb_jobname2" runat="server" SkinID="TextboxSmallRight"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text="Report Name" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_name" runat="server" SkinID="TextboxSmall"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="Label1" runat="server" Text="Job Number" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_jobnumber" runat="server" SkinID="TextboxSmall"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="l_prof" runat="server" Text="" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_prof" runat="server" SkinID="TextboxSmall"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="Label3" runat="server" Text="Date From" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_data1" runat="server" SkinID="TextboxDate"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tb_data1"
                                                Display="None"></asp:RequiredFieldValidator>
                                            <asp:ImageButton runat="server" ID="b_data1" ImageUrl="~/Images/calendario.png" CausesValidation="false"
                                                ImageAlign="TextTop" />
                                            <ajax:CalendarExtender ID="CalendarExtender1" runat="server" PopupPosition="BottomRight"
                                                TargetControlID="tb_data1" PopupButtonID="b_data1" CssClass="calendar" Format="dd/MM/yyyy" />
                                            <ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="tb_data1"
                                                MaskType="Date" Mask="99/99/9999" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                            </ajax:MaskedEditExtender>
                                            <ajax:MaskedEditValidator ID="MaskedEditValidator1" ControlExtender="MaskedEditExtender1"
                                                runat="server" ControlToValidate="tb_data1" IsValidEmpty="False" Display="Dynamic">
                                            </ajax:MaskedEditValidator>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="Label4" runat="server" Text="Date To" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_data2" runat="server" SkinID="TextboxDate"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_data2"
                                                Display="None"></asp:RequiredFieldValidator>
                                            <asp:ImageButton runat="server" ID="b_data2" ImageUrl="~/Images/calendario.png" CausesValidation="false"
                                                ImageAlign="TextTop" />
                                            <ajax:CalendarExtender ID="CalendarExtender2" runat="server" PopupPosition="BottomRight"
                                                TargetControlID="tb_data2" PopupButtonID="b_data2" CssClass="calendar" Format="dd/MM/yyyy" />
                                            <ajax:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="tb_data2"
                                                MaskType="Date" Mask="99/99/9999" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                            </ajax:MaskedEditExtender>
                                            <ajax:MaskedEditValidator ID="MaskedEditValidator2" ControlExtender="MaskedEditExtender2"
                                                runat="server" ControlToValidate="tb_data2" IsValidEmpty="False" Display="Dynamic">
                                            </ajax:MaskedEditValidator>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td colspan="2">
                                            <asp:CustomValidator ID="CustomValidator" runat="server" ErrorMessage="At least one of ReportName, JobName and JobNumber required"
                                                Display="Dynamic" ClientValidationFunction="AtLeastOneContact_ClientValidate"
                                                ForeColor="White" Font-Bold="true" />
                                            <asp:CompareValidator ID="cmpStartAndEndDates" runat="server" Display="Dynamic" Operator="LessThanEqual"
                                                ForeColor="White" Font-Bold="true" ControlToValidate="tb_data1" ControlToCompare="tb_data2"
                                                Type="Date" ErrorMessage="Start date must be less than end date." />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="pageRight" id="PageRight">
                                <cedim:CeGridView ID="gv_search" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                    AllowPaging="true" PagerStyle-CssClass="pager" HeaderStyle-ForeColor="White"
                                    RowStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" RowStyle-Height="25px"
                                    Visible="false" OnPageIndexChanging="gv_search_PageIndexChanging" OnRowDataBound="gv_search_OnRowDataBound"
                                    SkinID="Tables2" Width="100%" Height="100%" OnRowCommand="gv_search_OnRowCommand"
                                    EmptyDataRowStyle-Font-Bold="true" EmptyDataText="No Data" EmptyDataRowStyle-HorizontalAlign="Center"
                                    EmptyDataRowStyle-ForeColor="#518012">
                                    <Columns>
                                        <asp:BoundField ItemStyle-Width="0%" HeaderStyle-Width="0%" ReadOnly="True" InsertVisible="False"
                                            DataField="JobReport_ID" Visible="false" SortExpression="JobReport_ID" HeaderText="JobReport_ID"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="0%" HeaderStyle-Width="0%" ReadOnly="True" InsertVisible="False"
                                            DataField="FromPage" Visible="false" SortExpression="FromPage" HeaderText="FromPage"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField Visible="true">
                                            <ItemStyle Width="7%" HorizontalAlign="Center"></ItemStyle>
                                            <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                            <HeaderTemplate>
                                                <asp:Label ID="lb16" ForeColor="#518012" runat="server" Visible="true" SkinID="LabelHeader"
                                                    Text="Recipient"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_Recipient" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container,"DataItem.Recipient")%>'
                                                    ToolTip='<%# DataBinder.Eval(Container,"DataItem.FolderDescr")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-Width="15%" HeaderStyle-Width="15%" ReadOnly="True" InsertVisible="False"
                                            DataField="ReportName" Visible="true" SortExpression="ReportName" HeaderText="ReportName"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="10%" HeaderStyle-Width="10%" ReadOnly="True" InsertVisible="False"
                                            DataField="JobName" Visible="true" SortExpression="JobName" HeaderText="JobName"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="0%" HeaderStyle-Width="0%" ReadOnly="True" InsertVisible="False"
                                            DataField="TimeRef" Visible="false" SortExpression="TimeRef" HeaderText="TimeRef"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="0%" HeaderStyle-Width="0%" ReadOnly="True" InsertVisible="False"
                                            DataField="UserTimeRef" Visible="false" SortExpression="UserTimeRef" HeaderText="UserTimeRef"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="12%" HeaderStyle-Width="12%" ReadOnly="True" InsertVisible="False"
                                            DataField="UserTimeElab" Visible="true" SortExpression="UserTimeElab" HeaderText="UserTimeElab"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="12%" HeaderStyle-Width="12%" ReadOnly="True" InsertVisible="False"
                                            DataField="XferStartTime" Visible="true" SortExpression="XferStartTime" HeaderText="XferStartTime"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="6%" HeaderStyle-Width="6%" ReadOnly="True" InsertVisible="False"
                                            DataField="TotReports" Visible="false" SortExpression="TotReports" HeaderText="TotReports"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="10%" HeaderStyle-Width="10%" ReadOnly="True" InsertVisible="False"
                                            DataField="JobNumber" Visible="true" SortExpression="JobNumber" HeaderText="JobNumber"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="0%" HeaderStyle-Width="0%" ReadOnly="True" InsertVisible="False"
                                            DataField="ForPages" Visible="false" SortExpression="ForPages" HeaderText="ForPages"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField Visible="true" HeaderText="">
                                            <ItemStyle Width="2%"></ItemStyle>
                                            <HeaderStyle Width="2%" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="b_pdf" runat="server" CausesValidation="false" CommandName="ViewFile"
                                                    ImageUrl="~/Images/icona_pdf.png" ToolTip="View PDF" Width="20px" Height="20px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="true" HeaderText="">
                                            <ItemStyle Width="2%"></ItemStyle>
                                            <HeaderStyle Width="2%" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="b_note" runat="server" CausesValidation="false" CommandName="Note"
                                                    ImageUrl="~/Images/notes.png" ToolTip="View Notes" Width="20px" Height="20px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="true" HeaderText="">
                                            <ItemStyle Width="2%"></ItemStyle>
                                            <HeaderStyle Width="2%" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="b_check" runat="server" CausesValidation="false" CommandName="Checked"
                                                    ToolTip="Check" Enabled="false" Width="20px" Height="20px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </cedim:CeGridView>
                                <cedim:CeGridView ID="gv_searchH" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                    AllowPaging="true" PagerStyle-CssClass="pager" HeaderStyle-ForeColor="White"
                                    RowStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" RowStyle-Height="25px"
                                    Visible="false" OnPageIndexChanging="gv_search_PageIndexChanging" OnRowDataBound="gv_search_OnRowDataBound"
                                    SkinID="Tables2" Width="100%" Height="100%" OnRowCommand="gv_search_OnRowCommand"
                                    EmptyDataRowStyle-Font-Bold="true" EmptyDataText="No Data" EmptyDataRowStyle-HorizontalAlign="Center"
                                    EmptyDataRowStyle-ForeColor="#518012">
                                    <Columns>
                                        <asp:BoundField ItemStyle-Width="6%" HeaderStyle-Width="6%" ReadOnly="True" InsertVisible="False"
                                            DataField="Recipient" Visible="true" SortExpression="Recipient" HeaderText="Recipient"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="15%" HeaderStyle-Width="15%" ReadOnly="True" InsertVisible="False"
                                            DataField="ReportName" Visible="true" SortExpression="ReportName" HeaderText="ReportName"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="9%" HeaderStyle-Width="9%" ReadOnly="True" InsertVisible="False"
                                            DataField="JobName" Visible="true" SortExpression="JobName" HeaderText="JobName"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="15%" HeaderStyle-Width="15%" ReadOnly="True" InsertVisible="False"
                                            DataField="TimeRef" Visible="true" SortExpression="TimeRef" HeaderText="TimeRef"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="15%" HeaderStyle-Width="15%" ReadOnly="True" InsertVisible="False"
                                            DataField="UserTimeRef" Visible="true" SortExpression="UserTimeRef" HeaderText="UserTimeRef"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="15%" HeaderStyle-Width="15%" ReadOnly="True" InsertVisible="False"
                                            DataField="UserTimeElab" Visible="true" SortExpression="UserTimeElab" HeaderText="UserTimeElab"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="6%" HeaderStyle-Width="6%" ReadOnly="True" InsertVisible="False"
                                            DataField="TotPages" Visible="true" SortExpression="TotPages" HeaderText="TotPages"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="6%" HeaderStyle-Width="6%" ReadOnly="True" InsertVisible="False"
                                            DataField="JobNumber" Visible="true" SortExpression="JobNumber" HeaderText="JobNr"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ItemStyle-Width="8%" HeaderStyle-Width="8%" ReadOnly="True" InsertVisible="False"
                                            DataField="DDName" Visible="true" SortExpression="DDName" HeaderText="DDName"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ReadOnly="True" InsertVisible="False" DataField="ReportEntryId" Visible="false"
                                            SortExpression="ReportEntryId" HeaderText="" ItemStyle-Width="0%" HeaderStyle-Width="0%"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ReadOnly="True" InsertVisible="False" DataField="firstRBA" Visible="false"
                                            SortExpression="firstRBA" HeaderText="" ItemStyle-Width="0%" HeaderStyle-Width="0%"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ReadOnly="True" InsertVisible="False" DataField="lastRBA" Visible="false"
                                            SortExpression="lastRBA" HeaderText="" ItemStyle-Width="0%" HeaderStyle-Width="0%"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ReadOnly="True" InsertVisible="False" DataField="restore_id" Visible="false"
                                            SortExpression="restore_id" HeaderText="" ItemStyle-Width="0%" HeaderStyle-Width="0%"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField ReadOnly="True" InsertVisible="False" DataField="totpages2" Visible="false"
                                            SortExpression="totpages2" HeaderText="" ItemStyle-Width="0%" HeaderStyle-Width="0%"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField Visible="true" HeaderText="">
                                            <ItemStyle Width="3%"></ItemStyle>
                                            <HeaderStyle Width="3%" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="b_pdf" runat="server" CausesValidation="false" CommandName="ViewFile"
                                                    ImageUrl="~/Images/List.png" ToolTip="View File" Width="20px" Height="20px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ReadOnly="True" InsertVisible="false" DataField="Indexed" Visible="false"
                                            HeaderText="Index" ItemStyle-Width="0%" HeaderStyle-Width="0%" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" />
                                        <asp:TemplateField Visible="true" HeaderText="">
                                            <ItemStyle Width="3%"></ItemStyle>
                                            <HeaderStyle Width="3%" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="b_index" runat="server" CausesValidation="false" CommandName="Index"
                                                    ImageUrl="~/Images/Index.png" ToolTip='<%# Eval("Indexed")%>' Width="20px" Height="20px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </cedim:CeGridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <asp:UpdateProgress ID="UpdateProgress" runat="server" DisplayAfter="200">
                <ProgressTemplate>
                    <asp:Panel ID="panelUpdateProgress" runat="server">
                        <div>
                            <img src="~/Images/loader.gif" alt="" runat="server" />
                        </div>
                    </asp:Panel>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <ajax:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
                BackgroundCssClass="modalProgress" PopupControlID="panelUpdateProgress" BehaviorID="ModalProgress">
            </ajax:ModalPopupExtender>
            <asp:Panel ID="popupW" runat="server" CssClass="popupWarning">
                <div class="divWarning">
                    <asp:Label ID="lb_Popup" runat="server" Text="" SkinID="LabelWarning"></asp:Label>
                </div>
                <div class="divWarning">
                    <asp:Button ID="siPopup" CausesValidation="false" runat="server" Text="OK" Visible="true"
                        CssClass="buttonBig" OnClick="siPopup_Click" />
                </div>
            </asp:Panel>
            <asp:HiddenField ID="h2" runat="server" />
            <ajax:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="h2"
                BehaviorID="ModalPopupExtender2" PopupControlID="popupW" Drag="false" BackgroundCssClass="modal">
            </ajax:ModalPopupExtender>
            <asp:Panel ID="popupOKcanc" runat="server" CssClass="popupWarning">
                <div class="divWarning">
                    <asp:Label ID="Label7" runat="server" Text="Are you sure you want to check the selected report?"
                        SkinID="LabelWarning"></asp:Label>
                </div>
                <div class="divWarning">
                    <asp:Button ID="b_yes" CausesValidation="false" runat="server" Text="Yes" Visible="true"
                        CssClass="buttonBig" OnClick="b_yes_Click" />
                    <asp:Button ID="b_popupCanc" CausesValidation="false" runat="server" Text="Cancel"
                        Visible="true" CssClass="buttonBig" />
                </div>
            </asp:Panel>
            <asp:HiddenField ID="h3" runat="server" />
            <ajax:ModalPopupExtender ID="ModalPopupExtender3" runat="server" TargetControlID="h3"
                CancelControlID="b_popupCanc" BehaviorID="ModalPopupExtender3" PopupControlID="popupOKcanc"
                Drag="false" BackgroundCssClass="modal">
            </ajax:ModalPopupExtender>
            <asp:Button runat="server" ID="hiddenTargetControlForModalPopup" Style="display: none" />
            <ajax:ModalPopupExtender runat="server" ID="programmaticModalPopup" BehaviorID="programmaticModalPopupBehavior"
                TargetControlID="hiddenTargetControlForModalPopup" PopupControlID="programmaticPopup"
                BackgroundCssClass="modal" PopupDragHandleControlID="programmaticPopupDragHandle"
                RepositionMode="RepositionOnWindowScroll">
            </ajax:ModalPopupExtender>
            <asp:Panel ID="programmaticPopup" runat="server" Style="display: none; text-align: center;">
                <table id="myPopup" style="background-color: #e5f6d1;">
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="b_close" runat="server" CausesValidation="false" OnClick="b_close_Click"
                                ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%; height: 100%;">
                            <iframe id="myFrame" runat="server" scrolling="yes" style="width: 100%; height: 100%;
                                border: none; position: relative;"></iframe>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="cmdPopup" runat="server" Style="display: inline-table; text-align: center;
                width: 200px; height: 200px; position: relative">
                <table id="TablePopup" runat="server" style="background-color: #EFF1F4">
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="b_close2" runat="server" CausesValidation="false" OnClick="b_close2_Click"
                                ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                        </td>
                    </tr>
                    <tr style="background-color: #518012">
                        <td>
                            <asp:Label ID="Label5" runat="server" Text="AVAILABLE INDEXES" SkinID="LabelLoginBig"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Button runat="server" ID="b_cmd" Style="display: none" />
            <ajax:ModalPopupExtender runat="server" ID="cmdExt" BehaviorID="cmdExtBehavior" TargetControlID="b_cmd"
                PopupControlID="cmdPopup" BackgroundCssClass="modal">
            </ajax:ModalPopupExtender>
            <asp:Panel ID="p_notes" runat="server" Style="display: inline-table; text-align: center;
                position: relative">
                <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <table id="Table1" runat="server" style="background-color: #EFF1F4">
                            <tr>
                                <td style="text-align: right;" colspan="3">
                                    <asp:ImageButton ID="b_close3" runat="server" CausesValidation="false" OnClick="b_close3_Click"
                                        ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" colspan="3">
                                    <asp:DetailsView ID="dv_notes" runat="server" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true"
                                        AutoGenerateRows="False" CssClass="gridView" SkinID="TablesView" Visible="true"
                                        AllowPaging="true" OnPreRender="dv_notes_DataBound" OnItemUpdating="dv_notes_ItemUpdating"
                                        OnPageIndexChanging="dv_notes_PageIndexChanging" OnItemDeleting="dv_notes_ItemDeleting"
                                        OnItemInserting="dv_notes_ItemInserting" PagerStyle-CssClass="pager" HeaderText="Notes"
                                        OnModeChanging="dv_notes_ModeChanging" Width="500px" EmptyDataText="NO NOTES INSERTED"
                                        EmptyDataRowStyle-Font-Bold="true">
                                        <HeaderStyle Font-Bold="True" />
                                        <Fields>
                                            <asp:TemplateField HeaderText="Job Report ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="tb_JRID1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.JobReport_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:Label ID="tb_JRID2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.JobReport_ID") %>'></asp:Label>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Report ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="tb_repID1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ReportId") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:Label ID="tb_repID2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ReportId") %>'></asp:Label>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Version">
                                                <ItemTemplate>
                                                    <asp:Label ID="tb_ver1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.NoteId") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:Label ID="tb_ver2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.NoteId") %>'></asp:Label>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Update Time">
                                                <ItemTemplate>
                                                    <asp:Label ID="tb_updTime1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastUpdateTime") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:Label ID="tb_updTime2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastUpdateTime") %>'></asp:Label>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Update User">
                                                <ItemTemplate>
                                                    <asp:Label ID="tb_updUser1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AUTH_USER") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:Label ID="tb_updUser2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AUTH_USER") %>'></asp:Label>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Text">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="tb_text1" ReadOnly="true" Height="200px" Width="200px" TextMode="MultiLine"
                                                        runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.textString") %>'></asp:TextBox>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="tb_text2" Height="200px" Width="200px" TextMode="MultiLine" runat="server"
                                                        Text='<%# DataBinder.Eval(Container, "DataItem.textString") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <EditItemTemplate>
                                                    <asp:Button ID="lnkUpdate" runat="server" CausesValidation="True" CommandName="Update"
                                                        Text="Update" ValidationGroup="g1" CssClass="buttonSmall"></asp:Button>
                                                    <asp:Button ID="lnkCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                                                        Text="Cancel" CssClass="buttonSmall"></asp:Button>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Button ID="lnkEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                                        Text="Edit" CssClass="buttonSmall"></asp:Button>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <InsertItemTemplate>
                                                    <asp:Button ID="lnkInsert" runat="server" CausesValidation="True" CommandName="Insert"
                                                        Text="Insert" ValidationGroup="g1" CssClass="buttonSmall"></asp:Button>
                                                    <asp:Button ID="lnkCacel" runat="server" CausesValidation="False" CommandName="Cancel"
                                                        Text="Cancel" CssClass="buttonSmall"></asp:Button>
                                                </InsertItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Button ID="lnkNew" runat="server" CausesValidation="False" CommandName="New"
                                                        Text="New" CssClass="buttonSmall"></asp:Button>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:Button ID="lnkDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                        Text="Delete" CssClass="buttonSmall"></asp:Button>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <asp:Button runat="server" ID="b_notes" Style="display: none" />
            <ajax:ModalPopupExtender runat="server" ID="notes_ext" BehaviorID="notes_extBehavior"
                TargetControlID="b_notes" PopupControlID="p_notes" BackgroundCssClass="modal">
            </ajax:ModalPopupExtender>
            <asp:HiddenField ID="hiddenIndex" runat="server" Value="0" />
            <asp:Panel ID="p_ConfirmDel" runat="server" CssClass="popupWarningBig">
                <div class="divWarning">
                    <asp:Button ID="b_curr" CausesValidation="false" runat="server" Text="Current" Visible="true"
                        CssClass="buttonBig" OnClick="currPopup_Click" /></div>
                <div class="divWarning">
                    <asp:Button ID="b_all" CausesValidation="false" runat="server" Text="All Versions"
                        Visible="true" CssClass="buttonBig" OnClick="allPopup_Click" /></div>
                <div class="divWarning">
                    <asp:Button ID="b_canc" CausesValidation="false" runat="server" Text="Cancel" Visible="true"
                        CssClass="buttonBig" />
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hDel" runat="server" />
            <ajax:ModalPopupExtender ID="ConfDel_ext" runat="server" TargetControlID="hDel" CancelControlID="b_canc"
                BehaviorID="ConfDel_extBehavior" PopupControlID="p_ConfirmDel" Drag="false" BackgroundCssClass="modal">
            </ajax:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
