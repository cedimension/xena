﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Modificata per creare dinamicamente a runtine il _source a seconda del JSON restituito da ES
/// </summary>
/// 
namespace XReportSearch.Business
{
    public class Hit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public double _score { get; set; }
        public System.Dynamic.ExpandoObject _source { get; set; }
    }

    public class Hits
    {
        public int total { get; set; }
        public double max_score { get; set; }
        public List<Hit> hits { get; set; }
    }
}