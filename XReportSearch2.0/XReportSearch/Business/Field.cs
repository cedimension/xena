﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XReportSearch.Business
{
    public class Field
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Value_from { get; set; }
        public string Value_to { get; set; }
        public bool IsNumeric { get; set; }
        public bool IsDate { get; set; }

        public Field()
        {
        }

        public Field(string name)
        {
            this.Name = name;
            this.Value = "";
            this.Value_from = "";
            this.Value_to = "";
            this.IsNumeric = false;
            this.IsDate = false;
        }
    }
}