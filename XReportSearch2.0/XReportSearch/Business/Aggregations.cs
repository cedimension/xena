﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XReportSearch.Business
{
    public class Aggregations
    {
       public Suggest suggest { get; set; } 
      
    }

    public class Suggest
    {
        public int doc_count_error_upper_bound { get; set; }
        public int sum_other_doc_count { get; set; }
        public List<BucketLeaf> buckets { get; set; }
    }

    public class BucketLeaf { 
            public string key  { get; set; }
            public int doc_count { get; set; }
    
    }
}