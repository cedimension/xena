﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XReportSearch.Business;
using PlainElastic.Net.Queries;
using XReportSearch.Utils;
/// <summary>
/// NON PIU' USATA
/// </summary>
namespace XReportSearch.Business
{
    public class Source
    {
        public string id { get; set; }
        public int from_page { get; set; }
        public int last_page { get; set; }
        public int tot_pages { get; set; }
        public string date_elab { get; set; }
        public string index_name { get; set; }
        public string from_rba { get; set; }
        public string last_rba { get; set; }
        public string job_name { get; set; }
        public string report_name { get; set; }
        public string job_date_time { get; set; }
        public string report_date_time { get; set; }
        public string report_long_name { get; set; }
        public string report_short_name { get; set; }
        public string user_name { get; set; }

        public string lotto_id { get; set; }
    }
}