﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XReportSearch.Business
{
    public class RootObject
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public Shards _shards { get; set; }
        public Hits hits { get; set; }
        public Facets facets { get; set; }
        public Aggregations aggregations { get; set; } 
    }
}