﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PlainElastic.Net.Queries;
using System.ComponentModel;
using PlainElastic.Net.IndexSettings;
using PlainElastic.Net.Utils;
using PlainElastic.Net.Mappings;
using log4net;
using XReportSearch.Utils;

namespace XReportSearch.Business
{
    public class SearchOLD
    {
        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Properties
        [DefaultValue(null)]
        public List<Field> fields { get; set; }
        [DefaultValue("")]
        public string date_elab { get; set; }
        [DefaultValue("")]
        public string date_elab_From { get; set; }
        [DefaultValue("")]
        public string date_elab_To { get; set; }
        [DefaultValue("")]
        public string job_name { get; set; }
        [DefaultValue("")]
        public string report_name { get; set; }
        [DefaultValue("")]
        public string job_date_time { get; set; }
        [DefaultValue("")]
        public string job_date_time_From { get; set; }
        [DefaultValue("")]
        public string job_date_time_To { get; set; }
        [DefaultValue("")]
        public string report_date_time { get; set; }
        [DefaultValue("")]
        public string report_date_time_From { get; set; }
        [DefaultValue("")]
        public string report_date_time_To { get; set; }
        [DefaultValue("")]
        public string report_long_name { get; set; }
        [DefaultValue("")]
        public string report_short_name { get; set; }
        [DefaultValue("")]
        public string user_name { get; set; }

        public string dipendenza { get; set; }
        [DefaultValue("")]
        public string dipendenza_From { get; set; }
        [DefaultValue("")]
        public string dipendenza_To { get; set; }
        [DefaultValue("")]
        public string conto { get; set; }
        [DefaultValue("")]
        public string conto_From { get; set; }
        [DefaultValue("")]
        public string conto_To { get; set; }
        [DefaultValue("")]
        public string sottoconto { get; set; }
        [DefaultValue("")]
        public string sottoconto_From { get; set; }
        [DefaultValue("")]
        public string sottoconto_To { get; set; }
        [DefaultValue("")]
        public string sportello { get; set; }
        [DefaultValue("")]
        public string sportello_From { get; set; }
        [DefaultValue("")]
        public string sportello_To { get; set; }
        [DefaultValue("")]
        public string esercente { get; set; }
        [DefaultValue("")]
        public string esercente_From { get; set; }
        [DefaultValue("")]
        public string esercente_To { get; set; }
        [DefaultValue("")]
        public string stabilimento { get; set; }
        [DefaultValue("")]
        public string stabilimento_From { get; set; }
        [DefaultValue("")]
        public string stabilimento_To { get; set; }
        [DefaultValue("")]
        public string cassa { get; set; }
        [DefaultValue("")]
        public string cassa_From { get; set; }
        [DefaultValue("")]
        public string cassa_To { get; set; }

        [DefaultValue("")]
        public string divisa { get; set; }
        [DefaultValue("")]
        public string categoria { get; set; }
        [DefaultValue("")]
        public string causale { get; set; }
        [DefaultValue("")]
        public string carta { get; set; }
        [DefaultValue("")]
        public string ente { get; set; }
        [DefaultValue("")]
        public string importo { get; set; }
        [DefaultValue("")]
        public string importo_From { get; set; }
        [DefaultValue("")]
        public string importo_To { get; set; }
        [DefaultValue("")]
        public string data_lav { get; set; }
        [DefaultValue("")]
        public string data_lav_From { get; set; }
        [DefaultValue("")]
        public string data_lav_To { get; set; }
        [DefaultValue("")]
        public string tabulato { get; set; }

        //BPM
        [DefaultValue("")]
        public string agenzia { get; set; }
        [DefaultValue("")]
        public string agenzia_From { get; set; }
        [DefaultValue("")]
        public string agenzia_To { get; set; }
        [DefaultValue("")]
        public string data_elab { get; set; }
        [DefaultValue("")]
        public string data_elab_From { get; set; }
        [DefaultValue("")]
        public string data_elab_To { get; set; }
        [DefaultValue("")]
        public string data_rif { get; set; }
        [DefaultValue("")]
        public string data_rif_From { get; set; }
        [DefaultValue("")]
        public string data_rif_To { get; set; }
        [DefaultValue("")]
        public string lotto_id { get; set; }
        [DefaultValue("")]
        public string tipo_doc { get; set; }


        //dipendenza    =>  {type  =>  "integer",  index => "not_analyzed",  store => "yes"},  
        //conto         =>  {type  =>  "long",     index => "not_analyzed",  store => "yes"},  
        //sottoconto    =>  {type  =>  "integer",  index => "not_analyzed",  store => "yes"},  
        //esercente     =>  {type  =>  "long",     index => "not_analyzed",  store => "yes"},  
        //sportello     =>  {type  =>  "integer",  index => "not_analyzed",  store => "yes"},  
        //causale       =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
        //categoria     =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
        //divisa        =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
        //stabilimento  =>  {type  =>  "integer",  index => "not_analyzed",  store => "yes"},  
        //cassa         =>  {type  =>  "integer",  index => "not_analyzed",  store => "yes"}, 
        #endregion

        #region Methods
        private string ToUpper(string s)
        {
            if (s != null)
                s = s.ToUpper();
            return s;
        }

        public string BuildQueryManual(string sortList)
        {
            string query = " { " +
                                "\"query\": " +
                                    "{ \"bool\": " +
                                        "{ \"must\": " +
                                            "[ ";
            string queryEnd = "], " +
                                        "\"must_not\": [ ], " +
                                        "\"should\": [ ] " +
                                    "}" +
                                "}, " +
                                "\"from\": 0, " +
                                "\"size\": 10, " +
                                "\"sort\": [ ], " +
                                "\"facets\": { } " +
                            "}";
            string queryRes = "";
            foreach (Field f in this.fields)
            {
                if (f.Value == "" && f.Value_from == "" && f.Value_to == "")
                    continue;
                if (f.IsDate || f.IsNumeric)
                    queryRes += BuildRange(f);
                else
                    queryRes += BuildPrefix(f);
                queryRes += ",";
            }
            string finalQuery = query + queryRes.TrimEnd(',') + queryEnd;
            return finalQuery;
        }

        private string BuildPrefix(Field field)
        {
            return "{" +
                        "\"prefix\": {" +
                            "\"" + field.Name + "\" : " + "\"" + field.Value + "\"" +
                        "}" +
                    "}";
        }

        private string BuildRange(Field field)
        {
            string from = field.Value_from;
            string to = field.Value_to;
            string name = field.Name;
            if (field.IsDate)
            {
                from = String.Format("{0:s}", DateTime.Parse(field.Value_from));
                to = String.Format("{0:s}", DateTime.Parse(field.Value_to).AddHours(24));
            }
            else if (field.IsNumeric)
                name = field.Name.TrimEnd('_') + "#";
            return "{" +
                "\"range\": {" +
                    "\"" + name + "\": {" +
                        "\"from\": \"" + from + "\"," +
                        "\"to\": \"" + to + "\"" +
                    "}" +
                "}" +
            "}";
        }

        public string BuildQuery(string sortList)
        {
            Criterion criterion = BuildCriterion();
            List<string> sortItems = sortList.Split('_').ToList<string>();

            string elasticQuery = new QueryBuilder<SearchOLD>()
                .Query(q => q
                     .Bool(b => b
                        .Must(m => m
                            .Text(text => text
                                .Field("job_name")
                                .Query(ToUpper(this.job_name))
                                .Type(TextQueryType.phrase_prefix))
                            .Text(text => text
                                .Field("report_name")
                                .Query(ToUpper(this.report_name))
                                .Type(TextQueryType.phrase_prefix))
                            .Text(text => text
                                .Field("report_long_name")
                                .Query(ToUpper(this.report_long_name))
                                .Type(TextQueryType.phrase_prefix))
                            .Text(text => text
                                .Field("report_short_name")
                                .Query(ToUpper(this.report_short_name))
                                .Type(TextQueryType.phrase_prefix))
                            .Text(text => text
                                .Field("user_name")
                                .Query(ToUpper(this.user_name))
                                .Type(TextQueryType.phrase_prefix))
                            .QueryString(qs => qs
                                .DefaultField(search => search.divisa).Query(this.divisa))
                            .QueryString(qs => qs
                                .DefaultField(search => search.categoria).Query(this.categoria))
                            .QueryString(qs => qs
                                .DefaultField(search => search.causale).Query(this.causale))
                                 .QueryString(qs => qs
                                .DefaultField(search => search.carta).Query(this.carta))
                            .QueryString(qs => qs
                                .DefaultField(search => search.ente).Query(this.ente))
                            .QueryString(qs => qs
                                .DefaultField(search => search.tabulato).Query(this.tabulato))
                        )
                    )
                )
                .Size(100000)
                .Filter(f => f
                    .And(a => a
                        .Range(r => r
                            .Field(search => search.date_elab)
                            .From(criterion.DateFrom)
                            .To(criterion.DateTo)
                        )
                        .Range(r => r
                            .Field(search => search.job_date_time)
                            .From(criterion.JobDateFrom)
                            .To(criterion.JobDateTo)
                        )
                        .Range(r => r
                            .Field(search => search.report_date_time)
                            .From(criterion.ReportDateFrom)
                            .To(criterion.ReportDateTo)
                        )
                         .Range(r => r
                            .Field(search => search.data_lav)
                            .From(criterion.DataLavFrom)
                            .To(criterion.DataLavTo)
                        )
                        .NumericRange(r => r
                            .Field(search => search.conto)
                            .From(criterion.ContoFrom)
                            .To(criterion.ContoTo)
                            .IncludeLower(true)
                            .IncludeUpper(true)
                        )
                        .NumericRange(r => r
                            .Field(search => search.dipendenza)
                            .From(criterion.DipendenzaFrom)
                            .To(criterion.DipendenzaTo)
                            .IncludeLower(true)
                            .IncludeUpper(true)
                        )
                        .NumericRange(r => r
                            .Field(search => search.sottoconto)
                            .From(criterion.SottocontoFrom)
                            .To(criterion.SottocontoTo)
                            .IncludeLower(true)
                            .IncludeUpper(true)
                        )
                        .NumericRange(r => r
                            .Field(search => search.sportello)
                            .From(criterion.SportelloFrom)
                            .To(criterion.SportelloTo)
                            .IncludeLower(true)
                            .IncludeUpper(true)
                        )
                        .NumericRange(r => r
                            .Field(search => search.esercente)
                            .From(criterion.EsercenteFrom)
                            .To(criterion.EsercenteTo)
                            .IncludeLower(true)
                            .IncludeUpper(true)
                        )
                        .NumericRange(r => r
                            .Field(search => search.stabilimento)
                            .From(criterion.StabilimentoFrom)
                            .To(criterion.StabilimentoTo)
                            .IncludeLower(true)
                            .IncludeUpper(true)
                        )
                        .NumericRange(r => r
                            .Field(search => search.cassa)
                            .From(criterion.CassaFrom)
                            .To(criterion.CassaTo)
                            .IncludeLower(true)
                            .IncludeUpper(true)
                        )
                        .NumericRange(r => r
                            .Field(search => search.importo)
                            .From(criterion.ImportoFrom)
                            .To(criterion.ImportoTo)
                            .IncludeLower(true)
                            .IncludeUpper(true)
                        )
                    )
                )
                //.Sort(s => s
                //   .Field(search => search.agenzia, PlainElastic.Net.SortDirection.asc)
                //   .Field(search => search.date_elab, PlainElastic.Net.SortDirection.desc)
                // ) 
                //d.GetType().GetProperty("value2").GetValue(d, null);
               .BuildBeautified();

            elasticQuery = elasticQuery.Remove(elasticQuery.Length - 1);
            elasticQuery += ", \"sort\" : [ ";
            foreach (string item in sortItems)
            {
                elasticQuery += " { \"" + item + "\" : {\"order\" : \"asc\"} } , ";
            }
            elasticQuery += " { \"date_elab\" : {\"order\" : \"desc\"} } ] ";
            elasticQuery += "}";
            log.Debug("ELASTIC SEARCH QUERY: " + elasticQuery);
            return elasticQuery;
        }

        private Criterion BuildCriterion()
        {
            Business.Criterion c = new Business.Criterion();
            if (this.date_elab_From != null && this.date_elab_From != "")
                c.DateFrom = String.Format("{0:s}", DateTime.Parse(this.date_elab_From));
            if (this.date_elab_To != null && this.date_elab_To != "")
                c.DateTo = String.Format("{0:s}", DateTime.Parse(this.date_elab_To).AddHours(24));

            if (this.report_date_time_From != null && this.report_date_time_From != "")
                c.ReportDateFrom = String.Format("{0:s}", DateTime.Parse(this.report_date_time_From));
            if (this.report_date_time_To != null && this.report_date_time_To != "")
                c.ReportDateTo = String.Format("{0:s}", DateTime.Parse(this.report_date_time_To).AddHours(24));

            if (this.job_date_time_From != null && this.job_date_time_From != "")
                c.JobDateFrom = String.Format("{0:s}", DateTime.Parse(this.job_date_time_From));
            if (this.job_date_time_To != null && this.job_date_time_To != "")
                c.JobDateTo = String.Format("{0:s}", DateTime.Parse(this.job_date_time_To).AddHours(24));

            if (this.data_lav_From != null && this.data_lav_From != "")
                c.DataLavFrom = String.Format("{0:s}", DateTime.Parse(this.data_lav_From));
            if (this.data_lav_To != null && this.data_lav_To != "")
                c.DataLavTo = String.Format("{0:s}", DateTime.Parse(this.data_lav_To).AddHours(24));

            if (this.conto_From != null && this.conto_From != "")
                c.ContoFrom = this.conto_From;
            if (this.conto_To != null && this.conto_To != "")
                c.ContoTo = this.conto_To;

            if (this.dipendenza_From != null && this.dipendenza_From != "")
                c.DipendenzaFrom = this.dipendenza_From;
            if (this.dipendenza_To != null && this.dipendenza_To != "")
                c.DipendenzaTo = this.dipendenza_To;

            if (this.sottoconto_From != null && this.sottoconto_From != "")
                c.SottocontoFrom = this.sottoconto_From;
            if (this.sottoconto_To != null && this.sottoconto_To != "")
                c.SottocontoTo = this.sottoconto_To;

            if (this.sportello_From != null && this.sportello_From != "")
                c.SportelloFrom = this.sportello_From;
            if (this.sportello_To != null && this.sportello_To != "")
                c.SportelloTo = this.sportello_To;

            if (this.esercente_From != null && this.esercente_From != "")
                c.EsercenteFrom = this.esercente_From;
            if (this.esercente_To != null && this.esercente_To != "")
                c.EsercenteTo = this.esercente_To;

            if (this.stabilimento_From != null && this.stabilimento_From != "")
                c.StabilimentoFrom = this.stabilimento_From;
            if (this.stabilimento_To != null && this.stabilimento_To != "")
                c.StabilimentoTo = this.stabilimento_To;

            if (this.cassa_From != null && this.cassa_From != "")
                c.CassaFrom = this.cassa_From;
            if (this.cassa_To != null && this.cassa_To != "")
                c.CassaTo = this.cassa_To;

            if (this.importo_From != null && this.importo_From != "")
                c.ImportoFrom = this.importo_From;
            if (this.importo_To != null && this.importo_To != "")
                c.ImportoTo = this.importo_To;
            return c;
        }

        public string BuildQueryBPM()
        {
            Criterion criterion = BuildCriterionBPM();

            string elasticQuery = new QueryBuilder<SearchOLD>()
                .Query(q => q
                     .Bool(b => b
                        .Must(m => m
                            .Text(text => text
                                .Field("lotto_id")
                                .Query(this.lotto_id)
                                .Type(TextQueryType.phrase_prefix))
                            .Text(text => text
                                .Field("tipo_doc")
                                .Query(this.tipo_doc)
                                .Type(TextQueryType.phrase_prefix))
                        )
                    )
                )
                .Size(100000)
                .Filter(f => f
                    .And(a => a
                        .Range(r => r
                            .Field(search => search.data_elab)
                            .From(criterion.DataElabFrom)
                            .To(criterion.DataElabTo)
                        )
                        .Range(r => r
                            .Field(search => search.data_rif)
                            .From(criterion.DataRifFrom)
                            .To(criterion.DataRifTo)
                        )
                        .NumericRange(r => r
                            .Field(search => search.conto)
                            .From(criterion.ContoFrom)
                            .To(criterion.ContoTo)
                            .IncludeLower(true)
                            .IncludeUpper(true)
                        )
                        .NumericRange(r => r
                            .Field(search => search.agenzia)
                            .From(criterion.AgenziaFrom)
                            .To(criterion.AgenziaTo)
                            .IncludeLower(true)
                            .IncludeUpper(true)
                        )
                    )
                )
                .BuildBeautified();
            log.Debug("ELASTIC SEARCH QUERY: " + elasticQuery);
            return elasticQuery;
        }

        private Criterion BuildCriterionBPM()
        {
            Business.Criterion c = new Business.Criterion();
            if (this.data_elab_From != null && this.data_elab_From != "")
                c.DataElabFrom = String.Format("{0:s}", DateTime.Parse(this.data_elab_From));
            if (this.data_elab_To != null && this.data_elab_To != "")
                c.DataElabTo = String.Format("{0:s}", DateTime.Parse(this.data_elab_To).AddHours(24));

            if (this.data_rif_From != null && this.data_rif_From != "")
                c.DataRifFrom = String.Format("{0:s}", DateTime.Parse(this.data_rif_From));
            if (this.data_rif_To != null && this.data_rif_To != "")
                c.DataRifTo = String.Format("{0:s}", DateTime.Parse(this.data_rif_To).AddHours(24));

            if (this.conto_From != null && this.conto_From != "")
                c.ContoFrom = this.conto_From;
            if (this.conto_To != null && this.conto_To != "")
                c.ContoTo = this.conto_To;

            if (this.agenzia_From != null && this.agenzia_From != "")
                c.AgenziaFrom = this.agenzia_From;
            if (this.agenzia_To != null && this.agenzia_To != "")
                c.AgenziaTo = this.agenzia_To;
            return c;
        }

        private void QueryBuild()
        {
            //string query = "{ \"query\": { \"bool\": { \"must\": [";
            //if (this.job_name != null)
            //    query += "{ \"query_string\": { \"default_field\": \"job_name\", \"query\": \"" + this.job_name + "*\" } },";
            //if (this.report_name != null)
            //    query += "{ \"query_string\": { \"default_field\": \"report_name\", \"query\": \"" + this.report_name + "*\" } },";
            //if (this.dipendenza != null)
            //    query += "{ \"query_string\": { \"default_field\": \"dipendenza\", \"query\": \"" + this.dipendenza + "*\" } },";
            //if (this.conto != null)
            //    query += "{ \"query_string\": { \"default_field\": \"conto\", \"query\": \"" + this.conto + "*\" } },";
            //query = query.Remove(query.Length - 1, 1);
            //query += "]}}, \"size\": 100";
            //if (criterion.DateFrom != null && criterion.DateTo != null)
            //{
            //    query += ",\"filter\": { \"and\": [ { \"range\": {";
            //    query += " \"date_elab\": {  \"from\": \"" + criterion.DateFrom + "\", \"to\": \"" + criterion.DateTo + "\" } } } ] }";
            //}
            //query += "}";
            //string s = elasticQuery.Replace("\r\n", "").Replace("\\","");
        }
        #endregion
    }
}