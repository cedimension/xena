﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace XReportSearch.Business
{
    [Serializable]

    public class User
    {

        private string _profilo;
        private string[] _altprofilo;
        public string Username { get; set; }
        public string TokenString { get; set; }
        public string Torre { get; set; }
        public string TorrePG { get; set; }
        //public string Profilo { get { if (this.isAustrian() && "X1N4UVE".Equals(CodAuthor)) { return "BACAHR"; } else { return _profilo; } } set { _profilo = value; } }
        public string Profilo { get { if (this.isAustrian() && "X1N4UV".Equals(CodAuthor)) { return _profilo; } else { return _profilo; } } set { _profilo = value; } }
        //public string AltProfilo { get { if (this.isAustrian() && ("X1N4UVE".Equals(CodAuthor) || "X1N3UVE".Equals(CodAuthor))) { return "BACA"; } else { return _altprofilo; } } set { _altprofilo = value; } }
        public string[] AltProfilo { get { if (this.isAustrian()) {
            if ("X1N6UVE".Equals(CodAuthor)) { return new string[] { "BACAHR", "BACA", "BACAEG" }; }
            else if ("X1N4UV".Equals(CodAuthor)) { return new string[] {"BACAHR", "BACA"}; }
            else if ("X1N5UVE".Equals(CodAuthor)) { return new string[] { _profilo + "L", "BACA", "BACAEG"}; }
            else if ("X1N3UV".Equals(CodAuthor)) { return new string[] { _profilo + "L", "BACA" }; }
            else if ("X1N2UV".Equals(CodAuthor)) { return new string[] { _profilo + "L" }; }
            else { return _altprofilo; }
            }else{return _altprofilo;}} set { _altprofilo = value; } }
        public string CodAuthor { get; set; }
        public bool IsRoot { get; set; }
        public string SportCont { get; set; }
        public string OrganizationUnit { get; set; }

        public User()
        {
        }

        public User(string Username, string TokenString)
        {
            this.Username = Username;
            this.TokenString = TokenString;
        }

        public User(string Username, string TokenString, string Profilo, string Torre, string CodAuthor)
        {
            this.Username = Username;
            this.TokenString = TokenString;
            this.Profilo = Profilo;
            this.Torre = Torre;
            this.CodAuthor = CodAuthor;
        }

        public User(string Username, string TokenString, string Profilo, string Torre, string CodAuthor, string SportCont, string OrganizationUnit)
        {
            this.Username = Username;
            this.TokenString = TokenString;
            this.Profilo = Profilo;
            this.Torre = Torre;
            this.CodAuthor = CodAuthor;
            this.SportCont = SportCont;
            this.OrganizationUnit = OrganizationUnit;
        }

       public bool isAustrian(){
           if (this.TorrePG != null)
           {
               return ("BA".Equals(TorrePG) || "MA".Equals(TorrePG));
           }
           else if (this.Torre!= null) {
               return ("BA".Equals(Torre) || "MA".Equals(Torre));
           }
           else if (ConfigurationManager.AppSettings["ENV"]!= null) {
               return "BA".Equals(ConfigurationManager.AppSettings["ENV"].ToString().ToUpper())
                   || "MA".Equals(ConfigurationManager.AppSettings["ENV"].ToString().ToUpper());
           }
            return false;
        }

       public string getGlobalEnv()
       {
           if (this.TorrePG != null && !this.TorrePG.Equals(""))
           {
               return TorrePG;
           }
           else if (this.Torre != null && !this.Torre.Equals(""))
           {
               return Torre;
           }
           else if (ConfigurationManager.AppSettings["ENV"] != null)
           {
               return ConfigurationManager.AppSettings["ENV"].ToString().ToUpper();
                   
           }
           return "N/A";
       }

       public string[] getRealAltProfilo { get { return _altprofilo; } set { _altprofilo = value; } }

        public override string ToString()
        {
            return "Username: " + this.Username + "\nToken: " + this.TokenString + "Torre: " + this.Torre;
        }

    }
}