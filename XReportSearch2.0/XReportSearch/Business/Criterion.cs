﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// NON PIU' USATA
/// </summary>
namespace XReportSearch.Business
{
    public class Criterion
    {
        public string FullText { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public bool? Active { get; set; }
        public string ContoFrom { get; set; }
        public string ContoTo { get; set; }
        public string DipendenzaFrom { get; set; }
        public string DipendenzaTo { get; set; }
        public string SottocontoFrom { get; set; }
        public string SottocontoTo { get; set; }
        public string SportelloFrom { get; set; }
        public string SportelloTo { get; set; }
        public string EsercenteFrom { get; set; }
        public string EsercenteTo { get; set; }
        public string StabilimentoFrom { get; set; }
        public string StabilimentoTo { get; set; }
        public string CassaFrom { get; set; }
        public string CassaTo { get; set; }
        public string ReportDateFrom { get; set; }
        public string ReportDateTo { get; set; }
        public string JobDateFrom { get; set; }
        public string JobDateTo { get; set; }
        public string ImportoFrom { get; set; }
        public string ImportoTo { get; set; }
        public string DataLavFrom { get; set; }
        public string DataLavTo { get; set; }
        
        // BPM
        public string AgenziaFrom { get; set; }
        public string AgenziaTo { get; set; }
        public string DataElabFrom { get; set; }
        public string DataElabTo { get; set; }
        public string DataRifFrom { get; set; }
        public string DataRifTo { get; set; }
    }

}