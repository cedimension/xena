﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XReportSearch.Business
{
    public class Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int failed { get; set; }
    }
}