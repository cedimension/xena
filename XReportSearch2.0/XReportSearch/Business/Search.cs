﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PlainElastic.Net.Queries;
using System.ComponentModel;
using PlainElastic.Net.IndexSettings;
using PlainElastic.Net.Utils;
using PlainElastic.Net.Mappings;
using log4net;
using XReportSearch.Utils;

namespace XReportSearch.Business
{
    public class Search
    {
        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Properties
        [DefaultValue(null)]
        public List<Field> fields { get; set; }
        #endregion

        #region Methods
        private string ToUpper(string s)
        {
            if (s != null)
                s = s.ToUpper();
            return s;
        }
 
        public string BuildQueryManual(string sortList,int rowsPage)
        {
            string query =  " { " + 
                                "\"query\": " + 
                                    "{ \"bool\": " +
                                        "{ \"must\": " + 
                                            "[ ";
            string queryMiddle = "], " +
                                        "\"must_not\": [ ], " +
                                        "\"should\": [ ] " +
                                    "}" +
                                "}, " +
                                "\"from\": 0, " +
                                //"\"size\": "+(rowsPage*2).AsString()+", ";
                                "\"size\": " + (rowsPage * 30).AsString() + ", ";
            string queryEnd = "}";
            string queryRes = "";
            foreach (Field f in this.fields)
            {
                if (f.Value == "" && f.Value_from == "" && f.Value_to == "")
                    continue;
                if (f.IsDate || f.IsNumeric)
                    queryRes += BuildRange(f);
                else
                    queryRes += BuildPrefix(f);
                queryRes += ",";
            }
            string finalQuery = query + queryRes.TrimEnd(',') + queryMiddle + this.BuildSort(sortList) + queryEnd;
            return finalQuery;
        }

        private string BuildPrefix(Field field)
        {
            return "{" +
                        "\"prefix\": {" +
                            "\"" + field.Name + "\" : " + "\"" + field.Value + "\"" +
                        "}" +
                    "}";
        }

        private string BuildRange(Field field)
        {
            string from = field.Value_from;
            string to = field.Value_to;
            string name = field.Name;
            if (field.IsDate)
            {
                from = String.Format("{0:s}", DateTime.Parse(field.Value_from));
                to = String.Format("{0:s}", DateTime.Parse(field.Value_to).AddHours(24));
            }
            else if (field.IsNumeric)
                name = field.Name.TrimEnd('_') + "#";
            return "{" +
                "\"range\": {" +
                    "\"" + name + "\": {" +
                        "\"from\": \"" + from + "\"," +
                        "\"to\": \"" + to + "\"" +
                    "}" +
                "}" +
            "}";
        }

        private string BuildSort(string sortList)
        {
            List<string> sortItems = sortList.Split('_').ToList<string>();
            string sort = "\"sort\" : [ ";
            foreach (string item in sortItems)
            {
                sort += " { \"" + item + "\" : {\"order\" : \"asc\"} } , ";
            }
            sort += " { \"date_elab\" : {\"order\" : \"desc\"} } ";
            sort += "] ";
            return sort;
        }

        #endregion
    }
}