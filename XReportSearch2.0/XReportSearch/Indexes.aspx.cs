﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Configuration;
using System.Web.Services.Protocols;
using XReportSearch.Utils;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;
using XReportSearch.Business;


namespace XReportSearch
{
    public partial class Indexes : System.Web.UI.Page
    {
        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog log_ES = LogManager.GetLogger("ES_Log");
        #endregion

        #region Main Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Navigation.CombosSources = null;
                Navigation.CombosValues = null;
                Navigation.DynamicFormControls = null;
                LoadIndexes();
            }
            else
            {
                if (Navigation.DynamicFormControls != null)
                    LoadDynamicControls();
            }
        }
        #endregion

        #region Private Methods
        private void LoadIndexes()
        {
            try
            {
                log.Info("LoadIndexes() Started");
                XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface();
                XReportWebIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.DocumentData Data = BuildDocumentData1();
                XReportWebIface.Timeout = 600000;
                Navigation.IndexesData = XReportWebIface.getIndexes(Data);
                if (BindIndexCombo())
                    LoadIndexValues(true);
                log.Info("LoadIndexes() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadIndexes() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadIndexes() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private void LoadIndexValues(bool first)
        {
            try
            {
                log.Info("LoadIndexValues() Started");
                XReportWebIface.xreportwebiface XReportWebIface = new XReportWebIface.xreportwebiface();
                XReportWebIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.Timeout = 600000;
                if (first)
                    BuildDocumentData2();
                XReportWebIface.DocumentData Data = null; 
                Data = XReportWebIface.getIndexes(Navigation.IndexesData);
                if (Data.IndexEntries != null && Data.DISTINCT != "*")
                {
                    FillDictionaries(Data.IndexEntries[0].Columns[0].colname, Utility.IndexEntriesToTable2(Data.IndexEntries));
                    LoadDynamicControls();
                }
                else
                {
                    this.b_doc.Enabled = true;
                    this.b_doc.Visible = true;
                    Navigation.IndexesData = Data;
                }
                log.Info("LoadIndexValues() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadIndexValues() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadIndexValues() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private XReportWebIface.DocumentData BuildDocumentData1()
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();

                Entry.JobReportId = Navigation.ViewParameters.Split(';')[0];
                Entry.Columns = new XReportWebIface.column[0] { };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };
                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private void BuildDocumentData2()
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                Entry.JobReportId = Navigation.ViewParameters.Split(';')[0];
                Entry.Columns = new XReportWebIface.column[0] { };
                Navigation.IndexesData.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        private void FillDictionaries(string colname, DataTable source)
        {
            if (Navigation.CombosSources == null)
                Navigation.CombosSources = new Dictionary<string, DataTable>();
            if (Navigation.CombosValues == null)
                Navigation.CombosValues = new Dictionary<string, string>();
            if (!Navigation.CombosSources.ContainsKey(colname))
                Navigation.CombosSources.Add(colname, source);
            if (!Navigation.CombosValues.ContainsKey(colname))
                Navigation.CombosValues.Add(colname, "");
        }

        private void CreateDynamicControl(string dataValue, DataTable source, string selValue)
        {
            List<HtmlTableRow> rows = this.BuildControl("combobox", dataValue, source, selValue);
            foreach (HtmlTableRow row in rows)
            {
                this.ControlsTable.Rows.Insert(this.ControlsTable.Rows.Count, row);
                if (Navigation.DynamicFormControls == null)
                    Navigation.DynamicFormControls = new List<HtmlTableRow>();
                Navigation.DynamicFormControls.Add(row);
            }
        }

        private void LoadDynamicControls()
        {
            if (Navigation.DynamicFormControls != null)
            {
                foreach (HtmlTableRow row in Navigation.DynamicFormControls)
                {
                    this.ControlsTable.Rows.Remove(row);
                    row.Dispose();
                }
                Navigation.DynamicFormControls.Clear();
            }
           
            foreach (KeyValuePair<string, DataTable> item in Navigation.CombosSources)
                CreateDynamicControl(item.Key, item.Value, Navigation.CombosValues[item.Key]);
        }

        private AjaxControlToolkit.ComboBox CreateCombo(string dataValue, DataTable source, string selValue)
        {
            AjaxControlToolkit.ComboBox cb = new AjaxControlToolkit.ComboBox();
            cb.ID = dataValue;
            cb.AutoPostBack = true;
            cb.DropDownStyle = AjaxControlToolkit.ComboBoxStyle.DropDownList;
            cb.CssClass = "combobox";
            cb.EnableViewState = false;
            cb.RenderMode = AjaxControlToolkit.ComboBoxRenderMode.Block;

            ListItem listItem = new ListItem();
            listItem.Text = "--- Selezionare ---";
            listItem.Value = "";
            cb.Items.Add(listItem);

            cb.SelectedIndexChanged += new EventHandler(ComboBox_SelectedIndexChanged);
            BindCombo(ref cb, dataValue, source);
            if (selValue != "")
            {
                cb.SelectedValue = selValue;
                cb.Enabled = false;
            }
            return cb;
        }

        private List<HtmlTableRow> BuildControl(string typeControl, string dataValue, DataTable source, string selValue)
        {
            List<HtmlTableRow> rows = new List<HtmlTableRow>();
            HtmlTableRow row = new HtmlTableRow();
            row.Attributes.Add("class", "data");
            HtmlTableCell cellControl = new HtmlTableCell();
            HtmlTableCell cellControlLabel = new HtmlTableCell();
            cellControl.Width = "300px";

            switch (typeControl.ToLower())
            {
                case "combobox":
                    cellControlLabel.Controls.Add(Utility.CreateLabel(dataValue, dataValue));
                    cellControl.Controls.Add(CreateCombo(dataValue, source, selValue));
                    break;
                case "button":
                    ImageButton b = CreateButton();
                    cellControl.Controls.Add(b);
                    cellControl.Align = "right";
                    break;
                default:
                    break;
            }
            row.Cells.Add(cellControlLabel);
            row.Cells.Add(cellControl);
            rows.Insert(0, row);
            return rows;
        }

        private bool BindIndexCombo()
        {
            this.ddl_index.DataSource = Utility.IndexEntriesToTable2(Navigation.IndexesData.IndexEntries);
            this.ddl_index.DataTextField = "IndexName";
            this.ddl_index.DataValueField = "IndexName";
            this.ddl_index.DataBind();
            this.ddl_index.Items.Insert(0, new ListItem("---Selezionare---", "-1"));
            this.ddl_index.SelectedIndex = -1;
            if (this.ddl_index.Items.Count == 2)
            {
                this.ddl_index.SelectedIndex = 1;
                Navigation.IndexesData.IndexName = this.ddl_index.SelectedItem.Text;
                this.ddl_index.Enabled = false;
                return true;
            }
            else
                return false;
        }

        private void BindCombo(ref AjaxControlToolkit.ComboBox combo, string DataValue, DataTable source)
        {
            combo.DataSource = source;
            combo.DataTextField = DataValue;
            combo.DataValueField = DataValue;
            combo.DataBind();
            combo.Items.Insert(0, new ListItem("---Selezionare---", "-1"));
            combo.SelectedIndex = -1;
        }

        private ImageButton CreateButton()
        {
            ImageButton b = new ImageButton();
            b.ID = "b_doc";
            b.Attributes.Add("runat", "server");
            b.CommandName = "ViewFile";
            b.ImageUrl = "~/Images/icona_pdf.png";
            b.ToolTip = "View File";
            b.Click += new ImageClickEventHandler(b_doc_Click);
            b.Width = new Unit(20, UnitType.Pixel);
            b.Height = new Unit(20, UnitType.Pixel);
            b.Visible = true;
            return b;
        }
        #endregion

        #region Events
        protected void ComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            Navigation.IndexesData.DISTINCT = ((AjaxControlToolkit.ComboBox)sender).ID;

            XReportWebIface.column index = new XReportWebIface.column();
            index.colname = ((AjaxControlToolkit.ComboBox)sender).ID;
            index.Value = ((AjaxControlToolkit.ComboBox)sender).SelectedValue;
            index.Max = ((AjaxControlToolkit.ComboBox)sender).SelectedValue;
            index.Min = ((AjaxControlToolkit.ComboBox)sender).SelectedValue;

            if (!Navigation.CombosValues.ContainsKey(index.colname))
                Navigation.CombosValues.Add(index.colname, index.Value);
            else
                Navigation.CombosValues[index.colname] = index.Value;

            List<XReportWebIface.column> columns = Navigation.IndexesData.IndexEntries[0].Columns.ToList();
            columns.Add(index);
            Navigation.IndexesData.IndexEntries[0].Columns = columns.ToArray();
            LoadIndexValues(false);       
        }

        protected void ddl_index_SelectedIndexChanged(object sender, EventArgs e)
        {
            Navigation.IndexesData.IndexName = this.ddl_index.SelectedItem.Text;
            this.ddl_index.Enabled = false;
            LoadIndexValues(true);
        }

        protected void b_doc_Click(object sender, ImageClickEventArgs e)
        {
            string listPag = "";
            string jrid = "";
            foreach (XReportWebIface.IndexEntry entry in Navigation.IndexesData.IndexEntries)
            {
                jrid = entry.JobReportId;
                listPag += entry.JobReportId + "," + entry.FromPage + "," + entry.ForPages + ",";
            }
            if (listPag != "")
                listPag = listPag.Remove(listPag.Length-1);
            Navigation.ViewParameters = jrid + ";" + listPag;
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFile.aspx?" + SHA.GenTimeSHA256String() + "');", true);
        }
        #endregion
    }
}