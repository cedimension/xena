﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Configuration;
using System.Web.Services.Protocols;
using XReportSearch.Utils;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;
using PlainElastic.Net.Queries;
using XReportSearch.Business;
using PlainElastic.Net;
using PlainElastic.Net.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.Script.Services;
using Microsoft.Web.Administration;

namespace XReportSearch
{
    public partial class SearchHistory : System.Web.UI.Page
    {
        #region Log
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Main Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Navigation.User == null)
            {
                if (ConfigurationManager.AppSettings["DEBUG"].ToString() == "1")
                    Navigation.User = new User("xreport", "SUPER", "C0X1NADM", "C0", "X1NADM");
                else
                {
                    Navigation.Error = "Session ended. Please reload the application.";
                    Response.Redirect("~/Error.aspx");
                }
            }
            if (Page.Validators != null && Page.Validators.Count > 0)
                ScriptManager.RegisterOnSubmitStatement(this, Page.GetType(), "", "fnOnUpdateValidators()");
            //ScriptManager _scriptManager = ScriptManager.GetCurrent(this.Page);
            //_scriptManager.RegisterPostBackControl(this.gv_search);
            //this.lb_User.Text = "User: " + Navigation.User.Username + " - Profile: " + Navigation.User.Profilo;
            if (!Page.IsPostBack)
            {
                //if (Navigation.User != null)
                //    this.lb_User.Text += Navigation.User.Username;   
                LoadIndici();             
                //setta i due valori nell' indice
                if (Navigation.ViewParameters != null)
                    ChooseIndici();
                LoadDynamicControls();
            }
            else
            {
                BindGridView();
                LoadDynamicControls();
            }
        }
        #endregion

        #region Private Methods
        private void LoadHistoryReports()
        {
            if (Navigation.Search != null)
            {
                string query = "";
                if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.BPM.ToString())
                    query = Navigation.Search.BuildQueryBPM();
                else
                    query = Navigation.Search.BuildQuery(this.ddl_type.SelectedValue.ToString());
                string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
                int hostPort = int.Parse(ConfigurationManager.AppSettings["ElasticSearchHost_Port"]);
                var connection = new ElasticConnection(host, hostPort);
                string result = connection.Post(Commands.Search(Navigation.CurrentIndex, Navigation.CurrentType), query);
                log.Debug("ELASTIC SEARCH COMMANDS: " + Commands.Search(Navigation.CurrentIndex, Navigation.CurrentType).ToString());
                var serializer = new JsonNetSerializer();
                RootObject root = serializer.Deserialize<RootObject>(result);
                Navigation.GridSource = root.hits.hits;
                BindGridView();
            }
        }

        private void ChooseIndici()
        {
            if (Navigation.ViewParameters != null)
                BindValues(ref this.ddl_indice, ref this.ddl_type, Navigation.ViewParameters);
        }

        private void LoadIndici()
        {
            if (Navigation.Indici == null)
                GetIndici();
            BindCombo(ref this.ddl_indice, Navigation.Indici);
        }

        private void GetIndici()
        {
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int hostPort = int.Parse(ConfigurationManager.AppSettings["ElasticSearchHost_Port"]);
            var connection = new ElasticConnection(host, hostPort);
            string types = connection.Get(Commands.GetMapping("_all"));
            var serializer = new JsonNetSerializer();
            IDictionary<string, object> entryJ = serializer.Deserialize<IDictionary<string, object>>(types);
            Navigation.Indici = entryJ;
        }

        private void BindGridView()
        {
            if (this.ddl_indice.SelectedIndex > 0 && this.ddl_indice.SelectedIndex > 0)
            {
                object valore = Navigation.Indici[this.ddl_indice.SelectedValue];
                JObject listaTipi = (JObject)valore;
                List<BoundField> fields = Utility.BuildGrid(listaTipi, this.ddl_type.SelectedValue, this.gv_search);
                int width = 0;
                BoundField f = fields.Find(delegate(BoundField bf) { return bf.HeaderText == "Id"; });
                if (f == null && fields.Count > 0)
                    fields.Add(Utility.BuildBoundField("_id"));
                if (fields.Count > 0)
                    width = 65 / fields.Count;
                foreach (BoundField field in fields)
                {
                    if (field.HeaderText == "Report Name")
                    {
                        field.HeaderStyle.Width = Unit.Percentage(20);
                        field.ItemStyle.Width = Unit.Percentage(20);
                    }
                    else
                    {
                        field.HeaderStyle.Width = Unit.Percentage(width);
                        field.ItemStyle.Width = Unit.Percentage(width);
                    }
                    this.gv_search.Columns.Add(field);
                }
            }


            this.gv_search.PageSize = int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
            if (Navigation.CurrentGridPage != -1)
                this.gv_search.PageIndex = Navigation.CurrentGridPage;
            this.gv_search.RowCommand += new GridViewCommandEventHandler(gv_search_OnRowCommand);
            this.gv_search.DataSource = Navigation.GridSource;
            if (Navigation.GridSource != null)
            {
                double pages = (double)Navigation.GridSource.Count / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                int numPages = (int)Math.Ceiling(pages);
                this.gv_search.PagerSettings.FirstPageText = "1";
                this.gv_search.PagerSettings.LastPageText = "" + numPages + "";
            }
            this.gv_search.DataBind();
        }

        private void LoadDynamicControls()
        {
            if (Navigation.DynamicFormControls != null)
            {
                foreach (HtmlTableRow row in Navigation.DynamicFormControls)
                {
                    this.ControlsTable.Rows.Remove(row);
                    row.Dispose();
                }
            }
            if (this.ddl_indice.SelectedIndex != -1 && this.ddl_type.SelectedIndex != -1)
            {
                object valore = Navigation.Indici[Navigation.CurrentIndex];
                JObject listaTipi = (JObject)valore;
                Navigation.Search = new Business.Search();
                List<HtmlTableRow> rows = Utility.BuildForm(listaTipi, this.ddl_type.SelectedValue, ref this.gv_search);
                foreach (HtmlTableRow row in rows)
                    this.ControlsTable.Rows.Insert(this.ControlsTable.Rows.Count, row);
                Navigation.DynamicFormControls = rows;
            }
        }

        private void BindValues(ref AjaxControlToolkit.ComboBox comboBox, ref AjaxControlToolkit.ComboBox comboBoxTipi, string IndexValues)
        {
            if (IndexValues != "")
            {
                string[] valori = IndexValues.Split('-');
                //string index = valori[0];
                //comboBox.SelectedValue = index + "_" + valori[2].ToLower();
                //Navigation.CurrentIndex = index + "_" + valori[2].ToLower();
                //SetComboTipi();
                //comboBoxTipi.SelectedValue = valori[1].Replace(",", "_");
                string index = valori[0];
                string controld = valori[valori.Length - 1];
                string subindexes = IndexValues.Substring(index.Length + 1, (IndexValues.Length - index.Length - controld.Length - 2));
                comboBox.SelectedValue = index.ToLower() + "_" + controld.ToLower();
                Navigation.CurrentIndex = index.ToLower() + "_" + controld.ToLower();
                SetComboTipi();
                comboBoxTipi.SelectedValue = subindexes.Replace(",", "_");
                Navigation.CurrentType = comboBoxTipi.SelectedValue;
            }
        }

        private void SetComboTipi()
        {
            if (this.ddl_indice.SelectedIndex > 0)
            {
                Navigation.CurrentIndex = this.ddl_indice.SelectedValue;
                object valore = Navigation.Indici[Navigation.CurrentIndex];
                JObject listaTipi = (JObject)valore;
                BindComboTipi(ref this.ddl_type, listaTipi.Properties());
            }
        }

        private void BindCombo(ref AjaxControlToolkit.ComboBox comboBox, IDictionary<string, object> dictionary)
        {
            dictionary = dictionary.OrderBy(x => x.Key.Substring(x.Key.Length - 2, 2)).
                ThenBy(x => x.Key).ToDictionary(x => x.Key, y => y.Value);
            comboBox.DataSource = dictionary;
            comboBox.DataValueField = "Key";
            comboBox.DataTextField = "Key";
            comboBox.DataBind();
            comboBox.Items.Insert(0, new ListItem("---Selezionare---", "0"));
        }

        private void BindComboTipi(ref AjaxControlToolkit.ComboBox comboBox, IEnumerable<JProperty> source)
        {
            source = source.OrderBy(x => x.Name);
            comboBox.DataSource = source;
            comboBox.DataValueField = "Name";
            comboBox.DataTextField = "Name";
            comboBox.DataBind();
            comboBox.Items.Insert(0, new ListItem("---Selezionare---", "0"));
        }

        private void ClearGrid()
        {
            this.gv_search.Columns.Clear();
            Navigation.CurrentGridPage = 0;
            this.gv_search.DataSource = null;
            this.gv_search.DataBind();
            this.gv_search.Visible = false;
        }

        private void AddImageButton(GridViewRow row)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                ImageButton b = new ImageButton();
                b.ID = "b_doc";
                b.Attributes.Add("runat", "server");
                b.CommandName = "ViewFile";
                b.ImageUrl = "~/Images/List.png";
                b.ToolTip = "View File";
                b.Width = new Unit(20, UnitType.Pixel);
                b.Height = new Unit(20, UnitType.Pixel);
                b.Visible = true;
                TableCell cell = new TableCell();
                cell.Width = new Unit(5, UnitType.Percentage);
                cell.Controls.Add(b);
                row.Cells.AddAt(row.Cells.Count, cell);
            }
            else if (row.RowType == DataControlRowType.Header)
            {
                TableCell cell = new TableCell();
                cell.Width = new Unit(5, UnitType.Percentage);
                row.Cells.AddAt(row.Cells.Count, cell);
            }
        }

        private void SetAmbiente()
        {
            string controld = Navigation.CurrentIndex.Substring(this.ddl_indice.SelectedValue.Length - 2, 2);
            //this.lb_Ambiente.Text = "Ambiente:" + controld.ToUpper();
        }
        #endregion

        #region Events
        protected void gv_search_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Navigation.CurrentGridPage = e.NewPageIndex;
            BindGridView();
        }

        protected void buttonElasticSearch_Click(object sender, EventArgs e)
        {
            Navigation.User = new User("xreport", "SUPER", "C0X1NADM", "C0", "X1NADM");
            this.ClearGrid();
            LoadHistoryReports();
            Navigation.Search = null;
            this.gv_search.Visible = true;
            Navigation.CurrentGridPage = -1;
        }

        protected void ddl_indice_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetComboTipi();
            SetAmbiente();
            this.ClearGrid();
        }

        protected void ddl_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddl_type.SelectedIndex > 0)
            {
                Navigation.CurrentType = this.ddl_type.SelectedValue;
                this.ClearGrid();
            }
        }

        protected void gv_search_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            AddImageButton(e.Row);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Hit hit = (Hit)e.Row.DataItem;
                ImageButton b_pdf = (ImageButton)e.Row.FindControl("b_doc");
                string controld = this.ddl_indice.SelectedValue.Substring(this.ddl_indice.SelectedValue.Length - 2, 2);
                if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.BPM.ToString())
                    b_pdf.CommandArgument = hit.lotto_id + ";" + hit._id;
                else
                    b_pdf.CommandArgument = controld + ";" + hit._id + ";" + hit.from_rba + ";" + hit.last_rba + ";" + hit.tot_pages;
            }
        }

        protected void gv_search_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewFile")
            {
                //if (Navigation.Stream != null)
                //    Navigation.Stream.EndRead(Navigation.IAsyncResult);
                Navigation.ViewParameters = e.CommandArgument.ToString();
                //ClientScript.RegisterStartupScript(this.Page.GetType(), "", "window.open('ViewFileHistory.aspx');", true);
                ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFileHistory.aspx');", true);
            }
        }
        #endregion

        #region OLD
        //protected void gv_search_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Pager)
        //    {
        //        GridViewRow gvr = e.Row;
        //        LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //        lb = (LinkButton)gvr.Cells[0].FindControl("p1");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //        lb = (LinkButton)gvr.Cells[0].FindControl("p2");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //        lb = (LinkButton)gvr.Cells[0].FindControl("p4");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //        lb = (LinkButton)gvr.Cells[0].FindControl("p5");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //        lb = (LinkButton)gvr.Cells[0].FindControl("p6");
        //        lb.Command += new CommandEventHandler(lb_Command);
        //    }
        //}

        //void lb_Command(object sender, CommandEventArgs e)
        //{
        //    Navigation.CurrentGridPage = Convert.ToInt32(e.CommandArgument) - 1;
        //    BindGridView();
        //}

        //protected void gv_search_DataBound(object sender, EventArgs e)
        //{
        //    int numPages = 0;
        //    if (Navigation.GridSource != null && Navigation.GridSource.Count > 0)
        //    {
        //        double pages = (double)Navigation.GridSource.Count / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
        //        numPages = (int)Math.Ceiling(pages);

        //        GridViewRow gvrow = this.gv_search.BottomPagerRow;
        //        LinkButton lnk = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //        lnk.Text = "" + numPages + "";
        //        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        //        lblcurrentpage.Text = Convert.ToString(this.gv_search.PageIndex + 1);
        //        int[] page = new int[7];
        //        page[0] = this.gv_search.PageIndex - 2;
        //        page[1] = this.gv_search.PageIndex - 1;
        //        page[2] = this.gv_search.PageIndex;
        //        page[3] = this.gv_search.PageIndex + 1;
        //        page[4] = this.gv_search.PageIndex + 2;
        //        page[5] = this.gv_search.PageIndex + 3;
        //        page[6] = this.gv_search.PageIndex + 4;
        //        for (int i = 0; i < 7; i++)
        //        {
        //            if (i != 3)
        //            {
        //                if (page[i] < 1 || page[i] > this.gv_search.PageCount)
        //                {
        //                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //                    lnkbtn.Visible = false;
        //                }
        //                else
        //                {
        //                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
        //                    lnkbtn.Text = Convert.ToString(page[i]);
        //                    lnkbtn.CommandName = "PageNo";
        //                    lnkbtn.CommandArgument = lnkbtn.Text;
        //                }
        //            }
        //        }
        //        if (this.gv_search.PageIndex == 0)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //            lnkbtn.Visible = false;
        //        }
        //        if (this.gv_search.PageIndex == this.gv_search.PageCount - 1)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //            lnkbtn.Visible = false;
        //        }
        //        if (this.gv_search.PageIndex > this.gv_search.PageCount - 6)
        //        {
        //            Label lbmore = (Label)gvrow.Cells[0].FindControl("nmore");
        //            lbmore.Visible = false;
        //        }
        //        if (this.gv_search.PageIndex > this.gv_search.PageCount - 5)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
        //            lnkbtn.Visible = false;
        //        }
        //        if (this.gv_search.PageIndex < 5)
        //        {
        //            Label lbmore = (Label)gvrow.Cells[0].FindControl("pmore");
        //            lbmore.Visible = false;
        //        }
        //        if (this.gv_search.PageIndex < 4)
        //        {
        //            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
        //            lnkbtn.Visible = false;
        //        }
        //    }
        //}

        #endregion

        #region WebMethods
        [WebMethod]
        public static string[] GetCompletionList(string prefixText, int count, string contextKey)
        {
            string query = Utility.BuildQueryAutocomplete(contextKey, prefixText);
            string host = ConfigurationManager.AppSettings["ElasticSearchHost"];
            int hostPort = int.Parse(ConfigurationManager.AppSettings["ElasticSearchHost_Port"]);
            var connection = new ElasticConnection(host, hostPort);
            string result = connection.Post(Commands.Search(Navigation.CurrentIndex, Navigation.CurrentType), query);

            var serializer = new JsonNetSerializer();
            RootObject root = serializer.Deserialize<RootObject>(result);
            List<string> list = new List<string>();
            foreach (Term t in root.facets.suggest.terms)
                list.Add(t.term);
            return list.ToArray();
        }
        #endregion
    }
}