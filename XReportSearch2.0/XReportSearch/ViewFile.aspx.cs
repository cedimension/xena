﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Configuration;
using System.Web.Services.Protocols;
using XReportSearch.Utils;
using System.Data;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Xml;
using System.Text;
namespace XReportSearch
{
    public partial class ViewFile : System.Web.UI.Page
    {
        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Methods
        private string globUsername = "null";
        private string globCodAuthor = "null";


        
        private string globUsernameParameters = "null"; // user passed by parameters
        private string globCodAuthorParameters = "null"; // CodAuthor passed by parameters
        private string globViewParameters = "null";
        private string globViewParameters2 = "null";
        private string globViewParameters3 = "null";
        private string globSessionParameters = "null";
        
        private string[] globLogParameters = new string[] { "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null", "null"};


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (
                    (Request.Params["key"] != null)
                    && (!Request.Params["key"].Equals(""))
                    )
                {
                    string key = Request.Params["key"];
                    string convertedvalue = key.Replace(" ", "+");
                    if (!Signature.ValidateToken(convertedvalue))
                    {
                        throw new Exception("Link wrong or expired.");
                    }
                    string decryptedvalue = Signature.GetToken(convertedvalue).Value;
                    string encr_equal = "#&##&#"; string encr_and = "&#&&#&"; string encr_log = "&####&";
                    //throw new Exception("FORZATURA");
                    log.Info("List Parameters decrypted " + decryptedvalue);
                    string[] trueparameters = decryptedvalue.Split(new string[] { encr_and }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string element in trueparameters)
                    {
                        string[] keyvalues = element.Split(new string[] { encr_equal }, StringSplitOptions.RemoveEmptyEntries);
                        if ((keyvalues != null) && (keyvalues.Length == 2))
                        {
                            string strvalue = keyvalues[1].Equals("null") ? null : keyvalues[1];
                            switch (keyvalues[0])
                            {
                                case "viep1":
                                    globViewParameters = strvalue;
                                    break;
                                case "viep2":
                                    globViewParameters2 = strvalue;
                                    break;
                                case "viep3":
                                    globViewParameters3 = strvalue;
                                    break;
                                case "username":
                                    globUsernameParameters = strvalue;
                                    break;
                                case "codauthor":
                                    globCodAuthorParameters = strvalue;
                                    break;
                                case "session":
                                    globSessionParameters = strvalue;
                                    break;
                                case "viewlog":
                                    globLogParameters = strvalue.Split(new string[] { encr_log }, StringSplitOptions.RemoveEmptyEntries);
                                    break;
                            }
                        }
                    }
                    if(    (  globUsernameParameters  == null ) || (  globUsernameParameters .Equals("null" ) )
                        || (  globCodAuthorParameters == null ) || (  globCodAuthorParameters.Equals("null" ) )
                        || (globViewParameters == null) || (globViewParameters.Equals("null"))
                        || (globViewParameters2 == null) || (globViewParameters2.Equals("null"))
                        || (globLogParameters == null) ||  (globLogParameters.Length < 11)
                        //|| (  globViewParameters3     == null ) || (  globViewParameters3    .Equals("null" ) )
                        || (  globSessionParameters   == null ) || (  globSessionParameters  .Equals("null" ) ) )
                    {
                        throw new Exception("Some parameters are missing."); 
                    } 

                    if ( !globSessionParameters.Equals(Session.SessionID))
                    {
                        throw new Exception("Session expired - reload the application.");
                    }
                }
                else
                {
                    globViewParameters = Navigation.ViewParameters;
                    globViewParameters2 = Navigation.ViewParameters2;
                    globViewParameters3 = Navigation.ViewParameters3;
                    globLogParameters = Navigation.LogParameters;
                    globUsernameParameters = Navigation.User.Username;
                    globCodAuthorParameters = Navigation.User.CodAuthor;
                }


                //Navigation.User = null; //force the user to null for TEST

                if (Navigation.User == null)
                {
                    if ((globUsernameParameters == null) || (globUsernameParameters.Equals("null")))
                    {
                        //globUsername = "null";
                        string url = "null";
                        string servername = "null";
                        try
                        {
                            url = HttpContext.Current.Request.Url.AbsoluteUri;
                            servername = System.Environment.MachineName;
                }
                        catch { }
                        throw new Exception("Session expired - User [" + globUsernameParameters + "] - OriginalSession [" + globSessionParameters + "] - url [" + url + "] - servername [" + servername + "].");
                    }
                    globUsername = globUsernameParameters;
                    globCodAuthor = globCodAuthorParameters;
                    log.Info("User not found in session - user found in parameters:" + globUsername);
                }
                else
                {
                    log.Info("User found in session:" + Navigation.User.Username);
                    globUsername = Navigation.User.Username;
                    globCodAuthor = Navigation.User.CodAuthor; 
                }


            LoadFileOnline();
        }
            catch (Exception Ex)
            {
                log.Error("Failed to load the report: " + Ex.Message);
                Response.AppendToLog("LoadReport() " + Ex.Message + " " + globUsername);
                //Navigation.Error = Ex.Message;
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "text/plain";
                Response.AddHeader("Content-Disposition", "inline;filename=ERROR");
                if (Ex.Message.Contains("Session expired"))
                {
                    Response.Write("Session expired. Reload the application.");
                }
                else
                {
                Response.Write("Errore: " + Ex.Message);
                }
                Response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();

            }
        }

        private void LoadFileOnline()
        {
            if (globViewParameters != null && globViewParameters != "")
            {
                string[] pars = globViewParameters.Split(';');
                log.Info("Called ViewFile with ViewParameter " + globViewParameters + "---" + globViewParameters2);
                string typeDown = globViewParameters3;
                if (pars.Length >= 4)
                {
                    if (globViewParameters2 == "XLS")
                        //LoadXLS(pars[0], pars[1], pars[2], pars[3]);
                        //string JobReportID, string ReportID, string Pages, string FromPage, string totPages
                        LoadXLS(pars[0], pars[1], pars[2], pars[3], pars[4]);
                    else if (globViewParameters2 == "XLSX")
                        //LoadXLS(pars[0], pars[1], pars[2], pars[3]);
                        //string JobReportID, string ReportID, string Pages, string FromPage, string totPages
                        LoadXLSX(pars[0], pars[1], pars[2], pars[3], pars[4]);
                    else if (globViewParameters2 == "TXT")
                        LoadTxt(pars[0], pars[1], pars[2], pars[3], pars[4], typeDown);
                    else
                        LoadPDF(pars[0], pars[1], pars[2], pars[3], typeDown);
                }
                else
                {
                    //if (globViewParameters2 == "TXT")
                    //{
                    //    LoadTxt(pars[0], pars[1], pars[2], pars[3]);
                    //}
                    //else
                    //{
                        LoadPDFIndex(pars[0], pars[1]);
                    //}
                }
            }
        }

        private void LoadTxt(string JobReportID, string ReportID, string Pages, string FromPage, string totPages, string typeDown)
        {
            try
            {
                log.Info("LoadTxt() Started");
                string startSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture); string logParms = "";
                logParms += "JobReportID == " + JobReportID + "::ReportID == " + ReportID + "::Pages == " + Pages;
                XReportWebIface.xreportwebiface docIface = new XReportWebIface.xreportwebiface(globUsername, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                docIface.Timeout = 600000;

                XReportWebIface.DocumentData Data;
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "text/plain";
                int rangePages = Int32.Parse(ConfigurationManager.AppSettings["RangePages"]); int totP =  Int32.Parse(totPages);

                //if (globViewParameters3 != null && !globViewParameters3.Equals(""))
                if (typeDown != null && (!typeDown.Equals("")))
                {
                    Response.HeaderEncoding = Encoding.GetEncoding("iso-8859-1"); 
                    string fileName = globViewParameters3.ToString()  ;
                    //fileName = HttpUtility.UrlEncode(fileName, Encoding.UTF8);
                    Response.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}.txt\"" , fileName));
                    log.Info("LoadTxt() Download");
                }
                else
                {
                    Response.HeaderEncoding = Encoding.GetEncoding("iso-8859-1"); 
                    Response.AddHeader("Content-Disposition", "inline;filename=" + JobReportID + ReportID + ".txt");
                }
                    Data = BuildDocumentData(JobReportID, ReportID, Pages, FromPage);

                    Data = docIface.getReportEntryTxt(Data);
                    Response.BinaryWrite(Data.DocumentBody);
                    Response.Flush();
                //}
                //command paramters
                //jid + ";" + id + ";" + pages + ";" + fromPage + ";" + reportName + ";" + userTimeRef + ";" + xferStartTime + ";" + totPages;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                globViewParameters3 = null;
                //jid + ";" + id + ";" + pages + ";" + fromPage + ";" + reportName + ";" + userTimeRef + ";" + xferStartTime + ";" + totPages;
                string endSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                log.Info("LoadTxt() user:" + globUsername + ";profile: " + globCodAuthor + ";report_name:" + globLogParameters[4] + ";report_data:" + globLogParameters[6] + ";job_name:" + globLogParameters[7] + ";job_number:" + globLogParameters[8] + ";folder_path:" + globLogParameters[9]);
                Response.AppendToLog("LoadTxt_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + globUsername + "_" + endSP + "_" + logParms);
                //globLogParameters = null;
                //log.Info("LoadTxt_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + globUsername + "_" + endSP + "_" + logParms);
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadTxt() Failed: " + SoapEx.Message);
                Response.AppendToLog("LoadTxt() " + SoapEx.Message + " " + globUsername);
                //Navigation.Error = SoapEx.Message;
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "text/plain";
                Response.AddHeader("Content-Disposition", "inline;filename=ERROR");
                Response.Write("Errore: " + SoapEx.Message);
                Response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception Ex)
            {
                log.Error("LoadTxt() Failed: " + Ex.Message);
                Response.AppendToLog("LoadTxt() " + Ex.Message + " " + globUsername);
                //Navigation.Error = Ex.Message;
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "text/plain";
                Response.AddHeader("Content-Disposition", "inline;filename=ERROR");
                Response.Write("Errore: " + Ex.Message);
                Response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        private string convert_iso_8859_to_unicode(string s_iso_8859)
        {
            Encoding iso_8859 = Encoding.GetEncoding("iso-8859-1");
            Encoding unicode = Encoding.Unicode; 
            byte[] b_iso_8859 = iso_8859.GetBytes(s_iso_8859);
            byte[] b_unicode = Encoding.Convert(iso_8859, unicode, b_iso_8859);
            string s_unicode = unicode.GetString(b_unicode);
            return s_unicode;
        }


        private string convert_iso_8859_to_utf_8(string s_iso_8859)
        {
            Encoding iso_8859 = Encoding.GetEncoding("iso-8859-1");
            Encoding utf_8 = Encoding.UTF8;
            byte[] b_iso_8859 = iso_8859.GetBytes(s_iso_8859);
            byte[] b_utf_8 = Encoding.Convert(iso_8859, utf_8, b_iso_8859);
            string s_utf_8 = utf_8.GetString(b_utf_8);
            return s_utf_8;
        }
        private string convert_unicode_to_utf_8(string s_unicode)
        {
            Encoding unicode = Encoding.Unicode; 
            Encoding utf_8 = Encoding.UTF8;
            byte[] b_unicode = unicode.GetBytes(s_unicode);
            byte[] b_utf_8 = Encoding.Convert(unicode, utf_8, b_unicode);
            string s_utf_8 = utf_8.GetString(b_utf_8);
            return s_utf_8;
        }


        private void LoadPDF(string JobReportID, string ReportID, string Pages, string FromPage, string typeDown)
        {
            try
            {
                log.Info("LoadPDF() Started");
                string startSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture); string logParms = "";
                logParms += "JobReportID == "+JobReportID+"::ReportID == "+ReportID+"::Pages == "+Pages;
                //XReportWebIface.xreportwebiface docIface = new XReportWebIface.xreportwebiface();
                //docIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIface.xreportwebiface docIface = new XReportWebIface.xreportwebiface(globUsername, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                docIface.Timeout = 600000;
                XReportWebIface.DocumentData Data = BuildDocumentData(JobReportID, ReportID, Pages, FromPage);
                Data = docIface.getReportEntryPdf(Data);

                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";
                //if (globViewParameters3 != null && !globViewParameters3.Equals(""))
                if (typeDown != null && (!typeDown.Equals("")) && (!typeDown.Equals("null")))
                {
                    Response.HeaderEncoding = Encoding.GetEncoding("iso-8859-1"); 
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + globViewParameters3.ToString() + ".pdf");
                }
                else
                {
                    Response.HeaderEncoding = Encoding.GetEncoding("iso-8859-1"); 
                    Response.AddHeader("Content-Disposition", "inline;filename=" + Data.FileName + ".pdf");
                }
                
                Response.BinaryWrite(Data.DocumentBody);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                globViewParameters3 = null;
                string endSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                //log.Info("LoadPDF_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + globUsername + "_" + endSP + "_" + logParms);
                log.Info("LoadPdf() user:" + globUsername + ";profile: " + globCodAuthor + ";report_name:" + globLogParameters[4] + ";report_data:" + globLogParameters[6] + ";job_name:" + globLogParameters[7] + ";job_number:" + globLogParameters[8] + ";folder_path:" + globLogParameters[9]);
                Response.AppendToLog("LoadPDF_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + globUsername + "_" + endSP + "_" + logParms);
                //globLogParameters = null;
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadPDF() Failed: " + SoapEx.Message + " " + globUsername);
                //Navigation.Error = SoapEx.Message;
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "text/plain";
                Response.AddHeader("Content-Disposition", "inline;filename=ERROR");
                Response.Write("Errore: " + SoapEx.Message);
                Response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception Ex)
            {
                log.Error("LoadPDF() Failed: " + Ex.Message);
                //Navigation.Error = Ex.Message;
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "text/plain";
                Response.AddHeader("Content-Disposition", "inline;filename=ERROR");
                Response.Write("Errore: " + Ex.Message);
                Response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        private void LoadXLSX(string JobReportID, string ReportID, string Pages, string FromPage, string totPages)
        {
            try
            {
                log.Info("LoadXLSX() Started");
                XReportWebIface.xreportwebiface docIface = new XReportWebIface.xreportwebiface();
                docIface.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                docIface.Timeout = 600000;
                XReportWebIface.DocumentData Data = BuildDocumentData(JobReportID, ReportID, Pages, FromPage);
                Data = docIface.getReportEntryExcel(Data);
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "application/xlsx";
                if (globViewParameters3 != null && !globViewParameters3.Equals("") && !globViewParameters3.Equals("null"))
                {
                    Response.HeaderEncoding = Encoding.GetEncoding("iso-8859-1");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + globViewParameters3.ToString() + ".xlsx");
                }
                else
                {
                    Response.HeaderEncoding = Encoding.GetEncoding("iso-8859-1");
                    Response.AddHeader("Content-Disposition", "inline;filename=" + Data.FileName + ".xlsx");
                }
                Response.BinaryWrite(Data.DocumentBody);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                log.Info("LoadXLSX() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadXLSX() Failed: " + SoapEx.Message);
                //Navigation.Error = SoapEx.Message;
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "text/plain";
                Response.AddHeader("Content-Disposition", "inline;filename=ERROR");
                Response.Write("Errore: " + SoapEx.Message);
                Response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception Ex)
            {
                log.Error("LoadPDF() Failed: " + Ex.Message);
                //Navigation.Error = Ex.Message;
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "text/plain";
                Response.AddHeader("Content-Disposition", "inline;filename=ERROR");
                Response.Write("Errore: " + Ex.Message);
                Response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }


        private void LoadXLS(string JobReportID, string ReportID, string Pages, string FromPage, string totPages)
        {
            try
            {
                log.Info("LoadXLS() Started");
                string startSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture); string logParms = "";
                logParms += "JobReportID == " + JobReportID + "::ReportID == " + ReportID + "::Pages == " + Pages;
                XReportWebIface.xreportwebiface docIface = new XReportWebIface.xreportwebiface(globUsername, ConfigurationManager.AppSettings["XReportWebIfaceUrl"]);
                docIface.Timeout = 600000;

                XReportWebIface.DocumentData Data;
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel"; 
                //Response.ContentType = "text/plain";
                //int rangePages = Int32.Parse(ConfigurationManager.AppSettings["RangePages"]); int totP = Int32.Parse(totPages);
                
                //Response.AddHeader("Content-Disposition", "inline;filename=" + JobReportID + ReportID + ".txt");


                Data = BuildDocumentData(JobReportID, ReportID, Pages, FromPage);

                Data = docIface.getReportEntryTxt(Data);
                if (globViewParameters3 != null && !globViewParameters3.Equals("") &&  !globViewParameters3.Equals("null"))
                {
                    Response.HeaderEncoding = Encoding.GetEncoding("iso-8859-1"); 
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + globViewParameters3.ToString() + ".csv");
                }
                else
                {
                    Response.HeaderEncoding = Encoding.GetEncoding("iso-8859-1"); 
                    Response.AddHeader("Content-Disposition", "inline;filename=" + JobReportID + ReportID + ".csv");
                }
                
                Response.BinaryWrite(Data.DocumentBody);
                Response.Flush(); 
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                globViewParameters3 = null;
                string endSP = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                //log.Info("LoadXLS_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + globUsername + "_" + endSP + "_" + logParms);
                log.Info("LoadXLS() user:" + globUsername + ";profile: " + globCodAuthor + ";report_name:" + globLogParameters[4] + ";report_data:" + globLogParameters[6] + ";job_name:" + globLogParameters[7] + ";job_number:" + globLogParameters[8] + ";folder_path:" + globLogParameters[9]);
                Response.AppendToLog("LoadXLS_Start_at_" + startSP + "_-ID_0-_Times(us::sy::cus::csy):_0::0::0::0_" + globUsername + "_" + endSP + "_" + logParms);
                //globLogParameters = null;
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadXLS() Failed SoapEx: " + SoapEx.Message + " " + globUsername);
                //Navigation.Error = SoapEx.Message;
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "text/plain";
                Response.AddHeader("Content-Disposition", "inline;filename=ERROR");
                Response.Write("Errore: " + SoapEx.Message);
                Response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception Ex)
            {
                log.Error("LoadXLS() Failed: " + Ex.Message);
                //Navigation.Error = Ex.Message;
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "text/plain";
                Response.AddHeader("Content-Disposition", "inline;filename=ERROR");
                Response.Write("Errore: " + Ex.Message);
                Response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        private void LoadPDFIndex(string jrid, string listPag)
        {
            try
            {
                log.Info("LoadPDFIndex() Started");
                XReportWebIface.xreportwebiface XReportWebIFace = new XReportWebIface.xreportwebiface();
                XReportWebIFace.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                XReportWebIFace.Timeout = 600000;
                XReportWebIface.DocumentData Data = BuildDocumentDataIndex(jrid, listPag);
                Data = XReportWebIFace.getReportEntryPdf(Data);
                Response.ClearHeaders();
                Response.ClearContent();
                Response.Buffer = true;
                Response.ContentType = "application/pdf";
                Response.AddHeader("Content-Disposition", "inline;filename=" + Data.FileName + ".pdf");
                Response.BinaryWrite(Data.DocumentBody);
                //Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                log.Info("LoadPDFIndex() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadPDFIndex() Failed: " + SoapEx.Message);
                //Navigation.Error = SoapEx.Message;
                //this.l_error.Visible = true;
                //this.l_error.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadPDFIndex() Failed: " + Ex.Message);
                //Navigation.Error = Ex.Message;
                //this.l_error.Visible = true;
                //this.l_error.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                //throw Ex;
            }
        }

        public static XReportWebIface.DocumentData BuildDocumentData(string JobReportID, string ReportID, string Pages, string fromPage, string pageseparator = null)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column JobReportId = new XReportWebIface.column();
                JobReportId.colname = "JobReportId";
                JobReportId.Value = JobReportID;
                XReportWebIface.column ReportId = new XReportWebIface.column();
                ReportId.colname = "ReportId";
                ReportId.Value = ReportID;
                XReportWebIface.column ListOfPages = new XReportWebIface.column();
                ListOfPages.colname = "ListOfPages";
                ListOfPages.Value = JobReportID + "," + Pages;
                XReportWebIface.column FromPage = new XReportWebIface.column();
                FromPage.colname = "FromPage";
                FromPage.Value = fromPage;
                Entry.Columns = new XReportWebIface.column[4] { JobReportId, ReportId, ListOfPages, FromPage };
                
                if (pageseparator != null)
                {
                    XReportWebIface.column PageSep = new XReportWebIface.column();
                    PageSep.colname = "PageSeparator";
                    PageSep.Value = pageseparator;
                    Entry.Columns = new XReportWebIface.column[] { JobReportId, ReportId, ListOfPages, FromPage, PageSep };
                
                }
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                //Navigation.Error = SoapEx.Message;
                //this.l_error.Visible = true;
                //this.l_error.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                //Navigation.Error = Ex.Message;
                //this.l_error.Visible = true;
                //this.l_error.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                return null;
                //throw Ex;
            }
        }

        private XReportWebIface.DocumentData BuildDocumentDataPDF2(string JobReportID, string ReportID, string Pages, string fromPage)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();
                XReportWebIface.column JobReportId = new XReportWebIface.column();
                JobReportId.colname = "JobReportId";
                JobReportId.Value = JobReportID;
                XReportWebIface.column ReportId = new XReportWebIface.column();
                ReportId.colname = "ReportId";
                ReportId.Value = ReportID;
                XReportWebIface.column ListOfPages = new XReportWebIface.column();
                ListOfPages.colname = "ListOfPages";
                ListOfPages.Value = JobReportID + "," + Pages;
                XReportWebIface.column FromPage = new XReportWebIface.column();
                FromPage.colname = "FromPage";
                FromPage.Value = fromPage;

                Entry.Columns = new XReportWebIface.column[4] { JobReportId, ReportId, ListOfPages, FromPage };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                //Navigation.Error = SoapEx.Message;
                //this.l_error.Visible = true;
                //this.l_error.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                //Navigation.Error = Ex.Message;
                //this.l_error.Visible = true;
                //this.l_error.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                return null;
                //throw Ex;
            }
        }

        private XReportWebIface.DocumentData BuildDocumentDataIndex(string jrid, string listPag)
        {
            try
            {
                XReportWebIface.IndexEntry Entry = new XReportWebIface.IndexEntry();

                Entry.JobReportId = jrid;
                XReportWebIface.column JobReportId = new XReportWebIface.column();
                JobReportId.colname = "JobReportId";
                JobReportId.Value = jrid;
                XReportWebIface.column FromPage = new XReportWebIface.column();
                FromPage.colname = "FromPage";
                FromPage.Value = "";
                XReportWebIface.column ReportId = new XReportWebIface.column();
                ReportId.colname = "ReportId";
                ReportId.Value = "";
                XReportWebIface.column ListOfPages = new XReportWebIface.column();
                ListOfPages.colname = "ListOfPages";
                ListOfPages.Value = listPag;

                Entry.Columns = new XReportWebIface.column[4] { JobReportId, FromPage, ReportId, ListOfPages };
                XReportWebIface.DocumentData Data = new XReportWebIface.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new XReportWebIface.IndexEntry[1] { Entry };
                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                //Navigation.Error = SoapEx.Message;
                //this.l_error.Visible = true;
                //this.l_error.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                //Navigation.Error = Ex.Message;
                //this.l_error.Visible = true;
                //this.l_error.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                return null;
                //throw Ex;
            }
        }
        #endregion
    }
}