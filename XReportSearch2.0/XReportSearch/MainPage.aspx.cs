﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XReportSearch.Utils;
using XReportSearch.Business;
using System.Configuration;
using log4net;
using System.Web.Services.Protocols;
using System.Net;
using Microsoft.Web.Administration;

namespace XReportSearch
{
    public partial class MainPage : System.Web.UI.Page
    {
        #region Log
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            log.Debug("*** SESSION_ID: " + Session.SessionID + " * IS_NEW: " + Session.IsNewSession.ToString() + " * IS_COOKIELESS: " + Session.IsCookieless.ToString() + " * COOKIE_MODE: " + Session.CookieMode.ToString());
            SetUser();
            if (!Page.IsPostBack)
            {
                string tabs = ConfigurationManager.AppSettings["ExcludedTabs"];
                if (tabs != "")
                {
                    List<MenuItem> items = new List<MenuItem>();
                    string[] values = tabs.Split(',');
                    foreach (string v in values)
                    {
                        if (v == "0")
                            this.b_Tab1.Visible = false;
                        else if (v == "1")
                            this.b_Tab2.Visible = false;
                    }
                }
                if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.UNICREDIT.ToString())
                {
                    this.lb_User.Text = "User: " + Navigation.User.Username + " - Profile: " + Navigation.User.Profilo;
                }
                if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.VITTORIA.ToString() || ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.GENERALI.ToString())
                {
                    this.lb_User.Text = "User: " + Navigation.User.Username;
                }             
            }
        }

        private void SetUser()
        {
             if (ConfigurationManager.AppSettings["DEBUG"].ToString() == "1")
            {
                if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.VITTORIA.ToString() || ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.GENERALI.ToString())
                {
                    Navigation.User = new User("xreport", "", "xreport", "", "");
                    Navigation.User.IsRoot = true;
                }
                else
                    Navigation.User = new User("xreport", "", "X1NADM", "", "X1NADM", "N/A", "N/A");
            }
            else
            {
                if (Navigation.User == null)
                {
                    if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.VITTORIA.ToString() || ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.GENERALI.ToString())
                        Response.Redirect("~/Authentication/Login.aspx");
                    else if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.UNICREDIT.ToString())
                        TestPGE();
                }
            }
        }

        private void TestPGE()
        {
            try
            {
                string matricola = "";
                string token = "";
                if (Request.Params["user"] != null && Request.Params["token"] != null)
                {
                    log.Debug("BACKURL CHIAMATA");
                    matricola = Request.Params["user"];
                    token = Request.Params["token"];
                    log.Debug("USER: " + matricola);
                    log.Debug("TOKEN: " + token);
                    User user = new User(matricola, token);
                    PGE_WS.ApplicationSecurityCheckService PGE_WS = new PGE_WS.ApplicationSecurityCheckService();
                    PGE_WS.Url = ConfigurationManager.AppSettings["PGE_WS"];
                    string appCode = ConfigurationManager.AppSettings["ApplicationCode"];

                    PGE_WS.PGEabdb2 resp = PGE_WS.retrievePGEAuthority(matricola, token, appCode);
                    if (resp.PG_ERRORE != null && resp.PG_ERRORE.codiceErr != "" && resp.PG_ERRORE.codiceErr != "00")
                    {
                        log.Debug("Autenticazione non riuscita " + resp.PG_ERRORE.codiceErr + " - " + resp.PG_ERRORE.descError);
                    }
                    else
                    {
                        if (resp.PG_TORRE != null && resp.PG_TORRE != "")
                            user.Torre = resp.PG_TORRE.TrimEnd();
                        if (resp.PG_COD_AUTHOR != null && resp.PG_COD_AUTHOR != "")
                            user.CodAuthor = resp.PG_COD_AUTHOR.TrimEnd();
                        if (resp.PG_COD_AUTHOR == "X1NBAS")
                            user.Profilo = resp.PG_TORRE.TrimEnd() + resp.PG_FIL_INQ.TrimEnd();
                        else
                            user.Profilo = resp.PG_TORRE.TrimEnd() + resp.PG_COD_AUTHOR.TrimEnd();
                        Navigation.User = user;
                        log.Debug("PGE AUTH SUCCEDED: ");
                        log.Debug("User: " + user.Username);
                        log.Debug("Profilo: " + resp.PG_COD_AUTHOR);
                        log.Debug("Banca: " + resp.PG_BANCA);
                        log.Debug("Istituto: " + resp.PG_COD_IST);
                        log.Debug("Ruolo: " + resp.PG_RUOLO);
                        log.Debug("Torre: " + resp.PG_TORRE);
                        log.Debug("Ufficio: " + resp.PG_UFFICIO_INQ);
                        log.Debug("Filiale: " + resp.PG_FIL_INQ);
                    }
                }
            }
            catch (Exception Ex)
            {
                log.Error("TestPGE() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                throw Ex;
            }
        }

        protected void b_Tab1_Click(object sender, EventArgs e)
        {
            this.panelTable.Visible = true;
            this.panelTable2.Visible = false;
            this.b_Tab2.CssClass = "buttonMenu";
            this.b_Tab1.CssClass = "buttonMenuSelected";
        }

        protected void b_Tab2_Click(object sender, EventArgs e)
        {
            this.panelTable.Visible = false;
            this.panelTable2.Visible = true;          
            this.b_Tab1.CssClass = "buttonMenu";
            this.b_Tab2.CssClass = "buttonMenuSelected";
 
        }

        public void OpenViewFile()
        {
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "", "window.open('ViewFile.aspx?" + SHA.GenTimeSHA256String() + "');", true);
        }
        #endregion
    }
}