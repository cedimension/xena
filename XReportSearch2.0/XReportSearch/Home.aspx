﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="XReportSearch.Home"
    Theme="XReportSearchTheme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
    <link href="~/App_Themes/XReportSearchHomeStyle.css?1" rel="stylesheet" type="text/css" />
    <title>XReport Search</title>
</head>
<body onload="resize()" onresize="resize()" onclick="resize()" onbeforeprint="resize()">
    <script language="javascript" type="text/javascript">

        function resize() {
            var frame = document.getElementById("MainDiv");
            var windowheight = document.documentElement.clientHeight;
            var windowwidth = document.documentElement.clientWidth;
            frame.style.height = windowheight + "px";
            if (document.getElementById("txtHidData") != null) {
                document.getElementById("txtHidData").value = windowheight;
            }
            if (document.getElementById("txtHidData2") != null) {
                document.getElementById("txtHidData2").value = windowwidth;
            }
            frame.style.height = windowheight - 115 + "px";
        }
    </script>
    <form id="form1" runat="server">
    <input type="hidden" clientidmode="Static" id="txtHidData" runat="server" />
    <input type="hidden" clientidmode="Static" id="txtHidData2" runat="server" />
    <div class="body">
        <table class="tables">
            <tr class="rowLogo">
                <td>
                    <asp:Label ID="lb_user" runat="server" Visible="true" SkinID="LabelLogin"></asp:Label>
                </td>
                <td class="cellLogo">
                    <img id="Img1" alt="" runat="server" src="~/Images/logo_cedimension.png" />
                </td>
            </tr>
        </table>
        <div class="buttons">
            <table class="tables" id="MainDiv">
                <tr class="rowButtons">
                    <td class="cell1">
                        <asp:Button ID="b_elastic" CausesValidation="true" runat="server" Text="Elastic Search"
                            Visible="true" CssClass="buttonBig" OnClick="b_elastic_Click" />
                    </td>
                    <td class="cell1">
                        <asp:Button ID="b_online" CausesValidation="true" runat="server" Text="Online Search"
                            Visible="true" CssClass="buttonBig" OnClick="b_online_Click" />
                    </td>
                    <td class="cell2">
                        <asp:Button ID="b_history" CausesValidation="true" runat="server" Text="History Search"
                            Visible="true" CssClass="buttonBig" OnClick="b_history_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <table class="tables">
            <tr class="rowLogo">
                <td class="cellLogo">
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
