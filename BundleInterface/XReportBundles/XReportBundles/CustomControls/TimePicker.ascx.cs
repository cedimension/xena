﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CustomControls
{
    public partial class TimePicker : System.Web.UI.UserControl
    {
        private string _Time;
        public string Time
        {

            get { return txtHour.Text + txtMinute.Text + " " + txtDayPart.Text; }

            set { ParseTime(value); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void ParseTime(string TimeString)
        {

            // Validation of input

            if (TimeString.IndexOf(":") == -1)
            {

                return;
            }

            if ((TimeString.IndexOf("PM") == -1) && (TimeString.IndexOf("AM") == -1))
            {

                return;
            }

            // Good to go with format

            int ColonPos = TimeString.IndexOf(":");
            int AMPos = TimeString.IndexOf("AM");

            int PMPos = TimeString.IndexOf("PM");
            string sHour = TimeString.Substring(0, ColonPos);

            string sMinutes = TimeString.Substring(ColonPos, 3);
            string sDayPart = (TimeString.IndexOf("AM") != -1) ? TimeString.Substring(AMPos, 2) : TimeString.Substring(PMPos, 2);
            txtHour.Text = sHour;

            txtMinute.Text = sMinutes;

            txtDayPart.Text = sDayPart;

        }


    }
}