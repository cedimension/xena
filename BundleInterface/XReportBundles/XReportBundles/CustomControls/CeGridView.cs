﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XReportBundles.Utils;

namespace CustomControls
{
    [
   ToolboxData("<{0}:CeGridView runat=\"server\"> </{0}:CeGridView>")
   ]
    public class CeGridView : GridView
    {

        public CeGridView() 
        {
        }

        public Unit GridHeight { get; set; }

        private String CalculateWidth()
        {
            string strWidth = "auto";
            if (!this.Width.IsEmpty)
            {
                strWidth = String.Format("{0}{1}", this.Width.Value, ((this.Width.Type == UnitType.Percentage) ? "%" : "px"));
            }
            return strWidth;
        }

        private String CalculateHeight()
        {
            
            string strHeight = "200px";
            if (!this.GridHeight.IsEmpty)
            {
                strHeight = String.Format("{0}{1}", this.GridHeight.Value, ((this.GridHeight.Type == UnitType.Percentage) ? "%" : "px"));
            }
            return strHeight;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            //render header row 
            writer.Write("<table style='width:100%' border='0' cellspacing='0' cellpadding='0'>");
            writer.Write("<tr style='width:100%'>");
            writer.Write("<td>");
            writer.Write("<table ID='tbCustomGrid' border='0' cellspacing='" + this.CellSpacing.ToString() + "' cellpadding='" + this.CellPadding.ToString() + "' style='width:" + CalculateWidth() + "'>");
           
            GridViewRow customHeader = this.HeaderRow;      
            if (this.HeaderRow != null)
            {
                customHeader.ApplyStyle(this.HeaderStyle);
                if (AutoGenerateColumns == false)
                {
                    int i = 0;
                    foreach (DataControlField col in this.Columns)
                    {
                        customHeader.Cells[i].ApplyStyle(col.HeaderStyle);
                        customHeader.Cells[i].Visible = col.Visible;
                        i++;
                    }
                }
                customHeader.RenderControl(writer);
                this.HeaderRow.Visible = false;
            }
            writer.Write("</table>");
            writer.Write("</td>");
            writer.Write("<td style='background-color:#518012; width:18px'></td>");
            writer.Write("</tr>");
            writer.Write("</table>");

            //render data rows
            //string id = ClientID;
            string id = "divCustomGrid";
            writer.Write("<div id='" + id + "'  style='" +
                             "padding-bottom:5px;overflow-x:none;overflow-y:scroll;" +
                             "width:" + CalculateWidth() + ";" +
                             //"height:" + CalculateHeight() + ";" +
                             "background-color:#e5f6d1;'>");

            //get the pager row and make invisible
            GridViewRow customPager = this.BottomPagerRow;
            if (this.BottomPagerRow != null)
            {
                this.BottomPagerRow.Visible = false;
            }

            base.Render(writer);
            writer.Write("</div>");

            //render pager row
            if (customPager != null && this.PageCount > 0)
            {
                writer.Write("<table  border='0' cellspacing='" + this.CellSpacing.ToString() + "' cellpadding='" + this.CellPadding.ToString() + "' style='width:" + CalculateWidth() + "'>");
                customPager.ApplyStyle(this.PagerStyle);
                customPager.Visible = true;
                customPager.RenderControl(writer);
                writer.Write("</table>");
            }
        }
    }
}