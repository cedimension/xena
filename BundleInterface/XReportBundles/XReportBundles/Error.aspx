﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="XReportBundles.Error"  Theme="XReportBundlesTheme"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>XReport Search</title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link href="~/App_Themes/XReportBundles.css?2" rel="stylesheet" type="text/css" />
</head>
<body onload="resize()" onresize="resize()" onclick="resize()" onbeforeprint="resize()">
    <script language="javascript" type="text/javascript">

        function fnOnUpdateValidators() {
            var count = 0;
            for (var i = 0; i < Page_Validators.length; i++) {
                var val = Page_Validators[i];
                var ctrl = document.getElementById(val.controltovalidate);
                if (ctrl != null && ctrl.style != null) {
                    if (!val.isvalid) {
                        ctrl.style.borderColor = "Red";
                        ctrl.className = "comboboxError";
                        count++;
                    }
                    else {
                        ctrl.style.borderColor = "#518012";
                        ctrl.className = "combobox";
                    }
                }
            }
        }

        function resize() {
            var frame = document.getElementById("MainDiv");
            var page = document.getElementById("PageDiv2");
            var windowheight = document.documentElement.clientHeight;
            var windowwidth = document.documentElement.clientWidth;
            if (document.getElementById("txtHidData") != null) {
                document.getElementById("txtHidData").value = windowheight;
            }
            if (document.getElementById("txtHidData2") != null) {
                document.getElementById("txtHidData2").value = windowwidth;
            }
            if (frame.style != null && frame.style.height != null) {
                frame.style.height = windowheight + "px";
            }
            if (page.style != null && page.style.height != null) {
                page.style.height = (windowheight - 74) + "px";
            }
        }
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').add_showing(onshowing);
            resize();
        }

        function onshowing() {
            if ($find('ModalProgress') != null) {
                var windowwidth = document.documentElement.clientWidth;
                var windowheight = document.documentElement.clientHeight;
                $find('ModalProgress').set_X((parseInt(windowwidth) / 2) - 100);
                $find('ModalProgress').set_Y((parseInt(windowheight) / 2) - 100);
            }
        }

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

        function beginRequest() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').show();
        }

        function endRequest() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').hide();
        }

        function RefreshParent() {
            window.parent.location.href = window.parent.location.href;
        }
    </script>
    <input type="hidden" clientidmode="Static" id="txtHidData" runat="server" />
    <input type="hidden" clientidmode="Static" id="txtHidData2" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div id="MainDiv" class="main">
                <asp:Panel runat="server" ID="panelTable" class="page">
                    <table class="page" id="PageDiv">
                        <tr style="vertical-align: top; text-align: center">
                            <td>
                                <asp:Label ID="err" runat="server" SkinID="LabelErr"></asp:Label>
                            </td>
                        </tr>
                        <tr style="vertical-align: top; text-align: center">
                            <td>
                                <asp:Button ID="b_refresh" CausesValidation="false" runat="server" Text="Reload"
                                    Visible="true" CssClass="buttonBig" OnClientClick="RefreshParent()" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
