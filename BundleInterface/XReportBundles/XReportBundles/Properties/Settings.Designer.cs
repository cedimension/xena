﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XReportBundles.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "12.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://ufr-uj.collaudo.usinet.it:80/XA-PGE-WS/services/ApplicationSecurityCheck")]
        public string XReportBundles_PGE_WS_ApplicationSecurityCheckService {
            get {
                return ((string)(this["XReportBundles_PGE_WS_ApplicationSecurityCheckService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("HTTP://c0ctlpw003.sd01.unicreditgroup.eu:8089/XenaBundlesManagement/xrIISWebServ." +
            "asp")]
        public string XReportBundles_BundlesManagement_WS_xenabundlesmanagement {
            get {
                return ((string)(this["XReportBundles_BundlesManagement_WS_xenabundlesmanagement"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:8081/TEST_WS/Test_WS.asmx")]
        public string XReportBundles_Test_WS_Test_WS {
            get {
                return ((string)(this["XReportBundles_Test_WS_Test_WS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("HTTP://c0ctlpw003.sd01.unicreditgroup.eu:8089/XReportWebIface/xrIISWebServ.asp")]
        public string XReportBundles_XReportWebIface_xreportwebiface {
            get {
                return ((string)(this["XReportBundles_XReportWebIface_xreportwebiface"]));
            }
        }
    }
}
