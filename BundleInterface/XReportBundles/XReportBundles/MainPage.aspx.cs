﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Configuration;
using XReportBundles.Utils;
using XReportBundles.Business;

namespace XReportBundles
{
    public partial class MainPage : System.Web.UI.Page
    {

        #region Log
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Costants
        public const String AUTH_SUCCESS = "SUCCESS";
        #endregion

        #region Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Navigation.User == null)
            {
                if (ConfigurationManager.AppSettings["DEBUG"].ToString() == "1")
                    Navigation.User = new User("xreport", "SUPER", "C0X1NADM", "C0", "X1NADM");
                else if (ConfigurationManager.AppSettings["DEBUG"].ToString() == "2")
                {
                    Navigation.User = new User(Request.ServerVariables["LOGON_USER"], "SUPER", "", "", "");
                }
            }
            if (!Page.IsPostBack)
            {
                this.frame.Attributes.Add("src", "Bundles.aspx");
                this.frameElab.Attributes.Add("src", "BundlesElab.aspx");
                this.frameQ.Attributes.Add("src", "BundlesQueue.aspx");
            }
            if (ConfigurationManager.AppSettings["ApplicationType"].ToString() == Utility.ApplicationType.UNICREDIT.ToString())
            {
                this.lb_User.Text = "User: " + Navigation.User.Username;
            }
            else
            {
                this.lb_User.Text = "User: " + Navigation.User.Username;
            }
        }

        #endregion

        #region Events

        #endregion
    }
}
