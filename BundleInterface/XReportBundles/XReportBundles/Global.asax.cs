﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using XReportBundles.Utils;
using log4net;

namespace XReportBundles
{
    public class Global : System.Web.HttpApplication
    {
        #region Log
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Main Events
        protected void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Debug("********************************************* APPLICATION STARTED *********************************************");
            Server.ClearError();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session.Timeout = 240;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (ex != null)
            {
                if (XReportBundles.Utils.Navigation.Error != null)
                    if (ex != null)
                        XReportBundles.Utils.Navigation.Error = ex.Message + " - " + ex.InnerException + " - " + Request.Url.ToString();
                Server.ClearError();
                if (Navigation.User != null)
                    Response.Redirect("~/Error.aspx");
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {
            log.Debug("********************************************* SESSION END *********************************************");
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
        #endregion

    }
}
