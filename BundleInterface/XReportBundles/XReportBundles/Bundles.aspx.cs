﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using XReportBundles.Utils;
using System.Configuration;
using System.Web.Services.Protocols;
using XReportBundles.Business;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace XReportBundles
{
    public partial class Bundles : System.Web.UI.Page
    {
        #region Constants
        private string SORT_DESCENDING = "DESC";
        private string SORT_ASCENDING = "ASC";
        #endregion

        #region Log
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Main Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Navigation.User == null)
            {
                Navigation.Error = "Session ended. Please reload the application.";
                Response.Redirect("~/Error.aspx");
            }
            Navigation.SearchType = Utility.Searchtype.BUNDLES;
            if (!Page.IsPostBack)
            {
                this.tb_data_From.Attributes.Add("onchange", "TextChanged('tb_data_From')");
                this.tb_data_To.Attributes.Add("onchange", "TextChanged('tb_data_To')");
                this.tb_dataBundle_From.Attributes.Add("onchange", "TextChanged('tb_dataBundle_From')");
                this.tb_dataBundle_To.Attributes.Add("onchange", "TextChanged('tb_dataBundle_To')");
                this.tb_dataBundle_From.Text = Utility.GetToday();
                this.tb_dataBundle_To.Text = Utility.GetTomorrow();
                this.tb_time_From.Text = "00:00";
                this.tb_time_To.Text = "00:00";
                if (Navigation.SearchType == Utility.Searchtype.BUNDLES_ELAB)
                {
                    this.lb_jn.Visible = false;
                    this.tb_jn.Visible = false;
                    this.lb_rn.Visible = false;
                    this.tb_rn.Visible = false;
                    this.l_rec.Visible = false;
                    this.tb_rec.Visible = false;
                    this.Label3.Visible = false;
                    this.tb_data_From.Visible = false;
                    this.Label9.Visible = false;
                    this.tb_data_To.Visible = false;
                    this.b_data1.Visible = false;
                    this.b_data2.Visible = false;
                }
            }
            if (Page.Validators != null && Page.Validators.Count > 0)
            {
                ScriptManager.RegisterOnSubmitStatement(this, Page.GetType(), "", "fnOnUpdateValidators()");
            }
        }
        #endregion

        #region Private Methods

        private void BindGridViewOut()
        {
            int numPages = 0;
            if (Navigation.TableSource != null)
            {
                double pages = (double)Navigation.TableSource.Rows.Count / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                numPages = (int)Math.Ceiling(pages);
            }
            this.gvBundlesN.PageSize = int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
            if (Navigation.CurrentGridPage != -1)
                this.gvBundlesN.PageIndex = Navigation.CurrentGridPage;
            this.gvBundlesN.DataSource = Navigation.TableSource;
            this.gvBundlesN.Visible = true;
            this.gvBundlesN.PagerSettings.FirstPageText = "1";
            this.gvBundlesN.PagerSettings.LastPageText = "" + numPages + "";
            this.gvBundlesN.DataBind();
        }

        private BundlesManagement_WS.DocumentData BuildDocumentData()
        {
            try
            {
                BundlesManagement_WS.IndexEntry Entry = new BundlesManagement_WS.IndexEntry();
                BundlesManagement_WS.column BundleName = new BundlesManagement_WS.column();
                BundleName.colname = "BundleName";
                BundleName.Value = this.tb_bname.Text;
                BundlesManagement_WS.column BundleCategory = new BundlesManagement_WS.column();
                BundleCategory.colname = "BundleCategory";
                BundleCategory.Value = this.tb_Bcat.Text;
                BundlesManagement_WS.column JobName = new BundlesManagement_WS.column();
                JobName.colname = "JobName";
                JobName.Value = this.tb_jn.Text;
                BundlesManagement_WS.column ReportName = new BundlesManagement_WS.column();
                ReportName.colname = "ReportName";
                ReportName.Value = this.tb_rn.Text;
                BundlesManagement_WS.column From = new BundlesManagement_WS.column();
                From.colname = "From";
                if (this.tb_data_From.Text == Utility.DATE_MASK || this.tb_data_From.Text == "")
                    From.Value = "";
                else
                    From.Value = Utility.ParseDate(this.tb_data_From.Text, "{0:yyyyMMdd}");
                BundlesManagement_WS.column To = new BundlesManagement_WS.column();
                To.colname = "To";
                if (this.tb_data_To.Text == Utility.DATE_MASK || this.tb_data_To.Text == "")
                    To.Value = "";
                else
                    To.Value = Utility.ParseDate(this.tb_data_To.Text, "{0:yyyyMMdd}");

                BundlesManagement_WS.column BundleElabFrom = new BundlesManagement_WS.column();
                BundleElabFrom.colname = "BundleElabFrom";
                BundleElabFrom.Value = Utility.ParseDate(this.tb_dataBundle_From.Text, "{0:yyyy-MM-dd}");
                BundlesManagement_WS.column BundleElabTo = new BundlesManagement_WS.column();
                BundleElabTo.colname = "BundleElabTo";
                BundleElabTo.Value = Utility.ParseDate(this.tb_dataBundle_To.Text, "{0:yyyy-MM-dd}");

                BundleElabFrom.Value += " " + this.tb_time_From.Text;
                BundleElabTo.Value += " " + this.tb_time_To.Text;

                BundlesManagement_WS.column Limit = new BundlesManagement_WS.column();
                Limit.colname = "Limit";
                if (this.tb_limit.Text != "0")
                    Limit.Value = " TOP " + this.tb_limit.Text;
                //Limit.Value = this.tb_limit.Text;
                else
                    Limit.Value = "";
                BundlesManagement_WS.column Recipient = new BundlesManagement_WS.column();
                Recipient.colname = "Recipient";
                Recipient.Value = this.tb_rec.Text;

                log.Debug("BundleElabFrom.Value: " + BundleElabFrom.Value);
                log.Debug("BundleElabTo.Value: " + BundleElabTo.Value);

                Entry.Columns = new BundlesManagement_WS.column[10] { BundleName, BundleCategory, JobName, ReportName, From, To, Limit, Recipient, BundleElabFrom, BundleElabTo };
                BundlesManagement_WS.DocumentData Data = new BundlesManagement_WS.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new BundlesManagement_WS.IndexEntry[1] { Entry };
                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                return null;
                //throw Ex;
            }
        }

        private BundlesManagement_WS.DocumentData BuildDocumentDataForRec(string bundleID, string recipient)
        {
            try
            {
                BundlesManagement_WS.IndexEntry Entry = new BundlesManagement_WS.IndexEntry();
                BundlesManagement_WS.column FromDate = new BundlesManagement_WS.column();
                FromDate.colname = "From";
                if (this.tb_data_From.Text == Utility.DATE_MASK)
                    FromDate.Value = "";
                else
                    FromDate.Value = Utility.ParseDate(this.tb_data_From.Text, "{0:yyyyMMdd}");
                BundlesManagement_WS.column ToDate = new BundlesManagement_WS.column();
                ToDate.colname = "To";
                if (this.tb_data_To.Text == Utility.DATE_MASK)
                    ToDate.Value = "";
                else
                    ToDate.Value = Utility.ParseDate(this.tb_data_To.Text, "{0:yyyyMMdd}");
                BundlesManagement_WS.column BundleElabFrom = new BundlesManagement_WS.column();
                BundleElabFrom.colname = "BundleElabFrom";
                BundleElabFrom.Value = Utility.ParseDate(this.tb_dataBundle_From.Text, "{0:yyyy-MM-dd}");
                BundlesManagement_WS.column BundleElabTo = new BundlesManagement_WS.column();
                BundleElabTo.colname = "BundleElabTo";
                BundleElabTo.Value = Utility.ParseDate(this.tb_dataBundle_To.Text, "{0:yyyy-MM-dd}");
                BundleElabFrom.Value += " " + this.tb_time_From.Text;
                BundleElabTo.Value += " " + this.tb_time_To.Text;

                BundlesManagement_WS.column JobName = new BundlesManagement_WS.column();
                JobName.colname = "JobName";
                JobName.Value = this.tb_jn.Text;
                BundlesManagement_WS.column RecipientName = new BundlesManagement_WS.column();
                RecipientName.colname = "Recipient";
                RecipientName.Value = recipient;
                BundlesManagement_WS.column Text = new BundlesManagement_WS.column();
                Text.colname = "Text";
                Text.Value = this.tb_rn.Text;
                BundlesManagement_WS.column BundleId = new BundlesManagement_WS.column();
                BundleId.colname = "BundleId";
                BundleId.Value = bundleID;
                BundlesManagement_WS.column BundleName = new BundlesManagement_WS.column();
                BundleName.colname = "BundleName";
                BundleName.Value = this.tb_bname.Text;
                BundlesManagement_WS.column BundleCategory = new BundlesManagement_WS.column();
                BundleCategory.colname = "BundleCategory";
                BundleCategory.Value = this.tb_Bcat.Text;

                BundlesManagement_WS.column Limit = new BundlesManagement_WS.column();
                Limit.colname = "Limit";
                if (this.tb_limit.Text != "0")
                    Limit.Value = " TOP " + this.tb_limit.Text;
                //Limit.Value = this.tb_limit.Text;
                else
                    Limit.Value = "";

                Entry.Columns = new BundlesManagement_WS.column[11] { FromDate, ToDate, JobName, RecipientName, Text, BundleId, Limit, BundleName, BundleCategory, BundleElabFrom, BundleElabTo };
                BundlesManagement_WS.DocumentData Data = new BundlesManagement_WS.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new BundlesManagement_WS.IndexEntry[1] { Entry };
                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                return null;
                //throw Ex;
            }
        }

        private BundlesManagement_WS.DocumentData BuildDocumentDataForRequeue(string jobReportId, string reportId, string text, string reportTimeRef, string copies, string bundleId, string recipient, string bundlename)
        {
            try
            {
                BundlesManagement_WS.IndexEntry Entry = new BundlesManagement_WS.IndexEntry();
                BundlesManagement_WS.column BundleId = new BundlesManagement_WS.column();
                BundleId.colname = "BundleId";
                BundleId.Value = bundleId;
                BundlesManagement_WS.column Recipient = new BundlesManagement_WS.column();
                Recipient.colname = "Recipient";
                Recipient.Value = recipient;
                BundlesManagement_WS.column JobName = new BundlesManagement_WS.column();
                JobName.colname = "JobName";
                JobName.Value = this.tb_jn.Text;
                BundlesManagement_WS.column BundleName = new BundlesManagement_WS.column();
                BundleName.colname = "BundleName";
                BundleName.Value = this.tb_bname.Text;
                BundlesManagement_WS.column BundleName2 = new BundlesManagement_WS.column();
                BundleName2.colname = "BundleName2";
                BundleName2.Value = bundlename;
                BundlesManagement_WS.column BundleCategory = new BundlesManagement_WS.column();
                BundleCategory.colname = "BundleCategory";
                BundleCategory.Value = this.tb_Bcat.Text;
                BundlesManagement_WS.column ReportName = new BundlesManagement_WS.column();
                ReportName.colname = "ReportName";
                ReportName.Value = this.tb_rn.Text;
                BundlesManagement_WS.column FromDate = new BundlesManagement_WS.column();
                FromDate.colname = "From";
                if (this.tb_data_From.Text == Utility.DATE_MASK)
                    FromDate.Value = "";
                else
                    FromDate.Value = Utility.ParseDate(this.tb_data_From.Text, "{0:yyyyMMdd}");
                BundlesManagement_WS.column ToDate = new BundlesManagement_WS.column();
                ToDate.colname = "To";
                if (this.tb_data_To.Text == Utility.DATE_MASK)
                    ToDate.Value = "";
                else
                    ToDate.Value = Utility.ParseDate(this.tb_data_To.Text, "{0:yyyyMMdd}");
                BundlesManagement_WS.column BundleElabFrom = new BundlesManagement_WS.column();
                BundleElabFrom.colname = "BundleElabFrom";
                BundleElabFrom.Value = Utility.ParseDate(this.tb_dataBundle_From.Text, "{0:yyyy-MM-dd}");
                BundlesManagement_WS.column BundleElabTo = new BundlesManagement_WS.column();
                BundleElabTo.colname = "BundleElabTo";
                BundleElabTo.Value = Utility.ParseDate(this.tb_dataBundle_To.Text, "{0:yyyy-MM-dd}");
                BundleElabFrom.Value += " " + this.tb_time_From.Text;
                BundleElabTo.Value += " " + this.tb_time_To.Text;

                BundlesManagement_WS.column JobReportId = new BundlesManagement_WS.column();
                JobReportId.colname = "JobReportId";
                JobReportId.Value = jobReportId;
                BundlesManagement_WS.column ReportId = new BundlesManagement_WS.column();
                ReportId.colname = "ReportId";
                ReportId.Value = reportId;
                BundlesManagement_WS.column textId = new BundlesManagement_WS.column();
                textId.colname = "textId";
                textId.Value = text;
                BundlesManagement_WS.column ReportTimeRef = new BundlesManagement_WS.column();
                ReportTimeRef.colname = "ReportTimeRef";
                ReportTimeRef.Value = reportTimeRef;
                BundlesManagement_WS.column Copies = new BundlesManagement_WS.column();
                Copies.colname = "Copies";
                if (copies != "")
                    Copies.Value = copies;
                else
                    Copies.Value = "-1";

                Entry.Columns = new BundlesManagement_WS.column[16] { JobName, BundleName, BundleName2, BundleCategory, JobReportId, ReportId, textId, ReportTimeRef, Copies, ReportName, FromDate, ToDate, BundleId, Recipient, BundleElabFrom, BundleElabTo };
                BundlesManagement_WS.DocumentData Data = new BundlesManagement_WS.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new BundlesManagement_WS.IndexEntry[1] { Entry };
                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentDataForRequeue(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentDataForRequeue(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                return null;
                //throw Ex;
            }
        }

        private BundlesManagement_WS.DocumentData BuildDocumentDataVarSets(string jobName, string reportName, string recipient, string copies)
        {
            try
            {
                BundlesManagement_WS.IndexEntry Entry = new BundlesManagement_WS.IndexEntry();
                BundlesManagement_WS.column JobName = new BundlesManagement_WS.column();
                JobName.colname = "JobName";
                JobName.Value = jobName;
                BundlesManagement_WS.column ReportName = new BundlesManagement_WS.column();
                ReportName.colname = "ReportName";
                ReportName.Value = reportName;
                BundlesManagement_WS.column RecipientName = new BundlesManagement_WS.column();
                RecipientName.colname = "Recipient";
                RecipientName.Value = recipient;
                BundlesManagement_WS.column Copies = new BundlesManagement_WS.column();
                Copies.colname = "Copies";
                Copies.Value = copies;

                Entry.Columns = new BundlesManagement_WS.column[4] { JobName, ReportName, RecipientName, Copies };
                BundlesManagement_WS.DocumentData Data = new BundlesManagement_WS.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new BundlesManagement_WS.IndexEntry[1] { Entry };
                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentDataVarSets(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentDataVarSets(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                return null;
                //throw Ex;
            }
        }

        public void LoadBundles()
        {
            try
            {
                log.Debug("LoadBundles() Started");
                BundlesManagement_WS.xenabundlesmanagement BundlesManagementWS = new BundlesManagement_WS.xenabundlesmanagement();
                BundlesManagementWS.Url = ConfigurationManager.AppSettings["XenaBundlesManagementUrl"];
                BundlesManagement_WS.DocumentData Data = BuildDocumentData();
                BundlesManagementWS.Timeout = 600000;
                if (Navigation.SearchType == Utility.Searchtype.BUNDLES)
                    Data = BundlesManagementWS.GetAllBundles(Data);
                else if (Navigation.SearchType == Utility.Searchtype.BUNDLES_ELAB)
                    Data = BundlesManagementWS.GetAllBundlesElab(Data);
                Navigation.TableSource = Utility.IndexEntriesToTable(Data.IndexEntries);
                BindGridViewOut();
                log.Debug("LoadBundles() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadBundles() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadBundles() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw Ex;
            }
        }

        public void LoadBundlesNames(string bundleName)
        {
            try
            {
                log.Debug("LoadBundlesNames() Started");
                BundlesManagement_WS.xenabundlesmanagement BundlesManagementWS = new BundlesManagement_WS.xenabundlesmanagement();
                BundlesManagementWS.Url = ConfigurationManager.AppSettings["XenaBundlesManagementUrl"];
                BundlesManagement_WS.DocumentData Data = new BundlesManagement_WS.DocumentData();
                BundlesManagementWS.Timeout = 600000;
                Data = BundlesManagementWS.GetBundlesNames(Data);
                DataTable dt = Utility.IndexEntriesToTable(Data.IndexEntries);
                this.ddl_bnames.DataSource = dt;
                this.ddl_bnames.DataValueField = "BundleName";
                this.ddl_bnames.DataTextField = "BundleName";
                this.ddl_bnames.DataBind();
                this.ddl_bnames.SelectedValue = bundleName;
                log.Debug("LoadBundlesNames() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadBundlesNames() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                //throw SoapEx;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
            }
            catch (Exception Ex)
            {
                log.Error("LoadBundlesNames() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw Ex;
            }
        }

        public void LoadBundlesRecipient(string bundleID)
        {
            try
            {
                log.Debug("LoadBundles() Started");
                BundlesManagement_WS.xenabundlesmanagement BundlesManagementWS = new BundlesManagement_WS.xenabundlesmanagement();
                BundlesManagementWS.Url = ConfigurationManager.AppSettings["XenaBundlesManagementUrl"];
                BundlesManagement_WS.DocumentData Data = BuildDocumentDataForRec(bundleID, this.tb_rec.Text);
                BundlesManagementWS.Timeout = 600000;
                Data = BundlesManagementWS.GetBundlesRecipient(Data);

                Navigation.TableRecipient = Utility.IndexEntriesToTable(Data.IndexEntries);
                log.Debug("LoadBundles() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadBundles() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadBundles() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw Ex;
            }
        }

        public void LoadBundlesLog(string bundleID, string recipient)
        {
            try
            {
                log.Debug("LoadBundles() Started");
                BundlesManagement_WS.xenabundlesmanagement BundlesManagementWS = new BundlesManagement_WS.xenabundlesmanagement();
                BundlesManagementWS.Url = ConfigurationManager.AppSettings["XenaBundlesManagementUrl"];
                BundlesManagement_WS.DocumentData Data = BuildDocumentDataForRec(bundleID, recipient);
                BundlesManagementWS.Timeout = 600000;
                Data = BundlesManagementWS.GetBundlesLog(Data);

                Navigation.TableBundles = Utility.IndexEntriesToTable(Data.IndexEntries);
                log.Debug("LoadBundles() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadBundles() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadBundles() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw Ex;
            }
        }

        public void RequeueBundle(string bundleId, string bundleName, string jobReportId, string reportId, string text, string reportTimeRef, string copies)
        {
            try
            {
                log.Debug("RequeueBundle() Started");
                BundlesManagement_WS.xenabundlesmanagement BundlesManagementWS = new BundlesManagement_WS.xenabundlesmanagement();
                BundlesManagementWS.Url = ConfigurationManager.AppSettings["XenaBundlesManagementUrl"];
                BundlesManagement_WS.DocumentData Data = BuildDocumentDataForRequeue(jobReportId, reportId, text, reportTimeRef, copies, bundleId, "", bundleName);
                BundlesManagementWS.Timeout = 600000;
                Data = BundlesManagementWS.RequeueBundle(Data);
                if (Data.IndexEntries[0].Columns[0].colname == "ActionMessage")
                {
                    this.lb_warning.Text = Data.IndexEntries[0].Columns[0].Value;
                    ModalPopupExtenderOK.Show();
                }
                log.Debug("RequeueBundle() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("RequeueBundle() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("RequeueBundle() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw Ex;
            }
        }

        public void RequeueBundleGroup(string bundleId, string recipient, string copies, string bundleName, bool all)
        {
            try
            {
                log.Debug("RequeueBundleGroup() Started");
                BundlesManagement_WS.xenabundlesmanagement BundlesManagementWS = new BundlesManagement_WS.xenabundlesmanagement();
                BundlesManagementWS.Url = ConfigurationManager.AppSettings["XenaBundlesManagementUrl"];
                BundlesManagement_WS.DocumentData Data = BuildDocumentDataForRequeue("", "", "", "", copies, bundleId, recipient, bundleName);
                BundlesManagementWS.Timeout = 600000;
                if (all)
                    Data = BundlesManagementWS.RequeueBundles(Data);
                else
                    Data = BundlesManagementWS.RequeueBundlesRecipient(Data);
                if (Data.IndexEntries[0].Columns[0].colname == "ActionMessage")
                {
                    this.lb_warning.Text = Data.IndexEntries[0].Columns[0].Value;
                    ModalPopupExtenderOK.Show();
                }
                log.Debug("RequeueBundleGroup() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("RequeueBundleGroup() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("RequeueBundleGroup() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw Ex;
            }
        }

        public void UpdateVarSets(string jobName, string reportName, string recipient, string copies, bool update)
        {
            try
            {
                log.Debug("UpdateVarSets() Started");
                BundlesManagement_WS.xenabundlesmanagement BundlesManagementWS = new BundlesManagement_WS.xenabundlesmanagement();
                BundlesManagementWS.Url = ConfigurationManager.AppSettings["XenaBundlesManagementUrl"];
                BundlesManagement_WS.DocumentData Data = BuildDocumentDataVarSets(jobName, reportName, recipient, copies);
                BundlesManagementWS.Timeout = 600000;
                if (update)
                    Data = BundlesManagementWS.UpdatePrintCopies(Data);
                else
                    Data = BundlesManagementWS.DeletePrintCopies(Data);
                if (Data.IndexEntries[0].Columns[0].colname == "ActionMessage")
                {
                    this.lb_warning.Text = Data.IndexEntries[0].Columns[0].Value;
                    ModalPopupExtenderOK.Show();
                }
                log.Debug("UpdateVarSets() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("UpdateVarSets() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("UpdateVarSets() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw Ex;
            }
        }
        #endregion

        #region Events
        protected void buttonSearch_Click(object sender, EventArgs e)
        {
            Navigation.CurrentGridPage = 0;
            //if (Navigation.User == null)
            //Navigation.User = new User("xreport", "SUPER", "C0X1NADM", "C0", "X1NADM");
            if (this.tb_limit.Text == "")
                this.tb_limit.Text = "500";
            LoadBundles();
        }

        protected void gvBundlesN_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Status")) == 33)
                {

                    AjaxControlToolkit.CollapsiblePanelExtender coll = (AjaxControlToolkit.CollapsiblePanelExtender)e.Row.Cells[0].FindControl("ctlCollapsiblePanel");
                    Image image = (Image)e.Row.Cells[0].FindControl("imgCollapsible");
                    Image image_red = (Image)e.Row.Cells[0].FindControl("img_red");
                    Panel innerPanel = (Panel)e.Row.Cells[0].FindControl("pnlR");
                    image.Visible = false;
                    coll.Enabled = false;
                    e.Row.Cells[0].Controls.Remove(coll);
                    e.Row.Cells[0].Controls.Remove(innerPanel);
                    e.Row.Cells[0].BorderColor = System.Drawing.Color.Red;
                    e.Row.Cells[0].BorderStyle = BorderStyle.Solid;
                    e.Row.Cells[0].BorderWidth = new Unit(1, UnitType.Pixel);
                    image_red.Visible = true;
                    Image img_orange = (Image)e.Row.Cells[0].FindControl("img_orange");
                    e.Row.Cells[0].Controls.Remove(img_orange);
                    Image img_grey = (Image)e.Row.Cells[0].FindControl("img_grey");
                    e.Row.Cells[0].Controls.Remove(img_grey);
                    Image img_green = (Image)e.Row.Cells[0].FindControl("img_green");
                    e.Row.Cells[0].Controls.Remove(img_green);
                }
                else if (Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Status")) == 17)
                {
                    AjaxControlToolkit.CollapsiblePanelExtender coll = (AjaxControlToolkit.CollapsiblePanelExtender)e.Row.Cells[0].FindControl("ctlCollapsiblePanel");
                    Image image = (Image)e.Row.Cells[0].FindControl("imgCollapsible");
                    Panel innerPanel = (Panel)e.Row.Cells[0].FindControl("pnlR");
                    Image image_red = (Image)e.Row.Cells[0].FindControl("img_red");
                    Image img_orange = (Image)e.Row.Cells[0].FindControl("img_orange");
                    Image img_grey = (Image)e.Row.Cells[0].FindControl("img_grey");
                    Image img_green = (Image)e.Row.Cells[0].FindControl("img_green");
                    image.Visible = false;
                    coll.Enabled = false;
                    e.Row.Cells[0].Controls.Remove(coll);
                    e.Row.Cells[0].Controls.Remove(innerPanel);
                    e.Row.Cells[0].BorderColor = System.Drawing.Color.Orange;
                    e.Row.Cells[0].BorderStyle = BorderStyle.Solid;
                    e.Row.Cells[0].BorderWidth = new Unit(1, UnitType.Pixel);
                    img_orange.Visible = true;
                    e.Row.Cells[0].Controls.Remove(image_red);
                    e.Row.Cells[0].Controls.Remove(img_grey);
                    e.Row.Cells[0].Controls.Remove(img_green);
                }
                else if (Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Status")) == 4)
                {
                    AjaxControlToolkit.CollapsiblePanelExtender coll = (AjaxControlToolkit.CollapsiblePanelExtender)e.Row.Cells[0].FindControl("ctlCollapsiblePanel");
                    Image image = (Image)e.Row.Cells[0].FindControl("imgCollapsible");
                    Panel innerPanel = (Panel)e.Row.Cells[0].FindControl("pnlR");
                    Image image_red = (Image)e.Row.Cells[0].FindControl("img_red");
                    Image img_orange = (Image)e.Row.Cells[0].FindControl("img_orange");
                    Image img_grey = (Image)e.Row.Cells[0].FindControl("img_grey");
                    Image img_green = (Image)e.Row.Cells[0].FindControl("img_green");
                    image.Visible = false;
                    coll.Enabled = false;
                    e.Row.Cells[0].Controls.Remove(coll);
                    e.Row.Cells[0].Controls.Remove(innerPanel);
                    e.Row.Cells[0].Controls.Remove(image_red);
                    e.Row.Cells[0].Controls.Remove(img_orange);
                    e.Row.Cells[0].Controls.Remove(img_green);
                    img_grey.Visible = true;
                }
                else
                {
                    if (Navigation.SearchType == Utility.Searchtype.BUNDLES_ELAB)
                    {
                        AjaxControlToolkit.CollapsiblePanelExtender coll = (AjaxControlToolkit.CollapsiblePanelExtender)e.Row.Cells[0].FindControl("ctlCollapsiblePanel");
                        Image image = (Image)e.Row.Cells[0].FindControl("imgCollapsible");
                        Panel innerPanel = (Panel)e.Row.Cells[0].FindControl("pnlR");
                        image.Visible = false;
                        coll.Enabled = false;
                        e.Row.Cells[0].Controls.Remove(coll);
                        e.Row.Cells[0].Controls.Remove(innerPanel);
                        Image img_green = (Image)e.Row.Cells[0].FindControl("img_green");
                        img_green.Visible = true;
                    }
                    Image image_red = (Image)e.Row.Cells[0].FindControl("img_red");
                    Image img_orange = (Image)e.Row.Cells[0].FindControl("img_orange");
                    Image img_grey = (Image)e.Row.Cells[0].FindControl("img_grey");
                    e.Row.Cells[0].Controls.Remove(image_red);
                    e.Row.Cells[0].Controls.Remove(img_orange);
                    e.Row.Cells[0].Controls.Remove(img_grey);
                }
            }
        }

        protected void gvBundlesN_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Navigation.CurrentGridPage = e.NewPageIndex;
            BindGridViewOut();
        }

        protected void gvBundlesN_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string name = this.gvBundlesN.DataKeys[e.Row.RowIndex].Values[1].ToString();
                string id = this.gvBundlesN.DataKeys[e.Row.RowIndex].Values[0].ToString();
                ImageButton button = e.Row.FindControl("b_reqAll") as ImageButton;
                ImageButton buttonDet = e.Row.FindControl("b_detAll") as ImageButton;
                button.ToolTip = "Requeue Bundle ID " + id;
                button.CommandArgument = id + ";" + e.Row.RowIndex.ToString() + ";" + name;
                buttonDet.CommandArgument = e.Row.RowIndex.ToString();
                ImageButton coll = e.Row.FindControl("imgCollapsible") as ImageButton;
                coll.CommandArgument = e.Row.RowIndex.ToString();
            }
        }

        protected void gvBundlesR_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView grid = (GridView)sender;
                string id = grid.DataKeys[e.Row.RowIndex].Values[0].ToString();
                string rec = grid.DataKeys[e.Row.RowIndex].Values[1].ToString();
                string name = grid.DataKeys[e.Row.RowIndex].Values[2].ToString();
                ImageButton coll = e.Row.FindControl("imgCollapsible2") as ImageButton;
                coll.CommandArgument = e.Row.RowIndex.ToString();
                ImageButton button = e.Row.FindControl("b_reqRec") as ImageButton;
                button.CommandArgument = id + ";" + rec + ";" + e.Row.RowIndex.ToString() + ";" + name;
            }
        }

        protected void gvBundles_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView gvBundles = (GridView)sender;
                DataRowView view = (DataRowView)e.Row.DataItem;
                string jrid = view["JobReportId"].ToString();
                string id = view["ReportId"].ToString();
                string name = view["BundleName"].ToString();
                string textId = view["textId"].ToString();
                string time = view["ReportTimeRef"].ToString();
                string BundleId = view["BundleId"].ToString();
                ImageButton b_det = (ImageButton)e.Row.FindControl("b_det");
                ImageButton b_req = (ImageButton)e.Row.FindControl("b_req");
                //b_det.CommandArgument = jrid + ";" + id + ";" + name;
                b_det.CommandArgument = e.Row.RowIndex.ToString();
                b_req.CommandArgument = jrid + ";" + id + ";" + name + ";" + textId + ";" + time + ";" + BundleId;
            }
        }

        protected void gvBundles_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Details")
            {
                GridView gvBundles = (GridView)sender;
                int index = int.Parse(e.CommandArgument.ToString());
                DataTable dt = Utility.KeysToTable(gvBundles.DataKeyNames);
                Utility.FillTableFromKeys(ref dt, gvBundles.DataKeys[index]);
                this.dv_BundlesTop.Visible = false;
                this.dv_Bundles.Visible = true;
                this.dv_Bundles.DataSource = dt;
                this.dv_Bundles.DataBind();
                this.programmaticModalPopup.Show();
            }
            else if (e.CommandName == "Requeue")
            {
                this.HiddenCopiesOp.Value = "Copy";
                this.HiddenCopies.Value = e.CommandArgument.ToString();
                this.LoadBundlesNames(this.HiddenCopies.Value.ToString().Split(';')[2]);
                this.ModalPopupExtenderCopies.Show();
            }
            else if (e.CommandName == "VarSets")
            {
                this.lb_varSetsMes.Text = e.CommandArgument.ToString();
                string reportName = e.CommandArgument.ToString().Split(';')[0];
                string recipient = e.CommandArgument.ToString().Split(';')[1];
                this.lb_varSets.Text = "Update, delete or insert print copies number for report " + reportName + " and recipient " + recipient + ".";
                this.ModalPopupExtenderVarSets.Show();
            }
        }

        protected void b_copies_Click(object sender, EventArgs e)
        {
            string[] args;
            switch (this.HiddenCopiesOp.Value)
            {
                case "Copy":
                    args = this.HiddenCopies.Value.ToString().Split(';');
                    string jrid = args[0].ToString();
                    string id = args[1].ToString();
                    string name = args[2].ToString();
                    string text = args[3].ToString();
                    string time = args[4].ToString();
                    string BundleId = args[5].ToString();
                    RequeueBundle(BundleId, this.ddl_bnames.SelectedValue, jrid, id, text, time, this.tb_copies.Text);
                    break;
                case "CopyAll":
                    args = this.HiddenCopies.Value.ToString().Split(';');
                    RequeueBundleGroup(args[0].ToString(), this.tb_rec.Text, this.tb_copies.Text, this.ddl_bnames.SelectedValue, true);
                    break;
                case "CopyRecipient":
                    args = this.HiddenCopies.Value.ToString().Split(';');
                    RequeueBundleGroup(args[0].ToString(), args[1].ToString(), this.tb_copies.Text, this.ddl_bnames.SelectedValue, false);
                    break;
                default:
                    break;
            }
            this.tb_copies.Text = "";
            this.ModalPopupExtenderCopies.Hide();
        }

        protected void b_close_Click(object sender, EventArgs e)
        {
            this.programmaticModalPopup.Hide();
        }

        protected void gvBundlesN_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "RequeueAll")
            {
                this.HiddenCopiesOp.Value = "CopyAll";
                this.HiddenCopies.Value = e.CommandArgument.ToString();
                string[] args = e.CommandArgument.ToString().Split(';');
                AjaxControlToolkit.CollapsiblePanelExtender collapse = (AjaxControlToolkit.CollapsiblePanelExtender)this.gvBundlesN.Rows[int.Parse(args[1].ToString())].FindControl("ctlCollapsiblePanel");
                if (collapse != null && collapse.ClientState != null)
                {
                    if (collapse.ClientState.ToLower() == "false")
                        collapse.ClientState = "true";
                    else
                        collapse.ClientState = "false";
                }
                this.LoadBundlesNames(args[2]);
                this.ModalPopupExtenderCopies.Show();
            }
            else if (e.CommandName == "DetailsAll")
            {
                GridView gvBundles = (GridView)sender;
                int index = int.Parse(e.CommandArgument.ToString());
                DataTable dt = Utility.KeysToTable(gvBundles.DataKeyNames);
                Utility.FillTableFromKeys(ref dt, gvBundles.DataKeys[index]);
                this.dv_BundlesTop.Visible = true;
                this.dv_Bundles.Visible = false;
                this.dv_BundlesTop.DataSource = dt;
                this.dv_BundlesTop.DataBind();
                AjaxControlToolkit.CollapsiblePanelExtender collapse = (AjaxControlToolkit.CollapsiblePanelExtender)this.gvBundlesN.Rows[index].FindControl("ctlCollapsiblePanel");
                if (collapse != null && collapse.ClientState != null)
                {
                    if (collapse.ClientState.ToLower() == "false")
                        collapse.ClientState = "true";
                    else
                        collapse.ClientState = "false";
                }
                this.programmaticModalPopup.Show();
            }
        }

        protected void gvBundlesR_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "RequeueRec")
            {
                this.HiddenCopiesOp.Value = "CopyRecipient";
                this.HiddenCopies.Value = e.CommandArgument.ToString();
                GridView grid = (GridView)sender;
                string[] args = e.CommandArgument.ToString().Split(';');
                AjaxControlToolkit.CollapsiblePanelExtender collapse = (AjaxControlToolkit.CollapsiblePanelExtender)grid.Rows[int.Parse(args[2].ToString())].FindControl("CollapsiblePanelExtender1");
                if (collapse.ClientState.ToLower() == "false")
                    collapse.ClientState = "true";
                else
                    collapse.ClientState = "false";
                this.LoadBundlesNames(args[3]);
                this.ModalPopupExtenderCopies.Show();
            }
        }

        protected void cv_dates_ServerValidate(object source, ServerValidateEventArgs args)
        {
            bool date2Valid = Convert.ToDateTime(this.tb_dataBundle_From.Text) <= Convert.ToDateTime(this.tb_dataBundle_To.Text);
            bool date1Valid = false;
            if (this.tb_data_From.Text == "" || this.tb_data_To.Text == "")
                date1Valid = true;
            else if (this.tb_data_From.Text == Utility.DATE_MASK && this.tb_data_To.Text == Utility.DATE_MASK)
                date1Valid = true;
            else if (this.tb_data_From.Text != Utility.DATE_MASK && this.tb_data_To.Text != Utility.DATE_MASK)
                date1Valid = Convert.ToDateTime(this.tb_data_From.Text) <= Convert.ToDateTime(this.tb_data_To.Text);
            args.IsValid = date1Valid && date2Valid;
        }

        protected void imgCollapsible_Click(object sender, EventArgs e)
        {
            string id = ((ImageButton)sender).CommandName.ToString();
            if (((ImageButton)sender).ImageUrl == "~/Images/expandDG.png")
            {
                Navigation.CurrentExpandIndex = int.Parse(((ImageButton)sender).CommandArgument);
                this.LoadBundlesRecipient(id);
                GridView grid = this.gvBundlesN.Rows[Navigation.CurrentExpandIndex].FindControl("gvBundlesR") as GridView;
                grid.DataSource = Navigation.TableRecipient;
                grid.DataBind();
                ((ImageButton)sender).ImageUrl = "~/Images/collapseDG.png";
            }
            else
            {
                ((ImageButton)sender).ImageUrl = "~/Images/expandDG.png";
            }
        }

        protected void imgCollapsible2_Click(object sender, EventArgs e)
        {
            string id = ((ImageButton)sender).CommandName.ToString().Split(';')[0];
            string rec = ((ImageButton)sender).CommandName.ToString().Split(';')[1];
            if (((ImageButton)sender).ImageUrl == "~/Images/expandDG.png")
            {
                int rowIndex = int.Parse(((ImageButton)sender).CommandArgument);
                this.LoadBundlesLog(id, rec);
                GridView gridMaster = this.gvBundlesN.Rows[Navigation.CurrentExpandIndex].FindControl("gvBundlesR") as GridView;
                GridView grid = gridMaster.Rows[rowIndex].FindControl("gvBundles") as GridView;
                grid.DataSource = Navigation.TableBundles;
                grid.DataBind();
                ((ImageButton)sender).ImageUrl = "~/Images/collapseDG.png";
            }
            else
            {
                ((ImageButton)sender).ImageUrl = "~/Images/expandDG.png";
            }
        }

        protected void b_varSetsUpdate_Click(object sender, EventArgs e)
        {
            this.UpdateVarSets(this.lb_varSetsMes.Text.Split(';')[2], this.lb_varSetsMes.Text.Split(';')[0], this.lb_varSetsMes.Text.Split(';')[1], this.tb_VarSets.Text, true);
            this.tb_VarSets.Text = "0";
            LoadBundles();
        }

        protected void b_varSetsDelete_Click(object sender, EventArgs e)
        {
            this.UpdateVarSets(this.lb_varSetsMes.Text.Split(';')[2], this.lb_varSetsMes.Text.Split(';')[0], this.lb_varSetsMes.Text.Split(';')[1], this.tb_VarSets.Text, false);
            this.tb_VarSets.Text = "0";
            LoadBundles();
        }

        protected void popupError_Click(object sender, EventArgs e)
        {
            this.ModalPopupExtender_Error.Hide();
        }
        #endregion
    }
}