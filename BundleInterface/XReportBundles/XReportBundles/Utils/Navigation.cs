﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using XReportBundles.Business;

namespace XReportBundles.Utils
{
    public static class Navigation
    {
        #region Session Properties
        public static User User
        {
            get { return (User)GetSessionValue("User", null); }
            set { SetSessionValue("User", value); }
        }

        public static string Error
        {
            get { return (string)GetSessionValue("Error", ""); }
            set { SetSessionValue("Error", value); }
        }

        public static string CurrentSortDirection
        {
            get { return (String)GetSessionValue("CurrentSortDirection", null); }
            set { SetSessionValue("CurrentSortDirection", value); }
        }

        public static string CurrentSortExpression
        {
            get { return (String)GetSessionValue("CurrentSortExpression", null); }
            set { SetSessionValue("CurrentSortExpression", value); }
        }

        public static Utility.Searchtype SearchType
        {
            get { return (Utility.Searchtype)GetSessionValue("SearchType", Utility.Searchtype.BUNDLES); }
            set { SetSessionValue("SearchType", value); }
        }

        public static string ViewParameters
        {
            get { return (string)GetSessionValue("ViewParameters", ""); }
            set { SetSessionValue("ViewParameters", value); }
        }

        // Bundles.aspx
        public static int CurrentGridPage
        {
            get { return (int)GetSessionValue("CurrentGridPage", -1); }
            set { SetSessionValue("CurrentGridPage", value); }
        }

        public static int CurrentExpandIndex
        {
            get { return (int)GetSessionValue("CurrentExpandIndex", 0); }
            set { SetSessionValue("CurrentExpandIndex", value); }
        }
        
        public static DataTable TableSource
        {
            get { return (DataTable)GetSessionValue("TableSource", null); }
            set { SetSessionValue("TableSource", value); }
        }

        public static DataTable TableRecipient
        {
            get { return (DataTable)GetSessionValue("TableRecipient", null); }
            set { SetSessionValue("TableRecipient", value); }
        }

        public static DataTable TableBundles
        {
            get { return (DataTable)GetSessionValue("TableBundles", null); }
            set { SetSessionValue("TableBundles", value); }
        }

        // BundlesElab.aspx
        public static int CurrentGridPage_E
        {
            get { return (int)GetSessionValue("CurrentGridPage_E", -1); }
            set { SetSessionValue("CurrentGridPage_E", value); }
        }

        public static int CurrentExpandIndex_E
        {
            get { return (int)GetSessionValue("CurrentExpandIndex_E", 0); }
            set { SetSessionValue("CurrentExpandIndex_E", value); }
        }

        public static DataTable TableSource_E
        {
            get { return (DataTable)GetSessionValue("TableSource_E", null); }
            set { SetSessionValue("TableSource_E", value); }
        }

        public static DataTable TableRecipient_E
        {
            get { return (DataTable)GetSessionValue("TableRecipient_E", null); }
            set { SetSessionValue("TableRecipient_E", value); }
        }

        public static DataTable TableBundles_E
        {
            get { return (DataTable)GetSessionValue("TableBundles_E", null); }
            set { SetSessionValue("TableBundles_E", value); }
        }

        // BundlesElab.aspx
        public static int CurrentGridPage_Q
        {
            get { return (int)GetSessionValue("CurrentGridPage_Q", -1); }
            set { SetSessionValue("CurrentGridPage_Q", value); }
        }

        public static int CurrentExpandIndex_Q
        {
            get { return (int)GetSessionValue("CurrentExpandIndex_Q", 0); }
            set { SetSessionValue("CurrentExpandIndex_Q", value); }
        }

        public static DataTable TableSource_Q
        {
            get { return (DataTable)GetSessionValue("TableSource_Q", null); }
            set { SetSessionValue("TableSource_Q", value); }
        }

        public static DataTable TableRecipient_Q
        {
            get { return (DataTable)GetSessionValue("TableRecipient_Q", null); }
            set { SetSessionValue("TableRecipient_Q", value); }
        }

        public static DataTable TableBundles_Q
        {
            get { return (DataTable)GetSessionValue("TableBundles_Q", null); }
            set { SetSessionValue("TableBundles_Q", value); }
        }
        #endregion

        #region Methods
        private static void SetSessionValue(string SessionName, object value)
        {
            if (HttpContext.Current.Session != null) 
                HttpContext.Current.Session[SessionName] = value;
        }

        private static object GetSessionValue(string SessionName, object defaultvalue)
        {
            object res = defaultvalue;
            if (HttpContext.Current.Session != null) 
                res = HttpContext.Current.Session[SessionName] != null ? HttpContext.Current.Session[SessionName] : defaultvalue;
            return res;
        }

        private static void SetCurrentSortDirection(Utility.Searchtype type, string value)
        {
            Dictionary<string, string> CurrentSortDirection = null;
            if (HttpContext.Current.Session != null)
                CurrentSortDirection = HttpContext.Current.Session["CurrentSortDirection"] != null ? (Dictionary<string, string>)HttpContext.Current.Session["CurrentSortDirection"] : new Dictionary<string, string>();

            switch (type)
            {
                case Utility.Searchtype.BUNDLES:
                    if (CurrentSortDirection.ContainsKey("BUNDLES"))
                        CurrentSortDirection["BUNDLES"] = value;
                    else
                        CurrentSortDirection.Add("BUNDLES", value);
                    break;
                case Utility.Searchtype.BUNDLES_ELAB:
                    break;
                case Utility.Searchtype.BUNDLES_QUEUE:
                    break;
                default:
                    break;
            }
        }

        #endregion
    }
}