﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services.Protocols;
using log4net;
using System.Configuration;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Reflection;
using System.Globalization;
using System.Text;
using XReportBundles.BundlesManagement_WS;
using System.Collections.Specialized;

namespace XReportBundles.Utils
{
    public class Utility
    {
        #region Log
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Const
        public static string JOB_NAME = "job_name";
        public static string REPORT_NAME = "report_name";
        public static string WS_NO_DATA = "no data found";
        public static string DATE_MASK = "__/__/____";
        public static string TIME_MASK = "__:__";
        #endregion

        #region Enum
        public enum Searchtype 
        { 
            BUNDLES = 1,
            BUNDLES_ELAB = 2,
            BUNDLES_QUEUE = 3,
        };

        public enum ApplicationType
        {
            BPM,
            UNICREDIT,
            VITTORIA
        };

        #endregion

        #region Public Methods
        public static DataTable IndexEntriesToTable(IndexEntry[] Entries)
        {
            DataTable dt = new DataTable();
            if (Entries != null)
            {
                if (Entries[0].Columns[0].colname == "ActionMessage" && Entries[0].Columns[0].Value.ToLower().StartsWith(WS_NO_DATA))
                    return null;
                if (Entries.Length > 1)
                {
                    DataColumn dc;
                    DataRow dr;
                    dt.Columns.Add(new DataColumn("JobReportID"));
                    dt.Columns.Add(new DataColumn("FromPage"));
                    dt.Columns.Add(new DataColumn("ForPages"));
                    if (Entries[1].Columns != null)
                    {
                        foreach (column col in Entries[1].Columns)
                        {
                            if (!dt.Columns.Contains(col.colname))
                            {
                                dc = new DataColumn(col.colname);
                                dt.Columns.Add(dc);
                            }
                        }
                    }
                    foreach (IndexEntry entry in Entries)
                    {
                        if (entry.Columns != null && entry.Columns[0].colname != "ActionMessage")
                        {
                            dr = dt.NewRow();
                            foreach (column col in entry.Columns)
                            {
                                dr[col.colname] = col.Value;
                            }
                            dr["JobReportID"] = entry.JobReportId;
                            dr["FromPage"] = entry.FromPage;
                            dr["ForPages"] = entry.ForPages;
                            dt.Rows.Add(dr);
                        }
                    }
                }
            }
            return dt;
        }

        public static DataTable KeysToTable(string[] keys)
        {
             DataTable dt = new DataTable();
             if (keys.Length > 1)
             {
                 DataColumn dc;
                 foreach (string col in keys)
                 {
                     if (!dt.Columns.Contains(col))
                     {
                         dc = new DataColumn(col);
                         dt.Columns.Add(dc);
                     }
                 }
             }
             return dt;
        }

        public static void FillTableFromKeys(ref DataTable dt, DataKey keys)
        {
            DataRow dr = dt.NewRow();
            foreach (DataColumn col in dt.Columns)
            {
                string val = keys[col.ColumnName].ToString();
                dr[col.ColumnName] = val;
            }
            dt.Rows.Add(dr);
        }

        public static List<HtmlTableRow> BuildControl(string typeControl, string name)
        {
            List<HtmlTableRow> rows = new List<HtmlTableRow>();
            HtmlTableRow row = new HtmlTableRow();
            row.Attributes.Add("class", "data");
            HtmlTableCell cell = new HtmlTableCell();
            HtmlTableCell cellControl = new HtmlTableCell();
            HtmlTableRow rowTo;
            HtmlTableCell cellTo1;
            HtmlTableCell cellTo2;

            switch (typeControl.ToLower())
            {
                case "string":
                    cell.Controls.Add(Utility.CreateLabel(name, name));
                    row.Cells.Add(cell);
                    TextBox tb = Utility.CreateTextBox(name, false);
                    cellControl.Controls.Add(tb);
                    tb.Attributes.Add("autocomplete", "off");
                    cellControl.Controls.Add(CreateAutoComplete(REPORT_NAME, name));
                    break;
                case "date":
                    cell.Controls.Add(Utility.CreateLabel(name + "_From", name + " From"));
                    row.Cells.Add(cell);
                    rowTo = new HtmlTableRow();
                    rowTo.Attributes.Add("class", "data");
                    cellTo1 = new HtmlTableCell();
                    cellTo1.Controls.Add(Utility.CreateLabel(name + "_To", name + " To"));
                    rowTo.Cells.Add(cellTo1);
                    cellTo2 = new HtmlTableCell();
                    CreateDate(ref cellControl, name + "_From");
                    CreateDate(ref cellTo2, name + "_To");
                    rowTo.Cells.Add(cellTo2);
                    rows.Add(rowTo);
                    break;
                case "integer":
                case "long":
                    cell.Controls.Add(Utility.CreateLabel(name + "_From", name + " From"));
                    row.Cells.Add(cell);
                    cellControl.Controls.Add(Utility.CreateTextBox(name + "_From", false));
                    cellControl.Controls.Add(Utility.CreateNumericMask(name + "_From"));
                    rowTo = new HtmlTableRow();
                    rowTo.Attributes.Add("class", "data");
                    cellTo1 = new HtmlTableCell();
                    cellTo1.Controls.Add(Utility.CreateLabel(name + "_To", name + " To"));
                    rowTo.Cells.Add(cellTo1);
                    cellTo2 = new HtmlTableCell();
                    cellTo2.Controls.Add(Utility.CreateTextBox(name + "_To", false));
                    cellTo2.Controls.Add(Utility.CreateNumericMask(name + "_To"));
                    rowTo.Cells.Add(cellTo2);
                    rows.Add(rowTo);
                    break;
                default:
                    break;
            }
            row.Cells.Add(cellControl);
            rows.Insert(0, row);
            return rows;
        }

        public static TemplateField BuildButtonColumn()
        {
            TemplateField tf = new TemplateField();
            tf.HeaderStyle.Width = new Unit(5, UnitType.Percentage);
            tf.ItemStyle.Width = new Unit(5, UnitType.Percentage);
            tf.ItemTemplate = new ButtonTemplate();
            tf.HeaderText = "pdf";
            return tf;
        }

        public static string ParseDate(string date, string format)
        {
            DateTime dateTime = DateTime.Parse(date);
            return String.Format(format, dateTime);
        }

        public static string ParseDateTime(string date, string format)
        {
            DateTime dateTime = DateTime.ParseExact(date, format, null);
            return String.Format(format, dateTime);
        }

        public static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        static public string DecodeFrom64(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
            return returnValue;
        }

        public static int GetPageRange(int page)
        {
            double range = ((double)page * (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"])) / 1000;
            if ((int)Math.Ceiling(range) == 0)
                return 1;
            else
                return (int)Math.Ceiling(range);
        }

        public static int GetRealPage(int page)
        {
            if (page <= 40)
                return page;
            else
                return (page % 40);
        }
        
        public static bool ConvertStringToBool(string value)
        {
            if (value == "1")
            {
                return true;
            }
            else if (value == "0")
            {
                return false;
            }
            else
            {
                throw new FormatException("The string is not a recognized as a valid boolean value.");
            }
        }

        public static string GetToday()
        {
            return DateTime.Today.Day.ToString() + "/" + DateTime.Today.Month.ToString() + "/" + DateTime.Today.Year.ToString() + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute;
        }

        public static string GetTomorrow()
        {
            return DateTime.Today.AddDays(1).Day.ToString() + "/" + DateTime.Today.Month.ToString() + "/" + DateTime.Today.Year.ToString() + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute;
        }
        #endregion

        #region Dynamic Controls Methods
        private static void CreateDate(ref HtmlTableCell cellControl, string name)
        {
            TextBox tb = Utility.CreateTextBox(name, true);
            cellControl.Controls.Add(tb);
            Label lb = new Label();
            lb.Width = new Unit(6, UnitType.Pixel);
            cellControl.Controls.Add(lb);
            ImageButton cal = Utility.CreateCalendarButton(name);
            cellControl.Controls.Add(cal);
            cellControl.Controls.Add(Utility.CreateCalendar(name, cal.ID));
            AjaxControlToolkit.MaskedEditExtender mask = Utility.CreateDataMask(name);
            cellControl.Controls.Add(mask);
            cellControl.Controls.Add(Utility.CreateDataValidator(name, mask.ID));
        }

        private static Label CreateLabel(string name, string text)
        {
            Label lb = new Label();
            lb.ID = name + "_l";
            lb.Text = FirstCharToUpper(text.Replace("_", " "));
            lb.SkinID = "LabelFilter";
            lb.EnableViewState = false;
            return lb;
        }

        private static TextBox CreateTextBox(string name, bool isDate)
        {
            TextBox tb = new TextBox();
            tb.ID = name;
            if (isDate)
                tb.SkinID = "TextboxDate";
            else
                tb.SkinID = "TextboxSmall";
            tb.EnableViewState = false;
            if (tb.ID.Contains("_From"))
                tb.Attributes.Add("onchange", "TextChanged('" + tb.ClientID + "')");
            tb.TextChanged += new EventHandler(TextBox_TextChanged);
            return tb;
        }

        private static ImageButton CreateCalendarButton(string name)
        {
            ImageButton cal = new ImageButton();
            cal.ID = name + "calButton";
            cal.ImageUrl = "~/Images/calendario.png";
            cal.ImageAlign = ImageAlign.Top;
            cal.EnableViewState = false;
            cal.CausesValidation = false;
            return cal;
        }

        private static AjaxControlToolkit.CalendarExtender CreateCalendar(string name, string calendarName)
        {
            AjaxControlToolkit.CalendarExtender calendar = new AjaxControlToolkit.CalendarExtender();
            calendar.ID = name + "_calendar";
            calendar.TargetControlID = name;
            calendar.EnableViewState = false;
            calendar.Format = "dd/MM/yyyy";
            calendar.CssClass = "calendar";
            calendar.PopupButtonID = calendarName;
            calendar.PopupPosition = AjaxControlToolkit.CalendarPosition.BottomRight;
            return calendar;
        }

        private static AjaxControlToolkit.FilteredTextBoxExtender CreateNumericMask(string name)
        {
            AjaxControlToolkit.FilteredTextBoxExtender ext = new AjaxControlToolkit.FilteredTextBoxExtender();
            ext.ID = name + "_numeric";
            ext.TargetControlID = name;
            ext.EnableViewState = false;
            ext.FilterType = AjaxControlToolkit.FilterTypes.Numbers;
            return ext;
        }

        private static AjaxControlToolkit.FilteredTextBoxExtender CreateNumericFloatMask(string name)
        {
            AjaxControlToolkit.FilteredTextBoxExtender ext = new AjaxControlToolkit.FilteredTextBoxExtender();
            ext.ID = name + "_numericFloat";
            ext.TargetControlID = name;
            ext.EnableViewState = false;
            ext.FilterType = AjaxControlToolkit.FilterTypes.Custom;
            ext.ValidChars = "0123456789,";
            return ext;
        }

        private static AjaxControlToolkit.MaskedEditExtender CreateDataMask(string name)
        {
            AjaxControlToolkit.MaskedEditExtender ext = new AjaxControlToolkit.MaskedEditExtender();
            ext.ID = name + "_data";
            ext.TargetControlID = name;
            ext.CultureName = "it-IT";
            ext.EnableViewState = false;
            ext.MaskType = AjaxControlToolkit.MaskedEditType.Date;
            ext.Mask = "99/99/9999";
            return ext;
        }

        private static AjaxControlToolkit.MaskedEditValidator CreateDataValidator(string name, string extender)
        {
            AjaxControlToolkit.MaskedEditValidator val = new AjaxControlToolkit.MaskedEditValidator();
            val.ID = name + "_datavalidator";
            val.ControlExtender = extender;
            val.ControlToValidate = name;
            return val;
        }

        private static AjaxControlToolkit.AutoCompleteExtender CreateAutoComplete(string key, string name)
        {
            AjaxControlToolkit.AutoCompleteExtender ace = new AjaxControlToolkit.AutoCompleteExtender();
            ace.ID = name + "_ace";
            ace.TargetControlID = name;
            ace.ServiceMethod = "GetCompletionList";
            ace.MinimumPrefixLength = 1;
            ace.CompletionSetCount = 10;
            ace.EnableCaching = false;
            ace.CompletionInterval = 100;
            ace.FirstRowSelected = false;

            ace.CompletionListCssClass = "completionList";
            ace.CompletionListHighlightedItemCssClass = "itemHighlighted";
            ace.CompletionListItemCssClass = "listItem";
            ace.ContextKey = name;
            return ace;
        }

        private static AjaxControlToolkit.AccordionPane CreateAccordionPane(string name)
        {
            AjaxControlToolkit.AccordionPane acc = new AjaxControlToolkit.AccordionPane();
            acc.ID = name + "_Pane";
            Label header = new Label();
            header.Text = FirstCharToUpper(name);
            acc.HeaderContainer.Controls.Add(header);
            return acc;
        }

        private static HtmlTable CreateHtmlTable(string name)
        {
            HtmlTable table = new HtmlTable();
            table.ID = name;
            return table;
        }

        private static HtmlTableRow CreateHtmlTableRow(string css)
        {
            HtmlTableRow row = new HtmlTableRow();
            row.Attributes.Add("class", css);
            return row;
        }

        private static HtmlTableCell CreateHtmlTableCell()
        {
            HtmlTableCell cell = new HtmlTableCell();
            return cell;
        }

        public static BoundField BuildBoundField(string name)
        {
            BoundField field = new BoundField();
            field.FooterText = name;
            field.HeaderText = FirstCharToUpper(name.Replace("_", " "));
            //field.HeaderStyle.Width = Unit.Pixel(100);
            //field.ItemStyle.Width = Unit.Pixel(100);
            field.DataField = name;
            return field;
        }

        private static string FirstCharToUpper(string input)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input.ToLower());
        }
        #endregion

        #region Events
        private static void TextBox_TextChanged(object sender, System.EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            //Type searchType = Navigation.Search.GetType();
            //PropertyInfo[] searchProperties = searchType.GetProperties();
            //foreach (PropertyInfo prop in searchProperties)
            //{
            //    if (tb.ID == prop.Name)
            //    {
                    //prop.SetValue(Navigation.Search, tb.Text, null);
                    //break;
            //    }
            //}
        }
        #endregion
    }

    public class ButtonTemplate : ITemplate
    {
        public void InstantiateIn(System.Web.UI.Control container)
        {
            ImageButton b = new ImageButton();
            b.ID = "b_doc";
            b.Attributes.Add("runat", "server");
            b.CommandName = "ViewFile";
            b.ImageUrl = "~/Images/List.png";
            b.ToolTip = "View File";
            b.Width = new Unit(20, UnitType.Pixel);
            b.Height = new Unit(20, UnitType.Pixel);
            b.Visible = true;
            container.Controls.Add(b);
        }
    }
}