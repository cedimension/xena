﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Services.Protocols;
using System.Net;
using System.Xml;
using System.Text;
using log4net;
using XReportBundles.Utils;
using XReportBundles.BundlesManagement_WS;
using XReportBundles.Business;

namespace XReportBundles.Authentication
{
    public partial class Login : System.Web.UI.Page
    {
        #region Log
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Costants
        public const String AUTH_METHOD_NTLM = "NTLM";
        public const String AUTH_METHOD_RACF = "RACF";
        public const String AUTH_SUCCESS = "SUCCESS";
        #endregion

        #region Variables
        private string authenticatedUser;
        private string authenticatedPass;
        private string authenticationMethod = "";
        private string applicationName = "";
        #endregion

        #region Main Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            this.authenticationMethod = ConfigurationManager.AppSettings["AuthenticationMethod"];
            this.applicationName = ConfigurationManager.AppSettings["ApplicationName"];
            if (this.authenticationMethod == AUTH_METHOD_NTLM)
            {
                HideLogin();
                CheckNTLMAuthentication();
            }
            Page.ClientScript.RegisterOnSubmitStatement(Page.GetType(), "", "fnOnUpdateValidators()");
        }
        #endregion

        #region Private Methods
        private void HideLogin()
        {
            this.UserNameLabel.Visible = false;
            this.PasswordLabel.Visible = false;
            this.UserName.Visible = false;
            this.Password.Visible = false;
            this.LoginButton.Visible = false;
        }

        private void CheckNTLMAuthentication()
        {
            if (Navigation.User != null && (Navigation.User.Username) != "")
            {
                Server.Transfer("~/MainPage.aspx");
            }
            else
            {
                BundlesManagement_WS.DocumentData Data;
                switch (this.authenticationMethod)
                {
                    case AUTH_METHOD_NTLM:
                        //this.authenticatedUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                        //this.authenticatedUser = System.IO.Path.GetFileNameWithoutExtension(this.authenticatedUser);
                        this.authenticatedUser = Request.ServerVariables["LOGON_USER"];
                        Data = BuildDocumentData();
                        string res = Authenticated(ref Data);
                        if (res == "SUCCESS")
                        {
                            Navigation.User = new User(this.authenticatedUser, "");
                            log.Debug("User authenticated - NTLM");
                            Response.Redirect("~/MainPage.aspx");
                        }
                        else
                        {
                            this.lb_error.Visible = true;
                            this.lb_error.Text = res;
                            log.Debug("User authentication failed - NTLM");
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void CheckRACFAuthentication()
        {
            if (Navigation.User != null && (Navigation.User.Username) != "")
            {
                Server.Transfer("~/MainPage.aspx");
            }
            else
            {
                BundlesManagement_WS.DocumentData Data;
                switch (this.authenticationMethod)
                {
                    case AUTH_METHOD_RACF:
                        this.authenticatedUser = this.UserName.Text;
                        this.authenticatedPass = this.Password.Text;
                        Data = BuildDocumentData();
                        string res = Authenticated(ref Data);
                        if (res == "SUCCESS")
                        {
                            Navigation.User = new User(this.authenticatedUser, "");
                            log.Debug("User authenticated - RACF");
                            Response.Redirect("~/MainPage.aspx");
                        }
                        else
                        {
                            this.lb_error.Visible = true;
                            this.lb_error.Text = res;
                            log.Debug("User authentication failed - RACF");
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private BundlesManagement_WS.DocumentData BuildDocumentData()
        {
            try
            {
                BundlesManagement_WS.IndexEntry Entry = new BundlesManagement_WS.IndexEntry();
                BundlesManagement_WS.column UserName = new BundlesManagement_WS.column();
                UserName.colname = "UserName";
                UserName.Value = this.authenticatedUser.Split('\\')[1];
                BundlesManagement_WS.column AuthMethod = new BundlesManagement_WS.column();
                AuthMethod.colname = "AuthMethod";
                AuthMethod.Value = this.authenticationMethod;
                BundlesManagement_WS.column ApplName = new BundlesManagement_WS.column();
                ApplName.colname = "ApplName";
                ApplName.Value = this.applicationName;
                BundlesManagement_WS.column Password = new BundlesManagement_WS.column();
                Password.colname = "Password";
                Password.Value = this.authenticatedPass;

                Entry.Columns = new BundlesManagement_WS.column[4] { UserName, AuthMethod, ApplName, Password };
                BundlesManagement_WS.DocumentData Data = new BundlesManagement_WS.DocumentData();
                Data.IndexName = "Authentication";
                Data.IndexEntries = new BundlesManagement_WS.IndexEntry[1] { Entry };

                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                throw SoapEx;
            }
            catch (WebException WebEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + WebEx.Message);
                throw WebEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                throw Ex;
            }
        }

        private string Authenticated(ref BundlesManagement_WS.DocumentData Data)
        {
            try
            {
                //xreportwebiface XReportWebIFace = new xreportwebiface();
                //XReportWebIFace.Url = ConfigurationManager.AppSettings["XReportWebIfaceUrl"];
                BundlesManagement_WS.xenabundlesmanagement BundlesManagementWS = new BundlesManagement_WS.xenabundlesmanagement();
                BundlesManagementWS.Url = ConfigurationManager.AppSettings["XenaBundlesManagementUrl"];
                Data = BundlesManagementWS.getUserConfig(Data);
                if (Data.IndexEntries != null && Data.IndexEntries.Length > 0 && Data.IndexEntries[0].Columns != null)
                {
                    BundlesManagement_WS.column actionMessage = Data.IndexEntries[0].Columns.ToList().Find(delegate(BundlesManagement_WS.column c) { return c.colname == "ActionMessage"; });
                    if (actionMessage != null && actionMessage.Value.ToString().ToUpper() == AUTH_SUCCESS)
                        return "SUCCESS";
                    else
                        return Data.IndexEntries[0].Columns[1].Value.ToString();
                }
                return "Error during login.";
            }
            catch (SoapException SoapEx)
            {
                log.Error("Authenticated() Failed: " + SoapEx.Message);
                throw SoapEx;
            }
            catch (WebException WebEx)
            {
                log.Error("Authenticated() Failed: " + WebEx.Message);
                throw WebEx;
            }
            catch (Exception Ex)
            {
                log.Error("Authenticated() Failed: " + Ex.Message);
                throw Ex;
            }
        }

        #endregion

        #region Events
        protected void LoginButton_Click(object sender, ImageClickEventArgs e)
        {
            if (this.authenticationMethod == AUTH_METHOD_RACF)
                CheckRACFAuthentication();
        }
        #endregion
    }
}