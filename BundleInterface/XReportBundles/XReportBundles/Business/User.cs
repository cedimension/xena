﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XReportBundles.Business
{
    public class User
    {
        public string Username { get; set; }
        public string TokenString { get; set; }
        public string Torre { get; set; }
        public string Profilo { get; set; }
        public string CodAuthor { get; set; }

        public User()
        {
        }

        public User(string Username, string TokenString)
        {
            this.Username = Username;
            this.TokenString = TokenString;
        }

        public User(string Username, string TokenString, string Profilo, string Torre, string Cod)
        {
            this.Username = Username;
            this.TokenString = TokenString;
            this.Profilo = Profilo;
            this.Torre = Torre;
            this.CodAuthor = Cod;
        }

        public override string ToString()
        {
            return "Username: " + this.Username + "\nToken: " + this.TokenString + "Torre: " + this.Torre;
        }
    }
}