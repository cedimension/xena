﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Bundles.aspx.cs" Inherits="XReportBundles.Bundles" Theme="XReportBundlesTheme" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Namespace="CustomControls" TagPrefix="cedim" Assembly="XReportBundles" %>
<%@ Register TagPrefix="CE" TagName="TimePicker" Src="~/CustomControls/TimePicker.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>XReport Bundles</title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" charset="UTF-8" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link href="~/App_Themes/XReportBundles.css?2" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/XReportBundlesControls.css?2" rel="stylesheet" type="text/css" />
</head>
<body onload="resize()" onresize="resize()" onclick="resize()" onbeforeprint="resize()">
    <script language="javascript" type="text/javascript">
        function Limit_ClientValidate(source, args) {
            var ctrl = document.getElementById("<%= tb_limit.ClientID %>");
            if (ctrl.value == "") {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }

        function fnOnUpdateValidators() {
            var count = 0;
            for (var i = 0; i < Page_Validators.length; i++) {
                var val = Page_Validators[i];
                var ctrl = document.getElementById(val.controltovalidate);
                if (ctrl != null && ctrl.style != null) {
                    if (!val.isvalid) {
                        ctrl.style.borderColor = "Red";
                        ctrl.className = "comboboxError";
                        count++;
                    }
                    else {
                        ctrl.style.borderColor = "#518012";
                        ctrl.className = "combobox";
                    }
                }
            }
        }

        function resize() {
            var page = document.getElementById("PageDiv");
            var pageRight = document.getElementById("PageRight");
            var grid = document.getElementById("divCustomGrid");
            var tbGrid = document.getElementById("tbOutGrid");
            var windowheight = document.documentElement.clientHeight;
            var windowwidth = document.documentElement.clientWidth;
            if (document.getElementById("txtHidData") != null) {
                document.getElementById("txtHidData").value = windowheight;
            }
            if (document.getElementById("txtHidData2") != null) {
                document.getElementById("txtHidData2").value = windowwidth;
            }
            if (page.style != null && page.style.height != null) {
                page.style.height = (windowheight) + "px";
            }
            if (windowheight > 60 && grid != null && grid.style != null && grid.style.height != null && pageRight != null && pageRight.style != null) {
                grid.style.height = (windowheight - 60) + "px";
                pageRight.style.height = (windowheight - 60) + "px";
                if (windowwidth < 800) {
                    pageRight.style.fontSize = "0.5em";
                }
                else if (windowwidth > 2000) {
                    pageRight.style.fontSize = "2em";
                }
                else {
                    pageRight.style.fontSize = "0.8em";
                }
            }
        }

        function TextChanged(controlName) {
            var tb_To;
            if (controlName.indexOf('_From') !== -1)
                tb_To = document.getElementById(controlName.replace("_From", "_To"));
            var tb_From = document.getElementById(controlName);
            if (tb_From.value.length > 0 && tb_To != null)
                tb_To.value = tb_From.value;
        }
    </script>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"
            AsyncPostBackTimeout="600" />
        <script language="javascript" type="text/javascript">
            function pageLoad() {
                if ($find('ModalProgress') != null)
                    $find('ModalProgress').add_showing(onshowing);
                resize();
            }

            function onshowing() {
                if ($find('ModalProgress') != null) {
                    var windowwidth = document.documentElement.clientWidth;
                    var windowheight = document.documentElement.clientHeight;
                    $find('ModalProgress').set_X((parseInt(windowwidth) / 2) - 100);
                    $find('ModalProgress').set_Y((parseInt(windowheight) / 2) - 100);
                }
            }

            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

            function beginRequest() {
                if ($find('ModalProgress') != null)
                    $find('ModalProgress').show();
            }

            function endRequest() {
                if ($find('ModalProgress') != null)
                    $find('ModalProgress').hide();
            }

            var xPos, yPos;
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(BeginRequestHandler);
            prm.add_endRequest(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                if ($get('divCustomGrid') != null) {
                    xPos = $get('divCustomGrid').scrollLeft;
                    yPos = $get('divCustomGrid').scrollTop;
                }
            }
            function EndRequestHandler(sender, args) {
                if ($get('divCustomGrid') != null) {
                    $get('divCustomGrid').scrollLeft = xPos;
                    $get('divCustomGrid').scrollTop = yPos;
                }
            }

        </script>
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <table class="page" id="PageDiv">
                    <tr id="PageDiv2">
                        <td class="pageLeft">
                            <ajax:CollapsiblePanelExtender ID="CollapsiblePanelExtenderBar" runat="server" TargetControlID="pShow"
                                CollapseControlID="pHide" ExpandControlID="pHide" Collapsed="false" ImageControlID="imgArrows"
                                ExpandDirection="Horizontal" ExpandedImage="~/Images/hide.png" CollapsedImage="~/Images/show.png">
                            </ajax:CollapsiblePanelExtender>
                            <div>
                                <asp:Panel ID="pShow" runat="server">
                                    <table>
                                        <tr class="data">
                                            <td></td>
                                            <td class="buttons">
                                                <asp:Button ID="b_assign" CausesValidation="true" runat="server" Text="Search" Visible="true"
                                                    CssClass="buttonBig" OnClick="buttonSearch_Click" />
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td>
                                                <asp:Label ID="lb_limit" runat="server" Text="Max Rows" SkinID="LabelFilter"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb_limit" runat="server" SkinID="TextboxSmallLeft" Text="500" TabIndex="1"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td>
                                                <asp:Label ID="lb_bname" runat="server" Text="Bundle Name" SkinID="LabelFilter"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb_bname" runat="server" SkinID="TextboxSmallLeft" TabIndex="2"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td>
                                                <asp:Label ID="lb_Bcat" runat="server" Text="Bundle Category" SkinID="LabelFilter"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb_Bcat" runat="server" SkinID="TextboxSmallLeft" TabIndex="3"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td>
                                                <asp:Label ID="Label6" runat="server" Text="Bundle From" SkinID="LabelFilter"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb_dataBundle_From" runat="server" SkinID="TextboxDate" TabIndex="9"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tb_dataBundle_From"
                                                    Display="None"></asp:RequiredFieldValidator>
                                                <asp:ImageButton runat="server" ID="b_data3" ImageUrl="~/Images/calendario.png" CausesValidation="false"
                                                    ImageAlign="TextTop" />
                                                <ajax:CalendarExtender ID="CalendarExtender3" runat="server" PopupPosition="BottomRight"
                                                    TargetControlID="tb_dataBundle_From" PopupButtonID="b_data3" CssClass="calendar"
                                                    Format="dd/MM/yyyy" />
                                                <ajax:MaskedEditExtender ID="MaskedEditExtender3" runat="server" TargetControlID="tb_dataBundle_From"
                                                    MaskType="Date" Mask="99/99/9999" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                                </ajax:MaskedEditExtender>
                                                <ajax:MaskedEditValidator ID="MaskedEditValidator3" ControlExtender="MaskedEditExtender1"
                                                    runat="server" ControlToValidate="tb_dataBundle_From" IsValidEmpty="False" Display="Dynamic">
                                                </ajax:MaskedEditValidator>
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tb_time_From" BorderColor="#518012" BorderStyle="Solid"
                                                    ForeColor="#518012" Font-Size="11px" font-family="Verdana" Font-Bold="true" BorderWidth="2px"
                                                    Height="18px" Width="40px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tb_time_From"
                                                    Display="None"></asp:RequiredFieldValidator>
                                                <ajax:MaskedEditExtender ID="MaskedEditExtender5" runat="server" TargetControlID="tb_time_From"
                                                    MaskType="Time" Mask="99:99" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                                </ajax:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td>
                                                <asp:Label ID="Label4" runat="server" Text="Bundle To" SkinID="LabelFilter"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb_dataBundle_To" runat="server" SkinID="TextboxDate" TabIndex="10"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_dataBundle_To"
                                                    Display="None"></asp:RequiredFieldValidator>
                                                <asp:ImageButton runat="server" ID="b_data4" ImageUrl="~/Images/calendario.png" CausesValidation="false"
                                                    ImageAlign="TextTop" />
                                                <ajax:CalendarExtender ID="CalendarExtender2" runat="server" PopupPosition="BottomRight"
                                                    TargetControlID="tb_dataBundle_To" PopupButtonID="b_data4" CssClass="calendar"
                                                    Format="dd/MM/yyyy" />
                                                <ajax:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="tb_dataBundle_To"
                                                    MaskType="Date" Mask="99/99/9999" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                                </ajax:MaskedEditExtender>
                                                <ajax:MaskedEditValidator ID="MaskedEditValidator2" ControlExtender="MaskedEditExtender2"
                                                    runat="server" ControlToValidate="tb_dataBundle_To" IsValidEmpty="False" Display="Dynamic">
                                                </ajax:MaskedEditValidator>
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tb_time_To" BorderColor="#518012" BorderStyle="Solid"
                                                    ForeColor="#518012" Font-Size="11px" font-family="Verdana" Font-Bold="true" BorderWidth="2px"
                                                    Height="18px" Width="40px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="tb_time_To"
                                                    Display="None"></asp:RequiredFieldValidator>
                                                <ajax:MaskedEditExtender ID="MaskedEditExtender6" runat="server" TargetControlID="tb_time_To"
                                                    MaskType="Time" Mask="99:99" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                                </ajax:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td>
                                                <asp:Label ID="lb_jn" runat="server" Text="Job Name" SkinID="LabelFilter"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb_jn" runat="server" SkinID="TextboxSmallLeft" TabIndex="4"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td>
                                                <asp:Label ID="lb_rn" runat="server" Text="Report Name" SkinID="LabelFilter"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb_rn" runat="server" SkinID="TextboxSmallLeft" TabIndex="5"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td>
                                                <asp:Label ID="l_rec" runat="server" Text="Recipient" SkinID="LabelFilter"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb_rec" runat="server" SkinID="TextboxSmallLeft" TabIndex="6"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Text="Report From" SkinID="LabelFilter"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb_data_From" runat="server" SkinID="TextboxDate" TabIndex="7"></asp:TextBox>
                                                <asp:ImageButton runat="server" ID="b_data1" ImageUrl="~/Images/calendario.png" CausesValidation="false"
                                                    ImageAlign="TextTop" />
                                                <ajax:CalendarExtender ID="CalendarExtender1" runat="server" PopupPosition="BottomRight"
                                                    TargetControlID="tb_data_From" PopupButtonID="b_data1" CssClass="calendar" Format="dd/MM/yyyy" />
                                                <ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="tb_data_From"
                                                    MaskType="Date" Mask="99/99/9999" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                                </ajax:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td>
                                                <asp:Label ID="Label9" runat="server" Text="Report To" SkinID="LabelFilter"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tb_data_To" runat="server" SkinID="TextboxDate" TabIndex="8"></asp:TextBox>
                                                <asp:ImageButton runat="server" ID="b_data2" ImageUrl="~/Images/calendario.png" CausesValidation="false"
                                                    ImageAlign="TextTop" />
                                                <ajax:CalendarExtender ID="CalendarExtender4" runat="server" PopupPosition="BottomRight"
                                                    TargetControlID="tb_data_To" PopupButtonID="b_data2" CssClass="calendar" Format="dd/MM/yyyy" />
                                                <ajax:MaskedEditExtender ID="MaskedEditExtender4" runat="server" TargetControlID="tb_data_To"
                                                    MaskType="Date" Mask="99/99/9999" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                                </ajax:MaskedEditExtender>
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td colspan="2">
                                                <asp:RangeValidator ID="rangeVal" ControlToValidate="tb_limit" MinimumValue="0" MaximumValue="1000"
                                                    Type="Integer" ErrorMessage="Max Rows must be an integer between 0 and 1000."
                                                    Font-Size="Small" ForeColor="White" Font-Bold="true" runat="server">
                                                </asp:RangeValidator>
                                            </td>
                                        </tr>
                                        <tr class="data">
                                            <td colspan="2">
                                                <asp:CustomValidator ID="cv_dates" runat="server" ErrorMessage="Start date must be less/equal than end date."
                                                    Display="Dynamic" OnServerValidate="cv_dates_ServerValidate" ForeColor="White"
                                                    Font-Size="Small" Font-Bold="true" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                        </td>
                        <td class="pageLeftCollapse">
                            <div>
                                <asp:Panel ID="pHide" runat="server">
                                    <table>
                                        <tr class="data">
                                            <td>
                                                <asp:Image ID="imgArrows" runat="server" Height="25px" Width="15px" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                        </td>
                        <td class="pageRight" id="PageRight">
                            <cedim:CeGridView ID="gvBundlesN" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                AllowPaging="true" PagerStyle-CssClass="pager" HeaderStyle-ForeColor="White"
                                HeaderStyle-Height="25px" RowStyle-Height="25px" Visible="true" OnPageIndexChanging="gvBundlesN_PageIndexChanging"
                                SkinID="Tables2" Width="100%" Height="95%" EmptyDataRowStyle-Font-Bold="true"
                                EmptyDataText="No Data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="#518012"
                                ShowHeader="True" OnRowCreated="gvBundlesN_RowCreated" OnRowCommand="gvBundlesN_OnRowCommand"
                                OnRowDataBound="gvBundlesN_OnRowDataBound" DataKeyNames="BundleId,BundleName,BundleCategory,BundleSkel,BundleDest,ElabStartTime,ElabEndTime,InputLines,InputPages,ElabLines,ElabPages,Destination,XferStartTime,XferEndTime,XferOutBytes,ElabParameters,Status,ProcessedReports">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <table style="width: 100%; vertical-align: middle; text-align: center;">
                                                <tr>
                                                    <td style="width: 30px"></td>
                                                    <td style="width: 15%">
                                                        <asp:Label ID="lb_1" runat="server" Text="Name" SkinID="LabelFilter"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lb_2" runat="server" Text="Category" SkinID="LabelFilter"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lb_3" runat="server" Text="Skel" SkinID="LabelFilter"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lb_4" runat="server" Text="ElabStart" SkinID="LabelFilter"></asp:Label>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="lb_5" runat="server" Text="Status" SkinID="LabelFilter"></asp:Label>
                                                    </td>
                                                    <td style="width: 10%">
                                                        <asp:Label ID="lb_6" runat="server" Text="ElabPages" SkinID="LabelFilter"></asp:Label>
                                                    </td>
                                                    <td style="width: 20px"></td>
                                                    <td style="width: 20px"></td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Panel ID="pnlBundle" runat="server">
                                                <table style="width: 100%; vertical-align: middle; text-align: center;">
                                                    <tr>
                                                        <td style="width: 23px">
                                                            <asp:ImageButton ID="imgCollapsible" runat="server" CausesValidation="false" ImageUrl="~/Images/expandDG.png"
                                                                Width="17px" Height="17px" OnClick="imgCollapsible_Click" CommandName='<%#Eval("BundleId")%>' />
                                                            <asp:Image ID="img_red" runat="server" AlternateText="" Visible="false" ImageUrl="~/Images/red.png"
                                                                Width="17px" Height="17px" />
                                                            <asp:Image ID="img_orange" runat="server" AlternateText="" Visible="false" ImageUrl="~/Images/orange.png"
                                                                Width="17px" Height="17px" />
                                                            <asp:Image ID="img_grey" runat="server" AlternateText="" Visible="false" ImageUrl="~/Images/grey.png"
                                                                Width="17px" Height="17px" />
                                                            <asp:Image ID="img_green" runat="server" AlternateText="" Visible="false" ImageUrl="~/Images/green.png"
                                                                Width="17px" Height="17px" />
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lb_name2" runat="server" Text='<%#Eval("BundleName")%>' SkinID="LabelErr"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="Label15" runat="server" Text='<%#Eval("BundleCategory")%>' SkinID="LabelErr"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="Label16" runat="server" Text='<%#Eval("BundleSkel")%>' SkinID="LabelErr"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="Label10" runat="server" Text='<%#Eval("ElabStartTime")%>' SkinID="LabelErr"></asp:Label>
                                                        </td>
                                                        <td style="width: 10%">
                                                            <asp:Label ID="Label12" runat="server" Text='<%#Eval("Status")%>' SkinID="LabelErr"></asp:Label>
                                                        </td>
                                                        <td style="width: 10%">
                                                            <asp:Label ID="Label13" runat="server" Text='<%#Eval("ElabPages")%>' SkinID="LabelErr"></asp:Label>
                                                        </td>
                                                        <td style="width: 20px">
                                                            <asp:ImageButton ID="b_detAll" runat="server" CausesValidation="false" CommandName="DetailsAll"
                                                                ImageUrl="~/Images/details.gif" ToolTip="Details" Width="20px" Height="20px" />
                                                        </td>
                                                        <td style="width: 20px">
                                                            <asp:ImageButton ID="b_reqAll" runat="server" CausesValidation="false" ImageUrl="~/Images/queue.png"
                                                                ToolTip="Requeue" Width="20px" Height="20px" CommandName="RequeueAll" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlR" runat="server" Style="margin-left: 30px; margin-right: 30px; margin-top: 5px; margin-bottom: 5px; height: 0px;">
                                                <cedim:CeGridView ID="gvBundlesR" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                                    AllowPaging="false" PagerStyle-CssClass="pager" HeaderStyle-ForeColor="White"
                                                    HeaderStyle-Height="25px" RowStyle-Height="25px" Visible="true" SkinID="Tables2"
                                                    Width="100%" Height="95%" EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-HorizontalAlign="Center"
                                                    EmptyDataRowStyle-ForeColor="#518012" ShowHeader="False" DataKeyNames="BundleId,RecipientName,BundleName"
                                                    OnRowCreated="gvBundlesR_RowCreated" OnRowCommand="gvBundlesR_OnRowCommand" RowStyle-BorderColor="#518012"
                                                    RowStyle-BorderStyle="Solid" RowStyle-BorderWidth="1px">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Panel ID="pnlBundleR" runat="server">
                                                                    <table style="width: 100%; vertical-align: middle; text-align: left;">
                                                                        <tr>
                                                                            <td style="width: 20%">
                                                                                <asp:ImageButton ID="imgCollapsible2" runat="server" CausesValidation="false" ImageUrl="~/Images/expandDG.png"
                                                                                    Width="17px" Height="17px" OnClick="imgCollapsible2_Click" CommandName='<%#Eval("BundleId") + ";" + Eval("RecipientName")%>' />
                                                                            </td>
                                                                            <td style="width: 30%">
                                                                                <asp:Label ID="lb_R" runat="server" Text="Recipient: " SkinID="LabelErr"></asp:Label>
                                                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("RecipientName")%>' SkinID="LabelErr"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 30%">
                                                                                <asp:Label ID="Label2" runat="server" Text="Processed Reports: " SkinID="LabelErr"></asp:Label>
                                                                                <asp:Label ID="Label5" runat="server" Text='<%#Eval("ProcessedReports")%>' SkinID="LabelErr"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <asp:Label ID="Label11" runat="server" Text="Elab Pages: " SkinID="LabelErr"></asp:Label>
                                                                                <asp:Label ID="Label17" runat="server" Text='<%#Eval("ElabPages")%>' SkinID="LabelErr"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 20px">
                                                                                <asp:ImageButton ID="b_reqRec" runat="server" CausesValidation="false" ImageUrl="~/Images/queue.png"
                                                                                    ToolTip="Requeue" Width="20px" Height="20px" CommandName="RequeueRec" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnlLog" runat="server" Style="margin-left: 30px; margin-right: 30px; margin-top: 5px; margin-bottom: 10px">
                                                                    <asp:GridView AutoGenerateColumns="False" SkinID="TablesInner" ID="gvBundles" runat="server"
                                                                        Width="100%" Height="95%" OnRowCommand="gvBundles_OnRowCommand" OnRowDataBound="gvBundles_OnRowDataBound"
                                                                        DataKeyNames="BundleId, JobReportId, ReportId, ElabLines, ElabPages, ElabStartTime, ElabEndTime, ReportTimeRef, textId, RecipientName,
                                                                    JobReportName, JobName, JobNumber, JobExecutionTime, PendingOp, SrvName, UserTimeRef, UserTimeElab, UserRef, textString, 
                                                                    BundleName, BundleCategory, BundleSkel, BundleDest, BundleStartTime, BundleEndTime, InputLines, InputPages,  BundleLines,
                                                                    BundlePages, Destination, XferStartTime, XferEndTime, XferOutBytes, Status, ElabParameters,PrintedCopies,
                                                                    ReportName,FilterValue, TotPages, XferDateDiff, XferRecipient, CheckStatus,  HasNotes,LastUsedToView, LastUsedToCheck">
                                                                        <Columns>
                                                                            <asp:BoundField HeaderText="JobName" DataField="JobName" ItemStyle-Width="15%" />
                                                                            <asp:TemplateField HeaderText="ReportName" ItemStyle-Width="15%">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton SkinID="LinkButton" ID="lk_repName" runat="server" Text='<%#Eval("ReportName")%>'
                                                                                        CommandName="VarSets" CommandArgument='<%#Eval("ReportName") + ";" + Eval("RecipientName") + ";" + Eval("JobName") + ";" + Eval("BundleId")%>'></asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField HeaderText="ReportTimeRef" DataField="ReportTimeRef" ItemStyle-Width="20%" />
                                                                            <asp:BoundField HeaderText="ElabStartTime" DataField="ElabStartTime" ItemStyle-Width="20%" />
                                                                            <asp:BoundField HeaderText="ElabPages" DataField="ElabPages" ItemStyle-Width="10%" />
                                                                            <asp:BoundField HeaderText="PrintCop" DataField="PrintedCopies" ItemStyle-Width="10%" />
                                                                            <asp:BoundField HeaderText="PermCop" DataField="PermCopies" ItemStyle-Width="10%" />
                                                                            <asp:BoundField HeaderText="JobReportID" DataField="JobReportId">
                                                                                <HeaderStyle CssClass="hidden" />
                                                                                <ItemStyle CssClass="hidden" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="ReportId" DataField="ReportId">
                                                                                <HeaderStyle CssClass="hidden" />
                                                                                <ItemStyle CssClass="hidden" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="textId" DataField="textId">
                                                                                <HeaderStyle CssClass="hidden" />
                                                                                <ItemStyle CssClass="hidden" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="BundleId" DataField="BundleId">
                                                                                <HeaderStyle CssClass="hidden" />
                                                                                <ItemStyle CssClass="hidden" />
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25px">
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="b_det" runat="server" CausesValidation="false" CommandName="Details"
                                                                                        ImageUrl="~/Images/details.gif" ToolTip="Details" Width="20px" Height="20px" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25px">
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="b_req" runat="server" CausesValidation="false" ImageUrl="~/Images/queue.png"
                                                                                        ToolTip="Requeue" Width="20px" Height="20px" CommandName="Requeue" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                                <div style="clear: both">
                                                                    &nbsp;
                                                                </div>
                                                                <ajax:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="Server" TargetControlID="pnlLog"
                                                                    Collapsed="True" ExpandControlID="pnlBundleR" CollapseControlID="pnlBundleR"
                                                                    AutoCollapse="False" AutoExpand="False" ScrollContents="false" ImageControlID="imgCollapsible2"
                                                                    ExpandDirection="Vertical" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </cedim:CeGridView>
                                            </asp:Panel>
                                            <ajax:CollapsiblePanelExtender ID="ctlCollapsiblePanel" runat="Server" TargetControlID="pnlR"
                                                Collapsed="True" ExpandControlID="pnlBundle" CollapseControlID="pnlBundle" AutoCollapse="False"
                                                AutoExpand="False" ScrollContents="false" ImageControlID="imgCollapsible" ExpandDirection="Vertical" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </cedim:CeGridView>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="programmaticPopup" runat="server" Style="display: none; text-align: center; overflow: auto; height: 400px; width: 800px">
                    <table id="myPopup" style="background-color: #EFF1F4;">
                        <tr>
                            <td style="text-align: right;">
                                <asp:ImageButton ID="b_close" runat="server" CausesValidation="false" OnClick="b_close_Click"
                                    ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DetailsView ID="dv_BundlesTop" runat="server" FieldHeaderStyle-Wrap="false"
                                    FieldHeaderStyle-Font-Bold="true" AutoGenerateRows="False" CssClass="gridView"
                                    SkinID="TablesView" Visible="true" RowStyle-ForeColor="#518012" RowStyle-HorizontalAlign="Left"
                                    Width="775px">
                                    <Fields>
                                        <asp:BoundField HeaderText="Processed Reports" DataField="ProcessedReports" />
                                        <asp:BoundField HeaderText="BundleId" DataField="BundleId" />
                                        <asp:BoundField HeaderText="BundleName" DataField="BundleName" />
                                        <asp:BoundField HeaderText="BundleCategory" DataField="BundleCategory" />
                                        <asp:BoundField HeaderText="BundleSkel" DataField="BundleSkel" />
                                        <asp:BoundField HeaderText="BundleDest" DataField="BundleDest" />
                                        <asp:BoundField HeaderText="Bundle ElabStartTime" DataField="ElabStartTime" />
                                        <asp:BoundField HeaderText="Bundle ElabEndTime" DataField="ElabEndTime" />
                                        <asp:BoundField HeaderText="InputLines" DataField="InputLines" />
                                        <asp:BoundField HeaderText="InputPages" DataField="InputPages" />
                                        <asp:BoundField HeaderText="Bundle ElabLines" DataField="ElabLines" />
                                        <asp:BoundField HeaderText="Bundle ElabPages" DataField="ElabPages" />
                                        <asp:BoundField HeaderText="Destination" DataField="Destination" />
                                        <asp:BoundField HeaderText="XferStartTime" DataField="XferStartTime" />
                                        <asp:BoundField HeaderText="XferEndTime" DataField="XferEndTime" />
                                        <asp:BoundField HeaderText="XferOutBytes" DataField="XferOutBytes" />
                                        <asp:BoundField HeaderText="ElabParameters" DataField="ElabParameters" />
                                        <asp:BoundField HeaderText="Status" DataField="Status" />
                                    </Fields>
                                </asp:DetailsView>
                                <asp:DetailsView ID="dv_Bundles" runat="server" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true"
                                    AutoGenerateRows="False" CssClass="gridView" SkinID="TablesView" Visible="true"
                                    RowStyle-ForeColor="#518012" RowStyle-HorizontalAlign="Left" Width="775px">
                                    <Fields>
                                        <asp:BoundField HeaderText="JobReportID" DataField="JobReportId" />
                                        <asp:BoundField HeaderText="JobNumber" DataField="JobNumber" />
                                        <asp:BoundField HeaderText="JobReportName" DataField="JobReportName" />
                                        <asp:BoundField HeaderText="ReportId" DataField="ReportId" />
                                        <asp:BoundField HeaderText="Bundle ElabLines" DataField="ElabLines" />
                                        <asp:BoundField HeaderText="Bundle ElabPages" DataField="ElabPages" />
                                        <asp:BoundField HeaderText="ReportName" DataField="ReportName" />
                                        <asp:BoundField HeaderText="FilterValue" DataField="FilterValue" />
                                        <asp:BoundField HeaderText="TotPages" DataField="TotPages" />
                                        <asp:BoundField HeaderText="XferDateDiff" DataField="XferDateDiff" />
                                        <asp:BoundField HeaderText="XferRecipient" DataField="XferRecipient" />
                                        <asp:BoundField HeaderText="Text" DataField="TextString" />
                                        <asp:BoundField HeaderText="Status" DataField="Status" />
                                    </Fields>
                                </asp:DetailsView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button runat="server" ID="hiddenTargetControlForModalPopup" Style="display: none" />
                <ajax:ModalPopupExtender runat="server" ID="programmaticModalPopup" BehaviorID="programmaticModalPopupBehavior"
                    TargetControlID="hiddenTargetControlForModalPopup" PopupControlID="programmaticPopup"
                    BackgroundCssClass="modal" PopupDragHandleControlID="programmaticPopupDragHandle">
                </ajax:ModalPopupExtender>
                <asp:UpdateProgress ID="UpdateProgress" runat="server" DisplayAfter="200">
                    <ProgressTemplate>
                        <asp:Panel ID="panelUpdateProgress" runat="server">
                            <div>
                                <img id="Img1" src="~/Images/loader.gif" alt="" runat="server" />
                            </div>
                        </asp:Panel>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <ajax:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
                    BackgroundCssClass="modalProgress" PopupControlID="panelUpdateProgress" BehaviorID="ModalProgress">
                </ajax:ModalPopupExtender>
                <asp:Panel ID="popupError" runat="server" CssClass="popupError">
                    <table id="tbpopupError" style="background-color: #e5f6d1; width: 100%; height: 100%;">
                        <tr>
                            <td style="text-align: right;">
                                <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="false" OnClick="popupError_Click"
                                    ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100%; height: 100%; text-align: center">
                                <asp:Label ID="lb_popupError" runat="server" Text="" SkinID="LabelWarning"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:HiddenField ID="h_popupError" runat="server" />
                <ajax:ModalPopupExtender ID="ModalPopupExtender_Error" runat="server" TargetControlID="h_popupError"
                    BehaviorID="ModalPopupExtender_Error" PopupControlID="popupError" Drag="false" BackgroundCssClass="modal">
                </ajax:ModalPopupExtender>
                <asp:Panel ID="popupOK" runat="server" CssClass="popupWarning">
                    <div class="divWarning">
                        <asp:Label ID="lb_warning" runat="server" SkinID="LabelWarning"></asp:Label>
                    </div>
                    <div class="divWarning">
                        <asp:Button ID="b_popupCanc" CausesValidation="false" runat="server" Text="OK" Visible="true"
                            CssClass="buttonBig" />
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="h3" runat="server" />
                <ajax:ModalPopupExtender ID="ModalPopupExtenderOK" runat="server" TargetControlID="h3"
                    CancelControlID="b_popupCanc" BehaviorID="ModalPopupExtender3" PopupControlID="popupOK"
                    Drag="false" BackgroundCssClass="modal">
                </ajax:ModalPopupExtender>
                <asp:Panel ID="popupcopies" runat="server" Style="display: none; text-align: center; overflow: auto; height: 300px; width: 300px">
                    <table style="background-color: #EFF1F4;">
                        <tr>
                            <td style="text-align: right;">
                                <asp:ImageButton ID="b_close_cop" runat="server" CausesValidation="false" ImageUrl="~/Images/close.gif"
                                    ToolTip="Close" Width="14px" Height="14px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="divWarning">
                                    <asp:Label ID="Label8" runat="server" SkinID="LabelErr" Text="Bundle name: "></asp:Label>
                                    <asp:DropDownList ID="ddl_bnames" runat="server" AutoPostBack="False" RenderMode="Block"
                                        DropDownStyle="DropDown" Visible="true" CaseSensitive="False" CssClass="comboboxDoc">
                                    </asp:DropDownList>
                                </div>
                                <div class="divWarning">
                                    <asp:Label ID="Label7" runat="server" SkinID="LabelErr" Text="Number of copies: "></asp:Label>
                                    <asp:TextBox ID="tb_copies" runat="server" SkinID="TextboxDate"></asp:TextBox>
                                </div>
                                <div class="divWarning">
                                    <asp:Button ID="b_copies" CausesValidation="false" runat="server" Text="OK" Visible="true"
                                        CssClass="buttonBig" OnClick="b_copies_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:HiddenField ID="HiddenCopies" runat="server" />
                <asp:HiddenField ID="HiddenCopiesOp" runat="server" />
                <ajax:ModalPopupExtender ID="ModalPopupExtenderCopies" runat="server" TargetControlID="HiddenCopies"
                    BehaviorID="ModalPopupExtenderCopies" PopupControlID="popupcopies" Drag="false"
                    CancelControlID="b_close_cop" BackgroundCssClass="modal">
                </ajax:ModalPopupExtender>
                <asp:Panel ID="popupvarSets" runat="server" CssClass="popupWarningBig">
                    <table>
                        <tr>
                            <td style="text-align: right;">
                                <asp:ImageButton ID="ImageButtonCanc" runat="server" CausesValidation="false" ImageUrl="~/Images/close.gif"
                                    ToolTip="Close" Width="14px" Height="14px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="divWarning">
                                    <asp:Label ID="lb_varSets" runat="server" SkinID="LabelErr" Text=""></asp:Label>
                                </div>
                                <div class="divWarning">
                                    <asp:TextBox ID="tb_VarSets" runat="server" SkinID="TextboxSmallLeft" Text="0"></asp:TextBox>
                                </div>
                                <div class="divWarning">
                                    <asp:Button ID="b_varSetsUpdate" CausesValidation="false" runat="server" Text="Update"
                                        OnClick="b_varSetsUpdate_Click" Visible="true" CssClass="buttonBig" />
                                </div>
                                <div class="divWarning">
                                    <asp:Button ID="b_varSetsDelete" CausesValidation="false" runat="server" Text="Delete"
                                        Visible="true" CssClass="buttonBig" OnClick="b_varSetsDelete_Click" />
                                </div>
                                <div class="divWarning">
                                    <asp:Label ID="lb_varSetsMes" runat="server" SkinID="LabelErr" Visible="false"></asp:Label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:HiddenField ID="HiddenField1" runat="server" />
                <ajax:ModalPopupExtender ID="ModalPopupExtenderVarSets" runat="server" TargetControlID="HiddenField1"
                    BehaviorID="ModalPopupExtenderVarSets" PopupControlID="popupvarSets" Drag="false"
                    CancelControlID="ImageButtonCanc" BackgroundCssClass="modal">
                </ajax:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
