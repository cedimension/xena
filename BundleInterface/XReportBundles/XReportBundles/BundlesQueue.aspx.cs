﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using XReportBundles.Utils;
using System.Web.Services.Protocols;
using System.Configuration;
using XReportBundles.Business;
using System.Data;

namespace XReportBundles
{
    public partial class BundlesQueue : System.Web.UI.Page
    {
        #region Constants
        private string SORT_DESCENDING = "DESC";
        private string SORT_ASCENDING = "ASC";
        #endregion

        #region Log
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Main Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Navigation.User == null)
            {
                Navigation.Error = "Session ended. Please reload the application.";
                Response.Redirect("~/Error.aspx");
            }
            Navigation.SearchType = Utility.Searchtype.BUNDLES_QUEUE;
            if (!Page.IsPostBack)
            {
                this.tb_data_From.Attributes.Add("onchange", "TextChanged('tb_data_From')");
                this.tb_data_To.Attributes.Add("onchange", "TextChanged('tb_data_To')");
                this.tb_data_From.Text = Utility.GetToday();
                this.tb_data_To.Text = Utility.GetToday();
            }
            if (Page.Validators != null && Page.Validators.Count > 0)
            {
                ScriptManager.RegisterOnSubmitStatement(this, Page.GetType(), "", "fnOnUpdateValidators()");
            }
        }
        #endregion

        #region Private Methods
        private void BindGridViewOut()
        {
            int numPages = 0;
            if (Navigation.TableBundles_Q != null)
            {
                double pages = (double)Navigation.TableBundles_Q.Rows.Count / (double)int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
                numPages = (int)Math.Ceiling(pages);
            }
            this.gvBundlesN.PageSize = int.Parse(ConfigurationManager.AppSettings["GridViewRowsPage"]);
            if (Navigation.CurrentGridPage_Q != -1)
                this.gvBundlesN.PageIndex = Navigation.CurrentGridPage_Q;
            this.gvBundlesN.DataSource = Navigation.TableBundles_Q;
            this.gvBundlesN.Visible = true;
            this.gvBundlesN.PagerSettings.FirstPageText = "1";
            this.gvBundlesN.PagerSettings.LastPageText = "" + numPages + "";
            this.gvBundlesN.DataBind();
        }

        private BundlesManagement_WS.DocumentData BuildDocumentData()
        {
            try
            {
                BundlesManagement_WS.IndexEntry Entry = new BundlesManagement_WS.IndexEntry();
                BundlesManagement_WS.column BundleName = new BundlesManagement_WS.column();
                BundleName.colname = "BundleName";
                BundleName.Value = this.tb_bname.Text;
                BundlesManagement_WS.column JobName = new BundlesManagement_WS.column();
                JobName.colname = "JobName";
                JobName.Value = this.tb_jn.Text;
                BundlesManagement_WS.column ReportName = new BundlesManagement_WS.column();
                ReportName.colname = "ReportName";
                ReportName.Value = this.tb_rn.Text;
                BundlesManagement_WS.column From = new BundlesManagement_WS.column();
                From.colname = "From";
                From.Value = Utility.ParseDate(this.tb_data_From.Text, "{0:yyyyMMdd}");
                BundlesManagement_WS.column To = new BundlesManagement_WS.column();
                To.colname = "To";
                To.Value = Utility.ParseDate(this.tb_data_To.Text, "{0:yyyyMMdd}");
                BundlesManagement_WS.column Limit = new BundlesManagement_WS.column();
                Limit.colname = "Limit";
                if (this.tb_limit.Text != "0")
                    Limit.Value = this.tb_limit.Text;
                else
                    Limit.Value = "";
                Entry.Columns = new BundlesManagement_WS.column[6] { BundleName, JobName, ReportName, From, To, Limit };
                BundlesManagement_WS.DocumentData Data = new BundlesManagement_WS.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new BundlesManagement_WS.IndexEntry[1] { Entry };
                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                return null;
                //throw Ex;
            }
        }

        private BundlesManagement_WS.DocumentData BuildDocumentDataForDelete(string JRID, string ReportId, string bundleName)
        {
            try
            {
                BundlesManagement_WS.IndexEntry Entry = new BundlesManagement_WS.IndexEntry();
                BundlesManagement_WS.column BundleName = new BundlesManagement_WS.column();
                BundleName.colname = "BundleName";
                BundleName.Value = bundleName;
                BundlesManagement_WS.column JobReportID = new BundlesManagement_WS.column();
                JobReportID.colname = "JobReportID";
                JobReportID.Value = JRID;
                BundlesManagement_WS.column ReportID = new BundlesManagement_WS.column();
                ReportID.colname = "ReportID";
                ReportID.Value = ReportId;
                Entry.Columns = new BundlesManagement_WS.column[3] { BundleName, JobReportID, ReportID };
                BundlesManagement_WS.DocumentData Data = new BundlesManagement_WS.DocumentData();
                Data.IndexName = "";
                Data.IndexEntries = new BundlesManagement_WS.IndexEntry[1] { Entry };
                return Data;
            }
            catch (SoapException SoapEx)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                return null;
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("Failed WS Call in BuildDocumentData(): " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                return null;
                //throw Ex;
            }
        }

        public void LoadBundlesQueue()
        {
            try
            {
                log.Debug("LoadBundlesQueue() Started");
                BundlesManagement_WS.xenabundlesmanagement BundlesManagementWS = new BundlesManagement_WS.xenabundlesmanagement();
                BundlesManagementWS.Url = ConfigurationManager.AppSettings["XenaBundlesManagementUrl"];
                BundlesManagement_WS.DocumentData Data = BuildDocumentData();
                BundlesManagementWS.Timeout = 600000;
                Data = BundlesManagementWS.GetBundlesQueue(Data);
                Navigation.TableSource_Q = Utility.IndexEntriesToTable(Data.IndexEntries);
                if (Navigation.TableSource_Q != null)
                {
                    string[] columns = { "BundleName", "TOT"};
                    Navigation.TableBundles_Q = Navigation.TableSource_Q.DefaultView.ToTable(true, columns);
                    if (Navigation.TableBundles_Q != null)
                        BindGridViewOut();
                }
                else
                {
                    this.gvBundlesN.DataSource = null;
                    this.gvBundlesN.DataBind();
                }
                log.Debug("LoadBundlesQueue() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadBundlesQueue() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadBundlesQueue() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw Ex;
            }
        }

        public void DeleteBundlesQueueEntry(string JRID, string ReportId, string bundleName)
        {
            try
            {
                log.Debug("DeleteBundlesQueueEntry() Started");
                BundlesManagement_WS.xenabundlesmanagement BundlesManagementWS = new BundlesManagement_WS.xenabundlesmanagement();
                BundlesManagementWS.Url = ConfigurationManager.AppSettings["XenaBundlesManagementUrl"];
                BundlesManagement_WS.DocumentData Data = BuildDocumentDataForDelete(JRID, ReportId, bundleName);
                BundlesManagementWS.Timeout = 600000;
                if (JRID == "" && ReportId == "")
                    Data = BundlesManagementWS.DeleteBundlesQueue(Data);
                else
                    Data = BundlesManagementWS.DeleteBundlesQueueEntry(Data);
                if (Data.IndexEntries[0].Columns[0].colname == "ActionMessage" && Data.IndexEntries[0].Columns[0].Value.ToLower() != "success")
                {
                    this.lb_warning.Text = Data.IndexEntries[0].Columns[0].Value;
                    ModalPopupExtenderOK.Show();
                }
                this.LoadBundlesQueue();
                log.Debug("DeleteBundlesQueueEntry() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("DeleteBundlesQueueEntry() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("DeleteBundlesQueueEntry() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw Ex;
            }
        }

        public void LoadBundleRecipientList(string JRID, string ReportId, string bundleName)
        {
            try
            {
                log.Debug("LoadBundleRecipientList() Started");
                BundlesManagement_WS.xenabundlesmanagement BundlesManagementWS = new BundlesManagement_WS.xenabundlesmanagement();
                BundlesManagementWS.Url = ConfigurationManager.AppSettings["XenaBundlesManagementUrl"];
                BundlesManagement_WS.DocumentData Data = BuildDocumentDataForDelete(JRID, ReportId, bundleName);
                BundlesManagementWS.Timeout = 600000;
                Data = BundlesManagementWS.GetBundlesNameRecipient(Data);
                DataTable dt = Utility.IndexEntriesToTable(Data.IndexEntries);
                if (dt != null)
                {
                    string[] filter = { "BundleName", "BundleCategory", "BundleSkel" };
                    Navigation.TableRecipient_Q = dt;
                    this.gv_recOut.DataSource = dt.DefaultView.ToTable(true, filter);
                    this.gv_recOut.DataBind();
                }
                else
                {
                    this.gv_recOut.DataSource = null;
                    this.gv_recOut.DataBind();
                }
                log.Debug("LoadBundleRecipientList() Finished");
            }
            catch (SoapException SoapEx)
            {
                log.Error("LoadBundleRecipientList() Failed: " + SoapEx.Message);
                Navigation.Error = SoapEx.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw SoapEx;
            }
            catch (Exception Ex)
            {
                log.Error("LoadBundleRecipientList() Failed: " + Ex.Message);
                Navigation.Error = Ex.Message;
                this.lb_popupError.Text = "AN ERROR OCCURRED: <br/>" + Navigation.Error;
                this.ModalPopupExtender_Error.Show();
                //throw Ex;
            }
        }

        #endregion

        #region Events
        protected void buttonSearch_Click(object sender, EventArgs e)
        {
            Navigation.CurrentGridPage_Q = 0;
            //if (Navigation.User == null)
                //Navigation.User = new User("xreport", "SUPER", "C0X1NADM", "C0", "X1NADM");
            if (this.tb_limit.Text == "")
                this.tb_limit.Text = "500";
            LoadBundlesQueue();
        }

        protected void gvBundlesN_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Navigation.CurrentGridPage_Q = e.NewPageIndex;
            BindGridViewOut();
        }

        protected void gvBundlesN_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string bundleName = this.gvBundlesN.DataKeys[e.Row.RowIndex].Value.ToString();
                GridView subView = e.Row.FindControl("gvQueue") as GridView;
                if (Navigation.TableSource_Q != null)
                {
                    DataView dv = Navigation.TableSource_Q.DefaultView;
                    dv.RowFilter = "BundleName = '" + bundleName + "'";
                    subView.DataSource = dv.ToTable();
                    subView.DataBind();
                    ImageButton b_del = (ImageButton)e.Row.FindControl("b_delAll");
                    b_del.CommandArgument = ";" + ";" + bundleName;
                    ImageButton b_rec = (ImageButton)e.Row.FindControl("b_recAll");
                    b_rec.CommandArgument = ";" + ";" + bundleName;
                }
            }
        }

        protected void gvBundlesN_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteAll")
            {
                Navigation.ViewParameters = e.CommandArgument.ToString();
                string[] args = Navigation.ViewParameters.Split(';');
                this.lb_w.Text = "All the entries with bundle name " + args[2] + " will be deleted. Are you sure you want to continue?";
                this.ModalPopupExtenderConf.Show();
            }
            else if (e.CommandName == "RecipientAll")
            {
                string[] args = e.CommandArgument.ToString().Split(';');
                LoadBundleRecipientList(args[0], args[1], args[2]);
                this.ModalPopupExtenderRec.Show();
            }
        }

        protected void gvQueue_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView view = (DataRowView)e.Row.DataItem;
                string jrid = view["JobReport_ID"].ToString();
                string id = view["ReportId"].ToString();
                string name = view["BundleName"].ToString();
                ImageButton b_det = (ImageButton)e.Row.FindControl("b_det");
                b_det.CommandArgument = jrid + ";" + id + ";" + name;
                ImageButton b_del = (ImageButton)e.Row.FindControl("b_del");
                b_del.CommandArgument = jrid + ";" + id + ";" + name;
                ImageButton b_rec = (ImageButton)e.Row.FindControl("b_rec");
                b_rec.CommandArgument = jrid + ";" + id + ";" + name;
            }
        }

        protected void gvQueue_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Details")
            {
                string[] args = e.CommandArgument.ToString().Split(';');
                string jrid = args[0].ToString();
                string id = args[1].ToString();
                string name = args[2].ToString();
                DataTable dt = Navigation.TableSource_Q;
                DataView dv = dt.DefaultView;
                dv.RowFilter = "JobReport_ID = '" + jrid + "' AND ReportId = '" + id + "' AND BundleName = '" + name + "'";
                this.dv_Bundles.DataSource = dv.ToTable();
                this.dv_Bundles.DataBind();
                this.programmaticModalPopup.Show();
            }
            else if (e.CommandName == "Del")
            {
                Navigation.ViewParameters = e.CommandArgument.ToString();
                this.lb_w.Text = "Are you sure you want to delete the selected entry?";
                this.ModalPopupExtenderConf.Show();
            }
            else if (e.CommandName == "Recipient")
            {
                string[] args = e.CommandArgument.ToString().Split(';');
                LoadBundleRecipientList(args[0], args[1], args[2]);
                this.ModalPopupExtenderRec.Show();
            }
        }

        protected void gv_recOut_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string bundleName = this.gv_recOut.DataKeys[e.Row.RowIndex].Values[0].ToString();
                string bundleCat = this.gv_recOut.DataKeys[e.Row.RowIndex].Values[1].ToString();
                string bundleSkel = this.gv_recOut.DataKeys[e.Row.RowIndex].Values[2].ToString();
                GridView subView = e.Row.FindControl("gv_recipient") as GridView;
                if (Navigation.TableRecipient_Q != null)
                {
                    DataView dv = Navigation.TableRecipient_Q.DefaultView;
                    dv.RowFilter = "BundleName = '" + bundleName + "' AND BundleCategory = '" + bundleCat + "' AND BundleSkel = '" + bundleSkel + "'";
                    subView.DataSource = dv.ToTable();
                    subView.DataBind();
                }
            }
        }

        protected void b_close_Click(object sender, EventArgs e)
        {
            this.programmaticModalPopup.Hide();
        }

        protected void b_yes_Click(object sender, EventArgs e)
        {
            string[] args = Navigation.ViewParameters.Split(';');
            this.DeleteBundlesQueueEntry(args[0], args[1], args[2]);
            this.ModalPopupExtenderConf.Hide();
            this.LoadBundlesQueue();
        }

        protected void cv_dates_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = Convert.ToDateTime(this.tb_data_From.Text) <= Convert.ToDateTime(this.tb_data_To.Text);
        }

        protected void popupError_Click(object sender, EventArgs e)
        {
            this.ModalPopupExtender_Error.Hide();
        }
        #endregion
    }
}