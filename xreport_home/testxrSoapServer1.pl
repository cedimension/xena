#!/usr/bin/perl

use Data::Dumper;
use SOAP::Lite +trace => [qw(transport trace method fault debug)];
import SOAP::Data qw(name type attr);
use XML::Simple;
use MIME::Base64 qw();

use CGI;

*SOAP::Deserializer::typecast = sub { my( $self, $val, $name, $attrs, $children, $type ) = @_; 
#                     return undef if (caller())[2] == 2255;
#                     print ref($self), " TYPECAST for T:$type - N:$name: caller: ", join('::', (caller())), " CHILD: ", Dumper($children), "\nATTR:", Dumper($attrs), "\nVAL:", Dumper($val), "\n"; 
                      return { content => $val, %$attrs } if $name =~ /^\{.*\}columns$/i; 
                      return undef;
                    };

#use MIME::Entity;
#
#my $ent = build MIME::Entity
#    Type        => "text/plain",
#    Path        => "../attachment.txt",
#    Filename    => "PGEUserInfo.wsdl",
#    Disposition => "attachment";

sub xrCall {
  my ($soaph, $service, $DocumentData) = (shift, shift, shift);
  ($service, my $method) = $service =~ /^(?:(.*?)[\#\/\\])?(.*)$/;
  $service = 'xrQuery' unless $service;
  my $server = "http://$ARGV[0]/$service/?session=MyServer";
  unless (ref($soaph)) {
    $soaph = SOAP::Lite
    ->packager(SOAP::Lite::Packager::MIME->new()) 
#    ->parts([ $ent ])
     ;
  }
  *SOAP::Lite::Packager::headers_http = sub { print "xxxxxxxxxxx", @_, "\n";};
  my $rtype = 'REQUEST';
  @{$DocumentData}{qw(comment xmlns)} = ('Version 2.0', 'http://xreport.org/WebService');
  my $reqattrs = { map { $_ => $DocumentData->{$_} } grep !/^IndexEntries|NewEntries|DocumentBody/, keys %$DocumentData };
  print "Body len: ", length($DocumentData->{DocumentBody}), "\n";
  my $obj = $soaph->proxy("http://$ARGV[0]/$service/?session=MyServer")
    ->autotype(0)->on_action( sub { $service.'#'.$method })
    ->call(SOAP::Data->name('REQUEST')->attr($reqattrs), ( 
       map { my $n = $_; ($n =~ /^(?:Index|New)Entries$/
                  ?  map { my $c = delete($_->{Columns}); SOAP::Data::name($n)->attr( {DocumentType => 'IndexEntry', %$_} )
                     ->value([ (ref($c) eq 'ARRAY'
                            ? map { my $v = delete($c->{content}) || '';SOAP::Data::name('Columns')->attr($_)->value($v) } @{$c}
                            : map { my $v = delete($c->{$_}->{content}) || '';SOAP::Data::name('Columns')->attr({colname => $_, %{$c->{$_}}})->value($v) } keys %$c
                           )
                         ]) 
                       } @{$DocumentData->{$_}}
              : $n eq 'DocumentBody' ? SOAP::Data::name($_)->type('xsd:base64Binary' => $DocumentData->{$_})
                  : SOAP::Data->name($_)->attr({})->value([$DocumentData->{$_}])
                 ) } qw(IndexEntries NewEntries DocumentBody) )
                 ,SOAP::Header->name('Security')->value('pippo')
      )
      ;
  return $obj;
}

my $DocPAcli = SOAP::Lite
  ->proxy("http://$ARGV[0]/?session=MyServer")
#  ->packager(SOAP::Packager::MIME->new)
#  ->parts([ $ent ])
  ;

my $http = $DocPAcli->{'_transport'}
      ->{'_proxy'}
      ->{'_http_request'};
      
$http->header(hhhh => 'ggg', jjjj => 'kkkk');      

$reqs = { 
#           'DocumentsIFace/getReportEntryPdf' => { IndexName => 'CTDWEB_GENERIC_INDEX', TOP => 10, DocumentType => 'Application/x-pdf',
#                                                   IndexEntries => [ { Columns => { 'JobReportId' => { content => '99' }
#                                                                                  , 'ReportId'    => { content => '1' }    
#                                                                                  , 'ListOfPages' => { content => '99,1,1' } }
#                                                               } ],
#                                                   NewEntries => [{}],
#                                                   DocumentBody => '',
#                                                 },
#           'XreportWebIFace/downloadOutput' => { IndexName => 'CTDWEB_GENERIC_INDEX', TOP => 10, DocumentType => 'Application/x-pdf',
#                                                   IndexEntries => [ { Columns => { 'JobReportId' => { content => '99' }
#                                                                                  , 'Command'    => { content => 'O' }    
#                                                                                  , 'ReportId'    => { content => '1' }    
#                                                                                  , 'ListOfPages' => { content => '99,1,1' } }
#                                                               } ],
#                                                   NewEntries => [{}],
#                                                   DocumentBody => '',
#                                                 },
#           'XreportWebIFace/downloadInput' => { IndexName => 'CTDWEB_GENERIC_INDEX', TOP => 10, DocumentType => 'Application/x-pdf',
#                                                   IndexEntries => [ { Columns => { 'JobReportId' => { content => '99' }
#                                                                                  , 'Command'    => { content => 'O' }    
#                                                                                  , 'ReportId'    => { content => '1' }    
#                                                                                  , 'ListOfPages' => { content => '99,1,1' } }
#                                                               } ],
#                                                   NewEntries => [{}],
#                                                   DocumentBody => '',
#                                                 },
           'XreportWebIFace/manageJobReports' => { IndexName => 'CTDWEB_GENERIC_INDEX', TOP => 10, DocumentType => 'Application/x-pdf',
                                                   IndexEntries => [ { Columns => { 'JobReportId' => { content => '4119' }
                                                                                  , 'Command'    => { content => 'O' }    
                                                                                  , 'ReportId'    => { content => '1' }    
                                                                                  , 'ListOfPages' => { content => '99,1,1' } }
                                                               } ],
                                                   NewEntries => [{}],
                                                   DocumentBody => '',
                                                 },
##          'DocumentsIFace/getUserConfig'         => { IndexName => 'CTDWEB_GENERIC_INDEX', TOP => 10, DocumentType => 'Application/x-pdf',
#           'xreportwebiface/getUserConfig'         => { IndexName => 'CTDWEB_GENERIC_INDEX', TOP => 10, DocumentType => 'Application/x-pdf',
#                                                   IndexEntries => [ { Columns => { 'UserName'     => { content => "xreport" }
#                                                                                  , 'AuthMethod'    => { content => 'NTLM' }
#                                                                                  , 'ApplName'    => { content => 'oper' }
##                                                                                  , 'ApplName'    => { content => 'Documents' }
#                                                                     }
#                                                               } ],
#                                                   NewEntries => [{}],
#                                                   DocumentBody => '',
#                                                 },
        };
#for my $rnam (qw(GER AGE)) { 
#  my $DocumentData = { IndexName => 'CTDWEB_GENERIC_INDEX', TOP => 10, DocumentType => 'Application/x-pdf',
#             IndexEntries => [ { 
#                    Columns => {'VARNAME' => { Min => 'LIKE', content => "$rnam\%" },
#                            'XferStartTime' => {Min => '2009-09-01' },
#                           },
#                       },
#                     ],
#             NewEntries => [{}],
#             DocumentBody => '',
#           };
while ( my ($method, $DocumentData) = each %{$reqs} ) {
  my $obj = xrCall($DocPAcli, $method, $DocumentData);
  my $rawresp = $DocPAcli->transport()->http_response()->content();
  print sprintf('REquest returned %d bytes of Response Data: ', length($rawresp)), "\n";

  if ( ref($obj) && (my $res = $obj->fault()) ) {
    print sprintf('SOAP Fault received - Code: %s - Actor: %s - Msg: %s - Detail Message: ',
        $obj->faultcode(), $obj->faultactor(), $obj->faultstring()), "\n", $obj->faultdetail()->{description}, "\n";
    exit 0;    
  }
  elsif (ref($obj)) {
    my $res = $obj->result();
  }
  else {
    print "NO SOAP RESULT - Response Received:\n$rawresp\n";
    exit 0; 
  }

  my $objstruct = XMLin($rawresp,
                          KeyAttr => {'Columns' => 'colname'},
 #                         KeyAttr => [qw(IndexEntries NewEntries colname)],
                          ForceArray => [ qw(NewEntries IndexEntries Columns ValuesArray) ],
                          ContentKey => '-content',
                         );
                         
  warn "Parsed OBJECT:", Dumper($objstruct), "\n";
  my $ixe = $objstruct->{'soap:Body'}->{RESPONSE}->{IndexEntries};                       
  
  $ixe = [ $ixe ] unless ref($ixe) eq 'ARRAY';
  my $rows = [];
  foreach my $row ( @{$ixe} ) {
    my $rowcols = delete $row->{Columns};
    if ( ref($rowcols) ne 'HASH' ) {
       push @$rows, { %{$row}, Columns => $rowcols };
       next;
    }
    foreach $key ( keys %{$rowcols} ) {
      my $val = delete $rowcols->{$key}; 
      $rowcols->{$key} = (ref($val) ? $val->{content} : $val); 
    }
    push @$rows, { %{$row}, %{$rowcols} };
  }
  my @cols = (keys %{$rows->[0]});
  print join("\n", '=' x 80, join("\t", @cols), (map { join("\t", map {$_ || '' }values(%$_)) } @$rows), '=' x 80), "\n";
  my $docb64 = $objstruct->{'soap:Body'}->{RESPONSE}->{DocumentBody};
  my $fn = $objstruct->{'soap:Body'}->{RESPONSE}->{FileName};
  if ($docb64) {
    $fn = $method.'.out.bin' unless $fn;
    my $doc = MIME::Base64::decode_base64($docb64);
    print "BodyLen: ", length($doc), "\n";
    open OUTFILE, ">$fn";
    binmode OUTFILE;
    print OUTFILE $doc;
    close OUTFILE;
  }
#  last;
}
__END__

#  my $ixe = $obj->body();
#  my $ixe = $obj->valueof('//RESPONSE/IndexEntries');
#  my $doc = MIME::Base64::decode_base64($obj->valueof('//RESPONSE/DocumentBody'));
#  print Dumper($ixe);

# ->outputxml(1)
#    ->on_fault(sub { my($soap, my $res) = @_;
#         warn "ON FAULT SUB CALLED\n"; 
#         warn (ref($res) ? ($res->faultstring, Dumper($res->fault())) : ('NO RESULT', $soap->transport->status)), "\n";
#         return $res;
#       })

#  print "BodyLen: ", length($doc), "\n";
#  open OUTFILE, ">result.pdf";
#  binmode OUTFILE;
#  print OUTFILE $doc;
#  close OUTFILE;
#  open OUTFILE, ">result_variant.pdf";
#  binmode OUTFILE;
#use Win32::OLE::Variant;
#  print OUTFILE Win32::OLE::Variant->new( VT_UI1, $doc);
#  close OUTFILE;
#
#  open INFILE, "<testpdfform.pdf";
#  binmode INFILE;
#  read(INFILE, my $indoc, -s 'testpdfform.pdf');
#  close INFILE;
#  print "FILE SIZE: ", -s 'testpdfform.pdf', " VAR SIZE: ", length($indoc), "\n";
#  my $newobj = xrCall($DocPAcli, 'DocumentsIFace/getDocList', 
#             { IndexName => '_Resources', TOP => 10, DocumentType => 'pdfform', FileName => 'testpdfform',
#             IndexEntries => [ { 
#                    Columns => {'ResName' => { content => "testpdfform" },
#                            'ResClass' => { content => 'pdfform' },
#                           },
#                       },
#                     ],
#             DocumentBody => MIME::Base64::encode($indoc)
#           });
#  
#  $ixe = $newobj->valueof('//RESPONSE/IndexEntries');
##  print Dumper($ixe);
#
#  $ixe = [ $ixe ] unless ref($ixe) eq 'ARRAY';
#  $rows = [];
#  foreach my $row ( @{$ixe} ) {
#    push @$rows, { map { $_->{colname} => $_->{content} } @{$row->{Columns}} };
#  }
#  @cols = (keys %{$rows->[0]});
#  print join("\n", '=' x 80, join("\t", @cols), (map { join("\t", values(%$_)) } @$rows), '=' x 80), "\n";
#

#  ->service("http://hermes:60080/ResourceServer/?wsdl")
#  ->on_action( sub { "urn:HelloWorld#sayHello" })
#  ->uri('http://tempuri.org/')
      ->proxy($server)
#  ->use_prefix(0)
#  ->default_ns('urn:HelloWorld')

use strict;
use warnings;
use Data::Dumper;
use MIME::Base64 ();

use LWP;
use XML::Simple;

my $r = LWP::UserAgent->new;

my $cfgresp = $r->post( "http://$ARGV[0]/?getCentralConfig", 
            [ server => 'DocumentsIFace', 
              cfguser => '', 
            ] );
print $cfgresp->status_line(), "\n";

print Dumper(XMLin($cfgresp->content(), KeepRoot => 1)), "\n";

__END__

package XReport::SOAP::Client;

use base 'SOAP::Lite';

sub new {
  my ($self, $service) = (shift, shift);
  ($service, my $method) = $service =~ /^(?:(.*?)[\#\/\\])?(.*)$/;
  $service = 'xrQuery' unless $service;
  my $server = "http://hermes:60080/$service/?session=MyServer";
  unless (ref $self) {
    my $class = ref($self) || $self;
    $self = $class->SUPER::new();
  }
  my $server = shift;
  $self->proxy($server);
  retyurn $self;
}

1;

package main;

use XML::Simple;

my $DocumentData;

$DocumentData = { IndexName => 'pippo', TOP => 10, DocumentType => 'Application/x-pdf',
             IndexEntries => [ { JobReportId => 12367, FromPage => 2, ForPages => '3',
                    Columns => [{colname => 'ResName', Min => 'LIKE', content => 'CC%' },
                             {colname => 'XFerStartTime', Min => '2010-01-01' },
                            ],
                            },
                       { JobReportId => 12367, FromPage => 2, ForPages => '3',
                    Columns => [{colname => 'ResName', Min => 'LIKE', content => 'D%' },
                            ],
                            },
                     ],
             NewEntries => [],
             DocumentBody => ''
           };

$DocumentData = { IndexName => 'pippo', TOP => 10, DocumentType => 'Application/x-pdf',
             IndexEntries => [ { JobReportId => 12367, FromPage => 2, ForPages => '3',
                    Columns => {'ResName' => { Min => 'LIKE', content => 'CC%' },
                             'XFerStartTime' => {Min => '2010-01-01' },
                            },
                       },
                       { JobReportId => 12367, FromPage => 5, ForPages => '3',
                    Columns => {'ResName' => {Min => 'LIKE', content => 'D%' },
                            },
                       },
                     ],
             NewEntries => [],
             DocumentBody => {content => ''}
           };


my $reqattrs = { map { $_ => $DocumentData->{$_} } grep !/^IndexEntries|NewEntries|DocumentBody/, keys %$DocumentData };
$reqattrs->{comment} = 'Version 2.0';
my $rtype = 'REQUEST';

my $xml2 = <<END_XML;
<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><REQUEST IndexName="pippo" comment="Version 2.0" DocumentType="Application/x-pdf" TOP="10"><IndexEntries JobReportId="12367" FromPage="2" ForPages="3"><Columns Value="CC%" Min="LIKE" colname="ResName" /><Columns Min="2010-01-01" colname="XFerStartTime" /></IndexEntries><IndexEntries JobReportId="12367" FromPage="2" ForPages="3"><Columns Value="D%" Min="LIKE" colname="ResName" /></IndexEntries><NewEntries /><DocumentBody /></REQUEST></soap:Body></soap:Envelope>
END_XML
;
use Data::Dumper;
print Dumper(SOAP::Custom::XML::Deserializer->deserialize($xml2)->valueof('//REQUEST'));

__END__


my $xml1 = <<END_XML;
<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><REQUEST xx2="a2" xx1="a1"><IndexEntries soapenc:arrayType="xsd:anyType[2]" xsi:type="soapenc:Array"><Columns><content xsi:type="xsd:string">CC%</content><Min xsi:type="xsd:string">LIKE</Min><colname xsi:type="xsd:string">ResName</colname></Columns><Columns><Min xsi:type="xsd:date">2010-01-01</Min><colname xsi:type="xsd:string">XFerStartTime</colname></Columns></IndexEntries><IndexEntries soapenc:arrayType="xsd:anyType[1]" xsi:type="soapenc:Array"><Columns><content xsi:type="xsd:string">D%</content><Min xsi:type="xsd:string">LIKE</Min><colname xsi:type="xsd:string">ResName</colname></Columns></IndexEntries></REQUEST></soap:Body></soap:Envelope>
END_XML
;
my $xml2 = <<END_XML;
<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XM
LSchema" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><REQUEST IndexName="pippo" comment="Version 2.0" DocumentType="
Application/x-pdf" TOP="10"><IndexEntries soapenc:arrayType="xsd:anyType[2]" xsi:type="soapenc:Array"><Columns Value="CC%" Min="LIKE" colname="ResName" /><Columns Min="2010-01-01" colname="XFerStartTim
e" /></IndexEntries><IndexEntries soapenc:arrayType="xsd:anyType[1]" xsi:type="soapenc:Array"><Columns Value="D%" Min="LIKE" colname="ResName" /></IndexEntries></REQUEST></soap:Body></soap:Envelope>
END_XML
;
use Data::Dumper;
print Dumper(SOAP::Deserializer->deserialize($xml2)->body());
__END__

print     SOAP::Serializer->envelope
  (freeform =>  SOAP::Data->name($rtype)
   ->uri('http://tempuri.org/')
   ->attr($reqattrs),
   SOAP::Data->value
   (map { map { name($_)->uri('http://tempuri.org/')->value
        ( [ map { name('Columns')->uri('http://tempuri.org/')->attr($_) } @{$_->{Columns}} ] ) } @{$DocumentData->{$_}} }  
      qw(IndexEntries NewEntries)  ))
  ;

__END__
print $DocPAcli->REQUEST( #serializer->envelope('freeform'=>
             SOAP::Data->name('DocumentData')->uri('http://tempuri.org/') 
             => [SOAP::Data->name('IndexEntries')->uri('http://tempuri.org/')->value([{Columns => {colname => 'pippo'}}])]
            );


my $obj =
#$DocPAcli->call('listResources');

__END__



print "-" x 80, "\n";
print "Elabora: ", ref($obj), " -\n", Dumper($obj), "\n";
print "-" x 80, "\n";
__END__
my $rtype = 'RESPONSE';

my $DocumentData = { IndexName => 'pippo', TOP => 10, DocumentType => 'Application/x-pdf',
             IndexEntries => [ { Columns => {'PLUTO' => { Min => 'IN', content => 'V1 V2 V3' },
                             'PAPER' => { Min => 'IN', content => 'V1 V2 V3' },
                            },
                            },
                     ],
             NewEntries => [],
             DocumentBody => ''
           };
my $reqattrs = { map { $_ => $DocumentData->{$_} } grep !/^IndexEntries|NewEntries|DocumentBody/, keys %$DocumentData };
$reqattrs->{comment} = 'Version 2.0';

print     SOAP::Serializer->envelope
     (response => SOAP::Data->name('ResourceServer')->attr({xmlns => "http://xreport.org/WebService"})
      , SOAP::Data->name($rtype)->attr($reqattrs)
#      , SOAP::Data #->name($rtype)
      ->value({map { $_ => SOAP::Data->value($DocumentData->{$_}) } qw(IndexEntries NewEntries)} )
     )
;
__END__

#print SOAP::Serializer->serialize
#    (
print     SOAP::Serializer->envelope
     (response => SOAP::Header->new()
     , SOAP::Data->name($rtype)
      ->attr($reqattrs),
      SOAP::Data->value(map { SOAP::Data->name($_ => $DocumentData->{$_}) } qw(IndexEntries NewEntries) )
     )
  ;
#    );



\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1386\t5080\t25\t38530\t25\t5\t40127\t01534   \tNULL\tNULL\t6/20/2006 12:00:00 AM\t6/20/2006 12:00:00 AM\t1\t4
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1386\t5080\t17\t192025\t17\t5\t118953\t01534   \tNULL\tNULL\t6/20/2006 12:00:00 AM\t6/20/2006 12:00:00 AM\t5\t1
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1386\t5080\t19\t203686\t19\t330\t587\t00850   \tNULL\tNULL\t6/20/2006 12:00:00 AM\t6/19/2006 12:00:00 AM\t6\t6
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1386\t5080\t25\t217977\t25\t330\t185\t00850   \tNULL\tNULL\t6/20/2006 12:00:00 AM\t6/19/2006 12:00:00 AM\t12\t6
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1386\t5080\t0\t164041\t0\t330\t25069\t00386   \tNULL\tNULL\t6/20/2006 12:00:00 AM\t6/19/2006 12:00:00 AM\t18\t1
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1386\t5080\t0\t109394\t0\t5\t125215\t01534   \tNULL\tNULL\t6/20/2006 12:00:00 AM\t6/20/2006 12:00:00 AM\t19\t1
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1386\t5080\t17\t163247\t0\t0\t0\t01534   \tNULL\tNULL\t6/20/2006 12:00:00 AM\t6/20/2006 12:00:00 AM\t20\t1
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1386\t5080\t23\t213171\t23\t5\t121596\t01534   \tNULL\tNULL\t6/20/2006 12:00:00 AM\t6/20/2006 12:00:00 AM\t21\t2
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1386\t5080\t23\t239903\t23\t5\t125253\t01534   \tNULL\tNULL\t6/20/2006 12:00:00 AM\t6/20/2006 12:00:00 AM\t23\t2
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1386\t5080\t38\t237888\t38\t330\t600083\t00850   \tNULL\tNULL\t6/20/2006 12:00:00 AM\t6/19/2006 12:00:00 AM\t25\t6
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1386\t5080\t11\t116758\t11\t114\t2000181\t00850   \tNULL\tNULL\t6/20/2006 12:00:00 AM\t6/19/2006 12:00:00 AM\t31\t5
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1394\t5080\t0\t30103\t0\t0\t0\t01534   \tNULL\tNULL\t6/26/2006 12:00:00 AM\t6/26/2006 12:00:00 AM\t1\t1
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1394\t5080\t2\t179364\t2\t5\t118965\t00850   \tNULL\tNULL\t6/26/2006 12:00:00 AM\t6/23/2006 12:00:00 AM\t2\t3
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1394\t5080\t8\t140123\t8\t5\t106080\t00850   \tNULL\tNULL\t6/26/2006 12:00:00 AM\t6/23/2006 12:00:00 AM\t5\t3
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1394\t5080\t8\t206517\t8\t5\t120448\t00850   \tNULL\tNULL\t6/26/2006 12:00:00 AM\t6/23/2006 12:00:00 AM\t8\t3
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1394\t5080\t11\t94662\t11\t5\t118600\t00850   \tNULL\tNULL\t6/26/2006 12:00:00 AM\t6/23/2006 12:00:00 AM\t11\t3
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1394\t5080\t34\t200953\t34\t5\t119906\t00850   \tNULL\tNULL\t6/26/2006 12:00:00 AM\t6/23/2006 12:00:00 AM\t14\t3
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1394\t5080\t0\t28315\t0\t5\t116216\t01534   \tNULL\tNULL\t6/26/2006 12:00:00 AM\t6/26/2006 12:00:00 AM\t17\t2
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1394\t5080\t99\t20904\t0\t330\t605\t00136   \tNULL\tNULL\t6/26/2006 12:00:00 AM\t6/23/2006 12:00:00 AM\t19\t9
\\\\TCEREPORT\\XREPORT_WORK\\TEST_DOCPA_DOCLIST\\file1.pdf\t1394\t5080\t0\t20909\t0\t330\t415\t00386   \tNULL\tNULL\t6/26/2006 12:00:00 AM\t6/23/2006 12:00:00 AM\t28\t3
EOF
print "-" x 80, "\n";
print "Elabora: ", ref($obj), " -\n", Dumper($obj), "\n";
print "-" x 80, "\n";

__END__


print "Content-type: text/html\n\n";
 
use HTTP::Request::Common;
use LWP::UserAgent;
use CGI qw(:standard);
use strict;
use warnings;
 
my ($file, $result, $message);
my $filePath = '..';
 
$file = $filePath.'/PGEUserInfo.wsdl';
 
my $ua = LWP::UserAgent->new;
my $req = $ua->request(POST "http://$ARGV[0]",
          Content_Type => 'form-data',
          Content => [
        foo => "pippo",
        bar => "pluto",
        Upload => ["$file"]
          ]
);
 
print "\nRESPONSE -- \n" . $req->as_string;
 
# Check the outcome of the response
if ($req->is_success) {
    print $req->content;
}
else {
  print "\n in else not success\n";
}

__END__
