@echo off
setlocal enabledelayedexpansion
set my_orig_dir=%~dp0
set my_dir=\\%computername%\xena_work$\
set logFile=%my_dir%logZip.txt
cd C:\Windows\system32
net use X:  /DELETE /Y  > nul 2>&1
ping 127.0.0.1 -n 1  > nul 2>&1
net use X: \\%computername%\xena_work$ euriskom /USER:esp\xreport /P:yes
ping 127.0.0.1 -n 1  > nul 2>&1
net use X: \\%computername%\xena_work$ /P:yes
X:
@SET XREPORT_HOME=\\%computername%\xreport_home$
@ECHO START EXECUTION > %logFile%
FOR %%G IN (A1 A3 A4 A7 C7 SH C4 MA BA Z1 A1TEST) DO (
perl.exe "%XREPORT_HOME%\bin\zipAllFilesInFolder.pl" -PATHIN X:\%%G\Logs -XE LOG -XE txt -XE swp -XE swo -dodelete 2>&1 >> %logFile%
)
::organize in folders
FOR %%Z IN (A1 A3 A4 A7 C7 SH C4 MA BA Z1 A1TEST) DO (
@for /F "tokens=1 delims=:" %%f in ('dir X:\%%Z\Logs\*.zip /b') do (
@set newfile=%%f
@set newfile_with_single_delimiter=!newfile:.log=:!  
@for /F "tokens=1,2 delims=:" %%a in ('echo !newfile_with_single_delimiter!') do (
@set newdir=X:\%%Z\Logs\%%a 
@if not exist "!newdir!" (  
@mkdir "!newdir!" > nul
)
@if exist "!newdir!" ( 
@move "X:\%%Z\Logs\!newfile!" "!newdir!"  > nul 
) 
)
)
)
net use X:  /DELETE /Y > nul  2>&1  
@ENDLOCAL