Setlocal EnableDelayedExpansion
echo off
set my_datetime=%date:~6,4%-%date:~3,2%-%date:~0,2%_%time:~0,2%%time:~3,2%
echo %my_datetime%
::cd D:\xena\xreport_home\bin
@set BIN_PATH="\\localhost\D$\xena\xreport_home\bin"
SET FILELOG="\\localhost\xena_work$\LOGS\reloadSuggIndexElastic_%my_datetime%.LOG"
@echo.FILELOG=!FILELOG!
@SET XREPORT_HOME=\\C0ctlpw005\d$\xreport
::set LIST_OF_CTD=A1TEST A1 A3 A4 A7 SH C7 C4 MA BA Z1
::set LIST_OF_CTD=A1 A3 A4 A7 C7
set LIST_OF_CTD=A1 A4 A7 C7 C4 MA BA
::set LIST_OF_CTD=C7
::FOR %%G IN (%LIST_OF_CTD%) DO (
@SET PERL5LIB=!XREPORT_HOME!\perllib\5.16
@set cmd=perl -I D:\perl_modules\5.16\lib\perl5 !BIN_PATH!\fillindex_online.pl !LIST_OF_CTD!

for %%n in (5) do (
@echo.%cmd% >  %FILELOG%
%cmd% >>  %FILELOG% 2>&1
)
@endlocal
::@GOTO :EOF


