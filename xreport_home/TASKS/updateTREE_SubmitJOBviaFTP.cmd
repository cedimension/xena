::updateTREEviaFTP.cmd
Setlocal EnableDelayedExpansion
echo off
::D:\Xena\xreport_home\TASKS\updateTREE_SubmitJOBviaFTP.cmd USER=XREPORT PASS=FGQ61L TSO=it6a.intranet.unicredit.it DEST=XENABA MYDIR=C:\temp DATASET=BA.BMC.CTD.PARM MEMBER=CTDTREE
::D:\Xena\xreport_home\TASKS\updateTREE_SubmitJOBviaFTP.cmd USER=XREPORT PASS=FGQ61L TSO=it6a.intranet.unicredit.it DEST=XENAMA MYDIR=C:\temp DATASET=BA.BMC.CTD.PARM MEMBER=CTDTREE
::D:\Xena\xreport_home\TASKS\updateTREE_SubmitJOBviaFTP.cmd USER=XREPORT PASS=FGQ61L TSO=it6d.intranet.unicredit.it DEST=XENAMA MYDIR=C:\temp DATASET=BA.BMC.CTD.PARM MEMBER=CTDTREE



:CHECKPRM

@IF [%1] == [] (

 @echo.USER=%USER%
 @echo.TSO=%TSO%
 @echo.DEST=%DEST%
 @echo.DATASET=%DATASET%
 @echo.MEMBER=%MEMBER%
 @echo.MYDIR=%MYDIR%


 @IF NOT DEFINED TSO (
  @echo error "TSO" parameter has be specified. 
  @GOTO :EOF
 )
 @IF NOT DEFINED DEST (
  @echo error "DEST" parameter has be specified. 
  @GOTO :EOF
 )
 @IF NOT DEFINED DATASET (
  @echo error "DATASET" parameter has be specified. 
  @GOTO :EOF
 )
 @IF NOT DEFINED MEMBER (
  @echo error "MEMBER" parameter has be specified. 
  @GOTO :EOF
 )
 @IF NOT DEFINED MYDIR (
  @echo error "MYDIR" parameter has be specified. 
  @GOTO :EOF
 )
 @IF NOT DEFINED USER (
  @echo error "USER" parameter has be specified. 
  @GOTO :EOF
 )
 @IF NOT DEFINED PASS (
  @echo error "PASS" parameter has be specified. 
  @GOTO :EOF
 )
 
 @IF NOT EXIST "%MYDIR%" mkdir "%MYDIR%"
 @GOTO :DOCMD
 @GOTO :EOF
)

@SET _parm=%1
@SET _value=%2
@SHIFT
@SHIFT

@IF /I [%_parm%] == [USER] (
@SET USER=%_value%
@GOTO :LOOP
)
@IF /I [%_parm%] == [PASS] (
@SET PASS=%_value%
@GOTO :LOOP
)
@IF /I [%_parm%] == [TSO] (
@SET TSO=%_value%
@GOTO :LOOP
)
@IF /I [%_parm%] == [DEST] (
@SET DEST=%_value%
@GOTO :LOOP
)
@IF /I [%_parm%] == [DATASET] (
@SET DATASET=%_value%
@GOTO :LOOP
)
@IF /I [%_parm%] == [MEMBER] (
@SET MEMBER=%_value%
@GOTO :LOOP
)
@IF /I [%_parm%] == [MYDIR] (
@SET MYDIR=%_value%
@GOTO :LOOP
)

@ECHO.ERROR! Parameter [%_parm%] not managed.
@GOTO :EOF

:LOOP

@GOTO :CHECKPRM
@ENDLOCAL
@GOTO :EOF

:DOCMD

set jobfile=%MYDIR%\%USER%X
set file_ftpscript=%MYDIR%\ftpscript.txt
del "%file_ftpscript%" 2>nul
del "%jobfile%" 2>nul
del "%MYDIR%\log.txt" 2>nul
echo.//%USER%X JOB ^(US09A0^)^,CTD^,MSGCLASS^=G^,CLASS^=L^,NOTIFY^=^&SYSUID>> "%jobfile%"
echo.//^*--------------------------------------------------------------------^*>> "%jobfile%"
echo.//^* STAMPA PER CTL-D>> "%jobfile%"
echo.//^*--------------------------------------------------------------------^*>> "%jobfile%"
echo.//CTLD01   EXEC PGM^=ICEGENER>> "%jobfile%"
echo.//SYSPRINT DD SYSOUT^=^*>> "%jobfile%"
echo.//SYSIN    DD DUMMY>> "%jobfile%"
echo.//SYSUT1   DD DSN^=%DATASET%^(%MEMBER%^)^,DISP^=SHR>> "%jobfile%"
echo.//SYSUT2   DD SYSOUT^=^(C^,XRRENAME^)^,DEST^=%DEST%>> "%jobfile%"
echo.//^*>> "%jobfile%"
::)
echo.open %TSO%>> "%file_ftpscript%"
echo.%USER%>> "%file_ftpscript%"
echo.%PASS%>> "%file_ftpscript%"
echo.ASCII>> "%file_ftpscript%"
echo.QUOTE SITE FILEtype=JES>> "%file_ftpscript%"
echo.PUT "%jobfile%">> "%file_ftpscript%"
echo.close>> "%file_ftpscript%"
echo.quit>> "%file_ftpscript%"
echo.bye>> "%file_ftpscript%"
::echo.FTP -v -i -s:%file_ftpscript%
FTP -i -s:%file_ftpscript%
@ENDLOCAL
@GOTO :EOF