Setlocal EnableDelayedExpansion
echo off
set my_datetime=%date:~6,4%-%date:~3,2%-%date:~0,2%_%time:~0,2%%time:~3,2%
echo %my_datetime%
cd C:\Windows\system32
::debug
set sccommand=echo.SC
set sccommand=SC
SET LIST_OF_NSRV=3
SET LIST_OF_NSRV=1 2 3 4 5 6 7 8
::FOR ALL TYPES SET TYPE=
SET TYPE=
::SET TYPE=PDF
::SET TYPE=GTW
::SET TYPE=MIG
::SET TYPE=INI
::SET TYPE=
::SET TYPE=FTP
SET FILELOG="D:\disableServicesXN_ALL_ENV_%TYPE%_%my_datetime%.LOG"
SET FILELOGTORESTART="D:\disableServicesXN_ALL_ENV_to_restart_%TYPE%_%my_datetime%.LOG"
echo.echo off >>%FILELOGTORESTART%
::set LIST_OF_CTD=A1TEST A1 A3 A4 A7 SH C7 C4 MA BA Z1 H2
::set LIST_OF_CTD=A1TEST A1 A3 A4 A7 SH C7 C4 MA BA Z1 H2
set LIST_OF_CTD=H2
FOR %%G IN (%LIST_OF_CTD%) DO (
SET ENV=%%G
FOR %%n IN (%LIST_OF_NSRV%) DO (
::for /l %%n in (1,1,8) do (
for /f "tokens=2,*" %%a in ('sc \\C0CTLPW00%%n query state^= all ^| findstr.exe /i /l /c:"SERVICE_NAME: XN_!ENV!_%TYPE%"') do (
echo.SC \\C0CTLPW00%%n config %%a start= disabled
echo.SC \\C0CTLPW00%%n config %%a start= disabled >>%FILELOG%
%sccommand% \\C0CTLPW00%%n config %%a start= disabled >>%FILELOG% 2>&1
echo.SC \\C0CTLPW00%%n STOP %%a
echo.SC \\C0CTLPW00%%n STOP %%a >>%FILELOG%
%sccommand% \\C0CTLPW00%%n STOP %%a >>%FILELOG% 2>&1
echo.sc \\C0CTLPW00%%n config %%a start= delayed-auto >>%FILELOGTORESTART%
echo.SC \\C0CTLPW00%%n START %%a >>%FILELOGTORESTART%
)
)
)
echo.pause >>%FILELOGTORESTART%
pause
@GOTO :EOF
::sc config <service_name> start= disable
::for /l %%n in (1,1,8) do (
::for /f "tokens=2,*" %%a in ('sc \\C0CTLPW00%%n query state^= all ^| findstr.exe /i /l /c:"SERVICE_NAME: XN" ^| findstr.exe /i /l /c:"_MIG"') do (
::ECHO TRYING TO STOP %%a
::SC \\C0CTLPW00%%n STOP %%a
::)
::)
::SC \\C0CTLPW003 STOP XN_A1_PDF303
::SC \\C0CTLPW003 config XN_A1_PDF303 start= disabled
::SC \\C0CTLPW003 config XN_A1_PDF303 start= delayed-auto
::SC \\C0CTLPW003 START XN_A1_PDF303
::
::
::sc \\C0CTLPW001 config XN_A1_MIG101 start= delayed-auto
::SC \\C0CTLPW001 STOP XN_A1_MIG101
::sc \\C0CTLPW002 config XN_A1_MIG201 start= delayed-auto
::SC \\C0CTLPW002 STOP XN_A1_MIG201
::sc \\C0CTLPW003 config XN_A1_MIG301 start= delayed-auto
::SC \\C0CTLPW003 STOP XN_A1_MIG301
::sc \\C0CTLPW004 config XN_A1_MIG401 start= delayed-auto
::SC \\C0CTLPW004 STOP XN_A1_MIG401
::sc \\C0CTLPW005 config XN_A1_MIG501 start= delayed-auto
::SC \\C0CTLPW005 STOP XN_A1_MIG501
::sc \\C0CTLPW006 config XN_A1_MIG601 start= delayed-auto
::SC \\C0CTLPW006 STOP XN_A1_MIG601
::sc \\C0CTLPW007 config XN_A1_MIG701 start= delayed-auto
::SC \\C0CTLPW007 STOP XN_A1_MIG701
::sc \\C0CTLPW008 config XN_A1_MIG801 start= delayed-auto
::SC \\C0CTLPW008 STOP XN_A1_MIG801
::sc \\C0CTLPW001 config XN_A7_MIG101 start= delayed-auto
::SC \\C0CTLPW001 STOP XN_A7_MIG101
::sc \\C0CTLPW002 config XN_A7_MIG201 start= delayed-auto
::SC \\C0CTLPW002 STOP XN_A7_MIG201
::sc \\C0CTLPW003 config XN_A7_MIG301 start= delayed-auto
::SC \\C0CTLPW003 STOP XN_A7_MIG301
::sc \\C0CTLPW004 config XN_A7_MIG401 start= delayed-auto
::SC \\C0CTLPW004 STOP XN_A7_MIG401


