@echo off
setlocal enabledelayedexpansion
set my_dir=%~dp0
set filelog=%my_dir%log_MIG.txt
echo.START > %filelog% 2>&1
cd C:\Windows\system32
for /l %%n in (1,1,8) do (
for /f "tokens=2,*" %%a in ('sc \\C0CTLPW00%%n query ^| findstr /i /l /c:"SERVICE_NAME: XN" ^| findstr /i /l /c:"MIG"') do (
	echo.%date%_%time% - %computername% -TRYING TO STOP >> %filelog% 2>&1
	SC \\C0CTLPW00%%n STOP %%a >> %filelog% 2>&1
)
)
pause
@GOTO :EOF

::@call :checkService %nameservice% isActiveService
:checkService
for /f "tokens=2,*" %%a in ('sc %2 query ^| findstr /i /l /c:"%1"') do (
	set %3=Y
	GOTO :EOF
)
set %3=N
@GOTO :EOF