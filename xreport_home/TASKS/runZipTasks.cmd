@echo off
setlocal enabledelayedexpansion
set my_dir=%~dp0
set USR=xreport
set PWD=euriskom
cd C:\Windows\system32
echo.PROCESSING \\%computername%
SCHTASKS /Run /TN "XN_XENA_zipXENALogs" 
ping 127.0.0.1 -n 1 > nul
schtasks  /Query | findstr /i "xn"
for /l %%n in (1,1,8) do (
@IF /I NOT [\\%computername%]==[\\C0CTLPW00%%n] (
echo.PROCESSING \\C0CTLPW00%%n
SCHTASKS /Run /S \\C0CTLPW00%%n /U %USR% /P %PWD% /TN "XN_XENA_zipXENALogs"
ping 127.0.0.1 -n 1 > nul
schtasks  /Query /S \\C0CTLPW00%%n | findstr /i "xn"
)
)
pause
@GOTO :EOF