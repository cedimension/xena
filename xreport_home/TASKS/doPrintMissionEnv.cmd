@SETLOCAL

@SET bundle=BUNDLE2XR
@SET osuser=XXXXXXXX
@SET ospswd=XXXXXXXX
@SET wrk=d:\xena\xena_work\BUNDLES
::@SET wrk=\\C0CTLPW003\D$\OM\work
@SET XREPORT_HOME=d:\xena\xreport_home
@SET XREPORT_SITECONF=\\usnas710.intranet.unicredit.it\usxena$\xena\xreport_siteconf
@SET XRPERL=d:\strawberry\perl\bin\perl.exe

@IF NOT EXIST "%wrk%" mkdir "%wrk%"
@IF NOT EXIST "%XRPERL%" SET XRPERL=d:\strawberry\5.16\perl\bin\perl.exe

:CHECKPRM

@IF [%1] == [] (
 @IF NOT DEFINED pmiss (
  @echo Bundle must be specified with "name" parameter
  @GOTO :EOF
 )
 @IF [%osqual%] == [] (
  @CALL :DOCMD %pmiss% %bundle% %osuser% %osuser% %ospswd% %env%
 ) ELSE (
  @CALL :DOCMD %pmiss% %bundle% %osuser% %osqual% %ospswd% %env%
 )

 @GOTO :EOF
)

@SET _parm=%1
@SET _value=%2
@SHIFT
@SHIFT

@IF /I [%_parm%] == [name] (
@SET pmiss=%_value%
@GOTO :LOOP
)

@IF /I [%_parm%] == [type] (
@IF /I [%_value%] == [XML] ( @SET bundle=BUNDLE2ZOS ) ELSE @SET bundle=BUNDLE2XR
@IF /I [%_value%] == [NOXML] ( @SET bundle=BUNDLE2XREPO )
@GOTO :LOOP
)

@IF /I [%_parm%] == [xmlname] (
@SET bundle=%_value%
@GOTO :LOOP
)

@IF /I [%_parm%] == [osuser] (
@SET osuser=%_value%
@GOTO :LOOP
)

@IF /I [%_parm%] == [ospswd] (
@SET ospswd=%_value%
@GOTO :LOOP
)

@IF /I [%_parm%] == [cat] (
@SET cat=-cat %_value%
@GOTO :LOOP
)
@IF /I [%_parm%] == [skel] (
@SET skel=-skel %_value%
@GOTO :LOOP
)
@IF /I [%_parm%] == [osqual] (
@SET osqual=%_value%
@GOTO :LOOP
)

@IF /I [%_parm%] == [env] (
@SET env=%_value%
@GOTO :LOOP
)

@IF /I [%_parm%] == [HOME] (
@SET XREPORT_HOME=%_value%
@GOTO :LOOP
)

@ECHO.WARNING! Parameter [%_parm%] not managed.

:LOOP

@GOTO :CHECKPRM
@ENDLOCAL

@GOTO :EOF

:DOCMD
@IF NOT EXIST "%wrk%\%env%" mkdir "%wrk%\%env%"
@IF NOT EXIST "%wrk%\%env%\%pmiss%" mkdir "%wrk%\%env%\%pmiss%"
@SET COMMAND=%XRPERL% -I %XREPORT_HOME%\perllib %XREPORT_HOME%\sbin\xrAssemblyBundles.pl ^
-wrk %wrk%\%env%\%pmiss%  ^
-N %bundle% ^
-bundle %pmiss% ^
%cat% ^
%skel% ^
-dest FTP://%osuser%:%ospswd%@it0k.intranet.unicredit.it:%osqual% ^
-HOME %XREPORT_HOME% -SITECONF %XREPORT_SITECONF%\%env%
@ECHO.%COMMAND%
@%COMMAND%

@GOTO :EOF
