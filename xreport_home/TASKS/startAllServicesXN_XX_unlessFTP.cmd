@echo off
setlocal enabledelayedexpansion
set my_dir=%~dp0
set ENVTOEXCLUDE=A1TEST
set ENVTOEXCLUDE2=MA 
set ENVTOEXCLUDE3=BA 
set ENVTOEXCLUDE4=Z1 
set ENVTOEXCLUDE5=A1TEST


@set env_to_excplude=
@set env_to_excplude=
@set env_to_excplude=
@set env_to_excplude=


set filelog=%my_dir%log_START_ALL-XN_SERVICES.txt
echo.START - %date%_%time% - %computername% > %filelog% 2>&1
cd C:\Windows\system32
for /f "tokens=2,*" %%a in ('sc query state^= all ^| findstr /i /l /c:"SERVICE_NAME: XN" ^| findstr /i /l /v /c:"FTP"  /c:"_%ENVTOEXCLUDE1%_" /c:"_%ENVTOEXCLUDE2%_" /c:"_%ENVTOEXCLUDE3%_" /c:"_%ENVTOEXCLUDE4%_" /c:"_%ENVTOEXCLUDE5%_" ') do (
@call :checkLocalService %%a isActiveService
@IF /I [!isActiveService!]==[N] (
echo.%date%_%time% - %computername% -TRYING TO START %%a in %computername% >> %filelog% 2>&1
SC START %%a >> %filelog% 2>&1
) ELSE (echo.%%a already started in %computername% >> %filelog% 2>&1)
)
for /l %%n in (1,1,8) do (
IF /I not "%COMPUTERNAME%"=="C0CTLPW00%%n" (
for /f "tokens=2,*" %%a in ('sc \\C0CTLPW00%%n query state^= all ^| findstr /i /l /c:"SERVICE_NAME: XN" ^| findstr /i /l /v /c:"FTP"  /c:"_%ENVTOEXCLUDE1%_" /c:"_%ENVTOEXCLUDE2%_" /c:"_%ENVTOEXCLUDE3%_" /c:"_%ENVTOEXCLUDE4%_" /c:"_%ENVTOEXCLUDE5%_" ') do (
@call :checkService %%a \\C0CTLPW00%%n isActiveService
@IF /I [!isActiveService!]==[N] (
echo.%date%_%time% - %computername% -TRYING TO START %%a in \\C0CTLPW00%%n >> %filelog% 2>&1
SC \\C0CTLPW00%%n START %%a >> %filelog% 2>&1
) ELSE (echo.%%a already started in \\C0CTLPW00%%n >> %filelog% 2>&1)
)
)
)
echo.END - %date%_%time% - %computername% >> %filelog% 2>&1
exit 0
@GOTO :EOF
::@call :checkService %nameservice% isActiveService
:checkService
for /f "tokens=2,*" %%a in ('sc %2 query ^| findstr /i /l /c:"%1"') do (
	set %3=Y
	GOTO :EOF
)
set %3=N
@GOTO :EOF
::@call :checkService %nameservice% isActiveService
:checkLocalService
for /f "tokens=2,*" %%a in ('sc query ^| findstr /i /l /c:"%1"') do (
	set %2=Y
	GOTO :EOF
)
set %2=N
@GOTO :EOF