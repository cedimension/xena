@echo off
@setlocal EnableDelayedExpansion enableextensions
@set my_datetime=%date:~6,4%-%date:~3,2%-%date:~0,2%_%time:~0,2%%time:~3,2%
@set my_datetime=%my_datetime: =0%
@echo.%my_datetime%
@set DEBUG=N
@set my_dir=\\%computername%\xena_work$\LOGS\
@IF NOT EXIST "%my_dir%" mkdir "%my_dir%"
::set my_dir=%~dp0
@set listOfParameters=%*
@set logFile=%my_dir%logRestart.txt
@echo.logFile=!logFile!
@2>nul (>>!logFile! echo off) && (set locked=N) || (set locked=Y)
@IF /I [!locked!]==[Y] (
@set logFile=%my_dir%logRestart_%my_datetime%.txt
)
@IF NOT defined listOfParameters (
	@echo.error - no parameter found >> !logFile!
	@echo.error - no parameter found
	::exit
	goto :EOF
)
@IF /I [!DEBUG!]==[Y] (
	@echo.listOfParameters=%listOfParameters% >> !logFile!
	@echo.listOfParameters=%listOfParameters%
)
goto :checkParameters
:aftercheckParameters
:startService
cd C:\Windows\system32
::ping 127.0.0.1 -n 11 > nul
::exceptions
::@set env_to_excplude=C4
::@if /I not "x!serviceName:_%env_to_excplude%_=!"=="x!serviceName!" goto :EOF
::@set env_to_excplude=C7
::@if /I not "x!serviceName:_%env_to_excplude%_=!"=="x!serviceName!" goto :EOF
@set env_to_excplude=MA
@if /I not "x!serviceName:_%env_to_excplude%_=!"=="x!serviceName!" goto :EOF
@set env_to_excplude=BA
@if /I not "x!serviceName:_%env_to_excplude%_=!"=="x!serviceName!" goto :EOF
@set env_to_excplude=Z1
@if /I not "x!serviceName:_%env_to_excplude%_=!"=="x!serviceName!" goto :EOF
@set env_to_excplude=A1TEST
@if /I not "x!serviceName:_%env_to_excplude%_=!"=="x!serviceName!" goto :EOF
::@IF /I [!serviceName!]==[XN_C7_PDF403] goto :EOF
::@IF /I [!serviceName!]==[XN_C7_PDF404] goto :EOF
@IF /I [!DEBUG!]==[Y] (
	@echo.after exclusion test >> !logFile!
	@echo.after exclusion test
)
@call :checkService !serviceName! \\%computername% isActiveService
@IF /I [!isActiveService!]==[N] (
	echo.%date%_%time% - %computername% -TRYING TO START !serviceName! - eventRecordID[!eventRecordID!] >> !logFile!
	echo.%date%_%time% - %computername% -TRYING TO START !serviceName! - eventRecordID[!eventRecordID!]
	SC \\%computername% START !serviceName! | findstr /i /l /c:"STAT" /c:"running" >> !logFile!
	SC \\%computername% queryex !serviceName! | findstr /i /l /c:"PID" >> !logFile!
) ELSE (
@IF /I [!DEBUG!]==[Y] (
	@echo.The service \\%computername%\!serviceName! is already active - No action is needed >> !logFile!
	@echo.The service \\%computername%\!serviceName! is already active - No action is needed 
)
)
::exit
goto :EOF
::@call :checkService %nameservice% isActiveService
:checkService
for /f "tokens=2,*" %%a in ('sc %2 query ^| findstr /i /l /c:"%1"') do (
	set %3=Y
	GOTO :EOF
)
set %3=N
::exit
goto :EOF

:checkParameters
@set eventChannel=XXX
@set eventRecordID=XXX
@set eventSeverity=XXX
@set statusEvent=XXX
@set serviceName=XXX
@set filterXena3chars=XN_
@set filterServiceType=ALL
@set t=!listOfParameters!
@set t=!t:"=!
:checkParametersAfterInit
@IF /I [!DEBUG!]==[Y] (
	@echo.checkParametersAfterInit t=!t!  >> !logFile!
	@echo.checkParametersAfterInit t=!t!
)
::@echo.t=!t!
for /f "tokens=1,2,*" %%f in ("!t!") do (
	set t=%%g %%h
	@IF /I "%%f"=="-eventChannel" (
		@set eventChannel=%%g
		@set t=%%h
	)
	@IF /I "%%f"=="-eventRecordID" (
		@set eventRecordID=%%g
		@set t=%%h
	)
	@IF /I "%%f"=="-eventSeverity" (
		@set eventSeverity=%%g
		@set t=%%h
	)
	@IF /I "%%f"=="-statusEvent" (
		@set statusEvent=%%g
		@set t=%%h
	)
	@IF /I "%%f"=="-filterServiceType" (
		@set filterServiceType=%%g
		@set t=%%h
	)
	@IF /I "%%f"=="-filterXena3chars" (
		@set filterXena3chars=%%g
		@set t=%%h
	)
	@IF /I "%%f"=="-serviceName" (
		@set serviceName=%%g
		@set t=%%h
	)
	@set t=!t:  = !
)
@IF /I NOT "!t!"==" " goto :checkParametersAfterInit
for /f "tokens=2,* delims=()" %%a in ('echo.%listOfParameters%') do (
	@set serviceName=%%a
)
@set logFile=%my_dir%logRestart!filterXena3chars!!filterServiceType!.txt
@2>nul (>>!logFile! echo off) && (set locked=N) || (set locked=Y)
@IF /I [!locked!]==[Y] (
@set logFile=%my_dir%logRestart!filterXena3chars!!filterServiceType!_%my_datetime%.txt
@echo.logFile=!logFile!
)
@IF /I [!DEBUG!]==[Y] (
	@echo.statusEvent=!statusEvent!
	@echo.statusEvent=!statusEvent! >> !logFile!
	@echo.eventChannel=!eventChannel!
	@echo.eventChannel=!eventChannel! >> !logFile!
	@echo.eventSeverity=!eventSeverity!
	@echo.eventSeverity=!eventSeverity! >> !logFile!
	@echo.eventRecordID=!eventRecordID!
	@echo.eventRecordID=!eventRecordID! >> !logFile!
	@echo.filterServiceType=!filterServiceType!
	@echo.filterServiceType=!filterServiceType! >> !logFile!
	@echo.filterXena3chars=!filterXena3chars!
	@echo.filterXena3chars=!filterXena3chars! >> !logFile!
	@echo.serviceName=!serviceName!
	@echo.serviceName=!serviceName! >> !logFile!
)
::check if the listOfParameters contains the string stopped
@IF /I NOT [!statusEvent!]==[stopped] (
	@IF /I [!DEBUG!]==[Y] (
		@echo.listOfParameters does not contain the string 'stopped':[%listOfParameters%]
		@echo.listOfParameters does not contain the string 'stopped':[%listOfParameters%] >> !logFile!
	)
	::exit
	goto :EOF
)
::check if the service name begins with the string filterXena3chars (es. XN_ )
@set prefixServiceName=%serviceName:~0,3%
@IF /I NOT "!prefixServiceName!"=="!filterXena3chars!" (
	@IF /I [!DEBUG!]==[Y] (
		@echo.[!prefixServiceName!] is not !filterXena3chars!
		@echo.[!prefixServiceName!] is not !filterXena3chars! >> !logFile!
	)
	::exit
	goto :EOF
)
::check if the service name contains the string filterServiceType (INI GTW MIG PDF)
::use the value ALL to skip this check
@IF /I NOT [!filterServiceType!]==[ALL] (
	set serviceNameWithoutString=!serviceName:%filterServiceType%=!
	@IF /I [!serviceNameWithoutString!]==[!serviceName!] (
		@IF /I [!DEBUG!]==[Y] (
			@echo.!serviceName! does not contain the string %filterServiceType%
			@echo.!serviceName! does not contain the string %filterServiceType% >> !logFile!
		)
		::exit
		goto :EOF
	)
)
::check if the service name contains the string A1TEST
@set ENVTOEXCLUDE=A1TEST
set serviceNameWithoutString=!serviceName:%ENVTOEXCLUDE%=!
@IF /I NOT [!serviceNameWithoutString!]==[!serviceName!] (
	@IF /I [!DEBUG!]==[Y] (
		@echo.!serviceName! contains the string %ENVTOEXCLUDE%
		@echo.!serviceName! contains the string %ENVTOEXCLUDE% >> !logFile!
	)
	::exit
	goto :EOF
)
goto :aftercheckParameters