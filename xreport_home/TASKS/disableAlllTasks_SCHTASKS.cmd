@echo off
setlocal enabledelayedexpansion
@set my_datetime=%date:~6,4%-%date:~3,2%-%date:~0,2%_%time:~0,2%%time:~3,2%
@set my_datetime=%my_datetime: =0%
@echo.%my_datetime%
@set filelog=%~dp0..\disableAlllTasks_%my_datetime%_%computername%.txt
set USR=xreport
set PWD=euriskom
cd C:\Windows\system32
echo.PROCESSING \\%computername%
echo.PROCESSING \\%computername% >>  %filelog%
for /f %%a in ('SCHTASKS  /Query ^| findstr /i "xn"') do (
SCHTASKS /Change /RU %USR% /RP %PWD% /TN "%%a" /DISABLE >>  %filelog%
)
SCHTASKS  /Query | findstr /i "xn" >>  %filelog%
for /l %%n in (1,1,8) do (
@IF /I NOT [\\%computername%]==[\\C0CTLPW00%%n] (
echo.PROCESSING \\C0CTLPW00%%n
echo.PROCESSING \\C0CTLPW00%%n >>  %filelog%
for /f %%a in ('SCHTASKS  /Query /S \\C0CTLPW00%%n ^| findstr /i "xn"') do (
SCHTASKS /Change /S \\C0CTLPW00%%n /RU %USR% /RP %PWD% /TN "%%a"  /DISABLE >>  %filelog%
)
schtasks  /Query /S \\C0CTLPW00%%n | findstr /i "xn" >>  %filelog%
)
)
pause
@GOTO :EOF