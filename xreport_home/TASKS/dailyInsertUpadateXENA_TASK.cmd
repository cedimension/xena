::\\C0ctlpw004.sd01.unicreditgroup.eu\d$\xena\xreport_home\TASKS\dailyInsertUpadateXENA_TASK.cmd -normalMode -dryrun
::\\C0ctlpw004.sd01.unicreditgroup.eu\d$\xena\xreport_home_A1TEST\TASKS\dailyInsertUpadateXENA_TASK.cmd -normalMode -dryrun
::D:\xena\xreport_home\TASKS\dailyInsertUpadateXENA_TASK.cmd -normalMode -dryrun
@setlocal enabledelayedexpansion
@echo off
@set my_datetime=%date:~6,4%-%date:~3,2%-%date:~0,2%_%time:~0,2%%time:~3,2%
@set my_datetime=%my_datetime: =0%
@echo.%my_datetime%
@SET sqlcommand=ECHO.sqlcmd
@SET enableMigration=N
@SET disableMigration=N
@SET normalMode=N
@SET dryrun=N
@SET LISTOFENVIRONMENTS=A1 A4 A7 C7 SH C4 A3
::@SET LISTOFENVIRONMENTS=A3
::@SET LISTOFENVIRONMENTS=C7
::@SET LISTOFENVIRONMENTS=A1 A3 A4 A7 C7 SH C4 MA BA Z1
IF /I NOT [%computername%]==[C0CTLPW004] (
@echo.exit -1
@exit -1
)
IF /I [%1]==[-enableMigration] (
@ECHO.SET ENABLE MIGRATION MODE
@SET enableMigration=Y
)
IF /I [%1]==[-disableMigration] (
@ECHO.SET DISABLE MIGRATION MODE
@SET disableMigration=Y
)
IF /I [%1]==[-normalMode] (
@ECHO.SET NORMAL MODE
@SET normalMode=Y
)
IF /I [%2]==[-dryrun] (
@SET dryrun=Y
)
IF /I [%2]==[-debug] (
@SET dryrun=Y
)
IF /I NOT [%dryrun%]==[Y] (
@SET sqlcommand=sqlcmd
) ELSE (
@ECHO.SET DRYRUN MODE - sqlcommand=!sqlcommand!
)
@set my_dir=\\%computername%\xena_work$\
@SET FILELOG="%my_dir%dailyInsertUpadateXENA.LOG"
@SET FILELOG_TEMP="%my_dir%dailyInsertUpadateXENA.LOG._ENV_.TEMP"
@echo.%FILELOG%
@echo.%my_datetime% > %FILELOG%
@FOR %%G IN (%LISTOFENVIRONMENTS%) DO (
@SET SQL=
@SET cmd=
@call :printLines
@echo.ENV=%%G
@echo.ENV=%%G   >> %FILELOG% 2>&1
@call :addtorre %%G
@SET cmd=!sqlcommand! -S c0ctlpwdbh.sd01.unicreditgroup.eu^,7667 -d XNCTD%%G -U xreport -P euriskom.1 -Q "!SQL!"
@echo.cmd=!cmd!   >> %FILELOG%
@SET FILELOG_TEMP_ENV=!FILELOG_TEMP:_ENV_=%%G!
@echo.FILELOG_TEMP_ENV=!FILELOG_TEMP_ENV!
@del !FILELOG_TEMP_ENV! /F 2>nul
@!cmd! >nul 2> !FILELOG_TEMP_ENV!
)
@FOR %%G IN (%LISTOFENVIRONMENTS%) DO (
@call :printLines
@echo.ENV=%%G   >> %FILELOG% 2>&1
@call :printHeader
@SET FILELOG_TEMP_ENV=!FILELOG_TEMP:_ENV_=%%G!
@echo.FILELOG_TEMP_ENV=!FILELOG_TEMP_ENV!
@for /F "tokens=* " %%a in ('type !FILELOG_TEMP_ENV! ^| findstr /i /l /v /c:"------" /c:"0 rows affected" /c:"ExternalTableName"') do (
@set str=%%a
@call :check
@echo.!str! >> %FILELOG% 2>&1
)
@del !FILELOG_TEMP_ENV! /F 2>nul
)
@ENDLOCAL
@GOTO :EOF
:addtorre
@SET TORRE=%1
@SHIFT
IF /I [%enableMigration%]==[Y] (
@SET SQL=!SQL! ^
SET QUOTED_IDENTIFIER ON ;^
UPDATE WorkQueue SET Priority = 15, Status = 16, WorkClass = 126, TypeOfWork = 888  from XNCTD%TORRE%.dbo.tbl_WorkQueue WorkQueue, XNCTD%TORRE%.dbo.tbl_JobReports jr where WorkQueue.ExternalKey = jr.JobReportId and jr.HoldDays ^>= 540 and WorkQueue.TypeOfWork = 888 and WorkQueue.WorkClass is null and jr.XferStartTime ^<= DATEADD^(month, -1, GETDATE^(^) ^) and jr.XferStartTime ^>= '2015-09-01'  and WorkQueue.WorkClass is NULL and WorkQueue.Status = 16 ;^
UPDATE WorkQueue SET Priority = 15, Status = 16, WorkClass = 126, TypeOfWork = 888  from XNCTD%TORRE%.dbo.tbl_WorkQueue WorkQueue, XNCTD%TORRE%.dbo.tbl_JobReports jr, XNCTD%TORRE%.dbo.tbl_JobReportNames jrn where WorkQueue.ExternalKey = jr.JobReportId and jr.JobReportName = jrn.JobReportName and jrn.HoldDays ^> jr.HoldDays  and jrn.HoldDays ^>= 540 and WorkQueue.TypeOfWork = 888 and WorkQueue.WorkClass is null and jr.XferStartTime ^<= DATEADD^(month, -1, GETDATE^(^) ^) and jr.XferStartTime ^>= '2015-09-01'  and WorkQueue.WorkClass is NULL and WorkQueue.Status = 16 ;^
UPDATE WorkQueue SET Priority = 15, Status = 16, WorkClass = 126, TypeOfWork = 666  from XNCTD%TORRE%.dbo.tbl_WorkQueue WorkQueue where WorkQueue.TypeOfWork = 666 and WorkQueue.WorkClass is null ;^
UPDATE WorkQueue SET Priority = 15, Status = 16, WorkClass = 126, TypeOfWork = 555  from XNCTD%TORRE%.dbo.tbl_WorkQueue WorkQueue where WorkQueue.TypeOfWork = 555 and WorkQueue.WorkClass is null ;^
delete WorkQueue from XNCTD%TORRE%.dbo.tbl_JobReports JobReports, XNCTD%TORRE%.dbo.tbl_WorkQueue WorkQueue where JobReports.jobreportid = WorkQueue.externalkey and WorkQueue.typeofwork = 555 and  JobReports.LocalPathId_OUT = 'PWYYY' ;
)
IF /I [%disableMigration%]==[Y] (
@SET SQL=!SQL! ^
SET QUOTED_IDENTIFIER ON ;^
UPDATE WorkQueue SET WorkClass = null from XNCTD%TORRE%.dbo.tbl_WorkQueue WorkQueue where WorkQueue.TypeOfWork = 888 and WorkQueue.WorkClass = 126 ;^
UPDATE WorkQueue SET WorkClass = null from XNCTD%TORRE%.dbo.tbl_WorkQueue WorkQueue where WorkQueue.TypeOfWork = 666 and WorkQueue.WorkClass = 126 ;^
UPDATE WorkQueue SET WorkClass = null from XNCTD%TORRE%.dbo.tbl_WorkQueue WorkQueue where WorkQueue.TypeOfWork = 555 and WorkQueue.WorkClass = 126 ;
)
::--UPDATE jr  PendingOp to 12   ;^
::--delete WorkClass is null   ;^
::--update HoldDays from tbl_JobReportNames   ;^
::--migrazione dell'input - dopo 7 giorni   ;^
::--migrazione dell'output - dopo 7 giorni - solo per holdays ^> 62   ;^
::--cancellazione dei report - dopo 30 giorni dalla scadenza   ;^
::--requeue  ;^
::--DELETE BUNDLESQ OLDER THAN 15 DAYS ;
IF /I [%normalMode%]==[Y] (
@SET SQL=!SQL! ^
SET QUOTED_IDENTIFIER ON ;^
UPDATE jr SET PendingOp = 12 from XNCTD%TORRE%.dbo.tbl_JobReports jr LEFT JOIN  XNCTD%TORRE%.dbo.tbl_JobReportNames jrn ON jr.JobReportName = jrn.JobReportName where jr.status = 18 and jr.XferStartTime is not null AND jr.PendingOp ^<^> 12 and  GETDATE^(^) ^> DATEADD^(day, jr.HoldDays, jr.XferStartTime ^) and ^( ^(jrn.JobReportName is null^) or ^(^(jrn.JobReportName is not null^) and ^(jr.JobReportName ^<^> 'CDAMFILE'^) and  ^(GETDATE^(^) ^> DATEADD^(day, jrn.HoldDays, jr.XferStartTime^) ^) ^) ^) ;^
delete wq from XNCTD%TORRE%.dbo.tbl_WorkQueue wq where 'MIGRATION' =  ExternalTableName and WorkClass is null    ;^
update jr set HoldDays = jrn.HoldDays from XNCTD%TORRE%.dbo.tbl_JobReports jr ,  XNCTD%TORRE%.dbo.tbl_JobReportNames jrn where jr.jobreportname = jrn.jobreportname and jrn.HoldDays  ^> jr.HoldDays and status ^> 17 and XferStartTime is not null     ;^
insert into XNCTD%TORRE%.dbo.tbl_WorkQueue ^(ExternalTableName,	ExternalKey,	TypeOfWork,	InsertTime,	WorkClass,	Priority,	Status,	SrvParameters^) select 'MIGRATION' as ExternalTableName, jobreportid as ExternalKey, 666 as TypeOfWork, dateadd^(day, 7, XferStartTime^) as InsertTime, 126 as WorkClass, 15 as Priority, 16 as Status, 'C3' as SrvParameters from XNCTD%TORRE%.dbo.tbl_JobReports jr where   JobReportName not in ^('ST01','XRRENAME','XAFPRSCE','W3SVCLOG','TEST','TESTSTREAM'^) and status ^> 17 and XferStartTime is not null and LocalFileName not like '.default%' and LocalPathId_IN like 'PW00%' and not exists ^(select 1 from XNCTD%TORRE%.dbo.tbl_WorkQueue wq where wq.externalkey = jr.jobreportid and wq.typeofwork = 666^) and dateadd^(day, 7, XferStartTime^)  ^<  getdate^(^)    ;^
insert into XNCTD%TORRE%.dbo.tbl_WorkQueue ^(ExternalTableName,	ExternalKey,	TypeOfWork,	InsertTime,	WorkClass,	Priority,	Status,	SrvParameters^) select 'MIGRATION' as ExternalTableName, jobreportid as ExternalKey, 888 as TypeOfWork, dateadd^(day, 7, XferStartTime^) as InsertTime, 126 as WorkClass, 10 as Priority, 16 as Status, 'C3' as SrvParameters from XNCTD%TORRE%.dbo.tbl_JobReports jr where    JobReportName not in ^('CDAMFILE','ST01','XRRENAME','XAFPRSCE','W3SVCLOG','TEST','TESTSTREAM'^) and status = 18 and XferStartTime is not null and LocalFileName not like '.default%' and LocalPathId_OUT like 'PW00%' and HoldDays ^> 62 and not exists ^(select 1 from XNCTD%TORRE%.dbo.tbl_WorkQueue wq where wq.externalkey = jr.jobreportid and wq.typeofwork = 888^) and dateadd^(day, 7, XferStartTime^)  ^<  getdate^(^)     ;^
insert into XNCTD%TORRE%.dbo.tbl_WorkQueue ^(ExternalTableName,	ExternalKey,	TypeOfWork,	InsertTime,	WorkClass,	Priority,	Status,	SrvParameters^) select   'MIGRATION' as ExternalTableName, jobreportid as ExternalKey, 999 as TypeOfWork, dateadd^(day, 30^+HoldDays, XferStartTime^) as InsertTime, 748 as WorkClass, 5 as Priority, 16 as Status, 'C3' as SrvParameters from XNCTD%TORRE%.dbo.tbl_JobReports jr where    JobReportName not in ^('CDAMFILE','ST01','XRRENAME','XAFPRSCE','W3SVCLOG','TEST','TESTSTREAM'^) and status ^> 17 and XferStartTime is not null and not exists ^(select 1 from XNCTD%TORRE%.dbo.tbl_WorkQueue wq where wq.externalkey = jr.jobreportid and wq.typeofwork = 999^) and dateadd^(day, 30^+HoldDays, XferStartTime^) ^< getdate^(^) and PendingOp = 12     ;^
UPDATE WorkQueue SET Priority = coalesce^(^(WorkQueue.Priority ^+1^),10^) ,Status = 16 ,SrvName = NULL from XNCTD%TORRE%.dbo.tbl_WorkQueue WorkQueue, XNCTD%TORRE%.dbo.tbl_JobReports jr where WorkQueue.ExternalKey = jr.JobReportId and WorkQueue.Status in ^(31^) and  ^(^(WorkQueue.Priority is NULL^) or ^(WorkQueue.Priority ^< 31^)^) and WorkQueue.TypeOfWork in ^(666,777,1,555,888,999^) ;^
delete wq from XNCTD%TORRE%.dbo.tbl_WorkQueue wq where not exists ^(select 1 FROM XNCTD%TORRE%.dbo.tbl_JobReports where jobreportid = wq.externalkey^) ;^
DELETE BUNDLESQ FROM XNCTD%TORRE%.dbo.TBL_BUNDLESQ BUNDLESQ WHERE DATEDIFF^(DAY, INSERTTIMEREF, GETDATE^(^)^) ^> 15 ;
IF /I [%TORRE%]==[C7] (
@SET SQL=!SQL! ^
UPDATE JR SET HoldDays = 180   FROM XNCTD%TORRE%.dbo.tbl_JobReports JR where HoldDays ^> 180     ;^
delete wq from XNCTD%TORRE%.dbo.tbl_WorkQueue wq where 'MIGRATION' =  ExternalTableName and TypeOfWork in ^(666,555,888^) ;^
UPDATE JRN SET HoldDays = 180  FROM XNCTD%TORRE%.dbo.tbl_JobReportNames JRN where HoldDays ^> 180 ;
)
IF /I [%TORRE%]==[C4] (
@SET SQL=!SQL! ^
UPDATE JR SET HoldDays = 180   FROM XNCTD%TORRE%.dbo.tbl_JobReports JR where HoldDays ^> 180     ;^
delete wq from XNCTD%TORRE%.dbo.tbl_WorkQueue wq where 'MIGRATION' =  ExternalTableName and TypeOfWork in ^(666,555,888^) ;^
UPDATE JRN SET HoldDays = 180  FROM XNCTD%TORRE%.dbo.tbl_JobReportNames JRN where HoldDays ^> 180 ;
)
)
@GOTO :EOF
:check
if "!str:~-5!" == "     " (
set str=!str:~0,-5!
goto check
)
@GOTO :EOF
:printLines
@echo.------------------------------------------------------------------------------   >> %FILELOG%
@GOTO :EOF
:printHeader
@echo.ExternalTableName                                                ExternalKey TypeOfWork WorkId               InsertTime              WorkClass   Priority Status SrvName                          SrvParameters>> %FILELOG%
@GOTO :EOF