@setlocal enabledelayedexpansion
@echo off
@set my_datetime=%date:~6,4%-%date:~3,2%-%date:~0,2%_%time:~0,2%%time:~3,2%
@set my_datetime=%my_datetime: =0%
@echo.%my_datetime%
IF /I NOT [%computername%]==[C0CTLPW001] (
	@echo.exit -1
	@exit -1
)
@set my_dir=\\%computername%\xena_work$\
@SET FILELOG="%my_dir%_%my_datetime%_hourlyInsertUpadateXENA.LOG"
@SET FILELOG_TEMP="%my_dir%hourlyInsertUpadateXENA.LOG._ENV_.TEMP"
@echo.%FILELOG%
@echo.%my_datetime% > %FILELOG%
::@SET LISTOFENVIRONMENTS=A1 A3 A4 A7 C7 SH C4 MA BA Z1
@SET LISTOFENVIRONMENTS=A1 A4 A7 C7 SH C4
::@SET LISTOFENVIRONMENTS=A1TEST
@FOR %%G IN (%LISTOFENVIRONMENTS%) DO (
@SET SQL=
@SET cmd=
@call :printLines
@echo.ENV=%%G   >> %FILELOG% 2>&1
@call :addtorre %%G
@SET cmd=sqlcmd -S c0ctlpwdbh.sd01.unicreditgroup.eu^,7667 -d XNCTD%%G -U xreport -P euriskom.1 -Q "!SQL!"
@echo.cmd=!cmd!   >> %FILELOG%
@SET FILELOG_TEMP_ENV=!FILELOG_TEMP:_ENV_=%%G!
@echo.FILELOG_TEMP_ENV=!FILELOG_TEMP_ENV!
@!cmd! > !FILELOG_TEMP_ENV! 2>&1
)
@FOR %%G IN (%LISTOFENVIRONMENTS%) DO (
@call :printLines
@echo.ENV=%%G   >> %FILELOG% 2>&1
@call :printHeader
@SET FILELOG_TEMP_ENV=!FILELOG_TEMP:_ENV_=%%G!
@echo.FILELOG_TEMP_ENV=!FILELOG_TEMP_ENV!
@for /F "tokens=* " %%a in ('type !FILELOG_TEMP_ENV! ^| findstr /i /l /v /c:"------" /c:"0 rows affected" /c:"ExternalTableName"') do (
@set str=%%a
@call :check
@echo.!str! >> %FILELOG% 2>&1
)
del !FILELOG_TEMP_ENV! /F
)
@ENDLOCAL
@GOTO :EOF
:addtorre
@SET TORRE=%1
@SHIFT
@SET SQL=!SQL! ^
update WorkQueue set SrvName = null, Status = 16, Priority = 1 ^+ coalesce^(Priority,1^) from XNCTD%TORRE%.dbo.tbl_WorkQueue WorkQueue where WorkQueue.status in ^( 31, 262 ^) and  DATEDIFF ^( day , InsertTime , current_timestamp ^) ^<= 5 and ^(Priority is NULL or Priority ^< 31^) ; ^
update WorkQueue set ExternalTableName='MIGRATION' , TypeOfWork = 999, WorkClass = 748, Status = 16, SrvName = null from XNCTD%TORRE%.dbo.tbl_WorkQueue WorkQueue where status = 263 AND SrvName = 'SUSPENDED' AND TypeOfWork = 1 and exists ^(select 1 from XNCTD%TORRE%.dbo.tbl_JobReports jr2 ,  XNCTD%TORRE%.dbo.tbl_JobReports jr where  WorkQueue.ExternalKey = jr.JobReportId and jr.JobReportName not in ^('CDAMFILE', 'W3SVC', 'XRRENAME','XAFPRSCE'^) and jr.status = 31 and  jr2.JobReportId ^<^> jr.JobReportId and  jr2.RemoteFileName = jr.RemoteFileName and  jr2.JobReportName = jr.JobReportName and  jr2.JobNumber = jr.JobNumber and  jr2.JobExecutionTime = jr.JobExecutionTime and  jr2.XferInBytes = jr.XferInBytes and  jr2.JobName = jr.JobName and  jr2.status = 18 ^) ;
@GOTO :EOF
:check
if "!str:~-5!" == "     " (
set str=!str:~0,-5!
goto check
)
@GOTO :EOF
:printLines
@echo.------------------------------------------------------------------------------   >> %FILELOG%
@GOTO :EOF
:printHeader
@echo.ExternalTableName                                                ExternalKey TypeOfWork WorkId               InsertTime              WorkClass   Priority Status SrvName                          SrvParameters>> %FILELOG%
@GOTO :EOF