@echo off
setlocal enabledelayedexpansion
set my_dir=%~dp0
set USR=xreport
set PWD=euriskom
set fileXML="\\%computername%\xreport_home$\TASKS\XN_XENA_restartXN_XX_GTWRxxx_0secsDelay.xml"
set fileXML3="\\%computername%\xreport_home$\TASKS\XN_XENA_restartXN_xx_XXX_xxx_0secsDelay.xml"
set fileXML2="\\%computername%\xreport_home$\TASKS\XN_XENA_zipXENALogs.xml"
set fileXML4="\\%computername%\xreport_home$\TASKS\XN_XENA_dailyInsertUpadateXENA_TASK.xml"
cd C:\Windows\system32
echo.PROCESSING \\%computername%
SCHTASKS /Delete /TN "XN_XENA_restartXN_XX_GTWRxxx" /F
SCHTASKS /Delete /TN "XN_XENA_restartXN_xx_XXX_xxx" /F
SCHTASKS /Delete /TN "XN_XENA_zipXENALogs" /F
SCHTASKS /Delete /TN "XN_XENA_dailyInsertUpadateXENA_TASK" /F
ping 127.0.0.1 -n 1 > nul
SCHTASKS /Create /RU %USR% /RP %PWD% /TN "XN_XENA_restartXN_XX_GTWRxxx" /XML %fileXML%
SCHTASKS /Create /RU %USR% /RP %PWD% /TN "XN_XENA_restartXN_xx_XXX_xxx" /XML %fileXML3%
SCHTASKS /Create /RU %USR% /RP %PWD% /TN "XN_XENA_zipXENALogs" /XML %fileXML2%
SCHTASKS /Create /RU %USR% /RP %PWD% /TN "XN_XENA_dailyInsertUpadateXENA_TASK" /XML %fileXML4%
schtasks  /Query | findstr /i "xn"
for /l %%n in (1,1,8) do (
@IF /I NOT [\\%computername%]==[\\C0CTLPW00%%n] (
echo.PROCESSING \\C0CTLPW00%%n
SCHTASKS /Delete /S \\C0CTLPW00%%n /TN "XN_XENA_restartXN_XX_GTWRxxx" /F  /U %USR% /P %PWD%
SCHTASKS /Delete /S \\C0CTLPW00%%n /TN "XN_XENA_restartXN_xx_XXX_xxx" /F  /U %USR% /P %PWD%
SCHTASKS /Delete /S \\C0CTLPW00%%n /TN "XN_XENA_zipXENALogs" /F  /U %USR% /P %PWD%
SCHTASKS /Delete /S \\C0CTLPW00%%n /TN "XN_XENA_dailyInsertUpadateXENA_TASK" /F  /U %USR% /P %PWD%
ping 127.0.0.1 -n 1 > nul
SCHTASKS /Create /S \\C0CTLPW00%%n /RU %USR% /RP %PWD% /TN "XN_XENA_restartXN_XX_GTWRxxx" /XML %fileXML%
SCHTASKS /Create /S \\C0CTLPW00%%n /RU %USR% /RP %PWD% /TN "XN_XENA_restartXN_xx_XXX_xxx" /XML %fileXML3%
SCHTASKS /Create /S \\C0CTLPW00%%n /RU %USR% /RP %PWD% /TN "XN_XENA_zipXENALogs" /XML %fileXML2%
SCHTASKS /Create /S \\C0CTLPW00%%n /RU %USR% /RP %PWD% /TN "XN_XENA_dailyInsertUpadateXENA_TASK" /XML %fileXML4%
schtasks  /Query /S \\C0CTLPW00%%n | findstr /i "xn"
)
)
pause
@GOTO :EOF