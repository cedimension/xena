@echo off
setlocal enabledelayedexpansion
::set my_dir=%~dp0
set my_dir=\\%computername%\xena_work$\
set logFile=%my_dir%logRestartGTWR.txt
set ENVTOEXCLUDE=A1TEST
set ENVTOEXCLUDE2=MA
set ENVTOEXCLUDE3=BA
set ENVTOEXCLUDE4=Z1
cd C:\Windows\system32
ping 127.0.0.1 -n 11 > nul
::---------------------------------disable script--------------------------------------------------------
@GOTO :EOF
for /f "tokens=2,*" %%a in ('sc \\%computername% query state^= all ^| findstr /i /l /c:"SERVICE_NAME: XN" ^| findstr /i /l /c:"GTW" ^| findstr /i /l /v /c:"_%ENVTOEXCLUDE%_" /c:"_%ENVTOEXCLUDE2%_" /c:"_%ENVTOEXCLUDE3%_" /c:"_%ENVTOEXCLUDE4%_"') do (
@call :checkService %%a \\%computername% isActiveService 
@IF /I [!isActiveService!]==[N] (
	echo.%date%_%time% - %computername% -TRYING TO START %%a >> %logFile%
	echo.%date%_%time% - %computername% -TRYING TO START %%a 
	SC \\%computername% START %%a | findstr /i /l /c:"STAT" /c:"running" >> %logFile%
	SC \\%computername% queryex %%a | findstr /i /l /c:"PID" >> %logFile%
)
)
exit 0
@GOTO :EOF

::@call :checkService %nameservice% isActiveService
:checkService
for /f "tokens=2,*" %%a in ('sc %2 query ^| findstr /i /l /c:"%1"') do (
	set %3=Y
	GOTO :EOF
)
set %3=N
@GOTO :EOF