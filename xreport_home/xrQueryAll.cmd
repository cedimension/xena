@SETLOCAL

@call :startsvcs ADDES
@call :startsvcs CERIT
@call :startsvcs ETR
@call :startsvcs GERIT
@call :startsvcs NOMOS
@call :startsvcs POLIS
@call :startsvcs SARDEGNA

@goto :EOF

:startsvcs

@sc query XR_%1_GTWL%COMPUTERNAME:~-1%1 | FIND "STATE"
@sc query XR_%1_GTWR%COMPUTERNAME:~-1%1 | FIND "STATE"
@sc query XR_%1_GTWR%COMPUTERNAME:~-1%2 | FIND "STATE"
@sc query XR_%1_PDF%COMPUTERNAME:~-1%1 | FIND "STATE"
@sc query XR_%1_PDF%COMPUTERNAME:~-1%2 | FIND "STATE"
@sc query XR_%1_INI%COMPUTERNAME:~-1%1 | FIND "STATE"

@ENDLOCAL

@goto :EOF

