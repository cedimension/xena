#!/usr/bin/perl -w

use lib("$ENV{XREPORT_HOME}/perllib");

use strict;

use Data::Dumper qw(Dumper);

use XReport;
use XReport::JobREPORT;
use XReport::PDF::AcroForm;
use Convert::IBM390 qw(:all);

my $EBCDIC_To_ASCII_Table = 
#   0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
 '\x00\x01\x02\x03\x9C\x09\x86\x7F\x97\x8D\x8E\x0B\x0C\x0D\x0E\x0F'.  # 0
 '\x10\x11\x12\x13\x9D\x85\x08\x87\x18\x19\x92\x8F\x1C\x1D\x1E\x1F'.  # 1
 '\x80\x81\x82\x83\x84\x0A\x17\x1B\x88\x89\x8A\x8B\x8C\x05\x06\x07'.  # 2
 '\x90\x91\x16\x93\x94\x95\x96\x04\x98\x99\x9A\x9B\x14\x15\x9E\x1A'.  # 3
 '\x20\xA0\xA1\xA2\xA3\xA4\xA5\xA6\xA7\xA8\xD5\x2E\x3C\x28\x2B\x7C'.  # 4
 '\x26\xA9\xAA\xAB\xAC\xAD\xAE\xAF\xB0\xB1\x21\x24\x2A\x29\x3B\x5E'.  # 5
 '\x2D\x2F\xB2\xB3\xB4\xB5\xB6\xB7\xB8\xB9\xE5\x2C\x25\x5F\x3E\x3F'.  # 6
 '\xBA\xBB\xBC\xBD\xBE\xBF\xC0\xC1\xC2\x60\x3A\x23\x40\x27\x3D\x22'.  # 7
 '\xC3\x61\x62\x63\x64\x65\x66\x67\x68\x69\xC4\xC5\xC6\xC7\xC8\xC9'.  # 8
 '\xCA\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\xCB\xCC\xCD\xCE\xCF\xD0'.  # 9
 '\xD1\x7E\x73\x74\x75\x76\x77\x78\x79\x7A\xD2\xD3\xD4\x5B\xD6\xD7'.  # A
 '\xD8\xD9\xDA\xDB\xDC\xDD\xDE\xDF\xE0\xE1\xE2\xE3\xE4\x5D\xE6\xE7'.  # B
 '\x7B\x41\x42\x43\x44\x45\x46\x47\x48\x49\xE8\xE9\xEA\xEB\xEC\xED'.  # C
 '\x7D\x4A\x4B\x4C\x4D\x4E\x4F\x50\x51\x52\xEE\xEF\xF0\xF1\xF2\xF3'.  # D
 '\x5C\x9F\x53\x54\x55\x56\x57\x58\x59\x5A\xF4\xF5\xF6\xF7\xF8\xF9'.  # E
 '\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\xFA\xFB\xFC\xFD\xFE\xFF'   # F
;

my $translator = Convert::EBCDIC->new($EBCDIC_To_ASCII_Table);

$main::runparms = {};
my $jr = XReport::JobREPORT->Open($ARGV[1], 1);
my $userlib = c::getValues('userlib');

$main::runparms = {JobReportId => $ARGV[1], 
		   workdir => $ARGV[0], 
		   userlib => $userlib, 
		   jrh => $jr, 
		   reportcfg => $jr->getFileName('PARSE'),
		   input => $jr->get_INPUT_ARCHIVE(),
		  };


@{$main::runparms}{qw(JobReportName XferStartTime)} = $jr->getValues(qw(JobReportName XferStartTime));
$main::runparms->{parser} = XML::Simple::XMLin($main::runparms->{reportcfg},
					       ForceArray => [qw(exit page op rect report)]);
my @exits = @{$main::runparms->{parser}->{exit}} if exists $main::runparms->{parser}->{exit}; 

die "No DataMap specified in config for $main::runparms->{JobReportName}" unless exists $main::runparms->{parser}->{datamap};
my $dmname = $main::runparms->{parser}->{datamap}->{resname};

use XReport::Resources;
my $resh = new XReport::Resources( destdir => "$main::runparms->{workdir}/localres"
				   ,TimeRef => $main::runparms->{XferStartTime}
				 );



my $dmfile = $resh->getResources(ResName => $dmname, ResClass => 'txtdata', OutFile => "$dmname.txt") || die "Unable to get resource $dmname";

$main::runparms->{datamap_in} = $dmfile;

open(MAP, "<$dmfile") || die "open failed for \"$dmfile\" - $!";
#my @fields = (); 

my $unpack_str = '';
my $fattrs = [];
while(1) {
  my $l = <MAP>; last if !$l; chomp $l; next if $l =~ /^\s*$/;
  my @l = split(";", $l, 8);
  my ($lname, $lbeg, $lend) = @l[0,2,4];
  my $el = {};
  @{$el}{qw(lname lbeg lend maskin maskou descr)} = @l[0,2,4,5,6,7];
  push @$fattrs, $el;
#  push @fields, $lname;
  my $llen = $lend - $lbeg + 1;
  $unpack_str .= ( $el->{maskin} && $el->{maskin} =~ /^(P|Z)(\d+(?:\.\d)?)$/i ? ' '.lc($1).$2 : " e$llen");
  #print join(",", $lname, $lbeg, $lend, $llen), "\n";
}
close(MAP);

my @fields = map { $_->{lname} } sort { $a->{lbeg} <=> $b->{lbeg} } @$fattrs;
i::logit("Datamap $dmname defines " . scalar(@fields) . "fields");
#die Dumper(\@fields);

my $inh = $main::runparms->{input}->get_INPUT_STREAM($main::runparms->{jrh}, 'data') || die "unable to open INPUT - $!";
$inh->Open() || die "unable to open file associated to input stream - $!";

my $datastr = '';
while (!$inh->eof()) {
  my $datarec = '';
  my $l = $inh->read(my $buff, 2048);
  $datastr .= $buff;
  next unless ($datastr =~ /^(.*)\xff[\x01\x02\x03](.*)$/ || $inh->eof());
  ($datarec, $datastr) = ($inh->eof() ? ($datastr, '') : ($1, $2));

  i::logit("INPUT REC completed - " . length($datarec) . " bytes");

  use XReport::INFORMAT;
  use XReport::OUTFORMAT;
#  use POSIX qw(locale_h); # Imports setlocale() and the LC_ constants.
#  setlocale(LC_NUMERIC, "it_IT") or die "No Italian environment";


  my $values = {}; 
  @{$values}{@fields} = unpackeb($unpack_str, $datarec);
  my $masks = { map { $_->{lname} => {in => $_->{maskin}, ou => $_->{maskou} } } 
		grep { $_->{maskou} || ($_->{maskin} && $_->{maskin} !~ /^(P|Z)(\d+(?:\.\d)?)$/i) } @$fattrs };
  
  while ( my ($fld, $el) = each %$masks ) {
    my ($maskin, $maskou) = map { [ split(/\|/, $_, 2) ] } @{$el}{qw(in ou)};
#    i::logit("FLD: $fld MASKIN: $maskin->[0]::$maskin->[1] MASKIN: $maskou->[0]::$maskou->[1]");
    if ( $maskin && (my $infun = XReport::INFORMAT->can($maskin->[0])) ) {
      $values->{$fld} = &$infun($values->{$fld}, $maskin->[1] || undef );
    }
    if ( $maskou && $maskou->[0] =~ /^%/ ) {
      $values->{$fld} = sprintf($maskou->[0], $values->{$fld}) ;
    } elsif ( $maskou && (my $infun = XReport::OUTFORMAT->can($maskou->[0])) ) {
      $values->{$fld} = &$infun($values->{$fld}, $maskou->[1] || undef);
    }
    
  } 
  
  my $forms = [ grep /^\S+$/, map { $_ } unpack("(A4)*", delete $values->{ANLTDO}) ];
  (my $prtdest = delete $values->{ANLPRT}) =~ s/^\s*(\S+)\s*$/$1/;
  $prtdest = '' unless $prtdest;
  i::logit("PRTDEST set to $prtdest");
  $jr->setValues(OS400attrs => {'Output queue name' => $prtdest });
  #die Dumper($values), "\n", Dumper($forms);

  my $pdfcnt = 0;
  foreach my $form ( @$forms ) {
    my $outfile = (split /[\\\/]/, $jr->getFileName('PDFOUT',++$pdfcnt))[-1];
    i::logit("Now processing FORM $form - will fill \"$outfile\"");
    
    my $formFile = $resh->getResources(ResName => $form, ResClass => 'pdfform', OutFile => "$form.pdf") || die "Unable to get resource $form";
    i::logit("$form resource saved into \"$formFile\"");
    my $doc = XReport::PDF::AcroForm->Open("$formFile");
    $doc->UpdateFields($values);
    $doc->WritePdf("$main::runparms->{workdir}/$outfile");
    $doc->Close();
  }

}
$inh->Close;

my $perllib = c::getValues("userlib")."/perllib" if scalar(@exits); 

foreach my $exit ( grep { $_->{type} =~ /^post$/i } @exits ) {
  my ($perlclass, $ref) = @{$exit}{qw(perlclass ref)};
  $ref ne "" ? require "$perllib/$ref" : eval "require $perlclass" ;
  
  my $postexit = $perlclass->new($jr, $main::runparms->{workdir}, $exit);
  die "unable to instance new $perlclass" unless $postexit;
  $postexit->Commit(); 
  $postexit->Close();
} 

