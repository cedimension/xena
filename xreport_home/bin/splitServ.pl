
use lib($ENV{'XREPORT_HOME'}."/perllib");

use strict;

use XReport;
use XReport::Util qw(:CONFIG);
use XReport::WorkQueue;

use constant ST_RECEIVED => 16;
use constant ST_QUEUED => 16;
use constant ST_INPROCESS => 17;
use constant ST_COMPLETED => 18;
use constant ST_PROCERROR => 31;

InitServer(); 

my $wq = XReport::WorkQueue->new();

while(1) {
  my $workitem = $wq->getWorkItem();
  my $status = $workitem->doElab();

    $status == ST_COMPLETED ? $wq->delWorkItem() : $wq->suspendWorkItem($status);
  
  #exit;
}

TermServer();

