#!/usr/local/bin/perl -w
#######################################################################
# @(#) $Id: xrLpdServ.pl 2263 2008-09-16 15:28:10Z mpezzi $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################

my $version = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };
use strict;

use lib($ENV{'XREPORT_HOME'}."/perllib");

use Xreport;
use XReport::Util;
use XReport::SUtil;
use XReport::QUtil;
use XReport::logger;

sub Server;
sub Child;
sub GetDateTime;
sub queueForProcess;
sub LPRQUEGZ;
sub readCtlStream;
sub receivePrinterJob;

use constant XF_LPR => 1;
use constant XF_PSF => 2;
use constant XF_FTPB => 3;
use constant XF_FTPC => 4;
use constant XF_ASPSF => 5;
use constant XF_AFPSTREAM => 6;
use constant XF_MODCASTREAM => 7;
use constant XF_SCS => 8;
use constant XF_AS400 => 9;

my $exeType = \&Server;
my $con = 0;
$REQ::daemon = 0;
my $CliFno;

my ($SrvName, $debug) = getConfValues(qw(SrvName debugLevel));
InitServer('isdaemon'=>1,);
$SrvName = $XReport::cfg->{SrvName};

foreach my $i (0 .. $#ARGV) {
  if ($ARGV[$i] eq "-P") { # execution type 
    if ($ARGV[$i+1] eq "CHILD") {
      $exeType = \&Child;
      XRprId($REQ::prId = "$$(".$ARGV[$i+2].")");
      $CliFno  = $ARGV[$i+3];
      $REQ::conid = $con     = $ARGV[$i+4];
    }
  }
}
$REQ::SrvName = $SrvName;
#my $cfg = getInitHash($SrvName);

$REQ::SrvPort   = $XReport::cfg->{'daemon'}->{$SrvName}->{'port'} || 515;
$REQ::msgmax    = $XReport::cfg->{'daemon'}->{$SrvName}->{'maxdata'} || 32768;
#$REQ::WorkClass = $XReport::cfg->{'daemon'}->{$SrvName}->{'WorkClass'} || 32768;

exit (&$exeType);

sub Server {
  use IO::Socket;
  use IO::Select;

  my ($sock, $Cli, $currSt);
  &$logrRtn("$^O server Started ...");

  my $SrvSock = new IO::Socket::INET(Listen => 5, 
				     LocalPort => $REQ::SrvPort,
				     Proto => 'tcp', 
				     Reuse => 1) || &$logrRtn("unable to listen on $REQ::SrvPort - $!");

  &$logrRtn("Listener now waiting on port $REQ::SrvPort") if ($SrvSock);

  my $sel = new IO::Select( $SrvSock );

  &$logrRtn("now entering main loop") if ($SrvSock);
  for (my $con = 0; ; $con++) {
    $sock = $sel->can_read(15);
    next unless ($sock);
    $Cli = $SrvSock->accept || &$logrRtn("unable to accept connection - $! - $?");
    if ($Cli) {
      my ($cliport, $iaddr) = sockaddr_in(getpeername($Cli));
      $REQ::peer = inet_ntoa($iaddr);
      &$logrRtn($SrvName . " accepted connection from " . $REQ::peer);
      spawnProcess($0, "-N", $SrvName, ($REQ::daemon eq 0 ? '-_' : '-@'), "-P", "CHILD", "$$", getFileToken($Cli), $con);
    } else {
      return 1;
    }
  }
  &$logrRtn("now leaving main loop: $currSt ", (($sock) ? "Cli pending" : "Cli Exhausted"));
  $SrvSock->close;
  return 0;
}

sub doLog {
  if ($REQ::LpdSock && $REQ::xrClient) {
    syswrite($REQ::LpdSock, "\x01INFO " . strftime ("%Y %b %e %H:%M:%S", localtime()) . " ". join('', @_) ."\n");
  }
  &$logrRtn(@_);
}

sub Child {

  use IO::Socket;
  use IO::Select;

  %REQ::queRtns = (
	      '.default'  => \&LPRQUEGZ,     # $REQ::queRtns{'LPRQUEGZ'},
	      'LPRQUEGZ'  => \&LPRQUEGZ,
	      'LPRQUE'    => \&LPRQUEGZ,     # $REQ::queRtns{'LPRQUEGZ'},
	      'AS400PSF2' => \&AS400PSF2,
	      'LATE'      => \&LPRQUEGZ,     # $REQ::queRtns{'LATE'},
	      'GETFTPCQ'  => \&GETFTPCQ,
	      'GETFTPQ'   => \&GETFTPCQ,     # $REQ::queRtns{'GETFTPCQ'},
#	      'CTMPMFTP'  => \&CTMPMFTP,
	     );

  $REQ::prtname = "undefined";

  my $valPrtn = qr/([\w\.\-]+)/;

  $REQ::LpdSock = new IO::Socket::INET;

  openFileToken($REQ::LpdSock, $CliFno) || die("CliSock OPEN ERROR <$!>\n");
#  $REQ::peer = $REQ::LpdSock->PeerHost;
#  $REQ::port = $REQ::LpdSock->PeerPort;

  setsockopt($REQ::LpdSock, SOL_SOCKET, SO_RCVBUF, pack("l", $REQ::msgmax))   || die "Setsockopt error: $!";

#  select($REQ::LpdSock); $| = 1; select(STDOUT);

  ($REQ::port, $REQ::iaddr) = sockaddr_in(getpeername($REQ::LpdSock));

  $REQ::peer = inet_ntoa($REQ::iaddr);

  &$logrRtn("Serving connection $con from $REQ::peer at $REQ::port - maxblks $REQ::msgmax");
  $REQ::InEof = 0;
  my $sel = new IO::Select( $REQ::LpdSock );

  if (! $sel->can_read(240)) {
    &$logrRtn("Connection timed out during cntl pkt receive");
  } else {
    
    my $blen = sysread $REQ::LpdSock, my $cmd, $REQ::msgmax;
    
    if (!defined($cmd) || !$blen) {
      &$logrRtn("No Data received from requester at $REQ::peer");
    } 
    elsif ( $cmd =~ /^([\x02\x03\x04\x1a])([\w\.\-]+)\x0a$/o) {
      (my $req, $REQ::prtname) = ($1, $2);
    ($REQ::queuetype, $REQ::queuename, $REQ::queparms) = split(/\./,$REQ::prtname,3);
      unless ($REQ::queuename) {
	$REQ::queuename = $REQ::queuetype;
	$REQ::queuetype = '.default';
      }
      
      if ( $req =~ /[\x02\x1a]/ ) {
	$REQ::xrClient = ($req eq "\x1a");
	#      &$logrRtn("Peer requester xrClient: $REQ::xrClient");
	syswrite($REQ::LpdSock,"\x00",1);
	$REQ::InBytes = $REQ::DtaBytes = $REQ::OutBytes = 0;
      JOB: for (my $jobn = 0; ; $jobn++ ) {
	  $REQ::currJob = $jobn;
	  last JOB unless recvJob($jobn);
	}
	doLog("Bytes received: $REQ::InBytes Written: $REQ::OutBytes");
      }
      elsif ( $req =~ /[\x03\x04]/ ) {
	&$logrRtn("Queue status request received. Queue: $REQ::prtname");
	syswrite(LPRSOCK, "$^O $REQ::prtname next job will have " . ($con + 1) . " id" ) if ($req eq "\x04");
	syswrite(LPRSOCK, "$^O $REQ::prtname Receiver Ready");
	syswrite($REQ::LpdSock,"\x00",1);
      }
    }
    elsif ($cmd) {
      &$logrRtn("Unexpected cmd received: ", unpack("H*", $cmd), "");
      syswrite($REQ::LpdSock,"\x7F",1);
    }
    else {
      &$logrRtn("No Data received from requester at $REQ::peer");
    }
    
    shutdown($REQ::LpdSock,2);
    close($REQ::LpdSock);
    
    &$logrRtn("Queue SERVER $con for $REQ::prtname ENDED.");
    
    return(0);
  }
}

sub readLpdSock {
  use IO::Select;
  my $dbuff = '';
  my $bleft = ( defined($REQ::bleft) ? $REQ::bleft : $REQ::msgmax + 1 );
  if ( $bleft ) {

    $REQ::sel = new IO::Select( $REQ::LpdSock ) unless $REQ::sel;
    
    if (! $REQ::sel->can_read(240)) {
      &$logrRtn("Connection with $REQ::peer($REQ::port) timed out");
      return undef;
    }
    
    #  my $dbuff;
    my $bytes2read = ($REQ::msgmax > $bleft ? $bleft : $REQ::msgmax);
    my $blen = sysread( $REQ::LpdSock, $dbuff, $bytes2read );
#    doLog("Buffer ($blen) length ".length($dbuff), unpack("H*", $dbuff));
    die "Some error encounterd during port $REQ::port sysread - $!\n" unless defined($blen);
    if ( $blen ) {
      $bleft  -= $blen;
      $REQ::rbytes += $blen;
    } 
    $REQ::InEof = 1 if ( $bleft == 0 || length($dbuff) == 0 );
    $blen = sysread($REQ::LpdSock, my $null, 1) if ($bleft == 0);
    die "Some error encounterd during port $REQ::port sysread - $!" unless defined($blen);
    
  } 

  $REQ::buffer = substr($REQ::buffer . $dbuff, $REQ::sol);
  ($REQ::bpos, $REQ::sol, $REQ::blen) = (($REQ::bpos - $REQ::sol), 0, length($REQ::buffer));

  $REQ::bleft = $bleft if defined($REQ::bleft);

  return undef unless ($REQ::blen > 0);
  
  return ($REQ::bpos, $REQ::sol, $REQ::blen, $REQ::buffer);

}

sub readFtpSock {

  return undef unless $REQ::ftpDCH;

  &$logrRtn("Filling up buffer $REQ::pagcnt $REQ::lines $REQ::sol $REQ::bpos $REQ::blen") if ($REQ::debug);
  #    my $rb = $fhIn->read(my $NewBuffer, $::msgmax, 15);

  my $blen = $REQ::ftpDCH->read(my $dbuff, $REQ::msgmax, 15);

  $REQ::InEof = (! $blen );

  $REQ::rbytes += $blen if ($blen);

  $REQ::buffer = substr($REQ::buffer . $dbuff, $REQ::sol);

  ($REQ::bpos, $REQ::sol, $REQ::blen) = (($REQ::bpos - $REQ::sol), 0, length($REQ::buffer));
  &$logrRtn("Received $blen bytes $REQ::pagcnt $REQ::lines $REQ::sol $REQ::bpos $REQ::blen") if ($REQ::debug);
  
  return undef unless ($REQ::blen > 0);

  return ($REQ::bpos, $REQ::sol, $REQ::blen, $REQ::buffer);
}

sub recvJob {
  my $jobn = shift;
  my $cmd = <$REQ::LpdSock>;
  return undef unless $cmd;
  
  if ($cmd eq "\x01\x0a") {
    doLog("Client sent Abort Command - aborting jobs 0 - $#REQ::job ");
    syswrite($REQ::LpdSock,"\x00",1);
    return undef;
  }
  
#  if ($cmd =~ /^([\x02\x03])(\d+)\s([\w\.]+)\x0a$/) {
#    my ($cc, $numb, $infnam) = ($1, $2 , $3);
#    if ($cc eq "\x02") {
# SNDTCPSPLF DESTTYPE(*AS400) sends a \x04 command let's give a try
  if ($cmd =~ /^([\x02\x03\x04\x05])(?:(\d+)\s)?([\w\.\-]+)\x0a$/) {
    my ($cc, $numb, $infnam) = ($1, $2 , $3);
    if ($cc =~ /[\x02\x04]/) {
      
      if ($REQ::job[$jobn]{CtlFile}) {
	doLog("more than one Cntl File in the same job - aborting");
	syswrite($REQ::LpdSock,"\x7F",1);
	return undef;
      }
      
      doLog("Client sent Cntl file $REQ::currJob - name: $infnam bytes: $numb ", unpack("H*", $cmd));
      syswrite($REQ::LpdSock,"\x00",1);
      
      $REQ::job[$jobn]{CtlFile} = $infnam;
      $REQ::job[$jobn]{CtlBytes} = $REQ::bleft = $numb;
      ($REQ::rbytes, $REQ::wbytes, $REQ::buffer ) = (0, 0, '');
      $REQ::bpos = $REQ::sol = $REQ::InEof = 0;  
      do {
	readLpdSock() || last;
      } while ( ! $REQ::InEof );
      $REQ::job[$jobn]{CtlStream} = $REQ::buffer;
      
      if ($REQ::rbytes ne $numb) {
	doLog("receive failed for $REQ::currJob Cntl file: $numb expected - $REQ::rbytes received");
	syswrite($REQ::LpdSock,"\x7F",1);
	return undef;
      }
      
      doLog("Cntl subcommands received: $numb bytes", $REQ::buffer);
      syswrite($REQ::LpdSock,"\x00",1);
      
      if (! $REQ::job[$jobn]{DtaFile}) {
	return undef unless recvJob($jobn);
      }
      return $REQ::rbytes;
    }
    else { # cc = \x03 or \x05
      if ($REQ::job[$jobn]{DtaFile}) {
	doLog("more than one Data File in the same job - aborting");
	syswrite($REQ::LpdSock,"\x7F",1);
	return undef;
      }
      
      doLog("Client sent Data file $REQ::currJob - name: $infnam bytes: $numb ");
      syswrite($REQ::LpdSock,"\x00",1);
      ($REQ::rbytes, $REQ::wbytes, $REQ::buffer ) = (0, 0, '');
      $REQ::bpos = $REQ::sol = $REQ::InEof = 0;  
      my $xfrRtn = ($REQ::queRtns{$REQ::queuetype} or $REQ::queRtns{'.default'});

      my $brcvd = &$xfrRtn($jobn,
			   $REQ::job[$jobn]{DtaFile} = $infnam,
			   $REQ::job[$jobn]{DtaBytes} = $REQ::bleft = $numb);

      return undef unless $brcvd;

      $REQ::DtaBytes += $brcvd;
      return $brcvd
    }
  }
  else {
    $cmd =~ /^([\x02\x03\x04\x05])(?:(\d+)\s)?([\w\.\-]+)\x0a$/;
    my ($cc, $numb, $infnam) = ($1, $2 , $3);
    doLog("Unknown receive job subcmd received:", unpack("H*", $cmd), " -- ",  $cmd , "cc: $cc - numb: $numb fnam: $infnam");
    syswrite($REQ::LpdSock,"\x7F",1);
    return undef;
  }
}

sub setXferFileA {

  my ($queuetype, $queuename, $progr, $datetime) = @_;

  $datetime = GetDateTime() unless ($datetime && $datetime =~ /^\d{14}$/);
  my ($curryear, $currday, undef) = unpack("a4a4a*", $datetime);

#  my $xrspool = $XReport::cfg->{'xrspool'};
#  my $xrspool = getConfValues('LocalPath')->{'L1'}."/IN";
#  $xrspool =~ s/^file:\/\///;
#  if (! -e $xrspool."/".$curryear."/".$currday ) {
#    mkdir $xrspool."/".$curryear if (! -e $xrspool."/".$curryear);
#    mkdir $xrspool."/".$curryear."/".$currday;
#    die "LPDServ.pm unable to create $xrspool $curryear $currday dir\n" unless ( -e $xrspool."/".$curryear."/".$currday )
#  }
#  my $REQ::outpdir = $xrspool."/".$curryear."/".$currday;
#
#  my $filen = "$queuename.$datetime.$progr";
#  
#  return ($curryear."/".$currday."/".$filen, $xrspool, $datetime);

  my $filen = "$queuename.$datetime.$progr";
  my $tgt = XReport::ARCHIVE::newTargetPath('L1', "IN/$curryear/$currday");
#  $hndl->{LocalPathId_IN} = $tgt->{LocalPathId};
  my @xrspool = split /(?<![\/\\])[\/\\](?![\/\\])/, $tgt->{fqn};
  
  
   return (join('/', splice(@xrspool, -2))."/".$filen, join('/', @xrspool), $datetime);
}

sub setXferDate {
  return 'GETDATE()' unless ($REQ::queuetype eq "LATE" && $REQ::queparms =~ /^\d{14}$/ );
  my ($curryear, $currmth, $currday, $currhh, $currmm, $currss) = unpack("a4a2a2a2a2a2", $REQ::queparms);
  return join('/', ($curryear, $currmth, $currday))." ".join(':', ($currhh, $currmm, $currss));
}

sub logNewRequest {

  $REQ::xfermode = shift;

  doLog("Now adding conid: $REQ::conid prId: $REQ::prId xm: $REQ::xfermode st: $CD::stAccepted");
  my $reqid = QCreate XReport::QUtil(
				     SrvName        => $REQ::SrvName,
				     JobReportName  => $REQ::queuename,
				     LocalFileName  => $REQ::queuetype . $$ . $REQ::conid,
				     RemoteHostAddr => $REQ::peer,
				     XferStartTime  => setXferDate(),
				     XferMode       => $REQ::xfermode,
				     XferDaemon     => $REQ::SrvName,
				     Status         => $CD::stAccepted,
				     XferId         => $REQ::prId,
				     );
  $REQ::InBytes = 0;
  return $REQ::dbid = $reqid;
}

sub renameJobReport {
      my ($oldqn, $newqn) = (shift, shift); 
      my $olddf = $REQ::datafile;

      $REQ::queuename = $newqn;
      $REQ::datafile =~ s/$oldqn/$REQ::queuename/;
      if ( rename($olddf, $REQ::datafile) ) {
	doLog("renamed $olddf to $REQ::datafile");
	my $oldcf = $REQ::cntlfile;
	$REQ::cntlfile =~ s/$oldqn/$REQ::queuename/;
	if ( rename($oldcf, $REQ::cntlfile) ) {
	  doLog("renamed $oldcf to $REQ::cntlfile");
	} else {
	  $REQ::cntlfile = $oldcf;
	  $REQ::datafile = $olddf if rename($REQ::datafile, $olddf);
	  $REQ::queuename = $oldqn;
	}
      }
      else {
	$REQ::queuename = $oldqn;
      }
      return undef unless $REQ::queuename eq $newqn;
      return 1;
}

sub setRequestOK {
  my ($code, $reqid, $cntlfile, $datafile, $CtlStream) = @_;

  open(my $fhOut, ">$cntlfile") || do {
    doLog("DATA FILE OPEN ERROR: $!");
    return undef;
  };
  binmode $fhOut;
  my $wb = syswrite($fhOut, $CtlStream);

  close($fhOut);
  return undef if ($wb ne length($CtlStream));
  $REQ::cntlfile = $cntlfile;

  my %cinfo = ();
  foreach ( split(/\n/, $CtlStream) ) {

    /^\-odatat=(\w+).*$/             && do {
#      doLog( "row o: $1");
	$REQ::xfermode = XF_PSF;
	next;
      $cinfo{'fileformat'} = ($1 eq 'record' ? XF_PSF : $REQ::xfermode); next; };

    /^\-o(\w+)=(\w+).*$/             && do {
#      doLog("row o: $1 $2");
      my ($var, $val) = ($1, $2);
      $var = lc($var) if $var =~ /^recipient$/i;
      $cinfo{$var} = $2; next; };

    /^J([\w\.\?]+)$/ && do {
      (undef, undef, $cinfo{jname}, $cinfo{jnum}, undef) = split /\./, $1, 5; 
#      doLog("row J: $1 -- jname: $cinfo{jname} $cinfo{jnum}");
      next; };

    /^XAS\/400$/ && do {
#      doLog( "row o: $1");
	$REQ::xfermode = XF_AS400;
      next; };

    /^(\w)(.*)$/                     && do {
#      doLog("row G: $1 $2");
      $cinfo{$1} = $2; 
      next;};
  }
  if (!$cinfo{jname} && ($cinfo{N} =~  /^([\w\.\?]+)$/)) {
      (undef, undef, $cinfo{jname}, $cinfo{jnum}, undef) = split /\./, $1, 5; 
      ($cinfo{jname}, $cinfo{jnum}) = (split(/\./, $1))[2,3]; 
      $cinfo{jname} = (split(/\./, $1))[0]; 
#      doLog("row NforJ: $1 -- jname: $cinfo{jname} $cinfo{jnum}");
  }

  ($REQ::XferRecipient, my $newqn) = ('', $REQ::queuename);
  if ($REQ::xfermode == XF_AS400 ) {
    ($newqn, $cinfo{jname}) = ($cinfo{N}, $REQ::queuename);
  }
  elsif ($REQ::queuename eq 'XRNPF') {
    ($cinfo{'recipient'}, $newqn) = (split(/\./, $1))[1,2];
  }
  elsif ( $REQ::queuename eq 'CEREPORT' ) {
    ($newqn, $cinfo{jname}, $cinfo{rmtfn}) = ($cinfo{N}, $cinfo{J}, $cinfo{l});
  }
  elsif ($REQ::queuename =~ /^as400ascii$/i ) {
    $newqn = $cinfo{N};
  }
  elsif ($REQ::queuename =~ /^as400psf2$/i ) {
    ($newqn, $REQ::xfermode) = ($cinfo{N}, XF_ASPSF);
  }
  elsif ($cinfo{N} =~ /^NPF[^.]+\.([^.]+)\./) {
    $cinfo{'recipient'} = $1;
  }

  my $origdbr = dbExecute("select top 1 * from tbl_JobReportNames where JobReportName = '$REQ::queuename'");
  my $TypeOfWork = $origdbr->Fields()->Item('TypeOfWork')->Value() unless $origdbr->eof();
  $TypeOfWork = 999 unless $TypeOfWork;
  if ( $newqn && $REQ::queuename ne $newqn && renameJobReport($REQ::queuename, $newqn) ) {
    
    my $dbr = dbExecute("select top 1 * from tbl_JobReportNames where JobReportName = '$newqn'");
    $TypeOfWork = $dbr->Fields()->Item('TypeOfWork')->Value() unless $dbr->eof();
    $dbr->Close();
    
  }

  $REQ::XferRecipient = $cinfo{'recipient'} if $cinfo{'recipient'};


#  print "CtlStream: \n", $CtlStream, "\n";
#  die "-----------------\n";
#				   XferDaemon     => $REQ::SrvName,
  (my $ofil = $REQ::datafile) =~ s/^$REQ::outpdir\///; 
  my $req = QUpdate XReport::QUtil(
				   XferEndTime    => setXferDate(),
				   Status         => $code,
				   JobName        => ($cinfo{jname} || $cinfo{U}),
				   JobNumber      => ($cinfo{jnum} || ( $cinfo{U} && length($cinfo{U}) > 5 ? substr($cinfo{U},2,4) : '9999') ),
				   JobReportName  => $REQ::queuename,
				   XferRecipient  => $REQ::XferRecipient,
				   RemoteFileName => ($cinfo{rmtfn} || $cinfo{N}),
				   LocalFileName  => $ofil,
				   XferPages      => ($REQ::pagcnt || 1),
				   XferInBytes    => $REQ::InBytes,
				   XferMode       => $REQ::xfermode,
				   TypeOfWork     => $TypeOfWork,
				   Id             => $reqid,
				  );


  return $req;
}

sub setRequestErr {
  my $errmsg = shift;
  doLog($errmsg) if $errmsg;
  XReport::QUtil->QUpdate(
			  XferEndTime    => 'GETDATE()',
			  Status         => $CD::stRecvError,
			  ReportName     => $REQ::queuename,
			  XferInBytes    => $REQ::InBytes,
			  Id             => $REQ::dbid,
			 );
  
}

sub LPRQUEGZ {
  ## -*- Mode: Perl -*- #################################################
  # @(#) $Id: xrLpdServ.pl 2263 2008-09-16 15:28:10Z mpezzi $ 
  #
  # Copyrights(c) EURISKOM s.r.l.
  #######################################################################
  my $version = do { my @r = (q$Revision: 1.9 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };
  
  use Compress::Zlib;
  
#  my ($jobn, $infnam, $numb) = @_;
  my ($jobn, $infnam, $numb, $xfertype) = (shift, shift, shift, shift);
  $xfertype = XF_LPR unless $xfertype;
  
  $REQ::dbid = my $reqid = logNewRequest($xfertype);
  my $reqdt = ($REQ::queuetype eq "LATE" ? $REQ::queparms : undef);
  (my $filen, $REQ::outpdir, my $datetime) = setXferFileA($REQ::queuetype, $REQ::queuename, $REQ::dbid, $reqdt);
  
  $REQ::datafile = $REQ::outpdir."/".$filen.".DATA.TXT.gz";
  
  doLog("LPRQUEGZ " . $version . " receiving $numb data bytes to fullfill req $REQ::dbid");
  &$logrRtn("req $REQ::dbid will create file $REQ::datafile");

  ($REQ::OutBytes, $REQ::InBytes) = (0, 0);
  ($REQ::rbytes, $REQ::wbytes, $REQ::buffer ) = (0, 0, '');
  $REQ::bpos = $REQ::sol = 0;
  my $fhOut = gzopen($REQ::datafile, "wb") || do {
    setRequestErr("DATA FILE OPEN ERROR $!");
    syswrite($REQ::LpdSock,"\x7F",1);
    return undef;
  };

  while ( readLpdSock() ) {
    my $wb = $fhOut->gzwrite($REQ::buffer) || do {
      doLog("LPRQUEGZ " . $version . " Error during $REQ::datafile write : $fhOut->gzerror");
      last;
    };
    $REQ::wbytes += $wb;
    last if $wb ne length($REQ::buffer);
    $REQ::bpos = $REQ::sol = $wb;
  }
  
  $REQ::job[$jobn]{bytes}  = $REQ::rbytes;
  $REQ::job[$jobn]{wbytes} = $REQ::wbytes;
    
  $fhOut->gzclose();
  $REQ::InBytes  = $REQ::rbytes;
  $REQ::OutBytes = -s $REQ::datafile;
  
  if ((defined($REQ::bleft) && $REQ::rbytes ne $numb) or ($REQ::wbytes ne $REQ::rbytes)) {
    unlink $REQ::datafile;
    setRequestErr("receive failed for Data file: bytes expected $numb " . 
	  " recvd: "   . $REQ::rbytes .
	  " written: " . $REQ::OutBytes );
    syswrite($REQ::LpdSock,"\x7F",1);
    return undef;
  }
  
  doLog("Data file received - bytes expected: " . ($numb || 'UNSPEC') .
	" recvd: " . $REQ::InBytes .
	" written: " . $REQ::OutBytes );
  
  syswrite($REQ::LpdSock,"\x00",1);  # 
  
  if (! $REQ::job[$jobn]{CtlFile}) {
    if (! recvJob($jobn) ) {
      unlink $REQ::datafile;
      setRequestErr("Failed to receive CtlFile");
      return undef;
    }
  }
  
  $REQ::pagcnt = 0;

  return undef unless setRequestOK($CD::stReceived,
				   $REQ::dbid,
				   $REQ::outpdir."/".$filen.".CNTRL.TXT",
				   $REQ::datafile,
				   $REQ::job[$jobn]{'CtlStream'});
  
  
  return $REQ::job[$jobn]{bytes};
  
}

sub getLastFtpMsg {
  my @ftpmsgs = $REQ::ftpCCH->message();
  chomp(my $fmsg = $ftpmsgs[-1]);
  my $fcode = $REQ::ftpCCH->code();
  return ($fcode, $fmsg);
}

sub startFtpSession {

  my ($ftpserv, $ftpuser, $ftpport, $ftptype,
      $ftppwd, $ftppasv, @sitecmd, @dsnlist, @quotecmd);

  $#sitecmd = -1;
  $#quotecmd = -1;
  $#dsnlist = -1;
  $ftptype = "I";

  for (   split("\n", shift)  ) {
#    &$logrRtn($_);
    /^HOST\s+(\S+)\s*$/ && do {
#      doLog(" host Name ", $1);
      $ftpserv = $1;
      next;
    };
    /^PORT\s+(\S+)\s*$/ && do {
#      doLog(" host Port ", $1);
      $ftpport = $1;
      next;
    };
    /^PASV\s.*$/ && do {
#      doLog(" passive mode ");
      $ftppasv = 1;
      next;
    };
    /^USER\s+(\S+)\s*$/ && do {
#      doLog(" host user ", $1);
      $ftpuser = $1;
      next;
    };
    /^PWD\s+(\S+)\s*$/ && do {
#      doLog(" host password ", $1);
      $ftppwd = $1;
      next;
    };
    /^TYPE\s+(\S+)\s*$/ && do {
#      doLog(" Transfer Type ", $1);
      $ftptype = $1;
      next;
    };
    /^SITE\s+(\S+\s*.*)$/ && do {
#      doLog(" site cmd ", $1);
      $sitecmd[$#sitecmd+1] = $1;
      next;
    };
    /^QUOTE\s+(\S+\s*.*)$/ && do {
#      doLog(" quote cmd ", $1);
      $quotecmd[$#quotecmd+1] = $1;
      next;
    };
    /^DSN\s+(\S+)\s*$/ && do {
#      doLog(" host dsn ", $1);
      $dsnlist[$#dsnlist+1] = $1;
      next;
    };
  }

  unless ($ftpserv && $ftpuser && (scalar(@dsnlist) > 0)) {
    my $endmsg = "12 file Xfer failed - ";
    $endmsg .= "server unspecified - "  unless ($ftpserv);
    $endmsg .= "user unspecified - "    unless ($ftpuser);
    $endmsg .= "dataset unspecified - " unless (scalar(@dsnlist) > 0);
    $endmsg .= "terminating";
    doLog($endmsg);
    return undef;
  }
  
  if    ($ftpport and $ftppasv) {
    doLog(" logging in to ", $ftpserv, " port ", $ftpport, " pasv mode");
    $REQ::ftpCCH = new Net::FTP($ftpserv, Debug => $debug, Port => $ftpport, Passive => 1);
  }
  elsif ($ftpport) {
    doLog(" logging in to ", $ftpserv, " port ", $ftpport);
    $REQ::ftpCCH = new Net::FTP($ftpserv, Debug => $debug, Port => $ftpport);
  }
  elsif ($ftppasv) {
    doLog(" logging in to ", $ftpserv, " pasv mode");
    $REQ::ftpCCH = new Net::FTP($ftpserv, Debug => $debug, Passive => 1);
  }
  else {
    doLog(" logging in to ", $ftpserv);
    $REQ::ftpCCH = new Net::FTP($ftpserv, Debug => $debug);
  }
  return undef unless ($REQ::ftpCCH);

  if ($ftppwd) {
    $REQ::ftpCCH->login($ftpuser, $ftppwd);
  }
  else {
    $REQ::ftpCCH->login($ftpuser, "xrLpd");
  }
  
  my ($fcode, $fmsg) = getLastFtpMsg();

  doLog("logon for $ftpserv completed - resp: $fcode - $fmsg");
  if ($fcode ne '230') {
    $REQ::ftpCCH->quit();
    $REQ::ftpCCH = undef;
    return undef;
  }
  $REQ::ftpCCH->type($ftptype);
  
  for my $cmd ( @sitecmd ) {
    $REQ::ftpCCH->site($cmd);
    my ($fcode, $fmsg) = getLastFtpMsg();
    doLog("site $cmd for $ftpserv sent - resp: $fcode - $fmsg");
  }
  
  for my $cmd ( @quotecmd ) {
    $REQ::ftpCCH->quot($cmd);
    my ($fcode, $fmsg) = getLastFtpMsg();
    doLog("quote $cmd for $ftpserv sent - resp: $fcode - $fmsg");
  }
  
  return @dsnlist;

}

sub GETFTPCQ {
# -*- Mode: Perl -*- ##################################################
# @(#) $Id: xrLpdServ.pl 2263 2008-09-16 15:28:10Z mpezzi $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
  my $version = do { my @r = (q$Revision: 1.8 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

  use Net::Ftp;
  use Compress::Zlib;
  
  my ($jobn, $infnam, $numb) = @_;

  $REQ::dbid = my $reqid = logNewRequest(XF_FTPC);
  (my $filen, $REQ::outpdir, my $datetime) = setXferFileA($REQ::queuetype, $REQ::queuename, $REQ::dbid);

  $REQ::datafile = $REQ::outpdir."/".$filen.".DATA.TXT.gz";

  doLog("GETFTPQC " . $version . " receiving $numb data bytes to fullfill req $REQ::dbid");
  &$logrRtn("req $REQ::dbid will create file $REQ::datafile");

#  my $msgmax  = $REQ::msgmax;
#  my $debug   = $REQ::debug;
#  my $SrvName = $REQ::SrvName;
  my $xferOK = 0;
  
  my ($fhOut, $wrtRtn, $cloRtn);
  my ($dataBuff, @ftpStmt);
  
  ($REQ::rbytes, $REQ::wbytes, $REQ::buffer ) = (0, 0, '');
  $REQ::bpos = $REQ::sol = $REQ::InEof = 0;  
  do {
    readLpdSock() || last;
  } while (! $REQ::InEof );

  if (length($REQ::buffer) ne $numb) {
    setRequestErr("receive failed for Data file: bytes expected $numb " . 
	  " recvd: "   . length($REQ::buffer) .
	  "");
    syswrite($REQ::LpdSock,"\x7F",1);
    return undef;
  }

  my @dsnlist = startFtpSession($REQ::buffer);

  if (! $REQ::ftpCCH ) {
    setRequestErr("FTP Connection failed - terminating");
    syswrite($REQ::LpdSock,"\x7F",1);
    return undef;
  }

  if ( !scalar(@dsnlist) ) {
    setRequestErr("No dataset to receive - terminating");
    $REQ::ftpCCH->quit();
    $REQ::ftpCCH = undef;
    syswrite($REQ::LpdSock,"\x7F",1);
    return undef;
  }


  $fhOut = gzopen($REQ::datafile, "wb") || die("DATA FILE OPEN ERROR $!");
  
  doLog(" now receiving data into ", $REQ::datafile);
  my $bRead = 0;
  $xferOK = 1;
  
  my ($fcode, $fmsg);
  for my $dsn ( @dsnlist ) {
    $REQ::ftpDCH = $REQ::ftpCCH->retr($dsn);

    ($fcode, $fmsg) = getLastFtpMsg();
    
    my $outmsg;
    
    if ($REQ::ftpDCH && $fcode eq '125') {
      $outmsg = "receive for $dsn starting - resp: $fcode - $fmsg";
      doLog($outmsg);

      ($REQ::rbytes, $REQ::wbytes, $REQ::buffer ) = (0, 0, '');
      $REQ::bpos = $REQ::sol = 0;
      while ( readFtpSock() ) {
	my $swlen = $fhOut->gzwrite($REQ::buffer);
	$REQ::wbytes += $swlen;
	last if $swlen ne length($REQ::buffer);
	$REQ::bpos = $REQ::sol = $swlen;
      } 
      $REQ::InBytes += $REQ::rbytes;

      $REQ::ftpDCH->close();

      if (! $REQ::wbytes == $REQ::rbytes ) {
	$outmsg = "receive failed for $dsn - read $REQ::rbytes and write $REQ::wbytes does not match";
	doLog($outmsg);
	$xferOK = 0;
      }

      ($fcode, $fmsg) = getLastFtpMsg();

      $outmsg = "receive for $dsn ended - bytes: $REQ::rbytes resp: $fcode - $fmsg";
      doLog($outmsg);
    } else {
      $outmsg = "receive failed for $dsn - resp: $fcode - $fmsg";
      doLog($outmsg);
    }
    $xferOK = ($fcode eq '250' && $xferOK == 1);
    last unless $xferOK;
  }

  $fhOut->gzclose();
  $REQ::ftpCCH->quit();

  $REQ::job[$jobn]{wbytes} += $REQ::OutBytes = -s $REQ::datafile;
  $REQ::job[$jobn]{bytes}  += $REQ::InBytes;
  doLog("bytes read: $REQ::InBytes written: $REQ::OutBytes xferStat: $xferOK");

  doLog("ERROR - No bytes received during transfer") unless $REQ::InBytes;
  doLog("ERROR - Receive Session failed - ftp status:" .
       " - $fcode - $fmsg") unless $xferOK;

  if (! ($REQ::InBytes && $xferOK) ) {
    unlink $REQ::datafile;
    setRequestErr();
    syswrite($REQ::LpdSock,"\x7F",1);
    return undef;
  }

  if ($REQ::OutBytes > $REQ::InBytes) {
    unlink $REQ::datafile;
    setRequestErr("receive failed for Data file -" .
	  " recvd: "   . $REQ::InBytes .
	  " written: " . $REQ::OutBytes );
    syswrite($REQ::LpdSock,"\x7F",1);
    return undef;
  }

  syswrite($REQ::LpdSock,"\x00",1);

  if (! $REQ::job[$jobn]{CtlFile}) {
    if (! recvJob($jobn) ) {
      unlink $REQ::datafile;
      setRequestErr();
      return undef;
    }
  }

  $REQ::pagcnt = 1;

  map {s/\'//g} @dsnlist;
  (my $CtlStream = $REQ::job[$jobn]{'CtlStream'}) =~ s/^N.*$/'N'.join(' ', @dsnlist)/me;

  doLog("Data file received - bytes" .
	" recvd: "   . $REQ::InBytes .
	" written: " . $REQ::OutBytes .
	" - $fmsg");

  return undef unless setRequestOK($CD::stReceived,
				    $REQ::dbid,
				    $REQ::outpdir."/".$filen.".CNTRL.TXT",
				    $REQ::datafile,
				    $CtlStream);

  return $bRead;

}

sub AS400PSF2 {
  ## -*- Mode: Perl -*- #################################################
  # @(#) $Id: xrLpdServ.pl 2263 2008-09-16 15:28:10Z mpezzi $ 
  #
  # Copyrights(c) EURISKOM s.r.l.
  #######################################################################
  my $version = do { my @r = (q$Id 1.9 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

  my ($jobn, $infnam, $numb) = (shift, shift, shift);
  my @parms = ( @_ );
  doLog("AS400PSF2 " . $version . " Setting XFER_Mode to " . XF_ASPSF);
  return LPRQUEGZ($jobn, $infnam, $numb, XF_ASPSF, @parms);
  

}

__END__
