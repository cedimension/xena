use lib "$ENV{XREPORT_HOME}/perllib";
use strict "vars";

use POSIX qw(strftime);
use FileHandle;
use File::Basename;

use File::Path;
use File::Copy;
use XML::Simple;
use Symbol;

use Data::Dumper;

use XReport;
use XReport::Util;
use XReport::QUtil;
use XReport::Logger;
use XReport::DBUtil;
use XReport::JobREPORT;
use XReport::Spool;

(my $workdir = ($ARGV[0] eq '.' ? `cd` : $ARGV[0])) =~ s/\n$//gs;

my $JRID = $ARGV[1] || die "Unable to get JobReportId\n";
my $JOBID = "XREPORT.REQUEST.$JRID";
my $rptconf_in = "$ENV{XREPORT_SITECONF}\\userlib\\rptconf\\xrReqCatiaPlot.xml";
my $FQcfgfName = $workdir.'/xrReqCatiaPlot.xml';

my $debgRtn = sub { warn map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n"; };
$logrRtn = sub { print map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n"; };

require "$ENV{XREPORT_HOME}/bin/catia_functions.pl";

if (-d $workdir ) {
	my @flist = glob $workdir.'\*'; 
	&$logrRtn( "deleting now\n", join("\n", @flist));
	unlink @flist;
}

# Read, and copy to work dir, configuration from rptconf
my $cfgfhin = new FileHandle("<".$rptconf_in) || die "Unable to access rptconf $rptconf_in\n";
my $cfgfhou = new FileHandle(">".$FQcfgfName) || die "Unable to access rptconf $FQcfgfName\n";
my $cfgtext = '';
while (<$cfgfhin>) {
	my $lout = $_;
	print $cfgfhou $lout;
	$cfgtext .= $lout;
}
close $cfgfhin;
close $cfgfhou;
push @$main::files2arc, $FQcfgfName ;

&$debgRtn("Parsing ", $FQcfgfName, " contents:\n", $cfgtext);
my $reportcfg = XMLin($cfgtext,
		      ForceArray => [ qw(defaults mappings) ], ContentKey => '-content',
		      KeyAttr => {'defaults' => 'name', 'mappings' => 'name'},
		     );

&$logrRtn( "Accessing now $JRID\n");
my ($jr, $FQfileName);
my ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName, $XferEndTime, $remoteFileName);
my ($manfqn, $ixfname);

my $dbr = dbExecute("SELECT * FROM tbl_WorkQueue where ExternalKey = $JRID");
die "unable to fetch $JRID paramenters from queued request" if $dbr->eof();

my $defaults = $reportcfg->{defaults};
my $workid = $dbr->Fields()->Item('WorkId')->Value();

my $sparms = { split("\t", $dbr->Fields()->Item('SrvParameters')->Value()) };
$sparms->{doct} = ( $sparms->{doct} eq 'ECO/SM' ? 'ECO' : 'DIS');
$sparms->{model} =~ s/\$scale/$defaults->{paper_size}/;

my $usrreq  = { %$defaults, %$sparms };

my $usrdest = {
	Lib => $usrreq->{library} || '.',
	Line => $usrreq->{line} || '.',
	Paper => $usrreq->{paper_size} || '.'
};

my $fqdn = (split(/[\n\s]+/, `ping -n 1 -a $ENV{COMPUTERNAME}`))[2];

my $request = join(
	"\n",
	"LOGN: XREPORT REQUEST by $usrreq->{username}",
	"PARM: $usrreq->{model}",
	sprintf("MAIL: %-21s%-8s%-s", @{$usrdest}{qw(Lib Line Paper)}),
	'CMDP: '.join(
		' ',
		($usrreq->{doct}, $JOBID, @{$usrreq}{qw(line OUTPUT_TYPE)}),
		'NOPDF',
		$fqdn,
		$usrreq->{printer} || '.',
		$usrreq->{mailto} || '.'
	)
)."\n";

my $remotefilename = sendCatiaRequest($request, $JRID, $workdir)
  || die "Request to catia for drawing plot format failed - no file returned";

#Opening TAR ARCHIVE
use Archive::Tar;

#$Archive::Tar::DEBUG = 1;
my $targz = Archive::Tar->iter( $remotefilename, 1);
#my $targz = Archive::Tar->iter( $gzh);
die "Cannot open $remotefilename \n" unless $targz;
my $file2lpr;

while( my $f = eval { $targz->() } ) {
  my $fname = $f->name();
  if ( $fname =~ /\.vrf$/i ) {
    $file2lpr = "$workdir/$fname";
    warn "Extracting $file2lpr\n";
    Archive::Tar->extract_file($f, $file2lpr) or warn "Extraction of $fname in $workdir failed";
    last;
  }
}

print "AddFile: $JRID, $workid, $file2lpr\n";

XReport::Spool->AddFile(
  JobSpoolId => $JRID,
  WorkId => $workid,
  InputFileName => $file2lpr,
  PrintDest => "A2E3015"
);

exit 0;
