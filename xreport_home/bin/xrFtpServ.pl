#!/usr/local/bin/perl -w
#######################################################################
# @(#) $Id: xrFtpServ.pl 2249 2008-07-01 00:16:54Z mpezzi $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
my $VERSION = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

use strict;

use XML::Simple;

use Xreport;
use XReport::Util;
use XReport::SUtil;
use XReport::QUtil;
use XReport::JUtil;
use XReport::logger;
use XReport::Receiver::FTPServ;
$main::debug = 1;

exit (XReport::Receiver::processRequest());

sub Child {

  use IO::Socket;
  use IO::Select;

  my %queRtns = (
	      '.default'  => \&XRGATEWAY,
	      'LPRQUEGZ'  => \&XRGATEWAY,
	      'PASSTHROUGH'  => \&PASSTHROUGH,
	     );

  &$logrRtn("now starting child $main::SrvName $main::con");

  my $hndl = new XReport::Receiver::FTPServ(
			       'port'       => $main::SrvPort,
			       'conid'      => $main::con,
			       'SrvName'    => $main::SrvName,
			       'daemonMode' => $main::daemon,
			       'debugLevel' => $main::debug,
			       'prId'       => $main::prId,
			       'pagcnt'     => 0,
			       'logrtn'     => $logrRtn,
			       'passive'    => 0,
			 );

  return undef unless $hndl->Sock( $main::CliSock );
  my ($peeraddr, $peerport) = @{$hndl}{qw(peeraddr peerport)};
  $hndl->{'cinfo'}->{'H'} = $peeraddr;

  $hndl->FTPCsend("220 $main::SrvName FTP server ready.") || die "Unable to begin FTP Session" ;
  my $dcnt = my $oulen = my $inlen = 0;
  while ( defined(my $data = $hndl->FTPrecv()) ) {
    if ( exists( $data->{data} ) ) {
      $dcnt++;
      if ( !exists($hndl->{outfh}) ) {
        $hndl->DoLog("Output FileHandle not initialized - transfer terminating");
        $hndl->FTPCsend("501 Error during file allocation");
        last;
#      	last unless $hndl->{outfh} = $hndl->initJobReport();
#      	$hndl->FTPCsend("125 Storing data set ".(split(/[\\\/]+/, $hndl->{dfilen}))[-1]);
      }
      
      my $fhOut = $hndl->{outfh};
      my $bflen = length(my $buff = $data->{data});
      if ( $bflen > 0 ) {
       $hndl->DoLog("transfer begins - first $bflen bytes of data received") if ($oulen == 0); 
	   $inlen += $bflen;
	   my $wb = $fhOut->write($buff);
	   $oulen += $wb if $wb;
	   if ( !$wb || $wb != $bflen ) {
           $hndl->FTPCsend("501 Error during store");
	       $hndl->DoLog(" Error during  write : $!");
	       last;
	     }
       }
       else {
	       $fhOut->Close();
	       $hndl->DoLog("Receive for data stream ended - received $inlen bytes in $dcnt data packets");
	       $hndl->outbytes($oulen);
	       $hndl->dbytes($inlen);
	       $hndl->{dfilesz} = my $dfilesz = -s $hndl->{dfilen};
	
	       $hndl->DoLog("Data file received - bytes " .
		     " recvd: " . $inlen .
		     " written: " . $hndl->{dfilesz} );
        
	       my $dbc = new XReport::JUtil();
            unless ($dbc->JCAdd($hndl->{'dbid'}, $hndl->CtlStream())) {
           $hndl->FTPCsend("501 Duplicate stream error");
	       $hndl->DoLog(" Error during  Job Control ADD - FTP process aborting");
	       last;
        }

        unless ( $hndl->setRequestOK($CD::stReceived,
				     $hndl->{'dbodir'}."/".$hndl->{'dfilen'}.".CNTRL.TXT",
				     $hndl->{'dfilen'}) ) {
           $hndl->FTPCsend("501 Errore during store finalization");
	       $hndl->DoLog(" Error during Dbase Update - FTP process Aborting");
	       last;
	    }
	
	    delete $hndl->{'outfh'};
      }
    }

    if ( exists($data->{cmd}) ) {
      my ($STMT, $PARMS) = @{$data->{cmd}};
      #  while ( defined($hndl->FTPCrecv(my $STMT, my $PARMS)) ) {
      $PARMS = '' if $STMT eq 'QUIT';
      $hndl->DoLog('Received: STMT => ' . $STMT . ' <= PARMS => ' .  $PARMS. ' <=');
#      warn 'Received: STMT => ', $STMT,' <= PARMS => ', $PARMS, ' <=', "\n";
      if ($STMT eq 'QUIT') {
            $hndl->FTPCsend("221 FTP Session Completed."); 
            last;
      }
      
      my $STMThndl = '_cmd_'.$STMT;
      if ( my $coderef = $hndl->can($STMThndl) ) {
    	if ( $STMT eq 'USER' or $hndl->{'FTPuser'} or ($hndl->{'FTPusertemp'} and $STMT eq 'PASS') ) {
	     last unless defined(&$coderef($hndl, $PARMS));
    	}
    	else {
    	  if ( $hndl->{'FTPusertemp'} ) {
    	    $hndl->FTPCsend("530-Specify password for $hndl->{'FTPusertemp'}.") || last;
    	  }
    	  $hndl->FTPCsend("530 Not logged in.") || last;
    	}
      }
      elsif ( !$hndl->{'FTPuser'} ) {
    	 $hndl->FTPCsend("530 Not logged in.") || last;
      }
      else {
	    $hndl->FTPCsend("502 Command not implemented.") || last;
      }
    }
  }
  $hndl->DoLog("Ftp task handler terminated - $hndl->{rbytes} bytes received - $oulen bytes written");
  
  return 0;
}
