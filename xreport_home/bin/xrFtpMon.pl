
use lib("$ENV{'XREPORT_HOME'}/perllib");

use strict vars;

use Data::Dumper;

use Compress::Zlib;
use Net::FTP;
use File::Basename;

use XReport;
use XReport::QUtil;
use XReport::DBUtil;
use XReport::Util; use XReport::Util qw(InitServer TermServer);

my $debugLevel   = 0;
my $daemonMode = 0;
my $SrvName;

my ($basedir, $perl, $perllib, $gzip, $todir, @hosts); 

InitHousekeeping();

sub setXferFileA {
  my ($queuename, $progr, $datetime) = @_;

  $datetime = GetDateTime() unless ($datetime && $datetime =~ /^\d{14}$/);
  my ($curryear, $currday, undef) = unpack("a4a4a*", $datetime);

#  my $xrspool = getConfValues('LocalPath')->{'L1'}."/IN";
#  $xrspool =~ s/^file:\/\///;
#  if (! -e $xrspool."/".$curryear."/".$currday ) {
#    mkdir $xrspool."/".$curryear if (! -e $xrspool."/".$curryear);
#    mkdir $xrspool."/".$curryear."/".$currday;
#    die "unable to create $xrspool $curryear $currday dir\n" 
#	 unless 
#	(-e $xrspool."/".$curryear."/".$currday);
#  }
#
#  my $filen = "$curryear/$currday/$queuename.$datetime.$progr.DATA.TXT";
#  
#  return ($filen, $xrspool, $datetime);

  my $filen = "$queuename.$datetime.$progr.DATA.TXT";
  my $tgt = XReport::ARCHIVE::newTargetPath('L1', "IN/$curryear/$currday");
#  $hndl->{LocalPathId_IN} = $tgt->{LocalPathId};
  my @xrspool = split /(?<![\/\\])[\/\\](?![\/\\])/, $tgt->{fqn};
  
  
   return (join('/', splice(@xrspool, -2))."/".$filen, join('/', @xrspool), $datetime);
}

sub QDelete {
  my $jrid = shift;
  
  dbExecute("
    DELETE from tbl_JobReports 
	where 
	 JobReportId=$jrid
  ");
  
  dbExecute("
    DELETE from tbl_WorkQueue 
	where 
	 ExternalTableName='tbl_JobReports' 
	 AND ExternalKey = $jrid AND TypeOfWork = 1
  ");
}

sub FtpLoop {
  &$logrRtn("FtpLoop ENTRY");

  my %args = (
	'maxLS'   => 240,
    'sleepLS' => 60,
    'debug' => 0,
	%{$_[0]}
  );
  
  my ($host, $hostaddr, $hosttype, $user, $pass, $dir, $proc, $maxLS, $sleepLS, $debug) 
   =
  @args{qw(host hostaddr hosttype user pass dir proc maxLS sleepLS debug)};  
  
  my ($ftp, $rc, $fmFile, $toFile) = ();
  $debug = 0;
  
  $ftp = Net::FTP->new($hostaddr, Debug => $debug);
  if ( !$ftp ) {
    return -1;
  }

  $rc = $ftp->login($user, $pass);
  if (!$rc) {
    $ftp->quit();
	return -1;
  }
  my $dirList = $dir;
  
  for (1..$maxLS) {
    for $dir (@$dirList) {
	  my ($cwd, $prfx, $suffix) = @{$dir}{qw(cwd prfx suffix)}; my $MVS = $hosttype =~ /MVS/i;

      my $cwdFtp = ($MVS) ? "'$cwd'" : $cwd;
      my $lsFtp = ($MVS) ? "$prfx*$suffix" : "$prfx*$suffix";

	  my ($rename, $delete) = @{$dir}{qw(rename delete)};
	  
      $ftp->cwd($cwdFtp);
  
      my $lsprfx = $prfx; $lsprfx = substr($lsprfx,0,7) if length($lsprfx) >= 8; 
      #test if disconnected
      if ( $ftp->pwd() eq '' ) {
        $ftp->quit();
        return -1;
      }
	  ### remember: Sometimes ls $prfx\* gives nothing with IBM FTP (configuration error ??)
      my @files = grep(/^$prfx.*$suffix$/i, $ftp->ls($lsFtp));
	  #print "======\n", join("\n", @files), "\n";
      
      &$logrRtn("ls code = ". $ftp->code());
      if ( !scalar(@files) and $ftp->code() ne "250" and $ftp->code() ne "550" ) {
        $ftp->quit();
        return -1;
      }
  
      for $fmFile (@files) {
        if ( $fmFile !~ /^$prfx.*/ ) {
          $ftp->quit();
          die("ListError: FILE $fmFile DOESN'T START WITH $prfx !!.");
        }
        my $JobReportName = &{$dir->{'checkFile'}}($fmFile);
        next if $JobReportName eq "";
		
        my $dt = GetDateTime(); my $dbr;
		
		my $fmFile_ = ($MVS) ? "$cwd.$fmFile" : "$cwd/$fmFile" ;

        $dbr = dbExecute(
		  "SELECT * from tbl_JobReports where RemoteFileName = '$fmFile_'"
		);
        
		### prepare transfer
		
		my ($JobReportId, $Status);
		
		if ( !$dbr->eof() ) {
		  ($JobReportId, $Status) = $dbr->GetFieldsValues(qw(JobReportId Status)); $dbr->Close();
		  if ( $Status >= 16 and $rename ) {
		    my $toFile_ = &$rename( $fmFile_ ); 
		    $rc = $ftp->rename("'$fmFile_'", "'$toFile_'");
		    if ( $rc != 1 or $ftp->code() != 250 ) {
              &$logrRtn("RENAME ERROR for rc=$rc $fmFile_ $toFile_. NOW ENDING !! ftpcode=".$ftp->code());
              $ftp->quit();
              die("FtpError: RENAME Error");
		    }
		    next();
		  }
		  &$logrRtn("RESTARTING TRANSFER FOR FILE $fmFile_");
		}
		else {
          ### usare new Xfer
          $JobReportId = QCreate XReport::QUtil(
                         SrvName        => $SrvName,
                         JobReportName  => $JobReportName,
                         LocalFileName  => $JobReportName . $$ . '001',
                         RemoteHostAddr => $host,
                         RemoteFileName => $fmFile_,
                         XferStartTime  => "GETDATE()",
                         XferMode       => ($MVS) ? $CD::xmFTPc : $CD::xmLpr,
                         XferDaemon     => $SrvName,
                         Status         => $CD::stAccepted,
                         XferId         => 123,
                         );
		}
        ## ---------------

        my ($toFile, $todir, $datetime) = setXferFileA($JobReportName, $JobReportId);
        
		my $toFile_ = "$todir/$toFile";

        unlink glob("$toFile_\*");
		### end prepare transfer

        ### set mode
        if ( $MVS ) {
		  $rc = $ftp->quot("mode c");
          if (!$rc) {
            $ftp->quit();
	        return -1;
          }
          $rc = $ftp->binary();
          if (!$rc) {
            $ftp->quit();
	        return -1;
          }
	    }
        
        &$logrRtn("TRANSFER START $fmFile_ $fmFile $toFile");
		my $INPUT = $ftp->retr($fmFile); $rc = $ftp->code();
		if ( !$INPUT or ($rc != 125 and $rc != 225) ) {
	      if ( $rc == 450 ) {
		    &$logrRtn("FILE NOT AVAILABLE \"$fmFile\" ftpcode=".$rc);
			### reset mode
            if ($MVS) {
		      $rc = $ftp->quot("mode s");
              if (!$rc) {
                $ftp->quit();
	            return -1;
              }
              $rc = $ftp->ascii();
              if (!$rc) {
                $ftp->quit();
	            return -1;
              }
            }
			next;
		  }
		  &$logrRtn("ERROR AT CMD RETR for file \"$fmFile\" ftpcode=".$rc);
		  QDelete($JobReportId); 
          $ftp->quit();
	      return -1;
		}
		my $OUTPUT = gzopen("$toFile_\.PENDING", "wb");
		if ( !$OUTPUT ) {
	 	  die "ERROR AT GZOPEN for file \"$toFile\" $!"; 
		}
		
		my ($buff, $br, $tbr, $bw, $tbw);
		while(1) {
		  $br = $INPUT->read($buff, 32768); last if !$br; $tbr += $br;
		  $bw = $OUTPUT->gzwrite($buff); $tbw += $bw;
		}
		my $atOff = $OUTPUT->gztell(); $atOff = 0x100000000 + $atOff if $atOff < 0;
		if ( $tbw != $tbr or $atOff != $tbw ) {
		  &$logrRtn("BYTES WRITTEN $tbw/$atOff NOT EQUAL TO BYTES READ $tbr");
		}
		$OUTPUT->gzclose(); $INPUT->close(); $rc = $ftp->code();
		if ( $rc != 250 ) {
		  &$logrRtn("TRANSFER ENDED WITH ERROR \"$fmFile\" \"$toFile\" ftpcode=$rc");
		  QDelete($JobReportId); unlink "$toFile_\.PENDING";
          $ftp->quit();
	      return -1;
		}

		$rc = rename("$toFile_\.PENDING", "$toFile_\.gz");
	    if ( !$rc ) {
	      &$logrRtn("RENAME Error ($toFile_\.PENDING", "$toFile_\.gz) $!");
          $ftp->quit();
		  die ("FileError: RENAME Error $!");
	    }
	    $toFile .= ".gz";
		
        &$logrRtn("TRANSFER ENDED NORMALLY FROM $fmFile_");
        &$logrRtn(" INTO \"$toFile_\.gz\"");

        ### reset mode
        if ($MVS) {
		  $rc = $ftp->quot("mode s");
          if (!$rc) {
            $ftp->quit();
	        return -1;
          }
          $rc = $ftp->ascii();
          if (!$rc) {
            $ftp->quit();
	        return -1;
          }
        }
		### end transfer
        
        ### rename
        my $req = QUpdate XReport::QUtil(
                     XferEndTime    => 'GETDATE()',
                     Status         => $CD::stReceived,
                     JobName        => 'XYZ00001',
                     JobNumber      => 'JOB12345',
                     JobReportName  => $JobReportName,
                     XferRecipient  => '',
                     RemoteFileName => $fmFile_,
                     LocalFileName  => $toFile,
                     XferPages      => 0,
                     XferInBytes    => $tbr,
                     XferMode       => ($MVS) ? $CD::xmFTPc : $CD::xmLpr,
                     Id             => $JobReportId,
                    );
        ### ------
        
		if ( $rename ) {
		  my $toFile_ = &$rename( $fmFile_ ); 
		  $rc = $ftp->rename("'$fmFile_'", "'$toFile_'");
		  if ( $rc != 1 or $ftp->code() != 250 ) {
            &$logrRtn("RENAME ERROR for rc=$rc $fmFile_ $toFile_. NOW ENDING !! ftpcode=".$ftp->code());
            $ftp->quit();
            die("FtpError: RENAME Error");
		  }
		}
		else {
          if ( 0 and !$ftp->delete($fmFile) ) {
            &$logrRtn("DELETE ERROR for $fmFile. NOW ENDING !! ftpcode=".$ftp->code());
            $ftp->quit();
            die("FtpError: DELETE Error ftpcode=".$ftp->code());
          }
          else {
            &$logrRtn("FILE $fmFile DELETED SUCCESSFULLY.");
          }
		}
      }
    }

    &$logrRtn("SLEEPING NOW $sleepLS seconds");
	sleep $sleepLS;
  }

  $ftp->quit;

  return 1;
}

eval {
  while(1) {
    my $rc = FtpLoop( $hosts[0] );

    &$logrRtn("FtpLoop END $rc");
  
    sleep ( ($rc < 0) ? 120 : 60 );
  }
};
&$logrRtn($@);

#######################################################################
#
#######################################################################

sub InitHousekeeping {
  InitServer(); $SrvName = getConfValues('SrvName'); my $hosts;
  
  ($basedir, $perl, $perllib, $gzip, $todir, $hosts) 
    = 
  getConfValues('basedir', 'perl', 'perllib', 'gzip', 'todir', 'hosts');
  
  $hosts = $hosts->{'host'};
  if ( ref($hosts) eq 'HASH' ) {
    @hosts = ($hosts);
  }
  else {
    @hosts = @$hosts;
  }

  print join("\n", $todir, Dumper(@hosts)), "\n";
  
  my $dirList = $hosts[0]->{dir}; my $dir;
  for $dir (@$dirList) {
    my $if = $dir->{'if'}; my $code = "sub {\n  my \$fl = shift;\n";
	$if = [$if] if ref($if) ne "ARRAY";
    for (@$if) {
	  $code .= "  return '$_->{'jr'}' if \$fl =~ /$_->{'re'}/o;\n";
	}
	$code .= "  return undef;\n}\n";
	$dir->{'checkFile'} = eval($code);
	die "INVALID checkFile EXPRESSION $code / $@" if $@;
	if ( $dir->{rename} ) {
	  $code = "
	    sub { 
		  my \$t = \$_[0]; \$t =~ s$dir->{rename}; 
		  return \$t; 
		}
	  ";
	  $dir->{rename} = eval "$code";
	  die "INVALID rename EXPRESSION $code / $@" if $@;
	}
  }
}
