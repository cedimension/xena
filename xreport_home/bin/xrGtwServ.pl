#!perl
#######################################################################
# @(#) $Id$ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
my $VERSION = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

use strict;

use lib($ENV{'XREPORT_HOME'}."/perllib");
use XML::Simple;

use Xreport;
use XReport::Util qw($logger $logrRtn);
use XReport::SUtil;
use XReport::QUtil;
use XReport::JUtil;
use XReport::logger;
use XReport::Receiver::GTWServ;

exit (XReport::Receiver::processRequest());

sub Child {

  use IO::Socket;
  use IO::Select;

  my %queRtns = (
	      '.default'  => \&XRGATEWAY,
	      'LPRQUEGZ'  => \&XRGATEWAY,
	      'PASSTHROUGH'  => \&PASSTHROUGH,
	     );

  &$logrRtn("now starting child $main::SrvName $main::con");

  my $hndl = new XReport::Receiver::GTWServ(
					    'port'       => $main::SrvPort,
					    'conid'      => $main::con,
					    'SrvName'    => $main::SrvName,
					    'daemonMode' => $main::daemon,
					    'debugLevel' => $main::debug,
					    'prId'       => $main::prId,
					    'XferMode'   => $CD::xmPsf,
					    'pagcnt'     => 0,
					    'logrtn'     => $logrRtn,
					   );
  
  return undef unless $hndl->Sock( $main::CliSock );
  my ($peeraddr, $peerport) = @{$hndl}{qw(peeraddr peerport)};
  $hndl->{'cinfo'}->{'H'} = $peeraddr;

  my $cmdlen = $hndl->recvcmd(my $cmd, 250);

  return 1 unless defined($cmd) and $cmdlen > 0;

  my ($req, $prtQueue) = ($cmd =~ /^(.)(.*)\x0a$/o);
  my ($reqname, $queuetype, $queuename) = $hndl->ParseQue($prtQueue);

  if ($req eq "\x03" or $req eq "\x04") {
    $hndl->sendack("receive status cmd aknowledged - processing $prtQueue");
    return 0;
  }

  if ($req ne "\x02") {
    $hndl->DoLog("Unexpected cmd received: " . unpack("H*", $cmd));
    return 1;
  }
 
  my $xfrRtn = ($queRtns{$queuetype} or $queRtns{'.default'});

  $hndl->sendack("Receive JOB cmd received - Transfer for $queuename ($queuetype) starting");
  
  my $infile = $hndl->startJobRecv($reqname) or die "Error receive Job control data - Aborting";
  $hndl->DoLog("Job Control Data received ($infile) - Starting now $queuetype handler");
  
  my $ifbytes = &$xfrRtn($hndl, $reqname);
  $hndl->DoLog("Job $queuetype handler terminated - $ifbytes data bytes written");
  
  return 0;
  
}

sub PASSTHROUGH {
  my $hndl = shift;
  
  my $fhOut;
  my $CtlFile = $hndl->CtlFileN() . ".CNTRL";
  my $DtaFile = $hndl->DtaFileN() . ".DATA";

  open($fhOut, ">".$CtlFile ) || do {
    $hndl->DoLog("DATA FILE OPEN ERROR: $!");
    return undef;
  };
  binmode $fhOut;
  my $wb = syswrite($fhOut, $hndl->CtlStream() );

  my ($dfilen, $dbodir, $dbdatet) = setXferFileA(@{$hndl}{qw(queuetype queuename conid)});
   @{$hndl}{qw(dfilen dbodir dbdtime)} = ($dfilen, $dbodir, $dbdatet); 
  
  close($fhOut);
  $hndl->DoLog("N2O", "Cntl subcommands received: ", -s $CtlFile, " bytes written to\n--- $CtlFile");

  open($fhOut, ">".$DtaFile ) || do {
    $hndl->DoLog("N2O", "DATA FILE OPEN ERROR: $!");
    return undef;
  };
  binmode $fhOut;

  if ( !$hndl->sendack("Data Stream begin - from: $hndl->{'dfilen'} to: $DtaFile", pack("N", $main::con)) ) {
    setRequestErr("Start data stream request failed");
    return undef;
  }

  my $dcnt = my $oulen = my $inlen = 0;
  while ( 1 ) {

    my $bfrlen = $hndl->recvdata(my $buff);
    $dcnt += 1;
    return undef unless defined($bfrlen);
    
    last if ($bfrlen == 0);

    $inlen = $hndl->dbytes($hndl->dbytes() + $bfrlen);

    my $wb = syswrite($fhOut, $buff) || do {
      $hndl->DoLog("N2O", " Error during  write : $!");
      last;
    };

    last if $wb ne length($buff);

    $oulen += $wb;
    $hndl->sendack("Data buffer $dcnt received: $wb bytes") unless ($bfrlen == 0);

  }
  
  close($fhOut);
  $hndl->DoLog("N2O" . "Job Received - inlen: $inlen - oulen: $oulen");
  foreach my $nm ( keys %{ $hndl } ) {
    $hndl->DoLog("--- $nm:" . $hndl->{$nm}) unless ($nm eq 'cdata');
  }

  return undef unless ($oulen == $inlen);
  my $outsize = -s $DtaFile;

  $hndl->DoLog("N2O", "Data file received: $outsize bytes written to\n--- $DtaFile");

  return $outsize;

}

sub XRGATEWAY {
  ## -*- Mode: Perl -*- #################################################
  # @(#) $Id$ 
  #
  # Copyrights(c) EURISKOM s.r.l.
  #######################################################################
  my $hndl = shift; my $reqname = shift;
  my $version = do { my @r = (q$Revision: 1.9 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

  my $fhOut = $hndl->initJobReport() || return undef;
  my $reqid = $hndl->{dbid};
  if ( !$hndl->sendack("Data Stream begin - from: $hndl->{cinfo}->{'dfilen'} to: $hndl->{'dfilen'}", pack("N", $reqid)) ) {
    $hndl->setRequestErr("Start data stream request failed");
    return undef;
  }

  my $dcnt = my $bfrlen = my $oulen = my $inlen = 0;
  while ( defined($bfrlen = $hndl->recvdata(my $buff)) ) {

    $dcnt += 1;
    
    last unless $bfrlen;

    $inlen += $bfrlen;

    my $wb = $fhOut->write($buff) || do {
      $hndl->DoLog("N2O", " Error during  write : $!");
      last;
    };

    $oulen += $wb;
    last if $wb ne length($buff);

    $hndl->sendack("Data buffer $dcnt received: $wb bytes") unless ($bfrlen == 0);

  }

  $fhOut->Close();

  $hndl->outbytes($oulen);
  $hndl->dbytes($inlen);
  $hndl->{dfilesz} = my $dfilesz = -s $fhOut->{datafile};

  if (!defined($bfrlen) or ($oulen ne $inlen)) {
    unlink $fhOut->{datafile};
    # receive failed for Data file:  recvd: 65535 output: 800 written: 813
    $hndl->setRequestErr("receive failed for Data file: " . 
		  " recvd: "   . $inlen .
		  " output: "  . $oulen .
		  " written: " . $dfilesz );
    return undef;
  }

  $hndl->DoLog("Data file received - bytes " .
	" recvd: " . $inlen .
	" written: " . $dfilesz );
  
  return undef unless $hndl->setRequestOK(
				   $CD::stReceived,
				   $hndl->{dbodir}."/".$hndl->{dfilen}.".CNTRL.TXT",
				   $fhOut->{datafile},
				   $reqid,
				   $hndl->CtlStream() );
  
  return undef unless $hndl->{JCH}->JCAdd($reqid, $hndl->CtlStream());
  $hndl->sendack("Output receive completed correctly for $hndl->{cinfo}->{reqname}", $hndl->{'reportname'});
  #return $dfilesz;
  return $dfilesz if ( $hndl->wait4ready() );
  
  return undef;
  
}

;

