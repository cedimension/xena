#!/usr/bin/perl -w
package XReport::Distillr;

use 5.006;
use strict;
use warnings;

require Exporter;
require DynaLoader;

our @ISA = qw(Exporter DynaLoader);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use XReport::Distillr ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} }, qw( FileToPdf ) );

our @EXPORT = qw(
	
);
our $VERSION = '0.01';

use Win32::Registry;

sub new {
  my $class = shift;
  my $self = {};
  $main::HKEY_LOCAL_MACHINE->Open
    (
#       "SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\App Paths", 
     "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths", 
     my $distreg
    )
      or die("CANNOT OPEN Distiller REGISTRY ENTRY: $^E");
  $distreg->QueryValue("AcroDist.exe", my $progr);
  (my $version) = $progr =~ /Acrobat +([\d\.]+)/;
  i::logit("Acrobat distiller initialized progr: $progr - version: $version");
  print "Acrobat distiller initialized progr: $progr - version: $version\n";
  @{$self}{qw(progr version)} = ($progr, $version);
  
  bless $self, $class;
}

sub FileToPdf {
  my ($self, $fname, $FQpdfName) = (shift, shift, shift);
  $self = new($self) unless ref($self);

  my ($progr, $version) = @{$self}{qw(progr version)};
    
  $self->{progr} = $progr; # = "c:\\Program Files\\gs\\gs8.64\\bin\\gswin32c.exe";
  my $dcmd = "\"$progr\" /N /Q /O \"$FQpdfName\" \"$fname\"";

  my $psize = '-dEPSFitPage=true -sPAPERSIZE=a3';
  my $gsdcmd = join(' '
	       , "\"$progr\"" 
	       , "-q"
	       , "-dNOPAUSE"
	       , "-dBATCH"
	       , "-sDEVICE=pdfwrite"
	       , "-sOutputFile=\"$FQpdfName\""
#	       , "-dPDFWRDEBUG"
	       , "-r600"
	       , "-dPDFSETTINGS=/prepress"
#	       , "-dPDFFitPage"
	       , "-dLZWEncodePages=false"
	       , "-dUseFlateCompression=false"
	       , "-dParseDSCComments=false"
#	       , "-sPAPERSIZE=a3"
#	       , "-dDEVICEWIDTHPOINTS=582 -dDEVICEHEIGHTPOINTS=412"
	       , $psize
	       , "-dAutoRotatePages=/PageByPage"
	       , "-c .setpdfwrite"
#	       , "-c \"\<\</Orientation 3 \>\> setpagedevice\""
	       , "-f \"$fname\""
	      );
  i::logit("Calling Acrobat distiller: $dcmd");
  my $DistRetCode = system($dcmd);
}

1;

package main;

use lib "$ENV{XREPORT_HOME}/perllib";
use strict "vars";

use POSIX qw(strftime);
use FileHandle;
use File::Basename;

use File::Path;
use File::Copy 'cp';
#use Compress::Zlib;
use XML::Simple;

use Data::Dumper;

use XReport;
use XReport::Util;
use XReport::Logger;
use XReport::DBUtil;
use XReport::JobREPORT;
use XReport::PDF::DOC; 
use XReport::ARCHIVE;
#use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

(my $workdir = ($ARGV[0] eq '.' ? `cd` : $ARGV[0])) =~ s/\n$//gs;

my $JRID = $ARGV[1] || die "Unable to get JobReportId\n";
my $ArchiveContentFN = '_ArchiveContent.tsv';

#if (-d $workdir ) {
#  $workdir =~ s/\//\\/sig;
#  my @flist = glob $workdir.'\*'; 
#  &$logrRtn( "deleting now\n", join("\n", @flist));
#  unlink @flist;
#}

my $debgRtn = sub { warn map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n"; };
$logrRtn = sub { print map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n"; };

sub buildParser {
  my ($outvars, $invars) = (shift, shift);
  
  my $outxform = { ($outvars =~ /(\S+)(?:\s+(\w+)\.){0,1}/osgi)  };
  my $inxform = { ($invars =~ /(\S+)(?:\s+(\w+)\.){0,1}/osgi)  };
  &$debgRtn("outxform:\n", Dumper($outxform)); 
  &$debgRtn("inxform:\n", Dumper($inxform)); 
  require XReport::INFORMAT if scalar(keys %$inxform);
#  require XReport::OUTFORMAT if scalar(keys %$outxform);

  my $outlist = [grep !/\w+\./, split(/\s+/, $outvars) ];
  my $inlist = [grep !/\w+\./, split(/\s+/, $invars) ];
  
  my $code = "sub { my \$input = shift; return {\n";
  for my $j ( 0..$#{$outlist} ) {
    
    my ($outvarn, $invarn) = ($outlist->[$j], $inlist->[$j]);
    &$debgRtn("Checking $outvarn => $invarn");
    my $invalue = ($invarn =~ /^#(.*)?#$/ ? "'$1'" : 
		   $inxform->{$invarn} && XReport::INFORMAT->can($inxform->{$invarn}) ? 
		   'XReport::INFORMAT::'."$inxform->{$invarn}(\\\$input->{$invarn})" : 
		   "\$input->{$invarn}");
    $code .= $outvarn . ' => ' . ($invarn =~ /^#(.*)?#$/ ? $invalue . ",\n" : 
				  ($outxform->{$outvarn} && XReport::OUTFORMAT->can($outxform->{$outvarn}) ? 
				   'XReport::OUTFORMAT->'. "$outxform->{$outvarn}($invalue)" : $invalue
				  )
				  . " || '',\n"
				 ); 
  }
  $code .= "};\n}";
  &$debgRtn("CODE:\n", $code); 
  eval($code);
  if ($@) {
    &$logrRtn("CODE:\n", $code); 
    die "Error during building of parsing routine - $@";
  }
  return (eval($code), @$outlist);
}

sub getFH {
}

&$logrRtn( "Accessing now $JRID\n");
my ($jr, $FQfileName);
my ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName, $XferEndTime);
if ( $JRID =~ /^\d+$/) {
  $jr = XReport::JobREPORT->Open($JRID, 1);
  warn "==>here\n";
  ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName, $XferEndTime) = 
    $jr->getValues(qw(LocalFileName SrvName RemoteHostAddr XferMode TargetLocalPathId_IN XferDaemon JobReportName XferEndTime));
  my $JRAttrs;
  @{$JRAttrs}{qw(XferEndTime UserRef UserTimeRef UserTimeElab)} = ($XferEndTime, '', $XferEndTime, $XferEndTime);
  &$debgRtn("$JRID Attributes:\n", $JRAttrs);  
  
  $jr->deleteElab();
  
  my $cfgvars = {};
  @{$cfgvars}{qw(LocalFileName SrvName RemoteHostAddr XferMode
		 TargetLocalPathId_IN XferDaemon JobReportName)} = ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName);
  
  $tgtIN = 'L1' unless $tgtIN;
  
  my $rptconf_in = $jr->getFileName('PARSE');
  my $FQcfgName = $workdir.'/'. basename($jr->getFileName('LOGPARSE'));
  my $cfgfhin = new FileHandle("<".$rptconf_in) || die "unable to access rptconf $rptconf_in\n";
  my $cfgfhou = new FileHandle(">".$FQcfgName) || die "unable to access rptconf $FQcfgName\n";
  my $cfgtext = '';
  while (<$cfgfhin>) {
    my $lout = $_;
    $lout =~ s/\$(\w+)/$cfgvars->{$1}/sg;
    print $cfgfhou $lout;
    $cfgtext .= $lout;
  }
  close $cfgfhin;
  close $cfgfhou;
  
  &$debgRtn("Parsing ", $FQcfgName, " contents:\n", $cfgtext);
  my $reportcfg = XMLin($cfgtext,
			ForceArray => [ qw(index loadtar op ix) ],
			KeyAttr => {'index' => 'name', 'loadtar' => 'name', 'op' => 'name', ix => 'name'},
		       );
  
  ($FQfileName = getConfValues('LocalPath')->{$tgtIN}."/IN/$fileName") =~ s/^file:\/\///;
  die "Cannot access $FQfileName\n" unless -f $FQfileName;
#  (my $FQcntlName = $FQfileName) =~ s/DATA\.TXT\.gz$/CNTRL.TXT/osi;
#  die "Cannot access $FQcntlName\n" unless -f $FQcntlName;
}
else {
  $jr = new XReport::JobREPORT();
  $FQfileName = $ARGV[1];
}

use IO::Zlib qw(:gzip_external 0);
use Data::Dumper;

(my $manfqn = $workdir.'/'.basename($jr->getFileName('IXFILE', '$$MANIFEST$$'))) =~ s/\//\\/g;
my $manifest = new FileHandle(">$manfqn");

(my $fxrfqn = $workdir.'/'. basename($jr->getFileName('FILXREF'))) =~ s/\//\\/g;
my $filxref = new FileHandle(">$fxrfqn");
binmode $filxref;

(my $pxrfqn = $workdir.'/'. basename($jr->getFileName('PAGEXREF'))) =~ s/\//\\/g;
my $pagxref = new FileHandle(">$pxrfqn");

my $buffer;
&$logrRtn("Opening ", $FQfileName);
#my $gzh = new IO::Zlib($FQfileName, 'rb');

#Opening TAR ARCHIVE
use Archive::Tar;
use Win32::Registry;

#$Archive::Tar::DEBUG = 1;
my $targz = Archive::Tar->iter( $FQfileName, 1);
#my $targz = Archive::Tar->iter( $gzh);
die "Cannot open $FQfileName \n" unless $targz;

my $FromPage = 1;
my $ParsedPages;
print $filxref "StartItem\t$JobReportName\t\$\$\$\$\t\$\$\$\$\tPage=$FromPage\n"; 

my $fct = 0;
my $acrodist = new XReport::Distillr();
my $drawings = [];
my $info = { JobReportID => $JRID, Group => 'catia', ContentType => 'Application/pdf' };
while( my $f = eval { $targz->() } ) {
  my $fname = $f->name();
  Archive::Tar->extract_file($f, "$workdir/$fname") or warn "Extraction of $fname failed";
  if ( $fname =~ /\.ps$/i ) {
    warn "Extracting $fname\n";
    #    Archive::Tar->extract_file($f, "$workdir/$fname") or warn "Extraction of $fname failed";
    
    (my $wrk = ( $workdir eq '.' ? `cd` : $workdir)) =~ s/\n$//gs;
    $wrk =~ s/\//\\/g;
    my $FQpdfName = $wrk."\\". basename($jr->getFileName('PDFOUT'), $fct++);
    
    warn "INPUT file $fname will produce $FQpdfName\n";
    my $DistRetCode = $acrodist->FileToPdf("$wrk\\$fname", $FQpdfName);
    &$logrRtn("\"$acrodist->{progr}\" returned $DistRetCode");  

    my $doc = XReport::PDF::DOC->Open($FQpdfName);
    my $totPages = $doc->TotPages();
    $ParsedPages += $totPages;
    $doc->Close();
    my $toPage = ($FromPage - 1) + $totPages;
    @{$info}{qw(FromPage ForPages)} = ($FromPage, $totPages);
    print $pagxref "File=$fct From=$FromPage To=$toPage\n"; 
    #  print $manifest "PDFOUT.$fct\t", basename($outnam), "\t$totPages\n"; 
    push @$main::files2arc, $FQpdfName;
    $FQpdfName =~ s/\.pdf$/\.XREF/i;
    push @$main::files2arc, $FQpdfName;
  
    $FromPage += $totPages;
  }
  elsif ( $fname =~ /^XREPORT\.REQUEST\.[^\.]+\.txt$/ ) {
    my $buff = $f->get_content() or warn "Extraction of $fname failed";
    foreach ( grep /^PARM|CMDP/, split /\n/, $buff) {
      if ( /^PARM/ ) { 
	my ($rawdata) = ($_ =~ /^PARM:\s+([^\s].*)$/); 
	my ($drawnum, $sheet, $issue, $page_size, $status) = split(/\s+/, $rawdata, 5);
	($info->{sheet} = $sheet) =~ s/^0+//;
	($info->{issue} = $issue) =~ s/-//g;
	@{$info}{qw(status type)} = ( $status =~ /^RELEASED/i ? ('RELEASED', 'NOVPM')
				      : $status =~ /^PRE-RELEASED/i ? ('PRE-RELEASED', 'NOVPM')
				      : $status =~ /^PR/i ? ('PRE-RELEASED', 'VPM')
				      : $status =~ /^R/i ? ('RELEASED', 'VPM')
				      : ($status, 'UNKNOWN')
				    );
	@{$info}{qw(rawdata drawnum page_size)} = ($rawdata, $drawnum, $page_size ); 
      }
      elsif ( /^CMDP/ ) { 
	@{$info}{qw(doct line)} = (split(/\s+/, $_))[1,3]; 
	$info->{doct} = do {
	  local $_ = $info->{doct};
	  (m/^eco$/i ? 'ECO/SM'
	   : m/^partlist$/ ? 'PARTS LIST'
	   : 'DRAWINGS');
	}
      }
    } 
  }
}
my @ixcols = qw(JobReportID Group doct status line rawdata drawnum sheet issue FromPage ForPages type); 
  my $ixfname = basename($jr->getFileName('IXFILE', 'DRAWINGS'));
  my $ixfh = new FileHandle(">$workdir/$ixfname");
  print $ixfh join("\n", join("\t", @ixcols), join("\t", @{$info}{@ixcols}) ), "\n";
  $ixfh->close();
  push @$main::files2arc, "$workdir\\$ixfname"; 
  print $manifest join("\t", 'DRAWINGS', "$ixfname",1), "\n";

print $filxref "EndItem\t$JobReportName\t\$\$\$\$\t\$\$\$\$\tPage=", ( $FromPage - 1 ), "\n"; 

close $filxref;
#print $manifest join("\t", ('FILXREF', basename($fxrfqn), 2)), "\n";
push @$main::files2arc, $fxrfqn; 

$pagxref->close();
push @$main::files2arc, $pxrfqn ;

$manifest->close();
push @$main::files2arc, $manfqn ;

my $contentfn = "$workdir/\$\$ARC.\$\$MANIFEST\$\$.TXT";

my $contentfh = new FileHandle(">$contentfn") || die "Unable to open list to archive file - $!";
binmode $contentfh;
print $contentfh join("\n", @$main::files2arc), "\n";
$contentfh->close;

my $exit_code = 0;

