#!/usr/bin/perl -w

use strict;
use lib "$ENV{XREPORT_HOME}\\perllib";

use XReport;
use XReport::Util;
use XReport::DBUtil;

use Symbol;
use File::Basename;
use Compress::Zlib;

sub logIt {
	{ local $, = ""; print @_, "\n" }
}

sub debug2log {
	{ local $, = ""; print @_, "\n" }
}
$| = 1;

my $year  = "2013";
my $month = "02";
my $day   = "27";

( my $storagePath = getConfValues('LocalPath')->{'L1'} . "/IN" ) =~
  s/^file:\/\///;
logIt("main - Storage path is $storagePath");

die "main - $storagePath/$year/$month$day doesn't exists\n"
  if ( !-e "$storagePath/$year/$month$day" );

my @files = glob("$storagePath/$year/$month$day/*.{gz,tar}");

foreach my $file (@files) {
	logIt("main - Check file $file");
	my ( $jobReportName, $xferStartTime, $jobReportId ) =
	  ( basename($file) =~ /^(.*)\.(\d{14})\.(\d+)\.DATA/ );
	logIt(
"main - JobReportName is $jobReportName; JobReportId is $jobReportId; XferStartTime is $xferStartTime"
	);

	my $dbr = dbExecute(
		"SELECT * FROM tbl_JobReports where JobReportId = $jobReportId");
	if ( !$dbr->eof() ) {
		my $curJobReportName = $dbr->Fields->Item('JobReportName')->Value();
		die "main - ERROR with JobReportId $jobReportId:"
		  . " JobReportName extracted from filename ($jobReportName) isn't equal to"
		  . " JobReportName getted from DB ($curJobReportName)\n"
		  if ( $jobReportName !~ /^$curJobReportName$/ );
		logIt(
"\tmain - SKIP JobReportId $jobReportId($jobReportName): already exists"
		);
	}
	else {
		logIt(  "\tmain - Start to restore DB data for "
			  . "JobReportName $jobReportName($jobReportId)" );
		my ( $xYear, $xMonth, $xDay, $xHour, $xMin, $xSec ) =
		  ( $xferStartTime =~ /^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})$/ );
		my ($cntrlFile) =
		  "$jobReportName.$xferStartTime.$jobReportId.CNTRL.TXT";
		my $cntrlHandle = gensym();
		my ( $remoteFileName, $jobName, $jobNumber, $jobExecutionTime ) =
		  ( "", "", "", "" );

		logIt(  "\tmain - Get metadata from control file "
			  . dirname($file)
			  . "/$cntrlFile" );
		open( $cntrlHandle, "<" . dirname($file) . "/$cntrlFile" )
		  or die "main - ERROR: cannot open "
		  . dirname($file)
		  . "/$cntrlFile ($!)\n";
		while (<$cntrlHandle>) {
			my $line = $_;
			if ( $line =~ /^-ojdfilen=(.*)$/i ) {
				$remoteFileName = $1;
			}
			if ( $line =~ /^-ojjobname=(.*)$/i ) {
				$jobName = $1;
			}
			if ( $line =~ /^-ojjobnumber=(.*)$/i ) {
				$jobNumber = $1;
			}
			if ( $line =~ /^-ojcrdtim=(.*)\.\d+$/i ) {
				$jobExecutionTime = $1;
				$jobExecutionTime =~ s/-/\//g;
				$jobExecutionTime =~ s/T/ /;
			}
		}
		close($cntrlHandle);

		my $buffer;
		my $gz = gzopen( $file, "rb" )
		  or die "main - ERROR: Cannot open $file: $gzerrno\n";
		my $xferIn;
		$xferIn .= $buffer while $gz->gzread($buffer) > 0;
		die "main - Error reading from $file: $gzerrno"
		  . ( $gzerrno + 0 ) . "\n"
		  if $gzerrno != Z_STREAM_END;
		$gz->gzclose();
		my $xferInBytes = length($xferIn);
		logIt("\tmain - Size of inutp is $xferInBytes");

		eval {
			dbExecute("BEGIN TRANSACTION JOBREPORTADD");
			dbExecute("SET IDENTITY_INSERT tbl_JobReports ON");
			logIt(
				    "\tINSERT INTO tbl_JobReports ("
				  . "JobReportName,"
				  . "JobReportId,"
				  . "LocalFileName,"
				  . "RemoteFileName,"
				  . "JobName,"
				  . "JobNumber,"
				  . "JobExecutionTime,"
				  . "XferStartTime,"
				  . "XferInBytes"
				  . ") VALUES ("
				  . "'$jobReportName',"
				  . "$jobReportId,"
				  . "'$year/$month$day/"
				  . basename($file) . "',"
				  . "'$remoteFileName',"
				  . "'$jobName',"
				  . "'$jobNumber',"
				  . "'$jobExecutionTime',"
				  . "'$xYear/$xMonth/$xDay $xHour:$xMin:$xSec',"
				  . "$xferInBytes" . ")"

			);
			dbExecute(
				    "INSERT INTO tbl_JobReports ("
				  . "JobReportName,"
				  . "JobReportId,"
				  . "LocalFileName,"
				  . "RemoteFileName,"
				  . "JobName,"
				  . "JobNumber,"
				  . "JobExecutionTime,"
				  . "XferStartTime,"
				  . "XferInBytes"
				  . ") VALUES ("
				  . "'$jobReportName',"
				  . "$jobReportId,"
				  . "'$year/$month$day/"
				  . basename($file) . "',"
				  . "'$remoteFileName',"
				  . "'$jobName',"
				  . "'$jobNumber',"
				  . "'$jobExecutionTime',"
				  . "'$xYear/$xMonth/$xDay $xHour:$xMin:$xSec',"
				  . "$xferInBytes" . ")"

			);
			dbExecute("SET IDENTITY_INSERT tbl_JobReports OFF");
			logIt(  "\tINSERT INTO tbl_WorkQueue ("
				  . "ExternalTableName,"
				  . "ExternalKey,"
				  . "TypeOfWork,"
				  . "Status"
				  . ") VALUES ("
				  . "'tbl_JobReports',"
				  . "$jobReportId," . "1," . "16"
				  . ")" );
			dbExecute( "INSERT INTO tbl_WorkQueue ("
				  . "ExternalTableName,"
				  . "ExternalKey,"
				  . "TypeOfWork,"
				  . "Status"
				  . ") VALUES ("
				  . "'tbl_JobReports',"
				  . "$jobReportId," . "1," . "16"
				  . ")" );
			dbExecute("COMMIT TRANSACTION JOBREPORTADD");
		};
		if ($@) {
			logIt("main - SQL ERROR: $@");
			dbExecute("SET IDENTITY_INSERT tbl_JobReports OFF");
			dbExecute("ROLLBACK TRANSACTION JOBREPORTADD");
		}
	}
}
