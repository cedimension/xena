#!perl -w

use strict 'vars';

use Convert::EBCDIC;
use Data::Dumper;
use POSIX qw(strftime);

use XReport;
use XReport::JobREPORT;
use XReport::Receiver::CDAMStream;

#$main::veryverbose = 1;

sub read_record_FTPC {
  my $f_cdam = shift;
  my $filler = pop;
  
  my ($eob, $outrec) = ('', '');

  while( 1 ) {
    if ( (!$main::byteCache || length($main::byteCache) < 16000) && !$f_cdam->eof()) {
      $f_cdam->read(my $buff, 32000);
      $main::byteCache .= $buff;
    }
    
#    warn "cache(", length($main::byteCache), "): ", unpack("H40", $main::byteCache);
    (my $b1, $main::byteCache) = unpack("C a*", $main::byteCache); 

    if ( $b1 == 0 ) { # ctrl byte = 0 next must be a EOB byte (x'80'=eol, x'40'=eof)
      ($eob, $main::byteCache) = unpack("a a*", $main::byteCache);
      die("UserError: INVALID MODEC CONTROL BYTE for $b1 ".unpack("H80", $eob.$main::byteCache)) if ( $eob ne "\x80" and $eob ne "\x40" );
      # warn "EOB FOUND - ", unpack("H8", "\x00".$eob.$main::byteCache), "\n";
      next;
    }
    elsif ( ($b1 & 0x80) == 0 ) { # no rle flag, got next b1 bytes 
      (my $inrec, $main::byteCache) = unpack("a$b1 a*", $main::byteCache);
      $outrec .= $inrec;
    }
    elsif ( ($b1 & 0xc0) == 0x80 ) { # rle flag, next byte must be repeated $b1 times
      (my $inrec, $main::byteCache) = unpack("a a*", $main::byteCache);
      $outrec .= $inrec x ($b1 & 0x3f);
    }
    elsif ( ($b1 & 0xc0) == 0xc0 ) { # rle defauult flag, default byte must be repeated $b1 times
      $outrec .= $filler x ($b1 & 0x3f); #FTC si comporta in modo diverso a seconda se chiamato in modo EBCDIC (x'40') o BINARY (x'00')
    }
    else {
      die("UserError: INVALID MODEC MASK CTRL=$b1 buffer: ".unpack("H80", $main::byteCache));
    }
    last if $eob;
  }
  # warn "Returning ", length($outrec), " bytes record\n";  
  return $outrec;
}


sub read_record_FTPCE { return read_record_FTPC @_, "\x40"; }
sub read_record_FTPCB { return read_record_FTPC @_, "\x00"; }

sub read_record_fixed {
  my $f_cdam = shift;
  my $lrk = '';
  $lrk = (grep /^lrecl$/i, keys %{$f_cdam->{cinfo}})[0] 
      if ( exists($f_cdam->{cinfo}) && exists($f_cdam->{cinfo}->{RECFM}) && $f_cdam->{cinfo}->{RECFM} =~ /^F/ );
  my $lrecl = shift || 27998;
  $lrecl = $f_cdam->{cinfo}->{$lrk} if ($lrk && $f_cdam->{cinfo}->{$lrk});
  my $rlen = $f_cdam->read(my $buff, $lrecl);
  die "Read Failure - $! - ", ref($buff), " ", length($buff), "\n" unless defined($buff);
  $main::veryverbose && i::logit( "read returned ", length($buff), " bytes buffer - read returns: $rlen rec: ".unpack('H80', $buff));
  
  return $buff;
}

sub read_record_FTPB {
  my $f_cdam = shift;
  $f_cdam->read(my $rdw, 3);
  my ($flag, $ll) = unpack('a1n', $rdw);
  $f_cdam->read(my $buff, $ll);
  return $buff;
}

sub read_record_PSF {
  my $f_cdam = shift;
  $f_cdam->read(my $rdw, 2);
  my ($ll) = unpack('n', $rdw);
  $f_cdam->read(my $buff, $ll);
  return $buff;
}

sub writepages {
    my ($ofileh, $pagsbuff) = @_;
    my $pages = [ split /\xfd\xfe/, $pagsbuff ];
    my $lastpage = pop @$pages;
#  warn "EOF: ", $cdamfh->eof(), "DATA: ", length($data), " PAGES: ", scalar(@$pages), " PAG1: ", unpack("H80", $data.$pagsbuff),  "\n";
    foreach my $page ( @$pages ) { 
       $ofileh->write($page); 
    }
    return $lastpage;
}
print ''. POSIX::strftime("%Y-%m-%d %H:%M:%S.001",localtime()), " - $0($$) starting\n";
my ($workdir, $JRID) = @ARGV[0,1];
if (-d $workdir ) {
#  $workdir =~ s/\//\\/sig;
  my @flist = glob $workdir.'\*'; 
  warn "deleting now\n", join("\n", @flist), "\n";
  unlink @flist;
}

my $jr = XReport::JobREPORT->Open($JRID, 0);
#my ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName, $XferEndTime) = 
#  $jr->getValues(qw(LocalFileName SrvName RemoteHostAddr XferMode TargetLocalPathId_IN XferDaemon JobReportName XferEndTime));
my ($fileName, $SrvName, $PeerHost, $XferMode, $JobName, $JobReportName, $XferEndTime) = 
  $jr->getValues(qw(LocalFileName SrvName RemoteHostAddr XferMode XferDaemon JobReportName XferEndTime));
my $JRAttrs;
@{$JRAttrs}{qw(XferEndTime UserRef UserTimeRef UserTimeElab)} = ($XferEndTime, '', $XferEndTime, $XferEndTime);



$jr->deleteElab();
my $relatedEntries = [];
my $dbr = XReport::DBUtil::dbExecuteReadOnly('SELECT JobReportName, JobReportID, RemoteHostAddr, LocalFileName FROM tbl_JobReports '
            . "WHERE XferDaemon = 'CDAMPARSER' AND RemoteHostAddr = '$JRID'");
while ( !$dbr->eof() ) {
    my $flds = $dbr->GetFieldsHash();
    push @{$relatedEntries}, $flds->{JobReportID};
    warn "QUEUE FOR DELETE - ReportName: ", $flds->{JobReportName} 
               , " JRID: ", $relatedEntries->[-1]
               , " RemoteHostAdd: ", $flds->{RemoteHostAddr}
               , " inds: ", $flds->{LocalFileName}
               , "\n";
    $dbr->MoveNext();
}        
$dbr->Close();

while ( scalar(@{$relatedEntries}) ) {
    my $ojrid = shift @{$relatedEntries};
    XReport::JobREPORT->Open($ojrid, 0)->deleteAll();
}
# die "";
my $cfgvars = {};
#@{$cfgvars}{qw(LocalFileName SrvName RemoteHostAddr XferMode
#          TargetLocalPathId_IN XferDaemon JobReportName)} = ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName);
@{$cfgvars}{qw(LocalFileName SrvName RemoteHostAddr XferMode
           XferDaemon JobReportName)} = ($fileName, $SrvName, $PeerHost, $XferMode, $JobName, $JobReportName);
#my $inhndlr = XReport::Receiver::CDAMStream->new();
#my $rptconf = $XReport::cfg->{confdir}.'/'.$JobReportName.'.xml';
#warn "Checking rptconf $rptconf\n";
#my $override = -f $rptconf ? $inhndl->parseRptConf($rptconf) : {};
#my $cdamcfg = $inhndl->parseRptConf($rptconf);
#
#if (!defined($override) ) {
#  my $errmsg = "$cinfo->{JOBNM} ($cinfo->{OJBID}) rejected by rptconf - process terminated";
#  $inhndl->notifyError($errmsg);
#  die $errmsg;
#}
#$inhndl->{'cinfo'} = { %{$inhndl->{'cinfo'}}, %$override } if $override;

my $ijrar = $jr->get_INPUT_ARCHIVE();
my $cdamfh = $ijrar->get_INPUT_STREAM($jr, 'data');
i::logit("Input Archive handler ref: ", ref($ijrar), " Input file handler ref: ", ref($cdamfh));
my $readrtn = ($XferMode == XReport::ENUMS::XF_FTPB ? \&read_record_FTPB :
               $XferMode == XReport::ENUMS::XF_FTPC ? \&read_record_FTPCE :
               $XferMode == XReport::ENUMS::XF_PSF ? \&read_record_PSF :
               \&read_record_fixed);
               
$readrtn = \&read_record_fixed if ref($cdamfh) eq 'XReport::Storage::IN';               
#warn "CINFO: ", Dumper($cdamfh->{cinfo}), "\n";

my $hdr_re = qr/....\xe5\xf6[\xf0-\xf9]{2}[\xc1-\xc9\xd1-\xd9\xe1-\xe9\xf0-\xf9\x40]{7}/;
my $datapocket = '';
my ($newjrh, $ofileh);

while (!$cdamfh->eof() || ($main::bytecache && length($main::bytecache) > 0)) {
   my $rec = &$readrtn($cdamfh) unless $cdamfh->eof();
   $rec =~ s/^\x00+//g;
   last unless $rec;
   print "RECCCC: ", unpack('H80', $rec), "\n";
   my ($datalen, $flags, $recdata) = unpack('n a4 @0 n @6 /a', $rec);
   # some cdam (compressed ? migrated ?) contains more spool datasets, each one could be smaller 
   # than a physical record
   my @spoolds = split /($hdr_re)/s, $recdata;
   shift @spoolds if (scalar(@spoolds) > 1 && !$spoolds[0]);
   $main::veryverbose && i::logit("SPOOLDS data 1: ".unpack('H80', $spoolds[0])."\n"
         . ( scalar(@spoolds) > 1 ? "SPOOLDS data 2: ".unpack('H80', $spoolds[1])."\n" : ''));
         
   while ( scalar(@spoolds) ) {
      my $blockdata = shift @spoolds;
      if ( $blockdata =~ /^$hdr_re/s) {
      	 die "Invalid data sequence" if !scalar(@spoolds);
      	 $blockdata .= shift @spoolds;
         $newjrh->Close($datapocket) if ( $newjrh && $newjrh->{dfilen} && $newjrh->{outfh} );
         (my $hdrlen, my $datahdr, $blockdata ) = unpack('n @0 n @0 /a a*', $blockdata);
         $main::veryverbose && i::logit("New SPOOLDS found header($hdrlen -".length($datahdr)
                                       ."): ".unpack('H80', $datahdr));
         $newjrh = XReport::Receiver::CDAMStream->new(datahdr => $datahdr, 
                                     SrvName => 'CDAMPARSER', 
                                     peeraddr => $JRID,
                                     OrigReportName => $JobReportName,
                                     );
         $ofileh = $newjrh->initJobReport();
         die "CDAM Header not recognized - record: ", unpack("H80", $datahdr.$blockdata) unless $ofileh;
         $blockdata =~ s/^\xfd\xfe//;
      }
      if ( $newjrh && $newjrh->{dfilen} && $newjrh->{outfh} ) {
           $datapocket = writepages($ofileh, $datapocket.$blockdata) ;
      }
      else {
         i::logit("CDAM Record skipped due to spool header not found "
      	   ."- record: ". unpack('H80', $rec)."\n"
      	   ."- flags: ". unpack('H*', $flags). " recdata($datalen): ". unpack('H80', $recdata). "\n"
           ."- RE: $hdr_re\n"
      	   ."- blockdata: ". unpack('H80', $blockdata). "\n"
      	   );
      }
   } 
}
$newjrh->Close($datapocket) if ( $newjrh && $newjrh->{dfilen} && $newjrh->{outfh} );

