#!perl -w

use strict;

$| = 1;
my $oldfh = select STDERR;
$| = 1;
select $oldfh;


use XReport;
use XReport::DBUtil ();
use XReport::JobREPORT ();
#use XReport::ARCHIVE ();
#use XReport::ARCHIVE::JobREPORT (); 
use XReport::ARCHIVE::Centera ();
use Data::Dumper; 

use constant DEBUG => 0;
use constant EC_OK => 0;
use constant EC_USER_SHUTDOWN => 1;
use constant EC_UNKNOWN_ERROR => 2;
use constant EC_PERL_SYNTAX_ERROR => 9;
use constant EC_PERL_DIE3 => 10045;
use constant EC_PERL_DIE1 => 10061;
use constant EC_PERL_DIE2 => 255;

use IO::Uncompress::Unzip qw($UnzipError);

$main::myName = ( split( /[\/\\]/, $0 ) )[-1];
$main::myName = ( split( /\./, $main::myName ) )[0];


sub write2log
{
  my( $package, $filename, $line ) = caller(); 
  i::logit(join('', @_).'_caller:'.join('::',$package, $filename, $line));

}
sub debug2log
{
  return unless ($main::veryverbose || DEBUG) ;
  my( $package, $filename, $line ) = caller(); 
  i::logit(join('', @_).'_caller:'.join('::',$package, $filename, $line));

}
sub update_JobReportsCenteraClips {
    debug2log("$main::myName: call update_JobReportsCenteraClips()");
	my $JRID = shift;
	my $args = {@_};
    debug2log("$main::myName: Start to update db for $JRID - Fields: ".Data::Dumper::Dumper($args));
	my @cols = keys %{$args};
	my $sql = "IF EXISTS ( SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JRID ) "
       . "UPDATE tbl_JobReportsCenteraClipIds set "
       . join( ', ', map { "$_ = $args->{$_}" } grep { $args->{$_} && $args->{$_} !~ /^(?:NULL|'')$/ } @cols )
       . " where JobReportId = $JRID\n"
       . "ELSE INSERT into tbl_JobReportsCenteraClipIds ( JobReportId, ".join(', ', @cols).") VALUES ("
       . join(', ', ($JRID, @{$args}{@cols})).")\n";
    XReport::DBUtil::dbExecute( $sql  );   
    debug2log("$main::myName: sql to update db for $JRID: ".$sql);
}

sub update_JobReportsCenteraClipIDs_DELETED {
    debug2log("$main::myName: call update_JobReportsCenteraClipIDs_DELETED()");
	my $JRID = shift;
	my $args = {@_};
	my $CentClipId = $args->{CentClipId};
	die "parameter CentClipId missing in sub update_JobReportsCenteraClipIDs_DELETED() \n" unless $CentClipId;
    debug2log("$main::myName: Start to update db for $JRID - CentClipId: ".$CentClipId);
    debug2log("$main::myName: Start to update db for $JRID - Fields: ".Data::Dumper::Dumper($args));
	my @cols = keys %{$args};
	my $sql = "IF EXISTS ( SELECT * from tbl_JobReportsCenteraClipIDs_DELETED where JobReportId = $JRID AND CentClipId = $CentClipId ) "
       . "UPDATE tbl_JobReportsCenteraClipIDs_DELETED set "
       . join( ', ', map { "$_ = $args->{$_}" } grep { $args->{$_} && $args->{$_} !~ /^(?:NULL|'')$/ } @cols )
       . " where JobReportId = $JRID AND CentClipId = $CentClipId\n"
       . "ELSE INSERT into tbl_JobReportsCenteraClipIDs_DELETED ( JobReportId, ".join(', ', @cols).") VALUES ("
       . join(', ', ($JRID, @{$args}{@cols})).")\n";
    debug2log("$main::myName:  update db for $JRID - sql: ".$sql);
    XReport::DBUtil::dbExecute( $sql  );   
}

sub Migrate_INPUT_TO_CENTERA {
  debug2log("$main::myName: call Migrate_INPUT_TO_CENTERA()");
  my ($jr, $TargetLocalPathId, %args) = @_;
  my $migrationOK = 0;
  my $clipid = 0;
  my $FileNameIN = '';
  require XReport::ARCHIVE::Centera; 

  if ( !ref($jr) ) {
    require XReport::ARCHIVE::JobREPORT; 
    $jr = XReport::ARCHIVE::JobREPORT->Open($jr);
  } 
 
  my ($JobReportId, $LocalPathId, $LocalFileName) = $jr->getValues(qw(JobReportId LocalPathId_IN LocalFileName));

  my $dbr = XReport::DBUtil::dbExecute("SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId");
#  die("THE JobReport $JobReportId HAS ALREADY THE INPUT ARCHIVE IN CENTERA")
#                                              if !$dbr->eof() && $dbr->GetFieldsValues('INFILE_ClipId') ne '';
#
  my ( $CCIDtbl_INFile_ClipId, $CCIDtbl_PoolRef_IN  );
  if (!$dbr->eof())
  {
	($CCIDtbl_INFile_ClipId, $CCIDtbl_PoolRef_IN ) = $dbr->GetFieldsValues(qw(INFile_ClipId PoolRef_IN));
  }
  $dbr->Close();
  
  if ($CCIDtbl_INFile_ClipId)
  {
	write2log("$main::myName: THE JobReport $JobReportId HAS ALREADY THE INPUT ARCHIVE IN CENTERA - INFile_ClipId:$CCIDtbl_INFile_ClipId");
	my $INPUT = $jr->get_INPUT_ARCHIVE();
	die "Unable to acquire INPUT handler -", Data::Dumper::Dumper($jr), "\n" unless $INPUT;
	my $FileNameIN = $INPUT->getFileName();
	if ( $FileNameIN !~ /^file:/i) {
		debug2log("$main::myName: Archive type \"".substr($FileNameIN, 0, 8). "\" is not supported");
		return undef;
	}
	$FileNameIN =~ s/^file:\/\///i;
	debug2log("$main::myName: file:$FileNameIN");
	if((-e $FileNameIN) && (-f $FileNameIN))
	{
		write2log("$main::myName: THE INPUT ARCHIVE (JobReport:$JobReportId) is still in the location:[$FileNameIN].");
		update_JobReportsCenteraClipIDs_DELETED( $JobReportId
								   , CentClipId => "'$CCIDtbl_INFile_ClipId'"
								   , XferStartTime => 'NULL'
								   , ElabStartTime => 'NULL'
								   , PoolRef => "'$CCIDtbl_PoolRef_IN'"
								   );
		$migrationOK = 0;
	}
	else
	{
		write2log("$main::myName: THE INPUT ARCHIVE (JobReport:$JobReportId) is not anymore in the location:[$FileNameIN]->FILE MIGRATED WITH SUCCESS");
		$migrationOK = 1;
	}
  }
  if($migrationOK == 0)
  {
	  write2log("$main::myName: - BEGIN of INPUT MIGRATION TO CENTERA of JobReportId $JobReportId");
	#  my $iar = XReport::ARCHIVE::get_INPUT_ARCHIVE($jr, wrapper => 1);
	  my $INPUT = $jr->get_INPUT_ARCHIVE();
	  die "Unable to acquire INPUT handler -", Data::Dumper::Dumper($jr), "\n" unless $INPUT;
	#  die "missing archive handler -", Data::Dumper::Dumper($INPUT), "\n" unless $INPUT->{archiver};
	  my $TargetLocalPath = $XReport::cfg->{'LocalPath'}->{$TargetLocalPathId};
	  my ($PoolRef) = ($TargetLocalPath =~ /^\s*centera:\/\/(.*)\//);
	  
	  debug2log("$main::myName: Start to migrate \"$LocalFileName\"");
	  my $is = $INPUT->get_INPUT_STREAM($jr, random_access => 0); 
	  my $pool = XReport::ARCHIVE::Centera::FPPool->new($TargetLocalPathId); 
	  unless ( $pool ) {
		 debug2log("$main::myName: access to Centera Pool $TargetLocalPath($TargetLocalPathId) to store $LocalFileName failed");
		 return undef;
	  }
	  my $clip = $pool->Create_Clip($jr);
	  unless ( $clip ) {
		 debug2log("$main::myName: Create Centera clip to store $LocalFileName failed");
		 return undef;
	  }
	  my $fpath = $INPUT->getFileName();
	  if ( $fpath !~ /^file:/i) {
			debug2log("$main::myName: Archive type \"".substr($fpath, 0, 8). "\" is not supported");
			return undef;
	  }
	  $fpath =~ s/^file:\/\///i;
	  unless (-e $fpath && -f $fpath && -s $fpath) {
			write2log("$main::myName: Unable to access $fpath or file emty (size:".-s $fpath.") - migration failed");
			return undef;
	  }
	  debug2log("$main::myName: Migrating to Centera file=$fpath compr_method='stored'");
	  my $csize = $clip->storeFile2tag($fpath, 'file');
	  write2log("$main::myName: migrated $LocalFileName - $csize bytes stored"); 

	#  my $os = $clip->AddStream($LocalFileName, compr_method => $compr_method);

	#  while(1) {
	#      $is->read(my $rec, 131072); 
	#      last if $rec eq ''; 
	#      $os->write($rec);
	#  }
	#  $os->Close();
	#  $is->Close(); 

	  unless ( $clip ) {
		 return undef;
	  }
		  #my $clipid = $clip->Write(); 
		  $clipid = $clip->Write(); 
	  my $clipsize = $clip->GetTotalSize();
	  ($main::veryverbose || DEBUG) && $clip->ListFiles();  
	  update_JobReportsCenteraClips( $JobReportId
								   , PoolRef_IN => "'$TargetLocalPathId'"
								   , PoolRef => "''"
								   , LocalPathId_IN => "'$LocalPathId'"
								   , LocalPathId_OUT => "''"
								   , INFile_ClipId => "'$clipid'"
								   , INFile_TotalSize => $clipsize
								   , OUTFile_ClipId => "''"
								   , OUTFile_TotalSize => 'NULL'
								   );
		$FileNameIN = $fpath;
		$INPUT->Close();
		$migrationOK = 1;
    }
  if (( $args{'deleteFile'} == 1 ) && ( $migrationOK == 1 ) && (-e $FileNameIN) && (-f $FileNameIN) )
  {
	  write2log("$main::myName: DELETING FILE \"$FileNameIN\"");
	  unlink $FileNameIN or write2log("$main::myName: DELETE ERROR for INPUT ARCHIVE \"$FileNameIN\" $!");	
  }
#  $jr->Close();
#  $iar->Close();

  debug2log("$main::myName: - END of INPUT MIGRATION TO CENTERA of JobReportId=$JobReportId ClipId=$clipid");
  return ($migrationOK);
}

sub getOUTzipfilename {
  my ($jr, $LocalPathId, %args) = @_;
  my $LocalPath = c::getValues('LocalPath')->{$LocalPathId ||= "L1"};
  (my $iface, my $lx) = ($LocalPath =~ /^\s*([^:\s]+):\/\/(.*)$/); 
  die ("errro - LocalPath does begin with \"file:\" - LocalPath:$LocalPath") if ( lc($iface) ne  'file' );
  return  $lx ."/OUT/".$jr->getFileName('ZIPOUT');
}

sub getOriginalIN_FilenamePreMigration {
	debug2log("$main::myName: call getOriginalIN_FilenamePreMigration()");
	my ($jr ,  %args) = @_;  
	if ( !ref($jr) ) {
		require XReport::ARCHIVE::JobREPORT; 
		$jr = XReport::ARCHIVE::JobREPORT->Open($jr)
	}
	my ($JobReportId, $LocalFileName) = $jr->getValues(qw(JobReportId LocalFileName));

	my $dbrCentera = XReport::DBUtil::dbExecute("SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId"); 
	
	die ('FAILED MIGRATION FOR ['.$JobReportId.'] - INPUT ALREADY MIGRATED in CENTERA but the data are corrupted.Process the job again.' ) if ($dbrCentera->eof() or $dbrCentera->GetFieldsValues('LocalPathId_OUT') eq ''); 
	
	my $LocalPathId_INFromCentera = $dbrCentera->GetFieldsValues('LocalPathId_IN');
	$dbrCentera->Close();
	
	my $LocalPath = c::getValues('LocalPath')->{$LocalPathId_INFromCentera ||= "L1"};
	(my $iface, my $lx) = ($LocalPath =~ /^\s*([^:\s]+):\/\/(.*)$/); 
	die ("errro - LocalPath does begin with \"file:\" - LocalPath:$LocalPath") if ( lc($iface) ne  'file' );
	
	return  $lx ."/IN/".$LocalFileName ; 
}
sub getOriginalOUT_FilenamePreMigration {
	debug2log("$main::myName: call getOriginalOUT_FilenamePreMigration()");
	my ($jr , $LocalPathId_OUT, %args) = @_;  
	if ( !ref($jr) ) {
		require XReport::ARCHIVE::JobREPORT; 
		$jr = XReport::ARCHIVE::JobREPORT->Open($jr)
	}
	my ($JobReportId) = $jr->getValues(qw(JobReportId));

	my $dbrCentera = XReport::DBUtil::dbExecute("SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId"); 
	
	die ('FAILED MIGRATION FOR ['.$JobReportId.'] - OUTPUT ALREADY MIGRATED in CENTERA('.$LocalPathId_OUT.') but the data are corrupted.Process the job again.' ) if ($dbrCentera->eof() or $dbrCentera->GetFieldsValues('LocalPathId_OUT') eq ''); 
	
	my $LocalPathId_OUTFromCentera = $dbrCentera->GetFieldsValues('LocalPathId_OUT');
	$dbrCentera->Close();
	
	my $FileNameOUT = $jr->getFileName('ZIPOUT');	 
	my $storageOUT = $XReport::cfg->{LocalPath}->{$LocalPathId_OUTFromCentera};
	$FileNameOUT = $storageOUT . '/OUT/' . $FileNameOUT ; 
	die ('FAILED MIGRATION FOR ['.$JobReportId.'] - OUTPUT ALREADY MIGRATED in CENTERA('.$LocalPathId_OUT.') but the data are corrupted.Process the job again.') if ( $FileNameOUT !~ /^file:/i) ;
	$FileNameOUT =~ s/^file:\/\///i; 
	return $FileNameOUT;
}



sub Migrate_OUTPUT_TO_CENTERA {
  debug2log("$main::myName: call Migrate_OUTPUT_TO_CENTERA()");
  my ($jr , $TargetLocalPathId, %args) = @_; require XReport::ARCHIVE::Centera;
  my $migrationOK = 0;
  my $clipid = 0;
  if ( !ref($jr) ) {
    require XReport::ARCHIVE::JobREPORT; 
    $jr = XReport::ARCHIVE::JobREPORT->Open($jr)
  }

  my ($JobReportId, $LocalPathId, $LocalFileName) = $jr->getValues(qw(JobReportId LocalPathId_OUT LocalFileName));

  my $dbr = XReport::DBUtil::dbExecute("SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId");
  #die("THE JobReport $JobReportId HAS ALREADY THE OUTPUT ARCHIVE IN CENTERA")
  #                                                 if !$dbr->eof() && $dbr->GetFieldsValues('OUTFILE_ClipId') ne ''; 
  #$dbr->Close();
  
  
  my $old_OUTFILE_ClipId = $dbr->GetFieldsValues('OUTFILE_ClipId');
  if (!$dbr->eof() && $old_OUTFILE_ClipId ne '')
  {
		write2log("$main::myName: THE JobReport $JobReportId HAS ALREADY AN OUTPUT ARCHIVE IN CENTERA");
		my $sql = ""
				." IF NOT EXISTS (                                                                                                       "
				." SELECT * from tbl_JobReportsCenteraClipIDs_DELETED                                                                    "
				." where JobReportId ='$JobReportId'                                                                                     "
				." and CentClipId = '$old_OUTFILE_ClipId'                                                                                "
				." )                                                                                                                     "
				." INSERT into tbl_JobReportsCenteraClipIDs_DELETED ( JobReportId, CentClipId, ElabStartTime, PoolRef, XferStartTime )   "
				." select cid.JobReportId as JobReportId, cid.OUTFile_ClipId as CentClipId,                                              "
				." NULL as ElabStartTime, cid.LocalPathId_OUT as PoolRef, NULL as XferStartTime                                          "
				." from tbl_JobReportsCenteraClipIds as cid                                                                              "
				." where cid.JobReportId ='$JobReportId'                                                                                 "
				." and cid.OUTFile_ClipId = '$old_OUTFILE_ClipId'                                                                        "
				."\n";
		debug2log("$main::myName: sql:$sql");
		my $dbr_update = XReport::DBUtil::dbExecute($sql);
		if($@)
		{
			write2log("$main::myName: ERROR ON INSERT IN  tbl_JobReportsCenteraClipIDs_DELETED: $!"); 
			return 0;
		}
		$dbr_update->Close();
	}
  
  
#  if (!$dbr->eof() && $dbr->GetFieldsValues('OUTFILE_ClipId') ne '')
#  {
#	write2log("$main::myName: THE JobReport $JobReportId HAS ALREADY THE OUTPUT ARCHIVE IN CENTERA");
#	$dbr->Close();
#  }  
#  else
#  {
  $dbr->Close();
  write2log("$main::myName: - BEGIN of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId");

  my $OUTzipfile ; 
  eval {
  	$OUTzipfile = getOUTzipfilename($jr, $LocalPathId);
  };
  if($@)
  {
    write2log("Error in MIGRATION: ".$@);
  	write2log("$main::myName: - END of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId ClipId=$clipid"); 
  	return 0;
  }
  
  my $u = new IO::Uncompress::Unzip $OUTzipfile or do {
    write2log("Error in MIGRATION: "."Cannot open $OUTzipfile: $UnzipError");
  	write2log("$main::myName: - END of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId ClipId=$clipid"); 
  	return 0;	
  };
  my $TargetLocalPath = $XReport::cfg->{'LocalPath'}->{$TargetLocalPathId}; 
  my ($PoolRef) = ($TargetLocalPath =~ /^\s*centera:\/\/(.*)\//);
  my $pool = XReport::ARCHIVE::Centera::FPPool->new($TargetLocalPathId); 
  my $clip = $pool->Create_Clip($jr);
  
  my $status = $u->nextStream(); 
  if ($status < 0)
  {
    write2log("Error in MIGRATION: "."Error processing $OUTzipfile: $!\n" );
  	write2log("$main::myName: - END of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId ClipId=$clipid"); 
  	return 0;	
  }  
  
  while(1)
  {
  	my $FileName = $u->getHeaderInfo()->{Name};
  	if ($FileName =~ /\w+\.$JobReportId\.0\.#\d+\.log/) {
	  write2log( "todel=$FileName");
	}
	else
  	{
  		my $fsz  = $u->getHeaderInfo()->{UncompressedLength}[0] ;
		my $compr_method = ($FileName =~ /\.pdf$/i) ? 'stored' : 'deflate'; 
		debug2log("$main::myName: Migrating to Centera file=$FileName compr_method=$compr_method");

		my $os = $clip->AddStream($FileName, compr_method => $compr_method);
		my ($filestatus, $rec);
		while(1) { 
		  $filestatus = $u->read($rec, 131072);
		  last if $rec eq ''; 
		  $os->write($rec); 
		}

		$os->Close(); 
		if ( $fsz != $os->{origsize} ) {
		  write2log("store of $FileName failed - file size mismatch - Original Size: $fsz Tag size: $os->{size} Tag Original size: $os->{origsize}");
		  write2log("$main::myName: - END of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId ClipId=$clipid");
		  $u->close();
		  return 0;
		}  
		write2log("$main::myName: file=$FileName Migrated to Centera compr_method=$compr_method");
  	}
  	$status = $u->nextStream();
  	last unless ( $status > 0); 
  }
  $u->close();
  
  $clipid = $clip->Write(); 
  my $clipsize = $clip->GetTotalSize(); 
  ($main::veryverbose || DEBUG) && $clip->ListFiles();  

  update_JobReportsCenteraClips( $JobReportId
							   , PoolRef => "'$TargetLocalPathId'"
							   , PoolRef_IN => "''"
							   , LocalPathId_OUT => "'$LocalPathId'"
							   , LocalPathId_IN => "''"
							   , OUTFile_ClipId => "'$clipid'"
							   , OUTFile_TotalSize => $clipsize
							   , INFile_ClipId => "''"
							   , INFile_TotalSize => 'NULL'
							   );
 
  $migrationOK = 1;	
  if (( $args{'deleteFile'} == 1 ) and (-f $OUTzipfile) )
  {
	  write2log("$main::myName: DELETING FILE \"$OUTzipfile\"");
	  unlink $OUTzipfile or write2log("$main::myName: DELETE ERROR for OUTPUT ARCHIVE \"$OUTzipfile\" $!");
	  write2log("$main::myName: ERROR IN DELETING OF FILE \"$OUTzipfile\"") if(-f $OUTzipfile);
  }  
  write2log("$main::myName: - END of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId ClipId=$clipid");
  return ($migrationOK);
}

write2log("$main::myName: - $0($$) starting");
my $worrkdir = $ARGV[0];
my $JRID = $ARGV[1];
my $outmess = 'SUCCESS';

#my ($lpTarget, @clauses) = grep !/^-/, @ARGV;
#die "Target $ARGV[0] does not exists or it is not a centera type\n"
#    unless ( exists($XReport::cfg->{LocalPath}->{$lpTarget}) 
#            && $XReport::cfg->{LocalPath}->{$lpTarget} =~ /^centera:/i );
#(my ($PoolRef) = $lpTarget) =~ /^\s*centera:\/\/(.*)\//;

my $deleteFileAfterMigrationFlag = 0;
if ( grep /^-dodelete$/i, @ARGV ) {
	$deleteFileAfterMigrationFlag =1;
	debug2log("$main::myName: - MODE deleteFileAfterMigration ACTIVATED");
}
my $doMigrateInput = 0;
if ( grep /^-doMigrateInput$/i, @ARGV ) {
	$doMigrateInput =1;
	debug2log("$main::myName: - MODE doMigrateInput ACTIVATED");
}
elsif ( grep /^-noMigrateInput$/i, @ARGV ) {
	$doMigrateInput = 0;
	debug2log("$main::myName: - MODE noMigrateInput ACTIVATED");
}
my $doMigrateOutput = 0;
if ( grep /^-doMigrateOutput$/i, @ARGV ) {
	$doMigrateOutput =1;
	debug2log("$main::myName: - MODE doMigrateOutput ACTIVATED");
}
elsif ( grep /^-noMigrateOutput$/i, @ARGV ) {
	$doMigrateOutput =0;
	debug2log("$main::myName: - MODE noMigrateOutput ACTIVATED");
}
if (( grep /^-dd$/i, @ARGV ) || DEBUG ){
	$main::veryverbose =1;
	debug2log("$main::myName: - MODE veryverbose ACTIVATED");
}


my $lpTarget = XReport::Util::getConfValues('LPTARGET'); # = XReport::Util::getConfValues('LPTARGET') || 'C1';
foreach my $i (0 .. $#ARGV) 
{
	if (uc($ARGV[$i]) =~ /^\-(LPTARGET|SrvParameters)$/i) { # LPTARGET
	  $lpTarget = $ARGV[$i+1];
	  last;
	}
}
if(!$lpTarget)
{
	$outmess ="ERROR - The parameter LPTARGET/SrvParameters is missing";
	write2log("$main::myName: - $0($$) end - outmess=$outmess " );
	exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);
}

my $dbc = XReport::DBUtil->new();
#my $dbname = $dbc->get_dbname();
my $sql = "SELECT * FROM tbl_JobReports WHERE (JobReportId = $JRID)";
my $dbr = XReport::DBUtil::dbExecute( $sql );
#my $dbr = $dbc->dbExecuteForUpdate( $sql );
  #. join(') AND (', @clauses, "Status=18", "LocalPathId_IN <> '$lpTarget' OR LocalPathId_OUT <> '$lpTarget'").")" );

if($dbr->eof()) {
	write2log("$main::myName: - NO RECORD FOUND in tbl_JobReports - sql[$sql]");
	$outmess = 'FAILED MIGRATION FOR ['.$JRID.']- NO RECORD FOUND in tbl_JobReports';
	$dbr->Close();
}
else 
{
#while(!$dbr->eof()) {
  #my ( $JobReportId, $LocalFileName, $LocalPathId_IN, $LocalPathId_OUT, $HoldDays ) =
  my ( $JobReportId, $LocalFileName, $LocalPathId_IN, $LocalPathId_OUT, $HoldDaysOLD ) =
                                $dbr->GetFieldsValues(qw(JobReportId LocalFileName LocalPathId_IN LocalPathId_OUT HoldDays));
  $dbr->Close();
  my $jr = XReport::JobREPORT->Open($JobReportId, 0);
  my ($dbname, $HoldDays) = $jr->getValues(qw(rdbmsdbname HoldDays));
#  my $jr = XReport::ARCHIVE::JobREPORT->Open($JobReportId);
#  $jr->{retention_period} = 540; #$jr->getValues('HoldDays');
  debug2log("$main::myName: SELECTED: $dbname, $JobReportId, $LocalFileName, $LocalPathId_IN, $LocalPathId_OUT, $HoldDays");
#  $jr->{identify} = [ Database => $dbname, JobReportId => $JobReportId, LocalFileName => $LocalFileName, ]; 
#  $jr->setValues( rdbmsdbname => $dbname
#                , retention_period => $HoldDays
  $jr->setValues( retention_period => $HoldDays
                , identify => [ Database => $dbname, JobReportId => $JobReportId, LocalFileName => $LocalFileName ]);
	
	if($doMigrateInput)	
	{
		if (($LocalPathId_IN ne $lpTarget) and ($XReport::cfg->{LocalPath}->{$LocalPathId_IN} !~ /^centera:/i ) )
		{
			my $migrationOK = Migrate_INPUT_TO_CENTERA($jr, $lpTarget, deleteFile => $deleteFileAfterMigrationFlag);
			#Migrate_INPUT_TO_CENTERA($jr, $lpTarget, deleteFile => 0);
			# XReport::Archive::Util::Migrate_INPUT($JobReportId, 'C1', deleteFile => 1);
			if ($migrationOK == 1) 
			{
				#$dbr->SetFieldsValues('LocalPathId_IN', $lpTarget);
				#$dbr->Update();
				eval {
					my $setstring = [];
					push @$setstring, "LocalPathId_IN = '$lpTarget'";
					my $sql = "UPDATE tbl_JobReports set " . join(', ', @$setstring) . " WHERE JobReportId = $JobReportId \n";
					XReport::DBUtil::dbExecute($sql);
					debug2log("$main::myName: update UPDATE tbl_JobReports - sql:".$sql );
				};
				if($@)
				{
					$outmess ="ERROR IN UPDATE tbl_JobReports:".$@ ;
					write2log("$main::myName: - $0($$) end - outmess=$outmess " );
					exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);
				}
				debug2log("$main::myName: -NO ERROR IN UPDATE tbl_JobReports:".$@ );
			}
			else
			{
				$outmess = 'FAILED MIGRATION INPUT';
				write2log("$main::myName: - $0($$) end - outmess=$outmess " );
				exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);
			}
		}
		else
		{
			#$outmess = 'FAILED MIGRATION FOR ['.$JobReportId.']- INPUT ALREADY MIGRATED in CENTERA('.$lpTarget.')';
			#write2log("$main::myName: $outmess " );		
			write2log($main::myName.': INPUT FOR ['.$JobReportId.'] - ALREADY MIGRATED in CENTERA('.$LocalPathId_IN.') ' ); 
			if($deleteFileAfterMigrationFlag)
			{
				eval 
				{
					my $FileNameIN = getOriginalIN_FilenamePreMigration($jr);
					#write2log("$main::myName: FileNameIN=$FileNameIN");
					if(-f $FileNameIN )
					{
						(unlink $FileNameIN ) or write2log("$main::myName: error in deleting of file \"$FileNameIN\".: ".$!);
						write2log("$main::myName: file \"$FileNameIN\" delete with success." ) unless (-f $FileNameIN );  
					}
				};
				write2log("error in deleting of file: ".$@) if($@); 
			}
			#do I have delete the output fields in [tbl_JobReportsCenteraClipIds]?
		}
	}  
   
	if($doMigrateOutput)
	{
		if ( ($LocalPathId_OUT ne $lpTarget ) and ($XReport::cfg->{LocalPath}->{$LocalPathId_OUT} !~ /^centera:/i ) )
		{
			my $migrationOK = Migrate_OUTPUT_TO_CENTERA($jr, $lpTarget, deleteFile => $deleteFileAfterMigrationFlag); 
			#Migrate_OUTPUT_TO_CENTERA($jr, $lpTarget, deleteFile => 0); 
			#    XReport::Archive::Util::Migrate_OUTPUT($JobReportId, 'C1', deleteFile => 1); 
			if ($migrationOK == 1) 
			{
				#$dbr->SetFieldsValues('LocalPathId_OUT', $lpTarget);
				#$dbr->Update();
				eval {
					my $setstring = [];
					push @$setstring, "LocalPathId_OUT = '$lpTarget'";
					my $sql = "UPDATE tbl_JobReports set " . join(', ', @$setstring) . " WHERE JobReportId = $JobReportId \n";
					XReport::DBUtil::dbExecute($sql);
					debug2log("$main::myName: update UPDATE tbl_JobReports - sql:".$sql );
				};
				if($@)
				{
					$outmess ="ERROR IN UPDATE tbl_JobReports:".$@ ;
					write2log("$main::myName: - $0($$) end - outmess=$outmess " );
					exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);
				}
				debug2log("$main::myName: -NO ERROR IN UPDATE tbl_JobReports:".$@ );

			}
			else
			{
				$outmess = 'FAILED MIGRATION OUTPUT FOR ['.$JobReportId.']';
				write2log("$main::myName: - $0($$) end - outmess=$outmess " );
				exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);
			}
		}
		elsif($XReport::cfg->{LocalPath}->{$LocalPathId_OUT} =~ /^centera:/i )
		{
			write2log('$main::myName: - OUTPUT FOR ['.$JobReportId.'] ALREADY MIGRATED in CENTERA('.$LocalPathId_OUT.'.'); 
			if($deleteFileAfterMigrationFlag)
			{
				eval 
				{
					my $FileNameOUT = getOriginalOUT_FilenamePreMigration($jr , $LocalPathId_OUT);
					if(-f $FileNameOUT )
					{ 
						(unlink $FileNameOUT ) or write2log("$main::myName: error in deleting of file \"$FileNameOUT\": ".$!);
						write2log("$main::myName: file \"$FileNameOUT\" delete with success." ) unless (-f $FileNameOUT );  
					}
				};
				write2log("error in deleting of file: ".$@) if($@); 
			}
		}
		else
		{
			my $dbrCentera = XReport::DBUtil::dbExecute("SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId");
			#die("THE JobReport $JobReportId HAS ALREADY THE OUTPUT ARCHIVE IN CENTERA")
			#                                                 if !$dbr->eof() && $dbr->GetFieldsValues('OUTFILE_ClipId') ne ''; 
			#$dbr->Close();
			if ($dbrCentera->eof() or $dbrCentera->GetFieldsValues('LocalPathId_OUT') eq '')
			{
				$outmess = 'FAILED MIGRATION FOR ['.$JobReportId.'] - OUTPUT ALREADY MIGRATED in CENTERA('.$LocalPathId_OUT.') but the data are corrupted.Process the job again.';
				write2log("$main::myName: - $0($$) end - outmess=$outmess " );
				exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);	
			}
			my $LocalPathId_OUTFromCentera = $dbrCentera->GetFieldsValues('LocalPathId_OUT');
			$dbrCentera->Close();
			my $FileNameOUT = $jr->getFileName('ZIPOUT');	
			#my $storageOUT = $XReport::cfg->{LocalPath}->{$LocalPathId_OUT};
			my $storageOUT = $XReport::cfg->{LocalPath}->{$LocalPathId_OUTFromCentera};
			$FileNameOUT = $storageOUT . '/OUT/' . $FileNameOUT ;
			if ( $FileNameOUT !~ /^file:/i) {
				write2log("$main::myName: Archive type \"".substr($FileNameOUT, 0, 8). "\" is not supported");
				$outmess = 'FAILED MIGRATION FOR ['.$JobReportId.'] - OUTPUT ALREADY MIGRATED in CENTERA('.$LocalPathId_OUT.') but the data are corrupted.Process the job again.';
				write2log("$main::myName: - $0($$) end - outmess=$outmess " );
				exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);	
			}
			$FileNameOUT =~ s/^file:\/\///i; 
			if(-e $FileNameOUT && -f $FileNameOUT)
			{
				$outmess = 'FAILED MIGRATION FOR ['.$JobReportId.'] - OUTPUT ALREADY MIGRATED in CENTERA('.$LocalPathId_OUT.') but the OUTPUT ARCHIVE is still in the location:['.$FileNameOUT.'].Process the job again.';
				write2log("$main::myName: - $0($$) end - outmess=$outmess " );
				exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);			
			}
			else
			{
				write2log('$main::myName: - OUTPUT FOR ['.$JobReportId.'] ALREADY MIGRATED in CENTERA('.$lpTarget.'). file['.$FileNameOUT.'] already deleted.->FILE MIGRATED');
			}
		}
	}
}
#continue {
#  $dbr->MoveNext();
#}
#$dbr->Close();

#($main::veryverbose || DEBUG) and write2log("$main::myName: outmess=$outmess " );
write2log("$main::myName: - $0($$) end - outmess=$outmess " );
exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);

1;


__END__
my $lpTarget = $ARGV[1];
die "Target $ARGV[1] does not exists or it is not a centera type\n"
    unless ( exists($XReport::cfg->{LocalPath}->{$lpTarget}) 
            && $XReport::cfg->{LocalPath}->{$lpTarget} =~ /^centera:/i );
(my ($PoolRef) = $lpTarget) =~ /^\s*centera:\/\/(.*)/;

my $dbr = dbExecuteForUpdate( "SELECT * FROM tbl_JobReports WHERE ( JobReportid = $ARGV[0] ) and (Status=18) " 
                            . " AND not ((LocalPathId_IN = '$lpTarget') or (LocalPathId_OUT = '$lpTarget'))");
if ($dbr->eof()) {
	warn "Jobreport with ID $ARGV[0] not found or with unmatched attributes\n";
}

my ( $JobReportId, $LocalFileName, $LocalPathId_IN, $LocalPathId_OUT ) =
                   $dbr->GetFieldsValues(qw(JobReportId LocalFileName LocalPathId_IN LocalPathId_OUT));

print "$JobReportId, $LocalFileName, $LocalPathId_IN, $LocalPathId_OUT\n"; 

require XReport::Archive::JobREPORT; 
$jr = XReport::Archive::JobREPORT->Open($JobReportId)
  
my ($clipidIN, $clipidOUT);
my $dbrclips = dbExecute("SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId");

($clipidIN, $clipidOUT) = map { $dbrclips->GetFieldsValues($_) } qw(INFILE_ClipId OUTFILE_ClipId) if !$dbrclips->eof();
$dbrclips->Close();

die("THE JobReport $JobReportId is ALREADY Archived IN CENTERA") if ($clipidIN && $clipidOUT);

my $pool = XReport::Archive::Centera::FPPool->new($PoolRef); 

my $regParms = {};

my @set = ();
if (!$clipidIN && $LocalPathId_IN ne $lpTarget) {
  print localtime(), " - BEGIN of INPUT MIGRATION TO CENTERA of JobReportId $JobReportId\n";

  my $iar = XReport::ARCHIVE::get_INPUT_ARCHIVE($jr, wrapper => 1);
  my ($LocalFileName, $archiver) = @{$iar}{qw(LocalFileName archiver)};

  my $is = $archiver->get_INPUT_STREAM(); 
  $is->Open();

  my $clip = $pool->Create_Clip($jr);

  my $os = $clip->AddStream($LocalFileName, compr_method => 'stored', chunks => 'no');

  while(1) {
    $is->read(my $rec, 131072); 
    last if $rec eq ''; 
    $os->write($rec);
  }

  $is->Close(); 
  $os->Close(); 
  $regParms->{IN} = [ $clip->Write(), $clip->GetTotalSize() ];
  push @set, ("PoolRef_IN = $lpTarget", 
                    "INFILE_ClipId = '$regParms->{IN}->[0]'",
                    "INFILE_TotalSize = $regParms->{IN}->[1]"); 
  $dbr->SetFieldsValues('LocalPathId_IN', $lpTarget); 
  print localtime(), " - END of INPUT MIGRATION TO CENTERA of JobReportId $JobReportId\n";
}
   
if (!$clipidOUT && $LocalPathId_OUT ne $lpTarget) {
  print localtime(), " - BEGIN of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId\n";

  my $iar = XReport::ARCHIVE::get_OUTPUT_ARCHIVE($jr, wrapper => 1);
  my ($LocalFileName, $archiver) = @{$iar}{qw(LocalFileName archiver)};
  
  for my $FileName (@{$archiver->FileNames()}) {
    if ($FileName =~ /\w+\.$JobReportId\.0\.#\d+\.log/) {
      print "todel=$FileName\n"; next; 
    }

    my $compr_method = ($FileName =~ /\.pdf$/i) ? 'stored' : 'deflate';

    print "Migrating to Centera file=$FileName compr_method=$compr_method\n";

    my $os = $clip->AddStream($FileName, compr_method => $compr_method);

    my $is = $archiver->LocateFile($FileName); 
    $is->Open();
    
    while(1) {
      $is->read(my $rec, 131072);
      last if $rec eq ''; 
      $os->write($rec);
    }

    $is->Close(); 
    $os->Close(); 
  }
  $archiver->Close();
  $regParms->{OUT} = [ $clip->Write(), $clip->GetTotalSize() ];
  push @set, ("PoolRef = $lpTarget", 
                    "OUTFILE_ClipId = '$regParms->{OUT}->[0]'",
                    "OUTFILE_TotalSize = $regParms->{OUT}->[1]"); 
  $dbr->SetFieldsValues('LocalPathId_OUT', $lpTarget); 
  print localtime(), " - END of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId\n";
}                                                         

for ( qw(IN OUT) ) {
      if (!exists($regParms->{$_})) { 
         $regParms->{$_} = [ '', 'NULL' ]; 
      }
}
my $sql = ''
$sql   .= "IF EXISTS ( SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId ) ";
$sql   .= "UPDATE tbl_JobReportsCenteraClipIds set ".join(', ', @set)." where JobReportId = $JobReportId"
$sql   .= "ELSE INSERT into tbl_JobReportsCenteraClipIds "
$sql   .= "( JobReportId, PoolRef, INFILE_ClipId, INFILE_TotalSize, OUTFILE_ClipId, OUTFILE_TotalSize ) ";
$sql   .= sprintf("VALUES ($JobReportId, $lpTarget, '%s', %s, '%s', %s)\n", (map { @{$regParms->{$_}} } qw(IN OUT))); 
dbExecute($sql);

$dbr->Update();

if ( grep /^-dodelete$/, @ARGV ) {
    for ( qw(IN OUT) ) {
        next unless $regParms->{$_}->[0];
        my $getArchRtn = XReport::ARCHIVE->can(( $_ eq 'IN' ? 'get_INPUT_ARCHIVE' : 'get_OUTPUT_ARCHIVE'));       
        my $FileName = substr(&$getArchRtn($jr, wrapper => 1)->getFileName(), 7);
        print "DELETING FILE \"$FileName\"\n";
        unlink $FileName or die "DELETE ERROR for INPUT ARCHIVE \"$FileName\" $!"
    }
}
