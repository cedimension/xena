#!/usr/bin/perl -w
#######################################################################
# @(#) $Id: xrInstall.pl,v 1.2 2001/03/19 18:45:37 mpezzi Exp $ 
#  
# Copyrights(c) EURISKOM s.r.l.
#######################################################################

use strict;

use Win32;
use Win32::Daemon;

sub DumpError
{
    print GetError(), "\n";
}

sub GetError
{
    return( Win32::FormatMessage( Win32::Daemon::GetLastError() ) );
}
use XReport;
use XReport::Util;

my ($SrvName, $pgmName, $pgmDesc, $ComputerName);

my $action = shift;
my @services = ();

push @services, ($ARGV[0] eq "all" ? keys %{ $XReport::cfg->{'daemon'} } : @ARGV) if (@ARGV);
die "No services specified\n" if (@services == 0);

my $XReportDir = $ENV{"XREPORT_HOME"};

foreach $SrvName (keys %{ $XReport::cfg->{'daemon'} } ) {
  
  next if !grep(/^$SrvName$/, @services);
  $pgmName = $XReport::cfg->{'daemon'}->{$SrvName}->{'process'};
  unless ($pgmName) {
    print "Invalid Config for $SrvName\n";
    next;
  }
  $pgmDesc = ($XReport::cfg->{'daemon'}->{$SrvName}->{'display'} or '');

  ### set path 
  $ComputerName = uc($ENV{'COMPUTERNAME'}); 

  my $ServiceConfig = {
	      name    =>  $SrvName,
	      display =>  $pgmDesc,
	      path    =>  $^X,
              user    =>  "$ComputerName\\xreport",
	      pwd     =>  'euriskom',
	      parameters => "-I$XReportDir/perllib " . 
	                    "$XReportDir/bin/xrDaemon.pl -N ".$SrvName,
	     };

  if ($action eq "install") {
    print "Now installing\n";
    print "Name:  $ServiceConfig->{name}\n";
    print "Desc:  $ServiceConfig->{display}\n";
    print "Path:  $ServiceConfig->{path}\n";
    print "Parms: $ServiceConfig->{parameters}\n";
    if( Win32::Daemon::CreateService( $ServiceConfig ) ) {
      print "The $ServiceConfig->{display} ($SrvName) was successfully installed.\n";
    }
    else {
      print "Failed to add the $ServiceConfig->{display} service.\nError: " . GetError() . "\n";
    }
  } 
  elsif ($action eq "remove") {
    print "Now Removing\n";
    print "Name:  $ServiceConfig->{name}\n";
    print "Desc:  $ServiceConfig->{display}\n";
    print "Path:  $ServiceConfig->{path}\n";
    print "Parms: $ServiceConfig->{parameters}\n";
    if( Win32::Daemon::DeleteService( $ServiceConfig->{name} ) ) {
      print "The $ServiceConfig->{display} ($SrvName) was successfully removed.\n";
    }
    else {
      print "Failed to remove the $ServiceConfig->{display} ($SrvName) service.\nError: " . GetError() . "\n";
    }
  }
  next;

  
}

exit 0;
