#!perl -w

use strict;
#use POSIX qw(strftime);
use IO::File;

use XReport;
use XReport::DBUtil;
use XReport::BUNDLE;

use Data::Dumper;
$| = 1;
my $oldfh = select STDERR;
$| = 1;
select $oldfh;

print "calling arguments for $0, ", join(" " , @ARGV), "\n";

sub i::warnit {
    warn ''.localtime().' - '.join(' ', @_)."\n";
}

sub i::logit {
    warn ''.localtime().' - '.join(' ', @_)."\n";
}

my $dbhandle = XReport::DBUtil->new();
my $sql = $XReport::cfg->{sqlprocs}->{BundlesNames};
$dbhandle = dbExecute_NORETRY($sql);
