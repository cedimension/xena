#!/usr/bin/perl -w

use IO::Socket::INET;
use IO::Select;
use FileHandle;

my ($printvalet, $printername, $prtparms, $pdfname) = @ARGV[0,1,2,3];

my $sock = new IO::Socket::INET(PeerAddr => $printvalet) || die "unable to open connection - $! - $?";
binmode $sock;
my $fh = new FileHandle("<$pdfname") || die "Unable to open input file -$?";
binmode $fh;

print $sock  pack("a8 N n/a* n/a*", 'STRM2PRT', -s $pdfname, $printername, join("\t", split(/\s/, $prtparms)) );

print "now sending $pdfname to ", $sock->peerport(), "\n";
while (my $bl = $fh->read(my $buff, 4096) ) {
 print $sock $buff;
 last if $bl < 4096; 
}

$fh->close();
$sock->close();

__END__
