#!/usr/local/bin/perl -w
#######################################################################
# @(#) $Id: xrLprReq.pl,v 1.2 2001/03/19 18:45:38 mpezzi Exp $ 
#  
# Copyrights(c) EURISKOM s.r.l.
#######################################################################

my $version = sub { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };
use strict;

my ($queuename, $jname, $fname);
my ($huser, $hpass, $ftpe) = ('XREPORT', 'XREPORT', 'ASCII');

foreach my $i (0 .. $#ARGV) {
  if 	($ARGV[$i] eq "-d") { # enable debug mode
    $debug = 1;
  }
  elsif ($ARGV[$i] eq "-q") { # queue name
    $queuename = $ARGV[$i+1];
  }
  elsif ($ARGV[$i] eq "-j") { # job Name 
    $jname = $ARGV[$i+1];
  }
  elsif ($ARGV[$i] eq "-f") { # host file name
    $fname = $ARGV[$i+1];
  }
  elsif ($ARGV[$i] eq "-u") { # host user name
    $fname = $ARGV[$i+1];
  }
  elsif ($ARGV[$i] eq "-p") { # host password
    $fname = $ARGV[$i+1];
  }
  elsif ($ARGV[$i] eq "-t") { # transfer type
    $fname = $ARGV[$i+1];
  }
}

if (!$queuename or !$jname or !$fname) {
  print "ERROR: QUEUENAME and JNAME and FNAME must be specified\n";
  exit 12;
}



