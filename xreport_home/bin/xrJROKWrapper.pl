#!/usr/bin/perl -w

use lib "$ENV{XREPORT_HOME}/perllib";
use strict "vars";

use File::Basename;

use Data::Dumper;

use XReport;
use XReport::Util;
use XReport::DBUtil;
use XReport::JobREPORT;
use XReport::ENUMS qw(:ST_ENUMS);

my $workdir = $ARGV[0];
my $JRID = $ARGV[1] || die "Unable to get JobReportId\n";

$logrRtn = sub { print @_, "\n"; };

my $jr = XReport::JobREPORT->Open($JRID, 1);
dbExecute("UPDATE tbl_JobReports set Status = ". ST_COMPLETED . " WHERE JobReportId = $JRID");

exit 0;
