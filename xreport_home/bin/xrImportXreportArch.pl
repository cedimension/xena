#!/usr/bin/perl -w

package XReport::DBUtil::MSSQLTypes;

use strict "vars";

use constant typeCode => {
	  adEmpty => 0,
	  adTinyInt => 16,
	  adSmallInt => 2,
	  adInteger => 3,
	  adBigInt => 20,
	  adUnsignedTinyInt => 17,
	  adUnsignedSmallInt => 18,
	  adUnsignedInt => 19,
	  adUnsignedBigInt => 21,
	  adSingle => 4,
	  adDouble => 5,
	  adCurrency => 6,
	  adDecimal => 14,
	  adNumeric => 131,
	  adVarNumeric => 139,
	  adBoolean => 11,
	  adError => 10,
	  adUserDefined => 132,
	  adVariant => 12,
	  adIDispatch => 9,
	  adIUnknown => 13,
	  adGUID => 72,
	  adDate => 7,
	  adDBDate => 133,
	  adDBTime => 134,
	  adDBTimeStamp => 135,
	  adFileTime => 64,
	  adDBFileTime => 137,
	  adBSTR => 8,
	  adChar => 129,
	  adVarChar => 200,
	  adLongVarChar => 201,
	  adWChar => 130,
	  adVarWChar => 202,
	  adLongVarWChar => 203,
	  adBinary => 128,
	  adVarBinary => 204,
	  adLongVarBinary => 205,
	  adChapter => 136,
	  adPropVariant => 138,
};

use constant codeAttr => {
	  0 => { Name => 'adEmpty', Type => 'S' },
	  16 => { Name => 'adTinyInt', Type => 'N' },
	  2 => { Name => 'adSmallInt', Type => 'N' },
	  3 => { Name => 'adInteger', Type => 'N' },
	  20 => { Name => 'adBigInt', Type => 'N' },
	  17 => { Name => 'adUnsignedTinyInt', Type => 'N' },
	  18 => { Name => 'adUnsignedSmallInt', Type => 'N' },
	  19 => { Name => 'adUnsignedInt', Type => 'N' },
	  21 => { Name => 'adUnsignedBigInt', Type => 'N' },
	  4 => { Name => 'adSingle', Type => 'N' },
	  5 => { Name => 'adDouble', Type => 'N' },
	  6 => { Name => 'adCurrency', Type => 'N' },
	  14 => { Name => 'adDecimal', Type => 'N' },
	  131 => { Name => 'adNumeric', Type => 'N' },
	  139 => { Name => 'adVarNumeric', Type => 'N' },
	  11 => { Name => 'adBoolean', Type => 'N' },
	  10 => { Name => 'adError', Type => 'S' },
	  132 => { Name => 'adUserDefined', Type => 'S' },
	  12 => { Name => 'adVariant', Type => 'S' },
	  9 => { Name => 'adIDispatch', Type => 'S' },
	  13 => { Name => 'adIUnknown', Type => 'S' },
	  72 => { Name => 'adGUID', Type => 'S' },
	  7 => { Name => 'adDate', Type => 'D' },
	  133 => { Name => 'adDBDate', Type => 'D' },
	  134 => { Name => 'adDBTime', Type => 'D' },
	  135 => { Name => 'adDBTimeStamp', Type => 'D' },
	  64 => { Name => 'adFileTime', Type => 'D' },
	  137 => { Name => 'adDBFileTime', Type => 'D' },
	  8 => { Name => 'adBSTR', Type => 'S' },
	  129 => { Name => 'adChar', Type => 'S' },
	  200 => { Name => 'adVarChar', Type => 'S' },
	  201 => { Name => 'adLongVarChar', Type => 'S' },
	  130 => { Name => 'adWChar', Type => 'S' },
	  202 => { Name => 'adVarWChar', Type => 'S' },
	  203 => { Name => 'adLongVarWChar', Type => 'S' },
	  128 => { Name => 'adBinary', Type => 'S' },
	  204 => { Name => 'adVarBinary', Type => 'S' },
	  205 => { Name => 'adLongVarBinary', Type => 'S' },
	  136 => { Name => 'adChapter', Type => 'S' },
	  138 => { Name => 'adPropVariant', Type => 'S' },
};

sub getTypeCode {
  my ($class, $tn) = (shift, shift);
  return undef unless exists typeCode->{$tn};
  return typeCode->{$tn};
}

sub getCodeAttr {
  my ($class, $cd) = (shift, shift);
  return codeAttr->{13} unless exists codeAttr->{$cd};
  return codeAttr->{$cd};
}

1;

package XReport::INFORMAT;

sub DDMMYY10 {
  my $class = shift;
  (my $string = shift) =~ s/^(\d{2})[^\d]*(\d{2})[^\d]*(\d{4})$/$3-$2-$1/;
  return $string;
}

1;

use lib "$ENV{XREPORT_HOME}/perllib";
use strict "vars";

use FileHandle;
use File::Basename;

use File::Path;
use File::Copy 'cp';
use Compress::Zlib;
use XML::Simple;

use Data::Dumper;

use XReport;
use XReport::Util;
use XReport::DBUtil;
use XReport::JobREPORT;

use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

my $workdir = $ARGV[0];
my $JRID = $ARGV[1] || die "Unable to get JobReportId\n";

if (-d $workdir ) {
#  $workdir =~ s/\//\\/sig;
  my @flist = glob $workdir.'\*'; 
  &$logrRtn( "deleting now\n", join("\n", @flist));
  unlink @flist;
}

$logrRtn = sub { print @_, "\n"; };

sub buildParser {
  my ($outvars, $invars) = (shift, shift);
  
  my $outxform = { ($outvars =~ /(\S+)(?:\s+(\w+)\.){0,1}/osgi)  };
  my $inxform = { ($invars =~ /(\S+)(?:\s+(\w+)\.){0,1}/osgi)  };
#  print "outxform:\n", Dumper($outxform), "\n";
#  print "inxform:\n", Dumper($inxform), "\n";

  my $outlist = [grep !/\w+\./, split(/\s/, $outvars) ];
  my $inlist = [grep !/\w+\./, split(/\s/, $invars) ];
  
  my $code = "sub { my \$input = shift; return {\n";
  for my $j ( 0..$#{$outlist} ) {
    my $outvarn = $outlist->[$j];
    my $invarn = $inlist->[$j];
    my $invalue = ($invarn =~ /^#(.*)?#$/ ? "$1" : 
		   $inxform->{$invarn} && XReport::INFORMAT->can($inxform->{$invarn}) ? 
		   'XReport::INFORMAT->'."$inxform->{$invarn}(\$input->{$invarn})" : 
		   "\$input->{$invarn}");
    $code .= $outvarn . ' => ' . ($invarn =~ /^#(.*)?#$/ ? $invalue . ",\n" : 
				  ($outxform->{$outvarn} && XReport::OUTFORMAT->can($outxform->{$outvarn}) ? 
				   'XReport::OUTFORMAT->'. "$outxform->{$outvarn}($invalue)" : $invalue
				  )
				  . " || '',\n"
				 ); 
  }
  $code .= "};\n}";
  print "CODE:\n", $code, "\n";
  eval($code);
  if ($@) {
    print "CODE:\n", $code, "\n";
    die "Error during building of parsing routine - $@";
  }
  return (eval($code), @$outlist);
}

sub getFH {
}

my $jr = XReport::JobREPORT->Open($JRID, 1);
my ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName) = $jr->getValues(qw(LocalFileName SrvName RemoteHostAddr XferMode
										  TargetLocalPathId_IN XferDaemon JobReportName));

$jr->deleteElab();

my $cfgvars = {};
@{$cfgvars}{qw(LocalFileName SrvName RemoteHostAddr XferMode
	       TargetLocalPathId_IN XferDaemon JobReportName)} = ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName);

$tgtIN = 'L1' unless $tgtIN;

my $rptconf_in = $jr->getFileName('PARSE');
my $FQcfgName = $workdir.'/'. basename($jr->getFileName('LOGPARSE'));
my $cfgfhin = new FileHandle("<".$rptconf_in) || die "unable to access rptconf $rptconf_in\n";
my $cfgfhou = new FileHandle(">".$FQcfgName) || die "unable to access rptconf $FQcfgName\n";
while (<$cfgfhin>) {
  my $lout = $_;
  $lout =~ s/\$(\w+)/$cfgvars->{$1}/sg;
  print $cfgfhou $lout, "\n";
}
close $cfgfhin;
close $cfgfhou;

my $reportcfg = XMLin($FQcfgName,
		      ForceArray => [ qw(index reload op ix) ],
		      KeyAttr => {'index' => 'name', 'reload' => 'name', 'op' => 'name', ix => 'name'},
		     );

(my $FQfileName = getConfValues('LocalPath')->{$tgtIN}."/IN/$fileName") =~ s/^file:\/\///;
die "Cannot access $FQfileName\n" unless -f $FQfileName;
(my $FQcntlName = $FQfileName) =~ s/DATA\.TXT\.gz$/CNTRL.TXT/osi;
die "Cannot access $FQcntlName\n" unless -f $FQcntlName;

my $buffer;
&$logrRtn("Opening ", $FQfileName);
my $gz = gzopen($FQfileName, "rb");
die "Cannot open $FQfileName: ".$gzerrno."\n" unless $gz;

my $FQzipName = $workdir.'/XRARCHIVE.zip'; 
my $outzip = new FileHandle('>'.$FQzipName) || die "unable to open $FQcntlName - $?\n";

binmode $outzip;

print $outzip $buffer while $gz->gzread($buffer) > 0 ;

my $errgznum = ($gz->gzerror()+0);
my $errgzmsg = scalar($gz->gzerror());

$outzip->close;
$gz->gzclose() ;

if ($errgznum != Z_STREAM_END ) {
  unlink $FQzipName;
  die "Error reading from $FQfileName: $errgznum: $errgzmsg\n" 
}

#Opening ZIP ARCHIVE

my $zip = new Archive::Zip($FQzipName);
$main::files = [ $zip->memberNames() ];
print "ZIP FILE $FQzipName contains:\n", join("\n", @$main::files), "\n";

sub extractFile {
  my ($ftype, $zipflst) = (shift, [ @_ ]);
  die "$ftype inputfile not found" unless scalar(@$zipflst);
 
  my $FQoutName =  $workdir.'/'. basename($jr->getFileName( (ref($ftype) eq 'ARRAY' ? ($ftype->[0], $ftype->[1]) : $ftype) ));
  die "multiple $ftype found" if (scalar(@$zipflst) > 1 && $FQoutName !~ /\.\d+\.(?:\d+\#|\#\d+)/);
  foreach my $mnam ( @$zipflst ) {
    my ($id1, $id2, $id3) = ($mnam =~ /\.\d+\.(\d+)\.(?:(?:\#)(\d+)|(\d+)(?:\#))\./);
     my $fct = ($id2 ? $id2 : $id3 ? $id3 : $id1 );

    (my $outnam = $FQoutName) =~ s/\.(\d+)\.(\d+)\.(?:\d+\#|\#\d+)\./\.$1\.$fct.\#0\./; 
    print "now extracting ", $mnam, " as ", (ref($ftype) eq 'ARRAY' ? ($ftype->[0], " IDEN: ", $ftype->[1]) : $ftype), " to \"$outnam\"\n";
    die '$ftype extraction error' unless $zip->extractMember( $mnam, $outnam) == AZ_OK;
  }
  return $FQoutName;
}

#Processing PageXref
extractFile 'PAGEXREF', (grep /(?i:PageXref)/, @$main::files);

#Processing PDFS
extractFile 'PDFOUT', (grep /(?i:pdf)$/, @$main::files);

#Processing FilXref
extractFile 'FILXREF', (grep /(?i:FilXref)/, @$main::files);

#Processing FilterVarFiles
foreach my $tsvfile ( grep /(?i:IT\.((?:TO\.){0,1}\w+)\.\d+\.tsv)$/, @$main::files ) {
  my ($fiden) = ($tsvfile =~ /(?i:IT\.((?:TO\.){0,1}\w+)\.\d+\.tsv)$/);
  extractFile ['OUTLFILE', $fiden], ($tsvfile);
}

#Processing LogTables
my ($JRAttrs, $updates) = ({}, []);
my $OldXRTables = XMLin(extractFile 'LOGTABLES', (grep /(?i:LogTables)/, @$main::files));
my ($OXferEndTime, $UserRef, $UserTimeRef, $UserTimeElab) = @{$OldXRTables->{tbl_JobReports}}{qw(XferEndTime UserRef UserTimeRef UserTimeElab)};
$UserTimeRef = $OXferEndTime unless $UserTimeRef;
$UserTimeElab = $OXferEndTime unless $UserTimeElab;

@{$JRAttrs}{qw(XferEndTime UserRef UserTimeRef UserTimeElab)} = ($OXferEndTime, $UserRef, $UserTimeRef || $OXferEndTime, $UserTimeElab || $OXferEndTime);
foreach my $jrcol ( qw(UserRef UserTimeRef UserTimeElab) ) {
  push @$updates, "$jrcol = '" . $JRAttrs->{$jrcol} . "' ";
}

&$logrRtn("Now Updating tbl_JobReports with request attributes (". scalar(@$updates), ")\n". join(', ', @$updates));
dbExecute("UPDATE tbl_JobReports SET " . join(', ', @$updates) . " WHERE JobReportId = $JRID" );

#Processing INDEXES
use fields;
my $ixlist = [ grep /^(?i:\$\$IX\..+\.tsv)$/, @$main::files ];

foreach my $ixfnam ( @$ixlist) {
  print "Now processing $ixfnam\n"; 
  my ($ixtnam) = ($ixfnam =~ /^\$\$IX\.(\w+)\./i);
  my $indexrows = [split /[\r\n]+/, $zip->contents($ixfnam)];
  $main::indexes->{$ixtnam} = {
			       HDR => shift @$indexrows,
			       ROWS => [],
			      };
  foreach my $row ( @$indexrows ) {
    my $hdrs = [ split /\t/, $main::indexes->{$ixtnam}->{HDR} ];
    push @{$main::indexes->{$ixtnam}->{ROWS}}, { %$JRAttrs, map { shift @$hdrs => $_ } split /\t/, $row }
  }
}

sub setIndexParser {
  my $op = shift;
  my $test = $op->{test};
  foreach my $ixname ( keys %$main::indexes ) {
    next if $test ne "1" && $main::indexes->{$ixname}->{HDR} !~ /$test/;
    my ($parser, @outlist) = buildParser($op->{getVars}, $op->{Columns});
    foreach my $outixn ( split /\s+/, $op->{indexes} ) {
      next unless exists($reportcfg->{index}->{$outixn});
      $main::indexes->{$ixname}->{parser}->{$outixn} = $parser;
    }
  }
}

print "Now checking cfg keys \n", Dumper($reportcfg->{reload}), "\n"; 
foreach my $ixproc ( keys %{$reportcfg->{reload}->{$JobReportName}->{ix}} ) {
  my $ix = $reportcfg->{reload}->{$JobReportName}->{ix}->{$ixproc};
  my $ixname = $ixproc;
  next unless exists($main::indexes->{$ixname});
  my $outrtn = {};
  foreach my $opname ( keys %{$ix->{op}} ) {
    print "Now Processing $opname of $ixproc\n";
    my $op = $ix->{op}->{$opname};
    my $test = $op->{test};
    next if $test ne "1" && $main::indexes->{$ixname}->{HDR} !~ /$test/;
    print "Now Start INDEX $ixname processing for $op->{indexes}\n";
    ($main::indexes->{$ixname}->{parser}, my @outlist) = buildParser($op->{getVars}, $op->{Columns});
    foreach my $outixn ( split /\s+/, $op->{indexes} ) {
      next unless exists($reportcfg->{index}->{$outixn});
      $outrtn->{$outixn}->{outlist} = [ split /\s+/, $reportcfg->{index}->{$outixn}->{vars} ];
      $outrtn->{$outixn}->{entries} = $reportcfg->{index}->{$outixn}->{entries};
      $outrtn->{$outixn}->{table} = $reportcfg->{index}->{$outixn}->{table};
    }
  }
  foreach my $outixn ( keys %$outrtn ) {
    my $tabname = $outrtn->{$outixn}->{table};
    my $ixfname = basename($jr->getFileName('IXFILE', $tabname));
    @{$main::outfiles->{$tabname}}{qw(FH FNAME TOTENTRIES)} = (new FileHandle(">$workdir/$ixfname"), $ixfname, 0) unless exists($main::outfiles->{$tabname});
    $outrtn->{$outixn}->{FKEY} = $tabname;
    my $fh = $main::outfiles->{$outrtn->{$outixn}->{FKEY}}->{FH};
    print "Columns  will be stored into ", join('::', @{$outrtn->{$outixn}->{outlist}}), " of $outixn\n";
    print $fh join("\t", ('JobReportID', @{$outrtn->{$outixn}->{outlist}})), "\n";
    $outrtn->{$outixn}->{cnt} = 0;
  }
  foreach my $row ( @{$main::indexes->{$ixname}->{ROWS}}) {
    foreach my $outixn ( keys %$outrtn ) {
      next if $outrtn->{$outixn}->{entries} eq 'FIRST' and $outrtn->{$outixn}->{cnt} > 0;
      my $fh = $main::outfiles->{$outrtn->{$outixn}->{FKEY}}->{FH};
      print $fh join("\t", ($JRID, @{&{$main::indexes->{$ixname}->{parser}}($row)}{@{$outrtn->{$outixn}->{outlist}}})), "\n";
      $main::outfiles->{$outrtn->{$outixn}->{FKEY}}->{TOTENTRIES}++;
      $outrtn->{$outixn}->{cnt}++;
    }
  }
  foreach my $outixn ( keys %$outrtn ) {
    print "$outixn entries: $outrtn->{$outixn}->{entries}, count: $outrtn->{$outixn}->{cnt}\n";
  }
  my $manfqn = $workdir.'/'. basename($jr->getFileName('IXFILE', '$$MANIFEST$$'));
  my $manifest = new FileHandle(">$manfqn");
  
  foreach my $outixn ( keys %{$main::outfiles} ) {
    print "$outixn table entries count: $main::outfiles->{$outixn}->{TOTENTRIES}\n";
    my $fh = $main::outfiles->{$outixn}->{FH};
    close $fh;
    print $manifest join("\t", ($outixn, @{$main::outfiles->{$outixn}}{qw(FNAME TOTENTRIES)} ) ), "\n";
  }
  close $manifest;
}

unlink $FQzipName;

exit 0 unless scalar(keys %{$reportcfg->{reload}->{$JobReportName}->{ix}});

unless ( scalar(keys %{$main::outfiles}) ) {
  print "No indexes found that matches specifications\n";
  exit 1;
}

exit 0;
__END__ 

