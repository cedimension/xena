
use lib($ENV{'XREPORT_HOME'}."/perllib");

use strict vars;

use Symbol;
use FileHandle;

use XReport;
use XReport::Util;
use XReport::DBUtil;
use XReport::INPUT;
use XReport::JobREPORT;

use Convert::EBCDIC;

my $translator = new Convert::EBCDIC;

my ($BUNDLEID, $fileName, @JobList, %JobList);
my ($jr, $JobReportName, %JobReportName, $JobReport, %JobReports, $jrid, $progr);
my (@page, $pageStr, $lastcc, $lastline, $atLine, $atPage, $atCopy, $atFile);
my ($dateTime, $pagesToRead, $linesToRead, $JobExecDate, $JobExecTime);

my ($pageStr);

my ($SrvName, $PeerHost);
my ($INPUT, $OUTPUT, $LOGOUT);

my ($descr, $copies, $bytes, $lines, $pages, $jobName, $jobNumber);


my ($hdrre, $lpre); sub getJobEntry {}; sub parseJobExecDate {};

eval $XReport::cfg->{ctdfilter}->{code};

die "CTDFILTER CONFIG file error: $@" if $@;

$lpre = ""
 ."                       FFFFFFFFFF        IIIIII         NN       NN        EEEEEEEEEE *\n"
 ."                       FFFFFFFFFF        IIIIII         NNN      NN        EEEEEEEEEE *\n"
 ."                       FF                  II           NNNN     NN        EE *\n"
 ."                       FFFFFFFFFF          II           NN NN    NN        EEEEEEEEEE *\n"
 ."                       FFFFFFFFFF          II           NN  NN   NN        EEEEEEEEEE *\n"
 ."                       FF                  II           NN   NN  NN        EE *\n"
 ."                       FF                  II           NN    NN NN        EE *\n"
 ."                       FF                IIIIII         NN     NNNN        EEEEEEEEEE *\n"
 ."                       FF                IIIIII         NN      NNN        EEEEEEEEEE *\n"
 ." *\n"
 ." *\n"
 ." *\n"
 ." *\n"
 ."   SSSSSSSSS         TTTTTTTTTTT         AAAAAAAAA         MM      MM        PPPPPPPPP          EEEEEEEEEE *\n"
 ."  SSSSSSSSSSS        TTTTTTTTTTT        AAAAAAAAAAA        MMM    MMM        PPPPPPPPPP         EEEEEEEEEE *\n"
 ."  SS       SS            TTT            AA       AA        MMMM  MMMM        PP      PP         EE *\n"
 ."    SS                   TTT            AA       AA        MM MMMM MM        PP      PP         EEEEEEEEEE *\n"
 ."      SS                 TTT            AAAAAAAAAAA        MM  MM  MM        PPPPPPPPPP         EEEEEEEEEE *\n"
 ."        SS               TTT            AAAAAAAAAAA        MM      MM        PPPPPPPPP          EE *\n"
 ."  SS      SS             TTT            AA       AA        MM      MM        PP                 EE *\n"
 ."  SSSSSSSSSSS            TTT            AA       AA        MM      MM        PP                 EEEEEEEEEE *\n"
 ."   SSSSSSSSS             TTT            AA       AA        MM      MM        PP                 EEEEEEEEEE *\n"
;
$lpre = qr/$lpre/;

my $debug = 0;

sub MayBeBanner {
  if ( $pageStr =~ /((?:^[1 ]*\*{50,}[^\n]*\n){3})/mso ) {
    return 1;
  }
  else {
    return 0;
  }
}

sub IsLastBanner {
  if ( $pageStr =~ /$lpre/mso ) {
    return 1;
  }
  else {
    return 0;
  }
}

sub getHdrVars {
  my $hdrVars;
  if ( 
     @{$hdrVars}{qw(pages lines jobName jobNumber JobExecDate JobExecTime copies)} 
       = 
     $pageStr =~ /$hdrre/mso 
   ) {
    $hdrVars->{copies} = 1 if !$hdrVars->{copies};
    return $hdrVars;
  }
  return undef;
}

sub getPage {
  my $started = 0;
  
  @page = ($lastline);

  $started = 1 if $lastcc ne "\x5a" ;
  
  while(1) {
    last if ($lastline = $INPUT->getLine()) eq ''; $lastcc = substr($lastline,0,1); 
   
    last if $started && $lastcc =~ /\xf1|\x8b/;

    if ( $lastcc eq "\x5a" ) {
      my $sf = substr($lastline,3,3);
      if ( $started ) {
        last if $sf eq "\xd3\xab\xcc" or $sf eq "\xd3\xab\xca";
      }
      else {
        last if ( $sf eq "\xd3\xab\xcc");
      }
    }
    $started = 1 if $lastcc ne "\x5a" ;
    
    push @page, $lastline;
  }
  $pageStr = join("\n", map { $translator->toascii($_) } @page)
}

sub getJobList {
  @JobList = ();
  for (reverse(@page)) {
    my $d = $translator->toascii($_);
    last if $d =~ /---------------------/; logIt("$d");
    ($descr, $copies, $lines, $pages, $jobName, $jobNumber) = getJobEntry($d);
    for ($copies, $lines, $pages, $jobName, $jobNumber) {
      $_ =~ s/^ +//; $_ =~ s/ +$//;
    }
    unshift @JobList, [ $descr, $copies, $lines, $pages, $jobName, $jobNumber ];
    $JobList{"$jobName |$descr|"} = $jobName;
  }
  for ( @JobList ) {
    logIt("/", join("//", @$_ ), "/");
  }
  for (sort(keys(%JobList))) {
    print $LOGOUT "\%$_ = $JobList{$_}\n"
  }
}

sub logIt {
  print @_, "\n";
  print $LOGOUT "MSG: ", @_, "\n";
}

sub logPage {
  print $LOGOUT "BEGIN PAGE ////////////////////\n";
  for (@page) {
    print $LOGOUT "%", $translator->toascii($_), "\n";
  }
  print $LOGOUT "END PAGE ////////////////////\n";
}

sub logAllPages {
  while(!$INPUT->eof()) {
    getPage;logPage();
  }
}

sub INIT_ALL {
  if ( $BUNDLEID =~ /^\d+$/ ) {
    my $dbr = dbExecute(
      "SELECT * from tbl_ctd_IN_Bundles WHERE BUNDLEID = $BUNDLEID"
    );
    if ( $dbr->eof() ) {
      die("BUNDLEID $BUNDLEID Not FOUND.");
    }
    ($fileName, $SrvName, $PeerHost) = $dbr->GetFieldsValues(qw(LocalFileName SrvName HostAddr));
  }
  else {
    $fileName = $BUNDLEID; $BUNDLEID = -1; 
    $SrvName='OTHELLO'; 
    $PeerHost = '10.36.130.1';
  }
  if ( $fileName =~ /\.(\d{14})\./ ) {
    $dateTime = $1;
  }
  else {
    die "DATETIME for $fileName NOT FOUND.";
  }

  $fileName = getConfValues('XferArea')->{'CtdBundlesIN'}."/$fileName";

  ### todo: manage XferMode -----------------------------------------------------------------

  $INPUT = new XReport::INPUT::ILine(XReport::INPUT::IZlib->new($fileName), 4); $INPUT->Open();

  $LOGOUT = gensym(); my $logFileName = $fileName; $logFileName =~ s/(?:\.gz)?$/.log/i;

  open($LOGOUT,">$logFileName") 
   or 
  die("LOGFILE OUTPUT OPEN ERROR $logFileName rc=$!\n"); binmode $LOGOUT;
  
  $lastline = $INPUT->getLine(); $lastcc = substr($lastline,0,1);

  getPage(); getJobList(); logPage(); getPage(); logPage();

  return if $BUNDLEID <= 0;

  dbExecute(
   "UPDATE tbl_ctd_IN_Bundles SET UnbundleTimeStart = GETDATE(), Status = 17 WHERE BUNDLEID = $BUNDLEID"
  )
}

sub TERMINATE_ALL {
  close($LOGOUT);
}

sub NewXferStart {
  $jrid = "$JobReportName/$jobName/$jobNumber/$JobExecDate/$JobExecTime";
  
  if ( !exists($JobReports{$jrid}) ) {

    my @dateTimeSQL = splitJobExecTime($JobExecDate, $JobExecTime);
    my $dateTimeSQL = dateTimeSQL(@dateTimeSQL);

    $progr += 1;
    
    $jr = NewXferStart XReport::JobREPORT(
      'SrvName'       =>  $SrvName, 
      'JobReportName' =>  $JobReportName, 
      'JobName'       =>  $jobName, 
      'JobNumber'     => "JOB$jobNumber", 
      'pid'           =>  $BUNDLEID,
      'progr'         =>  $progr, 
      'dateTime'      =>  $dateTime, 
      'debug'         =>  $debug
    );
    
    $jr->setValues(
      'ReportName'       => $JobReportName, 
      'RemoteHostAddr'   => $PeerHost, 
      'JobName'          => $jobName,
      'JobNumber'        => "JOB$jobNumber", 
      'JobExecutionTime' => $dateTimeSQL, 
      'XferMode'         => 2,
      'XferInLines'      => $lines, #from Manifest
      'XferInPages'      => $pages, #from Manifest
      'SrvName'          => $SrvName,
      'Status'           => 15
    );
  
    $JobReports{$jrid} = [$jr, 0];

    logIt("$dateTimeSQL ", $jr->getValues('LocalFileName'));
  }
  else {
    $jr = $JobReports{$jrid}->[0];
  }

  $OUTPUT = $jr->getValues('$OUTPUT'); return $jr;
}

sub getReportName {
  my ($CtdJobName, $descr) = @_; my $CtdJobNameMask = $CtdJobName; my $dbr;
  
  logIt("/$CtdJobNameMask phase=1/ /$descr/");
  
  $dbr = dbExecute(
    "SELECT * from tbl_ctd_JobNameAlias WHERE CtdJobName = '$CtdJobNameMask' "
  );
  if ( !$dbr->eof() ) {
    $CtdJobNameMask = $dbr->GetFieldsValues('CtdAliasOf');
    $dbr->Close();
  }
  
  logIt("/$CtdJobNameMask phase=2/ /$descr/");

  $dbr = dbExecute(
    "SELECT * from tbl_ctd_XrefReportData " .
    "WHERE '$CtdJobNameMask' = CtdJobName and '$descr' = 'CtdReportname' "
  );
  if ( $dbr->eof() ) {
    $dbr = dbExecute(
      "SELECT * from tbl_ctd_XrefReportData " .
      "WHERE '$CtdJobNameMask' LIKE CtdJobname and '$descr' like RTRIM(CtdReportName) "
    );
  }
  return undef if $dbr->eof();
  
  return $dbr->GetFieldsValues('XrJobReportName');
}

sub ExtractJobReports {
  my $PageHasBeenRead = 0;
  
  READJOBS:
  for my $job (@JobList) {
    ($descr, $copies, $lines, $pages, $jobName, $jobNumber) = @$job; 
  
    $JobReportName = getReportName($jobName, $descr)
      or 
    die "REPORTNAME MISSING FOR <$jobName |$descr|>";
  
    die "INVALID REPORTNAME $JobReportName" if $JobReportName !~ /\w+/;
    
    logIt("START OF $jobName $descr $copies $pages $lines");
    
    for ($atCopy=1; $atCopy<=$copies; $atCopy++) {
      ($pagesToRead, $linesToRead, $atLine, $atPage, $bytes, $JobExecDate, $JobExecTime) 
        = 
      ($pages, $lines, 0, 0, 0, "", "");
  
      READBANNER:
      while($JobExecDate eq '' or $PageHasBeenRead) {
        if ( $INPUT->eof() ) {
          logIt("AT EOF $jobName $pages $pagesToRead"); last READJOBS;
        }
        if ( !$PageHasBeenRead ) {
          getPage();logPage();
        }
        $PageHasBeenRead = 0;

        my $hdrVars = getHdrVars(); next if !$hdrVars;

        ($JobExecDate, $JobExecTime) = @{$hdrVars}{qw(JobExecDate JobExecTime)};

        if ( 
          $jobName ne $hdrVars->{'jobName'} or
          $jobNumber ne $hdrVars->{'jobNumber'} or
          $copies != $hdrVars->{'copies'} or
          $pages != $hdrVars->{'pages'} or
          $lines != $hdrVars->{'lines'}
        ) {
          ### may be more copies for previous jobreport ??? ###
          ($JobExecDate, $JobExecTime) = ("", ""); 
          for ( qw(jobName jobNumber copies pages lines) ) {
            logIt("$_ ", eval('$'.$_), " ", $hdrVars->{$_});
          }
          logIt("NOT MATCHING HDR PAGE !");
        }
      }
  
      READREPORT:
      while( 1 ) {
        #die "AT EOF $jobName $pages $pagesToRead" if $INPUT->eof();
        getPage();
        {
          if ( (MayBeBanner() and getHdrVars()) or IsLastBanner() ) {
            if ( ($atLine != $lines) or ($atPage != $pages) ) {
              logIt("REPORT $progr MISSING OR INCOMPLETE !!!");
            }
            $PageHasBeenRead = 1; logPage();
            last READREPORT;
          }
          push @$job, NewXferStart() if $atPage == 0;
        }
        $atLine+=scalar(@page); $atPage += 1; 
        if ( $atCopy == 1 ) {
          $OUTPUT->putPage(\@page); 
          for ( @page ) {
            $bytes += length($_) + 2;
          }
        }
        $pagesToRead -= 1; $linesToRead -= scalar(@page);
      }
      $JobReports{$jrid}->[1]+=$bytes if $atCopy == 1;
    }
    $jr->setValues('XferInBytes', $OUTPUT->tell()) if $atPage > 0;
  
    $atFile++; #last if $atFile>=10;
    
    logIt(
      "MISMATCH on Number of Lines/Pages for JOB $jobName ",
      "|$descr| expLines=$lines recvLines=$atLine expPages= $pages recvPages=$atPage"
    ) 
    if ( $atLine!=$lines or $atPage!=$pages );
  }
}


sub RegisterJobReports {
  for $jrid ( keys(%JobReports) ) {
    my ($jr, $bytes) = ( @{$JobReports{$jrid}} ); next if !$jr; my $OUTPUT = $jr->getValues('OUTPUT'); 

    logIt("xfer/bytes computed=$bytes tell=", $OUTPUT->tell());

    $jr->NewXferEnd(); $jr->QueueForElab();
  }

  return if $BUNDLEID <= 0;

  dbExecute(
    "UPDATE tbl_ctd_IN_Bundles SET UnbundleTimeEnd = GETDATE(), Status = 18 WHERE BUNDLEID = $BUNDLEID"
  );
}

$BUNDLEID = shift || die("BUNDLEID/fileName parameter missing !!");

eval {
  INIT_ALL(); #logAllPages();exit;

  ExtractJobReports(); die "232323";

  RegisterJobReports();

  ### todo display reports missing ###;

  TERMINATE_ALL();
};

logIt("ERROR ERROR ???? $@") if $@;

dbExecute(
  "UPDATE tbl_ctd_IN_Bundles SET UnbundleTimeEnd = GETDATE(), Status = 31 WHERE BUNDLEID = $BUNDLEID"
)
if ( $@ and $BUNDLEID > 0 );
