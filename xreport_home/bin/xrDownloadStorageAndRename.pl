#!perl -w

use strict 'vars';
use constant DEBUG => 0; 
use constant COMPRESSION_STORED => 0;
use constant COMPRESSION_DEFLATED => 8;	
 
use Data::Dumper; 

use XReport;
use XReport::JobREPORT;
use XReport::DBUtil;  

use File::Basename;
use Archive::Tar;

use IO::String qw();
use Archive::Zip qw(); 

$main::myName = ( split( /[\/\\]/, $0 ) )[-1];
$main::myName = ( split( /\./, $main::myName ) )[0];

sub print2Log { my @array =@_; return i::logit("$main::myName: ".join(",", map { $_ =~ s/\//\\/g; $_ } @array)."_LINE[".(caller())[2]."]"); }


sub retrieveListIdByQuery {
	($main::veryverbose || DEBUG) && print2Log("call retrieveListIdByQuery()"); 
	my ($listJobReportId_ref) = @_;	
#	foreach my $id(724,725,726,727,730,734,735,736,737,738)
#	{
#		push @{$listJobReportId_ref}, $id;
#	}
	
	my ($dbr, @JobReportIds);

	use XML::Simple;
	use Data::Dumper;
	
	my $cfgFile  = $ENV{'XREPORT_SITECONF'}."/xml/".$main::myName.".xml"  ;
	
	die " cfgFile query not found : $cfgFile" unless(-f $cfgFile);
			 
	my $sql  =  XML::Simple::XMLin($cfgFile, ForceArray => [qw(job step)])->{'sqlprocs'}->{'query'};
	
	die " sql undefined : $cfgFile" unless $sql;
	 
	#
	#my $sql = ""
	#."\n select *                                            "
	#."\n from tbl_JobReports a                               "
	#."\n where status = 31                                   "
	#."\n and JobReportName = 'CDAMFILE'                      "
	#."\n and LocalFileName like '%CDAMFILE%'                 "
	#."\n and  LocalPathId_IN <> ''                           "
	#."\n --and  LocalPathId_IN <> 'C1'                          "
	#."\n and  LocalPathId_IN <> 'PWXXX'                      "
	#."\n --and JobReportId not in ( 146,410 )--only A1TEST   "
	#."\n order by JobReportId desc                           "
	#."\n";
	print2Log("query: $sql"); 
	$dbr = dbExecute($sql);

	while(!$dbr->eof()) {
			my $jrid = $dbr->Fields()->Item('JobReportId')->Value();
			push @{$listJobReportId_ref}, $jrid;
			$dbr->MoveNext();
	}
}
sub renameJobReportName {
	($main::veryverbose || DEBUG) && print2Log("call renameJobReportName()"); 
	my ($jr, $cfgvars_ref, $doUpdate) = @_; 
	my $JobReportId = $$cfgvars_ref->{JobReportId};
	my $oldJobReportName = $$cfgvars_ref->{JobReportName};
	my $newJobReportName = $$cfgvars_ref->{originalJobReportName} ;
	$$cfgvars_ref->{newJobReportName} = $newJobReportName;
	my $newLocalFileName = $$cfgvars_ref->{LocalFileName} ;
	if( $$cfgvars_ref->{centera} == 0 )
	{
		$newLocalFileName =~ s/$oldJobReportName/$newJobReportName/i ;	
		$$cfgvars_ref->{new_LocalFileName} = $newLocalFileName;
	}
	my $old_fullfilepath_IN = $$cfgvars_ref->{fullfilepath_IN} ;
	my $new_fullfilepath_IN = $old_fullfilepath_IN ;
	$new_fullfilepath_IN =~ s/$oldJobReportName/$newJobReportName/i ;
	$$cfgvars_ref->{new_fullfilepath_IN} = $new_fullfilepath_IN;
	my $old_fullfilepath_OUT = $$cfgvars_ref->{fullfilepath_OUT} ;
	my $new_fullfilepath_OUT = $old_fullfilepath_OUT ;
	$new_fullfilepath_OUT =~ s/$oldJobReportName/$newJobReportName/i ;	
	$$cfgvars_ref->{new_fullfilepath_OUT} = $new_fullfilepath_OUT;
	return unless($doUpdate);
	_updateDataFromSQL("BEGIN TRANSACTION REPORTADD");
	eval 
	{ 
		my $sql_update =
			"UPDATE tbl_JobReports SET JobReportName = '$newJobReportName', ".
			"LocalFileName = '$newLocalFileName' ".
			"WHERE (JobReportId = $JobReportId ) ;\n".
			"DELETE from tbl_WorkQueue  ".
			"WHERE (ExternalTableName='tbl_JobReports' ) ".
			"AND (ExternalKey = $JobReportId) ;\n".
			"INSERT INTO tbl_WorkQueue " .
			"( " .
			"TypeOfWork, Priority, Status, InsertTime, ExternalKey, ExternalTableName " .
			") " .
			"VALUES " .
			"( " .
			"1, 10, 16, GETDATE(), $JobReportId, 'tbl_JobReports' " .
			");\n";
		print2Log("sql=".$sql_update); 
		_updateDataFromSQL($sql_update);
		print2Log("UPDATED JobReports LocalFilename for JobReportId[$JobReportId]" );
		
		if( $$cfgvars_ref->{centera} == 0 )
		{
			if ((-e $old_fullfilepath_IN) && (-f $old_fullfilepath_IN) && ($old_fullfilepath_IN !~ /^centera:\/\//i ))
			{
				rename($old_fullfilepath_IN, $new_fullfilepath_IN) or die "Cannot rename $old_fullfilepath_IN to $new_fullfilepath_IN: $!, $^E" ;
				print2Log("Input archive file renamed with success: $new_fullfilepath_IN."); 
			}
			if ((-e $old_fullfilepath_OUT) && (-f $old_fullfilepath_OUT) && ($old_fullfilepath_OUT !~ /^centera:\/\//i ))
			{
				rename($old_fullfilepath_OUT, $new_fullfilepath_OUT) or die "Cannot rename $old_fullfilepath_OUT to $new_fullfilepath_OUT: $!, $^E" ;
				print2Log("Output archive file renamed with success: $new_fullfilepath_OUT."); 
			}
		}
	};
	if($@) {
		die " FAILED: $@";
		_updateDataFromSQL("ROLLBACK TRANSACTION REPORTADD");
		print2Log("ROLLBACK TRANSACTION REPORTADD" );
		
	} else {
		_updateDataFromSQL("COMMIT TRANSACTION REPORTADD");
		print2Log("COMMIT TRANSACTION REPORTADD" );
		
	}
}
sub readHeaderFile {
	($main::veryverbose || DEBUG) && print2Log("call readHeaderFile()"); 
	my ($filename, %args) = @_;   
	open(my $INPUT, '<:encoding(UTF-8)', $filename)
			or die "Could not open file '$filename' $!"; 
	binmode($INPUT); 
	my $header = ''; 
	my $OFH = new IO::String($header); 
	my $buff = ' ' x 512;
	while(1) {
		read($INPUT, $buff, 512);  
		last if $buff eq ''; 
		$OFH->syswrite($buff); 
		last;
	}
	close($INPUT);  
	$OFH->close();
	return $header;
}
sub retrieveOriginalJobReportName {
	($main::veryverbose || DEBUG) && print2Log("call retrieveOriginalJobReportName()"); 
	my ($jr, $cfgvars_ref, %args) = @_;  
	$$cfgvars_ref->{'centera'} = 0;
	$$cfgvars_ref->{'originalJobReportName'} = '';
	$$cfgvars_ref->{'fullfilepath_IN'} = ''; 
	$$cfgvars_ref->{'fullfilepath_OUT'} = ''; 
  
	my $fullfilepath_IN = $$cfgvars_ref->{TargetLocalPathIn}.'/IN/'. $$cfgvars_ref->{'LocalFileName'};
	$fullfilepath_IN =~ s/^file:\/\///i;
	my $fullfilepath_OUT = $$cfgvars_ref->{TargetLocalPathOut}.'/OUT/'. $$cfgvars_ref->{'LocalFileName'};
	$fullfilepath_OUT =~ s/^file:\/\///i;
	$fullfilepath_OUT =~ s/\.DATA\.TXT\.gz\.tar$/\.OUT\.#0\.zip/i;
	$$cfgvars_ref->{'fullfilepath_IN'} = $fullfilepath_IN;
	$$cfgvars_ref->{'fullfilepath_OUT'} = $fullfilepath_OUT;		
		
	my $originalJobReportName = ''; 
	if ($$cfgvars_ref->{TargetLocalPathIn} !~ /^centera:\/\//i)
	{
		die("Error in TargetLocalPathIn:".$$cfgvars_ref->{TargetLocalPathIn} ) if ($$cfgvars_ref->{TargetLocalPathIn} !~ /^file:\/\//i);
		($main::veryverbose || DEBUG) && print2Log("input from local Storage");
		$$cfgvars_ref->{'centera'} = 0;

		die("Error - file not found :[$fullfilepath_IN]") unless ((-e $fullfilepath_IN) && (-f $fullfilepath_IN));
		($main::veryverbose || DEBUG) && print2Log("fullfilepath_IN: $fullfilepath_IN");
		
		my $header = readHeaderFile($fullfilepath_IN);
		if( $header =~ /^([^\n]+)\.[0-9]+\.[0-9]+\.(DATA\.TXT\.gz|CNTRL\.TXT)/i )
		{
			$originalJobReportName = $1; 
			($main::veryverbose || DEBUG) && print2Log("header: [$header]"); 
			($main::veryverbose || DEBUG) && print2Log("originalJobReportName: [$originalJobReportName]");
		}
		else
		{
			my $tar = Archive::Tar->new($fullfilepath_IN); 
			foreach my $iFileName (grep  {$_ =~ /\.(DATA\.TXT\.gz|CNTRL\.TXT)[ \t]*$/i } $tar->list_files() ) {
				$iFileName =~ /^([^\n]+)\.[0-9]+\.[0-9]+\.(DATA\.TXT\.gz|CNTRL\.TXT)[ \t]*$/i;  
				($main::veryverbose || DEBUG) && print2Log("iFileName1: [$iFileName]");
				$originalJobReportName = $1;
				($main::veryverbose || DEBUG) && print2Log("originalJobReportName: [$originalJobReportName]");
				last;
			} 
		}
	}
	else
	{
		($main::veryverbose || DEBUG) && print2Log("input from Centera");   
		$$cfgvars_ref->{'centera'} = 1;			
		my $ijrar = $jr->get_INPUT_ARCHIVE();   
		my $cdamfh = $ijrar->get_INPUT_ARCHIVE();
		
		my $FileTags = $cdamfh->FileTags(); 
		foreach my $hash (@$FileTags)
		{ 
			foreach my $key (sort keys %$hash)
			{
				#print2Log("TESTSAN - FileTags - $key: ". $hash->{$key} ); 
				($main::veryverbose || DEBUG) && print2Log("FileTags - $key: ". $hash->{$key} ); 
				if(( $key =~ /^md5$/i) && (!($hash->{$key})) )
				{
					$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 1;
					print2Log("noCheckMd5 on XReport::ARCHIVE::Centera::File::INPUT.pm"); 
				} 
			}
		}			
		
		my $FileList = $cdamfh->FileList();  
		foreach my $iFileName (grep /\.DATA\.TXT\.gz\.tar$/i, @$FileList) { 
			($main::veryverbose || DEBUG) && print2Log("iFileName: ".$iFileName); 
			my $header = $cdamfh->HeaderFileContent($iFileName);  
			($main::veryverbose || DEBUG) && print2Log("header: [$header]"); 
			if( $header =~ /^([^\n]+)\.[0-9]+\.[0-9]+\.(DATA\.TXT\.gz|CNTRL\.TXT)/i )
			{
				$originalJobReportName = $1;  
				($main::veryverbose || DEBUG) && print2Log("originalJobReportName: [$originalJobReportName]");
			}
			else
			{
				my $FileContents = $cdamfh->FileContents($iFileName); 
				#print2Log("FileContents: $FileContents."); 
				
				my $OUTPUT = new IO::String($FileContents);
				$OUTPUT->binmode();
				my $tar = Archive::Tar->new($OUTPUT); 
				foreach my $iFileName (grep  {$_ =~ /\.(DATA\.TXT\.gz|CNTRL\.TXT)[ \t]*$/i } $tar->list_files() ) {
					$iFileName =~ /^([^\n]+)\.[0-9]+\.[0-9]+\.(DATA\.TXT\.gz|CNTRL\.TXT)[ \t]*$/i;  
					($main::veryverbose || DEBUG) && print2Log("iFileName1: [$iFileName]");
					$originalJobReportName = $1;
					($main::veryverbose || DEBUG) && print2Log("originalJobReportName: [$originalJobReportName]");
					last;
				}
				$OUTPUT->close; 
			}
			last;
		} 
	} 
	
	$$cfgvars_ref->{'originalJobReportName'} = $originalJobReportName;
	$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 0;
}
sub downloadInput {
	($main::veryverbose || DEBUG) && print2Log("call downloadInput()"); 
	my ($jr, $cfgvars, %args) = @_;
	my $FQoutName =  $cfgvars->{Workdir}.'/'. File::Basename::basename($cfgvars->{'LocalFileName'});
	$FQoutName =~ s/\.gz\.tar$/\.IN\.gz\.tar/; 
	my $TempFQoutName = "$FQoutName\.PENDING";
	if ($cfgvars->{TargetLocalPathIn} !~ /^centera:\/\//i)
	{
		die("Error in TargetLocalPathIn:".$cfgvars->{TargetLocalPathIn} ) if ($cfgvars->{TargetLocalPathIn} !~ /^file:\/\//i);
		$jr->setValues('line_iexec' => ''); 
        my $INPUT = $jr->get_INPUT_ARCHIVE();
        die "Unable to acquire INPUT_ARCHIVE handler -", Data::Dumper::Dumper($jr), "\n" unless $INPUT;

        my $is = $INPUT->get_INPUT_STREAM_GZ($jr, random_access => 0); 
		
		die "Unable to acquire INPUT_STREAM_GZ handler -", Data::Dumper::Dumper($jr), "\n" unless $is;
       
        ($main::veryverbose || DEBUG) && print2Log("INPUT STREAM acquired - INPUT REF: ".ref($INPUT)." IS ref: ".ref($is));
		
		my $outfh = IO::File->new($TempFQoutName, 'w');
        $outfh->binmode();
		$is->Open();
        my $buff = ' ' x 131072; 
		while(1) { 
			$is->read($buff, 131072); 
			last if $buff eq ''; 
			print $outfh $buff;  
		}
        $is->Close();
        $outfh->close();
	}
	else
	{
		($main::veryverbose || DEBUG) && print2Log("input from Centera");  
		my $ijrar = $jr->get_INPUT_ARCHIVE();   
		my $cdamfh = $ijrar->get_INPUT_ARCHIVE(); 
		
		my $FileTags = $cdamfh->FileTags(); 
		foreach my $hash (@$FileTags)
		{ 
			foreach my $key (sort keys %$hash)
			{
				($main::veryverbose || DEBUG) && print2Log("FileTags - $key: ". $hash->{$key} ); 
				if(( $key =~ /^md5$/i) && (!($hash->{$key})) )
				{
					$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 1;
					print2Log("noCheckMd5 on XReport::ARCHIVE::Centera::File::INPUT.pm"); 
				}
			}
		}
		
		my $FileList = $cdamfh->FileList();
		foreach my $iFileName (grep /\.DATA\.TXT\.gz\.tar$/i, @$FileList) {
			($main::veryverbose || DEBUG) && print2Log("iFileName: ".$iFileName); 
			my $INPUT = $cdamfh->LocateFile($iFileName); 
			die("ExtractFile $iFileName NOT Located") if !$INPUT; 

			$INPUT->Open();
			my $OUTPUT = IO::File->new(">". $TempFQoutName)
			or die("OPEN OUTPUT ERROR for \"$FQoutName\.PENDING\" $!"); 
			$OUTPUT->binmode();
			my $osize = $cdamfh->ExtractMember2FH($INPUT, $OUTPUT);
			($main::veryverbose || DEBUG) && print2Log("osize: ".$osize); 
        	$INPUT->Close();
        	$OUTPUT->close; 
			last;
		}
	} 
	
	die("Error - the Input file [$FQoutName] already exists.") if ((-e $FQoutName) && (-f $FQoutName));
	#unlink $FQoutName if ((-e $FQoutName) && (-f $FQoutName));
	rename($TempFQoutName, $FQoutName) or die("ERROR in RENAME file [$FQoutName]."); 
	print2Log("Input archive file created with success: $FQoutName.");
	$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 0; 
}
sub downloadOutput {
	($main::veryverbose || DEBUG) && print2Log("call downloadOutput()"); 
	my ($jr, $cfgvars, %args) = @_;
	my $jobReportId = $cfgvars->{JobReportId}; 
	
    my $ijrar;
    eval {
        $ijrar = XReport::JobREPORT::get_OUTPUT_ARCHIVE($jobReportId); 
		($main::veryverbose || DEBUG) && print2Log("ijrar REF: ".ref($ijrar)); 
    };
    my $openrc = $@;
    if($@) { 
		die "Unable to open Output Archive for JobReportId $jobReportId($@)";
    } 
	for my $FileName (@{$ijrar->FileNames()}) { 
			($main::veryverbose || DEBUG) && print2Log("FileName=$FileName");  
	}
	
	my $FQoutName =  $cfgvars->{Workdir}.'/'. File::Basename::basename($cfgvars->{LocalFileName});
	$FQoutName =~ s/\.DATA\.TXT\.gz\.tar$/\.OUT\.zip/; 
	
		
	for my $key (sort keys %$ijrar)
	{ 
		if(($key=~ /^FileName$/i) && ($ijrar->{$key} =~ /\.zip$/))
		{
			$FQoutName = $cfgvars->{Workdir}.'/'. File::Basename::basename($ijrar->{$key});	 
		}  
		($main::veryverbose || DEBUG) && print2Log("keys: \$ijrar->{$key}:[".$ijrar->{$key}."]");
	}
	
	my $TempFQoutName = "$FQoutName\.PENDING";
	($main::veryverbose || DEBUG) && print2Log("FQoutName: $FQoutName");
	my $outfh = IO::File->new($TempFQoutName, 'w');
	$outfh->binmode();
    eval {
		($main::veryverbose || DEBUG) && print2Log("JobReport $jobReportId Output archive opened"); 
        my $ozip = new Archive::Zip;
		
		#if ( ref($ijrar) !~ /^XReport::ARCHIVE::Zip$/i )
		#if ( ref($ijrar) =~ /^XReport::ARCHIVE::Centera::FPClip$/i ) 
		if ( ref($ijrar) =~ /Centera/i )
		{
			my $FileTags = $ijrar->FileTags(); 
			foreach my $hash (@$FileTags)
			{ 
				foreach my $key (sort keys %$hash)
				{					
					($main::veryverbose || DEBUG) && print2Log("FileTags - $key: ". $hash->{$key} ); 
					if(( $key =~ /^md5$/i) && (!($hash->{$key})) )
					{
						$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 1;
						print2Log("noCheckMd5 on XReport::ARCHIVE::Centera::File::INPUT.pm"); 
					}
				}
			}  
		}
		
        my $osize = 0;
        my $FileList = $ijrar->FileList();
        foreach my $iFileName (@$FileList) { 
			my $INPUT = $ijrar->LocateFile($iFileName);
			die("ExtractFile output archive $iFileName NOT Located") if !$INPUT; 
			my $member = ''; 
			my $OFH = new IO::String($member);

           $INPUT->Open();
           my $buff = ' ' x 131072;
			while(1) { 
				$INPUT->read($buff, 131072); 
				last if $buff eq ''; 
				$OFH->syswrite($buff); 
			}
           $INPUT->Close();
           $OFH->close();

           my $string_member = $ozip->addString( $member, $iFileName );
           $string_member->desiredCompressionMethod( COMPRESSION_DEFLATED );
        } 
		$ozip->writeToFileHandle($outfh); 
		$outfh->close();
	};
    if($@) { 
        die "Content not available for JobReportId $jobReportId($@)";
    }  
	#unlink $FQoutName if ((-e $FQoutName) && (-f $FQoutName));
	die("Error - the Output file [$FQoutName] already exists.") if ((-e $FQoutName) && (-f $FQoutName));
	rename($TempFQoutName, $FQoutName) or die("ERROR in RENAME file [$FQoutName].");
	print2Log("Output archive file created with success: $FQoutName."); 
	$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 0;
					
}
sub initiateJR(){
	($main::veryverbose || DEBUG) && print2Log("call initiateJR()"); 
	
	my ($JRID, $workdir, $jr_ref, $cfgvars_ref) = @_;		
	print2Log("Initialize work for JobReportId[$JRID]"); 

	${$jr_ref}  = XReport::JobREPORT->Open($JRID, 0);  

	my ($LocalPathId_IN, $LocalPathId_OUT, $jobReportId, $status, $fileName, $SrvName, $PeerHost, $XferMode, $JobName, $JobReportName, $XferEndTime) = 
			${$jr_ref}->getValues(qw(LocalPathId_IN LocalPathId_OUT JobReportId Status LocalFileName SrvName RemoteHostAddr XferMode XferDaemon JobReportName XferEndTime));
	my $TargetLocalPathIn = $XReport::cfg->{'LocalPath'}->{$LocalPathId_IN};
	my $TargetLocalPathOut = $XReport::cfg->{'LocalPath'}->{$LocalPathId_OUT};

	${$cfgvars_ref} = {}; 
	@{${$cfgvars_ref}}{qw(TargetLocalPathIn  TargetLocalPathOut LocalPathId_IN LocalPathId_OUT JobReportId Workdir XferEndTime Status LocalFileName SrvName RemoteHostAddr XferMode
						XferDaemon JobReportName)} = ($TargetLocalPathIn, $TargetLocalPathOut, $LocalPathId_IN, $LocalPathId_OUT, $jobReportId, $workdir, $XferEndTime, $status, $fileName, $SrvName, $PeerHost, $XferMode, $JobName, $JobReportName);
		
	foreach my $key (grep {defined ${$cfgvars_ref}->{$_}} sort keys %${$cfgvars_ref})
	{
		($main::veryverbose || DEBUG) && print2Log("$key: ". ${$cfgvars_ref}->{$key}); 
	} 

	($main::veryverbose || DEBUG) && print2Log("Data::Dumper::Dumper(\${$jr_ref}) -", Data::Dumper::Dumper(${$jr_ref}), "\n" );
		
	die "Syntax error in LocalFileName:".${$cfgvars_ref}->{LocalFileName} if ${$cfgvars_ref}->{LocalFileName} !~ /[^.]+\.(\d+)\.[\d#]+\.DATA.TXT/i ; 
		
}
sub _updateDataFromSQL {
  ($main::veryverbose || DEBUG) && print2Log("call _updateDataFromSQL()" );
	my ($sql_update) = (shift);
			my $dbr = XReport::DBUtil::dbExecute($sql_update);	
}

print2Log("- $0($$) starting");
my ($workdir, $JRID) = @ARGV[0,1];
#if (-d $workdir ) {
##  $workdir =~ s/\//\\/sig;
#  my @flist = glob $workdir.'\*'; 
#  warn "deleting now\n", join("\n", @flist), "\n";
#  unlink @flist;
#}
 
 
my $maxUpdates = 9999999;
my $countUpdates = 0;
foreach my $i(0..$#ARGV)
{
	if ($ARGV[$i] =~ /^-(maxUpdates|maxUpdate|max)$/i)
	{
		$maxUpdates = $ARGV[$i+1] ;
		print2Log("- maxUpdates: $maxUpdates");
		last;			
	}
}

my $retrieveOriginalJobReportName = 0;
if ( grep /^-retrieveOriginalJobReportName$/i, @ARGV ) {
	$retrieveOriginalJobReportName =1;
	print2Log("- MODE retrieveOriginalJobReportName ACTIVATED"); 
} 
my $doUpdate = 0;
if ( grep /^-(doUpdate|update)$/i, @ARGV ) {
	$doUpdate =1;
	print2Log("- MODE doUpdate ACTIVATED"); 
} 
else
{
	$doUpdate =0;
	print2Log("- MODE noUpdate ACTIVATED"); 
}
my $downloadInput = 0;
if ( grep /^-downloadInput$/i, @ARGV ) {
	$downloadInput =1;
	print2Log("- MODE downloadInput ACTIVATED"); 
} 
my $downloadOutput = 0;
if ( grep /^-downloadOutput$/i, @ARGV ) {
	$downloadOutput =1;
	print2Log("- MODE downloadOutput ACTIVATED"); 
} 
if (( grep /^-dd$/i, @ARGV ) || DEBUG ){
	$main::veryverbose =1;
	print2Log("- MODE veryverbose ACTIVATED"); 
}

$downloadOutput or $downloadInput or $retrieveOriginalJobReportName or die "$main::myName: - No action is required. You have to specify at least one of the following parameters: -downloadInput or -downloadOutput or -retrieveOriginalJobReportName";
$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 0;

my ($jr, $cfgvars);  
my @listJobReportId =();
if ( grep /^-(list|query)$/i, @ARGV )
{ 
	print2Log("- MODE list/query ACTIVATED"); 
	retrieveListIdByQuery(\@listJobReportId);
}
else
{
	print2Log("- MODE singleJobReportId ACTIVATED"); 
	push @listJobReportId, $JRID;
}

foreach my $JobReportId (@listJobReportId)
{ 
	&initiateJR($JobReportId, $workdir, \$jr, \$cfgvars);   
	downloadInput($jr, $cfgvars) if $downloadInput;
	downloadOutput($jr, $cfgvars) if $downloadOutput;
	if ($retrieveOriginalJobReportName) {
		retrieveOriginalJobReportName($jr, \$cfgvars);  
			
		foreach my $key ( sort keys %{$cfgvars})
		{
			($main::veryverbose || DEBUG) && print2Log("$key: [". $cfgvars->{$key}."]"); 
		} 
		if(($cfgvars->{'JobReportName'} ne $cfgvars->{'originalJobReportName'}) && ($cfgvars->{'originalJobReportName'} ne '' ) )
		{ 
			print2Log("::retrieveOriginalJobReportName - JobReportName[$JobReportId] : [".$cfgvars->{'JobReportName'}."] is different respect to the originalJobReportName [".$cfgvars->{'originalJobReportName'}."] ."); 
			if($cfgvars->{'originalJobReportName'} =~ /XRRENAME/i) 
			{
				print2Log("::retrieveOriginalJobReportName - the original JobReportName[$JobReportId] is [".$cfgvars->{'originalJobReportName'}."] no action is done. "); 
			}
			else
			{
				print2Log("before renameJobReportName");
				renameJobReportName($jr, \$cfgvars, $doUpdate) if $doUpdate;
				print2Log("after renameJobReportName");
				$countUpdates++;
				if($countUpdates >= $maxUpdates)
				{
					print2Log("Reached the max number oj JobReportId updates: $maxUpdates");
					last;
				}
			}
		}
		else
		{
			print2Log("::retrieveOriginalJobReportName - JobReportName[$JobReportId] : [".$cfgvars->{'JobReportName'}."] no action is done."); 
		}
	}

}
 
print2Log("- $0($$) ended");