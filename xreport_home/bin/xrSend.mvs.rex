/* Mode: REXX -----------------------------------------------------*/
/*     LISTA SPAZIO UTILIZZATO PER PROCEDURA FILE CICS + BATCH     */
/*-----------------------------------------------------------------*/
  TRACE 'n'
  PARSE ARG DSN2 PRTQ .
/*-----------------------------------------------------------------*/
/*     IMPOSTAZIONE COSTANTI DI CONTROLLO                          */
/*-----------------------------------------------------------------*/
  TOD = 'T'!!TIME('S')
  HNM = 'HOST OS390.BIPOP.IT'
  LNM = 'USER SSYS02'
  PAS = 'PWD IU71SETT'
  TYP = 'TYPE I'
  S1  = 'QUOTE MODE C'
  S2  = 'SITE AUTOMOUNT'
  CTL = "DSN '"DSN2"'"
/*-----------------------------------------------------------------*/
  QUEUE HNM
  QUEUE LNM
  QUEUE PAS
  QUEUE TYP
  QUEUE S1
  QUEUE S2
  QUEUE CTL
/*-----------------------------------------------------------------*/
 EXIT XRCLIENT("GETFTPCQ."PRTQ "172.26.2.134 515")
/*-----------------------------------------------------------------*/
