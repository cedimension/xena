
use strict vars;

use Data::Dumper;

use Compress::Zlib;
use File::Basename;
use FileHandle;
use Net::FTP;

use XReport;
use XReport::DBUtil; use XReport::QUtil;
use XReport::Util; use XReport::Util qw(InitServer TermServer);

my ($SrvName, $dirs);

use constant RF_ASCII => 1;
use constant RF_EBCDIC => 2;
use constant RF_AFP => 3;
use constant RF_VIPP => 4;

use constant XF_LPR => 1;
use constant XF_PSF => 2;
use constant XF_FTPB => 3;
use constant XF_FTPC => 4;

use constant ST_ACCEPTED => 0;
use constant ST_RECEIVED => 116;
use constant ST_QUEUED => 16;
use constant ST_INPROCESS => 17;
use constant ST_COMPLETED => 18;
use constant ST_PROCERROR => 31;

INITIALIZE();

sub setXferFileA {
  my ($queuename, $progr, $datetime) = @_;

  $datetime = GetDateTime() unless ($datetime && $datetime =~ /^\d{14}$/);
  my ($curryear, $currday, undef) = unpack("a4a4a*", $datetime);

#  my $storage = getConfValues('LocalPath')->{'L1'}."/IN";
#  if (! -e $storage."/".$curryear."/".$currday ) {
#    mkdir $storage."/".$curryear if (! -e $storage."/".$curryear);
#    mkdir $storage."/".$curryear."/".$currday;
#    die "unable to create $storage $curryear $currday dir\n" 
#	 unless 
#	(-e $storage."/".$curryear."/".$currday);
#  }
#
#  my $filen = "$curryear/$currday/$queuename.$datetime.$progr.DATA.TXT";
#  
#  return ($filen, $storage, $datetime);

  my $filen = "$queuename.$datetime.$progr.DATA.TXT";
  my $tgt = XReport::ARCHIVE::newTargetPath('L1', "IN/$curryear/$currday");
#  $hndl->{LocalPathId_IN} = $tgt->{LocalPathId};
  my @xrspool = split /(?<![\/\\])[\/\\](?![\/\\])/, $tgt->{fqn};
  
  
   return (join('/', splice(@xrspool, -2))."/".$filen, join('/', @xrspool), $datetime);
}

sub QDelete {
  my $jrid = shift;
  
  dbExecute("
    DELETE from tbl_JobReports 
	where JobReportId=$jrid
  ");
  
  dbExecute("
    DELETE from tbl_WorkQueue 
	where 
	 ExternalTableName='tbl_JobReports' 
	 AND ExternalKey = $jrid AND TypeOfWork = 1
  ");
}

sub DirLoop {
  &$logrRtn("DirLoop ENTRY");

  my %args = (
	'maxLS'   => 240,
    'sleepLS' => 60,
    'debug' => 0,
	%{$_[0]}
  );
  
  my ($host, $dir, $proc, $maxLS, $sleepLS, $debug) 
   =
  @args{qw(host dir proc maxLS sleepLS debug)};  
  
  my ($rc, $fmFile, $toFile) = ();
  
  my $dirList = $dir;
  
  for (1..$maxLS) {
    for $dir (@$dirList) {
	  my ($cwd, $prfx, $suffix) = @{$dir}{qw(cwd prfx suffix)}; 
      print "($cwd, $prfx, $suffix)\n";

	  my ($rename, $delete) = @{$dir}{qw(rename delete)};
      print "($rename, $delete)\n";

      my ($fmFile_, $JobReportName, $JobReportId, $INPUT);
  
      my @files = glob("$cwd/$prfx*$suffix");
  
      for $fmFile_ (@files) {
        $fmFile = basename($fmFile_); print "atFile $fmFile\n";
        if ( $fmFile !~ /^$prfx.*/ ) {
          die("ListError: FILE $fmFile DOESN'T START WITH $prfx !!.");
        }
        my $JobReportName = &{$dir->{'checkFile'}}($fmFile);
        next if $JobReportName eq "";
		
        my $dt = GetDateTime(); my $dbr;
		
		my ($JobReportId, $Status, $toFile, $todir, $datetime);
        
		### prepare transfer --------------------------------------------------

        $dbr = dbExecute("
           SELECT * from tbl_JobReportsPendingXfers
           WHERE HostName = '$host' AND RemoteFileName = '$fmFile_'
        "); 

        if ($dbr->eof()) {
          $JobReportId = QCreate XReport::QUtil(
                         SrvName        => $SrvName,
                         JobReportName  => $JobReportName,
                         LocalFileName  => $JobReportName . $$ . '001',
                         RemoteHostAddr => $host,
                         RemoteFileName => $fmFile_,
                         XferStartTime  => "GETDATE()",
                         XferMode       => XF_PSF,
                         XferDaemon     => $SrvName,
                         Status         => ST_ACCEPTED,
                         XferId         => 123,
                         );
          ($toFile, $todir, $datetime) = setXferFileA($JobReportName, $JobReportId);
          dbExecute("
            INSERT INTO tbl_JobReportsPendingXfers
            (HostName, RemoteFileName, JobReportid, TimeRef)
            VALUES ('$host', '$fmFile_', $JobReportId, $datetime)
          ");
        }
		else {
          $JobReportId = $dbr->Fields()->Item('JobReportid')->Value();
          $datetime = $dbr->Fields()->Item('TimeRef')->Value();
          ($toFile, $todir, $datetime) = setXferFileA($JobReportName, $JobReportId, $datetime);
        }

        $dbr->Close();	
        
		my $toFile_ = "$todir/$toFile"; unlink glob("$toFile_\*");
		
        ## --------------------------------------------------------------------

        my $INPUT = FileHandle->new();

        $INPUT->open("<$fmFile_")
          or
	 	die "ERROR AT INPUT OPEN for file \"$fmFile_\" $!"; binmode($INPUT);
 
		my $OUTPUT = gzopen("$toFile_\.PENDING", "wb")
		  or
	 	die "ERROR AT OUTPUT GZOPEN for file \"$toFile\" $!"; 
		
		my ($buff, $br, $tbr, $bw, $tbw);
		while(1) {
		  $br = $INPUT->read($buff, 32768); last if !$br; $tbr += $br;
		  $bw = $OUTPUT->gzwrite($buff); $tbw += $bw;
		}
		my $atOff = $OUTPUT->gztell(); $atOff = 0x100000000 + $atOff if $atOff < 0;
		if ( $tbw != $tbr or $atOff != $tbw ) {
		  &$logrRtn("BYTES WRITTEN $tbw/$atOff NOT EQUAL TO BYTES READ $tbr");
		}
		$OUTPUT->gzclose(); $INPUT->close(); 

		$rc = rename("$toFile_\.PENDING", "$toFile_\.gz");
	    if ( !$rc ) {
	      &$logrRtn("RENAME Error ($toFile_\.PENDING", "$toFile_\.gz) $!");
		  die ("FileError: RENAME Error $!");
	    }
	    $toFile .= ".gz";
		
        &$logrRtn(
          "TRANSFER ENDED NORMALLY FROM $fmFile_",
          " INTO \"$toFile_\.gz\""
        ); 
        
        my $req = QUpdate XReport::QUtil(
                  XferEndTime    => 'GETDATE()',
                  Status         => ST_RECEIVED,
                  JobName        => 'XYZ00001',
                  JobNumber      => 'JOB12345',
                  JobReportName  => $JobReportName,
                  XferRecipient  => '',
                  RemoteFileName => $fmFile_,
                  LocalFileName  => $toFile,
                  XferPages      => 0,
                  XferInBytes    => $tbr,
                  XferMode       => XF_PSF,
                  Id             => $JobReportId,
                  );
        
		if ( $rename ) {
		  my $toFile_ = &$rename( $fmFile_ ); 
		  if ( !rename("'$fmFile_'", "'$toFile_'") ) {
            &$logrRtn("RENAME ERROR for rc=$rc $fmFile_ $toFile_. NOW ENDING !!");
            die("FtpError: RENAME Error");
		  }
		}
		else {
          if ( !unlink($fmFile_) ) {
            &$logrRtn("DELETE ERROR for $fmFile. NOW ENDING !!");
            die("Error: DELETE Error $!");
          }
          else {
            &$logrRtn("FILE $fmFile DELETED SUCCESSFULLY.");
          }
		}

        $dbr = dbExecute("
          DELETE from tbl_JobReportsPendingXfers
          WHERE HostName = '$host' AND RemoteFileName = '$fmFile_'
        ");
      }
    }

    &$logrRtn("SLEEPING NOW $sleepLS seconds"); sleep $sleepLS;
  }

  return 1;
}

eval {
  while(1) {
    my $rc = DirLoop( $dirs );

    &$logrRtn("DirLoop END $rc");
  
    sleep ( ($rc < 0) ? 120 : 60 );
  }
};
&$logrRtn($@);

#######################################################################
#
#######################################################################

sub INITIALIZE {
  InitServer(); $SrvName = getConfValues('SrvName');
  
  $dirs = getConfValues('dirs'); 

  for my $dir (@{$dirs->{dir}}) {
    my $if = $dir->{'if'}; my $code = "sub {\n  my \$fl = shift;\n";
	$if = [$if] if ref($if) ne "ARRAY";
    for (@$if) {
	  $code .= "  return '$_->{'jr'}' if \$fl =~ /$_->{'re'}/o;\n";
	}
	$code .= "  return undef;\n}\n";
	$dir->{'checkFile'} = eval($code);
	die "INVALID checkFile EXPRESSION $code / $@" if $@;
	if ( $dir->{rename} ) {
	  $code = "
	    sub { 
		  my \$t = \$_[0]; \$t =~ s$dir->{rename}; 
		  return \$t; 
		}
	  ";
	  $dir->{rename} = eval "$code";
	  die "INVALID rename EXPRESSION $code / $@" if $@;
	}
  }
  print Dumper($dirs);
}
