#!/usr/bin/perl -w
#
# sincronizzazione con utenze RSS.
# legge un report formattato a partire dalla riga 2 nel seguente modo
# <cod utente> <Descr utente> <profilo RSS>
# tali definizioni vengono confrontate con le seguenti tabelle:
# tbl_Users
# tbl_UserProfiles
# tbl_UsersAliases
# vegono applicate solo le differenze
# le tabelle sono salvate per 9 generazioni
# chiamata:
#  perl xrSyncRSSUsers.pl <work directory> <JobReportId> <forceupdate(0|1)>
# 
# lo script non effettua pi� di 200 update a meno che il parametro forceupdate non sia valorizzato a 1
#

use strict;

use lib "$ENV{XREPORT_HOME}/perllib";

use XReport;
use XReport::DBUtil;
package XReport::DBUtil;

sub dbEx {
  my $self = shift; $self->CONNECT() if !$self->CONNECTED(); 
  my $rs;
  my $sql = join ' ', @_; 
  my $dbc = $self->{'dbc'}; 
  eval {$rs =  $dbc->Execute( "$sql" ); };
  print "Error detected\n" if $@;
  croak "$@\n" if $@;
  print "Error count detected " .  $dbc->Errors()->Count(). "\n".$dbc->Errors(0)->Description()."\n" if  $dbc->Errors()->Count() > 0;
  croak $dbc->Errors(0)->Description()."\n" if $dbc->Errors()->Count() > 0;
  return $rs;
}

1;

package main;

use XReport::JobREPORT;
use XReport::INPUT;
 require XReport::ARCHIVE;

#use IO::Zlib;
#use Convert::EBCDIC;

my $workdir = $ARGV[0];
my $jrid = $ARGV[1];
my $forceupdates = $ARGV[2] || 0;

die "unable to access working directory $workdir\n" unless -d $workdir;

my $jr = Open XReport::JobREPORT($jrid);
$jr->setValues(
    ijrar => $jr->get_INPUT_ARCHIVE(), ojrar => undef
);

my $INPUT = new XReport::INPUT($jr);
my ($XferStartTime, $JobReportName, $JobReportId, $WorkClass) = $jr->getValues(qw(XferStartTime JobReportName JobReportId WorkClass));

# controllo per richieste di updates precedenti non processate
my $updmsg = '';
my $msg2send = '';
my $xrdb = new XReport::DBUtil();

print "Now processing $JobReportName ($JobReportId) received at $XferStartTime\n";

my $RSScfg = c::getValues('RSSIface');
die "Unable to read RSS interface config" unless $RSScfg && ref($RSScfg)
  && exists($RSScfg->{MatchProfileMask};

my $crs = $xrdb->dbEx("SELECT MAX(JobReportId) AS oldjrid, 
          MAX(XferStartTime) AS oldxfertime, 
          COUNT(*) as unprocessed FROM tbl_JobReports 
          WHERE     (JobReportName = '$JobReportName') 
                AND (XferStartTime < '$XferStartTime') 
                AND (NOT (Status = 18)) 
                AND (NOT (JobReportId = $JobReportId))"
		     ); 

if (!$crs || $crs->eof()) {
  $updmsg = "holded - Unable to check for unprocessed updates";
  $msg2send = "Process Aborted";
} else {
  my $unprocessed = $crs->Fields->Item('unprocessed')->Value() ;
  if ($unprocessed eq undef) {
    $updmsg = "holded - Undefined unprocessed updates";
    $msg2send = "Process Aborted";
  } elsif ($unprocessed > 0) {
    my $oldjrid = $crs->Fields->Item('oldjrid')->Value() ;
    my $oldxfertime = $crs->Fields->Item('oldxfertime')->Value() ;
    $updmsg = "holded - $unprocessed previous updates still to process\n";
    $msg2send = "Process Aborted for $jrid - $oldjrid received at $oldxfertime has not been processed successfully\n";
  }
}
if ($updmsg) {
  $jr->SendElabEndNotifications("Users Profiles processing for $jrid $updmsg", $msg2send);
  exit 12;
}
#exit;
#my ($SrvName, $workdir) = getConfValues('SrvName', 'workdir');
#die "unable to obtain working directory\n" unless $workdir;


#$workdir .= "/$jobreportId" if -d $workdir/;
#my $workdir = '.';
my $reportfn = "$workdir/actions_reports.txt";

my $report = new FileHandle(">$reportfn");
my $rptsep = "-" x 80;
my @reportrow = ();
format activityrep_hdr =
@<<<<<<<<<<<<<<<<<<<<<<<< @|||||||||||||||||||||||||||||||||||         PAGE @0###
"".localtime()."",        "CeReport Profiles Activity Report", $%
User      Descr                  Profile        Action     NewDescr
---------------------------------------------------------------------------------
.
format activityrep =
@<<<<<<<< @<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<< @<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<
@reportrow
.
format_name $report "activityrep";
format_top_name $report "activityrep_hdr";

my $inputMask = $RSScfg->{InputMask} || "@2A8 @9A21 @30A42";
my @varlist = split /\s+/, ($RSScfg->{InputVars} || "UserName UserDescr Profile");
my $matchMask = qr/$RSScfg->{MatchProfileMask}/;
my $profiles = {};
my $usertab_old = {};
my $usertab_new = {};
my $usertab_ins = {};
my $usertab_upd = {};
my $usertab_input = {};
my $usertab_add = {};
my $usertab_del = {};
my $users = {};


$INPUT->Open();
my $ct;
while (my $line = $INPUT->getAsciiLine() ) {
  my $linfos = {};
  @{$linfos}{@varlist} = unpack($inputMask, $line);
  (my $UserName, my $UserDescr, my $ProfileName) = @{$linfos}{qw(UserName UserDescr Profile)};
#  print join('::', ($UserName, $UserDescr, $ProfileName)), "-\n"; # if $UserName =~ /M006973/;
#  $usertab_input->{$UserName}->{profiles}->{'CER01'} = 1 unless exists($usertab_input->{$UserName});
  $usertab_input->{$UserName}->{descr} = ($UserDescr ? $UserDescr : 'Not Specified');
  $usertab_input->{$UserName}->{profiles}->{$ProfileName} = 1;
#  last if $ct++ > 1000;
}
$INPUT->Close();

my $prs = $xrdb->dbEx("SELECT DISTINCT ProfileName FROM tbl_ProfilesFoldersTrees");
while (!$prs->eof()) {
 my $prof = $prs->Fields->Item('ProfileName')->Value() ;
 $profiles->{$prof} = 1;
 $prs->MoveNext();
  
}

my $keepMask = $RSScfg->{KeepProfileMask} || '\s*';
$keepMask = qr/$keepMask/;

my $urs = $xrdb->dbEx("SELECT a.UserName, b.UserDescr, a.ProfileName FROM tbl_UsersProfiles a
  LEFT OUTER JOIN tbl_Users b ON a.UserName = b.UserName");
#  WHERE a.ProfileName like 'WWW%' or ProfileName like 'CTD%'");
while (!$urs->eof()) {
 my $uname = $urs->Fields->Item('UserName')->Value() ;
 my $udesc = $urs->Fields->Item('UserDescr')->Value() || 'Not Specified';
 my $uprof = $urs->Fields->Item('ProfileName')->Value() ;
# print join('::', ($uname, $udesc, $uprof)), "\n" if $uname =~ /M006973/;
 $usertab_old->{$uname}->{descr} = $udesc;
 $usertab_old->{$uname}->{profiles}->{$uprof} = 1;
 if ($uprof =~ /$keepMask/) {
   $usertab_new->{$uname}->{descr} = $udesc;
   $usertab_new->{$uname}->{profiles}->{$uprof} = 1;
 }
# push @{$usertab_old->{$uname}->{profiles}}, $uprof unless grep(/$uprof/, @{$usertab_old->{$uname}->{profiles}});
 $users->{dbase}->{$uname} = ($udesc ? $udesc : 'Not Specified') ;
 $profiles->{$uprof} = 1;
 $urs->MoveNext();
  
}

print "+" x 60, "\n";
print "Dbase: ", scalar(keys %{$users->{dbase}}), " Input: ", scalar(keys %{$usertab_input}), "\n"; 
foreach my $uname (keys %{$users->{dbase}}) {
  print "Dbase ", $uname, "(", $users->{dbase}->{$uname}, ") not in input\n" unless exists($usertab_input->{$uname});
}

foreach my $UserName ( keys %$usertab_input ) {
  next unless grep /^$matchMask$/i, keys %{$usertab_input->{$UserName}->{profiles}};
  my $UserDescr = $usertab_input->{$UserName}->{descr};
#  print "Input ", $UserName, "(", $UserDescr, ") not in dbase\n" unless exists($users->{dbase}->{$UserName});
  foreach my $ProfileName (keys %{$usertab_input->{$UserName}->{profiles}}) {
    next if $ProfileName =~ /^(?:GR\wCER01|GR\wGT|GR\wUT)$/i;
    if (exists($profiles->{$ProfileName})) {
      $usertab_new->{$UserName}->{descr} = $UserDescr;
      $usertab_new->{$UserName}->{profiles}->{$ProfileName} = 1;
      if (exists($usertab_old->{$UserName})) {
	if ($UserDescr ne $usertab_old->{$UserName}->{descr}) {
	  print "$UserName Description changed From: ", $usertab_old->{$UserName}->{descr}, "To: ", $UserDescr, "\n";
	  $usertab_upd->{$UserName}->{descr} = $UserDescr ;
	  $usertab_upd->{$UserName}->{olddescr} = $usertab_old->{$UserName}->{descr};
	}
	if (!exists($usertab_old->{$UserName}->{profiles}->{$ProfileName})) {
#	  print "$ProfileName will be added to $UserName\n"  ;
	  $usertab_add->{$UserName}->{profiles}->{$ProfileName} = 1 ;
	  $usertab_add->{$UserName}->{descr} = $UserDescr;
	}
      } else {
#	print "$ProfileName will be added to $UserName\n";
	$usertab_add->{$UserName}->{descr} = $UserDescr ;
	$usertab_add->{$UserName}->{newuser} = 1 ;
	$usertab_add->{$UserName}->{profiles}->{$ProfileName} = 1 
      }
    }
  }
}

foreach my $uname ( keys %$usertab_old ) {
  if (exists($usertab_new->{$uname})) {
    foreach my $uprof ( keys %{$usertab_old->{$uname}->{profiles}}) {
      next if exists($usertab_new->{$uname}->{profiles}->{$uprof});
      print "$uprof will be deleted from $uname\n";  
      $usertab_upd->{$uname}->{profiles}->{$uprof} = 1;
      $usertab_upd->{$uname}->{olddescr} = $usertab_old->{$uname}->{descr};
    }
  } else {
#    print "$uname will be deleted\n";
#    $usertab_del->{$uname} = {};
    $usertab_del->{$uname}->{olddescr} = $usertab_old->{$uname}->{descr};
  }
}

sub mydbEx {
  print shift, "\n";
  return 1;
}

sub rotateTable {
  my $table = shift;
  eval { $xrdb->dbEx("DROP TABLE ${table}_V1"); }; print "RETRN: $@ --\n";return undef unless $@ =~ /(?:caution:|does not exist)/i or !$@;
  eval {$xrdb->dbEx("EXEC sp_rename '${table}_V2', '${table}_V1'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbEx("EXEC sp_rename '${table}_V3', '${table}_V2'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbEx("EXEC sp_rename '${table}_V4', '${table}_V3'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbEx("EXEC sp_rename '${table}_V5', '${table}_V4'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbEx("EXEC sp_rename '${table}_V6', '${table}_V5'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbEx("EXEC sp_rename '${table}_V7', '${table}_V6'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbEx("EXEC sp_rename '${table}_V8', '${table}_V7'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbEx("EXEC sp_rename '${table}_V9', '${table}_V8'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbEx("EXEC sp_rename '${table}_LAST', '${table}_V9'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /caution:/i or !$@;
  return 1;
}

sub execUpdate {
  my ($usertab_del, $usertab_add, $usertab_upd, $usertab_old, $report, $row) = (shift, shift, shift, shift, shift, shift);
  $xrdb->dbEx("SELECT * INTO dbo.tbl_Users_LAST FROM dbo.tbl_Users") || return "Users save copy Failed";;
  $xrdb->dbEx("SELECT * INTO dbo.tbl_UserAliases_LAST FROM dbo.tbl_UserAliases") || return "UserAliases save copy Failed";;
  $xrdb->dbEx("SELECT * INTO dbo.tbl_UsersProfiles_LAST FROM dbo.tbl_UsersProfiles") || return "UsersProfilessave copy  Failed";;

  print "processing delete\n";
  foreach my $uname (keys %$usertab_del) {
    $xrdb->dbEx("DELETE FROM tbl_Users WHERE UserName = '$uname'") || return "Users Delete of $uname Failed";
    $xrdb->dbEx("DELETE FROM tbl_UserAliases WHERE UserAlias = '$uname'") || return "Aliases Delete of $uname Failed";
    $xrdb->dbEx("DELETE FROM tbl_UsersProfiles WHERE UserName = '$uname'") || return "UserProfiles Delete for $uname Failed";
    @$row = ($uname, $usertab_del->{$uname}->{olddescr}, '__ALL__', 'DELETED', '');
    write $report;
  }
  
  print "processing adds\n";
  foreach my $uname (keys %$usertab_add) {
    my $udesc = $usertab_add->{$uname}->{descr};
    if (exists($usertab_add->{$uname}->{newuser})) {
      $xrdb->dbEx("INSERT INTO tbl_Users (UserName, UserDescr) values('$uname', '$udesc')") || return "Users Insert $uname Failed";
      $xrdb->dbEx("INSERT INTO tbl_UserAliases (UserAlias, UserName, UserAliasDescr) values('$uname', '$uname', '$udesc')") || return "UserAliases Insert $uname Failed";
      @$row = ($uname, $usertab_add->{$uname}->{descr}, '__NEW USER__', 'ADDED', '');
      write $report;
    }
    foreach my $uprof ( keys %{$usertab_add->{$uname}->{profiles}}) {
      $xrdb->dbEx("INSERT INTO tbl_UsersProfiles (UserName, ProfileName) values('$uname', '$uprof')") || return "UsersProfiles insert $uname/$uprof Failed";
      @$row = ($uname, $usertab_add->{$uname}->{descr}, $uprof, 'ADDED', '');
      write $report;
    }
  }
  
  print "processing updates\n";
  foreach my $uname (keys %$usertab_upd) {
    if (exists($usertab_upd->{$uname}->{descr})) {
      print "Changing descr for $uname\n";
      $xrdb->dbEx("UPDATE tbl_Users set UserDescr='" . $usertab_upd->{$uname}->{descr} . "' WHERE UserName = '$uname'") || return "Users Update $uname Failed";
      $xrdb->dbEx("UPDATE tbl_UserAliases set UserAliasDescr='" . $usertab_upd->{$uname}->{descr} . "' WHERE UserAlias = '$uname'") || return "Aliases Update $uname Failed";
      @$row = ($uname, $usertab_upd->{$uname}->{olddescr}, '', 'CHANGED', $usertab_upd->{$uname}->{descr});
      write $report;
    }
    foreach my $uprof (keys %{$usertab_upd->{$uname}->{profiles}}) {
      print "deleting $uprof for $uname\n";
      $xrdb->dbEx("DELETE FROM tbl_UsersProfiles WHERE UserName = '$uname' and ProfileName = '$uprof'") || return "delete $uname/$uprof Failed";
      delete $usertab_old->{$uname}->{profiles}->{$uprof};
      @$row = ($uname, $usertab_upd->{$uname}->{olddescr}, $uprof, 'DELETED', '');
      write $report;
      if (!scalar(keys %{$usertab_old->{$uname}->{profiles}}) and !exists($usertab_add->{$uname}->{profiles})) {
	print "deleting empty $uname\n";
	$xrdb->dbEx("DELETE FROM tbl_Users WHERE UserName = '$uname'") || return "delete $uname Failed";
	$xrdb->dbEx("DELETE FROM tbl_UserAliases WHERE UserAlias = '$uname'") || return "Aliases Delete of $uname Failed";
	@$row = ($uname, $usertab_upd->{$uname}->{olddescr}, '__ALL__', 'DELETED', '');
	write $report;
      }
    }
  }
  return 'OK';
}

sub simulateUpdate {
  my ($usertab_del, $usertab_add, $usertab_upd, $usertab_old, $report, $row) = (shift, shift, shift, shift, shift, shift);

  print "simulating delete\n";
  foreach my $uname (keys %$usertab_del) {
    @$row = ($uname, $usertab_del->{$uname}->{olddescr}, '__ALL__', 'DELETED', '');
    write $report;
  }
  
  print "simulating adds\n";
  foreach my $uname (keys %$usertab_add) {
    my $udesc = $usertab_add->{$uname}->{descr};
    if (exists($usertab_add->{$uname}->{newuser})) {
      @$row = ($uname, $usertab_add->{$uname}->{descr}, '__NEW USER__', 'ADDED', '');
      write $report;
    }
    foreach my $uprof ( keys %{$usertab_add->{$uname}->{profiles}}) {
      @$row = ($uname, $usertab_add->{$uname}->{descr}, $uprof, 'ADDED', '');
      write $report;
    }
  }
  
  print "simulating updates\n";
  foreach my $uname (keys %$usertab_upd) {
    if (exists($usertab_upd->{$uname}->{descr})) {
      print "Changing descr for $uname\n";
      @$row = ($uname, $usertab_upd->{$uname}->{olddescr}, '', 'CHANGED', $usertab_upd->{$uname}->{descr});
      write $report;
    }
    foreach my $uprof (keys %{$usertab_upd->{$uname}->{profiles}}) {
      print "deleting $uprof for $uname\n";
      delete $usertab_old->{$uname}->{profiles}->{$uprof};
      @$row = ($uname, $usertab_upd->{$uname}->{olddescr}, $uprof, 'DELETED', '');
      write $report;
      if (!scalar(keys %{$usertab_old->{$uname}->{profiles}}) and !exists($usertab_add->{$uname}->{profiles})) {
	print "deleting empty $uname\n";
	@$row = ($uname, $usertab_upd->{$uname}->{olddescr}, '__ALL__', 'DELETED', '');
	write $report;
      }
    }
  }
  return 'OK';
}

my ($modifes, $deletes, $inserts, $updates) = (0,0,0,0);
if ($modifes = scalar(keys %$usertab_upd)) {
  print "following $modifes users will be updated:\n",
    ""; #join("\n", keys %$usertab_upd), "\n";
  
}
if ($deletes = scalar(keys %$usertab_del)) {
  print "following $deletes users will be deleted:\n",
 ""; #    join("\n", keys %$usertab_del), "\n";
  
}
if ($inserts = scalar(keys %$usertab_add)) {
  print "following $inserts users will be added:\n",
    ""; #join("\n", keys %$usertab_add), "\n";
  
}
my $maxUpdates = $RSScfg->{MaxUpdatesPerRun} || 200;
$updates = $modifes + $deletes + $inserts;

if ($updates == 0) {
  $updmsg = 'detected No Changes';
} elsif ($updates < $maxupdates or $WorkClass == 128 or $forceupdates) {
  print "Starting to process $updates updates\n";
  eval { $xrdb->dbEx("BEGIN TRANSACTION USERUPDATE"); }; print "RETRN: $@ --\n"; 
  if ( $@ =~ /caution/i or !$@ ) {
    if (($updmsg = execUpdate($usertab_del, $usertab_add, $usertab_upd, $usertab_old, $report, \@reportrow)) eq 'OK') {
      eval { $xrdb->dbEx("COMMIT TRANSACTION USERUPDATE"); }; 
      print "RETRN: $@ --\n"; 
      return undef unless $@ =~ /caution/i or !$@;
      rotateTable 'tbl_Users';
      rotateTable 'tbl_UserAliases';
      rotateTable 'tbl_UsersProfiles';
      if ( $xrdb->dbEx("UPDATE tbl_JobReports SET Status = 18 WHERE JobReportId = $jrid") ) {
	print $report $rptsep, "\nUpdate Completed at ".localtime()."\n", $rptsep, "\n";
      }  else {
	print "JobReportId $jrid Status update failed";
      } 
    } else {
      eval { $xrdb->dbEx("ROLLBACK TRANSACTION UserUpdate"); }; 
      print "RETRN: $@ --\n"; 
      return undef unless $@ =~ /caution/i or !$@;
      print $report $rptsep, "\n$updmsg - Rollback executed at ".localtime()."\n", $rptsep, "\n";
    }
  }
} else {
  simulateUpdate($usertab_del, $usertab_add, $usertab_upd, $usertab_old, $report, \@reportrow);
  $updmsg = "holded - too many changes ($updates) - $modifes Updates, $deletes Deletion, $inserts Inserts";
} 


#my $ar = XReport::ARCHIVE::cre_OUTPUT_ARCHIVE($jr);

#print "INIT CREATING OUTPUT Archive\n";

#my @fileList = glob("$workdir/*\.$JobReportId\.*");
#for ( @fileList ) {
#  print "Adding $_ to output archive\n";
#  #todo: test not to compress big or pdf files and from config
#  if ($_ !~ /.*ps$/i and $_ !~ /^$JobReportName\.$JobReportId\.0\.#[^1-9]\d*\.log$/i) {
#    $ar->AddFile($_, basename($_)) 
#  }
#}
#$ar->Close();


close $report;
$report = new FileHandle("<$reportfn");
binmode $report;

my $msgbytes = read($report, $msg2send, -s $reportfn); 
close $report;
$msg2send .= "\n". 
      $rptsep."\nProcessing for $jrid Completed at ".localtime()."\n", $rptsep, "\n";
$jr->SendElabEndNotifications("Users Profiles processing for $jrid $updmsg", $msg2send);

exit;

__END__

sub prepare_bulk_insert{
  my ($file, $string, $sep, @varvalue) = (shift, shift, @_);
#  print $file $string, join($sep.$string, @varvalue ), "\n";
  print $string, join($sep.$string, @varvalue ), "\n";
}

print "+" x 80, "\n";
$outfile = new FileHandle(">$workdir/tbl_Users");
$outfile1 = new FileHandle(">$workdir/tbl_UsersProfiles");
print $outfile "UserName\tUserDescr\n";
print $outfile1 "UserName\tProfileName\n";

foreach my $UserName (keys %$usertab) {
  if (exists($usertab->{$UserName}->{profiles}) and scalar(keys %{$usertab->{$UserName}->{profiles}})) {
#    print $outfile join("\t", ($UserName, $usertab->{$UserName}->{descr})), "\n"; 
    print join("\t", ($UserName, ($usertab->{$UserName}->{descr} || '..'))), "\n"; 
    prepare_bulk_insert($outfile1, "$UserName\t",
		      "\n",
		      (keys %{$usertab->{$UserName}->{profiles}})
		     );
  }
}

close $outfile;
close $outfile1;


exit 0;
