use lib "$ENV{XREPORT_HOME}/perllib";
use strict "vars";

use Net::FTP;
use Net::Telnet;
use IO::String;
use IO::Compress::Gzip;

use XReport::Util;

my $catia_addr = "catiamf.agusta.it";
my $catia_path = "/cat_user/plot/mvs2aix/xreport/interactive";
my $catia_user = 'plot';
my $catia_pass = 'plot';

sub sendCatiaRequest {
	my ($request, $JRID, $gzpath) = (shift, shift, shift);
	my $JOBID = "XREPORT.REQUEST.$JRID";
	my $remotefilename = "PLOTARCH.XRJ$JRID";

	my $input = new IO::String($request);
	my $ftp = Net::FTP->new($catia_addr, Debug => 3) || die "unable to contact $catia_addr: $@";

	$ftp->login("plot", "plot") || die "Cannot login: ", $ftp->message();
	$ftp->cwd($catia_path) || die "Cannot switch to dir \"$catia_path\" in $catia_addr: ", $ftp->message();
	$ftp->binary();
	$ftp->put($input, "$JOBID\.txt");

	my $i_dbg_fh = new IO::String(my $i_debug);
	my $o_dbg_fh = new IO::String(my $o_debug);
	my $catia = new Net::Telnet (Timeout => 30, Prompt => '/[\%\#\>] ?$/', Input_log => $i_dbg_fh, Output_log => $o_dbg_fh);
	$catia->open($catia_addr);
	$catia->login($catia_user, $catia_pass);

	## Make sure prompt won't match anything in send data.
	my $prompt = "_$catia_addr\_4_xreport__";
	$catia->prompt("/$prompt\$/");
	eval {
		my ($resp) = $catia->cmd("export PS1='$prompt'");
		&$logrRtn("$catia_addr set Response: $resp");
		my ($line) = $catia->cmd("/cat_user/plot/mvs2aix/xreport/bin/interactive_job $JOBID.txt");
		&$logrRtn("$catia_addr response: $line");
		($line) = $catia->cmd("ls $catia_path");
		&$logrRtn("$catia_addr ls on dir: $line");
	};

	&$logrRtn("error: $@") if $@;
	&$logrRtn("Input trace: $i_debug");
	&$logrRtn("Output trace: $o_debug");

	&$logrRtn("Get $remotefilename from FTP");

	my $gzout = new IO::Compress::Gzip($gzpath."/".$remotefilename.".gz");
	$ftp->get($remotefilename, $gzout) or die("Get file $remotefilename failed: ".$ftp->message."\n");

	$ftp->close();
	$gzout->close();

	return $gzpath."/".$remotefilename.".gz";
}
