use lib "$ENV{XREPORT_HOME}/perllib";
use strict "vars";

use POSIX qw(strftime);
use FileHandle;
use File::Basename;

use File::Path;
use File::Copy 'cp';
#use Compress::Zlib;
use XML::Simple;

use Data::Dumper;

use XReport;
use XReport::Util;
use XReport::Logger;
use XReport::DBUtil;
use XReport::JobREPORT;
use XReport::PDF::DOC; 
use XReport::ARCHIVE;
#use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

(my $workdir = ($ARGV[0] eq '.' ? `cd` : $ARGV[0])) =~ s/\n$//gs;

my $JRID = $ARGV[1] || die "Unable to get JobReportId\n";
my $ArchiveContentFN = '_ArchiveContent.tsv';

if (-d $workdir ) {
#  $workdir =~ s/\//\\/sig;
  my @flist = glob $workdir.'\*'; 
  &$logrRtn( "deleting now\n", join("\n", @flist));
  unlink @flist;
}

my $debgRtn = sub { warn map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n"; };
$logrRtn = sub { print map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n"; };

sub buildParser {
  my ($outvars, $invars) = (shift, shift);
  
  my $outxform = { ($outvars =~ /(\S+)(?:\s+(\w+)\.){0,1}/osgi)  };
  my $inxform = { ($invars =~ /(\S+)(?:\s+(\w+)\.){0,1}/osgi)  };
  &$debgRtn("outxform:\n", Dumper($outxform)); 
  &$debgRtn("inxform:\n", Dumper($inxform)); 
  require XReport::INFORMAT if scalar(keys %$inxform);
#  require XReport::OUTFORMAT if scalar(keys %$outxform);

  my $outlist = [grep !/\w+\./, split(/\s+/, $outvars) ];
  my $inlist = [grep !/\w+\./, split(/\s+/, $invars) ];
  
  my $code = "sub { my \$input = shift; return {\n";
  for my $j ( 0..$#{$outlist} ) {
    
    my ($outvarn, $invarn) = ($outlist->[$j], $inlist->[$j]);
    &$debgRtn("Checking $outvarn => $invarn");
    my $invalue = ($invarn =~ /^#(.*)?#$/ ? "'$1'" : 
		   $inxform->{$invarn} && XReport::INFORMAT->can($inxform->{$invarn}) ? 
		   'XReport::INFORMAT::'."$inxform->{$invarn}(\\\$input->{$invarn})" : 
		   "\$input->{$invarn}");
    $code .= $outvarn . ' => ' . ($invarn =~ /^#(.*)?#$/ ? $invalue . ",\n" : 
				  ($outxform->{$outvarn} && XReport::OUTFORMAT->can($outxform->{$outvarn}) ? 
				   'XReport::OUTFORMAT->'. "$outxform->{$outvarn}($invalue)" : $invalue
				  )
				  . " || '',\n"
				 ); 
  }
  $code .= "};\n}";
  &$debgRtn("CODE:\n", $code); 
  eval($code);
  if ($@) {
    &$logrRtn("CODE:\n", $code); 
    die "Error during building of parsing routine - $@";
  }
  return (eval($code), @$outlist);
}

sub getFH {
}

sub rotateTable {
  my $xrdb = shift; my $table = shift; my $newTable = shift;
  eval { $xrdb->dbExecute("DROP TABLE ${table}_V1"); }; print "RETRN: $@ --\n";return undef unless $@ =~ /(?:caution:|does not exist)/i or !$@;
  eval {$xrdb->dbExecute("EXEC sp_rename '${table}_V2', '${table}_V1'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbExecute("EXEC sp_rename '${table}_V3', '${table}_V2'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbExecute("EXEC sp_rename '${table}_V4', '${table}_V3'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbExecute("EXEC sp_rename '${table}_V5', '${table}_V4'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbExecute("EXEC sp_rename '${table}_V6', '${table}_V5'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbExecute("EXEC sp_rename '${table}_V7', '${table}_V6'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbExecute("EXEC sp_rename '${table}_V8', '${table}_V7'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbExecute("EXEC sp_rename '${table}_LAST', '${table}_V8'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /(?:caution:|no item by the name)/i or !$@;
  eval {$xrdb->dbExecute("EXEC sp_rename '${table}',    '${table}_LAST'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /caution:/i or !$@;
  eval {$xrdb->dbExecute("EXEC sp_rename '${newTable}', '${table}'"); };print "RETRN: $@ --\n"; return undef unless $@ =~ /caution:/i or !$@;
  return 1;
}

my $TotEntries = 0;
&$logrRtn( "Accessing now $JRID\n");
my ($jr, $FQfileName);
my ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName, $XferEndTime, $remoteFileName, $rmtjob);
my ($manfqn, $ixfname, $rptconf_in, $FQcfgfName);
my $cfgvars = {};
if ( $JRID =~ /^\d+$/) {
  $jr = XReport::JobREPORT->Open($JRID, 1);
  ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName, $XferEndTime, $remoteFileName, $rmtjob) = 
    $jr->getValues(qw(LocalFileName SrvName RemoteHostAddr XferMode TargetLocalPathId_IN XferDaemon JobReportName XferEndTime RemoteFileName JobName));
  my $JRAttrs;
  @{$JRAttrs}{qw(XferEndTime UserRef UserTimeRef UserTimeElab RemoteFileName)} = ($XferEndTime, '', $XferEndTime, $XferEndTime, $remoteFileName);
  &$debgRtn("$JRID Attributes:\n", $JRAttrs);  
  
  $jr->deleteElab();

  @{$cfgvars}{qw(LocalFileName SrvName RemoteHostAddr XferMode
		 TargetLocalPathId_IN XferDaemon JobReportName)} = ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName);
  
  $tgtIN = 'L1' unless $tgtIN;
  
  $rptconf_in = $jr->getFileName('PARSE');
  $FQcfgfName = $workdir.'/'. basename($jr->getFileName('LOGPARSE'));

  ($FQfileName = getConfValues('LocalPath')->{$tgtIN}."/IN/$fileName") =~ s/^file:\/\///;
  die "Cannot access $FQfileName\n" unless -f $FQfileName;
  (my $FQcntlName = $FQfileName) =~ s/DATA\.TXT\.gz$/CNTRL.TXT/osi;
  die "Cannot access $FQcntlName\n" unless -f $FQcntlName;
  ($manfqn = $workdir.'/'.basename($jr->getFileName('IXFILE', '$$MANIFEST$$'))) =~ s/\//\\/g;
}
else {
  $FQfileName = $ARGV[1];
  $remoteFileName = $FQfileName;
  $remoteFileName = $ARGV[2] if $ARGV[2] && $ARGV[2] ne '.';
  $manfqn = "$workdir\\".'$$IDX.$$MANIFEST$$.tsv';
  $rptconf_in = "$ENV{XREPORT_SITECONF}\\userlib\\rptconf\\$ARGV[3].xml";
  $FQcfgfName = $workdir.'/'.$ARGV[3].'.xml';
  $rmtjob = 'F1CD000X';
}

use IO::Zlib qw(:gzip_external 0);
use Data::Dumper;

my $manifest = new FileHandle(">$manfqn");

my $cfgfhin = new FileHandle("<".$rptconf_in) || die "unable to access rptconf $rptconf_in\n";
my $cfgfhou = new FileHandle(">".$FQcfgfName) || die "unable to access rptconf $FQcfgfName\n";
my $cfgtext = '';
while (<$cfgfhin>) {
  my $lout = $_;
  $lout =~ s/([^\$])\$(\w+)/$1$cfgvars->{$2}/sg;
  $lout =~ s/\$\$/\$/sg;
  print $cfgfhou $lout;
  $cfgtext .= $lout;
}
close $cfgfhin;
close $cfgfhou;
push @$main::files2arc, $FQcfgfName ;

$main::debug = 1 if $cfgtext =~ /\<jobreport .*? debug\=\"1\".*?[^\/]>/is;
&$debgRtn("Parsing ", $FQcfgfName, " contents:\n", $cfgtext) if $main::debug;

my $reportcfg = XMLin($cfgtext,
		      ForceArray => [ qw(index masks globals) ], ContentKey => '-content',
		      KeyAttr => {globals => 'name', index => 'name', masks => 'name', loadtar => 'name', op => 'name', ix => 'name', libraries => 'name'},
		     );

print Dumper($reportcfg), "\n" if $main::debug;

my $IndexName = (split(/\s+/, $reportcfg->{report}->{ix}))[0];

my @ixcols = split(/\s+/, $reportcfg->{index}->{$IndexName}->{columns});
my @ixkeys = split(/\s+/, $reportcfg->{index}->{$IndexName}->{keycols});
$ixfname = ($jr ? basename($jr->getFileName('IXFILE', $IndexName))
	    : "\$\$IDX.$IndexName.tsv"
	    );

my ($globals, $masks) = @{$reportcfg}{qw(globals masks)};
while ( my ($k, $v) = each %$globals ) {
  next unless $v->{type} eq 'CODE';
  my $subref = eval "sub { ".$v->{content}." }";
  $globals->{$k} = $subref unless $@;
  print "EVAL Error: $@\n" if $@;
  
}

print Dumper(\{globals => $globals, masks => $masks}), "\n" if $main::debug;

&$logrRtn("Opening ", $FQfileName);
my $gzh = new IO::Zlib($FQfileName, 'rb');

my $ixfh = new FileHandle(">$workdir/$ixfname");
print $ixfh join("\t", @ixcols), "\n";

my ($dfltmask, $colsstr) = @{$masks->{_dflt_}}{qw(mask columns)};
my @dfltcols = ( split /\s+/, $colsstr );

@{$globals}{qw(_RFILENAME masks)} = ($remoteFileName, $masks );

my $pnums = {};

while (my $inline = $gzh->getline()) {
  $inline =~ s/[\x0d\x0a]+$//sg;
  my $row = {_INLINE => $inline };
  my @colvals = unpack($dfltmask, $inline);
  my ($maskkey, $mask, @columns) = &{$globals->{'_setmask_'}}(\@dfltcols, \@colvals, $globals);
  @{$row}{@columns} = ( $mask ? unpack($mask, $inline) : @colvals );
  my $colid = 0;
  my $ixrow = {};
  @{$ixrow}{@ixcols} = map { my $cnam = $ixcols[$colid]; $colid++; #print "SEEKING .$cnam -> ", ref($globals->{'.'.$cnam}), "\n" if $cnam =~ /model/; 
			     (exists($globals->{".$maskkey.$cnam"}) && (ref($globals->{".$maskkey.$cnam"}) eq 'CODE') ? &{$globals->{".$maskkey.$cnam"}}($_, $row, $globals)
			      : exists($globals->{'.'.$cnam}) && (ref($globals->{'.'.$cnam}) eq 'CODE') ? &{$globals->{'.'.$cnam}}($_, $row, $globals)
			      : !defined($_) && exists($globals->{$cnam}) ? $globals->{$cnam}
			      : $_ );
			   } @{$row}{@ixcols};

  if (!$main::lib || $main::lib ne $row->{Library}) {
    $main::lib = $row->{Library};
    &$logrRtn(sprintf("$rmtjob - RF: %22s, MID: %10s MSK: %35s", $remoteFileName, $maskkey, $mask)." INLINE: $inline"); # if $TotEntries == 2;
    &$logrRtn("IXROW: ", Dumper($ixrow));# if $TotEntries == 2;
    
  }
  my $key = join('::', map { s/\s+//g; $_ } @{$ixrow}{@ixkeys});
  &$logrRtn("Duplicate entry: $key MID: $maskkey MASK: $mask\nTHIS LINE: $inline\nPREV LINE: $pnums->{$key}") if exists($pnums->{$key});
  next if exists($pnums->{$key});
  $pnums->{$key} = $inline;
  print $ixfh join("\t", @{$ixrow}{@ixcols}), "\n";
  $TotEntries++;
}

$gzh->close();

$ixfh->close();
push @$main::files2arc, "$workdir\\$ixfname"; 
print $manifest join("\t", 'DRAWINGS', "$ixfname",1), "\n";
  
$manifest->close();

push @$main::files2arc, $manfqn ;

my $contentfn = "$workdir/\$\$ARC.\$\$MANIFEST\$\$.TXT";

my $contentfh = new FileHandle(">$contentfn") || die "Unable to open list to archive file - $!";
binmode $contentfh;
print $contentfh join("\n", @$main::files2arc), "\n";
$contentfh->close;

my $dbc = XReport::DBUtil->get_ix_dbc($IndexName);
my $newTableName = "tbl_IDX_$IndexName\_NEW";
my $TableName = "tbl_IDX_$IndexName";
$dbc->dbExecute("IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[$newTableName]') AND type in (N'U'))"
		. " DROP TABLE $newTableName");
$dbc->dbExecute( "select top 0 * INTO $newTableName from $TableName");

sub addrows2tbl { 
  my ($dbc, $tbl, $columns, $rows) = (shift, shift, shift, shift);
  my $cmd = "INSERT INTO $tbl ([". join('], [', @$columns) ."]) " ."\n" . join("\n UNION ALL ", @$rows) . "\n";
  &$logrRtn("Starting to insert " . scalar(@$rows) . " rows into $tbl");
  $dbc->dbExecute($cmd);
}

my $oixfh = new FileHandle("<$workdir/$ixfname") or die "Unable to open Index File \"$ixfname\" to load into $TableName";
  
my (@tblcols, @rows) = ((), ());
print "STARTING INSERTS", "\n";
while ( <$oixfh> ) {
  chomp;
  $_ =~ s/'/''/g;
  @tblcols = split(/\t/, $_), next if (!scalar(@tblcols));
  push @rows, "SELECT '"  . join("', '",  split /\t/, $_) . "'";
  addrows2tbl($dbc, "$newTableName", \@tblcols, \@rows), @rows = () if scalar(@rows) > 199; 
}
addrows2tbl($dbc, $newTableName, \@tblcols, \@rows) if scalar(@rows);
  
$oixfh->close();

rotateTable $dbc, $TableName, $newTableName;

my $exit_code = 0;
