#!/usr/bin/perl -w

package main;

use lib($ENV{'XREPORT_HOME'}."/perllib");

use strict;
use POSIX qw(strftime);

use XReport;
use XReport::Util;
use XReport::DBUtil;
use XReport::ENUMS qw(:ST_ENUMS);
use constant ST_RETRY => 19;

use XReport::JobREPORT;
use Data::Dumper;

use Symbol;
use File::Path;
use File::Basename;
use Win32::OLE qw( in );
use Win32::OLE::Variant;
use Win32::Job;
use Win32::EventLog;

use constant DEBUG => 0;

use constant EC_OK => 0;
use constant EC_USER_SHUTDOWN => 1;
use constant EC_UNKNOWN_ERROR => 2;
use constant EC_PERL_SYNTAX_ERROR => 9;
use constant EC_PERL_DIE3 => 10045;
use constant EC_PERL_DIE1 => 10061;
use constant EC_PERL_DIE2 => 255;
use constant TOW_INPUT_TO_MIGRATE => 666;
use constant TOW_OUTPUT_TO_MIGRATE => 888;


my $myName = ( split( /[\/\\]/, $0 ) )[-1];
$myName = ( split( /\./, $myName ) )[0];

my $cfg_jobs = undef;
$main::EventLog = Win32::EventLog->new("Application", '.') or die "Can't open Application EventLog\n";
sub doEventLog {
  my @es = ($main::SrvName, shift, shift, shift, shift);
  my $cfg = pop @es;
  my $evID = $cfg->{'EventID'} || "10001";
  $main::EventLog->Report(my $ei = {
				    Source => "CeReport",
				    Computer => '.',
				    EventType => EVENTLOG_ERROR_TYPE,
				    Category => 0,
				    EventID => $evID,
				    Strings => join("\0", @es),
				    Data => "Failure of $es[1] detected by Service $es[0] - WorkQueue key $es[2] - ExternalKey key $es[3] -",
#				    Data => "Failure of $es[1] detected by Service $es[0] - WorkQueue key $es[2] - ExternalKey key $es[3] -cfg ref: ". ref($cfg) ."\n" 
#				    .Dumper($cfg),
				   });
}

sub dequeue {
	my ($INTERVAL, $selectQUERY) = (shift, shift);
	my $dbr;
  
	GET_WORKITEM_LOOP: while( 1 ) {	
		eval {
			GetLock('LK_INPUTQUEUE');
	
			last GET_WORKITEM_LOOP if(!($dbr = dbExecute($selectQUERY))->eof());
	
			ReleaseLock('LK_INPUTQUEUE'); 
		};
		die "$myName: $@" if($@ and $@ !~ /User *Error *:/i);
	
		sleep $INTERVAL;
	}
  
	my ($WorkId, $ExternalTableName, $ExternalKey, $TypeOfWork) = $dbr->GetFieldsValues(qw(WorkId ExternalTableName ExternalKey TypeOfWork));
	$dbr->Close();

	dbExecute("UPDATE tbl_WorkQueue Set SrvName = '$main::SrvName', Status = ". ST_INPROCESS 
	  ." WHERE ExternalTableName = '$ExternalTableName' AND ExternalKey = $ExternalKey AND TypeOfWork = $TypeOfWork AND WorkId = $WorkId");
	ReleaseLock('LK_INPUTQUEUE'); 

	return ($WorkId, $ExternalTableName, $ExternalKey, $TypeOfWork);
}
#
#sub cre_OUTPUT_ARCHIVE {
#	my ($jr, $workdir, $script, $status) = (shift, shift, shift, shift);
#	require XReport::ARCHIVE;
#	my $setstring = [];
#	push @$setstring, "SrvName = '$main::SrvName'" if $main::SrvName;
#
#	my ($JobReportName, $JobReportId) = $jr->getValues(qw(JobReportName JobReportId));
#
#	my $ar = XReport::ARCHIVE::cre_OUTPUT_ARCHIVE($jr, @_);
#    my $pathid = $jr->getValues('LocalPathId_OUT');
#    push @$setstring, "LocalPathId_OUT = '$pathid'" if $pathid ;
#
#	i::logit("$myName: INIT CREATING OUTPUT Archive");
#	my $contentfn = "$workdir/\$\$ARC.\$\$MANIFEST\$\$.TXT";
#	my @fileList;
#	if (-f $contentfn) {
#	  my $contentfh = new FileHandle("<$contentfn") || die "Unable to open list to archive file - $!";
#	  binmode $contentfh;
#	  $contentfh->read(my $buff, -s $contentfn);
#	  $contentfh->close();
#	  push @fileList, ($contentfn, ( map { (split /\t/, $_)[0] } split /[\r\n]+/, $buff ), (grep /\.(?:log|stderr|stdout)$/i, glob("$workdir/*")));
#	} else {
#	  push @fileList, (grep !/\.ps$/i, glob("$workdir/*"));
#	}
#
#	for(@fileList) {
#	  next if !-f $_;
#	  /.ps$/i && next;
#	  i::logit("$myName: Add file $_ to archive");
#	  $ar->AddFile($_, basename($_)) 
#	}
#	$ar->Close();
#	my $fsz = -s $ar->{FileName};
#
#	i::logit("$myName: END CREATING OUTPUT Archive in $ar->{FileName} size: $fsz");
#
#	push @$setstring, "ElabOutBytes = $fsz";
#	push @$setstring, "Status = $status" if $status;
#	push @$setstring, "ElabEndTime = '" . strftime('%Y-%m-%d %H:%M:%S.%U', localtime) . "'";
#	return $setstring;
##	dbExecute("UPDATE tbl_JobReports set " . join(', ', @$setstring) . " WHERE JobReportId = $JobReportId");
#}
#
sub init_worker {
	my ($WorkId, $ExternalTableName, $ExternalKey) = (shift, shift, shift);
	my $setstring = [];
	if(-d "$main::XR_WRK/$ExternalTableName/$WorkId") {
	  (my $w32dir = "$main::XR_WRK/$ExternalTableName/$WorkId") =~ s/\//\\/g;
	  i::logit("$myName: $w32dir already exists - attempting to delete");
	  system("del /s /q $w32dir && rmdir /S /Q $w32dir");
	  if ( -d "$main::XR_WRK/$ExternalTableName/$WorkId" ) {
	    my $errmsg = "$myName: Unable to delete $w32dir - still exists";
	    i::logit( $errmsg);
	    die "$errmsg\n" ;
	  }
	  i::logit("$myName: $main::XR_WRK/$ExternalTableName/$WorkId deleted");
	}

	DEBUG and i::logit("$myName: Create work directory $main::XR_WRK/$ExternalTableName/$WorkId");
	eval {
		mkpath("$main::XR_WRK/$ExternalTableName/$WorkId");
	};
	if($@) {
		i::logit("$myName: Could not create $main::XR_WRK/$ExternalTableName/$WorkId: $@");
		die "$myName: Could not create $main::XR_WRK/$ExternalTableName/$WorkId: $@\n";
	}
#	push @$setstring, "Status = ". ST_INPROCESS;
#	push @$setstring, "ElabStartTime = '" . strftime('%Y-%m-%d %H:%M:%S.%U', localtime) . "'";
##	push @$setstring, "RetryCount = RetryCount + 1" if $ExternalTableName =~ /^tbl_JobSpool$/i;
##	my $keyn = ($ExternalTableName =~ /^tbl_JobSpool$/i ? 'JobSpoolId' : 'JobReportId' ); 
#	my $keyn = 'IDENTITYCOL'; 
#	dbExecute("UPDATE $ExternalTableName set " . join(', ', @$setstring) . " WHERE $keyn = $ExternalKey");
	return "$main::XR_WRK/$ExternalTableName/$WorkId";
}

sub get_status {
	my $currjob = shift;
	my $status = $currjob->status();
	my $jpid = shift @{ [(keys %$status)] };
	my $times = $status->{$jpid}->{time};
	my $sn = $main::jobsinfo->{'_PID'.$jpid}->{_sn};
	my $statmsg = "$myName: JobMon $jpid STATUS - process: $sn kernel: $times->{kernel}, User: $times->{user}, elapsed: $times->{elapsed}";
	i::logit($statmsg);
	return $statmsg;
}

use constant DESTINATION => '226.1.1.2:2000'; 
#use constant SOCKBUFL => 4096;
#use constant LOCLPORT => 12000;

sub JobMon {
	my $currjob = shift;
	my $statmsg = get_status($currjob);
	$main::mcsock->send($statmsg, 0, DESTINATION) if $main::mcsock;
	
# my $msock = $sel->can_read(5);
# print "msock not ready - $@\n" unless $msock;
# return 0 unless $msock;

# $sock->recv(my $data,1024);
# print "Received: ", length($data), "bytes from ", $sock->peerport(), " at ", $sock->peerhost(), "\n";
# return 0 unless $data;
# print "Received: ", $data, "\n" if $data;

# return 1 if $data =~ /^CANCEL.{0,1}$/i;
# my $retaddr = $sock->peerhost();
# if ($data =~ /^STATUS\s(\d+)\D{0,1}/i) {
#   my $retport = $1;
#   IO::Socket::INET->new(Proto => 'udp', PeerAddr => "$retaddr:$1")->send($statmsg);
#   my $retsock = IO::Socket::INET->new(Proto => 'udp', PeerAddr => "$retaddr:$retport")->send($statmsg);
#   $retsock->send($statmsg);
#   $retsock->shutdown(2);
#   $retsock->close();
#   undef $retsock;
#}
	return 0;
}

sub is_syntax_error {
  use File::Basename;
  my $filename = shift;
  my $handle = gensym();

  open($handle, "<$filename") or die "Cannot open $filename: $!\n";
  my $rc = 0;
  while(<$handle>) {
#    i::logit(basename($filename).": $_ ");
    if ( /^.*(?:BEGIN failed--compilation aborted|syntax +error|(?:UN){0,1}RECOVERABLE\s+ERROR|Status\s*=\s*31).*$/i ) 
      { $rc = 1; last }
  }
  close($handle);
  
  return $rc;
}

sub logFileContent {
  use FileHandle;
  my ($pfx, $fn) = (shift, shift);
  $fn =~ s/\\/\//g;
  unless (-s $fn) {
    return i::logit("$pfx - EMPTY");
  }
  my $fh = new FileHandle("<$fn") || return i::logit("$pfx - unable to open \"$fn\" - $!");

  binmode $fh;
  $fh->read(my $buff, -s $fn);
  close $fh;
  return i::logit( map {"$pfx - $_" } split /\n/, $buff);
}

#############################################################################################################
#	MAIN
#############################################################################################################

use IO::Socket::INET;
use IO::Select;


my $isdaemon = (grep(/^\-\@$/, @ARGV) ? 1 : 0);
InitServer('isdaemon' => $isdaemon); 

my $cfgFile = 'n_o_t_f_o_u_n_d';
$cfgFile = $ENV{'XREPORT_SITECONF'}."/".$ENV{USERNAME}."/xml/$myName.xml"  unless -e $cfgFile;
$cfgFile = $ENV{'XREPORT_SITECONF'}."/xml/$myName.xml" unless -e $cfgFile;
my $cfgmtime = (stat($cfgFile))[9];
$cfg_jobs =  XML::Simple::XMLin($cfgFile, ForceArray => [qw(job step)])->{'jobs'};
i::logit("$myName: Initialization completed from \"$cfgFile\" (" . localtime($cfgmtime) . ")");

$main::SrvName = getConfValues("SrvName");
$main::XR_WRK = getConfValues("workdir");
$main::mcsock = new IO::Socket::INET(Proto=>'udp',PeerAddr=>DESTINATION, Reuse => 1);

my $disabled_scripts = undef;

my $select_query =  "SELECT * FROM tbl_WorkQueue WHERE (ExternalTableName = 'MIGRATION') AND (";
#if (scalar( @{$cfg_jobs->{'job'}} )) {
#    $select_query .= "( (";
#    $select_query .= join(' OR ', map { $_->{status} = ST_QUEUED unless exists($_->{status});
#                              "(TypeOfWork = ".$_->{type}." AND Status = ".$_->{status}.")" 
#                                   } @{$cfg_jobs->{job}} 
#                          ); 
#    $select_query .= ") AND SrvName IS NULL ) OR ";
#}

if (scalar( @{$cfg_jobs->{'job'}} )) {
    $select_query .= "( (";
    $select_query .= join(' OR ', map { $_->{status} = ST_QUEUED unless exists($_->{status});
                              "(TypeOfWork = ".$_->{type}." AND Status = ".$_->{status}."" 
							  .( exists($_->{WorkClass}) ? ' AND WorkClass = '.$_->{WorkClass} : '').")" 
                                   } @{$cfg_jobs->{job}} 
                          ); 
    $select_query .= ") AND SrvName IS NULL ) OR ";
}

$select_query .= join(' OR '
                     , "( SrvName='$main::SrvName' )" 
                     , "( Status = " . ST_RETRY . " AND ISDATE(SrvName) = 1 AND GETDATE() > SrvName )"  
                     ); 
$select_query .= ") ORDER BY Priority DESC, WorkId";
	i::logit("$myName: $select_query");
DEBUG and i::logit("$myName: $select_query");

WKITEM: while(1) {

  foreach my $dscript (keys(%{$disabled_scripts})) {
    if($disabled_scripts->{$dscript} != (stat($dscript))[9]) {
      i::logit("$myName: $dscript has modified and it will be activated");
      delete $disabled_scripts->{$dscript};
    }
  }
  
  my $step = 0;
  my ($WorkId, $ExternalTableName, $ExternalKey, $TypeOfWork) = dequeue(3, $select_query );
  
  i::logit("$myName: Initialize work for TOW $TypeOfWork - ID: $WorkId($ExternalTableName $ExternalKey)");

  my $cur_cfg = (grep { uc($_->{type}) eq uc($TypeOfWork) } @{$cfg_jobs->{'job'}})[0];

  my $XREPORT_HOME = $ENV{'XREPORT_HOME'};
  my $sh_obj = Win32::OLE->new("WScript.Shell");
  my $env_obj = $sh_obj->Environment("Process");
  $env_obj->{'XREPORT_HOME'} = $XREPORT_HOME;

  my $exit_code = 0;
  my @disabled = grep { !-e $_->[1] 
  	                     or (exists($disabled_scripts->{$_->[0]}) && $disabled_scripts->{$_->[0]} eq (stat($_->[1]))[9]) } 
    map { [$_->{cmd}, "$XREPORT_HOME\\bin\\".basename($_->{cmd})] } @{$cur_cfg->{'step'}};
#use Data::Dumper;
#die "CFGJOBS: ", Dumper($cfg_jobs), "\nCFGCUR: ", Dumper($cur_cfg), "\nDISAB: ", Dumper(\@disabled);

  if ( scalar(@disabled) ) {
    i::logit("$myName: Work $WorkId will be discarded because \""
            .join('", "', map { $_->[1] } @disabled)."\" is disabled");
    $exit_code = -1;
  }

  my ($script, $scriptname) = ($disabled[0]->[0], basename($disabled[0]->[0])) if $exit_code;
  my $cur_work_dir = init_worker($WorkId, $ExternalTableName, $ExternalKey) unless $exit_code;

  if (!$exit_code) {

    foreach $script ( sort { $a->{order} > $b->{order} } @{$cur_cfg->{step}} ) {
      $step++;
      $scriptname = basename($script->{cmd});
      i::logit("$myName: Start step $step, script ".$script->{cmd});
      
	  $main::veryverbose and (exists($script->{parameters})) and i::logit( "$myName: parameters step $step, " . $script->{parameters} );				
    
      my $job = new Win32::Job();

    
      my $jpid =$job->spawn($^X, "perl -I \"$XREPORT_HOME\\perllib\" \"$XREPORT_HOME".'\\bin\\'."$scriptname\" \"$cur_work_dir\" $ExternalKey -N $main::SrvName "
								 .( exists($script->{parameters}) ? ' '.$script->{parameters} : ' '), 
			    {       
			     stdin  => 'NUL', # the NUL device
			     stdout => $cur_work_dir.'/'.$scriptname.$step.'.stdout',
			     stderr => $cur_work_dir.'/'.$scriptname.$step.'.stderr',
			     no_window => 1
			    }
			   );
      $main::jobsinfo->{'_PID'.$jpid } = {_sn => $scriptname};

      $job->watch(\&JobMon, 10);
      my $statmsg = get_status($job);
      $main::mcsock->send($statmsg, 0, DESTINATION) if $main::mcsock;

      #    my $jpid = shift @{[(keys(%{$job->status()}))]};
      $exit_code = $job->status()->{$jpid}->{'exitcode'};
      delete $main::jobsinfo->{'_PID'.$jpid};
	  
	  $main::veryverbose and i::logit("$myName: Step $step closed with exit code: [$exit_code]");
	  
      if ($exit_code == EC_OK ) {
		($main::veryverbose or DEBUG) and i::logit("$myName: Step $step closed normally exit code: $exit_code");
		next;
	  }

      logFileContent "STEP $step STDOUT", $cur_work_dir.'/'.$scriptname.$step.'.stdout';
      logFileContent "STEP $step STDERR", $cur_work_dir.'/'.$scriptname.$step.'.stderr';
      i::logit("$myName: Unexpected exit code $exit_code from $scriptname(".(stat($scriptname))[9].")");
    
      if (  $exit_code != EC_PERL_DIE1 and $exit_code != EC_PERL_DIE2 and $exit_code != EC_PERL_DIE3 and $exit_code != EC_PERL_SYNTAX_ERROR 
	    and !is_syntax_error($cur_work_dir.'/'.$scriptname.$step.'.stdout')
	    and !is_syntax_error($cur_work_dir.'/'.$scriptname.$step.'.stderr') ) {
	i::logit("$myName: No Relevant Failure detected from Step $step - exit code reset - Job continues");
	$exit_code = 0;
	next;
      }

      if (  $exit_code == EC_PERL_SYNTAX_ERROR 
	    or (is_syntax_error($cur_work_dir.'/'.$scriptname.$step.'.stderr'))) {
	i::logit("$myName: $scriptname(".(stat($scriptname))[9].") has generated a syntax error and it'll disabled");
	$disabled_scripts->{$scriptname} = (stat($scriptname))[9];
      } 

      last;
    
    }
  
    i::logit("$myName: Terminate work for TOW $TypeOfWork($WorkId) - exit code: $exit_code");
    my $work_status = ($exit_code ? exists($cur_cfg->{onerror}) && $cur_cfg->{onerror} =~ /^RETRY$/i ? ST_RETRY : ST_PROCERROR : ST_COMPLETED);

#    my $jr_obj;
#    my $setstring = []; my $keyn;
#    $keyn = 'IDENTITYCOL'; 
#    if ( !exists($cur_cfg->{DISP}) || $cur_cfg->{DISP} !~ /^LEAVE$/i ) {
#      if ( $ExternalTableName =~ /^tbl_JobReports$/i ) {
#	#      $keyn = 'JobReportId';
#	$jr_obj = Open XReport::JobREPORT($ExternalKey);
#	$setstring = cre_OUTPUT_ARCHIVE($jr_obj, $cur_work_dir, $main::SrvName, $work_status);
#      } 
#      else {
#	#      $keyn = ($ExternalTableName =~ /^tbl_JobSpool$/i ? 'JobSpoolId' : 'JobReportId' ); 
#	$keyn = 'IDENTITYCOL'; 
#	push @$setstring, "ElabEndTime = '" . strftime('%Y-%m-%d %H:%M:%S.%U', localtime) . "'";
#	push @$setstring, "Status = $work_status";
#      }
#      dbExecute("UPDATE $ExternalTableName set " . join(', ', @$setstring) . " WHERE $keyn = '$ExternalKey'");
#    }
  }

  my $WQwhere = "ExternalTableName = '$ExternalTableName' AND ExternalKey = $ExternalKey AND TypeOfWork = $TypeOfWork AND WorkId = $WorkId";
  if ($exit_code) {
    if ( exists($cur_cfg->{onerror}) && $cur_cfg->{onerror} =~ /^RETRY(?:;(\d+))?$/i ) {
      my $retry_interval = $1 || 60;
      my $retrytime = strftime('%Y-%m-%d %H:%M:%S.%U', localtime(time() + $retry_interval ) );
      dbExecute("UPDATE tbl_WorkQueue Set SrvName = convert(varchar, dateadd(ss, $retry_interval, GETDATE()), 120)," 
		." Status = ". ST_RETRY 
		." WHERE $WQwhere");
      i::logit("$myName: $WorkId($ExternalKey) has been set for processing $retry_interval seconds from now ($retrytime)");
    }
    else {
      dbExecute("UPDATE tbl_WorkQueue Set SrvName = 'SUSPENDED', Status = " . ST_PROCERROR . " WHERE $WQwhere");
      i::logit("$myName: $WorkId($ExternalKey) has been holded from processing (SUSPENDED)");
      doEventLog($scriptname, $WorkId, ( $ExternalKey || 'N/A' ), $script);
    }
  }
  else {
    if ( !exists($cur_cfg->{DISP}) || $cur_cfg->{DISP} !~ /^LEAVE$/i ) {
      dbExecute("DELETE from tbl_WorkQueue" . " WHERE $WQwhere");
      i::logit("$myName: $WorkId($ExternalKey) deleted from workqueue");
    }
    elsif ( exists($cur_cfg->{DISP}) && $cur_cfg->{DISP} =~ /^LEAVE$/i ) {
      dbExecute("UPDATE tbl_WorkQueue Set SrvName = NULL " ." WHERE $WQwhere");
      i::logit("$myName: $WorkId($ExternalKey) has been cleared for futher processing");
    }
    $main::veryverbose and logFileContent "STEP $step STDOUT", $cur_work_dir.'/'.$scriptname.$step.'.stdout';
    $cur_work_dir =~ s/\//\\/g;
    system("del /s /q $cur_work_dir && rmdir /S /Q $cur_work_dir") unless grep(/^\-nodirdel$/, @ARGV);
    i::logit("$myName: $cur_work_dir deleted");
  }
  
}

TermServer();
