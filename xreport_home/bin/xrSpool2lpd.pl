#!/usr/bin/perl -w

use strict;
use lib "$ENV{XREPORT_HOME}/perllib";

use POSIX qw(strftime);

use XReport;
use XReport::Spool;

use IO::Socket::INET;
use IO::Select;
#use Time::HiRes qw(gettimeofday);
#use Data::Dumper;

my $workdir = $ARGV[0];
my $SpoolId = $ARGV[1] || die "Unable to get JobSpoolId\n";
my $buffsize = 4096;
my $spoolds = XReport::Spool->OpenDS(JobSpoolId => $SpoolId, buffsize => $buffsize);
#$spoolds->getDestInfos ( PrintDest => $spoolds->{PrintDest} ) || die "getDestInfos failed for $spoolds->{PrintDest} - $!";
my $odsn = "$workdir/JOB$SpoolId.bin";
$odsn =~ s/\//\\/g;
my $file2lpr = new FileHandle(">$odsn") || die "Unable to create de-staging ds \"$odsn\" - $!"; 
binmode $file2lpr;
my $printername = $spoolds->{PrtSrvAddress};
print "printername: $printername\n";
#my @prtparms = ( "PrintDuplex" => $spoolds->{PrintDuplex} );
#push @prtparms, (  "PrintParameters", $spoolds->{PrintParameters} ) if $spoolds->{PrintParameters} && $spoolds->{PrintParameters} ne '.';

my $rcount = $spoolds->{RetryCount} || 0;
$spoolds->UpdateEntry(
		      XferId => $$,
		      XferDaemon => c::getValues('SrvName'),
		      XferStartTime => strftime('%Y-%m-%d %H:%M:%S', localtime),
		      RetryCount => $rcount + 1
		     );

my ($buff, $totbytes);
while ( my $nbytes = $spoolds->ReadDS(\$buff, $buffsize ) ) {
  print $file2lpr $buff;
  $totbytes += length($buff);
  last if $nbytes < $buffsize;
}
close $file2lpr;

my ($lpdserv, $lpdq) = split /:+/, $printername;
$lpdq = 'raw' unless $lpdq;
print "lpr -S $lpdserv -P $lpdq $odsn\n";
my $lprmsg = `lpr -S $lpdserv -P $lpdq $odsn`;

die "Unexpected result from lpr of \"$odsn\" - message $lprmsg" if $lprmsg;

#print join('::', gettimeofday()), "ACK from $recipient received - PKTID: $ctlstring bytes: $prtbytes buff: " . unpack("H24", $buff), "\n";
$spoolds->CloseDS(
		  Purge => 'Y', 
		  Status => $CD::stCompleted,
		  XferEndTime => strftime('%Y-%m-%d %H:%M:%S', localtime(time())),
		  XferInBytes => -s $spoolds->{DSFN},
		  XferOutBytes => $totbytes,
		  XferRecipient => $printername
		 );
print "PROCESSING COMPLETE\n";

exit 0;
