use strict;

use XReport::DBUtil;
use Date::Calc qw(Date_to_Days Day_of_Week Add_Delta_Days Delta_Days);

my (@dateRef, @toDay, %jrn, $jrn, $jrid, $hdays, $hgens);

my $dbr;

sub toDay {
  my @t = localtime();
  return ( $t[5]+1900, $t[4]+1, $t[3] );
}

sub ExpireDate {
  my ($hdays, @refDay) = @_;

  if ( !@refDay ) {
    @refDay = @toDay;
  }
  my $weekDay = Day_of_Week(@refDay);
  
  my $weeks = int($hdays/5);
  my $rest = $hdays - $weeks*5; 

  my $deltaDays = $weeks*7 + $rest + 
  ( ( (5 - $weekDay) > $rest )  
    ? 0
    : 2
  );

  return Add_Delta_Days(@refDay, $deltaDays);
}

@dateRef = (2001, 10, 5);
@toDay = toDay();

$dbr = dbGetDynamicRs(<<EOF);
SELECT  JobReportName, HoldDays, HoldGens
FROM  tbl_JobReportNames a
WHERE EXISTS( 
  SELECT b.JobReportName FROM tbl_JobReports b
  WHERE b.JobReportName = a.JobReportName
    and JobReportName LIKE 'CCG0000%'
)
EOF

while (!$dbr->eof()) {
  $jrn = $dbr->Fields->Item('JobReportName')->Value();
  $hdays = $dbr->Fields->Item('HoldDays')->Value();
  $hgens = $dbr->Fields->Item('HoldGens')->Value();
  $hgens = 5 if ( $hgens == 0 and $hgens == 0 ); 
  print "?? $jrn $hdays $hgens\n";
  $jrn{$jrn} = [$hdays, $hgens, 0];
  $dbr->MoveNext();
}
$dbr->close();

$dbr = dbGetDynamicRs(<<EOF);
SELECT  JobReportName, JobReportId, CONVERT(varchar, XferStartTime, 111) XferStartTime, ViewLevel 
FROM  tbl_JobReports
WHERE 
  JobReportName = 'CCG00005'
order by JobReportName, XferStartTime desc
EOF
#and JobReportName = 'CCG00005'


while (!$dbr->eof()) {
  my $jrn = $dbr->Fields->Item('JobReportName')->Value();
  my $jrid = $dbr->Fields->Item('JobReportId')->Value();
  my $xfrDay = $dbr->Fields->Item('XferStartTime')->Value();

  if ( !exists($jrn{$jrn}) ) {
    print "NOT EXISTS $jrn\n";
    $dbr->MoveNext();
	next;
  }
  
  my $aref = $jrn{$jrn};
  my ($hdays, $hgens, $tON) = @$aref;

  my (@xfrDay, @exprDay) = ();

  $hdays and do {
    @xfrDay = unpack("a4xa2xa2", $xfrDay);
    @exprDay = ExpireDate($hdays, @xfrDay);
  };
 
  my $hdaysExpired = ( !$hdays or (Delta_Days(@exprDay,@dateRef) > 0) );
  my $hgensExpired = ( !$hgens or $tON >= $hgens );

  #print "?? ", join("/",@xfrDay), " ", join("/",@exprDay), " ", Delta_Days(@exprDay,@dateRef), "\n";exit;
  
  if ( $hdaysExpired and $hgensExpired ) {
    print "$jrn $jrid ", join("/", @xfrDay), "\n";
    #$dbr->Fields->Item('ViewLevel')->SetProperty("Value", 2);
    #$dbr->Update();
  }
  else {
    $aref->[2] += 1;
  }
  
  $dbr->MoveNext();
}

$dbr->close();
