#!perl -w

use strict;

$| = 1;
my $oldfh = select STDERR;
$| = 1;
select $oldfh;


use XReport; 
use XReport::JobREPORT;
use Data::Dumper;  

#use Win32::Unicode::Dir 'mkpathW';
use File::Path 'make_path';
use File::Copy 'cp';
use File::Basename;

use constant DEBUG => 0;
use constant EC_OK => 0;
use constant EC_USER_SHUTDOWN => 1;
use constant EC_UNKNOWN_ERROR => 2;
use constant EC_PERL_SYNTAX_ERROR => 9;
use constant EC_PERL_DIE3 => 10045;
use constant EC_PERL_DIE1 => 10061;
use constant EC_PERL_DIE2 => 255;

use constant TOW_INPUT_TO_MIGRATE => 666;
use constant TOW_OUTPUT_TO_MIGRATE => 888;

use constant ST_RECEIVED => 16;
use constant ST_QUEUED => 16;
use constant ST_INPROCESS => 17;
use constant ST_COMPLETED => 18;
use constant ST_PROCERROR => 31;


$main::myName = ( split( /[\/\\]/, $0 ) )[-1];
$main::myName = ( split( /\./, $main::myName ) )[0];

sub print2Log { my @array =@_; return i::logit("$main::myName: ".join(",", map { $_ =~ s/\//\\/g; $_ } @array)."_LINE[".(caller())[2]."]"); }

sub MoveStorage {
	($main::veryverbose || DEBUG) && print2Log("call MoveStorage().");
	my $outmess = 'SUCCESS';
	my $FullFileNames ={};
	my ($jr , %args) = @_; 
	my $moveInput = $args{'moveInput'} if defined ($args{'moveInput'});
	my $moveOutput = $args{'moveOutput'} if defined ($args{'moveOutput'});
	unless( $moveInput or $moveOutput) {
		$outmess ="ERROR - The value of parameters moveInput[$moveInput] and moveOutput[$moveOutput] is wrong. Check the call."; 
		print2Log("$outmess.");
		return $outmess;
	}
	my $NewLocalPathId = ($args{'NewLocalPathId'} ? $args{'NewLocalPathId'} : '');
	($main::veryverbose || DEBUG) && print2Log("jr[$jr].");
	
	my $NewLocalPath = $XReport::cfg->{LocalPath}->{$NewLocalPathId};
	if ( $NewLocalPath !~ /^file:/i) {
		$outmess ="ERROR - The path associated to the parameter [$NewLocalPathId] is wrong. Check the configuration."; 
		print2Log("$outmess.");
		return $outmess;
	}
	$NewLocalPath =~ s/^file:\/\///i;	
	make_path ("$NewLocalPath") if (!-e "$NewLocalPath"); 
	unless ((-e $NewLocalPath) && (-e $NewLocalPath))
	{
		$outmess ="ERROR - The path [$NewLocalPath] associated to the parameter $NewLocalPathId does not exist. Check the configuration."; 
		print2Log("$outmess.");
		return $outmess;
	}
	($main::veryverbose || DEBUG) && print2Log("NewLocalPath[$NewLocalPath].");   
	
	if ( !ref($jr) ) {
		require XReport::ARCHIVE::JobREPORT; 
		eval {
			$jr = XReport::ARCHIVE::JobREPORT->Open($jr);
		};
		if ($@) { 
			if ( $@ =~ /JobReportId ([0-9]+) NOT FOUND/i) {  
				print2Log("ERROR in XReport::ARCHIVE::JobREPORT->Open($jr)-JobReportId $1 NOT FOUND. The direct extraction from db is needed.");
			}
			else
			{
				$outmess = "ERROR in XReport::ARCHIVE::JobREPORT->Open($jr)- $@" ;
				($main::veryverbose || DEBUG) && print2Log("$outmess.");
				return $outmess;
			}
		} 
	}
	my ($outmessGetDataJR, $JobReportId, $LocalPathId_IN,  $LocalPathId_OUT, $LocalFileName, $OldFullFileNameIN, $OldFullFileNameOUT) = getDataJR($jr); 
	($main::veryverbose || DEBUG) && print2Log("JobReportId[$JobReportId] - LocalPathId_IN[$LocalPathId_IN] - LocalPathId_OUT[$LocalPathId_OUT] - LocalFileName[$LocalFileName] - OldFullFileNameIN[$OldFullFileNameIN] - OldFullFileNameOUT[$OldFullFileNameOUT] - outmessGetDataJR[$outmessGetDataJR].");
	if( $outmessGetDataJR !~ /SUCCESS/i )
	{
		$outmess = $outmessGetDataJR;
		return $outmess;
	}
	
	#INPUT storage management
	my $inputCopied = 0; 
	if($moveInput)
	{
		if(($LocalPathId_IN eq $NewLocalPathId) or ($LocalPathId_IN eq '') )
		{
			($LocalPathId_IN eq $NewLocalPathId) and print2Log("LocalPathId_IN is already [$LocalPathId_IN] - no change is needed.");
			($LocalPathId_IN eq '') and print2Log("LocalPathId_IN is empty - no change is needed.");
		
		}
		else
		{ 
			if ( $OldFullFileNameIN !~ /^file:/i) {
				print2Log("(IN) Archive type \"".substr($OldFullFileNameIN, 0, 8). "\" is not supported. Maybe the file was migrated. No change is needed.");
			}
			else
			{ 
				$OldFullFileNameIN =~ s/^file:\/\///i;
				$OldFullFileNameIN =~ s/\\/\//g;
				unless (-e $OldFullFileNameIN)
				{
					$outmess ="ERROR - The original file [$OldFullFileNameIN] does not exist."; 
					($main::veryverbose || DEBUG) && print2Log("$outmess.");
					return $outmess;	
				}
				(my $NewFullFileNameIN = $XReport::cfg->{LocalPath}->{$NewLocalPathId}."/IN/$LocalFileName") =~ s/^file:\/\/// ;
				$NewFullFileNameIN =~ s/\\/\//g;
				my $NewDirnameIN = File::Basename::dirname($NewFullFileNameIN);
				make_path ("$NewDirnameIN") if (!-e "$NewDirnameIN"); 
				unless (-e $NewDirnameIN && -d $NewDirnameIN){
					$outmess = "unable to access $NewDirnameIN" ;
					($main::veryverbose || DEBUG) && print2Log("$outmess.");
					return $outmess;
				}
				($main::veryverbose || DEBUG) && print2Log("OldFullFileNameIN[$OldFullFileNameIN].");	 
				($main::veryverbose || DEBUG) && print2Log("NewFullFileNameIN[$NewFullFileNameIN].");
				eval {
					File::Copy::cp("$OldFullFileNameIN", "$NewFullFileNameIN");
					#File::Copy::cp("$OldFullFileNameIN", "$NewFullFileNameIN") unless -e $NewFullFileNameIN;
				};
				if ($@) {
					$outmess = "ERROR in copying the file [$NewFullFileNameIN]- $@" ;
					($main::veryverbose || DEBUG) && print2Log("$outmess.");
					return $outmess;
				} 	
				if(-e $NewFullFileNameIN) {
					$inputCopied = 1;
					$FullFileNames->{IN}->{NEW}=$NewFullFileNameIN;
					$FullFileNames->{IN}->{OLD}=$OldFullFileNameIN;
				}
				else {
					$outmess ="ERROR - The file [$NewFullFileNameIN] was not created."; 
					($main::veryverbose || DEBUG) && print2Log("$outmess.");
					return $outmess;
				}	
			}
		}
	}

	#OUTPUT storage management
	my $outputCopied = 0;  
	if($moveOutput)
	{
		if(($LocalPathId_OUT eq $NewLocalPathId) or ($LocalPathId_OUT eq '') ) 
		{
			($LocalPathId_OUT eq $NewLocalPathId) and print2Log("LocalPathId_OUT is already [$LocalPathId_OUT] - no change is needed.");
			($LocalPathId_OUT eq '') and print2Log("LocalPathId_OUT is empty - no change is needed.");
		}
		else
		{ 
			($main::veryverbose || DEBUG) && print2Log("OldFullFileNameOUT[$OldFullFileNameOUT]."); 
			if ( $OldFullFileNameOUT !~ /^file:/i) {
				print2Log("(OUT) Archive type \"".substr($OldFullFileNameOUT, 0, 8). "\" is not supported. Maybe the file was migrated. No change is needed.");
			}
			else { 
				$OldFullFileNameOUT =~ s/^file:\/\///i; 
				$OldFullFileNameOUT =~ s/\\/\//g;
				($main::veryverbose || DEBUG) && print2Log("OldFullFileNameOUT[$OldFullFileNameOUT].");
				unless (-e $OldFullFileNameOUT)
				{
					$outmess ="ERROR - The original file [$OldFullFileNameOUT] does not exist."; 
					($main::veryverbose || DEBUG) && print2Log("$outmess.");
					return $outmess;	
				}
				(my $NewFullFileNameOUT = $XReport::cfg->{LocalPath}->{$NewLocalPathId}."/OUT/$LocalFileName") =~ s/^file:\/\/// ;
				$NewFullFileNameOUT =~ s/\\/\//g;
				$NewFullFileNameOUT = File::Basename::dirname($NewFullFileNameOUT)."/".File::Basename::basename($OldFullFileNameOUT);
				my $NewDirnameOUT = File::Basename::dirname($NewFullFileNameOUT);
				make_path ("$NewDirnameOUT") if (!-e "$NewDirnameOUT"); 
				unless (-e $NewDirnameOUT && -d $NewDirnameOUT){
					$outmess = "unable to access $NewDirnameOUT" ;
					($main::veryverbose || DEBUG) && print2Log("$outmess.");
					return $outmess;
				}
				($main::veryverbose || DEBUG) && print2Log("OldFullFileNameOUT[$OldFullFileNameOUT].");	 
				($main::veryverbose || DEBUG) && print2Log("NewFullFileNameOUT[$NewFullFileNameOUT].");
				eval {
					File::Copy::cp("$OldFullFileNameOUT", "$NewFullFileNameOUT");
					#File::Copy::cp("$OldFullFileNameOUT", "$NewFullFileNameOUT") unless -e $NewFullFileNameOUT;
				};
				if ($@) {
					$outmess = "ERROR in copying the file [$NewFullFileNameOUT]- $@" ;
					($main::veryverbose || DEBUG) && print2Log("$outmess.");
					return $outmess;
				} 		
				if(-e $NewFullFileNameOUT) {
					$outputCopied = 1;
					$FullFileNames->{OUT}->{NEW}=$NewFullFileNameOUT;
					$FullFileNames->{OUT}->{OLD}=$OldFullFileNameOUT;
				}
				else { 
					$outmess ="ERROR - The file [$NewFullFileNameOUT] was not created."; 
					($main::veryverbose || DEBUG) && print2Log("$outmess.");
					return $outmess;
				} 
			}
		}
	}

	if($inputCopied or $outputCopied )
	{
		my $NewLocalPathId_IN = ($inputCopied ? $NewLocalPathId : $LocalPathId_IN);
		my $NewLocalPathId_OUT = ($outputCopied ? $NewLocalPathId : $LocalPathId_OUT);
	
		my $sql =""
		." UPDATE tbl_JobReports"
		." SET  LocalPathId_IN = '$NewLocalPathId_IN'"
		." ,LocalPathId_OUT = '$NewLocalPathId_OUT'"
		." OUTPUT Inserted.LocalPathId_IN,  Inserted.LocalPathId_OUT"
		." where JobReportId = '$JobReportId';";
		print2Log("sql:$sql.");
		eval {
			my $dbr_update = XReport::DBUtil::dbExecute($sql); 
			my ($newLocalPathId_IN, $newLocalPathId_OUT ) = $dbr_update->GetFieldsValues(qw(LocalPathId_IN LocalPathId_OUT));
			($main::veryverbose || DEBUG) && print2Log("newLocalPathId_IN:$newLocalPathId_IN newLocalPathId_OUT:$newLocalPathId_OUT.");
			$dbr_update->Close(); 
		};
		if($@)
		{ 
			$outmess ="ERROR ON UPDATE IN  tbl_JobReports: $!"; 
			($main::veryverbose || DEBUG) && print2Log("$outmess.");
			return $outmess;
		}
		print2Log("Operation of Update performed with success. ."); 
		
		foreach my $file (map {$_->{OLD}} grep { (-e $_->{OLD}) and (-e $_->{NEW}) } map {$FullFileNames->{$_}} (keys %$FullFileNames))
		{ 
			eval {unlink $file };
			if($@)
			{ 
				$outmess ="DELETE ERROR for the file \"$file\" $!"; 
				($main::veryverbose || DEBUG) && print2Log("$outmess.");
				return $outmess;
			} 
			print2Log("The file $file was deleted with success.");
		}
	}
	return $outmess;
}

sub getDataJR(){
	($main::veryverbose || DEBUG) && print2Log("call getData().");
	my ($jr , %args) = @_; 
	my ($outmess, $JobReportId, $LocalPathId_IN,  $LocalPathId_OUT, $LocalFileName, $OldFullFileNameIN, $OldFullFileNameOUT) = ('SUCCESS','','','','','','','','','','','','');
	if ( ref($jr) ) {
		($JobReportId, $LocalPathId_IN,  $LocalPathId_OUT, $LocalFileName) = $jr->getValues(qw(JobReportId LocalPathId_IN LocalPathId_OUT LocalFileName)); 
		if($LocalPathId_IN)	
		{
			my  $INPUT = $jr->get_INPUT_ARCHIVE(); 
			if(!$INPUT)
			{
				$outmess ="ERROR - Unable to acquire INPUT handler."; 
				($main::veryverbose || DEBUG) && print2Log("$outmess.");
				return ($outmess,$JobReportId, $LocalPathId_IN,  $LocalPathId_OUT, $LocalFileName, $OldFullFileNameIN, $OldFullFileNameOUT);
			} 		
			$OldFullFileNameIN = $INPUT->getFileName() ; 
		}	
		if($LocalPathId_OUT)	
		{
			$OldFullFileNameOUT = $jr->getFileName('ZIPOUT');	 
			$OldFullFileNameOUT = $XReport::cfg->{LocalPath}->{$LocalPathId_OUT}.'/OUT/'. $OldFullFileNameOUT ;	
		}		
	}
	else
	{
		my $dbr = XReport::DBUtil::dbExecute("SELECT * from tbl_JobReports where JobReportId = $jr"); 
		if (!$dbr->eof())
		{
			($JobReportId, $LocalPathId_IN,  $LocalPathId_OUT, $LocalFileName ) = $dbr->GetFieldsValues(qw(JobReportId LocalPathId_IN LocalPathId_OUT LocalFileName));
			$OldFullFileNameIN = ($LocalPathId_IN ? $XReport::cfg->{LocalPath}->{$LocalPathId_IN}."/IN/$LocalFileName" : '');
			$OldFullFileNameOUT = ($LocalPathId_OUT ? $XReport::cfg->{LocalPath}->{$LocalPathId_OUT}."/OUT/$LocalFileName" : '');
			$OldFullFileNameOUT =~ s/\.DATA\.TXT\.gz\.tar$/\.OUT\.#0\.zip/i;
		}
		$dbr->Close();
	}
	return ($outmess,$JobReportId, $LocalPathId_IN,  $LocalPathId_OUT, $LocalFileName, $OldFullFileNameIN, $OldFullFileNameOUT);
}

print2Log("$0($$) starting - FILE[".__FILE__."].");
my $worrkdir = $ARGV[0];
my $JRID = $ARGV[1];
my $outmess = 'SUCCESS';

if (( grep /^-dd$/i, @ARGV ) || DEBUG ){
	$main::veryverbose =1;
	($main::veryverbose || DEBUG) and print2Log("MODE veryverbose ACTIVATED.");
}

my $moveInput = 0;
if ( grep /^-moveInput$/i, @ARGV ) {
	$moveInput =1;
	print2Log("- MODE moveInput ACTIVATED"); 
} 
my $moveOutput = 0;
if ( grep /^-moveOutput$/i, @ARGV ) {
	$moveOutput =1;
	print2Log("- MODE moveOutput ACTIVATED"); 
} 
 
unless($moveInput or $moveOutput)
{
 	$outmess ="ERROR - The parameters -moveInput and -moveOutput are missing. Check the configuration."; 
 	print2Log("$0($$) end outmess=$outmess.");
	exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);
}
 
my $NewLocalPathId = undef;
foreach my $i (0 .. $#ARGV) 
{
	if (uc($ARGV[$i]) =~ /^\-NewLocalPathId$/i) { 
	  $NewLocalPathId = $ARGV[$i+1];
	  last;
	}
}

if(!defined($NewLocalPathId))
{
 	$outmess ="ERROR - The parameter -NewLocalPathId is missing. Check the configuration."; 
 	print2Log("$0($$) end outmess=$outmess.");
	exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);
}

$outmess=MoveStorage( $JRID
					, NewLocalPathId => "$NewLocalPathId" 
					, moveOutput => $moveOutput 
					, moveInput => $moveInput );
print2Log("$0($$) end outmess=$outmess.");
exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);

1;