#!perl -w

use strict;

$| = 1;
my $oldfh = select STDERR;
$| = 1;
select $oldfh;


use XReport;
use XReport::DBUtil ();
use XReport::JobREPORT ();
use XReport::ARCHIVE::Centera ();
use Data::Dumper; 
use constant EC_OK => 0;
use constant EC_USER_SHUTDOWN => 1;
use constant EC_UNKNOWN_ERROR => 2;
use constant EC_PERL_SYNTAX_ERROR => 9;
use constant EC_PERL_DIE3 => 10045;
use constant EC_PERL_DIE1 => 10061;
use constant EC_PERL_DIE2 => 255;

use constant DEBUG => 0;
my $myName = ( split( /[\/\\]/, $0 ) )[-1];
$myName = ( split( /\./, $myName ) )[0];

sub RenameIN_CDAMFILE {
  ($main::veryverbose || DEBUG) && i::logit("$myName: call RenameIN_CDAMFILE()" );
	
  my ($jr , %args) = @_; 
  if ( !ref($jr) ) {
    require XReport::ARCHIVE::JobREPORT; 
    $jr = XReport::ARCHIVE::JobREPORT->Open($jr)
  }
  ($main::veryverbose || DEBUG) && i::logit("$myName: after (XReport::ARCHIVE::JobREPORT->Open)" );
  my ($JobReportName, $JobReportId, $LocalPathId_IN, $LocalPathId_OUT, $LocalFileName, $JobStatus) = $jr->getValues(qw(JobReportName JobReportId LocalPathId_IN LocalPathId_OUT LocalFileName Status));  
  my $newReportName = ($args{'newReportName'} ? $args{'newReportName'} : 'CDAMFILE');
  my $doCheckOUTPUT = (exists($args{'doCheckOUTPUT'}) ? $args{'doCheckOUTPUT'} : '1');
  my $outmess = "";  
  if( $doCheckOUTPUT && ( ($JobStatus eq '18') || ($LocalPathId_OUT ne '') ) )
  {
	i::logit("$myName: OUTPUT ALREADY CREATED for JobReportId[$JobReportId].Status[$JobStatus].LocalPathId_OUT[$LocalPathId_OUT]" );
	$outmess = "OUTPUT ALREADY CREATED for JobReportId[$JobReportId]" ;
	return ($outmess);
  }
  
  my $sql = '';
  if($JobReportName !~ /^$newReportName$/i) {
		_updateDataFromSQL("BEGIN TRANSACTION REPORTADD");
		eval 
		{
			my $newLocalFileName_IN = $LocalFileName;
			$newLocalFileName_IN =~ s/$JobReportName/$newReportName/;
			my $LocalPath_IN = $XReport::cfg->{LocalPath}->{$LocalPathId_IN};
			die "Files on PERSISTENCE STORAGE -- Cannot rename $LocalFileName to $newLocalFileName_IN" if($LocalPath_IN !~ /^file:\/\//i);
			$LocalPath_IN =~ s/^file:\/\///;    
			$sql =
				"UPDATE tbl_JobReports SET JobReportName = '$newReportName', LocalFileName = '$newLocalFileName_IN' ".
				"WHERE (JobReportId = $JobReportId ) ";
			($main::veryverbose || DEBUG) && i::logit("$myName: sql=".$sql);
			#$outmess .= "UPDATED JobReports LocalFilename\n";
			_updateDataFromSQL($sql);
			($main::veryverbose || DEBUG) && i::logit("$myName: UPDATED JobReports LocalFilename for JobReportId[$JobReportId]" );
			my $oldLocalFileName_IN_Full = "$LocalPath_IN/IN/$LocalFileName";
			my $newLocalFileName_IN_Full = "$LocalPath_IN/IN/$newLocalFileName_IN";
			($main::veryverbose || DEBUG) && i::logit("$myName:  rename($oldLocalFileName_IN_Full, $newLocalFileName_IN_Full)" );
			die "Cannot rename $oldLocalFileName_IN_Full to $newLocalFileName_IN_Full: $!, $^E" if(!rename($oldLocalFileName_IN_Full, $newLocalFileName_IN_Full));	
#			if(($LocalPathId_OUT ne '') &&(!$doCheckOUTPUT))
#			{
#				my $LocalPath_OUT = $XReport::cfg->{LocalPath}->{$LocalPathId_OUT};
#				my $oldLocalFileName_OUT = $LocalFileName;
#				$oldLocalFileName_OUT =~ s/DATA.TXT.gz.tar$/OUT.#0.zip/i;
#				my $newLocalFileName_OUT = $oldLocalFileName_OUT;
#				$newLocalFileName_OUT =~ s/$JobReportName/$newReportName/;
#				die "Files on PERSISTENCE STORAGE -- Cannot rename $oldLocalFileName_OUT to $oldLocalFileName_OUT" if($LocalPath_OUT !~ /^file:\/\//i);
#				$LocalPath_OUT =~ s/^file:\/\///;   
#				my $oldLocalFileName_OUT_Full = "$LocalPath_OUT/OUT/$oldLocalFileName_OUT";
#				my $newLocalFileName_OUT_Full = "$LocalPath_OUT/OUT/$newLocalFileName_OUT";
#				i::logit("$myName:  rename($oldLocalFileName_OUT_Full, $newLocalFileName_OUT_Full)" );
#				
#				unless (-e $oldLocalFileName_OUT_Full && -f $oldLocalFileName_OUT_Full && -s $oldLocalFileName_OUT_Full)
#				{
#					($main::veryverbose || DEBUG) && i::logit("$myName: Unable to access $oldLocalFileName_OUT_Full or file emty (size:\".-s $oldLocalFileName_OUT_Full.\")");
#				}
#				else
#				{
#					eval
#					{
#						die "Cannot rename $oldLocalFileName_OUT_Full to $newLocalFileName_OUT_Full: $!, $^E" if(!rename($oldLocalFileName_OUT_Full, $newLocalFileName_OUT_Full));	
#					};
#					if($@) {
#						$outmess.= " FAILED: $@";
#						rename($newLocalFileName_IN_Full, $oldLocalFileName_IN_Full);
#						die "$@";
#					}
#				}
#
#			}
		};
		if($@) {
			$outmess.= " FAILED: $@";
			_updateDataFromSQL("ROLLBACK TRANSACTION REPORTADD");
			i::logit("$myName: ROLLBACK TRANSACTION REPORTADD" );
			
		} else {
			_updateDataFromSQL("COMMIT TRANSACTION REPORTADD");
			$outmess.="SUCCESS";
		}
	}
	else {
		$outmess = "THE REPORT NAME IS ALREADY [$newReportName]";
		i::logit("$myName: $outmess" );
	}
	return ($outmess);
}

sub _updateDataFromSQL {
  ($main::veryverbose || DEBUG) && i::logit("$myName: call _updateDataFromSQL()" );
	my ($sql) = (shift);
  	my $dbr = XReport::DBUtil::dbExecute($sql);	
}

i::logit("$myName:  - $0($$) starting");
my $worrkdir = $ARGV[0];
my $JRID = $ARGV[1];


my $newReportName = 'CDAMFILE';
foreach my $i (0 .. $#ARGV) 
{
	if ( $ARGV[$i] =~ /^\-newReportName$/i) { # newReportName
	  $newReportName = $ARGV[$i+1];
	  ($main::veryverbose || DEBUG) && i::logit("$myName: newReportName=$newReportName " );
	  last;
	}
}

my $doCheckOUTPUT = '1';
if ( grep /^-doCheckOUTPUT$/i, @ARGV ) {
	$doCheckOUTPUT ='1';
	($main::veryverbose || DEBUG) && i::logit("$myName: - MODE doCheckOUTPUT ACTIVATED");
}
elsif ( grep /^-noCheckOUTPUT$/i, @ARGV ) {
	$doCheckOUTPUT = '0';
	($main::veryverbose || DEBUG) && i::logit("$myName: - MODE noCheckOUTPUT ACTIVATED");
}


my $outmess = RenameIN_CDAMFILE($JRID, newReportName => $newReportName, doCheckOUTPUT => $doCheckOUTPUT );
#($main::veryverbose || DEBUG) && i::logit("$myName: outmess=$outmess " );
i::logit("$myName:  - $0($$) end - outmess=$outmess");
exit ($outmess =~ /SUCCESS/i ? EC_OK: EC_PERL_DIE2);

1;



