use strict;

use XReport;
use lib($main::Application->{'XREPORT_HOME'}."/perllib");

my $worrkdir = $ARGV[0];
my $JRID = $ARGV[1];
my $getParseFileName = 1 if (grep /-getParseFileName/, @ARGV);

use XReport::Util qw(:CONFIG);
use XReport::JobREPORT;
use Data::Dumper;
use FileHandle;


use constant ST_RECEIVED => 16;
use constant ST_QUEUED => 16;
use constant ST_INPROCESS => 17;
use constant ST_COMPLETED => 18;
use constant ST_PROCERROR => 31;

InitServer(isdaemon => 1); 
#no warn redefine;
*i::logit = sub { 
     foreach my $logln ( split /\n/, join(' ', map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_) ) {
        $main::logger->log($logln);
     }
 };
my $debgRtn = sub {
    i::logit(@_) if  ($main::debug || $main::veryverbose);
#    i::logit(join(' ', map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_)) if ($main::debug || $main::veryverbose); 
};
#$logrRtn = sub { print map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n"; };
my $logrRtn = sub { 
     i::logit(@_);
#    i::logit(join(' ', map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_)); 
};
#$main::Application->{NoApplicationLock} = 1;
#$main::debug = 1;
my $jr; 
#my $jr = XReport::JobREPORT->Open($JRID,1); 
if($getParseFileName)
{   
    $jr = XReport::JobREPORT->Open($JRID,1); 
	my ($ParseFileName,$JobReportName) = map { (split( /[\s,]+/, $_))[-1] } $jr->getValues(qw(ParseFileName JobReportName));
	my $confdir = $XReport::cfg->{confdir};
	$confdir = $XReport::cfg->{userlib}."/rptconf" unless $confdir && -d $confdir;
	my $rptconfXML = $confdir.'/'.$JobReportName.'.xml';
	$rptconfXML = $confdir.'/'.$ParseFileName unless (-f $rptconfXML); 
	i::logit("TESTSAN - JRID=$JRID - ParseFileName=$ParseFileName - JobReportName=$JobReportName  - rptconfXML=$rptconfXML ");
}
else
{
	 $jr = XReport::JobREPORT->Open($JRID,0); 
}
#i::logit("TESTSAN - Dumper(jr)=".Dumper($jr));
#my $status = $jr->doSingleParserElab();

my $fileName = join("/", $jr->getValues(qw($INPATH LocalFileName))); 
my $FileContents;
if ($fileName !~ /^centera:\/\//i)
{

	$fileName =~ s/\.DATA\.TXT\.gz$/.CNTRL.TXT/;
	i::logit("TESTSAN - fileName=".$fileName);

	my $cf = FileHandle->new("<$fileName")
	or die "OPEN FILE ERROR FOR CONTROL FILE \"$fileName\" rc=$!"; 
	#binmode($cf);

	#$cf->read(my $FileContents, -s $fileName);
	$cf->read(  $FileContents, 5000); 
	$cf->close();
	#i::logit("TESTSAN - FileContents=".$FileContents);

	#my $cinfo = $jr->readControlFile();
	#i::logit("TESTSAN - Dumper(cinfo)=".Dumper($cinfo));
	#
	#my @lines;
	#foreach my $key (keys %$cinfo)
	#{
	#	push @lines, split /[^a-zA-Z0-9_\-\.]+/, $cinfo->{$key};
	#	
	#}
}
else
{
	$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 0;
	my $ijrar = $jr->get_INPUT_ARCHIVE();   
	my $cdamfh = $ijrar->get_INPUT_ARCHIVE(); 

	my $FileTags = $cdamfh->FileTags(); 
	foreach my $hash (@$FileTags)
	{ 
		foreach my $key (sort keys %$hash)
		{
			#($main::veryverbose || DEBUG) && print2Log("FileTags - $key: ". $hash->{$key} ); 
			if(( $key =~ /^md5$/i) && (!($hash->{$key})) )
			{
				$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 1;
				i::logit("TESTSAN - noCheckMd5 on XReport::ARCHIVE::Centera::File::INPUT.pm"); 
			}
		}
	}

	my $FileList = $cdamfh->FileList();
	foreach my $iFileName (grep /\.DATA\.TXT\.gz\.tar$/i, @$FileList) {
		i::logit("TESTSAN - iFileName=$iFileName "); 
		my $INPUT = $cdamfh->LocateFile($iFileName); 
		die("ExtractFile $iFileName NOT Located") if !$INPUT;  
		$INPUT->Open(); 
		my $OUTPUT = new IO::String($FileContents);
		$OUTPUT->binmode();
		my $osize = 0; 
		my $buff = ' ' x 5000;
		while (1) {
		  $INPUT->read($buff, 5000); 
		  last if $buff eq '';
		  $osize += length($buff); 
		  $OUTPUT->write($buff);
		  last;
		} 
		$INPUT->Close();
		$OUTPUT->close; 
	}
	i::logit("TESTSAN - FileContents=$FileContents "); 
	$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 0;
}

my @lines;
push @lines, split /[^a-zA-Z0-9_\-\.]+/, $FileContents;
my %fields = map { $_ => 1 } qw(BLKSZ block_offsets_ttr CCHAR CTDDATE data_file_ttr DDNAM DSNAME eportname hdrl JobName LRECL lrecl maxlrec_detected OJBID RECFM RemoteFileName STEPN SYSID);
my %parameters;
$parameters{ JRID } = $JRID; 


foreach my $key(keys %fields)
{  
	$parameters{$key} = 'not found';
	
}
foreach my $i (0 .. $#lines) {
	if(exists($fields{$lines[$i]})) 
	{ 
		my $k=$lines[$i]; 
		my $v=$lines[++$i];
		$parameters{ $k } = $v; 
	} 
} 

my $line = "\t";
my $header = "\t";
foreach my $key(sort keys %parameters)
{ 
	#i::logit("TESTSAN - $key=".$parameters{$key}. "line=".__LINE__);  
	$line=$line.$parameters{$key}."\t";
	$header=$header.$key."\t";
} 
	i::logit("TESTSAN - >>>$header >>>");  
	i::logit("TESTSAN - >>>$line >>>");  
TermServer();

exit 0;
__END__
sub {
    my ($self, $page) = (shift, shift);
    my $getVars = 0;
    my $bundleslist = [];
    $U::UserVars{'$$bundles$$'} = '';
    @{U::UserVars}{qw(UserRef)} = ("RID ADD.TI GT 30.000");
    if ( 1 ) { 
        my @tvars;
    GETVARS01: { $getVars = 0;
                    if ( @tvars = unpack('a5', $page->GetRawPageRect(4,2,4,6)) ) {

                        @{U::UserVars}{'CUTVAR'} = @tvars;

                        $getVars = 1;
                    }
                    last GETVARS01 if $getVars;
                }                                                                                                                   
        if ($getVars) {

        }
    }
    $U::UserVars{'$$bundles$$'} = join(' ', @{$bundleslist} )
                                     if ( scalar(@{$bundleslist} );
    $U::UserVars{UserRef} = $page->getJobReportValues('JobReportDescr') if ( !exists($U::UserVars{UserRef}) ); 
    return $getVars;
}
