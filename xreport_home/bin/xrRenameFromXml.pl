#!perl -w

use strict;

$| = 1;
my $oldfh = select STDERR;
$| = 1;
select $oldfh;


use XReport;
use XReport::DBUtil ();
use XReport::JobREPORT ();
#use XReport::ARCHIVE ();
#use XReport::ARCHIVE::JobREPORT (); 
#use XReport::ARCHIVE::Centera ();
use Data::Dumper; 
use File::Copy;

use constant DEBUG => 0;
use constant EC_OK => 0;
use constant EC_USER_SHUTDOWN => 1;
use constant EC_UNKNOWN_ERROR => 2;
use constant EC_PERL_SYNTAX_ERROR => 9;
use constant EC_PERL_DIE3 => 10045;
use constant EC_PERL_DIE1 => 10061;
use constant EC_PERL_DIE2 => 255;


$main::myName = ( split( /[\/\\]/, $0 ) )[-1];
$main::myName = ( split( /\./, $main::myName ) )[0];

sub write2log
{
  my( $package, $filename, $line ) = caller(); 
  
  i::logit("$main::myName: - ".join('', @_).'_caller:'.join('::',$package, $filename, $line));

}

sub debug2log
{
  return unless ($main::veryverbose || DEBUG) ;
  my( $package, $filename, $line ) = caller(); 
  i::logit("$main::myName: - ".join('', @_).'_caller:'.join('::',$package, $filename, $line));

}


sub getJobreportnameFromXML {
    debug2log("$main::myName: call getJobreportnameFromXML()");
	my $request = shift;  
	foreach (qw(JOBNM	DDNAM	STEPN	CLASS	WRTRN J))
	{
		$request->{cinfo}->{$_} = '' unless (exists($request->{cinfo}->{$_}));
	}
	
	if (!$request->{cinfo}->{'JOBNM'} && ($request->{cinfo}->{N} =~  /^.*\.([\w\$]{8})\.(JOB[0-9]{5})\..*$/)) {
		$request->{cinfo}->{'JOBNM'} = $1;
		$request->{cinfo}->{'OJBID'} = $2 unless  (!$request->{cinfo}->{'OJBID'});
		write2log('changed JOBNM from N='. $request->{cinfo}->{'JOBNM'} );
	} 
	
	if($request->{cinfo}->{JOBNM} =~ /^[ \t]*$/ )
	{
		$request->{cinfo}->{JOBNM} = $request->{cinfo}->{J};
		write2log('changed JOBNM from J='. $request->{cinfo}->{'JOBNM'} );
	}
	
	my $confdir = $XReport::cfg->{confdir};
	$confdir = $XReport::cfg->{userlib}."/rptconf" unless $confdir && -d $confdir;
	my $reportname = 'XRRENAME';
	my $rptconf = ( -f $confdir.'/'.$reportname.'.xml' ? $confdir.'/'.$reportname.'.xml' : 	undef); 
	die "File $rptconf not found." unless(-f $rptconf); 
	write2log('rptconf='. $rptconf );
	my $newvals = parseRptConf($request, $rptconf);
	debug2log("Data::Dumper(newvals})= ".Dumper($newvals)); 
	return $newvals->{reportname} if (exists ($newvals->{reportname})); 
    return undef;
}

sub parseRptConf {   
  my ($request, $xml) = (shift, shift);
  my $newvals = {}; 
  return $newvals unless ( $xml && ($xml =~ /^</ or -e $xml) );
    my $xmlconf = c::parseXmlConfig($xml);
    my ($ck) = grep {/Receiver/i} (keys %$xmlconf);
    my ($xrcfgk) = grep /Receiver/i, keys %{$XReport::cfg}; 
	$xrcfgk = 'xrcfgk' unless $xrcfgk;
	debug2log('xrcfgk='. $xrcfgk );
	$XReport::cfg->{$xrcfgk} = {} unless exists($XReport::cfg->{$xrcfgk});
    return $newvals unless ($ck || $xrcfgk); 
    my $conf = ($ck ? c::mergeStruct($XReport::cfg->{$xrcfgk}, $xmlconf->{$ck}) : $XReport::cfg->{$xrcfgk});
    $xmlconf = undef;
	
	debug2log("CONF: ", Dumper($conf), "");

  if ( DEBUG && exists($conf->{DEBUG}) && $conf->{DEBUG}) 
  {
    $main::veryverbose = 1;
	
    write2log("Request Infos" . "-" x 50, "\n",
          Dumper($request), "\n",
          "-------------" . "-" x 50);
  }

  if ( exists($conf->{SELECT}) ) {
    my $testval0; eval  "\$testval0  = \"$conf->{SELECT}\";";
    my $matchval0; eval "\$matchval0 = \"$conf->{MATCH}\";";
    my $testre0 = qr/$matchval0/;
    if (!$testval0 or $testval0 !~ /$testre0/) {
      debug2log("SELECT $testval0 NOT MATCH $testre0");
      return $newvals;
    }
  }
  
    my $casecnt = 0; my @matched = ();
    foreach my $case (@{$conf->{CASE}}) {
        $casecnt++;
        my $testval; eval "\$testval = \"$case->{ATTRIB}\";";
        my $matchval; eval "\$matchval = \"$case->{MATCH}\";";
		next unless(defined $matchval);
        my $testre = qr/$matchval/;
        debug2log("case item $casecnt - Value to be checked: $testval ($case->{ATTRIB}) - matching value: $testre ($case->{MATCH})");
        next if $testval and $testval !~ /$testre/;
        return undef if (exists($case->{REJECT}) && $case->{REJECT} );
        push @matched, $casecnt;
        foreach my $assert ( @{$case->{ASSERT}} ) {
            foreach my $akey ( keys %$assert ) {
                my $nvkey = (
                    $akey =~ /reportname/i ? 'reportname' :
                    $akey =~ /recipient/i  ? 'XferRecipient' :
                    $akey =~ /typeofwork/i ? 'TypeOfWork' :
                    $akey =~ /workclass/i  ? 'WorkClass' :
                    $akey =~ /datetime/i   ? 'XferStartTime' :
                    $akey =~ /cinfo/i      ? 'usrcinfo' :
                    $akey
                    );
                next if exists($newvals->{$nvkey});
                my $cod = $assert->{$akey};
                $cod = '"'.$cod.'"' if $assert->{$akey} !~ /^[^\(]+\([^\)]+\)$/;
                debug2log("Setting key $nvkey eval'ing $cod");
                eval "\$newvals->{\$nvkey} = $cod;";
            }
        }
    }
 
    debug2log("New values matched in cases (", join(', ', @matched), ")");
    debug2log("----------", "-" x 60, "");
    debug2log(Dumper($newvals));
    debug2log("----------", "-" x 60, ""); 

  return $newvals;
}

sub getInputFileName { 
	my ( $LocalPathId_IN, $LocalPath, $LocalFileName ) = @_;
	my ($ptype, $fqp) = split /:\/\//, $LocalPath, 2;
	my $FileName = "$fqp/IN/$LocalFileName"; 
	return $FileName;
}

write2log("$0($$) starting");
my $worrkdir = $ARGV[0];

die "worrkdir not found" unless (-d "$worrkdir");

my $JRID = $ARGV[1];

die "JRID not found in the parameter list"  unless(defined $JRID);

my $outmess = 'SUCCESS';

if (( grep /^-dd$/i, @ARGV ) || DEBUG ){
	$main::veryverbose =1;
	write2log("MODE veryverbose ACTIVATED");
}
my $docheckxrrename;
if ( grep /^-(checkxrrename|docheckxrrename)$/i, @ARGV ){
	$docheckxrrename =1;
	write2log("MODE docheckxrrename ACTIVATED");
} 
if ( grep /^-(nocheckxrrename|ncheckxrrename)$/i, @ARGV ){
	$docheckxrrename =0;
	write2log("MODE nocheckxrrename ACTIVATED");
} 
my $doupdate;
if ( grep /^-(doupdate|update)$/i, @ARGV ){
	$doupdate =1;
	write2log("MODE doupdate ACTIVATED");
} 
if ( grep /^-(noupdate|nupdate)$/i, @ARGV ){
	$doupdate =0;
	write2log("MODE nopdate ACTIVATED");
}

my $showcinfo = 0;
if ( grep /^-(showcinfo|doshowcinfo)$/i, @ARGV ){
	$showcinfo =1;
	write2log("MODE showcinfo ACTIVATED");
} 
if ( grep /^-(noshowcinfo|nshowcinfo)$/i, @ARGV ){
	$showcinfo =0;
	write2log("MODE noshowcinfo ACTIVATED");
}


die ("Error - Some of the following  parameters are missing: -checkxrrename/-nocheckxrrename -doupdate/-noupdate -showcinfo/-noshowcinfo.") 
unless((defined $doupdate) and (defined $docheckxrrename) and (defined $showcinfo) );




my $newJobReportName;
while(1)
{
	my $jr = XReport::JobREPORT->Open($JRID, 0);
	my ($HoldDays, $JobReportName, $JobReportId, $LocalFileName, $LocalPathId_IN, $LocalPathId_OUT) = $jr->getValues(qw(HoldDays JobReportName JobReportId LocalFileName LocalPathId_IN LocalPathId_OUT)); 

	$JobReportName = '' if(!$JobReportName);
	
	if(($JobReportName !~ /^XRRENAME$/i ) && ($docheckxrrename))
	{
		$outmess = 'FAILED ATTEMP TO RENAME FOR ['.$JRID.'] - JobReportName is not equal to "XRRENAME".';
		last;
	} 
	my $LocalPath = $XReport::cfg->{LocalPath}->{$LocalPathId_IN} ;
	 
	write2log("SELECTED: $JobReportName, $JobReportId, $LocalFileName, $LocalPathId_IN, $LocalPathId_OUT, $HoldDays, $LocalPath");
	
	
	my $ijrar = $jr->get_INPUT_ARCHIVE();
	my $reportfh = $ijrar->get_INPUT_STREAM($jr); 
	
	if( ref($reportfh) ne 'XReport::Storage::IN')
	{
		write2log("Invalid input file handle type - ref(reportfh) ne 'XReport::Storage::IN'");
		$outmess = 'FAILED ATTEMP TO RENAME FOR ['.$JRID.'] - Invalid input file handle type.';
		last; 
	}
	
	write2log('CINFO FOR ['.$JRID."].= ".Dumper($reportfh->{cinfo})) if ($showcinfo or DEBUG);
	
	$newJobReportName = getJobreportnameFromXML($reportfh);
	$reportfh->Close() or die "Error in Close() - $!";
	
	unless($newJobReportName )
	{
		$outmess = 'Error - Jobreportname not determined in getJobreportnameFromXML() FOR ['.$JRID.'].';
		last;
	}
	write2log("JobReportName=\"".$JobReportName."\" - newJobReportName=\"".$newJobReportName."\"");
	$outmess.=" - JobReportName=\"".$JobReportName."\" - newJobReportName=\"".$newJobReportName."\" FOR [".$JRID.'].';
	
		
	if( $doupdate and $newJobReportName)
	{
		if($LocalPath !~ /^file:/i )
		{
			$outmess = 'FAILED ATTEMP TO RENAME FOR ['.$JRID.'] - storage not located in local disk.';
			last;
		} 
		my $InputFileName = getInputFileName( $LocalPathId_IN, $LocalPath, $LocalFileName );
		
		#deleteElab
		$jr->deleteElab();
			
		
		(my $newLocalFileName = $LocalFileName) =~ s/XRRENAME/$newJobReportName/g;   
		(my $newInputFileName = $InputFileName) =~ s/XRRENAME/$newJobReportName/g; 
		($newLocalFileName = $newLocalFileName) =~ s/$JobReportName/$newJobReportName/g;   
		($newInputFileName = $newInputFileName) =~ s/$JobReportName/$newJobReportName/g; 
		($newLocalFileName = $newLocalFileName) =~ s/\/\.$JobReportName/\/$newJobReportName\./g;   
		($newInputFileName = $newInputFileName) =~ s/\/\.$JobReportName/\/$newJobReportName\./g;  
		($newLocalFileName = $newLocalFileName) =~ s/\\\.$JobReportName/\\$newJobReportName\./g;   
		($newInputFileName = $newInputFileName) =~ s/\\\.$JobReportName/\\$newJobReportName\./g;  
		
		
		if (-f $InputFileName)
		{
			write2log("Trying to rename file \"".$InputFileName."\" to \"".$newInputFileName."\" .");
			rename($InputFileName,$newInputFileName) or die "Error in attempting to remame \"$InputFileName\" to \"$newInputFileName\" - $!.";
		}
		else
		{
			write2log("The file \"".$InputFileName."\" does not exist.");
		}
		
		if(-f $newInputFileName)
		{
			dbExecute(	" UPDATE jr                                        \n".
				" SET JobReportName = '$newJobReportName',         \n".
				" LocalFileName = '$newLocalFileName',             \n".
				" LocalPathId_OUT = '',             \n".
				" HoldDays = COALESCE(jrn.HoldDays , jr.HoldDays)  \n".
				" from tbl_JobReports jr                           \n".
				" left join tbl_JobReportNames jrn                 \n".
				" on  jrn.JobReportName = '$newJobReportName'      \n".
						" WHERE (JobReportId = $JobReportId );             \n");
			
			my $sql = 	" select *                                         \n".
						" from tbl_JobReports jr                           \n".
						" WHERE (JobReportId = $JobReportId )              \n".
						" AND JobReportName = '$newJobReportName'          \n".
						" AND LocalFileName = '$newLocalFileName'          \n"; 
			write2log("sql[$sql]");
			my $dbc = XReport::DBUtil->new();  
			my $dbr = $dbc->dbExecute( $sql ); 
			if($dbr->eof()) {
				write2log("Error in update of tbl_JobReports - sql[$sql]");
				$outmess = 'FAILED ATTEMP TO RENAME FOR ['.$JRID.']- Error in update of tbl_JobReports';
				$dbr->Close();
				last;
			}
			
			my ( $AfterUpdate_JobReportName, $AfterUpdate_LocalFileName, $AfterUpdate_HoldDays ) =
			$dbr->GetFieldsValues(qw(JobReportName   LocalFileName HoldDays ));
			$dbr->Close();
			debug2log("AfterUpdate_JobReportName[$AfterUpdate_JobReportName]");
			debug2log("AfterUpdate_LocalFileName[$AfterUpdate_LocalFileName]"); 
			debug2log("AfterUpdate_HoldDays[$AfterUpdate_HoldDays]");  
			if($AfterUpdate_JobReportName and ($AfterUpdate_JobReportName eq $newJobReportName))
			{
				write2log("UPDATE made with success: [$JobReportName -> $newJobReportName].");
				(my $xrrenameInputFileName = $newInputFileName) =~ s/$newJobReportName/XRRENAME/g; 
				if((-f $xrrenameInputFileName) and ($xrrenameInputFileName =~ /XRRENAME/))
				{
					unlink $xrrenameInputFileName or write2log("Error in deleting of file \"$xrrenameInputFileName\"");
					write2log("File \"$xrrenameInputFileName\" deleted with success.") unless (-f $xrrenameInputFileName);
				}				
				eval
				{
				
					dbExecute(	" UPDATE tbl_WorkQueue SET TypeOfWork = 1          \n". 
								" WHERE  (ExternalTableName = 'tbl_JobReports')    \n".
								" AND ExternalKey = $JobReportId                   \n".
								" AND TypeOfWork = 2                               \n".
								" ;                                                \n");

					my $sql2 = "                                           \n".
						" select * from tbl_WorkQueue                      \n".
						" WHERE  (ExternalTableName = 'tbl_JobReports')    \n".
						" AND ExternalKey = $JobReportId                   \n".
						" AND TypeOfWork = 1                               \n".
						" ;                                                \n"; 
					write2log("sql2[$sql2]"); 
					$dbr = $dbc->dbExecute( $sql2 ); 
					if($dbr->eof()) {
						write2log("No record found in tbl_WorkQueue with for ExternalKey = $JobReportId AND TypeOfWork = 2 .");
					}
					else
					{
						my ( $AfterUpdate_TypeOfWork ) = $dbr->GetFieldsValues(qw(TypeOfWork ));
						write2log("Set TypeOfWork from [2] to [$AfterUpdate_TypeOfWork] in tbl_WorkQueue with for ExternalKey = $JobReportId .");
					}
					$dbr->Close(); 
				};
				write2log("Error in UPDATE of tbl_WorkQueue: $!") if $!;
			}
			else
			{
				write2log("Error in update of tbl_JobReports - sql[$sql]");
				$outmess = 'FAILED ATTEMP TO RENAME FOR ['.$JRID.']- Error in update of tbl_JobReports'; 
			}
		}
		else
		{
			write2log( "Error in attempting to remame \"$InputFileName\" to \"$newInputFileName\". - The new file does not exist.");
			$outmess = "Error in attempting to remame";
			last;
		}
	}
	last;
}

	
write2log("$0($$) end - outmess=$outmess " );
exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);


1;
