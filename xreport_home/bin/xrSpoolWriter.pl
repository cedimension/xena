#!/usr/bin/perl -w

use strict;
use lib "$ENV{XREPORT_HOME}/perllib";

use POSIX qw(strftime);

use XReport;
use XReport::Spool;

use IO::Socket::INET;
use IO::Select;
#use Time::HiRes qw(gettimeofday);
#use Data::Dumper;

my $workdir = $ARGV[0];
my $SpoolId = $ARGV[1] || die "Unable to get JobSpoolId\n";
my $buffsize = 4096;
my $spoolds = XReport::Spool->OpenDS(JobSpoolId => $SpoolId, buffsize => $buffsize);
$spoolds->getDestInfos ( PrintDest => $spoolds->{PrintDest} ) || die "getDestInfos failed for $spoolds->{PrintDest} - $!";

my $printername = $spoolds->{PrtName};
my @prtparms = ( "PrintDuplex" => $spoolds->{PrintDuplex} );
push @prtparms, (  "PrintParameters", $spoolds->{PrintParameters} ) if $spoolds->{PrintParameters} && $spoolds->{PrintParameters} ne '.';

my $rcount = $spoolds->{RetryCount} || 0;
$spoolds->UpdateEntry(
		      XferId => $$,
		      XferDaemon => c::getValues('SrvName'),
		      XferStartTime => strftime('%Y-%m-%d %H:%M:%S', localtime),
		      RetryCount => $rcount + 1
		     );

my $sock = new IO::Socket::INET(PeerAddr => $spoolds->{PrtSrvAddress}) || die "unable to open connection to $spoolds->{PrtSrvAddress} - $! - $?";
my $sel = new IO::Select( $sock );

binmode $sock;
my $recipient = $sock->peerhost().':'.$sock->peerport();

#print join('::', gettimeofday()), " Waiting for Socket to become ready\n";
die "Connection timed out before initial send" unless $sel->can_write(15);

my $outbuff = pack("N n/a* n/a*", -s $spoolds->{DSFN}, $spoolds->{PrtName}, join("\t", @prtparms) );
$outbuff = pack("a8 n/a*", 'STRM2PRT', $outbuff);
#print "sending outbuff: ", join('--', unpack("a8 H60", $outbuff)), "\n";
my $clen = $sock->send($outbuff);
die "Error during initial write" unless $clen = length($outbuff);

my ($buff, $totbytes);
while ( my $nbytes = $spoolds->ReadDS(\$buff, $buffsize ) ) {
  while ( $buff ) {
#    print join('::', gettimeofday()), " Waiting for Socket to become ready\n";
    die "Connection timed out for data send - only $totbytes sent" unless $sel->can_write(15);
#    print join('::', gettimeofday()), " Sending ", length($buff), " bytes to peer\n";
    my $wlen = $sock->send($buff) || die "error during write - only $totbytes sent" ;
    $totbytes += $wlen;
#    print join('::', gettimeofday()), " $totbytes bytes sent to peer so far\n";
    $buff = ( $wlen < length($buff) ? substr($buff, $wlen) : '');
  }
  last if $nbytes < $buffsize;
}

die "Connection timed out waiting to recv last ACK" unless $sel->can_read(15);
$sock->recv($buff, 12);
#print join('::', gettimeofday()), " ", length($buff), " bytes received from peer\n";
my ($ctlstring, $prtbytes) = unpack("a8 N", $buff);

die "Invalid ACK from $recipient received - PKTID: $ctlstring bytes: $prtbytes buff: " . unpack("H24", $buff)
  unless $ctlstring eq 'PRTDSTRM' && $prtbytes == $totbytes;

#print join('::', gettimeofday()), "ACK from $recipient received - PKTID: $ctlstring bytes: $prtbytes buff: " . unpack("H24", $buff), "\n";
$spoolds->CloseDS(
		  Purge => 'Y', 
		  Status => $CD::stCompleted,
		  XferEndTime => strftime('%Y-%m-%d %H:%M:%S', localtime(time())),
		  XferInBytes => -s $spoolds->{DSFN},
		  XferOutBytes => $totbytes,
		  XferRecipient => $recipient
		 );
print "PROCESSING COMPLETE\n";

exit 0;
