use strict;

use XReport;
use lib($main::Application->{'XREPORT_HOME'}."/perllib");

my $worrkdir = $ARGV[0];
my $JRID = $ARGV[1];

use XReport::Util qw(:CONFIG);
use XReport::JobREPORT;

use constant ST_RECEIVED => 16;
use constant ST_QUEUED => 16;
use constant ST_INPROCESS => 17;
use constant ST_COMPLETED => 18;
use constant ST_PROCERROR => 31;

InitServer(isdaemon => 1); 
#no warn redefine;
*i::logit = sub { 
     foreach my $logln ( split /\n/, join(' ', map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_) ) {
        $main::logger->log($logln);
     }
 };
my $debgRtn = sub {
    i::logit(@_) if  ($main::debug || $main::veryverbose);
#    i::logit(join(' ', map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_)) if ($main::debug || $main::veryverbose); 
};
#$logrRtn = sub { print map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n"; };
my $logrRtn = sub { 
     i::logit(@_);
#    i::logit(join(' ', map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_)); 
};
#$main::Application->{NoApplicationLock} = 1;
my $jr = XReport::JobREPORT->Open($JRID, 1);
my $status = $jr->doSingleParserElab();

TermServer();

exit 0;
__END__
sub {
    my ($self, $page) = (shift, shift);
    my $getVars = 0;
    my $bundleslist = [];
    $U::UserVars{'$$bundles$$'} = '';
    @{U::UserVars}{qw(UserRef)} = ("RID ADD.TI GT 30.000");
    if ( 1 ) { 
        my @tvars;
    GETVARS01: { $getVars = 0;
                    if ( @tvars = unpack('a5', $page->GetRawPageRect(4,2,4,6)) ) {

                        @{U::UserVars}{'CUTVAR'} = @tvars;

                        $getVars = 1;
                    }
                    last GETVARS01 if $getVars;
                }                                                                                                                   
        if ($getVars) {

        }
    }
    $U::UserVars{'$$bundles$$'} = join(' ', @{$bundleslist} )
                                     if ( scalar(@{$bundleslist} );
    $U::UserVars{UserRef} = $page->getJobReportValues('JobReportDescr') if ( !exists($U::UserVars{UserRef}) ); 
    return $getVars;
}