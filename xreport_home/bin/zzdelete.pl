use strict 'vars';

use lib($ENV{'XREPORT_HOME'}."/perllib");

use File::Basename;

use XReport::Util;
use XReport::DBUtil;

use Win32::OLE;
use Win32::OLE::Const;

my $debug   = 0;
my $daemon = 0;
my $SrvName;

sub delReport {
  my ($JobReportName, $ReportID) = (shift, shift);

  my $dbr;

  $dbr = dbExecute(
    "SELECT LocalFileName from tbl_JobReports ".
	"where JobReportName = '$JobReportName' and JobReportID = $ReportID"
  );
  if ( $dbr->eof() ) {
    &$logrRtn("Report $JobReportName $ReportID NOT FOUND !?");
    return;
  }
  my $LocalFileName = $dbr->Fields->Item("LocalFileName")->Value();
  
  &$logrRtn("DELETING REPORT LINKS $ReportID $LocalFileName");
  $dbr = dbExecute(
    "DELETE from tbl_JobReportsElabOptions " .
    "WHERE JobReportID = $ReportID"
  );
  $dbr = dbExecute(
	"DELETE from tbl_LogicalReports " .
	"WHERE ReportName = '$JobReportName' and JobReportID = $ReportID"
  );
  $dbr = dbExecute(
    "DELETE from tbl_PhysicalReports " .
    "WHERE JobReportID = $ReportID"
  );

  $dbr = dbExecute(
   "DELETE tbl_JobReports WHERE JobReportID = $ReportID"
  );
}

my $JobReportName = shift || die("Usage delReport JobReportName ReportID..");

for (@ARGV) {
  s/^ +//;s/ +$//;
  if ( /^\d+$/ ) {
  }
  else {
    die("ReportID $_ NOT NUMERIC !?");
  }
  delReport($JobReportName, $_);
}
