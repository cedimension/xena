#!perl -w

use strict;

$| = 1;
my $oldfh = select STDERR;
$| = 1;
select $oldfh;


use XReport;
use XReport::DBUtil ();
use Data::Dumper; 
use constant EC_OK => 0;
use constant EC_USER_SHUTDOWN => 1;
use constant EC_UNKNOWN_ERROR => 2;
use constant EC_PERL_SYNTAX_ERROR => 9;
use constant EC_PERL_DIE3 => 10045;
use constant EC_PERL_DIE1 => 10061;
use constant EC_PERL_DIE2 => 255;

use constant DEBUG => 0;
$main::myName = ( split( /[\/\\]/, $0 ) )[-1];
$main::myName = ( split( /\./, $main::myName ) )[0];

sub RenameIN {
  ($main::veryverbose || DEBUG) && i::logit("$main::myName: call RenameIN()" );
  my ($jr , %args) = @_; 
  my $outmess = ""; 
  my $newReportName = ($args{'newReportName'} ? $args{'newReportName'} : 'CDAMFILE');

  my $sql = "SELECT * FROM tbl_JobReports WHERE (JobReportId = $jr)";
  my $dbc = XReport::DBUtil->new();
  my $dbr = $dbc->dbExecute($sql);
  if ( $dbr->eof() ) {
  	i::logit("$main::myName: - NO RECORD FOUND in tbl_JobReports - sql[$sql]");
  	$outmess = 'ERROR FOR ['.$jr.']- NO RECORD FOUND in tbl_JobReports';
  	return $outmess;
  }
  
  my ($JobReportName, $JobReportId, $LocalPathId_IN, $LocalPathId_OUT, $LocalFileName, $JobStatus ) =
  						$dbr->GetFieldsValues(qw(JobReportName JobReportId LocalPathId_IN LocalPathId_OUT LocalFileName Status));
  $dbr->Close();
		
		
  my $doCheckOUTPUT = (exists($args{'doCheckOUTPUT'}) ? $args{'doCheckOUTPUT'} : 1); 
  if( $doCheckOUTPUT && ( $JobStatus eq '18' ) )
  {
	i::logit("$main::myName: OUTPUT ALREADY CREATED for JobReportId[$JobReportId].Status[$JobStatus].LocalPathId_OUT[$LocalPathId_OUT]" );
	$outmess = "OUTPUT ALREADY CREATED for JobReportId[$JobReportId]" ;
	return ($outmess);
  }
  
  ($main::veryverbose || DEBUG) && i::logit("$main::myName: JobReportName[$JobReportName]" );
	
  my $sql_update = '';
  if( ($JobReportName eq '') or ($JobReportName !~ /^$newReportName$/i) ) {
		_updateDataFromSQL("BEGIN TRANSACTION REPORTADD");
		eval 
		{
			my $newLocalFileName_IN = $LocalFileName;
			if( $JobReportName eq '')
			{
				($main::veryverbose || DEBUG) && i::i::logit("$main::myName: newLocalFileName_IN(PRE)[$newLocalFileName_IN]" );
				$newLocalFileName_IN =~ s/\/\./\/$newReportName\./;
				($main::veryverbose || DEBUG) && i::logit("$main::myName: newLocalFileName_IN(POST)[$newLocalFileName_IN]" );
			}
			else
			{
				$newLocalFileName_IN =~ s/$JobReportName/$newReportName/;				
			}
			my $LocalPath_IN = $XReport::cfg->{LocalPath}->{$LocalPathId_IN};
			die "Files on PERSISTENCE STORAGE -- Cannot rename $LocalPath_IN/IN/$LocalFileName (in $LocalPathId_IN) to $newLocalFileName_IN" if($LocalPath_IN !~ /^file:\/\//i);
			$LocalPath_IN =~ s/^file:\/\///;    
			$sql_update =
				#"UPDATE tbl_JobReports SET JobReportName = '$newReportName', LocalFileName = '$newLocalFileName_IN' ".
				#"WHERE (JobReportId = $JobReportId ) ";
				"update jr                                      ".
				"SET JobReportName = '$newReportName',          ".
				"LocalFileName = '$newLocalFileName_IN',        ".
				"PendingOp = 0,                                 ".
				"HoldDays = jrn.HoldDays                        ".
				"from tbl_JobReports jr, tbl_JobReportnames jrn ".
				"where jrn.JobReportName = '$newReportName'     ".
				"AND JobReportId = $JobReportId ";         
  
  
			i::logit("$main::myName: sql=".$sql_update);
			#$outmess .= "UPDATED JobReports LocalFilename\n";
			_updateDataFromSQL($sql_update);
			i::logit("$main::myName: UPDATED JobReports LocalFilename for JobReportId[$JobReportId]" );
			my $oldLocalFileName_IN_Full = "$LocalPath_IN/IN/$LocalFileName";
			my $newLocalFileName_IN_Full = "$LocalPath_IN/IN/$newLocalFileName_IN";
			die "Cannot rename $oldLocalFileName_IN_Full to $newLocalFileName_IN_Full: $!, $^E" if(!rename($oldLocalFileName_IN_Full, $newLocalFileName_IN_Full));	
			i::logit("$main::myName:  rename($oldLocalFileName_IN_Full, $newLocalFileName_IN_Full)" );
#			if(($LocalPathId_OUT ne '') &&(!$doCheckOUTPUT))
#			{
#				my $LocalPath_OUT = $XReport::cfg->{LocalPath}->{$LocalPathId_OUT};
#				my $oldLocalFileName_OUT = $LocalFileName;
#				$oldLocalFileName_OUT =~ s/DATA.TXT.gz.tar$/OUT.#0.zip/i;
#				my $newLocalFileName_OUT = $oldLocalFileName_OUT;
#				$newLocalFileName_OUT =~ s/$JobReportName/$newReportName/;
#				die "Files on PERSISTENCE STORAGE -- Cannot rename $oldLocalFileName_OUT to $oldLocalFileName_OUT" if($LocalPath_OUT !~ /^file:\/\//i);
#				$LocalPath_OUT =~ s/^file:\/\///;   
#				my $oldLocalFileName_OUT_Full = "$LocalPath_OUT/OUT/$oldLocalFileName_OUT";
#				my $newLocalFileName_OUT_Full = "$LocalPath_OUT/OUT/$newLocalFileName_OUT";
#				i::logit("$main::myName:  rename($oldLocalFileName_OUT_Full, $newLocalFileName_OUT_Full)" );
#				
#				unless (-e $oldLocalFileName_OUT_Full && -f $oldLocalFileName_OUT_Full && -s $oldLocalFileName_OUT_Full)
#				{
#					($main::veryverbose || DEBUG) && i::logit("$main::myName: Unable to access $oldLocalFileName_OUT_Full or file emty (size:\".-s $oldLocalFileName_OUT_Full.\")");
#				}
#				else
#				{
#					eval
#					{
#						die "Cannot rename $oldLocalFileName_OUT_Full to $newLocalFileName_OUT_Full: $!, $^E" if(!rename($oldLocalFileName_OUT_Full, $newLocalFileName_OUT_Full));	
#					};
#					if($@) {
#						$outmess.= " FAILED: $@";
#						rename($newLocalFileName_IN_Full, $oldLocalFileName_IN_Full);
#						die "$@";
#					}
#				}
#
#			}
		};
		if($@) {
			$outmess.= " FAILED: $@";
			_updateDataFromSQL("ROLLBACK TRANSACTION REPORTADD");
			i::logit("$main::myName: ROLLBACK TRANSACTION REPORTADD" );
			
		} else {
			_updateDataFromSQL("COMMIT TRANSACTION REPORTADD");
			i::logit("$main::myName: COMMIT TRANSACTION REPORTADD" );
			$outmess.="SUCCESS";
		}
	}
	else {
		$outmess = "THE REPORT NAME IS ALREADY [$newReportName]";
		i::logit("$main::myName: $outmess" );
	}
	return ($outmess);
}

sub _updateDataFromSQL {
  ($main::veryverbose || DEBUG) && i::logit("$main::myName: call _updateDataFromSQL()" );
	my ($sql_update) = (shift);
  	my $dbr = XReport::DBUtil::dbExecute($sql_update);	
}

i::logit("$main::myName:  - $0($$) starting");
my $worrkdir = $ARGV[0];
my $JRID = $ARGV[1];


my $newReportName = 'CDAMFILE';
foreach my $i (0 .. $#ARGV) 
{
	if ( $ARGV[$i] =~ /^\-newReportName$/i) { # newReportName
	  $newReportName = $ARGV[$i+1];
	  ($main::veryverbose || DEBUG) && i::logit("$main::myName: newReportName=$newReportName " );
	  last;
	}
}

my $doCheckOUTPUT = 1;
if ( grep /^-doCheckOUTPUT$/i, @ARGV ) {
	$doCheckOUTPUT =1;
	($main::veryverbose || DEBUG) && i::logit("$main::myName: - MODE doCheckOUTPUT ACTIVATED");
}
elsif ( grep /^-noCheckOUTPUT$/i, @ARGV ) {
	$doCheckOUTPUT = 0;
	($main::veryverbose || DEBUG) && i::logit("$main::myName: - MODE noCheckOUTPUT ACTIVATED");
}


my $outmess = RenameIN($JRID, newReportName => $newReportName, doCheckOUTPUT => $doCheckOUTPUT );
#($main::veryverbose || DEBUG) && i::logit("$main::myName: outmess=$outmess " );
i::logit("$main::myName:  - $0($$) end - outmess=$outmess");
exit ($outmess =~ /SUCCESS/i ? EC_OK: EC_PERL_DIE2);

1;



