#!/usr/bin/perl -w

use strict;

use XML::Simple;
use XReport::DBUtil;
use Data::Dumper;

sub sqldo {
  my $sqlh = shift;
  my $sql = shift;
  #(my $sql = shift) =~ s/'(\d+)'/$1/g;
  $sql =~ s/'\$\$NULL\$\$'/NULL/g;
#  $sql =~ s/\'getdate\(\)\'/GETDATE()/ig;
  #debug2log(substr("SQL: $sql", 0, 500));
  my $result;
  eval {$result = $sqlh->Execute($sql); };
  if ($@) {
  	logIt("Error detected for ", join('::', caller())) if $@;
  	die "$@\n" if $@ && $@ !~ /^caution|Checking identity/i;
  }
  logIt("Error count detected for " . join('::', caller()) . " :" 
  		. $sqlh->Errors()->Count(). "\n" . $sqlh->Errors(0)->Description()) if  $sqlh->Errors()->Count() > 0;
  if ( $sqlh->Errors()->Count() > 0 && (my $errdesc = $sqlh->Errors(0)->Description()) !~ /^caution|Checking identity/i ) {
  	die "DB Error detected for ", join('::', caller()), ": $errdesc";
  }
  # return $result; 
  return XReport::DBUtil::RECORDSET->new($result);
}

sub loadXlsConfig {
	my $bookname = shift || 'ALL';

	my $lclcfg = { numversion => '1', 
		lastversion_suffix    => '_LAST',
		prevversion_sfxmask   => '_V%02D',
		temp_suffix           => '_$$TMP$$',
	    tblist => [ qw(JobReportNames
	             ReportNames
	             Os390PrintParameters
	             NamedReportsGroups
	             VarSetsValues
	             Folders
	             FoldersRules
	             Profiles
	             ProfilesFoldersTrees
	             Users
	             UserAliases
	             UsersProfiles) ]
	};

	my $cfgfn = "$main::Application->{XREPORT_SITECONF}\\xml\\xlsconfig.xml";
	my $xlscfg = XML::Simple::XMLin(
		$cfgfn,
		ForceArray => [qw(initrows)],
		KeyAttr    => { tables => '+name', cols => '+name', columns => '+name', sheets => '+name', books => '+name' },
		ContentKey => '-content'
	);

	my $xrdbcfg = XML::Simple::XMLin(
		"$main::Application->{XREPORT_HOME}\\bin\\xrdblayout.xml",
		ForceArray => [qw(initrows)],
		KeyAttr    => { tables => '+name', columns => '+name' },
		ContentKey => '-content'
	);

	foreach my $cfgvar ( keys %$lclcfg ) {
		$xlscfg->{spreadsheet}->{$cfgvar} = $lclcfg->{$cfgvar} unless exists($xlscfg->{spreadsheet}->{$cfgvar});
	}
	
	$xlscfg->{spreadsheet}->{tblist} = [ split(/\s+/, $xlscfg->{spreadsheet}->{tblist}) ] unless ref($xlscfg->{spreadsheet}->{tblist});

	foreach my $tblname ( @{ $xlscfg->{spreadsheet}->{tblist} } ) {
		my $initrows = $xrdbcfg->{tables}->{$tblname}->{initrows};
		my $tbcolumns = { map { $_->{name} => $_ } grep { !$_->{isidentity} && !$_->{computed} } values %{$xrdbcfg->{tables}->{$tblname}->{columns}} };

		my $keycol = [ map { $_->{name} } sort { $a->{iskey} <=> $b->{iskey} } grep { $_->{iskey} } values %{ $tbcolumns } ];
		
		my $columns = { map { $_ => { %{ $xrdbcfg->{dictionary}->{columns}->{$_} }, %{ $tbcolumns->{$_} } } } keys %$tbcolumns };
	    my $rowcols = [ keys %{$columns} ];

		@{$xlscfg->{tables}->{$tblname}}{qw(name keycol columns rowcols)} 
				= ($tblname, $keycol, $columns, $rowcols);

		addRows2Table( $xlscfg->{tables}->{$tblname}, @$initrows) if ( $initrows && scalar(@{$initrows}) );
				
		# logIt "TABLE $tblname: ", Dumper($xrdbcfg->{tables}->{$tblname}) if $initrows;

	}

	foreach my $sheetname ( keys %{ $xlscfg->{spreadsheet}->{sheets} } ) {
		my $sheetcfg = $xlscfg->{spreadsheet}->{sheets}->{$sheetname};
		$sheetcfg->{keycol} = [ 
			map { $_->{name} } sort { $a->{iskey} <=> $b->{iskey} } grep { $_->{iskey} } 
			           values %{ $sheetcfg->{cols} } ];

		$sheetcfg->{nodupcol} = ( map { $_->{name} } grep { $_->{dontdup} } values %{ $sheetcfg->{cols} })[0];
		my $gcols = [ grep { $_->{isgroup} } values %{ $sheetcfg->{cols} }];
		$sheetcfg->{groupcol} = $gcols->[0]->{name} if scalar(@$gcols);
		$sheetcfg->{chkcol} = ( map { $_->{name} } grep { $_->{required} } values %{ $sheetcfg->{cols} })[0];
		$sheetcfg->{defaultrow} =
	          { map { $_->{aliasof} => ( $xrdbcfg->{dictionary}->{columns}->{ $_->{aliasof} }->{default} || '' ) }
			  values %{ $sheetcfg->{cols} } };
	}
    
	return $xlscfg;
}

sub rowKey {
	my ($tbl, $row) = (shift, shift);
	my $keycol = $tbl->{keycol};
	return join("\t", @{$row}{@$keycol});
}

sub addRows2Table {
	my $tbl = shift;
    while ( @_ ) {
      my $inrow = shift;
      if ( $tbl->{keycol} ) {
      	my $keyval = rowKey($tbl, $inrow);
      	die "Column Key $tbl->{keycol} missing or missing value for keyed table $tbl->{name} missing - process aborted"
      		 unless defined($keyval);

        my $dfltrow = exists($tbl->{rows}->{$keyval}) ? $tbl->{rows}->{$keyval} 
      		: { map { $_ => $tbl->{columns}->{$_}->{default} } @{$tbl->{rowcols}} };

      	debug2log( "row key $keyval replaced in table") if exists($tbl->{rows}->{$keyval});
      	$tbl->{rows}->{$keyval} =
      		{ map { $_ => ( exists( $inrow->{$_} ) ? $inrow->{$_} : $dfltrow->{$_} ) }
              @{ $tbl->{rowcols} } }
      }		
      else {
      	push @{$tbl->{rows}}, { map { $_ => ( exists( $inrow->{$_} ) ? $inrow->{$_} : $tbl->{columns}->{$_}->{default} ) }
              @{ $tbl->{rowcols} } }; 
      }
	}
	return $tbl->{rows};
}

use SpreadSheet::WriteExcel;
use IO::Scalar;

sub fillSheet {
  my ($sheetcfg, $book, $sqlh) = (shift, shift, shift);
  my ($sheetname, $keycol, $groupcol, $nodupcol) = @{$sheetcfg}{qw(name keycol groupcol nodupcol)};
  (my $sql = join(' ', split( /\n/, $sheetcfg->{sqlorigin} ))) =~ s/ +/ /g;
  
  $main::ct = 0;
  my $worksheet = $book->add_worksheet($sheetname);
  my $labelformat = $book->add_format(bold => 1, bg_color => 'magenta');
  my @dataformat = ( $book->add_format(align => 'left', bg_color => 26), $book->add_format(align => 'left', bg_color => 'white') );
  my $leftalign = $book->add_format(align => 'left');
  my $vertlabel = $book->add_format(rotation => '90');
  $worksheet->keep_leading_zeros(1); # Set on

  my ($rownum, $col) = (0, 0);
  
  #my $dbr = dbExecute($sql);
  my $dbr = sqldo($sqlh, $sql);
  my $fields = $dbr->Fields(); 
  my $labels = [];
  for (0..$fields->Count()-1) {
  	my $fld =  $fields->Item($_);
    my $lbl = $fld->Name();
    my $lsize = length($lbl);
    my $fsize = ($dbr->eof() ? 0 : $fld->ActualSize());
    my $colsize = ($fsize && ($fsize > $lsize) ? $fsize : $lsize)*1.5;
    push @$labels, $lbl;
    $worksheet->set_column($_, $_, $colsize);
    $worksheet->write_string($rownum, $_, $lbl, $labelformat );
  }
  # print Dumper($labels), "\n";
  my $rows = {};
  while ( !$dbr->eof() ) {
    my $row = $dbr->GetFieldsHash(@{$labels});
	my $keyval = join("\t", map { $row->{$_} } @$keycol );
	#main::debug2log( "KEYCOL: $keycol, KEYVAL: $keyval, GROUPCOL: $groupcol, NODUPKEY: $nodupcol, ROW: ". Dumper($row));
    if ( !exists( $rows->{$keyval}) ) {
      $rows->{$keyval} = $row;
    }
    else {
      my %groups = ( map { $_ => 1 } sort (split(/;/, $rows->{$keyval}->{$groupcol}), $row->{$groupcol}) );
	  $rows->{$keyval}->{$groupcol} = join(';', sort keys %groups);
    }
  } continue { $dbr->MoveNext(); }
  
  
  my $lastnodupkey = '';
  my $fidx = 0;
  $nodupcol = $labels->[0] unless $nodupcol;
  foreach my $row ( sort { $a->{$nodupcol} cmp $b->{$nodupcol} } values %{ $rows } ) {
    $fidx = ($fidx == 0 ? 1 : 0);
    $rownum += 1; 
#    if ($lastnodupkey ne $row->{$nodupcol}) {
#      $lastnodupkey = $row->{$nodupcol};
#    }
#    else {
#      $row->{$nodupcol} = '';
#    }
    $worksheet->write_row($rownum, $col, [ @{$row}{@$labels} ], $dataformat[$fidx]);
  }
  return $rownum;
}

sub buildXLS {
  my ( $snames , $XLSref) = ( shift, shift );
  my $xlscfg = loadXlsConfig();
  open my $xlsfh, '>', $XLSref or die "unable to open \"SCALAR\" - $!";
  binmode $xlsfh;
  $snames = $xlscfg->{spreadsheet}->{books}->{ALL}->{sheetslist}
    unless ( $snames && ref($snames) && scalar(@$snames) );
  logIt("No sheet names to produce"), return unless ( $snames && ref($snames) && scalar(@$snames) );
  
#  logIt( "Start to produce " . join( '::', @$snames ));

  my $book = Spreadsheet::WriteExcel->new($xlsfh);
  
  #my $gbnum = $book->add_format(num_format => '0.0,,, "GB"');
  #my $mbnum = $book->add_format(num_format => '0.0,, "MB"');
  #my $kbnum = $book->add_format(num_format => '0.0, "KB"');
  
  my $dbc = XReport::DBUtil::getHandle('XREPORT');
  $dbc->CONNECT() if !$dbc->CONNECTED(); 
  my $sqlh = $dbc->{'dbc'};

  foreach my $sheetname (@$snames) {
    my $sheetcfg = $xlscfg->{spreadsheet}->{sheets}->{$sheetname};
#    logIt("Start to process $sheetname ");
    my $procname = $sheetcfg->{process};
    my $proc = \&fillSheet;
   	$proc = \&{$procname} if (defined(&{$procname}));
    my $rows   = &$proc( $sheetcfg, $book, $sqlh );
  }
  
  $book->close();
#  $xlsfh->close() or die "Error closing file: $!";
  return 1;
}

1;
