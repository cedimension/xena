#!/usr/bin/perl -w

use strict;

use XML::Simple;
use XReport::DBUtil;
use Data::Dumper;

sub sqldo {
  my $sqlh = shift;
  my $sql = shift;
  #(my $sql = shift) =~ s/'(\d+)'/$1/g;
  $sql =~ s/'\$\$NULL\$\$'/NULL/g;
#  $sql =~ s/\'getdate\(\)\'/GETDATE()/ig;
  #debug2log(substr("SQL: $sql", 0, 500));
  my $result;
  eval {$result = $sqlh->Execute($sql); };
  if ($@) {
  	logIt("Error detected for ", join('::', caller())) if $@;
  	die "$@\n" if $@ && $@ !~ /^caution|Checking identity/i;
  }
  logIt("Error count detected for " . join('::', caller()) . " :" 
  		. $sqlh->Errors()->Count(). "\n" . $sqlh->Errors(0)->Description()) if  $sqlh->Errors()->Count() > 0;
  if ( $sqlh->Errors()->Count() > 0 && (my $errdesc = $sqlh->Errors(0)->Description()) !~ /^caution|Checking identity/i ) {
  	die "DB Error detected for ", join('::', caller()), ": $errdesc";
  }
  # return $result; 
  return XReport::DBUtil::RECORDSET->new($result);
}

sub loadXlsConfig {
	my $bookname = shift || 'ALL';

	my $lclcfg = { numversion => '1', 
		lastversion_suffix    => '_LAST',
		prevversion_sfxmask   => '_V%02D',
		temp_suffix           => '_$$TMP$$',
	    tblist => [ qw(JobReportNames
	             ReportNames
	             Os390PrintParameters
	             NamedReportsGroups
	             VarSetsValues
	             Folders
	             FoldersRules
	             Profiles
	             ProfilesFoldersTrees
	             Users
	             UserAliases
	             UsersProfiles) ]
	};

	my $cfgfn = "$main::Application->{XREPORT_SITECONF}\\xml\\xlsconfig.xml";
	my $xlscfg = XML::Simple::XMLin(
		$cfgfn,
		ForceArray => [qw(initrows)],
		KeyAttr    => { tables => '+name', cols => '+name', columns => '+name', sheets => '+name', books => '+name' },
		ContentKey => '-content'
	);

	my $xrdbcfg = XML::Simple::XMLin(
		"$main::Application->{XREPORT_HOME}\\bin\\xrdblayout.xml",
		ForceArray => [qw(initrows)],
		KeyAttr    => { tables => '+name', columns => '+name' },
		ContentKey => '-content'
	);

	foreach my $cfgvar ( keys %$lclcfg ) {
		$xlscfg->{spreadsheet}->{$cfgvar} = $lclcfg->{$cfgvar} unless exists($xlscfg->{spreadsheet}->{$cfgvar});
	}
	
	$xlscfg->{spreadsheet}->{tblist} = [ split(/\s+/, $xlscfg->{spreadsheet}->{tblist}) ] unless ref($xlscfg->{spreadsheet}->{tblist});

	foreach my $tblname ( @{ $xlscfg->{spreadsheet}->{tblist} } ) {
		my $initrows = $xrdbcfg->{tables}->{$tblname}->{initrows};
		my $tbcolumns = { map { $_->{name} => $_ } grep { !$_->{isidentity} && !$_->{computed} } values %{$xrdbcfg->{tables}->{$tblname}->{columns}} };

		my $keycol = [ map { $_->{name} } sort { $a->{iskey} <=> $b->{iskey} } grep { $_->{iskey} } values %{ $tbcolumns } ];
		
		my $columns = { map { $_ => { %{ $xrdbcfg->{dictionary}->{columns}->{$_} }, %{ $tbcolumns->{$_} } } } keys %$tbcolumns };
	    my $rowcols = [ keys %{$columns} ];

		@{$xlscfg->{tables}->{$tblname}}{qw(name keycol columns rowcols)} 
				= ($tblname, $keycol, $columns, $rowcols);

		addRows2Table( $xlscfg->{tables}->{$tblname}, @$initrows) if ( $initrows && scalar(@{$initrows}) );
				
	    # logIt "TABLE $tblname: ", Dumper($xrdbcfg->{tables}->{$tblname}) if $initrows;

	}

	foreach my $sheetname ( keys %{ $xlscfg->{spreadsheet}->{sheets} } ) {
		my $sheetcfg = $xlscfg->{spreadsheet}->{sheets}->{$sheetname};
		$sheetcfg->{keycol} = [ 
			map { $_->{name} } sort { $a->{iskey} <=> $b->{iskey} } grep { $_->{iskey} } 
			           values %{ $sheetcfg->{cols} } ];

		$sheetcfg->{nodupcol} = ( map { $_->{name} } grep { $_->{dontdup} } values %{ $sheetcfg->{cols} })[0];
		my $gcols = [ grep { $_->{isgroup} } values %{ $sheetcfg->{cols} }];
		$sheetcfg->{groupcol} = $gcols->[0]->{name} if scalar(@$gcols);
		$sheetcfg->{chkcol} = ( map { $_->{name} } grep { $_->{required} } values %{ $sheetcfg->{cols} })[0];
		$sheetcfg->{defaultrow} =
	          { map { $_->{aliasof} => ( $xrdbcfg->{dictionary}->{columns}->{ $_->{aliasof} }->{default} || '' ) }
			  values %{ $sheetcfg->{cols} } };
	}
    
	return $xlscfg;
}

sub rowKey {
	my ($tbl, $row) = (shift, shift);
	my $keycol = $tbl->{keycol};
	return join("\t", @{$row}{@$keycol});
}

sub addRows2Table {
	my $tbl = shift;
    while ( @_ ) {
      my $inrow = shift;
      if ( $tbl->{keycol} ) {
      	my $keyval = rowKey($tbl, $inrow);
      	die "Column Key $tbl->{keycol} missing or missing value for keyed table $tbl->{name} missing - process aborted"
      		 unless defined($keyval);

        my $dfltrow = exists($tbl->{rows}->{$keyval}) ? $tbl->{rows}->{$keyval} 
      		: { map { $_ => $tbl->{columns}->{$_}->{default} } @{$tbl->{rowcols}} };

      	debug2log( "row key $keyval replaced in table") if exists($tbl->{rows}->{$keyval});
      	$tbl->{rows}->{$keyval} =
      		{ map { $_ => ( exists( $inrow->{$_} ) ? $inrow->{$_} : $dfltrow->{$_} ) }
              @{ $tbl->{rowcols} } }
      }		
      else {
      	push @{$tbl->{rows}}, { map { $_ => ( exists( $inrow->{$_} ) ? $inrow->{$_} : $tbl->{columns}->{$_}->{default} ) }
              @{ $tbl->{rowcols} } }; 
      }
	}
	return $tbl->{rows};
}

use SpreadSheet::WriteExcel;
use IO::Scalar;

sub fillSheet {
  my ($sheetcfg, $book, $sqlh) = (shift, shift, shift);
  my ($sheetname, $keycol, $groupcol, $nodupcol) = @{$sheetcfg}{qw(name keycol groupcol nodupcol)};
  (my $sql = join(' ', split( /\n/, $sheetcfg->{sqlorigin} ))) =~ s/ +/ /g;
  
  $main::ct = 0;
  my $worksheet = $book->add_worksheet($sheetname);
  my $labelformat = $book->add_format(bold => 1, bg_color => 'magenta');
  my @dataformat = ( $book->add_format(align => 'left', bg_color => 26), $book->add_format(align => 'left', bg_color => 'white') );
  my $leftalign = $book->add_format(align => 'left');
  my $vertlabel = $book->add_format(rotation => '90');
  $worksheet->keep_leading_zeros(1); # Set on

  my ($rownum, $col) = (0, 0);
  
  #my $dbr = dbExecute($sql);
  my $dbr = sqldo($sqlh, $sql);
  my $fields = $dbr->Fields(); 
  my $labels = [];
  for (0..$fields->Count()-1) {
  	my $fld =  $fields->Item($_);
    my $lbl = $fld->Name();
    my $lsize = length($lbl);
    my $fsize = ($dbr->eof() ? 0 : $fld->ActualSize());
    my $colsize = ($fsize && ($fsize > $lsize) ? $fsize : $lsize)*1.5;
    push @$labels, $lbl;
    $worksheet->set_column($_, $_, $colsize);
    $worksheet->write_string($rownum, $_, $lbl, $labelformat );
  }
  # print Dumper($labels), "\n";
  my $rows = {};
  while ( !$dbr->eof() ) {
    my $row = $dbr->GetFieldsHash(@{$labels});
	my $keyval = join("\t", map { $row->{$_} } @$keycol );
	#main::debug2log( "KEYCOL: $keycol, KEYVAL: $keyval, GROUPCOL: $groupcol, NODUPKEY: $nodupcol, ROW: ". Dumper($row));
    if ( !exists( $rows->{$keyval}) ) {
      $rows->{$keyval} = $row;
    }
    else {
      my %groups = ( map { $_ => 1 } sort (split(/;/, $rows->{$keyval}->{$groupcol}), $row->{$groupcol}) );
	  $rows->{$keyval}->{$groupcol} = join(';', sort keys %groups);
    }
  } continue { $dbr->MoveNext(); }
  
  
  my $lastnodupkey = '';
  my $fidx = 0;
  $nodupcol = $labels->[0] unless $nodupcol;
  foreach my $row ( sort { $a->{$nodupcol} cmp $b->{$nodupcol} } values %{ $rows } ) {
    $fidx = ($fidx == 0 ? 1 : 0);
    $rownum += 1; 
#    if ($lastnodupkey ne $row->{$nodupcol}) {
#      $lastnodupkey = $row->{$nodupcol};
#    }
#    else {
#      $row->{$nodupcol} = '';
#    }
    $worksheet->write_row($rownum, $col, [ @{$row}{@$labels} ], $dataformat[$fidx]);
  }
  return $rownum;
}

sub buildXLS {
  my ( $snames , $XLSref) = ( shift, shift );
  my $xlscfg = loadXlsConfig();
  open my $xlsfh, '>', $XLSref or die "unable to open \"SCALAR\" - $!";
  binmode $xlsfh;
  $snames = $xlscfg->{spreadsheet}->{books}->{ALL}->{sheetslist}
    unless ( $snames && ref($snames) && scalar(@$snames) );
  logIt("No sheet names to produce"), return unless ( $snames && ref($snames) && scalar(@$snames) );
  
#  logIt( "Start to produce " . join( '::', @$snames ));

  my $book = Spreadsheet::WriteExcel->new($xlsfh);
  
  #my $gbnum = $book->add_format(num_format => '0.0,,, "GB"');
  #my $mbnum = $book->add_format(num_format => '0.0,, "MB"');
  #my $kbnum = $book->add_format(num_format => '0.0, "KB"');
  
  my $dbc = XReport::DBUtil::getHandle('XREPORT');
  $dbc->CONNECT() if !$dbc->CONNECTED(); 
  my $sqlh = $dbc->{'dbc'};

  foreach my $sheetname (@$snames) {
    my $sheetcfg = $xlscfg->{spreadsheet}->{sheets}->{$sheetname};
#    logIt("Start to process $sheetname ");
    my $procname = $sheetcfg->{process};
    my $proc = \&fillSheet;
   	$proc = \&{$procname} if (defined(&{$procname}));
    my $rows   = &$proc( $sheetcfg, $book, $sqlh );
  }
  
  $book->close();
#  $xlsfh->close() or die "Error closing file: $!";
  return 1;
}

use SpreadSheet::ParseExcel;

sub parseSheet {
	my ( $book, $sheetconf ) = ( shift, shift );
	my $sheetName = $sheetconf->{name};

	debug2log("Parsing $sheetName");
#	my $wks_cells = $book->Worksheet($sheetName)->{Cells};
    my $wks_cells = $book->{$sheetName}->{rows};
	my ( $irow, $blanks ) = ( 0, 0 );

	my $defaultrow = $sheetconf->{defaultrow};
	my $cols       = [];
	my $rows       = [];
	my $chkcol;

	foreach my $col ( 0..$#{$book->{$sheetName}->{cols}} ) {
		my $cellval = $book->{$sheetName}->{cols}->[$col];
		if ( my $colname =
			( ( grep { uc($cellval) eq uc( $_->{name} ) } values %{ $sheetconf->{cols} } )[0] || { name => '' } )
			->{name} )
		{
			$chkcol = $col if uc( $sheetconf->{chkcol} ) eq uc($colname);
			$colname = $sheetconf->{cols}->{$colname}->{aliasof}
			  if exists( $sheetconf->{cols}->{$colname}->{aliasof} );
			push @{$cols}, $colname;
		}
		else {
			debug2log("Column $col ($cellval) skipped in $sheetName");
			push @{$cols}, "SKIP$col";
		}
	} 

	debug2log( "Parsing $sheetName chkcol: $chkcol Columns: ",
		join( "::", map { "column $_ =>" . ( $cols->[$_] || '_UNDEF_' ) } 0 .. $#{$cols} ) );

	while ( $blanks < 5 ) {
		my $cells = $wks_cells->[$irow];
        my $chkval =  $cells->[$chkcol];
		#   debug2log("Parsing $sheetName row $irow" );

		$blanks++, next if ( !defined( $chkval ) or $chkval !~ /\w+/ );
		$blanks = 0;
		my $inrow = {
			map {
				my $val = ( defined( $cells->[$_] ) ? $cells->[$_] : '' );
				$val =~ s/(?:^ +| +$)//g;
				$cols->[$_] => $val
			  } 0 .. $#{$cols}
		};
		push @{$rows},
		  {
			map { $_ => ( exists( $inrow->{$_} ) && $inrow->{$_} ? $inrow->{$_} : ( $defaultrow->{$_} || '' ) ) }
			  keys %$defaultrow
		  };
#		last if $irow > 39;
	}
	continue {
		$irow += 1;
	}
	debug2log( "end $sheetName Parsing - irow: $irow - ROWS: "
		  . scalar( @{$rows} ));# . "\n"
#		  . join( "\n", map { join( '::', %$_ ) } @$rows ) );
    $sheetconf->{_rows} = $rows;
     
	return $sheetconf;
}

sub _processReports_sheet {
	my ( $sheet, $tbls ) = ( shift, shift );
	my $rows = $sheet->{_rows};
    my $afpvars = 0;
	my $lclreportgroups = {};

	my ($irow) = (0);
	my $groups; # this will default to previous if column blank
	while ( scalar( @{$rows} ) ) {

		$irow++;
		my $inrow  = shift @{$rows};
		addRows2Table($tbls->{JobReportNames}, $inrow);
		@{$inrow}{qw(ReportName ReportDescr)} = @{$inrow}{qw(JobReportName JobReportDescr)};
		addRows2Table($tbls->{ReportNames}, $inrow);
		if ( ( exists( $inrow->{FormDef} ) && $inrow->{FormDef} !~ /^\s*|''$/ )
		   or ( exists( $inrow->{PageDef} ) && $inrow->{PageDef} !~ /^\s*|''$/ ) 
		   or ( exists( $inrow->{Chars} ) && $inrow->{Chars} !~ /^\s*|''$/ ) 
		   or ( exists( $inrow->{PrintControlFile} ) && $inrow->{PrintControlFile} !~ /^\s*|''$/ ) 
		   ) {
			addRows2Table($tbls->{Os390PrintParameters}, $inrow);
			$afpvars = 1;
		}

		 
		if ( exists( $inrow->{'_processReportGroup'} ) 
					&& ( my $colval = delete( $inrow->{'_processReportGroup'} ) ) !~ /^(?:\s*|'')$/ ) {
						$groups = [map { $_ =~ s/^\s+|\s+$//g; $_ } split /[,;]+/, $colval];
					} 
		foreach my $group ( @$groups ) {
			$lclreportgroups->{$group}->{_jrnames}->{rowKey($tbls->{JobReportNames}, $inrow)} = 1;
			$main::rprtcount++;
		}
	}

	my $tblnames = [ qw(JobReportNames ReportNames) ];
	push @$tblnames, qw(Os390PrintParameters) if $afpvars;

	foreach my $reportrule ( keys %{$lclreportgroups} ) {
		my $inrow = delete( $lclreportgroups->{$reportrule} );
		addRows2Table($tbls->{VarSetsValues}, 
			map { { VarSetName => $reportrule, VarName => ':REPORTNAME', VarValue => $_ } } keys %{$inrow->{_jrnames}}
			);
			#addRows2Table($tbls->{NamedReportsGroups}, {ReportGroupId => $reportrule, ReportRule => $reportrule} );
	}
	#push @$tblnames, qw(NamedReportsGroups VarSetsValues);
	push @$tblnames, qw(VarSetsValues);

	return $tblnames;
}

sub _processFolders_sheet {
	my ( $sheet, $tbls ) = ( shift, shift );
	my ($rows, $glblcfg) = @{$sheet}{qw(_rows _cfg)};

	my ( $lclfolders, $lcltreenodes, $lclfolderstreenodes, $lclreportsgroup ) = ( {}, {}, {}, {} );
	my ($irow)   = (0);
	my $rootname = $glblcfg->{rootname};
	while ( scalar( @{$rows} ) ) {
		$irow++;
		my $inrow  = shift @{$rows};
		my $keyval = rowKey($tbls->{Folders}, $inrow);

		my $TreeNode = [ map { $_ =~ s/^\s+|\s+$//g; $_ } split /\\+/, $keyval ];
		my $foldername = join( "\\", @$TreeNode );
		my $child      = $foldername;
		my $descrkeys  = [];
		while ( scalar(@$TreeNode) ) {
			pop @{$TreeNode};
			my $node = join( "\\", @$TreeNode );
			$lclfolders->{$child}->{ParentFolder} = ( $node || $rootname );
			push @$descrkeys, $child;

			$lcltreenodes->{$node}->{$child} = 1;
			$lclfolderstreenodes->{$child}   = $node;
			$child                           = $node;
		}

		my $TreeDescr;
		my $defaultdescr = "Report Type %s";
		if ( exists( $inrow->{'_processFoldersDescr'} )
			&& ( my $colval = delete( $inrow->{'_processFoldersDescr'} ) ) )
		{
			$TreeDescr = [ reverse map { $_ =~ s/^\s+|\s+$//g; $_ } split /\\+/, $colval ];
		}
		else {
			$TreeDescr = [ map { sprintf( $defaultdescr, $_ ) } @$descrkeys ];
		}
		foreach my $ix ( 0 .. $#{$TreeDescr} ) {
			last if $ix > $#{$descrkeys};

			my ( $node, $descr ) = ( $descrkeys->[$ix], $TreeDescr->[$ix] );
			$lclfolders->{$node}->{FolderDescr} = $descr
			  unless ( exists( $lclfolders->{$node}->{FolderDescr} )
				&& $lclfolders->{$node}->{FolderDescr} ne $descr
				&& $descr ne sprintf( $defaultdescr, $node ) );
		}

		if ( exists( $inrow->{'_processReportGroups'} )
			&& ( my $colval = delete( $inrow->{'_processReportGroups'} ) ) ) {
			foreach my $group ( map { $_ =~ s/^\s+|\s+$//g; $_ } split /[,;]+/, $colval ) {
				$lclreportsgroup->{"F${foldername}G${group}"} = {FolderName => $foldername, ReportGroupId => $group};
			}
		}
		#die "FOLDERS INROW: ", Dumper($inrow);
		@{$lclfolders->{$foldername}}{keys %$inrow} = @{$inrow}{keys %$inrow};
	}

	foreach my $foldername ( keys %{$lclfolders} ) {
		my $inrow = delete( $lclfolders->{$foldername} );
        $inrow->{FolderName} = $foldername;
        addRows2Table($tbls->{Folders}, $inrow);
	}

	addRows2Table($tbls->{FoldersRules},
		  map { delete($lclreportsgroup->{$_}) } keys %{$lclreportsgroup} );

	return [qw(Folders FoldersRules)];
}

sub _processFilterRules_sheet {
	my ( $sheet, $tbls ) = ( shift, shift );
	my $rows = $sheet->{_rows};
	my ($vargroups);
	while ( scalar( @{$rows} ) ) {
		my $inrow  = shift @{$rows};
		
		if ( exists( $inrow->{'VarSetName'} ) 
					&& ( my $colval = delete( $inrow->{'VarSetName'} ) ) !~ /^(?:\s*|'')$/ ) {
						$vargroups = [map { $_ =~ s/^\s+|\s+$//g; $_ } split /[,;]+/, $colval];
		} 
	    foreach my $vgroup ( @$vargroups ) { addRows2Table($tbls->{VarSetsValues}, { %$inrow, VarSetName => $vgroup } ); }
	}
	return [qw(VarSetsValues)];	
}

sub _processReportGroups_sheet {
	my ( $sheet, $tbls ) = ( shift, shift );
	my $rows = $sheet->{_rows};
	while ( scalar( @{$rows} ) ) {
		my $inrow  = shift @{$rows};
		next if join('', @{$inrow}{qw(ReportRule FilterRule RecipientRule)}) =~ /^\s*$/;
		addRows2Table( $tbls->{NamedReportsGroups}, $inrow );
	}
	return [qw(NamedReportsGroups)];	
}

sub _processVirtualRes_sheet {
	my ( $sheet, $tbls ) = ( shift, shift );
	my $rows = $sheet->{_rows};
	my ($vargroups);
	while ( scalar( @{$rows} ) ) {
		my $inrow  = shift @{$rows};
		
		if ( exists( $inrow->{'VarSetName'} ) 
					&& ( my $colval = delete( $inrow->{'VarSetName'} ) ) !~ /^(?:\s*|'')$/ ) {
						$vargroups = [map { $_ =~ s/^\s+|\s+$//g; $_ } split /[,;]+/, $colval];
		} 
	    foreach my $vgroup ( @$vargroups ) {
			if ( exists( $inrow->{'_processVResElements'} )
				&& ( my $colval = delete( $inrow->{'_processVResElements'} ) ) ) {
				addRows2Table($tbls->{VarSetsValues}, 
					map { { VarSetName => $vgroup, VarName => "SVC:$inrow->{VarName}", VarValue => $_ } } map { $_ =~ s/^\s+|\s+$//g; $_ } split /[,;]+/, $colval );
			}
	    }
	}
	return [qw(VarSetsValues)];	
}

sub _processProfiles_sheet {
	my ( $sheet, $tbls ) = ( shift, shift );
	my $rows = $sheet->{_rows};
	my ( $lclprofiles, $acls ) = ( {}, {} );

	my ($irow) = (0);
	my $descrkey = ( grep /descr$/i, keys %{ $tbls->{Profiles}->{columns} } )[0];

	while ( scalar( @{$rows} ) ) {

		$irow++;
		my $inrow  = shift @{$rows};
		my $keyval = rowKey($tbls->{Profiles}, $inrow);

		if ( exists( $inrow->{'_processProfilesAuthorities'} )
			&& ( my $colval = delete( $inrow->{'_processProfilesAuthorities'} ) ) )
		{
			foreach my $acl ( map { $_ =~ s/^\s+|\s+$//g; $_ } split /[,;]+/, $colval ) {
				$acls->{$keyval}->{$acl} = 1;
			}

		}
		my $descr = ( exists( $inrow->{$descrkey} ) && $inrow->{$descrkey} ? $inrow->{$descrkey} : "Profile $irow" );
		$lclprofiles->{$keyval} = { ProfileName => $keyval, $descrkey => $descr };
	}

	foreach my $profile ( keys %{$lclprofiles} ) {
		my $inrow = delete( $lclprofiles->{$profile} );
		addRows2Table($tbls->{Profiles}, $inrow);
		addRows2Table($tbls->{ProfilesFoldersTrees}, 
			map { { ProfileName => $profile, RootNode => $_ } } keys %{ $acls->{$profile} }
			) if exists( $acls->{$profile} );
	}

	return [qw(Profiles ProfilesFoldersTrees)];
}

sub _processUsers_sheet {
	my ( $sheet, $tbls ) = ( shift, shift );
	my $rows = $sheet->{_rows};

	my $UserName;
	my ( $lclusers, $lclusersaliases, $lcluserprofiles, $acls ) = ( {}, {}, {}, {} );
	my ($irow) = (0);
	while ( scalar( @{$rows} ) ) {

		$irow++;
		my $inrow  = shift @{$rows};
		my $keyval = rowKey($tbls->{UserAliases}, $inrow);

		if ( exists( $inrow->{'UserName'} )
			&& ( my $colval = $inrow->{'UserName'} ) )
		{
			$UserName = $colval;
			my $UserDescr = "User $colval";
			if ( exists( $inrow->{'UserDescr'} ) && ( my $descr = $inrow->{'UserDescr'} ) )	{
				$UserDescr = $descr;
			}

			my $mailaddr = '';
			if ( exists( $inrow->{'EMailAddr'} ) && ( my $mail = $inrow->{'EMailAddr'} ) )	{
				$mailaddr = $mail;
			}

			if ( exists( $inrow->{'_processUsersProfiles'} )
				&& ( my $list = delete( $inrow->{'_processUsersProfiles'} ) ) )
			{
				foreach my $acl ( map { $_ =~ s/^\s+|\s+$//g; $_ } split /[,;]+/, $list ) {
					$acls->{$UserName}->{$acl} = 1;
				}

			}
			$lclusers->{$UserName} = { UserName => $UserName, UserDescr => $UserDescr, EMailAddr => $mailaddr };
		}
		my $descr = (
			( exists( $inrow->{UserAliasDescr} ) && $inrow->{UserAliasDescr} )
			? $inrow->{UserAliasDescr}
			: "alias of user $UserName"
		);
		$lclusersaliases->{$keyval} = { UserAlias => $inrow->{UserAlias}, UserAliasDescr => $descr, UserName => $inrow->{UserName} };
	}

	foreach my $user ( keys %{$lclusers} ) {
		my $inrow = delete( $lclusers->{$user} );
		addRows2Table($tbls->{Users}, $inrow);
		addRows2Table($tbls->{UsersProfiles},
			map { { UserName => $user, ProfileName => $_ } } keys %{ $acls->{$user} }
		) if exists( $acls->{$user} );
	}

	addRows2Table($tbls->{UserAliases}, 
			map { delete( $lclusersaliases->{$_} ) } keys %{$lclusersaliases} 
			);

	return [qw(Users UserAliases UsersProfiles)];
}

sub xlsToHash {
    my $XLS = shift;
    my $ws = {} ;
	my $xlsparser = Spreadsheet::ParseExcel->new( 
	    CellHandler => sub {
	    	my ( $bk, $sidx, $row, $col, $cell) = @_;
	    	my $sname = $bk->worksheet($sidx)->{Name};
	    	if ($row == 0) {
	    		if ($col == 0) {
	    			$ws->{$sname} = { cols => [], rows => [] };
   	    	        $ws->{SheetCount}++;
	    		}
	    	    $ws->{$sname}->{cols}->[$col] = $cell->value();
	    	}
	    	else {
	    		$ws->{$sname}->{rows}->[$row - 1]->[$col] = $cell->value() 
	    		                        unless $col > $#{$ws->{$sname}->{cols}};
	    	}
	    	return;
	    },
        NotSetCell  => 1
    );

	my $excel  = $xlsparser->Parse($XLS);
	undef $excel;
	undef $xlsparser;
	return $ws;
} 

sub parseXLSConfig {
	my ( $selfref, $XLS, $snames ) = ( shift, shift );
	my $xlscfg = loadXlsConfig();
	$snames = $xlscfg->{spreadsheet}->{books}->{ALL}->{sheetslist}
	  unless ( $snames && ref($snames) && scalar(@$snames) );
	
	logIt("No sheet names to seek"), return unless ( $snames && ref($snames) && scalar(@$snames) );

	logIt( "Start to parse " . join( '::', @$snames ));# . "xlscfg:" . Dumper($xlscfg) );

    my $book = xlsToHash($XLS);

	if (scalar(keys %$book)) {
		logIt( "SpreadSheet Version: 0.49 " . " book keys: " . join( '::', keys %$book ) );
		debug2log(
			"book sheetcount: " . $book->{SheetCount}
			  . " book data: " . unpack( "H40", $XLS )
		);
	}
    undef $$XLS;

	if ( !$book || !scalar(keys %$book) ) {
		logIt( "Unable to parse - ref XLS: " . ref($XLS) . " ll: " . length($$XLS) );
		return undef;
	}

	my $tbls = $xlscfg->{tables};
	my $newtbls = [];
	
	foreach my $sheetname (@$snames) {
		my $sheet = $xlscfg->{spreadsheet}->{sheets}->{$sheetname};
		$sheet->{_cfg} = my $glblcfg = {map { $_ => $xlscfg->{spreadsheet}->{$_} } grep { !/^sheets|books$/} keys %{$xlscfg->{spreadsheet}} };
		logIt("Start to process $sheetname ");
		my $procname = $sheet->{process};
		my $proc     = $selfref->can($procname) || die " proc $procname not found for $sheetname";
		my $tnames   = &$proc( parseSheet( $book, $sheet ), $xlscfg->{tables} );
	  debug2log(  "$sheetname returned "
		  . join( '::', @$tnames )
		  . " - tables :\n"
		  . Dumper($glblcfg)
		  . Dumper($tnames)
#		  . Dumper( \[ map { $tbls->{$_} } @{$tnames} ] ) 
	       );
	    foreach my $tname ( @$tnames ) { push @$newtbls, $tname unless grep { $_ eq $tname } @$newtbls; }
	}

	foreach my $pkey ( keys %{$tbls->{ProfilesFoldersTrees}->{rows}} ) {
		my $en = $tbls->{ProfilesFoldersTrees}->{rows}->{$pkey};
		next if ( exists($tbls->{Folders}->{rows}->{$en->{RootNode}}));
		my $varsetentry = delete($tbls->{ProfilesFoldersTrees}->{rows}->{$pkey});
		
	}

=tables update
first we create a copy of the last version to be used as backup
then another copy in tempdb to be used for the changes, apply the
updates to the new table and eventually copy the content of the 
new tables into the current tables.
=cut

    my ($verlast, $sfxlast, $sfxtemp, $sfxmask, $tbls_to_backup) 
           = @{$xlscfg->{spreadsheet}}{qw(numversion lastversion_suffix temp_suffix prevversion_sfxmask tblist)}; 

	my $dbc = XReport::DBUtil::getHandle('XREPORT');
	$dbc->CONNECT() if !$dbc->CONNECTED();
	my $sqlh = $dbc->{'dbc'};

=rotate
the series of previous versions is shifted up by one and the oldest version is dropped
the oldest version is always version "1".  given 3 as the number of versions to keep
  V1 => DROP
  V2 rename to V1
  V3 rename to V2
  LAST rename to V3
  CURRENT copy to LAST
  TEMP if exists then DROP 
=cut
    foreach my $table ( map { "tbl_$_" } @$tbls_to_backup ) {
		if ( $verlast && $verlast =~ /^\d+/) {
			my $vsfx = sprintf($sfxmask, 1);
			sqldo($sqlh, "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[${table}${vsfx}]') AND type in (N'U')) DROP TABLE ${table}${vsfx}"); 
			return undef unless $@ =~ /(?:caution:|does not exist)/i or !$@;
			for my $pver ( map { sprintf($sfxmask, $_)} (2..$verlast) ) {
				sqldo($sqlh, "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[${table}${pver}]') AND type in (N'U')) EXEC sp_rename '${table}${pver}', '${table}${vsfx}'");
				$vsfx = $pver;	
			}
			sqldo($sqlh, "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[${table}${sfxlast}]') AND type in (N'U')) EXEC sp_rename '${table}${sfxlast}', '${table}${vsfx}'");
		}
		sqldo($sqlh, "SELECT * INTO dbo.${table}${sfxlast} FROM dbo.$table");
		sqldo($sqlh, "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[${table}${sfxtemp}]') AND type in (N'U')) DROP TABLE dbo.${table}${sfxtemp}"); 
	}
=update
temp table are created empty from the just created last version, then filled using the rows stored in memory
if a keep attribute exists in the config, then temp table will be updated using the rows of last version that
matches the keep criteria.
  LAST structure creates TEMP 
=cut	
	foreach my $table ( @$newtbls ) {
	  my $tmptab = "dbo.tbl_${table}${sfxtemp}";
	  my $lasttab = "dbo.tbl_${table}${sfxlast}";
		sqldo($sqlh, "SELECT TOP 0 * INTO $tmptab FROM $lasttab");
#		sqldo($sqlh, "TRUNCATE TABLE $tmptab");
		my ($where, $cols) = @{$tbls->{$table}}{qw(keep rowcols)};
		my $insert = "INSERT INTO $tmptab (" . join( ',', @$cols ) . ") ";
		
		sqldo($sqlh,  $insert . " SELECT " . join( ', ', @$cols ) . " FROM $lasttab WHERE $where") if $where;

		my $ic = 0; 
		my $tblrows = ( ref($tbls->{$table}->{rows}) eq 'ARRAY' ? $tbls->{$table}->{rows} 
        	:  [ map { delete($tbls->{$table}->{rows}->{$_}) } keys %{$tbls->{$table}->{rows}} ]
        	);

		logIt( "Start to load " . scalar( @{$tblrows} ) . " rows into $tmptab\n" );
		while ( scalar @{$tblrows}  ) {
			my @irows = splice( @{$tblrows}, 0, 100 );
			#logIt("Cols: ", join("\nCols: ", map { join('::', @{$_}{@$cols}) } @irows ), "\n" );
			sqldo($sqlh, $insert 
			      . " SELECT '" . join( "' UNION ALL SELECT '",
			                      map { join( "', '", map { $_ = '' if $_ eq '.'; $_ =~ s/'/''/g; $_} @{$_}{@$cols} ) } @irows ) . "'" );
			$ic += scalar(@irows);
		}
		logIt("Inserted $ic rows into $tmptab\n");
	}	
=deploy
  Truncate CURRENT
  CURRENT Re-SEEDED if REQUESTED
  content of TEMP inserted
=cut
	foreach my $table ( @$newtbls ) {
		my $tmptab = "dbo.tbl_${table}${sfxtemp}";
		my $lasttab = "dbo.tbl_${table}${sfxlast}";
		my ($clear, $reseed, $cols) = @{$tbls->{$table}}{qw(clear reseed rowcols)};
		$clear = 'TRUNCATE' unless $clear;
		debug2log( "start to process table $table (CLEAR: $clear) -  COLUMNS: ", join('::', @$cols) );
        sqldo($sqlh,  ( $clear && $clear eq 'DELETE' ? "DELETE FROM " : "TRUNCATE TABLE " ) . "dbo.tbl_$table" )
			  unless $clear && $clear eq 'SKIP';
		sqldo($sqlh, "DBCC CHECKIDENT (tbl_$table, RESEED, $reseed)") if $reseed ;
		sqldo($sqlh, "INSERT INTO dbo.tbl_$table (" . join( ',', @$cols ) . ") "
				. " SELECT " . join( ', ', @$cols ) . " FROM $tmptab"
			  );
		sqldo($sqlh, "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[${table}${sfxtemp}]') AND type in (N'U')) DROP TABLE dbo.${table}${sfxtemp}"); 
	}	

	return;

}

1;
