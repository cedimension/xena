#!perl
#######################################################################
# @(#) $Id$ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
my $VERSION = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

use strict;
#use Devel::Size qw[ total_size size ];
#$Devel::Size::warn = 0;

#use lib($ENV{'XREPORT_HOME'}."/perllib");
use XML::Simple;

use Xreport;
use XReport::Util qw($logger $logrRtn);
use XReport::SUtil;
use XReport::QUtil;
use XReport::JUtil;
use XReport::logger;
use XReport::Receiver_thread::GTWServ;

$main::globals = {};

$main::globals->{'XRDBASE'} = new XReport::JUtil(); 
if ( !$main::globals->{'XRDBASE'} ) {
	print "Unable to get XREPORT db handler\n";
	exit 1;
}

exit (XReport::Receiver_thread::processRequest());

sub Child {

  use IO::Socket;
  use IO::Select;

  my %queRtns = (
	      '.default'  => \&XRGATEWAY,
	      'LPRQUEGZ'  => \&XRGATEWAY,
	      'PASSTHROUGH'  => \&PASSTHROUGH,
	     );

  &$logrRtn("now starting child $main::SrvName $main::con");

  my $hndl = new XReport::Receiver_thread::GTWServ(
					    'port'       => $main::SrvPort,
					    'conid'      => $main::con,
					    'SrvName'    => $main::SrvName,
					    'daemonMode' => $main::daemon,
					    'debugLevel' => $main::debug,
					    'prId'       => $main::prId,
					    'XferMode'   => $CD::xmPsf,
					    'pagcnt'     => 0,
					    'logrtn'     => $logrRtn,
					   );
  
  return undef unless $hndl->Sock( $main::CliSock );
  my ($peeraddr, $peerport) = @{$hndl}{qw(peeraddr peerport)};
  $hndl->{'cinfo'}->{'H'} = $peeraddr;

  my $cmdlen = $hndl->recvcmd(my $cmd, 250);

  return 1 unless defined($cmd) and $cmdlen > 0;

  my ($req, $prtQueue) = ($cmd =~ /^(.)(.*)\x0a$/o);
  my ($reqname, $queuetype, $queuename) = $hndl->ParseQue($prtQueue);

  if ($req eq "\x03" or $req eq "\x04") {
    $hndl->sendack("receive status cmd aknowledged - processing $prtQueue");
    return 0;
  }

  if ($req ne "\x02") {
    $hndl->DoLog("Unexpected cmd received: " . unpack("H*", $cmd));
    return 1;
  }
 
  my $xfrRtn = ($queRtns{$queuetype} or $queRtns{'.default'});

  $hndl->sendack("Receive JOB cmd received - Transfer for $queuename ($queuetype) starting");
  
  my $infile = $hndl->startJobRecv($reqname);
  if ( !$infile ) {
   $hndl->DoLog("Error receiving Job control data - Aborting");
   return undef;
  }
  $hndl->DoLog("Job Control Data received ($infile) - Starting now $queuetype handler");
  
  my $ifbytes = &$xfrRtn($hndl, $reqname);
  $hndl->DoLog("Job $queuetype handler terminated - $ifbytes data bytes written");
  
  return 0;
  
}

sub PASSTHROUGH {
  my $hndl = shift;
  
  my $fhOut;
  my $CtlFile = $hndl->CtlFileN() . ".CNTRL";
  my $DtaFile = $hndl->DtaFileN() . ".DATA";

  open($fhOut, ">".$CtlFile ) || do {
    $hndl->DoLog("DATA FILE OPEN ERROR: $!");
    return undef;
  };
  binmode $fhOut;
  my $wb = syswrite($fhOut, $hndl->CtlStream() );

  my ($dfilen, $dbodir, $dbdatet) = setXferFileA(@{$hndl}{qw(queuetype queuename conid)});
   @{$hndl}{qw(dfilen dbodir dbdtime)} = ($dfilen, $dbodir, $dbdatet); 
  
  close($fhOut);
  $hndl->DoLog("N2O", "Cntl subcommands received: ", -s $CtlFile, " bytes written to\n--- $CtlFile");

  open($fhOut, ">".$DtaFile ) || do {
    $hndl->DoLog("N2O", "DATA FILE OPEN ERROR: $!");
    return undef;
  };
  binmode $fhOut;

  if ( !$hndl->sendack("Data Stream begin - from: $hndl->{'dfilen'} to: $DtaFile", pack("N", $main::con)) ) {
    setRequestErr("Start data stream request failed");
    return undef;
  }

  my $dcnt = my $oulen = my $inlen = 0;
  while ( 1 ) {

    my $bfrlen = $hndl->recvdata(my $buff);
    $dcnt += 1;
    return undef unless defined($bfrlen);
    
    last if ($bfrlen == 0);

    $inlen = $hndl->dbytes($hndl->dbytes() + $bfrlen);

    my $wb = syswrite($fhOut, $buff) || do {
      $hndl->DoLog("N2O", " Error during  write : $!");
      last;
    };

    last if $wb ne length($buff);

    $oulen += $wb;
    $hndl->sendack("Data buffer $dcnt received: $wb bytes") unless ($bfrlen == 0);

  }
  
  close($fhOut);
  $hndl->DoLog("N2O" . "Job Received - inlen: $inlen - oulen: $oulen");
  foreach my $nm ( keys %{ $hndl } ) {
    $hndl->DoLog("--- $nm:" . $hndl->{$nm}) unless ($nm eq 'cdata');
  }

  return undef unless ($oulen == $inlen);
  my $outsize = -s $DtaFile;

  $hndl->DoLog("N2O", "Data file received: $outsize bytes written to\n--- $DtaFile");

  return $outsize;

}

sub XRGATEWAY {
  ## -*- Mode: Perl -*- #################################################
  # @(#) $Id$ 
  #
  # Copyrights(c) EURISKOM s.r.l.
  #######################################################################
  my $hndl = shift; my $reqname = shift;
  my $version = do { my @r = (q$Revision: 1.9 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

  my $fhOut = $hndl->initJobReport() || return undef;
  my $reqid = $hndl->{dbid};
  if ( !$hndl->sendack("Data Stream begin - from: $hndl->{cinfo}->{'dfilen'} to: $hndl->{'dfilen'}", pack("N", $reqid)) ) {
    $hndl->setRequestErr("Start data stream request failed");
    return undef;
  }
#  my $oldverb = $main::veryverbose;
#  $main::veryverbose = 1;
  my $dcnt = my $bfrlen = my $oulen = my $inlen = 0;
  while ( 1 ) {
#    $hndl->DoLog("SOCK USAGE B4Recv: ".total_size( $hndl ));
    $bfrlen = $hndl->recvdata(my $buff); 
#    $hndl->DoLog("SOCK USAGE AFTER: ".total_size( $hndl ));
    last if !defined($bfrlen); 
    $dcnt += 1;
    
    if ( !$bfrlen ) {
      $hndl->DoLog( "RECVDATA call for Buffer $dcnt returned 0 (buff len: "
                  . length($buff)
                  . ") - total bytes written: $oulen - input bytes received: $inlen - leaving loop");
      last;
    }

    $inlen += $bfrlen;

#    $hndl->DoLog("OUTFH USAGE B4 Write: ".total_size( $fhOut ));
    my $wb = $fhOut->write($buff);
#    $hndl->DoLog("OUTFH USAGE after Write: ".total_size( $fhOut ));
    if ( !$wb || $wb != length($buff) ) {
      $bfrlen = length($buff);  
      $hndl->DoLog("N2O", " Error during write of buffer $dcnt bufflen: $bfrlen - written: $wb - info: $! - $?");
      last;
    };

    $oulen += $wb;
    $hndl->DoLog("Buffer $dcnt received ($bfrlen) - Bytes written to out handle: $wb total: $oulen - input bytes received: $inlen ")
      if ( ($inlen % 1048576 ) < (($inlen - $bfrlen) % 1048576)); 

#    $hndl->DoLog("SOCK USAGE B4Ack: ".total_size( $hndl ));
    $hndl->sendack("Data buffer $dcnt received: $wb bytes") unless ($bfrlen == 0);
#    $hndl->DoLog("SOCK USAGE AFTERAck: ".total_size( $hndl ));

  }

  $fhOut->Close();

  $hndl->outbytes($oulen);
  $hndl->dbytes($inlen);
  #$hndl->{dfilesz} = my $dfilesz = -s $fhOut->{datafile};
  my $dfilesz;
  
  $fhOut->{datafile} =~ s/[ \s]+$// ;
  if(-e $fhOut->{datafile})
  {
	$dfilesz = -s $fhOut->{datafile};
  } 
  elsif(-e $fhOut->{datafile}.'.tar' )
  {
	$dfilesz = -s $fhOut->{datafile}.'.tar'; 
  }
  else
  {
	$hndl->DoLog("File \$fhOut->{datafile} not foung[".$fhOut->{datafile}."]".__PACKAGE__.__LINE__);
  }
  $hndl->{dfilesz} = $dfilesz ;  

  if (!defined($bfrlen) or ($oulen != $inlen)) {
    unlink $fhOut->{datafile};
    $hndl->setRequestErr("receive failed for Data file: " . 
		  " recvd: "   . $inlen .
		  " output: "  . $oulen .
		  " written: " . $dfilesz );
#    $main::veryverbose = $oldverb;
    return undef;
  }

  $hndl->DoLog("Data file received - bytes " .
	" recvd: " . $inlen .
	" written: " . $dfilesz );
  
  my $okreq = $hndl->setRequestOK(
				   $CD::stReceived,
				   $hndl->{dbodir}."/".$hndl->{dfilen}.".CNTRL.TXT",
				   $fhOut->{datafile},
				   $reqid,
				   $hndl->CtlStream() );
  if ( !defined($okreq) ) {
     $hndl->DoLog("DataBase update failed for $reqid - receive aborted");
#     $main::veryverbose = $oldverb;
     return undef;
  }
  
  my $jcrc = $hndl->{JCH}->JCAdd($reqid, $hndl->CtlStream());
  if ( !defined($jcrc) ) {
     $hndl->DoLog("DataBase JOBCTL ADD failed for $reqid - receive aborted");
#     $main::veryverbose = $oldverb;
     return undef;
  }
  
  #$hndl->DoLog("Sending last ack to client: $hndl->{reportname}");
  #$hndl->sendack("GATEWAY Stream receive completed correctly for $hndl->{cinfo}->{reqname}", $hndl->{'reportname'});
  $hndl->DoLog("Sending last ack to client: $hndl->{cinfo}->{reportname}");  
  $hndl->sendack("GATEWAY Stream receive completed correctly for $hndl->{cinfo}->{reqname}", $hndl->{cinfo}->{reportname});
#  $main::veryverbose = $oldverb;
  $hndl->DoLog("GATEWAY Stream receive completed correctly for $hndl->{cinfo}->{reqname} - bytes: $dfilesz");

  
  return $dfilesz;
  
}

;
