

use strict;

$| = 1;
my $oldfh = select STDERR;
$| = 1;
select $oldfh;


use XReport;
use XReport::DBUtil ();
use XReport::JobREPORT;
use Data::Dumper;
#use XReport::JobREPORT ();


#use XReport::ARCHIVE::Centera ();
#use Data::Dumper; 

use constant DEBUG => 0;
use constant EC_OK => 0;
use constant EC_USER_SHUTDOWN => 1;
use constant EC_UNKNOWN_ERROR => 2;
use constant EC_PERL_SYNTAX_ERROR => 9;
use constant EC_PERL_DIE3 => 10045;
use constant EC_PERL_DIE1 => 10061;
use constant EC_PERL_DIE2 => 255;

use constant TOW_INPUT_TO_MIGRATE => 666;
use constant TOW_OUTPUT_TO_MIGRATE => 888;
use constant WC_TO_MIGRATE => 126;
use constant TOW_TO_DELETE => 999;
use constant WC_TO_DELETE => 748;
use constant MIG_DAYS_THRESHOLD => 62;
use constant DEFAULT_DAYS_TO_WAIT_TO_MIGRATE => 30;
use constant DEFAULT_DAYS_TO_WAIT_TO_DELETE => 30;




use constant ST_RECEIVED => 16;
use constant ST_QUEUED => 16;
use constant ST_INPROCESS => 17;
use constant ST_COMPLETED => 18;
use constant ST_PROCERROR => 31;


$main::myName = ( split( /[\/\\]/, $0 ) )[-1];
$main::myName = ( split( /\./, $main::myName ) )[0];


sub QueueForElab {
	my $outmess = 'SUCCESS';
	($main::veryverbose || DEBUG) and i::logit("$main::myName: call QueueForElab()");
	#my ($ExternalTableName, $JobReportId, $TypeOfWork, $lpTarget, %args) = @_;
	#my ($ExternalTableName, $JobReportId, $TypeOfWork, $lpTarget, $daysToWait, $WorkClass,$LocalPathId_IN, %args) = @_;
	my ($JobReportId, %args) = @_; 
	my ($ExternalTableName, $TypeOfWork, $lpTarget, $daysToWait, $WorkClass,$LocalPathId_IN, $JobReportName) 
	   = 
	@args{qw(ExternalTableName TypeOfWork lpTarget daysToWait WorkClass LocalPathId_IN JobReportName)}; 
	
	($main::veryverbose || DEBUG) and i::logit("$main::myName: call QueueForElab($ExternalTableName, $JobReportId, $TypeOfWork, $lpTarget, $daysToWait, $WorkClass,$LocalPathId_IN)");
	
	#check previous input migration
	if($TypeOfWork == TOW_INPUT_TO_MIGRATE)
	{
		#my $sql = "SELECT * FROM tbl_JobReports WHERE (JobReportId = $JobReportId)";
		#my $dbc = XReport::DBUtil->new();
		#my $dbr = $dbc->dbExecute($sql);
		#if ( $dbr->eof() ) {
		#	i::logit("$main::myName: - NO RECORD FOUND in tbl_JobReports - sql[$sql]");
		#	$outmess = 'ERROR FOR ['.$JobReportId.']- NO RECORD FOUND in tbl_JobReports';
		#	return $outmess;
		#}
		#my ( $LocalFileName, $LocalPathId_IN, $LocalPathId_OUT, $HoldDaysOLD ) =
        #                        $dbr->GetFieldsValues(qw(LocalFileName LocalPathId_IN LocalPathId_OUT HoldDays));
		#$dbr->Close();
		if (($LocalPathId_IN eq $lpTarget) )
		{
			i::logit($main::myName.': INPUT FOR ['.$JobReportId.'] - ALREADY MIGRATED in CENTERA('.$lpTarget.') ' );
			return $outmess;
		}
	}
	
	my $sql =
	"\n IF EXISTS(SELECT * FROM tbl_WorkQueue WHERE ExternalTableName ='$ExternalTableName' AND TypeOfWork = $TypeOfWork AND ExternalKey = $JobReportId) " .
	"\n UPDATE tbl_WorkQueue Set ExternalTableName='$ExternalTableName', ExternalKey=$JobReportId, ".
#	"\n InsertTime=GETDATE(), TypeOfWork=$TypeOfWork, WorkClass=NULL, Status=".ST_QUEUED.", SrvParameters='$lpTarget' ".
	"\n InsertTime=dateadd(day, $daysToWait,GETDATE()), TypeOfWork=$TypeOfWork, WorkClass=$WorkClass, Status=".ST_QUEUED.", SrvParameters='$lpTarget' ".
	"\n WHERE (ExternalTableName ='$ExternalTableName' AND TypeOfWork = $TypeOfWork AND ExternalKey = $JobReportId) ".
	"\n ELSE ".
	"\n INSERT into tbl_WorkQueue " .
	"\n (ExternalTableName, ExternalKey, InsertTime, TypeOfWork, WorkClass, Status, SrvParameters) " .
	"\n VALUES " .
#	"\n ('$ExternalTableName', $JobReportId, GETDATE(), $TypeOfWork, NULL, ".ST_QUEUED.", '$lpTarget') "
	"\n ('$ExternalTableName', $JobReportId, dateadd(day, $daysToWait,GETDATE()), $TypeOfWork, $WorkClass, ".ST_QUEUED.", '$lpTarget') ".
	"\n ;"
	;
	
	#($main::veryverbose || DEBUG) and i::logit("$main::myName: call QueueForElab() - sql: $sql");
	i::logit("$main::myName: call QueueForElab() - sql: $sql");
	
	eval { XReport::DBUtil::dbExecute($sql); };
	if ($@ )
	{
		$outmess = 'ERROR IN INSERT into tbl_WorkQueue - ERROR: $@';
		i::logit("$main::myName: call QueueForElab() - ERROR: $@");
	}
	
	$sql =	"\n SELECT * FROM tbl_WorkQueue ".
			"\n WHERE (ExternalTableName ='$ExternalTableName' AND TypeOfWork = $TypeOfWork AND ExternalKey = $JobReportId) ".
			"\n ;";
	my $dbr = XReport::DBUtil::dbExecute($sql);  
	if($dbr->eof())
	{
		$dbr->Close();
		$outmess = 'ERROR IN INSERT into tbl_WorkQueue - ERROR: NO RECORD INSERTED';
		i::logit("$main::myName: call QueueForElab() - ERROR: NO RECORD INSERTED"); 
	}
	i::logit("$main::myName: QueueForElab() - Record inserted into tbl_WorkQueue for $JobReportName/$JobReportId:".join('::',$dbr->GetFieldsValues(qw(ExternalTableName ExternalKey InsertTime TypeOfWork WorkClass Status SrvParameters))));
	$dbr->Close();
	
	
	return $outmess;
}


sub getInfo {
	($main::veryverbose || DEBUG) and i::logit("$main::myName: call getInfo()");
	my ($JobReportId, %args) = @_;
	my $lpTarget = XReport::Util::getConfValues('LPTARGET') or undef;
	my $confdir = $XReport::cfg->{confdir};
	$confdir = $XReport::cfg->{userlib}."/rptconf" unless $confdir && -d $confdir;
	my $jr = XReport::JobREPORT->Open($JobReportId);
	#my ($ParseFileName,$JobReportName)= (split( /[\s,]+/ , $jr->getValues(qw(ParseFileName JobReportName))))[-1];
	my ($ParseFileName,$JobReportName,$HoldDays,$LocalPathId_IN)= (map { (split /[\s,]+/, $_)[-1] } $jr->getValues(qw(ParseFileName JobReportName HoldDays LocalPathId_IN)));

	my $rptconfXML = $confdir.'/'.$ParseFileName;

	($main::veryverbose || DEBUG) and ($lpTarget) and i::logit("$main::myName: - lpTarget(from xreport.cfg)=[$lpTarget]");
	if(-e $rptconfXML)
	{
		my $cfg_jobreport_rptconf = undef;
		$cfg_jobreport_rptconf =  XML::Simple::XMLin($rptconfXML);
		$lpTarget = $cfg_jobreport_rptconf->{'LPTARGET'} if( exists($cfg_jobreport_rptconf->{LPTARGET}) && $cfg_jobreport_rptconf->{LPTARGET});
		($main::veryverbose || DEBUG) and (exists($cfg_jobreport_rptconf->{LPTARGET})) and i::logit("$main::myName: - lpTarget(from rptconfXML)=[$lpTarget]");
		($main::veryverbose || DEBUG) and i::logit("$main::myName: - rptconfXML=[$rptconfXML] - cfg_jobreport_rptconf:".Dumper($cfg_jobreport_rptconf));
	}

	return ($lpTarget, $JobReportName,$HoldDays,$LocalPathId_IN);
}

i::logit("$main::myName: - $0($$) starting");
my $worrkdir = $ARGV[0];
my $JRID = $ARGV[1];
my $outmess = 'SUCCESS';

if (! -d $worrkdir ) { 
	$outmess ="ERROR"; 
	i::logit("$main::myName: - $0($$) end - outmess=$outmess - Invalid worrkdir:$worrkdir." );
	exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);
}

if ($JRID !~ /^[0-9]+$/i) { 
	$outmess ="ERROR"; 
	i::logit("$main::myName: - $0($$) end - outmess=$outmess - Invalid JRID:$JRID." );
	exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);
}

	

if (( grep /^-dd$/i, @ARGV ) || DEBUG ){
	$main::veryverbose =1;
	($main::veryverbose || DEBUG) and i::logit("$main::myName: - MODE veryverbose ACTIVATED");
}

my $doMigrateInput = 0;
if ( grep /^-doMigrateInput$/i, @ARGV ) {
	$doMigrateInput =1;
	($main::veryverbose || DEBUG) and i::logit("$main::myName: - MODE doMigrateInput ACTIVATED");
}
my $doMigrateOutput = 0;
if ( grep /^-doMigrateOutput$/i, @ARGV ) {
	$doMigrateOutput =1;
	($main::veryverbose || DEBUG) and i::logit("$main::myName: - MODE doMigrateOutput ACTIVATED");
}

my $nocheckHoldDays = 0;
if ( grep /^-nocheckHoldDays$/i, @ARGV ) {
	$nocheckHoldDays =1;
	($main::veryverbose || DEBUG) and i::logit("$main::myName: - MODE nocheckHoldDays ACTIVATED");
}
unless ( grep /^-(doMigrateOutput|doMigrateInput)$/i, @ARGV ) {
	$outmess ="ERROR - Both the parameters -doMigrateInput/-doMigrateOutput are missing. Check the configuration."; 
	i::logit("$main::myName: - $0($$) end outmess=$outmess");
	exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);
}


my $ExternalTableName ='MIGRATION';
foreach my $i (0 .. $#ARGV) 
{
	if (uc($ARGV[$i]) =~ /^\-ExternalTableName$/i) { 
	  $ExternalTableName = $ARGV[$i+1];
	  last;
	}
}

my $daysToWaitToMigrate = DEFAULT_DAYS_TO_WAIT_TO_MIGRATE ;
foreach my $i (0 .. $#ARGV) 
{
	if (uc($ARGV[$i]) =~ /^\-daysToWaitToMigrate$/i) { 
	  $daysToWaitToMigrate = $ARGV[$i+1];
	  last;
	}
}
my $daysToWaitToDelete = DEFAULT_DAYS_TO_WAIT_TO_DELETE ;
foreach my $i (0 .. $#ARGV) 
{
	if (uc($ARGV[$i]) =~ /^\-daysToWaitToDelete$/i) { 
	  $daysToWaitToDelete = $ARGV[$i+1];
	  last;
	}
}
foreach my $key (qw(daysToWaitToMigrate daysToWaitToDelete))
{
	my $value =eval('$'.$key);
	if ($value !~ /^[0-9]+$/i) { 
		$outmess ="ERROR"; 
		i::logit("$main::myName: - $0($$) end - outmess=$outmess - The parameter -$key is missing or wrong for $JRID, NO migration to do" );
		exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);
	}
}


#my ($lpTarget, $JobReportName) = getInfo($JRID); 
my ($lpTarget, $JobReportName,$HoldDays,$LocalPathId_IN) = getInfo($JRID);

i::logit($main::myName.": after getInfo:$JRID,$lpTarget, $JobReportName,$HoldDays,$LocalPathId_IN" );


if(!$lpTarget)
{
	$outmess ="ERROR"; 
	#$outmess ="SUCCESS"; 
	i::logit("$main::myName: - $0($$) end - outmess=$outmess - The parameter LPTARGET is missing in the configuration for $JobReportName, NO migration to do" );
	exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);
}


my $TypeOfWork = TOW_INPUT_TO_MIGRATE;
my $WorkClass = WC_TO_MIGRATE;
my $daysToWait = $daysToWaitToMigrate;

if($doMigrateOutput)
{
	if ($nocheckHoldDays or ($HoldDays > MIG_DAYS_THRESHOLD))
	{
		$TypeOfWork = TOW_OUTPUT_TO_MIGRATE; 
		$WorkClass = WC_TO_MIGRATE;
		$daysToWait = $daysToWaitToMigrate;	
	}
	else
	{	
		$TypeOfWork = TOW_TO_DELETE; 
		$WorkClass = WC_TO_DELETE;
		$daysToWait = $daysToWaitToDelete + $HoldDays;
	}
}
 
#$outmess=QueueForElab( $ExternalTableName, $JRID , ($doMigrateOutput ? TOW_OUTPUT_TO_MIGRATE : TOW_INPUT_TO_MIGRATE ), $lpTarget, $daysToWait);
$outmess = QueueForElab( $JRID
	   , ExternalTableName => $ExternalTableName
	   , JRID => $JRID
	   , TypeOfWork => $TypeOfWork
	   , WorkClass => $WorkClass
	   , LocalPathId_IN => $LocalPathId_IN
	   , lpTarget => $lpTarget
	   , daysToWait => $daysToWait 
	   , JobReportName => $JobReportName  );

i::logit("$main::myName: - $0($$) end outmess=$outmess");
exit ($outmess =~ /^SUCCESS$/i ? EC_OK: EC_PERL_DIE2);

1;