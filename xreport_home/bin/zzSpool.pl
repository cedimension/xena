#!/usr/bin/perl -w

use strict;
use lib "$ENV{XREPORT_HOME}/perllib";

use POSIX qw(strftime);

use XReport;
use XReport::Spool;

my $workid = XReport::Spool
   ->AddFile(
	     JobReportId => 106109,
	     TypeOfWork => 27,
	     InputFileName => "xrSpoolWriter.pl",
	     PrintDest => "A2E3015",
	     SrvParameters => join("\t", (type => 'PLT__VPM'
                                     , status => 'RELEASED',
                                     , doct => 'DRAWINGS'
                                     , group => 'catia'
                                     , model => 'EA6411T502   01 -A- 1        R'
                                     , library => 'RELEASED_EH101'
                                     , line => 'EH101'
                                     , issue => 'A'
                                     , sheet => '1'
                                     , drawnum => 'EA6411T502'
                                     , username => 'Administrator'
                                     , mailto => ''
                              ) ) );



exit 0;
