#!/usr/bin/perl -w

use lib "$ENV{XREPORT_HOME}/perllib";
use strict "vars";

use File::Basename;

use Data::Dumper;

use XReport;
use XReport::Util;
use XReport::DBUtil;
use XReport::JobREPORT;

my $workdir = $ARGV[0];
my $JRID = $ARGV[1] || die "Unable to get JobReportId\n";

$logrRtn = sub { print @_, "\n"; };

my $jr = XReport::JobREPORT->Open($JRID, 1);
my ($lclfileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName, $UserTimeRef) =
    $jr->getValues(qw(LocalFileName SrvName RemoteHostAddr XferMode
                      TargetLocalPathId_IN XferDaemon JobReportName XferEndTime));

my $manfqn = $workdir.'/'. basename($jr->getFileName('IXFILE', '$$MANIFEST$$'));
my $manifest = new FileHandle("<$manfqn");


while (<$manifest>) {
  chomp;
  my ($TableName, $filename, $TotEntries) = split /\t/, $_; 
  &$logrRtn("DELETING INDEX ENTRIES from \"$TableName\" ");
  dbExecute(
	    "DELETE from tbl_JobReportsIndexTables where JobReportId = $JRID AND IndexName = '$TableName'"
	   );
  my $dbc = XReport::DBUtil->get_ix_dbc($TableName);
  $dbc->dbExecute(
		  "DELETE from tbl_IDX_$TableName where JobReportId = $JRID"
		 );  
  dbExecute(
   "INSERT INTO tbl_JobReportsIndexTables (IndexName, JobReportId, UserTimeRef, TotEntries)
    VALUES ('$TableName', $JRID, '$UserTimeRef', $TotEntries)
   "
  );
  print "now starting to load INDEX $TableName from $filename ($TotEntries Entries)\n";
  my $ixfh = new FileHandle("<$workdir/$filename") or die "Unable to open \"$workdir/$filename\" - $!";
  my @ixcols = ();
  my $info = {};
  while ( <$ixfh> ) {
    chomp;
    @ixcols = grep(!/^type$/, split(/\t/, $_)), next unless scalar(@ixcols);
    @{$info}{@ixcols} = split(/\t/, $_), last;
  }

  my $dbr = $dbc->dbExecuteForUpdate("SELECT * from tbl_idx_$TableName"
				     . " WHERE drawnum = '$info->{drawnum}'"
				     . " AND sheet = '$info->{sheet}'"
				     . " AND issue = '$info->{issue}'"
				    );
  my $OLDJRID;
  print "(updat/insert)ing row - columns: " . join('::', @ixcols), "\n";
  print "(updat/insert)ing row - " . join('::', %$info), "\n";
  if ( $dbr->eof() ) {
    $dbr->AddNew();
  }
  else {
    $OLDJRID = $dbr->Fields()->{JobReportID}->Value();
  }
  foreach my $fld ( @ixcols ) { $dbr->Fields()->{$fld} = $info->{$fld}; };
  $dbr->Update();
  $dbr->Close();
  if ( $OLDJRID && $OLDJRID != $JRID ) {
    dbExecute("UPDATE tbl_JobReports set DeletionMark = 1 where JobReportID = $OLDJRID");
  }


}

exit;

