#!/usr/bin/perl -w
#######################################################################
# @(#) $Id: xrFtpServ.pl 2196 2008-03-10 09:55:02Z mpezzi $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################

use strict "vars";

use POSIX qw(strftime);
use FileHandle;
use File::Basename;

use File::Path;
use File::Copy 'cp';
use Compress::Zlib;
use XML::Simple;
use Digest::SHA1;
use Data::Dumper;

use XReport;
use XReport::Util;
use XReport::DBUtil;
use XReport::JobREPORT;
use XReport::PDF::DOC; 
use XReport::Archive;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

$main::workdir = $ARGV[0];
my $JRID = $ARGV[1] || die "Unable to get JobReportId\n";
my $ArchiveContentFN = '_ArchiveContent.tsv';

if (-d $main::workdir ) {
#  $main::workdir =~ s/\//\\/sig;
  my @flist = glob $main::workdir.'\*'; 
  i::logit( "deleting now\n", join("\n", @flist));
  unlink @flist;
}

my $debgRtn = sub { warn map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n"; };
$logrRtn = sub { print map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n"; };

sub buildParser {
  my ($outvars, $invars) = (shift, shift);
  
  my $outxform = { ($outvars =~ /(\S+)(?:\s+(\w+)\.){0,1}/osgi)  };
  my $inxform = { ($invars =~ /(\S+)(?:\s+(\w+)\.){0,1}/osgi)  };
  # &$debgRtn("outxform:\n", Dumper($outxform)); 
  # &$debgRtn("inxform:\n", Dumper($inxform)); 
  require XReport::INFORMAT if scalar(keys %$inxform);
#  require XReport::OUTFORMAT if scalar(keys %$outxform);

  my $outlist = [grep !/\.$/, split(/\s+/, $outvars) ];
  my $inlist = [grep !/\.$/, split(/\s+/, $invars) ];
  
  my $code = "sub { my \$input = shift; return {\n";
  for my $j ( 0..$#{$outlist} ) {
    
    my ($outvarn, $invarn) = ($outlist->[$j], $inlist->[$j]);
    &$debgRtn("Checking $outvarn => $invarn");
    my $invalue = ($invarn =~ /^#(.*)?#$/ ? "'$1'" :
         $inxform->{$invarn} && XReport::INFORMAT->can($inxform->{$invarn}) ? 
           'XReport::INFORMAT::'."$inxform->{$invarn}(\\\$input->{$invarn})" : 
           "\$input->{$invarn}");
    $code .= $outvarn . ' => ' . ($invarn =~ /^#(.*)?#$/ ? $invalue . ",\n" : 
                  ($outxform->{$outvarn} && XReport::OUTFORMAT->can($outxform->{$outvarn}) ? 
                   'XReport::OUTFORMAT->'. "$outxform->{$outvarn}($invalue)" : $invalue
                  )
                  . ",\n"   # evita blank per 0 (zero)
#                 . " || '',\n" 
                 ); 
  }
  $code .= "};\n}";
  #&$debgRtn("CODE:\n", $code); 
  eval($code);
  if ($@) {
    i::logit("CODE:\n", $code); 
    die "Error during building of parsing routine - $@";
  }
  return (eval($code), @$outlist);
}

use Data::Dumper;

sub extractArchFile {
  my ($arch, $fn, $partnum, $FromPage) = @_ ; 
  die "Unable to find $fn - hdrs:", Dumper($main::hdrs) unless exists($main::hdrs->{$fn});
  my $hdr = $main::hdrs->{$fn};
  my $FQoutName =  $main::workdir.'/'. basename($main::jr->getFileName( 'DATAOUT', $partnum + 1 ));
  my $outfh = IO::File->new($FQoutName, 'w');
  $outfh->binmode();
  
  $arch->seek($hdr->{ttr}, 0);
  my $bytes = 0;
  my $bytes2read = (16000 < $hdr->{size} ? 16000 : $hdr->{size});
  while ( $bytes < $hdr->{size} ) {
    $arch->read(my $buff, $bytes2read);
    last unless $buff;
    $buff = substr($buff, 0, ($hdr->{size} - $bytes)) if (( $bytes + length($buff)) > $hdr->{size} );
    print $outfh $buff;
    $bytes += length($buff);  
  }
  $outfh->close();
  return [[$FQoutName, $FromPage + 1, -1]];
}

sub extractArchPdfFile {
  my ($arch, $fn, $partnum, $FromPage) = @_ ; 
  die "Unable to find $fn" unless exists($main::hdrs->{$fn});
  my $hdr = $main::hdrs->{$fn};
  my $FQoutName =  $main::workdir.'/'. basename($main::jr->getFileName( 'PDFOUT', $partnum +1));
  my $outfh = IO::File->new($FQoutName, 'w');
  $outfh->binmode();
  
  $arch->seek($hdr->{ttr}, 0);
  my $bytes = 0;
  my $bytes2read = (16000 < $hdr->{size} ? 16000 : $hdr->{size});
  while ( $bytes < $hdr->{size} ) {
    $arch->read(my $buff, $bytes2read);
    last unless $buff;
    $buff = substr($buff, 0, ($hdr->{size} - $bytes)) if (( $bytes + length($buff)) > $hdr->{size} );
    print $outfh $buff;
    $bytes += length($buff);  
  }
  $outfh->close();
  
  my $doc = XReport::PDF::DOC->Open($FQoutName);
  my $totPages = $doc->TotPages();
  $doc->Close();
  
  return [[$FQoutName, $FromPage + 1, $totPages]];
}

use PDF::Create;
use POSIX qw();

sub convertArchFile2Pdf {
  my ($arch, $fn, $partnum, $FromPage) = @_ ; 
  die "Unable to find $fn" unless exists($main::hdrs->{$fn});
  my $hdr = $main::hdrs->{$fn};
  my $sz = $hdr->{size};
  $arch->seek($hdr->{ttr}, 0);
  my $hasindexes = $main::hdrs->{cinfo}->{HasIndexes};
  my @datarif = reverse split /[-.\s\:]/, substr($main::hdrs->{cinfo}->{TRANSFER_DATE}, 0, 19);
  $datarif[-2] -= 1; $datarif[-1] -= 1900;   
  my $FQoutName;
    
  my $bytes = 0;
  my $bytes2read = (16000 < $sz ? 16000 : $sz);
  my $pagebuff = '';
  my $pagelimit = ($hasindexes ? 3000 : 999999999);
  my $files = [];
  my $pagenum = $FromPage;
  my ($pdf, $f1);
  while ( 1 ) {
    $arch->read(my $buff, $bytes2read);
    $buff = substr($buff, 0, ($sz - $bytes)) if (( $bytes + length($buff)) > $sz );
    $bytes += length($buff);  
    last unless length($pagebuff.$buff);
    my @pages = split /\x0c/, $pagebuff.$buff;
    $pagebuff = ( $bytes < $sz && scalar(@pages) > 1 ? pop @pages : '');
    while ( scalar(@pages) ) {
        my $maxl = 0;
        my @lines = map { my $l = length($_); s/\s+$//g if $l > 250; $l = length($_); $maxl = $l if $l > $maxl; $_ } 
                                                                                              split /\n/, shift @pages;
        while ( scalar(@lines) ) {
           if ( 0 == ($pagenum % $pagelimit)  ) {
              # initialize PDF
              if ( $FQoutName ) {
                 $pdf->close();
                 my $doc = XReport::PDF::DOC->Open($FQoutName);
                 $doc->Close();
                 &$debgRtn("Convert Closing Pdf  $FQoutName,-- $FromPage, -- $pagenum"); 
                 push @{$files}, [ $FQoutName, $FromPage, $pagenum ];
                 $FromPage = 0; 
              }
              $FQoutName =  $main::workdir.'/'
                            . basename($main::jr->getFileName( 'PDFOUT', scalar(@{$files}) + $partnum + 1 ));
              $pdf = PDF::Create->new('filename'     => $FQoutName,
                                      'Author'       => 'xrArchLoader',
                                         'Title'        => ($main::hdrs->{cinfo}->{UserRef} || ''),
                                         'CreationDate' => \@datarif  );
              $f1 = $pdf->font('BaseFont' => 'Courier');
           } 
           $pagenum += 1;
           $FromPage = $pagenum unless $FromPage;
           my ($fntsz, $mb, $lpp);
           # A4L [ 842, 595] F12 -> (595/12 = 49) -> LPP: 44; F10 -> ( 595/10 = 59,5) -> LPP: 58
           #                 F9 -> (595/9 = 66) -> LPP: 66; F8 -> (595/8 = 74) -> LPP: 72
           # A3L [1190, 842] F8  -> (842/8 = 105,25) -> LPP: 104
           # A4  [ 595, 842] F12 -> (842/12 = 70) -> LPP: 69; F10 -> ( 842/10 = 84,2) -> LPP: 83
           #                 F9 -> (842/9 = 93) -> LPP: 92; F8 -> (842/8 = 105,25) -> LPP: 104
           $fntsz = 10;
           if    ( $maxl < 99 ) { $mb = [0, 0,  595, 842]; $lpp = 83; }
           elsif ( $maxl < 141) { $mb = [0, 0,  842, 595]; $lpp = 58; }
           elsif ( $maxl < 197) { $mb = [0, 0, 1190, 842]; $lpp = 83; }
           else                 { $mb = [0, 0, int($maxl * 599 * $fntsz / 1000)];
                                  $mb->[3] = int($mb->[2] * 595 / 842); 
                                  $lpp = int($mb->[3] / $fntsz) - 2;
                                }
           if ($lpp == 58 && scalar(@lines) > $lpp ) { $mb = [0, 0, 842, 1190]; $lpp = 118 }
           
           print "MAX LL: $maxl in $pagenum ", join('::', @{$mb}), " Lines ($lpp): ", scalar(@lines), "\n";
           my $pdef = $pdf->new_page('MediaBox' => $mb);
           my $page = $pdef->new_page();
           $page->printnl(join("\n", splice @lines, 0, $lpp), $f1, $fntsz, 10, ($mb->[3] - 10));
        }
    } 
  }
  if ( $FQoutName ) {
     $pdf->close();
     my $doc = XReport::PDF::DOC->Open($FQoutName);
     $doc->Close();
     &$debgRtn("End convert Closing Pdf  $FQoutName,-- $FromPage, -- $pagenum"); 
     push @{$files}, [ $FQoutName, $FromPage, $pagenum ];
  }
  return $files;
}

#Processing INDEXES
sub readContents {
  my ($arch, $ixfnam, $ixproc ) = @_ ; 
  my $hdr = $ixproc->{tarhdr};
  $arch->seek($hdr->{ttr}, 0);
  my $bytes = 0;
  my $bytes2read = (16000 < $hdr->{size} ? 16000 : $hdr->{size});
  my $ixdata = '';
  while ( $bytes < $hdr->{size} ) {
    $arch->read(my $buff, $bytes2read);
    $buff = substr($buff, 0, ($hdr->{size} - $bytes)) if (( $bytes + length($buff)) > $hdr->{size} );
    $ixdata .= $buff;
    $bytes += length($buff);  
  }
  my @ixlines = split /\n/, $ixdata;
  my $rownum = 0;
  my $fldsep = ($ixproc->{FLDSEP} ? qr/$ixproc->{FLDSEP}/ : qr/\t/ );   
  i::logit("Now processing $ixfnam FLDSEP: $fldsep");  
#  my $indexrows = [split /[\r\n]+/, $arch->contents($ixfnam)];
  my $hdrline = '';
  my $fnamcol = $ixproc->{FNAMCOL} || ($ixproc->{NOHDR} ? 'C00000' : 'NOME_FILE');
  my $ftypcol = $ixproc->{FTYPCOL} || ($ixproc->{NOHDR} ? 'C00001' : 'TIPO');
  my $testline = $ixlines[0];
  if ( $ixproc->{NOHDR} ) {
     my $ct = 0; $hdrline = join($ixproc->{FLDSEP} || "\t", map { sprintf('C%05.0d',$ct++) } split /$fldsep/, $ixlines[0]);
  }
  else { 
         $hdrline = shift @ixlines;
         $rownum += 1;
  }
  $hdrline =~ s/(^|$fldsep)$fnamcol($fldsep|$)/$1_MEMNAME_$2/i; 
  $hdrline =~ s/(^|$fldsep)$ftypcol($fldsep|$)/$1_MIMETYPE_$2/i; 
  my $index_in = {
           HDR => $hdrline,
           FIRSTROW => $testline,
           ROWS => [],
           FRMPCOL => $ixproc->{FRMPCOL} || '',
           FORPCOL => $ixproc->{FORPCOL} || '',
           TOTPCOL => $ixproc->{TOTPCOL} || '',
           };
      
      # &$debgRtn("$ixfnam ATTRIBUTES:\n", $index_in);  
   my @rowflds = split /$fldsep/, $index_in->{HDR};
   my @JRflds = keys %{$main::JRAttrs};
   while ( scalar(@ixlines) ) {
      my $rowinfo = {};
      @{$rowinfo}{@JRflds, @rowflds} = ( @{$main::JRAttrs}{@JRflds}, split /$fldsep/, shift @ixlines );
      $rownum += 1;

      $rowinfo->{_MEMNAME_} =~ s/\s+$//;
      $rowinfo->{_MIMETYPE_} =~ s/\s+$//;
      $rowinfo->{_MIMETYPE_} =~ 'Application/x-pdf' if ( !$rowinfo->{_MIMETYPE_} && $rowinfo->{_MEMNAME_} =~ /pdf$/i );
    
      my $rfnam = $rowinfo->{_MEMNAME_};

      if ( grep /$rfnam/, keys $main::hdrs ) {
         push @{$index_in->{ROWS}}, $rowinfo;
      }
      else {
         i::logit("line $rownum of $rfnam skipped - ", $rfnam, " not in archive" );  
      }
   }

  i::logit("added ", scalar(@{$index_in->{ROWS}}), " rows to $ixfnam " );  
# # &$debgRtn("$ixtnam rows:\n", @{$index_in->{ROWS}->[0..3]});  
  return (exists($index_in->{ROWS}) && scalar($index_in->{ROWS}) ? $index_in : undef);
}

$main::jr = XReport::JobREPORT->Open($JRID, 1);
my ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName, $UserRef, $XferEndTime) = 
  $main::jr->getValues(qw(LocalFileName SrvName RemoteHostAddr XferMode TargetLocalPathId_IN XferDaemon JobReportName UserRef XferEndTime));
@{$main::JRAttrs}{qw(XferEndTime UserRef UserTimeRef UserTimeElab)} = ($XferEndTime, $UserRef, $XferEndTime, $XferEndTime);
# &$debgRtn("$JRID Attributes:\n", %{$main::JRAttrs});  

$main::jr->deleteElab();

my $cfgvars = {};
@{$cfgvars}{qw(LocalFileName SrvName RemoteHostAddr XferMode TargetLocalPathId_IN XferDaemon JobReportName)} 
            = ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName);

$tgtIN = 'L1' unless $tgtIN;

my $rptconf_in = $main::jr->getFileName('PARSE');
my $FQcfgName = $main::workdir.'/'. basename($main::jr->getFileName('LOGPARSE'));
my $cfgfhin = new FileHandle("<".$rptconf_in) || die "unable to access rptconf $rptconf_in\n";
my $cfgfhou = new FileHandle(">".$FQcfgName) || die "unable to access rptconf $FQcfgName\n";
my $cfgtext = '';
while (<$cfgfhin>) {
  my $lout = $_;
  $lout =~ s/\$(\w+)/$cfgvars->{$1}/sg;
  print $cfgfhou $lout;
  $cfgtext .= $lout;
}
close $cfgfhin;
close $cfgfhou;

# # &$debgRtn("Parsing ", $FQcfgName, " contents:\n", $cfgtext);
#my $reportcfg = XML::Simple::XMLin($cfgtext,
#              ForceArray => [ qw(index loadarch op ix) ],
#              KeyAttr => {'index' => 'name', 'loadarch' => 'name', 'op' => 'name', ix => 'name'},
#              Variables => {%{$cdamfh->{cinfo}}, %$cfgvars}
#             );
#$main::indexes = { map { ("_$_.tsv" => $reportcfg->{loadarch}->{$JobReportName}->{ix}->{$_}) } 
#                                                       keys( %{$reportcfg->{loadarch}->{$JobReportName}->{ix}}) };
my $ijrar = $main::jr->get_INPUT_ARCHIVE();
my $cdamfh = $ijrar->get_INPUT_STREAM($main::jr, 'data');
i::logit("Input Archive handler ref: ", ref($ijrar), " Input file handler ref: ", ref($cdamfh));
die "Invalid input file handle type" unless ref($cdamfh) eq 'XReport::Storage::IN';; 
my $lrk = '';
$lrk = (grep /^lrecl$/i, keys %{$cdamfh->{cinfo}})[0] if ( exists($cdamfh->{cinfo}) );
my $lrecl = shift || 32000;
$lrecl = $cdamfh->{cinfo}->{$lrk} if ($lrk && $cdamfh->{cinfo}->{$lrk});
my $FQarchName = $main::workdir.'/XRARCHIVE.tar'; 
my $arch = new IO::File($FQarchName, 'w') || die "unable to open $FQarchName - $?\n";
binmode $arch;
while ( !$cdamfh->eof() ) {
   my $rlen = $cdamfh->read(my $buff, $lrecl);
   die "Read Failure - $! - ", ref($buff), " ", length($buff), "\n" unless defined($buff);
   $main::veryverbose && i::logit( "read returned ", length($buff), " bytes buffer - read returns: $rlen rec: ".unpack('H80', $buff));
   print $arch $buff;   
}
$arch->close;
my $reportcfg = XML::Simple::XMLin($cfgtext,
              ForceArray => [ qw(index loadarch op ix) ],
              KeyAttr => {'index' => 'name', 'loadarch' => 'name', 'op' => 'name', ix => 'name'},
              Variables => {%{$cdamfh->{cinfo}}, %$cfgvars}
             );
$main::indexes = { map { ("_$_.tsv" => $reportcfg->{loadarch}->{$JobReportName}->{ix}->{$_}) } 
                                                        keys( %{$reportcfg->{loadarch}->{$JobReportName}->{ix}}) };

#Opening arch ARCHIVE

use Archive::Tar::Stream qw();

my $archsize = -s $FQarchName;
$arch = new IO::File($FQarchName, 'r') || die "unable to open $FQarchName - $?\n";
binmode $arch;
my $tas = Archive::Tar::Stream->new(infh => $arch);
$main::hdrs = { cinfo => $cdamfh->{cinfo} };
my $filenum = 0;
while ( 1 ) {
   my $currpos = $arch->tell();
   last if ( !($currpos < $archsize) || ($archsize - $currpos) < 512 );
   $arch->read(my $hdrstr, 512);
   last if $hdrstr eq "\x00" x 512;     
   my $hdr = $tas->ParseHeader($hdrstr);
   $hdr->{ttr} = $arch->tell();
   my $fn = $hdr->{name};
   if ( exists($main::indexes->{$fn}) ) {
      $main::indexes->{$fn}->{tarhdr} = $hdr;
   }
   else {
     $filenum += 1;
     $hdr->{filenum} = $filenum; 
     $main::hdrs->{$hdr->{name}} = $hdr;
   }
   my $fsize = $hdr->{size};
   my $size2skip = $fsize + (512 - ($fsize % 512));
   $arch->seek($size2skip, 1);
}

my $fxrfqn = $main::workdir.'/'. basename($main::jr->getFileName('FILXREF'));
my $filxref = new FileHandle(">$fxrfqn");
my $pxrfqn = $main::workdir.'/'. basename($main::jr->getFileName('PAGEXREF'));
my $pagxref = new FileHandle(">$pxrfqn");

print $filxref "StartItem\t$JobReportName\t\$\$\$\$\t\$\$\$\$\tPage=1\n"; 

#my @datafiles = grep /pdf$/i, @$main::files;

my $ParsedPages = 0;
my $partnum = -1;
foreach my $hdr ( sort { $a->{filenum} <=> $b->{filenum} } grep { exists($_->{filenum}) } values %{$main::hdrs} ) {
    next if exists($hdr->{RemoteHostAddr});
  my $listfn = $hdr->{name};
  print "processing $listfn - hdr: ", Dumper($hdr), "\n";
  my ($totPages, $forPages) = (1, -1);
  my $outnam;
  if ( $listfn =~ /\.pdf$/i && !$reportcfg->{loadarch}->{$JobReportName}->{IGNRPDFS}) {
     $outnam = extractArchPdfFile($arch, $listfn, $partnum, $ParsedPages );
  }
  elsif ( $listfn =~ /\.txt$/i ) {
     $outnam = convertArchFile2Pdf($arch, $listfn, $partnum, $ParsedPages );
  }
  else { 
     $outnam = extractArchFile($arch, $listfn, $partnum, $ParsedPages );
  }
  $totPages = $outnam->[-1]->[2];
  $forPages = $totPages unless $totPages == -1;
  $totPages = 1 if $totPages == -1;
  &$debgRtn("After convert LFN $listfn -- PP  $ParsedPages -- TP $totPages"); 
  $main::datafiles->{($reportcfg->{loadarch}->{$JobReportName}->{IGNRCASE} ? uc($listfn) : $listfn )} 
                                                                    = [ $outnam->[0]->[1], $totPages];
  $ParsedPages += $totPages;
  
  while ( scalar(@{$outnam}) ) {
     my ($outfn, $fp, $tp) = @{shift @{$outnam}};
     $partnum += 1;
     print $pagxref "File=$partnum From=$fp To=$tp\n"; 
  #  print $manifest "PDFOUT.$fct\t", basename($outnam), "\t$totPages\n"; 
     push @{$main::files2arc}, $outfn;
     (my $xreffn = $outfn) =~ s/\.[^\.\\\/]+$/\.XREF/i;
     push @{$main::files2arc}, $xreffn if -e $xreffn;
  
  }
}

print $filxref "EndItem\t$JobReportName\t\$\$\$\$\t\$\$\$\$\tPage=", $ParsedPages, "\n"; 
close $filxref;
push @$main::files2arc, $fxrfqn; 
close $pagxref;
push @$main::files2arc, $pxrfqn; 

&$debgRtn("Now checking cfg keys \n", $reportcfg->{loadarch});
if ( grep { exists($_->{tarhdr}) } values %{$main::indexes} ) {
   my $manfqn = $main::workdir.'/'. basename($main::jr->getFileName('IXFILE', '$$MANIFEST$$'));
   my $manifest = new FileHandle(">$manfqn");

    foreach my $ixname ( keys %{$main::indexes} ) {
      next unless exists($main::indexes->{$ixname}->{tarhdr});
      my $ix = $main::indexes->{$ixname};
      my $ix_cont = readContents($arch, $ixname, $ix) || next;;
    # next unless exists($main::indexes->{$ixname});
      my $outrtn = {};
      foreach my $opname ( keys %{$ix->{op}} ) {
        &$debgRtn("Now Processing 1$opname"); 
        my $op = $ix->{op}->{$opname};
        my $test = $op->{test};
        next if $test ne "1" && $ix_cont->{FIRSTROW} !~ /$test/;
        #&$debgRtn("Now Start INDEX $ixname processing for $op->{indexes}"); 
        ($ix_cont->{parser}, my @outlist) = buildParser($op->{getVars}, $op->{Columns});
        #&$debgRtn("Parser built for $ixname outlist: ", join("::", @outlist)); 
        foreach my $outixn ( split /\s+/, $op->{indexes} ) {
          next unless exists($reportcfg->{index}->{$outixn});
          $outrtn->{$outixn}->{outlist} = [ split /\s+/, $reportcfg->{index}->{$outixn}->{vars} ];
          $outrtn->{$outixn}->{entries} = $reportcfg->{index}->{$outixn}->{entries};
          $outrtn->{$outixn}->{table} = $reportcfg->{index}->{$outixn}->{table};
        }
      }
      foreach my $outixn ( keys %$outrtn ) {
        my $tabname = $outrtn->{$outixn}->{table};
        my $ixfname = basename($main::jr->getFileName('IXFILE', $tabname));
        @{$main::outfiles->{$tabname}}{qw(FH FNAME TOTENTRIES)} = (new FileHandle(">$main::workdir/$ixfname"), $ixfname, 0) unless exists($main::outfiles->{$tabname});
        $outrtn->{$outixn}->{FKEY} = $tabname;
        my $fh = $main::outfiles->{$outrtn->{$outixn}->{FKEY}}->{FH};
        #&$debgRtn("Columns  will be stored into ", join('::', @{$outrtn->{$outixn}->{outlist}}), " of $outixn"); 
        print $fh join("\t", ('JobReportID', @{$outrtn->{$outixn}->{outlist}}, 'FromPage', 'ForPages' )), "\n";
        $outrtn->{$outixn}->{cnt} = 0;
      }
      while ( scalar(@{$ix_cont->{ROWS}}) ) {
        my $row = shift @{$ix_cont->{ROWS}};
        &$debgRtn("Processing index row: ", join('::', %$row));
        foreach my $outixn ( keys %$outrtn ) {
          next if $outrtn->{$outixn}->{entries} eq 'FIRST' and $outrtn->{$outixn}->{cnt} > 0;
          my $memname = ($reportcfg->{loadarch}->{$JobReportName}->{IGNRCASE} ?  uc($row->{_MEMNAME_}) : $row->{_MEMNAME_});
          die "File $memname not found in archive - aborting job" unless exists($main::datafiles->{$memname});
          die "Specified size of $memname ($ix_cont->{TOTPCOL}) different from real size in archive - Aborting job"
                                       if $ix_cont->{TOTPCOL} and $ix_cont->{TOTPCOL} != $main::datafiles->{$memname}->[1];
          my ($fromPage, $forPages) = @{$main::datafiles->{$memname}};
          #&$debgRtn("FromPage :: $fromPage -- ForPage :: $forPages");
          $fromPage += ($row->{$ix_cont->{FRMPCOL}} - 1) if $ix_cont->{FRMPCOL} and $row->{$ix_cont->{FRMPCOL}};
          $forPages = $row->{$ix_cont->{FORPCOL}} if $ix_cont->{FORPCOL} and $row->{$ix_cont->{FORPCOL}};
          #&$debgRtn("FromPage :: $fromPage -- ForPage :: $forPages");
          my $fh = $main::outfiles->{$outrtn->{$outixn}->{FKEY}}->{FH};
          print $fh join("\t", ($JRID
                                , map { s/^(\d\d\d\d\-\d\d\-\d\d) (\d\d\:\d\d\:\d\d)$/$1T$2/; $_ } 
                                        @{&{$ix_cont->{parser}}($row)}{@{$outrtn->{$outixn}->{outlist}}}
                                , $fromPage, $forPages)), "\n";
          $main::outfiles->{$outrtn->{$outixn}->{FKEY}}->{TOTENTRIES}++;
          $outrtn->{$outixn}->{cnt}++;
        }
      }
      foreach my $outixn ( keys %$outrtn ) {
        # &$debgRtn("$outixn entries: $outrtn->{$outixn}->{entries}, count: $outrtn->{$outixn}->{cnt}"); 
      }
    
      foreach my $outixn ( keys %{$main::outfiles} ) {
        my ($outnam, $totentries) = @{$main::outfiles->{$outixn}}{qw(FNAME TOTENTRIES)}; 
        &$debgRtn("$outixn table entries count: $main::outfiles->{$outixn}->{TOTENTRIES}"); 
        my $fh = $main::outfiles->{$outixn}->{FH};
        close $fh;
        print $manifest join("\t", ($outixn, basename($outnam), $totentries ) ), "\n";
        push @$main::files2arc, "$main::workdir/".$main::outfiles->{$outixn}->{FNAME}; 
    
      }
    
    }
    
    close $manifest;
    push @$main::files2arc, $manfqn; 
}
push @$main::files2arc, $FQcfgName; 
my $contentfn = "$main::workdir/\$\$ARC.\$\$MANIFEST\$\$.TXT";
my $contentfh = new FileHandle(">$contentfn") || die "Unable to open list to archive file - $!";
binmode $contentfh;
print $contentfh join("\n", @$main::files2arc), "\n";
$contentfh->close;

my $exit_code = 0;

if ( scalar(keys %{$reportcfg->{loadarch}->{$JobReportName}->{ix}}) ) {

  unless ( scalar(keys %{$main::outfiles}) ) {
    i::logit("No indexes found that matches specifications"); 
    $exit_code = 1;
  }
}
my $sha1Key = Digest::SHA1::sha1_base64($main::hdrs->{cinfo}->{FOLDPATH});
my $xrdbh = XReport::DBUtil::getHandle('XREPORT');
$main::jr->setValues('ParsedPages' => $ParsedPages);
$main::jr->setValues('bundlesPackage' => '');
$main::jr->putFields;
$main::jr->addLogicalReportsTexts([$main::hdrs->{cinfo}->{UserRef}]);
my $dbr = $xrdbh->dbExecute("SELECT dbo.XferDateDiff('".$main::jr->getValues(qw (XferStartTime))."') As XferDateDiff");
my $XferDateDiff = $dbr->GetFieldsValues('XferDateDiff') or  die("INVALID XferStartTime DETECTED.");
$main::jr->setValues(XferDateDiff => $XferDateDiff);
$main::jr->addLogicalReports([{rn => ((keys %{$main::jr->getReportDefs()})[0] || ''), fn => '$$$$', fv => '$$$$|'.$main::hdrs->{cinfo}->{UserRef}, totp => $ParsedPages,rid=>1, rdef => $main::jr->getReportDefs()}]);
$main::jr->addPhysicalReports([{rid => 1, totp => $ParsedPages, lop => "1,$ParsedPages"}]);
$xrdbh->dbExecute("IF NOT EXISTS (SELECT 1 FROM tbl_VarSetsValues where VarSetName = '".$sha1Key."' and VarName = ':REPORTNAME' and VarValue = '".$main::jr->getValues('OrigJobReportName').$JobReportName."' )
        INSERT INTO tbl_VarSetsValues (VarSetName, VarName, VarValue) values ('".$sha1Key."', ':REPORTNAME', '".$main::jr->getValues('OrigJobReportName').$JobReportName."') ");
$xrdbh->dbExecute("IF NOT EXISTS (SELECT 1 from tbl_NamedReportsGroups where ReportGroupId = '".$sha1Key."' and ReportRule = '".$sha1Key."' )
        INSERT INTO tbl_NamedReportsGroups (ReportGroupId, ReportRule,FilterVar,FilterRule,RecipientRule) values('".$sha1Key."', '".$sha1Key."', '\$\$\$\$', '__UNDEF__', '') ");
$xrdbh->dbExecute("IF NOT EXISTS (SELECT 1 FROM tbl_FoldersRules where FolderName = '".$main::hdrs->{cinfo}->{FOLDPATH}."' and ReportGroupId = '".$sha1Key."' ) INSERT INTO tbl_FoldersRules (FolderName, ReportGroupId,FORBID) values ('".$main::hdrs->{cinfo}->{FOLDPATH}."', '".$sha1Key."', 0) ");
##  manca inserimento sulla VarSetsValues e NamedReportGroups--> valutare se � da considerarsi da fare come configurazione
exit $exit_code;

__END__ 

<jobreport name="${JobReportName}" language="Italian">

 <index name="GENERIC_IX" type="PAGE" entries="ONLY" entries_="ONLY"
   vars="DOCDATE REPORTNAME VARNAME VARVALUE"
   table="CTDWEB_GENERIC_INDEX"
 />
 
 
<loadarch name="${JobReportName}" test="1" IGNRCASE="1" >
 <!-- nome del file che contiene gli indici suffissato da '_' e con estensione tsv-->

   <ix name="INDEXES" test="1" FLDSEP="\t" FRMPCOL="FromPage" FORPCOL="ForPages" FNAM_COL="NOME_FILE" FTYPCOL="TIPO" >  
 
 <!-- get vars variabili che deve prende ( uguali a var negli indici, si possono caricare meno variabili )-->
   <!-- columns nome delle variabili nel file-->
   <!-- test fa il test sulla prima linea-->
   <!-- NOME_FILE TIPO ID_DOC IndexName VarName VarValue FromPage ForPages-->
        <op name="GETIX" test="[\w\.]\t" 
         Columns="DOCDATE REPORTNAME VarName VarValue"
         getVars="DOCDATE REPORTNAME VARNAME VARVALUE"
     indexes="GENERIC_IX"
     />
   </ix>
 </loadarch>
</jobreport>

