
use strict;

use Cwd;
use File::Basename;

use XReport;
use XReport::Util;
use XReport::DBUtil;

my ($ReportType, $ReportDescr, $ReportID, $XferStartTime, $JobName, $JobNumber, $ReportStatus) = ();
my ($tarFileNumIN, $tarFileNameIN, $tarFileNumOUT, $tarFileNameOUT);
my ($FileName, $BaseFileName, $XferMode, $lpdProgr) = ();
my ($SrvName, $CurrSplitter);

my ($xrspool, $dbr, $InQLock);

$::xrcfg = XRgetConf();


sub VerifyTarFileIN {
  die "VerifyTarFileIN\n";
  my ($dbr, $tarFileNum, $tarFileName, $cmdOut, $tardir, $toList);
  
  my $at = $XferStartTime; $at =~ s/[\/ :]//g;
  my ($atYear, $atDay) = unpack("a4a4", $at);

  my $dirTar = "$xrspool/tarFiles/$atYear/$atDay";

  $dbr = dbExecute(
    "SELECT TarFileNumberIN from tbl_reports " .
	"where ReportType = '$ReportType' AND LocalFileName = '$BaseFileName'"
  );

  if ( !$dbr->eof() ) {
    $tarFileNum = $dbr->Fields->Item('TarFileNumberIN')->Value();
  }
  else {
    die "???? 123\n";
  }

  $tarFileName = "$dirTar/IN.$atYear$atDay\.\#$tarFileNum\.tar";

  if ( !-e $tarFileName ) {
    die "tar file $tarFileName not found ??!!\n";
  }

  $toList = basename($FileName); $toList =~ s/.DATA.TXT$//;
  
  $cmdOut = qx{
    tar -t -f $tarFileName $toList\.*
  };

  if ( $cmdOut !~ /^$toList\.DATA.TXT/m ) {
    die "REPORT FILE for $tarFileNum $toList NOT FOUND !!??\n";
  }

  &$logrRtn("VERIFY tarFileIN \n$cmdOut");
}

sub AppendToTarFileIN {
  die "AppendToTarFileIN\n";
  my ($dbr, $XferDate, $tarFileNum, $tarFileName, $cmdOut, $curdir, $tardir, $toTar);

  my $at = $XferStartTime; $at =~ s/[\/ :]//g;
  my ($atYear, $atDay) = unpack("a4a4", $at);
    
  my $dirTar = "$xrspool/tarFiles/$atYear/$atDay";
  if ( ! -e $dirTar ) {
    mkdir("$xrspool/tarFiles/$atYear");
    mkdir("$xrspool/tarFiles/$atYear/$atDay");
  }
  $XferDate = substr($XferStartTime,0,10).' 00:00:00';
  $dbr = dbExecute(
    "SELECT * FROM tbl_TarFilesDir ".
	"where XferDate = '$XferDate' "
  );

  if (!$dbr->eof() ) {
    $tarFileNum = $dbr->Fields->Item('NumTarFilesIN')->Value();
  }
  else {
    $dbr = dbExecute(
      "INSERT INTO tbl_TarFilesDir ".
	  "(XferDate) VALUES('$XferDate') "
    );
	$tarFileNum = 0;
  }
  $dbr->Close();
  $tarFileName = "$dirTar/IN.$atYear$atDay\.\#$tarFileNum\.tar";
  
  if ($tarFileNum == 0 or (-s $tarFileName) > 32000000) {
    $tarFileNum++;
	$dbr = dbExecute(
	  "UPDATE tbl_tarFilesDir ".
	  "set NumTarFilesIN = $tarFileNum "
	);
    $tarFileName = "$dirTar/IN.$atYear$atDay\.\#$tarFileNum\.tar";
  }
  
  if ( !-e $tarFileName ) {
    my $cmdOut = qx{
	  tar -vcf \"$tarFileName\"
	};
  }
 
  $curdir = getcwd();  # -C get error because of drive name f:/ interpreted as relative dir
  $tardir = dirname($FileName); 
  if ( !chdir($tardir) ) {
    die("UNABLE to chdir to $tardir $!")
  }
  $toTar = basename($FileName); $toTar =~ s/\.DATA\.TXT$//i;
  $cmdOut = qx{
	tar -vuf $tarFileName $toTar\.*
  };
  &$logrRtn("INSERTED FILE to TarFileIN \n$cmdOut");
  $cmdOut = qx{
    tar -t -f $tarFileName $toTar\.*
  };
  &$logrRtn("VERIFY for INSERTED FILE to TarFileIN \n$cmdOut");
  chdir($curdir);
  if ( $cmdOut !~ /^$toTar\.DATA\.TXT/mi or $cmdOut !~ /^$toTar\.CNTRL\.TXT/mi ) {
    print ("Tar Add ERROR ??!!");
  }

  return ($tarFileNum, $tarFileName);
}


sub GetRprtAttr2 {
  my $FileName = shift;
  
  # per adesso definiamo il file type come ftpc
  my $XferMode = 4;
    
  chomp $FileName;
  my $FileNameOrig = $FileName; $FileNameOrig =~ s/\.gz$//i;
  my ($BaseFileName, $reportDir, $reportExtension) = File::Basename::fileparse($FileNameOrig, '\.[^\.]+');
    
  my ($ReportType, $ArrivalTime, $lpdProgr) = ($BaseFileName =~ /([^\.]+)\.(\d+)\.([\#\d]+)\.DATA/);
  $ArrivalTime =~ s|(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)|$1/$2/$3 $4:$5:$6|;
    
  my $cntrlFileName = $reportDir.$BaseFileName.".TXT";
  $cntrlFileName =~ s/\.DATA\.TXT$/.CNTRL.TXT/;
    
  &$logrRtn("Now accessing $cntrlFileName");
   	
  if ( !-e $cntrlFileName ) {
    print "CNTRL FILE $cntrlFileName MISSING ??!!\n";
	if ( $ReportType eq 'AFP00000' ) {
	  return (
       $FileName, $ReportType, $BaseFileName.'.TXT',
	   'SEGSTA12', 'JOB99999', $ArrivalTime, $lpdProgr, 2
      );
	}
    return ();
  }
  open(CNTRL,"$cntrlFileName") or die("OPEN CNTRL File ERROR $!\n");
  my $cntrlString = join("",<CNTRL>);
  (undef, my $JobName, my $JobNumber) = ($cntrlString =~ /\nJ\w+\.(\w+)\.(\w+)\.(\w+)\./);
  if ( $cntrlString =~ /^-ofileformat=record/mi ) {
	$XferMode = 2;
  }
  else {
	$XferMode = 1;
  }
  close(CNTRL);
  
  return (
    $FileName, $ReportType, $BaseFileName.'.TXT',
	$JobName, $JobNumber, $ArrivalTime, $lpdProgr, $XferMode
  );
}

sub addReport {
    $FileName = shift;
	print "QQ $FileName\n";
  
    $InQLock = GetLock('AssignInputQueue');
   
    ($FileName, $ReportType, $BaseFileName, $JobName, $JobNumber, $XferStartTime, $lpdProgr, $XferMode)
	  = 
	GetRprtAttr2($FileName);
    if ( !$FileName ) {
	  #print LOG "BYPASSED !!";
	  return;
	}
    &$logrRtn("AT=$XferStartTime RT=$ReportType RF=$BaseFileName JI=$JobName JN=$JobNumber");

    if ( $JobName eq 'SEGSTA99' and $ReportType ne 'SEG00001' ) {
  	  my $ffm = $FileName; my $fto=$ffm; 
  	  $fto  =~ s/\/$ReportType/\/SEG00001/;
  	  print "$ffm $fto";
  	  rename ($ffm.".gz", $fto."gz") or die("rename error");
  	  $ffm =~ s/\.DATA\./.CNTRL./;
  	  $fto =~ s/\.DATA\./.CNTRL./;
  	  rename ($ffm, $fto) or die("rename error");
  	  $ReportType = 'SEG00001';
  	  $FileName = $fto;
  	  $BaseFileName =~ s/\/$ReportType/\/SEG00001/;
	  $XferMode = 2;
  	}
    elsif ( $JobName eq 'SEGSTA12'  and $ReportType ne 'SEG00003' ) {
  	  my $ffm = $FileName; my $fto=$ffm; 
  	  $fto  =~ s/\/$ReportType/\/SEG00003/;
  	  print "$ffm $fto\n";
  	  rename ($ffm, $fto) or die("rename error $!");
  	  $ffm =~ s/\.DATA\./.CNTRL./;
  	  $fto =~ s/\.DATA\./.CNTRL./;
  	  if ( !-e $ffm ) {
  	    open(CNTRL, ">$ffm") or die("CNTRL open error");
        print CNTRL
  	     "HTCPBIP1\n" .
         "POSCHE1\n" .
         "JJES2.OSCHE1.SEGSTA12.JOB05770.D0000179.?\n" .
         "CTCPBIP1\n" .
         "LOSCHE1\n" .
         "fdfA251TCPBIP1\n" .
         "UdfA251TCPBIP1\n" .
         "NJES2.OSCHE1.SEGSTA12.JOB05770.D0000179.?\n" .
         "-ofileformat=record"
        ;
  	    close(CNTRL);
      }
  	  rename ($ffm, $fto) or die("rename error");
  	  $ReportType = 'SEG00003';
  	  $FileName = $fto;
  	  $BaseFileName =~ s/\/$ReportType/\/SEG00003/;
	  $XferMode = 2;
    }
    print "QQ $ReportType $FileName\n";	
    $dbr = dbExecute(
		     "SELECT SrvName, ReportID, Status from tbl_Reports " .
		     "WHERE " .
		     "ReportType = '$ReportType' AND LocalFileName LIKE '$FileName'"
		    );
    if ( !$dbr->eof() ) {
      $ReportID = $dbr->Fields->Item('ReportId')->Value();
      $ReportStatus = $dbr->Fields->Item('Status')->Value();
    }
    else {
	  $ReportStatus = "";
      $ReportID = 0;
    }
    $dbr->Close();
    
#    if ( $ReportID > 0 and $ReportStatus ne $CD::stCompleted ) {
#      &$logrRtn("Already OWNED by $CurrSplitter");
#      ReleaseLock($InQLock);
#	  #exit;
#      return;
#    }
    
    if ( $ReportID > 0) {
	  #($tarFileNumIN, $tarFileNameIN) = VerifyTarFileIN();
    }
	else {
	  #($tarFileNumIN, $tarFileNameIN) = AppendToTarFileIN();
	  #BEGIN TRANSACTION
	  print "?? $ReportType $FileName\n";
	  print LOG " $ReportType $FileName\n";
      #ReleaseLock($InQLock);
	  #next;
	  #exit;
	  #$dbr = dbExecute(
	  #       "BEGIN TRANSACTION"
	  #      );
      $dbr = dbExecute(
		     "INSERT INTO tbl_Reports " . 
		     "( " . 
		     "ReportType, LocalFileName, SrvName, Status," .
		     "JobName, JobNumber, XferStartTime, XferMode" .
		     ") " .
		     "VALUES " .
		     "( " .
		     "'$ReportType', '$FileName', '$SrvName', $CD::stQueued, " .
		     "'$JobName', '$JobNumber', '$XferStartTime', $XferMode" .
		     ")"
		    );
	  $dbr = dbExecute(
		     "SELECT ReportID from tbl_Reports " .
		     "WHERE " .
		     "ReportType = '$ReportType' AND LocalFileName = '$FileName'"
		    );
      $ReportID = $dbr->Fields->Item('ReportID')->Value();
      $dbr->Close();
	  $dbr = dbExecute(
		     "INSERT INTO tbl_WorkQueue " .
			 "( " .
			 "TypeOfWork, WorkClass, Priority, Status, InsertTime, ExternalKey, ExternalTableName " .
			 ") " .
			 "VALUES " .
			 "( " .
             "1, 64, 0, $CD::stQueued, GETDATE(), $ReportID, 'tbl_Reports' " .
		     ")"
			);
	  #$dbr = dbExecute(
	  #       "COMMIT TRANSACTION"
	  #      );
	  #END TRANSACTION
	}
        
    ReleaseLock($InQLock);
}


sub dirScan {
  my ($dir, $mask, $rt) = (shift, shift, shift);
  my @files = glob("$dir/*");
  for (@files) {
    if ( -d $_ ) {
      dirScan($_, $mask, $rt);
      print "$_\n";
	  #&$rt($_);
	}
    else {
      print "$_\n";
	  &$rt($_) if /$mask/o;
    }
  }
}

$xrspool = $::xrcfg->{xrspool};

open(LOG, ">addReport.log");

dirScan('//N2091940/xreport/SpoolAreaIN/2001/0424','.*\.gz$', \&addReport);

close(LOG);
