#!perl -w

use strict qw(vars);

use DBI;

use XReport;
#use XReport::DBUtil qw();
use XReport::Storage::IN;
sub i::logit { print @_, "\n"; }
sub i::warnit { print @_, "\n"; }

sub loadOldReports {
	my ($dbh, $fname) = @_;
    my ($stmnt, $sth, $rv);
    
	$rv = $dbh->do(qq(CREATE TABLE OLDJobReports
	      ( JobReportId INTEGER PRIMARY KEY ASC 
	      , LocalFileName VARCHAR(100) NOT NULL
	      );) );
	if (!defined($rv)) { die "Table JobReports create Error - ", $DBI::errstr; } else {
	   print "JobReports Table created successfully\n";
	}	
	$dbh->do('BEGIN');
	$sth = $dbh->prepare(qq(INSERT INTO XrJobReports ( JobReportId ) VALUES ( ? ) ) );
	if (!defined($sth)) { die "prepare SQL failed - ", $DBI::errstr; }

    my $xrdbh = XReport::DBUtil::getHandle('XREPORT');
    my $xrdbr = $xrdbh->dbExecute(qq(SELECT JobReportId, LocalFileName 
                                     FROM MIMBASDB77.xreport_new.dbo.tbl_JobReports 
                                     WHERE status = 18 and xferstarttime > '2015-11-01' 
                                  ));
    my @jobreports = ();
    while ( !$xrdbr->eof() ) {
       my $fields = $xrdbr->Fields();
       my $jobreport = {};
       my ($jrid, $rfn) = map { $xrdbr->Fields()->Item($_)->Value() } qw(JobReportId LocalFileName);
       $sth->execute($jrid, $rfn);
       $xrdbr->MoveNext();
    }
	$dbh->do('COMMIT');
	    
	$stmnt = qq(SELECT count(*) from OLDJobReports;);
	$sth = $dbh->prepare( $stmnt );
	$rv = $sth->execute();
	if(!defined($rv) || $rv < 0){
	   die "Jobreport Select Error - ", $DBI::errstr;
	}
	while(my @row = $sth->fetchrow_array()) {
	      print "JobReport Count $row[0]\n";
	}
	
    return $dbh;
}

sub loadNewReports {
	my ($dbh, $fname) = @_;
    my ($stmnt, $sth, $rv);
    
	$rv = $dbh->do(qq(CREATE TABLE NEWJobReports
	      ( JobReportId INTEGER PRIMARY KEY ASC 
	      );) );
	if (!defined($rv)) { die "Table JobReports create Error - ", $DBI::errstr; } else {
	   print "JobReports Table created successfully\n";
	}	
	$dbh->do('BEGIN');
	$sth = $dbh->prepare(qq(INSERT INTO NewJobReports ( JobReportId ) VALUES ( ? ) ) );
	if (!defined($sth)) { die "prepare SQL failed - ", $DBI::errstr; }

    my $xrdbh = XReport::DBUtil::getHandle('XREPORT');
    my $xrdbr = $xrdbh->dbExecute(qq(SELECT RemoteFileName 
                                     FROM xreport.dbo.tbl_JobReports 
                                     WHERE XferStartTime > '2015-11-01' and RemoteFileName like 'JOBREPORTID.%' 
                                  ));
    my @jobreports = ();
    while ( !$xrdbr->eof() ) {
       my $fields = $xrdbr->Fields();
       my $jobreport = {};
       (my $jrid = $xrdbr->Fields()->Item('RemoteFileName')->Value()) =~ s /JOBREPORTID\.//;
       $sth->execute($jrid);
       $xrdbr->MoveNext();
    }
	$dbh->do('COMMIT');
	    
	$stmnt = qq(SELECT count(*) from NEWJobReports;);
	$sth = $dbh->prepare( $stmnt );
	$rv = $sth->execute();
	if(!defined($rv) || $rv < 0){
	   die "Jobreport Select Error - ", $DBI::errstr;
	}
	while(my @row = $sth->fetchrow_array()) {
	      print "JobReport Count $row[0]\n";
	}
	
    return $dbh;
}

my $parms = { map { split /=/, $_, 2 } map { /^\-mirror(.*)$/; $1 } grep /^-mirror/, @ARGV };

my $dbh = DBI->connect("DBI:SQLite:dbname=:memory:", "", "", { RaiseError => 0 }) 
                      or die $DBI::errstr;
$dbh->do("PRAGMA journal_mode = OFF");

loadNewReports $dbh;
loadOldReports $dbh;


my $fhin = XReport::Storage::IN->Open( LocalPath => 'file://C:\home\Customers\fiditalia'
                                     , LocalFileName => 'APLEST01C.20150922083250.2385135.DATA.TXT.gz');

my $cinfo = $fhin->Cinfo();

use Data::Dumper;
print Dumper($cinfo);
