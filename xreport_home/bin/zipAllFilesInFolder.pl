
package main;
use strict ;
use warnings ;
use Archive::Zip qw( :ERROR_CODES );
use Cwd;
use Cwd 'abs_path';
use File::Path;
use constant DEBUG => 0;

sub compressToZip()
{
	my( $path, $debug, $dodelete, @excludedExtensions) = @_;
	die "error in Sub compressToZip : path missing" unless defined $path;
	die "error in Sub compressToZip : debug missing" unless defined $debug;
	die "error in Sub compressToZip : dodelete missing" unless defined $dodelete;
	(-e  $path) || die "error in Sub compressToZip : the path does not exist[$path]";
	$path =~ s/[\\]+$|[\/]+$//g;

	#change working directory
	my $old_dir = getcwd;
	chdir("$path") or die "cannot change: $!\n";
	my @filteredFiles = `dir /b /A-D`;
	
	#my @filteredFiles = map {uc($_)} @files;
	#my @filteredFiles = grep { not /\.log$|\.LOG$|\.zip$|\.7z$/ } @files;
	foreach (@excludedExtensions) 
	{
		my $reUC = uc($_);  
		my $reLC = lc($_);
		$reUC = qr/$reUC/;  
		$reLC = qr/$reLC/;
		#print "\n re=$re";
		($debug || DEBUG) && print "\n----excludedExtensions>[$_]";
		($debug || DEBUG) && print "\n----reUC>[$reUC]";
		($debug || DEBUG) && print "\n----reLC>[$reLC]";
		@filteredFiles = grep { not /\.${reUC}$/ } @filteredFiles;
		@filteredFiles = grep { not /\.${reLC}$/ } @filteredFiles;
	}
	chop(@filteredFiles);
	my $len_path = (length $path);
	my $regex = qr/^(.{$len_path})([\/\\]+)([^\n]*)$/;
	foreach my $file (@filteredFiles) 
	{
		my $zip = Archive::Zip->new;
		$file =~ $regex;
		my $member = $3;
		(-d $file ) && next;
		$zip->addFile($file,$member);   
		my $filezip = $file.'.zip';
		($debug || DEBUG) && print "\n filezip[$filezip]";
		#(-e  $filezip) && system("del /q $filezip");
		my $rc = $zip->writeToFileNamed($filezip);
		($debug || DEBUG) && print "\n rc = [$rc]";	
		if(  $rc == AZ_OK )
		{
			if($dodelete)
			{
				($debug || DEBUG) && print "\n TRYING TO DELETE [$file]";
				if(-e  $file) 
				{
					unlink $file;
				}
			}
		}
		else
		{
			die "error in creation of ZipFile rc: $rc\n";
		}
	}
	chdir("$old_dir") or die "cannot change: $!\n";
}



my $debug = 0;
my $dodelete = 0;
my ($pathIN, @excludedExtensions);

#parameters check
foreach my $i (0 .. $#ARGV) 
{
	if 	(($ARGV[$i] eq "-D")||($ARGV[$i] eq "-d")||($ARGV[$i] eq "-debug")||($ARGV[$i] eq "-DEBUG")) { # set debugLevel 
		$debug = 1;
	}
	elsif ($ARGV[$i] eq "-PATHIN") { # PATHIN
		$pathIN = $ARGV[$i+1];
	}
	elsif ($ARGV[$i] eq "-XE") { # XE
		push(@excludedExtensions,$ARGV[$i+1]);
	}
	elsif ($ARGV[$i] eq "-dodelete") { # dodelete
		$dodelete = 1;
	}
}


($debug || DEBUG) && print "\n----Parameter List";
foreach my $parameter (@ARGV) {
	($debug || DEBUG) && print "\n----parameter>[$parameter]";
}

$pathIN || die "The -PATHIN parameter is missing\n\n";

push(@excludedExtensions,'zip');
push(@excludedExtensions,'7z');
&compressToZip($pathIN, $debug, $dodelete, @excludedExtensions );

__END__
cd C:\Users\saiello\Desktop\temp\esempiPerl
perl zipAllFilesInFolder.pl -PATHIN C:\Users\saiello\Desktop\dsadasads -XE LOG
perl zipAllFilesInFolder.pl -XE LOG -XE txt -PATHIN "C:\Users\saiello\Desktop\temp\New folder (18)"
