use strict;

use XReport::DBUtil;
use Date::Calc qw(Date_to_Days Day_of_Week Add_Delta_Days Delta_Days);

my (@dateRef, @toDay, %jrn, $jrn, $jrid, $hdays, $hgens);

my $dbr;

sub toDay {
  my @t = localtime();
  return ( $t[5]+1900, $t[4]+1, $t[3] );
}

sub ExpireDate {
  my ($hdays, @refDay) = @_;

  if ( !@refDay ) {
    @refDay = @toDay;
  }
  my $weekDay = Day_of_Week(@refDay);
  
  my $weeks = int($hdays/5);
  my $rest = $hdays - $weeks*5; 

  my $deltaDays = $weeks*7 + $rest + 
  ( ( (5 - $weekDay) > $rest )  
    ? 0
    : 2
  );

  return Add_Delta_Days(@refDay, $deltaDays);
}

@dateRef = toDay();
@toDay = toDay();

$dbr = dbGetDynamicRs("
  SELECT  JobReportName, HoldDays, HoldGens
  FROM  tbl_JobReportNames a
  WHERE EXISTS( 
    SELECT b.JobReportName FROM tbl_JobReports b
    WHERE b.JobReportName = a.JobReportName
  )
");

while (!$dbr->eof()) {
  $jrn = uc($dbr->Fields->Item('JobReportName')->Value());
  $hdays = $dbr->Fields->Item('HoldDays')->Value();
  $hgens = $dbr->Fields->Item('HoldGens')->Value();
  #$hgens = 5 if ( $hgens == 0 and $hgens == 0 ); 
  print "?? $jrn $hdays $hgens\n";
  $jrn{$jrn} = [$hdays, $hgens, 0];
  $dbr->MoveNext();
}
$dbr->close();

$dbr = dbGetDynamicRs("
  SELECT  JobReportName, JobReportId, CONVERT(varchar, XferStartTime, 111) XferStartTime, LocalFileName
  FROM  tbl_JobReports
  order by JobReportName, XferStartTime desc
");


while (!$dbr->eof()) {
  my $jrn = uc($dbr->Fields->Item('JobReportName')->Value());
  my $jrid = $dbr->Fields->Item('JobReportId')->Value();
  my $xfrDay = $dbr->Fields->Item('XferStartTime')->Value();

  if ( !exists($jrn{$jrn}) ) {
    print "NOT EXISTS $jrn\n";
    $dbr->MoveNext();
	next;
  }
  
  my $aref = $jrn{$jrn};
  my ($hdays, $hgens, $tON) = @$aref;

  my (@xfrDay, @exprDay) = ();

  $hdays and do {
    @xfrDay = unpack("a4xa2xa2", $xfrDay);
    @exprDay = ExpireDate($hdays, @xfrDay);
  };
 
  my $hdaysExpired = ( !$hdays or (Delta_Days(@exprDay,@dateRef) > 0) );
  my $hgensExpired = ( !$hgens or $tON >= $hgens );

  #print "?? ", join("/",@xfrDay), " ", join("/",@exprDay), " ", Delta_Days(@exprDay,@dateRef), "\n";#exit;
  
  if ( $hdaysExpired and $hgensExpired ) {
    print "$jrn $jrid ", join("/", @xfrDay), "\n";
	if ($jrn eq 'CORNER1') {
	  for my $tbl (qw(tbl_LogicalReports tbl_PhysicalReports)) {
	    dbExecute(
	      "DELETE FROM $tbl WHERE JobReportId = $jrid"
	    );
	  }
	  my $dataFile = $dbr->Fields()->Item('LocalFilename')->Value();
	  my $cntrlFile = $dataFile;
	  $cntrlFile =~ s/DATA.TXT.gz$/CNTRL.TXT/i;
	  my $outFile = $dataFile;
	  $outFile =~ s/DATA.TXT.gz$/OUT.ZIP/i;
	  print "$dataFile\n$cntrlFile\n$outFile\n";
	  unlink ($dataFile, $cntrlFile, $outFile);
	
      $dbr->Delete();
	}
  }
  else {
    $aref->[2] += 1;
  }

  $dbr->MoveNext();
}

$dbr->close();
