#D:\strawberry\perl\bin>perl.exe -I "%PERL5LIB%" "%XREPORT_HOME%\bin\xrDeleteAll.pl" . 5622 -SITECONF "%XREPORT_SITECONF%" -HOME "%XREPORT_HOME%" -N XN_A1TEST_PDF304 -checkPendingOp
#!perl -w

use strict 'vars';
use constant DEBUG => 0; 

use Data::Dumper; 

use XReport;
use XReport::JobREPORT;
use XReport::DBUtil;  

$main::myName = ( split( /[\/\\]/, $0 ) )[-1];
$main::myName = ( split( /\./, $main::myName ) )[0];

sub print2Log { return i::logit("$main::myName: ".join(",", @_ )."_LINE[".(caller())[2]."]"); }

sub retrieveListIdByQuery {
	($main::veryverbose || DEBUG) && print2Log("call retrieveListIdByQuery()"); 
	my ($listJobReportId_ref) = @_;	
	
	my $xmlFile = "$XReport::cfg->{xreportsiteconf}/xml/$main::myName.xml"; 
	($main::veryverbose || DEBUG) && print2Log(Dumper("xmlFile",$xmlFile));  
	my $newcfg;
	eval { $newcfg = XML::Simple::XMLin($xmlFile ) if $xmlFile; };
	die "error in XML::Simple::XMLin: $@" if $@ ;  
	($main::veryverbose || DEBUG) && print2Log(Dumper("newcfg=",$newcfg));
	
	my $sql = $newcfg->{sql};
	print2Log("query: $sql");   
	
	my ($dbr); 
	
	eval { $dbr = dbExecute($sql);  } ;
	die "error in dbExecute: $@" if $@ ;  
	
	while(!$dbr->eof()) {
			my $qjrid = $dbr->Fields()->Item('JobReportId')->Value();
			push @{$listJobReportId_ref}, $qjrid;
			$dbr->MoveNext();
	}	
}

print2Log("- $0($$) starting");
my ($workdir, $JRID) = @ARGV[0,1];
 
if (( grep /^-dd$/i, @ARGV ) || DEBUG ){
	$main::veryverbose =1;
	print2Log("- MODE veryverbose ACTIVATED"); 
}

my @listJobReportId =();
if ( grep /^-(list|query)$/i, @ARGV )
{ 
	print2Log("- MODE list/query ACTIVATED"); 
	retrieveListIdByQuery(\@listJobReportId);
}
else
{
	print2Log("- MODE singleJobReportIds ACTIVATED"); 
	push @listJobReportId, $JRID;
	foreach my $i(0..$#ARGV)
	{
		if ($ARGV[$i] =~ /^-(jobreportid|jid|j)$/i)
		{
			$i++;
			push @listJobReportId, $ARGV[$i] ;
		} 
	}
}


my $mode_dryrun = 0;
if ( grep /^-(dryrun)$/i, @ARGV )
{ 
	print2Log("- MODE dryrun ACTIVATED");  
	$mode_dryrun = 1;
}

my $checkPendingOp = 0;
if ( grep /^-(checkPendingOp)$/i, @ARGV )
{ 
	print2Log("- MODE checkPendingOp ACTIVATED");  
	$checkPendingOp = 1;
}

my $deleteCDAMsons = 0;
if ( grep /^-(deleteCDAMsons)$/i, @ARGV )
{ 
	print2Log("- MODE deleteCDAMsons ACTIVATED");  
	$deleteCDAMsons = 1;
}




print2Log("- no JobReportId to process.") unless scalar(@listJobReportId);

while ( scalar(@listJobReportId) ) {
	my $JobReportID = shift @listJobReportId;
	print2Log("- processing JobReportID[$JobReportID].");
	next if($mode_dryrun);
	
	my $jr = XReport::JobREPORT->Open($JobReportID, 0);
	my ($Status, $fileName, $LocalFileName, $RemoteHostAddr, $XferMode, $JobName, $JobReportName, $XferEndTime, $PendingOp) = 
	  $jr->getValues(qw(Status LocalFileName LocalFileName RemoteHostAddr XferMode XferDaemon JobReportName XferEndTime PendingOp));
	my $JRAttrs;
	@{$JRAttrs}{qw(Status LocalFileName RemoteHostAddr JobReportID JobReportName XferEndTime UserRef UserTimeRef UserTimeElab PendingOp)} 
	= ($Status, $LocalFileName, $RemoteHostAddr, $JobReportID, $JobReportName, $XferEndTime, '', $XferEndTime, $XferEndTime, $PendingOp);
	
	$PendingOp = $PendingOp  or 0;
	if(($checkPendingOp) and ($PendingOp != 12))
	{
		print2Log("- Error in deleting JobreportId[$JRAttrs->{JobReportID}]: PendingOp[$PendingOp] not equal to 12."); 
		die("- Error in deleting JobreportId[$JRAttrs->{JobReportID}]: PendingOp[$PendingOp] not equal to 12."); 
	}
	
	if( ($JobReportName =~ /^CDAMFILE$/i) and $deleteCDAMsons )
	{
		my $relatedEntries = [];
		my $dbr = XReport::DBUtil::dbExecuteReadOnly('SELECT Status, JobReportName, JobReportID, RemoteHostAddr, LocalFileName FROM tbl_JobReports '
					. "WHERE XferDaemon = 'CDAMPARSER' AND RemoteHostAddr = '$JobReportID'");
		while ( !$dbr->eof() ) {
			my $flds = $dbr->GetFieldsHash();
			push @{$relatedEntries}, $flds->{JobReportID};
			print2Log("- QUEUE FOR DELETE - ReportName: ", $flds->{JobReportName} 
					   , " JobReportID: ", $flds->{JobReportID}
					   , " RemoteHostAdd: ", $flds->{RemoteHostAddr}
					   , " Status: ", $flds->{Status}
					   , " LocalFileName: ", $flds->{LocalFileName} );
			$dbr->MoveNext();
		}        
		$dbr->Close();

		while ( scalar(@{$relatedEntries}) ) {
			my $ojrid = shift @{$relatedEntries};
			print2Log("... deleting JobreportId[$ojrid]"); 
			eval { XReport::JobREPORT->Open($ojrid, 0)->deleteAll(); };
			if ($@) {
				print2Log("- Error in deleting JobreportId[$ojrid]: $@."); 
				die("- Error in deleting JobreportId[$ojrid]: $@."); 
			}
			print2Log("- JobreportId[$ojrid] deleted with success."); 
		}
	}
	print2Log("- QUEUE FOR DELETE - ReportName: ", $JRAttrs->{JobReportName} 
			   , " JRID: ", $JRAttrs->{JobReportID}
			   , " RemoteHostAdd: ", $JRAttrs->{RemoteHostAddr}
			   , " LocalFileName: ", $JRAttrs->{LocalFileName} 
			   , " Status: ", $JRAttrs->{Status} );
	print2Log("... deleting JobreportId[$JRAttrs->{JobReportID}]"); 
	eval { $jr->deleteAll(); };
	if ($@) {
		print2Log("- Error in deleting JobreportId[$JRAttrs->{JobReportID}]: $@."); 
		die("- Error in deleting JobreportId[$JRAttrs->{JobReportID}]: $@."); 
	}
	print2Log("- JobreportId[$JRAttrs->{JobReportID}] deleted with success."); 
}
print2Log("- $0($$) ended");