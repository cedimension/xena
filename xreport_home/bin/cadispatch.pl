use Win32::OLE;

use lib('f:/xreport/perllib');
use POSIX;

use Convert::EBCDIC;

$translator = new Convert::EBCDIC;

InitEnvironment();

open(INPUT,"<cadisp.UNLOAD") || die("INPUT open errors $!");

open(USERS,">cadisp.USERS") || die("INPUT open errors $!");
open(REPORTS,">cadisp.REPORTS") || die("INPUT open errors $!");
open(FOLDERS,">cadisp.FOLDERS") || die("INPUT open errors $!");
open(REPFOLD,">cadisp.REPFOLD") || die("INPUT open errors $!");
open(USERFOLD,">cadisp.USERFOLD") || die("INPUT open errors $!");

$nr=0;
while(!eof(INPUT)) {
  $nr++;
  read(INPUT,$_,255);

  $_ =~ s/ *$//;

  $_ = $translator->toascii($_);
  if ( $_ =~ /^A[1]/ ) {
    ($Name, $Descr) = unpack("x4a8x8a32", $_);
	$Name =~ s/ *$//;
    if (!exists($reports{$Name})) {
	  $Descr =~ s/ *$//;
	  $Descr =~ s/'/''/g;
	  $reports{$Name} = $Descr;
	}
	@t = unpack("\@121AAA3A2A3", $_);
    #print REPORTS $_, "\n";
    #print REPORTS @t, "\n";
	$t[2]+=0;
	$t[3]+=0;
	$t[4]+=0;
	$dbr = dbExecute(
	 "SELECT * from tbl_ReportTypes " .
	 "where ReportType = '$Name' "
	);
	if ( $dbr->eof() ) {
	  next if $Name =~ /^PRINTER/ or $Name =~ /^PROVA/;
	  print "MISSING ReportType $Name ?!\n";
	  $dbr->Close();
	  my $Group = substr($Name, 0, 2);
	  $dbr = dbExecute(
	   "INSERT into tbl_ReportTypes " .
	   "(ReportGroup, ReportType, ReportDescr, PrintFlag, ViewOnlineFlag, HoldDays, HoldGens, LinesPerPage) " .
	   "VALUES " .
	   "('$Group', '$Name', '$Descr', '$t[0]', '$t[1]', $t[2], $t[3], $t[4] )" 
	  );
	}
	else {
	  $dbr->Close();
	}
	next;
	dbExecute(
     "UPDATE tbl_ReportTypes set " .
     "  PrintFlag = '$t[0]', " .
     "  ViewOnlineFlag = '$t[1]', " .
     "  HoldDays = $t[2], " .
     "  HoldGens = $t[3], " .
     "  LinesPerPage = $t[4] " .
     "Where ReportType = '$Name'" 
    );
	$iupd++;
	print "update $iupd\n";
  }
  elsif ( $_ =~ /^A[2]/ ) {
    ($Name, $Descr) = unpack("x4a16x10x8x20a32", $_);
	$Name =~ s/ *$//;
	next if $Name =~ /^GR/;
    if (!exists($folders{$Name})) {
	  $Descr =~ s/ *$//;
	  $Descr =~ s/'/''/g;
	  $dbr = dbExecute(
	   "SELECT * from tbl_Folders " .
	   "where FolderName = '$Name' "
	  );
	  if ( $dbr->eof() ) {
	    print "MISSING Folder $Name ?!\n";
	    $dbr->Close();
	    my $Group = substr($Name, 0, 1);
	    $dbr = dbExecute(
	     "INSERT into tbl_Folders " .
	     "(FolderGroup, FolderName, FolderDescr) " .
	     "VALUES " .
	     "('$Group', '$Name', '$Descr')" 
	    );
	  }
	  else {
	    $dbr->Close();
	  }
	  $folders{$Name} = $Descr;
	}
    #print FOLDERS $_, "\n";
  }
  elsif ( $_ =~ /^A[3]/ ) {
    next;
    ($report, $jobname, $folder) = unpack("x4a8a8a16", $_);
	next if $report eq 'PRINTER' or $folder =~ /^GR/;
	my $c = substr($folder,0,1);
	next if $c eq 'A' or $c eq 'H' or $c eq 'Z';
	$dbr = dbExecute(
	  "SELECT * from tbl_FoldersReportTypes " .
	  "where ReportType = '$report' AND FolderName = '$folder'"
	);
	if ( $dbr->eof() ) {
	  print "$report $folder\n";
      print REPORTS $_, "\n";
	  $dbr->Close();
	  $dbr = dbExecute(
	   "INSERT into tbl_FoldersReportTypes " .
	   "(ReportType,  FolderName, SelKeyName, SelKeyValues)" .
	  	" VALUES " .
	  	"('$report', '$folder', '', '')"
	  );
	  $dbr->Close();
	}
	$dbr->Close();
    next;
    print REPORTS $_, "\n";
    $a3{$folder} = 1 if $report eq "PFG00810";
  }
  elsif ( $_ =~ /^A[4]/ ) {
    next;
    ($report, $folder, $filiale) = unpack("x4a8x8a16x9a32", $_);
    $a4{$folder} = 1 if $report eq "PFG00810";
    ($report, $folder, $filiale) = unpack("x4a8x8a16x9a32", $_);
	$report =~ s/ *$//;
	$folder =~ s/ *$//;
	$filiale =~ s/ *$//;
	my $c = substr($folder,0,1);
	if ( 0 and ($c eq 'A' or $c eq 'H' or $c eq 'Z') ) {
	  if ( !exists($autofolder{$folder}) ) {
	    dbExecute(
		  "UPDATE tbl_Folders " .
		  "set SelKeyName = 'FILIALE', SelKeyValues = '$filiale' " .
		  "where FolderName = '$folder' "
		);
        $autofolder{$folder} = 1;
	  }
      if ( !exists($autoreport{$report}) ) {
	    dbExecute(
		  "UPDATE tbl_ReportTypes " .
		  "set AutoFolderFlag = 'Y' " .
		  "where ReportType = '$report' "
		);
        $autoreport{$report} = 1;
	  }
	  next;
	}
	next if $c eq 'A' or $c eq 'H' or $c eq 'Z';
	$dbr = dbExecute(
	 "SELECT * from tbl_FoldersReportTypes " .
	 "where ReportType = '$report' and FolderName = '$folder' "
	);
	if ( !$dbr->eof() and ($filiale ne $dbr->Fields->Item('SelKeyValues')->Value()) ) {
	  print "?? $report $folder $filiale\n";
	}
	if ( $dbr->eof() ) {
	  print "MISSING $report $folder $filiale\n";
	  next;
	  $dbr->Close();
	  if ( !exists($repfold{"$report $folder $filiale"}) ) {
        print 
  	     "INSERT INTO tbl_FoldersReportTypes " . 
         "(ReportType, FolderName, SelKeyName, SelKeyValues) " .
  	     " VALUES " .
  	     "('$report', '$folder', 'FILIALE', '$filiale');\n"
        ;
        $dbr = dbExecute(
  	     "INSERT INTO tbl_FoldersReportTypes " . 
         "(ReportType, FolderName, SelKeyName, SelKeyValues) " .
  	     " VALUES " .
  	     "('$report', '$folder', 'FILIALE', '$filiale')"
        );
  	    $repfold{"$report $folder $filiale"} = "";
	  }
	  $dbr->Close();
	}
	else {
	  #print "FOUND $report $folder $filiale\n";
	  $dbr->Close();
	}
	next;
	if ( !exists($repfold{"$report $folder $filiale"}) ) {
      print REPFOLD
  	  "INSERT INTO tbl_ReportFolders " . 
        "(ReportID, FolderID, VarName, VarValue) " .
  	  " VALUES " .
  	  "('$report', '$folder', 'Filiale', '$filiale');\n"
      ;
      dbExecute(
  	  "INSERT INTO tbl_ReportFolders " . 
        "(ReportID, FolderID, VarName, VarValue) " .
  	  " VALUES " .
  	  "('$report', '$folder', 'Filiale', '$filiale')"
      );
  	  $repfold{"$report $folder $filiale"} = "";
	}
    #print REPFOLD $_, "\n";
	next;
    print REPFOLD $_, "\n";
    next;
    print REPFOLD $_, "\n" if substr($_,36,13) !~ /Y001022EQ\d\d\d /;
    next;
    print REPFOLD $_, "\n" if /CCG00105/;
    next;
  }
  elsif ( $_ =~ /^A[F]/ ) {
    $Name = unpack("x4a8", $_);
	$Name =~ s/ *$//;
    if (!exists($users{$Name})) {
	  $users{$Name} = "";
	}
	$dbr = dbExecute(
	  "SELECT * from tbl_Users " .
	  "where UserId = '$Name'"
	);
	if ( $dbr->eof() ) {
	  print "MISSING USER $Name !!\n";
	  $dbr->Close();
	  my $Group = substr($user,0,4);
	  dbExecute (
	   "INSERT INTO tbl_Users " . 
       "(UserGroup, UserID, UserDescr) " .
	   " VALUES " .
	   "('$Group', '$Name', '')"
      );
	}
	else {
	  $dbr->Close();
	}
    #print USERS $_, "\n";
  }
  elsif ( $_ =~ /^A[G]/ ) {
    ($user, $folder) = unpack("x4a16x16a16", $_);
	next if $folder =~ /^GR/;
	$user =~ s/ *$//;
	$folder =~ s/ *$//;
	$dbr = dbExecute(
	  "SELECT * from tbl_UsersFolders " .
	  "where UserId = '$user' and FolderName = '$folder' "
	);
	if ( $dbr->eof() ) {
	print "MISSING USER-Folder $user $folder !!\n";
      dbExecute(
  	   "INSERT INTO tbl_UsersFolders " . 
       "(UserID, FolderName) " .
  	   " VALUES " .
  	   "('$user', '$folder')"
      );
  	  $repfold{"$user $folder"} = "";
	}
    #print REPFOLD $_, "\n";
  }
}
open(DIFF, ">differences.txt");
$dbr = dbExecute( "SELECT * from tbl_ReportTypes " );
while(!$dbr->eof()) {
  $rt = $dbr->Fields->Item("ReportType")->Value();
  if (!exists($reports{$rt}) ) {
    print DIFF "DELETED REPORT $rt!!\n";
  }
  $dbr->MoveNext();
}
$dbr = dbExecute( "SELECT * from tbl_Folders " );
while(!$dbr->eof()) {
  $fl = $dbr->Fields->Item("FolderName")->Value();
  if (!exists($folders{$fl}) ) {
    print DIFF "DELETED FOLDER $fl!!\n";
  }
  $dbr->MoveNext();
}
$dbr = dbExecute( "SELECT * from tbl_Users " );
while(!$dbr->eof()) {
  $us = $dbr->Fields->Item("UserId")->Value();
  if (!exists($users{$us}) ) {
    print DIFF "DELETED USER $us!!\n";
  }
  $dbr->MoveNext();
}
exit;
@a = keys(%autofolder);
@b = keys(%autoreport);
print "$#a $#b";
exit;
for (keys(%a3)) {
  print "$_\n" if !exists($a4{$_});
}
exit;
for (sort(keys(%reports))) {
  print REPORTS "INSERT INTO tbl_Reports " . 
    "(ReportID, Descr) " .
	" VALUES " .
	"('$_', '$reports{$_}');\n"
  ;
}

for (sort(keys(%folders))) {
  print FOLDERS "INSERT INTO tbl_Folders " . 
    "(FolderID, Descr) " .
	" VALUES " .
	"('$_', '$folders{$_}');\n"
  ;
}

for (sort(keys(%users))) {
  print USERS "INSERT INTO tbl_Users " . 
    "(UserID, Descr) " .
	" VALUES " .
	"('$_', '');\n"
  ;
}

close(INPUT);
close(USERS);
close(REPORTS);
close(FOLDERS);
close(REPFOLD);
close(USERFOLD);


sub InitEnvironment {
  $gscmd ="gswin32c -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite";
  $basedir = "d:/CePost";
  $workdir = "d:/CePost/WorkInProgress";

  $cstr_OMS = 
    "PROVIDER=SQLOLEDB;DATA SOURCE=n2091940;" .
    "DATABASE=xreport;" .
    "USER ID=sa;PASSWORD=;"
  ; 

  $dbc = Win32::OLE->new("ADODB.Connection");
  die "ADODB Create Connection Error !!!" if !$dbc;
  $dbc->Open($cstr_OMS);
  die "ADODB Open Connection Error !!!" if !$dbc;
}

sub dbExecute {
  my $r = $dbc->Execute( "$_[0]" );
  if ( $dbc->Errors()->Count() > 0 ) {
    print "$_[0]\n", $dbc->Errors(0)->Description, "\n";
	die "Exiting ON DataBase ERROR !!!";
  }
  return $r;
}
