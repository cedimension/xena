#!/usr/bin/perl

use strict;
use POSIX qw(strftime);

use Win32::OLE qw(in);
use Win32::OLE::Enum;

use lib "$ENV{XREPORT_HOME}/perllib";

use XReport::Util;
use XReport::DBUtil;
package XReport::DBUtil;
#use Carp;

sub dbEx {
  my $self = shift; 
#  return print join(' ', @_), "\n";
  $self->CONNECT() if !$self->CONNECTED(); 
  my $rs;
  my $sql = join ' ', @_; 
  my $dbc = $self->{'dbc'}; 
  eval {$rs =  $dbc->Execute( "$sql" ); };
  croak "$sql\n$@\n" if $@;
  print "Error count detected " .  $dbc->Errors()->Count(). "\n" if  $dbc->Errors()->Count() > 0;
  croak $dbc->Errors(0)->Description()."\nfor $sql\n" if $dbc->Errors()->Count() > 0;
  return $rs;
}

1;

package main;
use XReport::JobREPORT;
use XReport::INPUT;

require XReport::Archive;

use Data::Dumper;
use XML::Simple;

sub addrows2tbl { 
  my ($dbc, $tbl, $columns, $rows) = (shift, shift, shift, shift);
  my $cmd = "INSERT INTO $tbl (". join(', ', map { "[$_]" } @$columns) .") VALUES\n" . join("\n, ", @$rows) . ";\n";
  &$logrRtn("Starting to insert " . scalar(@$rows) . " rows into $tbl");
  $dbc->dbExecute($cmd);
}

sub LoadIndexTable_SQL {
  my ($dbc, $FileName, $TableName) = (shift, shift, shift); 
  $FileName =~ s/\\/\//g;

#  print "DBC: ", ref($dbc), " FN: $FileName, TN: $TableName, SERV: $sqlserv\n";
  my $ixfh = new FileHandle("<$FileName") or die "Unable to open Index File \"$FileName\" to load into $TableName";
  
  my (@tblcols, @rows) = ((), ());
  while ( <$ixfh> ) {
    chomp;
    $_ =~ s/'/''/g;
    @tblcols = split(/\t/, $_), next if (!scalar(@tblcols));
    
#    push @rows, "SELECT '"  . join("', '",  split /\t/) . "'";
    push @rows, '('  . join(", ", map { $_ =~ /^#\$#NULL#\$#$/ ? 'NULL' : "'$_'"}  split /\t/) . ")";
    addrows2tbl($dbc, "$TableName", \@tblcols, \@rows), @rows = () if scalar(@rows) > 299; 
  }
  addrows2tbl($dbc, "$TableName", \@tblcols, \@rows) if scalar(@rows);
  
  $ixfh->close();
}

my $workdir = $ARGV[0];
my $jrid = $ARGV[1];
die "unable to access working directory $workdir\n" unless -d $workdir;

my $todaystr = POSIX::strftime("%y%m%d", localtime());
my $toDayFile = 'ex'.$todaystr.'.log';
my $tsvFile=(split(/[\/\\\.]/, $0))[-2].$todaystr;

my $myName=(split(/[\/\\]/, $0))[-1];
$myName=(split(/\./, $myName))[0];
#my $myName=(split(/[\/\\\.]/, $0))[-2];
my $cfgFile = 'n_o_t_f_o_u_n_d';
my  $cfgFile = $ENV{'XREPORT_SITECONF'}."/".$ENV{USERNAME}."/xml/$myName.xml"  unless -e $cfgFile;
$cfgFile = $ENV{'XREPORT_SITECONF'}."/xml/$myName.xml" unless -e $cfgFile;

my $cfg = XMLin($cfgFile);

foreach my $entry (keys %{$cfg->{'selections'}}) {

  print "==> entry: $entry", Dumper($cfg->{'selections'}->{$entry}) 
    unless exists($cfg->{'selections'}->{$entry}->{linesel});

  die "Error !!!\n" unless exists($cfg->{'selections'}->{$entry}->{linesel});
  my $table = $cfg->{'selections'}->{$entry};
  $cfg->{'selections'}->{$entry}->{re} =  qr/$table->{linesel}/im;
  delete $cfg->{'selections'}->{$entry}->{linesel};

  die "Error !!!\n" unless exists($cfg->{'selections'}->{$entry}->{rowflds});
  $cfg->{'selections'}->{$entry}->{flds} = [ split(/\s+/, $table->{rowflds}) ];
  delete $cfg->{'selections'}->{$entry}->{rowflds};

  next unless exists($cfg->{'selections'}->{$entry}->{exec4line});
  $cfg->{'selections'}->{$entry}->{lineSub} = eval('sub {' . $table->{exec4line} . '}');
  delete $cfg->{'selections'}->{$entry}->{exec4line};

}    

my $jr = Open XReport::JobREPORT($jrid);
$jr->setValues(
    ijrar => $jr->get_INPUT_ARCHIVE(), ojrar => undef, ElabContext => {}
);

my $INPUT = new XReport::INPUT($jr);
my ($XferStartTime, $JobReportName, $JobReportId, $WorkClass) = $jr->getValues(qw(XferStartTime JobReportName JobReportId WorkClass));
print "JRID: $JobReportId\n";

$INPUT->Open();
my $logflds = [];
my $logentry = {};
my $lct = 0;
while (my $line = $INPUT->getLine() ) {
  print "LINE: $line\n";
  next if $line =~ /^\s*$/;
  if ($line =~ /^\s*#fields:[\s\t]+(\w.*)$/i) {
    (my $flds = $1) =~ s/[\(\)]/-/g;
    @$logflds = split(/-?\s/, $flds);
    print "FLDS: ", join('::', @$logflds), "\n";
    next;
  }
  next unless scalar(@$logflds);
  next if $line =~ /^\s*#/;
  
  foreach my $entry (keys %{$cfg->{'selections'}}) {
    print "==> entry: $entry", Dumper($cfg->{'selections'}->{$entry}) unless exists($cfg->{'selections'}->{$entry}->{re});
    die "Error !!!\n" unless exists($cfg->{'selections'}->{$entry}->{re});
    unless ($line =~ $cfg->{'selections'}->{$entry}->{re}) {
      print "$entry SKIP: $line\n" if ($line =~ /getobject/i && $entry eq 'wwwroot');
      next;
    }
    print "$entry MATCH: $line\n";
    @{$logentry->{$entry}}{(@$logflds)} = split /\t/, $line;
    $logentry->{$entry}->{JobReportId} = $JobReportId;
    
    if (exists($cfg->{'selections'}->{$entry}->{lineSub})) {
      my $sub = $cfg->{'selections'}->{$entry}->{lineSub};
      &$sub($logentry->{$entry}, $line);
    }
#    my $ofkey = $reqenv.'::'.$entry;
    if (!exists($cfg->{'ofiles'}->{$entry}->{outfile})) {
      my $ofn = $cfg->{'ofiles'}->{$entry}->{outfn} = $workdir.'/'.$tsvFile.'.'.$entry.'.tsv';
      my $ofile = $cfg->{'ofiles'}->{$entry}->{outfile} = new FileHandle(">$ofn") || die "unable to open $ofn\n";
      #my $ofile = $cfg->{'ofiles'}->{$entry}->{outfile} = $ofn;
      print $ofile join("\t", @{$cfg->{'selections'}->{$entry}->{flds}}), "\n";
      print "to $ofile: ", join("\t", @{$cfg->{'selections'}->{$entry}->{flds}}), "\n";
    }
    my $ofile = $cfg->{'ofiles'}->{$entry}->{outfile};
    print $ofile join("\t", @{$logentry->{$entry}}{@{$cfg->{'selections'}->{$entry}->{flds}}}), "\n";
    print Dumper($logentry->{$entry}), "\n";
    print "to $ofile: ", join("::", @{$logentry->{$entry}}{@{$cfg->{'selections'}->{$entry}->{flds}}}), "\n";
  }
  #      last;
}
$INPUT->Close();

my $xrdb = new XReport::DBUtil();
foreach my $entry (keys %{$cfg->{'ofiles'}}) {
  my $ofile = $cfg->{'ofiles'}->{$entry}->{outfile};
  close $ofile;
  (my $ofn = $cfg->{'ofiles'}->{$entry}->{outfn}) =~ s/\//\\/g;
  print "now Removing previous entries .. $ofn\n";
  $xrdb->dbEx("DELETE FROM tbl_web_activity_$entry 
                      WHERE JobReportId = $JobReportId"
	     );
  

  print "now Bulk Insert .. $ofn\n";
  LoadIndexTable_SQL($xrdb, $ofn, "tbl_web_activity_$entry");


  #$xrdb->dbEx("BULK INSERT tbl_web_activity_$entry FROM '$ofn'
#	              WITH (  FIRSTROW  = 2,
#                              ROWS_PER_BATCH = 300,
#	                      FIELDTERMINATOR = '\\t',
#	                      ROWTERMINATOR   = '\\n')"
#	     );
  
}

exit 0;
__END__

#CEREPORTAPPL	CEWEBSERVICE	DOCSVC	TDOC01	2006-06-27	09:52:28	W3SVC25570656	10.15.34.184	POST	/GETDOCUMENT.ASP	GETDOCBYID_START_AT_2006-06-27+11:52:24_-ID_NO-IDENT-_TIMES(US::SY::CUS::CSY):_5.234::8.734::0::0_-_3_DOCUMENTS+RETRIEVED_-_118377_SENT_IN_BASE64_ENCODED_FORMAT	80	-	10.15.50.34	AXIS/1.2.1	-	200	0	0
#2006-03-29 18:16:12 10.134.13.11 POST /getDocument.asp GetDocList_start_at_2006-03-29+20:16:09_-_Times(us::sy::cus::csy):_4.937::1.531::0::0_-_10_lines_from_IBAPPL 8085 - 172.26.131.121 Mozilla/4.0+(compatible;+MSIE+6.0;+MS+Web+Services+Client+Protocol+2.0.40607.16) 200 0 0
#2006-03-29 18:16:26 10.134.13.11 POST /getDocument.asp GetDocById_start_at_2006-03-29+20:16:17_-_Times(us::sy::cus::csy):_10.421::3.328::0::0_-_1_Documents+Retrieved_-_198769_sent_in_base64_encoded_format 8085 - 172.26.131.121 Mozilla/4.0+(compatible;+MSIE+6.0;+MS+Web+Services+Client+Protocol+2.0.40607.16) 200 0 0
			 #	   oper => {
			 #		       're' => qr/^.+\sGET\s.*oper.+funzione=delete.+\s200[\d\s]+.*$/imo,
			 #		       'flds' => [qw(reqenv s-computername reqtime reqaddr)],
			 #		       'lineSub' => sub {
			 #			 my ($logentry, $line) = (shift, shift);
			 #			 $logentry->{'reqtime'}    = join(' ', @{$logentry}{qw(date time)});
			 #			 $logentry->{'reqaddr'}    = $logentry->{'c-ip'};
			 #			 $logentry->{'s-computername'} = $ENV{COMPUTERNAME} unless $logentry->{'s-computername'};



__END__
#2005-04-22 15:37:54 W3SVC2000223162 N2391997 127.0.0.1 GET /GetObject.asp FolderName=ALL.U.U920XXX6&JobReportId=2560&ReportId=1&FileFormat=Pdf 80 DMNBPB03\xreport 127.0.0.1 HTTP/1.1 Mozilla/4.0+(compatible;+MSIE+6.0;+Windows+NT+5.2;+.NET+CLR+1.1.4322) ASPSESSIONIDQSBAATDQ=MPKHCCIAOIDJEJAPOJNHLFMO;+IXR%5FUser=LocalSessionCookie=e360c6aa2daa055db27a550bee4591a5 - localhost 200 0 0 193 388 187
#2006-05-08 06:03:06 W3SVC1556122602 N239190I 10.134.3.70 GET /CeReportBds/GetObject.asp FolderName=AG03331&JobReportId=326620&ReportId=28&FileFormat=Pdf 80 - 10.234.4.21 HTTP/1.0 Mozilla/4.0+(compatible;+MSIE+6.0;+Windows+NT+5.1;+SV1) STAR_URI_cereportbds=lAlqSIduxLsBhaSznhNk;+IXR%5FUser=LocalSessionCookie=007ba60561ac347310788e502ee2387a;+ASPSESSIONIDQCTQDSRT=GICDGPGDJFGKOGGKCLOMJHJF;STAR_USERID=u016911;STAR_PASSWORD=XXXXXXXX;STAR_ROLES=CER01;STAR_TIMESTAMP=20060508080813;STAR_REMOTE_ADDRESS=10.234.4.21;STAR_SESSION_TIME=20060508100132;STAR_USERS_IN_CACHE=2 - cereport.bds.capitalia.it 200 0 64 5694 434 406
#
#isql -S Clu06SQLVS1 -d xreport_bds  -E

  BULK INSERT tbl_IisLog
    FROM '\\clu01data3\xreport_work_bds\Temp\accesses.tsv'
     WITH
      (   
	      FIRSTROW  = 2,
          FIELDTERMINATOR = '\t',
          ROWTERMINATOR   = '\n'
	  )
#
