#!perl -w

use strict;

use POSIX qw();
use File::Basename;

use Net::SSH2;
#use Net::OpenSSH;
use Net::SFTP::Foreign;
use IO::Socket::INET;
use Data::Dumper;
use XML::Simple qw();
#use File::Basename qw();

use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use IO::String();

use XReport;
use XReport::JobREPORT qw();
use XReport::DBUtil qw();
use XReport::Logger;
use XReport::ARCHIVE;

$| =1;
select(STDERR);
$| = 1;
select(STDOUT);


sub diewithmsg {
  i::logit(@_);
  die join('', @_);
}

sub XML::Simple::sorted_keys {
    my ($self, $name, $href) = @_;
    my $hkeys = {map {$_ => $_ } keys %{$href} };
    my @klist = ();
    my $knownkeys = {
	      destinazione => [qw(CodiceCliente Utente CodicePostalizzazione NomePdf StatoLettera Destinatario
                      Presso Indirizzo CAP Localita Provincia Stato DestCartolina1 DestCartolina2
                      DestCartolina3 DestCartolina4 DestCartolinaExtraInfo Mittente DisplayName
                      Destinatario CC CCN BounceAddress Oggetto Testo Allegato )]
    }; 

    push @klist, delete($hkeys->{name}) if ( exists($href->{name}) );
    if ( exists($knownkeys->{$name}) ) {
        foreach my $key ( @{$knownkeys->{$name}} ) {
            push @klist, delete($hkeys->{$key}) if ( exists($href->{$key}) );
        }
    }

    push @klist, keys %{$hkeys};
    return (grep { defined($_) } @klist);    
}

sub insertLR {

  my ( $jrid, $jrn, $XferStartTime, $UserRef, $XferRecipient ) = @_;
  my $insertlr = qq{ 
	   IF not exists (SELECT 1 from tbl_LogicalReports where JobReportId = '$jrid' AND ReportId = 1 AND FilterValue = '\$\$\$\$' AND ReportName = '$jrn') 
	    BEGIN
       INSERT INTO tbl_LogicalReports 
        (ReportName, FilterValue, TotPages, CheckStatus, 
		JobReportId, ReportId, XferDateDiff, XferRecipient)
         VALUES ('$jrn', '\$\$\$\$', 1, 0, $jrid, 1, DATEDIFF(day, '2000-01-01', '$XferStartTime'), '$XferRecipient')
        END
       
	   IF not exists (SELECT 1 from tbl_LogicalReportsTexts where textString = '$UserRef') 
	    BEGIN
	     INSERT INTO tbl_LogicalReportsTexts (textString) VALUES ('$UserRef') 
	    END

	   INSERT INTO tbl_LogicalReportsRefs ( JobReportId, ReportId, UserTimeRef, FilterVar, TextId)
	   SELECT $jrid, 1, '$XferStartTime', '\$\$\$\$', TextId FROM dbo.tbl_LogicalReportsTexts where textString = '$UserRef'

       };
  print "SQL: $insertlr\n";	   
  i::logit("Adding new logical report");
  XReport::DBUtil::dbExecute_NORETRY($insertlr);

}

sub processRequest {
  my $jrid = shift; 
  my ($ep, $jr, $setstring) = ({}, undef, []);
  $jr = XReport::JobREPORT->Open($jrid);
  my ($log, $data, $zipout, $lclfn, $docfn ) = map { my $fn = $jr->getFileName($_); print "$_ => $fn\n"; $fn } qw(LOGFILE DATAOUT ZIPOUT INPUT PDFOUT);
  $main::logger->AddFile( 'REQUESTLOG', $log );
  my $proc_workdir = dirname($log);
  mkdir $proc_workdir unless (-e $proc_workdir && -d $proc_workdir);
  diewithmsg("Unable to access $proc_workdir") unless (-e $proc_workdir && -d $proc_workdir);
  my @filelist = map { basename($_) } ($log, $data);

  my ($RJRID, $RJRN, $RXFST) = $jr->getValues(qw(JobReportId JobReportName XferStartTime));
  
  i::logit("Removing previous elab data for JobReportId $RJRID($RJRN)");
  
  $jr->deleteElab();
  
  i::logit("opening $lclfn");
  my $iar = $jr->get_INPUT_ARCHIVE()
             ->get_INPUT_STREAM();
  my $docfh = IO::File->new(">$docfn");
  $docfh->binmode();
  while ( !$iar->eof()) {
    $iar->read(my $buff, 32760);
    print $docfh $buff;
  }
  $docfh->close();
  i::logit("document data is ".(-s $docfn)." bytes");
  my $cinfo = $iar->Cinfo();
  # print "CINFO: ", Dumper($iar->Cinfo());
  my $XferRecipient = $cinfo->{OWNER};
  my $md = { map { (my $a = $_) =~ s/^_XFERMD_//; $a => ($cinfo->{$_} || '') } grep(/^_XFERMD_/, keys(%{$cinfo})) };
  $md->{NomePdf} = sprintf($RJRN.'.%s.D%s%s%s.T%s%s%s.'.$RJRID.'.pdf', $XferRecipient, split(/[\-: T]/, $RXFST));
  (my $UserRef = sprintf('%s (%s)', @{$md}{qw(Destinatario CAP)})) =~ s/'/''/g;
  my $mdxml = XML::Simple::XMLout( $md, NoAttr => 1, RootName => 'destinazione' );
  diewithmsg("XML Metadata build error") unless $mdxml;

  my $datafh = IO::File->new(">$data");
  $datafh->binmode();
  print $datafh $mdxml;
  $datafh->close();

  my $pathid = $jr->getValues('LocalPathId_OUT');
  push @$setstring, "LocalPathId_OUT = '$pathid'" if $pathid ;
    
  my $oar = XReport::ARCHIVE::cre_OUTPUT_ARCHIVE($jr, @_);
  i::logit("OUTPUT Archive Initialized into ".$oar->{FileName});

  $oar->AddFile($data, basename($data));
  i::logit("file $data added to archive");
  insertLR($RJRID, $RJRN, $RXFST, $UserRef, $XferRecipient);
  
  $main::logger->RemoveFile('REQUESTLOG') if $main::logger && ref($main::logger);
  $oar->AddFile($log, basename($log));
  i::logit("file $log added to archive");

  $oar->Close();
  my $fsz = -s $oar->{FileName};
  i::logit("END CREATING OUTPUT Archive in $oar->{FileName} size: $fsz");
  return ( $data, $docfn, $md );
}

sub newZipFile {
      my ($ep, $lclcfg, $jr) = @_;
      my ($exec_workdir, $tgtpfx) = @{$lclcfg}{qw(exec_workdir tgtpfx)};
	  $ep->{ziplist} = [] unless exists($ep->{ziplist});
	  my $fileid = scalar(@{$ep->{ziplist}});
      (my $zipname = $jr->getFileName('PDFOUT', $fileid)) =~ s/\.pdf$/\.zip/ if ( $jr && ref($jr) );
	  $zipname = POSIX::strftime("$exec_workdir/${tgtpfx}.%Y%m%d_%H%M%S.\#$fileid.zip", localtime()) unless ( $zipname );
	  $ep->{zipname} = $zipname;
	  return Archive::Zip->new();
}

sub getJobsToProcess {
   my $ep = shift;
   my ($srvname, $wqclause, $maxitems) = @{$ep}{qw(srvname wqclause maxitems)};
   
   $ep->{dbc} = XReport::DBUtil->new(DBNAME => 'XREPORT') unless ($ep->{dbc});
   $ep->{ziplist} = [] unless exists($ep->{ziplist});
   my $zipid = scalar(@{$ep->{ziplist}});
   #if ( $zipid ) {
      
      $wqclause = 'LEFT OUTER JOIN ( ' 
	            . join(' UNION ALL ', ( map { "SELECT * FROM #zipmap$_" } (0..($#{$ep->{ziplist}} - 0)) ), 'SELECT * FROM #zipmap'. $zipid) 
				. ') ttbl on tbl_WorkQueue.WorkId = ttbl.WorkId ' 
				. $wqclause 
				. ' and ttbl.WorkId is null';
   #}
   my $jobs = [];
   my $tmp_create = qq{
      IF OBJECT_ID ('tempdb..#zipmap$zipid', 'U') IS NOT NULL BEGIN DROP TABLE #zipmap$zipid END;
	  
	  CREATE TABLE #zipmap$zipid (WorkId int, JobReportId int, Status int, SrvName varchar(255), ElabStartTime datetime );
	  
	  UPDATE TOP($maxitems) tbl_WorkQueue SET tbl_WorkQueue.Status=17, tbl_WorkQueue.SrvName='$srvname'
	  OUTPUT inserted.WorkId, inserted.ExternalKey as JobReportId, '17' as Status, inserted.SrvName, GETDATE() as ElabStartTime INTO #zipmap$zipid
	  FROM tbl_WorkQueue
	  $wqclause ;
   };
   i::logit("Creating temp table: $tmp_create");
   $ep->{dbc}->dbExecute_NORETRY($tmp_create);

   my $dbr = $ep->{dbc}->dbExecute_NORETRY(qq{
      SELECT JR.JobReportId, JR.LocalPathId_IN, JR.LocalPathId_OUT, JR.LocalFileName, WQ.WorkId, WQ.Status
      FROM #zipmap$zipid WQ
      INNER JOIN tbl_JobReports JR on WQ.JobReportId = JR.JobReportId
   });
   
   while ( !$dbr->eof() ) {
      push @{$jobs}, $dbr->GetFieldsHash();
   } continue { $dbr->MoveNext(); }
   $dbr->Close();
   i::logit("Found ".scalar(@{$jobs})." Request to process"); 
   return $jobs;

}

sub updateTempTable {
   my ($ep, $statuslist) = @_;
   return undef unless scalar(@{$statuslist});
   my @varlist = keys %{$statuslist->[0]};
   
   my $tbl = '#zipmap'.scalar(@{$ep->{ziplist}});
   $ep->{dbc} = XReport::DBUtil->new(DBNAME => 'XREPORT') unless ($ep->{dbc});
   
   # my $updTSQL = qq{
	     # UPDATE $tbl SET $tbl.Status = DST.Status 
		 # FROM (VALUES $updvalues) AS DST(WorkId, Status)
		 # WHERE $tbl.WorkId = DST.WorkId;
		 
		 # UPDATE tbl_WorkQueue set SrvName = NULL
		 # FROM $tbl 
		 # WHERE tbl_WorkQueue.WorkId = $tbl.WorkId
   # };
   my $updvalues = '(VALUES'
                 . '('.join('),(', map { join(',', @{$_}{@varlist}) } @{$statuslist} )
				 . ')) AS DST('.join(', ', @varlist). ')';
   my $updTSQL = qq{
	     UPDATE $tbl SET $tbl.Status = DST.Status 
		 FROM $updvalues
		 WHERE $tbl.WorkId = DST.WorkId;
   };
   i::logit("Updating temp table: $updTSQL");
   $ep->{dbc}->dbExecute_NORETRY($updTSQL);
}

sub sendFile {
      my ($ep, $lclcfg, $lclfn, $outsfx) = @_;
	  
      my ($serverlist, $tgtdir, $tgtpfx) = @{$lclcfg}{qw(tgtserver tgtpath tgtpfx)};
      my ($ssh_pk, $ssh_usr, $ssh_passphrase) = @{$lclcfg->{sshuser}}{qw(pk name passphrase)};
      $ssh_pk =~ s/\//\\/g;
      my $tgtfn = join('.' , ($tgtpfx, $ep->{timestamp}, $outsfx));
	  my $tgtfqdn = "$tgtdir/$tgtfn";
      i::logit("File $lclfn ready to xmit into $tgtfqdn - ".(-s $lclfn)." bytes");

      unless ( exists($ep->{sftp}) ) {
        my $sock;
        foreach my $server ( @$serverlist ) {
          $sock = IO::Socket::INET->new($server);
          last if $sock;
          i::logit("connect to server $server failed - $! - $^E - trying next");
        }
        die "Unable to connect to servers" unless $sock;
        my $ssh2 = Net::SSH2->new(($main::debug ? (trace => -1) : ()) );						
        $ssh2->debug($main::debug || 0);
        $ssh2->connect($sock) or $ssh2->die_with_error( "SSH2 Connection failed - ");

        $ssh2->auth_publickey( $ssh_usr, undef, $ssh_pk, $ssh_passphrase) or $ssh2->die_with_error( "Authentication failed using $ssh_pk - ");
						  
        my $sftp = Net::SFTP::Foreign->new(ssh2 => $ssh2, backend => 'Net_SSH2');
        $sftp->error and
               die "Unable to stablish SFTP connection: ". $sftp->error;	
	    $ep->{sftp} = $sftp;
	  }

	  #return "-rw-rw-rw- $tgtfqdn 99 99"; 
	  i::logit("lclfn = $lclfn, tgtfqdn = $tgtfqdn");
      $ep->{sftp}->put($lclfn, $tgtfqdn) or die "put failed: " . $ep->{sftp}->error;
      my $ls = $ep->{sftp}->ls($lclcfg->{tgtpath}, wanted => qr/$tgtfn/, realpath => 1)
                            or die "unable to list File $tgtfn: ".$ep->{sftp}->error;
      $main::debug && i::logit("REMOTE ENTRY: $_->{longname} == $_->{realpath}") for (@$ls);
      my $rmtentry = $ls->[0]->{longname};
      return $rmtentry;	  
}

my ($srvname, $userlib, $workdir) = @{$XReport::cfg}{qw(SrvName userlib workdir)};
$main::logger = XReport::Logger->new($srvname) unless $main::logger; 

my ( $exec_workdir, $jrid, @wparms ) = @ARGV;
my ($ep, $jr) = ({srvname => $srvname}, undef);

if ( $jrid !~ /^\d+$/ ) {
   ($ep->{jrn}) = split(/\./, basename($jrid), 2);
}
else {
   $jr = XReport::JobREPORT->Open($jrid);
   @{$ep}{qw(jrid jrn xfst uref)} = $jr->getValues(qw(JobReportId JobReportName XferStartTime JobReportDescr));
   my $workdir = $XReport::cfg->{workdir}."\\".$srvname;
   mkdir $workdir unless (-e $workdir);
   die "unable to allocate $workdir" unless (-e $workdir && -d $workdir);
   $main::logger->AddFile( 'BATCHLOG', $jr->getFileName('LOGFILE') );
   $jr->deleteElab();
}
$ep->{timestamp} = sprintf('%s%s%s_%s%s%s', split(/[\-: \.]/, 
                           ($jr ? $ep->{xfst}
						        : POSIX::strftime('%Y-%m-%d %H:%M:%S', localtime())
                           )));

my $lclcfg = XML::Simple::XMLin("$userlib/rptconf/$ep->{jrn}.xml"
                               , ForceArray => [qw(tgtserver workparm)]
                               , Variables => $XReport::cfg, VarAttr => 'name', ContentKey => '-content')
							   ->{VpoSftp};
$lclcfg->{exec_workdir} = $exec_workdir;
@{$ep}{qw(serverlist tgtdir tgtpfx)} = @{$lclcfg}{qw(tgtserver tgtpath tgtpfx)};
@{$ep}{qw(maxitems tow wqclause)} = @{$lclcfg->{workparm}}{qw(maxitems TypeOfWork wqselection)};
$ep->{maxitems} = 500 unless $ep->{maxitems};

#die Data::Dumper::Dumper(\{lcfg => $lclcfg, ep=> $ep});

#my $zip2xmit = newZipFile($ep, $lclcfg, $jr);
while ( 1 ) { 
  my $rows = getJobsToProcess($ep) ;
  last unless scalar(@{$rows});
	
  my $reqcnt = 0;
  my @statuslist = ();
  my $zip2xmit = newZipFile($ep, $lclcfg, $jr);
  while ( scalar(@{$rows}) ) {
	my $row = shift @{$rows};
	my ($mdfn, $docfn, $md);
	my ($workid, $JRID) = @{$row}{qw(WorkId JobReportId)};
	my $endstatus = 31;
	eval { ($mdfn, $docfn, $md) = processRequest($JRID); };
	my $errmsg = $@;
	i::logit("Process for request $JRID failed - $errmsg") if $errmsg;
	
	if ( !$errmsg ) {
	   my $doczfn = $md->{NomePdf};
	   i::logit("adding $docfn to delivery archive as $doczfn");
	   my $member = $zip2xmit->addFile($docfn, $doczfn, COMPRESSION_STORED);
	   if ( $member ) {
		 (my $xmlfn = $doczfn) =~ s/\.pdf$/\.xml/ig;
		 i::logit("adding $xmlfn to delivery archive");
		 my $xmlmem = $zip2xmit->addFile($mdfn, $xmlfn);
		 $zip2xmit->removeMember($member) unless $xmlmem;
		 if ( $xmlmem ) {
		   $endstatus = 18;
		   $reqcnt += 1;
		   
		 }
	   }
	}
	push @statuslist, { WorkId => $workid, Status => $endstatus };
  }
  
  if ( $reqcnt ) {
	my $zipname = $ep->{zipname};
	my $zipcount = scalar(@{$ep->{ziplist}}) + 1;
	unless ( $zip2xmit->writeToFileNamed( $zipname ) == AZ_OK ) {
		diewithmsg("Error during store of $zipname")
	}
	my $rmtinfos = '';
	eval { $rmtinfos = sendFile($ep, $lclcfg, $zipname, "\#$zipcount.zip"); };
	my $xmiterr = $@;
	if ( $xmiterr ) {
	   #unlink $zipname;
	   #diewithmsg("aborting proceess due to send failed for $zipname - $xmiterr");
	   i::logit("send failed for $zipname - $xmiterr");
	}
	else {
	  eval { updateTempTable $ep, \@statuslist; };
	  my $sqlerr = $@;
	  if ( $sqlerr ) {
		 i::logit("update temp table error - $sqlerr");
		 unlink $zipname;
	  }
	  else {
		 push @{$ep->{ziplist}}, [ basename($zipname), -s $zipname, $rmtinfos,
		       [ map { [$_->fileName(), $_->uncompressedSize()] } ($zip2xmit->members()) ] ];
	  }
	}
	$reqcnt = 0;
  }
  else {
      my $tbl = '#zipmap'.scalar(@{$ep->{ziplist}});
      my $updTSQL = qq{
	     UPDATE $tbl SET $tbl.Status = 17; 
      };
      i::logit("Updating temp table: $updTSQL");
      $ep->{dbc}->dbExecute_NORETRY($updTSQL);
	  my $sqlerr = $@;
	  if ( $sqlerr ) {
		 i::logit("update temp table error - $sqlerr");
	  }
      push @{$ep->{ziplist}}, [ ];
  }
  last;
}

if ( scalar(grep { scalar(@{$_}) } @{$ep->{ziplist}} ) ) {
    my $ziplist = [ grep { scalar(@{$_}) } @{$ep->{ziplist}} ];
	i::logit("Creating eot file: ", map { join("\t", @{$_}) } @{$ziplist});
	(my $tfname = $jr->getFileName('DATAOUT', 0)) =~ s/\.pdf$/\.dat/ if ( $jr && ref($jr) );
    $tfname = POSIX::strftime("$exec_workdir/$ep->{tgtpfx}_%Y%m%d_%H%M%S.\#0.dat", localtime()) unless ( $tfname );
    my $tfh = IO::File->new(">$tfname");
    $tfh->binmode();
    while ( scalar(@$ziplist) ) {
       my $zipinfo = shift @{$ziplist};
       print $tfh join("\t", @{$zipinfo}[0..2]), "\n";
       print $tfh ('=' x 7).'> '.join("\n".('=' x 7).'> ', map { join("\t", @{$_}) } @{$zipinfo->[3]})."\n";
    }
    $tfh->close();
	my $tinfos;
    eval { $tinfos = sendFile($ep, $lclcfg, $tfname, 't'); };
    my $xmiterr = $@;
	if ( $xmiterr ) {
	   i::logit("send failed for $tfname - $xmiterr");
	}
	else {
# my $tabunion = join(' UNION ALL ', ( map { "SELECT * FROM #zipmap$_" } (0..$#{$ep->{ziplist}}) ), 'SELECT * FROM #zipmap'. $zipid) ;
      my $tabunion = join(' UNION ALL ', map { "SELECT * FROM \#zipmap$_" } (0..$#{$ep->{ziplist}}) );
#        UPDATE tbl_WorkQueue set tbl_WorkQueue.Status = TWQ.Status, tbl_WorkQueue.TypeOfWork = 32, tbl_WorkQueue.SrvName = NULL 
	  my $mainupd = qq{
        BEGIN TRANSACTION;

        UPDATE tbl_WorkQueue set tbl_WorkQueue.Status = TWQ.Status, tbl_WorkQueue.SrvName = TWQ.SrvName 
	    FROM ( $tabunion ) TWQ 
        WHERE TWQ.WorkId = tbl_WorkQueue.WorkId;
	  
        UPDATE tbl_JobReports SET tbl_JobReports.Status = TWQ.Status, tbl_JobReports.SrvName = TWQ.SrvName,
		       tbl_JobReports.UserTimeElab = '$ep->{xfst}', tbl_JobReports.XrefData = '$ep->{jrid}',
			   tbl_JobReports.ElabStartTime = TWQ.ElabStartTime, tbl_JobReports.ElabEndTime = GETDATE()
	    FROM ( $tabunion ) TWQ 
        WHERE tbl_JobReports.JobReportId = TWQ.JobReportId;

        COMMIT;
	  };
	  i::logit("Now Updating main tables: $mainupd");
	  $ep->{dbc}->dbExecute_NORETRY( $mainupd );
#	  $tfh = IO::File->new(">>$tfname");
#      $tfh->binmode();
#      print $tfh "\n", join("\t", basename($tfname) . '.t', -s $tfname . '.t', $tinfos);
#      $tfh->close();
	}
}

$ep->{sftp}->disconnect() if $ep->{sftp};


# print Dumper(\{ sl => $serverlist, td => $tgtdir, tp => $tgtpfx, pk => $ssh_pk
            # , SrvName => $srvname, tow => $tow, tgtfqdn => $tgtfqdn
            # , usr => $ssh_usr,pw => $ssh_passphrase, lim => $maxitems
			# , jobparms => $rows, ep => $ep});
			
insertLR(@{$ep}{qw(jrid jrn xfst uref)}, '');

$main::logger->RemoveFile('BATCHLOG') if $main::logger && ref($main::logger);
