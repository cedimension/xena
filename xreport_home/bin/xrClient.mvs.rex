/* Mode: REXX *******************************************************
 * @(#) $Id: xrSockCli.rex,v 1.2 2001/03/19 18:45:38 mpezzi Exp $
 *
 * Copyrights(c) EURISKOM s.r.l.
 ********************************************************************/

server_port = 8777
server_address = 10.254.14.242
trace 'n'
debug = 'N'

crlf = x2c('25')
parse arg xrQueue server_address server_port .
if server_address = '' then server_address = "172.26.3.55"
if server_port = '' then server_port = "8777"
SMFID = mvsvar('SYSSMFID')
address TSO "ALLOC F(XRXFOUT) SYSOUT(a)"
A = outtrap('LSDD.')
address TSO "LISTA ST SY"
B = outtrap('OFF')
do i = 2 to LSDD.0
  parse var LSDD.I NMDS nmdd .
  if NMDS = 'NULLFILE' then iterate
  i = i + 1
  parse var LSDD.I NMDD DISP .
  if NMDD = 'XRXFOUT' then leave
end
if NMDD <> 'XRXFOUT' then,
  call enditall '', '', 'unable to get XRXFOUT dd allocation', 12
else,
  parse var NMDS JUSER'.'JNAME'.'JID'.'.

say 'EXEC IDS: SMFID:' SMFID 'JNAME:' JNAME 'JUSER:' JUSER 'JID:' JID
ctrlfile = ''
ctrlfile = ctrlfile!!'H'!!SMFID!!crlf
ctrlfile = ctrlfile!!'P'!!JUSER!!crlf
ctrlfile = ctrlfile!!'J'!!'JES2.'JUSER'.'JNAME'.'JID'.?'!!crlf
ctrlfile = ctrlfile!!'fdf'!!JID!!SMFID!!crlf
ctrlfile = ctrlfile!!'N'!!'JES2.'JUSER'.'JNAME'.'JID'.?'!!crlf
datafile = ''
do forever
  parse pull TSIN
  if TSIN = '' then leave
  datafile = datafile!!TSIN!!crlf
  end

call Connect2 JID, server_address, server_port

parse value SendMsg('09'x!!xrQueue!!crlf )  with response
call RecvMsg 10
parse value SendMsg('02'x!!length(ctrlfile)!!' cf'!!JID!!SMFID!!crlf ),
        with response
call RecvMsg 10
parse value SendMsg(ctrlfile)  with response
parse value SendMsg('00'x)  with response
call RecvMsg 10
parse value SendMsg('03'x!!length(datafile)!!' df'!!JID!!SMFID!!crlf ),
        with response
call RecvMsg 10
parse value SendMsg(datafile)  with response
parse value SendMsg('00'x)  with response
parse value RecvMsg(3600) with xrRetc xrMsg
if xrRetc = '' then,
  call enditall sockid, subtaskid, 'TimedOut', 8
else if xrRetc = 0 then,
  call enditall sockid, subtaskid, xrMsg, 0
else,
 call enditall sockid, subtaskid, xrMsg, xrRetc

exit 0

RecvMsg: procedure expose sockid subtaskid debug crlf
parse arg timeout .

parse value,
        Socket('Select', 'Read' sockid 'Write' 'Exception' sockid, timeout),
        with sockrc cnt 'READ' r_id 'WRITE' w_id 'EXCEPTION' e_id
if cnt = 0 then,
   return ''

if r_id = sockid then,
   parse value Socket('Recv', sockid, 32760) with sockrc data_length msgIn
else
  call enditall,
  sockid, subtaskid,  "fatal error, RECV WAIT received invalid response", 12

if debug = 'Y' then do
  say "RECV RC:" sockrc "LEN:" data_length "MSG:" msgIn
  say "HEX MSG:" c2x(msgIn)
  end

if sockrc /= 0 then,
  call enditall sockid, subtaskid,  "fatal error, RECV Msg 1 got rc=" sockrc, 12

do forever
 /*
 lfpos = pos(crlf, msgIn)
 parse var msgIn Climsg =(lfpos) nmsgIn
 parse var CliMsg msgId Msg
if debug = 'Y' then,
  say "HEX CL1:" lfpos c2x(msgId) "-:-" c2x(Msg)
  */
 saveIn = msgIn
 parse var msgIn CliMsg (crlf) msgIn
 parse var CliMsg mc 2 msgId Msg
if debug = 'Y' then,
  say "HEX CLI:" lfpos c2x(msgId) "-:-" c2x(Msg)
  /*
   if msgId /= 'INFO' then do
      msgIn = CliMsg!!crlf!!MsgIn
      leave
      end
   else say "xrInfo -" Msg
   if msgIn = '' then,
      return RecvMsg(timeout)
  end
  */
 if mc /= '01'x then do
    msgIn = saveIn
    leave
    end
 else say "xr"msgId Msg
 if msgIn = '' then,
    return RecvMsg(timeout)
end
if debug = 'Y' then,
  say "HEX CTL:" c2x(msgIn)
return c2d(substr(msgIn, 1, 1)) substr(msgIn, 2)

SendMsg: procedure expose sockid subtaskid debug crlf
parse arg msgOut
parse value Socket('Send', sockid, msgOut) with sockrc msgIn

if sockrc /= 0 then
  call enditall sockid, subtaskid, " fatal error, SEND got rc=" sockrc, 12
return msgIn

Connect2: procedure expose sockid subtaskid debug crlf
parse arg session, server_address, server_port .

parse value Socket('Initialize', session) with sockrc subtaskid str2
if sockrc /= 0 then,
  call enditall '', '', "fatal error, SOCKET INITIALIZE got rc=" sockrc, 12

/* activate a socket, get a socket id          */
str = Socket('Socket', 'af_inet', 'stream', 'tcp')
parse var str sockrc sockid
if sockrc /= 0 then,
  call enditall '', subtaskid, " fatal error, SOCKET ACTIVATE got rc=" sockrc, 1

/* turn on EBCDIC to ASCII translation         */
sockrc = Socket('SetSockOpt', sockid, 'sol_socket', 'SO_ASCII', 'on')
if sockrc /= 0 then,
  call enditall '', subtaskid, "fatal error, SETSOCKOPT got rc=" sockrc, 12

server_info = 'AF_INET'  server_port  server_address
sockrc = Socket('Connect', sockid, server_info)
if sockrc /= 0 then
  call enditall '', subtaskid, "fatal error, SOCKET CONNECT got rc=" sockrc, 12

return

enditall:
parse arg endsock, endsubtsk, endmsg, endrc
if endsock <> '' then,
   call socket 'Close', endsock
if endsubtsk <> '' then,
   call socket 'Terminate', endsubtsk

 do while endmsg <> ''
  parse var endmsg mc 2 msgIn (crlf) endmsg
  if mc == '01'x then,
    say "xr"msgIn
  else,
    say mc!!msgIn
  if endmsg = '' then leave
 end

if endrc == '' then,
   endrc = 0
exit endrc

