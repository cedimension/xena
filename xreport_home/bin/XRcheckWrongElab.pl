##################################################################################
#
# XRcheckWrongElab - Script per identificare i Reports "Sporcati" da un errata elaborazione
#                    del CDAM
#
##################################################################################

use strict;
use lib "$ENV{XREPORT_HOME}/perllib";

use XReport;
use XReport::Util;
use XReport::Logger;
use XReport::DBUtil;
use XReport::QUtil;
use XReport::JobREPORT;
use Data::Dumper;



( my $workdir = ( $ARGV[0] eq '.' ? `cd` : $ARGV[0] ) ) =~ s/\n$//gs;
my $enva = $ARGV[1] || die "Environment not set!!!!!";

my $parms->{inferiore} = $ARGV[2] || die "Inferiore not set";
$parms->{superiore} = $ARGV[3] || die "Superiore not set";

my @JRIDtoClean;
# Svuota la directory di lavoro activate in a dispatcher
#if ( -d $workdir ) {
#	my @flist = glob $workdir . '\*';
#	&$logrRtn( "main - deleting now\n", join( "\n", @flist ) );
#	unlink @flist;
#}

# Definisce la subroutine per i messaggi di debug
my $debgRtn = sub {
	warn map { $_ ? ref($_) ? Dumper($_) : $_ : 'NULL' } @_, "\n";
};
# Definisce la subroutine per i messaggi di log
$logrRtn = sub {
	use File::Basename;
	my $localTime = scalar localtime();
	print "$localTime - ".basename($0)." - ", map { $_ ? ref($_) ? Dumper($_) : $_ : 'NULL' } @_, "\n";
};
&$logrRtn("main - Load xml ".Dumper($XReport::cfg->{JobsIdsToChecks}));

my $reportfn = "$workdir/$enva-".$parms->{superiore}."--".$parms->{inferiore}."-checkedReports.txt";

my $report = new FileHandle(">>$reportfn");
print "PROOOOOOOOOOOOVABBBBBBBBBB", "\n", $report;
my $rptsep = "-" x 50;
my @reportrow = ();
format activityrep_hdr =
@<<<<<<<<<<<<<<<<<<<<<<<< @|||||||||||||||||||||||||||||||||||         PAGE @0###
"".localtime()."",        "Xena Check Activity Reports", $%
Env JobReportid      DateRif             SizeMax       TotSize
---------------------------------------------------------------------------------
.
format activityrep =
@<<<@<<<<<<<<<@<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<< @<<<<<<<<<<
@reportrow
.
format_name $report "activityrep";
format_top_name $report "activityrep_hdr";


my $query  = $XReport::cfg->{sqlprocs}->{JobsIdsToChecks};
$query =~ s/#{([^\{]+)}#/$parms->{$1}/g;
my $dbc;
print "OLEEEEEEE";
print "qwuery\n",$query;
sleep 5 ;
print "dollaro0\n " , $0;
eval{
	$dbc = XReport::DBUtil::getHandle ('XREPORT');
};
die ("ERRORE SQL $@") if($@);

my $dbr = $dbc->dbExecute_NORETRY($query);
while(!$dbr->eof()){
	my $JRID = $dbr->Fields()->Item('JobReportId')->Value() || die "Unable to get JobReportId\n";
	
	push @JRIDtoClean , $JRID;
		
}continue {$dbr->MoveNext();}

&$logrRtn("main - Start Analyzing ");
my ($totSize ,$totRepo) = (0, 0);

write $report;
my ($jr, $ijrar, $reportfh);
#foreach my $id (@JRIDtoClean){
while(scalar(@JRIDtoClean)){
    my $id = shift(@JRIDtoClean);
	&$logrRtn("main - Open JobReportId $id");
	eval {
      $jr = XReport::JobREPORT->Open($id, 1);
	  $ijrar = $jr->get_INPUT_ARCHIVE();
      $reportfh = $ijrar->get_INPUT_STREAM($jr);
    };
    if($@){
        print "skipping Jid -->$id<--for error -- $@" && next; 
      }
    (die "Invalid input file handle type" && next) unless ref($reportfh) eq 'XReport::Storage::IN';
	my $maxrl = (grep /^maxlrec_detected$/i, keys %{$reportfh->{cinfo}})[0] || 0 ;
    my $maxdet = $reportfh->{cinfo}->{maxlrec_detected} || 0;
    print "dim--> ", $maxdet, "\n" if($maxrl);
    next if ($maxdet < 400);
	my ($inB, $outB, $XferStartT, $remoteFN) = $jr->getValues(qw(XferInBytes ElabOutBytes XferStartTime RemoteFileName)); 
	#$jr->deleteAll(); 
	$totSize += $inB+$outB; $totRepo  += 1;
	@reportrow = ($enva, $id , $XferStartT , $maxdet, $totSize);
	write $report;
    $report->flush();
	&$logrRtn("main - Checked JobReportId $id");
    $jr->Close();

}
@reportrow = ("---", "---------", "--Fine Elaborazione--", "--Totale Totale Errati--", "--Totale By--");
write $report;
@reportrow = ("---", "---------", "---------------------" , $totRepo, $totSize);
write $report;
close($report);
&$logrRtn("main - Cleaning ended");
exit 1;


