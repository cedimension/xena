use strict vars;

use lib("$ENV{'XREPORT_HOME'}/perllib");

use strict;

use Data::Dumper;

use Compress::Zlib;
use Net::FTP;
use File::Basename;

use XReport;
use XReport::DBUtil;
use XReport::Util; use XReport::Util qw(InitServer TermServer);

my ($SrvName, $basedir, $perl, $perllib, $gzip, $todir, @hosts); 

INIT_ALL();

sub DateTime {
  my ($fmFile, $DD, $MM, $YY, $HHMMSS) = (shift, '', '', '');
    
  return "20$YY$MM$DD$HHMMSS"
    if 
  ($DD, $MM, $YY, $HHMMSS) = $fmFile =~ /\.D(\d{2})(\d{2})(\d{2})\.T(\d{6})/; return GetDateTime();
}

sub FtpLoop {
  &$logrRtn("FtpLoop ENTRY");

  my %args = (
	'maxLS'   => 3,
    'sleepLS' => 60,
    'debug' => 0,
	%{$_[0]}
  );
  
  my ($host, $hostaddr, $hosttype, $user, $pass, $cwd, $prfx, $proc, $maxLS, $sleepLS, $debug) 
   =
  @args{qw(host hostaddr hosttype user pass cwd prfx proc maxLS sleepLS debug)};  
  
  my ($ftp, $rc, $fmFile, $toFile) = (); $debug = 0;
  
  $ftp = Net::FTP->new($hostaddr, Debug => $debug) or do { return -1};

  $ftp->login($user, $pass) or do { $ftp->quit(); return -1; };
  
  my $MVS = $hosttype =~ /MVS/i;

  $ftp->cwd( $MVS ? "'$cwd'" : $cwd ) or do { $ftp->quit(); return -1; };
  
  for (1..$maxLS) {
    my $lsprfx = $prfx; $lsprfx = substr($lsprfx,0,7) if length($lsprfx) >= 8; 

	$ftp->pwd() ne '' or do { $ftp->quit(); return -1; }; #test if disconnected

    my @files = grep(/^$prfx.*/io, $ftp->ls()); # ls $prfx\* gives nothing with IBM FTP 
    
	&$logrRtn("ls code = ". $ftp->code());

	do { $ftp->quit(); return -1; }
	 if 
    !scalar(@files) and $ftp->code() ne "250" and $ftp->code() ne "550";

    for $fmFile (@files) {
      do {
        $ftp->quit();
	    die("ListError: FILE $fmFile DOESN'T START WITH $prfx !!.");
	  }
	  if $fmFile !~ /^$prfx.*/;

      my ($dt, $BUNDLEID, $dbr); $dt = DateTime($fmFile); 
	  
	  $dbr = dbExecute(
	    "SELECT * from tbl_ctd_INBUNDLES " .
		"WHERE Host='$host' AND Dir='$cwd' AND FileName='$fmFile'" 
	  );

	  if ( $dbr->eof() ) {
	    $toFile = "$fmFile\.$dt\.????????.PENDING"; 
	    dbExecute(
	      "INSERT into tbl_ctd_INBUNDLES " .
		  "(Host, Dir, FileName, Status, LocalFileName, DateTimeString, HostAddr, SrvName, XferMode) " .
		  "VALUES " .
		  "('$host', '$cwd', '$fmFile', 0, '$toFile', '$dt', '$hostaddr', '$SrvName', 4)"
	    );
	    $dbr = dbExecute(
	      "SELECT * from tbl_ctd_INBUNDLES " .
		  "WHERE Host='$host' AND Dir='$cwd' AND FileName='$fmFile'" 
	    );
	    if ( $dbr->eof() ) {
	      &$logrRtn("DataBase INSERT Error for file $toFile");
          $ftp->quit(); die("DatabaseError: INSERT Error for file $toFile");
	    }
	    $BUNDLEID = $dbr->GetFieldsValues('BUNDLEID');
	    $toFile = "$fmFile\.$dt\.$BUNDLEID"; 
		dbExecute(
	      "UPDATE tbl_ctd_INBUNDLES " .
		  "SET LocalFileName='$toFile.IN.gz' " .
		  "WHERE Host='$host' AND Dir='$cwd' AND FileName='$fmFile'"
	    );
	  }

	  else {
	    $toFile = basename($dbr->GetFieldsValues('LocalFileName')); 
		$toFile =~ s/\.IN\.gz$//i;
	    $BUNDLEID = $dbr->GetFieldsValues('BUNDLEID');
	  }

	  $dbr->Close(); $dbr = undef;

      die "INVALID TOFILE VALUE \"$todir/$toFile\" " if ($todir eq '' or $toFile eq '') ;

      my $toFile_ = "$todir/$toFile.IN.gz"; unlink glob("$toFile_\*");
      
      #-- begin transfer -----------------------------------------------------
	  
	  $ftp->quot("mode c") or do { $ftp->quit(); return -1; };
      $ftp->binary() or do { $ftp->quit(); return -1; };

      &$logrRtn("TRANSFER START $fmFile $toFile_");

	  my $INPUT = $ftp->retr($fmFile); $rc = $ftp->code();
	  if ( !$INPUT or ($rc != 125 and $rc != 225) ) {
	    if ( $rc == 450 ) {
		  &$logrRtn("FILE NOT AVAILABLE \"$fmFile\" ftpcode=".$rc);
          if ($MVS) {
		    $ftp->quot("mode s") or do { $ftp->quit(); return -1; };
            $rc = $ftp->ascii() or do { $ftp->quit(); return -1; };
          }
		  next;
		}
		&$logrRtn("ERROR AT CMD RETR for file \"$fmFile\" ftpcode=".$rc);
        $ftp->quit(); return -1;
      }
	  my $OUTPUT = gzopen("$toFile_\.PENDING", "wb");
      if ( !$OUTPUT ) {
         die "ERROR AT GZOPEN for file \"$toFile\" $!"; 
      }
      
      my ($buff, $br, $tbr, $bw, $tbw);
      while(1) {
        $br = $INPUT->read($buff, 32768); last if !$br; $tbr += $br;
        $bw = $OUTPUT->gzwrite($buff); 
        if ($bw != $br) {
          my $atOff = $OUTPUT->gztell(); $atOff = 0x100000000 + $atOff if $atOff < 0;
          &$logrRtn("BYTES WRITTEN $bw/$atOff NOT EQUAL TO BYTES READ $br"); $ftp->quit(); return -1;
        }
        $tbw += $bw;
      }
      if ($tbw != $tbr) {
        my $atOff = $OUTPUT->gztell(); $atOff = 0x100000000 + $atOff if $atOff < 0; 
        &$logrRtn("TOTAL BYTES WRITTEN $tbw/$atOff NOT EQUAL TO TOTAL BYTES READ $tbr");
		$OUTPUT->gzclose(); $ftp->quit(); return -1;
      }
      $rc = $OUTPUT->gzclose(); die("GZCLOSE ERROR DETECTED. gz=$rc") if $rc != 0;
      $INPUT->close() or die("UNABLE TO CLOSE FTP DATASTREAM"); 
      $rc = $ftp->code();
      if ( $rc != 226 and $rc != 250 ) {
        &$logrRtn("TRANSFER ENDED WITH ERROR \"$fmFile\" \"$toFile\" ftpcode=$rc");
        unlink "$toFile_\.PENDING";
        $ftp->quit();
        return -1;
      }
      ### test for total_in e total_out /// test for filesize = total_out+18


	  $rc = rename("$toFile_\.PENDING", "$toFile_") ;

	  $rc = $ftp->quot("mode s") or do { $ftp->quit(); return -1; };
      $rc = $ftp->ascii() or do { $ftp->quit(); return -1; }; 

      #-- end transfer -------------------------------------------------------
	  
      ### update inbundle record in database
	  dbExecute(
	    "UPDATE tbl_ctd_INBUNDLES " .
		"SET Status=16 WHERE Host='$host' AND Dir='$cwd' AND FileName='$fmFile'"
	  );
	  
	  do {
	    &$logrRtn("DELETE ERROR for $fmFile. NOW ENDING !! ".$ftp->code());
        $ftp->quit();
		die("FtpError: DELETE Error ".$ftp->code());
	  }
      if !$ftp->delete($fmFile);

	  &$logrRtn("FILE $fmFile DELETED SUCCESSFULLY.");
	  
      ### insert into workqueue
	  dbExecute("
	    INSERT INTO tbl_WorkQueue
		(ExternalTableName, ExternalKey, TypeOfWork, Status)
		VALUES ('tbl_ctd_INBUNDLES', $BUNDLEID, 1, 16)
	  ");
    }

    &$logrRtn("SLEEPING NOW $sleepLS seconds"); sleep $sleepLS;
  }

  $ftp->quit(); return 1;
}

eval {
  my ($host, $rc);
  while(1) {
    $rc = -1;
    for $host (@hosts) {
      $rc = FtpLoop($host) > 0 ? 1 : $rc;
    }
    &$logrRtn("FtpLoop END $rc");
    sleep ( ($rc < 0) ? 180 : 60 );
  }
};
&$logrRtn($@);

#######################################################################
#
#######################################################################

sub INIT_ALL {
  InitServer(); $SrvName = getConfValues('SrvName');

  $todir = getConfValues('XferArea')->{'CtdBundlesIN'};
  
  ($basedir, $perl, $perllib, $gzip, my $hosts) 
    = 
  getConfValues('basedir', 'perl', 'perllib', 'gzip', 'hosts');
  
  $hosts = $hosts->{host};

  if ( ref($hosts) eq 'HASH' ) {
    @hosts = ($hosts);
  }
  else {
    @hosts = @$hosts;
  }

  print join("\n", $todir, Dumper(@hosts)), "\n";
}
