#!/usr/bin/perl -w

package CTDParser;

use strict qw(vars);

use Data::Dumper;
use IO::File;
use File::Basename;
use XML::Simple;
use Digest::SHA1;
use Encode qw(from_to);
use Convert::EBCDIC;
use Data::Alias qw(alias copy);
use Storable 'dclone';
#use XReport;
use XReport::METABASE::SQLServer;
#use XReport::JobREPORT;
use re 'eval';
our $EBCDIC_ITALIAN_To_ISO8859_1 = 
#   0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
 '\x00\x01\x02\x03\x9C\x09\x86\x7F\x97\x8D\x8E\x0B\x0C\x0D\x0E\x0F'.  # 0
 '\x10\x11\x12\x13\x9D\x85\x08\x87\x18\x19\x92\x8F\x1C\x1D\x1E\x1F'.  # 1
 '\x80\x81\x82\x83\x84\x0A\x17\x1B\x88\x89\x8A\x8B\x8C\x05\x06\x07'.  # 2
 '\x90\x91\x16\x93\x94\x95\x96\x04\x98\x99\x9A\x9B\x14\x15\x9E\x1A'.  # 3
 '\x20\xA0\xE2\xE4\x7B\xE1\xE3\xE5\x5C\xF1\xB0\x2E\x3C\x28\x2B\x21'.  # 4
 '\x26\x5D\xEA\xEB\x7D\xED\xEE\xEF\x7E\xDF\xE9\x24\x2A\x29\x3B\x5E'.  # 5
 '\x2D\x2F\xC2\xC4\xC0\xC1\xC3\xC5\xC7\xD1\xF2\x2C\x25\x5F\x3E\x3F'.  # 6
 '\xF8\xC9\xCA\xCB\xC8\xCD\xCE\xCF\xCC\xF9\x3A\xA3\xA7\x27\x3D\x22'.  # 7
 '\xD8\x61\x62\x63\x64\x65\x66\x67\x68\x69\xAB\xBB\xF0\xFD\xFE\xB1'.  # 8
 '\x5B\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\xAA\xBA\xE6\xB8\xC6\xA4'.  # 9
 '\xB5\xEC\x73\x74\x75\x76\x77\x78\x79\x7A\xA1\xBF\xD0\xDD\xDE\xAE'.  # A
 '\xA2\x23\xA5\xB7\xA9\x40\xB6\xBC\xBD\xBE\xAC\x7C\xAF\xA8\xB4\xD7'.  # B
 '\xE0\x41\x42\x43\x44\x45\x46\x47\x48\x49\xAD\xF4\xF6\xA6\xF3\xF5'.  # C
 '\xE8\x4A\x4B\x4C\x4D\x4E\x4F\x50\x51\x52\xB9\xFB\xFC\x60\xFA\xFF'.  # D
 '\xE7\xF7\x53\x54\x55\x56\x57\x58\x59\x5A\xB2\xD4\xD6\xD2\xD3\xD5'.  # E
 '\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\xB3\xDB\xDC\xD9\xDA\x9F'   # F
 ;
 
 our $EBCDIC_GERMAN273_To_ISO8859_1 = 
# 	0	1	2	3	4	5	6	7	8	9	A	B	C	D	E	F
 '\x00\x01\x02\x03\x9C\x09\x86\x7F\x97\x8D\x8E\x0B\x0C\x0D\x0E\x0F'.  #0
 '\x10\x11\x12\x13\x9D\x85\x08\x87\x18\x19\x92\x8F\x1C\x1D\x1E\x1F'.  #1
 '\x80\x81\x82\x83\x84\x0A\x17\x1B\x88\x89\x8A\x8B\x8C\x05\x06\x07'.  #2
 '\x90\x91\x16\x93\x94\x95\x96\x04\x98\x99\x9A\x9B\x14\x15\x9E\x1A'.  #3
 '\x20\xA0\xE2\x7B\xE0\xE1\xE3\xE5\xE7\xF1\xC4\x2E\x3C\x28\x2B\x21'.  #4
 '\x26\xE9\xEA\xEB\xE8\xED\xEE\xEF\xEC\x7E\xDC\x24\x2A\x29\x3B\x5E'.  #5
 '\x2D\x2F\xC2\x5B\xC0\xC1\xC3\xC5\xC7\xD1\xF6\x2C\x25\x5F\x3E\x3F'.  #6
 '\xF8\xC9\xCA\xCB\xC8\xCD\xCE\xCF\xCC\x60\x3A\x23\xA7\x27\x3D\x22'.  #7
 '\xD8\x61\x62\x63\x64\x65\x66\x67\x68\x69\xAB\xBB\xF0\xFD\xFE\xB1'.  #8
 '\xB0\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\xAA\xBA\xE6\xB8\xC6\xA4'.  #9
 '\xB5\xDF\x73\x74\x75\x76\x77\x78\x79\x7A\xA1\xBF\xD0\xDD\xDE\xAE'.  #A
 '\xA2\xA3\xA5\xB7\xA9\x40\xB6\xBC\xBD\xBE\xAC\x7C\xAF\xA8\xB4\xD7'.  #B
 '\xE4\x41\x42\x43\x44\x45\x46\x47\x48\x49\xAD\xF4\xA6\xF2\xF3\xF5'.  #C
 '\xFC\x4A\x4B\x4C\x4D\x4E\x4F\x50\x51\x52\xB9\xFB\x7D\xF9\xFA\xFF'.  #D
 '\xD6\xF7\x53\x54\x55\x56\x57\x58\x59\x5A\xB2\xD4\x5C\xD2\xD3\xD5'.  #E
 '\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\xB3\xDB\x5D\xD9\xDA\x9F'  #F
;
 

our $translator = Convert::EBCDIC->new($EBCDIC_GERMAN273_To_ISO8859_1);

#sub i::logit { warn localtime().' - '.join('', @_)."\n"; }

sub XML::Simple::sorted_keys {
    my ($self, $name, $href) = @_;
    my $hkeys = {map {$_ => $_ } keys %{$href} };
    my @klist = ();
    my $knownkeys = {
          jobreport => [qw(cateList linePos rect exec index report page)]
        , page      => [qw(category bundles holddays contid test vars op)]
        , vars      => [qw(constant unpack concat)]
        , op        => [qw(test bundles indexes getVars)]
        , receiver  => [qw(DEBUG CASE)]
        , CASE      => [qw(MATCH ATTRIB ASSERT)]
        , 'index'   => [qw(table entries vars)]
    }; 

    push @klist, delete($hkeys->{name}) if ( exists($href->{name}) );
    if ( exists($knownkeys->{$name}) ) {
        foreach my $key ( @{$knownkeys->{$name}} ) {
            push @klist, delete($hkeys->{$key}) if ( exists($href->{$key}) );
        }
    }

    push @klist, keys %{$hkeys};
    return (@klist);    
}

sub new {
    my $class = shift;
    my $self = { @_ };
	#my $thispath = dirname $0;    
	my $thispath = $XReport::cfg->{'userlib'}.'/tree';
    (@{$self}{qw(filtervars varxlate tree bkupclasses defaultrename)}, my $exceptions, my $uassign) 
                = @{XMLin($thispath.'/CTDconfig.xml',
                        ForceArray => ['exceptions', 'split', 'skip', 'assignuser', 'ASSERT', 'skipre','update'],
                        KeepRoot => 0, 
                        KeyAttr => ['ctdcol', 'varname', 'name'], 
                        VarAttr => 'ctdcol',
                        ValueAttr => ['ctdjobmask'],
                        ContentKey => '-xrcol'
                        )}{qw(filtervars varxlate tree backupcategories defaultrename exceptions assignuser)};
   $self->{metabase} = XReport::METABASE::SQLServer->new(skiptables => 1);
   
   i::logit("PROCESS CONFIG LOADED - ", Dumper(\{STRUCT => $self, EXCEPTIONS => $exceptions, ASSIGNUSER => $uassign}));
   
   
   if ( exists($self->{tree}->{skip}) && (my $treeskip = delete($self->{tree}->{skip}) ) ) {
      my $skipmask = join('|', map { $_->{rmask}}  @{$treeskip});
      $self->{tree}->{skipmask} = qr/(?:$skipmask)/;
   }
   if ( exists( $exceptions->{forcemissing} ) ) {
      my $skipmask = join('|', map { $_->{jrnmask}}  @{$exceptions->{forcemissing}->{skip}});
      $self->{forcemissing}->{skip} = qr/(?:$skipmask)/;
   }
   if ( exists( $exceptions->{jobnmatch} ) ) {
#      my $axx = '';
      my $jobnmask = join('|', map { $_->{jobnmask} 
#                                     . "(?\{ '$_->{jobnmask}'; })" 
                                     . "(?\{ \$main::rematch = '$_->{jobnmask}'; })" 
                                   } sort { $b->{jobnmask} cmp $a->{jobnmask} } @{$exceptions->{jobnmatch}->{re}});
      $self->{jobnre} = qr/(?:$jobnmask)/x;
      $jobnmask = join('|', 
           map { $_->{jobnmask} } sort { $b->{jobnmask} cmp $a->{jobnmask} } @{$exceptions->{jobnmatch}->{skipre}});
      $self->{jobnre2skip} = qr/(?:$jobnmask)/;
   }
   my $code = "sub { my \$recipient = shift;\n";
   if (ref($uassign) eq 'ARRAY' && scalar(@$uassign)) {
     $code .= join("\n", map {
        "if (\$recipient =~ /".$_->{recipient}."/) { return ".$_->{xrcol}."; }"
                    } @$uassign) . "\n";
   }
   $self->{userassign} = eval $code."return \$recipient;\n}";
   
   die "User assign code generation failed - $@\n", Dumper(\{uassign => $uassign, code => $code}), "\n" if $@;
    #add parameter to manage table creation with postfix _AGG to manage agg tables .    
    $self->{tables} = $self->{metabase}->readTablesMetaData(
       initrows => 1,
       include => [ qw(JobReportNames ReportNames Os390PrintParameters 
                             VarSetsValues NamedReportsGroups FoldersRules Folders  
                             ProfilesFoldersTrees Profiles UsersProfiles Users UserAliases
                             BundlesNames LogicalReportsTexts) ]);
    
                        
    $self->{filtervarslist} = [ reverse keys %{$self->{filtervars}} ];
    $self->{matchkeys} = [qw(_S __ _C _D _F _W)];
    $self->{matchflds} = {};
    @{$self->{matchflds}}{@{$self->{matchkeys}}} = qw(STEPN DDNAM CLASS DEST FORMS WRTRN);
    
    #die "Print Missions prtmissdir directory not defined" unless $self->{prtmissdir};   
    die "Report  Defin. reportsdir directory not defined" unless $self->{reportsdir};   
    die "Out  Directory outputdir  directory not defined" unless $self->{outputdir};    

    my $ctdtree_fn = $self->{ctdtreefil} || die "ctdtree file name not defined";
    $self->{treefh} = new IO::File("<$ctdtree_fn") || die "Unable to open $ctdtree_fn";
    $self->{treefh}->binmode();

    #my $ctdperm_fn = $self->{ctdpermfil} || die "ctdperm file name not defined";
    #$self->{permfh} = new IO::File("<$ctdperm_fn") || die "Unable to open $ctdperm_fn";
    #$self->{permfh}->binmode();
    
    # my $indexfilr_fn = $self->{indexfile} || die "index file create not defined"; 
    # $self->{filecreatefh} = new IO::File(">$indexfilr_fn") || die "unable to create  CreateIndexes.sql  in \"$self->{outputdir}\"- $? - $!";
    # $self->{filecreatefh}->binmode();

    opendir( $self->{dirfh}, $self->{reportsdir} ) || die "unable to open Directory \"$self->{reportsdir}\"";
    #opendir( $self->{pmdirfh}, $self->{prtmissdir} ) || die "unable to open Directory \"$self->{prtmissdir}\"";
    
    return bless $self, $class;
} 



sub getFoldersList {
=pod

=head1 getFoldersList

partendo da un'arbitrario recipient(folder) (primo parametro) cerca tutti i livelli inferiori
con livello uguale a quello specificato nel secondo parametro di chiamata e restituisce un array
contenente tutti gli hash parent delle entrate corrispondenti al criterio

=cut 
    my $self = shift;
    my ($rootname, $lvl, $varname, $parents) = @_;
    die "Attempt to build $rootname varlist without FilterVar", 
            " - called by ", join('::', (caller())[0,2]), "\n" unless $varname; 
    die "do user lvl* called with not numeric $lvl ", 
            " - called by ", join('::', (caller())[0,2]), "\n" unless $lvl =~ /^\d+$/; 
    
    alias my $root = $self->{recipients}->{$rootname};
    i::logit("Called getFoldersList with $rootname , $lvl , $varname con figli".Dumper(scalar keys %{$root->{_chlds}})."\n Figli ::".Dumper(sort keys %{$root->{_chlds}} )) ;   
    foreach my $user ( keys %{$root->{_chlds}} ) {
        alias my $folder = $self->{recipients}->{$user};
        die "malformed folder: ", Dumper($folder), unless ($folder->{lvl} && $folder->{lvl} =~ /^\d+/ );
        $self->getFoldersList($folder->{name}, $lvl, $varname, $parents) 
               if ( $folder->{lvl} < $lvl && exists($folder->{_chlds}) && scalar(keys %{$folder->{_chlds}}) );
		##in order to process all folder under an existing level if  starting point is ROOT level added this condition to dig all levels of tree
		## all level bigger will be added 
		 i::logit("Try to getFoldersList with firstname $parents->{__firstname__} :: name $folder->{name} :: level $folder->{lvl} and passed level $lvl");
		$self->getFoldersList($folder->{name}, $folder->{lvl}, $varname, $parents) 
               if ( $parents->{__firstname__} eq 'ROOT' &&  $folder->{lvl} >= $lvl );
		#################################################################################################################	
	
=head2 process recipient synonims

we add an entry to varsetvalues for each synonym defined in the tree 
plus the recipient name itself

=cut
	#i::logit("getFoldersList\n user .. $user folderlvl :: $folder->{lvl} lvl :: $lvl "); 
		## ##in order to process all folder under an existing level if  starting point is ROOT level added the condition to dig all levels of tree
		## all level bigger will be added if child is equal or bigger than that level 
        if (($folder->{lvl} && $folder->{lvl} == $lvl) || ($parents->{__firstname__} eq 'ROOT' && $folder->{lvl} &&  $folder->{lvl} > $lvl)) { 
            $parents->{$root->{name}} = $root;
            if ( exists($folder->{filtervals}) && scalar(@{$folder->{filtervals}})) {
               foreach my $varval ( @{$folder->{filtervals}} ) { 
                  next unless $varval;
                  $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
                              {VarSetName => $folder->{name}, VarName => $varname, VarValue => $varval });
               }                
            }
            $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
                  {VarSetName => $folder->{name}, VarName => $varname, VarValue => $folder->{name} });
                  
            $self->{metabase}->addRows2Table($self->{tables}->{NamedReportsGroups}, 
                  {ReportGroupId => $folder->{name}, ReportRule => $parents->{__firstname__}, 
                                           FilterRule => $folder->{name}, FilterVar => $varname});
            $self->{metabase}->addRows2Table($self->{tables}->{FoldersRules},
                         {FolderName => $folder->{FolderName}, ReportGroupId => $folder->{name}});              
        }
    }
    return $parents;
}

sub getFilterVarName {
    my ($self, $jn) = (shift, shift);
    return undef unless $jn;
    my $filtervar = (grep { $jn =~ m/$self->{filtervars}->{$_}/ } @{$self->{filtervarslist}})[0];
    die "Filtervar not found for $jn" unless $filtervar && $filtervar ne '';
    return  $filtervar;
}

sub xlateCtdColumn {
=pod
=h2 translate column names

CTD index variables are named in such a way that provides some sort of explanation on 
what is expected to type in when searching. Is possible to normailize such names 
using elements ( "varxlate" ) of the xml configuration ("CTDConfig.xml").
e.g:
   <varxlate ctdcol="ctd var name" xrcol="XReport col name">
   
the original variable name is returned if not found in config   

=cut
    my $self = shift;
    alias my $vardescr = shift;
    if ( !exists($self->{varxlate}->{$vardescr}) ) {
        #warn "Index col $vardescr not managed\n";
        return $vardescr;
    }
    return $self->{varxlate}->{$vardescr};
}

sub reprec_R {
=pod

=head2 report definitions Record analyzer routines

=cut

    my ($self, $rline, $memlines) = (shift, shift, shift);

    my ( $jname, $owner, $catname, $var1 ) = unpack("A8 x20 A8 A20 A8", $rline);
    # category name can include special chars that ar not supported by xml - let's get rid of'em
    $catname =~ s/[^\d\w\s\*]/_/g;

    if ( exists($self->{currcat})) {
        my $category = delete($self->{currcat});
        #TODO verify following
#        $self->{cats}
#            ->{$category->{catname}} 
#                    = $category;
    }
    
    $self->{currcat} = {};
    $self->{defaults} = {};
    @{$self->{currcat}}{qw(jobname owner catname var1 defaults)} 
        = ( $jname, $owner, $catname, $var1, $self->{defaults});
    
    while ( scalar(@$memlines) && $memlines->[0] =~ /^\s*([^RN])(.*)$/ ) {
        # process records before N and up to next R
        my $result = $self->reprecDispatcher( shift @{$memlines}, $memlines );
        #return undef unless defined($result);
    }
    # at this point we have N|R|eof
    return $self ;
}

sub reprec_F {
=h2 Defaults statement

 defaults specifications contains default copies and recipient 
 for the jobs matched by the "R" record

F01SCARTI

=cut

    my ($self, $rline, $memlines) = (shift, shift, shift);
    @{$self->{defaults}}{qw(copies destuser)} = unpack("A2 x4 A8", $rline);
    return $self;
    
}

sub reprec_N { # ON Statement
=h2 "ON" statement

job attributes selection statement

NCLASS
NDSN
NTRNDSN

=cut

    my ($self, $rline, $memlines) = (shift, shift, shift);
    my $jobname = $self->{currcat}->{jobname};
    
    
    my $jrdefs = {};
    my $onlyjobsel = 0;
    while ( $self->{memlines}->[0] =~ /^X/ ) {
        (my $cont) = ((shift @{$self->{memlines}}) =~ /^.(.+?)\s*$/ )  ;
        $rline =~ s/\s+$//g;
        
        $rline .= ($rline =~ /,$/ ? '' : ',').$cont if $cont;
    }
    $rline =~ s/,\s*$//g;
    if ( $rline =~ /^CLASS.{27}(.+)$/ ) {
        @{$jrdefs}{qw(_C _D _W _F)} = unpack("A8 A8 A8 A4", $1);
    } 
    elsif ( $rline =~ /^(?:DSN|TRNDSN)\s+([^\s].+)\s*$/ ) {
        $jrdefs = { map { /^\s*([^\s]+)\s*$/; $1 =~ /^(?:PGMSTEP|PROCSTEP)/ ? '_S' 
                                            : $1 =~ /^DDNAME/ ? '__' 
                                            : $1 } split /[\=,]/, uc($1) };
    }
    if ( exists($jrdefs->{JOBNAME}) && !scalar(grep !/^(?:JOBNAME|LAST|DATE)$/i, keys( %{$jrdefs})) ) {
    	$onlyjobsel = 1;
    } 
    my $jobnre = $jobname;
    my $jobnqre = $jobname;
    my $pfxlen = length($jobname) unless ( $jobname =~ /[\?\*]/ ); 
    if ( $jobname =~ /[\?\*]/ ) {
        (my $pfx = $jobname) =~ s/[\?\*]+$//g;
#        $pfxlen = length($pfx);
        $jobnre =~ s/\?/\[\\\$\\w\]/g; 
        $jobnre =~ s/\*/\[\^;\]\+/g;
        $jobnre =~ s/\$/\\\$/g;
        $pfxlen = length($jobnre);
        $jobnqre = qr/^$jobnre/; 
        $jobname = $self->{currmember};
    }
    elsif ( $jobname !~ /$self->{jobnre2skip}/ && $jobname =~ /$self->{jobnre}/ ) {
#    elsif ( $jobname =~ /$self->{jobnre}/ ) {
        $jobnre = $main::rematch;
#        (my $jobmatchstr = $jobnre) =~ s/^\^|(?:\.|\[\^[^\]+]\])?[\+\*]$//g;
#        ($jobmatchstr) = ($jobname =~ /($jobnre)/);  
#        $pfxlen = length($jobmatchstr);
        $jobnre =~ s/\./\[\^;\]/g;
        $jobnqre = qr/$jobnre/; 
        $jobnre =~ s/^\^//;
        $pfxlen = length($jobnre);
    }
    my $flds = $self->{matchflds};
    my $fkeys = $self->{matchkeys};
    my $jrn = join('.', $jobname, grep { $_ } map { $jrdefs->{$_} ? $_.$jrdefs->{$_} : undef } @{$fkeys});
    my $filtervarname = $self->getFilterVarName($jrn);
    my $alreadyJobname = 0;
    #unless ( exists($self->{jobreports}->{$jrn}) ) {
    if(!exists($self->{jobreports}->{$jrn})) {
    my $jrinfos = 
       { cat => $self->{currcat}
       	, namekey => $jrn
       , inputmember => $self->{currmember}
       , jobname => qq/$jobname/
       , JobReportName => qq/$jrn/
       , case => { pfxlen => $pfxlen, jnqre => $jobnqre, originjn => $jobname
              , MATCH => join(';', $jobnre
                     , grep { $_ } map { $jrdefs->{$_} ? $flds->{$_}.'='.$jrdefs->{$_} : undef } @{$fkeys} )
              , ATTRIB => join(';', "\$request->{cinfo}->{JOBNM}"
                     , grep { $_ } map { $jrdefs->{$_} ? $flds->{$_}."=\$request->{cinfo}->{$flds->{$_}}" : undef } @{$fkeys} )
              , ASSERT => [ { reportname => join('.', ($jobname eq $self->{currcat}->{jobname} ? "\$request->{cinfo}->{JOBNM}" : $jobname )
                    , grep { $_ } map { $jrdefs->{$_} ? $_."\$request->{cinfo}->{$flds->{$_}}" : undef } @{$fkeys}  ) } ] 
                  }
         , pagcnt => 0 
         , pages => {} 
         , FilterVar => $filtervarname 
         };
#        warn "ADDING $jobname to list FilterVar: $filtervarname JRINFOs: ", Dumper($jrinfos), "\n";
        i::logit("CASO ONDSN PRIMO INSERT $jobname -K $jrn -- ".Dumper($self->{jobreports}->{$jrn}->{pagescategory}));
        @{$jrinfos}{qw(prtcopies)} = @{$self->{defaults}}{qw(prtcopies)};
        $self->{jobreports}->{$jrn} = $jrinfos;
	#put evidence a page of this category has already been created
	push @{$self->{jobreports}->{$jrn}->{pagescategory}} ,$self->{currcat}->{catname};
    } else {
	 i::logit("CASO ONDSN RIP PRIMA J $jobname -K $jrn ".Dumper($self->{jobreports}->{$jrn}->{pagescategory}));	
	# if you are in this else mean you have a new ONDSN in the same jrn (e.g. jobreportkey)   
	#new category to create to be passed to new page  push @{$self->{jobreports}->{$jrn}->{pagescategory}} = ;
	push @{$self->{jobreports}->{$jrn}->{pagescategory}} ,$self->{currcat}->{catname};
       	$alreadyJobname = 1 if(scalar( grep /$self->{currcat}->{catname}/, @{$self->{jobreports}->{$jrn}->{pagescategory}}) > 1);
	 i::logit("CASO ONDSN RIP DOPO J $jobname -K $jrn".scalar( grep /$self->{currcat}->{catname}/, @{$self->{jobreports}->{$jrn}->{pagescategory}}));	

    }
    #$self->{jobreports}->{$jrn}->{onlyjobsel} = 1 if $onlyjobsel;
    $self->{jobreports}->{$jrn}->{onlyjobsel}->{presence} = 1 if $onlyjobsel;
#    my $jrn = $jobname;
#    $jrn = $self->{currmember} if $jrn =~ /\?/;
#    $jrn = join('.', $jrn, grep { $_ } map { $jrdefs->{$_} ? $_.$jrdefs->{$_} : undef } qw(_C _W _D _F _S __));
#    my $filtervarname = $self->getFilterVarName($jrn);
#    my $jrinfos = { cat => $self->{currcat}, catlist => {},
#            JobReportName => $jrn, pages => {},pagcnt => 0, FilterVar => $filtervarname };
#    $self->{jobreports}->{$jrn} = $jrinfos unless exists($self->{jobreports}->{$jrn}); 

    alias $self->{currjobreport} = $self->{jobreports}->{$jrn};
    $self->{currjobreport}->{doforce} = 'ROOT';
    die "CURRJR: ", Dumper($self->{currjobreport}), "\n" unless $self->{currjobreport}->{FilterVar};
    @{$self->{currjobreport}}{qw(prtcopies)} = @{$self->{defaults}}{qw(prtcopies)};
    my $result;  
    while ( scalar(@$memlines) && $memlines->[0] =~ /^\s*([^WNRT\s])(.*)$/ ) {
       my $result = $self->reprecDispatcher(shift @{$memlines}, $memlines);
       return undef unless defined($result);
    }                 
    while ( scalar(@$memlines) && $memlines->[0] =~ /^\s*([^NR\s])(.*)$/ ) {
       my $currpage = $self->reprecDispatcher(shift @{$memlines}, $memlines,$self->createNewPage());
       return undef unless defined($self->pagePostProcess($currpage,$alreadyJobname));
    }
    
    return $self;   
}


sub pagePostProcess {
    #### this sub put in currjobreport all pages with a new rectid
    #### if rectid already exists all pages with same rectid are merged
    #### 2017/11 if rect id is from an alternative ONDSN with same jobnamekey , another category is created and  rectid and
    #### current category are coherently changed 
    my ($self, $currpage, $already) = @_;
    my ($rectid, $rectval, $uref) = map { $_ ? $_ : '' } @{$currpage}{qw(test rectval UserRef)};
    $uref = '__FROMDBDEF__' unless $uref;
    ## 2017/11 create e new category to ensure parsing be aware of When line covering overlapped pages
    my $currentCat = $self->{currcat}->{catname}.($already ? '_'.sprintf('%02d', scalar(grep /$self->{currcat}->{catname}/i , @{$self->{currjobreport}->{pagescategory}})) : '');
    $rectid = $currentCat.'.'.$rectid;
    $rectid .= '.'.$uref;
        
    if ( exists( $self->{currjobreport}->{pages}->{$rectid} )) {
        alias my $opage = $self->{currjobreport}->{pages}->{$rectid};
        if (exists($opage->{bundles}) && exists($currpage->{bundles}) ) {
            $opage->{bundles} = { %{$opage->{bundles}}, %{$currpage->{bundles}} };
        }
        elsif ( exists($currpage->{bundles}) ) {
            $opage->{bundles} = { %{$currpage->{bundles}} };
        }
        alias my $oldops = $opage->{op};
        foreach my $op ( @{$currpage->{op}} ) {
            push @{$oldops}, $op 
               unless grep { $_->{getVars} eq $op->{getVars} 
                   && $_->{test} eq $op->{test} } @{$oldops};
        }
        foreach my $oldix ( 0..$#{$oldops} ) {
           alias my $oldop = $oldops->[$oldix];
           my $op = (grep { $_->{getVars} eq $oldop->{getVars} 
                   && $_->{test} eq $oldop->{test} } @{$currpage->{op}})[0];
           next unless exists($op->{bundles});
           $oldop->{bundles} = {} unless exists($oldop->{bundles});
           $oldop->{bundles} = { %{$oldop->{bundles}}, %{$op->{bundles}}}
        }
    }
    else {
        $currpage->{category} = $currentCat;
        $self->{currjobreport}->{pagcnt} += 1;
        $currpage->{name} = $currentCat.'.'.sprintf('PAG%03d', $self->{currjobreport}->{pagcnt});
        $self->{currjobreport}->{pages}->{$rectid} = $currpage;
    }
    $self->{currjobreport}->{catlist}->{$currentCat} = scalar(keys %{$self->{currjobreport}->{catlist}}) * 10;
    return $self->{currjobreport}->{pages}->{$rectid};
}


sub reprec_W { # WHEN STATEMENT
=h2 WHEN Statement

report page content selection

Wlllllssssseeeee   [A|O| ]
Y...........

=cut

    my ($self, $rline, $memlines) = (shift, shift, shift);
    my $currpage = shift;

    my $jrn = $self->{currjobreport}->{JobReportName};
    delete $currpage->{initialdummy};
#    if ( !$currpage || $currpage->{initialdummy} ) {
#       $currpage = { test => '', rectval => '' };
#    }
    my $bool = ''; my $testvals = []; my $ops = [];
=head2 AND/OR

Concatenation parameter. If specified, another WHEN line is
opened. Valid values are:

A (And) The page is identified only if both WHEN conditions
are true. Use this value when two or more distinct strings on the
report page are required to accomplish the identification.

O (Or) The page is identified if either of the WHEN conditions
is true. Use this value if the identification strings may reside in
more than one distinct area on the report page.

If both A and O are specified in the same WHEN statement, the
strings concatenated by A are evaluated first.

=cut    
    do {
        die "unexpected eof at line: W$rline\n" if !scalar(@$memlines);
        die "When line \"W\" without match \"Y\" line: W$rline\n" if ( $memlines->[0] !~ /^Y/ && $rline !~ /^\s*$/ );
    
        my $Yline = substr(shift @$memlines, 1) if $memlines->[0] =~ /^Y/;
        my ($match, $oper, $quote, $string, $Ystrlen) = ({rect => '_NULL_'}, '', '', ($currpage->{rectval} ? '__BLANK__' : ''), 0);
        if ( $rline !~ /^\s*$/ ) {
            (@{$match}{qw(froml tol fromc toc)}, $bool, my $contid) = ($rline =~ /^(\d{5})(\d{5})(\d{5})(\d{5})(.)..(.)/ );
            $contid = 'N' unless $contid && $contid eq 'Y';
=head2 contid

N (No) Every page must be identified. Default if REF NEXT
PAGE is set to N or blank. Pages that do not contain an
identifying string are directed to the default recipient

Y (Yes) Continue identification to the next pages. Default if
REF NEXT PAGE is set to Y. Unidentified pages are directed to
the user identification obtained from the previous pages
under the same ON statement.

=cut
            $match->{rect} = sprintf('%05d%05d%05d%05d', @{$match}{qw(froml tol fromc toc)});
            $bool = '' if $bool eq ' ';
            $currpage->{whenline} = $match->{froml} unless exists($currpage->{whenline});
            $currpage->{contid} = $contid unless exists($currpage->{contid});
        
            $Ystrlen = ($match->{toc} - $match->{fromc} + 1);
            $match->{filterval} = substr($Yline, 0, $Ystrlen ); # =~ s/\s+$//g;
            my $setname = 'VAL'.substr('00000'.$match->{filterval}, -5);

=head2 match string

can be 1 through 50 characters.

The string can contain blanks, single quotes and hex format. When
not enclosed in single quotes, the length of the string to be
compared is measured from the start of the field to the last
non-blank character.
Example:
MYVAL1
' EMP '
x'hhhhhh'
%%vtitle
.[op-comp].{string| {%%var2 | %%system_var}[(<startpos>,<length>)]}

op-comp is the comparison operator. The following operators are
supported:
GT Greater than
LT Less than
GE ?Greater than or equal to
LE Less than or equal to
EQ ?Equal to
GL Not equal to
NE No matching value was found in the range. The line for the next set of DO statements cannot be determined.

=cut

            ($oper, $quote) = ($Yline =~ /^(?:\.(NE|EQ|LT|GT|LE|GE)\.)?(')?([^']+)\2?\s*$/);
            $oper = 'EQ' unless $oper;
            $string = ($quote ? $3 : unpack("A*", $3)) if $3;
            #$string =~ s/\>/\\x3E/g;
            #$string =~ s/\</\\x3C/g;
     #TODO subst with buildRectDef
            $self->{currjobreport}->{rect}->{'R'.$match->{rect}} =  
                            { coord => '('.join(',', map { s/^0+//; $_ } @{$match}{qw(froml fromc tol toc)} ).')' };
        }
        if ( $match->{rect} ne '_NULL_') {
        
           if ((length($string) != $Ystrlen or $match->{froml} != $match->{tol} ) and ($oper =~ /^(?:NE|EQ)$/ )) {

              $currpage->{test} .= '(('.join(',', map { s/^0+//; $_ } @{$match}{qw(froml fromc tol toc)} ).')'
                                . ($oper eq 'EQ' ? "." : "!").$string.")";
           }
           else {
             $currpage->{test} .= '(R'.$match->{rect}.".".($oper || 'EQ').".'$string')";
        }
           $currpage->{test} .= ($bool =~ /^A/i ? '.AND.' : $bool =~ /^O/i ? '.OR.' : '');
        }
    
        elsif ( $currpage->{test} ) {
            die "Empty condition for composite WHEN - LINE: W$rline in $jrn";
        }
        else {
            $currpage->{test} = 1;
        }

        push @{$testvals}, $string if $string;
        if ( $bool && $memlines->[0] =~ /^W(.*)$/ ) {
            $rline = substr(shift @$memlines, $-[1]);
        }
        elsif ( $bool ) {
            die "more WHEN lines expected in $self->{currmember}: for line: W$rline - found: $memlines->[0]\n";
            return undef;
        }
        elsif ( !$bool && $memlines->[0] =~ /^W(.*)$/ ) {
            $bool = 'O';
            $rline = substr(shift @$memlines, $-[1]);
        }
        elsif ( !$oper ) { 
        }

    } while ( $bool) ;
    if ($memlines->[0] =~ /^W/) {
        i::logit("Unexpected WHEN record in $jrn - LINE: W$rline");
        return undef;
    }
    if ( $currpage->{test} ne '1' ) {
       my $varval = $currpage->{test}; 
       $varval = Digest::SHA1::sha1_base64($varval) if length($varval) > 30;
       push @{$currpage->{setvars}}, {name=> "$self->{currjobreport}->{FilterVar}", constant=> "$varval"};
    }
    $currpage->{rectval} = join("\t", @{$testvals});
    push @{$currpage->{op}}, $ops->[0] if scalar(@{$ops}) > 0; 
    while ( scalar(@$memlines) && $memlines->[0] =~ /^\s*([^WNR\s])(.*)$/ ) { 
        my $result = $self->reprecDispatcher(shift @$memlines, $memlines, $currpage);
        return undef unless defined($result);
    }
    return $currpage;    
}

sub setBundle {
    my $self = shift;
    my ($loc, $pmiss) = (shift, shift);
    $loc->{bundles}->{$pmiss} = $self->{currjobreport}->{prtcopies} 
                  ; #unless $self->{currjobreport}->{prtcopies} && $self->{currjobreport}->{prtcopies} !~ /^\0+$/;

}

sub reprec_P { # PRINT STATEMENT
=head2 PRINT COPIES

=cut

    my ($self, $rline, $memlines) = (shift, shift, shift);
    my $currpage = shift;

      my ($prtcopies, $prtuser) = unpack("A2 x2 A8", $rline);
      $self->{currjobreport}->{prtcopies} = $prtcopies if $prtcopies =~ /^\d+$/;
      $self->{currjobreport}->{prtuser} = $prtuser if $prtuser !~ /^\s*$/ ; # read prtuser even if copies == 0 for xreport is useful have e recipient name to be used && $prtcopies !~ /^0+$/ ;
      return $self;
      
}

sub addRect {
=head2 XReport rectangle specification insertion

=cut

    my ($self) = (shift);
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};
    
    my ($strline, $strscol, $strecol) = @_;
    my $rectid = sprintf('R%05d%05d%05d%05d', ($strline, $strline, $strscol, $strecol));
#TODO subst with buildRectDef
    $jobreport->{rect}->{$rectid} =  { coord => '('.join(',', map { s/^0+//; $_ } ($strline, $strscol, $strline, $strecol) ).')' };
    return $rectid;
}

sub solveConstant {
=head2 internal Constant names 

#   EXTWTR
#   FILE
#   JOBNAME
#   USERID

=cut

   my ($self, $varv) = (shift, shift);
   if ( $varv =~ /^\%\%([^\s]+)/ ) {
     my $vs = $1;
     my ($vn, $vp, $vl ) = ( $vs =~ /^([^\(\s]+)(?:\((\d+),(\d+)\))?/ );
     my $vpl = '';
     $vpl = '('.($vp - 1).",$vl)" if ( $vp ); 
     my $varspec = ( $vn eq 'JOBNAME' ? 'JobName'.$vpl
                   : $vn eq 'JOBID' ? 'JobNumber'.$vpl
                   : $vn eq 'FILE' ? 'RemoteFileName'.$vpl
                   : $vn eq 'USER' ? 'XferRecipient'.$vpl
                   : $vn eq 'EXTWTR' ? 'WRITERN'.$vpl
                   : $vn eq 'DDNAME' ? 'DDNAM'.$vpl
                   : $vn.$vpl);
                   
     return { elabvalue => $varspec };
   } 
   else {
     return { constant => $varv };
   }
   return undef; 
}

sub addSetVars {
   my ($self, $currpage) = (shift, shift);
   my ($varn, $strings_in) = @_;
   
   return 0 unless scalar(@{$strings_in});
   my $strings =  [map {@$_[1] =~ s/\�\=/\!\=/g; @$_[1] =~ s/\</\&lt\;/gs; +{@$_[0] => @$_[1]}; } map {[each %{$_}]} @$strings_in];
   #map { s/\�\=/\!\=/g; $_} map {values %{$_}} @{$strings};
   $currpage->{UserRef} .= join('.', map { s/\�\=/\!\=/g; $_} map { values %{$_}} @$strings) if $varn eq 'UserRef'; 
   my $numset = scalar(@{$strings});
   my $setvars = { name => $varn, %{shift @{$strings}} };
   $setvars->{concat} = $strings if scalar(@{$strings});
   push @{$currpage->{setvars}}, $setvars;
   return $numset;
}

sub ctdDONAME {
=h1. DO NAME
DO Name cases
TNAME string
   -  sets report name to the specified string
TNAME * rrrrrssssseeeee
   -  sets report name to the string found at pos 
      in report
TNAME *+*P string|rrrrrssssseeeee|%%varname
   -  sets the report name to this spec concatenated to the prev do name       
TNAME *P+* string|rrrrrssssseeeee|%%varname
   -  sets the report name the prev do name concatenated to this spec       
=cut    

    my ($self, $args) = (shift, shift);
    my ($memlines, $currpage) = @_;
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};

    my $strings = [];
    $currpage->{UserRef} = '';
    my $varvalue;
    while (1) {
      my ($rname, @strp) = unpack( ( $args =~ /.{20}[^\d]\d/ ? 'x A20 (A3)3' 
                                    : $args =~ /.{20}\d{5}/ ? 'A20 (A5)3' : 'A35'), $args);
      my ($concat, $text) = ( $rname =~ /^\*(\+\*P|P\+\*|P\+(?=%%)?)?(.*)$/ );
      
      if ( $rname !~ /^\*/ ) {
        if ( !exists($jobreport->{JobReportDescr}) && $rname !~ /%%[^\s]+/ ) {
           $jobreport->{JobReportDescr} = $rname;
        }
        elsif ( $rname =~ /%%[^\s]+/ || $jobreport->{JobReportDescr} ne $rname ) {
           push @{$strings}, map { $self->solveConstant($_) } grep( !/^\s*$/, split( /(%%[^\s]+)/, $rname));
        }
      }
      else {
	      #die "DO NAME * Requested but rect missing in $jobname - RLINE: \"TNAME   $args\" CURRENT PAGE: "
	      #                    , Dumper($currpage), "\n" if !scalar(@strp);
         
	 my $rectid; 
          my $strlen;
	  if(scalar(@strp)){
	  	$rectid = sprintf('R%05d%05d%05d%05d', @strp[0,0,1,2]);
		$strlen = ($strp[2] - $strp[1] + 1);
		$jobreport->{rect}->{$rectid} =  { coord => '('.join(',',map { s/^0+//; $_ } @strp[0,1,0,2] ).')' };
	  }			   
	  #my $rectid = sprintf('R%05d%05d%05d%05d', @strp[0,0,1,2]);
	  #my $strlen = ($strp[2] - $strp[1] + 1);
     #TODO subst with buildRectDef
     	 #$jobreport->{rect}->{$rectid} =  { coord => '('.join(',',map { s/^0+//; $_ } @strp[0,1,0,2] ).')' };
	 i::logit("valore concat $concat --- e valore text $text");
         if ( !$concat ) {
            push @{$strings}, { unpack => "$rectid.A$strlen" }  if(scalar(@strp));
            push @{$strings}, $self->solveConstant($text) if $text !~ /^\s*$/;
            
         }
         else {
            if ( $concat =~ /^(?:\+\*P|P\+\*)$/ ) {
               if ( !scalar(@{$strings}) && exists($jobreport->{JobReportDescr}) ) {
                  push @{$strings}, { constant => $jobreport->{JobReportDescr} };
               } 
	       #die "REQUIRED PREV DO NAME missing - CURRENT PAGE: ", Dumper($currpage), "\n" 
	       i::logit("ADDING EMPTY CONSTANT PREV CONCAT ABSENT") unless scalar(@{$strings});
	       push @{$strings}, { constant => " " } unless scalar(@{$strings});
               my $prev = pop @{$strings};                                                                 
               push @{$strings}, ( $concat eq '+*P' ? ( { unpack => "$rectid.A$strlen" }, $prev )
                                             : ( $prev, { unpack => "$rectid.A$strlen" } )  );
               if ( $text && $text !~ /^\s*$/ ) {
                  $text =~ s/\s+$//g;
                  push @{$strings}, map { $self->solveConstant($_) } grep( !/^\s*$/, split( /(%%[^\s]+)/, $text));
               }
            }
            elsif ( $concat eq 'P+' ) {
               $text =~ s/\s+$//g;
               push @{$strings}, map { $self->solveConstant($_) } grep( !/^\s*$/, split( /(%%[^\s]+)/, $text));
	       i::logit("DO NAME what caso concat P+");
            }
         }
         $varvalue = join('.', grep {$_} map { $_ ? $_ : undef}  map { @{$_}{qw(constant unpack elabvalue)} } @{$strings} );                                   
      }
      last unless (scalar(@{$memlines}) && $memlines->[0] =~ /^TNAME/);
      $args = substr(shift @{$memlines}, 8); 
    }
    if ( $varvalue && $currpage->{test} && $currpage->{test} eq '1' ) {
       $currpage->{namevarval} = length($varvalue) > 30 ? Digest::SHA1::sha1_base64($varvalue) : $varvalue;
    }
    my $numstr = $self->addSetVars($currpage, 'UserRef', $strings) if scalar(@{$strings});
    
    return $self;
}

sub ctdDOPRINT {
=h1. DO PRINT

TPRINT {mission}

print mission destination

=cut    

    my ($self, $args) = (shift, shift);
    my ($memlines, $currwhen) = @_;
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};
    #( my $pmiss =  unpack("x7 A8", $args)) =~ s/\s+$//g;
    my $pmiss = [grep !/^\s*$/ , map{ s/\s+$//g; $_ } unpack("x7 (A8)*", $args)] ;
    #$self->setBundle((exists($currwhen->{op}) ? $currwhen->{op}->[-1] : $currwhen), $pmiss); 
    i::logit("DO PRINT".Data::Dumper($pmiss));
    foreach my $itempm (@{$pmiss}){
    	$self->setBundle((exists($currwhen->{op}) ? $currwhen->{op}->[-1] : $currwhen), $itempm);
	i::logit("adding Bundle $itempm to $jobreport");	
    }
    return $self;
}


=h2 parse ctd coordinates

ctd coordinates could be specified using 3 or 5 digit fields.

=cut
$::posRE = qr/\s*(\d+?)?\*\s+([^\s].+)?/o;


sub ctdDOSET {
=h1 DO SET
   DO SET Format:
   A7  # the string "SET    "
   A8  # varname
   A1  # if = 'Y' the concatenate with next
   A1  # CASE op L: lower U: Upper ' ': no op
   A1  # string type C: Const in next Yrec X: Xpath in next Yrec *: get from pos
   A15 # position within page lllllssssseeeee
=cut

    my ($self, $args) = (shift, shift);
    my ($memlines, $currwhen) = @_;
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};

    my $strings = [];
    my $varn;
    while ( my ($currvar, $concat, $case, $type, $ln, $sc, $ec) = unpack('A8 (A1)3 (A5)3', $args) ) {
        $varn = $currvar unless $varn;
        die "concatenation not supported for different var names in $jobname \"$currvar\" ne \"$varn\"\n" unless $currvar eq $varn; 
        die "SET Type $type Not supported " unless $type =~ /^[CX\*]$/; 
        die "Required \"Y\" line missing for DO SET record\n" 
                                      if $type =~ /^[CX]$/ && $memlines->[0] !~ /^Y/;
        if ( $type =~ /^[CX]$/ ) {
          push @{$strings}, $self->solveConstant(unpack('xA60', shift @$memlines));
        }
        else {
          my $rectid = $self->addRect($ln, $sc, $ec);
          push @{$strings}, {unpack => "$rectid.A".($ec - $sc + 1)};
        }
        last unless $concat && $concat eq 'Y';
        die "No set record to satisfy required concatenation" if $memlines->[0] !~ /^TSET/;
        $args = substr(shift @{$memlines}, 8);
    }
    return $self->addSetVars($currwhen, $varn, $strings);

}

sub buildRectDef {
    my ($self, $orient, @coord) = @_;
    my $rectdef = { name => sprintf('R%05d%05d%05d%05d', @coord)
                  , coord => sprintf('(%d,%d,%d,%d)', @coord[0,2,1,3])
                  };
    $rectdef->{Orient} = $orient if defined($orient);
    return $rectdef;
}

sub parseWebConfig {
    my ($self, $currwhen, $douser, $memlines, $wfn) = @_;
    my $rrule = $douser;
       if ( $douser && -e $wfn ) {
#          warn "RETRIEVING WEB CONFIG $douser from \"$wfn\"\n";
          my $wfh = new IO::File("<$wfn") || die "Unable to open $wfn";
          $wfh->binmode();
          $wfh->read(my $cfgstream, -s $wfn);
          $wfh->close();
          my @lines =  grep { $_ !~ / *do +set +JOBNAME /i } split(/\n/, $cfgstream);
          my @outlines = ();
          $currwhen->{webconfig}->{$douser} = {} unless exists($currwhen->{webconfig}->{$douser});
          alias my $cfg = $currwhen->{webconfig}->{$douser};
          alias my $webwhen = $cfg->{when}->[-1] if exists($cfg->{when});
           
          while ( scalar(@lines) ) {
             my $line = shift @lines;
             $line .= ' '.shift @lines if $line =~ /^ *when *$/i;
             if ( $webwhen 
                && exists($webwhen->{indexes}) 
                && ($line =~ /^ *when +([^ ].*)$/i || !scalar(@lines) ) ) {
                push @{$webwhen->{op}}, { test => 1, getVars => '__IDUMMY__', unpack => '(1,2,1,3).a2', 
                    indexes => join(' ', sort @{delete $webwhen->{indexes}} ) };
             }
             if ( $line =~ /^ *when +([^ ].*)$/i ) {
                push @{$cfg->{when}}, {};
                alias $webwhen = $cfg->{when}->[-1];
                my $whenstr = $1;
                $webwhen = ( $whenstr =~ /^ *True *$/ ? { test => 1 } : 
                  { ( $whenstr =~ /((?:Line|Col|Orient)) +((?:\d+,\d+|\d+))/ig )
                  , ( $whenstr =~ /(String) *= *"([^"]+)"/ ? ( $1, $2 ) : () ) } );
                if ( !exists($webwhen->{test}) ) {
                   push @{$cfg->{rect}}, 
                     $self->buildRectDef((exists($webwhen->{Orient}) ? delete $webwhen->{Orient} : undef)
                           ,( split(/,/, delete $webwhen->{Line}), split(/,/, delete $webwhen->{Col} ) ));
                   if ( exists($webwhen->{String}) ) {
                      $webwhen->{test} = '('.$cfg->{rect}->[-1]->{name}.".EQ.'".$webwhen->{String}."')";
                      delete $webwhen->{String};
                   }        
                }
             }
             elsif ( $line =~ /^ *do +set +\* ([^ ]+) ([^ ].*)$/i ) {
                my ($varnam, $setstr) = ($1, $2);
                my $setvar = { name => $varnam, ( $setstr =~ /((?:Line|Col|Orient)) +((?:\d+,\d+|\d+)) ?/ig ) };
                push @{$cfg->{rect}}, 
                $self->buildRectDef((exists($setvar->{Orient}) ? delete $setvar->{Orient} : undef)
                      ,( $setvar->{Line}, delete $setvar->{Line}, split(/,/, delete $setvar->{Col} ) ) );
                $setvar->{unpack} = $cfg->{rect}->[-1]->{name}.'.A*';      
                push @{$webwhen->{setvars}}, $setvar;
             }
             elsif ( $line =~ /^ *do +name +([^ ].*+)$/i ) {
                my ($setstr) = ($1);
                $setstr =~ s/^"|"$//g;
                my ($fval, @strings) = grep !/^\s*$/, split(/"/, $setstr);
                my @setval = ($fval =~ /^%%/ ? ( elabvalue => substr($fval, 2) ) : ( constant => $fval ) );
                push @{$webwhen->{setvars}}, { name => 'UserRef', (@setval) };
                while ( scalar(@strings) ) {
                    my $val = shift @strings;
                    $webwhen->{setvars}->[-1]->{concat} = [] unless exists($webwhen->{setvars}->[-1]->{concat});
                    push @{$webwhen->{setvars}->[-1]->{concat}}, 
                          ($val =~ /^%%/ ? { elabvalue => substr($val, 2) } : { constant => $val } );
                }
             }
             elsif ( $line =~ /^ *do +dest(?:group)? +"([^"]+)"/i ) {
                my ($recipient) = ($1);
                if ( $douser && exists($self->{recipients}->{$douser})
                     && exists($self->{recipients}->{$douser}->{FolderName}) && exists($webwhen->{test}) ) {
                   $douser = $self->{recipients}->{$douser}->{parent};
                }
             }
             elsif ( $line =~ /^ +do +index "([^"]+)" (?:line +0 col \d{1,3},\d{1,3} orient \d mask "\*" )?(.*)$/i ) {
                my ($vardescr, $loc) = ( $1, $2 );
                (my $varname = $self->xlateCtdColumn($vardescr)) =~ s/ /_/g;
                my ($ln, $eln, $scol, $ecol, $orient) = ($loc =~ /line (\d{1,3})(?:,(\d{1,3}))? col (\d{1,3}),(\d{1,3}) orient (\d)/i) if $loc;
                $eln = $ln unless defined($eln);
                push @{$cfg->{rect}}, $self->buildRectDef($orient, $ln, $eln, $scol, $ecol);
                my $rectid = $cfg->{rect}->[-1]->{name};
                my $strlen = $ecol - $scol + 1;
                push @{$webwhen->{op}}, { test => 1, getVars => $varname, unpack => "$rectid.a$strlen" };
                $self->{indexcols}->{$varname}->{$rectid} = $vardescr;
                my $indexname = $varname;
                $cfg->{indexes}->{$indexname} = [$varname];
                $cfg->{INDEX}->{$indexname} = { name => $indexname, vars => $varname, entries => 'FIRST', table => $indexname };
                push @{$webwhen->{indexes}}, $indexname;
             }
             elsif ( $line =~ /^ *([^ ]+) +([^ ]+)(?: +([^ ].*))?$/i ) {
                my ($stmnt, $verb, $verbstr) = ($1, $2, $3);
                push @{$webwhen->{unparsed}->{$stmnt}}, { name => uc($verb), args => $verbstr }; 
             }
             push @outlines, $line;
          }
          $cfg->{input} = join("\n", @outlines);
       }
       return $rrule;
}

sub ctdDOUSER {

=h1. DO USER statement

Do user record cases:

TUSER string
   - string specifies the recipient name for the report
TUSER *FORCE-string|*FORCE-*        [ rrr(rr)sss(ss)eee(ee) ]
   - string in this case is the tree level name where to start from when seeking synonims
   - the statement is valid ON Statement wide
TUSER [nn]*        [ rrr(rr)sss(ss)eee(ee) ]
   - nn is the level id (10 to nn) where to seek recipients synonim or names found at 
     row rrr(rr) from col sss(ss) to col eee(ee) of report. 
     Optional, if missing level 10 is assumed or any previous FORCE specified
     if coordinates are omitted they must be taken from the "WHEN" record
     Is possible to have multiple specs for a "WHEN" group, in this case only the first not blank
     string has to be considered
TUSER [nn]%%varname
   - 
=cut    
    my $self = shift;
    my $args = shift;

    my ($memlines, $currpage) = @_;
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};
    
#    my ($lvl, $douser, $strpos, $flags, $usrmsk6, $usrmsk, $parentbranch) 
#                                  = (undef, '', {}, '', 'A2 A20 (A5)3 A*', 'x A2 A20 (A3)3 A*', qr/^.*$/);
#    if ( $args =~ /^.{26}\d{6}/ ) { # we have a rectangle specified
#       ($lvl, $douser, @{$strpos}{qw(lnum fromcol tocol)}, $flags) 
#              = unpack(( $args =~ /^.{32}\d/ ? $usrmsk6 : $usrmsk ), $args);
#       die "PARSE FAILS - MEM: $self->{currmember} LVL: $lvl USER: $douser ARGS: \"$args\" POS: "
#                                                                                , Dumper($strpos), "\n"
#          if ( $strpos->{lnum} =~ /[^\d]/ || $strpos->{fromcol} =~ /[^\d]/ || $strpos->{tocol} =~ /[^\d]/ );
#       $lvl = undef if $lvl =~ /^\s*$/;
#       $strpos->{lvl} = $lvl if defined($lvl);
#       $strpos->{len} = $strpos->{tocol} - $strpos->{fromcol} + 1;
#    }
#    elsif ( $args =~ /^.\d/ ) { # a decimal in the 9th char of the card indicates the presence of lvl in any ver
#       ($strpos->{lvl}, $douser, @{$strpos}{qw(dummy dummy dummy)}, $flags) 
#              = unpack(( $args =~ /^ / ? $usrmsk : $usrmsk6 ), $args);
#       delete $strpos->{dummy};       
#       $lvl = $strpos->{lvl};
#    }
#    else {
#       ($douser, $flags) = ($args =~ /^\s*([^\s]+)(?:\s+([^\s].+)|\s*)$/);
#    }
    if ( $args =~ /^\s*\*FORCE-([^\s]+)\s*/ ) {
=head2 FORCE Statement

This option can be used for defining sub-trees. When the user name starts with the
string *FORCE followed by either a user name or asterisk (*), special handling
occurs. This means that when two or more DO USER statements are used, the search
for the user name is not outside the branch? of the tree identified by the user name in
the *FORCE statement. If *FORCE? * is specified, a search is performed for user name
in the specified line or column range. After it is identified and a search is performed
for another user under it in the tree (because of another DO USER statement), the
search is limited to children of the identified user. *FORCE? affects all DO USER
statements in the current ON block even if they are defined in another WHEN block.

=cut
       my $forcename = $1;
       die "DO FORCE-* unsupported " if $forcename eq '*';
          $jobreport->{doforce} = $forcename;
    }
    elsif ( $args =~ /^\s*\*([^\s]{1,8})\s*/ ) { 
=head2 generic user name

Begin the
USER NAME with an asterisk, followed by a member name of 1 through 8 characters.
CONTROL-D then reads the list of users from the specified member, in the library
defined by DD statement DAGENUSR. (parm directory in ctdconfig)

=cut
      my $member = $1;
      if ( $self->{parmsdir} && -d  $self->{parmsdir} && -e $self->{parmsdir}."/$member" ){
         my $pfn = $self->{parmsdir}."/$member";
         my $pfh = new IO::File("<$pfn") || die "Unable to open $pfn";
         $pfh->binmode();
         $pfh->read(my $memstream, -s $pfn);
         $pfh->close();
         #my $plines = [ map { from_to($_, 'cp37', 'latin1'); substr($_, 44, 1) = 'A'; $_ } 
		 my $plines = [ map {  my $eline = $translator->toascii($_); substr($eline, 44, 1) = 'A'; $eline } 
                          unpack("(a80)*", $memstream) ];
         unshift @{$memlines}, @{$plines};
       
         i::logit("do user member $member processed for $jobreport->{JobReportName} - ".scalar(@{$plines})." processed");
      }
    }
    #TODO web config parsing - chk for file $douser.def
    else {
       my ($rtnName, $douser, $currargs);
       if ( $args =~ /^\s*([^\s\*]*\*)\s+([^\s].+)\s*$/ ) {
          ($douser, $currargs) = ($1, $2); 
#          $rtnName = 'ctdDONORMAL';
          $rtnName = 'ctdDOUSERSTAR';
       }
       else {
          $rtnName = 'ctdDOUSERDIRECT';
          my ($fsearch, $fhard, $fsyn, $fconcat);
          ($douser, $currargs, $fsearch, $fhard, $fsyn, $fconcat) = unpack('A22 a15 (a)4', $args);
          $douser =~ s/^\s+//g;
#             ($douser,$currargs) = ( $args =~ /^\s*([^\s\*]+)\s+([^\s]+)?\s*$/ );
       }
#       my $rtnName = 'ctdDO'.($args =~ /^\s*([^\s]*)\*/ ? 'NORMAL' : 'DIRECT');
       my $subh = $self->can( $rtnName );
       die "Unable to find DO User handling routine $rtnName - args: $args" unless ref($subh) eq 'CODE';
       return &$subh($self, $douser, $currargs, @_);
    }
    return $self;
    
    push @{$currpage->{dousers}} , $args;
    while(scalar(@$memlines) && @$memlines[0] =~ /^\s*TUSER(.*)/){
        push @{$currpage->{dousers}} , $1;
        shift @$memlines;   
    }
    if (my $subh = $self->can('ctdDOALLUSERS')) {
            return &$subh($self, $args, @_);
    }
    die "DOALLUSER Routine not found - ".Dumper($currpage->{dousers});
}

sub ctdDOUSERDIRECT {
=head2

a specific folder must contain a specific portion of the report
1. the entire jobreport - when the currpage has no test specified (test = 1)
   in this case lrep 0 must be selected 
   (filterrule :: varsetname = _UNDEF_ filtervar :: VarName = $$$$ filtervalue :: VarValue $$$$)
   the report name must be inserted in the list belonging to the folder ( reportrule :: varsetname = foldernamekey )
2. the portion of the report matched by the when condition (test <> 1)
   in this case we must select the lrep that is identified by the FilterValue = test ( check reprec_W )
   (filterrule :: varsetname = foldernamekey filtervar :: VarName = report var name, filtervalue :: varvalue = test )
   the report name must be inserted in the list belonging to the parent of the folder 
   ( reportrule :: varsetname = parent foldernamekey )
   
   for the time being the connection between folders and rules is created here, inserting the needed entries in 
   the list of rules belonging to the folder, joining it to the rule on the foldernamekey 

=cut
    my ($self, $douser, $currargs) = (shift, shift, shift);
    my ( $memlines, $currpage ) = @_;  
    alias my $jobreport = $self->{currjobreport};
    unless ( exists($self->{recipients}->{$douser}) && exists($self->{recipients}->{$douser}->{FolderName})) {
        i::logit("Report recipient $douser requested in $jobreport->{JobReportName} not found in CTDTREE - no tables update");
        return $self;
    } 
    
    my $rgid = $douser; # ReportgroupId always = recipient name (FolderNameKey)
    my ( $rrule, $frule, $fvar, $fval ) 
        = ( $douser, '__UNDEF__', 'CUTVAR', '$$$$' ); #this setting will cover the test=1 case

    if ( $currpage->{rectval} && (( split /\t/, $currpage->{rectval})[0]) ) { 
       $fvar  = $jobreport->{FilterVar};
       $fval  = $currpage->{test}; 
       $fval = Digest::SHA1::sha1_base64($fval) if length($fval) > 30;
	   i::logit("Creating key for USERDIRECT whit $rrule and $jobreport->{JobReportName} and $fval");
	   ###modified 20171219 no more the parent but the single recipient rule (can cause multiple entries for each reportrule)
       #$rrule = $self->{recipients}->{$douser}->{parent};
	   # if(exists($main::doublekey->{uniquekeys}->{$frule.$fval.$rrule.$fvar})){
			# if($main::doublekey->{uniquekeys}->{$frule.$fval.$rrule.$fvar}  ne $jobreport->{JobReportName}){
				# i::logit("Double key happen for ".$frule.$fval.$rrule.$fvar." generating new key val");
				# $main::doublekey->{uniquekeys} =  {$frule.$fval.$rrule.$fvar => $jobreport->{JobReportName}};
				# $fval = $fval.$jobreport->{JobReportName};
				# $fval = Digest::SHA1::sha1_base64($fval) if length($fval) > 30;
			# }
	   # }else{
			# $main::doublekey->{uniquekeys} =  {$frule.$fval.$rrule.$fvar => $jobreport->{JobReportName}};
	   # }
	   ### could be checked if already exists on varsetsvalues because is added next few lines
	   if(exists($main::doublekey->{uniquekeys}->{$fvar.$fval})){
			#### mi interessa intercettare solo i valori uguali in JobReportname diversi ? per me si
			my $foundJRN = "";
			#i::logit("STAMPO ARCANO".Dumper($main::doublekey->{uniquekeys}->{$fvar.$fval}));
			if(scalar(grep{ my @key = keys (%$_); my $value =  $_->{$key[0]} ; if(($jobreport->{JobReportName} ne $value and ($douser ne $key[0] ))){ $foundJRN = $value;}} @{$main::doublekey->{uniquekeys}->{$fvar.$fval}})){
		   #if(!$self->{metabase}->checkSingleValue($self->{tables}->{VarSetsValues}, 
			   #{ VarSetName => $frule, VarName => $fvar, VarValue => $fval }))
			   my $oldval = $fval;
			   foreach my $chiave (keys %{$main::doublekey->{uniquekeys}}){
					if($fvar.$oldval ne $chiave 
						&& scalar(grep{ my @key = keys (%$_); my $value =  $_->{$key[0]} ; 
									 if(($foundJRN eq $value and ($douser eq $key[0] ))){$_}}
									@{$main::doublekey->{uniquekeys}->{$chiave}})
						){
						
						$fval = $fval.$jobreport->{JobReportName};
						$fval = Digest::SHA1::sha1_base64($fval) if length($fval) > 30;
						$currpage->{setvars} = [grep { $_->{name} ne $fvar } @{$currpage->{setvars}}];
						push @{$currpage->{setvars}} , {name => $fvar, constant => "$fval"};
						i::logit("Double key happen for $douser $jobreport->{JobReportName} $oldval generating new key val === $fval");
						$currpage->{'keygenerated'} = $fval;
						last;
					}
				}
				push @{$main::doublekey->{uniquekeys}->{$fvar.$fval}} ,{"$rrule" => $jobreport->{JobReportName}};
			}else{
				if(scalar(grep{ my @key = keys (%$_); my $value =  $_->{$key[0]} ; if(($jobreport->{JobReportName} eq $value and ($douser ne $key[0] ))){$_}} @{$main::doublekey->{uniquekeys}->{$fvar.$fval}})
					or scalar(grep{ my @key = keys (%$_); my $value =  $_->{$key[0]} ; if(($jobreport->{JobReportName} ne $value and ($douser eq $key[0] ))){$_}} @{$main::doublekey->{uniquekeys}->{$fvar.$fval}})
				){
					if(exists($currpage->{'keygenerated'})){
						$fval = $currpage->{'keygenerated'};
						i::logit("Double key inherited");
					}else{
						push @{$currpage->{'keyrecicled'}} , $douser;
						i::logit("Double key remain");
					}
					push @{$main::doublekey->{uniquekeys}->{$fvar.$fval}} ,{"$rrule" => $jobreport->{JobReportName}};
					#i::logit("Double key happen but not new ".Dumper($main::doublekey->{uniquekeys}->{$fvar.$fval}));
					i::logit("Double key happen but not new $rrule$fvar$fval");
				}
			}
			
		}else{
			$main::doublekey->{uniquekeys}->{$fvar.$fval} = [{"$rrule" => $jobreport->{JobReportName}}];
			i::logit("Inserting first time key  $rrule$fvar$fval ----");
		}
       if ( $fval eq '__BLANK__' ) {
#TODO explain _BLANK_          
          $rrule .= '__BLANK__';
          $frule = '__BLANK__';
       } 
       else { 
          $frule = $douser;
       }
    }

#TODO this has to be moved in the folder build process
=head3 
  there are just 2 rules for each folder, rr=fnk|fr=_UNDEF_|fv=CUTVAR and rr=parent fnk|fr=fnk|fv=CUTVAR 
  may be they can be created up front for each folder

=cut  

    $self->{metabase}->addRows2Table($self->{tables}->{FoldersRules}, 
           { FolderName => $self->{recipients}->{$douser}->{FolderName}, ReportGroupId => $rgid});
    $self->{metabase}->addRows2Table($self->{tables}->{NamedReportsGroups}, 
           { ReportGroupId => $rgid, ReportRule => $rrule, FilterRule => $frule, FilterVar => $fvar });

    $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
           { VarSetName => $frule, VarName => $fvar, VarValue => $fval });
	foreach my $currdouser(@{$currpage->{'keyrecicled'}}){
		if($currdouser ne $douser){
			i::logit("Re-adding lost rrule on varsetsvalue $currdouser$fvar$fval");
			$self->{metabase}->addRows2Table($self->{tables}->{FoldersRules}, 
			   { FolderName => $self->{recipients}->{$currdouser}->{FolderName}, ReportGroupId => $currdouser});
			$self->{metabase}->addRows2Table($self->{tables}->{NamedReportsGroups}, 
			   { ReportGroupId => $currdouser, ReportRule => $currdouser, FilterRule => $currdouser, FilterVar => $fvar });
			$self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
			   { VarSetName => $currdouser, VarName => $fvar, VarValue => $fval });
		}
	}	   
	if ( $currpage->{namevarval} && $currpage->{test} && $currpage->{test} eq '1' ) {
       $self->addSetVars($currpage, $jobreport->{FilterVar}, [{ constant => $currpage->{namevarval} }])
                   unless grep { $_->{constant} eq $currpage->{namevarval} } @{$currpage->{setvars}};
       $self->{metabase}->addRows2Table($self->{tables}->{NamedReportsGroups}, 
              { ReportGroupId => $rgid, ReportRule => $rrule, FilterRule => $douser, FilterVar => $jobreport->{FilterVar} });
       $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
              { VarSetName => $douser, VarName => $jobreport->{FilterVar}, VarValue => $currpage->{namevarval} });
    }
    $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
           { VarSetName => $rrule, VarName => ':REPORTNAME', VarValue => $jobreport->{JobReportName} });
    # add a list of VarSetName to VarSetsValues for other Reports who inherith definitions(more qualified r. definitions) from onlyjobsel 
    $jobreport->{onlyjobsel}->{reportRule}->{$rrule} = 1 if($jobreport->{onlyjobsel}->{presence});
    return $self;
}



sub ctdDOUSERSTAR {
=head2

portion of the report must be assigned to folders according to the content of specified rectangles in the page

Special handling occurs when two or more DO USER=* statements are used. When
both a child and a parent are identified in DO USER=* statements, the parent receives
a copy of the report only if parameter TYPE is set to U. If parameter TYPE is set to C
(or blank), multiple DO USER statements can be used to point to a Recipient Tree
location.

Example:
Two DO USER=* statements are specified with different LINE/COL references. The
first yields the string BRANCH3 (parent) and the second yields the string FINANCE
(child). If TYPE is set to blank (or C), only user FINANCE (at BRANCH3) receives the
report. CONTROL-V does not generate a copy for the parent (BRANCH3). The first
DO USER statement specifies that the report belongs to the Finance Department of
BRANCH3 and not to the Finance Department of another branch.

=cut
    my ($self, $douser, $actparm, $memlines, $currpage) = @_;
    die "do user undef - rline: $actparm\n" unless $douser;

    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};

    my ($parentbranch, $childlvl) = (qr/^.*$/, undef);
     ##se non � del tipo 60* � del tipo string generica e quindi recupero il valore di doforce e recupero il suo valore 
    if ( $douser =~ /^(\d+)\*/  ) {
       $childlvl = $1;
    }
    if ( !$childlvl) {
          $childlvl = ( (exists($jobreport->{doforce})  && $jobreport->{doforce} !~ /ROOT/)? $self->{recipients}->{$jobreport->{doforce}}->{lvl} : '10');
    }
        
    my ($strline, $strscol, $strecol);
    if ( $actparm =~ /^\d{6,9}[^\d]/) {
       ($strline, $strscol, $strecol) = ($actparm =~ /^(\d{3})?(\d{3})(\d{3})/ );
    }
    elsif ( $actparm =~ /^\d{10,15}[^\d]/ ) { 
       ($strline, $strscol, $strecol) = ($actparm =~ /^(\d{5})?(\d{5})(\d{5})/ );
    }
    if ( exists($currpage->{whenline}) && !($strline && $strline =~ /^\d+$/)) {
       $strline = $currpage->{whenline};
    }
    my $strlen = ($strline ? $strecol - $strscol + 1 : undef); 
    if ($strlen && exists($jobreport->{doforce})) {
       ##warn "processing force for $jobreport->{doforce} recipient - checking $childlvl to distribute $jobreport->{JobReportName}\n";
       my $rectid = sprintf('R%05d%05d%05d%05d', ($strline, $strline, $strscol, $strecol));
     #TODO subst with buildRectDef
       $jobreport->{rect}->{$rectid} =  { coord => '('.join(',', map { s/^0+//; $_ } ($strline, $strscol, $strline, $strecol) ).')' };
       push @{$currpage->{op}}, { test => 1, getVars => $jobreport->{FilterVar}, unpack => "$rectid.a$strlen" };
       $parentbranch = $jobreport->{doforce};
       if ( exists($self->{recipients}->{$parentbranch}) ) {
          $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
                 {VarSetName => $parentbranch, VarName => ':REPORTNAME', VarValue => $jobreport->{JobReportName}});
          my $hfid = join('.', ($parentbranch, $childlvl,$jobreport->{FilterVar}));
          my $parents = {__firstname__ => $parentbranch};
          $self->{hostfolders}->{$hfid} = 
                   $self->getFoldersList($parentbranch, $childlvl, $jobreport->{FilterVar}, $parents )
                                                                   unless exists( $self->{hostfolders}->{$hfid} );
       }
    }
#    elsif (exists($self->{forcemissing}->{skip}) 
#           && $jobreport->{JobReportName} =~ /^$self->{forcemissing}->{skip}/) {
#       (my $recpos) = ($actparm =~ /^\s*(\d+)[^\d]*$/); 
#       #warn "do user $childlvl* (recpos: $recpos - $strlen ) process skipped for $jobreport->{JobReportName} as req by config - no force present\n";
#    }
    else {
       my $parent = $jobreport->{doforce} || '';
       die "dynamic do user without parent branch for $jobreport->{JobReportName} - LVL: $childlvl - strlen: $strlen parent: $parent\n"
              if !$parent;
    }
    return $self;
}

sub doGenericUserStar {
	my $self = shift;
	#read from generic xml file configuration 
	# 1) parent folder from wich do folder list
	#2) folder level from which start 
	#BANCAS3
	#HOLDNEW
	#PIONEER
	#CARTOUB
	#UBCASA 
	#UBMNEW 
	#XELION 
	#BUMBRIA
	i::logit("Method doGenericUserStar ----".Dumper($self->{recipients}));
	#if specified "ROOT" means a definition 	TUSER *  without level so all level are included
	#if you want to force  from a particolare Folder , instead of ROOT , write forldername eg 'BANCAS3' , 
	# pay attention , if there is a FORCE BANCAS3 , all jobReportaName who wants link to BANCAS3 must contains this REPORTSET.
	foreach my $update (@{$self->{'tree'}->{'update'}}){
		#my ($parentbranch , $childlvl, $filtervar) = ('ROOT', 35,  $self->getFilterVarName('AAAAAAAA'));
		my ($parentbranch , $childlvl, $filtervar) = ($update->{'force'}, $update->{'level'},  $self->getFilterVarName('AAAAAAAA'));
		if ( exists($self->{recipients}->{$parentbranch}) ) {
			  #$self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
			   #      {VarSetName => $parentbranch, VarName => ':REPORTNAME', VarValue => $jobreport->{JobReportName}});
			   i::logit("Method doGenericUserStar ----start getFoldersList");
			  my $hfid = join('.', ($parentbranch, $childlvl,$filtervar));
			  my $parents = {__firstname__ => $parentbranch};
			  $self->{hostfolders}->{$hfid} = 
					   $self->getFoldersList($parentbranch, $childlvl, $filtervar, $parents )
																	   unless exists( $self->{hostfolders}->{$hfid} );
		}
	}	
	return $self;
}

sub printSqlHeaders{
    my ($fh, $env) = (shift, shift);
    print $fh "USE [xnctd".$env."] \n\n";
    print $fh "GO \n";
    print $fh "SET ANSI_NULLS ON \n";
    print $fh "GO \n";
    print $fh "SET QUOTED_IDENTIFIER ON \n";
    print $fh "GO \n";
    print $fh "SET ANSI_PADDING ON \n";
    print $fh "GO \n";

   return 1;	
}

sub printSqlIndex{
    my ($fh, $indexName, $varcolumn) = (shift,shift,shift);
    print $fh "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'tbl_IDX_$indexName') AND type in (N'U')) DROP TABLE [tbl_IDX_$indexName]\n";
    print $fh " GO \n";
    print $fh "CREATE TABLE [dbo].[tbl_IDX_$indexName]( \n";
    print $fh "[JobReportId] [int] NOT NULL, \n";
    map{ if($_){ print $fh  " [$_->{varname}] [varchar]($_->{length}) NOT NULL, \n" }; } @{$varcolumn};

    print $fh "	[FromPage] [int] NOT NULL, \n";
    print $fh "	[ForPages] [int] NOT NULL, \n";
    print $fh " CONSTRAINT [PK_tbl_IDX_$indexName] PRIMARY KEY CLUSTERED \n"; 
    print $fh " (\n ";
    print $fh "	[JobReportId] ASC, \n";
    map{ if($_){ print $fh  " [$_->{varname}] ASC, \n" }; } @{$varcolumn};
    print $fh " [FromPage] ASC, \n";
    print $fh " [ForPages] ASC \n";
    print $fh ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] \n";
    print $fh ") ON [PRIMARY]\n";
    print $fh "GO \n\n";
    print $fh "if not exists (select * from tbl_IndexTables where IndexName = '$indexName' and  DataBaseName = 'XRINDEX')" ;
    print $fh "INSERT INTO tbl_IndexTables (IndexName, DataBaseName) values ('$indexName', 'XRINDEX') \n";
    print $fh "GO \n\n";
    return 1;	

}

sub printAlterColumn{
	my ($fh, $indexName, $varcolumn) = (shift,shift,shift);
	print $fh " ALTER TABLE [dbo].[tbl_IDX_$indexName] DROP CONSTRAINT [PK_tbl_IDX_$indexName] ;\n";
	print $fh " GO \n";
	map { if ($_){print $fh " ALTER TABLE tbl_IDX_$indexName ALTER COLUMN [$_->{varname}] [varchar]($_->{length}) NOT NULL ; \n GO \n"}; } @{$varcolumn};
	print $fh "ALTER TABLE [dbo].[tbl_IDX_$indexName] ADD  CONSTRAINT [PK_tbl_IDX_$indexName] PRIMARY KEY CLUSTERED \n";
	print $fh "( \n";
	print $fh "[JobReportId] ASC , \n";
	map{ if($_){ print $fh  " [$_->{varname}] ASC, \n" }; } @{$varcolumn};
	print $fh " [FromPage] ASC, \n";
    	print $fh " [ForPages] ASC \n";
	print $fh ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] \n";
    	
    	print $fh "GO \n\n";
}

sub ctdDOINDEX {
=h1. DO INDEX



=cut    
    my ($self, $args) = (shift, shift);
    my ($memlines, $currwhen) = @_;
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};
    
    my $rline = 'INDEX  '.$args;
    my $vars = [];
    do { 
            $rline = substr(shift @{$memlines}, 1) unless $rline;
            if ( $rline !~ /^\s*$/ ) {
                my ($lvl, $vardescr, @strpos) = ($rline =~ /^INDEX\s\s[S\s](..)([^\s]+)\s*(\d{5})(\d{5})(\d{5})[^\d]/);
                $lvl = '' if $lvl eq '  ';
                die "subindex definition without main index" if $lvl && !scalar(@{$vars});
                $lvl = (scalar(@{$vars}) && $lvl ? $lvl - 1 : 0);

                my $varname = $self->xlateCtdColumn($vardescr);
		#$vars->[$lvl] = $varname;
	
                my $strlen = ($strpos[0] ? $strpos[2] - $strpos[1] + 1 : undef); 
                die "=========== $self->{currmember} ==========" unless $strpos[0];
		$vars->[$lvl] = {varname => $varname, length => $strlen};
                my $rectid = sprintf('R%05d%05d%05d%05d', @strpos[0,0,1,2]);
     #TODO subst with buildRectDef
                $jobreport->{rect}->{$rectid} =  { coord => '('.join(',', map { s/^0+//; $_ } @strpos[0,1,0,2] ).')' };

                push @{$currwhen->{op}}, { test => 1, getVars => $varname, unpack => "$rectid.a$strlen" };

                $self->{indexcols}->{$varname}->{$rectid} = $vardescr;
		$self->{indexcols}->{$varname}->{re} = "$rectid.([^\\s]{1,$strlen})";
            }
            $rline = undef;
    } while ( scalar(@{$memlines}) && ($memlines->[0] =~ /^TINDEX/ || $memlines->[0] =~ /^T\s*$/) ) ;
    i::logit("ecco la struttura env $main::environment");
    my $varlist = [grep { {$_} } @{$vars}];
    i::logit("ecco la struttura vars ".Dumper($varlist));	
    my $findfh = $self->{filecreatefh};
    #initializing sql file creating all index
    printSqlHeaders($findfh, $main::environment) if(!scalar(keys %{$self->{indexes}}));
    #adding default regular expression in index definition
    my $count = 0;
    my $re = [map{ $count++; my $sfx = ($count == 1 ? 're' : 're'.$count) ; +{$sfx => $self->{indexcols}->{$_}->{re}}} map{$_->{varname}} grep{ $_->{varname}} @{$varlist}];
    
    (my $indexname = join('_', map{$_->{varname}} grep {$_->{varname}} @{$varlist})) =~ s/ /_/g;
    if(!exists($self->{indexes}->{$indexname})){
    	i::logit("generating ... $indexname");
    	my $printed = printSqlIndex($findfh,$indexname, $varlist);
    	die "INDICE $indexname NON GENERATO " if !$printed;
    }elsif (checkAndUpdate($self->{indexes}->{$indexname}, $varlist)){
	printAlterColumn($findfh, $indexname, $self->{indexes}->{$indexname});    
    }
    $self->{indexes}->{$indexname} = $varlist;
    $jobreport->{INDEX}->{$indexname} = { name => $indexname, vars => join(' ', sort map{$_->{varname}} grep{ $_->{varname}} @{$varlist}), entries => 'FIRST', table => $indexname };
    push @{$currwhen->{op}}, { test => 1, (map{ %{$_} } @{$re}), getVars => '__IDUMMY__', unpack => '(1,2,1,3).a2', indexes => $indexname };
    return $self;
}

sub checkAndUpdate{
	my ($oldlist , $newlist) = (shift, shift); my $rvalue = 0;
		print "PRIMA OLDLIST".Dumper($oldlist);
		map{my $colname = $_->{varname}; my $length = $_->{length} ;map { if($_->{varname} eq $colname && $_->{length} < $length) {print "if Loop prima $_->{length} "; $_->{length} = $length; print "if Loop dopo $_->{length} "; $rvalue = 1}} @{$oldlist}} @{$newlist}; print "DOPO OLDLIST".Dumper($oldlist);
	return $rvalue;	
}

sub getNsetHoldDays {
    my ($self, $args, $currpage) = (shift, shift, shift);
    my ($bmiss, $lblday)  = ($args =~ m/\s*([^\s]+?)([^\s]{3})\s*$/);
    die "BACKUP POLICY $lblday  NOT MANAGED".Dumper($self->{bkupclasses}), "\n" 
                                     unless exists($self->{bkupclasses}->{"C_$lblday"});
    $currpage->{backup} = $self->{bkupclasses}->{"C_$lblday"} 
                if ( !exists($currpage->{backup}) || $currpage->{backup} < $self->{bkupclasses}->{"C_$lblday"}); 

    return $self;
}


sub ctdDOMIGRATE {
=h1. DO MIGRATE



=cut    
    my ($self, $args) = (shift, shift);
    my ($memlines, $currpage) = @_;
    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};

    return $self->getNsetHoldDays($args, $currpage);
    return $self;
}

sub ctdDOBACKUP {
=h1. DO BACKUP



=cut    
    my ($self, $args) = (shift, shift);
    my ($memlines, $currpage) = @_;

    alias my $category = $self->{currcat};
    alias my $jobreport = $self->{currjobreport};
    alias my $jobname = $category->{jobname};

    return $self->getNsetHoldDays($args, $currpage);
}

sub reprec_T {
=head2 DO Record dispatcher

=cut
    my ($self, $rline, $memlines) = (shift, shift, shift);
    my $currpage = shift;

    return $self if $rline =~ /^\s*$/;
    my ($action, $args) = unpack('A7 a*', $rline);
    $action =~ s/\s+$//;
    die "UNKNOWN FORMAT for DO Record " unless $action;

    if (my $subh = $self->can('ctdDO'.$action)) {
        return &$subh($self, $args, $memlines, $currpage, @_);
    }

    die "RECORD T$rline non gestito\n";
    
}


sub reprec_C {
    my ($self, $rline, $memlines) = (shift, shift, shift);
    my $currpage = shift;
    i::logit("CALLING REPREC_C $rline");
    my $jobreport = $self->{currjobreport};
      $jobreport->{ddattrs} = join(';', map {
        my ($k, $val) = map { s/\s+$//g; $_ } split /=/, $_;
        $jobreport->{PageDef} = 'P1'.uc($val) if $k eq 'PAGEDEF'; 
        $jobreport->{FormDef} = 'F1'.uc($val) if $k eq 'FORMDEF'; 
        $jobreport->{Chars} = uc($val) if $k eq 'CHARS'; 
        $k.'='.$val;
      } ( $rline =~ /((?:\w+=\([^)]+?\)|\w+=[^(,]*)),?/g ));
	 i::logit("Reprec_C $jobreport->{PageDef}");
    return $self;
}

sub addReport {
    my $self = shift;
    my $currjobreport = shift;
    i::logit("Aggiungo Job ".Dumper($currjobreport));
    my $namekey = delete($currjobreport->{namekey});
    my $onlyjobsel = delete($currjobreport->{onlyjobsel});
    my $xmlfn = 'NULL.xml';
    if ( !exists($self->{rptconfs}) ) {
       my $xmlhash = { name => '$JobReportName', linePos => 'raw'
#           , xmlgroup => '$$$' 
       };
       my $xml = XML::Simple::XMLout($xmlhash, RootName => 'jobreport', NoEscape => 1);
       my $xmlsha1 = Digest::SHA1::sha1_hex($xml);
       my $outdir = $self->{outputdir}.'/rptconf';
       mkdir $outdir unless -d $outdir;
       $self->{rptconfs}->{$xmlsha1} = $xmlfn;
       my $xmlpath = $outdir.'/'.$xmlfn;
       my $xmlfh = new IO::File(">$xmlpath") 
                    || die "unable to write $self->{currmember} \"$xmlfn\" ($xmlpath) in \"$self->{outputdir}\"- $? - $!";
       $xmlfh->binmode();
       print $xmlfh $xml;        
       $xmlfh->close();
        
    }
    my $jrn = $currjobreport->{JobReportName};
    i::logit("Adding Report $jrn");
    return $xmlfn if $currjobreport->{JobReportName} eq '__DEFAULTS__';
    
    if ( exists($currjobreport->{pages}) || exists($currjobreport->{backup}) ) {
       my @pages = sort { $a->{name} cmp $b->{name} } values %{delete $currjobreport->{pages}};
       my $xmlhash = { name => '$JobReportName'
                     , linePos => 'raw', page => []
#                     , xmlgroup => substr($currjobreport->{JobReportName}, 0, 3)
#                     , bundles => {}
                     };
#       $xmlhash->{backup} = $currjobreport->{backup};
       #if (grep { $_->{namevarval} } @pages )
       my $single = ( scalar(@pages) == 1);
       while ( scalar(@pages) ) {
          my $currpage = shift @pages;
          my $hdays = delete $currpage->{backup} || 0;
          $xmlhash->{holddays} = $hdays if ( $hdays && (!exists($xmlhash->{holddays}) || $xmlhash->{holddays} < $hdays) ) ;
          my $catSfx = '';
          ADD_NOOPS: while(1) {
			  i::logit("looping  while ---count op ".(exists($currpage->{op}) ? scalar(@{$currpage->{op}}) : "not exists"));
             if (   $currpage->{test} eq '1'
                && (!exists($currpage->{op}) || !scalar(@{$currpage->{op}}))
                && (!exists($currpage->{fields}) || !scalar(@{$currpage->{fields}})) 
#                && (!exists($currpage->{setvars}) || !scalar(@{$currpage->{setvars}}))
                && (!exists($currpage->{bundles}) || !scalar(keys %{$currpage->{bundles}}))
                ) { 
                 # useless to define a page with no actions defined
                if (!exists($currpage->{setvars}) || !scalar(@{$currpage->{setvars}})
                    || (!scalar(grep { $_->{constant} and $_->{name} ne '$$$$' and $_->{constant} ne '$$$$' } @{$currpage->{setvars}} )
	    		&& !scalar(grep { $_->{elabvalue} } @{$currpage->{setvars}}))) {
                 # we transfer bundles in the jobreport element but what about categories ? 
#                $xmlhash->{bundles} = { %{$xmlhash->{bundles}}, %{$currpage->{bundles} } }
#                                                           if ( exists($currpage->{bundles}) ) ;
		 i::logit("SKIPPING creation of $currpage->{name}");
				  #to avoid creating useless category -> side effect is the creation of  useless LogicalReports during parsing
				  delete $currjobreport->{catlist}->{$currpage->{category}.'_NOOPS'};
				  delete $currjobreport->{catlist}->{$currpage->{category}} if(!$catSfx);
                   last ADD_NOOPS;
                }
             }
             push @{$xmlhash->{page}}, { };
             alias my $xmlpage = $xmlhash->{page}->[-1];
             $xmlpage->{name} = sprintf('PAGE%03d', scalar(@{$xmlhash->{page}}));
             $xmlpage->{category} = ($currpage->{category} || '_NOCATEG_').$catSfx;
             $xmlpage->{test} = 1;
	     $xmlpage->{linePos} = $currpage->{linePos} if $currpage->{linePos};
             $xmlpage->{holddays} = $currpage->{backup} if $currpage->{backup};
             $xmlpage->{test} = $currpage->{test} if $currpage->{test};
             $xmlpage->{contid} = $currpage->{contid} if $currpage->{contid};
             $xmlpage->{bundles} = join(' ', sort map { $_.'/'.$currpage->{bundles}->{$_} } keys %{$currpage->{bundles}} ) 
                                                if $currpage->{bundles};
             if ( exists($currpage->{setvars}) && scalar(@{$currpage->{setvars}}) ) {
		i::logit("Adding $xmlpage->{name} SetVars ".Dumper("SIIIIIIIIIIIIIIIIIII", @{$currpage->{setvars}}));
                my $attrarray = [ $single ? (grep { $_->{name} ne 'UserRef' || exists($_->{constant}) || ($_->{name} eq 'UserRef' && !exists($_->{constant})) } @{$currpage->{setvars}}) : @{$currpage->{setvars}} ];
                i::logit("Adding $xmlpage->{name} SetVars ".Dumper("SIIIIIIIIII attribute" , @{$attrarray}));
                $xmlpage->{vars} = [grep {!($_->{name} eq "\$\$\$\$" && $_->{constant} eq "\$\$\$\$") } @{$attrarray}] 
                                                                                                 if scalar(@{$attrarray});
                delete $xmlpage->{vars} if (exists($xmlpage->{vars})  && !scalar(@{$xmlpage->{vars}}) );
             }
             if ( exists($currpage->{fields}) && grep /UserRef /, @{$currpage->{fields}}) {
                my $varsarray = $xmlpage->{vars};
                $xmlpage->{vars} = [grep { !($_->{name} eq 'UserRef' && exists($_->{unpack}) ) } @{$varsarray}];
             }
             $xmlpage->{fields} = join(" ", @{$currpage->{fields}})
                  if (exists($currpage->{fields}) && scalar($currpage->{fields}));

             my $FilterVar = '';
            i::logit("After setvars");
             last ADD_NOOPS if(!exists($currpage->{op}) || !scalar(@{$currpage->{op}}));
             my $operations = delete $currpage->{op};
             my $dupGetVars = '';
             while (scalar(@{$operations}) ) {
                my $op = shift @{$operations};
#                $op->{test} = 1 if !scalar(@{$operations});
                $xmlpage->{op} = [] unless exists($xmlpage->{op});
                $op->{name} = 'OP'.scalar(@{$xmlpage->{op}});
                if (exists($op->{getVars})) {
                   $FilterVar = $op->{getVars} unless $op->{getVars} eq $FilterVar;
                   $dupGetVars = $FilterVar if (  $op->{test} eq '1' 
                                      && scalar(grep {  $_->{name} eq $FilterVar } @{$xmlpage->{vars}}));
                }
                if ( exists($op->{bundles}) ) {
                   my $opbundles = delete $op->{bundles}; 
                   my $bundlesstr = join(' ', sort map { $_.'/'.$opbundles->{$_} } keys %{$opbundles} );
                   if ( $op->{test} eq '1' && !scalar(@{$operations}) && $bundlesstr ) {
                      $xmlpage->{bundles} = $bundlesstr;
                      
                      $currpage->{bundles} = {} unless exists($currpage->{bundles});
                      $currpage->{bundles} = { %{$currpage->{bundles}}, %{$opbundles} }; 
                   }  
                   elsif ( $bundlesstr ) { $op->{bundles} = $bundlesstr; }
                }
                push @{$xmlpage->{op}}, $op ;
             }
			 i::logit("After while operation");
             if ( $dupGetVars ) {
                $xmlpage->{vars} = [ grep { $_->{name} ne $dupGetVars } @{ delete $xmlpage->{vars} } ];
                delete $xmlpage->{vars} unless scalar(@{$xmlpage->{vars}});
             }
			 i::logit("After dupgetVars");
             $catSfx = '_NOOPS';
             $currjobreport->{catlist}->{$currpage->{category}.'_NOOPS'} 
                                  = $currjobreport->{catlist}->{$currpage->{category}} + 1; 
          } #end while 1
       } # end while @pages   
        
       $currjobreport->{HoldDays} = delete $xmlhash->{holddays} if ( exists($xmlhash->{holddays}) && $xmlhash->{holddays} );
       if ( !exists($xmlhash->{page}) || !scalar(@{$xmlhash->{page}}) ) {
#          	$xmlhash->{xmlgroup} = '$$$';
		### 2017/11 readded simple case because of parsing
		# with empty page defined,  parsing  can create LogicalReportsRefs with CUTVAR included
		i::logit("adding empty page to $currjobreport->{JobReportName}");
		my $page;
		$page->{name} = sprintf('PAGE%03d', 1);
		$page->{test} = 1;
		push @{$xmlhash->{page}}, $page;

       }
       elsif ( exists($xmlhash->{page}) && scalar(@{$xmlhash->{page}}) == 1 
                    && $xmlhash->{page}->[0]->{test} eq '1' 
                    && !exists($xmlhash->{page}->[0]->{bundles}) 
                    && !exists($xmlhash->{page}->[0]->{vars}) 
                    && !exists($xmlhash->{page}->[0]->{fields}) 
                    && (!exists($xmlhash->{page}->[0]->{op}) 
                        || !scalar(@{$xmlhash->{page}->[0]->{op}})) 
                    ) {
          		### 2017/11 readded simple case because of parsing
			# with empty page defined parsing create LogicalReportsRefs with CUTVAR included
			  my $page = delete $xmlhash->{page};
			  #$page->{name} = sprintf('PAGE%03d', 1);
			  #$page->{test} = 1;
			  #$xmlhash->{page} = $page;
 #         $xmlhash->{xmlgroup} = '$$$';
 #            $xmlhash->{bundles} = $page->{bundles} if exists($page->{bundles});
       }
        
#        if ( exists($xmlhash->{page}) && scalar(@{$xmlhash->{page}}) == 1
#                && !exists($xmlhash->{page}->[0]->{bundles}) 
#                && !exists($xmlhash->{page}->[0]->{vars}) 
#                && !exists($xmlhash->{page}->[0]->{fields}) 
#                && (!exists($xmlhash->{page}->[0]->{op}) 
#                    || !scalar(@{$xmlhash->{page}->[0]->{op}})) ) {
#                    $xmlfn = 'NULL.xml';
#                    #delete $currjobreport->{FilterVar};
#                }           
        else {
            i::logit("adding a new page with ");	
            $xmlhash->{'index'} = $currjobreport->{INDEX} if exists($currjobreport->{INDEX}) && scalar(keys %{$currjobreport->{INDEX}});
            $xmlhash->{rect} = $currjobreport->{rect} if exists($currjobreport->{rect}) && scalar(keys %{$currjobreport->{rect}});
            $xmlhash->{cateList} = join('|', sort keys %{$currjobreport->{catlist}}  )
#                 sort {$currjobreport->{catlist}->{$a} <=> $currjobreport->{catlist}->{$b} } keys %{$currjobreport->{catlist}}  )
#                 sort keys { map { $_->{category} => 1 } @{$xmlhash->{page}} } )
                                                       if (exists($xmlhash->{page}) && scalar(@{$xmlhash->{page}}));
        }
	my $pageList = delete $xmlhash->{page};
	$xmlhash->{page} = [(grep { $_->{test} ne '1'} @{$pageList}),(grep { $_->{test} eq '1'} @{$pageList})];
	
        my $xml = XML::Simple::XMLout($xmlhash, RootName => 'jobreport', NoEscape => 1);
        my $xmlsha1 = Digest::SHA1::sha1_hex($xml);

        if ( !exists($self->{rptconfs}->{$xmlsha1}) ) {
#           (my $grpre = qr/$xmlhash->{xmlgroup}/) =~ s/\$/\\\$/g;
#           $self->{rptconfs}->{$xmlsha1} 
#             = sprintf('%s%05d.xml', $xmlhash->{xmlgroup}, scalar( grep { $_ ne 'NULL.xml' && $_ =~ /^$grpre/ } values %{$self->{rptconfs}}));
#          $self->{rptconfs}->{$xmlsha1} = join('.', ($xmlhash->{xmlgroup}, $xmlsha1, 'xml'));
           $self->{rptconfs}->{$xmlsha1} = $currjobreport->{JobReportName}.'.xml';
           my $outdir = $self->{outputdir}.'/rptconf';
           mkdir $outdir unless -d $outdir;
           $xmlfn = $self->{rptconfs}->{$xmlsha1};
           my $xmlpath = $outdir.'/'.$xmlfn;
           my $xmlfh = new IO::File(">$xmlpath") 
                 || die "unable to write $self->{currmember} \"$xmlfn\" ($xmlpath) in \"$self->{outputdir}\"- $? - $!";
           $xmlfh->binmode();
           print $xmlfh $xml;        
           $xmlfh->close();
        }
        else {
           $xmlfn = $self->{rptconfs}->{$xmlsha1};
        }
    }
    
    $currjobreport->{JobReportDescr} = $currjobreport->{JobReportName} unless $currjobreport->{JobReportDescr};
    @{$currjobreport}{qw(ParseFileName CharsPerLine)} = ("*,$xmlfn", 400);
    $self->{metabase}->addRows2Table($self->{tables}->{JobReportNames}, $currjobreport);
    @{$currjobreport}{qw(ReportName ReportDescr)} = @{$currjobreport}{qw(JobReportName JobReportDescr)};
    $self->{metabase}->addRows2Table($self->{tables}->{ReportNames}, $currjobreport);
	#####adding to varsetsValues matching default Recipient defined in Reprec_F line to manage error/scarti logical Reports #######
	if($currjobreport->{cat}->{defaults}->{destuser}){
		i::logit("adding DESTUSER for $currjobreport->{JobReportName} and not prtuser");
		$self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
			{ VarSetName =>  $currjobreport->{cat}->{defaults}->{destuser}, VarName => ':REPORTNAME', VarValue => $currjobreport->{JobReportName} });
	}else{
		i::logit("adding PRTUSER $currjobreport->{prtuser} for $currjobreport->{JobReportName} and not destuser");
		$self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues},
				{ VarSetName =>  ($currjobreport->{prtuser} ? $currjobreport->{prtuser} : 'NODFT'), VarName => ':REPORTNAME', VarValue => $currjobreport->{JobReportName} });
	}	
    i::logit("Adding FD  $currjobreport->{FormDef} PD  $currjobreport->{PageDef} to rename process");   
    if ( ( exists( $currjobreport->{FormDef} ) && $currjobreport->{FormDef} !~ /^(?:\s*|'')$/ )
           or ( exists( $currjobreport->{PageDef} ) && $currjobreport->{PageDef} !~ /^(?:\s*|'')$/ ) 
           or ( exists( $currjobreport->{Chars} ) && $currjobreport->{Chars} !~ /^(?:\s*|'')$/ ) 
           or ( exists( $currjobreport->{PrintControlFile} ) && $currjobreport->{PrintControlFile} !~ /^(?:\s*|'')$/ ) 
           ) {
            $self->{metabase}->addRows2Table($self->{tables}->{Os390PrintParameters}, $currjobreport);
            i::logit("adding Os390params $currjobreport->{PageDef}");
#           $afpvars = 1;
        }
    my $renamematch = $currjobreport->{case}->{MATCH};
    my $assert = $currjobreport->{case}->{ASSERT}->[0]->{reportname};
    my $attribute = $currjobreport->{case}->{ATTRIB};
#   warn "MATCHRENAME: ", Dumper($jrdata->{case}), "\n";
    if ( !exists($main::xrrenlist->{$renamematch.'='.$assert}) ) {
       $main::xrrenlist->{$renamematch.'='.$assert} = 1;
       unshift @{$main::xrrename->{receiver}->{CASE}}, $currjobreport->{case}
#         unless $jrdata->{case}->{ASSERT}->[0]->{reportname} eq $self->{defaultrename}->{ASSERT}->[0]->{reportname}
          ;
       i::logit("Added ASSERT \"$assert\" when \"$attribute\" MATCHes \"$renamematch\" to rename process");   
    }
    return $xmlfn;
    
}

sub createNewPage {
    my $self = shift;
    my $currpage = { test => '', rectval => '', UserRef => ''
#                   , pages => {}
                   , initialdummy => 1, name => $self->{currcat}->{catname}.'.PAG000' };
#    $currpage->{bundles} = { $self->{currjobreport}->{prtuser} => $self->{currjobreport}->{prtcopies} }
#                                                                           if $self->{currjobreport}->{prtuser};
    return $currpage;
}


sub reprecDispatcher {
    my ($self, $record, $memlines) = (shift, shift, shift);
    my $rectype;
    if ( ($rectype, my $recdata) = ($record =~ /^\s*([RDMVIQOSFNPCLWZAYTXUJ0])(.*)/) ) {
        if ( my $subh = $self->can('reprec_'.$rectype) ) { 
                    return &$subh($self, $recdata, $memlines, @_);
        }
    }
    else {  
      die "Record type not handled in $self->{currmember}: ", $record, "\n";
    }
    return $record;
}

sub dupHash {
	my $src = shift;
	my $tgt = {};
	foreach my $attr ( keys %{$src} ) {
		if ( ref($src->{$attr}) eq 'HASH' ) { $tgt->{$attr} = dupHash($src->{$attr}); }
		elsif ( ref( $src->{$attr}) eq 'ARRAY' ) {
			$tgt->{$attr} = [ map { ref($_) eq 'HASH' ? dupHash($_) : $_ } @{$src->{$attr}} ]; 
		}
		else { $tgt->{$attr} = $src->{$attr}; }
	}
	return $tgt;
}


sub parseReports {
    my $self = shift;   
    my $dh = $self->{dirfh};
    i::logit("Started parseReports TIMES: ", join('::', times()));

    MEMBER: foreach ( readdir $dh ) {
        
        next MEMBER if $_ =~ /^\.\.?$/;
        my $ctdreps_fn = "$self->{reportsdir}/$_";
        my $mname = $self->{currmember} = basename($ctdreps_fn);
        i::logit("Processing now $mname");
        my $rfh = new IO::File("<$ctdreps_fn") || die "Unable to open $ctdreps_fn - $!\n"; 
        binmode $rfh;
        $rfh->read(my $memstream, -s $ctdreps_fn);
		$self->{memlines} = [ map { my $eline = $translator->toascii($_); $eline } unpack("(a80)*", $memstream) ];
        #$self->{memlines} = [ map { from_to($_, 'cp37', 'latin1'); $_ } unpack("(a80)*", $memstream) ];
        unless (scalar(@{$self->{memlines}}) && $self->{memlines}->[0] =~ /^\s*R/) {
            i::logit("First row of $mname not \"R\" - skip to next member");
            next MEMBER;
        }

        while (scalar(@{$self->{memlines}})) {
           my $currrec = shift @{$self->{memlines}};
            
           my $result = $self->reprecDispatcher($currrec, $self->{memlines});
           # se un metodo mi restituisce undef devo comunque andare al prossimo record N che me ne puo' definire un altro
           # altrimenti mi perdo tutti quelli di quel membro
           if (!defined($result)) {
              delete $self->{currjobreport};
              while (scalar(@{$self->{memlines}}) && $self->{memlines}->[0] !~ /^\s*[RN]/ ) {
                 shift @{$self->{memlines}}
              }
           }
        }
        my $fullreppages = (grep { $_->{onlyjobsel}->{presence}  } values %{$self->{jobreports}})[0];
        my $allstreamsPages;
        if ( $fullreppages && exists($fullreppages->{onlyjobsel}->{presence} ) ) {
        	my $oldrepkey = $fullreppages->{namekey}; my $fullpagesrect = $fullreppages->{rect};
		#$allstreamsPages = { pages => {}, rect => dupHash($fullreppages->{rect}) };
		$allstreamsPages = { pages => {}, rect => dclone \%{$fullpagesrect} };
		i::logit("PROVA ".Dumper($allstreamsPages));
            while ( my ($jpagekey, $jpage) = each %{$fullreppages->{pages}} ) {
                  my ($kcateg, $kpage) = split /\./, $jpagekey, 2;
                  my $newpage = dclone $jpage;
		  
		  my $newpagekey = $kcateg.'_ALLSTREAMS.'.$kpage;
		  #i::logit("Nuova $kpage pagin", Dumper($newpage));
		  ### add Db ReportName by tag constant  to all  pages  with key '__FROMDBDEF__' 
		  $self->addSetVars($newpage, 'UserRef', [{ constant => $self->{currjobreport}->{JobReportDescr}}] ) if ($kpage =~ /__FROMDBDEF__/);
		  #i::logit("Nuova pagin dopo " ,Dumper($newpage)); 
                  my $catnum = $fullreppages->{catlist}->{$jpage->{category}};
                  $newpage->{category} .= '_ALLSTREAMS';
                  $newpage->{backup} = 0; 
#                  $jpage->{namekey} = $kcateg.'_ALLSTREAMS.'.$kpage;
                  $allstreamsPages->{catlist}->{$newpage->{category}} = $catnum;
                  $allstreamsPages->{pages}->{$newpagekey} = $newpage;
		  $allstreamsPages->{onlyjobsel}->{reportRule} = {%{$fullreppages->{onlyjobsel}->{reportRule}}};
		  
            }
            $self->addReport(  delete $self->{jobreports}->{$oldrepkey} );
        }
        foreach(sort keys (%{$self->{jobreports}})) {
           my $currentreport = delete $self->{jobreports}->{$_};
	   if ( $allstreamsPages ) {
               $currentreport->{catlist} = { %{$currentreport->{catlist}}, %{$allstreamsPages->{catlist}} } ;
               foreach my $rectk ( keys %{$allstreamsPages->{rect}}) {
               	 $currentreport->{rect}->{$rectk} = { %{$allstreamsPages->{rect}->{$rectk}} }
               	                                                  if !exists($currentreport->{rect}->{$rectk});
               }
	       #if allstreamPages exists must be added to VarSetValues onlyJobReport's reportRules
	       i::logit("EXISTS ONLYJOBSEL ?".Dumper($allstreamsPages->{onlyjobsel}));    
	       foreach my $rrule(keys %{$allstreamsPages->{onlyjobsel}->{reportRule}}){
		  i::logit("ADDING to $currentreport->{JobReportName} report rules");
	       	$self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
           		{ VarSetName => $rrule, VarName => ':REPORTNAME', VarValue => $currentreport->{JobReportName} });	
	       }
               while ( my ($jpagekey, $jpage) = each %{$allstreamsPages->{pages}} ) {
		  #i::logit("Nuovo stream $jpagekey " ,Dumper($jpage)) if ($jpagekey =~ /FROMDBDEF/);
           	  $currentreport->{pages}->{$jpagekey} = $jpage;
		  i::logit("Nuovo stream $jpagekey " ,Dumper($currentreport->{pages}->{$jpagekey})) if ($jpagekey =~ /FROMDBDEF/);
               }
	   }
	   $self->addReport($currentreport);
        }
#        $self->addReport($alljobpages) if $alljobpages;
            
        $rfh->close();
        $rfh = undef;
    }
    return $self;
}

=pod

=head2 tree definition Record analyzer routines

=cut

sub treerec_S {
=pod

=head2 Synonims record

=cut

        my ($self, $recinfos) = (shift, shift);
        alias my $recipient = $self->{recipient};
        
        if ( my @members = grep !/^(?:TSO-|S|C|\s)/,
              map { ( split( /;/, $_ ) )[0] } ( unpack( "(A20)*", $recinfos ) ) ) {
            push @{$recipient->{filtervals}}, @members;         
        }
    
        
		push @{$recipient->{TSOUsers}} , map { $_ =~ s/^TSO-N-\d{5}|^TSO\-|^\s+|\s+$//g; $_;} grep /^TSO\-/, map { ( split( /;/, $_ ) )[0] } ( unpack( "(A20)*", $recinfos ) ); 
		if (  grep /^TSO\-/, map { ( split( /;/, $_ ) )[0] } ( unpack( "(A20)*", $recinfos ) )	  ) 
			  {
                $recipient->{toprofile} = 1;
			   }
}

sub linkChildParent {
    my $self = shift;
    my ($lvlname, $plvlname) = (shift, shift);
    i::logit("LINKING $lvlname $plvlname EEE "); #.Dumper($self->{recipients}->{$plvlname}));
    $self->{recipients}->{$lvlname}->{parent} = $plvlname;
    $self->{recipients}->{$plvlname}->{_chlds}->{$lvlname} = {}; # $recipient;
    $self->{recipients}->{$lvlname}->{FolderName} = "$lvlname";
    i::logit("LINKING $lvlname $plvlname  \n".Dumper(scalar(keys %{$self->{recipients}->{$plvlname}->{_chlds}})));
    return $self unless ($self->{allrecipients}->{$plvlname}->{name}); # && !exists($self->{recipients}->{$plvlname}->{FolderName}));
    #$self->linkChildParent($plvlname, ($self->{allrecipients}->{$plvlname}->{plvlname} || 'ROOT'));
    $self->{recipients}->{$lvlname}->{ParentFolder} = $self->{recipients}->{$plvlname}->{FolderName};
    $self->{recipients}->{$lvlname}->{FolderName} = $self->{recipients}->{$lvlname}->{ParentFolder}."\\".$lvlname;
    return $self;
}

sub linkChildParent_old {
    my $self = shift;
    my ($lvlname, $plvlname) = (shift, shift);
    i::logit("LINKING $lvlname $plvlname EEE ".Dumper($self->{recipients}->{$plvlname}));
    $self->{recipients}->{$lvlname}->{parent} = $plvlname;
    $self->{recipients}->{$plvlname}->{_chlds}->{$lvlname} = {}; # $recipient;
    $self->{recipients}->{$lvlname}->{FolderName} = "$lvlname";
    return $self unless $self->{recipients}->{$plvlname}->{FolderName};
    $self->{recipients}->{$lvlname}->{ParentFolder} = $self->{recipients}->{$plvlname}->{FolderName};
    $self->{recipients}->{$lvlname}->{FolderName} = $self->{recipients}->{$lvlname}->{ParentFolder}."\\".$lvlname;
    return $self;
} 

sub treerec_U {
=pod

=head2 Recipient Record

=cut

    my ($self, $recinfos) = (shift, shift);
    my ( $lvl, $lvlname, $plvl, $plvlname, $lvldesc ) = unpack( "(A2 A8)2 A*", $recinfos );
#    return $self if $lvlname =~ /^.*\*/;
    if ( $lvlname =~ /^.*\*/ ) {
        i::logit("SKIPPING $lvlname ($plvlname) because wild cards are not supported");
        return undef;
    }
    
#    return $self if $plvlname && !exists($self->{recipients}->{$plvlname}); 
    #if ( $plvlname && !exists($self->{recipients}->{$plvlname}) ) {
    #    i::logit("SKIPPING $lvlname ($plvlname) because parent does not exists");
    #    return undef;
    #}
    
    if ( exists($self->{tree}->{skipmask}) && ($lvlname =~ /$self->{tree}->{skipmask}/ || $plvlname =~ /$self->{tree}->{skipmask}/ ) ) {
        i::logit("SKIPPING $lvlname ($plvlname) as stated in config");
        return undef;
    }
    $self->{allrecipients}->{$lvlname} = {name => $lvlname, lvl => $lvl, FolderDescr => $lvldesc, plvl=> $plvl, plvlname => $plvlname, FolderNameDescr => '', address => [] };
    my $currrecipient = $self->{recipient}->{name};
    alias $self->{recipient} = $self->{allrecipients}->{$lvlname};
    
    #$self->linkChildParent($lvlname, ( $currFolder->{plvlname} || 'ROOT' ));
    
    #if ( exists($self->{tree}->{skipmask}) 
    #          && ($self->{recipient}->{FolderName} =~ /^.*\\?$self->{tree}->{skipmask}\\/ ) ) {
    #   my $plvlname = $self->{recipient}->{parent};
    #   delete $self->{recipients}->{$plvlname}->{_chlds}->{$lvlname} unless $plvlname eq 'ROOT';
    #   alias $self->{recipient} = $self->{recipients}->{$currrecipient};
    #   delete $self->{recipients}->{$lvlname};
    #   return undef;       
    #}

    return $self;
}

sub check_addFolder{
   my ($self, $currFolder) = (shift, shift);
   i::logit("ADDING ".Dumper($currFolder));
   my $lvlname = $currFolder->{name};
   $self->{recipients}->{$lvlname} = 
      {name => $lvlname, lvl => $currFolder->{lvl}, FolderDescr => $currFolder->{FolderDescr}, FolderNameDescr => $currFolder->{FolderNameDescr}, address => $currFolder->{address}, filtervals => $currFolder->{filtervals} , TSOUsers => $currFolder->{TSOUsers}};
    my $currrecipient = $self->{recipient}->{name};
    #alias $self->{recipient} = $self->{recipients}->{$lvlname};
    
    $self->linkChildParent($lvlname, ( $currFolder->{plvlname} || 'ROOT' ));
    
    if ( exists($self->{tree}->{skipmask}) 
              && ($currFolder =~ /^.*\\?$self->{tree}->{skipmask}\\/ ) ) {
       my $plvlname = $currFolder->{plvlname};
       delete $self->{recipients}->{$plvlname}->{_chlds}->{$lvlname} unless $plvlname eq 'ROOT';
       #alias $self->{recipient} = $self->{recipients}->{$currrecipient};
       delete $self->{recipients}->{$lvlname};
       return undef;       
    }
    

    return $self;
}

sub treerec_U_old {
=pod

=head2 Recipient Record

=cut

    my ($self, $recinfos) = (shift, shift);
    my ( $lvl, $lvlname, $plvl, $plvlname, $lvldesc ) = unpack( "(A2 A8)2 A*", $recinfos );
#    return $self if $lvlname =~ /^.*\*/;
    if ( $lvlname =~ /^.*\*/ ) {
        i::logit("SKIPPING $lvlname ($plvlname) because wild cards are not supported");
        return undef;
    }
    
#    return $self if $plvlname && !exists($self->{recipients}->{$plvlname}); 
    if ( $plvlname && !exists($self->{recipients}->{$plvlname}) ) {
        i::logit("SKIPPING $lvlname ($plvlname) because parent does not exists");
        return undef;
    }
    
    if ( exists($self->{tree}->{skipmask}) && ($lvlname =~ /$self->{tree}->{skipmask}/ || $plvlname =~ /$self->{tree}->{skipmask}/ ) ) {
        i::logit("SKIPPING $lvlname ($plvlname) as stated in config");
        return undef;
    }
    $self->{recipients}->{$lvlname} = 
      {name => $lvlname, lvl => $lvl, FolderDescr => $lvldesc, FolderNameDescr => '', address => [] };
    my $currrecipient = $self->{recipient}->{name};
    alias $self->{recipient} = $self->{recipients}->{$lvlname};
    
    $self->linkChildParent($lvlname, ( $plvlname || 'ROOT' ));
    
    if ( exists($self->{tree}->{skipmask}) 
              && ($self->{recipient}->{FolderName} =~ /^.*\\?$self->{tree}->{skipmask}\\/ ) ) {
       my $plvlname = $self->{recipient}->{parent};
       delete $self->{recipients}->{$plvlname}->{_chlds}->{$lvlname} unless $plvlname eq 'ROOT';
       alias $self->{recipient} = $self->{recipients}->{$currrecipient};
       delete $self->{recipients}->{$lvlname};
       return undef;       
    }
    

    return $self;
}

sub treerec_D {
=pod

=head2 Definition Record

=cut

    my ($self, $recinfos) = (shift, shift);
    alias my $recipient = $self->{recipient};
    alias my $branchid = $recipient->{FolderName};
    my @adarray = map { (unpack("A7A*", $_))[1] } grep /\%ADDR\%/, unpack("(A80)*", $recinfos);
    push @{$recipient->{address}}, @adarray;
}

sub traverseTree {
    my $self = shift;
    my $root = shift;
    my $path = shift;
    
    foreach my $childk ( sort keys %{$root} ) {
        
        next unless exists($self->{recipients}->{$childk}) 
                    ;
        if ( $childk =~ /^[^\d]+\d{5}-\d{5}/ ) {
            my $child = delete $self->{recipients}->{$childk};
            alias my $parent = $self->{recipients}->{$child->{parent}};
            delete $parent->{_chlds}->{$childk};
            my @childlist = sort keys %{$child->{_chlds}};
            my $newname = join('.', @childlist[0,-1]);
            while ( scalar(@childlist) ) {
                #warn " " x 8, join(", ", splice(@childlist, 0 , 10)), "\n"
            }
            if ( exists($self->{recipients}->{$newname}) ) {
                my $newchild = delete $self->{recipients}->{$newname};
                $newchild->{_chlds} = {%{$newchild->{_chlds}}, %{$child->{_chlds}}};
                $child = $newchild;
            }

            $self->{recipients}->{$newname} = $child;
            $parent->{_chlds}->{$newname} = {};
            $child->{name} = $newname;
            $child->{FolderDescr} = "Container for folders $newname";
            $childk = $newname; 
                        
        }
        $self->{recipients}->{$childk}->{parent} = $path->[-1];
        $self->{recipients}->{$childk}->{ParentFolder} = join("\\", @$path);
        $self->{recipients}->{$childk}->{FolderName} = join("\\", (@$path,$childk));
        $self->traverseTree( $self->{recipients}->{$childk}->{_chlds},[@$path,$childk]) 
                            if scalar(keys %{$self->{recipients}->{$childk}->{_chlds}});
    }
    
    return $self;
} 

sub processPermRecord {
  my $self = shift;
  my @infos = unpack('(n/a)*', $_[0]);
  $main::numpermrec++;
  i::logit("Processing now  permanent record $main::numpermrec") 
                                 if ( ($main::numpermrec % 100000) == 1 );
  my ($user, $rname, $jname, $nc) = 
          map { $_ =~ s/^\s+|\s+$//g; $_ } (unpack('A12 A59 A91', $infos[0]), unpack('x42 A4',$infos[1]));
  my $attrs = { map { $_ ? uc($_) : '' } (join('', splice(@infos, 2) ) 
                     =~ /(Pagedef|Formdef|Chars|Output)\:(?:\s([^\s]{1,20}))?/ig) };
  $self->{textStrings}->{$rname} = 1;
  return '' if $nc =~ /^DFT$/i; 

  $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, 
                   { VarSetName => 'PRTC:'.$user, VarName => "$jname/$rname", VarValue => $nc } );
  return '';
}

sub parsePermanent {
=pod

=head2 permanent file parsing routine

=cut
    my $self = shift;   
    my $permfh = $self->{permfh};
    i::logit("Started parsePerm");
    
    my $recll = 133;                                  # usually printouts are fb 133 ;
    $permfh->read(my $hdr, 3);
    my $modeb = 1 if ( $hdr =~ /[\x80\x40](..)/ );   # chk first 3 chars 
    if ( $modeb ) {
       my ($rect, $ll) = unpack('an', $hdr);
       $recll = $ll + 3; # to satisfy next read
     }
    $permfh->read(my $eline, $recll - 3);           # skip first line ( usually company hdr )

    my $recbuff; my $reccount = 0;
    while ( !$permfh->eof() ) {
        my $rect = "\x80";
        if ( $modeb ) {
            $permfh->read(my $rdw, 3);
            ($rect, $recll) = unpack('an', $rdw);
        }
        $permfh->read(my $eline, $recll);
        #from_to($eline, 'cp37', 'latin1');
		$eline = $translator->toascii($eline);
        next if $eline =~ /^(?:\s*|1BMC.*)$/;
	$reccount++;
	i::logit("parsing line number --- $reccount ") if($reccount < 200);
        $recbuff .= pack('n/a*',substr($eline, 1)); 
	#$recbuff = ( $recbuff =~ /from user: [^\s]/i ? '' 
	#            : $self->processPermRecord($recbuff) ) if ( $recbuff =~ /\-{10} end of record \-{10}/i );
	$recbuff = ( $recbuff =~ /from user: [^\s]/i ? '' 
	            : $self->processPermRecord($recbuff) ) if ( ($reccount % 2) == 0  );            

        last if ($modeb && $rect eq "\x40");
    }
    
    foreach my $rname ( keys %{$self->{textStrings}} ) {
        delete $self->{textStrings}->{$rname};
#       #warn "Adding Perm data to textTable: TS: $rname\n";
#        $self->{metabase}->addRows2Table($self->{tables}->{LogicalReportsTexts}, { textString => $rname} );
    }
    return $self;
}

sub parseTree_new {
=pod

=head2 CTDTREE parsing routine

=cut
    my $self = shift;   
    my $treefh = $self->{treefh};
    i::logit("Started parseTree_new");
    
    $self->{ctdtree} = {};

    TREELINE: while ( !$treefh->eof() ) {
        $treefh->read(my $eline, 80);
        #from_to($eline, 'cp37', 'latin1');
		$eline = $translator->toascii($eline);
        next if $eline =~ /^\s/;
    my ( $typer, $recdata ) = ( $eline =~ /^(.)(.*)$/ );
         next TREELINE if ( $self->{skipuntil_u} && $typer ne 'U' );
         delete $self->{skipuntil_u} if exists( $self->{skipuntil_u} );

        if ( !exists($self->{lasttreetype}) ) {
            @{$self}{qw( lasttreetype lasttreedata )} = ( $typer, $recdata );
            next;
        }
        if ( $self->{lasttreetype} eq $typer ) {
            $self->{lasttreedata} .= " ".$recdata;
            next TREELINE unless $treefh->eof();
        }

        my ( $rectype, $recinfos ) = @{$self}{qw( lasttreetype lasttreedata )};
        @{$self}{qw( lasttreetype lasttreedata )} = ( $typer, $recdata );
        
        if ( my $subh = $self->can('treerec_'.$rectype) ) {
           my $ret = &$subh($self, $recinfos);
           if ( $rectype eq 'U' && !$ret ) {
	   #triggering skip to next U line beacuase if !ret means method must skip current folder (see treerec_U)   
             $self->{skipuntil_u} = 1;
         @{$self}{qw( lasttreetype lasttreedata )} = ('','') if $typer ne 'U';
             next TREELINE;
           }
        }
    }
        
    $treefh->close();
    $treefh = undef;
    delete $self->{treefh};

    #return $self;
    
    
    foreach my $fname (sort {$self->{allrecipients}->{$a}->{'lvl'}  <=> $self->{allrecipients}->{$b}->{'lvl'}} keys %{$self->{allrecipients}} ) {
      if($self->{allrecipients}->{$fname}->{'lvl'} == '10' || ($self->{allrecipients}->{$fname}->{plvlname} && exists($self->{allrecipients}->{$self->{allrecipients}->{$fname}->{plvlname}}))){ 
		$self->check_addFolder($self->{allrecipients}->{$fname});
	#$self->traverseTree($self->{recipients}->{$fname}->{_chlds}, [$fname]) if $paname && $paname eq 'ROOT';
	}else{
            i::logit("SKIPPING ".Dumper($self->{allrecipients}->{$fname})." ($self->{allrecipients}->{$fname}->{plvlname}) because parent does not exists");		
	}
    }
    delete $self->{allrecipients};
    #i::logit("ECCO IL TREE\n".Dumper($self->{recipients}));
    
    return $self;
}

sub parseTree {
=pod

=head2 CTDTREE parsing routine

=cut
    my $self = shift;   
    my $treefh = $self->{treefh};
    i::logit("Started parseTree");
    
    $self->{ctdtree} = {};

    TREELINE: while ( !$treefh->eof() ) {
        $treefh->read(my $eline, 80);
       	#from_to($eline, 'cp37', 'latin1');
		$eline = $translator->toascii($eline);
        next if $eline =~ /^\s/;
    my ( $typer, $recdata ) = ( $eline =~ /^(.)(.*)$/ );
         next TREELINE if ( $self->{skipuntil_u} && $typer ne 'U' );
         delete $self->{skipuntil_u} if exists( $self->{skipuntil_u} );

        if ( !exists($self->{lasttreetype}) ) {
            @{$self}{qw( lasttreetype lasttreedata )} = ( $typer, $recdata );
            next;
        }
        if ( $self->{lasttreetype} eq $typer ) {
            $self->{lasttreedata} .= " ".$recdata;
            next TREELINE unless $treefh->eof();
        }

        my ( $rectype, $recinfos ) = @{$self}{qw( lasttreetype lasttreedata )};
        @{$self}{qw( lasttreetype lasttreedata )} = ( $typer, $recdata );
        
        if ( my $subh = $self->can('treerec_'.$rectype) ) {
           my $ret = &$subh($self, $recinfos);
           if ( $rectype eq 'U' && !$ret ) {
             $self->{skipuntil_u} = 1;
         @{$self}{qw( lasttreetype lasttreedata )} = ('','') if $typer ne 'U';
             next TREELINE;
           }
        }
    }
        
    $treefh->close();
    $treefh = undef;
    delete $self->{treefh};

    return $self;
    
    
    foreach my $fname ( keys %{$self->{recipients}} ) {
        my $paname = $self->{recipients}->{$fname}->{parent};
        $self->traverseTree($self->{recipients}->{$fname}->{_chlds}, [$fname]) if $paname && $paname eq 'ROOT';
    }
    return $self;
}

=pod

=head2 Print Mission definitions Record analyzer routines

=cut

sub pmissrec_P {  #Print mission record
=pod

=head2 Print mission record analyzer

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    @{$pmiss}{qw(mission group owner category ttype maxwait mnum)}
        = unpack("A8 A20 A8 A20 A3 A2", $pline);
   
    CFGLINE: while ( scalar(@{$self->{memlines}}) ) {
        last if $self->{memlines}->[0] =~ /^P/;
        
        my $ctdline = shift @{$self->{memlines}};
        die "Not Supported statement in $self->{currmember}: ", $ctdline, "\n" 
                            unless $ctdline =~ /^[CDNXBMVWTA0SIO]/;
        if ($ctdline =~ m/^\s*([^\s])(.*)/ && (my $subh = $self->can('pmissrec_'.$1)) ) {
            my $result = &$subh($self, $2, $self->{memlines}, $pmiss);
            if ( !defined($result) ) { 
               while( scalar(@{$self->{memlines}}) && $self->{memlines}->[0] !~ /^P/ ) { 
                  shift @{$self->{memlines}}; 
               }
               return undef;
            }
        }
    }
    @{$pmiss}{qw(BundleName BundleCategory)} = map { $_ ? $_ : ''} @{$pmiss}{qw(mission category)};
    $pmiss->{ChunkDelivery} = ($pmiss->{free} eq 'C' ? 1 : 0);
    return $self;
}


sub addRecipient {
    my ($self, $pline) = (shift, shift);
    alias my $area = shift;
    my $pattern = unpack("A8", $pline);
    my $varval = '';
    if ( $pattern =~ /[\?\*]/ ) {
        if ( $pattern eq '*' ) {
            $varval = '%';
        }
        else {
            ($varval = $pattern) =~ s/\?/_/g;
            $varval =~ s/\*/\%/g;
        }
    }
    else {
        $varval = $pattern;
    }
    push @{$area}, $varval;
    return $self;
}

sub pmissrec_N {  #include record
=pod

=head2 Include recipient record analyzer

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
#    warn "processing N mission record: ", $pline, "\n";
    $self->{recipients} = {} unless exists($self->{recipients});
    my $pmiss = shift;
    $self->addRecipient($pline, $pmiss->{':BundleIncludePattern'});
        
    return $self;
}

sub pmissrec_W {  # when record
=pod

=head2 When record parser (time schedule)

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    my $t = unpack('a4', $pline);
}

sub pmissrec_X {  #exclude record
=pod

=head2 Exclude recipient record analyzer

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    $self->addRecipient($pline, $pmiss->{':BundleExcludePattern'});
        
    return $self;
}

sub pmissrec_C {  #Overriding Parms
=pod

=head2 Print parameters record

=cut


    my ($self, $pline, $memlines) = (shift, shift, shift);
    return undef unless $pline =~ /^Y/; # ( only Batch = 'Y' )
    my $pmiss = shift;
    @{$pmiss}{qw(batch BundleSkel free timeout BundleDest BundleUdest BundleWriter BundleForm BundleClass BundlePrtopt)}
       = unpack('A A8 A A2 (A8)4 A A8', $pline);

    return $self;
}

sub pmissrec_D {  #Description record
=pod

=head2 Description record analyzer

=cut


    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    @{$pmiss}{qw(BundleDescr BundleUdata)} = unpack("A50 A29", $pline);
    return $self;
}

sub pmissrec_A { #ACIF record
=pod

=head2 ACIF record analyzer

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    $pmiss->{acif} = 'Y' if $pline =~ /^Y/;
}

sub pmissrec_B { #sort record
=pod

=head2 Sort Output record analyzer

=cut


    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    my %SFIELDS = ( '1' => 'FolderName'
        ,'2' => 'JobName'
        ,'3' => 'JobReportName'
        ,'4' => 'Category'
        ,'5' => 'FolderLevel'
        ,'6' => 'FolderName'
        ,'7' => 'Formdef'
        ,'8' => 'Chars'
        ,'9' => 'PrintParameters'
        ,'T' => 'UserTimeRef'
    );

    $pmiss->{BundleOrder} = join(',', ( map { my ($c, $o) = unpack('AA', $_); "$SFIELDS{$c}/".( $o ? $o : 'A') } split(/\,/, $pline)));
    return $self;
}

sub pmissrec_M { # WHEN record (CAL)
=pod

=head2 Calendar record

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    ($pmiss->{retro}, my $dtype, my $dstring) = unpack("A A a*", $pline);
    if ( $dtype !~ /^[CDWM]$/ ) {
        return undef;
    }
    if ( $dtype eq 'C' ) {}
    elsif ( $dtype eq 'D' ) {} 
    elsif ( $dtype eq 'W' ) {} 
    else {} 
    
    return $self;
}

sub pmissrec_V { 
=pod

=head2 Date record (WeekDays)

=cut

    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    ($pmiss->{shft}, my $stype, my $dtype, my $dstring) = unpack("A A A a*", $pline);
    
    return $self;
}

sub pmissrec_T { 
=pod

=head2 Printer Type record

=cut


    my ($self, $pline, $memlines) = (shift, shift, shift);
    my $pmiss = shift;
    @{$pmiss}{qw(BundlePrtype BundlePrdest PRCHUNK BundlePrudest)} = unpack("A20 A8 A6 A8", $pline);
    
    return $self;
}

sub parseMissions {
=pod

=head2 Print mission definitions analyzer

=cut

    
    my $self = shift;   
    my $dh = $self->{pmdirfh};
    i::logit("Started parseMissions - Recipients: ", scalar(keys %{$self->{recipients}}));
    
    MEMBER: foreach  ( readdir $dh ) {

        next MEMBER if $_ =~ /^\./ ;
        my $ctdmiss_fn = "$self->{prtmissdir}/$_";
        my $mname = $self->{currmember} = basename($ctdmiss_fn);

        my $rfh = new IO::File("<$ctdmiss_fn") || die "Unable to open $ctdmiss_fn - $!\n"; 
        binmode $rfh;
        $rfh->read(my $memstream, -s $ctdmiss_fn);
		$self->{memlines} = [ map { my $eline = $translator->toascii($_); $eline } unpack("(a80)*", $memstream) ];
        #$self->{memlines} = [ map { from_to($_, 'cp37', 'latin1'); $_ } unpack("(a80)*", $memstream) ];
        if (scalar(@{$self->{memlines}}) && $self->{memlines}->[0] =~ /^\s*P/) {
            CONFIGLINE: while ( scalar(@{$self->{memlines}}) ) {
                my $ctdline = shift @{$self->{memlines}};
                die "UnkNown statement in $self->{currmember}: ", $ctdline, "\n" 
                            unless $ctdline =~ /^[PCDNXBMVWTA0SIO]/;
                my $result;
                my $pmiss = { InputMember => $mname };
                if ($ctdline =~ m/^\s*([^\s])(.*)/ && (my $subh = $self->can('pmissrec_'.$1)) ) {
                    $result = &$subh($self, $2, $self->{memlines}, $pmiss);
                }
                else {
                    $result = undef;
                }
                if ( $result ) {
                    my ($incount, $excount) = map { exists($pmiss->{$_}) ? scalar(@{$pmiss->{$_}}) : 0 } 
                                       qw(:BundleIncludePattern :BundleExcludePattern);

                    $self->{metabase}->addRows2Table($self->{tables}->{BundlesNames}, $pmiss);
                    die "PMISSION undef: ", Dumper($pmiss), "\n" if (!defined($pmiss->{BundleName}) || !defined($pmiss->{BundleCategory}) || !defined($pmiss->{BundleSkel}) );
                    $self->{metabase}->addRows2Table($self->{tables}->{VarSetsValues}, map { my $area = $_; map { 
                        { VarSetName => join('_', @{$pmiss}{qw(BundleName BundleCategory BundleSkel)})
                        ,VarName => $area
                        ,VarValue => $_
                        }
                    } @{$pmiss->{$_}} } qw(:BundleIncludePattern :BundleExcludePattern) );
                }
            }
        }
        $rfh->close();
    }
    return $self;
}


sub createFolders_new {
    my $self = shift;
    i::logit("Started createFolders");
    while ( scalar(keys %{$self->{recipients}}) ) {
        my $FolderId = (keys %{$self->{recipients}})[0];
        my $folder = delete $self->{recipients}->{$FolderId};
        next unless $FolderId ;
        alias my $fname = $folder->{FolderName};
        next unless $fname;
         
        die "no FolderName: fname: $fname fid: $FolderId ", Dumper($folder), "\n" unless $folder->{FolderName};
        if ( $folder->{FolderName} =~ /\\/ ) {
            unless ( exists($folder->{ParentFolder}) && $folder->{ParentFolder} ne '' ) {
                my @lvls = split(/\\/, $folder->{FolderName});
                pop @lvls;
                $folder->{ParentFolder} = join("\\", @lvls)
            }
        }
        else {
            $folder->{ParentFolder} = 'ROOT';
        }
        
        $folder->{FolderAddr} = join("\\n", map { $_ ? $_ : '' } @{$folder->{address}});
        $self->{metabase}->addRows2Table($self->{tables}->{Folders}, $folder);
        if (my $UName = &{$self->{userassign}}($FolderId)) {
            $self->{metabase}->addRows2Table($self->{tables}->{Users}, 
                {UserName => $UName, UserDescr => "User to access $FolderId"});
            $self->{metabase}->addRows2Table($self->{tables}->{UserAliases}, 
                {UserName => $UName, UserAlias => $UName, UserDescr => "User to access $FolderId"});
            $self->{metabase}->addRows2Table($self->{tables}->{UsersProfiles}, 
                {ProfileName => $FolderId, UserName => $UName});
            $self->{metabase}->addRows2Table($self->{tables}->{Profiles}, 
                {ProfileName => $FolderId, ProfileDescr => $folder->{FolderDescr}});
            $self->{metabase}->addRows2Table($self->{tables}->{ProfilesFoldersTrees}, 
                {ProfileName => $FolderId, RootNode => $folder->{FolderName}});
        }
    }

    return $self;
}


sub createFolders {
    my $self = shift;
    i::logit("Started createFolders");
    while ( scalar(keys %{$self->{recipients}}) ) {
        my $FolderId = (keys %{$self->{recipients}})[0];
        my $folder = delete $self->{recipients}->{$FolderId};
        next unless $FolderId ;
        alias my $fname = $folder->{FolderName};
        next unless $fname;
         
        die "no FolderName: fname: $fname fid: $FolderId ", Dumper($folder), "\n" unless $folder->{FolderName};
        if ( $folder->{FolderName} =~ /\\/ ) {
            unless ( exists($folder->{ParentFolder}) && $folder->{ParentFolder} ne '' ) {
                my @lvls = split(/\\/, $folder->{FolderName});
                pop @lvls;
                $folder->{ParentFolder} = join("\\", @lvls)
            }
        }
        else {
            $folder->{ParentFolder} = 'ROOT';
        }
        
        $folder->{FolderAddr} = join("\\n", map { $_ ? $_ : '' } @{$folder->{address}});
        $self->{metabase}->addRows2Table($self->{tables}->{Folders}, $folder);
        if (my $UName = &{$self->{userassign}}($FolderId)) {
            $self->{metabase}->addRows2Table($self->{tables}->{Users}, 
                {UserName => $UName, UserDescr => "User to access $FolderId"});
            $self->{metabase}->addRows2Table($self->{tables}->{UserAliases}, 
                {UserName => $UName, UserAlias => $UName, UserAliasDescr => "User to access $FolderId"});
            $self->{metabase}->addRows2Table($self->{tables}->{UsersProfiles}, 
                {ProfileName => $FolderId, UserName => $UName});
            $self->{metabase}->addRows2Table($self->{tables}->{Profiles}, 
                {ProfileName => $FolderId, ProfileDescr => $folder->{FolderDescr}});
            $self->{metabase}->addRows2Table($self->{tables}->{ProfilesFoldersTrees}, 
                {ProfileName => $FolderId, RootNode => $folder->{FolderName}});
			i::logit("numero di TSOUSER   $FolderId".scalar(@{$folder->{TSOUsers}}));
			foreach my $TSOUser (@{$folder->{TSOUsers}}){
				next if((!$TSOUser) or ($TSOUser =~ /^\s*$/));
				$self->{metabase}->addRows2Table($self->{tables}->{UserAliases}, 
					{UserName => $UName, UserAlias => $TSOUser, UserAliasDescr => "User authorized directly to $FolderId"});
			}
        }
    }

    return $self;
}

sub sqldo {
    my $fh = shift;
    print $fh @_, "\nGO\n";
}

sub writeTables {
    my $self = shift;
    my @tables = @_;
    @tables = keys %{$self->{tables}} unless scalar(@tables);
    my $outdir = $self->{outputdir};
    
    my $sfxtemp = '_temp';
    my $sfxlast = '_last';
    ## added postfix _AGG to populate tables _AGG who will be reference for refreshing data on target table
    foreach my $tableName ( @tables ) {
        if ( !exists($self->{tables}->{$tableName}) ) {
            next;
        }
        my $table = delete $self->{tables}->{$tableName};
        i::logit("Processing now table $tableName - rows: ", (ref($table->{rows}) eq 'ARRAY' ? scalar(@{$table->{rows}}) : scalar(keys %{$table->{rows}})));        
        my $tblfh = new IO::File(">$outdir/tbl_$tableName.sql");
        sqldo($tblfh, $self->{metabase}->preliminarySettings());    
        sqldo($tblfh, $self->{metabase}->rotateTable(table => $table, tblsfx => '_bk'
              , sequence => [ {v2 => 'v1'}, {v3 => 'v2'}, {v4 => 'v3'}, {LAST => 'v4'} ]));
        sqldo($tblfh, $self->{metabase}->SQLCreate(tables => [ $table ], tblsfx => '_bk', nopk => 1, initrows => 1));
        sqldo($tblfh, $self->{metabase}->copyTable(table => $table, insfx => '', outsfx => '_bk'));
		sqldo($tblfh, $self->{metabase}->truncateTable(table => $table, tblsfx => '_AGG'));
        #sqldo($tblfh, $self->{metabase}->truncateTable(table => $table, tblsfx => ''));
        sqldo($tblfh, $self->{metabase}->copyTable(table => $table, insfx => '_bk', outsfx => '_AGG', 
                                                       where => $table->{keep})) if ( exists($table->{keep}) );
	sqldo($tblfh, $self->{metabase}->genInsert(table => $table, tblsfx => '_AGG'));  
        sqldo($tblfh, $self->{metabase}->ansiSetting("OFF"));
        $tblfh->close();
        $tblfh = undef;
        
    }   
    
    return $self;

}

1;

package main;
use Data::Dumper;
use XML::Simple;
use XReport;
use XReport::JobREPORT;
use XReport::DBUtil;
#use strict;
$| = 1;
my $xx = select STDERR;
$| = 1;
select $xx;
$main::doublekey;
$main::xrrename = {
    name=>"\$JobReportName", language=>"Italian",
    receiver  => { 
        DEBUG => 1,
        CASE  => [ 
#        { MATCH => '.+', ATTRIB => '$request->{cinfo}->{JOBNM}',
#                ASSERT => { reportname => "\$request->{cinfo}->{JOBNM}.'._S'.\$request->{cinfo}->{STEPN}.'.__'.\$request->{cinfo}->{DDNAM}"}
#        }
         ]
    } 
};
my $Workdir = $ARGV[0];
my $JRID = $ARGV[1];
my $jr = XReport::JobREPORT->Open($JRID, 1);
my $cfgvars = {};
my $FQoutName =  $Workdir.'/'. File::Basename::basename($jr->getValues(qw(LocalFileName)));
$FQoutName =~ s/\.DATA\.TXT\.gz\.tar$/\.bin/i; 

i::logit("filename ahahhaha $FQoutName");
#my $FQoutName = 'UPDATE_TREE.20181029155226.347165.bin';
#$main::environment = substr($ARGV[0],rindex($ARGV[0], "_")+1);
#$main::environment eq 'MA' ? $main::environment = 'BA' : ($main::environment =~ /^A/ ? 'C0' : $main::environment);
my $odir = $ARGV[0];
my $struct = CTDParser->new( reportsdir => $Workdir.''
	#,prtmissdir => $ARGV[0].'/prtmission'
                            #,ctdtreefil => $Workdir.'/ctdtree.bin'
							,ctdtreefil => $FQoutName
	#		    		    ,ctdpermfil => $ARGV[0].'/ctdperm.bin'
			    #,indexfile  => $ARGV[1].'/CreateIndexes.sql'
                            ,outputdir  => $Workdir )
    ->parseTree_new()
    #->parseMissions()
    #->parseReports()
    ;
$jr->Close();
my $xrr = $main::xrrename;
my $wildre = qr/\(\?(?:\-[xism]{1,4}|\^)\:/;
my $wildcases = [ sort { $b->{pfxlen} <=> $a->{pfxlen} } 
#                  grep { $_->{MATCH} =~ /^$wildre/ } @{$main::xrrename->{receiver}->{CASE}}];
                  grep { $_->{jnqre} =~ /^$wildre/ } @{$main::xrrename->{receiver}->{CASE}}];
#my $wildclist = join('|', map { (my $matchre = $_->{MATCH}) =~ s/\\t/\\\\t/g; $matchre } @{$wildcases} ); 
my $wildclist = join('|', map { $_->{MATCH} } @{$wildcases} ); 
$wildclist = qr/^(?:$wildclist)/;
my $normcases = [ 
            grep { $_->{MATCH} =~ /$wildclist/ 
                 || $_->{originjn} =~ /$struct->{jobnre2skip}/ 
                 || $_->{ASSERT}->[0]->{reportname} ne $struct->{defaultrename}->{ASSERT}->[0]->{reportname} }
            grep { $_->{MATCH} !~ /^$wildre/ } @{delete $main::xrrename->{receiver}->{CASE}}];
$main::xrrename->{receiver}->{CASE} 
      = [ # ( sort { $b->{MATCH} cmp $a->{MATCH} }
          map { (my $m = $_->{MATCH}) =~ s/\\([tw])/\\\\$1/g; 
                {MATCH => $m, ATTRIB => $_->{ATTRIB}, ASSERT => $_->{ASSERT} } } (@{$normcases}
#                ) 
        , map { (my $m = $_->{MATCH}) =~ s/\\([tw])/\\\\$1/g; 
                {MATCH => $m, ATTRIB => $_->{ATTRIB}, ASSERT => $_->{ASSERT} } } 
          (grep { $_->{MATCH} =~ /^$wildre(?!\\w\+\))/ } @{$wildcases})
        , map { (my $m = $_->{MATCH}) =~ s/\\([tw])/\\\\$1/g; 
                {MATCH => $m, ATTRIB => $_->{ATTRIB}, ASSERT => $_->{ASSERT} } } 
          (grep { $_->{MATCH} =~ /^$wildre(?=\\w\+\))/ } @{$wildcases})
        , $struct->{defaultrename} ) ];

#my $cases = [(sort { $b->{MATCH} cmp $a->{MATCH} } @{delete $main::xrrename->{receiver}->{CASE}}), $struct->{defaultrename} ];
#$main::xrrename->{receiver}->{CASE} = $cases;
            
(my $a = XML::Simple::XMLout($main::xrrename, RootName => 'jobreport') ) =~ s/\>\s*\<(ASSERT|\/CASE)/\>\<$1/gs;
$a =~ s/\-\&gt\;/\-\>/gs;

# open RPTCONF, ">$odir/rptconf/XRRENAME.xml";
# binmode RPTCONF;
# print RPTCONF $a, "\n";
# close RPTCONF;

#$struct->writeTables(qw(JobReportNames ReportNames Os390PrintParameters));

$struct->doGenericUserStar()->createFolders()
#->parsePermanent()
->writeTables()
;

__END__
