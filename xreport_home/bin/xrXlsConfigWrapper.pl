#!/usr/bin/perl -w

use lib "$ENV{XREPORT_HOME}/perllib";
use strict "vars";

use File::Basename;
use Compress::Zlib;

use Data::Dumper;

use XReport;
use XReport::Util;
use XReport::DBUtil;
use XReport::JobREPORT;

sub logIt {
  for (@_) {
    my $t = $_;
#    $t =~ s/\n/<br\/>/sg;
    print "$t\n";
  }
}

sub debug2log {
	print $_."\n";
}

my $workdir = $ARGV[0];
my $JRID = $ARGV[1] || die "Unable to get JobReportId\n";

$logrRtn = sub { print @_, "\n"; };

my $jr = XReport::JobREPORT->Open($JRID, 1);
my ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName) = $jr->getValues(qw(LocalFileName SrvName RemoteHostAddr XferMode TargetLocalPathId_IN XferDaemon JobReportName));

(my $FQfileName = getConfValues('LocalPath')->{'L1'}."/IN/$fileName") =~ s/^file:\/\///;
die "Cannot access $FQfileName\n" unless -f $FQfileName;
my $buffer;
&$logrRtn("Opening ", $FQfileName);
my $gz = gzopen($FQfileName, "rb");
die "Cannot open $FQfileName: ".$gzerrno."\n" unless $gz;
my $XLS;

$XLS .= $buffer while $gz->gzread($buffer) > 0;

my $errgznum = ($gz->gzerror()+0);
my $errgzmsg = scalar($gz->gzerror());

$gz->gzclose() ;

if ($errgznum != Z_STREAM_END ) {
  die "Error reading from $FQfileName: $errgznum: $errgzmsg\n" 
}

require "$ENV{'XREPORT_HOME'}/bin/loadExcelConfig.pl";
logIt("DB: ".length($XLS));
logIt("Processing Started at ". localtime());
parseXLSConfig('main',\$XLS);
logIt("CeReport XlsConfig Ended at " . localtime());

exit;

