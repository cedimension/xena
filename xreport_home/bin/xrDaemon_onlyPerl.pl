#!/usr/local/bin/perl -w
#######################################################################
# @(#) $Id: xrDaemon.pl 2250 2008-07-02 12:50:03Z mpezzi $ 
#  
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
#use strict;
#use Data::Dumper;
#use lib($ENV{'XREPORT_HOME'}."/perllib");

BEGIN {
   require XReport;
   XReport->import(redirect => 1, rotatelog => 1);
}

use Win32;
use Win32::Daemon;
use Win32::Console;


use XReport::Util;
use XReport::SUtil;

my $version = do { my @r = (q$Revision: 1.3 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

#die "xxxx", Dumper($main::Application), "\n";
#__END__

InitServer(isdaemon => 1, rotatelog => 1);
(my $testrun) = grep /\-testrun/, @main::ARGV;
#die "testrun: $testrun", join('--::--', @main::ARGV);

my ($SrvName, $debug, $basedir) = getConfValues(qw(SrvName debugLevel basedir));

my $cfg = $XReport::cfg->{'daemon'}->{$SrvName};
my $pgmName = $cfg->{'process'} || die "Program Name to start missing";
my $procAff = $cfg->{$SrvName}->{'procaff'} if ($cfg->{'procaff'});
$pgmName = "$basedir/bin/$pgmName" unless $pgmName =~ /^[\/\\]{2}/;

my $State;

my $ProcessObj = 0;

my $SLEEP_TIMEOUT = 1;
my $SERVICE_BITS = USER_SERVICE_BITS_8;

Win32::Daemon::Timeout( 5 );

Win32::Daemon::StartService() || die "Service not Started - $! - $? - $@\n";
&$logrRtn("xrDaemon Service Started main loop HOME: $ENV{XREPORT_HOME}");
Win32::Daemon::SetServiceBits( $SERVICE_BITS ) || die "xrDaemon Service not set bits\n";
&$logrRtn("xrDaemon Service Service bits set");
#if (!Win32::Daemon::ShowService()) { 
#  &$logrRtn ("xrDaemon ShowService Failed");
#  die "xrDaemon ShowService Failed\n";
#}
#$State = Win32::Daemon::QueryLastMessage();
#&$logrRtn ("last Message $State");

&$logrRtn("xrDaemon Service Loop main loop");
while ( SERVICE_START_PENDING != Win32::Daemon::State() ) {
	&$logrRtn("xrDaemon Start pending state detected sleeping for $SLEEP_TIMEOUT seconds");
	sleep( $SLEEP_TIMEOUT);
	last if $testrun;
}
	
while( 1 ) {
	$State = Win32::Daemon::State();
#  &$logrRtn("Checking State $State." );
  if( !$ProcessObj && ( $testrun || SERVICE_START_PENDING == $State ) ) {
    # Initialization code
    &$logrRtn("xrDaemon Service pending. Setting state to Running." );
    Win32::Daemon::State( SERVICE_RUNNING );
    &$logrRtn( "xrDaemon Starting $pgmName" );
    $ProcessObj = spawnProcess($pgmName, "-N", $SrvName, '-@');
    $ProcessObj->SetProcessAffinitymask($procAff) if ($procAff);
  }
  elsif( SERVICE_PAUSE_PENDING == $State ) {
    &$logrRtn( "xrDaemon Pausing..." );
    $ProcessObj->Suspend();
    Win32::Daemon::State( SERVICE_PAUSED );
    next;
  }
  elsif( SERVICE_CONTINUE_PENDING == $State ) {
    &$logrRtn( "xrDaemon resuming... " );
    $ProcessObj->Resume();
    Win32::Daemon::State( SERVICE_RUNNING );
    next;
  }
  elsif( SERVICE_CONTROL_SHUTDOWN == $State ) {
    &$logrRtn( "xrDaemon Shutting Down... " );
    Win32::Daemon::State( SERVICE_STOP_PENDING, 30 );
	next;
    }
  elsif( SERVICE_STOP_PENDING == $State ) {
    &$logrRtn( "xrDaemon Stopping..." );
    $ProcessObj->Kill(0);
    last;
  }
#  elsif( SERVICE_RUNNING == $State ) {
#    if ( !kill(0, $ProcessObj->GetProcessID()) ) {
#        last;
#      }
#    }
  elsif( SERVICE_CONTROL_INTERROGATE == $State ) {
#    &$logrRtn( "xrDaemon Query... " );
    if ( $ProcessObj->Wait(0) ) {
      last;
    }
    Win32::Daemon::State( SERVICE_RUNNING );
  }
  else {
    &$logrRtn( "xrDaemon Unknown state $State... " );
    if ( $ProcessObj->Wait(0) ) {
      last;
    } else {
      Win32::Daemon::State( SERVICE_RUNNING );
	}
  }
  sleep( $SLEEP_TIMEOUT );
}

Win32::Daemon::State( SERVICE_STOPPED );
sleep( $SLEEP_TIMEOUT );
Win32::Daemon::StopService();

exit 0;

__END__

