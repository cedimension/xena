
use lib("$ENV{'XREPORT_HOME'}/perllib");

use strict;

use XReport::Util;
use XReport::DBUtil;


sub ExportTo_HIST {

  my @JobReportIds = @_; my ($maxl, $totcycles, $atcycle) = (200, 0, 0);

  $totcycles = int(scalar(@JobReportIds)/$maxl)+1;
  
  while( @JobReportIds ) {
    my @jrids = splice( @JobReportIds, 0, $maxl ); $atcycle++;
	
    &$logrRtn("MIGRATE CYCLE $atcycle/$totcycles FOR ". scalar(@jrids) ." ELEMENTS");

    dbExecute("BEGIN TRANSACTION");
  
    &$logrRtn("DELETE FROM HISTORY BEGIN ....");
    dbExecute("
      DELETE FROM tbl_LogicalReports_HIST
      WHERE  JobReportId IN( ". join(",",@jrids) ." )
    ");
    dbExecute("
      DELETE FROM tbl_PhysicalReports_HIST
      WHERE  JobReportId IN( ". join(",",@jrids) ." )
    ");
	dbExecute("
      DELETE FROM tbl_JobReportsElabOptions_HIST
      WHERE  JobReportId IN( ". join(",",@jrids) ." )
    ");
    dbExecute("
      DELETE FROM tbl_JobReports_HIST
      WHERE  JobReportId IN( ". join(",",@jrids) ." )
    ");
    &$logrRtn("DELETE FROM HISTORY END");
    
    &$logrRtn("INSERT INTO HISTORY BEGIN ...");
    &$logrRtn("INSERT INTO HISTORY tbl_JobReports BEGIN ...");
    dbExecute("
      INSERT INTO tbl_JobReports_HIST
      SELECT *
      FROM   tbl_JobReports WITH (INDEX(PK_tbl_JobReports))
      WHERE  JobReportId IN( ". join(",",@jrids) ." )
    ");
    &$logrRtn("INSERT INTO HISTORY tbl_JobReportsElabOptions BEGIN ...");
	dbExecute("
      INSERT INTO tbl_JobReportsElabOptions_HIST
      SELECT *
      FROM   tbl_JobReportsElabOptions WITH (INDEX(PK_tbl_JobReportsElabOptions))
      WHERE  JobReportId IN( ". join(",",@jrids) ." )
    ");
    &$logrRtn("INSERT INTO HISTORY tbl_PhysicalReports BEGIN ...");
    dbExecute("
      INSERT INTO tbl_PhysicalReports_HIST
      SELECT *
      FROM   tbl_PhysicalReports WITH (INDEX(PK_tbl_PhysicalReports))
      WHERE  JobReportId IN( ". join(",",@jrids) ." )
    ");
    &$logrRtn("INSERT INTO HISTORY tbl_LogicalReports BEGIN ...");
    dbExecute("
      INSERT INTO tbl_LogicalReports_HIST
      SELECT *
      FROM   tbl_LogicalReports WITH (INDEX(PK_tbl_LogicalReports))
      WHERE  JobReportId IN( ". join(",",@jrids) ." )
    ");
    &$logrRtn("INSERT INTO HISTORY END");

	&$logrRtn("DELETE FROM CURRENT BEGIN ....");
	&$logrRtn("DELETE FROM CURRENT tbl_LogicalReports BEGIN ....");
    dbExecute("
      DELETE FROM tbl_LogicalReports
      WHERE  JobReportId IN( ". join(",",@jrids) ." )
    ");
	&$logrRtn("DELETE FROM CURRENT tbl_PhysicalReports BEGIN ....");
    dbExecute("
      DELETE FROM tbl_PhysicalReports
      WHERE  JobReportId IN( ". join(",",@jrids) ." )
    ");
	&$logrRtn("DELETE FROM CURRENT tbl_JobReportsElabOptions BEGIN ....");
	dbExecute("
      DELETE FROM tbl_JobReportsElabOptions
      WHERE  JobReportId IN( ". join(",",@jrids) ." )
    ");
	&$logrRtn("DELETE FROM CURRENT tbl_JobReports BEGIN ....");
    dbExecute("
      DELETE FROM tbl_JobReports
      WHERE  JobReportId IN( ". join(",",@jrids) ." )
    ");
    &$logrRtn("DELETE FROM CURRENT END");
  
    dbExecute("COMMIT TRANSACTION");
  }

}

sub ImportFrom_Hist {
}

my ($dbr, @JobReportIds);

$dbr = dbExecute("
  SELECT JobReportId FROM tbl_JobReports
  WHERE 
     DATEDIFF([DAY], XferStartTime, GETDATE()) > 500
");

while(!$dbr->eof()) {
  my $jrid = $dbr->Fields()->Item('JobReportId')->Value();
  push @JobReportIds, $jrid;
  $dbr->MoveNext();
}

ExportTo_HIST( @JobReportIds );
