use strict;

##347165 BA
#perl.exe -I "%PERL5LIB%" "%XREPORT_HOME%\bin\xrUpdateTree.pl" "%XREPORT_TEMP%" 9727 -SITECONF "%XREPORT_SITECONF%" -HOME "%XREPORT_HOME%" -N XENA_TEST3

use XReport;
use lib($main::Application->{'XREPORT_HOME'}."/perllib");
use XReport::Util qw(:CONFIG);
use XReport::JobREPORT; 
#use IO::File; 
use Archive::Tar;
use constant DEBUG => 1;
#use constant DEBUG => 1;
use Compress::Zlib;

#use constant ST_RECEIVED => 16;
#use constant ST_QUEUED => 16;
#use constant ST_INPROCESS => 17;
#use constant ST_COMPLETED => 18;
#use constant ST_PROCERROR => 31;

InitServer(isdaemon => 1); 
#no warn redefine;
*i::logit = sub { 
     foreach my $logln ( split /\n/, join(' ', map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_) ) {
        $main::logger->log($logln);
     }
 };
 

sub print2Log { my @array =@_; return i::logit("$main::myName: ".join(",", map { $_ =~ s/\//\\/g; $_ } @array)."_LINE[".(caller())[2]."]"); }


sub fromTAR2GZ {
	my($inTARfile) = @_;
	die "input file not specified" unless $inTARfile;
	die "input file is not tar file" if ($inTARfile !~ /^(.*)\.tar$/s);
	my $inGZfile = "$1.gz" ;
	$inGZfile =~ s/\.gz\.gz/\.gz/gi;
	unlink $inGZfile if (-f $inGZfile);  
	
	foreach my $filename (grep /\.DATA\.TXT\.gz$/i, Archive::Tar->list_archive($inTARfile))
	{
		my $tar = Archive::Tar->new($inTARfile);
		unlink $inGZfile if (-f $inGZfile); 
		$tar->extract_file($filename, $inGZfile );
		last;
	}
	print2Log( "file '$inGZfile' created with success.\n") if (-f $inGZfile); 
	die "input file does '$inGZfile' not exist" unless (-f $inGZfile);
	unlink $inTARfile if (-f $inTARfile);  
	return $inGZfile; 
}


sub downloadInput {
	($main::veryverbose || DEBUG) && print2Log("call downloadInput()"); 
	my $JRID = shift;
	my $args = {@_};
	my $Workdir = $args->{Workdir};
	
	
	my $jr = XReport::JobREPORT->Open($JRID, 1);
	print2Log("ref(jr)=".ref($jr));
	
	my ($LocalPathId_IN, $LocalPathId_OUT, $jobReportId, $status, $fileName, $SrvName, $PeerHost, $XferMode, $JobName, $JobReportName, $XferEndTime) = 
			$jr->getValues(qw(LocalPathId_IN LocalPathId_OUT JobReportId Status LocalFileName SrvName RemoteHostAddr XferMode XferDaemon JobReportName XferEndTime));
	my $TargetLocalPathIn = $XReport::cfg->{'LocalPath'}->{$LocalPathId_IN};
	my $TargetLocalPathOut = $XReport::cfg->{'LocalPath'}->{$LocalPathId_OUT};

	my $cfgvars = {}; 
	@{$cfgvars}{qw(TargetLocalPathIn  TargetLocalPathOut LocalPathId_IN LocalPathId_OUT JobReportId Workdir XferEndTime Status LocalFileName SrvName RemoteHostAddr XferMode
						XferDaemon JobReportName)} = ($TargetLocalPathIn, $TargetLocalPathOut, $LocalPathId_IN, $LocalPathId_OUT, $jobReportId, $Workdir, $XferEndTime, $status, $fileName, $SrvName, $PeerHost, $XferMode, $JobName, $JobReportName);
		
		
		
	my $FQoutName =  $cfgvars->{Workdir}.'/'. File::Basename::basename($cfgvars->{'LocalFileName'});
	$FQoutName =~ s/\.gz\.tar$/\.IN\.gz\.tar/; 
	my $TempFQoutName = "$FQoutName\.PENDING"; 
	unlink $TempFQoutName if(-f $TempFQoutName );
	
	if ($cfgvars->{TargetLocalPathIn} !~ /^centera:\/\//i)
	{
		die("Error in TargetLocalPathIn:".$cfgvars->{TargetLocalPathIn} ) if ($cfgvars->{TargetLocalPathIn} !~ /^file:\/\//i);
		$jr->setValues('line_iexec' => ''); 
        my $INPUT = $jr->get_INPUT_ARCHIVE();

		print2Log("ref(INPUT)=".ref($INPUT));
		
        die "Unable to acquire INPUT_ARCHIVE handler -", Data::Dumper::Dumper($jr), "\n" unless $INPUT;

        my $is = $INPUT->get_INPUT_STREAM_GZ($jr, random_access => 0); 
		
		print2Log("ref(is)=".ref($is));
		
		
		die "Unable to acquire INPUT_STREAM_GZ handler -", Data::Dumper::Dumper($jr), "\n" unless $is;
       
        ($main::veryverbose || DEBUG) && print2Log("INPUT STREAM acquired - INPUT REF: ".ref($INPUT)." IS ref: ".ref($is));
		
		my $outfh = IO::File->new($TempFQoutName, 'w');
        $outfh->binmode();
		$is->Open();
        my $buff = ' ' x 131072; 
		while(1) { 
			$is->read($buff, 131072); 
			last if $buff eq ''; 
			print $outfh $buff;  
		}
        $is->Close();
        $outfh->close();
	}
	else
	{
		($main::veryverbose || DEBUG) && print2Log("input from Centera");  
		my $ijrar = $jr->get_INPUT_ARCHIVE();   
		print2Log("ref(ijrar)=".ref($ijrar));
		my $cdamfh = $ijrar->get_INPUT_ARCHIVE(); 
		
		my $FileTags = $cdamfh->FileTags(); 
		foreach my $hash (@$FileTags)
		{ 
			foreach my $key (sort keys %$hash)
			{
				($main::veryverbose || DEBUG) && print2Log("FileTags - $key: ". $hash->{$key} ); 
				if(( $key =~ /^md5$/i) && (!($hash->{$key})) )
				{
					$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 1;
					print2Log("noCheckMd5 on XReport::ARCHIVE::Centera::File::INPUT.pm"); 
				}
			}
		}
		
		my $FileList = $cdamfh->FileList();
		foreach my $iFileName (grep /\.DATA\.TXT\.gz\.tar$/i, @$FileList) {
			($main::veryverbose || DEBUG) && print2Log("iFileName: ".$iFileName); 
			my $INPUT = $cdamfh->LocateFile($iFileName); 
			die("ExtractFile $iFileName NOT Located") if !$INPUT; 

			$INPUT->Open();
			my $OUTPUT = IO::File->new(">". $TempFQoutName)
			or die("OPEN OUTPUT ERROR for \"$FQoutName\.PENDING\" $!"); 
			$OUTPUT->binmode();
			my $osize = $cdamfh->ExtractMember2FH($INPUT, $OUTPUT);
			($main::veryverbose || DEBUG) && print2Log("osize: ".$osize); 
        	$INPUT->Close();
        	$OUTPUT->close; 
			last;
		}
	} 
	
	unlink $FQoutName if(-f $FQoutName );
	die("Error - the Input file [$FQoutName] already exists.") if (-f $FQoutName );
	#unlink $FQoutName if ((-e $FQoutName) && (-f $FQoutName));
	$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 0; 
	rename($TempFQoutName, $FQoutName) or die("ERROR in RENAME file [$FQoutName]."); 
	$FQoutName = fromTAR2GZ($FQoutName) if ($FQoutName =~ /^(.*)\.tar$/s);
	$FQoutName = fromGZ2TXT($FQoutName) if ($FQoutName =~ /^(.*)\.gz$/s);
	my $expandedFQoutName = fromEBCDIC2EXPANDED($FQoutName) if ($FQoutName =~ /^(.*)\.bin$/s);
	#unlink $FQoutName or die("ERROR in deleting of file [$FQoutName]."); 
	rename($FQoutName, $FQoutName.".NOTEXPANDED") or die("ERROR in RENAME file [$expandedFQoutName]."); 
	rename($expandedFQoutName, $FQoutName) or die("ERROR in RENAME file [$expandedFQoutName]."); 
	print2Log("Input archive file created with success: $FQoutName.") if(-f $FQoutName );  
	die("Input archive wasn't created with success: $FQoutName.") unless (-f $FQoutName );  
	
}

sub fromGZ2TXT {
	my($inGZfile) = @_;
	my $buffer;
	my $gz = Compress::Zlib::gzopen($inGZfile, "rb");
	die "unable to open $inGZfile: $gzerrno\n" unless $gz;
	#die "unable to open $inGZfile" unless $gz;
	print2Log( "Input File $inGZfile opened."); 
	
	my $TXTfile = $inGZfile.'.TXT'; 
	$TXTfile =~ s/\.DATA\.TXT\.IN\.gz\.TXT$/\.bin/g; 
	unlink $TXTfile if (-f $TXTfile); 

	open(my $OUT_TXT_handler,">$TXTfile")  or die("LOGFILE OUTPUT OPEN ERROR $TXTfile rc=$!\n"); 
	binmode $OUT_TXT_handler;
	print $OUT_TXT_handler $buffer while $gz->gzread($buffer) > 0 ;
	my $errgznum = ($gz->gzerror()+0);
	my $errgzmsg = scalar($gz->gzerror());
	$OUT_TXT_handler->close;
	$gz->gzclose() ;
	print2Log(  "file '$TXTfile' created with success.") if (-f $TXTfile); 
	die "input file does '$TXTfile' not exist" unless (-f $TXTfile);
	unlink $inGZfile if (-f $inGZfile); 
	return $TXTfile;
}

sub fromEBCDIC2EXPANDED{
	my($TXTfile) = @_; 	 
	my $outfn = $TXTfile.'.ASCII.TXT' ;  
	my $outfnexpanded = $TXTfile.'.EXPANDED.TXT' ;  
	unlink $outfn if (-f $outfn); 
	my $OUThandler;
	if($main::veryverbose || DEBUG)
	{
		open($OUThandler,">$outfn")  or die("LOGFILE OUTPUT OPEN ERROR $outfn rc=$!\n");  
		binmode $OUThandler; 
	}
	open(my $OUThandlerEXP,">$outfnexpanded")  or die("LOGFILE OUTPUT OPEN ERROR $outfnexpanded rc=$!\n"); 
	
	binmode $OUThandlerEXP; 
	
	my $INhandler = new IO::File("<$TXTfile") || die "Unable to open \"$TXTfile\" - $!";    
	my $translator = Convert::EBCDIC->new(); 
	while ( !$INhandler->eof() ) {
		$INhandler->read(my $buff, 2);
		my $lrec = unpack("n", $buff); 
		$INhandler->read(my $rec, $lrec); 
		
		$main::line++;
		($main::veryverbose || DEBUG) && print2Log( $main::line.":". unpack('H*', $rec)) if $main::line < 10;
		$main::doManageAFP = 1 if($rec =~ /^\x5a/);
		print $OUThandlerEXP $rec;
		
		print ($OUThandler $translator->toascii($rec), "\r\n") if ($main::veryverbose || DEBUG); 
	}
	if ($main::veryverbose || DEBUG)
	{
		close $OUThandler ; 
		die "input file does '$outfn' not exist" unless (-f $outfn);
		print2Log( "file '$outfn' created with success.") if (-f $outfn);
	}
	close $OUThandlerEXP;
	close $INhandler;
	die "input file does '$outfnexpanded' not exist" unless (-f $outfnexpanded);
	print2Log( "file '$outfnexpanded' created with success.") if (-f $outfnexpanded);
	return $outfnexpanded; 
}

 

my $debgRtn = sub {
    i::logit(@_) if  ($main::debug || $main::veryverbose); 
}; 
my $logrRtn = sub { 
     i::logit(@_); 
};


my $Workdir = $ARGV[0];
my $JRID = $ARGV[1];
$main::myName = ( split( /[\/\\]/, $0 ) )[-1];
$main::myName = ( split( /\./, $main::myName ) )[0];

mkdir $Workdir unless -d $Workdir;
die "unable to access $Workdir" unless ( -d $Workdir ); 

downloadInput($JRID, Workdir => $Workdir ); 

TermServer();

exit 0;
__END__
my $status = $jr->doSingleParserElab();

sub {
    my ($self, $page) = (shift, shift);
    my $getVars = 0;
    my $bundleslist = [];
    $U::UserVars{'$$bundles$$'} = '';
    @{U::UserVars}{qw(UserRef)} = ("RID ADD.TI GT 30.000");
    if ( 1 ) { 
        my @tvars;
    GETVARS01: { $getVars = 0;
                    if ( @tvars = unpack('a5', $page->GetRawPageRect(4,2,4,6)) ) {

                        @{U::UserVars}{'CUTVAR'} = @tvars;

                        $getVars = 1;
                    }
                    last GETVARS01 if $getVars;
                }                                                                                                                   
        if ($getVars) {

        }
    }
    $U::UserVars{'$$bundles$$'} = join(' ', @{$bundleslist} )
                                     if ( scalar(@{$bundleslist} );
    $U::UserVars{UserRef} = $page->getJobReportValues('JobReportDescr') if ( !exists($U::UserVars{UserRef}) ); 
    return $getVars;
}