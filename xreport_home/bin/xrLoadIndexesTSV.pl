#!/usr/bin/perl -w

use lib "$ENV{XREPORT_HOME}/perllib";
use strict "vars";

use File::Basename;

use Data::Dumper;

use XReport;
use XReport::Util;
use XReport::DBUtil;
use XReport::JobREPORT;

my $workdir = $ARGV[0];
my $JRID = $ARGV[1] || die "Unable to get JobReportId\n";

$logrRtn = sub { print @_, "\n"; };

my $jr = XReport::JobREPORT->Open($JRID, 1);
my ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName) = $jr->getValues(qw(LocalFileName SrvName RemoteHostAddr XferMode
										  TargetLocalPathId_IN XferDaemon JobReportName));

my $manfqn = $workdir.'/'. basename($jr->getFileName('IXFILE', '$$MANIFEST$$'));
my $manifest = new FileHandle("<$manfqn");
exit 0 unless $manifest;

while (<$manifest>) {
  chomp;
  my ($TableName, $FileName, $TotEntries) = split /\t/, $_; 
  &$logrRtn("DELETING INDEX ENTRIES from \"$TableName\" ");
  dbExecute(
	    "DELETE from [tbl_JobReportsIndexTables] where JobReportId = $JRID AND IndexName = '$TableName'"
	   );
  my $dbc = XReport::DBUtil->get_ix_dbc($TableName);
  $dbc->dbExecute(
		  "DELETE from [tbl_IDX_$TableName] where JobReportId = $JRID"
		 );  
  print "now starting to load INDEX $TableName from $FileName ($TotEntries Entries)\n";
  eval { $jr->LoadIndexTable( $TableName, $workdir.'/'.$FileName, $TotEntries ); };
  print "Error code is now $?\n"; # $? = 128;
  die "LoadIndexTable ERROR:\n$@" if $@;
#  if ( $TotEntries > 1 ) {
#    $jr->LoadIndexTable( $TableName, $workdir.'/'.$FileName, $TotEntries );
#  }
#  else {
#    my $fqn = $workdir.'/'.$FileName;
#    my $fh = new FileHandle("<$fqn");
#    read($fh, my $buff, -s $fqn);
#    my $values = join(',', map { ( /\D/ ? "'$_'" : $_ ) } split /\t/, ( split /[\r\n]+/, $buff )[1] );
#    print "Values to insert:\n", $values, "\n";
#    $dbc->dbExecute(
#		  "INSERT into tbl_IDX_$TableName VALUES($values)"
#		 );  
    
#  }
#  print "MESSAGES: ", $@, "\n";
}

exit;

