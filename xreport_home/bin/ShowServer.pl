#/usr/bin/perl -w

use strict;

use FileHandle;
use File::Basename;
use Data::Dumper;
use POSIX qw(strftime);

sub showIISLog {
  my @html = ();
  return '<pre>'."Unable to access Log File \"$main::ShowServer_Info->{iis_logfile}\" for site $main::ShowServer_Info->{siteid}\n"
    .'</pre>' unless $main::ShowServer_Info->{iis_logfile} && -f $main::ShowServer_Info->{iis_logfile};
  push @html, "File: $main::ShowServer_Info->{iis_logfile} size: $main::ShowServer_Info->{iis_logsize} at ". localtime()."\n";
  (my $iis_logfh = new FileHandle("<$main::ShowServer_Info->{iis_logfile}")) || push @html, "ERROR ACCESSING $main::ShowServer_Info->{iis_logfile} - $!";
  if ( $iis_logfh ) {
    $iis_logfh->binmode();
    $iis_logfh->seek(-63000, 2);
    $iis_logfh->read(my $buff, 63000);
    $iis_logfh->close();
    $buff =~ s/[\s\x00]+$//gs;
    $buff = substr($buff, (length($buff) - 32760)) if length($buff) > 32760;
#    $buff =~ s/.*(.{1,32760})$/$1/;
    push @html, $buff."\n";
  }
  push @html, localtime()."\n";
  return '<pre>'.join('',@html).'</pre>';
}

sub strip{
	my $str = $_[0];

	$str =~ s/</</g;
	$str =~ s/>/>/g;
	$str =~ s/[\x00-\x1F]/<b>.<\/b>/g;
	return $str;
}

sub TableHdr {
  my ($title) = (shift);
  my $lvl = shift || 'h2';
  $title .= " ".$main::Application->{'ApplName'};
  return ("<HR /><$lvl>$title</$lvl><HR />"
	  . '<table title="'.$title.'" cellpadding=3 cellspacing=2 WIDTH="98%">'
	  . "<tr><th style=\"width: 15\%;\">Name</th><th>Value</th></tr>\n"
	 );
}

sub addTableRow {
  return '<tr><td style="width: 15%;">' . $_[0] . '</td><td>' . $_[1] . '</td></tr>';
}

sub showCollection {
  my ($hdr, $collection) = (shift, shift);
  my $keysub = shift;

  my @html = ();
  push @html, TableHdr($hdr);
  
  for my $j ( 1..$collection->{'Count'} ) {
    my $keyn = $collection->Key($j);
    next if $keyn eq 'ALL_HTTP' or $keyn eq 'ALL_RAW' or $keyn eq 'iis.cfg.perl'; 
    push @html, addTableRow($keyn, ($keysub ? &{$keysub}($keyn)->Item() : $collection->Item($j)) );
  }
  push @html, '</table>';
  return join('', @html);
}

sub showVarHash {
  my $hdr = shift;
  my $VarHash = shift;
  my @html = ();

  push @html, ($_[0] ?  '<table>' : TableHdr($hdr) );
  while ( my ($var, $val) = each %$VarHash ) {
    push @html, addTableRow($var, (ref($val) eq 'HASH' ? showVarHash($var, $val, 1) : $val ));
  }
  push @html, '</table>';
  return join('', @html);
}

sub PerlNameSpace(\%$)
{
  my ($package,$packname) =  @_;
  my $collection = {};
  
  foreach my $symname (sort keys %$package) {
    our ($sym, %sym, @sym);
    local *sym = $$package{$symname};
    $collection->{Functions}->{strip($symname).'()'} = ref(\&sym)  if (defined &sym);
    $collection->{Arrays}->{"\@".strip($symname)}    = ref(\@sym)  if (defined @sym);
    $collection->{Scalars}->{"\$".strip($symname)}   = strip($sym) if ((defined $sym) && (strip($symname) !~ /Config_SH|summary/i)); 
    $collection->{Hashes}->{"\%".strip($symname)}    = ref(\%sym)  if ((defined %sym) && !($symname =~/::/));
    $collection->{Packages}->{strip($symname)}       = PerlNameSpace(\%sym, $symname) if (($packname =~ /main::/i) 
										&& (defined %sym) 
										&& ($symname =~ /::/) 
										&& ($symname !~ /^main::/i) );
  }
  return $collection;
}

sub checkDirAccess {
  my $dirpath = shift;
  return 'Does Not Exist' unless -e $dirpath;
  return 'Not A Directory' unless -d $dirpath;

  my $probebuff = "W" x 80;
  my $probefile = "$dirpath/probe.txt";
  my $expected_size = length($probebuff) + 2;
  
  my $xfh = new FileHandle(">$probefile") || return "unable to create $probefile - $!";

  print $xfh $probebuff, "\n";
  $xfh->close();

  return ( $expected_size == -s $probefile ? 'OK' : 'Error during write - probe size: ' . $expected_size . ' File size: ' . -s $probefile);
}

sub checkWorkDir {
  my ($hdr, $workdir) = (shift, shift);

  return TableHdr($hdr).addTableRow('WORKDIR', 'Not Configured').'</table>' unless $workdir;  
  return TableHdr($hdr).addTableRow($workdir, 'Not a directory').'</table>' unless -d $workdir;  

  return (TableHdr($hdr)
	  . addTableRow($workdir, 'OK')
	  . join('', map {addTableRow("$workdir\\$_", checkDirAccess("$workdir\\$_"))} qw(UsersSaveArea UsersXML UsersExcel))
	  . '</table>'
	 );  
}

sub ShowServer {
my $parms = { @_ };
my $xrcfg = {};
my $skipXreport = delete($parms->{skipXreport});
my $rqst = $main::Request->ServerVariables('QUERY_STRING')->item() || '';
if ( !$skipXreport ) {
#  eval "use lib \"\$ENV{XREPORT_HOME}\\\\perllib\"; require XReport; XReport->import();";
#  $xrcfg->{error} = $@ if $@;
  if ( !$@ && $rqst =~ /reloadcfg/i ) {
    $XReport::initialized = 0;
    $main::Application->{'iis.cfg.perl'} = '';
    $main::Application->{'cfg.processed'} = '0';
  }
  eval "use lib \"\$ENV{XREPORT_HOME}\\\\perllib\"; require XReport; XReport->import();";
  $xrcfg->{error} = $@ if $@;
  if ( !$@ && $rqst =~ /reloadcfg/i ) {
    $main::Application->{'cfg.reloaded'} = ''. localtime() . ''; 
  }
}
$xrcfg = $XReport::cfg unless $@;

my $VERSION = '1.3';

my $todaystr = POSIX::strftime('%y%m%d', localtime());
my $toDayFile = 'ex'.$todaystr.'.log';

my $hname = $ENV{'COMPUTERNAME'};
my $UserName = $main::Session->{'UserName'};
#$main::ShowServer_Info->{iis_logfile} = Win32::OLE->GetObject("IIS://$ENV{'COMPUTERNAME'}/W3SVC/1")->{'LogFileDirectory'}."\\W3SVC1\\$toDayFile";
$main::ShowServer_Info->{siteid} = $main::Request->ServerVariables('INSTANCE_ID')->Item();
my $olepath = "IIS://$ENV{'COMPUTERNAME'}/W3SVC/$main::ShowServer_Info->{siteid}";
my $siteobj = Win32::OLE->GetObject($olepath);
$main::ShowServer_Info->{OLEResult} = Win32::FormatMessage(Win32::GetLastError()) . " trying to access \"$olepath\"";
my $iis_logdir = $siteobj->{'LogFileDirectory'} if $siteobj;
$main::ShowServer_Info->{iis_logfile} = "$iis_logdir\\W3SVC$main::ShowServer_Info->{siteid}\\$toDayFile" if $iis_logdir;
unless ( -f $main::ShowServer_Info->{iis_logfile} ) {
  $toDayFile = 'nc'.$todaystr.'.log';
  $main::ShowServer_Info->{iis_logfile} = "$iis_logdir\\W3SVC$main::ShowServer_Info->{siteid}\\$toDayFile" if $iis_logdir;
}
#$main::ShowServer_Info->{iis_logfile} = Win32::OLE->GetObject("IIS://$ENV{'COMPUTERNAME'}/W3SVC/$main::ShowServer_Info->{siteid}")->{'LogFileDirectory'}."\\W3SVC$main::ShowServer_Info->{siteid}\\$toDayFile";
$main::ShowServer_Info->{iis_logsize} = -s $main::ShowServer_Info->{iis_logfile} if $main::ShowServer_Info->{iis_logfile};
my $sitepath = $main::Server->MapPath('.');
my $xrpath = dirname(dirname($sitepath));

$Data::Dumper::Terse = 1;
$main::Response->Write(''
		       . '<html>'
		       . '<head>'
		       . '<title>ShowServer '.$VERSION.'</title>'
		       . '<style>'
		       . ' body { border: 0; margin:5; padding:0; background-color: white;}'
		       . ' body { font-family: Tahoma, Arial, Verdana; font-size:11px; color: #000080;text-align: left;}   '
		       . ' a:link, a:visited {color: #483D8B;font-style: italic;text-decoration: none;}'
		       . ' a:visited {color: #4B0082;}'
		       . ' th {border: 1px solid darkgray;font-size: 16px; color: #E8E800; background: #483D8B; }'
		       . ' td {border: 1px solid darkgray;font-size: 12px; }'
		       . '</style>'
		       . '</head>'
		       . '<body>'
		       . '<h1>Show General Variables at '.POSIX::strftime('%y%m%d %H%M%S', localtime()).'</h1> '
		       . '<hr noshade size="2" color=#000080>'
		       . showVarHash("Misc Known Variables", 
				     {
                                      'OLEResult'            => $main::ShowServer_Info->{OLEResult},
				      'siteid'               => $main::Request->ServerVariables('INSTANCE_ID')->Item(),
				      'IIS log file'         => $main::ShowServer_Info->{iis_logfile},
				      'AUTH_USER'            => $main::Request->ServerVariables('AUTH_USER')->Item(),
				      'cfg.authenticate'     => $main::Application->{'cfg.authenticate'}."\\$UserName",
				      'Computer Name'        => $hname, 
				      'Process Id'           => $$,
				      'Application name'     => $main::Application->{'ApplName'},
				      'Application cwd'      => Win32->GetCwd(),
				      'Application Domain'   => Win32->DomainName(),
				      'Application NodeName' => Win32->NodeName(),
				      'Application Login'    => Win32->LoginName(),
				      'Site Work Directory'  => $main::Application->{'cfg.workdir'},
                                      'Root Parent Dir'      => $xrpath,
				      'Current Request Cont.'=> $parms->{content} || '',
				     })
		       . checkWorkDir("Site Working Directory Status", $main::Application->{'cfg.workdir'})
		       . showVarHash("SYSTEM ENVIRONMENT",\%ENV)
		       . showCollection("IIS Server Collection", $main::Request->{'ServerVariables'}, sub {$main::Request->ServerVariables(@_)})
		       . showCollection("IIS Application Collection", $main::Application->{Contents})
		       . showCollection("IIS Session Collection", $main::Session->{'Contents'})
		       . showCollection("Request Params", $main::Request->{'Params'}, sub {$main::Request->Params(@_)})
		       . showCollection("Request Cookies", $main::Request->{'Cookies'}, sub {$main::Request->Cookies(@_)})
                       );
$main::Response->Write('<h2>Last 32k of IIS log</h2><hr/>'.showIISLog().'<hr/>');
$main::Response->Write(''
		       . '<h2>Perl NameSpace</h2><hr/><pre>$main:: => '.Dumper(\%{ PerlNameSpace(%main::, 'main::')}).'</pre><hr/>'
		       . '</body></html>'
		      );
}

1;
