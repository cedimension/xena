use DBI;
use File::Slurp;
use File::Copy;
#use SQL::SplitStatement;
use XReport;
use Data::Dumper;

use constant DB_TIMEOUT => 600;
use constant DEBUG => 0;
use constant DO_NOT_WRITE_WARNINGS => 1;



sub write2log
{
  my( $package, $filename, $line ) = caller(); 
  i::logit(join('', @_).'_caller:'.join('::',$package, $filename, $line));

}
sub debug2log
{
  return unless ($main::veryverbose || DEBUG) ;
  my( $package, $filename, $line ) = caller(); 
  i::logit(join('', @_).'_caller:'.join('::',$package, $filename, $line));

}


sub connectDB {
	my( $dbstr, $autoCommit) = @_;
	$autoCommit = 0  unless $autoCommit;
	$autoCommit = 1  if $autoCommit;
	write2log ("sub connectDB() autoCommit:[$autoCommit]");
	die "error in Sub connectDB : dbstr missing" unless defined $dbstr;
	my $cstr_dbstr = $XReport::cfg->{'dbase'}->{$dbstr};
	die "ApplicationError: MISSING DB DEFINITION FOR $dbstr"  if (!$cstr_dbstr);
	(my $Provider) = $cstr_dbstr =~ /PROVIDER=([^;]+);/i;
	(my $user) = $cstr_dbstr =~ /User ID=([^;]+);/i;
	(my $db_pass) = $cstr_dbstr =~ /Password=([^;]+);/i;
	(my $db_name) = $cstr_dbstr =~ /Initial Catalog=([^;]+);/i;
	(my $db_instance) = $cstr_dbstr =~ /Data Source=([^;]+);/i;
	(my $is) = $cstr_dbstr =~ /Integrated Security=([^;]+);/i;

	my $dsn  = "DBI:ADO:Provider=$Provider;Persist Security Info=False;".
	(($user and ($user ne ""))?"User ID=$user;PWD=$db_pass;":"").
	(($is and ($is ne ""))?"Integrated Security=$is;":"").
	"Initial Catalog=$db_name;Data Source=$db_instance;";
	
	debug2log ("dsn:[$dsn]");

	my $dbh = DBI->connect($dsn,$user,$db_pass,{ RaiseError => 1, AutoCommit => $autoCommit, ado_ConnectionTimeout => DB_TIMEOUT, CommandTimeout => DB_TIMEOUT})
			  or die("CONNECT ERROR:$DBI::errstr");
	write2log ("Connected to [$db_name]");
	return $dbh ;
}

my $Workdir = $ARGV[0];
my $JRID = $ARGV[1];
write2log ("starting! workdir:[$Workdir] - JRID:[$JRID].");  
die("The configuration file xrUpdateTreeTables.xml does not exist.") unless(exists $XReport::cfg->{xrUpdateTreeTables}); 
my $update_tree_sql_scripts_dir = $XReport::cfg->{xrUpdateTreeTables}->{update_tree_sql_scripts_dir};



die "The dir \"$update_tree_sql_scripts_dir\" does not exist." unless(-d $update_tree_sql_scripts_dir);
my $dryRun = $XReport::cfg->{xrUpdateTreeTables}->{dryRun};
die "The value of parameter dryrun is not correct:$dryRun" if $dryRun !~ /^[01]$/;
opendir my $dir, "$update_tree_sql_scripts_dir" or die "Cannot open directory: $!";
my @files = readdir $dir;
closedir $dir;
foreach(grep{$_ =~ /\.sql$/i} @files)
{
	unlink "$Workdir/$_.sql" if(-f "$Workdir/$_.sql");
	copy("$update_tree_sql_scripts_dir/$_","$Workdir/$_") or die "Error in copy of $update_tree_sql_scripts_dir/$_.sql - $!";
}

my @listOfTables =  qw(Profiles Folders FoldersRules UsersProfiles UserAliases Users VarSetsValues NamedReportsGroups  ProfilesFoldersTrees);

my $autoCommit = $XReport::cfg->{xrUpdateTreeTables}->{autoCommit}; 
die "The value of parameter autoCommit is not correct:$autoCommit" if $autoCommit !~ /^[01]$/;
$autoCommit = 0 if $dryRun;
my $dbh = connectDB('XREPORT', $autoCommit);
write2log( "Parameter-list. autoCommit=[$autoCommit] - dryRun=[$dryRun].");

foreach my $prefix (qw(tbl_ Merge_tbl_)) {
	foreach my $table (@listOfTables) {
		my $sqlFile = "$Workdir/$prefix$table.sql";
		die "File not found:$sqlFile" unless (-f $sqlFile); 
		write2log( "Processing of [$sqlFile].");
		my $sql_blob = read_file( $sqlFile ) ;
		my @statements = grep{length($_)} split(/GO\s*[\r\n]+/i, $sql_blob);

		foreach my $statement (@statements) {
			die ("The statement contains GO:$statement") if($statement =~ /GO\s*[\r\n]+/i);

			if(DO_NOT_WRITE_WARNINGS )
			{
				open(OLDSTDERR, '>&STDERR') or die "Error in open: $!";
				close STDERR or die "Error in close: $!"; 
			} 

			eval {$dbh->do( $statement );};
			if(( $@ ) or ($dbh->errstr))
			{
				if(DO_NOT_WRITE_WARNINGS)
				{
					open(STDERR, '>&OLDSTDERR') or die "Error in open: $!";
				}
				my @description_errors = grep{$_ !~ /Changing any part of an object name could break scripts and stored procedures/i}
				map{ $_ =~ s/^\s*Description\s*:\s*//; $_}  grep {/Description\s*:/i} split(/[\n\r]+/, $dbh->errstr);
				die "An error occurred for statement[$statement]:".join('::',@description_errors) if(scalar(@description_errors));
				die "An error occurred:$@"  if( $@ and ($@ =~ /error/i));
				if(DO_NOT_WRITE_WARNINGS)
				{
					close STDERR or die "Error in close: $!";
				} 
			}
			if(DO_NOT_WRITE_WARNINGS)
			{
				open(STDERR, '>&OLDSTDERR') or die "Error in open: $!";
			}
			
		}
	}
}

if(($autoCommit == 0) and (!$dryRun))
{
	eval {$dbh->do( "COMMIT" );};
	if(( $@ ) or ($dbh->errstr))
	{
		die "An error occurred:".$@;
	}
	write2log("COMMIT executed with succes!");
} 
write2log("ROLLBACK executed with succes!") if(($autoCommit == 0) and ($dryRun));
write2log("Ending!");


