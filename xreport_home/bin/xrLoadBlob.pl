#!/usr/bin/perl -w
#######################################################################
# @(#) $Id: xrFtpServ.pl 2196 2008-03-10 09:55:02Z mpezzi $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################

use lib "$ENV{XREPORT_HOME}/perllib";
use strict "vars";

use POSIX qw(strftime);
use FileHandle;
use File::Basename;

use File::Path;
use File::Copy 'cp';
use Compress::Zlib;
use XML::Simple;

use Data::Dumper;

use XReport;
use XReport::Util;
use XReport::DBUtil;
use XReport::JobREPORT;
use XReport::ARCHIVE;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

my $workdir = $ARGV[0];
my $JRID = $ARGV[1] || die "Unable to get JobReportId\n";
my $ArchiveContentFN = '_ArchiveContent.tsv';

if (-d $workdir ) {
#  $workdir =~ s/\//\\/sig;
  my @flist = glob $workdir.'\*'; 
  &$logrRtn( "deleting now\n", join("\n", @flist));
  unlink @flist;
}

my $debgRtn = sub { warn map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n" if $main::debug; };
my $logrRtn = sub { print map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n"; };

sub buildParser {
  my ($outvars, $invars) = (shift, shift);
  
  my $outxform = { ($outvars =~ /(\S+)(?:\s+(\w+)\.){0,1}/osgi)  };
  my $inxform = { ($invars =~ /(\S+)(?:\s+(\w+)\.){0,1}/osgi)  };
  &$debgRtn("outxform:\n", Dumper($outxform)); 
  &$debgRtn("inxform:\n", Dumper($inxform)); 
  require XReport::INFORMAT if scalar(keys %$inxform);
#  require XReport::OUTFORMAT if scalar(keys %$outxform);

  my $outlist = [grep !/\w+\./, split(/\s+/, $outvars) ];
  my $inlist = [grep !/\w+\./, split(/\s+/, $invars) ];
  
  my $code = "sub { my \$input = shift; return {\n";
  for my $j ( 0..$#{$outlist} ) {
    
    my ($outvarn, $invarn) = ($outlist->[$j], $inlist->[$j]);
    &$debgRtn("Checking $outvarn => $invarn");
    my $invalue = ($invarn =~ /^#(.*)?#$/ ? "'$1'" : 
		   $inxform->{$invarn} && XReport::INFORMAT->can($inxform->{$invarn}) ? 
		   'XReport::INFORMAT::'."$inxform->{$invarn}(\\\$input->{$invarn})" : 
		   "\$input->{$invarn}");
    $code .= $outvarn . ' => ' . ($invarn =~ /^#(.*)?#$/ ? $invalue . ",\n" : 
				  ($outxform->{$outvarn} && XReport::OUTFORMAT->can($outxform->{$outvarn}) ? 
				   'XReport::OUTFORMAT->'. "$outxform->{$outvarn}($invalue)" : $invalue
				  )
				  . " || '',\n"
				 ); 
  }
  $code .= "};\n}";
  &$debgRtn("CODE:\n", $code); 
  eval($code);
  if ($@) {
    &$logrRtn("CODE:\n", $code); 
    die "Error during building of parsing routine - $@";
  }
  return (eval($code), @$outlist);
}

sub getFH {
}

my $jr = XReport::JobREPORT->Open($JRID, 1);
my ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName, $XferEndTime) = 
  $jr->getValues(qw(LocalFileName SrvName RemoteHostAddr XferMode TargetLocalPathId_IN XferDaemon JobReportName XferEndTime));
my $JRAttrs;
@{$JRAttrs}{qw(XferEndTime UserRef UserTimeRef UserTimeElab)} = ($XferEndTime, '', $XferEndTime, $XferEndTime);
&$debgRtn("$JRID Attributes:\n", $JRAttrs);  

$jr->deleteElab();

my $cfgvars = {};
@{$cfgvars}{qw(LocalFileName SrvName RemoteHostAddr XferMode
	       TargetLocalPathId_IN XferDaemon JobReportName)} = ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName);

$tgtIN = 'L1' unless $tgtIN;

my $rptconf_in = $jr->getFileName('PARSE');
my $FQcfgName = $workdir.'/'. basename($jr->getFileName('LOGPARSE'));
my $cfgfhin = new FileHandle("<".$rptconf_in) || die "unable to access rptconf $rptconf_in\n";
my $cfgfhou = new FileHandle(">".$FQcfgName) || die "unable to access rptconf $FQcfgName\n";
my $cfgtext = '';
while (<$cfgfhin>) {
  my $lout = $_;
  $lout =~ s/\$(\w+)/$cfgvars->{$1}/sg;
  print $cfgfhou $lout;
  $cfgtext .= $lout;
}
close $cfgfhin;
close $cfgfhou;

&$debgRtn("Parsing ", $FQcfgName, " contents:\n", $cfgtext);
my $reportcfg = XMLin($cfgtext,
		      ForceArray => [ qw(index loadzip op ix) ],
		      KeyAttr => {'index' => 'name', 'loadzip' => 'name', 'op' => 'name', ix => 'name'},
		     );

(my $FQfileName = getConfValues('LocalPath')->{$tgtIN}."/IN/$fileName") =~ s/^file:\/\///;
die "Cannot access $FQfileName\n" unless -f $FQfileName;
(my $FQcntlName = $FQfileName) =~ s/DATA\.TXT\.gz$/CNTRL.TXT/osi;
die "Cannot access $FQcntlName\n" unless -f $FQcntlName;

my $buffer;
&$logrRtn("Opening ", $FQfileName);
my $gz = gzopen($FQfileName, "rb");
die "Cannot open $FQfileName: ".$gzerrno."\n" unless $gz;

my $FQzipName = $workdir.'/XRARCHIVE.zip'; 
my $outzip = new FileHandle('>'.$FQzipName) || die "unable to open $FQcntlName - $?\n";

binmode $outzip;

print $outzip $buffer while $gz->gzread($buffer) > 0 ;

my $errgznum = ($gz->gzerror()+0);
my $errgzmsg = scalar($gz->gzerror());

$outzip->close;
$gz->gzclose() ;

if ($errgznum != Z_STREAM_END ) {
  unlink $FQzipName;
  die "Error reading from $FQfileName: $errgznum: $errgzmsg\n" 
}

#Opening ZIP ARCHIVE

my $zip = new Archive::Zip($FQzipName);
$main::files = [ $zip->memberNames() ];
&$debgRtn("ZIP FILE $FQzipName contains:\n", join("\n", @$main::files)); 

sub extractFile {
  my ($ftype, $zipflst) = (shift, [ @_ ]);  die "$ftype inputfile not found" unless scalar(@$zipflst);
 
  my $FQoutName =  $workdir.'/'. basename($jr->getFileName( (ref($ftype) eq 'ARRAY' ? ($ftype->[0], $ftype->[1]) : $ftype) ));
  die "multiple $ftype found" if (scalar(@$zipflst) > 1 && $FQoutName !~ /\.\d+\.(?:\d+\#|\#\d+)/);
  &$debgRtn("outfile radix: $FQoutName");
  foreach my $mnam ( @$zipflst ) {
    &$debgRtn("now extracting ", $mnam, " as ", (ref($ftype) eq 'ARRAY' ? ($ftype->[0], " IDEN: ", $ftype->[1]) : $ftype), " to \"$FQoutName\""); 
    die '$ftype extraction error' unless $zip->extractMember( $mnam, $FQoutName) == AZ_OK;

 }
  return $FQoutName;
}

#Processing INDEXES
sub readContents {
  my ($ixtnam, $ixproc) = (shift, shift);
#  use fields;
  my $ixfnam = (grep /^_(?i:$ixtnam)/, @$main::files)[0];
  return undef unless $ixfnam;
  my $fldsep = ($ixproc->{FLDSEP} ? qr/$ixproc->{FLDSEP}/ : qr/\t/ );   
  &$logrRtn("Now processing $ixfnam FLDSEP: $fldsep");  
  my $indexrows = [split /[\r\n]+/, $zip->contents($ixfnam)];

  if ( $ixproc->{NOHDR} ) {
    my $ct = 0;
    unshift @$indexrows, join($ixproc->{FLDSEP} || "\t", map { sprintf('C%05.0d',$ct++) } split /$fldsep/, $indexrows->[0]);
  }
  my $fnamcol = $ixproc->{FNAMCOL} || ($ixproc->{NOHDR} ? 'C00000' : 'NOME_FILE');
  my $ftypcol = $ixproc->{FTYPCOL} || ($ixproc->{NOHDR} ? 'C00001' : 'TIPO');
  &$debgRtn("FNAMCOL: $fnamcol FTYPCOL: $ftypcol");  

  my $testline = $indexrows->[0];
  (my $hdrline = shift @$indexrows) =~ s/(^|$fldsep)$fnamcol($fldsep|$)/$1_MEMNAME_$2/i; 
  
  $hdrline =~ s/(^|$fldsep)$ftypcol($fldsep|$)/$1_MIMETYPE_$2/i; 
  $main::indexes->{$ixtnam} = {
			       HDR => $hdrline,
			       FIRSTROW => $testline,
			       ROWS => [],
			       FRMPCOL => $ixproc->{FRMPCOL} || '',
			       FORPCOL => $ixproc->{FORPCOL} || '',
			       TOTPCOL => $ixproc->{TOTPCOL} || '',
			      };

  &$debgRtn("$ixfnam ATTRIBUTES:\n", $main::indexes->{$ixtnam});  
  my %members = ();
  foreach my $row ( @$indexrows ) {
    my $hdrs = [ split /$fldsep/, $main::indexes->{$ixtnam}->{HDR} ];
    push @{$main::indexes->{$ixtnam}->{ROWS}}, { %$JRAttrs, map { shift @$hdrs => $_ } split /$fldsep/, $row };
    my $memname = $main::indexes->{$ixtnam}->{ROWS}->[-1]->{_MEMNAME_};
    if ( my ($arcfn) = (grep /$memname/i, @$main::files)[0] ) {
      $memname = $main::indexes->{$ixtnam}->{ROWS}->[-1]->{_MEMNAME_} = $arcfn;
      $members{ $memname } = $main::indexes->{$ixtnam}->{ROWS}->[-1]->{_MIMETYPE_} if $memname; 
    }
    else {
      &$logrRtn("READCONTENTS: $memname not found");
    }
  }
  
  &$debgRtn("$ixtnam rows:\n", $main::indexes->{$ixtnam}->{ROWS});  
  &$debgRtn("$ixtnam members:\n", \%members);  
  return %members;
  
}

my $manfqn = $workdir.'/'. basename($jr->getFileName('IXFILE', '$$MANIFEST$$'));
my $manifest = new FileHandle(">$manfqn");

&$debgRtn("Now checking cfg keys \n", $reportcfg->{loadzip});  
while ( my ($ixname, $ix) = each %{$reportcfg->{loadzip}->{$JobReportName}->{ix}} ) {
  %main::members = ( %main::members, readContents($ixname, $ix) );
}

my $fxrfqn = $workdir.'/'. basename($jr->getFileName('FILXREF'));
my $filxref = new FileHandle(">$fxrfqn");
my $pxrfqn = $workdir.'/'. basename($jr->getFileName('PAGEXREF'));
my $pagxref = new FileHandle(">$pxrfqn");

my $FromPage = 1;
print $filxref "StartItem\t$JobReportName\t\$\$\$\$\t\$\$\$\$\tPage=$FromPage\n"; 
my @datafiles = keys %main::members;
for my $fct ( 0..$#datafiles ) {
  my $mimetype = delete $main::members{ $datafiles[$fct] };
  my $outnam = extractFile(['DATAOUT', $fct], $datafiles[$fct]);
  my $toPage = ($FromPage - 1) + 1;
  print $pagxref "File=$fct From=$FromPage To=$toPage\n"; 
#  print $manifest "PDFOUT.$fct\t", basename($outnam), "\t$totPages\n"; 
  push @$main::files2arc, $outnam; 
  
  $main::members{ $datafiles[$fct] } = [$outnam, $mimetype, $FromPage, -1];
  &$debgRtn("Filled $fct $datafiles[$fct] \n", $main::members{$datafiles[$fct]});  
  $FromPage += 1;
}
print $filxref "EndItem\t$JobReportName\t\$\$\$\$\t\$\$\$\$\tPage=", ( $FromPage - 1 ), "\n"; 

close $filxref;
#print $manifest join("\t", ('FILXREF', basename($fxrfqn), 2)), "\n";
push @$main::files2arc, $fxrfqn; 
close $pagxref;
#print $manifest join("\t", ('PAGEXREF', basename($pxrfqn), scalar(@datafiles))), "\n"; 
push @$main::files2arc, $pxrfqn; 

foreach my $ixname ( keys %{$reportcfg->{loadzip}->{$JobReportName}->{ix}} ) {
  my $ix = $reportcfg->{loadzip}->{$JobReportName}->{ix}->{$ixname}; 
  next unless exists($main::indexes->{$ixname});
  my $outrtn = {};
  foreach my $opname ( keys %{$ix->{op}} ) {
    &$debgRtn("Now Processing $opname of $ixname"); 
    my $op = $ix->{op}->{$opname};
    my $test = $op->{test};
    next if $test ne "1" && $main::indexes->{$ixname}->{FIRSTROW} !~ /$test/;
    &$debgRtn("Now Start INDEX $ixname processing for $op->{indexes}"); 
    ($main::indexes->{$ixname}->{parser}, my @outlist) = buildParser($op->{getVars}, $op->{Columns});
    &$debgRtn("Parser built for $ixname outlist: ", join("::", @outlist)); 
    foreach my $outixn ( split /\s+/, $op->{indexes} ) {
      next unless exists($reportcfg->{index}->{$outixn});
      $outrtn->{$outixn}->{outlist} = [ split /\s+/, $reportcfg->{index}->{$outixn}->{vars} ];
      $outrtn->{$outixn}->{entries} = $reportcfg->{index}->{$outixn}->{entries};
      $outrtn->{$outixn}->{table} = $reportcfg->{index}->{$outixn}->{table};
    }
  }
  foreach my $outixn ( keys %$outrtn ) {
    my $tabname = $outrtn->{$outixn}->{table};
    my $ixfname = basename($jr->getFileName('IXFILE', $tabname));
    @{$main::outfiles->{$tabname}}{qw(FH FNAME TOTENTRIES)} = (new FileHandle(">$workdir/$ixfname"), $ixfname, 0) unless exists($main::outfiles->{$tabname});
    $outrtn->{$outixn}->{FKEY} = $tabname;
    my $fh = $main::outfiles->{$outrtn->{$outixn}->{FKEY}}->{FH};
    &$debgRtn("Columns  will be stored into ", join('::', @{$outrtn->{$outixn}->{outlist}}), " of $outixn"); 
    print $fh join("\t", ('JobReportID', @{$outrtn->{$outixn}->{outlist}}, 'FromPage', 'ForPages' )), "\n";
    $outrtn->{$outixn}->{cnt} = 0;
  }
  foreach my $row ( @{$main::indexes->{$ixname}->{ROWS}}) {
    &$debgRtn("Processing index row: ", join('::', %$row));
    foreach my $outixn ( keys %$outrtn ) {
      next if $outrtn->{$outixn}->{entries} eq 'FIRST' and $outrtn->{$outixn}->{cnt} > 0;
      my ($memname) = $row->{_MEMNAME_};
      die "File $memname not found in archive - aborting job" unless $memname;
#      die "Specified size of $memname ($main::indexes->{$ixname}->{TOTPCOL}) different from real size in archive - Aborting job"
#	if $main::indexes->{$ixname}->{TOTPCOL} and $main::indexes->{$ixname}->{TOTPCOL} != $main::files2arc->{$memname}->[1];
      my ($outname, $mimetype, $fromPage, $forPages) = @{$main::members{$memname}};
      $fromPage += ($row->{$main::indexes->{$ixname}->{FRMPCOL}} - 1) if $main::indexes->{$ixname}->{FRMPCOL} and $row->{$main::indexes->{$ixname}->{FRMPCOL}};
      $forPages = $row->{$main::indexes->{$ixname}->{FORPCOL}} if $main::indexes->{$ixname}->{FORPCOL} and $row->{$main::indexes->{$ixname}->{FORPCOL}};
      my $fh = $main::outfiles->{$outrtn->{$outixn}->{FKEY}}->{FH};
      print $fh join("\t", ($JRID, @{&{$main::indexes->{$ixname}->{parser}}($row)}{@{$outrtn->{$outixn}->{outlist}}}), $fromPage, $forPages), "\n";
      $main::outfiles->{$outrtn->{$outixn}->{FKEY}}->{TOTENTRIES}++;
      $outrtn->{$outixn}->{cnt}++;
    }
  }
  foreach my $outixn ( keys %$outrtn ) {
    &$debgRtn("$outixn entries: $outrtn->{$outixn}->{entries}, count: $outrtn->{$outixn}->{cnt}"); 
  }

  foreach my $outixn ( keys %{$main::outfiles} ) {
    my ($outnam, $totentries) = @{$main::outfiles->{$outixn}}{qw(FNAME TOTENTRIES)}; 
    &$logrRtn("$outixn table entries count: $main::outfiles->{$outixn}->{TOTENTRIES}"); 
    my $fh = $main::outfiles->{$outixn}->{FH};
    close $fh;
    print $manifest join("\t", ($outixn, basename($outnam), $totentries ) ), "\n";
    push @$main::files2arc, "$workdir/".$main::outfiles->{$outixn}->{FNAME}; 

  }

}


close $manifest;
push @$main::files2arc, $manfqn; 

my $contentfn = "$workdir/\$\$ARC.\$\$MANIFEST\$\$.TXT";
my $contentfh = new FileHandle(">$contentfn") || die "Unable to open list to archive file - $!";
binmode $contentfh;
print $contentfh join("\n", @$main::files2arc), "\n";
$contentfh->close;

my $exit_code = 0;

if ( scalar(keys %{$reportcfg->{loadzip}->{$JobReportName}->{ix}}) ) {

  unless ( scalar(keys %{$main::outfiles}) ) {
    &$logrRtn("No indexes found that matches specifications"); 
    $exit_code = 1;
  }
}

exit $exit_code;

__END__ 

