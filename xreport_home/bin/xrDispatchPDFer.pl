#!/usr/bin/perl -w

package main;

use strict;
use POSIX qw(strftime);

use XReport;
use XReport::Util;
use XReport::DBUtil;
use XReport::ENUMS qw(:ST_ENUMS);
use constant ST_RETRY => 19;

use XReport::JobREPORT;
use Data::Dumper;

use Symbol;
use File::Path;
use File::Basename;
use Win32::OLE qw( in );
use Win32::OLE::Variant;
use Win32::Job;
use Win32::EventLog;
use Time::HiRes qw( time );

use constant DEBUG => 0;
use constant DEQUEUE_INTERVAL_IN_SEC => 30;

use constant EC_OK                => 0;
use constant EC_USER_SHUTDOWN     => 1;
use constant EC_UNKNOWN_ERROR     => 2;
use constant EC_PERL_SYNTAX_ERROR => 9;
use constant EC_PERL_DIE3         => 10045;
use constant EC_PERL_DIE1         => 10061;
use constant EC_PERL_DIE2         => 255;

use constant DESTINATION => '226.1.1.2:2000';

#use constant DEBUG => 0;
#use POSIX qw(strftime); 
#File::Basename qw(basename); 
use constant EXITCODE2RESTART => 123; 


#use constant SOCKBUFL => 4096;
#use constant LOCLPORT => 12000;

$main::EventLog = Win32::EventLog->new( "Application", '.' ) or XReport::Util::logAndDie "Can't open Application EventLog\n";

sub doEventLog {
    my @es   = ( $main::SrvName, shift, shift, shift, shift );
    my $cfg  = pop @es;
    my $evID = $cfg->{'EventID'} || "10001";
    $main::EventLog->Report(
        my $ei = {
            Source    => "Xena",
            Computer  => '.',
            EventType => EVENTLOG_ERROR_TYPE,
            Category  => 0,
            EventID   => $evID,
            Strings   => join( "\0", @es ),
            Data => "Failure of $es[1] detected by Service $es[0] - WorkQueue key $es[2] - ExternalKey key $es[3] -",

#                   Data => "Failure of $es[1] detected by Service $es[0] - WorkQueue key $es[2] - ExternalKey key $es[3] -cfg ref: ". ref($cfg) ."\n"
#                   .Dumper($cfg),
                 }
    );
}

sub localGetLock {
    my $LockName =shift ;
	my $activatedLogitForLock =shift ; 
	XReport::Util::logAndDie "DISPATCHER: Error in localGetLock($LockName) - resource already held by the same process." if $main::resourceLocked ;  
	$main::resourceLocked = 1;
	my $timeBeforeToLock = time(); 
	my $rc = GetLock($LockName);
	XReport::Util::logAndDie "DISPATCHER: Error in GetLock($LockName)" if($rc < 0); 
	$main::timeStartLock = time();
	$main::elapsedTimeBeforeToLock = sprintf("%.2f", $main::timeStartLock - $timeBeforeToLock);   
	i::logit("DISPATCHER: GetLock('$LockName') called:$rc - time before to hold the resource: ".$main::elapsedTimeBeforeToLock ) if $activatedLogitForLock; 
	return $rc;
}

sub localReleaseLock {
    my $LockName =shift ;
	my $activatedLogitForLock =shift ;
	my $ExternalKey = shift;
	
	
	my $rc = ReleaseLock($LockName);
	XReport::Util::logAndDie "DISPATCHER: Error in ReleaseLock($LockName) - rc:$rc" if($rc < 0); 
	my $timeEndLock = time();
	my $elapsedTimeToLock = sprintf("%.2f", $timeEndLock - $main::timeStartLock);
	i::logit("DISPATCHER: ReleaseLock('$LockName') called:$rc - time before to hold: ".$main::elapsedTimeBeforeToLock." - time to hold: $elapsedTimeToLock - ExternalKey: ".(defined $ExternalKey?$ExternalKey :'NOT FOUND' )) if $activatedLogitForLock; 
	$main::resourceLocked = 0;
	return $rc;
}


sub dequeue {
    my ( $INTERVAL, $selectQUERY,$LockName ) = ( shift, shift, shift );
	$LockName = 'LK_INPUTQUEUE_PDF' unless $LockName;
    my $dbr;

	
	my $activatedLogitForLock = 1; 

    GET_WORKITEM_LOOP: while (1) {

        eval {
            #GetLock('LK_INPUTQUEUE_PDF');
            localGetLock($LockName,$activatedLogitForLock); 

			$dbr = dbExecute($selectQUERY);
			XReport::Util::checkDate2Restart();
            last GET_WORKITEM_LOOP unless $dbr->eof();
			
			$dbr->Close();

            #ReleaseLock('LK_INPUTQUEUE_PDF');
            localReleaseLock($LockName,$activatedLogitForLock);
			$activatedLogitForLock = 0;
        };
		my $errormsg = $@ or $!;
        #XReport::Util::logAndDie "DISPATCHER: $@" if ( $@ and $@ !~ /User *Error *:/i );
		XReport::Util::logAndDie "DISPATCHER: $errormsg" if ( $errormsg and $errormsg !~ /User *Error *:/i );
		i::logit("DISPATCHER: error= $errormsg") if ( $errormsg );
		
        sleep $INTERVAL; 
		XReport::Util::checkDate2Restart();
    }

    my ( $WorkId, $ExternalTableName, $ExternalKey, $TypeOfWork, $initStatus ) =
                          $dbr->GetFieldsValues(qw(WorkId ExternalTableName ExternalKey TypeOfWork Status));
    $dbr->Close();

    #dbExecute("UPDATE tbl_WorkQueue Set SrvName = '$main::SrvName', Status = Status + 1 WHERE WorkId = $WorkId");
	
	dbExecute("UPDATE tbl_WorkQueue Set SrvName = '$main::SrvName', Status = ". ST_INPROCESS ." WHERE ExternalTableName = '$ExternalTableName' AND ExternalKey = $ExternalKey AND TypeOfWork = $TypeOfWork ");

 
    #ReleaseLock('LK_INPUTQUEUE_PDF');
	localReleaseLock($LockName,1,$ExternalKey);

    return ( $WorkId, $ExternalTableName, $ExternalKey, $TypeOfWork, $initStatus );
}

sub cre_OUTPUT_ARCHIVE {
    my ( $jr, $workdir, $script, $status ) = ( shift, shift, shift, shift );
    require XReport::ARCHIVE;
    my $setstring = [];
    push @$setstring, "SrvName = '$main::SrvName'" if $main::SrvName;

    my ( $JobReportName, $JobReportId ) = $jr->getValues(qw(JobReportName JobReportId));

    my $ar = XReport::ARCHIVE::cre_OUTPUT_ARCHIVE( $jr, @_ );
    my $pathid = $jr->getValues('LocalPathId_OUT');
    push @$setstring, "LocalPathId_OUT = '$pathid'" if $pathid ;

    i::logit("DISPATCHER: INIT CREATING OUTPUT Archive using LocalPathId $pathid");
    my $contentfn = "$workdir/\$\$ARC.\$\$MANIFEST\$\$.TXT";
    my @fileList;
    if ( -e $contentfn ) {
        my $contentfh = new FileHandle("<$contentfn") || XReport::Util::logAndDie "Unable to open list to archive file - $!";
        binmode $contentfh;
        $contentfh->read( my $buff, -s $contentfn );
        $contentfh->close();
        push @fileList,
          ( $contentfn,
            ( map { ( split /\t/, $_ )[0] } split /[\r\n]+/, $buff ),
            ( grep /\.(?:log|stderr|stdout)$/i, glob("$workdir/*") )
          );
    }
    else {
        push @fileList, ( grep !/\.ps$/i, glob("$workdir/*") );
    }

    for (@fileList) {
        $ar->AddFile( $_, basename($_) ) if -f $_ && $_ !~ /\.ps/i;
    }
    $ar->Close();
    my $fsz = -s $ar->{FileName};

    i::logit("DISPATCHER: END CREATING OUTPUT Archive in $ar->{FileName} ($pathid) size: $fsz");

    push @$setstring, "ElabOutBytes = $fsz";
    push @$setstring, "Status = $status";
    push @$setstring, "ElabEndTime = '" . strftime( '%Y-%m-%d %H:%M:%S.%U', localtime ) . "'";
    return $setstring;

}

sub init_worker {
    my ( $WorkId, $ExternalTableName, $ExternalKey, $status ) = ( shift, shift, shift, shift );
    my $setstring = [];
    my $cur_work_dir = "$main::XR_WRK/$ExternalTableName/$ExternalKey";
    if ( grep( /^\-WORKDIR$/, @ARGV ) ) {
        for my $argi ( 0..$#ARGV ) {
            if ( $ARGV[$argi] eq '-WORKDIR' ) {
                $argi += 1;
                $cur_work_dir = $ARGV[$argi];
                last;
            }
        }
    }
    # da togliere quando questo verr� sostituito al dispatcher corrente
    $cur_work_dir = $XReport::cfg->{workdir}."\\$main::SrvName";
    if ( -d $cur_work_dir ) {
        ( my $w32dir = $cur_work_dir ) =~ s/\//\\/g;
        i::logit("DISPATCHER: $w32dir already exists - attempting to delete");
# da ripristinare quando sostituisce xrDispatcher.pl
        system("del /s /q $w32dir\\* && rmdir /S /Q $w32dir\\localres");
#        if ( -d $cur_work_dir ) {
#            my $errmsg = "DISPATCHER: Unable to delete $w32dir - still exists";
#            i::logit($errmsg);
#            XReport::Util::logAndDie "$errmsg\n";
#        }
        i::logit("DISPATCHER: $cur_work_dir deleted");
    }

    DEBUG and i::logit("DISPATCHER: Create work directory $cur_work_dir");
    eval { mkpath($cur_work_dir); };
    if ($@) {
        i::logit("DISPATCHER: Could not create $cur_work_dir: $@");
        XReport::Util::logAndDie "DISPATCHER: Could not create $cur_work_dir: $@\n";
    }
    push @$setstring, "ElabStartTime = '" . strftime( '%Y-%m-%dT%H:%M:%S.%U', localtime ) . "'";
    push @$setstring, "Status = $status";

#   push @$setstring, "RetryCount = RetryCount + 1" if $ExternalTableName =~ /^tbl_JobSpool$/i;
    my $keyn = ( $ExternalTableName =~ /^tbl_JobSpool$/i ? 'JobSpoolId' : 'JobReportId' );

#   my $keyn = 'IDENTITYCOL';
    dbExecute( "UPDATE $ExternalTableName set " . join( ', ', @$setstring ) . " WHERE $keyn = $ExternalKey" );
    i::logit("Worker initialized - WorkDir: $cur_work_dir");
    return $cur_work_dir;
}

sub get_status {
    my $currjob = shift;
    my $status  = $currjob->status();
    my $jpid    = shift @{ [ ( keys %$status ) ] };
    my $times   = $status->{$jpid}->{time};
    my $sn      = $main::jobsinfo->{ '_PID' . $jpid }->{_sn};
    my $statmsg = "DISPATCHER: JobMon $jpid STATUS - process: $sn"
                . " kernel: $times->{kernel}, User: $times->{user}, elapsed: $times->{elapsed}";
    return $statmsg;
}

sub JobMon {
    my $currjob = shift;
    my $statmsg = get_status($currjob);
    $main::veryverbose && i::logit($statmsg);
    $main::mcsock->send( $statmsg, 0, DESTINATION ) if $main::mcsock;

# my $msock = $sel->can_read(5);
# print "msock not ready - $@\n" unless $msock;
# return 0 unless $msock;

# $sock->recv(my $data,1024);
# print "Received: ", length($data), "bytes from ", $sock->peerport(), " at ", $sock->peerhost(), "\n";
# return 0 unless $data;
# print "Received: ", $data, "\n" if $data;

# return 1 if $data =~ /^CANCEL.{0,1}$/i;
# my $retaddr = $sock->peerhost();
# if ($data =~ /^STATUS\s(\d+)\D{0,1}/i) {
#   my $retport = $1;
#   IO::Socket::INET->new(Proto => 'udp', PeerAddr => "$retaddr:$1")->send($statmsg);
#   my $retsock = IO::Socket::INET->new(Proto => 'udp', PeerAddr => "$retaddr:$retport")->send($statmsg);
#   $retsock->send($statmsg);
#   $retsock->shutdown(2);
#   $retsock->close();
#   undef $retsock;
#}
    return 0;
}

sub is_syntax_error {
    use File::Basename;
    my $filename = shift;
    my $handle   = gensym();

    open( $handle, "<$filename" ) or XReport::Util::logAndDie "Cannot open $filename: $!\n";
    my $rc = 0;
    my $errmsg_re = qr/^.*(?:BEGIN failed.+compilation aborted|syntax +error|(?:UN){0,1}RECOVERABLE\s+ERROR|Status\s*=\s*31).*$/i;
    while (<$handle>) {
        if ( /$errmsg_re/ ) { $rc = 1; last; }
    }
    close($handle);

    return $rc;
}

#sub logFileContent {
#    use FileHandle;
#    my ( $pfx, $fn ) = ( shift, shift );
#    $fn =~ s/\\/\//g;
#
#    if ( -s $fn ) {
#        my $fh = new FileHandle("<$fn") 
#               || return i::logit("$pfx - unable to open \"$fn\" - $!");
#
#        binmode $fh;
#        $fh->read( my $buff, -s $fn );
#        close $fh;
#        my @lines = split /[\r\n]+/, $buff;
#        i::logit( map { "$pfx - $_" }  @lines );
#        return [ @lines ] if scalar(@lines) < 10;
#        return [ splice @lines, -10 ];
#    } 
#    else {
#    	i::logit("$pfx - EMPTY");
#    	return [];
#    }
#}


sub logFileContent {
    use File::ReadBackwards;
    my ( $pfx, $fn ) = ( shift, shift );
    $fn =~ s/\\/\//g;
	
	my @lines;
    if ( -s $fn )
	{
        my $fh = File::ReadBackwards->new($fn) || return i::logit("$pfx - unable to open \"$fn\" - $!");
		my $last_non_blank_line;
		for my $i (1..10)
		{
			$last_non_blank_line = $fh->readline;
			if ((defined $last_non_blank_line) and ($last_non_blank_line =~ /\S/))
			{
				chomp($last_non_blank_line);
				unshift (@lines,$last_non_blank_line);
			}
		}
    }
	if (scalar(@lines))
	{
		i::logit( map { "$pfx - $_" }  @lines );
		return [ @lines ] ;
	}
	else
	{
		i::logit("$pfx - EMPTY");
		return [];
	}
}

sub manageStatusErrorMsg {
	my ($codeMsgHash, $codeIsFoundHash,$codeMsgString ) = ( shift, shift, shift );
	map { delete $codeMsgHash->{$_} } keys %{$codeMsgHash};
	map { delete $codeIsFoundHash->{$_} } keys %{$codeIsFoundHash};
	#es. $codeMsgString: '254:xxxxxxxxxxxxxxx xxxxxxxxxxxxxxxxxxxxx|255:Unable to get tbl_JobReportNames data for|256:XXXXX to get tbl_JobReportNames data for	
	foreach my $codeMsg (split /\|/, $codeMsgString)
	{
		if( $codeMsg =~ /^(\d+):(.*)$/ )
		{
			$codeMsgHash->{$1} = $2;
		}
	}
	
	foreach my $key (keys %{$codeMsgHash})
	{	
		$codeIsFoundHash->{$key} = 0;
	}
}
#############################################################################################################
#   MAIN
#############################################################################################################

use IO::Socket::INET;
use IO::Select;

my $isdaemon = ( grep( /^\-\@$/, @ARGV ) ? 1 : 0 );

my $cfg = $XReport::cfg; 
my $appl = $main::Application;
my $XREPORT_HOME = $appl->{XREPORT_HOME};
my $XREPORT_SITECONF = $appl->{XREPORT_SITECONF};

InitServer( 'isdaemon' => $isdaemon );

my $myName = ( split( /[\/\\]/, $0 ) )[-1];
$myName = ( split( /\./, $myName ) )[0];

#configuration from the file specified in parameter cfgFileXML
my $cfgFileNameFromParameter = undef;
foreach my $i (0 .. $#ARGV) 
{
	if (uc($ARGV[$i]) =~ /^\-(cfgFile|cfgFileXML)$/i) { # cfgFile
	  $cfgFileNameFromParameter = $ARGV[$i+1];
	  $cfgFileNameFromParameter =~ s/\.xml$//i;
	  i::logit("DISPATCHER: cfgFileNameFromParameter:[$cfgFileNameFromParameter]");
	  last;
	}
}

if ($cfgFileNameFromParameter) {
	i::logit("DISPATCHER: cfgFileNameFromParameter:[$cfgFileNameFromParameter]") ;
	my $cfgFile = 'n_o_t_f_o_u_n_d';
	$cfgFile = $ENV{'XREPORT_SITECONF'}."/xml/".$cfgFileNameFromParameter.".xml"  ;
	my $cfg_jobs = undef ;
	$cfg_jobs =  XML::Simple::XMLin($cfgFile, ForceArray => [qw(job step)])->{'jobs'};
	my $cfgmtime = (stat($cfgFile))[9];
	i::logit("DISPATCHER: Initialization completed from \"$cfgFile\" (" . localtime($cfgmtime) . ")");
	undef $cfg->{jobs};
	$cfg->{jobs} = $cfg_jobs;
}

my $LockName =(exists $cfg->{jobs}->{'LockName'} ? $cfg->{jobs}->{'LockName'} : 'LK_INPUTQUEUE_PDF');

i::logit("DISPATCHER: LockName \"$LockName\""); 

my $disabled_scripts = undef;
my $configs = { map { $_ => ( stat($_) )[9] } 
                 grep !/^\s*$/, split /;/, $main::Application->{'path.configs'} };

$main::SrvName = getConfValues("SrvName");
$main::XR_WRK  = getConfValues("workdir");
$main::mcsock  = new IO::Socket::INET( Proto => 'udp', PeerAddr => DESTINATION, Reuse => 1 );
if ( !scalar( @{ $cfg->{jobs}->{'job'} } ) ) {
	i::logit("Config contains no jobs - $myName ending");
	exit 0;
}
else {
   i::logit("Config contains Following jobs for $myName: ". Dumper($cfg->{jobs}));
  
}

my $whclause = [];
foreach my $job ( @{ $cfg->{jobs}->{job} } ) {
	my $sel  = "(TypeOfWork = $job->{type} AND WorkClass ";
	$sel .= !exists($job->{WorkClass}) ? ' IS NULL'
	     : ' IN ('.join(',', split /[;, ]+/, $job->{WorkClass}).')';
	push @{$whclause}, $sel . " AND ( (SrvName='$main::SrvName') "
              . " OR (Status = $job->{status} AND SrvName IS NULL)"
        #      . " OR (Status IN (3, 19, 35, 51) AND ISDATE(SrvName) = 1 AND GETDATE() > SrvName)))";
              . " ))";
}

my $select_query = "SELECT TOP 1 * FROM tbl_WorkQueue with (NOLOCK) WHERE " . join( ' OR ',  @{$whclause}  )
              . " ORDER BY WorkClass  DESC, Priority DESC, WorkId";
#DEBUG and i::logit("DISPATCHER: $select_query");
i::logit("DISPATCHER: select_query: \n$select_query\n");

XReport::Util::checkDate2Restart();
#die("TESTSAN - Forced ERROR !!!");
#XReport::Util::logAndDie("TESTSAN - Forced ERROR !!!");
XReport::Util::logAndCroak("TESTSAN - Forced ERROR !!!");


#$main::veryverbose = 1;
#while(1)
#{
#	XReport::Util::checkDate2Restart();
#	sleep 10;
#}

WKITEM: while (1) {

    foreach my $dscript ( keys( %{$disabled_scripts} ) ) {
        if ( $disabled_scripts->{$dscript} && $disabled_scripts->{$dscript} != ( stat($dscript) )[9] ) {
            i::logit("DISPATCHER: $dscript has modified and it will be activated");
            delete $disabled_scripts->{$dscript};
        }
    }
	i::logit("DISPATCHER: before dequeue");

    my $step = 0;
    my ( $WorkId, $ExternalTableName, $ExternalKey, $TypeOfWork, $status ) = dequeue( DEQUEUE_INTERVAL_IN_SEC, $select_query,$LockName );
    #my ( $WorkId, $ExternalTableName, $ExternalKey, $TypeOfWork, $status ) = dequeue( 3, $select_query,$LockName );
	

    i::logit("DISPATCHER: Initialize work for TOW $TypeOfWork - ID: $WorkId($ExternalTableName $ExternalKey $status)");
    my $keyn = ( $ExternalTableName =~ /^tbl_JobSpool$/i ? 'JobSpoolId' : 'JobReportId' );

#  $keyn = 'IDENTITYCOL';

    my $cur_cfg = ( grep { uc( $_->{type} ) eq uc($TypeOfWork) } @{ $cfg->{jobs}->{'job'} } )[0];
    my $cfgCodes = {};
    if ( exists( $cur_cfg->{statusCodes} ) ) {
        $cfgCodes = $cur_cfg->{statusCodes};
    }
    my $statusCodes = {};
    if ( exists( $cfgCodes->{start} ) && $cfgCodes->{start} && $cfgCodes->{start} != ( $status + 1 ) ) {
        dbExecute("UPDATE tbl_WorkQueue Set Status = $cfgCodes->{start} WHERE WorkId = $WorkId");
        $statusCodes->{start} = $cfgCodes->{start};
    }
    else {
        $statusCodes->{start} = $status + 1;
    }

    foreach ( [ end => 1 ], [ retry => 2 ], [ error => 14 ] ) {
        my ( $cd, $val ) = @$_;
        $statusCodes->{$cd} = ( exists( $cfgCodes->{$cd} )
                                ? $cfgCodes->{$cd}
                                : $statusCodes->{start} + $val
                              );
    }

    my $exit_code = 0;
	
	my ($codeMsgHash , $codeIsFoundHash) = ({},{});
	
	
    my $disabled  = [ grep { !-e $_->[1]
          or ( exists( $disabled_scripts->{ $_->[0] } ) && $disabled_scripts->{ $_->[0] } eq ( stat( $_->[1] ) )[9] )
                       } map { [ $_->{cmd}, "$XREPORT_HOME\\$_->{cmd}" ] } @{ $cur_cfg->{'step'} } ];

    if ( scalar(@{$disabled}) ) {
        i::logit( "DISPATCHER: Work $WorkId will be discarded because \"" 
                . join( '", "', map { $_->[1] } @{$disabled} )
                   . "\" is disabled" );
        $exit_code = -1;
    }

    my ( $script, $scriptname ) = ( (grep { $_->{cmd} eq $disabled->[0]->[0] } @{ $cur_cfg->{step} } )[0],
                                                basename( $disabled->[0]->[0] ) ) if $exit_code;
    my $cur_work_dir = init_worker( $WorkId, $ExternalTableName, $ExternalKey, $statusCodes->{start} )
                                                                                            unless $exit_code;
    my @wqsets = ("UPDATE tbl_WorkQueue SET ");

    if ( !$exit_code ) {

        foreach $script ( sort { $a->{order} > $b->{order} } @{ $cur_cfg->{step} } ) {
            $step++;
            $scriptname = basename( $script->{cmd} );
            my $cmdfpath = "$XREPORT_HOME\\$script->{cmd}";
            i::logit( "DISPATCHER: Start step $step, script " . $script->{cmd} );
			if(exists($script->{parameters}))
			{
				i::logit( "DISPATCHER: parameters step $step, " . $script->{parameters} );				
			}

            my $job = new Win32::Job();
            my $jpid = $job->spawn( $^X,
                "perl -I \"$XREPORT_HOME\\perllib\" \"$cmdfpath\" \"$cur_work_dir\" $ExternalKey "
                . " -HOME \"$XREPORT_HOME\" -SITECONF \"$XREPORT_SITECONF\" " 
                . " -N $main::SrvName -workid:$WorkId "
				.( exists($script->{parameters}) ? ' '.$script->{parameters} : ' '),
                { stdin  => 'NUL',                                                   # the NUL device
                  stdout => $cur_work_dir . '/' . $scriptname . $step . '.stdout',
                  stderr => $cur_work_dir . '/' . $scriptname . $step . '.stderr',
                  no_window => 1
                }
            );
            $main::jobsinfo->{ '_PID' . $jpid } = { _sn => $scriptname };

            $job->watch( \&JobMon, 10 );
            my $statmsg = get_status($job);
            $main::mcsock->send( $statmsg, 0, DESTINATION ) if $main::mcsock;

            $exit_code = $job->status()->{$jpid}->{'exitcode'};
            delete $main::jobsinfo->{ '_PID' . $jpid };

            my $last10stdout = 
            logFileContent "STEP $step STDOUT", $cur_work_dir . '/' . $scriptname . $step . '.stdout';
            my $last10stderr =
            logFileContent "STEP $step STDERR", $cur_work_dir . '/' . $scriptname . $step . '.stderr';
			
			#check if the logs contain the error messages related to the codes in codeMsgHash
			if (exists($script->{codeMsgString}) or exists($cur_cfg->{errorMsgString}->{codeMsgString}))
			#if (exists($script->{codeMsgString}))
			{
				my $cur_cfg_or_script = (exists($script->{codeMsgString}) ? $script : $cur_cfg->{errorMsgString});
				#manageStatusErrorMsg($codeMsgHash,$codeIsFoundHash,$script->{codeMsgString});
				manageStatusErrorMsg($codeMsgHash,$codeIsFoundHash,$cur_cfg_or_script->{codeMsgString});
				foreach my $key (keys %{$codeMsgHash})
				{	
					my $msg = qr/$codeMsgHash->{$key}/i;
					$codeIsFoundHash->{$key} = 0;
					if (grep /$msg/, (@{$last10stdout}, @{$last10stderr} ))
					{	
						$codeIsFoundHash->{$key}=1;
						i::logit("DISPATCHER: found msg:".$codeMsgHash->{$key});
						last;
					}
				}
			}
			

            my $okre = qr/$script->{okre}/ if exists($script->{okre});
            if ( $exit_code == EC_OK ) {
            	
                DEBUG and i::logit("DISPATCHER: Step $step closed normally exit code: $exit_code");
                next if !exists($script->{okre});
                next if grep /$okre/, (@{$last10stdout}, @{$last10stderr} ); 
            }

            if ( exists($script->{okre}) ) {
                if ( grep /$okre/, (@{$last10stdout}, @{$last10stderr} )  )  {
                    i::logit("DISPATCHER: expected normal ending string (\"$okre\") detected into STDOUT - exit code reset - Job continues");
                    $exit_code = 0;
                    next;
                }
                else {
                    i::logit("DISPATCHER: expected normal ending string (\"$okre\") not detected into STDOUT - Job Aborting");
                    $exit_code = 255;
                    last;
                }
            }
            else {
                i::logit( "DISPATCHER: Unexpected exit code $exit_code from $scriptname(" 
                                                                . ( stat($cmdfpath) )[9] . ")" );
            }
            
            if (     $exit_code != EC_PERL_DIE1
                 and $exit_code != EC_PERL_DIE2
                 and $exit_code != EC_PERL_DIE3
                 and $exit_code != EC_PERL_SYNTAX_ERROR
                 and !is_syntax_error( $cur_work_dir . '/' . $scriptname . $step . '.stdout' )
                 and !is_syntax_error( $cur_work_dir . '/' . $scriptname . $step . '.stderr' ) ) {
                i::logit("DISPATCHER: No Relevant Failure detected from Step $step - exit code reset - Job continues");
                $exit_code = 0;
                next;
            }

            if ( $exit_code == EC_PERL_SYNTAX_ERROR
                 or ( is_syntax_error( $cur_work_dir . '/' . $scriptname . $step . '.stderr' ) ) ) {
                i::logit( "DISPATCHER: $scriptname(" 
                          . ( stat($scriptname) )[9] . ") has generated a syntax error and it'll disabled" );
                $disabled_scripts->{$scriptname} = ( stat($scriptname) )[9];
            }

            last;

        }

        i::logit("DISPATCHER: Terminate work for TOW $TypeOfWork($WorkId) - exit code: $exit_code");

        my $work_status = $statusCodes->{(!$exit_code ? 'end'
                            : exists( $cur_cfg->{onerror} ) && $cur_cfg->{onerror} =~ /^RETRY$/i ? 'retry' 
                            : 'error' 
                            )
                            };
        $wqsets[1] = "Status = $work_status";

        my $jr_obj;
        my $setstring = [];
        if ( !exists( $cur_cfg->{DISP} ) || $cur_cfg->{DISP} !~ /^LEAVE$/i ) {
            if ( $ExternalTableName =~ /^tbl_JobReports$/i ) {
                eval { $jr_obj = Open XReport::JobREPORT($ExternalKey) };
                if ( $@ ) {
                	i::logit("JobReport Open Failed - $@");
                    $work_status = $statusCodes->{'error'};
					#TESTSAN 20160516
					$wqsets[1] = "Status = $work_status";
                    $exit_code = 255;
                    push @$setstring, "ElabEndTime = '" . strftime( '%Y-%m-%d %H:%M:%S.%U', localtime ) . "'";
					#reset the db connection of Xreport::JobReport
					$main::xrdbc = undef if($main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil');
                }
                else {
                    $setstring = cre_OUTPUT_ARCHIVE( $jr_obj, $cur_work_dir, $main::SrvName, $work_status );
                }
            }
            else {
                push @$setstring, "ElabEndTime = '" . strftime( '%Y-%m-%dT%H:%M:%S.%U', localtime ) . "'";
            }
        }
        my $ppsqlfn = "$cur_work_dir/\$\$POST.\$\$PROCESS\$\$.$ExternalKey.sql";
        if ( $work_status == $statusCodes->{'end'} && -e $ppsqlfn && -s $ppsqlfn ) {
            my $ppsqlfh = new FileHandle("<$ppsqlfn");
            my $dbc     = dbExecute("BEGIN TRANSACTION");
            while (<$ppsqlfh>) {
                chomp;
                my $sqlstmnt = $_;
                eval { dbExecute($sqlstmnt) };
                if ($@) {
					#TESTSAN 20180223
					#Cannot insert duplicate key in object 
					my $sqlerror  = "$@ - $!"; 
					$sqlerror =~ s/[\n\r\t\s]+/ /g;
					#$sqlerror  = join('::',split /\r\n/, $sqlerror); 
					i::logit("DISPATCHER: Error in dbExecute from file POST.PROCESS_X.sql: [$sqlerror]");
					#check if the logs contain the error messages related to the codes in codeMsgHash
					if (exists($script->{codeMsgString}) or exists($cur_cfg->{errorMsgString}->{codeMsgString}))
					{
						my $cur_cfg_or_script = (exists($script->{codeMsgString}) ? $script : $cur_cfg->{errorMsgString}); 
						manageStatusErrorMsg($codeMsgHash,$codeIsFoundHash,$cur_cfg_or_script->{codeMsgString});
						foreach my $key (sort keys %{$codeMsgHash})
						{	
							my $msg = qr/$codeMsgHash->{$key}/i;
							DEBUG and i::logit("DISPATCHER: check codeMsgHash->{$key}=".$codeMsgHash->{$key});
							$codeIsFoundHash->{$key} = 0;
							if ($sqlerror =~ /$msg/ )  
							{	
								$codeIsFoundHash->{$key}=1;
								i::logit("DISPATCHER: found msg:".$codeMsgHash->{$key});
								last;
							}
						}
					}
					#
                    $work_status = $statusCodes->{ ( exists( $cur_cfg->{onerror} )
                                   && $cur_cfg->{onerror} =~ /^RETRY$/i ? 'retry' : 'error' ) };
					#TESTSAN 20180223
					$wqsets[1] = "Status = $work_status";
                    $exit_code = 255;
					#
                    last;
                }
            }
            if ( $work_status == $statusCodes->{'end'} ) {
                eval {dbExecute("COMMIT TRANSACTION")};
            }
            else {
                eval {dbExecute("ROLLBACK TRANSACTION")};
            }
        }
        i::logit( "setstring: " . Dumper($setstring) );
        $setstring = [ grep !/^Status =/i, @$setstring ];
        i::logit( "setstring2: " . Dumper($setstring) );
        push @$setstring, "Status = $work_status";
        my $sqlst = "UPDATE $ExternalTableName set " 
                    . join( ', ', @$setstring ) . " WHERE $keyn = '$ExternalKey'";
        i::logit("DISPATCHER - updating table: $sqlst");
        dbExecute($sqlst);
    }
    if ($exit_code) {
        if ( exists( $cur_cfg->{onerror} ) && $cur_cfg->{onerror} =~ /^RETRY(?:;(\d+))?$/i ) {
            my $retry_interval = $1 || 60;
            my $retrytime = strftime( '%Y-%m-%d %H:%M:%S.%U', localtime( time() + $retry_interval ) );
            push @wqsets, ( "SrvName = convert(varchar, dateadd(ss, $retry_interval, GETDATE()), 120)",
"DISPATCHER: $WorkId($ExternalKey) has been set for processing $retry_interval seconds from now ($retrytime)"
              );
        }
        else {
			foreach my $key (grep {$codeIsFoundHash->{$_}} keys %{$codeIsFoundHash} )
			{	
				i::logit("DISPATCHER: found msg[".$codeMsgHash->{$key}."]. Status set to [".$key."]");
				$wqsets[1] = "Status = ".$key;
				last;
			}
			
            push @wqsets, ( "SrvName = 'SUSPENDED'",
                "DISPATCHER: $WorkId($ExternalKey) has been holded from processing (SUSPENDED)"
              );
            doEventLog( $scriptname, $WorkId, ( $ExternalKey || 'N/A' ), $script ) unless(grep {$codeIsFoundHash->{$_}} keys %{$codeIsFoundHash}) ;
        }
    }
    else {
        if ( !exists( $cur_cfg->{DISP} ) || $cur_cfg->{DISP} !~ /^LEAVE$/i ) {
            @wqsets = ( "DELETE from tbl_WorkQueue", "DISPATCHER: $WorkId($ExternalKey) deleted from workqueue" );
        }
        elsif ( exists( $cur_cfg->{DISP} ) && $cur_cfg->{DISP} =~ /^LEAVE$/i ) {
            push @wqsets, "TypeOfWork = $cur_cfg->{To_TypeOfWork}"
                          if exists( $cur_cfg->{To_TypeOfWork} ) && $cur_cfg->{To_TypeOfWork};
            my $SrvName = 'NULL';
            $SrvName = "'$cur_cfg->{To_SrvName}'" if exists( $cur_cfg->{To_SrvName} ) && $cur_cfg->{To_SrvName};
            push @wqsets, ( "SrvName = $SrvName", 
                            "DISPATCHER: $WorkId($ExternalKey) has been cleared for futher processing" );
        }
    }
    my $logmsg = pop @wqsets;
    my $sqlst  = shift @wqsets;
    $sqlst .= join( ', ', @wqsets ) . " WHERE ExternalTableName = '$ExternalTableName' AND ExternalKey = $ExternalKey AND TypeOfWork = $TypeOfWork AND WorkId = $WorkId ";

    i::logit("DISPATCHER - updating WQ table: $sqlst");
    eval { dbExecute($sqlst) };
    if ($@) {
        i::logit("DISPATCHER UNRECOVERABLE ERROR!!! $@");
      last;
    }
    i::logit($logmsg);

    if ( $cur_work_dir ) {
        logFileContent "STEP $step STDOUT", $cur_work_dir . '/' . $scriptname . $step . '.stdout';
        logFileContent "STEP $step STDERR", $cur_work_dir . '/' . $scriptname . $step . '.stderr';
        $cur_work_dir =~ s/\//\\/g;
        system("del /s /q $cur_work_dir && rmdir /S /Q $cur_work_dir") unless grep( /^\-nodirdel$/, @ARGV );
        i::logit("DISPATCHER: $cur_work_dir deleted");
    }

}

TermServer();
