#/usr/bin/perl -w

use FileHandle;
use Data::Dumper;
use File::Basename;
use POSIX qw(strftime);

sub setGlobals {
  #$main::logfile = Win32::OLE->GetObject("IIS://$ENV{'COMPUTERNAME'}/W3SVC/1")->{'LogFileDirectory'}."\\W3SVC1\\$toDayFile";
  $main::siteid = $main::Request->ServerVariables('INSTANCE_ID')->Item();
  my $olepath = "IIS://$ENV{'COMPUTERNAME'}/W3SVC/$main::siteid";
  my $siteobj = Win32::OLE->GetObject($olepath);
  $main::OLEResult = Win32::FormatMessage(Win32::GetLastError()) . " trying to access \"$olepath\"";
  
  my $logdir = $siteobj->{'LogFileDirectory'} if $siteobj;
  my $todaystr = POSIX::strftime("%y%m%d", localtime());
  my $toDayFile = 'ex'.$todaystr.'.log';
  $main::logfile = "$logdir\\W3SVC$main::siteid\\$toDayFile" if $logdir;
  unless ( -f $main::logfile ) {
    $toDayFile = 'nc'.$todaystr.'.log';
    $main::logfile = "$logdir\\W3SVC$main::siteid\\$toDayFile" if $logdir;
  }
  #$main::logfile = Win32::OLE->GetObject("IIS://$ENV{'COMPUTERNAME'}/W3SVC/$main::siteid")->{'LogFileDirectory'}."\\W3SVC$main::siteid\\$toDayFile";
  $main::logsize = -s $main::logfile if $main::logfile;
  
  my $sitepath = $main::Server->MapPath('.');
  $main::xrpath = dirname(dirname($sitepath));
}

sub showIISLog {
  my @html = ();
  return '<pre>'."Unable to access Log File \"$main::logfile\" for site $main::siteid\n"
    .'</pre>' unless $main::logfile && -f $main::logfile;
  push @html, "File: $main::logfile size: $main::logsize at ". localtime()."\n";
  (my $logfh = new FileHandle("<$main::logfile")) || push @html, "ERROR ACCESSING $main::logfile - $!";
  if ( $logfh ) {
    $logfh->binmode();
    $logfh->seek(-63000, 2);
    $logfh->read(my $buff, 63000);
    $logfh->close();
    $buff =~ s/[\s\x00]+$//gs;
    $buff = substr($buff, (length($buff) - 32760)) if length($buff) > 32760;
#    $buff =~ s/.*(.{1,32760})$/$1/;
    push @html, $buff."\n";
  }
  push @html, localtime()."\n";
  return '<pre>'.join('',@html).'</pre>';
}

sub strip{
	my $str = $_[0];

	$str =~ s/</</g;
	$str =~ s/>/>/g;
	$str =~ s/[\x00-\x1F]/<b>.<\/b>/g;
	return $str;
}

sub TableHdr {
  my ($title) = (shift);
  my $lvl = shift || 'h2';
  $title .= " ".$main::Application->{'ApplName'};
  return ("<HR /><$lvl>$title</$lvl><HR />"
	  . '<table title="'.$title.'" cellpadding=3 cellspacing=2 WIDTH="98%">'
	  . "<tr><th style=\"width: 15\%;\">Name</th><th>Value</th></tr>\n"
	 );
}

sub addTableRow {
  return '<tr><td style="width: 15%;">' . $_[0] . '</td><td>' . $_[1] . '</td></tr>';
}

sub showCollection {
  my ($hdr, $collection) = (shift, shift);
  my $keysub = shift;

  my @html = ();
  push @html, TableHdr($hdr);
  
  for my $j ( 1..$collection->{'Count'} ) {
    my $keyn = $collection->Key($j);
    next if $keyn eq 'ALL_HTTP' or $keyn eq 'ALL_RAW' or $keyn eq 'iis.cfg.perl'; 
    push @html, addTableRow($keyn, ($keysub ? &{$keysub}($keyn)->Item() : $collection->Item($j)) );
  }
  push @html, '</table>';
  return join('', @html);
}

sub showVarHash {
  my $hdr = shift;
  my $VarHash = shift;
  my @html = ();

  push @html, TableHdr($hdr);
  while ( my ($var, $val) = each %$VarHash ) {
    push @html, addTableRow($var, (ref($val) eq 'HASH' ? showVarHash($var, $val) : $val ));
  }
  push @html, '</table>';
  return join('', @html);
}

sub PerlNameSpace(\%$)
{
  my ($package,$packname) =  @_;
  my $collection = {};
  
  foreach my $symname (sort keys %$package) {
    local *sym = $$package{$symname};
    $collection->{Functions}->{strip($symname).'()'} = ref(\&sym)  if (defined &sym);
    $collection->{Arrays}->{"\@".strip($symname)}    = ref(\@sym)  if (defined @sym);
    $collection->{Scalars}->{"\$".strip($symname)}   = strip($sym) if ((defined $sym) && (strip($symname) !~ /Config_SH|summary/i)); 
    $collection->{Hashes}->{"\%".strip($symname)}    = ref(\%sym)  if ((defined %sym) && !($symname =~/::/));
    $collection->{Packages}->{strip($symname)}       = PerlNameSpace(\%sym, $symname) if (($packname =~ /main::/i) 
										&& (defined %sym) 
										&& ($symname =~ /::/) 
										&& ($symname !~ /^main::/i) );
  }
  return $collection;
}

sub checkDirAccess {
  my $dirpath = shift;
  return 'Does Not Exist' unless -e $dirpath;
  return 'Not A Directory' unless -d $dirpath;

  my $probebuff = "W" x 80;
  my $probefile = "$dirpath/probe.txt";
  my $expected_size = length($probebuff) + 2;
  
  my $xfh = new FileHandle(">$probefile") || return "unable to create $probefile - $!";

  print $xfh $probebuff, "\n";
  $xfh->close();

  return ( $expected_size == -s $probefile ? 'OK' : 'Error during write - probe size: ' . $expected_size . ' File size: ' . -s $probefile);
}

sub checkWorkDir {
  my ($hdr, $workdir) = (shift, shift);

  return TableHdr($hdr).addTableRow('WORKDIR', 'Not Configured').'</table>' unless $workdir;  
  return TableHdr($hdr).addTableRow($workdir, 'Not a directory').'</table>' unless -d $workdir;  

  return (TableHdr($hdr)
	  . addTableRow($workdir, 'OK')
	  . join('', map {addTableRow("$workdir\\$_", checkDirAccess("$workdir\\$_"))} qw(UsersSaveArea UsersXML UsersExcel))
	  . '</table>'
	 );  
}

sub ShowServer {
  setGlobals();
  my $parms = { @_ };
  my $xrcfg = {};
  my $skipXreport = delete($parms->{skipXreport});
  if ( !$skipXreport ) {
    my $rqst = $main::Request->ServerVariables('QUERY_STRING')->item() || '';
    eval "use lib \"\$ENV{XREPORT_HOME}\\\\perllib\"; require XReport; XReport->import();";
    $xrcfg->{error} = $@ if $@;
    if ( !$@ && $rqst =~ /reloadcfg/i ) {
      $XReport::initialized = 0;
      $main::Application->{'iis.cfg.perl'} = '';
      $main::Application->{'cfg.processed'} = '0';
      XReport->import();
      $main::Application->{cfg.reloaded} = ''. localtime() . ''; 
    }
  }
  $xrcfg = $XReport::cfg unless $@;
  
  $Data::Dumper::Terse = 1;
  my $thisFile = __FILE__;
  my $thisVersion = $thisFile . " mtime: ".POSIX::strftime("%Y-%m-%d %H:%M:%S", localtime((stat($thisFile))[9]));
  $main::Response->Write(''
			 . '<html>'
			 . '<head>'
			 . '<title>ShowServer '.$thisVersion.'</title>'
			 . '<style>'
			 . ' body { border: 0; margin:5; padding:0; background-color: white;}'
			 . ' body { font-family: Tahoma, Arial, Verdana; font-size:11px; color: #000080;text-align: left;}   '
			 . ' a:link, a:visited {color: #483D8B;font-style: italic;text-decoration: none;}'
			 . ' a:visited {color: #4B0082;}'
			 . ' th {border: 1px solid darkgray;font-size: 16px; color: #E8E800; background: #483D8B; }'
			 . ' td {border: 1px solid darkgray;font-size: 12px; }'
			 . '</style>'
			 . '</head>'
			 . '<body>'
			 . '<h1>Show General Variables at '.POSIX::strftime('%y%m%d %H%M%S', localtime()).'</h1> '
			 . '<hr noshade size="2" color=#000080>'
			 . showVarHash("Misc Known Variables", 
				       {
					 'rootpath'             => $main::Server->MapPath('/')
					,'dotpath'              => $main::Server->MapPath('.')
					,'OLEResult'            => $main::OLEResult
					,'siteid'               => $main::Request->ServerVariables('INSTANCE_ID')->Item()
					,'IIS log file'         => $main::logfile
					,'AUTH_USER'            => $main::Request->ServerVariables('AUTH_USER')->Item()
					,'cfg.authenticate'     => $main::Application->{'cfg.authenticate'}."\\".$main::Session->{'UserName'}
					,'Computer Name'        => $ENV{'COMPUTERNAME'}
					,'Process Id'           => $$
					,'Application name'     => $main::Application->{'ApplName'}
					,'Application cwd'      => Win32->GetCwd()
					,'Application Domain'   => Win32->DomainName()
					,'Application NodeName' => Win32->NodeName()
					,'Application Login'    => Win32->LoginName()
					,'Site Work Directory'  => $main::Application->{'cfg.workdir'}
					,'Root Parent Dir'      => $main::xrpath
					,'Current Request Cont.'=> $parms->{content} || ''
				       })
			 . checkWorkDir("Site Working Directory Status", $main::Application->{'cfg.workdir'})
			 . showVarHash("SYSTEM ENVIRONMENT",{ %ENV })
			 . showCollection("IIS Server Collection", $main::Request->{'ServerVariables'}, sub {$main::Request->ServerVariables(@_)})
			 . showCollection("IIS Application Collection", $main::Application->{Contents})
			 . showCollection("IIS Session Collection", $main::Session->{'Contents'})
			 . showCollection("Request Params", $main::Request->{'Params'}, sub {$main::Request->Params(@_)})
			 . showCollection("Request Cookies", $main::Request->{'Cookies'}, sub {$main::Request->Cookies(@_)})
			);
  $main::Response->Write('<h2>Last 32k of IIS log</h2><hr/>'.showIISLog().'<hr/>');
  $main::Response->Write(''
			 . '<h2>Perl NameSpace</h2><hr/><pre>$main:: => '.Dumper(\%{ PerlNameSpace(%main::, 'main::')}).'</pre><hr/>'
			 . '</body></html>'
			);
}

1;
