use strict; 

use XReport::AUTOLOAD;

use Data::Dumper qw(Dumper);
use Data::Alias qw(alias);
use XReport;
#use XReport::CTD::IndexFile;
use Search::ElasticSearch;
#use ElasticSearch;

my $normalize_field = {
  'DIPENDENZA' => 'dipendenza',
  'CONTO' => 'conto',
  'CONTO-DEP' => 'conto',
  'DIVISA' => 'divisa',
  'BATTERE.DIVISA' => 'divisa',
  'BATTERE.DIPENDENZA' => 'dipendenza',
  'BATTERE.CONTO' => 'conto',
  'BATTERE.COD.ESERCE' => 'esercente',
  'ESERCENTE' => 'esercente',
  'STABILIMENTO' => 'stabilimento',
  'CASSA' => 'cassa',
  'SPORTELLO' => 'sportello',
};

my $es_types_table = {
 'conto' => ['conto' , ['conto']],
 'dipendenza' => ['dipendenza' , ['dipendenza']],
 'dipendenza,conto' => ['dipendenza' , ['dipendenza', 'dipendenza_conto']],
 'dipendenza,sportello' => ['dipendenza' , ['dipendenza', 'dipendenza_sportello']],
 'esercente' => ['esercente' , ['esercente']],
 'esercente,stabilimento,cassa' => ['esercente' , ['esercente', 'stabilimento', 'stabilimento_cassa']],
 'divisa,dipendenza,conto' => ['divisa', ['divisa', 'dipendenza', 'dipendenza_conto']],
};

my $es = Search::Elasticsearch->new(
  client => "2_0::Direct", 
  nodes => 'c0ctlpw005.sd01.unicreditgroup.eu:8089',
  #max_requests => 10_000,                 # default 10_000
  #trace_calls  => 'log_file',
  #no_refresh   => 0 | 1,
);

my %prefix_fields = (
  job_name      =>  {type  =>  "string",   index => "not_analyzed",  store => "yes", "copy_to" => "full_jrepname"},  
  report_name   =>  {type  =>  "string",   index => "not_analyzed",  store => "yes", "copy_to" => "full_jrepname"},
  full_jrepname =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},
  #date_elab     =>  {type  =>  "date",     index => "not_analyzed",  store => "yes"},
  index_name    =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"}, 
);
my %suffix_fields = (
  from_rba      =>  {type  =>  "string",   index => "no",   store => "yes"},  
  last_rba      =>  {type  =>  "string",   index => "no",   store => "yes"},  
  from_page     =>  {type  =>  "integer",  index => "no",   store => "yes"},  
  last_page     =>  {type  =>  "integer",  index => "no",   store => "yes"},  
  tot_pages     =>  {type  =>  "integer",  index => "no",   store => "yes"},
);

my %varying_fields = (
  dipendenza    =>  {type  =>  "integer",  index => "not_analyzed",  store => "yes"},  
  conto         =>  {type  =>  "long",     index => "not_analyzed",  store => "yes"},  
  sottoconto    =>  {type  =>  "integer",  index => "not_analyzed",  store => "yes"},  
  sportello     =>  {type  =>  "integer",  index => "not_analyzed",  store => "yes"},  
  causale       =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
  categoria     =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
  divisa        =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
  esercente     =>  {type  =>  "long",     index => "not_analyzed",  store => "yes"},  
  stabilimento  =>  {type  =>  "integer",  index => "not_analyzed",  store => "yes"},  
  cassa         =>  {type  =>  "integer",  index => "not_analyzed",  store => "yes"},  
  carta         =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
  importo       =>  {type  =>  "double",   index => "not_analyzed",  store => "yes"},  

  tabulato      =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
  data_lav      =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},
  fondo         =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
  ente          =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
  sgest         =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
  
);

my %reports_fields = (
  report_short_name  =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
  report_long_name   =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
  report_date_time   =>  {type  =>  "date",     index => "not_analyzed",  store => "yes"},
  user_name          =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
  user_data          =>  {type  =>  "string",   index => "no",            store => "yes"},  
  job_number         =>  {type  =>  "string",   index => "no",            store => "yes"},  
  job_name           =>  {type  =>  "string",   index => "not_analyzed",  store => "yes"},  
  job_step_name      =>  {type  =>  "string",   index => "no",            store => "yes"},  
  ddname             =>  {type  =>  "string",   index => "no",            store => "yes"},  
  job_date_time      =>  {type  =>  "date",     index => "not_analyzed",  store => "yes"},
  ddname             =>  {type  =>  "string",   index => "no",            store => "yes"},  
  sysid              =>  {type  =>  "string",   index => "no",            store => "yes"},  
);

#  report_short_name  
#  report_long_name   
#  report_date        
#  user_name          
#  user_data          
#  job_number         
#  job_name           
#  job_step_name      
#  ddname             
#  job_date           
#  ddname             
#  sysid
#  from_rba 
#  last_rba 
#  from_page
#  last_page
#  tot_pages 

sub make_properties {
  my @fields = @_; return (properties => {%prefix_fields, (map {$_ => $varying_fields{$_}} @fields), %suffix_fields})  
}

my %index_mappings = (
   jobn_repn => {
    jobn_repn => {
        properties => {%prefix_fields}    
    }
  },

  reports => {
    reports => {
      properties => {%reports_fields, %suffix_fields}  
    }
  },
  carta => {
    carta => {
      make_properties(qw(carta)),
    },
  },
  importo => {
    importo => {
      make_properties(qw(importo)),
    },
  },
  dipendenza => {
    dipendenza => {
      make_properties(qw(dipendenza)),
    },
    dipendenza_conto => {
      make_properties(qw(dipendenza conto)),
    },
    dipendenza_sportello => {
      make_properties(qw(dipendenza sportello)),                           
    }
  },
  conto => {
	conto => {
      make_properties(qw(conto))
    },
	conto_causale => {
      make_properties(qw(conto causale))
    },
	conto_causale_divisa => {
      make_properties(qw(conto causale divisa))
    },
	conto_dipendenza => {
      make_properties(qw(conto dipendenza))
    },
  },
  estero => {
	conto => {
      make_properties(qw(conto)) 
    },
	conto_divisa => {
      make_properties(qw(conto divisa))
    },
	conto_sottoconto => {
      make_properties(qw(conto sottoconto))
    },
	conto_sottoconto_divisa => {
      make_properties(qw(conto sottoconto divisa))
    },
  },
  esercente => {
    esercente => {
	  make_properties(qw(esercente))
    },
	stabilimento => {
      make_properties(qw(esercente stabilimento))
    },
	stabilimento_cassa => {
      make_properties(qw(esercente stabilimento cassa))
    }
  },
  divisa => {
	divisa => {
      make_properties(qw(divisa))
    },
    importo => {
      make_properties(qw(divisa importo))
    },
	categoria => {
      make_properties(qw(divisa categoria))
    },
	categoria_conto => {
      make_properties(qw(divisa categoria conto))
    },
	dipendenza => {
      make_properties(qw(divisa dipendenza))
    },
	dipendenza_conto => {
      make_properties(qw(divisa dipendenza conto))
    },
	conto => {
      make_properties(qw(divisa conto))
    },
	conto_dipendenza => {
      make_properties(qw(divisa conto dipendenza))
    }
  },
  tabulato => { 
    tabulato => {
      make_properties(qw(tabulato))
    },
	tabulato_data_lav => {
      make_properties(qw(tabulato data_lav))
    },
	fondo_data_lav => {
      make_properties(qw(tabulato data_lav fondo))
    },
	sgest_data_lav => {
      make_properties(qw(tabulato data_lav sgest))
    },
	sgest_fondo_data_lav => {
      make_properties(qw(tabulato data_lav sgest fondo))
    },
	fondo => {
      make_properties(qw(tabulato fondo))
    },
	fondo_data_lav => {
      make_properties(qw(tabulato fondo data_lav))
    }
  },
  data_lav => { 
    data_lav => {
      make_properties(qw(data_lav))
    },
  },
  ente => { 
    ente => {
      make_properties(qw(ente))
    },
    importo => {
      make_properties(qw(ente importo))
    },
  },
);

sub online_index_to_es {
    my ($ctd_id, $ientry, $es_index_name, $es_type_name, $job_name, $report_name) = @_;
     $es->index(
      index => $es_index_name, type  => $es_type_name,
      id    => "$job_name/$ientry",
      body  => {
        job_name      => $job_name,
        report_name   => $report_name,
        index_name    => $es_index_name,
        #date_elab     => $date_elab,
        }
    );
}

sub make_index {
  my ($ctd_id, $index) = @_; my $index_name = $index."_".$ctd_id;
  #eval {
  #  my $result = $es->delete_index(index => $index_name);
  #};
    print Dumper("ok", {index => $index_name , mappings => {%{$index_mappings{$index}}}});
  my $result = $es->indices->create(
    index => $index_name, 
    body => 
        {mappings => {%{$index_mappings{$index}}}}
  );
}

sub delete_index {
  my ($ctd_id, $index) = @_; my $index_name = $index."_".$ctd_id;
  my $result = $es->indices->delete(index => $index_name);
}
my $result;


#my $ctd_id = "a1test";
#for my $ctd_id (qw(a1 a3 a7)) {
for my $ctd_id (@ARGV){
	$ctd_id = lc $ctd_id;
    my $dbr = dbExecute("insert into [XNCTD$ctd_id].[dbo].[tbl_Job_ReportMappings] 
    select distinct computedJobName, textString from [XNCTD$ctd_id].[dbo].tbl_JobReports jr with (nolock) join [XNCTD$ctd_id].[dbo].tbl_LogicalReportsRefs lrf with (nolock) on jr.JobReportId = lrf.Jobreportid
    join [XNCTD$ctd_id].[dbo].tbl_LogicalReportsTexts lrt with (nolock)  on lrf.Textid = lrt.Textid
    where XFerStartTime > '2015-10-01 00:00:00.000'
    and not exists (select 1 from [XNCTD$ctd_id].[dbo].[tbl_Job_ReportMappings] jrm with (nolock) where jrm.[JobReportNameKey] = computedJobName
    and jrm.[XRReportName] = textString)");

    if ($@) {
      print "$@\n";
      die "problem with insert into table for XNCTD$ctd_id ";
    }

    if(!$dbr->eof()){
        print "message from db";
    }

  eval{
    $result = delete_index($ctd_id, 'jobn_repn');
  };
  print Dumper("errore", $@) if($@);
  $result = make_index($ctd_id, 'jobn_repn');



  #where JobReportNameKey <> '$PO293C0' or XRReportName <> 	'CONTO SCALARE'
  my $dbr = dbExecute("select * from [XNCTD$ctd_id].[dbo].tbl_job_ReportMappings order by 1");
  #my $dbr = dbExecute("
  #  select a.*, b.key_fields from tbl_ctd_restore_entries_$ctd_id a inner join tbl_ctd_index_entries_$ctd_id b on a.file_id = b.index_id
  #  
  #  where a.file_type = 'index' and a.es_status <> 1 and b.key_fields = 'DIPENDENZA,CONTO' 
  #    and exists(
  #      select volser from tbl_ctd_bkp_restored_volsers c
  #      where volser = a.from_volser and end_status=16
  #    )
  #");

  #CONTO-DEP,DIPENDENZA

  #use XReport::CTD::Context;
  #my $ctx = XReport::CTD::Context->new();
  my $iloop = 1;
  while (!$dbr->eof()) {
    #my ($index_file_name, $restore_id, $file_id, $tot_blocks) = $dbr->getValues(qw(file_name restore_id file_id tot_blocks));
    my ($jobnamekey,$reportname) = $dbr->getValues(qw(JobReportNameKey XRReportName));
    print "$jobnamekey--", $iloop."\n";
    eval {
      online_index_to_es($ctd_id, $iloop,"jobn_repn_$ctd_id", 'jobn_repn', $jobnamekey, $reportname);
      #ctd_index_file_to_es($ctd_id, $index_file_name);
      $iloop += 1; #last if $iloop >= 10;
    };
    if ($@) {
      print "$@\n";
      open(OUT, ">zzqqbad.$jobnamekey.TXT"); binmode(OUT);
      print OUT "bad_elab";
      
      close(OUT);
      print "------ $jobnamekey ---- $@"; last;
    }
  }
  continue {
    $dbr->MoveNext();
  }
  print "-------end load $ctd_id----- ";
}
print "-------end elab-----";
__END__

if (0) {
my $result = $es->search(
        index   => 'divisa_a7',               # all
        type    => 'dipendenza_conto',
        query  => { 
          prefix => {_id => "21247/"},
        }
    );

print Dumper($result);
}
#__END__





