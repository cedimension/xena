#!perl -w

use strict;

$| = 1;
my $oldfh = select STDERR;
$| = 1;
select $oldfh;


use XReport;
use XReport::DBUtil ();
use XReport::JobREPORT ();
use XReport::ARCHIVE::JobREPORT ();
use Data::Dumper; 


my ($workdir, $JobReportId, $newname) = grep !/^-/, @ARGV;
my $jr = XReport::ARCHIVE::JobREPORT->Open($JobReportId);
my $INPUT = $jr->get_INPUT_ARCHIVE();
my $fpath = $INPUT->getFileName();
if ( $fpath !~ /^file\:/i) {
	my $diemsg = "Archive type \"".substr($fpath, 0, 10). "\" is not supported";
	i::logit($diemsg);
	die $diemsg;
}
if ( $fpath !~ /^file\:/i) {
	my $diemsg = "Archive type \"".substr($fpath, 0, 10). "\" is not supported";
	i::logit($diemsg);
	die $diemsg;
}
$fpath =~ s/^file:\/\///i;
unless (-e $fpath && -f $fpath && -s $fpath) {
	my $diemsg = "Unable to access $fpath or file empty (size:".-s $fpath.") - migration failed";
	i::logit($diemsg);
	die $diemsg;
}
my ($JobReportName, $LocalFileName) 
          = $jr->getValues(qw(JobReportName LocalFileName));
my $newfpath = $fpath =~ s/$JobReportName/$newname/;
my $newLFN = $LocalFileName =~ s/$JobReportName/$newname/;
 
unless ( rename( $fpath, $newfpath ) ) {
	my $diemsg = "Rename from \"$fpath\" to \"$newfpath\" failed - aborting - $! - $^E";
	i::logit($diemsg);
	die $diemsg;
}

unless ( -e $newfpath && -f $newfpath ) {
	my $diemsg = "Rename from \"$fpath\" to \"$newfpath\" not verified - aborting";
	i::logit($diemsg);
	die $diemsg;
}
 
my $dbc = XReport::DBUtil->new();
$dbc->dbExecute_NORETRY (''
            . " UPDATE tbl_JobReports "
            . " SET JobReportName = '$newname', LocalFileName = '$newLFN' "
            . " WHERE JobReportId = $JobReportId "
            );

$jr->Close();

my $currjr = XReport::ARCHIVE::JobREPORT->Open($JobReportId);
my $currINPUT = $currjr->get_INPUT_ARCHIVE();
my $currfpath = $currINPUT->getFileName();
my ($currJobReportName, $currLFN) = $jr->getValues(qw(JobReportName LocalFileName));

if ( $currLFN eq $newLFN && $currfpath eq $newfpath && $currJobReportName eq $newname ) {
	i::logit("JobReport $JobReportId renamed successfully to $newname (\"$newLFN\")");
	exit 0;
} 

rename $newfpath, $fpath;
die "DBUPDATE Failed - file rename reversed check $JobReportId, \"$fpath\", \"$newfpath\"";
