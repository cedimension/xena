#!/usr/local/bin/perl -w
#######################################################################
# @(#) $Id: xrDaemon.pl 2250 2008-07-02 12:50:03Z mpezzi $
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
#use strict;
#use Data::Dumper;
#use lib($ENV{'XREPORT_HOME'}."/perllib");

use constant DO_REDIRECT => 1;
use constant DEBUG => 0;

BEGIN {
   require XReport;
   XReport->import(redirect => DO_REDIRECT, rotatelog => 1);
}

use Win32;
use Win32::Daemon;
use Win32::Console;

use XReport::Util;
use XReport::SUtil;

#use POSIX qw(strftime);
#use File::Basename qw(basename);
use constant EXITCODE2RESTART => 123;
use constant MAX_RETRY_ATTEMPTS => 3;

my $version = do { my @r = (q$Revision: 1.3 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

#XReport::Util::logAndDie "xxxx", Dumper($main::Application), "\n";
#__END__

InitServer(isdaemon => 1, rotatelog => 1);
(my $testrun) = grep /\-testrun/, @main::ARGV;
#XReport::Util::logAndDie "testrun: $testrun ARGS:", join('--::--', @main::ARGV);
if ( $testrun ) {
	print "TESTRUN: ovveriding logrtn\n";
	$logrRtn = sub { print @_, "n"; };
}

my ($SrvName, $debug, $basedir, $hash_LOGROTATION) = getConfValues(qw(SrvName debugLevel basedir LOGROTATION));
my ($EXITCODE2RESTART, $MAX_RETRY_ATTEMPTS) = (EXITCODE2RESTART, MAX_RETRY_ATTEMPTS);
$EXITCODE2RESTART = $1 if (( $hash_LOGROTATION =~ '^HASH') and ($hash_LOGROTATION->{'EXITCODE2RESTART'}) and ($hash_LOGROTATION->{'EXITCODE2RESTART'} =~ /^(\d+)$/)) ;
$MAX_RETRY_ATTEMPTS = $1 if (( $hash_LOGROTATION =~ '^HASH') and ($hash_LOGROTATION->{'MAX_RETRY_ATTEMPTS'}) and ($hash_LOGROTATION->{'MAX_RETRY_ATTEMPTS'} =~ /^(\d+)$/)) ;


my $cfg = $XReport::cfg->{'daemon'}->{$SrvName};
my $pgmName = $cfg->{'process'} || XReport::Util::logAndDie "Program Name to start missing";
my @pgmparms = ('-N', $SrvName, '-@');
@pgmparms = ( map {
	/^"(.+)"?$/ ? $1 : /^([^"].*)"$/ ? $1 : $_
                 } split /\s+/, $cfg->{'procparm'} ) if exists $cfg->{'procparm'};
if(grep /^-(daemon|@)$/i, @pgmparms )
{
	#remove -daemon/-@ and add -N <SrvName> -@
	foreach my $i (0 .. $#pgmparms)
	{
		if($pgmparms[$i] =~ /^-(daemon|@)$/i)
		{
			splice @pgmparms, $i+1, 1;
			last;
		}
	}
#	while(1)
#	{
#		my $first = shift @pgmparms;
#		($first =~ /^-(daemon|@)$/i) and  last;
#		push @pgmparms, $first;
#	}
	push @pgmparms, ('-N', $SrvName, '-@');
}

my $procAff = $cfg->{$SrvName}->{'procaff'} if ($cfg->{'procaff'});
$pgmName = "$basedir/bin/$pgmName" unless $pgmName =~ /^[\/\\]{2}/;
my $forkcmd = ($pgmName =~ /\.pl$/ ? XReport::SUtil->can('spawnProcess')
                                   : XReport::SUtil->can('spawnProcessNotPerl') );
$forkcmd = XReport::SUtil->can('spawnProcess') unless $forkcmd;

my $State;

my $ProcessObj = 0;

my $SLEEP_TIMEOUT = 1;
my $SERVICE_BITS = USER_SERVICE_BITS_8;

Win32::Daemon::Timeout( 5 );

Win32::Daemon::StartService() || XReport::Util::logAndDie "Service not Started - $! - $? - $@\n";
&$logrRtn("xrDaemon Service Started main loop HOME: $ENV{XREPORT_HOME}");
Win32::Daemon::SetServiceBits( $SERVICE_BITS ) || XReport::Util::logAndDie "xrDaemon Service not set bits\n";
&$logrRtn("xrDaemon Service Service bits set");
#if (!Win32::Daemon::ShowService()) {
#  &$logrRtn ("xrDaemon ShowService Failed");
#  XReport::Util::logAndDie "xrDaemon ShowService Failed\n";
#}
#$State = Win32::Daemon::QueryLastMessage();
#&$logrRtn ("last Message $State");

&$logrRtn("xrDaemon Service Loop main loop");
while ( SERVICE_START_PENDING != Win32::Daemon::State() ) {
	&$logrRtn("xrDaemon Start pending state detected sleeping for $SLEEP_TIMEOUT seconds");
	sleep( $SLEEP_TIMEOUT);
	last if $testrun;
}

my $firstIteration=1;
my $RETRY_ATTEMPT = 0;
my $ProcessID = -1;
while( 1 ) {
	$State = Win32::Daemon::State();
#  &$logrRtn("Checking State $State." );
  if( !$ProcessObj && ( $testrun || SERVICE_START_PENDING == $State ) ) {
    # Initialization code
    &$logrRtn("xrDaemon Service pending. Setting state to Running." );
    Win32::Daemon::State( SERVICE_RUNNING );
    &$logrRtn( "xrDaemon Starting $pgmName" );
    print "$0 starting \"$pgmName\" PARMS: ", join('::', @pgmparms), "\n" if $testrun;


	if(($firstIteration) && ($pgmName =~ /xrmultiplex.exe/i))
	{
		$firstIteration = 0;
		#test if the port is aready in use
		&$main::logrRtn("xrDaemon test port xrmultiplex.exe.");
		$cfg->{'procparm'} =~ /([0-9]+)[^0-9]*$/;
		my $listenerPort = $1;
		unless($listenerPort)
		{
			&$main::logrRtn("xrDaemon Error - the port is not correct. Check 'procparm' in the configuration.");
			#exit 0;
			last;
		}
		my $results = `netstat -anop tcp | findstr /i /c:\":$listenerPort \" | findstr /i /c:\"LISTENING\"`;
		if( $results)
		{
			chomp($results) if $results;
			$results =~ /LISTENING[ \t\s]+([0-9]+)$/i;
			my $pid = $1;
			&$main::logrRtn("xrDaemon Error - The process cannot be started, another process already use the tcp port :\"$listenerPort\":");
			&$main::logrRtn("xrDaemon OUT_netstat:".$results);
			$results = `TASKLIST /FI \"PID eq ${pid}\"  /FO csv /NH`;
			chomp($results) if $results;
			&$main::logrRtn("xrDaemon OUT_TASKLIST:".$results) if $results;
			$results = `wmic process where \"ProcessID=${pid}\" get CommandLine, ExecutablePath, CreationDate /FORMAT:VALUE | findstr /i /c:"CommandLine" /c:"CreationDate" /c:"ExecutablePath"`;
			chomp($results) if $results;
			&$main::logrRtn("xrDaemon OUT_wmic:".$results) if $results;
			#exit 0;
			last;
		}
		&$main::logrRtn("xrDaemon test port ok - the port $listenerPort is free.");
	}

    $ProcessObj = &$forkcmd($pgmName, @pgmparms);

    $ProcessObj->SetProcessAffinitymask($procAff) if ($procAff);
	$ProcessID = $ProcessObj->GetProcessID();
	&$logrRtn("xrDaemon Start of [".File::Basename::basename($pgmName)."] ID[$ProcessID] pending." );
  }
  elsif( SERVICE_PAUSE_PENDING == $State ) {
    &$logrRtn( "xrDaemon Pausing..." );
    $ProcessObj->Suspend();
    Win32::Daemon::State( SERVICE_PAUSED );
    next;
  }
  elsif( SERVICE_CONTINUE_PENDING == $State ) {
    &$logrRtn( "xrDaemon resuming... " );
    $ProcessObj->Resume();
    Win32::Daemon::State( SERVICE_RUNNING );
    next;
  }
  elsif( SERVICE_CONTROL_SHUTDOWN == $State ) {
    &$logrRtn( "xrDaemon Shutting Down... " );
    Win32::Daemon::State( SERVICE_STOP_PENDING, 30 );
	next;
    }
  elsif( SERVICE_STOP_PENDING == $State ) {
    &$logrRtn( "xrDaemon Stopping..." );
    $ProcessObj->Kill(0);
    last;
  }
#  elsif( SERVICE_RUNNING == $State ) {
#    if ( !kill(0, $ProcessObj->GetProcessID()) ) {
#        last;
#      }
#    }
  elsif( SERVICE_CONTROL_INTERROGATE == $State ) {
#    &$logrRtn( "xrDaemon Query... " );
    $ProcessObj->GetExitCode(my $exitcode);
    if ( $ProcessObj->Wait(0) ) {
      &$logrRtn( "xrDaemon failed to detect $pgmName ID[$ProcessID] RC[$exitcode] running... stopping.." );
	  if(($exitcode eq $EXITCODE2RESTART) or ($RETRY_ATTEMPT < $MAX_RETRY_ATTEMPTS))
	  {
		$RETRY_ATTEMPT++;
		$RETRY_ATTEMPT = 0 if(($exitcode eq $EXITCODE2RESTART));
	    &$logrRtn("xrDaemon found exitcode $exitcode - the process [".File::Basename::basename($pgmName)."] will be restarted - attempt $RETRY_ATTEMPT of $MAX_RETRY_ATTEMPTS..." );
	    my $logfname = getConfValues("logsdir")."/$SrvName.LOG";
		$logger->RemoveFile('ServerLog');
        if(-f $logfname)
        {
		    &$logrRtn("xrDaemon - LOG [$logfname] will be stored." );
			if(DO_REDIRECT)
			{
				close(STDOUT) || XReport::Util::logAndDie("Error in close(STDOUT) - $! - $^E");
				close(STDERR) || XReport::Util::logAndDie("Error in close(STDERR) - $! - $^E");
			}
        	rename($logfname, $logfname.POSIX::strftime('%Y%m%d%H%M%S', localtime) ) or XReport::Util::logAndDie("Error in rename of file $logfname - $! - $^E");
        }
		if(DO_REDIRECT)
		{
			open( STDOUT, ">> $logfname" ) || XReport::Util::logAndDie( "Unable to redirect STDOUT to $logfname" );
			open( STDERR, ">&STDOUT" ) || XReport::Util::logAndDie( "Unable to redirect STDERR to $logfname" );
		}
		$logger->AddFile('ServerLog', $logfname);
		# Initialization code
		&$logrRtn("xrDaemon Service pending. Setting state to Running." );
		Win32::Daemon::State( SERVICE_RUNNING );
		&$logrRtn( "xrDaemon Starting $pgmName" );
		print "$0 starting \"$pgmName\" PARMS: ", join('::', @pgmparms), "\n" if $testrun;
		$ProcessObj = &$forkcmd($pgmName, @pgmparms);
		&$logrRtn( "xrDaemon Started $pgmName - processObj:".ref($ProcessObj) );
		$ProcessObj->SetProcessAffinitymask($procAff) if ($procAff);
		$ProcessID = $ProcessObj->GetProcessID();
		&$logrRtn("xrDaemon Start of [".File::Basename::basename($pgmName)."] ID[$ProcessID] pending." );
		next;
	  }
	  &$logrRtn("xrDaemon found exitcode $exitcode - the process will be stopped..." );
	  last;
    }
    Win32::Daemon::State( SERVICE_RUNNING );
  }
  else {
    &$logrRtn( "xrDaemon Unknown state $State... " );
    if ( $ProcessObj->Wait(0) ) {
      &$logrRtn( "xrDaemon failed to detect $pgmName running... stopping.." );
      last;
    } else {
      Win32::Daemon::State( SERVICE_RUNNING );
	}
  }
  sleep( $SLEEP_TIMEOUT );
}

Win32::Daemon::State( SERVICE_STOPPED );
sleep( $SLEEP_TIMEOUT );
Win32::Daemon::StopService();

exit 0;

__END__
