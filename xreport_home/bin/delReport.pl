use strict 'vars';

use File::Basename;

use XReport::Util;
use XReport::DBUtil;

use Win32::OLE;
use Win32::OLE::Const;

my $debug   = 0;
my $daemon = 0;
my $SrvName;

$::xrcfg = XRgetConf();

my $ReportType = shift || die("Usage delReport ReportType ReportID..");

for (@ARGV) {
  s/^ +//;s/ +$//;
  if ( /^\d+$/ ) {
  }
  else {
    die("ReportID $_ NOT NUMERIC !?");
  }
  delReport($ReportType, $_);
}
