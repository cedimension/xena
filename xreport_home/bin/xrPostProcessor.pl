#!/usr/bin/perl -w

package Local::Routines;

sub getAttribute {
 my ($self, $var) = (shift, shift);
 my $value = $self->{$var};
 warn "getAttribute $var: $value\n";
 return $value;
}

1;

package main;
$main::debug = 1;
use lib "$ENV{XREPORT_HOME}/perllib";
use strict "vars";

use File::Basename;

use Data::Dumper;

use XReport;
use XReport::Util;
use XReport::DBUtil;
use XReport::JobREPORT;
use XReport::ENUMS qw(:ST_ENUMS);

use XML::Simple;

my $workdir = $ARGV[0];
my $JRID = $ARGV[1] || die "Unable to get JobReportId\n";
my $perllib = $XReport::cfg->{userlib}."/perllib";

$logrRtn = sub { print @_, "\n"; };

my $jr = XReport::JobREPORT->Open($JRID, 1);
my @varlist = qw(LocalFileName SrvName RemoteHostAddr XferMode TargetLocalPathId_IN TargetLocalPathId_OUT JobName RemoteFileName JobReportName);
my $cfgvars = {};
@{$cfgvars}{@varlist} = $jr->getValues(@varlist);

my $rptconf_in = $jr->getFileName('PARSE');
my $rptconf_buff = '';
my $cfgfhin = new FileHandle("<".$rptconf_in) || die "unable to access rptconf $rptconf_in\n";
while (<$cfgfhin>) {
  my $lout = $_;
  $lout =~ s/\$(\w+)/$cfgvars->{$1}/sg;
  $rptconf_buff .= $lout;
}
close $cfgfhin;

my $reportcfg = XMLin($rptconf_buff,
              ForceArray => [ qw(index reload op ix exit) ],
              KeyAttr => {'index' => 'name', 'reload' => 'name', 'op' => 'name', ix => 'name'},
              Variables => $cfgvars
             );

foreach my $exit ( @{$reportcfg->{exit}} ) {
  warn ( map { "=" x 20 . " " . $exit->{$_} . " " . "=" x 20 . "\n" } qw(type perlclass ref) ) if $main::debug;
  next unless $exit->{type} =~ /^post$/i;
  my $self = bless $exit, 'Local::Routines';
  warn "Exit ref: ", ref($self), "\n" if $main::debug;
  use Data::Dumper;
  warn Dumper($self), "\n" if $main::debug;
  my $perlclass = $exit->{perlclass};
  my $ref = $exit->{ref};
  require "$perllib/$ref";
  
  my $postexit = $perlclass->new( $jr, $workdir, $self, $logrRtn );
  
  $postexit->Commit($jr); 
  $postexit->Close();

}

exit 0;
