use lib "$ENV{XREPORT_HOME}\\perllib";

use XReport;
use Data::Dumper;
use FileHandle;

use XReport::SOAP;
use LWP;

my $rserver = c::getValues('resourceServer');
die "Unable to get Resource server address" unless $rserver;
my $ProjectName = $ARGV[0];

print XReport::SOAP::buildResponse
	  ( {IndexName => '_Resources', 
	     IndexEntries => XReport::SOAP::whereEntries
	     ( [ { ResName => $ProjectName,
		   ResClass => 'prj',
		   XferStartTime => ['1970-01-01', '2009-09-25'],
		 } ] )
	    }), "\n";

my $cfgresp = LWP::UserAgent->new()
  ->post( "$rserver/?getResources", 
	  #			[ cfguser => $ENV{USERNAME},
	  Content_type => 'text/xml',
	  SOAPAction => 'getResources',
	  Content => XReport::SOAP::buildResponse
	  ( {IndexName => '_Resources', 
	     IndexEntries => XReport::SOAP::whereEntries
	     ( [ { ResName => $ProjectName,
		   ResClass => 'prj',
		   XferStartTime => ['1970-01-01', '2009-09-25'],
		 } ] )
	    }, 'REQUEST'),
	  #			] 
	);

if ( !$cfgresp->is_success() ) {
  i::logit("Error response from server - ", $cfgresp->status_line, 
	  $cfgresp->content());
}
else {
  my $response = XReport::SOAP::xtractParm(XReport::SOAP::_XMLin($cfgresp->content()), qr/RESPONSE/);
  my ($fn, $ft, $content) = @{$response}{qw(FileName DocumentType DocumentBody)} if $response;
  i::logit("FN: $fn FT: $ft, content: ". length($content));
  
  if ($fn) {
    my $fh = new FileHandle(">$fn");
    binmode $fh;
    XReport::SOAP::_decode_base64($content, sub { my ($fh, $buff) = (shift, shift); print $fh $buff; }, $fh);
    close $fh;
  }
}
