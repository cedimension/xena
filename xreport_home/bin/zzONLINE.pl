use strict;

use XReport::DBUtil;
use Date::Calc qw(Date_to_Days Day_of_Week Add_Delta_Days Delta_Days);

my (@dateRef, @toDay, %rt, $rt, $rid, $hdays, $hgens);

my $dbr;

sub toDay {
  my @t = localtime();
  return ( $t[5]+1900, $t[4]+1, $t[3] );
}

sub ExpireDate {
  my ($hdays, @refDay) = @_;

  if ( !@refDay ) {
    @refDay = @toDay;
  }
  my $weekDay = Day_of_Week(@refDay);
  
  my $weeks = int($hdays/5);
  my $rest = $hdays - $weeks*5; 

  my $deltaDays = $weeks*7 + $rest + 
  ( ( (5 - $weekDay) > $rest )  
    ? 0
    : 2
  );

  return Add_Delta_Days(@refDay, $deltaDays);
}

@dateRef = (2001, 10, 5);
@toDay = toDay();

$dbr = dbGetDynamicRs(<<EOF);
SELECT  ReportName, HoldDays, HoldGens
FROM  tbl_ReportNames a
WHERE EXISTS( 
  SELECT b.ReportName FROM tbl_Reports b
  WHERE b.ReportName = a.ReportName
)
EOF
#and ReportName LIKE 'CCG0000%'

while (!$dbr->eof()) {
  $rt = $dbr->Fields->Item('ReportName')->Value();
  $hdays = $dbr->Fields->Item('HoldDays')->Value();
  $hgens = $dbr->Fields->Item('HoldGens')->Value();
  $hgens = 5 if ( $hgens == 0 and $hgens == 0 ); 
  #print "?? $rt $hdays $hgens\n";
  $rt{$rt} = [$hdays, $hgens, 0];
  $dbr->MoveNext();
}
$dbr->close();

$dbr = dbGetDynamicRs(<<EOF);
SELECT  ReportName, ReportId, CONVERT(varchar, XferStartTime, 111) XferStartTime, ViewLevel 
FROM  tbl_Reports
WHERE ViewLevel = '1'
order by ReportName, XferStartTime desc
EOF
#and ReportName = 'CCG00005'


while (!$dbr->eof()) {
  my $rt = $dbr->Fields->Item('ReportName')->Value();
  my $rid = $dbr->Fields->Item('ReportId')->Value();
  my $xfrDay = $dbr->Fields->Item('XferStartTime')->Value();

  if ( !exists($rt{$rt}) ) {
    print "NOT EXISTS $rt\n";
    $dbr->MoveNext();
	next;
  }
  
  my $aref = $rt{$rt};
  my ($hdays, $hgens, $tON) = @$aref;

  my (@xfrDay, @exprDay) = ();

  $hdays and do {
    @xfrDay = unpack("a4xa2xa2", $xfrDay);
    @exprDay = ExpireDate($hdays, @xfrDay);
  };
 
  my $hdaysExpired = ( !$hdays or (Delta_Days(@exprDay,@dateRef) > 0) );
  my $hgensExpired = ( !$hgens or $tON >= $hgens );

  #print "?? ", join("/",@xfrDay), " ", join("/",@exprDay), " ", Delta_Days(@exprDay,@dateRef), "\n";exit;
  
  if ( $hdaysExpired and $hgensExpired ) {
    print "$rt $rid ", join("/", @xfrDay), "\n";
    $dbr->Fields->Item('ViewLevel')->SetProperty("Value", 2);
    $dbr->Update();
  }
  else {
    $aref->[2] += 1;
  }
  
  $dbr->MoveNext();
}

$dbr->close();
