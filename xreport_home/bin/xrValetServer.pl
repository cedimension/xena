
sub listPrinterPorts {
  use Win32::TieRegistry 0.20 qw(
				  TiedRef $Registry
				  ArrayValues 1  SplitMultis 1  AllowLoad 1
				  REG_SZ REG_EXPAND_SZ REG_DWORD REG_BINARY REG_MULTI_SZ
				  KEY_READ KEY_WRITE KEY_ALL_ACCESS
			       );
  my $kpath = "LMachine\\SYSTEM\\CurrentControlSet\\Control\\Print\\Monitors\\";
  my $ports = {};

  my $printMonKey = $Registry->Open($kpath) || die "Can't read \"$kpath\" key: $^E\n";
  $printMonKey->ArrayValues(1);
  
  foreach my $subKeyName (  $printMonKey->SubKeyNames() ) {
    my $subKey = $printMonKey->{$subKeyName} || die "Can't read \"$kpath$subKeyName\\\" key: $^E\n";
    if (my $portsKey = $subKey->{Ports}) {
      foreach my $portName ( $portsKey->SubKeyNames() ) {
	my $regPort = $portsKey->{$portName} || die "Can't read \"$kpath$subKeyName\\Ports\\$portName\\\" key: $^E\n";
	$ports->{$portName} = { map { ($_ =~ /^\\?(.*)/) => ( $regPort->{$_}->[0] =~ /^0x(.*)$/ ? unpack("N", pack("H*", $1)) :  $regPort->{$_}->[0] ) } keys %{$regPort} };
      }
    }
  }
  return $ports;
}

sub getPrinterPort {
  my $name = shift;
  $main::printerports = listPrinterPorts() unless $main::printerports;
  my $properties = $main::printerports->{$name};
  return {} unless $properties;
  return $properties;
}


sub PortName {
  my ($name, $value) = (shift, shift);
  return { $name => 'N/A'  } unless $value;
  return { $name => $value } if $value =~ /(?:^\\\\|:$)/;
  my $list = {};
  my @props = qw(HostName IPAddress  HWAddress PortNumber Protocol NetworkTimeout);
  @{$list}{@props} = @{getPrinterPort($value)}{@props};
  return { $name => $value, %$list };
}

sub PaperSizesSupported { 
  my ($name, $list) = (shift, shift); 
  return { $name => 'N/A'} unless $list;
  my $decoded = { map { $_ => ( /^0$/ ? 'Unknown'
				: /^1$/ ? 'Other'
				: /^4$/ ? 'C'
				: /^5$/ ? 'D'
				: /^6$/ ? 'E'
				: /^7$/ ? 'Letter'
				: /^8$/ ? 'Legal'
				: /^11$/ ? 'NA-Number-10-Envelope'
				: /^15$/ ? 'NA-Number-9-Envelope'
				: /^21$/ ? 'A3'
				: /^22$/ ? 'A4'
				: /^23$/ ? 'A5'
				: /^49$/ ? 'JIS B0'
				: /^54$/ ? 'JIS B5'
				: /^55$/ ? 'JIS B6'
				: $_ ) } @$list };
  return {$name => join("\t", @{$decoded}{sort keys %$decoded})};
}

sub setValue {
  #  print "setting Values for $_[0]\n";
  my ($name, $value) = (shift, shift);
  return { $name => (defined($value) ? $value : 'N/A') };
}

sub listPrinters {
  use Win32::OLE('in');
  my $ports = listPrinterPorts();
  my @propertiesList = qw(
			   Caption
			   Comment 
			   Description 
			   DeviceID 
			   DriverName 
			   ExtendedPrinterStatus 
			   HorizontalResolution 
			   InstallDate 
			   LanguagesSupported 
			   Location 
			   MaxSizeSupported 
			   PaperSizesSupported 
			   PortName 
			   PrinterState 
			   PrinterStatus 
			   Queued 
			   ServerName 
			   Status 
			   StatusInfo 
			   SystemName 
			);

  use constant wbemFlagReturnImmediately => 0x10;
  use constant wbemFlagForwardOnly => 0x20;
  my $computer = ".";
  my $objWMIService = Win32::OLE->GetObject
    ("winmgmts:{impersonationLevel=impersonate}\\\\$computer\\root\\CIMV2") or die "WMI connection failed.\n";
  my $colItems = $objWMIService->ExecQuery
    ("SELECT * FROM Win32_Printer","WQL",wbemFlagReturnImmediately | wbemFlagForwardOnly);
  
  my $printers = {};
  foreach my $objItem (in $colItems) {
    $printers->{$objItem->{Name}} = 
      { map { my $setcode = main->can($_) || \&main::setValue; 
	      %{&$setcode($_, $objItem->{$_})} } @propertiesList};
  }
  return $printers;
}
my $cfg = $XReport::cfg->{daemon}->{$ARGV[1]};
my ($SrvAddr, $SrvPort, $msgmax) = @{$cfg}{qw(ListenAddr ListenPort msgmax)};

$SrvAddr = "0.0.0.0" unless $SrvAddr;
$SrvPort = 9888 unless $SrvPort;
$msgmax = 4096 unless $msgmax;

use IO::Socket;
use IO::Select;
use Win32API::File qw(:ALL);
use Win32::Process;

my ($sock, $Cli, $currSt);
i::logit("$^O $ARGV[1] Central server Started for $SrvAddr\:$SrvPort...");

my $SrvSock = new IO::Socket::INET(Listen => 5, 
				   LocalAddr => "$SrvAddr:$SrvPort",
				   Proto => 'tcp', 
				   Reuse => 1) || die "unable to listen on $SrvPort - $!\n";

i::logit("Listener now waiting on port $SrvPort") if ($SrvSock);

my $sel = new IO::Select( $SrvSock );

my $currcmd = (split /[\\\/]+/, $0)[-1];
(my $clicmd = "perl \"$XReport::cfg->{clicodefn}\" $msgmax "); 
i::logit("now entering main loop (cmd: \"$clicmd\")") if ($SrvSock);
for (my $con = 0; ; $con++) {
  $sock = $sel->can_read(15);
  next unless ($sock);
  $Cli = $SrvSock->accept || i::logit("unable to accept connection - $! - $?");
  last unless $Cli;

  my ($cliport, $iaddr) = sockaddr_in(getpeername($Cli));
  $REQ::peer = inet_ntoa($iaddr);
  i::logit("$main::servername accepted connection from " . $REQ::peer);
  Win32::Process::Create( my $ProcessObj, "$^X", "$clicmd " . FdGetOsFHandle( $Cli->fileno ), 1, Win32::Process->NORMAL_PRIORITY_CLASS, "." )
      || i::logit("ERROR Launching $clicmd");
}
i::logit("now leaving main loop: $currSt ", (($sock) ? "Cli pending" : "Cli Exhausted"));

$SrvSock->close;

