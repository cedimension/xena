#!/usr/bin/perl -w

use strict;
use SpreadSheet::ParseExcel;
use XML::Simple;
use XReport::DBUtil;

use Data::Dumper;

sub sqldo {
  my $sqlh = shift;
  (my $sql = shift) =~ s/'(\d+)'/$1/g;
  $sql =~ s/'\$\$NULL\$\$'/NULL/g;
  debug2log(substr("SQL: $sql", 0, 150));
  my $result;
  eval {$result = $sqlh->Execute($sql); };
  if ($@) {
  	logIt("Error detected for ", join('::', caller())) if $@;
  	die "$@\n" if $@ && $@ !~ /^caution|Checking identity/i;
  }
  logIt("Error count detected for " . join('::', caller()) . " :" 
  		. $sqlh->Errors()->Count(). "\n" . $sqlh->Errors(0)->Description()) if  $sqlh->Errors()->Count() > 0;
  if ( $sqlh->Errors()->Count() > 0 && (my $errdesc = $sqlh->Errors(0)->Description()) !~ /^caution|Checking identity/i ) {
  	die "DB Error detected for ", join('::', caller()), ": $errdesc";
  }
  return $result; 
}

sub restoreLastConfig {
  my $xlscfg = XML::Simple::XMLin(
				  "$main::Application->{XREPORT_SITECONF}\\xml\\xlsconfig.xml",
				  ForceArray => [qw(initrows)],
				  KeyAttr    => { tables => '+name', cols => '+name', columns => '+name', sheets => '+name', books => '+name' },
				  ContentKey => '-content'
				 );
  
  foreach my $tblname ( keys %{ $xlscfg->{xrdb}->{tables} } ) {
    logIt( "start to init table $tblname" );
    my ($initrows, $tbcolumns) = map { delete($xlscfg->{xrdb}->{tables}->{$tblname}->{$_}) } qw(initrows columns);
    my $keycol = ( map { $_->{name} } grep { $_->{iskey} } values %{ $tbcolumns } )[0];
    my $columns = { map { $_ => { %{ $tbcolumns->{$_} }, %{ $xlscfg->{dictionary}->{cols}->{$_} } } } keys %$tbcolumns };
    my $rowcols = [ keys %{$columns} ];
    
    @{$xlscfg->{xrdb}->{tables}->{$tblname}}{qw(name keycol columns rowcols)} 
      = ($tblname, $keycol, $columns, $rowcols);
        
  }

  my $lclcfg = { numversion => '1', 
		 lastversion_suffix    => '_LAST',
		 prevversion_sfxmask   => '_V%02D',
		 temp_suffix           => '_$$TMP$$',
		 backup => [ qw(JobReportNames
				ReportNames
				Os390PrintParameters
				NamedReportsGroups
				VarSetsValues
				Folders
				FoldersRules
				Profiles
				ProfilesFoldersTrees
				Users
				UserAliases
				UsersProfiles) ]
	       };
  foreach my $cfgvar ( keys %$lclcfg ) {
    $lclcfg->{$cfgvar} = $xlscfg->{spreadsheet}->{$cfgvar} if exists($xlscfg->{spreadsheet}->{$cfgvar});
  }
  
  $lclcfg->{backup} = [ split(/\s+/, $lclcfg->{backup}) ] unless ref($lclcfg->{backup});
  
  my ($verlast, $sfxlast, $sfxtemp, $sfxmask, $tbls_to_backup) 
    = @{$lclcfg}{qw(numversion lastversion_suffix temp_suffix prevversion_sfxmask backup)}; 
  
  my $dbc = XReport::DBUtil::getHandle('XREPORT');
  $dbc->CONNECT() if !$dbc->CONNECTED();
  my $sqlh = $dbc->{'dbc'};
  my $rs = sqldo($sqlh, 
		 "SELECT count(*) as numtables from sysobjects where name in('" 
		 . join("', '", map { "tbl_$_${sfxlast}" } @$tbls_to_backup) 
		 . "') and type = 'U'");

  my $numtables = $rs->Fields()->Item('numtables')->Value();
  die "Number of \"_LAST\" tables does not match with number of tables to restore - process aborted"
    unless $numtables eq scalar(@$tbls_to_backup);
  
  foreach my $table ( @$tbls_to_backup ) {
    my $lasttab = "dbo.tbl_${table}${sfxlast}";
    my ($clear, $reseed, $cols) = @{$xlscfg->{xrdb}->{tables}->{$table}}{qw(clear reseed rowcols)};
    $clear = 'TRUNCATE' unless $clear;
    logIt( "start to process table $table (CLEAR: $clear) -  COLUMNS: ", join('::', @$cols) );
    sqldo($sqlh,  ( $clear && $clear eq 'DELETE' ? "DELETE FROM " : "TRUNCATE TABLE " ) . "dbo.tbl_$table" );
    sqldo($sqlh, "DBCC CHECKIDENT (tbl_$table, RESEED, $reseed)") if $reseed ;
    sqldo($sqlh, "INSERT INTO dbo.tbl_$table (" . join( ',', @$cols ) . ") "
	  . " SELECT " . join( ', ', @$cols ) . " FROM $lasttab"
	 );
    logIt( "restore ended for table $table (CLEAR: $clear) -  COLUMNS: ", join('::', @$cols) );
  }	


  return $xlscfg;
}

1;
