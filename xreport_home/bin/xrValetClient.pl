use IO::Socket;
use IO::Select;
use File::Temp;
use Win32API::File qw(:ALL);

my ($msgmax, $CliFno) = @ARGV[0,1];
print "Now Entering CLIENT $0 - PARMS: ", join('::', @ARGV), "\n";
my $Sock = new IO::Socket::INET;

OsFHandleOpen($Sock, $CliFno, "rw") or die("CliSock OPEN ERROR <$!>\n");
setsockopt($Sock, SOL_SOCKET, SO_RCVBUF, pack("l", $msgmax))   || die "Setsockopt error: $!";

($REQ::port, $REQ::iaddr) = sockaddr_in(getpeername($Sock));

$REQ::peer = inet_ntoa($REQ::iaddr);

print "Serving connection $con from $REQ::peer at $REQ::port - maxblks $msgmax\n";
$REQ::InEof = 0;
my $pdffh = File::Temp->new( SUFFIX => '.pdf', TEMPLATE => "PH".$$."XXXX", OPEN => 1, UNLINK => 1 )
        || die "Unable to open temp pdf File";
binmode $pdffh;
my ($pdfname, $prtlen, $prtparms, $printername) = ($pdffh->filename(), 0, '', '');

my $sel = new IO::Select( $Sock );

if (! $sel->can_read(240)) {
    die "SEL1 Connection timed out during cntl pkt receive";
} 
else {
    
    my $blen = sysread($Sock, my $buff, $msgmax);
    die "No Data received from requester at $REQ::peer" if (!defined($buff) || !$blen);
    die sprintf("Data stream with invalid Header detected - cmdid: %s prtname: %s buff ll: %d", 
	unpack("H2 H40", $buff), length($buff) ) unless $buff =~  /^STRM2PRT/;
 
    (my $cmdid, my $fsize, $printername, $prtparms, $buff ) = unpack("a8 N n/a* n/a* a*", $buff);
    my $errmsg = ( !$printername ? " Null PrtName -" : '' )
               . (!$fsize ? " Zero Length Data -" : '')
               . '';
    die "Stream Error: - ".$errmsg if $errmsg;
    print "writing into file \"$pdfname\" to be printed to $printername\n";

    $blen = length($buff);
    if (!$blen) {
      die "SEL2 Connection timed out during receive" if (! $sel->can_read(15) );
      $blen = sysread($Sock, $buff, $msgmax);
    }
    die "No Print Data received ($fsize expected) from requester at $REQ::peer" if !$blen;
    

    while ( $blen ) {
	print $pdffh $buff;
	$prtlen += length($buff); 
        last if !$sel->can_read(240);
        $blen = sysread($Sock, $buff, $msgmax);
    }
    die "Unmatched size of Print Data - $fsize expected - $prtlen received" unless $fsize == $prtlen;
    $pdffh->close();
}

my $gsparms = join(' ', qw(-dNoCancel -dSAFER -dBATCH -dNOPAUSE -sDEVICE=mswinpr2 -r600), 
		   split(/\t/, $prtparms), 
#		   "-I\"c:\\Program Files\\gs\\gs8.64\\Resource\"", 
#		   "-I\"c:\\Program Files\\gs\\fonts\"", 
		   "-I\"c:\\Program Files\\gs\\gs8.64\\lib\"", 
		   "-sFONTPATH=\"C:\\WINDOWS\\FONTS\"", 
		   "-sOutputFile=\"\%printer\%$printername\"", 
		   "\"$pdfname\""); 
print "calling gs with parms=\"$gsparms\"\n";
system "\"c:\\Program Files\\gs\\gs8.64\\bin\\gswin32c.exe\" $gsparms";

print $Sock pack("a8 N", 'PRTDSTRM', $prtlen);
  
shutdown($Sock,2);
close($Sock);
