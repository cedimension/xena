use lib "$ENV{XREPORT_HOME}/perllib";
use strict "vars";

use POSIX qw(strftime);
use FileHandle;
use File::Basename;

use File::Path;
use File::Copy;
use XML::Simple;
use Symbol;

use Data::Dumper;

use XReport;
use XReport::Util;
use XReport::QUtil;
use XReport::Logger;
use XReport::DBUtil;
use XReport::JobREPORT;

(my $workdir = ($ARGV[0] eq '.' ? `cd` : $ARGV[0])) =~ s/\n$//gs;

my $JRID = $ARGV[1] || die "Unable to get JobReportId\n";
my $JOBID = "XREPORT.REQUEST.$JRID";
my $rptconf_in = "$ENV{XREPORT_SITECONF}\\userlib\\rptconf\\xrReqCatiaDraw.xml";
my $FQcfgfName = $workdir.'/xrReqCatiaDraw.xml';

my $debgRtn = sub { warn map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n"; };
$logrRtn = sub { print map {$_ ? ref($_) ? Dumper($_) : $_ : 'NULL'} @_, "\n"; };

require "$ENV{XREPORT_HOME}/bin/catia_functions.pl";

if (-d $workdir ) {
	my @flist = glob $workdir.'\*'; 
	&$logrRtn( "deleting now\n", join("\n", @flist));
	unlink @flist;
}

# Read, and copy to work dir, configuration from rptconf
my $cfgfhin = new FileHandle("<".$rptconf_in) || die "Unable to access rptconf $rptconf_in\n";
my $cfgfhou = new FileHandle(">".$FQcfgfName) || die "Unable to access rptconf $FQcfgfName\n";
my $cfgtext = '';
while (<$cfgfhin>) {
	my $lout = $_;
	print $cfgfhou $lout;
	$cfgtext .= $lout;
}
close $cfgfhin;
close $cfgfhou;
push @$main::files2arc, $FQcfgfName ;

&$debgRtn("Parsing ", $FQcfgfName, " contents:\n", $cfgtext);
my $reportcfg = XMLin($cfgtext,
		      ForceArray => [ qw(defaults mappings) ], ContentKey => '-content',
		      KeyAttr => {'defaults' => 'name', 'mappings' => 'name'},
		     );

&$logrRtn( "Accessing now $JRID\n");
my ($jr, $FQfileName);
my ($fileName, $SrvName, $PeerHost, $XferMode, $tgtIN, $JobName, $JobReportName, $XferEndTime, $remoteFileName);
my ($manfqn, $ixfname);

my $dbr = dbExecute("SELECT * FROM tbl_WorkQueue where ExternalKey = $JRID");
die "unable to fetch $JRID paramenters from queued request" if $dbr->eof();

my $defaults = $reportcfg->{defaults};
my $srv_parms = $dbr->Fields()->Item('SrvParameters')->Value();
my $sparms = { split("\t", $dbr->Fields()->Item('SrvParameters')->Value()) };
$sparms->{doct} = ( $sparms->{doct} eq 'ECO/SM' ? 'ECO' : 'DIS');
$sparms->{model} =~ s/\$scale/$defaults->{paper_size}/;

my $usrreq  = { %$defaults, %$sparms };

my $usrdest = {
	Lib => $usrreq->{library} || '.',
	Line => $usrreq->{line} || '.',
	Paper => $usrreq->{paper_size} || '.'
};

my $fqdn = (split(/[\n\s]+/, `ping -n 1 -a $ENV{COMPUTERNAME}`))[2];

my $request = join(
	"\n",
	"LOGN: XREPORT REQUEST by $usrreq->{username}",
	"PARM: $usrreq->{model}",
	sprintf("MAIL: %-21s%-8s%-s", @{$usrdest}{qw(Lib Line Paper)}),
	'CMDP: '.join(
		' ',
		($usrreq->{doct}, $JOBID, @{$usrreq}{qw(line OUTPUT_TYPE)}),
		'NOPDF',
		$fqdn,
		$usrreq->{printer} || '.',
		$usrreq->{mailto} || '.'
	)
)."\n";
my $jrhndl = XReport::JobREPORT->Open($JRID);
my ($ofil, $reportname, $dbxfst, $dbxfet) = $jrhndl->getValues('LocalFileName', 'JobReportName', 'XferStartTime', 'XferEndTime');
$jrhndl->Close();

my $xferstarttime = GetDateTime();
my $remotefilename = "";
&$debgRtn("Send request to Catia");
eval {
	$remotefilename = sendCatiaRequest($request, $JRID, $workdir);
};
if($@) {
	&$debgRtn("Request failed: $@");
	&$debgRtn("XferStartTime: $xferstarttime");

	my ($curryear, $curmon, $currday, $curhour, $curmin, $cursec) = unpack("a4a2a2a2a2a2", $xferstarttime);
	$dbxfst = "$curryear/$curmon/$currday $curhour:$curmin:$cursec";

	my $req = QUpdate XReport::QUtil(
		XferStartTime => $dbxfst,
		LocalFileName => $curryear."/".$curmon.$currday."/$reportname.$xferstarttime.$JRID.DATA.TXT.gz",
		Status => 31,
		Id => $JRID
	);
	die "$@";
}
my $xferendtime = GetDateTime();

my ($curryear, $curmon, $currday, $curhour, $curmin, $cursec) = unpack("a4a2a2a2a2a2", $xferstarttime);

my $localpath = getConfValues('LocalPath')->{'L1'}."/IN";
$localpath =~ s/^file:\/\///;

if (! -e $localpath."/".$curryear."/".$curmon.$currday ) {
	mkdir $localpath."/".$curryear if (! -e $localpath."/".$curryear);
	mkdir $localpath."/".$curryear."/".$curmon.$currday;
	die "unable to create $localpath $curryear $curmon$currday dir\n" unless ( -e $localpath."/".$curryear."/".$curmon.$currday )
}

$ofil = $curryear."/".$curmon.$currday."/$reportname.$xferstarttime.$JRID.DATA.TXT.gz";
&$debgRtn("Set output file to $ofil");

copy("$remotefilename", $localpath."/".$ofil) or die("Cannot copy ".$remotefilename." to $localpath/$ofil: $!");

$dbxfst = "$curryear/$curmon/$currday $curhour:$curmin:$cursec";
($curryear, $curmon, $currday, $curhour, $curmin, $cursec) = unpack("a4a2a2a2a2a2", $xferendtime);
$dbxfet = "$curryear/$curmon/$currday $curhour:$curmin:$cursec";

my $req = QUpdate XReport::QUtil(
	XferStartTime => $dbxfst,
	XferEndTime => $dbxfet,
	Status => 16,
	TypeOfWork => 24,
	JobName => 'xrReqCatiaDraw',
	JobNumber => "XRJ$JRID",
	RemoteFileName => $remotefilename,
	LocalFileName => $curryear."/".$curmon.$currday."/$reportname.$xferstarttime.$JRID.DATA.TXT.gz",
	Id => $JRID
);

if(exists($sparms->{printer}) && $sparms->{printer} !~ /^\s*$/) {
	my $workid = XReport::Spool->AddFile(
		JobReportId => $JRID,
		TypeOfWork => 27,
		InputFileName => "NOFILE",
		PrintDest => "A2E3015",
		Status => $CD::stReceiving,
		SrvParameters => $srv_parms
	);
}