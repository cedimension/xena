#!/usr/bin/perl -w

use strict;

#use Compress::Zlib;
use IO::String;

use lib "$ENV{XREPORT_HOME}/perllib";

use Data::Dumper;

use XReport;
use XReport::JobREPORT;
use XReport::INPUT;
use XReport::INPUT::AS400;
use XReport::QUtil;

sub CheckReport {
  my $jr = shift;
  my $jrid = $jr->getValues('JobReportId');
  my $maincfg = $XReport::cfg;
  delete $maincfg->{sqlprocs};
  UPDATE: foreach my $spec ( @{$XReport::cfg->{UpdateInputStream}} ) {
      next UPDATE unless exists($spec->{actions});
    CHKVAR: while (my ($selvar, $selval) = each %$spec ) {
	next if ref($selval);
	my $jrval;
	eval { $jrval = $jr->getValues($selvar); };
	my $err = $@;
	next CHKVAR if $err || $jrval eq $selval;
	next UPDATE if $jrval && !$selval;
	my $matchre = qr/$selval/;
	next UPDATE unless $jrval =~ /$matchre/;
      }
      return $spec->{actions};
    }
  i::logit("$jrid failed check - no actions returned");
  return [];
}

sub rename2AS400var {
  my $jr = shift;
  my $parms = { @_ }; 
  return undef unless exists($parms->{as400var}) && $parms->{as400var};
  my $as400var = $parms->{as400var};
  my $input = $jr->get_INPUT_ARCHIVE();
  my $inh = $input->get_INPUT_STREAM($jr, 'data') || die "unable to open INPUT - $!";
  $inh->Open() || die "unable to open file associated to input stream - $!";

#  my $infile = gzopen($ARGV[0], "rb") 
#    or die "Cannot open $ARGV[0]: $gzerrno\n" ;
  my $fcache = '';
  while ( length($fcache) < 32000 ) {
    last if $inh->read(my $buff, 4096) < 1;
    $fcache .= $buff;
    last if length($buff) < 4096;
  }
  $inh->Close();

  my $streamfh = new IO::String($fcache);
  #sub IO::String::Open { my $self = shift; return { $self, '', 0}; };
  #sub IO::String::Close { return $_[0]->close(); }
  my $spattrs = XReport::INPUT::AS400::data_unpacker->new($streamfh); # XReport::INPUT::IZlib->new($ARGV[0]));
  
  my @keys = XReport::INPUT::AS400::reader::spoolFields();
  return undef unless grep /^$as400var$/, @keys; # JobReportName, XferRecipient
  my ($JobReportName, $XferRecipient) = $jr->getValues('JobReportName', 'XferRecipient');
  my $newName = $spattrs->getValues($as400var);
  i::logit("Renaming $JobReportName to $newName");
  $jr->ChangeJobReportValues($newName, $XferRecipient);
}

sub updateWQ {
  my $jr = shift;
  my $parms = { @_ }; 
  my $TypeOfWork = $parms->{TypeOfWork};
  my $JobReportId = $jr->getValues('JobReportId');
  my @QUpdate_parms = ( Id => $JobReportId );
  push @QUpdate_parms, ( Status => $parms->{Status} ) if exists($parms->{Status}) && $parms->{Status};
  push @QUpdate_parms, ( TypeOfWork => $parms->{TypeOfWork} ) if exists($parms->{TypeOfWork}) && $parms->{TypeOfWork};
  i::logit("Updating WorkQueue - parms: " .join('::', @QUpdate_parms));
  XReport::QUtil->QUpdate(@QUpdate_parms);
}

die "No Update section in config file - process aborted" unless exists($XReport::cfg->{UpdateInputStream});

my $jr = Open XReport::JobREPORT($ARGV[1], 1);
die "Error retrieving JobReport data for JRID $ARGV[1]" unless $jr;

my $maxcc = -1;
foreach my $action ( sort { $a->{sequence} <=> $b->{sequence} } @{ CheckReport( $jr ) } ) {
#  print Dumper($action);
  my $act_name = delete( $action->{perform} );
  next unless $act_name;
  if ( my $act_code = main->can($act_name) ) {
    my $act_rc;
    eval { $act_rc = &$act_code($jr, %$action); };
    die "$act_name for JRID $ARGV[1] failed - cc: $act_rc - $@" if $@;
    $maxcc = $act_rc if ($maxcc == -1 || $act_rc > $maxcc);
  }
}

die "No action found for JRID $ARGV[1]" if $maxcc == -1; 
i::logit("Rename Input processing completed for JRID $ARGV[1] MAXCC: $maxcc");

__END__

