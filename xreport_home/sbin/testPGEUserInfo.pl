#!perl -w

use strict;
use Data::Dumper;
use SOAP::Lite;
print SOAP::Lite->VERSION(), "\n";

my $parms = { server => 'ufr-uj.collaudo.usinet.it/XA-PRE-WS/services',
			service => 'PGEUserInfo',
			uri => 'urn://bfutil.pre.xframe.usi.it',
			method => 'getExtendPreUserInfo',
  			NS => 'PGEUI',
  			codiceTorre => "UJ", 
  			userId => $ARGV[0], 
  			aliasApplicazione => "X1N",
  			@ARGV
			};

my ( $server, $service, $method, $uri, $ns ) = map { delete $parms->{$_} } qw(server service method uri NS);	  			

my $soap = SOAP::Lite->new();
my $result = $soap
  			->proxy("http://$server/$service")
	    	->uri($uri)
  			->autotype(0)
 			->on_debug(sub{print @_, "\n"})
  			->call( "$ns\:$method" => (map { SOAP::Data
  				                    ->prefix($ns)
  				                    ->name($_ => $parms->{$_}) } keys %$parms) )
			->result()
			;

print "SOAP_RESULT: ", Dumper($result), "\n";

