#!perl -w

use strict;

$| = 1;
my $oldfh = select STDERR;
$| = 1;
select $oldfh;

sub i::warnit {
	return i::logit( @_ );
}


use XReport;
use XReport::DBUtil ();
use XReport::ARCHIVE ();
use XReport::ARCHIVE::JobREPORT (); 
use XReport::ARCHIVE::Centera ();

sub update_JobReportsCenteraClips {
	my $JRID = shift;
	my $args = {@_};
	my @cols = keys %{$args};
    XReport::DBUtil::dbExecute( "IF EXISTS ( SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JRID ) "
       . "UPDATE tbl_JobReportsCenteraClipIds set "
       . join( ', ', map { "$_ = $args->{$_}" } grep { $args->{$_} && $args->{$_} !~ /^(?:NULL|'')$/ } @cols )
       . " where JobReportId = $JRID\n"
       . "ELSE INSERT into tbl_JobReportsCenteraClipIds ( JobReportId, ".join(', ', @cols).") VALUES ("
       . join(', ', ($JRID, @{$args}{@cols})).")\n"
       ); 
#  $dbr = XReport::DBUtil::dbExecute("
#    SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId
#  ");
#
#  if ($dbr->eof()) {
#    XReport::DBUtil::dbExecute("
#      INSERT into tbl_JobReportsCenteraClipIds (
#        JobReportId, PoolRef, INFILE_ClipId, OUTFILE_ClipId, INFILE_TotalSize, OUTFILE_TotalSize
#      ) 
#      VALUES ($JobReportId, 'C1', '$clipid', '', $clipsize, NULL)
#    ");
#  }
#  else {
#    XReport::DBUtil::dbExecute("
#      UPDATE tbl_JobReportsCenteraClipIds set INFILE_ClipId = '$clipid', INFILE_TotalSize = $clipsize where JobReportId = $JobReportId
#    ");
#  }
  
}

sub Migrate_INPUT_TO_CENTERA {
  my ($jr, $TargetLocalPathId, %args) = @_; require XReport::ARCHIVE::Centera; 

  if ( !ref($jr) ) {
    require XReport::ARCHIVE::JobREPORT; 
    $jr = XReport::ARCHIVE::JobREPORT->Open($jr);
  } 
 
  my ($JobReportId, $LocalPathId) = $jr->getValues(qw(JobReportId LocalPathId_IN));

  my $dbr = XReport::DBUtil::dbExecute("SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId");
  die("THE JobReport $JobReportId HAS ALREADY THE INPUT ARCHIVE IN CENTERA")
                                                    if !$dbr->eof() && $dbr->GetFieldsValues('INFILE_ClipId') ne '';
  $dbr->Close();

  i::logit(" - BEGIN of INPUT MIGRATION TO CENTERA of JobReportId $JobReportId");

#  my $iar = XReport::ARCHIVE::get_INPUT_ARCHIVE($jr, wrapper => 1);
  my ($LocalFileName) = @{$jr}{qw(LocalFileName)};
  my $INPUT = XReport::ARCHIVE::get_INPUT_ARCHIVE($jr);
  die "Unable to acquire INPUT handler -", Data::Dumper::Dumper($jr), "\n" unless $INPUT;
#  die "missing archive handler -", Data::Dumper::Dumper($INPUT), "\n" unless $INPUT->{archiver};
#  my $TargetLocalPath = $XReport::cfg->{'LocalPath'}->{$TargetLocalPathId};
#  my ($PoolRef) = ($TargetLocalPath =~ /^\s*centera:\/\/(.*)\//);
  my ( $pool, $clip );
  $main::veryverbose && i::logit("Start to migrate \"$LocalFileName\"");
  if ($LocalFileName =~ /DATA\.TXT(?:\.gz|\.gz\.tar)?$/i) {
    my $is = $INPUT->get_INPUT_STREAM($jr, random_access => 0); 
    $is->Open();

    my $compr_method = 'stored';
    i::logit("Migrating to Centera file=$LocalFileName compr_method=$compr_method");

    $pool = XReport::ARCHIVE::Centera::FPPool->new($TargetLocalPathId) unless $pool; 
    $clip = $pool->Create_Clip($jr) unless $clip;

    my $os = $clip->AddStream($LocalFileName, compr_method => $compr_method);

    while(1) {
      $is->read(my $rec, 131072); 
      last if $rec eq ''; 
      $os->write($rec);
    }
    $os->Close();
    $is->Close(); 

  }
  else {
    for my $FileName (@{$INPUT->FileNames()}) {
      my $compr_method = ($FileName =~ /\.pdf$/i) ? 'stored' : 'deflate';

      i::logit("Migrating to Centera file=$FileName compr_method=$compr_method");
      my $is = $INPUT->LocateFile($FileName); 
      $is->Open(); 

      my $pool = XReport::ARCHIVE::Centera::FPPool->new($TargetLocalPathId) unless $pool; 
      my $clip = $pool->Create_Clip($jr) unless $clip;

      my $os = $clip->AddStream($FileName, compr_method => $compr_method);
      while(1) {
        $is->read(my $rec, 131072); 
        last if $rec eq ''; 
        $os->write($rec);
      }

      $is->Close(); $os->Close(); 
    }

  }
  $INPUT->Close();
  return undef unless $clip;
   
  my $clipid = $clip->Write(); 
  my $clipsize = $clip->GetTotalSize();
  $main::veryverbose && $clip->ListFiles();  
  update_JobReportsCenteraClips( $JobReportId
                               , PoolRef_IN => "'$TargetLocalPathId'"
                               , PoolRef => "''"
                               , LocalPathId_IN => "'$LocalPathId'"
                               , LocalPathId_OUT => "''"
                               , INFile_ClipId => "'$clipid'"
                               , INFile_TotalSize => $clipsize
                               , OUTFile_ClipId => "''"
                               , OUTFile_TotalSize => 'NULL'
                               );

  if ( $args{'deleteFile'} == 1 ) {
    my $FileName = substr($jr->getFileName(), 7);
#    my $FileName = substr($iar->getFileName(), 7);
    i::logit("DELETING FILE \"$FileName\"");
    unlink $FileName or die "DELETE ERROR for INPUT ARCHIVE \"$FileName\" $!"
  }
#  $jr->Close();
#  $iar->Close();

  i::logit(" - END of INPUT MIGRATION TO CENTERA of JobReportId $JobReportId ClipId=$clipid");
}

sub Migrate_OUTPUT_TO_CENTERA {
  my ($jr , $TargetLocalPathId, %args) = @_; require XReport::ARCHIVE::Centera;

  if ( !ref($jr) ) {
    require XReport::ARCHIVE::JobREPORT; 
    $jr = XReport::ARCHIVE::JobREPORT->Open($jr)
  }

  my ($JobReportId, $LocalPathId) = $jr->getValues(qw(JobReportId LocalPathId_OUT));

  my $dbr = XReport::DBUtil::dbExecute("SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId");
  die("THE JobReport $JobReportId HAS ALREADY THE OUTPUT ARCHIVE IN CENTERA")
                                                   if !$dbr->eof() && $dbr->GetFieldsValues('OUTFILE_ClipId') ne ''; 
  $dbr->Close();

  i::logit(" - BEGIN of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId");

  my ($LocalFileName) = @{$jr}{qw(LocalFileName)};
  my $INPUT = XReport::ARCHIVE::get_OUTPUT_ARCHIVE($jr);
  die "Unable to acquire INPUT handler -", Data::Dumper::Dumper($jr), "\n" unless $INPUT;
#  die "missing archive handler -", Data::Dumper::Dumper($INPUT), "\n" unless $INPUT->{archiver};
#  my $TargetLocalPath = $XReport::cfg->{'LocalPath'}->{$TargetLocalPathId};
#  my $iar = XReport::ARCHIVE::get_OUTPUT_ARCHIVE($jr, wrapper => 1);
#  my ($archiver, $LocalFileName ) = @{$iar}{qw(archiver LocalFileName)};
#  
#  my $TargetLocalPath = getConfValues('LocalPath')->{$TargetLocalPathId};
#  my ($PoolRef) = ($TargetLocalPath =~ /^\s*centera:\/\/(.*)\//);

  my $pool = XReport::ARCHIVE::Centera::FPPool->new($TargetLocalPathId); 
  my $clip = $pool->Create_Clip($jr);

  for my $FileName (@{$INPUT->FileNames()}) {
    if ($FileName =~ /\w+\.$JobReportId\.0\.#\d+\.log/) {
      print "todel=$FileName\n"; next; 
    }

    my $compr_method = ($FileName =~ /\.pdf$/i) ? 'stored' : 'deflate';

    i::logit("Migrating to Centera file=$FileName compr_method=$compr_method");

    my $os = $clip->AddStream($FileName, compr_method => $compr_method);

    my $is = $INPUT->LocateFile($FileName); 
    $is->Open();
    
    while(1) {
      $is->read(my $rec, 131072);
      last if $rec eq ''; 
      $os->write($rec);
    }

    $is->Close(); $os->Close(); 
  }

  my $clipid = $clip->Write(); 
  my $clipsize = $clip->GetTotalSize(); 
  $main::veryverbose && $clip->ListFiles();  

  update_JobReportsCenteraClips( $JobReportId
                               , PoolRef => "'$TargetLocalPathId'"
                               , PoolRef_IN => "''"
                               , LocalPathId_OUT => "'$LocalPathId'"
                               , LocalPathId_IN => "''"
                               , OUTFile_ClipId => "'$clipid'"
                               , OUTFile_TotalSize => $clipsize
                               , INFile_ClipId => "''"
                               , INFile_TotalSize => 'NULL'
                               );

#  $dbr = XReport::DBUtil::dbExecute("SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId");
#
#  if ($dbr->eof()) {
#    XReport::DBUtil::dbExecute("INSERT into tbl_JobReportsCenteraClipIds (
#        JobReportId, PoolRef, INFILE_ClipId, OUTFILE_ClipId, INFILE_TotalSize, OUTFILE_TotalSize
#      ) 
#      VALUES ($JobReportId, '$TargetLocalPathId', '', '$clipid', NULL, $clipsize)
#    ");
#  }
#  else {
#    XReport::DBUtil::dbExecute("UPDATE tbl_JobReportsCenteraClipIds set "
#    . "OUTFILE_ClipId = '$clipid', "
#    . "OUTFILE_TotalSize = $clipsize "
#    . "WHERE JobReportId = $JobReportId "
#    );
#  }
    
  if ( $args{'deleteFile'} == 1 ) {
    my $FileName = substr($INPUT->getFileName(), 7);
    print "DELETING FILE \"$FileName\"\n";
    unlink $FileName or die "DELETE ERROR for INPUT ARCHIVE \"$FileName\" $!"
  }
  $INPUT->Close();

  i::logit(" - END of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId ClipId=$clipid");
}

my ($lpTarget, @clauses) = grep !/^-/, @ARGV;
die "Target $ARGV[0] does not exists or it is not a centera type\n"
    unless ( exists($XReport::cfg->{LocalPath}->{$lpTarget}) 
            && $XReport::cfg->{LocalPath}->{$lpTarget} =~ /^centera:/i );
(my ($PoolRef) = $lpTarget) =~ /^\s*centera:\/\/(.*)\//;

my $dbc = XReport::DBUtil->new();
my $dbname = $dbc->get_dbname();
my $dbr = $dbc->dbExecuteForUpdate( "SELECT * FROM tbl_JobReports WHERE ("
          . join(') AND (', @clauses, "Status=18", "LocalPathId_IN <> '$lpTarget' OR LocalPathId_OUT <> '$lpTarget'").")" );

while(!$dbr->eof()) {
  my ( $JobReportId, $LocalFileName, $LocalPathId_IN, $LocalPathId_OUT ) =
                                $dbr->GetFieldsValues(qw(JobReportId LocalFileName LocalPathId_IN LocalPathId_OUT));
  my $jr = XReport::ARCHIVE::JobREPORT->Open($JobReportId);
  $jr->{retention_period} = 540; #$jr->getValues('HoldDays');
  i::logit("SELECTED: $dbname, $JobReportId, $LocalFileName, $LocalPathId_IN, $LocalPathId_OUT, $jr->{retention_period}");
  $jr->{identify} = [ Database => $dbname, JobReportId => $JobReportId, LocalFileName => $LocalFileName, ]; 
  do {
    Migrate_INPUT_TO_CENTERA($jr, $lpTarget, deleteFile => '0');
#    XReport::Archive::Util::Migrate_INPUT($JobReportId, 'C1', deleteFile => '1');
    $dbr->SetFieldsValues('LocalPathId_IN', $lpTarget); $dbr->Update();
  }
  if $LocalPathId_IN ne $lpTarget;
   
  do {
    Migrate_OUTPUT_TO_CENTERA($jr, $lpTarget, deleteFile => '0'); 
#    XReport::Archive::Util::Migrate_OUTPUT($JobReportId, 'C1', deleteFile => '1'); 
    $dbr->SetFieldsValues('LocalPathId_OUT', $lpTarget); $dbr->Update();
  }
  if $LocalPathId_OUT ne $lpTarget; 
}
continue {
  $dbr->MoveNext();
}
$dbr->Close();

__END__
my $lpTarget = $ARGV[1];
die "Target $ARGV[1] does not exists or it is not a centera type\n"
    unless ( exists($XReport::cfg->{LocalPath}->{$lpTarget}) 
            && $XReport::cfg->{LocalPath}->{$lpTarget} =~ /^centera:/i );
(my ($PoolRef) = $lpTarget) =~ /^\s*centera:\/\/(.*)/;

my $dbr = dbExecuteForUpdate( "SELECT * FROM tbl_JobReports WHERE ( JobReportid = $ARGV[0] ) and (Status=18) " 
                            . " AND not ((LocalPathId_IN = '$lpTarget') or (LocalPathId_OUT = '$lpTarget'))");
if ($dbr->eof()) {
	warn "Jobreport with ID $ARGV[0] not found or with unmatched attributes\n";
}

my ( $JobReportId, $LocalFileName, $LocalPathId_IN, $LocalPathId_OUT ) =
                   $dbr->GetFieldsValues(qw(JobReportId LocalFileName LocalPathId_IN LocalPathId_OUT));

print "$JobReportId, $LocalFileName, $LocalPathId_IN, $LocalPathId_OUT\n"; 

require XReport::Archive::JobREPORT; 
$jr = XReport::Archive::JobREPORT->Open($JobReportId)
  
my ($clipidIN, $clipidOUT);
my $dbrclips = dbExecute("SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId");

($clipidIN, $clipidOUT) = map { $dbrclips->GetFieldsValues($_) } qw(INFILE_ClipId OUTFILE_ClipId) if !$dbrclips->eof();
$dbrclips->Close();

die("THE JobReport $JobReportId is ALREADY Archived IN CENTERA") if ($clipidIN && $clipidOUT);

my $pool = XReport::Archive::Centera::FPPool->new($PoolRef); 

my $regParms = {};

my @set = ();
if (!$clipidIN && $LocalPathId_IN ne $lpTarget) {
  print localtime(), " - BEGIN of INPUT MIGRATION TO CENTERA of JobReportId $JobReportId\n";

  my $iar = XReport::ARCHIVE::get_INPUT_ARCHIVE($jr, wrapper => 1);
  my ($LocalFileName, $archiver) = @{$iar}{qw(LocalFileName archiver)};

  my $is = $archiver->get_INPUT_STREAM(); 
  $is->Open();

  my $clip = $pool->Create_Clip($jr);

  my $os = $clip->AddStream($LocalFileName, compr_method => 'stored', chunks => 'no');

  while(1) {
    $is->read(my $rec, 131072); 
    last if $rec eq ''; 
    $os->write($rec);
  }

  $is->Close(); 
  $os->Close(); 
  $regParms->{IN} = [ $clip->Write(), $clip->GetTotalSize() ];
  push @set, ("PoolRef_IN = $lpTarget", 
                    "INFILE_ClipId = '$regParms->{IN}->[0]'",
                    "INFILE_TotalSize = $regParms->{IN}->[1]"); 
  $dbr->SetFieldsValues('LocalPathId_IN', $lpTarget); 
  print localtime(), " - END of INPUT MIGRATION TO CENTERA of JobReportId $JobReportId\n";
}
   
if (!$clipidOUT && $LocalPathId_OUT ne $lpTarget) {
  print localtime(), " - BEGIN of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId\n";

  my $iar = XReport::ARCHIVE::get_OUTPUT_ARCHIVE($jr, wrapper => 1);
  my ($LocalFileName, $archiver) = @{$iar}{qw(LocalFileName archiver)};
  
  for my $FileName (@{$archiver->FileNames()}) {
    if ($FileName =~ /\w+\.$JobReportId\.0\.#\d+\.log/) {
      print "todel=$FileName\n"; next; 
    }

    my $compr_method = ($FileName =~ /\.pdf$/i) ? 'stored' : 'deflate';

    print "Migrating to Centera file=$FileName compr_method=$compr_method\n";

    my $os = $clip->AddStream($FileName, compr_method => $compr_method);

    my $is = $archiver->LocateFile($FileName); 
    $is->Open();
    
    while(1) {
      $is->read(my $rec, 131072);
      last if $rec eq ''; 
      $os->write($rec);
    }

    $is->Close(); 
    $os->Close(); 
  }
  $archiver->Close();
  $regParms->{OUT} = [ $clip->Write(), $clip->GetTotalSize() ];
  push @set, ("PoolRef = $lpTarget", 
                    "OUTFILE_ClipId = '$regParms->{OUT}->[0]'",
                    "OUTFILE_TotalSize = $regParms->{OUT}->[1]"); 
  $dbr->SetFieldsValues('LocalPathId_OUT', $lpTarget); 
  print localtime(), " - END of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId\n";
}                                                         

for ( qw(IN OUT) ) {
      if (!exists($regParms->{$_})) { 
         $regParms->{$_} = [ '', 'NULL' ]; 
      }
}
my $sql = ''
$sql   .= "IF EXISTS ( SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId ) ";
$sql   .= "UPDATE tbl_JobReportsCenteraClipIds set ".join(', ', @set)." where JobReportId = $JobReportId"
$sql   .= "ELSE INSERT into tbl_JobReportsCenteraClipIds "
$sql   .= "( JobReportId, PoolRef, INFILE_ClipId, INFILE_TotalSize, OUTFILE_ClipId, OUTFILE_TotalSize ) ";
$sql   .= sprintf("VALUES ($JobReportId, $lpTarget, '%s', %s, '%s', %s)\n", (map { @{$regParms->{$_}} } qw(IN OUT))); 
dbExecute($sql);

$dbr->Update();

if ( grep /^-dodelete$/, @ARGV ) {
    for ( qw(IN OUT) ) {
        next unless $regParms->{$_}->[0];
        my $getArchRtn = XReport::ARCHIVE->can(( $_ eq 'IN' ? 'get_INPUT_ARCHIVE' : 'get_OUTPUT_ARCHIVE'));       
        my $FileName = substr(&$getArchRtn($jr, wrapper => 1)->getFileName(), 7);
        print "DELETING FILE \"$FileName\"\n";
        unlink $FileName or die "DELETE ERROR for INPUT ARCHIVE \"$FileName\" $!"
    }
}
