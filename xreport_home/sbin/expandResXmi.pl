#use lib $ENV{'XREPORT_HOME'}."/perllib";
use POSIX;

#use Convert::EBCDIC;
use File::Basename;
#use Convert::IBM390 qw(:all);
use Symbol;
use XReport::AFP::IOStream;
use XReport::TRANSMIT::INPUT;

#$translator = new Convert::EBCDIC;

sub XpandXmit {
  my $tr = XReport::TRANSMIT::INPUT->new(DEBUG => 1); 
  
  $tr->EXPAND_LIBRARY(
		      INPUT => $ARGV[0],
		      OUTDIR => './binary',
		      DEBLOCK => 1,
		      DEBUG => './unxmit.log',
		      VERBOSE => $VERBOSE
		     );

}


sub CopyFile {
  my ($lib, $fileName) = @_;
  open(IN,"<binary/$lib/$fileName") or die("?? IN $!"); binmode(IN);
  open(PSF,">psf/$lib/$fileName") or die("?? OUT $!"); binmode(PSF);
  while(!eof(IN)) {
    $rc = read(IN, $c5A, 1);
    if ( $c5A ne "\x5A") {
      die "5A ?? ", unpack("h2", $c5A), " ?? ", tell(IN);
    }
    $rc = read(IN, $ll_, 2);
    $ll = unpack("n", $ll_)-2;
    $rc = read(IN, $l, $ll);
    #print $translator->toascii($l), "\n";
    print PSF pack("n",$ll+3), $c5A, $ll_, $l;
  }
  close(PSF);
  close(IN);
}

sub TRANSLATE_AfpFile {
  my ($fileName, $psFileName) = (shift, shift); my $OUTPUT = gensym();
  open($OUTPUT,">$psFileName") or die("OUTPUT OPEN ERROR \"$psFileName\" $!"); binmode($OUTPUT);
  print $OUTPUT join("\n", @{XReport::AFP::IOStream::ExpandResourceFile($fileName)});
  close($OUTPUT);
}

sub makepsf {
  for $lib (qw(FDEFLIB PDEFLIB OVERLIB PSEGLIB PAGELIB FORMLIB)) {
    for $fileName (glob("binary/$lib/*")) {
	  $fileName = basename($fileName);
      print "$lib $fileName\n";
	  CopyFile($lib, basename($fileName));
    }
  }
}

sub makeps {
  for $lib (qw(FDEFLIB PDEFLIB OVERLIB PSEGLIB PAGELIB FORMLIB)) {
    mkdir "ps/$lib" if !-d "ps/$lib";
    for $fileName (glob("binary/$lib/*")) {
	  $fileName = basename($fileName);
    	next if $fileName =~ /^(?:APS1IOCA|S1GIGI)$/;
      print "$lib $fileName\n";
	  TRANSLATE_AfpFile("binary/$lib/$fileName", "ps/$lib/$fileName\.ps");
    }
  }
}

XpandXmit();
#makepsf();
makeps();
