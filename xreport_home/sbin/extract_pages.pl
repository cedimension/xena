use Data::Dumper;
use XReport;
use XReport::ARCHIVE::INPUT;
use XReport::ENUMS qw(:XF_ENUMS);


use IO::Handle;
use Convert::EBCDIC;
use IO::File;
use Cwd;

sub fromEBCDIC2ASCII{
	my($TXTfile,$ASCHIIfile) = @_;
	unlink $ASCHIIfile if (-f $ASCHIIfile);
	open(my $OUThandler,">$ASCHIIfile")  or die("LOGFILE OUTPUT OPEN ERROR $ASCHIIfile rc=$!\n");
	binmode $OUThandler;
	die "input file does '$TXTfile' not exist" unless (-f $TXTfile);
	my $INhandler = new IO::File('<'.$TXTfile) || die "Unable to open \"$TXTfile\" - $!";
	my $translator = Convert::EBCDIC->new();

	my $fileEmpty =1;
	while ( !$INhandler->eof() ) {
		$fileEmpty = 0;
		$INhandler->read(my $buff, 2);
		my $lrec = unpack("n", $buff);
		$INhandler->read(my $rec, $lrec);
		print $OUThandler $translator->toascii($rec), "\r\n";
	}
	print "file $TXTfile is empty!\n" if $fileEmpty ;
	close $OUThandler;
	die "input file does '$ASCHIIfile' not exist" unless (-f $ASCHIIfile);
	print "file '$ASCHIIfile' createte with success.\n" if (-f $ASCHIIfile);
}

my $myDir = cwd();
my $jobReportId;
my $frompage = 1;
my $forpages = 1;
$main::INPUTencode  = 'cp37';
$main::OUTPUTencode  = 'latin1';



#parameters check
foreach my $i (0 .. $#ARGV)
{
	if(($ARGV[$i] =~ /^\-jobReportId$/i) or ($ARGV[$i] =~ /^\-jr$/i)  )
	{ # jobReportId
		$jobReportId = $ARGV[$i+1];
		next;
	}
	if(($ARGV[$i] =~ /^\-dir$/i) or ($ARGV[$i] =~ /^\-mydir$/i)  )
	{ # jobReportId
		$myDir =  $ARGV[$i+1];
		next;
	}
	if($ARGV[$i] =~ /^\-frompage$/i)
	{ # jobReportId
		$frompage =  $ARGV[$i+1];
		next;
	}
	if($ARGV[$i] =~ /^\-forpages$/i)
	{ # jobReportId
		$forpages =  $ARGV[$i+1];
		next;
	}
	if ($ARGV[$i] =~ /^\-dd$/i)   { # veryverbose mode
		$main::debug = 1;
		$main::veryverbose = 1;
	}
	if ($ARGV[$i] =~ /^\-cutfirstchar$/i)   { # veryverbose mode
		$main::cutfirstchar = 1;
	}

	if($ARGV[$i] =~ /^\-OUTPUTencode$/i)	{ #OUTPUTencode
		$main::OUTPUTencode  =  $ARGV[$i+1];
		next;
	}
	if($ARGV[$i] =~ /^\-INPUTencode$/i)	{ #INPUTencode
		$main::INPUTencode  =  $ARGV[$i+1];
		next;
	}


}


$jobReportId || die "The -jobReportId parameter is missing\n\n";
die "The directory '$myDir' does not exist.\n\n" unless (-e $myDir);




my $output_STDOUT = $myDir."/jr_".$jobReportId."_".$frompage."_".$forpages."_STDOUT.txt";
my $output_STDERR = $myDir."/jr_".$jobReportId."_".$frompage."_".$forpages."_STDERR.txt";

open OUTPUT, '>', $output_STDOUT or die $!;
open ERROR,  '>', $output_STDERR  or die $!;

STDOUT->fdopen( \*OUTPUT, 'w' ) or die $!;
STDERR->fdopen( \*ERROR,  'w' ) or die $!;

foreach(qw(jobReportId frompage forpages myDir)) { my $name = '$'.$_; my $value = eval($name); $main::veryverbose and print "\nList of parameter: $name=[$value]\n"; }

my $readexit = sub {
    $self = $_[0];
	$main::veryverbose and print "DEBUG - cinfo" , Data::Dumper($self->{blocks_input}->{cinfo}) ;
	$self = $_[0];
	my $retstr = $_[1];
	#$retstr =~ s/([^\n\r]*)(\n|\r)$/$1\r\n/g;
	#$retstr =~ s/\x00/ /g; 
	$retstr =~ s/\x00//g; 
	return $retstr;
    #return $_[1];
};
$readexit = sub {
	my $self = $_[0];
	my $retstr = substr($_[1],1);
	$retstr =~ s/(\r?\n|\r\n?)/\r\n/g;
	$retstr =~ s/\x00/ /g;
	$main::veryverbose and print "DEBUG cinfo" , Data::Dumper($self->{blocks_input}->{cinfo}) ;
	return $retstr;
	#return substr($_[1],1);
} if($main::cutfirstchar);



my $rfilename = $myDir."/jr_".$jobReportId."_".$frompage."_".$forpages."_REP_".$main::INPUTencode."_to_".$main::OUTPUTencode .".TXT";
my $streamer = new IO::File ">$rfilename";

my $jrref = $jobReportId;
do {
	require XReport::ARCHIVE::JobREPORT; 
	$jrref = XReport::ARCHIVE::JobREPORT->Open($jrref)
} 
if !ref($jrref);
  

#my $jr = XReport::ARCHIVE::INPUT->Open($jobReportId, wrapper => 1, random_access => 1,
#  outxlate => $main::INPUTencode.':'.$main::OUTPUTencode , recexit => $readexit, change5A => 1, pagedelim => "\x31"
#);
my $jr;
my $changeFirstASAchar = 1;
if( $jrref->getValues(qw(ReportFormat)) eq XReport::ENUMS::RF_ASCII)
{
	$changeFirstASAchar = 0;
	$readexit = sub { 
		my $self = $_[0];
		$main::veryverbose and print "DEBUG - cinfo" , Data::Dumper($self->{blocks_input}->{cinfo}) ;
		my $retstr = $_[1]; 
		$retstr =~ s/\x00//g;  
		#$retstr =~ s/\x0d/\x0a/g; 
		$retstr =~ s/(\r?\n|\r\n?)/\r\n/g; 
		return $retstr;
	};
	$jr = XReport::ARCHIVE::INPUT->Open($jrref, wrapper => 1, random_access => 1
	,recexit => $readexit
	);
}
else
{
	$jr = XReport::ARCHIVE::INPUT->Open($jrref, wrapper => 1, random_access => 1,
	outxlate => $main::INPUTencode.':'.$main::OUTPUTencode , recexit => $readexit, change5A => 1, pagedelim => "\x31"
	);
}
 my $pageSep = 'AAAAAAAAAAAAAAAAAAAAAAAAAA';
my ($pages, $lines);
#eval { ($pages, $lines) = $jr->extract_pages(1, 14, tostreamer => $streamer ) };
eval { ($pages, $lines) = $jr->extract_pages($frompage, $forpages, tostreamer => $streamer, $pageSep ? (pagesep => $pageSep) : undef) };
die $@ if $@;


foreach(qw(pages lines)) { my $name = '$'.$_; my $value = eval($name); $main::veryverbose and print "\nDEBUG - List of parameter: $name=[$value]\n"; }


$main::veryverbose and  print "DEBUG - extract_pages($frompage, $forpages, tostreamer => $rfilename)\n";
close $streamer;
unless(-s $rfilename)
{
	die("Error - The file [".$rfilename."] is empty. [".(-s $rfilename)."]");
}


print Dumper("ECCO", $pages, $lines);
__END__
#
#
#
#JobReportId	ReportId	TotPages	ListOfPages
#2809518	0	1	1,1
#2809518	1	1	1,1
#2809518	2	1	1,1
#2809518	3	1	1,1
#2809519	0	1	1,1
#2809519	1	1	1,1
#2809519	2	1	1,1
#2809519	3	1	1,1
#2809520	0	1	1,1
#2809520	1	1	1,1
#2809520	2	1	1,1
#2809521	0	1	1,1
#2809521	1	1	1,1
#2809521	2	1	1,1
#2809521	3	1	1,1

#perl extract_pages.pl -jr 3267157 -frompage 1 -forpages 12
#perl extract_pages.pl -jr 2805511 -frompage 1 -forpages 1
#perl extract_pages.pl -jr 2820596 -frompage 1 -forpages 18 -dd
#perl extract_pages.pl -jr 2820596 -frompage 13666 -forpages 1 -dd
#perl extract_pages.pl -jr 909 -frompage 1 -forpages 1 -dd
#perl extract_pages.pl -jr 3502636 -frompage 1 -forpages 18 -Ncutfirstchar -Ndd
#perl extract_pages.pl -jr 3502628 -frompage 1 -forpages 15 -Ncutfirstchar -Ndd -INPUTencode cp1250 -OUTPUTencode UTF-8
#perl extract_pages.pl -jr 294637 -frompage 1 -forpages 1 -Ncutfirstchar -Ndd
#perl extract_pages.pl -jr 3812936  -frompage 172743 -forpages 1 -Ncutfirstchar -Ndd
#perl extract_pages.pl -jr 3411183  -frompage 82500 -forpages 2500 -Ncutfirstchar -Ndd
#perl extract_pages.pl -jr 3411183  -frompage 82500 -forpages 1250 -Ncutfirstchar -Ndd
#perl extract_pages.pl -jr 3411183  -frompage 84000 -forpages 1250 -Ncutfirstchar -Ndd
#perl extract_pages.pl -jr 198432  -frompage 1 -forpages 62 -Ncutfirstchar -Ndd
#perl extract_pages.pl -jr 3411183  -frompage 84000 -forpages 1250 -Ncutfirstchar -Ndd
#perl extract_pages.pl -jr 198432  -frompage 1 -forpages 62 -Ncutfirstchar -Ndd
#perl extract_pages.pl -jr 198432  -frompage 62 -forpages 1 -Ncutfirstchar -Ndd
#perl extract_pages.pl -jr 198432  -frompage 1 -forpages 62 -Ncutfirstchar -Ndd -INPUTencode cp37 -OUTPUTencode cp37
#perl extract_pages.pl -jr 198432  -frompage 1 -forpages 62 -Ncutfirstchar -Ndd -INPUTencode cp37 -OUTPUTencode cp37
#perl C:\Users\xreport\Desktop\NewFolder\prova_Extract_pages\extract_pages.pl -jr 717270  -frompage 1 -forpages 1 -Ncutfirstchar -Ndd


#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
