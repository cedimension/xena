#!perl -w

use strict;
use Data::Dumper;
use SOAP::Lite;
print SOAP::Lite->VERSION(), "\n";

my $parms = { server => 'ufr-uj.collaudo.usinet.it/XA-PGE-WS/services',
			service => 'ApplicationSecurityCheck',
			uri => 'urn://tokenservices.pge.xframe.usi.it',
			method => 'retrievePGEAuthority',
  			NS => 'PGEUI',
  			userName => $ARGV[0],
  			token => 'XD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48dG9rZW4+PHVzZXJJZD5FRTAyMTk0PC91c2VySWQ+PGV4cGlyYXRpb24+MjAwOC0wOC0wNlQxNDo1NDoxMS4wMzArMDIwMDwvZXhwaXJhdGlvbj48c2lnbmF0dXJlPlBkeW5CRVUvSTlUUE85L1BRR2Y4bmNPQXJpajV2Q2lwd1NpbFRMSjl2VGNHMkFOL0tjQXoyeEhFQStqMW9TdVhTZjlqT2FJNEZZMExSaXlQNXVOYWtsc3crL0NzL21taFhkRjMzZzJCaTErVnZGcFkxY053V0dOVERYRmE5aGlpczh1YU5GVGRaeUxVSWQ2R1FVbHB2M2JickpZbzdSWThMMGc0Nm1rN0lVMD08L3NpZ25hdHVyZT48c2lnbmVyPmNuPXVzdzJuMDk0LmVzcC5pbnRlcm5hbC51c2luZXQuaXQsb3U9c3VwcHRlY25vbG9naWNvLG89dWdpcyxsPXZlcm9uYSxzdD1pdGFseSxjPWl0PC9zaWduZXI+PC90b2tlbj4=', 
  			appCode => "X1N",
  			@ARGV
			};

my ( $server, $service, $method, $uri, $ns ) = map { delete $parms->{$_} } qw(server service method uri NS);	  			

my $soap = SOAP::Lite->new();
$main::SOAPstring = '';
my $response = $soap
  			->proxy("http://$server/$service")
	    	->uri($uri)
  			->autotype(0)
 			->on_debug(sub{$main::SOAPstring .= join('', @_). "\n";})
  			->call( "$ns\:$method" => (map { SOAP::Data
  				                    ->prefix($ns)
  				                    ->name($_ => $parms->{$_}) } keys %$parms) );
print "SOAP POST DATA:\n", $main::SOAPstring, "\nEND SOAP DATA\n";
my $result = $response->result();
$result = $response->fault() unless $result;

print "SOAP_RESULT: ", Dumper($result), "\n";

