@SETLOCAL
@set jobid=0000%RANDOM%
@set rfn=0000000000000000000000000000000000000000%3
@echo connecting to %1 to send %3 (%rfn%) as %2
@(echo bin
echo cd %2
echo quote site JOBNM=%~n0
echo quote site JOBID=JOB%jobid:~-5%
echo quote site JORIGIN=%COMPUTERNAME%
echo quote site prmod=ASCII
echo ASCII
echo quote stat
echo put %3
echo QUIT) | ftp -d -i -A %1 
@ENDLOCAL
