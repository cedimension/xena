#!/usr/bin/perl -w

package main;

use lib("$ENV{XREPORT_HOME}/perllib");

use strict;

use File::Basename;
use File::Path;
use File::Copy;
use FileHandle;

use XReport::Util;
use XReport::DBUtil;
use XReport::JobREPORT;

#TODO: use POSIX time print func
( my $script = $0 ) =~ s/.pl$//;
($script) = pop @{ [ split /[\\\/]/, $script ] };
my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) = localtime();
my $timid = sprintf( '%04d%02d%02d%02d%02d%02d', $year + 1900, $mon + 1, $mday, $hour, $min, $sec );
my $daystring = sprintf( '%04d-%02d-%02d', $year + 1900, $mon + 1, $mday );
use Win32::OLE;

#TODO: use config file to define step width
my $stepwidth = 400;       # ITEM per OGNI LOOP
my $outpath   = 'DUMP';    # NOME LOGICO della LOCAZIONE dove vengono SPOSTATI i FILE

$| = 1;
my $logfh;
my $logfnam = "${script}_${$}_${timid}_${outpath}_EXECMSGS.log";
$logfh = new FileHandle(">$logfnam");
die "unable to open logfile - $!" unless $logfh;
my $prevout = select($logfh);
$| = 1;
select $prevout;

sub logprint {
    print $logfh "$script " . localtime() . " ", @_, "\n";
    print "$script " . localtime() . " ", @_, "\n";
}

my ( $debug, $daemon, $SrvName, $RemoteDB, $RemotePath, $LocalPath ) =
  getConfValues(qw(debugLevel daemonMode SrvName RemoteDB RemotePath LocalPath));
my $default_path = $LocalPath->{'DUMP'};

#-------------------------------------------

my %AllJobReports = ();
logprint "Begin JobReport selection";
my $dbr = dbExecute( "
SELECT * FROM  dbo.tbl_JobReports
 WHERE ((PendingOp = 12)  
 AND (LocalPathId_IN = '$outpath')  
 AND (LocalPathId_OUT = '$outpath')  
 AND (SrvName LIKE 'DUMPD.\%')
 AND (ISDATE(SUBSTRING(Srvname, 7, 10)) = 1)
 AND (DATEDIFF(day, CONVERT(datetime, SUBSTRING(Srvname, 7, 10)), GETDATE()) > 30) 
 ) 
    " );

exit 0 if $dbr->eof();
my ( $jobrfnd, $filefnd ) = ( 0, 0 );
while ( !$dbr->eof ) {
    my $JRID     = $dbr->Fields()->Item('JobReportId')->Value();
    my ( $fname, $fpath, $fsufx ) = fileparse( $dbr->Fields()->Item('LocalFileName')->Value(), "DATA\.TXT\.gz" );
    $jobrfnd++;
    $filefnd += 3;

    unless ( $LocalPath->{'DUMP'} =~ /^file:\/\//i  ) {
        logprint "JobReport $JRID skipped - DUMP path definition \"$LocalPath->{'DUMP'}\" not managed";
        next;
    }

    my ($in , $ou) = map { (my $x = $LocalPath->{'DUMP'}.$_) =~ s/^file:\/\///i; $x } qw(/IN/ /OU/); 

    my $fradix = $fpath . $fname;
    $AllJobReports{$JRID} =  [ map { $in.$fradix.$_ } qw(DATA.TXT.GZ CNTRL.TXT), $ou . $fradix . 'OUT.#0.ZIP' ]

}
continue {
    $dbr->MoveNext();
}
$dbr->Close();

my ( $jobrcnt, $filecnt ) = ( 0, 0 );

logprint "delete BEGINS For ", scalar( keys %AllJobReports ), " JobReports of $jobrfnd selected";
while ( scalar( keys %AllJobReports ) ) {

    for ( 1 .. $stepwidth ) {
        last unless scalar( keys %AllJobReports );

        my ( $JRID, $FILES ) = (%AllJobReports);
        logprint "BEGIN of complete delete for $JRID";

        my $jrh = XReport::JobREPORT->Open( $JRID, 0 );
        $jrh->deleteElab();
        $jobrcnt++;

        foreach my $filesrc (@$FILES) {
            unlink $filesrc if -e $filesrc;
            $filecnt++;
        }

        dbExecute("DELETE from tbl_JobReports where JobReportId = $JRID");
        delete $AllJobReports{$JRID};

    }

}

logprint "Deleted $jobrcnt JobReports of $jobrfnd - deleted $filecnt files of $filefnd\n";
exit 0;

