setlocal EnableDelayedExpansion
echo off
SET dir_usr[A1]=W3SVC888002
SET dir_wsrv[A1]=W3SVC888901
SET dir_usr[A1TEST]=W3SVC505002
SET dir_wsrv[A1TEST]=W3SVC505901
SET dir_usr[A3]=W3SVC777002
SET dir_wsrv[A3]=W3SVC777901
SET dir_usr[A4]=W3SVC666002
SET dir_wsrv[A4]=W3SVC666901
SET dir_usr[A7]=W3SVC555002
SET dir_wsrv[A7]=W3SVC555901
SET dir_usr[C4]=W3SVC222002
SET dir_wsrv[C4]=W3SVC222901
SET dir_usr[C7]=W3SVC333002
SET dir_wsrv[C7]=W3SVC333901
SET dir_usr[SH]=W3SVC444002
SET dir_wsrv[SH]=W3SVC444901

::@GOTO RIPRISTNA_PROCESSED
::@GOTO MOVE_OLD_LOGS_TO_PROCESSED
@GOTO RUN_LOG_ENRICHED
@GOTO FINE
exit
:RIPRISTNA_PROCESSED
FOR %%z IN (A1 A1TEST A3 A4 A7 C4 C7 SH) DO (
for /l %%i in (1,1,4) do (
set dir_usr=\\C0CTLPW00%%i\d$\IISLOGS\%%z\!dir_usr[%%z]!
set dir_wsrv=\\C0CTLPW00%%i\d$\IISLOGS\%%z\!dir_wsrv[%%z]!
echo.ENV=%%z
echo.SERVER=C0CTLPW00%%i
echo.dir_usr=!dir_usr!
echo.dir_wsrv=!dir_wsrv!
for /f "tokens=1,2,*" %%a in ('dir /b "!dir_usr!\processed\*" ^| findstr.exe /i /c:".log"') DO (
move "!dir_usr!\processed\%%a" "!dir_usr!\">nul
)
for /f "tokens=1,2,*" %%a in ('dir /b "!dir_wsrv!\processed\*" ^| findstr.exe /i /c:".log"') DO (
move "!dir_wsrv!\processed\%%a" "!dir_wsrv!\" >nul
)
)
)
pause
@GOTO FINE
exit
:MOVE_OLD_LOGS_TO_PROCESSED
FOR %%z IN (A1 A1TEST A3 A4 A7 C4 C7 SH) DO (
echo.ENV=%%z
echo.dir_usr[%%z]=!dir_usr[%%z]!
echo.dir_wsrv[%%z]=!dir_usr[%%z]!
IF NOT EXIST "d:\IISLOGS\%%z\!dir_usr[%%z]!\processed" mkdir "d:\IISLOGS\%%z\!dir_usr[%%z]!\processed"
forfiles /p "d:\IISLOGS\%%z\!dir_usr[%%z]!" /m *.LOG /D -15 /C "cmd /c move @path d:\IISLOGS\%%z\!dir_usr[%%z]!\processed\" 2>nul
IF NOT EXIST "d:\IISLOGS\%%z\!dir_wsrv[%%z]!\processed" mkdir "d:\IISLOGS\%%z\!dir_wsrv[%%z]!\processed"
forfiles /p "d:\IISLOGS\%%z\!dir_wsrv[%%z]!" /m *.LOG /D -15 /C "cmd /c move @path d:\IISLOGS\%%z\!dir_wsrv[%%z]!\processed\" 2>nul
)
pause
@GOTO FINE
exit

:RUN_LOG_ENRICHED
SET perlfile=\\C0CTLPW004\d$\xena\xreport_home_A1TEST\sbin\iisLogEnricher.pl
FOR %%z IN (A1 A1TEST A3 A4 A7 C4 C7 SH) DO (
for /l %%i in (1,1,4) do (
set destdir=\\C0CTLPW00%%i\d$\TEMP\Giuseppe\new
set dir_usr=\\C0CTLPW00%%i\d$\IISLOGS\%%z\!dir_usr[%%z]!
set dir_wsrv=\\C0CTLPW00%%i\d$\IISLOGS\%%z\!dir_wsrv[%%z]!
echo.ENV=%%z
echo.SERVER=C0CTLPW00%%i
echo.dir_usr=!dir_usr!
echo.dir_wsrv=!dir_wsrv!
IF NOT EXIST "!destdir!" mkdir "!destdir!"
IF EXIST "!dir_usr!"  perl "%perlfile%" DEST=FILE:!destdir! LOGDIR=!dir_usr!  "APPLIST=XRSearch:XN%%zUsrPool,XN%%zUiPortal,esp\xreport" SERVER=C0CTLPW00%%i
IF EXIST "!dir_wsrv!" perl "%perlfile%" DEST=FILE:!destdir! LOGDIR=!dir_wsrv! "APPLIST=XReportWebIface:XN%%zAdmPool,XN%%zWebSvcs,esp\xreport"  SERVER=C0CTLPW00%%i
)
)
pause
@GOTO FINE
exit
:FINE