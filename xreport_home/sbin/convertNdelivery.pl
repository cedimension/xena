#!perl -w

use IO::File;
use Data::Dumper;

my $remotefn = $ARGV[2];
my $outmode = ($ARGV[3] || 'B');
$main::veryverbose = ($ARGV[4] || 0);

sub i::warnit {
    warn ''.localtime().' - '.join(' ', @_)."\n";
}

sub i::logit {
    warn ''.localtime().' - '.join(' ', @_)."\n";
}

my $infn = $ARGV[0];
my $oufn = $remotefn;

my $in = new IO::File("<$infn");
$in->binmode();
my $ou = new IO::File(">$oufn");
$ou->binmode();

my $chunksize = (128*1024);

my ($destType, $destAddress) = ($ARGV[1] =~ /^\s*([^\s\/\:]+):\/\/(.+)$/);
my $filename = $oufn;
i::logit("Trying to load $destType handler to deliver Bundle(".(-s $infn)." bytes) to $destAddress ($remotefn)");
my $destclass = "XReport::Deliver::".$destType;
eval "require $destclass;"; die $@ if $@;
my $destfh = $destclass->new(bxxxx => ''
                          , remote => $destAddress.'/'.$remotefn
                          , 'local' => $infn
                          , inbuffcnt => 0
                          , outmode => $outmode 
                          );
my $formatrec = $destfh->{recproc};
my $flushbuff = $destfh->{flushproc};

i::logit("starting to convert $infn (".(-s $infn)." bytes) to $oufn");
while ( !$in->eof() ) {
    my $filepos = $in->tell();
    $in->read(my $buff, $chunksize);
    my @inrows = unpack('(a2 X2 n X2 n/a)*', $buff);
    @{$destfh}{qw(outbuff lastrec)} = ('', 0);
#    i::logit("processing buffer ".++$main::bcount." at $filepos recs:".scalar(@recs));

    while ( my $nrecs = scalar(@inrows) ) {
        my ($lpfx, $rowlen, $rowdata) = splice(@inrows, 0, 3);
        if ( length($lpfx) == 1 ) {
               $in->read( my $newbuff, 1 );
               $rowlen = unpack('n', $lpfx.$newbuff);
        }
        
        if ( length($rowdata) != $rowlen ) {
        	die "incongruent len and record size len: $rowlen size: ".length($rowdata) 
        	                                                     if (scalar(@inrows) || $in->eof());
            my $len2read = $rowlen - length($rowdata);
            $in->read( my $newbuff, $len2read ) if $len2read > 0;
            $rowdata .= ($newbuff || '');
            die "incongruent len and record size after adj len: $rowlen size: ".length($rowdata)
                                                                if ( length($rowdata) != $rowlen );
        }
        $main::rcount++;
        next if ( $lpfx eq "\x00\x00");
        i::logit("writing    record $main::rcount at $filepos ($rowlen)"
                 ." recsize: ".length($rowdata)
                 ." lpfx: ".unpack('H*', $lpfx)
                 ." recs:".scalar(@inrows)) if $main::veryverbose;
        $destfh->{lastrec} = length($destfh->{outbuff});
        $destfh->{outbuff} .= &{$formatrec}($rowdata);
        die "Record too long at $filepos ($destfh->{lastrec}) RLEN: $rowlen\n"  if ((length($destfh->{outbuff}) - $destfh->{lastrec})  > 300);
        $filepos += (length($destfh->{outbuff}) - $destfh->{lastrec});
    }
    &{$flushbuff}($destfh) if $in->eof();
    print $ou $destfh->{outbuff} if length($destfh->{outbuff});
}
$in->close();
$ou->close();

i::logit("starting to transfer $oufn (".(-s $oufn)." bytes) to $destAddress");
if ( my $fulldelivery = $destfh->can('_sendfile_MVS') ) {
     &{$fulldelivery}( $destfh, 'local' => $oufn );
}
else { die "MVS FTP sendfile non supported"; }
i::logit("transfer to $destAddress completed");
