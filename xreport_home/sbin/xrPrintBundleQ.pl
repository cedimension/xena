#!perl -w

use strict;
use IO::File;

use XReport;
use XReport::DBUtil;

sub extractReportPages {
  my ($self, $rinfo, $ListOfPages) = (shift, shift, shift);
#  $main::veryverbose && i::warnit("EXTRACT PAGES PARMS: "
#                .  Dumper(\{SELF => $self, RINFO => $rinfo, LOP => $pagelist}));
  require XReport::EXTRACT;
  require XReport::PDF::DOC;

  my $query = XReport::EXTRACT->new();
  my $pagelist = [ @{$ListOfPages} ];
  
  my $pdfname = "LREP.$rinfo->{JobReportId}.$rinfo->{ReportId}.pdf";
  my $pdfFQname = $self->{workdir}."\\".$pdfname;
#  warn "extract finfo for $pdfname: ", Dumper($rinfo), "\n";
  my $streamer = new IO::File "> $pdfFQname";
  warn "Unable to open \"$pdfFQname\" - $!\n" unless $streamer;
  return undef unless $streamer;

  $query->{docTo} = XReport::PDF::DOC->Create($streamer) ;

  my $elist;
  eval { $elist = $query->ExtractPages(
                       QUERY_TYPE => 'FROMPAGES',
                       PAGE_LIST => \@$pagelist ,
                       FORMAT => "PRINT",
                       ENCRYPT => 'YES',
                       OPTIMIZE => 1,
                       TO_DIR => $self->{workdir},
                       REQUEST_ID => $pdfname,
                       #                   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"}
                      );
    };
  if ( !$elist || scalar(@$elist) == 0 ) {
  	warn "PARMS: ", Dumper(\{SELF => $self, RINFO => $rinfo, LOP => $pagelist});
    die "chiave richiesta non accessibile in Archivio - $@";
    return undef;
  }
  
  return $pdfname;
#TODO: check if and how to close extract  
}

sub mergePDFList {
    my ($self, $info, $pdflist, $outfname) = (shift, shift, shift, shift);
    return;
#    my ($lpdserver, $queue, $gsdevice) = split /:/, $parms->{prtdest};
    my $gsdevice = 'pswriter';
    use Data::Dumper;
#   warn "printPDFList parms: ", Dumper(\{finfo => $finfo, pdflist => $pdflist, prtdest => [$lpdserver, $queue, $gsdevice]});
    
    my $outFQname = $self->{workdir}."\\".$outfname;
    (my $gscmd = "\"".$XReport::cfg->{gs}->{cmd}."\" ") =~ s/\//\\/g; 

    $gscmd .= "-dBATCH -dNOPAUSE -dSAFER -dQUIET -sDEVICE=$gsdevice -sOutputFile=$outFQname "
        . join(' ', map { s/\//\\/g; $_ } @$pdflist);
    warn "GSCMD: ", $gscmd, "\n";
    system $gscmd;

}

sub printPDFList {
    my ($self, $finfo, $pdflist) = (shift, shift, shift);
#    my ($lpdserver, $queue, $gsdevice) = split /:/, $parms->{prtdest};
    my $gsdevice = 'pclwriter';
    use Data::Dumper;
#   warn "printPDFList parms: ", Dumper(\{finfo => $finfo, pdflist => $pdflist, prtdest => [$lpdserver, $queue, $gsdevice]});
    
    (my $outfname = 'Bundle.'.$finfo->{RecipientName}.'.'.$gsdevice.'.pr') =~ s/[\\\/]/_/g;
    my $outFQname = $self->{workdir}."\\".$outfname;
    (my $gscmd = "\"".$XReport::cfg->{gs}->{cmd}."\" ") =~ s/\//\\/g; 

    $gscmd .= "-dBATCH -dNOPAUSE -dSAFER -dQUIET -sDEVICE=$gsdevice -sOutputFile=$outFQname "
        . join(' ', map { s/\//\\/g; $_ } @$pdflist);
    warn "GSCMD: ", $gscmd, "\n";
    system $gscmd;

#    my $lpcmd = "\"".$main::Application->{XREPORT_HOME}."\\sbin\\lpr.exe\" -S $lpdserver -P $queue \"$outfname\"";
    my $lpcmd = "\"".$main::Application->{XREPORT_HOME}."\\sbin\\spool.exe\" \"$queue\" \"$outfname\"";
    warn "LPCMD: ", $lpcmd, "\n";
    system $lpcmd;
}

sub callGStoPDF {
    (my $outpdf = shift) =~ s/\//\\/g;
    my $cinfo = shift;
    my $cfgcode = [ split /\n/, shift ];
    my $pscode = ["%!PS"
        ,"/browse-dict { { exch (key: ) print == (val: ) print == } forall } bind def"
        ,"/textheight { gsave { 100 100 moveto (HIpg) true charpath pathbbox exch pop 3 -1 roll pop exch sub } "
        ,"  stopped { pop pop currentfont /FontMatrix get 3 get } if grestore } bind def"
        ,"/beginpage {  save /sssnap exch def currentpagedevice /PageSize get aload pop /PH exch def /PW exch def 0 PH moveto } bind def"
        ,"/endpage { sssnap restore showpage } bind def"
        ,"/stringshow {"
        ,"dup type /dicttype eq not { /intext exch def 1 dict dup /text intext put  } if"
        ,"<< /fname /Helvetica-Bold /text (BANNER) /fscale 1 /align (center) /pitch 0 >>"
        ,"copy begin"
        ,"currentpagedevice /PageSize get aload pop /PH exch def /PW exch def"
        ,"fname findfont [10 0 pitch 10 0 0] makefont setfont"
        ,"currentfont text stringwidth pop (XX) stringwidth pop add PW exch div fscale mul scalefont setfont"
        ,"/ldisp align (left) eq { 20 } { PW text stringwidth pop sub 2 div } ifelse def"
        ,"ldisp 0 textheight sub rmoveto  "
        ,"text show "
        ,"text stringwidth  pop 0 exch sub ldisp sub 0 rmoveto"
        ,"end"
        ,"} bind def"
        ,"/LabeledBox { "
        ,"/bkup save def"
        ,"<< /boxwidth { PW 40 sub } /boxlabel ( ) /boxfont [ /Helvetica-Bold 12 ] /text [[( ) ( ) ( )] [( ) ( ) ( )] [( ) ( ) ( )]] /boxpos [20 0] >>"
        ,"copy begin  "
        ,"currentdict /textfont known not { /textfont boxfont def } if"
        ,"boxpos aload pop /ybpos exch def /xbpos exch def"
        ,"textfont aload pop exch findfont exch scalefont setfont"
        ,"/xchar (XX) stringwidth pop 2 div def /txtwidth boxwidth xchar 4 mul sub def /lheight textheight def /thikness lheight cvi def"
        ,"/yinc lheight 1.5 mul cvi def  /boxheight text length yinc mul def /CardH boxheight yinc add yinc 2 div add def"
        ,"/xpos xbpos xchar 2 mul add def "
        ,"/ypos ybpos boxheight add def "
        ,"xbpos 0 CardH yinc add ybpos add sub rmoveto /ip [ currentpoint ] def"
        ,"newpath ip aload pop moveto 0 CardH rlineto boxwidth 0 rlineto 0 0 CardH sub rlineto closepath .5 setlinewidth stroke "
        ,"newpath ip aload pop moveto thikness thikness rmoveto 0 CardH thikness 2 mul sub rlineto boxwidth thikness 2 mul sub 0 rlineto 0 thikness 2 mul CardH sub rlineto closepath"
        ,"2 setlinewidth stroke"
        ,"gsave"
        ,"ip aload pop /ypos exch boxheight add def /origx exch thikness xchar add add def "
        ,"text {"
        ,"aload pop /rightstring exch def /dotstring exch def /leftstring exch def "
        ,"origx ypos moveto leftstring stringwidth pop 0 rmoveto 0 setgray"
        ,"0 dotstring stringwidth pop dup 2 div txtwidth rightstring stringwidth pop sub exch sub leftstring stringwidth pop sub "
        ,"{dotstring show} for "
        ,"origx ypos moveto 0 setgray leftstring show"
        ,"origx ypos moveto txtwidth rightstring stringwidth pop sub 0 rmoveto rightstring show "
        ,"/ypos ypos yinc sub def "
        ,"} forall"
        ,"ip aload pop CardH add moveto"
        ,"0 setgray"
        ,"boxfont aload pop exch findfont exch scalefont setfont"
        ,"boxlabel show"
        ,"grestore"
        ,"end"
        ,"bkup restore"
        ,"} bind def"
     ];
    
#   warn "cinfo: ", Dumper($cinfo), "\n";
    if ( exists($XReport::cfg->{gs}->{pageparms}) ) {
        unshift @$cfgcode, '<<'.$XReport::cfg->{gs}->{pageparms}.'>> setpagedevice' ;
    }

    push @$pscode, ( 
      map { $_ =~ s/\#\{(.+?)\}\#/$cinfo->{$1}/g; $_ =~ s/\\/\\\\/g; $_ } 
          @$cfgcode );
    
#   warn "PSCODE: ", join("\n", @$pscode), "\n";
    (my $gscmd = "\"".$XReport::cfg->{gs}->{cmd}."\" ") =~ s/\//\\/g; 
    $gscmd .= join (' ' 
        ,"-q -dBATCH -dNOPAUSE -dSAFER -dQUIET -dNoCancel"
        ,"-sDEVICE=pdfwrite"
        ,$XReport::cfg->{gs}->{pdfparms} 
        ,"-sOutputFile=$outpdf"
        ,"-"
        );
        
    warn "GSCMD: ", $gscmd, "\n";
    open(HGS, "| $gscmd") or die $!;
    foreach ( @$pscode ) {
        print HGS $_, "\n";
    }
    close HGS;

    return $outpdf;
}

sub createReportBanner {
    my ($self, $rinfo) = (shift, shift);
    print "ReportBanner: ", Dumper(\{rinfo => $rinfo});
    
    return (map { (my $oname = 'Bundle.'.$rinfo->{RecipientName}.'.'.$rinfo->{ReportName}.'.'.$_ ) =~ s/[\\\/]/_/g; 
        callGStoPDF("$self->{workdir}/$oname", $rinfo, $XReport::cfg->{ReportBannerPS} ) } ('head.pdf', 'tail.pdf')); 

}

sub createFolderBanner {
    my ($self, $finfo) = (shift, shift);

    return (map { (my $oname = 'Bundle.'.$finfo->{RecipientName}.'.'.$_ ) =~ s/[\\\/]/_/g; 
        callGStoPDF("$self->{workdir}/$oname", $finfo, $XReport::cfg->{FolderBannerPS} ) } ('head.pdf', 'tail.pdf')); 
}

$| = 1;
my $oldfh = select STDERR;
$| = 1;
select $oldfh;

sub processBundleMission {
    my ($self, $binfo) = (shift, shift);
#die  Dumper({ SELF => $self
#            , INFO => $binfo
#            });
#       
    i::logit("Bundle ". join('::', @{$binfo}{qw(BundleName BundleCategory BundleSkel)})
        . "($binfo->{BundleKey}) process begins");

    my $bname = $binfo->{BundleName};
    
    $binfo->{Status} = 4;
    $self->insertBundleEntry($binfo);

    $binfo->{SKIPPED} = [];
    my $dbr = $self->queryBundlesLogicalReports( $binfo );
    if ($dbr->eof()) {
       i::logit("No results returned from query - no reports  to be processed");
       $self->updateBundleEntry($binfo);
       return undef;
    }
    else {
      while (!$dbr->eof()) {
          my $flds = $dbr->GetFieldsHash();
          my $ListOfPages = [ $flds->{JobReportId}, split /\,/, $flds->{ListOfPages} ];
          $main::veryverbose && i::warnit("Processing $flds->{JobReportId}($flds->{JobReportName}) LOP: $flds->{ListOfPages}"
          . " PSET: " . ( defined($flds->{PERMCopies}) ? $flds->{PERMCopies} : '')
          . " BQ: $flds->{BQCopies}"
          . " PrintCopies: $flds->{PrintCopies}"
          );
          if ( defined($flds->{PrintCopies}) && $flds->{PrintCopies} == 0 ) {
              i::logit(sprintf("Recipent \"%s\" of Bundle %s will not include %d pages from page %d of JOB %s(%d/%d) as for "
                . (defined($flds->{PERMCopies}) ? " PERMANENT Specification"
                  : defined($flds->{BQCopies}) ? "REPORT CONFIGURATION ($flds->{ParseFileName})" 
                  : "REPORT Definition" ),
                           (@{$flds}{qw(RecipientName BundleName)}
                                    , @{$ListOfPages}[2, 1], @{$flds}{qw(JobName JobReportId ReportId)})));
              @{$flds}{qw(InputPages InputLines PrintedLines PrintedPages PrintStatus)} = (0, 0, 0, 0, 2);
              push @{$binfo->{SKIPPED}}, $flds;
          }
          elsif ( $flds->{outfilename} = extractReportPages($self, $flds, $ListOfPages ) ) {
              foreach my $fldn ( qw(FolderAddr SpecialInstr) ) {
                  foreach ( 1..9 ) { $flds->{$fldn.'.'.$_} = ''; }
                  my $fldc = 1;            
                  foreach my $txtpart ( split /\\n/, $flds->{$fldn} ) { $flds->{$fldn.'.'.$fldc++} = $txtpart; }
              }            
              $binfo->{folders}->{$flds->{RecipientName}} = XReport::BUNDLE::setPrintInfo( $binfo, 
                                       { InputPages => 0, InputLines => 0, reports => {} } ) 
                                           unless exists($binfo->{folders}->{$flds->{RecipientName}});
              my $uinfo = $binfo->{folders}->{$flds->{RecipientName}};

              $uinfo->{reports}->{$flds->{UserRef}} = XReport::BUNDLE::setPrintInfo( $flds,
                         XReport::BUNDLE::setPrintInfo($uinfo, { InputPages => 0, InputLines => 0, jobs => [] } ) ) unless exists($uinfo->{reports}->{$flds->{UserRef}});
              my $rinfo = $uinfo->{reports}->{$flds->{UserRef}};
              
              i::logit(sprintf("File \"%s\" (JOB $flds->{JobName} JRID %d from page %d) will add %d pages to bundle",
                       ($flds->{outfilename}, (@{$ListOfPages})[0,1,2])));
#              warn "EXTRACT PARMS: ", Dumper(\[LOP => $ListOfPages, FLDS => $flds]);
              for ( qw(Pages Lines) ) {
                  $binfo->{'Input'.$_} += $flds->{'Input'.$_};
                  $uinfo->{'Input'.$_} += $flds->{'Input'.$_};
                  $rinfo->{'Input'.$_} += $flds->{'Input'.$_};
              }           
              push @{$rinfo->{jobs}}, $flds;
          }
          else {
              @{$flds}{qw(InputPages InputLines PrintedLines PrintedPages PrintStatus)} = (0, 0, 0, 0, 15);
              push @{$binfo->{SKIPPED}}, $flds;
          }
        }
        continue { $dbr->MoveNext(); }
    }
    $dbr->Close();
    while ( scalar(@{$binfo->{SKIPPED}}) ) {
        my $skippedinfo = shift @{$binfo->{SKIPPED}}; 
#           i::logit("SKIPPED INFO:\n".Dumper($skippedinfo)) if ++$main::ct < 5;
        $self->updateQItem($skippedinfo);
    }
    
#        warn "BINFO: ", Dumper($binfo), "\n";
#    my $streamer = $self->initBundleOutput($binfo);
    
    $binfo->{Status} = 8;
    
    my @unames = sort keys %{$binfo->{folders}};
    i::logit("processing $bname Recipient list - ".scalar(@unames)." recipients");

    $binfo->{IndexArray} = [ map { map { sort {$a->{JobName} cmp $b->{JobName}}  @{$_->{jobs}} } 
                                             values %{$binfo->{folders}->{$_}->{reports}} } @unames ]; 
    if ( !scalar(@{$binfo->{IndexArray}}) ) {
        i::logit("Bundle ". join('::', @{$binfo}{qw(BundleName BundleCategory BundleSkel)})
                . "($binfo->{BundleKey}) no jobs to process - terminating ");
        $self->closeBundleLog($binfo);
        return $binfo;
    }
#    $binfo = $self->createBanner( 'BundleBanner', 
#                                    scalar(@{$binfo->{IndexArray}}) ? 
#                                               { %{$binfo->{IndexArray}->[0]}, %{$binfo} } : $binfo);
#    $binfo->{ElabPages} += $binfo->{BannerPages};
#    $binfo->{ElabLines} += $binfo->{BannerLines};
#    $main::veryverbose and i::warnit("Calling streamer (", ref($streamer), ") to write ", scalar(@{$binfo->{BundleBannerHead}}), " bundle head lines");
#    $streamer->write(join('', @{$binfo->{BundleBannerHead}} ));
    
    while ( scalar(@unames) ) {
        my $recipient = shift @unames;
        my $uinfo = delete $binfo->{folders}->{$recipient};
        $uinfo->{IndexArray} = [ map { sort {$a->{JobName} cmp $b->{JobName} }  @{$_->{jobs}} } 
                                                                          values %{$uinfo->{reports}} ];
        next unless scalar(@{$uinfo->{IndexArray}});
        
#        $uinfo = $self->createBanner( 'UserBanner',
#                                    scalar(@{$uinfo->{IndexArray}}) ? 
#                                         { %{$uinfo->{IndexArray}->[0]}, %{$uinfo} } : $uinfo);
        my ($fhead, $ftail) = createFolderBanner($uinfo);
        my @pdflist = ($fhead);
#        $binfo->{ElabPages} += $uinfo->{BannerPages};
#        $binfo->{ElabLines} += $uinfo->{BannerLines};
#        $streamer->write(join('', @{$uinfo->{UserBannerHead}} ));
        while ( my ($userref, $rinfo) = each %{$uinfo->{reports}} ) {
            next unless (ref($rinfo) eq 'HASH' && exists($rinfo->{jobs}) && ref($rinfo->{jobs}) eq 'ARRAY');
#                $main::verbose and i::warnit("rinfo: ".ref($rinfo)." REPORTS: ".ref($rinfo->{jobs})." NUM: ".scalar(@{$rinfo->{jobs}}));
#            my ($rhead, $rtail) = createReportBanner($self, $jinfo);
            my $jobs = [ sort {$a->{JobName} cmp $b->{JobName}} @{delete $rinfo->{jobs}} ];
 #           push @pdflist, $rhead;
            while ( scalar(@{$jobs})) {
                my $jinfo = shift @{$jobs};
                $jinfo = XReport::BUNDLE::setPrintInfo( $rinfo, $jinfo );
#                $jinfo = $self->createBanner( 'JobBanner', $jinfo);
                my ($jhead, $jtail) = createReportBanner($self, $jinfo);
                for ( 1..$jinfo->{PrintCopies} ) {
                	push @pdflist, ($jhead, $jinfo->{outfilename}, $jtail);
                }
                $self->updateQItem($jinfo);
            }
#            push @pdflist, $rtail;
        }
        push @pdflist, $ftail;
        printPDFList $self, $uinfo, \@pdflist;
#       $streamer->write(join('', @{$uinfo->{UserBannerTail}}))
    } 
#   $streamer->write(join('', @{$binfo->{BundleBannerTail}}));
    i::logit("Closing $binfo->{outfilename} for bundle $binfo->{BundleKey}");
#    $streamer->close();
    $self->endElab($binfo);
    
    # register begin delivery
    $binfo->{Status} = 16;
    # try to delivery bundle
    $binfo->{Status} = ($self->bundleDelivery($binfo, $self->BundleFileSfx($binfo)) ? 32 : 63);
    # register result of delivery
#    $self->closeBundleLog($binfo);
    
    i::logit("Bundle ". join('::', @{$binfo}{qw(BundleName BundleCategory BundleSkel)})
            . "($binfo->{BundleKey}) process ended");
        
}

$main::veryverbose = 1;
$main::verbose = 1;

use XReport::BUNDLE;
use Symbol;
my $bundle = XReport::BUNDLE->bundleInit();

sub i::warnit {
    warn ''.localtime().' - '.join(' ', @_)."\n";
}

my $blist = $bundle->getBundlesList();
print "Request will process ", scalar(@{$blist}), " Bundles Definitions\n";
while ( scalar( @{$blist} ) ) {
    processBundleMission($bundle, $bundle->newBundleElab(shift @{$blist}));
}
