#!/usr/bin/perl -w

use SOAP::Lite; # service => 'file:./xrIdxUpdate.wsdl';
#use SOAP::Serializer; # service => 'file:./xrIdxUpdate.wsdl';
 

use strict 'vars';
use Filehandle;
use File::Basename;
use Data::Dumper;
use XML::Simple;
use XReport::SOAP;
use LWP::UserAgent;

use constant EF_PRINT => 1;

my ($server, $service, $method, $ifname) = @ARGV[0,1, 2, 3];
my $ResourceName = (split /\./, (split /[\\\/]/, $ifname)[-1])[0];
my $IndexEntries = XMLout({'IndexEntries' => [ {Columns => [{colname => 'UserName', Value => 'ce'}]} ]}, KeepRoot => 1, NoIndent => 1);
my $NewEntries = XMLout({'NewEntries' => [ {} ] }, KeepRoot => 1, NoIndent => 1);

my $envelope = ( SOAP::Lite
		->on_action(sub{'"' . join('', @_). '"'})
		->encprefix('soapenc')
		->envprefix('soapenv')
		->autotype(0)
		->on_debug(sub{print @_, "\n"})
		->outputxml(1)
		->serializer->envelope(method => $method,
				       SOAP::Data->name('REQUEST')
				       ->attr( {IndexName => '_resources', TOP => 10, DocumentType => 'PDFFORM', FileName => $ResourceName} )
				       ->value( $IndexEntries . $NewEntries )
				      )
	       );

my ($msghdr, $msgtail) = ($envelope =~ /^(.*<\/(?:IndexEntries|NewEntries)>)(<\/REQUEST>.*)$/s);
$msghdr .= '<DocumentBody>';
$msgtail = '</DocumentBody>'.$msgtail;
print "envelope: ", length($msghdr)+length($msgtail), "\n", $msghdr.$msgtail, "\n";

use LWP::UserAgent;
my $ua = LWP::UserAgent->new;
my $URL = 'http://'.$server.'/'.$service.'/';

my $req = HTTP::Request->new(POST => $URL, HTTP::Headers->new);

$req->authorization_basic('administrator', '...........');
$req->header('SOAPAction' => $method,
	     'Accept' => ['text/xml']
	    );
$req->content_type(join '; ', 'text/xml', ''); 

my $res = $ua->request($req
		       );
print "Status Line: ", $res->status_line, "\n";
print "Content: ", $res->content, "\n";
