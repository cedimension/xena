#!/usr/bin/perl -w

use lib "$ENV{XREPORT_HOME}\\perllib";

use XReport;
use XReport::JobREPORT;
use XReport::Resources;
use Data::Dumper;

exit unless $ARGV[0] && $ARGV[1];

my ($userlib, $workdir, $SrvName) = c::getValues(qw(userlib workdir SrvName)); 
$workdir = $ARGV[1] if $ARGV[1] && -d $ARGV[1];
my $jobreport = Open XReport::JobREPORT($ARGV[2], 1);
my $timeref = $jobreport->getValues('XferStartTime');
my $resh = new XReport::Resources( destdir => "$workdir/localres"
				   ,TimeRef => $timeref
				 );

my $resname = $ARGV[0];
print "requesting  $resname ($timeref) Resource to be stored into $resh->{destdir}\n";
$resh->getResources(ResName => $resname, ResClass=>'prj');

print "requesting List\n";
my @indexEntries = $resh->listResources();
print "LIST FROM SERVER: ", Dumper(\@indexEntries);

my $inpfn = $ARGV[1];
my $inpfh = new FileHandle("<$inpfn") || die "unable to open $inpfn";
my $inpsz = -s $inpfn;
binmode $inpfh;
$inpfh->sysread(my $InpData, $inpsz);
close $inpfh;
print "File $inpfn read: $inpsz bytes - sending for store $resname Resource from filedata: ", length($InpData), " bytes\n";
my $result = $resh->storeResource(resources => [{ ResName => $resname, ResClass => 'prj', ResFileData => $InpData}]); 
print "STORE Result: ", Dumper($result);

print "requesting  $resname Resource to be stored into $resh->{destdir}\n";
$resh->getResources(ResName => $resname, ResClass=>'prj');
my $resfn = "$resh->{destdir}/$resname.prj";
my $resfh = new FileHandle("<$resfn") || die "unable to open $resfn";
my $ressz = -s $resfn;
binmode $resfh;
$resfh->sysread(my $ResData, $ressz);
close $resfh;

print "File $resfn read: $ressz bytes - sending for Execute $resname\_new Resource from filedata: ", length($ResData), " bytes\n";
(my $fname, $result) = $resh->execProject(ResName => "${resname}_new", ResClass => 'prj', ResFileData => $ResData); 
print "EXEC Result:", Dumper($result);

`start $fname`;

__END__
