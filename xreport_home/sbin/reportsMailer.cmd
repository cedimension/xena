@rem = '--*-Perl-*--
@echo off
@SETLOCAL
@set today=%date:~6,4%-%date:~3,2%-%date:~0,2%
@set this=%~n0
@set cfgn=%~n1
@SET Path=C:\Perl\site\bin;C:\Perl\bin;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Program Files (x86)\BBWin\bin;C:\Program Files\Puppet Labs\Puppet\bin;C:\strawberry\c\bin;C:\Program Files (x86)\Microsoft SQL Server\110\Tools\Binn\ManagementStudio\;C:\Program Files (x86)\Microsoft SQL Server\110\Tools\Binn\;C:\Program Files\Microsoft SQL Server\110\Tools\Binn\;C:\Program Files (x86)\Microsoft SQL Server\110\DTS\Binn\
@set XREPORT_SITECONF=C:\XREPORT_FIDITALIA\xreport_siteconf\fiditalia
@set PERL5LIB=C:\XREPORT_FIDITALIA\xreport_home\perllib
@set XREPORT_HOME=C:\XREPORT_FIDITALIA\xreport_home
@set XREPORT_WORK=C:\XREPORT_FIDITALIA\xreport_work
@perl -x -S %0 %* >>%XREPORT_WORK%\logs\%cfgn%_%today%.log 2>&1
@GOTO :EOF
@rem ';
#!perl -w 

use strict;

use Net::SMTP;
use XML::Simple;
use XReport;
use XReport::DBUtil;
use Data::Dumper;
use POSIX qw(strftime);

sub getTimeStr {
   my $tm = $_[0] || time();
   return strftime('%Y-%m-%d %H:%M:%S', localtime($tm));
}

my $cfgn = $ARGV[0];
die "File \"$cfgn\" non trovato o inaccessibile" unless ( -e $cfgn && -f $cfgn );
my $cfgh = XMLin($cfgn, ForceArray => [qw(To report)])->{mailer};

my $mailbody = '';
foreach my $rep ( @{$cfgh->{report}} ) {
  print getTimeStr()." - Processing report $rep->{ident}\n";
  my $sql = $rep->{querysql};
  warn "exec SQL: $sql" if $main::debug;
  my $dbr = dbExecute($sql);
  unless ( $dbr ) {
     warn getTimeStr()." - Error during query for $rep->{ident}";
  }
  next unless $dbr && !$dbr->eof();
  my $repbody = '';
  my @flds = $dbr->GetFieldsNames();
  while (!$dbr->eof()) {
    $repbody = join("\n", split(/[\x0d\x0a]+/,$rep->{hdr})) unless $repbody;
    my @row = $dbr->GetFieldsValues(@flds);
    $repbody .= "\n".join(' ', @row ); 
    $dbr->MoveNext();
  }
  unless ( $repbody ) {
     warn getTimeStr()." - no results from query for $rep->{ident}";
  }
  $mailbody .= "\n" if $mailbody;
  $mailbody .= $repbody;
}

$mailbody = 'Nessun risultato disponibile per il report eseguito il '.getTimeStr() unless $mailbody;

my $smtp = Net::SMTP->new($cfgh->{server}, Debug => ($main::debug || 0));
unless ( $smtp ) {
   warn getTimeStr()." - Errore durante inizializzazione SMTP";
   exit 255;
}
$smtp->mail($cfgh->{From});
if ( $smtp->to(@{$cfgh->{To}}) ) {
   print getTimeStr()." - Sending report to: ".join(',', @{$cfgh->{To}})."\n";
   $smtp->data();
   $smtp->datasend(join("\n", "Content-Type: text/plain"
                                ,( map { "$_: ".$cfgh->{$_} } qw(From Subject))
								,( map { "To: $_" } @{$cfgh->{To}} ) )
                  ."\n\n"
				  . $cfgh->{mailhdr}."\n"
				  . $mailbody
                  );				  
   $smtp->dataend();
}
else { warn getTimeStr()." - ERROR: ", $smtp->message(); }
$smtp->quit();
print getTimeStr()." - Report delivery completed\n";
exit 0;


   
   

