//EE00092H JOB (US25U0),CTD,MSGCLASS=H,CLASS=L,NOTIFY=&SYSUID           JOB23174
//*                                                                             
/*XEQ IT2A                                                                      
//* A1                                                                          
//CPY      PROC I=,O=,ODISP=                                            
//COPY     EXEC PGM=IEBCOPY,PARM='REPLACE'                              
//SYSPRINT DD   SYSOUT=*                                                
//SYSIN    DD   DUMMY                                                   
//SYSUT1   DD DISP=SHR,DSN=&I,DCB=(BLKSIZE=32008,LRECL=32000)         
//SYSUT2   DD   DSN=&O,DISP=&ODISP                                      
//         PEND                                                         
//*                                                                     
//CPYU     PROC I=,O=,ODISP=                                                    
//COPY     EXEC PGM=IEBCOPY,PARM='REPLACE'                                      
//SYSPRINT DD   SYSOUT=*                                                        
//SYSIN    DD   DUMMY                                                           
//SYSUT1   DD DISP=SHR,DSN=&I         
//SYSUT2   DD   DSN=&O,DISP=&ODISP                                              
//         PEND                                                                 
//*                                                                             
//XMITLIB  PROC LIB=                                                            
//XMIT EXEC PGM=IKJEFT01,REGION=0K,                                             
// PARM='XMIT XREPORT.RESRCE OUTDDNAME(XAFPRSCE) DDNAME(&LIB) NOLOG'                        
//*&LIB  DD DISP=(OLD,PASS),DSN=*.ALLOC.&LIB                                    
//FDEFLIB  DD DISP=(OLD,PASS),DSN=*.ALLOC.FDEFLIB                               
//PDEFLIB  DD DISP=(OLD,PASS),DSN=*.ALLOC.PDEFLIB                               
//OVERLIB  DD DISP=(OLD,PASS),DSN=*.ALLOC.OVERLIB                               
//PSEGLIB  DD DISP=(OLD,PASS),DSN=*.ALLOC.PSEGLIB                               
//FONTLIB  DD DISP=(OLD,PASS),DSN=*.ALLOC.FONTLIB                               
//IMAGLIB  DD DISP=(OLD,PASS),DSN=*.ALLOC.IMAGLIB                               
//XAFPRSCE DD DSN=*.ALLOC.XAFPRSCE,DISP=(MOD,PASS)
//SYSPRINT DD SYSOUT=*                                                  
//SYSTSPRT DD SYSOUT=*                                                  
//SYSTSIN  DD DUMMY                                                         
//*                                                                     
//         PEND                                                         
//*                                                                     
//*  ALLOCAZIONE LIBRERIE TEMPORANEE PER TRASMETTERE LE RISORSE         
//*                                                                     
//DELETE   EXEC PGM=IKJEFT01,PARM='DEL XAFPRSCE.XMIT'                           
//SYSPRINT DD SYSOUT=*                                                          
//SYSTSPRT DD SYSOUT=*                                                          
//SYSTSIN  DD DUMMY                                                             
//ALLOC    EXEC PGM=IEFBR14                                             
//FDEFLIB  DD DSN=&&FDEFLIB,DISP=(,PASS),                               
//         UNIT=SYSDA,SPACE=(TRK,(1000,500,2000))                               
//PDEFLIB  DD DSN=&&PDEFLIB,DISP=(,PASS),                               
//         UNIT=SYSDA,SPACE=(TRK,(1000,500,2000))                               
//OVERLIB  DD DSN=&&OVERLIB,DISP=(,PASS),                               
//         UNIT=SYSDA,SPACE=(TRK,(1000,500,2000))                               
//PSEGLIB  DD DSN=&&PSEGLIB,DISP=(,PASS),                               
//         UNIT=SYSDA,SPACE=(TRK,(2000,1000,4000))                              
//FONTLIB  DD DSN=&&FONTLIB,DISP=(,PASS),                                       
//         UNIT=SYSDA,SPACE=(TRK,(1000,500,2000))                               
//IMAGLIB  DD DSN=&&IMAGLIB,DISP=(,PASS),                               
//         UNIT=SYSDA,SPACE=(TRK,(1000,500,2000))                               
//XAFPRSCE DD DISP=(NEW,CATLG),DSN=EE00092.XAFPRSCE.XMIT,                       
//         UNIT=SYSDA,SPACE=(TRK,(3000,2500,4000))                              
//*                                                                     
//* DI SEGUITO LE COPIE DELLE LIBRERIE DELLE RISORSE NEI TEMP           
//* ATTENZIONE A MANTENERE LE SEQUENZE OPPORTUNE DEGLI STEP IN          
//* QUANTO I MEMBRI VENGONO RIMPIAZZATI PER NOMI UGUALI.                
//* L'ORDINE DA SEGUIRE DEVE ESSERE INVERSO A QUELLO DI                 
//* CONCATENAZIONE PER MANTENERE LO STESSO EFFETTO                      
//*                                                                     
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.OVERLIB,I=SYS4.AFPA0.OVERLIB
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.OVERLIB,I=SYS4.AFPR0.OVERLIB                  
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.OVERLIB,I=SYS4.AFPS0.OVERLIB                  
//*EXEC CPY,ODISP=(OLD),O=*.ALLOC.OVERLIB,I=SYS4.AFP.OVERLIB                    
//*                                                                     
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PSEGLIB,I=SYS4.AFPR0.PSEGLIB
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PSEGLIB,I=SYS4.AFPS0.PSEGLIB                  
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PSEGLIB,I=SYS4.AFPA0.PSEGLIB                  
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PSEGLIB,I=SYS4.AFPC0.PSEGLIB                  
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PSEGLIB,I=SYS4.AFPC0.R240.PSEGLIB             
//*                                                                     
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS4.AFP.READ.PDE3800O
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS4.AFP.READ.PDE3820O              
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS4.AFPR0.PDEFLIB                  
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS4.AFPA0.PDEFLIB                  
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS4.AFPC0.R240.PDE240O             
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS4.AFPA0.DAVR.PDEFLIB             
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS4.AFPA0.PPFA.PAGELIB             
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS4.AFPS0.XPDEFLIB                 
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS4.AFPC0.VER.PDELIB               
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS4.AFP.SDP.PDE3800O               
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS1.PDEFLIB                        
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS3.PSF.BDS.PDEFLIB                
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS3.XPDEFLIB                       
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.PDEFLIB,I=SYS4.AFP.PDEFLIB                    
//*                                                                     
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS4.AFP.READ.FDE3800O
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS4.AFP.READ.FDE3820O              
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS4.AFPR0.FDEFLIB                  
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS4.AFPA0.FDEFLIB                  
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS4.AFPC0.R240.FDE240O             
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS4.AFPA0.DAVR.FDEFLIB             
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS4.AFPA0.PPFA.FORMLIB             
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS4.AFPS0.XFDEFLIB                 
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS4.AFPC0.VER.FDELIB               
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS4.AFP.SDP.FDE3800O               
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS1.FDEFLIB                        
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS3.PSF.BDS.FDEFLIB                
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS3.XFDEFLIB                       
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FDEFLIB,I=SYS4.AFP.FDEFLIB                    
//*                                                                     
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FONTLIB,I=SYS4.AFPA0.SONORANB
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FONTLIB,I=SYS1.FONTLIBB                       
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FONTLIB,I=SYS1.FONTOLN                        
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FONTLIB,I=SYS4.AFPA0.FONT300                  
// EXEC CPY,ODISP=(OLD),O=*.ALLOC.FONTLIB,I=SYS4.AFPA0.FONTLIBB                 
//*                                                                     
// EXEC CPYU,ODISP=(OLD),O=*.ALLOC.IMAGLIB,I=SYS1.IMAGELIB                       
// EXEC CPYU,ODISP=(OLD),O=*.ALLOC.IMAGLIB,I=SYS4.IMAGELIB                       
//*                                                                     
//*                                                                     
//* DI SEGUITO LO SCARICO DELLE LIBRERIE IN FORMATO XMIT            
//* SU DATASET SEQUENZIALE. MANTENERE LO STESSO ORDINE          
//* (FDEFLIB,PDEFLIB,OVERLIB,PSEGLIB,FONTLIB)                
//*                                                                     
// EXEC XMITLIB,LIB=FDEFLIB                                                                    
// EXEC XMITLIB,LIB=PDEFLIB                                                                    
// EXEC XMITLIB,LIB=OVERLIB                                                                    
// EXEC XMITLIB,LIB=PSEGLIB                                                                    
// EXEC XMITLIB,LIB=FONTLIB                                                                    
// EXEC XMITLIB,LIB=IMAGLIB                                                                    
//*                                                                     
//* DI SEGUITO LA TRASMISSIONE DEL DATASET DELLE RISORSE            
//* AL SERVER XREPORT (PRMOD EBCDIC) (AL MOMENTO ASTERISCATO)                   
//*                                                                     
//FTP2XREP EXEC PGM=FTP,COND=EVEN,PARM='-e 10.248.162.90 21'                    
//XAFPRSCE DD   DISP=SHR,DSN=*.ALLOC.XAFPRSCE                                   
//SYSPRINT DD SYSOUT=*                                                          
//SYSIN DD *
 EE00092 PIPPETTO                                                               
QUOTE SITE JOBNM=XAFPRSCE
 QUOTE SITE JORIGIN=IT2A                                                        
QUOTE SITE JOBID=JOB99999
QUOTE SITE PRMOD=BINSTREAM
 BINARY                                                                         
CD XAFPRSCE
 PUT //DD:XAFPRSCE                                                              
 QUIT                                                                           
