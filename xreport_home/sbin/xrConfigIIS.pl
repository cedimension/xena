#! perl -w

package XReport::INSTALL::iisSetup;

use strict;
use Carp;

use Win32::OLE;

use constant MD_AUTH_ANONYMOUS => 0x00000001;   # Anonymous authentication available. 
use constant MD_AUTH_BASIC => 0x00000002;       # Basic authentication available. 
use constant MD_AUTH_NT => 0x00000004;          # Windows authentication schemes available  

use constant MD_ACCESS_READ => 0x00000001;              # Allow read access. 
use constant MD_ACCESS_WRITE => 0x00000002;             # Allow write access. 
use constant MD_ACCESS_EXECUTE => 0x00000004;           # Allow file execution (includes script permission). 
use constant MD_ACCESS_SOURCE => 0x00000010;            # Allow source access. 
use constant MD_ACCESS_SCRIPT => 0x00000200;            # Allow script execution. 
use constant MD_ACCESS_NO_REMOTE_WRITE => 0x00000400;   # Local write access only. 
use constant MD_ACCESS_NO_REMOTE_READ => 0x00001000;    # Local read access only. 
use constant MD_ACCESS_NO_REMOTE_EXECUTE => 0x00002000; # Local execution only. 
use constant MD_ACCESS_NO_REMOTE_SCRIPT => 0x00004000;  # Local host access only. 

use constant MD_DIRBROW_SHOW_DATE => 0x00000002;      # Show date. 
use constant MD_DIRBROW_SHOW_TIME => 0x00000004;      # Show time. 
use constant MD_DIRBROW_SHOW_SIZE => 0x00000008;      # Show file size. 
use constant MD_DIRBROW_SHOW_EXTENSION => 0x00000010; # Show file name extension. 
use constant MD_DIRBROW_LONG_DATE => 0x00000020;      # Show full date. 
use constant MD_DIRBROW_LOADDEFAULT => 0x40000000;    # Load default page, if it exists. 
use constant MD_DIRBROW_ENABLED => 0x80000000;        # Enable directory browsing. 

use constant MD_ISOLATED_LOW => 0x00000000;    # appl isolated low (in process)
use constant MD_ISOLATED_HIGH => 0x00000001;   # appl isolated high (out of process)
use constant MD_ISOLATED_MEDIUM => 0x00000002; # appl isolated medium (out of process pooled)


sub setAttrs {
 my ($self, $objref, %args) = @_;
# $objref = $self->{W3SVC}->GetObject('IIsWebServer', '999001');
 my $logger = $self->{logger};
 while( my ($attr, $attrval) =  each %args ) {
   warn "setting $attr to $attrval";
    $objref->Put($attr, $attrval);
#    $objref->{$attr} = $attrval;
    my $errstr = Win32::OLE::LastError();
    confess("SetAttr ERROR for $attr: $errstr") if $errstr;
  }
  $objref->SetInfo();
 
}

sub CreateAppPool {
  my ($self, %args) = @_; my $logger = $self->{'logger'};
  my %attrs = ( 
	       'WamUserName' => $self->{XRUser},
	       'WamUserPass' => $self->{XRPass},
	       'AppPoolIdentityType' => 3,
	       'PeriodicRestartTime' => 240,
	       'AppPoolAutoStart' => 1,
	       'AppPoolCommand' => 1,
	       'AppPoolState' => 2,
	       'MaxProcesses' => 5,
	       %args);
  my $PoolName = delete $attrs{Name} || $self->{PoolName};
  confess "Pool name not specified" unless $PoolName;

  my $newpool = $self->{W3SVC}
    ->GetObject('IIsApplicationPools', 'AppPools')
    ->Create('IIsApplicationPool', $PoolName) 
      or confess("CreateAppPool ERROR: ".Win32::OLE::LastError());
  $newpool->SetInfo();

  $self->setAttrs($newpool, %attrs);
  return $self;
}

sub CreateWebServer {
  my ($self, %args) = @_; my $logger = $self->{'logger'};
  my %attrs = (
	       'ServerAutoStart' => 'True',
	       'ServerState' => 2,
	       'LogFileDirectory' => $self->{LogsDir} || "$ENV{windir}\\system32\\LogFiles",
	       'LogFilePeriod' => 1,
	       'LogFileLocaltimeRollover' => 'True',
	       'LogFileTruncateSize' => 20971520,
	       %args);

  my $name = delete($attrs{Name});
  confess "Server name not specified " unless $name;

  my $webServerNum = delete $attrs{WebServerNum};

  my $PoolName = delete $attrs{AppPoolId} || $self->{PoolName};
  confess "PoolName Not Specified" unless $PoolName;

  my $defaultDoc = delete($attrs{DefaultDoc});
  my ($rootPath, $user, $pass) = map { delete($attrs{$_}) } qw(Path UNCUserName UNCPassword);
  $rootPath = $self->{XRDummy} unless $rootPath;

  $user = $self->{XRUser} unless $user;
  $pass = $self->{XRPass} unless $pass;

  my $webServer;
  if ( $webServerNum ) {
    $webServer = $self->{W3SVC}
      ->Create("IIsWebServer", "$webServerNum");
  } 
  else {
    $webServer = $self->{W3SVC}
      ->Create("IIsWebServer", undef);
  }
  confess("CreateWebServer ERROR: ".Win32::OLE::LastError()) unless $webServer;
  $webServer->SetInfo();

  $webServer->Create("IIsFilters", "filters");
  $webServer->SetInfo();

  $self->setAttrs($webServer, 
		  %attrs,
		  ServerComment => $name);

  my $vdir = $self->CreateVDIR($webServer, 'Name' => 'root',
		   'WebServerNum' => $webServerNum,
		   'Path' => $rootPath,
		   'AccessFlags' => 517,
		   'AccessExecute' => 'True',
		   'AccessSource' => 'False',
		   'AccessRead' => 'True',
		   'AccessWrite' => 'False',
		   'AccessScript' => 'True',
		   'AccessNoRemoteExecute' => 'False',
		   'AccessNoRemoteRead' => 'False',
		   'AccessNoRemoteWrite' => 'False',
		   'AccessNoRemoteScript' => 'False',
		   'AccessNoPhysicalDir' => 'False',
		   'DirBrowseFlags' => '1073741886',
		   'EnableDirBrowsing' => 'False',
		   'DirBrowseShowDate' => 'True',
		   'DirBrowseShowTime' => 'True',
		   'DirBrowseShowSize' => 'True',
		   'DirBrowseShowExtension' => 'True',
		   'DirBrowseShowLongDate' => 'True',
		   'EnableDefaultDoc' => 'True',
		   'AuthFlags' => 4,
		   'AuthBasic' => 'False',
		   'AuthAnonymous' => 'False',
		   'AuthNTLM' => 'True',
		   'AuthMD5' => 'False',
		   'AuthPassport' => 'False',
		   'ScriptMaps' => [
				    ".asa,$ENV{WINDIR}\\system32\\inetsrv\\asp.dll,5,GET,HEAD,POST,TRACE",
				    ".asax,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,5,GET,HEAD,POST,DEBUG",
				    ".ascx,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,5,GET,HEAD,POST,DEBUG",
				    ".ashx,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,1,GET,HEAD,POST,DEBUG",
				    ".asmx,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,1,GET,HEAD,POST,DEBUG",
				    ".asp,$ENV{WINDIR}\\system32\\inetsrv\\asp.dll,5,GET,HEAD,POST,TRACE",
				    ".aspl,C:\\Perl\\bin\\PerlEx30.dll,5,GET,HEAD,POST",
				    ".aspx,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,1,GET,HEAD,POST,DEBUG",
				    ".axd,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,1,GET,HEAD,POST,DEBUG",
				    ".cdx,$ENV{WINDIR}\\system32\\inetsrv\\asp.dll,5,GET,HEAD,POST,TRACE",
				    ".cer,$ENV{WINDIR}\\system32\\inetsrv\\asp.dll,5,GET,HEAD,POST,TRACE",
				    ".config,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,5,GET,HEAD,POST,DEBUG",
				    ".cs,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,5,GET,HEAD,POST,DEBUG",
				    ".csproj,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,5,GET,HEAD,POST,DEBUG",
				    ".idc,$ENV{WINDIR}\\system32\\inetsrv\\httpodbc.dll,5,GET,POST",
				    ".licx,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,5,GET,HEAD,POST,DEBUG",
				    ".pl,C:\\Perl\\bin\\perlis.dll,5,GET,HEAD,POST",
				    ".plex,C:\\Perl\\bin\\PerlEx30.dll,5,GET,HEAD,POST",
				    ".plx,C:\\Perl\\bin\\perlis.dll,5,GET,HEAD,POST",
				    ".rem,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,1,GET,HEAD,POST,DEBUG",
				    ".resources,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,5,GET,HEAD,POST,DEBUG",
				    ".resx,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,5,GET,HEAD,POST,DEBUG",
				    ".shtm,$ENV{WINDIR}\\system32\\inetsrv\\ssinc.dll,5,GET,POST",
				    ".shtml,$ENV{WINDIR}\\system32\\inetsrv\\ssinc.dll,5,GET,POST",
				    ".soap,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,1,GET,HEAD,POST,DEBUG",
				    ".stm,$ENV{WINDIR}\\system32\\inetsrv\\ssinc.dll,5,GET,POST",
				    ".vb,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,5,GET,HEAD,POST,DEBUG",
				    ".vbproj,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,5,GET,HEAD,POST,DEBUG",
				    ".vsdisco,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,1,GET,HEAD,POST,DEBUG",
				    ".webinfo,$ENV{WINDIR}\\Microsoft.NET\\Framework\\v2.0.50727\\aspnet_isapi.dll,5,GET,HEAD,POST,DEBUG",
				   ],
		    'AppPoolId' => $PoolName,
		  );

  $self->setAttrs($vdir, 'DefaultDoc' => $defaultDoc, 'EnableDefaultDoc' => 'True') if $defaultDoc;
  $self->setAttrs($vdir, 'UNCUserName' => $user, 'UNCPassword' => $pass ) if $user;

  return $webServer;
}

sub  CreateVDIR {
  my ($self, $webServer, %args) = @_;
  my $logger = $self->{'logger'};
  my %attrs = (
	       'UNCUserName' => $self->{XRUser},
	       'UNCPassword' => $self->{XRPass},
	       'AccessFlags' => 517,
	       'AccessExecute' => 'True',
	       'AccessSource' => 'False',
	       'AccessRead' => 'True',
	       'AccessWrite' => 'False',
	       'AccessScript' => 'True',
	       'AccessNoRemoteExecute' => 'False',
	       'AccessNoRemoteRead' => 'False',
	       'AccessNoRemoteWrite' => 'False',
	       'AccessNoRemoteScript' => 'False',
	       'AccessNoPhysicalDir' => 'False',
	       'DirBrowseFlags' => 62,
	       'EnableDirBrowsing' => 'False',
	       'DirBrowseShowDate' => 'True',
	       'DirBrowseShowTime' => 'True',
	       'DirBrowseShowSize' => 'True',
	       'DirBrowseShowExtension' => 'True',
	       'DirBrowseShowLongDate' => 'True',
	       'EnableDefaultDoc' => 'True',
	       'AuthFlags' => 4,
	       'AuthBasic' => 'False',
	       'AuthAnonymous' => 'False',
	       'AuthNTLM' => 'True',
	       'AuthMD5' => 'False',
	       'AuthPassport' => 'False',
	       %args);

  my ($webServerNum, $name, $PoolName, $defaultDoc, $enableDefDoc)
    = map { delete $attrs{$_} } 
	      qw(WebServerNum Name AppPoolId DefaultDoc EnableDefaultDoc);
  $PoolName = $self->{PoolName} unless $PoolName;
  confess "PoolName Not Specified" unless $PoolName;

  my ($rootPath, $user, $pass) = map { delete($attrs{$_}) } qw(Path UNCUserName UNCPassword);
  $rootPath = $self->{XRDummy} unless $rootPath;

  $user = $self->{XRUser} unless $user;
  $pass = $self->{XRPass} unless $pass;

  my ($vdir, $root, $vdirname) = ($webServer->GetObject("IIsWebVirtualDir", 'root'), $webServer, 'root');
  
  my @branches = ($name =~ /^root$/i ? () : split /[\\\/]/, $name);
  while ( @branches ) {
    $vdirname = shift @branches;
    $root = $vdir;
    $vdir = $root->GetObject("IIsWebVirtualDir", $vdirname);
    confess("Createvdir ERROR: ".Win32::OLE::LastError()) if !$vdir && @branches;
  }

  if ( $vdir ) {
    $vdir->AppDeleteRecursive();
    $root->delete("IIsWebVirtualDir", $vdirname);
  }
 
  $vdir = $root->Create("IIsWebVirtualDir", $vdirname)
    or confess("Createvdir ERROR: ".Win32::OLE::LastError());
  $root->SetInfo();

  $attrs{AppFriendlyName} = $vdirname;
  $attrs{AppFriendlyName} = $root->{ServerComment} if $root->{KeyType} eq 'IIsWebServer';
  $attrs{AppRoot} = $root->{AppRoot}."/$vdirname" if $root->{KeyType} eq 'IIsWebVirtualDir';
  $attrs{AppRoot} = "/LM/W3SVC/$webServerNum/Root" if $root->{KeyType} eq 'IIsWebServer';

  $self->setAttrs($vdir, 
		  %attrs,
		  'AppIsolated' => 1,
		  'Path' => $rootPath,
		 );

  $vdir->Invoke("AppCreate3",  0, ($PoolName || "XreportPool"), 1 ); 

  $self->setAttrs($vdir, 
	       'AppIsolated' => 2,
		 );
  $self->setAttrs($vdir, 'DefaultDoc' => $defaultDoc) if $defaultDoc;
  $self->setAttrs($vdir , 'EnableDefaultDoc' => ($defaultDoc ? 'True' : 'False'));
  $self->setAttrs($vdir, 'UNCUserName' => $user, 'UNCPassword' => $pass ) if $user;

  return $vdir;
}

sub new {
  my $className = shift; 
  my %args = ( @_ );
  my $targetServer = delete($args{ServerName}) || 'localhost';
  my $w3svc = Win32::OLE->GetObject("IIS://$targetServer/W3SVC")
    or confess("Get Object W3SVC ERROR: ".Win32::OLE::LastError());
  
  bless { W3SVC => $w3svc,
	 ,%args
	}, $className;
}

1;

package main;

use strict;

my $target = $ARGV[0] || 'localhost';

my $IIS = new  XReport::INSTALL::iisSetup( ServerName => $target,
					   LogsDir => "C:\\WebLogs",
					   XRUser => "ACCS4169\\Administrator",
					   XRPass => "Agusta.it",
					   XRDummy => "\\\\ACCS4169\\X-Report_siteconf\\webroot",
					   PoolName => 'XReportPool',
					 );

$IIS->CreateAppPool();

my $webServer = $IIS->CreateWebServer('Name' => "XRServices",
				      'ServerBindings' => [qw(:60080:)],
				      'SecureBindings' => [qw(:60443:)],
				      'WebServerNum' => 999001,
				     );

$IIS->CreateVDIR($webServer, 'Name' => 'ConfigServer',
		 'Path' => "\\\\ACCS4169\\X-Report\\xrsitiweb\\CeAdminWeb\\IISWebServ",
		 'DefaultDoc' => 'xrIISWebServ.asp',
		 'AnonymousUserName' => $IIS->{XRUser},
		 'AnonymousUserPass' => $IIS->{XRPass},
		 'AuthFlags' => 5,
		 'AuthBasic' => 'False',
		 'AuthAnonymous' => 'True',
		 'AuthNTLM' => 'True',
		 'AuthMD5' => 'False',
		 'AuthPassport' => 'False',
		);

$IIS->CreateVDIR($webServer, 'Name' => 'ResourceServer',
		 'Path' => "\\\\ACCS4169\\X-Report\\xrsitiweb\\CeAdminWeb\\IISWebServ",
		 'DefaultDoc' => 'xrIISWebServ.asp',
		 'AnonymousUserName' => $IIS->{XRUser},
		 'AnonymousUserPass' => $IIS->{XRPass},
		 'AuthFlags' => 5,
		 'AuthBasic' => 'False',
		 'AuthAnonymous' => 'True',
		 'AuthNTLM' => 'True',
		 'AuthMD5' => 'False',
		 'AuthPassport' => 'False',
		);

$IIS->CreateVDIR($webServer, 'Name' => 'xrPrintIface',
		 'Path' => "\\\\ACCS4169\\X-Report\\xrsitiweb\\CeAdminWeb\\IISWebServ",
		 'DefaultDoc' => 'xrIISWebServ.asp',
		 'AnonymousUserName' => $IIS->{XRUser},
		 'AnonymousUserPass' => $IIS->{XRPass},
		 'AuthFlags' => 5,
		 'AuthBasic' => 'False',
		 'AuthAnonymous' => 'True',
		 'AuthNTLM' => 'True',
		 'AuthMD5' => 'False',
		 'AuthPassport' => 'False',
		);

$IIS->CreateVDIR($webServer, 'Name' => 'CatiaIface',
		 'Path' => "\\\\ACCS4169\\X-Report\\xrsitiweb\\CeAdminWeb\\IISWebServ",
		 'DefaultDoc' => 'xrIISWebServ.asp',
		 'AnonymousUserName' => $IIS->{XRUser},
		 'AnonymousUserPass' => $IIS->{XRPass},
		 'AuthFlags' => 5,
		 'AuthBasic' => 'False',
		 'AuthAnonymous' => 'True',
		 'AuthNTLM' => 'True',
		 'AuthMD5' => 'False',
		 'AuthPassport' => 'False',
		);


my $adminServer = $IIS->CreateWebServer('Name' => "XRAdmin",
				      'ServerBindings' => [qw(:8080:)],
				      'SecureBindings' => [qw(:8443:)],
				      'WebServerNum' => 999002,
				     );

$IIS->CreateVDIR($adminServer, 'Name' => 'oper',
		 'Path' => "\\\\ACCS4169\\X-Report\\xrsitiweb\\CeAdminWeb\\wwwoper",
		 'DefaultDoc' => 'OperFrame.html',
		);

$IIS->CreateVDIR($adminServer, 'Name' => 'defin',
		 'Path' => "\\\\ACCS4169\\X-Report\\xrsitiweb\\CeAdminWeb\\wwwdefin",
		 'DefaultDoc' => 'FrameMain.html',
		);


my $userServer = $IIS->CreateWebServer('Name' => "XReportUser",
				      'ServerBindings' => [qw(:80:)],
				      'SecureBindings' => [qw(:443:)],
				      'WebServerNum' => 999003,
				      'Path' => "\\\\ACCS4169\\X-Report\\xrsitiweb\\CeUserWeb\\wwwroot",
				      'DefaultDoc' => 'FrameUser.html',
				     );

my $userServer = $IIS->CreateWebServer('Name' => "XReportUser",
				      'ServerBindings' => [qw(:8081:)],
				      'SecureBindings' => [qw(:81443:)],
				      'WebServerNum' => 999004,
				      'Path' => "\\\\ACCS4169\\X-Report\\user_website\\CatiaIfaceClient",
				      'DefaultDoc' => 'index.html',
				     );


