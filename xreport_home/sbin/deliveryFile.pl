#!perl -w

use IO::File;
my $remotefn = $ARGV[2];
my $outmode = ($ARGV[3] || 'B');
sub i::warnit {
    warn ''.localtime().' - '.join(' ', @_)."\n";
}

sub i::logit {
    warn ''.localtime().' - '.join(' ', @_)."\n";
}

$main::veryverbose = ($ARGV[4] || 0);
my $starttime = time();
    my ($destType, $destAddress) = ($ARGV[1] =~ /^\s*([^\s\/\:]+):\/\/(.+)$/);
    my $filename = $ARGV[0];
    i::logit("Trying to load $destType handler to deliver Bundle(".(-s $filename)." bytes) to $destAddress ($remotefn)");
    my $destclass = "XReport::Deliver::".$destType;
    my $destfh;
    eval "require $destclass;"; die $@ if $@;
    
        my $bundlefh = new IO::File("<$filename") 
                              || die "Unable to open \"$filename\" - $!";

        my $chunksize = (127*1024);    
        $destfh = $destclass->open( bxxx => ''
#                                , encoding => 'E'
                                , outmode => $outmode
                                , remote => $destAddress.'/'.$remotefn
                                , 'local' => $filename
                                , inputfh => $bundlefh
                                , inbuffcnt => 0
                                , chunksize => $chunksize
                                 );
        my $buffcnt = 0;                                
        while ( !$bundlefh->eof() ) {
#           $main::veryverbose and i::warnit("Writing now buffer ".(++$buffcnt)." of ".$filename." at ", $bundlefh->tell());
           $bundlefh->read(my $buff, $chunksize);
           $destfh->write($buff) if length($buff);
          last if $destfh->{numrecs} > 22320 && !length($destfh->{inbuff}) && $main::veryverbose > 3;
        }
        $main::veryverbose and i::warnit("Closing now Out Dest - buffer read: ".(++$buffcnt)
                                         ." last Pos: ". $bundlefh->tell()
                                         );
        
        $destfh->close();
        $bundlefh->close();
        my $xfertime = (time() - $starttime);
        my $xferspeed = 0;
        my $xferspeed = ($destfh->{XferOutBytes} / $xfertime) / 1024; 
    i::logit("Xfer terminated - ".$destfh->{XferOutBytes}." sent in $xfertime Seconds $xferspeed Kbytes/sec");