#!/usr/bin/perl -w

use strict;

use Net::FTP;
use Net::Telnet ();
use IO::String;

my $catia_addr = "catiamf.agusta.it";
my $catia_path = "/cat_user/plot/mvs2aix/xreport";
my $pdfserv = "ACCS4109";
my $JOBID = "XREPORT.REQUEST.$$";
my $catia_user = 'plot';
my $catia_pass = 'plot';

(my $pstr = join(' ', @ARGV)) =~ s/ *, */,/g; 
my $sparms = { map { my @p = split(/ *= */, $_);(uc($p[0]), $p[1]) } grep /=/, split(/,/, $pstr) };


my $usrinfo = { mvsuser => 'A96467'
		, first => 'Diego'
		, last => 'Masini'
		, dept  => 'AMB'
		, teln  => '0000'
	      };

my $usrreq  = { PN => 'EA6301B508'       # Part Number
		, FOGLIO => '22'         # Sheet Num
		, ESP => '12-'           # Issue
		, CARTA => 'A4'          # Paper Format to be printed
		, STATO => 'R'           # blueprint status (RELEASED/R - PRE-RELEASED/PR)
		, TIPO => 'DIS'          # blueprint type (DIS: 2D ECO: no 2D)
		, LINEA   => 'EH101'     # blueprint line 
		, FORMATO  => 'GL2'      # Ouput type (CGM/GL2/VRF)
                , GRUPPO => 'HOLD_EH101' # library
		, %$sparms               # call arguments override
	      };


my $usrdest = { lib => $usrreq->{GRUPPO} || '.'
		, line => $usrreq->{LINEA} || '.'
		, paper => $usrreq->{CARTA} || '.'
	      };

my $fqdn = (split(/[\n\s]+/, `ping -n 1 -a $ENV{COMPUTERNAME}`))[2];

my $request = join("\n"
                   , "LOGN: XREPORT REQUEST 12345"
                   , sprintf("PARM: %-13s%-3s%-4s%-9s%-s", 
		                 @{$usrreq}{qw(PN FOGLIO ESP CARTA STATO)})
                   , sprintf("MAIL: %-21s%-8s%-s",
		                 @{$usrdest}{qw(lib line paper)})
                   , 'CMDP: '.join(' '
                                   , ($usrreq->{TIPO}
				   , $JOBID
				   , @{$usrreq}{qw(LINEA FORMATO)})
                                   , (grep(/nopdf/i, @ARGV) ? 'NOPDF' : $usrdest->{paper})
                                   , $fqdn
                                   , $usrreq->{DEST} || '.'
                                   , $usrreq->{MAILTO} || '.'
			          )
                   )."\n";

#use Data::Dumper;
print 
#      Dumper($sparms), "\n",
#      Dumper($usrreq), "\n",
      $request;

my $ftp = Net::FTP->new($catia_addr, Debug => 3)
  || die "unable to contact $catia_addr: $@";

$ftp->login("plot", "plot") 
  || die "Cannot login: ", $ftp->message();

$ftp->cwd($catia_path) 
  || die "Cannot switch to dir \"$catia_path\" in $catia_addr: ", $ftp->message();

$ftp->binary();
my $input = new IO::String($request);
$ftp->put($input, "$JOBID\.txt");

$ftp->close();

__END__










my $catlogon = sprintf("RICH.:%-19sUFF. %-18sTEL.%-5s%-19s\n", 
		       join(' ', @{$usrinfo}{qw(last first)}), @{$usrinfo}{qw(dept teln)}, '22/11/2009 13:45:00'); 
my $catparm  = sprintf("%-13s%-3s%-4s%-9s%-s\n", 
		       @{$usrreq}{qw(PN sheet issue paper status)});
my $catmail  = sprintf("%-21s%-8s%-s\n",
		       @{$usrdest}{qw(lib line paper)});

my $ftp = Net::FTP->new($catia_addr, Debug => 3)
  || die "unable to contact $catia_addr: $@";

$ftp->login("plot", "plot") 
  || die "Cannot login: ", $ftp->message();

$ftp->cwd($catia_path) 
  || die "Cannot switch to dir \"$catia_path\" in $catia_addr: ", $ftp->message();

$ftp->binary();
my $input = new IO::String($catlogon);
$ftp->put($input, "$JOBID\.txt");

$input = new IO::String($catparm);
$ftp->put($input, "$JOBID\.par");

$ftp->close();
my $i_dbg_fh = new IO::String(my $i_debug);
my $o_dbg_fh = new IO::String(my $o_debug);
my $catia = new Net::Telnet (Timeout => 30
                             , Prompt => '/[\%\#\>] ?$/'
                             , Input_log => $i_dbg_fh
                             , Output_log => $o_dbg_fh
                            );
$catia->open($catia_addr);
$catia->login($catia_user, $catia_pass);

## Make sure prompt won't match anything in send data.
my $prompt = "_$catia_addr\_4_xreport__";
$catia->prompt("/$prompt\$/");
eval {
my ($resp) = $catia->cmd("export PS1='$prompt'");
print "$catia_addr set Response:\n", $resp, "\n";
my ($line) = $catia->cmd("/cat_user/plot/mvs2aix/xreport/plotmvs2aix_vpm "
		      . join(' ', ($usrreq->{drwtyp}, 
				   $JOBID, 
				   @{$usrreq}{qw(line otype)}),
                                   $usrdest->{paper}
			    )); 

print "$catia_addr response:\n", $line, "\n";
};
print "error: $@\n" if $@;
print "Input trace:\n", $i_debug, "\n";
print "Output trace:\n", $o_debug, "\n";
