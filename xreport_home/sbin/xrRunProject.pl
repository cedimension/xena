#!/usr/bin/perl -w

use strict;

use File::Path;

use lib "$ENV{XREPORT_HOME}/perllib";

use XReport;
use XReport::Project::LineEditor;
use XReport::PDF::DOC;
use XReport::Resources;

my ($workdir, $ProjectName) = @ARGV[0,1];
$workdir = '.' unless -d $workdir;

#warn "Processing $ProjectName\n";

mkpath "$workdir/RUNPROJ/localres";

my %cValues = (
  workdir => "$workdir/RUNPROJ",
  userlib => "$ENV{XREPORT_SITECONF}/userlib",
  SrvName => 'RUNPROJ'
);

sub c::getValues {
  return @cValues{@_}
}
my $docTo = XReport::PDF::DOC->Create("$workdir/RUNPROJ/$ProjectName.pdf");

my $lProject = XReport::Project::LineEditor->Open($ProjectName, run_in_memory => 1,
 						  ( $ARGV[2] ? ('filen', $ARGV[2]) :
						    ('resh', new XReport::Resources
						     (destdir => "$workdir/RUNPROJ/localres",
						      ResourceServer => {ListenAddr => 'BAC25VM', ListenPort => 60080}
						     ) ) ),
				      workdir => $workdir,
				     );

$lProject->TestFlowQueue($docTo);


$docTo->Close();


