#!/usr/bin/perl -w

use Symbol;
use File::Path;
use File::Basename;
use Win32::OLE qw( in );
use Win32::OLE::Variant;
use Win32::Job;
use Win32::EventLog;
use Data::Dumper;
use strict;

sub get_status {
	my $currjob = shift;
	my $status = $currjob->status();
	my $jpid = shift @{ [(keys %$status)] };
	my $times = $status->{$jpid}->{time};
	my $sn = $main::jobsinfo->{'_PID'.$jpid}->{_sn};
	my $statmsg = "ASSEMBLY: JobMon $jpid STATUS - process: $sn kernel: $times->{kernel}, User: $times->{user}, elapsed: $times->{elapsed}";
	return $statmsg;
}

sub JobMon {
	my $currjob = shift;
	my $statmsg = get_status($currjob);
	$main::mcsock->send($statmsg, 0, "DESTINATION") if $main::mcsock;

	return 0;
}

my $sh_obj = Win32::OLE->new("WScript.Shell");
my $env_obj = $sh_obj->Environment("Process");
my $scriptname = "doAssemblyBundles.pl";
my $bundleName = "omitted";
my ($cur_work_dir, $XREPORT_HOME , $XREPORT_SITECONF) = @{ENV}{qw(TMP XREPORT_HOME XREPORT_SITECONF)};
my @IARG = (@ARGV);
foreach my $i (0 .. $#IARG) {
    if ($IARG[$i] eq "-HOME") { # HOME to be set
	       ($XREPORT_HOME = $IARG[$i+1]) =~ s/\\/\//g;
	       $i++;
	}
	elsif ($IARG[$i] eq "-SITECONF") { # SITECONF to be set 
	       ($XREPORT_SITECONF = $IARG[$i+1]) =~ s/\\/\//g;
	       $i++;
	}
  	elsif ($IARG[$i] =~ /-wrk/i) { # SITECONF to be set 
	       ($cur_work_dir = $IARG[$i+1]) =~ s/\\/\//g;
           (my $mk_dir = $IARG[$i+1]);
           system("mkdir ".$mk_dir) || die "$!" unless -d $mk_dir;
	       $i++;
	}
    elsif ($IARG[$i] =~ /-BUNDLE/i) { # SITECONF to be set 
	       ($bundleName = $IARG[$i+1]) =~ s/\\/\//g;
	       $i++;
	}
    elsif ($IARG[$i] =~ /-sel/i) { # SITECONF to be set 
	    $i++;   
        $IARG[$i] = "\"$IARG[$i]\"";
	       
	}   
	elsif ($IARG[$i] eq '-d') { # Debug mode
	       $main::debug = 1;

	}
}

$env_obj->{'XREPORT_HOME'} = $XREPORT_HOME;
$env_obj->{'XREPORT_SITECONF'} = $XREPORT_SITECONF;
#warn Dumper($XREPORT_HOME, $XREPORT_SITECONF, $cur_work_dir);
my $exit_code = 0;
my $job = new Win32::Job();
my $jpid =$job->spawn($^X, "perl -I \"$XREPORT_HOME/perllib\" \"$XREPORT_HOME".'/sbin/'."$scriptname\" ".join( ' ', @IARG) , 
			    {       
			     stdin  => 'NUL', # the NUL device
			     stdout => $cur_work_dir.'/'.$scriptname.".$bundleName.$$.stdout",
			     stderr => $cur_work_dir.'/'.$scriptname.".$bundleName.$$.stderr",
			    }
			   );
$main::jobsinfo->{'_PID'.$jpid } = {_sn => $scriptname};

$job->watch(\&JobMon, 10);
 my $statmsg = get_status($job);
$exit_code = $job->status()->{$jpid}->{'exitcode'};
delete $main::jobsinfo->{'_PID'.$jpid};
if ($exit_code == 0 ) {
    $main::debug and i::logit("ASSEMBLY: pid $$ closed normally exit code: $exit_code");
}else  {
    $main::debug and i::logit("ASSEMBLY: pid $$ closed normally exit code: $exit_code");
}
