#!/usr/bin/perl -w

use strict;
#use LWP::Debug qw(+ -conns);
use LWP::Simple;

use SOAP::Lite; # +trace => qw(all);

use MIME::Base64;
use XML::Simple;

use Data::Dumper;

sub listPrinterPorts {
  use Win32::TieRegistry 0.20 qw(
				  TiedRef $Registry
				  ArrayValues 1  SplitMultis 1  AllowLoad 1
				  REG_SZ REG_EXPAND_SZ REG_DWORD REG_BINARY REG_MULTI_SZ
				  KEY_READ KEY_WRITE KEY_ALL_ACCESS
			       );
  my $kpath = "LMachine\\SYSTEM\\CurrentControlSet\\Control\\Print\\Monitors\\";
  my $ports = {};

  my $printMonKey = $Registry->Open($kpath) || die "Can't read \"$kpath\" key: $^E\n";
  $printMonKey->ArrayValues(1);
  
  foreach my $subKeyName (  $printMonKey->SubKeyNames() ) {
    my $subKey = $printMonKey->{$subKeyName} || die "Can't read \"$kpath$subKeyName\\\" key: $^E\n";
    if (my $portsKey = $subKey->{Ports}) {
      foreach my $portName ( $portsKey->SubKeyNames() ) {
	my $regPort = $portsKey->{$portName} || die "Can't read \"$kpath$subKeyName\\Ports\\$portName\\\" key: $^E\n";
	$ports->{$portName} = { map { ($_ =~ /^\\?(.*)/) => ( $regPort->{$_}->[0] =~ /^0x(.*)$/ ? unpack("N", pack("H*", $1)) :  $regPort->{$_}->[0] ) } keys %{$regPort} };
      }
    }
  }
  return $ports;
}

sub getPrinterPort {
  my $name = shift;
  $main::printerports = listPrinterPorts() unless $main::printerports;
  my $properties = $main::printerports->{$name};
  return {} unless $properties;
  return $properties;
}


sub PortName {
  my ($name, $value) = (shift, shift);
  return { $name => 'N/A'  } unless $value;
  return { $name => $value } if $value =~ /(?:^\\\\|:$)/;
  my $list = {};
  my @props = qw(HostName IPAddress  HWAddress PortNumber Protocol NetworkTimeout);
  @{$list}{@props} = @{getPrinterPort($value)}{@props};
  return { $name => $value, %$list };
}

sub PaperSizesSupported { 
  my ($name, $list) = (shift, shift); 
  return { $name => 'N/A'} unless $list;
  my $decoded = { map { $_ => ( /^0$/ ? 'Unknown'
				: /^1$/ ? 'Other'
				: /^4$/ ? 'C'
				: /^5$/ ? 'D'
				: /^6$/ ? 'E'
				: /^7$/ ? 'Letter'
				: /^8$/ ? 'Legal'
				: /^11$/ ? 'NA-Number-10-Envelope'
				: /^15$/ ? 'NA-Number-9-Envelope'
				: /^21$/ ? 'A3'
				: /^22$/ ? 'A4'
				: /^23$/ ? 'A5'
				: /^49$/ ? 'JIS B0'
				: /^54$/ ? 'JIS B5'
				: /^55$/ ? 'JIS B6'
				: $_ ) } @$list };
  return {$name => join("\t", @{$decoded}{sort keys %$decoded})};
}

sub setValue {
#  print "setting Values for $_[0]\n";
  my ($name, $value) = (shift, shift);
  return { $name => (defined($value) ? $value : 'N/A') };
}

sub listPrinters {
  use Win32::OLE('in');
  my $ports = listPrinterPorts();
  my @propertiesList = qw(
			   Caption
			   Comment 
			   Description 
			   DeviceID 
			   DriverName 
			   ExtendedPrinterStatus 
			   HorizontalResolution 
			   InstallDate 
			   LanguagesSupported 
			   Location 
			   MaxSizeSupported 
			   PaperSizesSupported 
			   PortName 
			   PrinterState 
			   PrinterStatus 
			   Queued 
			   ServerName 
			   Status 
			   StatusInfo 
			   SystemName 
			);

  use constant wbemFlagReturnImmediately => 0x10;
  use constant wbemFlagForwardOnly => 0x20;
  my $computer = ".";
  my $objWMIService = Win32::OLE->GetObject
    ("winmgmts:{impersonationLevel=impersonate}\\\\$computer\\root\\CIMV2") or die "WMI connection failed.\n";
  my $colItems = $objWMIService->ExecQuery
    ("SELECT * FROM Win32_Printer","WQL",wbemFlagReturnImmediately | wbemFlagForwardOnly);
  
  my $printers = {};
  foreach my $objItem (in $colItems) {
    $printers->{$objItem->{Name}} = 
      { map { my $setcode = main->can($_) || \&main::setValue; 
	      %{&$setcode($_, $objItem->{$_})} } @propertiesList};
  }
  return $printers;
}

sub xtractParm {
  my ($struct, $parm) = (shift, shift);
  my $result = undef;
  if (ref($struct) eq 'HASH') {
    foreach my $k ( keys %$struct ) {
#      main::debug2log("checking struct '", $k, "' for '$parm' result: ", $k =~ /^$parm$/i);
      return  $struct->{$k} if $k =~ /^$parm$/i;
      $result = xtractParm($struct->{$k}, $parm);
      return $result if $result;
    }
  }
  elsif (ref($struct) eq 'ARRAY') {
    foreach my $el ( @$struct ) {
      $result = xtractParm($el, $parm);
      return $result if $result;
    }
  }
  return undef;
}

sub XRwebRequest {
  use File::HomeDir;
  use File::Path;

  my $parms = { @_ };
  
  my $server = $parms->{server} || '127.0.0.1:80';
  delete $parms->{server};
  my $service = $parms->{service} || 'PrintIface';
  delete $parms->{service};
  my $method = $parms->{method} || 'RegisterPrinters';
  delete $parms->{method};
  
  unless (defined($main::WSDL) && exists($main::WSDL->{$service}) ) {
    my $workdir = File::HomeDir->my_data()."/XReport/$main::servername";
    mkpath $workdir unless -d $workdir;
    die "unable to create \"$workdir\" - $!" unless -d $workdir; 
    #   $main::wsdlfh = File::Temp->new( SUFFIX => '.wsdl', TEMPLATE => $service."XXXX", OPEN => 1, UNLINK => 1 )
    #     || die "Unable to open temp WSDL File";
    #   $main::WSDL = $main::wsdlfh->filename();
    if (defined($main::WSDL) ) {
      $main::WSDL->{$service} = "$workdir/$service.WSDL";
    }
    else {
      $main::WSDL = { $service => "$workdir/$service.WSDL" };
    }
    print "fserv: ", $main::WSDL->{$service}, "\n";
    my $httpresp = LWP::Simple::getstore("http://$server/$service\.asp?WSDL", $main::WSDL->{$service} );
    die "Unable to get WSDL data" unless $httpresp eq "200";
    
    #   print "tempFile: ", $main::WSDL, "\n";
    #binmode $wsdlfh;
    #   $main::wsdlfh->seek(0,0);
    #while (<$wsdlfh>) {
    #  print $_;
    #}
    #   print $main::WSDL, "has been created\n";
  }
  # system "dir *.wsdl";
  
  my $RQSTattr = {};
  for my $attr (qw(IndexName TOP ORDERBY DocumentType FileName temporary)) {
    if (exists($parms->{$attr})) {
      $RQSTattr->{$attr} = $parms->{$attr};
      delete $parms->{$attr};
    }
  }
  my $DocumentData = {};
  foreach my $valname ( qw(IndexEntries NewEntries) ) {
    next unless exists $parms->{$valname};
    foreach my $ie ( @{$parms->{$valname}}) {
      my @attrn = [ grep !/^Columns$/, keys %{$ie} ];
      my $attrs = {};
      @{$attrs}{@attrn} = @{$ie}{@attrn};
      push @{$DocumentData->{$valname}}, SOAP::Data->value({Columns => $ie->{Columns}});
    }
  }
  
  print "Sending request to $server for $service($method)\n";
  my $client = (SOAP::Lite->uri(lc($service))
		->service("file:$main::WSDL->{$service}")
		#	       ->service("http://$server/$service.asp?wsdl")
		->proxy("http://$server/$service.asp")
		->encprefix('soapenc')
		->envprefix('soapenv')
		->autotype(0)
		#	       ->on_debug(sub{print @_})
	       );
  die "Service $service does not provide method $method" unless $client->can($method);
  print "requesting method $method\n";
  
  my $result = ($client
		->uri(lc($service))
		->on_action(sub {return '"' . join('#', @_). '"'})
		->outputxml(1)
		->call($method,
		       SOAP::Data->name('REQUEST')->attr( $RQSTattr )
		       ->value(XMLout({'IndexEntries' => $parms->{IndexEntry}}, KeepRoot => 1, NoIndent => 1)
			       . XMLout({'NewEntries' => $parms->{NewEntries}}, KeepRoot => 1, NoIndent => 1)
			      )
		      )
	       );
  
  print "RESPONSE: ", ref($result), "\n";
  return $result;
}

sub Server {
  my ($SrvPort, $msgmax) = (shift, shift);

  use IO::Socket;
  use IO::Select;
  use Win32API::File qw(:ALL);
  use Win32::Process;

  my ($sock, $Cli, $currSt);
  print "$^O server Started for port $SrvPort...\n";

  my $SrvSock = new IO::Socket::INET(Listen => 5, 
				     LocalPort => $SrvPort,
				     Proto => 'tcp', 
				     Reuse => 1) || die "unable to listen on $SrvPort - $!\n";

  print "Listener now waiting on port $SrvPort\n" if ($SrvSock);

  my $sel = new IO::Select( $SrvSock );

  my $currcmd = (split /[\\\/]+/, $0)[-1];
  (my $clicmd = "perl \"$0\" $msgmax ") =~ s/$currcmd/xrPrinterHandler.pl/;
  print "now entering main loop (cmd: \"$clicmd\")\n" if ($SrvSock);
  for (my $con = 0; ; $con++) {
    $sock = $sel->can_read(15);
    next unless ($sock);
    $Cli = $SrvSock->accept || warn "unable to accept connection - $! - $?\n";
    last unless $Cli;
    my ($cliport, $iaddr) = sockaddr_in(getpeername($Cli));
    $REQ::peer = inet_ntoa($iaddr);
    print "$main::servername accepted connection from " . $REQ::peer . "\n";
    Win32::Process::Create( my $ProcessObj, "$^X", "$clicmd " . FdGetOsFHandle( $Cli->fileno ), 1, Win32::Process->NORMAL_PRIORITY_CLASS, "." )
	|| warn "ERROR Launching $clicmd";
  }
  print "now leaving main loop: $currSt ", (($sock) ? "Cli pending" : "Cli Exhausted"), "\n";
  $SrvSock->close;
  return 0;
}

use File::HomeDir;

my @parms = @ARGV;
$main::servername = 'xrPrintValet';
$main::thisaddr   = 'localhost:10099';
$main::xrhost     = 'localhost:10080';
$main::xruri      = 'PrintIface';
$main::msgmax     = 4096;

while ( @parms ) {
  print "PARMS: ", join("::", @parms), "\n";
  $_ = shift @parms;
  ( 
   /^-N$/ ? do { $main::servername   = shift @parms; }
   : /^-U$/ ? do { $main::xruri      = shift @parms; }
   : /^-S$/ ? do { $main::xrhost     = shift @parms; }
   : /^-P$/ ? do { $main::thisaddr = shift @parms; }
   : /^-msgmax$/ ? do { $main::msgmax = shift @parms; }
   : 1
  );
}
use FileHandle;
use LWP;

my $browser = LWP::UserAgent->new;
  
my $url = "http://$server/ConfigServer#getCentralConfig";
my $response = $browser->post( "http://$server/ConfigServer/getCentralConfig", 
			       [ server => $main::servername, 
				 cfguser => $ENV{USERNAME}, 
			       ] );

die "xx: ", $response->{uri}, " :: ", Dumper($response);

my ($server, $uri) = ($main::xrhost, $main::xruri);
my $result = XRwebRequest( server => $server, 
			   service => 'ConfigServer', 
			   method => 'getCentralConfig',
			   IndexName => 'DOCSVC',
			   DocumentType => $main::servername,
			   IndexEntries => [],
			   NewEntries => [],
			 );

my $respdata = xtractParm(XMLin($result,
				ForceArray => [ qw(NewEntries IndexEntries Columns ValuesArray) ],
				KeyAttr => {'Columns' => 'colname'},
			       ),
			  qr/RESPONSE/) if $result;
print "response: ", Dumper($respdata), "\n", "Body: ", decode_base64($respdata->{DocumentBody}), "\n";

$XReport::cfg = XMLin(decode_base64($respdata->{DocumentBody})) || die "Unable to parse config from server";
print "cfg: ", Dumper($XReport::cfg), "\n";

my $serverHandler = eval 'sub {' . $XReport::cfg->{ServerCode} . '}';
die "Error compiling ServerCode - $@" if $@;

my $workdir = File::HomeDir->my_data()."/XReport/$main::servername";
mkpath $workdir unless -d $workdir;
die "unable to create \"$workdir\" - $!" unless -d $workdir; 
$XReport::cfg->{clicodefn} = "$workdir/$main::servername\_CLIENT.pl";
my $clicodefh = new FileHandle(">$XReport::cfg->{clicodefn}");
binmode $clicodefh;
print $clicodefh $XReport::cfg->{ClientCode};
close $clicodefh;

my ($IFhost, $IFport) = @{$XReport::cfg->{PrintIface}}{qw(ListenAddr ListenPort)};
my $IFServer = "$IFhost\:$IFport";
#$main::clicodefh = File::Temp->new( SUFFIX => '.pl', TEMPLATE => $main::servername."_CLI", OPEN => 1, UNLINK => 1 )
#  || die "Unable to open temp WSDL File";
#$c::cfg->{clicodefn} = $main::clicodefh->filename();
#print $main::clicodefh $c::cfg->{ClientCode};
#close $main::clicodefh;

my $printers = listPrinters();

my $update = [ ];
while ( my ($name, $props) = each %{ $printers } ) {
  $props->{PrtName} = $name;
  $props->{PrtSrvAddress} = $main::thisaddr;
  push @$update, { Columns => [ map { { 'colname' => $_, 'Value' => $props->{$_} } } (keys %$props ) ] };
}


$result = XRwebRequest( server => $IFServer, 
			   service => 'PrintIface', 
			   method => 'RegisterPrinters',
			   IndexName => '_printers', 
#			   TOP => 10,
			   DocumentType => 'Printer',
			   IndexEntries => [],
			   NewEntries => $update,
			 );

print "FAULT: ", join ', ',
  $result->faultcode(),
  $result->faultactor(),
  $result->faultstring(),
  $result->faultdetail() if $result && ref($result) && $result->can('fault') && $result->fault();

$respdata = xtractParm(XMLin($result,
				ForceArray => [ qw(NewEntries IndexEntries Columns ValuesArray) ],
				KeyAttr => {'Columns' => 'colname'},
			       ),
			  qr/RESPONSE/) if $result;
#print "RESULT: ", Dumper(xtractParm(XMLin($result, KeyAttr => {'Columns' => 'colname'}, ForceArray => [ qw(NewEntries IndexEntries Columns ValuesArray) ]), qr/RESPONSE/)), "\n";
print "PRINTERS accepted: ", join(', ', map { $_->{Columns}->{PrtName}->{Value} } @{$respdata->{IndexEntries}}), "\n";

#print "=========>\n", Dumper($respdata), "\n<===========\n" if $result;
print "No Results returned\n" unless $result;

&$serverHandler($main::thisaddr, $main::msgmax) if $serverHandler;

#print Dumper(listPrinters()), "\n";

__END__

