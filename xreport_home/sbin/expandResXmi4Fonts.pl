#use lib $ENV{'XREPORT_HOME'}."/perllib";
use POSIX;

#use Convert::EBCDIC;
use File::Basename;
use Convert::IBM390 qw(:all);
use Symbol;
use XReport::AFP::IOStream;
use XReport::TRANSMIT::INPUT;


#$translator = new Convert::EBCDIC;
use strict;
sub i::logit { print @_, "\n"; }
sub i::warnit { print @_, "\n"; }

$main::veryverbose = 1 if   (scalar grep {$_ =~ /^-dd$/} @ARGV);
$main::debug = 1 if   (scalar grep {$_ =~ /^-dd$/} @ARGV);

our $discarded_resources = "COIBM275|COIBM275";
#our $discarded_resources = "S1E00385|S1A00385|S1112000|S1000385|S1000028|P1ASAP03|XXA96RDL|_S1GNVRET|VUOTO|\$\$PDSMAN|S1000002|COIBM275|COIBMABA|COIBMABB|FONTABZ0|FONTABZ1|FONTBBZ0|FONTCB20|FONTCBZ0|FONTGBZ0|FONTHBZ0|FONTKBZ0|FONTLBZ1|FONTLBZ2|FONTLBZ3|FONTOBZ0|FONTPB20|FONTSBZ0|COIBM275|COIBMABA|COIBMABB|FONTABZ0|FONTABZ1|FONTBBZ0|FONTCB20|FONTCBZ0|FONTGBZ0|FONTHBZ0|FONTKBZ0|FONTLBZ1|FONTLBZ2|FONTLBZ3|FONTOBZ0|FONTPB20|FONTSBZ0|FONTTBZ1|ILC";#"APS1IOCA|S1GIGI|P1ASAP03|XXA96RDL|COIBM275|COIBMABA|COIBMABB|FONTABZ0";
our @list_of_folder ;

sub XpandXmit {
  print "XpandXmit()\n";
  return unless ( -e $ARGV[0] && -f $ARGV[0] && $ARGV[0] ) =~/.xmi[^\.]*$/i;
  my $tr = XReport::TRANSMIT::INPUT->new(DEBUG => 1); 
  mkdir "binary" if !-d "binary";
  
  $tr->EXPAND_LIBRARY(
		      INPUT => $ARGV[0],
		      OUTDIR => './binary',
		      DEBLOCK => 1,
		      DEBUG => './unxmit.log',
		      VERBOSE => 1
		     );

}


sub CopyFile {
  #print "TESTSAN - CopyFile()[".__LINE__."]\n";
  my ($lib, $fileName) = @_;
  #return if (uc($fileName)  eq 'CXBAR030' );
  my $fsize = -s "binary/$lib/$fileName";
  return unless $fsize;
  open(IN,"<binary/$lib/$fileName") or die("?? IN $!"); binmode(IN);
  read(IN, my $fullfile, $fsize );
  my $outfile = '';
  close(IN);
  if ( substr($fullfile, 0, 1 ) ne "\x5A" ) {
       warn "processing $fsize bytes of $fileName\n";
       my ($recll, $c5a, $ll_) = unpack('n a1 n', $fullfile);
	   if ( $c5a ne "\x5A")# || $recll != ( $ll_ + 1 ) );
	   {
	        #print "TESTSAN - CopyFile()[".__LINE__."]\n";
			print "Error in CopyFile. $fileName discarded: 5A ?? ", unpack("H*", $c5a.substr($fullfile, 0, 5)), " $recll ?? $ll_ ", tell(IN)."\n" ;
			$discarded_resources.="|$fileName";
			return;
	   }
	   #print "TESTSAN - CopyFile()[".__LINE__."]\n";
       die "5A ?? ", unpack("H*", $c5a.substr($fullfile, 0, 5)), " $recll ?? $ll_ ", tell(IN) if ( $c5a ne "\x5A");# || $recll != ( $ll_ + 1 ) );
	   my @vblines = unpack('(n/a)*', $fullfile);
	   warn "processing ".scalar(@vblines)." lines of $fileName\n";
	   while ( scalar(@vblines) ) {
	     #print "TESTSAN - CopyFile()[".__LINE__."]\n";
	     my $l = shift @vblines;
		 my $outl = '';
		 
		 my $TESTSANcount=0;
         while ( 1 ) {
		    $TESTSANcount++;
		    my ($c5a, $rec, $l) = unpack( 'a1 n @1 /a a*', $l);
	        warn "TESTSAN - processing lines of $fileName:($c5a, $rec, $l)_length(\$outl):".length($outl);
			die "Reached the max number of counter." if $TESTSANcount > 100;
			$outl .= $c5a.$rec;
            last if (length($l) == 0 || substr($l, 0, 1) ne "\x5a" ); 
		 }
         $outfile .= pack('n/a*', $outl) if $outl;		 
	   }
  }
  else {
     $outfile = join('', map { pack('n/a*', "\x5a".$_) } unpack('(x1 n @1 /a)*', $fullfile));
  }
  mkdir "psf" if !-d "psf";
  mkdir "psf/$lib" if !-d "psf/$lib";
  open(PSF,">psf/$lib/$fileName") or die("?? OUT $!"); binmode(PSF);
  (my $rest = uc(substr($fileName, 0, 1))) =~ tr/CFOPSTX/\x40\xFE\xFC\xFD\xFB\x41\x42/;
  my $BRS = Convert::IBM390::packeb('H* E8 H* c6', 'D3A8CE000001', $fileName, '00000821', $rest);
  print PSF pack('n/a*', pack('H* n a*', '5A', length($BRS)+2, $BRS));
  print PSF $outfile; 
  my $ERS = Convert::IBM390::packeb('H* E8', 'D3A9CE000001', $fileName);
  print PSF pack('n/a*', pack('H* n a*', '5A', length($ERS)+2, $ERS));
  print "Copied $lib $fileName from binary to psf\n";
  close(PSF);
  
  if((-f "psf/$lib/$fileName")  && (-s "psf/$lib/$fileName" ) )
  {
  	print "\n PS created with success from the file [$fileName]."."[".__LINE__."]";
  	unlink "binary/$lib/$fileName" if   (scalar grep {$_ =~ /^-deleteAfterConversion$/} @ARGV);
  }
}

sub TRANSLATE_AfpFile {
  #print "TRANSLATE_AfpFile()\n";
  my ($fileName, $psFileName) = (shift, shift);
  die("Error in TRANSLATE_AfpFile() - The file \"$fileName\" does not  exist.") unless (-f $fileName);
  #my $fileFullPath = abs_path($fileName); 
  #die("Error in TRANSLATE_AfpFile() - The file \"$fileFullPath\" does not  exist.") unless (-f $fileFullPath);

  my $odir = dirname($psFileName);
#  $XReport::AFP::IOStream::ps = XReport::AFP::PRINTPS->new($odir);
#  my $OUTPUT = gensym();
#  open($OUTPUT,">$psFileName") or die("OUTPUT OPEN ERROR \"$psFileName\" $!"); binmode($OUTPUT);
  #print "TESTSAN - XReport::AFP::IOStream::ExpandResourceFile($fileName, $odir)\n";
  my @pslines;
  
  eval {@pslines= ( XReport::AFP::IOStream::ExpandResourceFile($fileName, $odir) );};
  
  if($@)
  {
	print "\n An Error occurred in TRANSLATE_AfpFile for the file [$fileName]:".$@."[".__LINE__."]";
	eval{unlink $psFileName if (-f $psFileName) ;};
	return;
  }
  
  
  unless(scalar @pslines)
  {
	print "\n Error in TRANSLATE_AfpFile for the file [$fileName], the file ps created is  empty."."[".__LINE__."]";
	#eval{unlink $psFileName if (-f $psFileName);};
  }
  
  if((-f $psFileName)  && (-s $psFileName) )
  {
  	print "\n PS created with success from the file [$fileName]."."[".__LINE__."]";
  	unlink $fileName if   (scalar grep {$_ =~ /^-deleteAfterConversion$/} @ARGV);
  }
  elsif(-f $psFileName)
  { 
  	print "\n Error in TRANSLATE_AfpFile: the file [$psFileName] is empty."."[".__LINE__."]";
	#eval{unlink $psFileName if (-f $psFileName);};
  }
  else
  {
  	print "\n Error in TRANSLATE_AfpFile: the file [$psFileName] does not exist."."[".__LINE__."]";
  }
  print "Converted $fileName - ps lines: ".scalar(@pslines)."\n";
#  print $OUTPUT join("\n", @pslines);
#  close($OUTPUT);
}

sub makepsf {
  print "makepsf()\n";
  #for my $lib (qw(FDEFLIB PDEFLIB OVERLIB PSEGLIB PAGELIB FORMLIB FONTLIB)) {
  for my $lib (@list_of_folder) {
    for my $fileName (glob("binary/$lib/*")) {
	  $fileName = basename($fileName);
	  next if $fileName =~ qr/^(?:$discarded_resources)$/o; 
	  CopyFile($lib, basename($fileName));
    }
  }
}

sub makeps {
  print "makeps()\n";
  my $inloc = shift || 'psf';
  #for my $lib (qw(FDEFLIB PDEFLIB OVERLIB PSEGLIB PAGELIB FORMLIB FONTLIB)) {
  for my $lib (@list_of_folder) {
    mkdir "ps" if !-d "ps";
    mkdir "ps/$lib" if !-d "ps/$lib";
    for my $fileName (glob("$inloc/$lib/*")) {
	  $fileName = basename($fileName);
    	#next if $fileName =~ /^(?:APS1IOCA|S1GIGI)$/;
		#next if $fileName =~ /^(?:P1ASAP03|_____XXA96RDL)$/;
		next if $fileName =~ qr/^(?:$discarded_resources)$/o; 
		
	  print("TRANSLATE_AfpFile(\"$inloc/$lib/$fileName\", \"ps/$lib/$fileName\.ps\"\n");
	  TRANSLATE_AfpFile("$inloc/$lib/$fileName", "ps/$lib/$fileName\.ps");
#	  if((-f "ps/$lib/$fileName\.ps" ) && (-s "ps/$lib/$fileName\.ps" ))
#	  {
#		print("file \"ps/$lib/$fileName\.ps\" created with success.\n");
#		unlink "$inloc/$lib/$fileName" if (scalar grep {$_ =~ /^-deleteAfterConversion$/} @ARGV);
#	  }
#	  else
#	  {
#		unlink "ps/$lib/$fileName\.ps" unless (-s "ps/$lib/$fileName\.ps" );
#		print("error in creation of file \"ps/$lib/$fileName\.ps\".\n");
#	  }
    }
  }
}




@list_of_folder = qw(OVLYLIB FDEFLIB PDEFLIB OVERLIB PSEGLIB PAGELIB FORMLIB FONTLIB); 
for(map {$_ =~ /^-only(.*)$/i; "$1";}  grep {$_ =~ /^-only(OVLYLIB|FDEFLIB|PDEFLIB|OVERLIB|PSEGLIB|PAGELIB|FORMLIB|FONTLIB)$/i} @ARGV)
{
	print("Mode only$_ actived.\n");
    @list_of_folder = ($_);
	last;
}
print join('::',@list_of_folder);

XpandXmit() unless (scalar grep {$_ =~ /^-(noxmit)$/i} @ARGV);
makepsf() unless (scalar grep {$_ =~ /^-(nopsf)$/i} @ARGV);
makeps() unless (scalar grep {$_ =~ /^-(nops)$/i} @ARGV);
#makeps('binary');
