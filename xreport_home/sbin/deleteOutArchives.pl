#!perl

=pod

da utilizzare per eliminare file dall'archivio out dell'xreport
chiamata: 
perl -I%XREPORT_HOME%/perllib %XREPORT_HOME%/sbin/deleteOutArchives.pl + 
                + condizioni di select es: "JobReportName = 'GQORB2C0.__SYSUT2'"
                + Directory dove copiare i files da cancellare es: D:\WORK\SALVAXR
                + file di comandi per le cancellazioni es: D:\WORK\DELETEXR.CMD
                
Lo script copia i file dei Jobreport selezionati dalla condizione specificata come primo parametro
nella directory specificata come secondo parametro ricostruendo la struttura di origine sotto una 
directory con nome uguale al contenuto della colonna LocalPathId_OUT.
In aggiunta scrive sul file specificato come terzo parametro i comandi di delete dei file fisici.

Tutto ci� per permettere le dovute verifiche prima della cancellazione vera e propria. 
La cancellazione vera e propria dovr� essere effettuata eseguendo il file di comandi costruito.

In caso di necessit� di ripristino, baster� copiare le directory OUT delle varie directory PATHID 
nelle locazioni specificate nell'elemento LocalPath di xreport.cfg  

La directory target non deve esistere o se esiste deve essere vuota.

E' richiesta la presenza delle variabili di ambiente XREPORT_HOME e XREPORT_SITECONF  

=cut

use File::Path ();
use IO::File ();
use File::Basename ();

use strict;
use XReport;
use XReport::DBUtil ();

my $whclause = $ARGV[0];
die "Specify a selection criteria for the JobReports to delete" unless $whclause;

my $dumpdir = $ARGV[1];
if ( !-e $dumpdir ) {
	File::Path::mkpath($dumpdir);
	die "Unable To create \"$dumpdir\" - $? - $!" unless (-e $dumpdir && -d $dumpdir);
}
else {
    die "Invalid temporary location \"$dumpdir\"" unless -d $dumpdir;
    opendir DIRFH, $dumpdir || die "Unable to access temporary location \"$dumpdir\" - $? - $!";
    die "Temporary Location \"$dumpdir\" is not empty - process aborted " unless eof(DIRFH);
    close(DIRFH); 
}

my $script_file = $ARGV[2];
my $scrdir = File::Basename::dirname($script_file); 
if ( !-e $scrdir ) {
    File::Path::mkpath($scrdir);
    die "Unable To create \"$scrdir\" - $? - $!" unless (-e $scrdir && -d $scrdir);
}
else {
    die "Invalid script location \"$scrdir\"" unless -d $scrdir;
}
my $scrfh = IO::File->new(">$script_file") || die "Creation of script file \"$script_file\" - $? - $!";

no warnings 'redefine';

sub i::logit {
	print localtime().'', @_;
}

sub moveOutFile {
	my ($jrid, $pathid, $lclfn, $dumpdir, $scrfh) = @_;
	$lclfn =~ s/.DATA\.[^\.]+\.gz(?:\.tar)?$//;
	$dumpdir =~ s/\\/\//g;
	my ( $lclname, $lclpath ) = map { $_ =~ s/\\/\//g; $_ =~ s/\/$//; $_ } File::Basename::fileparse($lclfn);
	my $pathdef = $XReport::cfg->{LocalPath}->{$pathid};
    return i::logit("Unable to resolve PATHID $pathid for JobReport $jrid") unless $pathdef;
    my ($pathtype, $pathname) = ($pathdef =~ /^([^\/]+)\/\/(.*)$/);	
    return i::logit("Path type $pathtype for JobReport $jrid unsupported ") unless $pathtype =~ /^file:/i;

    (my $fqfnpfx = "$pathname/OUT/$lclfn") =~ s/\\/\//g;
    my @flist = glob qq("${fqfnpfx}.*");
    return i::logit("No Files \"$lclfn\" for JobReport $jrid found") unless scalar(@flist);

    foreach my $fqfn ( @flist ) {
      return i::logit("File \"$fqfn\" for JobReport $jrid is not accessible") unless -e $fqfn;
    }
    
    (my $outpath = "$dumpdir/$pathid/OUT/$lclpath") =~ s/\/$//;
    File::Path::mkpath $outpath || return i::logit("Unable to create \"$outpath\" for JobReport $jrid- $? - $!") 
                                                                                                   if (!-e $outpath );
    return i::logit("Output path $outpath for JobReport $jrid is not accessible or is not a directory")
           unless ( -e $outpath && -d $outpath );
           
    foreach my $fqfn ( @flist ) {
       my $infn = File::Basename::basename($fqfn); 
       my $outfn = "$outpath/$infn";
       my $infh = IO::File->new("<$fqfn") || return i::logit("Unable to read \"$infn\" for JobReport $jrid- $? - $!");
       $infh->binmode();
       my $outfh = IO::File->new(">$outfn") || return i::logit("Unable to open \"$outfn\" to write JobReport $jrid- $? - $!");
       $outfh->binmode();
       while ( !$infh->eof() ) {
       	  $infh->read(my $buff, 32000);
       	  print $outfh $buff if length($buff);
       }
       $infh->close();
       $outfh->close();
       return i::logit("Copy of \"$infn\" to \"$outpath\" failed insize: ".-s $fqfn." outsize: ".-s $outfn)
                     unless (-s $fqfn == -s $outfn);
    }
    
    foreach my $fqfn ( @flist ) {
    	print $scrfh "DEL $fqfn\n";
        i::logit "File \"$fqfn\" copied to $dumpdir - added to deletionscript";
        $::fcount++;
    }
	        
}

my $dbr;
my $sql = "SELECT COUNT(*) AS NumFiles FROM tbl_JobReports WHERE $whclause";
eval {$dbr = XReport::DBUtil::dbExecuteReadOnly($sql);};
die "File count query failed - $@" if $@;
die "File Count Query returned no result - process aborted" if $dbr->eof();
my $numfiles = $dbr->Fields()->Item('NumFiles')->Value();
$dbr->Close();
unless ( $numfiles ) {
   i::logit("No files Match the selection criteria specified ($whclause) - process ended");
   exit 0;
}

i::logit("Starting now to process $numfiles JobReports Items");

$sql = "SELECT top 100 * FROM tbl_JobReports WHERE $whclause ";
eval {$dbr = XReport::DBUtil::dbExecuteReadOnly($sql);};
die "File selection query failed - $@" if $@;
die "File selection Query returned no result - process aborted" if $dbr->eof();
while ( !$dbr->eof() ) {
  my ($jrid, $pathid, $lclfn) = $dbr->GetFieldsValues(qw(JobReportId LocalPathId_OUT LocalFileName));
#  $pathid = 'L1' if $pathid =~ /^\s*$/;
  moveOutFile $jrid, $pathid || 'L1', $lclfn, $dumpdir, $scrfh;	
  $::jrcount++;
} continue { $dbr->MoveNext() }
$dbr->Close();
$scrfh->close();

i::logit("Processed $::fcount files for $::jrcount Jobreports over $numfiles Selected Items");
exit 0;