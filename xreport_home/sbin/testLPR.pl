#!/usr/bin/perl -w
  
  use strict;
  use vars '@ARGV';
  
  use XReport;
  use XReport::Deliver::LPR;
  use IO::File;
  use File::Basename;
  
  die "usage: $0 <filename> <printer> <queue>\n" if (@ARGV < 3);
  
  my $lp = new XReport::Deliver::LPR(
        StrictRFCPorts => 0,
        RemoteServer => $ARGV[1],
        RemotePort => 515,
        PrintErrors => 0,
        RaiseErrors => 0,
  ) or die "Can't create print context\n";
  
  die "Can't find $ARGV[0] or is not a file" unless (-e $ARGV[0] && -f $ARGV[0] );
  my $size = -s $ARGV[0]; 
  my $fh = new IO::File $ARGV[0], O_RDONLY or die "Can't open $ARGV[0]: $! - $^E\n";
  $fh->binmode();

  $lp->connect() or die "Can't connect to printer: ".$lp->error."\n";
  my $jobkey = $lp->new_job(undef, $main::Application->{ApplName}) or die "Can't create new job: ".$lp->error."\n";
  $lp->send_jobs($ARGV[2]) or die "Can't send jobs: ".$lp->error."\n";
  # Can easily print postscript by changing method to job_mode_postscript
  #$lp->job_mode_text($jobkey) or die "Can't set job mode to text: ".$lp->error."\n";
  $lp->job_set_source_filename($jobkey, basename($ARGV[0]));
  $lp->job_set_user_id($jobkey, 'PIPPETTO');
  if ( $ARGV[3] && -e $ARGV[3] && -f $ARGV[3] ) {
     my $ctlsz = -s $ARGV[3];
     my $ctlfh = new IO::File $ARGV[3], O_RDONLY or die "Can't open $ARGV[3]: $! - $^E\n";
     $ctlfh->binmode();
	 $ctlfh->read(my $cfstr, $ctlsz);
	 $ctlfh->close();
	 my $cfsiz = length($cfstr);
	 die "Ctl file read failed - data expected: $ctlsz - got $cfsiz bytes" unless ( $cfsiz == $ctlsz );
     $lp->job_send_control_scalar($jobkey, $cfstr);
  }
  else {
  $lp->job_send_control_file($jobkey, {
          'N' => 'testLPR.pl',
          'form' => 'PIPPO',
          'block_offsets_ttr' => '5120',
          'JobName' => 'MIOJOB',
          'CRDTIM' => '2015-11-27T17:41:48.47',
          'data_block_size' => 131072,
          'FIXRecfm' => 1,
          'f' => 'dfA000PLUTO_SERV',
          'input_data_size' => 1582,
          'JobOrigin' => 'thor',
          'maxlrec_detected' => 512,
          'P' => 'PIPPETTO',
          'RemoteFileName' => 'testLPR.pl',
          'LRECL' => 512,
          'JNUM' => 'J123456',
          'JOBNM' => 'MIOJOB',
          'H' => 'thor',
          'ORIGIN' => 'thor',
          'data_file_ttr' => 4096,
          'JobNumber' => 'J123456',
          'fileformat' => 5,
          'OJBID' => 'J123456',
          'JNAM' => 'MIOJOB',
          'OWNER' => 'PIPPETTO'
  }) or die "Can't send control file: ".$lp->error."\n";
  }
  $lp->job_send_data($jobkey, '', $size);
  
  while (!$fh->eof()) {
        $fh->read(my $buff, 32000);
        $lp->job_send_data($jobkey, $buff);
  }
  
  $lp->disconnect();