::perl D:\xena\xreport_home\sbin\FtpcExpandAndConvertToAscii.pl -filein D:\TEMP\A1\XREPORTN.D0180123.T023034.20180123122226.1897.IN.gz -onlyexpanded -mydir D:\TEMP\A1
#!/usr/bin/perl -w
use Compress::Zlib;
use strict;
use Convert::EBCDIC;
use IO::File; 


$| = 1;

sub getModecILine {
  my $fillchar = "\x00";
#  for ( my ($buffer, $outrec, $eor) = (shift, '', 0); !$eor && $buffer;) {
  my ($buffer, $outrec, $eor) = (shift, '', 0);
  while (1) {
    my $save = $buffer;
      my $b1 = unpack('C', $buffer);
      ($eor, $b1, $buffer) = unpack('x a C X a*', $buffer) unless $b1;
      die("Format Error: INVALID MODEC CONTROL SEQUENCE $eor " . 
      join('::', unpack("H2 H64", $eor.$buffer))) if ($eor && ($eor !~ /^[\x40\x80]$/ or !$b1));
#      print join('::', unpack("H2 H64", $eor.$buffer)), "\n";
      (my $fill, $buffer) = (($b1 & 0xc0) == 0xc0 ? ($fillchar, substr($buffer, 1)) :
                 unpack((!($b1 & 0x80) ? 'C/a*' : 'x a' )." a*", $buffer));
      $outrec .= $fill x ( ($b1 & 0x80) ?  ($b1 & 0x3f) : 1);
#      $outrec .= $b1 if $b1 eq "\x00";
#      print unpack("H64", $save), "\n";# if $eor && substr($outrec, 3, 3) eq "\xd3\xa8\xdf";
#      print unpack("H64", $outrec), "\n";# if $eor && substr($outrec, 3, 3) eq "\xd3\xa8\xdf";
#      print length($outrec), "::", unpack("H64", $outrec), "\n" if $eor; # && substr($outrec, 3, 3) eq "\xd3\xa8\xdf";
#      die "XXX" if length($outrec) == 16 && substr($outrec, 3, 3) eq "\xd3\xa8\xdf";
      if ( $eor ) {
      	$outrec = pack('n/a', $outrec);
       return (wantarray ? ($outrec, $buffer) : $outrec);
      }
  }
}

sub fromGZ2TXT {
	my($inGZfile, $TXTfile) = @_;
	my $buffer;
	my $gz = gzopen($inGZfile, "rb");
	die "unable to open $inGZfile: $gzerrno\n" unless $gz;
	print "Input File $inGZfile opened.\n";
	unlink $TXTfile if (-f $TXTfile);

	open(my $OUT_TXT_handler,">$TXTfile")  or die("LOGFILE OUTPUT OPEN ERROR $TXTfile rc=$!\n"); 
	binmode $OUT_TXT_handler;
	print $OUT_TXT_handler $buffer while $gz->gzread($buffer) > 0 ;
	my $errgznum = ($gz->gzerror()+0);
	my $errgzmsg = scalar($gz->gzerror());
	$OUT_TXT_handler->close;
	$gz->gzclose() ;
	print "file '$TXTfile' createte with success.\n" if (-f $TXTfile); 
	die "input file does '$TXTfile' not exist" unless (-f $TXTfile);
}

sub fromEBCDIC2ASCII{
	my($TXTfile,$ASCHIIfile) = @_;
	unlink $ASCHIIfile if (-f $ASCHIIfile); 
	open(my $OUThandler,">$ASCHIIfile")  or die("LOGFILE OUTPUT OPEN ERROR $ASCHIIfile rc=$!\n"); 
	binmode $OUThandler; 
	my $INhandler = new IO::File("<$TXTfile") || die "Unable to open \"$TXTfile\" - $!";    
	my $translator = Convert::EBCDIC->new(); 
	while ( !$INhandler->eof() ) {
		$INhandler->read(my $buff, 2);
		my $lrec = unpack("n", $buff); 
		$INhandler->read(my $rec, $lrec); 
		print $OUThandler $translator->toascii($rec), "\r\n";
	}
	close $OUThandler;
	die "input file does '$ASCHIIfile' not exist" unless (-f $ASCHIIfile);
	print "file '$ASCHIIfile' createte with success.\n" if (-f $ASCHIIfile);
}

sub toExpanded{
	my($inpfn,$outfn) = @_;

my $ftpcin = gzopen($inpfn, "rb");
die "unable to open $inpfn: $gzerrno\n" unless $ftpcin;
print "Input File $inpfn opened\n";

my $dataou = gzopen($outfn, "wb");
die "unable to open $outfn: $gzerrno\n" unless $dataou;
print "Output File $outfn opened\n";

# init buffers
my $totbytes = $ftpcin->gzread(my $byteCache, 131072);
my ($buff, $lct, $outbytes) = ('', 0);
while ( $byteCache ) {
  if (length($byteCache) < 131072 && !$ftpcin->gzeof()) {
    my $bcount = $ftpcin->gzread(my $inbuff, 131072);
    $totbytes += $bcount;
		warn "read $bcount bytes from input - $totbytes bytes so far\n" if $main::veryverbose;
    $byteCache .= $inbuff;
  }
  (my $string, $byteCache) = getModecILine($byteCache);
#  my ($c5a, $ll5a, $str5a) = unpack('a n a*', $string);
#  $string = pack('a n a*', $c5a, length($str5a)+2, $str5a);
#  $string = pack("n/a*", $string);
  if (length($buff.$string) > 131072) {
     my $outcnt = $dataou->gzwrite($buff);
		warn "wrote $outcnt bytes to output - ", $outbytes += $outcnt, " bytes so far - lines: $lct\n" if $main::veryverbose;
    $buff = '';
  }
  $buff .= $string;
  $lct++;
}
if (length($buff)) {
		warn "writing ", length($buff), " last bytes to output\n" if $main::veryverbose;
     my $rc = $dataou->gzwrite($buff);
    $buff = '';
}

# flush buffer if any
#ie "no data in the last buffer" unless length($buff) > 0;
#$dataou->gzwrite($buff);

#  close DATAOU;
$dataou->gzclose();
$ftpcin->gzclose();

print "output lines written: $lct\n";
}


my $current_dir = 'C:\temp';

#my ($inpfn, $outfnEXPANDED) = @ARGV[0,1];
my $inpfn = $ARGV[0];
my $outfnEXPANDED;
#die "output file not specified" unless $outfnEXPANDED; 
    foreach my $i (0 .. $#ARGV) 
	{
		if ($ARGV[$i] =~ /onlyexpanded/i) { # onlyexpanded 
		  $main::onlyexpanded = 1;
		}
 		elsif ($ARGV[$i] =~ /fileout/i) { # fileout 
 		  $outfnEXPANDED = $ARGV[$i+1]; 
 		  $i++;
 		}
 		elsif ($ARGV[$i] =~ /fileout/i) { # fileout 
 		  $outfnEXPANDED = $ARGV[$i+1]; 
 		  $i++;
 		}
 		elsif ($ARGV[$i] =~ /mydir/i) { # mydir 
 		  $current_dir = $ARGV[$i+1]; 
 		  $i++;
#		elsif ($ARGV[$i] eq "-N") { # Server Name 
#		  $applname = $ARGV[$i+1]; 
#		  $i++;
#		}
#		elsif ($ARGV[$i] eq "-HOME") { # HOME to be set
#		  ($XREPORT_HOME = $ARGV[$i+1]) =~ s/\\/\//g;
#		  $i++;
#		}
#		elsif ($ARGV[$i] eq "-SITECONF") { # SITECONF to be set 
#		  ($XREPORT_SITECONF = $ARGV[$i+1]) =~ s/\\/\//g;;
#		  $i++;
#		  $confvar = 0;
#		}  
#		elsif ($ARGV[$i] eq '-@') { # Daemon Mode 
#		  $main::daemonMode = 1;
#		}
#		elsif ($ARGV[$i] eq '-d') { # Debug mode
#		  $main::debug = 1;
#		  $largs{debug} = 1;
#		}
	}
 		}
die "input file not specified" unless $inpfn;
die "mydir does not exist" unless (-e $current_dir);


$outfnEXPANDED= $current_dir . "\\" . ( split( /[\/\\]/, $inpfn ) )[-1]. ".EXPANDED"   unless $outfnEXPANDED;
unlink $outfnEXPANDED if (-f $outfnEXPANDED);

toExpanded($inpfn,$outfnEXPANDED);

die "GZ input file not specified" unless $outfnEXPANDED;
die "input file '$outfnEXPANDED' does not exist" unless (-d $outfnEXPANDED); 

my $TXTfile = $outfnEXPANDED.'.EBCDIC.TXT'; 
fromGZ2TXT($outfnEXPANDED,$TXTfile);

unless($main::onlyexpanded)
{
	my $ASCHIIfile = $outfnEXPANDED.'.ASCII.TXT'; 
fromEBCDIC2ASCII($TXTfile,$ASCHIIfile);
}

exit 0;
