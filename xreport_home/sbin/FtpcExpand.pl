#!/usr/bin/perl -w
use Compress::Zlib;
use strict;

$| = 1;

sub getModecILine {
  my $fillchar = "\x00";
#  for ( my ($buffer, $outrec, $eor) = (shift, '', 0); !$eor && $buffer;) {
  my ($buffer, $outrec, $eor) = (shift, '', 0);
  while (1) {
    my $save = $buffer;
      my $b1 = unpack('C', $buffer);
      ($eor, $b1, $buffer) = unpack('x a C X a*', $buffer) unless $b1;
      die("Format Error: INVALID MODEC CONTROL SEQUENCE $eor " . 
      join('::', unpack("H2 H64", $eor.$buffer))) if ($eor && ($eor !~ /^[\x40\x80]$/ or !$b1));
#      print join('::', unpack("H2 H64", $eor.$buffer)), "\n";
      (my $fill, $buffer) = (($b1 & 0xc0) == 0xc0 ? ($fillchar, substr($buffer, 1)) :
                 unpack((!($b1 & 0x80) ? 'C/a*' : 'x a' )." a*", $buffer));
      $outrec .= $fill x ( ($b1 & 0x80) ?  ($b1 & 0x3f) : 1);
#      $outrec .= $b1 if $b1 eq "\x00";
#      print unpack("H64", $save), "\n";# if $eor && substr($outrec, 3, 3) eq "\xd3\xa8\xdf";
#      print unpack("H64", $outrec), "\n";# if $eor && substr($outrec, 3, 3) eq "\xd3\xa8\xdf";
#      print length($outrec), "::", unpack("H64", $outrec), "\n" if $eor; # && substr($outrec, 3, 3) eq "\xd3\xa8\xdf";
#      die "XXX" if length($outrec) == 16 && substr($outrec, 3, 3) eq "\xd3\xa8\xdf";
      if ( $eor ) {
      	$outrec = pack('n/a', $outrec);
       return (wantarray ? ($outrec, $buffer) : $outrec);
      }
  }
}

my ($inpfn, $outfn) = @ARGV[0,1];
die "input file not specified" unless $inpfn;
die "output file not specified" unless $outfn;

my $ftpcin = gzopen($inpfn, "rb");
die "unable to open $inpfn: $gzerrno\n" unless $ftpcin;
print "Input File $inpfn opened\n";

my $dataou = gzopen($outfn, "wb");
die "unable to open $outfn: $gzerrno\n" unless $dataou;
print "Output File $outfn opened\n";

# init buffers
my $totbytes = $ftpcin->gzread(my $byteCache, 131072);
my ($buff, $lct, $outbytes) = ('', 0);
while ( $byteCache ) {
  if (length($byteCache) < 131072 && !$ftpcin->gzeof()) {
    my $bcount = $ftpcin->gzread(my $inbuff, 131072);
    $totbytes += $bcount;
    warn "read $bcount bytes from input - $totbytes bytes so far\n";
    $byteCache .= $inbuff;
  }
  (my $string, $byteCache) = getModecILine($byteCache);
#  my ($c5a, $ll5a, $str5a) = unpack('a n a*', $string);
#  $string = pack('a n a*', $c5a, length($str5a)+2, $str5a);
#  $string = pack("n/a*", $string);
  if (length($buff.$string) > 131072) {
     my $outcnt = $dataou->gzwrite($buff);
    warn "wrote $outcnt bytes to output - ", $outbytes += $outcnt, " bytes so far - lines: $lct\n";
    $buff = '';
  }
  $buff .= $string;
  $lct++;
}
if (length($buff)) {
    warn "writing ", length($buff), " last bytes to output\n";
     my $rc = $dataou->gzwrite($buff);
    $buff = '';
}

# flush buffer if any
#ie "no data in the last buffer" unless length($buff) > 0;
#$dataou->gzwrite($buff);

#  close DATAOU;
$dataou->gzclose();
$ftpcin->gzclose();

print "output lines written: $lct\n";
exit 0;