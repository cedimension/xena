TRUNCATE TABLE tbl_JobReportsDiscardedPages
TRUNCATE TABLE tbl_JobReportsFileRanges
TRUNCATE TABLE tbl_CenteraClips
TRUNCATE TABLE tbl_LogicalReports
TRUNCATE TABLE tbl_LogicalReportsRefs
TRUNCATE TABLE tbl_LogicalReportsTexts
TRUNCATE TABLE tbl_PhysicalReports
TRUNCATE TABLE tbl_JobReportsMixedReportIds
TRUNCATE TABLE tbl_JobReportsLocalFileCache
TRUNCATE TABLE tbl_JobReportsIndexTables
TRUNCATE TABLE tbl_JobCtlInput
TRUNCATE TABLE tbl_JobReportsElabs
TRUNCATE TABLE tbl_JobReportsBtrees
TRUNCATE TABLE tbl_JobReportsBtreesBlobs
TRUNCATE TABLE tbl_JobReportsCenteraClipIds
TRUNCATE TABLE tbl_JobReportsTarFiles
TRUNCATE TABLE tbl_JobReportsXfers
TRUNCATE TABLE tbl_JobReportsPendingXfers
TRUNCATE TABLE tbl_JobReportsDiscardedPages
TRUNCATE TABLE tbl_JobReportsElabs
TRUNCATE TABLE tbl_JobReportsLocalFileCache
TRUNCATE TABLE tbl_JobReportsElabOptions
TRUNCATE TABLE tbl_EmailJobReportsStat
TRUNCATE TABLE tbl_JobReportsFileRanges
TRUNCATE TABLE tbl_JobReports
TRUNCATE TABLE tbl_WorkQueue
TRUNCATE TABLE tbl_JobReportsBtrees
TRUNCATE TABLE tbl_JobReportsBtreesBlobs
TRUNCATE TABLE tbl_Bundles
TRUNCATE TABLE tbl_BundlesQ
TRUNCATE TABLE tbl_BundlesLog
DBCC CHECKIDENT (tbl_JobReports, RESEED, 100000)
DBCC CHECKIDENT (tbl_WorkQueue, RESEED, 100000)

