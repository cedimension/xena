#!perl -w

use strict;
use IO::File;

use XReport;
use XReport::DBUtil;

my $runparms = {
	workdir => $ARGV[0],
	folder => $ARGV[1],
	selector => $ARGV[2],
	prtdest => $ARGV[3],
	};

die "Unable to access $runparms->{workdir} - $!" unless -d $runparms->{workdir};

sub extractReportPages {
  my ($rinfo, $pagelist) = (shift, shift);
  my $finfo = $rinfo->{folder};
  require XReport::EXTRACT;
  require XReport::PDF::DOC;

  my $query = XReport::EXTRACT->new();
  
  (my $pdfname = 'Bundle.'.$finfo->{FolderName}.'.'.$pagelist->[0].'.pdf') =~ s/[\\\/]/_/g;
  $pdfname = $finfo->{workdir}."\\".$pdfname;
#  warn "extract finfo for $pdfname: ", Dumper($rinfo), "\n";
  my $streamer = new IO::File "> $pdfname";
  warn "Unable to open \"$pdfname\" - $!\n" unless $streamer;
  return undef unless $streamer;

  $query->{docTo} = XReport::PDF::DOC->Create($streamer) ;

  my $elist;
  eval { $elist = $query->ExtractPages(
				       QUERY_TYPE => 'FROMPAGES',
				       PAGE_LIST => \@$pagelist ,
				       FORMAT => "PRINT",
				       ENCRYPT => 'YES',
				       OPTIMIZE => 1,
				       TO_DIR => $finfo->{workdir},
				       REQUEST_ID => "Bundle.".$finfo->{FolderName}.'.'.$rinfo->{ReportName},
				       #				   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"}
				      );
	};

  if ( scalar(@$elist) == 0 ) {
    die "chiave richiesta non accessibile in Archivio - $@";
    return undef;
  }
  
  return $pdfname;
#TODO: check if and how to close extract  
}

sub printPDFList {
	my ($parms, $finfo, $pdflist) = (shift, shift, shift);
	my ($lpdserver, $queue, $gsdevice) = split /:/, $parms->{prtdest};
	use Data::Dumper;
#	warn "printPDFList parms: ", Dumper(\{finfo => $finfo, pdflist => $pdflist, prtdest => [$lpdserver, $queue, $gsdevice]});
	
	(my $outfname = 'Bundle.'.$finfo->{FolderName}.'.'.$gsdevice.'.pr') =~ s/[\\\/]/_/g;
	$outfname = $finfo->{workdir}."\\".$outfname;
	(my $gscmd = "\"".$XReport::cfg->{gs}->{cmd}."\" ") =~ s/\//\\/g; 

	$gscmd .= "-dBATCH -dNOPAUSE -dSAFER -dQUIET -sDEVICE=$gsdevice -sOutputFile=$outfname "
		. join(' ', map { s/\//\\/g; $_ } @$pdflist);
	warn "GSCMD: ", $gscmd, "\n";
	system $gscmd;

	my $lpcmd = "\"".$main::Application->{XREPORT_HOME}."\\sbin\\lpr.exe\" -S $lpdserver -P $queue \"$outfname\"";
	warn "LPCMD: ", $lpcmd, "\n";
	system $lpcmd;
}

sub callGStoPDF {
	(my $outpdf = shift) =~ s/\//\\/g;
	my $cinfo = shift;
	my $cfgcode = [ split /\n/, shift ];
	my $pscode = ["%!PS"
		,"/browse-dict { { exch (key: ) print == (val: ) print == } forall } bind def"
		,"/textheight { gsave { 100 100 moveto (HIpg) true charpath pathbbox exch pop 3 -1 roll pop exch sub } "
		,"	stopped { pop pop currentfont /FontMatrix get 3 get } if grestore } bind def"
		,"/beginpage {  save /sssnap exch def currentpagedevice /PageSize get aload pop /PH exch def /PW exch def 0 PH moveto } bind def"
		,"/endpage { sssnap restore showpage } bind def"
		,"/stringshow {"
		,"dup type /dicttype eq not { /intext exch def 1 dict dup /text intext put  } if"
		,"<< /fname /Helvetica-Bold /text (BANNER) /fscale 1 /align (center) /pitch 0 >>"
		,"copy begin"
		,"currentpagedevice /PageSize get aload pop /PH exch def /PW exch def"
		,"fname findfont [10 0 pitch 10 0 0] makefont setfont"
		,"currentfont text stringwidth pop (XX) stringwidth pop add PW exch div fscale mul scalefont setfont"
		,"/ldisp align (left) eq { 20 } { PW text stringwidth pop sub 2 div } ifelse def"
		,"ldisp 0 textheight sub rmoveto  "
		,"text show "
		,"text stringwidth  pop 0 exch sub ldisp sub 0 rmoveto"
		,"end"
		,"} bind def"
		,"/LabeledBox { "
		,"/bkup save def"
		,"<< /boxwidth { PW 40 sub } /boxlabel ( ) /boxfont [ /Helvetica-Bold 12 ] /text [[( ) ( ) ( )] [( ) ( ) ( )] [( ) ( ) ( )]] /boxpos [20 0] >>"
		,"copy begin  "
		,"currentdict /textfont known not { /textfont boxfont def } if"
		,"boxpos aload pop /ybpos exch def /xbpos exch def"
		,"textfont aload pop exch findfont exch scalefont setfont"
		,"/xchar (XX) stringwidth pop 2 div def /txtwidth boxwidth xchar 4 mul sub def /lheight textheight def /thikness lheight cvi def"
		,"/yinc lheight 1.5 mul cvi def  /boxheight text length yinc mul def /CardH boxheight yinc add yinc 2 div add def"
		,"/xpos xbpos xchar 2 mul add def "
		,"/ypos ybpos boxheight add def "
		,"xbpos 0 CardH yinc add ybpos add sub rmoveto /ip [ currentpoint ] def"
		,"newpath ip aload pop moveto 0 CardH rlineto boxwidth 0 rlineto 0 0 CardH sub rlineto closepath .5 setlinewidth stroke "
		,"newpath ip aload pop moveto thikness thikness rmoveto 0 CardH thikness 2 mul sub rlineto boxwidth thikness 2 mul sub 0 rlineto 0 thikness 2 mul CardH sub rlineto closepath"
		,"2 setlinewidth stroke"
		,"gsave"
		,"ip aload pop /ypos exch boxheight add def /origx exch thikness xchar add add def "
		,"text {"
		,"aload pop /rightstring exch def /dotstring exch def /leftstring exch def "
		,"origx ypos moveto leftstring stringwidth pop 0 rmoveto 0 setgray"
		,"0 dotstring stringwidth pop dup 2 div txtwidth rightstring stringwidth pop sub exch sub leftstring stringwidth pop sub "
		,"{dotstring show} for "
		,"origx ypos moveto 0 setgray leftstring show"
		,"origx ypos moveto txtwidth rightstring stringwidth pop sub 0 rmoveto rightstring show "
		,"/ypos ypos yinc sub def "
		,"} forall"
		,"ip aload pop CardH add moveto"
		,"0 setgray"
		,"boxfont aload pop exch findfont exch scalefont setfont"
		,"boxlabel show"
		,"grestore"
		,"end"
		,"bkup restore"
		,"} bind def"
	 ];
	
#	warn "cinfo: ", Dumper($cinfo), "\n";
    my $prtvars = {};
    foreach my $k ( keys %$cinfo ) {
    	next if ( $k eq 'finfo' ) ;
    	my @val = (split /\\/, $cinfo->{$k});
    	$prtvars->{$k} = (scalar(@val) > 1 ? 
   	    join("\\", @val[-2,-1]) : join('', @val));
    } 
    foreach my $k ( keys %{$cinfo->{'finfo'}} ) {
    	my @val = (split /\\/, $cinfo->{'finfo'}->{$k});
    	$prtvars->{$k} = (scalar(@val) > 1 ? 
    	join("\\", @val[-2,-1]) : join('', @val));
    }
    
	if ( exists($XReport::cfg->{gs}->{pageparms}) ) {
		unshift @$cfgcode, '<<'.$XReport::cfg->{gs}->{pageparms}.'>> setpagedevice' ;
	}

	push @$pscode, ( 
	  map { $_ =~ s/\#\{(.+?)\}\#/$prtvars->{$1}/g; $_ =~ s/\\/\\\\/g; $_ } 
	      @$cfgcode );
	
#	warn "PSCODE: ", join("\n", @$pscode), "\n";
	(my $gscmd = "\"".$XReport::cfg->{gs}->{cmd}."\" ") =~ s/\//\\/g; 
	$gscmd .= join (' ' 
		,"-q -dBATCH -dNOPAUSE -dSAFER -dQUIET -dNoCancel"
		,"-sDEVICE=pdfwrite"
		,$XReport::cfg->{gs}->{pdfparms} 
		,"-sOutputFile=$outpdf"
		,"-"
		);
		
	warn "GSCMD: ", $gscmd, "\n";
	open(HGS, "| $gscmd") or die $!;
    foreach ( @$pscode ) {
    	print HGS $_, "\n";
    }
    close HGS;

    return $outpdf;
}

sub createReportBanner {
	my $rinfo = shift;
	print "ReportBanner: ", Dumper(\{rinfo => $rinfo});
	
	my $finfo = $rinfo->{folder};
	return (map { (my $oname = 'Bundle.'.$finfo->{FolderName}.'.'.$rinfo->{ReportName}.'.'.$_ ) =~ s/[\\\/]/_/g; 
		callGStoPDF("$finfo->{workdir}/$oname", {%$runparms, %$rinfo}, $XReport::cfg->{ReportBannerPS} ) } ('head.pdf', 'tail.pdf')); 

}

sub createFolderBanner {
	my $finfo = shift;

	return (map { (my $oname = 'Bundle.'.$finfo->{FolderName}.'.'.$_ ) =~ s/[\\\/]/_/g; 
		callGStoPDF("$finfo->{workdir}/$oname", {%$runparms, %$finfo}, $XReport::cfg->{FolderBannerPS} ) } ('head.pdf', 'tail.pdf')); 
}

sub getFolderList {

  my $values = {UserName => '.none.', FolderName => shift };
  my $queryExpr = $XReport::cfg->{sqlprocs}->{FolderChildFolders};
  $queryExpr =~ s/\#\{(.+?)\}\#/$values->{$1}/g;

  my $dbr = XReport::DBUtil::dbExecute_NORETRY($queryExpr);
  my $flist = {};
  my $parents = {};
  while ( $dbr && !$dbr->eof() ) {
  	my $flds = $dbr->GetFieldsHash();
    $flist->{$flds->{FolderName}} = 1   if $flds->{HasReports};
    $parents->{$flds->{FolderName}} = 1 if $flds->{HasChilds}; 
  }
  continue {
    $dbr->MoveNext();
  }
  $dbr->Close() if $dbr;
  return ( keys %$flist, map { getFolderList($_) } keys %$parents );

}

my $folders = {};
my $values = {};
foreach my $fname ( getFolderList($runparms->{folder}) ) {
  my $reports = $folders->{$fname}->{namelist} = {};
  $values = { UserName => '.none', FolderName => $fname };

  my $queryExpr = $XReport::cfg->{sqlprocs}->{FolderReportNames};
  $queryExpr =~ s/\#\{(.+?)\}\#/$values->{$1}/g;
#  warn 'FolderReportNames: ', $queryExpr, "\n";
  my $dbr = XReport::DBUtil::dbExecute_NORETRY($queryExpr);
  while ( $dbr && !$dbr->eof() ) {
  	my $flds = $dbr->GetFieldsHash();
    $reports->{$flds->{ReportName}} = $flds;
    $reports->{$flds->{ReportName}}->{reportlist} = {};
  }
  continue {
    $dbr->MoveNext();
  }
  $dbr->Close() if $dbr;
  
  foreach my $rname ( keys %$reports ) {
    @{$values}{qw(ReportName UserSpec)} = ($rname, $runparms->{selector});
	($values->{_UserSpecSelector_} = $XReport::cfg->{sqlprocs}->{selector}) 
													=~ s/\#\{(.+?)\}\#/$values->{$1}/g;
#	warn "VALUES: ", Dumper($values), "\n"; 
    $queryExpr = $XReport::cfg->{sqlprocs}->{ReportNameIntervalReports};
    $queryExpr =~ s/\#\{(.+?)\}\#/$values->{$1}/g;
#    die 'ReportNameIntervalReports: ', $queryExpr, "\n";
    
    $dbr = XReport::DBUtil::dbExecute_NORETRY($queryExpr);
    while ( $dbr && !$dbr->eof() ) {
		my $flds = $dbr->GetFieldsHash();
		my $jrid = delete $flds->{JobReportID};
		my @ListOfPages = ($jrid, split /\,/, delete $flds->{ListOfPages} );
	  	foreach my $fldn ( grep { $flds->{$_} =~ /\\n/ } keys %$flds ) {
  			my $fldc = 1;
  			foreach my $txtpart ( split /\\n/, $flds->{$fldn} ) {
  				$flds->{$fldn.'.'.$fldc++} = $txtpart;
  			}
  		}
		#	warn 'folder flds: ', Dumper($flds), "\n";
		$reports->{$rname}->{reportlist}->{$jrid} = $flds;
		push @{$reports->{$rname}->{reportlist}->{$jrid}->{ListOfPages}}, @ListOfPages; 
    }
    continue {
      $dbr->MoveNext();
    }
    $dbr->Close() if $dbr;
  }
}

while ( my ($fname, $finfo) = each %$folders ) {
  @{$finfo}{qw(FolderName FolderPages workdir pdfs)} = ($fname, 0, $runparms->{workdir}, []);
#	warn 'finfo: '. Dumper($finfo)."\n";
  my $namelist = delete $finfo->{namelist};
  while ( my ($rname, $rinfo) = each %$namelist ) {
    @{$rinfo}{qw(ReportName ReportPages folder pdfs)} = ($rname, 0, $finfo, []);
    my $reportlist = delete $rinfo->{reportlist};
#    warn 'rinfo: '. Dumper($rinfo)."\n";
    while (my ($JRID, $jrinfo) = each %$reportlist ) {
	    my $ListOfPages = delete $jrinfo->{ListOfPages};
      if ( my $pdfname = extractReportPages($rinfo, [ @{$ListOfPages} ] ) ) {
		push @{$rinfo->{pdfs}}, $pdfname;
		printf "Adding now %s(%d) from page %d to bundle pages: %d\n", ($rname, @{$ListOfPages}[0,1,2]);
#    warn 'jrinfo: '. Dumper($jrinfo)."\n";
		$rinfo->{ReportPages} += $ListOfPages->[2];
		$rinfo = { %$jrinfo, %$rinfo };
		$finfo = { %$rinfo, %$finfo };
      }
    }
    if (scalar(@{$rinfo->{pdfs}})) {
    	my ($headpdf, $tailpdf) = createReportBanner($rinfo);
    	push @{$rinfo->{pdfs}}, $tailpdf; unshift @{$rinfo->{pdfs}}, $headpdf;
    	push @{$finfo->{pdfs}}, @{delete $rinfo->{pdfs}};
    	$finfo->{FolderPages} += $rinfo->{ReportPages};
    }
  }
  if (scalar(@{$finfo->{pdfs}})) {
  	my ($fheadpdf, $ftailpdf) = createFolderBanner($finfo);
  	my @pdflist = ($fheadpdf, @{delete $finfo->{pdfs}}, $ftailpdf);
  	printPDFList $runparms, $finfo, \@pdflist;
  }
}
