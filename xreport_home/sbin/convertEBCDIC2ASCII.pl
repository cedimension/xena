#!/usr/bin/perl -w
use strict;
use Compress::Zlib;
use Convert::EBCDIC;
use IO::File; 
use Archive::Tar;



sub fromTAR2GZ {
	my($inTARfile) = @_;
	die "input file not specified" unless $inTARfile;
	die "input file is not tar file" if ($inTARfile !~ /^(.*)\.tar$/s);
	my $inGZfile = "$1.gz" ;
	$inGZfile =~ s/\.gz\.gz/\.gz/gi;
	unlink $inGZfile if (-f $inGZfile); 
	
	foreach my $filename (grep /\.DATA\.TXT\.gz$/i, Archive::Tar->list_archive($inTARfile))
	{
		my $tar = Archive::Tar->new($inTARfile);
		unlink $inGZfile if (-f $inGZfile); 
		$tar->extract_file($filename, $inGZfile );
		last;
	}
	print "file '$inGZfile' created with success.\n" if (-f $inGZfile); 
	die "input file does '$inGZfile' not exist" unless (-f $inGZfile);
	return $inGZfile; 
}
sub fromGZ2TXT {
	my($inGZfile) = @_;
	my $buffer;
	my $gz = gzopen($inGZfile, "rb");
	die "unable to open $inGZfile: $gzerrno\n" unless $gz;
	print "Input File $inGZfile opened.\n";
	my $TXTfile = $inGZfile.'.EBCDIC.TXT'; 
	unlink $TXTfile if (-f $TXTfile); 

	open(my $OUT_TXT_handler,">$TXTfile")  or die("LOGFILE OUTPUT OPEN ERROR $TXTfile rc=$!\n"); 
	binmode $OUT_TXT_handler;
	print $OUT_TXT_handler $buffer while $gz->gzread($buffer) > 0 ;
	my $errgznum = ($gz->gzerror()+0);
	my $errgzmsg = scalar($gz->gzerror());
	$OUT_TXT_handler->close;
	$gz->gzclose() ;
	print "file '$TXTfile' created with success.\n" if (-f $TXTfile); 
	die "input file does '$TXTfile' not exist" unless (-f $TXTfile);
	return $TXTfile;
}

sub fromEBCDIC2ASCII{
	my($TXTfile,$outfn,$outfnexpanded) = @_;
	unlink $outfn if (-f $outfn); 
	unlink $outfnexpanded if (-f $outfnexpanded); 
	open(my $OUThandler,">$outfn")  or die("LOGFILE OUTPUT OPEN ERROR $outfn rc=$!\n"); 
	open(my $OUThandlerEXP,">$outfnexpanded")  or die("LOGFILE OUTPUT OPEN ERROR $outfnexpanded rc=$!\n"); 
	binmode $OUThandler; 
	binmode $OUThandlerEXP; 
	my $INhandler = new IO::File("<$TXTfile") || die "Unable to open \"$TXTfile\" - $!";    
	my $translator = Convert::EBCDIC->new(); 
	while ( !$INhandler->eof() ) {
		$INhandler->read(my $buff, 2);
		my $lrec = unpack("n", $buff); 
		$INhandler->read(my $rec, $lrec); 
		print $OUThandler $translator->toascii($rec), "\r\n";
		print $OUThandlerEXP $rec;
	}
	close $OUThandlerEXP;
	close $OUThandler;
	die "input file does '$outfn' not exist" unless (-f $outfn);
	print "file '$outfn' created with success.\n" if (-f $outfn);
}

my ($inGZfile, $outfn, $outfnexpanded) = @ARGV[0,1];
die "input file not specified" unless $inGZfile;

$inGZfile = fromTAR2GZ($inGZfile) if ($inGZfile =~ /^.*\.tar$/s);
$outfn = $inGZfile.'.ASCII.TXT' unless $outfn; 
die "input file '$inGZfile' does not exist" unless (-f $inGZfile); 
$outfnexpanded = $inGZfile.'.EXPANDED.TXT' unless $outfnexpanded; 
my $TXTfile = fromGZ2TXT($inGZfile);
fromEBCDIC2ASCII($TXTfile,$outfn,$outfnexpanded);

