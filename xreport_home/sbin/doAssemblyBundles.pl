#!perl -w

use strict;
use POSIX qw(strftime);
#use POSIX qw(strftime);
use IO::File;

use XReport;
use XReport::DBUtil;
use XReport::BUNDLE;

use Data::Dumper;
$| = 1;
my $oldfh = select STDERR;
$| = 1;
select $oldfh;

print "calling arguments for $0, ", join(" " , @ARGV), "\n";

my @IARG = (@ARGV);

$main::veryverbose = 0;
$main::verbose = 0;
foreach my $i (0 .. $#IARG) {
    if ($IARG[$i] eq '-d') { # Debug mode
	       $main::debug = 1;
		   $main::verbose = 1;
	}
	elsif ($IARG[$i] eq '-dd') { # Debug mode
	       $main::debug = 1;
		   $main::verbose = 1;
	       $main::veryverbose = 1;
	}
}

sub i::warnit2 {
    warn ''.localtime().' - '.join(' ', @_)."\n";
}

sub i::logit2 {
    warn ''.localtime().' - '.join(' ', @_)."\n";
}


sub localGetLock {
    my $LockName =shift ;
	my $activatedLogitForLock =shift ;
	die "doAssemblyBundles: Error in localGetLock($LockName) - resource already held by the same process." if $main::resourceLocked ;
	$main::resourceLocked = 1;
	my $timeBeforeToLock = time();
	my $rc = GetLock($LockName);
	die "doAssemblyBundles: Error in GetLock($LockName)" if($rc < 0);
	$main::timeStartLock = time();
	$main::elapsedTimeBeforeToLock = sprintf("%.2f", $main::timeStartLock - $timeBeforeToLock);
	i::logit("doAssemblyBundles: GetLock('$LockName') called:$rc - time before to hold the resource: ".$main::elapsedTimeBeforeToLock ) if $activatedLogitForLock;
	return $rc;
}

sub localReleaseLock {
    my $LockName =shift ;
	my $activatedLogitForLock =shift ;
	my $rc = ReleaseLock($LockName);
	die "doAssemblyBundles: Error in ReleaseLock($LockName) - rc:$rc" if($rc < 0);
	my $timeEndLock = time();
	my $elapsedTimeToLock = sprintf("%.2f", $timeEndLock - $main::timeStartLock);
	i::logit("doAssemblyBundles: ReleaseLock('$LockName') called:$rc - time before to hold: ".$main::elapsedTimeBeforeToLock." - time to hold: $elapsedTimeToLock " ) if $activatedLogitForLock;
	$main::resourceLocked = 0;
	return $rc;
}

sub processBundleMission {
    my ($self, $binfo) = (shift, shift);
#die  Dumper({ SELF => $self
#            , INFO => $binfo
#            });
#
    i::logit("Bundle ". join('::', @{$binfo}{qw(BundleName BundleCategory BundleSkel)})
        . "($binfo->{BundleKey}) process begins");




    my $bname = $binfo->{BundleName};

    $binfo->{Status} = 4;
    $self->insertBundleEntry($binfo);
    $self->updateWrkDir($binfo);
    $binfo->{SKIPPED} = [];
    my $dbr = $self->queryBundlesLogicalReports( $binfo );
    if ($dbr->eof()) {
	   #i::logit("DEBUG - self: ", Dumper($self),"_line=".__LINE__);
       i::logit("------------------->>>>>>>>>>No results returned from query - no reports  to be processed");
       $self->updateBundleEntry($binfo);
       $self->clearOutPut($binfo);
       return undef;
    }
    else {
      while (!$dbr->eof()) {
          my $flds = $dbr->GetFieldsHash();
          my $jrlop = [ split /\,/, $flds->{ListOfPages} ];
          my $ListOfPages = [ ];
          while ( scalar(@{$jrlop}) ) {
          	push @{$ListOfPages}, ($flds->{JobReportId}, splice @{$jrlop}, 0, 2);
          }
		  i::logit("doAssemblyBundles.pl::processBundleMission() - new : JobReportId:". $flds->{JobReportId} ."- ListOfPages:".join(',',@{$ListOfPages})."_line=".__LINE__); 

          $main::veryverbose && i::warnit("Processing $flds->{JobReportId}($flds->{JobReportName}) LOP: $flds->{ListOfPages}"
          . " PSET: " . ( defined($flds->{PERMCopies}) ? $flds->{PERMCopies} : '')
          . " BQ: $flds->{BQCopies}"
          . " PrintCopies: $flds->{PrintCopies}"
          );
          if ( defined($flds->{PrintCopies}) && $flds->{PrintCopies} == 0 ) {
              i::logit(sprintf("Recipent \"%s\" of Bundle %s will not include %d pages "
                . " from page %d of JOB %s(%d/%d) as for "
                . ((defined($flds->{PERMCopies}) && $flds->{PERMCopies} == 0) ? " PERMANENT Specification" 
                  : defined($flds->{BQCopies}) ? "REPORT CONFIGURATION ($flds->{ParseFileName})"
                  : "REPORT Definition" ),
                           (@{$flds}{qw(RecipientName BundleName)}
                                    , @{$ListOfPages}[2, 1], @{$flds}{qw(JobName JobReportId ReportId)})));
              @{$flds}{qw(InputPages InputLines PrintedLines PrintedPages PrintStatus)} = (0, 0, 0, 0, 2);
              push @{$binfo->{SKIPPED}}, $flds;
          }
          elsif ( $flds->{outfilename} = $self->extractReportPages($flds, $ListOfPages ) ) {
              foreach my $fldn ( qw(FolderAddr SpecialInstr) ) {
                  foreach ( 1..9 ) { $flds->{$fldn.'.'.$_} = ''; }
                  my $fldc = 1;
                  foreach my $txtpart ( split /\\n/, $flds->{$fldn} ) { $flds->{$fldn.'.'.$fldc++} = $txtpart; }
              }

              @{$flds}{qw(odate otime)} = ($flds->{JobTimeRef} =~ /^(.{10})(?:.(.*))?$/);
              @{$flds}{qw(odate otime)} = ($flds->{XferStartTime} =~ /^(.{10})(?:.(.*))?$/) unless $flds->{odate};

			  $flds->{odate}  = ($4.'/'.$3.'/'.$2) if( $flds->{odate} =~ /^([\d]{2})([\d]{2}).([\d]{2}).([\d]{2})$/ );
              $flds->{odate}  = ($4.'/'.$3.'/'.$2) if( $flds->{odate} =~ /^([\d]{4})\-([\d]{2})\-([\d]{2})$/ );

              $binfo->{folders}->{$flds->{RecipientName}} =  XReport::BUNDLE::setPrintInfo( $flds,
                                        XReport::BUNDLE::setPrintInfo( $binfo, { InputPages => 0, InputLines => 0, reports => {} } ) ) unless exists($binfo->{folders}->{$flds->{RecipientName}});
              my $uinfo = $binfo->{folders}->{$flds->{RecipientName}};


			  #12/02/2018 aggiunto campo JobReportId alla chiave in reports
              #$uinfo->{reports}->{$flds->{JobName}.'_'.$flds->{UserRef}.'_'.$flds->{JNumber}} = XReport::BUNDLE::setPrintInfo( $flds,
              #           XReport::BUNDLE::setPrintInfo($uinfo, { InputPages => 0, InputLines => 0, jobs => [] } ) ) unless exists($uinfo->{reports}->{$flds->{UserRef}});
              #my $rinfo = $uinfo->{reports}->{$flds->{JobName}.'_'.$flds->{UserRef}.'_'.$flds->{JNumber}};

              $uinfo->{reports}->{$flds->{JobName}.'_'.$flds->{UserRef}.'_'.$flds->{JobReportId}} = XReport::BUNDLE::setPrintInfo( $flds,
                         XReport::BUNDLE::setPrintInfo($uinfo, { InputPages => 0, InputLines => 0, jobs => [] } ) ) unless exists($uinfo->{reports}->{$flds->{UserRef}});
              my $rinfo = $uinfo->{reports}->{$flds->{JobName}.'_'.$flds->{UserRef}.'_'.$flds->{JobReportId}};

              i::logit(sprintf("File \"%s\" (JOB $flds->{JobName} JRID %d from page %d) will add %d pages (%d lines) to bundle",
                       ($flds->{outfilename}, @{$ListOfPages}[0,1], @{$flds}{qw(InputPages InputLines)})));

              for ( qw(Pages Lines) ) {
                  $binfo->{'Input'.$_} += $flds->{'Input'.$_};
                  $uinfo->{'Input'.$_} += $flds->{'Input'.$_};
                  $rinfo->{'Input'.$_} += $flds->{'Input'.$_};
              }
              #@{$flds}{qw(odate otime)} = ($flds->{JobTimeRef} =~ /^(.{10})(?:.(.*))?$/);
              #@{$flds}{qw(odate otime)} = ($flds->{XferStartTime} =~ /^(.{10})(?:.(.*))?$/) unless $flds->{odate};

              #$flds->{odate}  = ($4.'/'.$3.'/'.$2) if( $flds->{odate} =~ /^([\d]{2})([\d]{2}).([\d]{2}).([\d]{2})$/ );


              $flds->{otime} = '' unless $flds->{otime};
              push @{$rinfo->{jobs}}, $flds;
          }
          else {
              i::logit(sprintf("Error in extractReportPages - File \"%s\" (JOB $flds->{JobName} JRID %d from page %d) will not add %d pages (%d lines) to bundle",
                       ($flds->{outfilename}, @{$ListOfPages}[0,1], @{$flds}{qw(InputPages InputLines)})));
              @{$flds}{qw(InputPages InputLines PrintedLines PrintedPages PrintStatus)} = (0, 0, 0, 0, 15);
              push @{$binfo->{SKIPPED}}, $flds;
          }
        }
        continue { $dbr->MoveNext(); }
    }
    $dbr->Close();
	#i::logit("DEBUG - BINFO: ", Dumper($binfo),"_line=".__LINE__);
    while ( scalar(@{$binfo->{SKIPPED}}) ) {
        my $skippedinfo = shift @{$binfo->{SKIPPED}};
        i::logit("SKIPPED INFO:\n".Dumper($skippedinfo)) if ++$main::ct < 5;
        $self->updateQItem($skippedinfo);
    }
	
    my @unames = sort keys %{$binfo->{folders}};
    i::logit("processing $bname Recipient list - ".scalar(@unames)." recipients");
	if ( !scalar(@unames) )
	{
		i::logit("processing $bname  - no file to produce.");
	    $self->endElab($binfo);
		$self->closeBundleLog($binfo);
		$self->clearOutPut($binfo);
		i::logit("Bundle ". join('::', @{$binfo}{qw(BundleName BundleCategory BundleSkel)})
				. "($binfo->{BundleKey}) process ended");
		return;
	}

#        warn "BINFO: ", Dumper($binfo), "\n";
    my $streamer = $self->initBundleOutput($binfo);

    $binfo->{Status} = 8;
#        $self->registerActivity($binfo);


    #my @unames = sort keys %{$binfo->{folders}};
    #i::logit("processing $bname Recipient list - ".scalar(@unames)." recipients");

    $binfo->{IndexArray} = [ map { map { sort {$a->{JobName} cmp $b->{JobName}}  @{$_->{jobs}} }
                                             values %{$binfo->{folders}->{$_}->{reports}} } @unames ];
    $binfo = $self->createBanner( 'BundleBanner',
                                    scalar(@{$binfo->{IndexArray}}) ?
                                               { %{$binfo->{IndexArray}->[0]}, %{$binfo} } : $binfo);
    $binfo->{ElabPages} += $binfo->{BannerPages};
    $binfo->{ElabLines} += $binfo->{BannerLines};
    $main::veryverbose and i::warnit("Calling streamer (", ref($streamer), ") to write ", scalar(@{$binfo->{BundleBannerHead}}), " bundle head lines");
    $streamer->write(join('', map { $self->packrec($_) } @{$binfo->{BundleBannerHead}} ));

    while ( scalar(@unames) ) {
        my $recipient = shift @unames;
        my $uinfo = delete $binfo->{folders}->{$recipient};
        $uinfo->{IndexArray} = [ map { sort {$a->{JobName} cmp $b->{JobName} }  @{$_->{jobs}} }
                                                                          values %{$uinfo->{reports}} ];
        next unless scalar(@{$uinfo->{IndexArray}});

        $uinfo = $self->createBanner( 'UserBanner',
                                    scalar(@{$uinfo->{IndexArray}}) ?
                                         { %{$uinfo->{IndexArray}->[0]}, %{$uinfo} } : $uinfo);
        $binfo->{ElabPages} += $uinfo->{BannerPages};
        $binfo->{ElabLines} += $uinfo->{BannerLines};
        $streamer->write(join('', map { $self->packrec($_) } @{$uinfo->{UserBannerHead}} ));
        while ( my ($jobname_userref, $rinfo) = each %{$uinfo->{reports}} ) {
          next unless (ref($rinfo) eq 'HASH' && exists($rinfo->{jobs}) && ref($rinfo->{jobs}) eq 'ARRAY');
#         $main::verbose and i::warnit("rinfo: ".ref($rinfo)." REPORTS: ".ref($rinfo->{jobs})." NUM: ".scalar(@{$rinfo->{jobs}}));
          my $jobs = [ sort {$a->{JobName} cmp $b->{JobName}} @{delete $rinfo->{jobs}} ];
          while ( scalar(@{$jobs})) {
            my $jinfo = shift @{$jobs};
            $jinfo = XReport::BUNDLE::setPrintInfo( $rinfo, $jinfo );
            # Setting odate and otime for actual report
            @{$jinfo}{qw (odate otime)} = unpack('A10x A8', $jinfo->{JobTimeRef});
			$jinfo->{odate}  = ($4.'/'.$3.'/'.$2) if( $jinfo->{odate} =~ /^([\d]{2})([\d]{2}).([\d]{2}).([\d]{2})$/ );
            #die "PROVA__".Dumper($jinfo);
            $jinfo = $self->createBanner( 'JobBanner', $jinfo);
            my $infile = $jinfo->{outfilename};
            #### Print copies management already in Banner Not necessary to insert for $jinfo->{PrintCopies} print data
			my $maxPrintCopies = (exists($self->{maxPrintCopies}) ? $self->{maxPrintCopies} : 4);
			if ($jinfo->{PrintCopies}  > $maxPrintCopies)
			{
				i::warnit("WARNING!!!!!!!!! PrintCopies forced to $maxPrintCopies. The original value was:".$jinfo->{PrintCopies});
				$jinfo->{PrintCopies} = $maxPrintCopies;
			}


            for my $p ( 1..$jinfo->{PrintCopies} ) {
			  $main::verbose and i::warnit(__PACKAGE__." - for PrintCopies - ($_)JobName:".$jinfo->{JobName} ."- UserRef:".$jinfo->{UserRef} ."- PrintCopies:".$jinfo->{PrintCopies} ."- JobNumber:".$jinfo->{JobNumber});
              $binfo->{ElabPages} += $jinfo->{BannerPages};
              $binfo->{ElabLines} += $jinfo->{BannerLines};
              if ( exists($jinfo->{JobBannerHead}) ) {
                $main::veryverbose and i::warnit("Calling streamer (", ref($streamer), ") to write "
                             .scalar(@{$jinfo->{JobBannerHead}})." job head lines at ".$streamer->tell());
                $streamer->write(join('', map { $self->packrec($_) } @{$jinfo->{JobBannerHead}} ));
              }
              $self->addReport2Bundle($infile,$jinfo,$streamer, $binfo) || die "Unable to open \"$infile\" - $!";

              $streamer->write(join('', map { $self->packrec($_) } @{$jinfo->{JobBannerTail}}));
              $binfo->{ElabPages} += $jinfo->{InputPages};
              $binfo->{ElabLines} += $jinfo->{InputLines};
              $jinfo->{PrintedPages} += $jinfo->{InputPages} if ($p == 1);
              $jinfo->{PrintedLines} += $jinfo->{InputLines} if ($p == 1);
            }

			#$jinfo->{PERMCopies} = -1 unless (defined($jinfo->{PERMCopies}));

			$self->updateQItem($jinfo);
          }
        }
		i::logit("UserBannerTail");
        $streamer->write(join('', map { $self->packrec($_) } @{$uinfo->{UserBannerTail}}))
    }
    $streamer->write(join('', map { $self->packrec($_) } @{$binfo->{BundleBannerTail}}));
    i::logit("Closing $binfo->{outfilename} for bundle $binfo->{BundleKey}");
    $streamer->close();
    $self->endElab($binfo);

    # register begin delivery
    $binfo->{Status} = 16;
    # try to delivery bundle
    $binfo->{Status} = ($self->bundleDelivery($binfo, $self->BundleFileSfx($binfo)) ? 32 : 63);
    # register result of delivery
    $self->closeBundleLog($binfo);
    $self->clearOutPut($binfo);
    #    while (scalar(@{$self->{logicalreports}}) ) { my $fn = shift @{$self->{logicalreports}}; unlink $fn;}
    i::logit("Bundle ". join('::', @{$binfo}{qw(BundleName BundleCategory BundleSkel)})
            . "($binfo->{BundleKey}) process ended");

}

#$main::veryverbose = 0;
#$main::verbose = 1;

my $bundle = XReport::BUNDLE->bundleInit();

my $blist = $bundle->getBundlesList();
print "Request will process ", scalar(@{$blist}), " Bundles Definitions\n";
while ( scalar( @{$blist} ) ) {
	my $belement = shift @{$blist};
	my $LockName = $belement->{BundleKey};
	my $activatedLogitForLock = 1;
	my $rc = localGetLock($LockName,$activatedLogitForLock);
	if($rc != 0)
	{
		i::logit("Resource $LockName holded by another process - no action to do.");
		localReleaseLock($LockName,$activatedLogitForLock);
		next;
	}
	i::logit("Resource $LockName holded by the current process.");

    #processBundleMission($bundle, $bundle->newBundleElab(shift @{$blist}));
    processBundleMission($bundle, $bundle->newBundleElab($belement));

	localReleaseLock($LockName,$activatedLogitForLock);
}
