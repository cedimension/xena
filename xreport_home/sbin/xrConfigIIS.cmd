@echo off
@setlocal EnableDelayedExpansion
@set _ServerName=CEWEB
@set _LogsDir=C:\WebLogs
@set _XRUser=BACRSM\ce
@set _XRPass=cececece
@set _XRDummy=\\cesql\xreport_siteconf\webroot

@call :createObject AppPools/XreportPool "IIsApplicationPool"
@set _ServerID=AppPools/XreportPool
@call :setObject WamUserName "%_XRUser%"
@call :setObject WamUserPass "%_XRPass%"
@call :setObject AppPoolIdentityType 3
@call :setObject PeriodicRestartTime 240
@call :setObject AppPoolAutoStart 1

@call :createWebServer 999001 XrServices 60080
@call SET _ServerID=999001/root
@call :setObject AnonymousUserName "%_XRUser%"
@call :setObject AnonymousUserPass "%_XRPass%"
@call :setObject AuthFlags 5
@call :setObject AuthBasic False
@call :setObject AuthAnonymous True
@call :setObject AuthNTLM True
@call :setObject AuthMD5 False
@call :setObject AuthPassport False
@call :createVDIR ResourceServer \\bac25\xreport\xrsitiweb\CeAdminWeb\IISWebServ xrIISWebServ.asp 
@call :createVDIR xrPrintIFace \\bac25\xreport\xrsitiweb\CeAdminWeb\IISWebServ xrIISWebServ.asp 
@call :createVDIR ConfigServer \\bac25\xreport\xrsitiweb\CeAdminWeb\IISWebServ xrIISWebServ.asp 

@call :createWebServer 999002 XrAdmin 8080
@call :createVDIR XrDefin \\bac25\xreport\xrsitiweb\CeAdminWeb\wwwdefin frameMain.html 
@call :createVDIR XrOper  \\bac25\xreport\xrsitiweb\CeAdminWeb\wwwoper  OperFrame.html 

@call :createWebServer 999003 XreportUser 80
@call :createVDIR XReport  \\bac25\xreport\xrsitiweb\CeUserWeb\wwwroot  frameUser.html 

@endlocal

@goto :EOF

:AdsUtil
@call set _action=%~1
@call set _objName=%~2
@call set _objValue=%3
@SHIFT
@SHIFT
@SHIFT

@echo -s:%_ServerName% %_action% W3SVC/%_ObjName% %_objValue% 
cscript /B c:\InetPub\AdminScripts\adsutil.vbs -s:%_ServerName% %_action% W3SVC/%_ObjName% %_objValue% 
@goto :EOF 

:createObject
@echo Creating object "%~2" NAME:%~1 
@call :AdsUtil CREATE %~1 "%~2"
@goto :EOF

:setObject
call @set _objName=%~1
call @set _objValue=%2
@SHIFT
@SHIFT
@echo Setting object %_objName% for %_ServerID%
@call :AdsUtil SET %_ServerID%/%_objName% %_objValue%
@goto :EOF

:createWebServer
@call set _ServerID=%1

@call :createObject %_ServerID% "IIsWebServer" 
@call :setObject ServerComment "%2" 
@call :setObject ServerBindings ":%3:"
@call :setObject ServerAutoStart True
@call :setObject ServerState 2
@call :setObject AuthFlags 0
@call :setObject AuthBasic False
@call :setObject AuthAnonymous False
@call :setObject AuthNTLM False
@call :setObject AuthMD5 False
@call :setObject AuthPassport False
@call :setObject LogFileDirectory "%_LogsDir%"
@call :setObject LogFilePeriod 1
@call :setObject LogFileLocaltimeRollover True
@call :setObject LogFileTruncateSize 20971520

@call :createObject %_ServerID%/filters "IIsFilters"

@call :createObject %_ServerID%/root "IIsWebVirtualDir"
@set _ServerID=%_ServerID%/root
@call :setObject AppRoot "/LM/W3SVC/%_ServerID%/Root"
@call :setObject AppFriendlyName "%2"
@call :setObject AppIsolated 2
@call :setObject Path "%_XRDummy%"
@call :setObject UNCUserName "%_XRUser%"
@call :setObject UNCPassword "%_XRPass%"
@call :setupVDIR
@call :setObject EnableDefaultDoc False
@call :setObject DirBrowseFlags 1073741886

goto :EOF

:createVDIR
@call :createObject %_ServerID%/%1 "IIsWebVirtualDir"
@call SET _ServerID=%_ServerID%/%1
@call :setObject AppRoot "/LM/W3SVC/%_ServerID%/%1"
@call :setObject AppFriendlyName "%1"
@call :setObject AppIsolated 2
@call :setObject Path "%2"
@call :setObject DefaultDoc "%3"
@call :setObject UNCUserName "%_XRUser%"
@call :setObject UNCPassword "%_XRPass%"
@call :setupVDIR
@goto :EOF

:setupVDIR
@call :setObject AccessFlags 517
@call :setObject AccessExecute True
@call :setObject AccessSource False
@call :setObject AccessRead True
@call :setObject AccessWrite False
@call :setObject AccessScript True
@call :setObject AccessNoRemoteExecute False
@call :setObject AccessNoRemoteRead False
@call :setObject AccessNoRemoteWrite False
@call :setObject AccessNoRemoteScript False
@call :setObject AccessNoPhysicalDir False
@call :setObject DirBrowseFlags 62
@call :setObject EnableDirBrowsing False
@call :setObject DirBrowseShowDate True
@call :setObject DirBrowseShowTime True
@call :setObject DirBrowseShowSize True
@call :setObject DirBrowseShowExtension True
@call :setObject DirBrowseShowLongDate True
@call :setObject EnableDefaultDoc True
@call :setObject AuthFlags 4
@call :setObject AuthBasic False
@call :setObject AuthAnonymous False
@call :setObject AuthNTLM True
@call :setObject AuthMD5 False
@call :setObject AuthPassport False
@call :setObject AppPoolId "XreportPool"
@goto :EOF

