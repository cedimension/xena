#!/usr/bin/perl

use strict;
use constant MCAST_GROUP => '226.1.1.9'; 
use constant MCAST_PORT => '3000'; 
use constant DESTINATION => "MCAST_GROUP:MCAST_PORT"; 
use constant SOCKBUFL => 64000;
use constant LOCLPORT => 12000;

use IO::Select;
use IO::Socket;

use lib "$ENV{XREPORT_HOME}/perllib";
use XReport::Multicast;

my $ifip = ( $ARGV[0] && $ARGV[0] ne '.' ) ? $ARGV[0] : '0.0.0.0';
my $usrport = ( $ARGV[1] && $ARGV[1] ne '.' ) ? $ARGV[1] : MCAST_PORT;
my $mcgroup = ( $ARGV[2] && $ARGV[2] ne '.' ) ? $ARGV[2] : MCAST_GROUP;
 
$|=1; 
#my $sock = new XReport::Multicast('LocalAddr' => '0.0.0.0', 'Proto' => 'udp', 'LocalPort' => MCAST_PORT, 'Reuse' => 1) or die "Can't create socket:$!"; 
my $sock = new XReport::Multicast('LocalAddr' => $ifip, 'LocalPort' => $usrport) or die "Can't create socket:$!"; 
$sock->mcast_add($mcgroup, $ifip);
my $retsock = IO::Socket::INET->new(Proto => 'udp');

my $sel = new IO::Select( $sock );
  
while (1) { 
  while ($sel->can_read( 2 )) {
    if ( my $peer = $sock->recv(my $msg,SOCKBUFL,MSG_PEEK) ) {
      $sock->recv(my $msg,SOCKBUFL);
      my ($port, $peeraddr) = sockaddr_in($peer) if $peer; 
      my $ltm = scalar(localtime()); 
      if ( $msg && $msg =~ /^\s*(20\d\d-\d\d-\d\d.\d\d:\d\d:\d\d(?:\.\d\d\d)?)\s*(.*)$/ ) {
        ($ltm, $msg) = ($1, $2);
      }
      print $ltm . " - ", inet_ntoa($peeraddr) . ":" . $port . ": $msg\n" if $msg; 
      
      if ($msg =~ /^.*\s\w+\sSTATUS\s(\d+)\D{0,1}$/i) {
	my $retport = $1;
	my $peerip = inet_ntoa($peeraddr);
	my $sin = sockaddr_in($retport, $peeraddr);
	print "PID $$ Sending back status to $peerip, $retport\n"; 
	$retsock->send("Status for $ENV{COMPUTERNAME} PID: $$", undef, $sin );
#	$retsock->send("Status for $ENV{COMPUTERNAME}", undef, $peerip.':'.$retport );
      }
    }
  } continue {
    #    print "Waiting for more data\n";
  }
  
  
#  print localtime() . " - No data in the last 2 Sec\n";
} 
