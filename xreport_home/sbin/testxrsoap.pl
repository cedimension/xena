#!/usr/bin/perl -w

use SOAP::Lite; # service => 'file:./xrIdxUpdate.wsdl';
#use SOAP::Serializer; # service => 'file:./xrIdxUpdate.wsdl';
 

use strict 'vars';
use Filehandle;
use File::Basename;
use Data::Dumper;
use XML::Simple;

use constant EF_PRINT => 1;

my ($server, $service, $method, $ifname) = @ARGV[0,1, 2, 3];
my $ResourceName = (split /\./, (split /[\\\/]/, $ifname)[-1])[0];
my $IndexEntries = XMLout({'IndexEntries' => [ {} ]}, KeepRoot => 1, NoIndent => 1);
my $NewEntries = XMLout({'NewEntries' => [ {} ] }, KeepRoot => 1, NoIndent => 1);

my $envelope = ( SOAP::Lite
		->on_action(sub{'"' . join('', @_). '"'})
		->encprefix('soapenc')
		->envprefix('soapenv')
		->autotype(0)
		->on_debug(sub{print @_, "\n"})
		->outputxml(1)
		->serializer->envelope(method => $method,
				       SOAP::Data->name('REQUEST')
				       ->attr( {IndexName => '_resources', TOP => 10, DocumentType => 'PDFFORM', FileName => $ResourceName} )
				       ->value( $IndexEntries . $NewEntries )
				      )
	       );

my ($msghdr, $msgtail) = ($envelope =~ /^(.*<\/(?:IndexEntries|NewEntries)>)(<\/REQUEST>.*)$/s);
$msghdr .= '<DocumentBody>';
$msgtail = '</DocumentBody>'.$msgtail;
print "envelope: ", length($msghdr)+length($msgtail), "\n", $msghdr.$msgtail, "\n";


my $inpdf = new FileHandle("<$ifname") || die "unable to open $ifname\n";
binmode $inpdf;
my $fsize = -s $ifname;
my $cache = $msghdr;
my $b64len = 57*76;
my $insize = $b64len*7;
my $outsize = $b64len + length($cache);
my $dataread = 0;
my $dataout = 0;
my $encdsize = 0;

sub readFully {
#  my ($fh, $cache) = (shift, shift);
  my $buffer = ''; my $outbuff = '';
  
  print "ReadFully: cache has ", length($cache), " bytes left - outsize is $outsize\n";
  if ($cache && length($cache) >= $outsize ) {
    ($outbuff, $cache) = unpack("a".$outsize." a*", $cache);
  } 

  elsif ($dataread < $fsize ) {
    my $rb = read($inpdf, $buffer, $insize);
    $dataread += length($buffer);
    print "ReadFully: $dataread of $fsize bytes read so far\n";
    
    foreach my $decdbuff ( unpack("(a".$b64len.")*", $buffer) ) {
      my $encdbuff = MIME::Base64::encode_base64($decdbuff);
      print "Shorter segment detected: ", length($decdbuff), " => ", length($encdbuff), "\n" if length($decdbuff) < $b64len; 
      $cache .= $encdbuff;
      $encdsize += length($encdbuff);
    }
    
    print "ReadFully: cache has now ", length($cache), " bytes $encdsize bytes encoded so far\n";
    ($outbuff, $cache) = unpack("a".$outsize." a*", $cache);
    $outbuff .= $msgtail if (!$cache && $dataread >= $fsize);
  } 

  elsif ( $cache ) {
    print "ReadFully: last cache has ", length($cache), " bytes left\n";
    ($outbuff, $cache) = ($cache . $msgtail, '');
  }
  
  else {
    $outbuff = '';
  }
  $dataout += length($outbuff);
  print "ReadFully - buff: ", length($outbuff), " bytes $dataout bytes returned so far (", 
    ($dataout - length($msghdr) - ($outbuff ? 0 : length($msgtail))), "/", 
      (length($msghdr) + ($outbuff ? 0 : length($msgtail))), ")\n";
  $outsize = $b64len;
  return $outbuff; 
}

use LWP::UserAgent;
my $ua = LWP::UserAgent->new;
my $URL = 'http://'.$server.'/'.$service.'/';

my $expected_length;
my $bytes_received = 0;
my $b64size = (((($fsize - ($fsize%3))/3) + (($fsize%3)**0))*4);
my $bytelength = length($msghdr) + length($msgtail) + $b64size + (((($b64size - ($b64size%76))/76) + (($b64size%76)**0)));
print "expected len: $bytelength\n";
my $req = HTTP::Request->new(POST => $URL, HTTP::Headers->new, \&readFully);

$req->authorization_basic('administrator', '...........');
$req->header('SOAPAction' => $method,
	     'Accept' => ['text/xml']
	    );
$req->content_type(join '; ', 'text/xml', ''); 
$req->content_length($bytelength);

my $res = $ua->request($req,
		       sub {
			 my($chunk, $res) = @_;
			 $bytes_received += length($chunk);
			 unless (defined $expected_length) {
			   $expected_length = $res->content_length || 0;
			 }
			 if ($expected_length) {
                        printf STDERR "%d%% - ",
			  100 * $bytes_received / $expected_length;
		      }
			 print STDERR "XX ==>>$bytes_received bytes received\n";
			 
			 # XXX Should really do something with the chunk itself
			 print $chunk;
		       });
print "bytes sent: $dataout Status Line: ", $res->status_line, "\n";

__END__

#my $IndexEntries = XMLout({'IndexEntries' => [{Columns => [{colname => 'CDG', Min => '99', Max => '999999'}]}]}, KeepRoot => 1, NoIndent => 1);
#my $IndexEntries = 
#'<IndexEntries>'
#.'<Columns colname="VAR1">FOINFDOC</Columns>'
#.'<Columns colname="GRU">000</Columns>'
#.'<Columns colname="CAT">000</Columns>'
#.'<Columns colname="DOC">ICFOIN</Columns>'
#.'<Columns colname="RIF">2006/11/30</Columns>'
#.'<Columns colname="PROD">2006/11/30</Columns>'
#.'<Columns colname="FIL">00099</Columns>'
#.'<Columns colname="CON">0000000</Columns>'
#.'<Columns colname="CDG">20030</Columns>'
#.'<Columns colname="VAR2">2006/00000130</Columns>'
#.'<Columns colname="ABI">9990</Columns>'
#.'</IndexEntries>';

my $envelope = ( SOAP::Lite   #->uri($server)
#		->service('file:./xrIdxUpdate.wsdl')
#		->service('http://'.$server.'/'.$service.'/?wsdl')
#		->proxy('http://'.$server.'/'.$service.'.asp')
#		->on_action(sub{'"' . $_[0] . '"'})
		->on_action(sub{'"' . join('', @_). '"'})
		->encprefix('soapenc')
		->envprefix('soapenv')
		->autotype(0)
		->on_debug(sub{print @_, "\n"})
		->outputxml(1)
		->serializer->envelope(method => $method,
				       SOAP::Data->name('REQUEST')
				       ->attr( {IndexName => '_resources', TOP => 10, DocumentType => 'PDFFORM', FileName => $ResourceName} )
				       ->value( $IndexEntries . $NewEntries )
				      )
);
