#!/usr/bin/perl -w

use lib "$ENV{XREPORT_HOME}/perllib";
use XReport;
use XReport::JobREPORT;

my $jobReportId = $ARGV[0];
my $outfn = $ARGV[1];
sub PutLine {
	my ($line) = (shift);

	$line = substr($line->AsciiValue(), 1); 
  
#	$line =~ s/([\(\)\\])/\\$1/g; return "$line\n";
	$line =~ s/\\([\(\)])/$1/g; return "$line\n";
}

sub getInputFile {
	my $input_content;

	eval {
		my $jobReportHandle = XReport::JobREPORT->Open($jobReportId);
		$jobReportHandle->setValues(ijrar => $jobReportHandle->get_INPUT_ARCHIVE(), ojrar => undef);
		my $input_handle = XReport::INPUT->new($jobReportHandle);
		while (my $page = $input_handle->GetPage()) {
	          $input_content .= "\x0c" if $input_content;
		  for (@{$page->lineList()}) {
			$input_content .= PutLine($_);
		  }
                }
		$input_handle->Close();
	};
	$errorMessage .= $@ if($@);

	return $input_content;
}

my $content = getInputFile();
open OUT, ">$outfn";
print OUT $content, "\n";
close OUT;
