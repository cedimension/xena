#!/usr/bin/perl -w

use lib "$ENV{XREPORT_HOME}\\perllib";

use XReport;
use XReport::JobREPORT;
use XReport::Resources;
use Data::Dumper;

exit unless $ARGV[0] && $ARGV[1];

my ($userlib, $workdir, $SrvName) = c::getValues(qw(userlib workdir SrvName)); 

my $resname = $ARGV[0];
$workdir = $ARGV[1] if $ARGV[1] && -d $ARGV[1];

my $resh = new XReport::Resources( destdir => "$workdir/localres"
				   ,TimeRef => '2010-12-31T23:59:59'
				 );
print "requesting  $resname Resource to be stored into $resh->{destdir}\n";
$resh->getResources(ResName => $resname, ResClass=>'prj');
my $resfn = "$resh->{destdir}/$resname.prj";
my $resfh = new FileHandle("<$resfn") || die "unable to open $resfn";
my $ressz = -s $resfn;
binmode $resfh;
$resfh->sysread(my $ResData, $ressz);
close $resfh;

print "File $resfn read: $ressz bytes - sending for Execute $resname Resource from filedata: ", length($ResData), " bytes\n";

(my $fname, $result) = $resh->execProject(ResName => "${resname}", ResClass => 'prj', ResFileData => $ResData); 
print "EXEC returned $fname - Result:", Dumper($result);

`start $fname`;

__END__
