#!/usr/bin/perl -w

package main;

use lib("$ENV{XREPORT_HOME}/perllib");

use strict;

use File::Basename;
use File::Path;
use File::Copy;
use FileHandle;

use XReport::Util;
use XReport::DBUtil;

#TODO: use POSIX time print func
(my $script = $0) =~ s/.pl$//;
($script) = pop @{ [split /[\\\/]/, $script] };
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
                                                localtime();
my $timid = sprintf('%04d%02d%02d%02d%02d%02d', $year+1900, $mon+1, $mday, $hour, $min, $sec);
my $daystring = sprintf('%04d-%02d-%02d', $year+1900, $mon+1, $mday);
use Win32::OLE;

#TODO: use config file to define step width
my $stepwidth = 400;   # ITEM per OGNI LOOP
my $outpath = 'DUMP'; # NOME LOGICO della LOCAZIONE dove vengono SPOSTATI i FILE  

$| = 1;
my $logfh;
my $logfnam = "${script}_${$}_${timid}_${outpath}_EXECMSGS.log";
$logfh = new FileHandle(">$logfnam");
die "unable to open logfile - $!" unless $logfh;
my $prevout = 
  select($logfh); $| = 1;
select $prevout;

sub logprint {
  print $logfh "$script ".localtime()." ", @_, "\n";
  print "$script ".localtime()." ", @_, "\n";
}

my ($debug, $daemon, $SrvName, $RemoteDB, $RemotePath, $LocalPath) = 
  getConfValues(qw(debugLevel daemonMode SrvName RemoteDB RemotePath LocalPath));
my $default_path = $LocalPath->{'L1'};

#-------------------------------------------

my %AllJobReports = (); 
logprint "Begin JobReport selection";
my $dbr = dbExecute("
SELECT * 
 FROM  tbl_JobReports jr
 left outer join tbl_jobreportnames jrn on jr.jobreportname = jrn.jobreportname
 WHERE jr.status = 16  and jrn.JobreportName is null
    ");

exit 0 if $dbr->eof();  

open BGNSTAT, ">${script}_${$}_${timid}_${outpath}_BGNSTAT.log";
open PROGRESS, ">${script}_${$}_${timid}_${outpath}_PROGRESS.log";
open RESULTS, ">${script}_${$}_${timid}_${outpath}_RESULTS.log";

my $stdout = 
  select(BGNSTAT); $| = 1;
select PROGRESS;   $| = 1;
select RESULTS;    $| = 1;
select $stdout;

while ( !$dbr->eof ) {   
  my $JRID = $dbr->Fields()->Item('JobReportId')->Value();
  my $pathidin = $dbr->Fields()->Item('LocalPathID_IN')->Value();
  $pathidin = 'L1' unless $pathidin;
  my $pathidou = $dbr->Fields()->Item('LocalPathID_OUT')->Value();
  $pathidou = 'L1' unless $pathidou;
  my ($fname, $fpath, $fsufx) = fileparse($dbr->Fields()->Item('LocalFileName')->Value(), "DATA\.TXT\.gz\.tar");
  (my $oldfnamin = $LocalPath->{$pathidin}) =~ s/^file:\/\///;
  (my $oldfnamou = $LocalPath->{$pathidou}) =~ s/^file:\/\///;


  $AllJobReports{$JRID} = [$oldfnamin.'/IN/', $oldfnamou.'/OUT/', $fpath, $fname];
  print BGNSTAT "SELECTED:\t$JRID\t$pathidin\t$pathidou\t$fpath\t$fname\n";
}continue {
  $dbr->MoveNext();
}
  
close BGNSTAT;

logprint "rename BEGINS For ", scalar(keys %AllJobReports), " JobReports";
UPDATE: while ( scalar(keys %AllJobReports )) {
  
  for ( 1..$stepwidth ) {
    last unless scalar(keys %AllJobReports);

    my ($JRID, $JINFO) = ( %AllJobReports  );
    my ($in, $ou, $fpath, $fname) = ( @$JINFO );

    my $fradix = $fpath.$fname;
    
    my $files = {$tgtin.$fradix.'DATA.TXT.gz.tar' => $in.$fradix.'DATA.TXT.gz.tar', 
         $tgtin.$fradix.'CNTRL.TXT' => $in.$fradix.'CNTRL.TXT', 
         $tgtou.$fradix.'OUT.#0.ZIP' => $ou.$fradix.'OUT.#0.ZIP', 
        };

    foreach my $filetgt ( keys %$files ) {
      my $filesrc = $files->{$filetgt};
      if ( -e $filesrc)  {
    unless (copy($filesrc, $filetgt) && -e $filetgt && (-s $filesrc == -s $filetgt) ) {
      logprint "Copy Failure for",  $filesrc, " to ", $filetgt, " - $!";
      foreach my $tgtfile ( keys %$files ) {
        unlink $tgtfile if -e $tgtfile;
      }
      last UPDATE;
    }
    my ($srcname, $srcpath) = fileparse($filesrc);
    my ($inpname, $inppath) = fileparse($filetgt);
    print PROGRESS "COPIED:\t$JRID\t$srcpath\t$srcname\t$inppath\n";
    $filecnt++;
      }
      
    }
    
    $JobReports{$JRID} = [$in, $ou, $fradix];
    delete $AllJobReports{$JRID};
    
  }
   
  last unless scalar(keys %JobReports);
  logprint scalar(keys %JobReports), " JRID processed - Updating DB";
  
  dbExecute("BEGIN TRANSACTION");
  $dbtran= 1;
  dbExecute("UPDATE    tbl_JobReports" 
        . " SET              LocalPathId_IN = '$outpath'"
        . ", LocalPathId_OUT = '$outpath'"
        . ", SrvName = 'DUMPD.".$daystring."'"
        . " where JobReportid IN (" . join(',', keys %JobReports) . ")"
       );
  my $vfy = dbExecute("select DISTINCT LocalPathId_IN+LocalPathId_OUT as dbpathid from tbl_JobReports where JobReportId IN (" .
        join(',', keys %JobReports) . ")");
  last UPDATE if $vfy->eof();
  $jobrcnt += scalar(keys %JobReports);
  my $dbpathid  = $vfy->Fields()->Item('dbpathid')->Value();
   
  $vfy->MoveNext();
  last UPDATE unless $vfy->eof();
  last UPDATE unless $dbpathid eq $outpath.$outpath;
  dbExecute("COMMIT TRANSACTION");
  $dbtran= 0;
  logprint scalar(keys %JobReports), " JRID processed - Update Completed - Deleting files";
  my $filedel = 0;
  foreach my $JRID ( keys %JobReports ) {
    my ($in, $ou, $fradix) = ( @{$JobReports{$JRID}} );
    my $files = [$in.$fradix.'DATA.TXT.gz.tar', 
         $in.$fradix.'CNTRL.TXT', 
         $ou.$fradix.'OUT.#0.ZIP' 
        ];
    foreach my $filesrc ( @$files ) {
      unlink $filesrc;
      $filedel++;
      print RESULTS "DELETED:\t$JRID\t$filesrc\n"; 
    }
    delete $JobReports{$JRID};
  }
  logprint $filedel, " files Deleted";
}

my $failmsg = '';
if (scalar(keys %JobReports )) {
  dbExecute("ROLLBACK TRANSACTION") if $dbtran;
  logprint "Process not completed Following reports have been copied, but db is not updated" ;
  map { logprint $_ } keys %JobReports;
  $failmsg = scalar(keys %JobReports) . " - files copied but db not updated";
}

if ( scalar(keys %AllJobReports) ) {
  logprint "Process not completed - Following reports have not been copied" ;
  map { logprint $_ } keys %AllJobReports;
  $failmsg = scalar(keys %AllJobReports) . " - files still to be copied";
}  

close PROGRESS;
close RESULTS;

logprint "Number of JobReports dumped: $jobrcnt - number of files moved: $filecnt\n" . $failmsg;
exit 0;
