@rem = q(--*-Perl-*--
@echo off
@SETLOCAL
@SET PERL5LIB=%~dp0..\perllib
@set today=%date:~6,4%-%date:~3,2%-%date:~0,2%
@C:\strawberry\perl\bin\perl.exe -x -S %0 %* -myself:%~n0 >>%~n0_%today%.log 2>&1
@rem perl -x -S %0 %* -myself:%~n0
exit /B %errorlevel%
goto :EOF
@rem );

#!perl -w

  use strict;
  use vars '@ARGV';
  
  #use IO::File;
  use IO::String;
  use File::Basename qw();
  use Data::Dumper;
  use POSIX qw();

  use Net::FTP;

  $| = 1;
  select STDERR;
  $| = 1;
  select STDOUT;
 
sub doLog {
  return print ''.POSIX::strftime('%Y-%m-%d %H:%M:%S ', localtime()), @_, "\n";
}
  my $parms = {  map { my ($n,$v) = split /:/, $_, 2; $v = 1 unless $v; substr($n,1) => $v } grep /^-/, @ARGV };
  doLog("Starting to execute $parms->{myself} ".($parms->{TXT} ? " - ($parms->{TXT})" : ''));
  if ( $parms->{check}) {
    doLog("Invoking check to enable proceeding - check: $parms->{check}");
    my $rc = system($parms->{check});
	$rc = $rc >> 8;
	doLog("Check procedure returned $rc - ".($rc ? 'Process terminates' : 'Process Continues'));
    exit 0 if $rc;
  }
  my ($scriptname) = split /\./, File::Basename::basename($0);
  my $ftpoutf = POSIX::strftime("${scriptname}_%Y%m%d_%H%M%S.data", localtime()); 
  #die Dumper(\{ARGV => \@ARGV, PERL5LIB => \@INC, ZERO => "$scriptname ($0) - $ftpoutf", parms => $parms});

  if ( !$parms->{JRN} || !$parms->{S} ) {
     doLog("usage: $0 -JRN:<JobReportName> -S:<XReport Server[:port]> [\"-TXT:<string to send as input>\"]");
	 exit -1;
  }
  $parms->{TXT} = "dummy" unless $parms->{TXT};
  my $infh = IO::String->new( $parms->{TXT} );
  $infh->open();
  my $ftpc = Net::FTP->new($parms->{S}, Debug => 9, Passive => 0);
  my $newmsg = $@;
  if ( !$ftpc ) {
    doLog("Cannot connect to server $parms->{S}: $newmsg");
    exit -1;
  }
  my $loginrc = $ftpc->login("Anonymous", "Anonymous\@$ENV{COMPUTERNAME}");
  if ( !$loginrc ) {
     doLog("Cannot login to server $parms->{S}: ".$ftpc->message());
     exit -1;
  }
  $ftpc->binary();
#  $ftpc->cwd($parms->{JRN}) or die "Cannot change to directory $parms->{JRN} ".$ftpc->message();
#  my $ftpmsg = (split(/\n/, $ftpc->message()))[0];
#  if ($ftpmsg !~ /^Current .* is now .*$parms->{JRN}/i) {
#    warn "$ftpmsg - expected: Current...is now...$parms->{JRN}";
#    exit 255;
#  }
  $ftpc->site('JOBNM='. $scriptname);
  $ftpc->site('JOBID=JOB'. $$);
  $ftpc->site('JORIGIN='. $ENV{COMPUTERNAME});
  $ftpc->site('PRMOD=ASCII');
  $ftpc->site('RemoteFileName='.$ftpoutf);
  my $remotefile = $ftpc->put($infh, $parms->{JRN});
  my $putmsg = $ftpc->message();
  $ftpc->quit();
  doLog("$putmsg");
__END__
  
  $lp->job_send_control_file($jobkey, {
          'N' => 'testLPR.pl',
          'form' => 'PIPPO',
          'block_offsets_ttr' => '5120',
          'JobName' => 'MIOJOB',
          'CRDTIM' => '2015-11-27T17:41:48.47',
          'data_block_size' => 131072,
          'FIXRecfm' => 1,
          'f' => 'dfA000PLUTO_SERV',
          'input_data_size' => 1582,
          'JobOrigin' => 'thor',
          'maxlrec_detected' => 512,
          'P' => 'PIPPETTO',
          'RemoteFileName' => 'testLPR.pl',
          'LRECL' => 512,
          'JNUM' => 'J123456',
          'JOBNM' => 'MIOJOB',
          'H' => 'thor',
          'ORIGIN' => 'thor',
          'data_file_ttr' => 4096,
          'JobNumber' => 'J123456',
          'fileformat' => 5,
          'OJBID' => 'J123456',
          'JNAM' => 'MIOJOB',
          'OWNER' => 'PIPPETTO'
  }) or die "Can't send control file: ".$lp->error."\n";
  $lp->job_send_data($jobkey, '', $size);
  
  while (!$fh->eof()) {
        $fh->read(my $buff, 32000);
        $lp->job_send_data($jobkey, $buff);
  }
  
  $lp->disconnect();