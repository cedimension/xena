#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use Carp ();
$SIG{__WARN__} = \&Carp::cluck;

use Win32 ();
use Storable ();

require XReport::Logger;

sub write2log { 
  XReport::Logger->log(" mod_perl_init $$ - ".join("\n\t$$ - ", @_));
}

use mod_perl2 ();
use Apache2::ServerUtil ();

use Data::Dumper;

use lib "$ENV{XREPORT_HOME}/perllib";
#use XReport;

$ENV{MOD_PERL} or die "not running under mod_perl!";
#use Apache::Registry ( );
#use LWP::UserAgent ( );
use Apache::DBI ( );
use DBI ( );

use Carp ( );
$SIG{__WARN__} = \&Carp::cluck;

(my $applpath = "$ENV{XREPORT_HOME}/xrsitiweb/CeAdminWeb/") =~ s/[\/\\]$//;
(my $parentpath = "$ENV{XREPORT_HOME}/xrsitiweb/") =~ s/[\/\\]$//;
my ($applname, $appluser, $scriptname ) = ($ENV{XREPORT_APPLNAME}, Win32->LoginName(), '' );
my $XREPORT_HOME = $ENV{'XREPORT_HOME'};
my $XREPORT_SITECONF = $ENV{'XREPORT_SITECONF'};
my $confvar = 1 if $XREPORT_SITECONF;

die("XREPORT_HOME VAR NOT SET") unless $XREPORT_HOME;
die "UNABLE TO ACCESS \"$XREPORT_HOME\"" if !-d $XREPORT_HOME;

$XREPORT_SITECONF = $XREPORT_HOME."\\userlib\\siteconf" unless $XREPORT_SITECONF;
#    die "UNABLE TO ACCESS \"$XREPORT_SITECONF\"" if !-d $XREPORT_SITECONF;

@{$main::Application}{qw(ApplUser ApplPath ApplName ApplParentPath XREPORT_HOME XREPORT_SITECONF)} = 
  ($appluser, $applpath, $applname, $parentpath, $ENV{XREPORT_HOME}, $ENV{XREPORT_SITECONF});

my @applperllib = ( "$XREPORT_HOME\\perllib" ); 
my $loclperllib = "$parentpath\\perllib";
unshift @applperllib, $loclperllib if ($parentpath &&  $XREPORT_HOME !~ /^$parentpath$/i && -d $loclperllib );  
$main::Application->{'ApplPerllib'} = join(';', @applperllib) if scalar(@applperllib);

require XReport::Config;

my $cfg = XReport::Config::loadConfig($appluser, $applname, 
				      "$XREPORT_SITECONF/$appluser/xml",
				      "$XREPORT_SITECONF/xml",
				      "$XREPORT_HOME/bin"
				     );

$cfg->{'workdir'} = $XREPORT_HOME.'/WORK' unless $cfg->{'workdir'};
mkdir $cfg->{'workdir'} unless -d $cfg->{'workdir'};
die "unable to access $cfg->{'workdir'}" unless -d $cfg->{'workdir'};
$main::Application->{'cfg.workdir'} = $cfg->{'workdir'};

$cfg->{'logsdir'} = $cfg->{'workdir'}.'/logs/webappls' unless $cfg->{'logsdir'};
die "unable to access $cfg->{'logsdir'}" unless -d $cfg->{'logsdir'};
$main::Application->{'cfg.logsdir'} = $cfg->{'logsdir'};

#<!--XRINDEX>PROVIDER=SQLOLEDB;DATA SOURCE=CESQL;DATABASE=xrindex;User Id=xreport;password=euriskom;</XRINDEX-->
#<XREPORT>PROVIDER=SQLOLEDB;Network Library=DBNMPNTW;DATA SOURCE=CESQL;Initial Catalog=xreport;Integrated Security=SSPI;</XREPORT>
while ( my ($db, $dbstr) = each %{$cfg->{dbase}} ) {
  my $cparms = { split /[\=;]/, $dbstr };
  $cparms->{'DATABASE'} = delete $cparms->{'Initial Catalog'} if exists($cparms->{'Initial Catalog'});
  $cparms->{'ACCESS'} = "Trusted_Connection=yes" if exists($cparms->{'Trusted_Connection'});
  $cparms->{'ACCESS'} = "Trusted_Connection=yes" if exists($cparms->{'Integrated Security'});
  my ($ukey) = grep /^user id$/, keys %$cparms;
  my ($pkey) = grep /^password$/, keys %$cparms;
  $cparms->{'ACCESS'} = "Uid=$cparms->{$ukey};Pwd=$cparms->{$pkey}" if $ukey;
  my ($server, $instance, $port) = split /[\\,]/, $cparms->{'DATA SOURCE'};
  $instance = $server unless $instance;
  $port = '1433' unless $port;

  $cfg->{odbcdb}->{$db} = "Driver=\{SQL Server\}"
    . ";Server=$server\\$instance"
    . ";Address=$server,$port"
    . ";Database=".$cparms->{'DATABASE'}
    . ";Network=DBMSSOCN"
    . ";".$cparms->{'ACCESS'}
    . ";APP=".$applname
    ;
}

# this will set one connection more
my $dbc = DBI->connect('dbi:ODBC:'.$cfg->{odbcdb}->{'XREPORT'});
my $rowset = $dbc->selectall_arrayref("SELECT * from tbl_IndexTables", {Slice => {}});
DBI::db::disconnect($dbc);
$dbc = undef;

foreach my $row ( @$rowset ) {
  $main::Application->{IndexDBName}->{$row->{IndexName}} = $row->{DatabaseName}
      if exists($cfg->{odbcdb}->{$row->{DatabaseName}});
}

Apache::DBI->connect_on_init('dbi:ODBC:'.$cfg->{odbcdb}->{'XREPORT'});

$main::Application->{'iis.cfg.perl'} = Storable::freeze($cfg);

my ($ListenPort, $ListenAddr) = @{$cfg}{qw(ListenPort ListenAddr)};
write2log("Apache Perl interface started - cmd: $0 - ARGV: ", join('::', @ARGV),
	"ENV: ", split(/\n/, Dumper($cfg->{daemon}->{$applname})),
	);

(my $docroot = "$XREPORT_HOME/xrsitiweb/webservice") =~ s/\\/\//g;
my $apache_cfg = ["Listen $ListenAddr\:$ListenPort", "ServerName $ListenAddr"];  #, "NameVirtualHost $ListenAddr"];
push @$apache_cfg, (''
		, "DocumentRoot $ENV{XREPORT_SITECONF}/webroot"
		, 'ServerPath /$applname'
		, "\<LocationMatch \".*\"\>" #^/(?!XReport-Status)\"\>"
		, "Order deny,allow"
		, "Deny from all"
		, "\</LocationMatch\>"
);

#Apache2::ServerUtil->server
#  ->add_config([
#		"\<Directory \"$docroot\">"
#		, "Order deny,allow"
#		, "Deny from all"
#		, "\</Directory\>"
#	       ]);
#;

push @$apache_cfg, (''
		  , "PerlSetVar ReloadDebug On"
#		  , "PerlModule Apache::DBI"
#		  , "PerlModule Apache2::Reload"
#		  , "PerlInitHandler Apache2::Reload"
#		  , "PerlSetVar ReloadDirectories \"$ENV{XREPORT_HOME}/bin/webservices $ENV{XREPORT_SITECONF}/userlib/perllib\""
#		  , "PerlSetVar ReloadAll Off"
#		  , "PerlSetVar ReloadModules \"$applname\""
		  , "\<LocationMatch \"/$applname/.*\"\>"
		  , "SetHandler perl-script"
		  , "PerlHandler XReport::SOAP::ApacheServer"
		  , "Order deny,allow"
		  , "Allow from .cedimension.org"
		  , "Allow from .thepezzis.org"
		  , "Allow from 169.254.2.2"
		  , "Allow from 192.168.1.13"
		  , "Allow from 192.168.1.103"
		  , "Deny from all"
		  , "\</LocationMatch\>"
		 );


push  @$apache_cfg, (''
		    , "\<LocationMatch  \".*/(?i:XReport-Status).*\>"
		    , "SetHandler perl-script"
		    , "PerlHandler XReport::ApacheStatus"
		    , "Order deny,allow"
		    , "Allow from .cedimension.org"
		    , "Allow from .thepezzis.org"
		    , "Allow from 192.168.1.13"
		    , "Allow from 169.254.2.2"
		    , "Deny from all"
		    , "\</LocationMatch\>"
		   ); 

write2log("Apache config:\n" . join("\n", @$apache_cfg) . "\n");

Apache2::ServerUtil->server()->add_config( $apache_cfg );


1;

__END__
