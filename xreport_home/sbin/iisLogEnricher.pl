#!/usr/bin/perl -w

use strict;
use POSIX qw(strftime);

use Win32::Job;

use FileHandle;
use File::Basename;
use File::Path;
use File::Copy;

use Data::Dumper;

$|= 1;
my $runargs = { map { split /=/, $_, 2 } @ARGV };

my $todaystr = POSIX::strftime("%y%m%d", localtime());
my $toDayFile = 'ex'.$todaystr.'.log';

my $mylogdir = $runargs->{LOGDIR};
die "Unable to access log  dir" unless -d $mylogdir;
my $myserver = $runargs->{SERVER};
die "Parameter SERVER in missing." unless $myserver;
my $parkdir = $mylogdir.'\processed';
mkpath $parkdir unless -d $parkdir;
unless ( -d $parkdir ) {
    doLog("Unable to access $parkdir - skipping to next webServer");
    next;
}

#my $hostname = $ENV{COMPUTERNAME};
my $hostname = $myserver;
my $mylogfqn = "$mylogdir/iisLogEnricher.ex$todaystr.log";
my $mylogfh = new FileHandle(">>$mylogfqn") || die "Unable to open $mylogfqn";
$mylogfh->autoflush();

sub doLog{
  print $mylogfh POSIX::strftime("%y%m%d %H:%M:%S", localtime()), " - ", @_, "\n";
}

$runargs->{DEST} = 'FTP:127.0.0.1' unless exists($runargs->{DEST});
my ($desttype, $logdest) = split /:/, $runargs->{DEST}, 2;

$desttype = 'FTP' unless $desttype;
my $deliveryRtn = main->can($desttype.'2XReport')
					|| die "Delivery Type $desttype not handled";

my $vdirs = {};

foreach my $appinfo ( split /\|/, $runargs->{APPLIST} ) {
  my ($applname, $infostr) = split /:/, $appinfo, 2;
  $applname = lc($applname);
  #$vdirs->{$applname} = [ $applname, split(/[,;]/, $infostr), $ENV{COMPUTERNAME} ];
  $vdirs->{$applname} = [ $applname, split(/[,;]/, $infostr), $myserver ]; 
  $vdirs->{$applname}->[2] =~ s/\\/_/g; 	
  my ($vdirname, $applpool, $applid, $appluser) = @{$vdirs->{$applname}};
  doLog "LogDir: $mylogdir TodayFile: $toDayFile pool: $applpool Vdir: $vdirname Appl: $applid ApplUser: $appluser";
}

sub get_status {
	my $currjob = shift;
	my $status = $currjob->status();
	my $jpid = shift @{ [(keys %$status)] };
	my $times = $status->{$jpid}->{time};
	my $statmsg = "JobMon $jpid STATUS - kernel: $times->{kernel}, User: $times->{user}, elapsed: $times->{elapsed}";
	doLog($statmsg);
}

sub JobMon {
	my $currjob = shift;
	get_status($currjob);
	return 0;
}

sub LPR2XReport {
  my ($outfqn, $logdest) = (shift, shift);
  my ($destaddr, $tgtfile) = split /\/\//, $logdest, 2;
#  print "LPR -S $logdest -P XRW3SLOG $outfn.$appluser -ojN$appluser\n";
#  return 0;
  my $scriptname = (split(/\./, (split(/[\\\/]/, $0))[-1]))[0];
  
  #  my $sh_obj = Win32::OLE->new("WScript.Shell");
  #  my $env_obj = $sh_obj->Environment("Process");
  #  $env_obj->{'XREPORT_HOME'} = $ENV{'XREPORT_HOME'};
  my ($logfn, $logdir, $sfx) = fileparse($outfqn);
  my $exit_code = 1;
  my $msgfn = "$logdir.$logfn.$scriptname.".$$;
  
  my $job = Win32::Job->new();
  
  doLog "Contacting $destaddr to send $outfqn";
  $job->spawn('LPR.EXE', "lpr -S $destaddr -P XRW3SVCLOG $outfqn", 
	      {       
	       #	     stdin  => 'NUL', # the NUL device
	       stdout => $msgfn.".stdout",
	       stderr => $msgfn.".stderr",
	      }
	     );
  
  $job->watch(\&JobMon, 10);
  
  get_status($job);
  
  my $jpid = shift @{[(keys(%{$job->status()}))]};
  $exit_code = $job->status()->{$jpid}->{'exitcode'};
  
  if($exit_code != 0 ) {
    doLog("Step closed abnormally with exit code $exit_code");
  } else {
    my $buff = '';
    my $fqn = "$msgfn.stdout";
    open MSGF, "<$fqn";
    read MSGF, $buff, -s $fqn if -s $fqn;
    close MSGF;
    my $msgs = $buff;
    unlink $fqn;
    $buff = '';
    $fqn = "$msgfn.stderr";
    open MSGF, "<$fqn";
    read MSGF, $buff, -s $fqn if -s $fqn;
    close MSGF;
    $msgs .= $buff;
    unlink $fqn;
    if ( $msgs =~ /error/im ) {
      doLog("LPR has reported an error- STDOUT/ERR follows");
      doLog($msgs);
      doLog("----------------------------------------------");
      $exit_code = 8;
    } else {
      doLog("LPR closed normally(".$job->status()->{$jpid}->{'exitcode'}.")");
    }
    doLog "Deleting Enriched file $outfqn";
    unlink $outfqn;
  }
  return $exit_code; 
  
}

sub FILE2XReport {
  my ($outfqn, $logdest) = (shift, shift);
  $logdest =~ s/^\/\///;
  my ($logfn, $logdir, $sfx) = fileparse($outfqn);
  if ( -d $logdest ) {
  	$logdest = $logdest.'/'.$logfn.'.'.$sfx;
  }
  move($outfqn, $logdest);
  doLog "Move of $outfqn to $logdest Failed" unless -e $logdest;
  return (-e $logdest ? 0 : 255);
}

sub FTP2XReport {
  my ($outfqn, $logdest) = (shift, shift);
  my ($destaddr, $tgtfile) = split /\/\//, $logdest, 2;
  my ($destusr, $destpwd, $destserv, $destport ) = ( $destaddr =~ /^(?:([^\@\:])(?:\:([^\@]+))?\@)?([^\:]+)(?:\:(\d+))?$/);
  $destusr = $ENV{USERNAME} unless $destusr;
  $destpwd = $destusr.'@'.$ENV{COMPUTERNAME};
  $destserv = '127.0.0.1' unless $destserv;
  $destport = 21 unless $destport;
  $tgtfile = 'XRW3SVCLOG' unless $tgtfile;
  
#  print "LPR -S $logdest -P XRW3SLOG $outfn.$appluser -ojN$appluser\n";
#  return 0;
  my $scriptname = (split(/\./, (split(/[\\\/]/, $0))[-1]))[0];
  
  #  my $sh_obj = Win32::OLE->new("WScript.Shell");
  #  my $env_obj = $sh_obj->Environment("Process");
  #  $env_obj->{'XREPORT_HOME'} = $ENV{'XREPORT_HOME'};
  my ($logfn, $logdir, $sfx) = fileparse($outfqn);
  my $exit_code = 1;
  my $msgfn = "$logdir.$logfn.$scriptname.".$$;
  my $cmdfqn = "$logdir.$logfn.$scriptname.".$$.".ftp";
  (my $ftpoutf = $outfqn) =~ s/\//\\/g;
  $outfqn =~ s/\\/\//g;

  open FTPCMD, ">$cmdfqn";
  print FTPCMD 'QUOTE SITE RemoteFileName=', $outfqn, "\n";
  print FTPCMD 'QUOTE SITE JOBNM=', $scriptname, "\n";
  print FTPCMD 'QUOTE SITE JOBID=JOB', $$, "\n";
  print FTPCMD 'QUOTE SITE JORIGIN=', $ENV{COMPUTERNAME}, "\n";
  print FTPCMD 'ASCII', "\n";
  print FTPCMD 'quote site CT=MIME::text/xml', "\n";
  print FTPCMD 'PUT ', $ftpoutf, " ", $tgtfile, "\n";
  print FTPCMD 'QUIT', "\n";
  close FTPCMD;

  my $job = Win32::Job->new();
  
  doLog "Contacting $destserv to send $outfqn";
  $job->spawn('FTP.EXE', "ftp -d -A -s:$cmdfqn $destserv ", 
	      {       
	       #	     stdin  => 'NUL', # the NUL device
	       stdout => $msgfn.".stdout",
	       stderr => $msgfn.".stderr",
	      }
	     );
  
  $job->watch(\&JobMon, 10);
  
  get_status($job);
  
  my $jpid = shift @{[(keys(%{$job->status()}))]};
  $exit_code = $job->status()->{$jpid}->{'exitcode'};
  unlink $cmdfqn;

  if($exit_code != 0) {
    doLog("Step closed abnormally with exit code $exit_code");
  } else {
    my $buff = '';
    my $fqn = "$msgfn.stdout";
    open MSGF, "<$fqn";
    read MSGF, $buff, -s $fqn if -s $fqn;
    close MSGF;
    my $msgs = $buff;
    unlink $fqn;
    $buff = '';
    $fqn = "$msgfn.stderr";
    open MSGF, "<$fqn";
    read MSGF, $buff, -s $fqn if -s $fqn;
    close MSGF;
    $msgs .= $buff;
    unlink $fqn;
    if ( $msgs =~ /not connected|error|Store\s+rejected/im ) {
      doLog("FTP has reported an error - STDOUT/ERR follows");
      doLog($msgs);
      doLog("----------------------------------------------");
      $exit_code = 8;
    } else {
      doLog("FTP closed normally(".$job->status()->{$jpid}->{'exitcode'}.")");
      doLog($msgs);
      doLog("----------------------------------------------");
    }
    doLog "Deleting Enriched file $outfqn";
    unlink $outfqn;
  }
  return $exit_code; 
  
}

my $cfg = { };

my $apppools = {};
my %outinfo = ();

#my @logs = <"$mylogdir/u_ex*.log">; 
#use File::Glob qw( :bsd_glob );
#@logs = map { glob } $mylogdir/u_ex*.log ;

opendir my $dir, $mylogdir or die "Cannot open directory: $!"; 
my @logs = grep /^u_ex.*\.log$/i, readdir $dir;
closedir $dir; 
 
doLog("Init foreach Loop $mylogdir".Dumper("array",@logs));  
foreach my $baselogfqn ( @logs ) {
  my $logfqn = "$mylogdir/$baselogfqn";
  doLog("logfqn=$logfqn");  
  if ( $logfqn =~ /$toDayFile$/i ) {
    doLog("Skipping $logfqn as current log file");
    next;
  }
  
  my ($webloc, $logfn) = (split(/[\\\/]/, $logfqn))[-2, -1];
  my $outfqn = "$mylogdir/enriched.$hostname.$webloc.$logfn";
  doLog "now reading from $logfqn";
  
  open LOG, "<$logfqn" or die "Cannot open file log $mylogdir.'/'.$logfqn: $!"; 

  my $logflds = [];
  my $logentry = {};
  my $lct = 0;
  my %users = ();
  my %lastflds =();
  my $currflds = '';

  while (<LOG>) {

    if (/^#fields:\s+(\w.*)$/i) {
		(my $flds = $1) =~ s/[\(\)]/-/g;
		@$logflds = split(/-?\s/, $flds);
		$currflds = "#fields: VDIR POOLNAME APPLID POOLUSER COMPUTERNAME " . join(' ', @$logflds);
		next;
    }
    next unless scalar(@$logflds);
    next if /^#/;

    @{$logentry}{(@$logflds)} = split /\s/, $_;
    $logentry->{'cs-uri-stem'} =~ s/^\///;
    my @uri = map { lc($_) }split(/\//, $logentry->{'cs-uri-stem'}, 2);
    my ($vname, $request) = (scalar(@uri) != 2 ? ('root', $uri[0]) : (@uri[0,1]));
#use Data::Dumper;
#die Dumper($vdirs);
	unless (exists($vdirs->{$vname}) ) {
		warn "ApplName $vname not found in selection - skipping record\n";
		next;
	}
    my $appluser = $vdirs->{$vname}->[2];
    my $line = uc(join("\t", (@{$vdirs->{$vname}}, @{$logentry}{(@$logflds)})));
    unless (exists($users{$appluser})) {
		doLog "now opening $outfqn.$appluser";
		$users{$appluser} = new FileHandle(">$outfqn".'.'.$appluser);
		$lastflds{$appluser} = '';
    }
 #   die Dumper(\%users);
    my $outfh =  $users{$appluser};
    if ($lastflds{$appluser} ne $currflds) {
		print $outfh $currflds, "\n";
		$lastflds{$appluser} = $currflds;
    }
    print $outfh $line, "\n";
  }
  close LOG;

  doLog "Start moving of $logfqn from $mylogdir to $parkdir";
  
  my $parkfqn = $parkdir.'/'.$logfn;
  move($logfqn, $parkfqn) or die "Cannot move file $logfqn to $parkfqn: $!"; 
  doLog "Move of $logfn from $mylogdir to $parkdir Failed" unless -e $parkfqn;

  foreach my $appluser (keys %users) {
    my $outfh =  $users{$appluser};
    close $outfh;
    if ( -e $parkfqn ) {
    	if (&$deliveryRtn($outfqn.'.'.$appluser, $logdest) != 0 ) {
			move($parkfqn, $logfqn);
			die "Delivery to ${desttype}:${logdest} failed - process aborted for $logfqn";
    	}
    }
  }
}

exit 0;
