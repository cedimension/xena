/* REXX **************************************************************/
/*                                                                   */
/*********************************************************************/
/* trace 'r'  */
parse arg xrhost repnames
jdict = getJobInfo()
dddict = getDDList()
 /*
do while jdict <> ''
  parse var jdict el jdict

  say el":" value('jinfo.'el)
end

say "DDS:" dddict
do while dddict <> ''
  parse var dddict el dddict
  say el":" value('ddlist.'el)
end
  */
ds2send = 0
dssentok = 0
do i=0 while repnames <> ''
  parse var repnames repformat reportname repnames
  if  repformat <> 'PSF' &,
      repformat <> 'FTPC' &,
      repformat <> 'AFPSTREAM' &,
      repformat <> 'ASCII',
                     then   iterate i

  if symbol('ddlist.CEREPDD'i) <> 'VAR' then iterate
  dslist = value('ddlist.CEREPDD'i)
  do j=0 while dslist <> ''
    parse var dslist dsname dsorg recfm dslist
    ds2send = ds2send + 1
    if repformat <> 'PSF' | abbrev(recfm, 'VB', 2) then do
      sndrc = send2Cereport(xrhost,repformat,reportname,dsname)
      if sndrc = 0 then dssentok = dssentok + 1
    end
    else do
       say "Only RECFM=VB/VBS is supported for" repformat
       say "DATASET" dsname "Has RECFM="recfm "- discarded"
       iterate j
    end
  end
end

if dssentok = 0 then exit 8
if ds2send == dssentok then exit 0
else exit 4

send2Cereport: procedure expose jinfo. ddlist.
parse arg xrhost,repformat,reportname,dsname
parse var xrhost xrhost':'ftpmode .

  "newstack"
  if symbol('DDLIST.NETRC') <> 'VAR' then do
    queue jinfo.userid 'notspecified'
  end
  if ftpmode == 'PASV' then,
      queue "LOCSITE FWF"
  queue "QUOTE SITE JORIGIN="jinfo.sysid
  queue "QUOTE SITE OWNER="jinfo.userid
  queue "QUOTE SITE JOBNM="jinfo.jobname
  queue "QUOTE SITE JOBID="jinfo.jobtype||jinfo.jobnum
  queue "QUOTE SITE PRMOD="repformat
  queue "QUOTE SITE RemoteFilename="dsname
  queue "LOCSITE automount"
  queue "binary"
  if repformat == 'PSF' then,
      queue "LOCSITE rdw"
  else if repformat == 'FTPC' then do
    queue "MODE C"
    queue "EBCDIC"
    end
  queue "put '"dsname"'" reportname
  queue "quit"
  queue ""
/* call FTP using stack as input                                      */
  address TSO "ftp -i" xrhost "(exit "

   ftpcode = rc                     /* FTP returns 5 digit returncode */
   ftpcode = right(ftpcode,5,'0')
   ftpscmd = substr(ftpcode,1,2)    /* subcommand = 1st 2 digits      */
   ftprply = substr(ftpcode,3,3)    /* reply code = last 3 digits     */
                          /* display subcommand and reply code        */
   say "*****************************"
   say "FTP SUBCOMMAND = " ftpscmd
   say "FTP REPLY CODE = " ftprply
   say "*****************************"
   ftpretcd = ''
   select
          when ftpscmd = "00"  then ftpretcd = 0
          when ftprply = "110" then ftpretcd = 0
          when ftprply = "120" then ftpretcd = 0
          when ftprply = "125" then ftpretcd = 0
          when ftprply = "150" then ftpretcd = 0
          when ftprply = "200" then ftpretcd = 0
          when ftprply = "211" then ftpretcd = 0
          when ftprply = "212" then ftpretcd = 0
          when ftprply = "213" then ftpretcd = 0
          when ftprply = "214" then ftpretcd = 0
          when ftprply = "215" then ftpretcd = 0
          when ftprply = "220" then ftpretcd = 0
          when ftprply = "221" then ftpretcd = 0
          when ftprply = "226" then ftpretcd = 0
          when ftprply = "230" then ftpretcd = 0
          when ftprply = "250" then ftpretcd = 0
          when ftprply = "257" then ftpretcd = 0
          when ftprply = "331" then ftpretcd = 0
          when ftprply = "332" then ftpretcd = 0
          when ftprply = "550" then ftpretcd = 4
   otherwise ftpretcd = 8
   end
   if ftpretcd > 4 then
   do
     "delstack"           /* delete unused commands from stack        */
     exit ftpretcd                  /* exit when FTP call in error    */
   end
/* ****************************************************************** */
   "delstack"             /* delete unused commands from stack        */

return ftpretcd
/*********** �REFRESH BEGIN JOBINFO  2002/09/11 01:12:59 *************/
/* getJOBINFO  - Get job related data from control blocks          */
/*-------------------------------------------------------------------*/
/* N/A      - None                                                   */
/*********************************************************************/
getJobInfo: procedure expose jinfo.
/*********************************************************************/
/* Chase control blocks                                              */
/*********************************************************************/
          numeric digits(32)

          tcb      = ptr(540,4)                    /* PSATOLD in PSA */
          tiot     = ptr(tcb+12,4)                  /* TCBTIO in TCB */
          ascb     = ptr(548,4)
          jscb     = ptr(tcb+180,4)                /* TCBJSCB in TCB */
          ssib     = ptr(jscb+316,4)             /* JSCBSSIB in JSCB */
 /*       jct      = ptr(jscb+260,4)    */                /* jscbjct */
          jct      = swareq(stg(jscb+260,4))     /* JSCBJCTA in JSCB */
 /*       act      = ptr(jct+56,3)      */               /* actprgnm */
          act      = ptr(jct+40,3)                /* JCTACTAD in JCT */
          jinfo.sysid    = MVSVAR('SYSNAME')
          jinfo.userid   = USERID()
          jinfo.jobname  = strip(stg(tiot,8))            /* TIOCNJOB */
/*          jobname        = stg(jct+8,8) */
          jinfo.prgnm    = strip(stg(act+24,20))
          jinfo.asid     = c2d(stg(ascb+36,2))
          jinfo.jobtype  = stg(ssib+12,3)
          jinfo.jobnum   = strip(stg(ssib+15,5),'L',0)
          jinfo.msgclass = stg(jct+6,1)
          jinfo.stepname = strip(stg(tiot+8,8))
          jinfo.procstep = strip(stg(tiot+16,8))
          jinfo.program  = strip(stg(jscb+360,8))
          jinfo.dict  = 'sysid userid jobname jobtype jobnum prgnm',
                        'msgclass stepname procstep program asid'
/*********************************************************************/
/* Return job data                                                   */
/*********************************************************************/
          return jinfo.dict
/*********** �REFRESH END   JOBINFO  2002/09/11 01:12:59 *************/
/*********** �REFRESH BEGIN getdsnl  2002/07/13 15:45:36 *************/
/*           get 1st dsn(mem) for ddname                             */
/*********************************************************************/
getDDList: procedure expose ddlist.
        ddlist.dict = ''
    /*  ddname=translate(left(Arg(1),8)) */
        tiotptr=24+ptr(12+ptr(ptr(ptr(16,4),4),4),4)
        tioelngh=c2d(stg(tiotptr,1))         /* Length of 1st entry */
        Do Until tioelngh=0                  /* Scan until dd found */
          tioeddnm=strip(stg(tiotptr+4,8))   /* Get ddname from tiot */
          if c2x(substr(tioeddnm,1,1)) == '00' then
             return ddlist.dict
/*        If tioeddnm = ddname Then Do        */
            tioelngh=c2d(stg(tiotptr,1)) /* Length of next entry */
            tioejfcb=stg(tiotptr+12,3)
            jfcb=swareq(tioejfcb) /* Convert sva to 31-Bit addr */
            dsname=strip(stg(jfcb,44)) /* Dsname jfcbdsnm */
            member=strip(stg(jfcb+44,8)) /* member name */
            if tioeddnm <> '' then,
              rcld = listdsi(tioeddnm "FILE")
            else
              rcld = listdsi("'"dsname"'")
            if rcld <> '0' then,
               parse value 'N/A N/A' with sysdsorg sysrecfm
            If member <> '' Then,
              dsname=dsname'('member')'
            if tioeddnm <> '' then do
              ddlist.dict = ddlist.dict tioeddnm
               ddlist.tioeddnm = dsname sysdsorg sysrecfm
               prevdd = tioeddnm
            end
            else do
               tioeddnm = prevdd
               ddlist.tioeddnm = ddlist.tioeddnm,
                                     dsname sysdsorg sysrecfm
            end
      /*    Return dsname */
      /*  End */
          tiotptr=tiotptr+tioelngh /* Get next entry */
          tioelngh=c2d(stg(tiotptr,1)) /* Get entry length */
        End
        Return ddlist.dict
/*********** �REFRESH BEGIN SWAREQ   2002/07/13 15:45:36 *************/
/* PTR      - Pointer to a storage location                          */
/*-------------------------------------------------------------------*/
swareq: procedure
           if right(c2x(arg(1)),1) <> 'F' then,       /* SWA=BELOW ? */
                   return c2d(arg(1))+16       /* Yes, return SVA+16 */
           sva = c2d(arg(1))                   /* Convert to decimal */
           tcb = ptr(540,4)                           /* TCB PSATOLD */
           jscb = ptr(tcb+180,4)                     /* JSCB TCBJSCB */
           qmpl = ptr(jscb+244,4)                   /* QMPL JSCBQMPI */
           qmat = ptr(qmpl+24,4)                       /* QMAT QMADD */
           do while sva > 65536
                   qmat = ptr(qmat+12,4)        /* Next QMAT QMAT+12 */
                   sva=sva-65536            /* 010006F -&gt; 000006F */
                   end
           return ptr(qmat+sva+1,4)+16
/*********** �REFRESH END   SWAREQ   2002/07/13 15:45:36 *************/
/*********** �REFRESH BEGIN PTR      2002/07/13 15:45:36 *************/
/* PTR      - Pointer to a storage location                          */
/*-------------------------------------------------------------------*/
/* ARG(1)   - Storage Address                                        */
/*********************************************************************/
 ptr: procedure
 return c2d(storage(d2x(arg(1)),arg(2)))
/*********** �REFRESH END   PTR      2002/07/13 15:45:36 *************/
/*********** �REFRESH BEGIN STG      2002/07/13 15:49:12 *************/
/* STG      - Return the data from a storage location                */
/*-------------------------------------------------------------------*/
/* ARG(1)   - Location                                               */
/* ARG(2)   - Length                                                 */
/*********************************************************************/
 stg: procedure
 return storage(d2x(arg(1)),arg(2))
/*********** �REFRESH END   STG      2002/07/13 15:49:12 *************/