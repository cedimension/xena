/****** Object:  Table [dbo].[tbl_Resources]    Script Date: 11/11/2009 00:23:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Resources](
	[ResGroup] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ResClass] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ContentType] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MD5_HASH] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RemoteOrigin] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[XferStartTime] [datetime] NULL,
	[XferEndTime] [datetime] NULL,
	[ResFileData] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF