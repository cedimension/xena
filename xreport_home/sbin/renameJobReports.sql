Update tbl_JobReports 
set JobReportName = Descrs.NewReportName
FROM tbl_JobReports AS ujrn  
JOIN ( SELECT attrs.JobReportName as OldReportName, attrs.Job + '.__' + DD as NewReportName
FROM (
SELECT jr.JobReportName, 
  (CASE CHARINDEX('.', jr.JobReportName, 1)
        WHEN 0 then jr.JobReportName
        ELSE SUBSTRING(jr.JobReportName, 1, charindex('.', jr.JobReportName, 1) - 1) 
   END) as Job, 
  (CASE CHARINDEX('._S', jr.JobReportName, 1)
        WHEN 0 then ''
        ELSE CASE charindex('.', jr.JobReportName, CHARINDEX('._S', jr.JobReportName, 1) + 3)
          WHEN 0 THEN SUBSTRING(jr.JobReportName, CHARINDEX('._S', jr.JobReportName, 1) + 3, LEN(jr.JobReportName)) 
          ELSE SUBSTRING(jr.JobReportName, CHARINDEX('._S', jr.JobReportName, 1) + 3, charindex('.', jr.JobReportName, CHARINDEX('._S', jr.JobReportName, 1) + 3) - (CHARINDEX('._S', jr.JobReportName, 1) + 3)) 
        END
   END) as Step, 
  (CASE CHARINDEX('._C', jr.JobReportName, 1)
        WHEN 0 then ''
        ELSE CASE charindex('.', jr.JobReportName, CHARINDEX('._C', jr.JobReportName, 1) + 2)
          WHEN 0 THEN SUBSTRING(jr.JobReportName, CHARINDEX('._C', jr.JobReportName, 1) + 3, LEN(jr.JobReportName)) 
          ELSE SUBSTRING(jr.JobReportName, CHARINDEX('._C', jr.JobReportName, 1) + 3, charindex('.', jr.JobReportName, CHARINDEX('._C', jr.JobReportName, 1) + 3) - (CHARINDEX('._C', jr.JobReportName, 1) + 3)) 
        END
   END) as Class,
  (CASE CHARINDEX('._W', jr.JobReportName, 1)
        WHEN 0 then ''
        ELSE CASE charindex('.', jr.JobReportName, CHARINDEX('._W', jr.JobReportName, 1) + 3)
          WHEN 0 THEN SUBSTRING(jr.JobReportName, CHARINDEX('._W', jr.JobReportName, 1) + 3, LEN(jr.JobReportName)) 
          ELSE SUBSTRING(jr.JobReportName, CHARINDEX('._W', jr.JobReportName, 1) + 3, charindex('.', jr.JobReportName, CHARINDEX('._W', jr.JobReportName, 1) + 3) - (CHARINDEX('._W', jr.JobReportName, 1) + 3)) 
        END
   END) as Writer,
  (CASE CHARINDEX('._D', jr.JobReportName, 1)
        WHEN 0 then ''
        ELSE CASE charindex('.', jr.JobReportName, CHARINDEX('._D', jr.JobReportName, 1) + 3)
          WHEN 0 THEN SUBSTRING(jr.JobReportName, CHARINDEX('._D', jr.JobReportName, 1) + 3, LEN(jr.JobReportName)) 
          ELSE SUBSTRING(jr.JobReportName, CHARINDEX('._D', jr.JobReportName, 1) + 3, charindex('.', jr.JobReportName, CHARINDEX('._D', jr.JobReportName, 1) + 3) - (CHARINDEX('._D', jr.JobReportName, 1) + 3)) 
        END
   END) as Dest,
  (CASE CHARINDEX('.__', jr.JobReportName, 1)
        WHEN 0 then ''
        ELSE CASE charindex('.', jr.JobReportName, CHARINDEX('.__', jr.JobReportName, 1) + 3)
          WHEN 0 THEN SUBSTRING(jr.JobReportName, CHARINDEX('.__', jr.JobReportName, 1) + 3, LEN(jr.JobReportName)) 
          ELSE SUBSTRING(jr.JobReportName, CHARINDEX('.__', jr.JobReportName, 1) + 3, charindex('.', jr.JobReportName, CHARINDEX('.__', jr.JobReportName, 1) + 3) - (CHARINDEX('.__', jr.JobReportName, 1) + 3)) 
        END
   END) as DD 
 FROM  tbl_JobReports jr
 left outer join tbl_JobReportNames jrn on jr.JobReportName = jrn.JobReportName
 WHERE jr.status = 16  and jrn.JobReportName is null
) as attrs ) as Descrs on ujrn.JobReportName = Descrs.OldReportName