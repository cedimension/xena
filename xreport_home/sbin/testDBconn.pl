#!/usr/bin/perl -w

use strict;
use Win32::OLE;
use Win32::OLE::Variant;
use Digest::MD5;
$|=1;

use XReport;
my $cstr_OMS = $XReport::cfg->{dbase}->{XREPORT};
print "Configured Connection String:\n$cstr_OMS\n";

##-------------------------------------------
#my $cstr_OMS =  "Provider=SQLOLEDB;Data Source=EQTRISSTSQL01;Initial Catalog=CERIT_xreport;Integrated Security=SSPI;";
#my $cstr_OMS =  "PROVIDER=SQLOLEDB;Network Library=DBNMPNTW;DATA SOURCE=EQTRISSTSQL01;Initial Catalog=SOGEI_xreport;Integrated Security=SSPI;Application Name=oper;";
#my $cstr_OMS =  "PROVIDER=SQLOLEDB;Network Library=DBNMPNTW;DATA SOURCE=EQTRISSTSQL01\EQTRISSTSQL01,1433;Initial Catalog=SOGEI_xreport;Trusted_Connection=Yes;Application Name=oper;";
#my $cstr_OMS =   "Provider=SQLNCLI10.0;Network Library=DBMSSOCN;Integrated Security=SSPI;Initial Catalog=sogei_xreport;Data Source=EQTRISSTSQL01;DataTypeCompatibility=80";
#my $cstr_OMS =   "Provider='SQLNCLI10';Integrated Security='SSPI';Initial Catalog='sogei_xreport';Data Source='EQTRISSTSQL01';DataTypeCompatibility=80;Persist";
#my $cstr_OMS =   'PROVIDER=SQLOLEDB;Data Source=EQTRISSTSQL01,1433;Initial Catalog=sogei_xreport;Integrated Security=SSPI;Use Procedure For Prepare=0;Packet Size=1024;';
#My $h=win32::Ole->new("ADODB.Connection");$h->Open("PROVIDER=SQLOLEDB;Data Source=panoramix,3653;Database=bacprod_xreport;Integrated Security=SSPI")
#my $cstr_OMS = "Driver={SQL Server};"
#		       . "Server=EQTRISSTSQL01\\EQTRISSTSQL01;" 
#		       . "Address=EQTRISSTSQL01,1433;" 
#		       . "Network=DBMSSOCN;" #tcpip 
#		       . "Network=DBNMPNTW;" #Named Pipes 
#		       . "Network=dbmsrpcn;" #multi Protocol 
#		       . "Database=sogei_xreport;" 
#		       . "Trusted_Connection=yes,APP=oper";
#my $cstr_OMS =  "Provider=SQLOLEDB;Network Library=DBMSSOCN;Data Source=26.2.62.112\EQTRISSTSQL01,1433;Integrated Security=SSPI;User ID=EQTRISORSE\xreportaccount;Password=abcdABCD1234;";
#my $cstr_OMS =  "Provider=SQLOLEDB;Network Library=DBNMPNTW;Data Source=26.2.62.112\\EQTRISSTSQL01;Integrated Security=SSPI;User ID=EQTRISORSE\\xreportaccount;Password=abcdABCD1234;";
#my $cstr_OMS =  "Provider=SQLOLEDB;Network Library=DBMSSOCN;Data Source=26.2.62.112\\EQTRISSTSQL01,1433;Integrated Security=SSPI;User ID=EQTRISORSE\\xreportaccount;Password=abcdABCD1234;";
#my $cstr_OMS =  "Provider=SQLOLEDB;Network Library=DBMSSOCN;Data Source=26.2.62.112\\EQTRISSTSQL01,1433;Trusted_Connection=yes"; #tcpip
#my $cstr_OMS =  "Provider=SQLOLEDB;Network Library=dbmsrpcn;Data Source=26.2.62.112\\EQTRISSTSQL01,1433;Trusted_Connection=yes"; #multi proto
##-------------------------------------------

#'Provider=SQLOLEDB.1;Integrated Security=SSPI;Data Source=EQTRISSTSQL01;Extended Properties="Database=sogei_xreport";UseProcedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=EQTRISSTPRT01;Use Encryption for Data=False;Tag with column collation when possible=False',

Win32::OLE->Option(Warn => 2, _NewEnum => 1);
my $Conn = Win32::OLE->CreateObject("ADODB.Connection");
#my $Conn = Win32::OLE->CreateObject("ADODB.Recordset");
  
print "opening: $cstr_OMS\n";
$Conn->{ConnectionTimeOut} = 200;
#$Conn->{Version} = '3.0';
$Conn->{Mode} = 1;
$Conn->Open($cstr_OMS);
use Data::Dumper;
#print Dumper($Conn);
#foreach my $prop (in $Conn->{Properties} ) {
#print Dumper($prop);
#}
if ( $Conn->Errors()->Count() > 0 ) {
  print "DB OPEN ERROR: ". $Conn->Errors(0)->Description(), "\n";
#  exit;
}

print "Executing using ".ref($Conn)."\n";
my $rs = $Conn->Execute("SELECT * from tbl_Users");

if (!$rs) {
  my $errs = $Conn->Errors();
  foreach my $error ( in $errs ) {
    print "ERROR: ", $error->Description(), "\n";
  }
}
else {
  while ( !$rs->eof() ) {
    my $fields = $rs->Fields(); 
    print "row ", $main::rct++, ": ",
      join(";", map { $_->Name()."=".$_->Value()  } (in $fields)), "\n";
    $rs->MoveNext();
  }
}

__END__
use DBI;

my $dbh = DBI->connect("dbi:ODBC:Driver={SQL Server};"
		       . "Server=EQTRISSTSQL01\\EQTRISSTSQL01;"
		       . "Address=EQTRISSTSQL01,1433;"
		       . "Network=DBMSSOCN;" #tcpip
#		       . "Network=DBNMPNTW;" #Named Pipes
#		       . "Network=dbmsrpcn;" #multi Protocol
		       . "Database=sogei_xreport;"
		       . "Trusted_Connection=yes,APP=oper"
		       , undef, undef,
                  { RaiseError => 1, odbc_cursortype => 2}) ;

my $rowset = $dbh->selectall_arrayref("SELECT * from tbl_Users", {Slice => {}});

use Data::Dumper;
print Dumper($rowset);

__END__
_
