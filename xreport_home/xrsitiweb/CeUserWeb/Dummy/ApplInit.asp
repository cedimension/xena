<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

use strict;
our ($Server, $Application, $Session, $ScriptHost, $Request, $Response);

use lib("$ENV{'XREPORT_HOME'}/perllib");


use XReport::Util;

our ($COMPUTERNAME, $XREPORT_HOME);

($COMPUTERNAME, $XREPORT_HOME) = @ENV{qw(COMPUTERNAME XREPORT_HOME)};

(undef, undef, my $IISPath) = split(/\//, $Request->ServerVariables('APPL_MD_PATH')->Item(), 3);
my $IIS = Win32::OLE->GetObject("IIS://$COMPUTERNAME/$IISPath");

my $ApplName = ($IIS ? $IIS->{'AppFriendlyName'} : 'CeWebsrvc') ;

$Application->{'cfg.ApplName'} = $ApplName;

my ($wrkpath, $waconf, $LPath, $dbcfg) = getConfValues('workdir', 'webappl', 'LocalPath', 'dbase');

$Application->{'cfg.workdir'} = $wrkpath;

foreach my $itemn ( keys %{ $waconf->{$ApplName} } ) {
  $Application->{'cfg.'.$itemn} = $waconf->{$ApplName}->{$itemn};
}

foreach my $varnm ( keys %{ $LPath } ) {
  $Application->{'cfg.localpath.'.$varnm} = $LPath->{$varnm};
}
$Application->{'cfg.localpath.default'} = $LPath->{'L1'} unless $LPath->{'default'};

if ($dbcfg) {
  foreach my $dbcnn ( keys %{ $dbcfg } ) {
    $Application->{'cfg.db.'.$dbcnn} = $dbcfg->{$dbcnn};
  }
}

$Application->{'cfg.processed'} = 1;

return 1;
</SCRIPT>
