sub Check_AUTHENTICATION {
  my $ca = $main::Application->{'cfg.authenticate'};
  # my $UserName = $main::Session->{'UserName'};
  
  my ($domain, $user, $role) = (undef, undef, undef);
  debug2log("chkAUTH - User:$UserName;CA:$ca");
  
  $user = $main::Session->{RACFUserName};
  $role = $main::Session->{RACFUserRole};
  unless ( $user ) {
    $main::Response->Write("\<DIV ID=\"EXEC\"\>\<script>\x0d\x0atop.LogonRACFUser()\x0d\x0a\</script\>\</DIV\>");
    TERMINATE_REQUEST();
  }
  
  debug2log("chkAUTH - User:$UserName;newuser:$user;role:$role");
  if ( !$user ) {
    $main::Response->Clear();
    $main::Response->{'Buffer'} = 1;
    EXIT_ERROR("Impossibile verificare credenziali - Contattare Help Desk", 0);
  }

  @{$main::requestVars}{qw(NetName UserName)} = ($domain, $user); 
  return $main::requestVars->{'UserName'}; 

}

1;