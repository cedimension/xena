<%@ LANGUAGE=PerlScript %>
<%

use strict vars;

use FileHandle;
use CGI qw(:standard);
use MIME::Base64;

use lib $main::Application->{'XREPORT_HOME'}."\\perllib";
use XReport;

my $siteconf = $main::Application->{'XREPORT_SITECONF'};
$siteconf .= '/webroot' if $siteconf;
$siteconf = $main::Server->MapPath('/') unless $siteconf;

my $ulfqn = $siteconf.'/'.$main::Application->{ApplName}.'UserLogo.pl'; 
unless ( -e $ulfqn ) {
  $ulfqn = $siteconf.'/UserLogo.pl';
}
#$main::Response->Write('<!-- calling --- '.$ulfqn.' calling -->');
if ( -e $ulfqn ) {
 do "$ulfqn";
}
else {
  my ($ulfqn, $htmlstr, $bcnt, $fsz) = ('', '', 0,0);
  
  my ($logomime, $logofn, $logohtitle, $logotitle, $logoalttxt) 
    = @{$XReport::cfg}{qw(logomimetype logofile logohtitle logotitle logoalttxt)};
  $logomime =  'image/gif' unless $logomime;
  $logofn =  'logoxreport.gif' unless $logofn;
  $logohtitle  = 'Portale Stampe Elettroniche' unless $logohtitle;
#  my $logotitle =  'Portale - Stampe Elettroniche ($logofn)' unless $logotitle;
  $logoalttxt =  'Logo for '.$main::Application->{'ApplName'} unless $logotitle;

  my $imgfqn = "$siteconf\\sitelogo.gif";
  my $imgurl = '';
  unless ( -e $imgfqn ) {
    $imgurl = "images/$logofn";
    $imgfqn = $main::Server->MapPath($imgurl);
  }
  unless ( -e $imgfqn ) {
    $imgurl = "images/logoxreport.jpg";
    $imgfqn = $main::Server->MapPath($imgurl);
  }
  $logotitle =  "Portale Stampe Elettroniche" unless $logotitle;

  my $imgb64;
  if ( !$imgurl && -e $imgfqn ) {
    my $imghndl = new FileHandle("<$imgfqn");
    binmode $imghndl;
    $fsz = -s $imgfqn;
    $bcnt = $imghndl->read(my $imgstr, $fsz); 
    $imgb64 = MIME::Base64::encode_base64($imgstr) if $imgstr; 
    $imgb64 = join("\x0a", unpack("(A72)*", join('', split(/[\x0d\x0a]+/, MIME::Base64::encode_base64($imgstr))))) if $imgstr; 
#    $imgb64 = MIME::Base64::encode_base64($imgstr) if $imgstr; 
#    $logotitle =  "Portale Stampe Elettroniche";
    $imghndl->close;
  }

  my $htmlstr = join
    ('', start_html($logohtitle)
     , div({align => 'center'}, join
	   ('', '<center>', hr({size=>'3', color=>'#008000', align=>'center'})
	    , table({width=>'100%', cellspacing=>'0', cellpadding=>'0', height=>'80%'}, tbody
		    (Tr([td( {align=>'center', height=>'40%', valign=>'middle'}
			     , table({width=>'100%', cellspacing=>'0', cellpadding=>'0', height=>'80%'}, 
				     #				   Tr(td({width=>'100%', align=>'center'}, img({border => 0, src => $imgurl, ALT=>"Logo"}))))
				     Tr(td({width=>'100%', align=>'center'}, 
					   ($imgurl 
					    ? img({border => 0, src => $imgurl, ALT=>$logoalttxt}) 
					    : $imgb64 
					    ? img({src => "data:$logomime;base64;".$imgb64, ALT=>$logoalttxt})
					    : h5(font({size=>'8', color=>'#FF0000', face=>'Arial Black'}, $logoalttxt))
					   ))))
			   )
			 , td({align=>'center'}
			      , h4(font({size=>'8', color=>'#FF0000', face=>'Arial Black'},$logotitle)
				   . font({size=>'6', color=>'#FF0000', face=>'Script MT Bold'},br())))
			]) ) )
	    , hr({size=>'3', color=>'#008000', align=>'center'})
	    , '</center>'
	   ))
     , end_html()
    );

  return $main::Response->Write($htmlstr) if $htmlstr;
}

   1;
%>
