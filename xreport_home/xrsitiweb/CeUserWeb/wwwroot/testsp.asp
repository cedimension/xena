<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

### vim: set syn=perl: ###

use lib("$ENV{'XREPORT_HOME'}/perllib");

use strict;

BEGIN {
  require XReport::LoginWin32;
  XReport::LoginWin32::RevertToSelf();  
}

use Symbol;
use File::Basename;
use File::Path;
use Win32::OLE; 
use Win32::OLE::Variant;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

our ($Application, $Server, $Session, $Request, $Response, $XREPORT_HOME, $workdir, $dbdateformat); 

my ($JobReportName, $JobReportId, $ReportId, $FileFormat, $dateTime, $FilterValue, $FileId);
my ($LocalPathId_IN, $LocalFileName, $baseFileName);
my ($Zip, $ZipFileName, @PageXref);
my ($FolderName, $ReportName, $ElabFormat);

my ($NetName, $UserName, $FoldersList, $dbc, @ReportIds, $ListOfReportIds); 

my ($ENVxr, %LockTable, $filecache, $requestVars);


my @patterns = (
		'x03456   ',    # 3456
		'x00001   ',    # 001
		'x00703   ',    # 703
		'x00405   ',    # 405
		'x99875   ',    # 9875
		'x90001   ',    # 001
		'x10971   ',    # 971
		'   771   ',    # 971
		'  6781   ',    # 971
		'   031   ',    # 971
	       );
foreach my $num ( @patterns ) {
  (my $cod) = $num =~ /^..[0\s]{0,2}(\d{3,4}).*$/;
  $Response->Write("<br/>1 ".$num." cod=>".$cod." ..n=> ".$+." ..N=> $^N ..1=> $1 ..2=> $2 ..3=> $3 ..4=> $4 <br/>");
  $cod = '';
}


# ^(\w+)(\w+)?(?(2)\2\1|\1)


</SCRIPT>
