
sub Check_AUTHENTICATION {
  my $ca = $main::Application->{'cfg.authenticate'};
  my $UserName = $main::Session->{'UserName'};
  my $sstate = $main::Session->{'xrstate'};
  my ($domain, $user, $role) = (undef, undef, undef);
  debug2log("chkAUTH - User:$UserName;CA:$ca;XRSTATE:$sstate");
  $main::Session->{'xrstate'} = 1 if $sstate == 0;
  
  if ( $main::Request->ServerVariables("AUTH_USER")->Item() eq "" ) {
    $main::Response->Clear();
    $main::Response->{'Buffer'} = 1;
    $main::Response->{'Status'} = "401 Unauthorized";
    $main::Response->AddHeader("WWW-Authenticate","NTLM");
    debug2log("chkAUTH - EMPTY AUTH_USER _ TERMINATE REQUEST;XRSTATE:$sstate");
  
    TERMINATE_REQUEST();
  }
  @{$main::requestVars}{qw(NetName UserName)} 
                  = split(/\\/, $main::Request->ServerVariables("AUTH_USER")->Item() );
  
  return $main::requestVars->{'UserName'}; # = $main::Request->ServerVariables("AUTH_USER")->Item();
  @{$main::requestVars}{qw(NetName UserName)} = ($domain, $user); 
  return $main::requestVars->{'UserName'}; 

}

1;