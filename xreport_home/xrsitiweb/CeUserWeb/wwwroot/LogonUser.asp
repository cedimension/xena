<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

### vim: set syn=perl: ###

use strict; 

use FileHandle; use URI::Escape; use Win32::OLE; use Digest::MD5;

our ($Server, $Application, $Session, $Request, $Response, $XREPORT_HOME);

my ($requestVars, $dbc);

sub CreateObject {
  return $Server->CreateObject($_[0]);
}

sub CONNECT {
  $dbc = Win32::OLE->new("ADODB.Connection");

  $dbc->{"ConnectionTimeout"} = 10;
  $dbc->{"CommandTimeout"} = 30;
	
  $dbc->Open($Application->{"Cfg.OMS"});
}

sub DISCONNECT {
  $dbc->Close();
}

sub dbExecute {
  my $sql = $_[0]; my $dbr = CreateObject("ADODB.Recordset");
    
  $dbr->Open($sql, $dbc, 3, 3);
  
  return $dbr;
}

sub getFormVars {
  return {} if $Request->ServerVariables("REQUEST_METHOD")->Item() ne "POST";
  
  my ($strVars, $FormVars) = ($Request->BinaryRead($Request->TotalBytes()), {}); 
  
  $strVars =~ s/\+/ /g;
  
  my @FormVars = map {uri_unescape($_)} split("&",$strVars);
  for (@FormVars) {
    my ($VarName, $VarValue) = $_ =~ /^(\w+)=(.*)$/;
    $FormVars->{$VarName} = $VarValue;
  }

  return $FormVars;
}

sub VerifyUserPassword {
  my ($UserName, $Password, $NewPassword, $ConfirmNewPassword) 
    = 
  @{$requestVars}{qw(UserName Password NewPassword ConfirmNewPassword)};

  my $dbr = dbExecute("SELECT * from tbl_Users WHERE UserName = '$UserName'");
  
  if ( !$dbr->eof() ) {
    my $dbPassword = $dbr->Fields()->Item('Password')->Value();
    my $PassMustBeChanged = $dbr->Fields()->Item('PassMustBeChanged')->Value();
    if ( $Password eq $dbPassword ) {
      if ($NewPassword ne "" and $NewPassword eq $ConfirmNewPassword) {
        dbExecute("
          UPDATE tbl_Users set Password = '$NewPassword', PassMustBeChanged = 0 
          WHERE UserName = '$UserName'
        ");
      }
      elsif ($NewPassword ne "") {
        EXIT_ERROR("Conferma Nuova Password ERRATA");
      }
      elsif ($PassMustBeChanged and $NewPassword eq "") {
        EXIT_ERROR("La Password DEVE ESSERE CAMBIATA");
      }
    }
    else {
      $UserName = undef;
    }
  }
  else {
    $UserName = undef;
  }
 
  $dbr->Close();
  
  return $UserName;
}

sub EXIT_ERROR {
  my ($UserName, $MESSAGE) = ("$requestVars->{'UserName'}", shift);
  
  my $xml = CreateObject("MSXML2.DOMDocument.4.0"); $xml->{"async"} = 0;
  my $xslt = CreateObject("MSXML2.DOMDocument.4.0"); $xslt->{"async"} = 0;
   
  my $rc = $xml->loadXML(
    "<LogonUser UserName=\"$UserName\" MESSAGE=\"$MESSAGE\"/>"
  ); 

  $xslt->load($Server->MapPath("xsl/LogonUser.xsl"));

  $Response->Write($xml->transformNode($xslt)); exit();
}

sub CookieExpireTime {
  my @tm = (qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec));
  my @td = (qw(Sun Mon Tue Wed Thu Fri Sat));

  my @g = localtime(shift);
  $g[5] += 1900;
  for (0..2) {
    $g[$_] = substr("0$g[$_]",-2);
  }
  return "$td[$g[6]], $g[3]-$tm[$g[4]]-$g[5] $g[2]:$g[1]:$g[0] GMT";
}

sub SqlCookieExpireTime {
  my @g = localtime(shift);
  $g[5] += 1900; $g[4] += 1;
  for (0..2) {
    $g[$_] = substr("0$g[$_]",-2);
  }
  return "$g[5]/$g[4]/$g[3] $g[2]:$g[1]:$g[0]";
}

sub SetUserLocalSessionCookie {
  my ($cookie, $cookie_hex, $cookie_exp); require Digest::MD5;

  my $UserName = $Session->{'UserName'}; 
  
  my $ExpireTime = time()+12*3600;
  
  my $AUTH_USER = 
    $Request->ServerVariables('AUTH_USER')->Item()
      ||
    "$Application->{'cfg.authenticate'}\\$UserName"
  ; 
  my $REMOTE_ADDR = $Request->ServerVariables('REMOTE_ADDR')->Item(); 

  $cookie = "$AUTH_USER/$REMOTE_ADDR";
  $cookie_hex = Digest::MD5::md5_hex($cookie);
  $cookie_exp = SqlCookieExpireTime($ExpireTime);
  
  my $dbr = dbExecute("
    SELECT * from tbl_UsersLocalSessionCookies
    WHERE CookieHexString = '$cookie_hex'
  ");
  if ( $dbr->eof() ) {
    dbExecute("
      INSERT into tbl_UsersLocalSessionCookies
      (CookieHexString, CookieExpireTime, UserName, AUTH_USER, REMOTE_ADDR)
      Values ('$cookie_hex', '$cookie_exp', '$UserName', '$AUTH_USER', '$REMOTE_ADDR')
    ");
  }
  else {
    dbExecute("
      UPDATE tbl_UsersLocalSessionCookies

      SET
        CookieHexString  = '$cookie_hex', 
        CookieExpireTime = '$cookie_exp', 
        UserName         = '$UserName', 
        AUTH_USER        = '$AUTH_USER', 
        REMOTE_ADDR      = '$REMOTE_ADDR'
        
      WHERE CookieHexString = '$cookie_hex'
    ");
  }
  $dbr->Close();

  $Response->{'Cookies'}->{'IXR_User'}->{'LocalSessionCookie'} = $cookie_hex;
  $Response->{'Cookies'}->{'IXR_User'}->{'expires'} = CookieExpireTime($ExpireTime);
}

sub VerifyUser {
  Win32::OLE->new('LoginAdmin.ImpersonateUser')->Logoff();

  for (qw(Password NewPassword ConfirmNewPassword)) {
    $requestVars->{$_} = Digest::MD5::md5_hex($requestVars->{$_})
      if
    $requestVars->{$_} ne "";
  }
	
  if ($requestVars->{"UserName"} ne "" and VerifyUserPassword()) {
    $Session->{"UserName"} = $requestVars->{"UserName"};
    SetUserLocalSessionCookie();
  }
  elsif ($requestVars->{"UserName"} eq "") {
    EXIT_ERROR("Inserire Utente/Password");
  }

  if ( $Session->{"UserName"} ne "") {
    $Response->Write("
      <DIV ID=\"EXEC\">
        \<\script> top.opener.focus(); top.opener.restart_tree_exp(); top.close(); \</\script>
      </DIV>
    ");
  }
  else {
    EXIT_ERROR("Utente/Password INVALIDI");
  }
}

sub INIT_ALL {
  CONNECT(); 

  $requestVars = getFormVars();
}

sub TERMINATE_ALL {
  DISCONNECT();
}

INIT_ALL();

VerifyUser(); 

TERMINATE_ALL();
    
</SCRIPT>
