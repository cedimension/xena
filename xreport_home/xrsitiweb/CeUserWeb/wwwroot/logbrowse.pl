#!/usr/bin/perl -w
sub BEGIN {
  use CGI qw/:standard *table :html3/;
  ($XReport::interpr) = ($^X =~ /[\/\\]([\w\.]*)\Z/);
  (my $sysexec) = ($0 =~ /(.*)[\/\\][\w\-]*\.plx*\Z/i);
  unshift (@INC, $sysexec."/../perllib");
  $XReport::sysexec = $sysexec;
}

use XReport;
use Win32::Service;

=pod
CurrentState = 1 Stopped
currentState = 4 Started
=cut

use strict;
use warnings;
use CGI qw/:standard *table :html3/;
$|=1;

my $htmlhead = start_html( 
			  -title=>'XReport Process Start',
			  -meta=>{'copyright'=>'EURISKOM s.r.l.'},
			  -style=>{
				   -src=>'cereport.css',
				  },
			 );
my $result = '';
if ((my $reqid = param('reqid')) && (my $srvname = param('name'))) {
  my $logfile = $XReport::sysexec . "/../logs/" . $srvname . ".log";
  open(LOGF, "<$logfile");
  my (@before, @after);
  $#before = -1;
  $#after = -1;
  my $found = 0;
  while (<LOGF>) {
    $#before = -1 if ($#before > 8 && $found == 0);
    $before[++$#before] = $_ unless $found == 1;
    $after[++$#after] = $_ if $found == 1;
    last if $#after > 12;
    /^.*fullfill req $reqid.*$/ && ($found = 1);
  }
#  $result = `grep -A 13 -B 7 'fullfill req $reqid' $logfile 2>&1`;
  if ($found) {
    $result .= join "", @before;
    $result .= join "", @after;
  }else {
    $result = "No entries found for request in $logfile";
  }
  
  print header(), $htmlhead;
  print h2({-align=>'center'}, "Log entries for request $reqid in $logfile");
  print pre($result);
  print end_html();
  
} else {
  print header(), $htmlhead;
}
exit();
