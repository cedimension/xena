<SCRIPT LANGUAGE="PerlScript" RUNAT="SERVER">

use lib("$main::Application->{XREPORT_HOME}/perllib");
use strict; 

use XReport;
use CGI qw(:standard meta);

our ($Application, $Session, $Request, $Response);
my @routines = qw(logFunctions.pl showFunctions.pl authFunctions.pl); 
my $binpath = $main::Request->ServerVariables('APPL_PHYSICAL_PATH')->Item();
my $ca = $main::Application->{'cfg.authenticate'};
push @routines, "authFunctions_$ca.pl" if ( -f $binpath."authFunctions_$ca.pl");
for my $fname ( @routines ) {
	my $fpath = $binpath.$fname;
	require "$fpath";
}
$CGI::CRLF = "\x0d\x0a";

$main::requestVars = getFormVars();

$main::Response->{'Content-Type'} = "text/html";
$main::Response->AddHeader("Pragma", 'no-cache');
$main::Response->AddHeader("Expires", '-1');

#my $htmlbuff = <<EOF;
#<html>
my $htmlbuff = <<EOF;
<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head>
<title>Xreport Web Gui</title>
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
EOF

$htmlbuff .= "<scr" . "ipt src=\"js/"
		. join(".js\" type=\"text/javascript\"></scr"."ipt>\n<scr"."ipt src=\"js/", 
		       qw(cereport cookie user) ) 
		. ".js\" type=\"text/javascript\"></scr"."ipt>\n";
#		. ".js\" defer=\"defer\" type=\"text/javascript\"></scr"."ipt>\n";

$htmlbuff .= '<scr'.'ipt>'.join(";", ( 
		"ExpandRequest=\"FromUsers.asp\"", 
		"U_init()", 
		map { (my $kn = $_) =~ s/^pgesat_//;"U.$kn=\"".$main::requestVars->{$_}."\""; } keys %$main::requestVars
		) ).";</scr"."ipt>\n";

($htmlbuff .= <<EOF) =~ s/([^\r])\n/$1\r\n/gs;
</head>

<frameset id="Main" cols="310,*" ONLOAD="INIT_ALL()">
 <frame id="Left" name="Left" src="LeftUser.html" marginwidth="0" scrolling="AUTO">
 <frame id="FText" name="FText" src="TextUser.html" marginwidth="0" scrolling="AUTO">
</frameset>

</html>
EOF

debug2log("htmlbuff: ", $htmlbuff);
#$main::Response->{'Content-Length'} = length($htmlbuff);

$main::Response->Write($htmlbuff);
#$main::Request->Flush();                       

</SCRIPT>