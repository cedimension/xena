sub Check_AUTHENTICATION {

	my ( $domain, $user, $role ) = ( undef, undef, undef );
	debug2log("chkAUTH_PGE - User:$UserName;CA:PGE");
    my $auth_user = $main::Request->ServerVariables("AUTH_USER")->Item();
	( $domain, $user ) = ($auth_user =~ /\\/ ? split( /\\/, $auth_user, 2 ) : $auth_user);
	EXIT_ERROR( "User Name not provided to this session", 0 ) unless $user;
	
	debug2log("chkAUTH_PGE - AuthUser:$user;Domain:$domain");
	my $netuser = $user;
	my $currentProf = $main::Session->{U_User_Profile};
	my $UAttrs = {};
	
    if ( $currentProf ) {
		debug2log("chkAUTH_PGE - CurrentProf:". substr($currentProf, 0, 8));
    	@{$UAttrs}{qw(username torre filiale istituto banca nome cognome ruolo profilo)} = split /\t/, $currentProf;
    	($user, $role) = @{$UAttrs}{qw(username ruolo)};
    }
	else {
		my ($result, $pgeToken) = ({}, '');
		if ( my $specialu = $XReport::cfg->{PGE}->{specialuser}->{ uc($netuser) } ) {
			debug2log("chkAUTH_PGE - SpecialUser:$specialu->{UserName}");
			$user = uc( delete $specialu->{UserName} );
			$result = delete $specialu->{WSresp};
			foreach ( keys %$specialu ) {
				next unless $specialu->{$_};
				$main::requestVars->{$_} = $specialu->{$_};
			}
			$pgeToken = "This_IS_A_FAKE_FOR_X1N_$netuser";
			$main::Session->{PGEToken} = ''; 
		}
		else {
			$pgeToken = $main::requestVars->{token};
			if ( !$pgeToken || $pgeToken eq 'undefined' ) {
			    debug2log("chkAUTH_PGE - token not found - requestVars:" . Dumper($main::requestVars));
				$main::Response->Clear();
				$main::Response->{'Buffer'} = 1;
				EXIT_ERROR( "OUTSIDE XFRAME ACCESS not ALLOWED", 0 );
			}
			require SOAP::Lite;
            my $parms = { userName => $netuser, 
            			token => $pgeToken, 
            			applCode => $XReport::cfg->{'PGE'}->{applid} };
			debug2log( "chkAUTH_PGE - calling PGE for user:$netuser;applid:" . $XReport::cfg->{'PGE'}->{applid} );
			( my $PGEurl = $XReport::cfg->{'PGE'}->{url} ) =~ s/\#\{(.+?)\}\#/$UAttrs->{$1}/g;
			$main::SOAPstring = '';
			my $resp = SOAP::Lite->uri('urn://tokenservices.pge.xframe.usi.it')
							  ->proxy($PGEurl)
  							  ->autotype(0)
							  ->on_debug(sub{$main::SOAPstring .= join('', @_). "\n";})
							  ->on_fault(sub {})
  							  ->call( "PGEUI:retrievePGEAuthority" 
  							          => (map { SOAP::Data
  				                    			->prefix('PGEUI')
  				                    			->name($_ => $parms->{$_}) } qw(userName token applCode) ) );
			debug2log( "chkAUTH_PGE - PGE data exchange:\n$main::SOAPstring\n========== END EXCHANGE DATA ========");
  			$result = $resp->result();
  			$result = $resp->fault() unless $result;
  			
  			if ( exists($result->{PGEfailed}) && $result->{PGEfailed}->{codiceErr} ) {
  				$result->{qw(faultcode faultstring)} = @{$result->{PGEfailed}}{qw(codiceErr descError)}; 
  			}
		}

		debug2log( "chkAUTH_PGE - PGE response:" . Dumper($result) );
		if ( !$result || $result->{faultstring} ) {
			$main::Response->Clear();
			$main::Response->{'Buffer'} = 1;
			EXIT_ERROR( "PG Error upon request for\\n\\t"
						  . join( '/', ( $XReport::cfg->{'PGE'}->{applid}, $netuser ) ) . ":\\n"
						  . ($result ? $result->{faultstring} : "undefined response" ),
						0 );
		}
		
		@{$UAttrs}{qw(cognome nome ruolo profilo torre istituto banca filiale )} =
		  map { $_ =~ s/\s+$//; $_ =~ s/'/''/g; $_; } ( @{$result}{qw(PG_COGNOME_OPER PG_NOME_OPER PG_RUOLO 
		  															  PG_COD_AUTHOR 
		  															  PG_TORRE PG_COD_IST PG_COD_ABI)},
		  						($netuser =~ /^UP\./i ? $result->{PG_COD_SPORT_CONT} : $result->{PG_UFFICIO_INQ}) );
		  						
		$UAttrs->{filiale} = substr( "00000". $UAttrs->{filiale}, -5 );

		$UAttrs->{istituto} = substr( "00" . $UAttrs->{istituto}, -2 );
		$UAttrs->{istituto} = '01' if $UAttrs->{istituto} =~ /^(?:52|83|86)$/;

		debug2log( "chkAUTH_PGE - UAttrs:" . Dumper($UAttrs) );
		
		eval "\$user = $XReport::cfg->{PGE}->{assignuser};";
		debug2log( "chkAUTH_PGE - assignuser user:$user");

		$UAttrs->{username} = $user;
		$main::Session->{U_User_Profile} = join( "\t", 
				@{$UAttrs}{ qw(username torre filiale istituto banca nome cognome ruolo profilo) } );

		$role = $UAttrs->{ruolo};

		@{$main::requestVars}{qw(U_Torre U_Filiale U_Istituto U_Banca U_Nome U_Cognome U_Ruolo U_Profile )}
	  		= @{$UAttrs}{qw(torre filiale istituto banca nome cognome ruolo profilo)};
		@{$main::requestVars}{qw(U_IsMine)} = ('0');
		@{$main::requestVars}{qw(U_IsMine)} = ('1')
		  if $UAttrs->{ruolo} =~ /^(?:RTRESROL|RTADDROL|BRRESROL|BRADDROL|BDRESROL|BDADDROL|RTARCTER)$/;
	}

	debug2log("chkAUTH_PGE - User:$UserName;newuser:$user;role:$role");
	if ( !$user ) {
		$main::Response->Clear();
		$main::Response->{'Buffer'} = 1;
		EXIT_ERROR( "Impossibile verificare credenziali - Contattare Help Desk", 0 );
	}

	@{$main::requestVars}{qw(NetName UserName)} = ( $domain, $user );
	return $main::requestVars->{'UserName'};

}

1;