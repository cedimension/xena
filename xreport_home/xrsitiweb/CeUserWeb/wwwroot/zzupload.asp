<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

###-- vim: set syn=perl: --###

use lib("$ENV{'XREPORT_HOME'}/perllib");

use strict; 

BEGIN {
  require XReport::LoginWin32;
  XReport::LoginWin32::RevertToSelf();  
}

use Symbol;
use IO::Scalar; 
use Spreadsheet::ParseExcel;

use XReport::DBUtil;

our ($Server, $Application, $Session, $Request, $Response, $XREPORT_HOME);

my ($workdir, $xlsFile, $logger);

sub logIt {
  my $self = shift;
  for (@_) {
    my $t = $_; $t =~ s/\n/<br\/>/g; $Response->Write($t);
  }
}

sub TRIM {
  for (@_) {
    $_ =~ s/(?:^ +| +$)//g;
  }
}

sub ExtractXls {
  my $rec = $Request->BinaryRead($Request->TotalBytes()); my $OUTPUT = gensym();

  my $INPUT = IO::Scalar->new(\$rec);

  my $boundary = <$INPUT>; $boundary =~ s/\s+$//;
  while(!eof($INPUT)) {
    my $line = <$INPUT>;
    if ( $line =~ /application\/vnd.ms-excel/ ) {
      open($OUTPUT, ">$xlsFile"); binmode($OUTPUT);
      <$INPUT>;
      while(!eof($INPUT)) {
        $line = <$INPUT>; last if $line =~ /^$boundary/;
        print $OUTPUT $line;
      }
      truncate $OUTPUT, tell($OUTPUT)-2;
      close($OUTPUT);
    }
  }

  close($INPUT);
}

sub ParseExcel {
  use Spreadsheet::ParseExcel;

  my $book = Spreadsheet::ParseExcel::Workbook->Parse($xlsFile);
  
  my (
    $wks_reports,
    $wks_profiles, $wks_users
  )
  = @{$book->{Worksheet}};
  
  my ($wks_cells);
  
  my ($wks_cells, $irow, $cells, $blanks);
  
  my (@JobReportNames, %Folders, %TreeNodes, %FoldersReportNames, %FoldersTreeNodes); 

  my (@Users, @Profiles, %ProfilesFolders);
  
  my ($FolderName, $FolderDescr);
  
  my ($JobReportName, $JobReportdescr);
  
  my ($ProfileName, $ProfileDescr);
  
  my ($UserName, $UserDescr);

  ### profiles ----------------------------------------------------- ###
  $wks_cells=$wks_profiles->{Cells}; $irow = 0; $blanks=0;
  while($blanks<5) {
    $cells = $wks_cells->[$irow]; 
  
    do { 
     $blanks += 1; next; 
    } 
    if (!defined($cells->[0]) or $cells->[0]->Value() !~ /\w+/); $blanks = 0;
    
    my (
      $ProfileName, $ProfileDescr, $TreeNode
    )
    = map {defined($_) ? $_->Value() : ''} @$cells[0..2];

    TRIM($ProfileName, $ProfileDescr, $TreeNode);
  
    push @Profiles, [$ProfileName, $ProfileDescr, $TreeNode];
  
    $logger->logIt("==$ProfileName, $ProfileDescr ==\n");
  }
  continue {
    $irow += 1;
  }
  
  ### folders and reports ------------------------------------------ ###
  $wks_cells=$wks_reports->{Cells}; $irow = 2; $blanks=0;
  while($blanks<5) {
    $cells = $wks_cells->[$irow]; 
  
    do { 
     $blanks += 1; next; 
    } 
    if (!defined($cells->[3]) or $cells->[3]->Value() !~ /\w+/); $blanks = 0;
    
    my (
      $TreeNode,
      $DOCUMENTO, 
      $FOLDER, 
      $Tipologia, 
      $Leggibile, 
      $BitMapped, 
      $FormDef,
      $Offset,
      $Uffici, 
      $GG_ritenzione
    )
    = map {defined($_) ? $_->Value() : ''} @$cells[0..9];
  
    TRIM($FOLDER, $DOCUMENTO, $Tipologia, $Leggibile);
  
    if ( $DOCUMENTO ne '' ) {
      ($FolderName, $FolderDescr) = ($FOLDER, $DOCUMENTO);
       $Folders{$FolderName} = $FolderDescr;
       push @{$TreeNodes{$TreeNode}}, $FolderName;
       $FoldersTreeNodes{$FolderName} = $TreeNode;
    }
  
    push @JobReportNames, [$Leggibile, "$FolderDescr -- $Tipologia (Leggibile)", $FormDef, $Offset];
    push @JobReportNames, [$BitMapped, "$FolderDescr -- $Tipologia (Stampabile)", $FormDef, $Offset] if $BitMapped;

    push @{$FoldersReportNames{"$FolderName"}}, $Leggibile;
    push @{$FoldersReportNames{"$FolderName"}}, $BitMapped if $BitMapped;
  
    my @profs = grep(/^\w+$/, split(/\W+/, $Uffici));
    
    for my $ProfileName (@profs) {
      $ProfilesFolders{$ProfileName}->{$FolderName} = 1;
    }
  
    $logger->logIt("== $FolderName, $FolderDescr, $Tipologia, $Leggibile, $BitMapped, $Uffici, $GG_ritenzione ==\n");
  }
  continue {
    $irow += 1;
  }
  
  for my $ProfileName(sort(keys(%ProfilesFolders))) {
    $logger->logIt("ProfileName=$ProfileName Folders=", join(",", sort(keys(%{$ProfilesFolders{$ProfileName}}))), "\n");
  }
 
  
  ### users -------------------------------------------------------- ###
  $wks_cells=$wks_users->{Cells}; $irow = 2; $blanks=0;
  while($blanks<5) {
    $cells = $wks_cells->[$irow]; 
  
    do { 
     $blanks += 1; next; 
    } 
    if (!defined($cells->[0]) or $cells->[0]->Value() =~ /^\s$/); $blanks = 0;
    
    my (
      $NOMINATIVO, 
      $PROFILO,
      $UTENZA, 
      $E_MAIL 
    )
    = map {defined($_) ? $_->Value() : ''} @$cells[0..3];

    TRIM($NOMINATIVO, $PROFILO, $UTENZA, $E_MAIL);
  
    push @Users, [$UTENZA, $NOMINATIVO, $E_MAIL, $PROFILO];
  
    $logger->logIt("$NOMINATIVO, $PROFILO, $UTENZA, $E_MAIL ==\n");
  }
  continue {
    $irow += 1;
  }

  dbExecute("
    TRUNCATE TABLE tbl_JobReportNames
  ");
  dbExecute("
    TRUNCATE TABLE tbl_Os390PrintParameters
  ");
  dbExecute("
    TRUNCATE TABLE tbl_ReportNames
  ");

  for ( @JobReportNames ) {
    my ($JobReportName, $JobReportDescr, $FormDef, $Offset) = @$_; $JobReportDescr =~ s/'/''/g;
    dbExecute("
      INSERT INTO tbl_JobReportNames (JobReportName, JobReportDescr)
      VALUES ('$JobReportName', '$JobReportDescr')
    ");
    my ($ReportName, $ReportDescr) = ($JobReportName, $JobReportDescr);
    dbExecute("
      INSERT INTO tbl_ReportNames (ReportName, ReportDescr)
      VALUES ('$ReportName', '$ReportDescr')
    ");
    if ( $FormDef ne '' ) {
      my $t1Fonts = substr($JobReportName,-1) eq '0' ? 0 : 1; $Offset ||= '30 0';
      dbExecute("
        INSERT INTO tbl_OS390PrintParameters (JobReportName, t1Fonts, LaserAdjust, FormDef)
        VALUES ('$JobReportName', $t1Fonts, '[$Offset]', '$FormDef')
      ");
    }
  }

  dbExecute("
    TRUNCATE TABLE tbl_Folders
  ");
  dbExecute("
    TRUNCATE TABLE tbl_FoldersReportNames_LL1
  ");

  my ($FolderId, $FolderName) = (0, ''); my $OUTPUT = gensym();

  for my $FolderName ( keys(%Folders) ) {
    my $FolderDescr = $Folders{$FolderName}; $FolderDescr =~ s/'/''/g;
    dbExecute("
      INSERT INTO tbl_Folders (FolderName, FolderDescr)
      VALUES ('$FolderName', '$FolderDescr')
    ");
    for my $ReportName (@{$FoldersReportNames{"$FolderName"}}) {
      dbExecute("
        INSERT INTO tbl_FoldersReportNames_LL1 (FolderName, ReportName)
        VALUES ('$FolderName', '$ReportName')
      ");
    }
  }

  open($OUTPUT, ">$XREPORT_HOME/userlib/folderstrees/folderstrees.xml");
  print $OUTPUT "<root>\n";

  print $OUTPUT " <node name=\"ALL\" descr=\"Folders Disponibili\">\n";
  for my $TreeNode (sort(keys(%TreeNodes))) {
    print $OUTPUT "  <node name=\"$TreeNode\" descr=\"$TreeNode\">\n";
    for my $FolderName (sort @{$TreeNodes{$TreeNode}}) {
      my $FolderDescr = $Folders{$FolderName}; $FolderDescr =~ s/'/''/g;
      print $OUTPUT "   <node name=\"$FolderName\" descr=\"$FolderDescr\" folder=\"1\"/>\n";
    }
    print $OUTPUT "  </node>\n";
  }
  print $OUTPUT " </node>\n";

  print $OUTPUT " <node name=\"profiles\" descr=\"links for profiles\">\n";
  for my $ProfileName ( sort(keys(%ProfilesFolders)) ) {
    print $OUTPUT "  <node name=\"$ProfileName\" descr=\"Folders Disponibili\">\n";

    my @Folders = sort(keys(%{$ProfilesFolders{$ProfileName}})); my %TreeNodesFolders;

    for my $FolderName (@Folders) {
      push @{$TreeNodesFolders{$FoldersTreeNodes{$FolderName}}}, $FolderName;
    }
    for my $TreeNode (sort(keys(%TreeNodesFolders))) {
      print $OUTPUT "   <node name=\"$TreeNode\" descr=\"$TreeNode\">\n";
      for my $FolderName (@Folders) {
        print $OUTPUT "    <link root=\"/ALL/$TreeNode/$FolderName\"/>\n";
      }
      print $OUTPUT "   </node>\n";
    }
    print $OUTPUT "  </node>\n";
  }
  print $OUTPUT " </node>\n";

  print $OUTPUT " </root>\n";
  close($OUTPUT);
  
 
  dbExecute("
    TRUNCATE TABLE tbl_UsersProfiles 
  ");
  dbExecute("
    TRUNCATE TABLE tbl_Profiles
  ");
  dbExecute("
    TRUNCATE TABLE tbl_Users
  ");
  dbExecute("
    TRUNCATE TABLE tbl_ProfilesFoldersTrees
  ");
    
  for ( @Profiles ) {
    my ($ProfileName, $ProfileDescr) = @$_; $ProfileDescr =~ s/'/''/g;
    dbExecute("
      INSERT INTO tbl_Profiles (ProfileName, ProfileDescr)
      VALUES ('$ProfileName', '$ProfileDescr')
    ");
  }
    
  for ( @Users ) {
    my ($UserName, $UserDescr, $EMailAddr, $ProfileName) = @$_; $UserDescr =~ s/'/''/g;
    dbExecute("
      INSERT INTO tbl_Users (UserName, UserDescr, EMailAddr)
      VALUES ('$UserName', '$UserDescr', '$EMailAddr')
    ");
    dbExecute("
      INSERT INTO tbl_UsersProfiles (UserName, ProfileName)
      VALUES ('$UserName', '$ProfileName')
    ");
  }
  
  for my $ProfileName ( sort(keys(%ProfilesFolders)) ) {
    dbExecute("
      INSERT INTO tbl_ProfilesFoldersTrees (ProfileName, RootNode)
      VALUES ('$ProfileName', '/profiles/$ProfileName')
    ");
  }
  dbExecute("
    INSERT INTO tbl_ProfilesFoldersTrees (ProfileName, RootNode)
    VALUES ('SUPER', '/ALL')
  ");
}

sub INIT_ALL {
  $XREPORT_HOME = $ENV{'XREPORT_HOME'}; my $sessid = $Session->{"SessionId"}; 

  $workdir = $Application->{'cfg.workdir'} || "$ENV{TEMP}";

  $xlsFile = "$workdir/cfg.$sessid.xls";

  $logger = {}; bless $logger;
}

sub TERMINATE_ALL {
  $Response->Write("MA CHISSA' CHI LO SA....."); unlink $xlsFile;
}

INIT_ALL();

ExtractXls(); ParseExcel();

TERMINATE_ALL();

</SCRIPT>
