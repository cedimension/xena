<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

### vim: set syn=perl: ###
;

use lib("$main::Application->{XREPORT_HOME}/perllib");

use strict;

use constant EF_PDF => 1;
use constant EF_HTML => 2;

use Symbol;
use Fcntl qw(:flock);
use File::Basename;
use File::Path;
use Win32::OLE; 
use Win32::OLE::Variant;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

use XReport;

our ($XREPORT_HOME, $workdir, $dbdateformat, $lockdir, $filecachedir); 

my ($JobReportName, $JobReportId, $ReportId, $FileFormat, $dateTime, $FilterValue, $FileId);
my ($LocalPathId_OUT, $LocalFileName, $baseFileName);
my ($Zip, $ZipFileName, @PageXref);
my ($FolderName, $ReportName, $ElabFormat);

our ($NetName, $UserName, $FoldersList, $dbc, @ReportIds, $ListOfReportIds); 

my ($ENVxr, %LockTable, $filecache, $requestVars);

sub min($$) {
  ($_[0]<=$_[1]) ? $_[0] : $_[1];
}

sub max($$) {
  ($_[0]>=$_[1]) ? $_[0] : $_[1];
}

sub dbExecute {
  my $rs = $dbc->Execute( $_[0] );
  if ( $dbc->Errors()->Count() > 0 ) {
    WRITE_ERROR("$_[0]\\n" . $dbc->Errors(0)->Description);
    # $main::Response->Write("Exiting ON DataBase ERROR !!!");
    return undef;
  }
  return $rs;
}

sub GetLock {
  my $LockName = shift; my $lckFh = gensym(); $LockTable{$LockName} = $lckFh;
  open($lckFh, ">$lockdir/$LockName.lck") 
   or return WRITE_ERROR("Unable to open LockFile $LockName - $!\n");
   
  flock($lckFh, LOCK_EX);
}

sub ReleaseLock {
  my $LockName = shift; my $lckFh = $LockTable{$LockName};
  flock($lckFh, LOCK_UN); 
  close($lckFh);
  unlink "$lockdir/$LockName.lck"; delete $LockTable{$LockName}; 
}

sub ReleaseAllLocks {
  for my $LockName (keys(%LockTable)) {
    my $lckFh = $LockTable{$LockName};
    flock($lckFh, LOCK_UN); 
    close($lckFh);
    unlink "$lockdir/$LockName.lck"; delete $LockTable{$LockName}; 
  }
}

sub getFileCache {
  my $dbr = dbExecute("SELECT * from tbl_JobReportsLocalFileCache where JobReportId = $JobReportId "); my ($LocalBasket, $dir);
  return undef if !defined($dbr);
  if ( $dbr->eof() ) {
    GetLock("FileCache_$JobReportId", 30);
    $dbr = dbExecute(
      "SELECT * from tbl_JobReportsLocalFileCache where JobReportId = $JobReportId "
    );
    return undef if !defined($dbr);
    if ( $dbr->eof() ) {
      $LocalBasket = int(rand(64))+1;
      dbExecute(
        "INSERT INTO tbl_JobReportsLocalFileCache (JobReportId, LocalBasket) "
       ."VALUES ($JobReportId, $LocalBasket) "
      );
  	  return undef if !defined($dbr);
    }
    $dbr = dbExecute(
      "SELECT * from tbl_JobReportsLocalFileCache where JobReportId = $JobReportId "
    );
    return undef if !defined($dbr);
    ReleaseLock("FileCache_$JobReportId");
  }

  $LocalBasket = substr("0000".$dbr->Fields()->Item('LocalBasket')->Value(), -5);
  $dir = "$filecachedir/$LocalBasket"; 
  mkdir($dir) if !-e $dir;
  
  return $dir;
}

sub getFoldersList {
  my $UserName_ = $UserName; $UserName_ =~ s/\\/_/g;
  my $fileName = "$workdir/UsersSaveArea/$UserName_\.FoldersList";
  
  debug2log("reading FolderList from $fileName");
  my $fh = new FileHandle("<$fileName") ||  
  	debug2log("Unable to open $fileName - $!");
  return undef unless $fh;	
  binmode($fh); 
  $fh->read($FoldersList, -s $fileName); 
  $fh->close();
  return 1;
}

sub VerifyFolderName {
  debug2log("ver FolderName : $FolderName; FolderLIst: $FoldersList");	
  if ($FolderName ne "" and $FoldersList eq "") {
  	my $resp = getFoldersList();
    return undef unless $resp;
  }
  return 1;
  
  return WRITE_ERROR("FolderName :: $FolderName :: NOT AUTHORIZED")
    if $FolderName ne "" and $FoldersList !~ /\'$FolderName\'/;
}

sub GetReportIds {
  my $dbr;
  debug2log("Getting ReportIds: $ReportId");	
  if ( $ReportId !~ /\d+/ ) {
    $FolderName = $main::Request->QueryString("FolderName")->Item();
    $ReportName = $main::Request->QueryString("ReportName")->Item();
    
    return undef unless VerifyFolderName();
    
    $dbr = dbExecute(
      "EXECUTE ce_JobReportReports$ENVxr "
     ." \@FolderName='$FolderName', \@ReportName='$ReportName', \@JobReportId=$JobReportId "
    );
	return undef if !defined($dbr);
    
    while(!$dbr->eof()) {
      my $FilterValue = $dbr->Fields->Item('FilterValue')->Value();
      my $ReportId = $dbr->Fields->Item('ReportId')->Value();
      
      push @ReportIds, [$FilterValue, $ReportId];
      
      $dbr->MoveNext();
    }
    $dbr->Close();
  }
  else {
    push @ReportIds, ["", $ReportId];
  }

  $ListOfReportIds = join(',', map {$_->[1]} @ReportIds); 
  debug2log("getReportIds List: ".$ListOfReportIds);
  
  return 1 if scalar(@ReportIds) <= 1;
  require Digest::MD5;
  my $MD5Hash = Digest::MD5::md5_hex($ListOfReportIds);

  $dbr = dbExecute(
    "SELECT ReportId from tbl_JobReportsMixedReportIds "
   ."WHERE JobReportId=$JobReportId AND MD5Hash = 0x$MD5Hash "
  );

  if ( $dbr && $dbr->eof() ) {
  	debug2log ("no mixed reportids for $JobReportId .. trying getlock");
    if ( GetLock("MixedReports_$JobReportId", 30) ) {
	    $dbr = dbExecute(
	      "SELECT ReportId from tbl_JobReportsMixedReportIds "
	     ."WHERE JobReportId=$JobReportId AND MD5Hash = 0x$MD5Hash "
	    );
	    if ( $dbr && $dbr->eof() ) {
		  debug2log ("no mixed reportids for $JobReportId .. creating new");
	      $dbr = dbExecute(
	        "SELECT MAX(ReportId) from tbl_JobReportsMixedReportIds "
	       ."WHERE JobReportId=$JobReportId "
	      );
	      if ( $dbr ) {
		      $ReportId = $dbr->Fields()->Item(0)->Value();
		      if ( $ReportId ne "" ) {
		        $ReportId += 1;
		      }
		      else {
		        $dbr = dbExecute(
		          "SELECT MAX(ReportId) from tbl_PhysicalReports$ENVxr "
		         ."WHERE JobReportId=$JobReportId "
		        );
		        $ReportId = $dbr->Fields()->Item(0)->Value()+1 if $dbr;
		      }
		      $dbr = dbExecute(
		        "INSERT INTO tbl_JobReportsMixedReportIds "
		       ."(JobReportId, MD5Hash, ReportId, ListOfReportIds) "
		       ."VALUES "
		       ."($JobReportId, 0x$MD5Hash, $ReportId, '$ListOfReportIds')"
		      ) if $dbr;
	      }
	    }
		debug2log ("Releasing lock");
        ReleaseLock("MixedReports_$JobReportId");
    }
    else { $dbr->Close(); $dbr = undef; }
  }
  elsif ( $dbr ) {
    $ReportId = $dbr->Fields()->Item(0)->Value();
  }
  debug2log ("GetReportIds terminating dbr: ".ref($dbr));
  return undef unless $dbr; 

  $dbr->Close() if $dbr;
  
  return 1;
}

sub PREPARE_EXTRACT {
  my $dbr;
  debug2log("Preparing for extract");
  for my $ENVxr_ ( "" ) { #, "_HIST") {
    $dbr = dbExecute("
      SELECT JobReportName, LocalPathId_OUT, LocalFileName FROM tbl_JobReports$ENVxr_
      WHERE JobReportId = $JobReportId
    "); 
    if (!$dbr->eof()) { $ENVxr=$ENVxr_; last; }
  }

  if ( !$dbr->eof() ) {
    $JobReportName = $dbr->Fields->Item('JobReportName')->Value();
    $LocalPathId_OUT = $dbr->Fields->Item('LocalPathId_OUT')->Value();
    $LocalFileName = $dbr->Fields->Item('LocalFileName')->Value();
  }
  else {
    $main::Response->Write("NOT FOUND $JobReportId $ReportId");
    return undef;
  }

  $JobReportName = uc($JobReportName);

  ### check JobReportName JobReportId ReportId

  $Zip = Archive::Zip->new();

  ($baseFileName = basename($LocalFileName)) =~ s/(\.\d{4}\d{4}\d{6}\.$JobReportId)\.[^.]+\.[^ ]+\.gz(?:\.tar)?$/$1/;
  $dateTime = (split(/\./, $baseFileName))[-2];
  my ($year, $day) = $dateTime =~ /^(\d{4})(\d{4})\d{6}/;

  my $dirIN;
  #mpezzi -----------------------------------------------
  #if ($LocalPathId_OUT eq "") {
  #  $dirIN = "$XREPORT_HOME/storage/OUT/$year/$day";
  #}
  #else {
  #  $dirIN = $main::Application->{"cfg.localpath.".$LocalPathId_OUT}."/OUT/$year/$day";
  #}

  $LocalPathId_OUT = 'default' if ($LocalPathId_OUT eq "");
  $dirIN = $main::Application->{"cfg.localpath.".$LocalPathId_OUT}."/OUT/$year/$day";
  $dirIN =~ s/^file:\/\///;
  #mpezzi -----------------------------------------------


  $ZipFileName = "$dirIN/$JobReportName\.$dateTime\.$JobReportId\.OUT\.#0\.ZIP";

  $ZipFileName = "$dirIN/$JobReportName\.$dateTime\.$JobReportId\.OUT\.ZIP"
   if
  !-e $ZipFileName;
  return WRITE_ERROR("OUT ARCHIVE $ZipFileName (<$dateTime> $baseFileName) NOT FOUND") unless -e $ZipFileName;

  if ( $Zip->read( $ZipFileName ) != AZ_OK ) {
    return WRITE_ERROR("ZIPFILE OPEN ERROR <$dateTime> $baseFileName $ZipFileName");
  }

  #my $PageXrefTxt = $Zip->contents("\$\$PageXref.$JobReportId\.txt"); 

  #for(split("\n",$PageXrefTxt)) {
  #  next if $_ !~ /File=(\d+) +From=(\d+) +To=(\d+)/;
  #  push @PageXref, [$1, $2, $3];
  #}
  
  push @PageXref, ( map { $_ =~ /File=(\d+) +From=(\d+) +To=(\d+)/; [$1, $2, $3] } 
              grep /File=\d+ +From=\d+ +To=\d+/, split("\n", $Zip->contents("\$\$PageXref.$JobReportId\.txt") ) );
  
  if ( !scalar(@PageXref) ) {
    my $dbr = dbExecute("
      SELECT TotPages from tbl_PhysicalReports WHERE JobReportId = $JobReportId AND ReportId = 0
    ");
    push @PageXref, [0, 1, $dbr->Fields->Item(0)->Value()]; $dbr->Close();
  }
  
  return 1;
  
}

sub expListOfPages {
  use Data::Dumper;
  my $ListOfPages = shift; my (@ListOfPages, %ListOfPages);

#  my @LOP = split(/\D+/, $ListOfPages);
  for ( my @LOP = split(/\D+/, $ListOfPages); @LOP; ) {
#  while ( @LOP ) {
    my ($fm, $req) = (shift @LOP, shift @LOP); 

    my $to = $fm + $req - 1; my $found = 0;

#    for ($_=0; $_<scalar(@PageXref); $_+=3) {
#      if ( 
#        ( $fm >= $PageXref[$_+1] and $fm <= $PageXref[$_+2] ) or
#        ( $to >= $PageXref[$_+1] and $to <= $PageXref[$_+2] )
#      ) {
#        my $FileId = $PageXref[$_]; 
#        if ( !exists($ListOfPages{"$FileId"}) ) {
#          $ListOfPages{$FileId} = [$FileId];
#        }
#        my $fm_ = max($fm,$PageXref[$_+1]);
#        my $to_ = min($to,$PageXref[$_+2]);
#        push @{$ListOfPages{$FileId}}, $fm_, $to_- $fm + 1;
#        $found += $to_ - $fm_ + 1;
#      }
#    }
    foreach my $pel ( @PageXref ) {
      if ( ( $fm >= $pel->[1] and $fm <= $pel->[2] ) or
	   ( $to >= $pel->[1] and $to <= $pel->[2] ) 
      ) {
        my $FileId = $pel->[0]; 
        $ListOfPages{$FileId} = [$FileId] if ( !exists($ListOfPages{"$FileId"}) );

        my $maxfm_ = max($fm,$pel->[1]);
        my $minto_ = min($to,$pel->[2]);
        push @{$ListOfPages{$FileId}}, $maxfm_, $minto_- $fm + 1;
        $found += $minto_ - $maxfm_ + 1;
      }
    }
#    die "$fm $to $req ====\n====". Dumper(\@PageXref) ."=====\n==== " . Dumper(\%ListOfPages) ."=====\n===== $found ======\n";
    die "NUMBER MISMATCH ". join("/", $JobReportId, $fm, $to, $req, $found) if ( $req != $found );
  }
  for my $FileId (sort(keys(%ListOfPages))) {
    my $fileList = $ListOfPages{$FileId};
    if ( 
      $fileList->[1] == $PageXref[$FileId]->[1]
      and $fileList->[2] ==  $PageXref[$FileId]->[2] - $PageXref[$FileId]->[1] + 1
     ) {
      push @ListOfPages, $FileId;
    }
    else {
      push @ListOfPages, $fileList;
    }
  }

#  die "====". $ListOfPages ."=====\n====". Dumper(\@PageXref) ."=====\n==== " . Dumper(\%ListOfPages) ."=====\n=====" . Dumper(\@ListOfPages) . "======\n";
  return \@ListOfPages
}

sub ExtractAndConvertToHtml {
  my $FileId = shift; 

  $filecache = getFileCache();
  return undef if !defined($filecache);
  
   mkpath($filecache);

  my $fileName_raw = ExtractFileToCache( $FileId, 'dat' ); my $fileName_html;

  ($fileName_html = $fileName_raw) =~ s/\.dat$/.html/i;

  if ( !-e $fileName_html ) {
    ### todo: getLock/releaseLock
    my ($INPUT, $OUTPUT) = (gensym(),gensym()); 

    open($INPUT, "<$fileName_raw") 
     or
    die("OPEN INPUT ERROR for \"$fileName_raw\" $!"); binmode($INPUT);

    open($OUTPUT, ">$fileName_html\.PENDING") 
     or
    die("OPEN OUTPUT ERROR for \"$fileName_html\.PENDING\" $!"); binmode($OUTPUT);

    my ($rec, $beg, $end, $line, @classes, $ipage, $iline, $iclass);

    $end = ((-s $fileName_raw) - 8); seek($INPUT, $end, 0); read($INPUT, $rec, 4); 
    $beg = unpack("N", $rec) + 12;  seek($INPUT, $beg, 0); read($INPUT, $rec, $end-$beg-4);

    my @PageOffsets = unpack("N*", $rec); $ipage=0; @classes = ('even', 'odd');

    print $OUTPUT 
     "<link type=\"text/css\" rel=\"stylesheet\" href=\"css/cereport.css\">".
     "<body class=\"htmlobject\">"
    ;

    for ($end = shift(@PageOffsets); @PageOffsets; ) {
      $beg=$end; $end = shift(@PageOffsets); $iclass = 0; $iline = 0;
      seek($INPUT, $beg, 0); read($INPUT, $rec, $end-$beg); 
      print $OUTPUT "<a name=\"PAGE$ipage\"/><pre>\n";
      my ($at, $atmax, $ll) = (0, length($rec), 0);
      while($at<$atmax) {
        if ( ($iline % 4) == 0 ) {
          print $OUTPUT "</div>" if $iline > 0;
          print $OUTPUT "<div class=\"$classes[$iclass++ % 2]\">";
        }
        $ll = unpack("n", substr($rec,$at,2)); 
        $line = substr($rec, $at+3, $ll-1); 
        $line =~ s/\</\&lt;/g;
        print $OUTPUT "$line\n"; $iline += 1; $at += 2+$ll;
      }
      print $OUTPUT "</pre><hr size=\"1\" style=\"page-break-after:always;\"/>"; $ipage+=1;
    }

    print $OUTPUT "\n</body>";


    close($OUTPUT); close($INPUT);

    rename("$fileName_html\.PENDING", "$fileName_html"); 
  }
}

sub ExtractAndConvertToPdfAndSend {
  my $FileId = shift; 

  $filecache = getFileCache();
  return undef if !defined($filecache);
  mkpath($filecache);

   my $INPUT = gensym(); 

  require XReport::PDF::REPORT;

  my $f_dat = ExtractFileToCache( $FileId, 'dat' ); my ($f_lock, $f_pdf);

  ($f_pdf = $f_dat) =~ s/\.dat$/.pdf/i;

  $f_lock = "CreatePdfReport_$JobReportId\_$FileId";

  do {
    GetLock($f_lock, 30); 
    ReleaseLock($f_lock) if -e $f_pdf;
  } 
  if (!-e $f_pdf);

  if (!-e $f_pdf) {
    my $dbr = dbExecute("
      SELECT * FROM tbl_JobReportNames WHERE JobReportName = '$JobReportName'
    ");
    my $cpl = $dbr->Fields()->Item('CharsPerLine')->Value(); 
    my $lpp = $dbr->Fields()->Item('LinesPerPage')->Value(); 
    $dbr->Close();

    open($INPUT, "<$f_dat") 
     or
    die("OPEN INPUT ERROR for \"$f_dat\" $!"); binmode($INPUT);

    my $doc = XReport::PDF::REPORT->new(
      CharsPerLine=>$cpl, LinesPerPage => $lpp, OUTPUT=> $f_pdf
    );

    my ($beg, $end, $xref, $line, $rec, @lList);

    $end = ((-s $f_dat) - 8); seek($INPUT, $end, 0); read($INPUT, $rec, 4); $xref = unpack("N", $rec);
    $beg = $xref + 12;  seek($INPUT, $beg, 0); read($INPUT, $rec, $end-$beg);

    my @PageOffsets = (unpack("N*", $rec), $xref); 

    for ($end = shift @PageOffsets; @PageOffsets; ) {
      $beg = $end; $end = shift @PageOffsets; 

      seek($INPUT, $beg, 0); 
      read($INPUT, $rec, $end-$beg);

      my ($at, $atmax, $ll) = (0, length($rec), 0);
      while($at < $atmax) {
        $ll = unpack("n", substr($rec,$at,2)); 
        $line = substr($rec, $at+3, $ll-1); 
        push @lList, $line; $at += 2+$ll;
      }
      $doc->NewPageFomLineList(\@lList); @lList=();
    }

    $doc->Close(); close($INPUT);

    rename("$f_pdf\.PENDING", "$f_pdf"); ReleaseLock($f_lock); 
  };

  $main::Response->{'ContentType'} = "application/pdf";
  
  $main::Response->AddHeader("Cache-Control", "max-age=43200");
#  $main::Response->AddHeader("Content-Length", -s $f_pdf);
  $main::Response->AddHeader("ETag", "XReport/$f_pdf");

  open($INPUT, "<$f_pdf"); binmode($INPUT); my $rec;

  while(!eof($INPUT)) {
    read($INPUT, $rec, 32768);
    $main::Response->BinaryWrite(new Win32::OLE::Variant(VT_UI1, $rec));
  }
  close($INPUT);
}

sub ExtractAndSendObject { 
  my ($FileId, $FileFormat) = (shift, shift); my $ContentType = 'application/pdf';

  $ContentType = 'application/vnd.ms-excel' 
   if 
  uc($FileFormat) eq 'EXCEL';

  ($ContentType, $FileFormat) = ('Application/xreport-text', 'txt') 
   if 
  uc($FileFormat) eq 'TEXT';

  my $fileName = "$JobReportName\.$JobReportId\.$FileId\.#0\.".$FileFormat;
  $main::Response->AddHeader("Content-Disposition", 'attachment; filename='.$fileName);

  my $accept_encoding = $main::Request->ServerVariables->{'HTTP_ACCEPT_ENCODING'}->Item(); 
  
  my $zipMember = $Zip->memberNamed( $fileName ); my $ObjectLength;

  return WRITE_ERROR(
    "ZIP FILE MEMBER NOT FOUND -$dateTime- $baseFileName -- $ZipFileName -- $fileName"
  ) 
  if !$zipMember;
  
  $main::Response->{'ContentType'} = $ContentType;
  
  $main::Response->AddHeader("Cache-Control", "max-age=43200");
  
  if ( 1 or $accept_encoding !~ /gzip/ ) {
    $zipMember->desiredCompressionMethod(COMPRESSION_STORED);
    $ObjectLength = $zipMember->uncompressedSize(); 
  }
  else {
    ### todo: add headers and trailers to make a real gziped formatted file // 
    ### note: for ADOBE PDF OCX OBJECT gzip doesn't work ------------------ //
    $zipMember->desiredCompressionMethod(COMPRESSION_DEFLATED);
    $ObjectLength = $zipMember->compressedSize();
    $main::Response->AddHeader("Content-Encoding","gzip");
    # -------------------------------------------------------------------#
  }

  $main::Response->AddHeader("ETag", "XReport/$fileName");
  
  $zipMember->rewindData();

#  $main::Response->AddHeader("Content-Length", $ObjectLength);

  my $lastChunkLength = 0; my $pdfV = undef;

  while( !$zipMember->readIsDone() ) {
    my ($ref, $status) = $zipMember->readChunk( 32768 );
    if ( $status != AZ_OK and $status != AZ_STREAM_END ) {
      die ("READ ERROR SESSIONID=".$main::Session->SessionID()." rc=$status");
    }
    if ( !$pdfV or $lastChunkLength != length($$ref) ) {
      $pdfV = new Win32::OLE::Variant(VT_UI1, $$ref);
    }
    else {
      $pdfV->Put($$ref);
    }
    $main::Response->BinaryWrite($pdfV);
    $lastChunkLength = length($$ref); 
  }

  $zipMember->endRead();
}

sub ExtractFileToCache {
  my ($FileId, $ext) = (shift, shift); my $fileName = "$JobReportName\.$JobReportId\.$FileId.#0.$ext";
  
  return "$filecache/$fileName" if -e "$filecache/$fileName";

  GetLock("ExtractJobReport_$JobReportId", 30);

  if ( -e "$filecache/$fileName" ) {
    ReleaseLock("ExtractJobReport_$JobReportId");
    return;
  }
 
  my $desired = COMPRESSION_STORED;
  
  my $zipMember = $Zip->memberNamed( $fileName ); my $ObjectLength;

  if ( !$zipMember ) {
    return WRITE_ERROR("ZIP FILE MEMBER NOT FOUND <$dateTime> $baseFileName $ZipFileName $fileName");
  }
  $zipMember->desiredCompressionMethod(COMPRESSION_STORED);

  $zipMember->rewindData();

  my $OUTPUT = gensym();
  open($OUTPUT, ">$filecache/$fileName.PENDING") 
   or 
  return WRITE_ERROR("OUTPUT OPEN ERROR for \"$filecache/$fileName.PENDING\" $!"); binmode($OUTPUT);

  my $lastChunkLength = 0; 
  while( !$zipMember->readIsDone() ) {
    my ($ref, $status) = $zipMember->readChunk( 32768 );
    if ( $status != AZ_OK and $status != AZ_STREAM_END ) {
      $main::Response->AppendToLog("GetObject READ ERROR SessionID<". $main::Session->SessionID(). "> $status ??\n");
    }
    print $OUTPUT $$ref;
    $lastChunkLength = length($$ref); 
  }
  close($OUTPUT);

  $zipMember->endRead();

  rename("$filecache/$fileName.PENDING", "$filecache/$fileName");

  ReleaseLock("ExtractJobReport_$JobReportId");

  return "$filecache/$fileName";
}

sub CheckPdfFile {
  open(PDFIN, "<$_[0]") or return 0; seek(PDFIN,-50,2); read(PDFIN, my $str,50); close(PDFIN);
  
  return $str =~ /startxref\s+(\d+)\s+\%\%EOF/s;
}

sub BuildAndSendObject {
  ### todo: manage more than a file from  
  my $lclcfg = { %$XReport::cfg };
  delete $lclcfg->{sqlprocs};
  use Data::Dumper;
  debug2log("BuildAndSendObject - Values:\n".Dumper($lclcfg));
  
  my ($ReportId, $ListOfPages) = @_; my $fileTo = "$JobReportName\.$JobReportId\.$ReportId\.#0.pdf";
  
  if ( !-e "$filecache/$fileTo" or !CheckPdfFile("$filecache/$fileTo") ) {
  
    if ( GetLock("ExtractReport_$JobReportId\_$ReportId", 30) ) {
    
	    require XReport::PDF::DOC; 
	    
	    if ( !-e "$filecache/$fileTo" or !CheckPdfFile("$filecache/$fileTo")) { 
	      my $docTo = XReport::PDF::DOC->Create("$filecache/$fileTo.PENDING") ;
	      
	      for my $ListOfPages (@$ListOfPages) {
	        my $docFm = XReport::PDF::DOC->Open(
	          ExtractFileToCache( $ListOfPages->[0], 'pdf' )
	        );
	        shift @$ListOfPages;
	        $docTo->CopyPages($docFm, @$ListOfPages); $docFm->Close();
	      }
	      
	      $docTo->Close();
	
	      rename("$filecache/$fileTo.PENDING", "$filecache/$fileTo");
	    }
	    ReleaseLock("ExtractReport_$JobReportId\_$ReportId");
    }
  }
  my $fileName = "$filecache/$fileTo"; my $ObjectLength = -s "$fileName";

  $main::Response->{'ContentType'} = "application/pdf";
  $main::Response->AddHeader("Content-Disposition", "inline; filename=$fileTo");
  
  $main::Response->AddHeader("Cache-Control", "max-age=43200");
#  $main::Response->AddHeader("Content-Length", $ObjectLength);
  $main::Response->AddHeader("ETag", "XReport/$fileTo");

  my $INPUT = gensym;

  open($INPUT, "<$fileName") or return WRITE_ERROR("INPUT OPEN ERROR $!"); binmode($INPUT);
  my $lastChunkLength = 0; my ($pdf, $pdfV) = (undef, undef);
  while( !$INPUT->eof() ) {
    read($INPUT, $pdf, 32768);
    if ( !$pdfV or $lastChunkLength != length($pdf) ) {
      $pdfV = new Win32::OLE::Variant(VT_UI1, $pdf);
    }
    else {
      $pdfV->Put($pdf);
    }
    $main::Response->BinaryWrite($pdfV);
    $lastChunkLength = length($pdf); 
  }
  close($INPUT);
}

sub checkUser {
  debug2log("Checking UserName: ".$requestVars->{UserName});  
  $UserName = $requestVars->{'UserName'}; return $UserName if $UserName ne ""; 
  
  my $cookieuname = GetUserLocalSessionCookie(); return $cookieuname if $cookieuname ne "";
  return WRITE_ERROR('UTENZA NON CONOSCIUTA O SCADUTA'); 

  #EXIT_ERROR("UTENZA NON CONOSCIUTA O SCADUTA");
  return undef;
}

sub getInputVars { 
	
	debug2log("getInputVars - starting");
	use URI::Escape;
	my @reqStrings = ();
  	if ( my $qryStr = $main::Request->ServerVariables('QUERY_STRING')->item() ) { 
	    $qryStr =~ s/\+/ /g;
	    push @reqStrings, $qryStr;
  	}
    
	my $tb = $main::Request->TotalBytes();
	debug2log("getInputVars - binary Reading $tb totalbytes");
	if ( $tb ) {
		(my $pstStr = $main::Request->BinaryRead($tb)) =~ s/\+/ /g;
		push @reqStrings, $pstStr; 
	}
	
	debug2log("getInputVars - "
				."\ncaller: ". join('::', (caller())[0,2])
				."\nJoin: ".join('&',@reqStrings)
				);
	
	my $methodVars = {
		map { my ($varn, $varv) = $_ =~  /^(\w+)=(.*)$/; 
  			$varn => ( $varn =~ /^(?:UserTimeRef)$/ ? 
  			           sql_convertdate($varv)
  		               : $varv);} split("&",uri_unescape(join('&',@reqStrings)))
	};
	
	RESTART_REQUEST() unless scalar(keys %$methodVars);
	
  	return $methodVars;
}

sub GETOBJ_INIT_ALL {
  ($NetName, $UserName) = split(/\\/, $main::Request->ServerVariables("AUTH_USER")->Item());
  my $iv = getInputVars();
  
  debug2log("GetObject INIT Started:\n"
  	.Dumper(\{NetName => $NetName,
  			UserName => $UserName,
  			InputVars => $iv, 
  			}));

  $XREPORT_HOME = $main::Application->{XREPORT_HOME}; $ENVxr = ""; 

  $dbc = $main::Server->CreateObject('ADODB.Connection');
  $dbc->Open($main::Application->{'cfg.db.XREPORT'});

  $workdir = $main::Application->{'cfg.workdir'} || "$ENV{TEMP}";
  $dbdateformat = uc($main::Application->{'cfg.db.dateformat'}) || "YMD";
  $lockdir = $XReport::cfg->{'LocksHome'};
  $filecachedir = $XReport::cfg->{'filecache'};

  $JobReportId = $main::Request->QueryString("JobReportId")->Item();
  $ReportId = $main::Request->QueryString("ReportId")->Item();
  $ReportId = 0 unless $ReportId;
  $FileId = $main::Request->QueryString("FileId")->Item();
  $FileFormat = $main::Request->QueryString("FileFormat")->Item();

  $main::Response->{'Buffer'} = 0; 
  $FileFormat = lc($FileFormat);

  return undef unless checkUser();
  
  return undef unless PREPARE_EXTRACT();

  debug2log("GetObject INIT Ended:\n"
  	.Dumper(\{JRID => $JobReportId,
  			RepId => $ReportId,
  			ReportName => $ReportName,
  			FileId => $FileId,
  			FileFormat => $FileFormat,
  			UserName => $UserName,
  			}));
  			
  return 1;
}

sub GETOBJ_TERMINATE_ALL {
  debug2log("getObject TERMINATE_ALL called by: ".join('::', (caller())[0,2])." - ".$_[0]);
  ReleaseAllLocks(); $dbc->Close() if $dbc;
}

sub BY_REPORTIDs {
  debug2log("BY_REPORTID - Starting");
  return undef unless GetReportIds();
  
  my $ListOfPages = ""; 

  my $dbr = dbExecute("
    SELECT ReportId, ListOfPages from tbl_PhysicalReports$ENVxr
    WHERE JobReportId=$JobReportId AND ReportId IN ($ListOfReportIds)
    ORDER BY ReportId 
  ");
  return undef unless $dbr;
  
  while(!$dbr->eof()) {
    $ListOfPages .= $dbr->Fields()->Item('ListOfPages')->Value() . ' ';
    $dbr->MoveNext();
  }
  $dbr->Close();
  debug2log("BY_REPORTID LOP: ".$ListOfPages);
  $ListOfPages = expListOfPages($ListOfPages); 
#  die "===>4 LOP: ". join('::', @{$ListOfPages->[0]}) . " <===\n";

  if ( scalar(@$ListOfPages) == 1 and !ref($ListOfPages->[0]) ) {
    eval { ExtractAndSendObject( $ListOfPages->[0], $FileFormat ) };
  }
  else {
    eval {
      
  		$filecache = getFileCache();
  		return undef if !defined($filecache);
  		mkpath($filecache);

   		my $ReportIdExtractedFile = $ReportId; 

      $ReportIdExtractedFile .=  (($ListOfReportIds =~ /\,/) ? '#MIXED' : '#UNIQUE');
    
      BuildAndSendObject($ReportIdExtractedFile, $ListOfPages);

      $dbr = dbExecute(
        "UPDATE tbl_JobReportsLocalFileCache "
       ."set UsedTimes = UsedTimes+1, LastUsedTime = GETDATE() "
       ."WHERE JobReportId = $JobReportId "
      );
      return undef unless $dbr;
    };
  }
}

sub BY_FILEID {
  debug2log("BY_FILEID - Starting\n");
  getFoldersList() if $FoldersList eq "";

  my $dbr = dbExecute ("
    SELECT DISTINCT a.FilterValue, a.ReportId from tbl_LogicalReports$ENVxr a
      INNER JOIN tbl_FoldersReportNames_LL1 b ON 
      a.ReportName = b.ReportName 
      AND a.FilterValue = b.FilterValue 
      AND a.XferRecipient = b.RecipientName 
    WHERE 
      b.FolderName IN($FoldersList)
      AND a.JobReportId = $JobReportId
      AND a.ReportId = 0
  ");
  return undef unless $dbr;
  
  return WRITE_ERROR("NOT AUTHORIZED // $FoldersList // $ListOfReportIds // !!!") if $dbr->eof(); $dbr->Close();
  return undef unless $dbr;

  $dbr = dbExecute("
    SELECT ElabFormat FROM tbl_JobReportsElabOptions WHERE JobReportId = $JobReportId
  ");
  return undef unless $dbr;

  if (!$dbr->eof()) {
    $ElabFormat = $dbr->Fields->Item('ElabFormat')->Value();
  }
  else {
    $ElabFormat = EF_PDF;
  }
  if ( $ElabFormat == EF_PDF ) { 
    ExtractAndSendObject( $FileId, $FileFormat );
  }
  else {
    ExtractAndConvertToPdfAndSend( $FileId );
  }
}

my @routines = qw(logFunctions.pl authFunctions.pl); 
my $binpath = $main::Request->ServerVariables('APPL_PHYSICAL_PATH')->Item();
my $ca = $main::Application->{'cfg.authenticate'};
push @routines, "authFunctions_$ca.pl" if ( -f $binpath."authFunctions_$ca.pl");
for my $fname ( @routines ) {
	my $fpath = $binpath.$fname;
	require "$fpath";
}
debug2log("getObject Iinitialized Fileid: $FileId");

if ( GETOBJ_INIT_ALL() ) { 
	my $resp = ($FileId ? BY_FILEID() : BY_REPORTIDs());
}

debug2log("getObject Terminating");
GETOBJ_TERMINATE_ALL();

</SCRIPT>
