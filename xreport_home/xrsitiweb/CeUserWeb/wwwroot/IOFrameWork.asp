<% @Language=Perlscript %>
<%

use lib("$ENV{XREPORT_HOME}/perllib");

use XReport;
use XML::Parser::Expat;
use Win32::OLE;

my $upxml = $Request->Form( 'XMLChangedSub' )->Item();
my $tblName = $Request->Form( 'tblName' )->Item();
my $tblConditions = $Request->Form( 'tblConditions' )->Item();

my $keys;
my $fldType;
my $attributes;
my $xml = "";
my $dbc;
my $dbError = "";
my $dbcstr = $XReport::cfg->{dbase}->{XREPORT};

if( $tblName !~ /^$/ ) {
	$dbc = Win32::OLE->CreateObject( "ADODB.Connection" );
	if( !$dbc ) {
		$dbError = "ADODB create connection error";
	} else {
		$dbc->Open( $dbcstr );
		if( !$dbc or $dbc->State() == adStateClosed ) {
			$dbError = "ADODB Open connection error";
		} else {
			$dbc->LetProperty( "CommandTimeout", "600" );
		
			if( $upxml !~ /^$/ ) {
				my $rs = $dbc->Execute( "exec sp_pkeys \@table_name = \'$tblName\'" );
				if( $dbc->Errors()->Count() > 0 ) {
					$dbError = $dbc->Errors(0)->Description();
				} else {
					my $Fields = $rs->Fields(); my $FieldsCount = $Fields->Count();
	
					while( !$rs->eof() ) {
						$keys->{$Fields->Item( 'COLUMN_NAME' )->Value()} = $Fields->Item( 'KEY_SEQ' )->Value();
						$rs->MoveNext();
					}
					$rs->Close();
		
					$rs = $dbc->Execute( "exec sp_columns \@table_name = \'$tblName\'" );
					if( $dbc->Errors()->Count() > 0 ) {
						$dbError = $dbc->Errors(0)->Description();
					} else {
						$Fields = $rs->Fields(); $FieldsCount = $Fields->Count();
		
						while( !$rs->eof() ) {
							$fldType->{$Fields->Item( 'COLUMN_NAME' )->Value()} = $Fields->Item( 'DATA_TYPE' )->Value();
							$rs->MoveNext();
						}
						$rs->Close();
	
						my $parser = new XML::Parser::Expat;
						$parser->setHandlers( 'Start' => \&startHandler, 'End' => \&endHandler );
						$parser->parse( $upxml );
					}
				}
			}
	
			if( $tblName !~ /^$/ ) {
				my $rs = $dbc->Execute( "select top 100 * from $tblName $tblConditions" );
				if( $dbc->Errors()->Count() > 0 ) {
					$dbError = $dbc->Errors(0)->Description();
				} else {
					$xml = "<data>\n";
		
					my $Fields = $rs->Fields(); my $FieldsCount = $Fields->Count();

					if( !$rs->eof() ) {
						while (!$rs->eof()) {
							$xml .= "<row ";
							for (my $j=0; $j < $FieldsCount; $j++) {
								$field = $Fields->Item($j); 
								$field_name = $field->Name(); $field_type = $field->Type(); 
								$field_value = $field->Value();
								$xml .= "$field_name=\"".xmlquote( $field_value )."\" "; 
							}
							substr($xml,-1) = "/>\n";
							$rs->MoveNext();
						}
					} else {
						$rs->Close();
						$rs = $dbc->Execute( "exec sp_columns \@table_name = \'$tblName\'" );
						if( $dbc->Errors()->Count() > 0 ) {
							$dbError = $dbc->Errors(0)->Description();
						} else {
							$Fields = $rs->Fields(); $FieldsCount = $Fields->Count();
			
							$xml .= "<row ";
							while( !$rs->eof() ) {
								$field_name = $Fields->Item( 'COLUMN_NAME' )->Value(); 
								$xml .= "$field_name=\"\" "; 
								$rs->MoveNext();
							}
							substr($xml,-1) = "/>\n";
							$rs->Close();
						}
					}
			
					$xml .= "</data>";
					$rs->Close();
				}
			}
		}
		$dbc->Close();
	}
}

sub xmlquote {
  my $t = shift; $t =~ s/ +$//g;
  
  $t =~ s/&/&amp;/g;
  $t =~ s/</&lt;/g;
  $t =~ s/>/&gt;/g;
  $t =~ s/\"/&quot;/g;
  $t =~ s/\"/&apos;/g;
  
  $t =~ s/([\x80-\xff])/"\&\#".unpack("C",$1).";"/eg;
  
  return $t;
}

sub startHandler {
	my ( $p, $el, @attr ) = @_;

	if( $el =~ /^row$/ ) {
		for( my $count = 0; $count <= $#attr; $count += 2 ) {
			$attributes->{$attr[$count]} = $attr[$count + 1];
		}

		my $sql = "";
		if( $attributes->{action} =~ /^update$/ ) {
			$sql = "update $tblName set ";
			foreach my $fld( keys( %{$attributes} ) ) {
				$sql .= "$fld = ".
						sqlquote( $attributes->{$fld}, $fldType->{$fld} ).
						", " if( !defined( $keys->{$fld} ) and $fld !~ /^action$/ );
			}
			substr( $sql, -2 ) = " where ";
			my $flag = 0;
			foreach my $fld( keys( %{$keys} ) ) {
				$sql .= "and " if( $flag == 1 );
				$sql .= "$fld = ".sqlquote( $attributes->{$fld}, $fldType->{$fld} )." ";
				$flag = 1;
			}
			substr( $sql, -4 ) = "" if( $sql =~ /^.*and\s$/ );
		}

		if( $attributes->{action} =~ /^insert$/ ) {
			$sql = "insert into $tblName ( ";
			foreach my $fld( keys( %{$attributes} ) ) {
				$sql .= "$fld, " if( $fld !~ /^action$/ );
			}
			substr( $sql, -2 ) = " ) values( ";
			foreach my $fld( keys( %{$attributes} ) ) {
				$sql .= sqlquote( $attributes->{$fld}, $fldType->{$fld} ).", " if( $fld !~ /^action$/ );
			}
			substr( $sql, -2 ) = " )";
		}

		if( $attributes->{action} =~ /^delete$/ ) {
			$sql = "delete $tblName where ";
			my $flag = 0;
			foreach my $fld( keys( %{$keys} ) ) {
				$sql .= "and " if( $flag == 1 );
				$sql .= "$fld = ".sqlquote( $attributes->{$fld}, $fldType->{$fld} )." ";
				$flag = 1;
			}
			substr( $sql, -4 ) = "" if( $sql =~ /^.*and\s$/ );
		}

		my $rs = $dbc->Execute( $sql );
		if( $dbc->Errors()->Count() > 0 ) {
			$dbError = $dbc->Errors(0)->Description();
		}
		$Response->Write( $sql );
	}
}

sub endHandler {
}

sub sqlquote {
	my ( $val, $type ) = @_;

	$val =~ s/&amp;/&/g;
	$val =~ s/&lt;/</g;
	$val =~ s/&gt;/>/g;
	$val =~ s/&apos;/\"/g;
  
	$val =~ s/"\&\#".unpack("C",$1).";"/([\x80-\xff])/eg;

	$val = "\'$val\'" if( ( $type == 12 ) or ( $type == 1 ) );
	$val = 0 if( ( $val == "" ) and ( ( $type == -7 ) or ( $type == 3 ) or ( $type == 4 ) or ( $type == 5 ) ) );

	return $val;
}
%>

<html>
<head><title></title></head>
<body>
<form id="frmData" method="POST" action="IOFrameWork.asp">
   <xml id="DataXML">
<% $Response->Write( $xml ); %>
   </xml>
<xml id="XMLChanged">
</xml>
<textarea name="XMLChangedSub">
</textarea>
<input type="hidden" name="tblName" value="<% $Response->Write( $tblName ); %>" />
<input type="hidden" name="tblConditions" value="<% $Response->Write( $tblConditions ); %>" />
</form>
<% $Response->Write( $dbError ); %>
</body>
</html>
