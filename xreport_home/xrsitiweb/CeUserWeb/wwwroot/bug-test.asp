<% @LANGUAGE = PerlScript %>
<html>

<head><title>Perscript bug demo</title></head>

<body>


<%
use vars qw($Response $Server);


$Response->Write("<p>Refresh this page, and every other time you do so it will throw an exception.!</p>\n");
$Response->End;

%>

</body>
</html>
