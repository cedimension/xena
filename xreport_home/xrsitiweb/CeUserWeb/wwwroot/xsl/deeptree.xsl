<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:msxsl="urn:schemas-microsoft-com:xslt"
>
<xsl:output media-type="text/html" encoding="UTF-8" indent="yes"/>
 
  <xsl:template match="/">
   <html>
    <head>
     <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
     <META HTTP-EQUIV="Expires" CONTENT="-1"/>
	 <style>
	  body, TD {
	    font-size:14;
	  }
	 </style>
    </head>
    <body>
	 <span id="CONTENT">
      <xsl:apply-templates select="UserTrees"/>
     </span>
    </body>
	<script> top.TreeExp.End() </script>
   </html>
  </xsl:template>
  

  <xsl:template match="UserTrees">
   <span class="TREEHDR">Cartelle Tabulati per <xsl:value-of select="@for"/></span><br/>
   <table onclick="TabClicked()" ftype="Top" fref="/FolderList" width="100%" border="0" cellspacing="0" cellpadding="0">
    <xsl:apply-templates/>
   </table>
  </xsl:template>


  <xsl:template match="TreeNode[TreeNode]">
   <tr class="fl" >
	<xsl:if test="@folder=1">
	 <xsl:attribute name="target">fl:<xsl:value-of select="@name"/></xsl:attribute>
	</xsl:if>
    <xsl:attribute name="HasChilds">1</xsl:attribute>
    <td>
	 <img src="images/plus.gif" class="fPM"/>
	</td>
    <td nowrap="true">
	 <span class="flk">&#160;<xsl:value-of select="@name"/>&#160;<xsl:value-of select="@descr"/></span>
	</td>
	<td width="99%">&#160;</td>
   </tr>
   
   <tr style="display:none">
    <td></td>
	<td colspan="2">
	 <table border="0" cellspacing="0" cellpadding="0">
	  <xsl:apply-templates/>
	 </table>
	</td>
   </tr>
  </xsl:template>


  <xsl:template match="TreeNode">
   <tr class="fl" >
	<xsl:attribute name="target">fl:<xsl:value-of select="@name"/></xsl:attribute>
    <xsl:attribute name="fref">fl:<xsl:value-of select="@name"/></xsl:attribute>
    <xsl:attribute name="HasChilds">0</xsl:attribute>
    <td>
	 <img src="images/red.gif"/>
	</td>
    <td nowrap="true">
	 <span class="flk">&#160;<xsl:value-of select="@name"/>&#160;<xsl:value-of select="@descr"/></span>
	</td>
	<td width="99%">&#160;</td>
   </tr>
  </xsl:template>
  

</xsl:stylesheet>
