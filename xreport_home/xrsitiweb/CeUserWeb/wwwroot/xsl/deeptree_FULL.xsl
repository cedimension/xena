<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:msxsl="urn:schemas-microsoft-com:xslt"
 xmlns:js="urn:JScript"
>
<xsl:output media-type="text/html" encoding="UTF-8" indent="yes"/>
 

  <msxsl:script language="JScript" implements-prefix="js"><![CDATA[
   var atLevel = -1, isLast = new Array()
   
   function setLevel(incr) {
     atLevel += incr
	 if ( incr > 0 ) {
	   isLast[atLevel] = arguments[1]
	 }
	 return ""
   }
   
   function getLevel() {
     return atLevel
   }
   
   function getLeft() {
     var left = '', img
     for (var i=0;i<atLevel;i++) {
	   img = (isLast[i]) ? "blank" : "I"
       left += "<td><img src=\"images/"+img+".png\"/></td>"
	 }
	 return left;
   }

  ]]></msxsl:script>
  

  <xsl:template match="/">
   <html>
    <head>
     <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
     <META HTTP-EQUIV="Expires" CONTENT="-1"/>
	 <style>
	  body, TD {
	    font-size:14;
	  }
	 </style>
    </head>
    <body>
	 <span id="CONTENT">
      <xsl:apply-templates select="UserTrees"/>
     </span>
    </body>
	<script> top.TreeExp.End() </script>
   </html>
  </xsl:template>
  

  <xsl:template match="UserTrees">
   <span class="TREEHDR">Folders for <xsl:value-of select="@for"/></span><br/>
   <table onclick="TabClicked()" ftype="Top" fref="/UFolderList" colspan="20" width="100%" border="0" cellspacing="0" cellpadding="0">
    <xsl:apply-templates/>
   </table>
  </xsl:template>


  <xsl:template match="TreeNode[TreeNode]">
   <xsl:value-of select="js:setLevel(1,position()=last())"/>
  
   <tr class="fl" >
	<xsl:if test="@folder=1">
	 <xsl:attribute name="target">fl:<xsl:value-of select="@name"/></xsl:attribute>
	 <xsl:attribute name="fref">fl:<xsl:value-of select="@name"/></xsl:attribute>
	</xsl:if>
    <xsl:value-of select="js:getLeft()" disable-output-escaping="yes"/>
    <td>
	 <xsl:choose>
	  <xsl:when test="position()!=last()">
	   <img src="images/Tplus.png" class="fPM"/>
	  </xsl:when>
	  <xsl:otherwise>
	   <img src="images/Lplus.png" class="fPM"/>
	  </xsl:otherwise>
	 </xsl:choose>
	</td>
    <td>
	 <img class="fPM">
	  <xsl:attribute name="src">
	   <xsl:choose>
	    <xsl:when test="@folder=1">images/foldericon.gif</xsl:when>
	    <xsl:otherwise>images/nodeicon.gif</xsl:otherwise>
	   </xsl:choose>
	  </xsl:attribute>
	 </img>
	</td>
	<td>&#160;</td>
    <td nowrap="true">
	 <span class="flk"><!--xsl:value-of select="@name"/-->&#160;<xsl:value-of select="@descr"/></span>
	</td>
	<td width="99%">&#160;</td>
   </tr>
   
   <tr style="display:none">
	<td colspan="20" width="100%">
	 <table border="0" cellspacing="0" cellpadding="0">
	  <xsl:apply-templates/>
	 </table>
	</td>
   </tr>
   
   <xsl:value-of select="js:setLevel(-1)"/>
  </xsl:template>


  <xsl:template match="TreeNode">
   <xsl:value-of select="js:setLevel(1,position()=last())"/>
  
   <tr class="fl" >
	<xsl:attribute name="target">fl:<xsl:value-of select="@name"/></xsl:attribute>
    <xsl:attribute name="fref">fl:<xsl:value-of select="@name"/></xsl:attribute>
    <xsl:value-of select="js:getLeft()" disable-output-escaping="yes"/>
    <td>
	 <xsl:choose>
	  <xsl:when test="position()!=last()">
	   <img src="images/T.png"/>
	  </xsl:when>
	  <xsl:otherwise>
	   <img src="images/L.png"/>
	  </xsl:otherwise>
	 </xsl:choose>
	</td>
    <td>
	 <img class="fPM">
	  <xsl:attribute name="src">
	   <xsl:choose>
	    <xsl:when test="@folder=1">images/foldericon.gif</xsl:when>
	    <xsl:otherwise>images/nodeicon.gif</xsl:otherwise>
	   </xsl:choose>
	  </xsl:attribute>
	 </img>
	</td>
	<td>&#160;</td>
    <td nowrap="true">
	 <span class="flk"><!--xsl:value-of select="@name"/-->&#160;<xsl:value-of select="@descr"/></span>
	</td>
	<td width="99%">&#160;</td>
   </tr>
   
   <xsl:value-of select="js:setLevel(-1)"/>
  </xsl:template>
  

</xsl:stylesheet>
