<?xml version="1.0" ?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/"> 
   <HTML>
    <HEAD>
     <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
     <META HTTP-EQUIV="Expires" CONTENT="-1"/>
    </HEAD>
    <BODY>
	 <SPAN id="CONTENT">
       <xsl:apply-templates/>
	 </SPAN>
    </BODY>
	<!--script> top.TreeExp.End() </script-->
   </HTML>
  </xsl:template>

  <xsl:template match="JobReportNamesList">
   <table 
     onclick="TabClicked()"  
     oncontextmenu="TabRightClicked();return false;" 
	 ftype="ReportNamesList" width="100%" cellSpacing="2">
    <thead>
     <tr class="dbhdr">
      <td>JobReportName</td>
      <td>JobReportDescr</td>
      <td>Report<br/>Format</td>
      <td>Elab<br/>Format</td>
      <td>Chars/<br/>Line</td>
      <td>Lines/<br/>Page</td>
      <td>Page<br/>Orientation</td>
      <td>ParseFileName</td>
     </tr>
    </thead>
    <tbody>
     <xsl:apply-templates select="JobReportName"/>
    </tbody>
   </table>
  </xsl:template>

  <xsl:template name="ReportFormat">
   <xsl:choose>
    <xsl:when test="@ReportFormat = 0">Utility</xsl:when>
    <xsl:when test="@ReportFormat = 1">Ascii</xsl:when>
    <xsl:when test="@ReportFormat = 2">Ebcdic</xsl:when>
    <xsl:when test="@ReportFormat = 3">Afp</xsl:when>
   </xsl:choose>
  </xsl:template>

  <xsl:template name="ElabFormat">
   <xsl:choose>
    <xsl:when test="@ElabFormat = 1">Pdf</xsl:when>
    <xsl:when test="@ElabFormat = 2">Data</xsl:when>
   </xsl:choose>
  </xsl:template>

  <xsl:template name="PageOrient">
   <xsl:choose>
    <xsl:when test="@PageOrient = 'P'">Portrait</xsl:when>
    <xsl:when test="@PageOrient = 'L'">Landscape</xsl:when>
   </xsl:choose>
  </xsl:template>
 
  <xsl:template match="JobReportName">
   <tr class="dbtr">
    <td><xsl:value-of select="@JobReportName"/></td>
    <td><xsl:value-of select="@JobReportDescr"/></td>
    <td><xsl:call-template name="ReportFormat"/></td>
    <td><xsl:call-template name="ElabFormat"/></td>
    <td><xsl:value-of select="@CharsPerLine"/></td>
    <td><xsl:value-of select="@LinesPerPage"/></td>
    <td><xsl:call-template name="PageOrient"/></td>
    <td><xsl:value-of select="@ParseFileName"/></td>
   </tr>
  </xsl:template>

</xsl:stylesheet>
