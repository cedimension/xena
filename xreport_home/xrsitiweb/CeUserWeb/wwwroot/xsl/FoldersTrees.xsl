<?xml version="1.0"?>

<xsl:stylesheet 
	version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:js="urn:JScript"
>
<xsl:output media-type="text/xml" encoding="UTF-8" indent="yes"/>

 <msxsl:script language="JScript" implements-prefix="js"><![CDATA[
   function xpath_expr(root_expr) {
     var l = root_expr.split("/"); 
	 if ( l[0] == "" ) {
	   l.shift()
	 }
	 for (var j=0; j<l.length; j++) {
	   l[j] = "/node[\@name='" + l[j] + "']"
	 }
	 return "/root" + l.join("");
   }
   
   function select_tree(nodeList) {
     var tree_ele = nodeList.nextNode(); 
	 var root_attr = tree_ele.getAttributeNode("root");
     return tree_ele.selectNodes(xpath_expr(root_attr.value));
   }  
 ]]></msxsl:script>
 
 
 <xsl:template match="/">
  <UserTrees>
   <xsl:attribute name="for"><xsl:value-of select="/root/user/@name"/></xsl:attribute>
   <xsl:apply-templates/>
  </UserTrees>
 </xsl:template>


 <xsl:template match="/root/usertree">
  <xsl:for-each select="js:select_tree(.)">
   <xsl:call-template name="ExpandTree"/>
  </xsl:for-each>
 </xsl:template>


 <xsl:template name="ExpandTree">
   <xsl:call-template name="ExpandNode"/>
 </xsl:template>


 <xsl:template name="ExpandNode">
  <TreeNode>
   <xsl:attribute name="name"> <xsl:value-of select="@name"/> </xsl:attribute>
   <xsl:attribute name="descr"> <xsl:value-of select="@descr"/> </xsl:attribute>
   <xsl:attribute name="folder">
	<xsl:choose>
	 <xsl:when test="./@folder">
      <xsl:value-of select="@folder"/>
	 </xsl:when>
	 <xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
   </xsl:attribute>
   
   <xsl:for-each select="./node|./link">
    <xsl:choose>
	
	 <xsl:when test="local-name() = 'node'">
      <xsl:call-template name="ExpandNode"/>
     </xsl:when>
	 
	 <xsl:when test="local-name()='link'">
      <xsl:for-each select="js:select_tree(.)">
       <xsl:call-template name="ExpandTree"/>
      </xsl:for-each>
	 </xsl:when>
	 
    </xsl:choose>
   </xsl:for-each>
  </TreeNode>
 </xsl:template>
  
</xsl:stylesheet>
