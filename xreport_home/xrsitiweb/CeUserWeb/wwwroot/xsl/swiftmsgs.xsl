<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:msxsl="urn:schemas-microsoft-com:xslt"
 xmlns:js="urn:JScript"
>
<xsl:output media-type="text/html" encoding="UTF-8" indent="yes"/>
  
  <msxsl:script language="JScript" implements-prefix="js"><![CDATA[
   var atrow = 0

   function getClass() {
     atrow += 1; 
	 
     return (Math.floor(atrow/2)*2 == atrow) ? 'even' : 'odd';
   }
   
   function datepart(dateTime) {
     return dateTime.substring(0,10)
   }
  ]]></msxsl:script>
 
  <xsl:template match="/">
   <html>
    <head>
     <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
     <META HTTP-EQUIV="Expires" CONTENT="-1"/>
	 <style>
	  
	 </style>
    </head>
    <body>
	 <span id="CONTENT">
      <xsl:apply-templates select="swiftmsgList"/>
	 </span>
    </body>
	<script> top.TreeExp.End() </script>
   </html>
  </xsl:template>
  
  <xsl:template match="swiftmsgList[@detail=0]">
   <br/>
   <table class="indexList" onclick="TabClicked()">
    <thead>
     <tr>
      <th>sel</th>
      <th class="fsort" type="String">MITT</th>
      <th class="fsort" type="Date">DEST</th>
      <th class="fsort" type="String">DIVISA</th>
      <th class="fsort" type="Date">IMPORTO</th>
      <th class="fsort" type="Number">I/O</th>
      <th class="fsort" type="String">ORDINANTE</th>
      <th class="fsort" type="String">BENEFICIARIO</th>
      <th class="fsort" type="String">DATAARRIVO</th>
      <th class="fsort" type="String">RR</th>
      <th class="fsort" type="String">TRN</th>
      <th class="fsort" type="String">UMR</th>
      <th class="fsort" type="String">KEYSW</th>
     </tr>
    </thead>
	<tbody>
	 <xsl:for-each select="swiftmsg">
      <xsl:call-template name="swiftmsgList"/>
	 </xsl:for-each>
	</tbody>
	<tfoot>
	 <td colspan="13"></td>
	</tfoot>
   </table>
  </xsl:template>
  
  <xsl:template name="swiftmsgList">
    <tr>
	  <xsl:attribute name="class"><xsl:value-of select="js:getClass()"/></xsl:attribute>
     <td class="sel">
	  <xsl:attribute name="title"><xsl:value-of select="@JobReportId"/>/<xsl:value-of select="@FromPage"/>/<xsl:value-of select="@ForPages"/></xsl:attribute>
	  &#160;sel&#160;</td>
	 <td nowrap="1"><xsl:value-of select="@MITT"/></td>
	 <td nowrap="1"><xsl:value-of select="@DEST"/></td>
	 <td nowrap="1"><xsl:value-of select="@DIVISA"/></td>
	 <td nowrap="1" align="right"><xsl:value-of select="format-number(@IMPORTO div 100,'###,###,###,###,###.00')"/></td>
	 <td nowrap="1" align="right"><xsl:value-of select="@DIREZIONE"/></td>
	 <td nowrap="1"><xsl:value-of select="@ORDINANTE"/></td>
	 <td nowrap="1"><xsl:value-of select="@BENEFICIARIO"/></td>
	 <td nowrap="1"><xsl:value-of select="js:datepart(string(@DATAARRIVO))"/></td>
	 <td nowrap="1"><xsl:value-of select="@RR"/></td>
	 <td nowrap="1"><xsl:value-of select="@TRN"/></td>
	 <td nowrap="1"><xsl:value-of select="@UMR"/></td>
	 <td nowrap="1"><xsl:value-of select="@KEYSW"/></td>
	</tr>
  </xsl:template>
 
  <xsl:template match="swiftmsgList[@detail=1]">
   <span class="detailList">
	<xsl:for-each select="swiftmsg">
     <xsl:call-template name="swiftmsgDetail"/>
	</xsl:for-each>
   </span>
  </xsl:template>

  <xsl:template name="swiftmsgDetail">
   <br/>
   <table class="detailHdr">
	<tbody>
     <tr><td>MITT</td><td><xsl:value-of select="@MITT"/></td></tr>
     <tr><td>DEST</td><td><xsl:value-of select="@DEST"/></td></tr>
     <tr><td>DIVISA</td><td><xsl:value-of select="@DIVISA"/></td></tr>
     <tr><td>IMPORTO</td><td align="right"><xsl:value-of select="format-number(@IMPORTO div 100,'###,###,###,###,###.00')"/></td></tr>
     <tr><td>I/O</td><td><xsl:value-of select="@DIREZIONE"/></td></tr>
     <tr><td>ORDINANTE</td><td><xsl:value-of select="@ORDINANTE"/></td></tr>
     <tr><td>BENEFICIARIO</td><td><xsl:value-of select="@BENEFICIARIO"/></td></tr>
     <tr><td>DATAARRIVO</td><td><xsl:value-of select="js:datepart(string(@DATAARRIVO))"/></td></tr>
     <tr><td>RR</td><td><xsl:value-of select="@RR"/></td></tr>
     <tr><td>TRN</td><td><xsl:value-of select="@TRN"/></td></tr>
     <tr><td>UMR</td><td><xsl:value-of select="@UMR"/></td></tr>
     <tr><td>KEYSW</td><td><xsl:value-of select="@KEYSW"/></td></tr>
	</tbody>
   </table>
   <pre class="detailBody"><xsl:value-of select="."/></pre>
   <hr class="detailEnd" width="100%" size="1"/>
  </xsl:template>

</xsl:stylesheet>
