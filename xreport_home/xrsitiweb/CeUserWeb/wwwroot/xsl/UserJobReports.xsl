<?xml version="1.0" ?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


  <xsl:template match="/NOMATCH">
   <HTML>
    <link type="text/css" rel="stylesheet" href="css/cereport.css"/>
    <script type="text/javascript" src="js/tablesort.js"></script>
    <body>
	 <span id="CONTENT">
       <xsl:apply-templates/>
	 </span>
    </body>
	<script> top.TreeExp.End() </script>
   </HTML>
  </xsl:template>
 
  <xsl:template match="JobReportsList">
   <table
     onclick="TabClicked()" oncontextmenu="TabRightClicked();" 
	 ftype="ReportNamesList" width="100%" cellSpacing="2">
    <thead>
     <tr class="urh">
      <th class="wRF">!!</th>
      <th class="fsort" type="String">Name</th>
      <th class="fsort" type="Date">DateTimeXfer.</th>
      <th class="fsort" type="Number" align="right">NP</th>
      <th class="fsort" type="String">Description</th>
      <th class="fsort" type="Date">DateRef.</th>
      <th class="fsort" type="Date">DateElab.</th>
      <th class="fsort" type="String">JobName</th>
      <th class="fsort" type="String">JobNumbr</th>
     </tr>
	</thead>
	<tbody>
     <xsl:apply-templates/>
	</tbody>
   </table>
  </xsl:template>

  <xsl:template match="JobReport[@TotReports = 1 and @FileRangesVar = '' and @isGRANTED = 1]">
   <tr> 
    <xsl:choose>
     <xsl:when test="@isGRANTED = 1">
      <td class="sel">&#160;-&#160;</td>
      <td class="tpdf">
	   <xsl:attribute name="jrid"><xsl:value-of select="@JobReportId"/></xsl:attribute>
	   <xsl:attribute name="rid"><xsl:value-of select="@ReportId"/></xsl:attribute>
	   <xsl:value-of select="@ReportName"/>
	  </td>
     </xsl:when>
	 <xsl:otherwise>
      <td>&#160;-&#160;</td><td><xsl:value-of select="@ReportName"/></td>
	 </xsl:otherwise>
	</xsl:choose>
	<td nowrap="true"><xsl:value-of select="@XferStartTime"/></td>
    <td align="right"><xsl:value-of select="@TotPages"/></td> 
    <td nowrap="true">
	  <xsl:value-of select="@ReportDescr"/>
	  <xsl:if test="@UserRef != ''">
	    &#160;<span class="UserRef"><xsl:value-of select="@UserRef"/></span>
	  </xsl:if>
	</td>
	<td nowrap="true"><xsl:value-of select="@UserTimeRef"/></td>
	<td nowrap="true"><xsl:value-of select="@UserTimeElab"/></td>
    <td><xsl:value-of select="@JobName"/></td>
    <td>
	 <xsl:attribute name="title">
	  <xsl:value-of select="@JobNumber"/>/<xsl:value-of select="@JobReportId"/>/<xsl:value-of select="@ReportId"/>
	 </xsl:attribute>
	 <xsl:value-of select="@JobNumber"/>
	</td>
   </tr>
  </xsl:template>

  <xsl:template match="JobReport[@TotReports = 1 and @FileRangesVar != '' and @isGRANTED = 1]">
   <tr> 
    <xsl:attribute name="fref"><xsl:value-of select="@ReportName"/>/<xsl:value-of select="@JobReportId"/>/FILERANGES</xsl:attribute>
    <xsl:choose>
     <xsl:when test="@isGRANTED = 1">
      <td><img class="fPM" src="images/plus.gif"></img></td>
      <td><xsl:value-of select="@ReportName"/></td>
     </xsl:when>
	 <xsl:otherwise>
      <td>&#160;-&#160;</td><td><xsl:value-of select="@ReportName"/></td>
	 </xsl:otherwise>
	</xsl:choose>
	<td nowrap="true"><xsl:value-of select="@XferStartTime"/></td>
    <td align="right"><xsl:value-of select="@TotPages"/></td> 
    <td nowrap="true">
	  <xsl:value-of select="@ReportDescr"/>
	  <xsl:if test="@UserRef != ''">
	    &#160;<span class="UserRef"><xsl:value-of select="@UserRef"/></span>
	  </xsl:if>
	</td>
	<td nowrap="true"><xsl:value-of select="@UserTimeRef"/></td>
	<td nowrap="true"><xsl:value-of select="@UserTimeElab"/></td>
    <td><xsl:value-of select="@JobName"/></td>
    <td>
	 <xsl:attribute name="title">
	  <xsl:value-of select="@JobNumber"/>/<xsl:value-of select="@JobReportId"/>/<xsl:value-of select="@ReportId"/>
	 </xsl:attribute>
	 <xsl:value-of select="@JobNumber"/>
	</td>
   </tr>

    
   <tr style="display:none">
    <td colspan="8">
	<table class="FolderList" style="display:none" width="95%" cellSpacing="2">
	 <xsl:apply-templates select="ReportsList"/>
	</table>
    </td>
   </tr>

  </xsl:template>

  <xsl:template match="JobReport[@TotReports &gt; 1 and @isGRANTED = 1]">
   <tr> 
    <xsl:choose>
     <xsl:when test="@TotPages &gt; 125">
      <xsl:attribute name="fref"><xsl:value-of select="@ReportName"/>/<xsl:value-of select="@JobReportId"/></xsl:attribute>
       <td>
	    <img class="fPM" src="images/plus.gif"></img></td>
     </xsl:when>
     <xsl:otherwise>
      <td class="sel">&#160;-&#160;</td>
     </xsl:otherwise>
    </xsl:choose>
    <td class="tpdf">
     <xsl:attribute name="jrn"><xsl:value-of select="@ReportName"/></xsl:attribute>
	 <xsl:attribute name="jrid"><xsl:value-of select="@JobReportId"/></xsl:attribute>
	 <xsl:value-of select="@ReportName"/></td>
    <td nowrap="true"><xsl:value-of select="@XferStartTime"/></td>
    <td align="right">
	 <xsl:value-of select="@TotPages"/></td> 
	<td nowrap="true">
	  <xsl:value-of select="@ReportDescr"/>
	  <xsl:if test="@UserRef != ''">
	    &#160;<span class="UserRef"><xsl:value-of select="@UserRef"/></span>
	  </xsl:if>
	</td>
	<td nowrap="true"><xsl:value-of select="@UserTimeRef"/></td>
	<td nowrap="true"><xsl:value-of select="@UserTimeElab"/></td>
    <td><xsl:value-of select="@JobName"/></td>
	<td nowrap="true">
	 <xsl:attribute name="title">/<xsl:value-of select="@JobReportId"/></xsl:attribute>
	 <xsl:value-of select="@JobNumber"/></td>
	<td nowrap="true"><xsl:value-of select="@TotReports"/></td>
   </tr>

   <tr style="display:none">
    <td colspan="8">
	<table class="FolderList" style="display:none" width="95%" cellSpacing="2">
	 <xsl:apply-templates select="ReportsList"/>
	</table>
    </td>
   </tr>

  </xsl:template>

  <xsl:template match="ReportsList">
   <div class="window" style="height:250;overflow:auto;">
   <table ftype="JobReportsList">
    <thead>
     <tr class="urh">
      <th class="fRF">!!</th>
      <th class="fsort" type="String">FilterVar</th>
      <th class="fsort" type="String">FilterValue</th>
      <th class="fsort" type="Number">NP</th>
     </tr>
    </thead>
    <tbody>
     <xsl:apply-templates select="Report" />
	</tbody>
   </table>
   </div>
  </xsl:template>

  <xsl:template match="Report">
   <tr> 
    <td class="sel">&#160;-&#160;</td>
    <td class="tpdf">
	 <xsl:attribute name="jrid"><xsl:value-of select="@JobReportId"/></xsl:attribute>
	 <xsl:attribute name="rid"><xsl:value-of select="@ReportId"/></xsl:attribute>
	 <xsl:value-of select="@FilterVar"/>
	</td>
	<td nowrap="true"><xsl:value-of select="@FilterValue"/></td>
    <td align="right">
	 <xsl:attribute name="title">/<xsl:value-of select="@JobReportId"/>/<xsl:value-of select="@ReportId"/></xsl:attribute>
	 <xsl:value-of select="@TotPages"/></td> 
   </tr>
  </xsl:template>

</xsl:stylesheet>
