<?xml version="1.0" ?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/"> 
   <html>
    <title>Logon per utente XREPORT</title>
    <head>
     <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
     <META HTTP-EQUIV="Expires" CONTENT="-1"/>
     <link type="text/css" rel="stylesheet" href="css/cereport.css"/>
     <style>
       input {
         border-style=solid;border-width=1;border-color=gray;
       }
     </style>
    </head>
      <xsl:apply-templates/>
   </html>
  </xsl:template>


  <xsl:template match="LogonUser">
  
   <body style="margin:35px;">
    <form action="LogonUser.asp" METHOD="POST">
     <center>
     <h3>STAMPE WEB</h3><br/>
     <table>
      <tr>
       <td>UTENTE:</td>
       <td><input type="text" name="UserName" length="14" maxlength="16">
       <xsl:attribute name="value"><xsl:value-of select="@UserName"/></xsl:attribute></input></td></tr>
      <tr>
       <td>PASSWORD:</td>
       <td><input type="password" name="Password" length="14" maxlength="16"/></td></tr>
      <tr>
       <td colspan="2"><span>&#160;</span></td></tr>
      <tr>
       <td>NUOVA PASSWORD:</td>
       <td><input type="password" name="NewPassword" length="14" maxlength="16"/></td></tr>
      <tr>
       <td>CONFERMA:</td>
       <td><input type="password" name="ConfirmNewPassword" length="14" maxlength="16"/></td></tr>
      <tr>
       <td colspan="2"><span>&#160;</span></td></tr>
      <tr>
       <td colspan="2"><span>&#160;</span></td></tr>
     </table>
     <input type="submit" value="  LOGON  " length="14"/>
     <div/>
     <h4 style="background-color:#DEDFDE;color:#00009C"><xsl:value-of select="@MESSAGE"/></h4>
     </center>
    </form>
   </body>

  </xsl:template>

</xsl:stylesheet>
