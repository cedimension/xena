<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:msxsl="urn:schemas-microsoft-com:xslt"
>
<xsl:output media-type="text/html" encoding="UTF-8" indent="yes"/>

  <xsl:template match="/root">
   <span class="TREEHDR">Report Folders</span><br/>
   <span class="TREEHDR">for&#160;&#160;<font size="4"><xsl:value-of select="@for"/></font></span><br/>
   <span class="TREEHDR"></span><br/>
   <table onclick="TabClicked()" ftype="Top" fref="/FolderList" width="100%" border="0" cellspacing="0" cellpadding="0">
    <xsl:apply-templates/>
   </table>
  </xsl:template>
  

  <xsl:template match="/FoldersList">
   <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <xsl:apply-templates/>
   </table>
  </xsl:template>

  <xsl:template match="FoldersList">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template name="lastNode">
    <xsl:param name="fullpath"></xsl:param>
    <xsl:choose><xsl:when test="contains($fullpath, '\')">
      <xsl:call-template name="lastNode">
        <xsl:with-param name="fullpath" select="substring-after($fullpath, '\')"></xsl:with-param>
      </xsl:call-template></xsl:when>
      <xsl:otherwise><xsl:value-of select="$fullpath" /></xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="Folder[@HasChilds > 0]">
   <tr class="fl" >
    <xsl:if test="@HasReports > 0">
	 <xsl:attribute name="target">fl:<xsl:value-of select="@FolderName"/></xsl:attribute>
	 <xsl:attribute name="HasObjects">-<xsl:value-of select="@HasReports"/></xsl:attribute>
    </xsl:if>
    <xsl:attribute name="fref">fl:<xsl:value-of select="@FolderName"/></xsl:attribute>
	<xsl:attribute name="HasChilds">-<xsl:value-of select="@HasChilds"/></xsl:attribute>
    <td>
	 <img src="images/plus.gif" class="fPM"/>
	</td>
    <td nowrap="true">
      <!-- xsl:attribute name="IsMine"><xsl:value-of select="@IsMine"/></xsl:attribute -->
      <span class="flk" style="color:blue">&#160;<xsl:call-template name="lastNode">
        <xsl:with-param name="fullpath" select="@FolderName" />
        </xsl:call-template>&#160;<xsl:value-of select="@FolderDescr"/>
      </span>
    </td>
    <td width="99%">&#160;</td>
   </tr>
   
   <tr style="display:none">
    <td></td>
	<td colspan="2">
	 <table border="0" cellspacing="0" cellpadding="0">
	 </table>
	</td>
   </tr>
  </xsl:template>


  <xsl:template match="Folder[(@HasReports > 0 or count(./Folder) = 0) and @HasChilds = 0]">
   <tr class="fl" >
	<xsl:if test="@HasReports > 0">
	 <xsl:attribute name="target">fl:<xsl:value-of select="@FolderName"/></xsl:attribute>
	 <xsl:attribute name="HasObjects">-<xsl:value-of select="@HasReports"/></xsl:attribute>
    </xsl:if>
    <xsl:attribute name="fref">fl:<xsl:value-of select="@FolderName"/></xsl:attribute>
	<xsl:attribute name="HasChilds">-<xsl:value-of select="@HasChilds"/></xsl:attribute>
    <td>
	 <img src="images/red.gif"/>
	</td>
    <td nowrap="true">
      <!-- xsl:attribute name="IsMine"><xsl:value-of select="@IsMine"/></xsl:attribute -->
	 <span class="flk">&#160;<xsl:call-template name="lastNode">
        <xsl:with-param name="fullpath" select="@FolderName"></xsl:with-param>
        </xsl:call-template>&#160;<xsl:value-of select="@FolderDescr"/>
     </span>
	</td>
	<td width="99%">&#160;</td>
   </tr>
  </xsl:template>
  

</xsl:stylesheet>
