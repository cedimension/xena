<?xml version="1.0" ?> 
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/NOMATCH"> 
   <HTML>
    <HEAD>
     <META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
     <META HTTP-EQUIV="Expires" CONTENT="-1"/>
    </HEAD>
    <BODY>
	 <SPAN id="CONTENT">
       <xsl:apply-templates/>
	 </SPAN>
    </BODY>
	<script> top.TreeExp.End() </script>
   </HTML>
  </xsl:template>


  <xsl:template match="ReportNamesList">
   <table 
     onclick="TabClicked()"  oncontextmenu="TabRightClicked()" 
	 ftype="ReportNamesList" width="100%" cellSpacing="2">
    <tr class="urh" align="left">
     <th class="wRF" width="1%">!!</th>
     <td width="10%">Name</td>
     <td width="90%">Description</td>
    </tr>
    <xsl:apply-templates select="ReportName" />
   </table>
  </xsl:template>
	
	
  <xsl:template match="ReportName">
   <tr>
   <xsl:choose>
    <xsl:when test="@isGRANTED = 1">
	  <xsl:attribute name="fref"><xsl:value-of select="@ReportName"/></xsl:attribute>
      <td width="1%">
	   <img class="fPM" src="images/plus.gif"></img></td>
      <td width="10%" class="rn_yes"><xsl:value-of select="@ReportName"/></td>
      <td width="90%" nowrap="true"><xsl:value-of select="@ReportDescr"/>
       <xsl:if test="@HasIndex != ''">
	    &#160;<img  class="idxref" nochange="1" src="images/idxref.gif" style="position:relative;top:+3">
        <xsl:attribute name="title"><xsl:value-of select="@HasIndex"/></xsl:attribute>
        </img>
       </xsl:if>
      </td>
     </xsl:when>
    <xsl:otherwise>
      <td width="1%">&#160;-&#160;</td>
      <td width="10%" class="rn_no"><xsl:value-of select="@ReportName"/></td>
      <td width="90%" nowrap="true"><xsl:value-of select="@ReportDescr"/></td>
	 </xsl:otherwise>
	</xsl:choose>
   </tr>
   
   <tr style="display:none">
    <td colspan="3">
	<table class="FolderList" style="display:none" width="95%" cellSpacing="2">
	 <xsl:apply-templates select="JobReportsList" />
	</table>
    </td>
   </tr>
  </xsl:template>

  <xsl:template match="JobReportsList">
   <div class="window" style="height:250;overflow:auto;">
   <table ftype="JobReportsList">
    <thead>
     <tr class="urh">
      <th class="fRF">!!</th>
      <th class="fsort" type="String">JobName</th>
      <th class="fsort" type="Date">DateTimeXfer.</th>
      <th class="fsort" type="String">Ref.</th>
      <th class="fsort" type="Date">DateRef.</th>
      <th class="fsort" type="Date">DateElab.</th>
      <th class="fsort" type="Number">NP</th>
      <th class="fsort" type="String">JobNumber</th>
     </tr>
    </thead>
    <tbody>
     <xsl:apply-templates select="JobReport" />
	</tbody>
   </table>
   </div>
  </xsl:template>

  <xsl:template match="JobReport[@TotReports = 1 and @FileRangesVar = '']">
   <tr> 
	<xsl:choose>
     <xsl:when test="@isGRANTED = 1">
      <td class="sel">&#160;-&#160;</td>
      <td class="tpdf">
	   <xsl:attribute name="jrid"><xsl:value-of select="@JobReportId"/></xsl:attribute>
	   <xsl:attribute name="rid"><xsl:value-of select="@ReportId"/></xsl:attribute>
       <xsl:if test="@ExistTypeMap != 0">
	     <xsl:attribute name="et"><xsl:value-of select="@ExistTypeMap"/></xsl:attribute>
       </xsl:if>
	   <xsl:value-of select="@JobName"/>
	  </td>
     </xsl:when>
	 <xsl:otherwise>
      <td>&#160;-&#160;</td><td><xsl:value-of select="@JobName"/></td>
	 </xsl:otherwise>
	</xsl:choose>
    <td nowrap="true"><xsl:value-of select="@XferStartTime"/></td>
	<td nowrap="true"><span class="UserRef"><xsl:value-of select="@UserRef"/></span></td>
	<td nowrap="true"><xsl:value-of select="@UserTimeRef"/></td>
	<td nowrap="true"><xsl:value-of select="@UserTimeElab"/></td>
    <td align="right">
	 <xsl:value-of select="@TotPages"/></td> 
	<td>
	 <xsl:attribute name="title">/<xsl:value-of select="@JobReportId"/>/<xsl:value-of select="@ReportId"/></xsl:attribute>
	 <xsl:value-of select="@JobNumber"/></td>
   </tr>
  </xsl:template>

  <xsl:template match="JobReport[@TotReports = 1 and @FileRangesVar != '']">
   <tr> 
    <xsl:attribute name="fref"><xsl:value-of select="@JobReportId"/>/FILERANGES</xsl:attribute>
	<xsl:choose>
     <xsl:when test="@isGRANTED = 1">
      <td><img class="fPM" src="images/plus.gif"></img></td>
      <td>
       <xsl:if test="@ExistTypeMap != 0">
	     <xsl:attribute name="et"><xsl:value-of select="@ExistTypeMap"/></xsl:attribute>
       </xsl:if>
       <xsl:value-of select="@JobName"/>
      </td>
     </xsl:when>
	 <xsl:otherwise>
      <td>&#160;-&#160;</td><td><xsl:value-of select="@JobName"/></td>
	 </xsl:otherwise>
	</xsl:choose>
    <td nowrap="true"><xsl:value-of select="@XferStartTime"/></td>
	<td nowrap="true"><span class="UserRef"><xsl:value-of select="@UserRef"/></span></td>
	<td nowrap="true"><xsl:value-of select="@UserTimeRef"/></td>
	<td nowrap="true"><xsl:value-of select="@UserTimeElab"/></td>
    <td align="right">
	 <xsl:value-of select="@TotPages"/></td> 
	<td>
	 <xsl:attribute name="title">/<xsl:value-of select="@JobReportId"/>/<xsl:value-of select="@ReportId"/></xsl:attribute>
	 <xsl:value-of select="@JobNumber"/></td>
   </tr>
   
   <tr style="display:none">
    <td colspan="8">
	<table class="FolderList" style="display:none" width="95%" cellSpacing="2">
	 <xsl:apply-templates select="ReportsList"/>
	</table>
    </td>
   </tr>

  </xsl:template>

  <xsl:template match="JobReport[@TotReports &gt; 1]">
   <tr> 
    <xsl:choose>
     <xsl:when test="@TotPages &gt; 10">
      <xsl:attribute name="fref"><xsl:value-of select="@JobReportId"/></xsl:attribute>
       <td>
	    <img class="fPM" src="images/plus.gif"></img></td>
     </xsl:when>
     <xsl:otherwise>
      <td class="sel">&#160;-&#160;</td>
     </xsl:otherwise>
    </xsl:choose>
    <td class="tpdf">
     <xsl:attribute name="jrn"><xsl:value-of select="../@ReportName"/></xsl:attribute>
	 <xsl:attribute name="jrid"><xsl:value-of select="@JobReportId"/></xsl:attribute>
	 <xsl:value-of select="@JobName"/></td>
    <td nowrap="true"><xsl:value-of select="@XferStartTime"/></td>
	<td nowrap="true"><span class="UserRef"><xsl:value-of select="@UserRef"/></span></td>
	<td nowrap="true"><xsl:value-of select="@UserTimeRef"/></td>
	<td nowrap="true"><xsl:value-of select="@UserTimeElab"/></td>
    <td align="right">
	 <xsl:value-of select="@TotPages"/></td> 
	<td nowrap="true">
	 <xsl:attribute name="title">/<xsl:value-of select="@JobReportId"/></xsl:attribute>
	 <xsl:value-of select="@JobNumber"/></td>
   </tr>

   <tr style="display:none">
    <td colspan="8">
	<table class="FolderList" style="display:none" width="95%" cellSpacing="2">
	 <xsl:apply-templates select="ReportsList"/>
	</table>
    </td>
   </tr>

  </xsl:template>

  <xsl:template match="ReportsList">
   <div class="window" style="height:250;overflow:auto;">
   <table ftype="JobReportsList">
    <thead>
     <tr class="urh">
      <th class="fRF">!!</th>
      <th class="fsort" type="String">FilterVar</th>
      <th class="fsort" type="String">FilterValue</th>
      <th class="fsort" type="Number">NP</th>
     </tr>
    </thead>
    <tbody>
     <xsl:apply-templates select="Report" />
	</tbody>
   </table>
   </div>
  </xsl:template>

  <xsl:template match="Report">
   <tr> 
    <td class="sel">&#160;-&#160;</td>
    <td class="tpdf">
	 <xsl:attribute name="jrid"><xsl:value-of select="@JobReportId"/></xsl:attribute>
	 <xsl:attribute name="rid"><xsl:value-of select="@ReportId"/></xsl:attribute>
	 <xsl:value-of select="@FilterVar"/>
	</td>
	<td nowrap="true"><xsl:value-of select="@FilterValue"/></td>
    <td align="right">
	 <xsl:attribute name="title">/<xsl:value-of select="@JobReportId"/>/<xsl:value-of select="@ReportId"/></xsl:attribute>
	 <xsl:value-of select="@TotPages"/></td> 
   </tr>
  </xsl:template>

  <xsl:template match="FileRangesList">
   <div class="window" style="height:250;overflow:auto;">
   <table ftype="FileRangesList">
    <thead>
     <tr class="urh">
      <th class="fRF">!!</th>
      <th class="fsort" type="String">FileVar</th>
      <th class="fsort" type="String">FromValue</th>
      <th class="fsort" type="String">ToValue</th>
      <th class="fsort" type="Number">NP</th>
     </tr>
    </thead>
    <tbody>
     <xsl:apply-templates select="FileRange" />
	</tbody>
   </table>
   </div>
  </xsl:template>

  <xsl:template match="FileRange">
   <tr> 
    <td class="sel">&#160;-&#160;</td>
    <td class="tpdf">
	 <xsl:attribute name="jrid"><xsl:value-of select="@JobReportId"/></xsl:attribute>
	 <xsl:attribute name="fid"><xsl:value-of select="@FileId"/></xsl:attribute>
	 <xsl:value-of select="@FileRangesVar"/>
	</td>
	<td nowrap="true"><xsl:value-of select="@FromValue"/></td>
	<td nowrap="true"><xsl:value-of select="@ToValue"/></td>
    <td align="right">
	 <xsl:attribute name="title">/<xsl:value-of select="@JobReportId"/>/<xsl:value-of select="@FileId"/></xsl:attribute>
	 <xsl:value-of select="@TotPages"/></td> 
   </tr>
  </xsl:template>

</xsl:stylesheet>
