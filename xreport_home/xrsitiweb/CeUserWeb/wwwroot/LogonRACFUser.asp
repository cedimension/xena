<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

### vim: set syn=perl: ###

use strict; 

use lib("$main::Application->{XREPORT_HOME}/perllib");

use CGI qw(:standard);
use URI::Escape;
use Net::FTP;
use Data::Dumper;
use POSIX qw(strftime);
use XReport;
use XReport::DBUtil;

sub logIt {
  for (@_) {
    my $t = $_; $t =~ s/\n/<br\/>/sg; $main::Response->Write($t);
  }
}

sub TRIM {
  for (@_) {
    $_ =~ s/(?:^ +| +$)//g;
  }
}

my $reqmet   = $main::Request->ServerVariables('REQUEST_METHOD'    )->item();
my $qrystr   = $main::Request->ServerVariables('QUERY_STRING'      )->item();
my $thisurl  = $main::Request->ServerVariables("SCRIPT_NAME"          )->Item();
my $racfhost = $main::Application->{'cfg.racfhost'};
die "RACF Host not defined" unless $racfhost;

my $ffile = $main::Request->ServerVariables('APPL_PHYSICAL_PATH')->Item()."\\showFunctions.pl";
require "$ffile";

sub VerifyRACFPassword {
  my ($UserName, $Password, $newUserName) = (shift, shift, undef);
  debug2log("Checking $UserName over DB");

  my $dbr = dbExecute("SELECT * from tbl_Users WHERE UserName = '$UserName'");
  my $userondb = !$dbr->eof();
  $dbr->Close();
  $main::lastMessage = "XReport config for $UserName not found - Access denied ";
  $main::Session->{RACFMessage} = $main::lastMessage;
  return undef unless $userondb;
 
  my $ftp = Net::FTP->new($racfhost, Debug => 0);
  if ( !$ftp ) {
    $main::lastMessage = "unable to contact $racfhost - retry later";
  }
  else {
    my $ftprc = $ftp->login($UserName, $Password);
    $newUserName = ($ftprc ? (split(/\s/, $ftp->message()))[0] : undef); # paldovini: array index 1 with normal FTP; for RACF you must use index 0
    $main::lastMessage = ($newUserName ? "Login completed for $UserName - Racf User is $newUserName" 
		    : "Login Failed for $UserName - ".$ftp->message());
    $ftp->quit();
  }
  $main::Session->{RACFUserName} = $newUserName;
  $main::Session->{RACFMessage} = $main::lastMessage;
  debug2log("LogonRACFUser - User:$newUserName; RACFUser:".$main::Session->{RACFUserName});
  
  return $newUserName;
}


my $FormVars = { map { split /=/, $_ } map {uri_unescape($_)} split(/\&/, $qrystr) };
$main::lastMessage = $FormVars->{message};

my ($inputForm) = (
		   start_html("XREPORT Login")
		   . h3("Login to XREPORT - RACF host: $racfhost")
		   . start_multipart_form( -method => 'POST', -action => $thisurl.'?'.$qrystr) #, -onSubmit => "self.close()")  
		   . table(Tr( td(['USERID:', textfield( -name => "UserName", -size => 15, -maxlength => 15 )]) )
			   ,Tr( td(['PASSWORD:', password_field( -name => "Password", -size => 15, -maxlength => 15 )]) )
			   ,Tr( td({-colspan => 2}, submit('Action', 'Login')) )
			   ,Tr( td({-colspan => 2}, hidden("QUERYSTRING", $qrystr) ) )
			   ,Tr( td({-colspan => 2}, '$lastMessage$') ))
		   . endform()
		   . end_html()
		  );
  
my $newUserName = undef;

while (1) {
  my $requestVars = {};
  (my $outhtml = $inputForm) =~ s/\$lastMessage\$/$main::lastMessage/;

  return $main::Response->Write($outhtml) unless $reqmet =~ /^POST$/i;

  my $TotalBytes = $main::Request->TotalBytes();

  if ($TotalBytes) {
    my $rec = '';
    while ($TotalBytes > 0) {
      my $bytes2read = ($TotalBytes > 4096 ? 4096 : $TotalBytes);
    
      my $buff = $main::Request->BinaryRead($bytes2read); 
      last unless length($buff);
      $rec .= $buff;
      $TotalBytes -= length($buff);
    }
  
    $rec =~ s/\+/ /g;
  
    $requestVars = { map { split /"?(?:\x0d\x0a)+/ } split /-+\w+(?:--|\x0d\x0aContent-Disposition: form-data; name=")/, $rec };
  }
#  debug2log("Request Vars:" . Dumper($requestVars));

  last if ($requestVars->{"UserName"} and $newUserName = VerifyRACFPassword( @{$requestVars}{qw(UserName Password)} )); 

  $main::lastMessage = "User Missing - Inserire Utente/Password" unless $requestVars->{"UserName"};
  $reqmet = '';

}

$main::Session->{"UserName"} = $newUserName;

SetUserLocalSessionCookie();

return  $main::Response->Write
  (div({-ID => 'EXEC'} 
       , script("top.opener.focus();"
		. " top.opener.location.reload();"
		. " top.close();")
      ));


</SCRIPT>
