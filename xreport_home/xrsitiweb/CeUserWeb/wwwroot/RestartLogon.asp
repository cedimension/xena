<!-- vim: set syn=perl: -->

<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

use strict; 

use FileHandle; use URI::Escape; use Win32::OLE; use Digest::MD5;

our ($Server, $Application, $Session, $Request, $Response, $XREPORT_HOME);

my ($requestVars, $dbc);

sub CreateObject {
  return $Server->CreateObject($_[0]);
}

sub CONNECT {
  $dbc = Win32::OLE->new("ADODB.Connection");

  $dbc->{"ConnectionTimeout"} = 10;
  $dbc->{"CommandTimeout"} = 30;
	
  $dbc->Open($Application->{"cfg.db.XREPORT"});
  return $dbc if $dbc->State() != 0;
 
  my $dberr; $dberr = ($dbc->Errors(0)) ? $dbc->Errors(0)->Description() : "UNKNOWN";

  die( 
   "ERROR CONNECTING TO XREPORT DATABASE dberr=$dberr \n" .  
   " at ". $ENV{'COMPUTERNAME'} . " \n" .
   "UserName=". Win32->LoginName() . "\n" .
   "connstr=". $Application->{"cfg.db.XREPORT"} . "\n"
  );

}

sub DISCONNECT {
  $dbc->Close();
}

sub dbExecute {
  my $sql = $_[0]; my $dbr = CreateObject("ADODB.Recordset");
    
  $dbr->Open($sql, $dbc, 3, 3);

  if ( $dbc->Errors()->Count() > 0 ) {
    die $dbc->Errors(0)->Description()."/".$sql;
  }
  
  return $dbr;
}

sub RESTART_LOGON {
  $Response->Clear();
  
  $Response->{'Buffer'} = 1;
  $Response->{'Status'} = "401 Unauthorized";
  $Response->AddHeader("WWW-Authenticate","NTLM");
  
  $Response->End(); exit();
}


sub VerifyUser {
#  Win32::OLE->new('LoginAdmin.ImpersonateUser')->Logoff(); 
  CONNECT();

  my ($domain, $user) = (undef, undef);
  if    ($Application->{'cfg.authenticate'} eq 'NTLM') {
    ($domain, $user) =  split( /\\/, $Request->ServerVariables("AUTH_USER")->Item() ) ;
  }
  elsif ($Application->{'cfg.authenticate'} eq 'ISAPI') {
    $user = $Request->ServerVariables("HTTP_AUTHORIZATION")->Item();
  }
  else {
    $Response->Write("<DIV ID=\"EXEC\">" .
		     "\<\script  type=\"text/javascript\" src=\"js/cereport.js\">\<\/script>" .
		     "\<\script>top.LogonUser()\<\/script>" .
		     "</DIV>");
    return;
  
  }

  if (!$user) {
    $Response->Write(
		     "<DIV ID=\"EXEC\">" .
		     "\<\script  type=\"text/javascript\" src=\"js/cereport.js\">\<\/script>" .
		     "\<\script>ERROR_MSG('Identificazione User Fallita, contattare HELP DESK', 0)\<\/script>" .
		     "</DIV>"
		    ); 
    return;
    
  }

  @{$requestVars}{qw(NetName UserName)} = ($domain, $user); 

  my $UserAlias = $requestVars->{'UserName'};

  my $dbr = dbExecute("SELECT * from tbl_UserAliases WHERE UserAlias='$UserAlias'");
  
  if ($dbr->eof()) {
    $Response->Write(
		     "<DIV ID=\"EXEC\">" .
		     "\<\script  type=\"text/javascript\" src=\"js/cereport.js\">\<\/script>" .
		     "\<\script>INVALID_USERNAME('$UserAlias non definito in CeReport', 1)\<\/script>" .
		     "</DIV>"
		    ); 
  }

  my $UserName = $dbr->Fields()->Item("UserName")->Value();
  
  $Session->{'UserName'} = $requestVars->{'UserName'} = $UserName;

#  $Response->Write(
#		   "<DIV ID=\"EXEC\">" .
#		     "\<\script  type=\"text/javascript\" src=\"js/cereport.js\">\<\/script>" .
#		   "\<\script>restart_tree_exp();\</\script>" .
#		   "</DIV>"
#		  );
}

VerifyUser();
    
</SCRIPT>
