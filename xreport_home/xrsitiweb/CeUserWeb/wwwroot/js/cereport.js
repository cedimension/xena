
var MainFrame, LeftFrame, LeftBody, TextFrame, TextBody, IOArea, IOAreaFrame;

var HideShow, reSize, LeftFrameWidth, LeftFrameHeight;

var current = new Object();

var TreeExp;

var IE_Name, IE_Version, ioprogr = 0;

var LoadingMessage = "<table><tr><td>&nbsp;</td><td class=MSG>LOADING ... ATTENDERE</td><tr></table>";

function ERROR_MSG(msg, restart) {
  alert(msg);

  if (restart) document.URL = document.URL;
}

function INVALID_USERNAME() {
  var choice = self.showModalDialog("RestartLogon.asp", self);

  if ( choice == 'RESTART_LOGON' ) {
    self.setTimeout('RestartLogon()', 571);
  }
  else {
  }
}

function RestartLogon() {
  var t = '<form id="req" action="RestartLogon.asp" method="POST"></form>';

  IOArea.document.body.innerHTML = t ;
 
  IOArea.req.submit();
}

function LogonRACFUser(msg) {
  if ( msg == null )
       msg = 'Inserire Utente e Password';
  self.open("LogonRACFUser.asp?message=" + msg, "LogonUser", "width=500,height=400,top=70,left=70");
}

function LogonUser() {
  self.open("LogonUser.asp", "LogonUser", "width=500,height=400,top=70,left=70");
}

function restart_tree_exp() {
  ioprogr--;self.setTimeout('_Expand( TreeExp.nodetoexp, true )', 571);
}

function restart_get_pdf() {
  alert("restart_get_pdf");
}

function LogoffUser() {
  alert(self.document.URL); 
}

function VerUserAgent() {
  var userAgent = navigator.userAgent;
  var  tk = userAgent.split(';');
  while(tk[1].charAt(0) == ' ') {
    tk[1] = tk[1].substr(1);
  }
  tk = tk[1].split(' ');
  self.IE_Name = tk[0];
  self.IE_Version = tk[1];
}

VerUserAgent();

function InvertSelection( e ) {
  if ( IE_Version < 5 ) return
  if ( e ) {
    if ( !e.original_color ) {
	  e.original_color = e.currentStyle.color;
      e.style.color = 'white';
      e.style.backgroundColor = '#0000AD';
	}
    else {
      e.style.color = e.original_color;
      e.style.backgroundColor = 'white';
	  e.original_color = null;
	}
    var clist = e.children; var ll = clist.length;
    for (var i=0; i<ll; i+=1) {
      InvertSelection(clist[0]);
    }
  }
}

function SetLeftNode(leftnode, lastImg) {
  if ( leftnode == current.leftnode ) return

  if ( current.leftnode ) {
    //current.lastImg.src = current.lastImg.src.replace(/open([^\/]+)$/, "$1")
    InvertSelection(current.leftnode) ;
  }
  //lastImg.src = lastImg.src.replace(/([^\/]+)$/, "open$1")
  InvertSelection(leftnode);
  
  current.leftnode = leftnode;
  current.lastImg = lastImg;
  
  FText.SetLeftNode( leftnode );
}


function _Expand(e, force) {
  top.TreeExp.Expand(e, force, false);
}

function TreeExpRequest() { 
  this.Expand = TreeExpRequestExpand;
  this.Start = TreeExpRequestStart;
  this.End = TreeExpRequestEnd;
}
TreeExp = new TreeExpRequest;


function TreeExpRequestExpand( e, force, istextnode ) {
  if(this.active) {
	return;
  }

  var o_orig = e, fTAB, fTr0, fTr1, img, currImg, lastImg, eLeft = false, eText = false;
  var activeBody = null, activeMainWindow = null, targetWindow = null;

  var nodetoexp = e;

  while ( e.tagName != 'TR' ) {
    e = e.parentElement;
  }
  fTr0 = e;  

  if ( fTr0.className == 'fl' ) {
    nodetoexp = fTr0.cells[fTr0.cells.length-2].children[0];
  }
  var target = nodetoexp.target = fTr0.target;

  fTAB = fTr0.parentElement.parentElement;
  fTr1 = fTAB.rows[fTr0.rowIndex+1];

  img = fTr0.all.tags('IMG'); // img list
 
  currImg = img[(img.length)>1 ? img.length-2 : 0]; // img plus-minus
  lastImg = img[(img.length)>1 ? img.length-1 : 0]; // img folder open-close (if present)

  if ( (istextnode || o_orig != currImg) && target ) {
    activeBody = TextBody.all.item("MainTab").activeBody;
    activeMainWindow = activeBody.activeMainWindow;

	if (activeBody.expand != "1" && !force) {
	  SetLeftNode( nodetoexp, lastImg );
      return 
	}
    
	targetWindow = activeBody.all.item(target);
	if (targetWindow && targetWindow.id === 'CONTENT') { targetWindow = null; }
    if ( targetWindow && !activeBody.force && !force ) {
	  if ( activeMainWindow ) {
        activeMainWindow.style.display = "none";
	  }
      targetWindow.style.display = "";
	  activeBody.activeMainWindow = targetWindow;
	}
	else {
      activeMainWindow = activeBody.activeMainWindow;

	  if (!FText.CheckFields()) {
	    return
	  }
      eText = true;
	  
	  if ( !targetWindow ) {
	    LoadHTML = '<div id="' + target + '" class="TextMainWindow">' + LoadingMessage + '</div>';
        activeBody.all('CONTENT').insertAdjacentHTML('beforeEnd', LoadHTML);
	  }
	  else {
        targetWindow.innerHTML = LoadingMessage;
	  }
	  targetWindow = activeBody.all.item(target);
	  
	  if ( activeMainWindow ) {
	    activeMainWindow.style.display = 'none';
	  }
	  activeMainWindow = activeBody.all.item(target);

	  activeMainWindow.style.display = '';

	  activeBody.activeMainWindow = activeMainWindow;
	}

	if (!istextnode) SetLeftNode( nodetoexp, lastImg );
  }

  else if (o_orig == currImg && currImg.src.match(/plus|minus/i) || istextnode ) {
    if (fTr1.style.display == "none") {
	  currImg.src = currImg.src.replace(/plus/i, "minus");
	  if ( lastImg != currImg && !lastImg.nochange ) {
        lastImg.src = lastImg.src.replace(/([^\/]+)$/, "open$1");
	  }
      fTr1.style.display='';
      //-----
	  var ele = fTAB.parentElement;
	  while ( ele && ele.tagName && ele.tagName != 'BODY' ) {
	    if ( ele.className == 'window' ) {
	      var minHeight = (ele.scrollHeight > 350) 
            ? 350 
            : ele.scrollHeight;
	      if (ele.offsetHeight < minHeight) {
            ele.style.height = minHeight+10;
		  }
	    }
	    ele = ele.parentElement;
	  }
      //-----
    }
    else if (!force) {
	  currImg.src = currImg.src.replace(/minus/i, "plus");
	  if ( lastImg != currImg ) {
        lastImg.src = lastImg.src.replace(/open([^\/]+)$/, "$1");
	  }
      fTr1.style.display="none";
      //-----
	  var ele = fTAB.parentElement;
	  while ( ele && ele.tagName && ele.tagName != 'BODY' ) {
	    if ( ele.className == 'window' && ele.offsetHeight > ele.scrollHeight ) {
          ele.style.height = ele.scrollHeight+10;
	    }
	    ele = ele.parentElement;
	  }
      //-----
    }
  
    if ( fTr1.all.tags('TABLE')[0].rows.length == 0 || force ) {
      if (!istextnode && fTr0.HasChilds != 0) eLeft = true;
      else eText = true;

      fTr1.style.display = "";
      fTr1.all.tags('TABLE')[0].outerHTML = LoadingMessage; 
    }
  }

  this.nodetoexp = nodetoexp;
  this.activeBody = activeBody; 
  this.activeMainWindow = activeMainWindow;

  this.eLeft = eLeft; this.eText = eText;

  if ( this.eLeft|| this.eText ) this.Start(fTr0);
}

function TreeExpRequestStart(fTr0) { 
  //sometime ? error pointing to IOArea (to debug)
  if ( IOArea == null ) return 
  //-----------------------------------------------------------

  var fTAB = fTr0.parentElement.parentElement;
  var fTr1 = fTAB.rows[fTr0.rowIndex+1];

  this.fTAB=fTAB; this.fTr0=fTr0; this.fTr1=fTr1;

  var IOAreaBody = IOArea.document.body, t = '';
  var fref = fTr0.fref;

  while ( fTAB.ftype != "Top" ) {
    if ( fTAB.parentElement.className == 'window' ) { //Table contained in a DIV in a TD
	  fTr1 = fTAB.parentElement.parentElement.parentElement;
	  fTAB = fTr1.parentElement.parentElement;
	  fTr0 = fTAB.rows[fTr1.rowIndex-1];
	}
    else if ( fTAB.parentElement.tagName == "TD" ) { //Table contained in a TD
	  fTr1 = fTAB.parentElement.parentElement;
	  fTAB = fTr1.parentElement.parentElement;
	  fTr0 = fTAB.rows[fTr1.rowIndex-1];
	}
	else {
	  fTr0 = fTAB.fTr0;
      fTAB = fTr0.parentElement.parentElement;
      fTr1 = fTAB.rows[fTr0.rowIndex+1];
	}
	if ( !fTr0.fref ) continue;
	
    fref = fTr0.fref + '/' + fref;
    
  }
  fref = fTAB.fref + '/' + fref;
  //alert("fref=" + fref);

  t += '<form id="req" action="'+ExpandRequest+'" method="POST">';

  t += '<input type="hidden" name="ioprogr" value="' + ++ioprogr + '">';
  t += '<input type="hidden" name="fref" value="' + fref + '">';
  t += '<input type="hidden" name="eLeft" value="' + this.eLeft + '">';
  t += '<input type="hidden" name="eText" value="' + this.eText + '">';
  t +=  FText.GetPARMS() ;
  //note questo viene da user.js
  t +=  U_serParameters();
  t += '</form>';

  IOAreaBody.innerHTML = t; 
 
  IOArea.req.submit();

  this.active = true;
}

function TreeExpRequestEnd() {
  //during refresh win95/98 ? IE4 ? error pointing to IOArea
  if ( IOArea == null ) return 
  //-----------------------------------------------------------
    
  //test if we requested something (maybe back was clicked!)
  if ( this.nodetoexp == null ) {
    return
  }
  if ( this.active == false ) {
    return
  }
  //-----------------------------------------------------------

  var IOAreaBody = IOArea.document.body, lclLeft, LeftHTML, lclText, TextHTML, target = null;

  lclLeft = IOAreaBody.document.all('Left'); lclText = IOAreaBody.document.all('FText');
  if (lclLeft == null || lclText == null) {
    return
  }
  if (lclText == null) {
    return
  }
  LeftHTML = lclLeft.innerHTML; TextHTML = lclText.innerHTML;

  if ( this.eLeft && this.fTr0.HasChilds < 0 ) {
    if ( this.fTAB.className != "replace" ) {
      target = this.fTr1.all.tags('TABLE')[0];
      target.outerHTML = LeftHTML;
    }
    else {
      target = this.fTAB;
      target.outerHTML = LeftHTML;
    } 
    this.fTr0.HasChilds = Math.abs(this.fTr0.HasChilds);
  }
    
  if (this.eText) {
    if (this.nodetoexp.target) {
      var leftnode = this.nodetoexp;
      this.activeMainWindow.innerHTML = TextHTML;
	  target = this.activeMainWindow.all.tags('TABLE')[0];
	  target.fTr0 = this.fTr0; //link ritorno
	  target.leftnode = leftnode; //link ritorno x refresh
      this.fTr0.HasObjects = Math.abs(this.fTr0.HasObjects);
    }
    else {
      target = this.fTr1.all.tags('TABLE')[0];
      target.outerHTML = TextHTML;
      this.fTr0.all.tags('IMG')[0].src="images/minus.gif";
	  if (this.fTr1.all.tags('DIV')[0]) {
	    target = this.fTr1.all.tags('DIV')[0];
	  }
    }
  }

  if ( target && target.className == 'window' ) {
    if ( target.offsetHeight > target.scrollHeight ) {
      target.style.height = target.scrollHeight+10;
    }
	var e = target.parentElement;
	while ( e.tagName != 'BODY' ) {
	  if ( e.className == 'window' ) {
	    var minHeight = (e.scrollHeight > 350) 
          ? 350 
          : e.scrollHeight;
	    if (e.offsetHeight < minHeight) {
          e.style.height = minHeight+10;
		}
        if ( IE_Version >= 5 ) {
		  e.doScroll('scrollbarDown');
		}
		break;
	  }
	  e = e.parentElement;
	}
  }
 
  if ( IE_Version >= 5 ) {
    IOAreaBody.innerHTML = ""; 
  }
  else {
    self.setTimeout("IOArea.document.body.innerHTML = ''", 100); //IE 4
  }

  this.active = false;
}

function IOArea_OnLoad(IOArea) {
  if (!TreeExp.active) {
    return;
  }
  TreeExp.active = false;
  IOArea = Left.IOArea;
  var IOAreaDocument = null;
  try {
	  IOAreaDocument = IOArea.document;
  } catch(e) {
    alert("Errore di comunicazione. L'interfaccia verra' ricaricata");
    top.location.reload();
    return;
  }
  var IOAreaBody = IOAreaDocument.body;

  if ( IOAreaBody.document.all('LEFT') ) {
    //TreeExp.End()
  }
  if ( IOAreaBody.document.all('EXEC') ) {
   // delete ATTENDERE PREGO
  }
  else {
    //alert(IOAreaBody.document.body.innerHTML)
    //errorWin = window.open( "ShowInnerHtml.html")
  }
}

function ShowInnerHtml( body ) {
  var IOAreaBody = IOArea.document.body;
  body.innerHTML = IOAreaBody.document.body.innerHTML;
}


function LeftClicked(){
  if (Left.hideAllMenuScriptlets) {
    Left.hideAllMenuScriptlets();
  }
  if (FText.hideAllMenuScriptlets) {
    FText.hideAllMenuScriptlets();
  }
  return

  with (Left.contentFrame) { if(!document.all) return;
   
    if ( HideShow.src.indexOf("images/next1.gif") > 0 ) {
	  treeSize();
	}
  
    var e = event.srcElement;

	if ( e.tagName == "IMG" && e.className == "fPM" ) {
	  _Expand( e, false );
	}	

  }
}

function TextClicked(){
  if (Left.hideAllMenuScriptlets) {
    Left.hideAllMenuScriptlets();
  }
  if (FText.hideAllMenuScriptlets) {
    FText.hideAllMenuScriptlets();
  }
  return;

  with (FText) { if(!document.all) return;
  
    var e = event.srcElement;

	if ( e.tagName == "IMG" && e.className == "fPM" ) {
	  _Expand( e, false );
	}	

  }
}

function LeftOnResize() {
  LeftFrameWidth = LeftFrame.style.width;
  LeftFrameHeight = LeftFrame.style.height;

  if ( IE_Version >= 5 ) {
    HideShow.style.left = ( LeftFrame.style.width - 20 );
  }
  else {
    self.setTimeout("HideShow.style.left = (LeftFrame.style.width - 20)", 150);
  }
}

function INIT_ALL() {
  MainFrame = top.document.body.all.item('Main');
  LeftFrame  = top.document.body.all.item('Left');
  TextFrame = top.document.body.all.item('FText');
  // var verIE = IE_Version;
  // if ( verIE === "8.0" ) { FText = TextFrame.document.frames; }

  LeftBody  = Left.document.body;
  //la parte destra risulta protetta 
  //se presente altra applicazione
  TextBody = FText.document.body;

  IOAreaFrame = Left.document.body.all.item('IOArea');
  IOArea = Left.IOArea;
  //IOAreaFrame.onload = IOArea_OnLoad //non funziona

  Left.contentFrame.document.onclick = LeftClicked;
  LeftFrame.onresize = LeftOnResize;
  
  //FText.document.onclick = TextClicked
 
  LeftBody.insertAdjacentHTML(
    "afterBegin",
	"<img id=\"HideShow\" style=\"position:absolute;\"></img>"
  );
  
  HideShow = LeftBody.all.item('HideShow');
  //HideShow.style.position = "absolute"
  HideShow.style.zIndex = 15;

  HideShow.onclick = treeSize;
  
  HideShow.style.left = (LeftFrame.style.width - 20);
  HideShow.style.top = 3;

  HideShow.src = "images/prev1.gif";
  HideShow.title = "Maximize";
  
  U_init();
  
  if ( Left.contentFrame.ExpandMe ) {
    _Expand( Left.contentFrame.ExpandMe, false );
  }
  else if ( FText.ExpandMe ) {
    //alert( FText.ExpandMe.outerHTML )
    //_Expand( FText.ExpandMe, false )
  }
}

function treeSize() {
  if (HideShow.src.indexOf("images/prev1.gif") > 0 ) {
    LeftFrame.onresize = null;
    HideShow.style.left = 0;
    LeftFrameWidth = LeftFrame.width;
	LeftBody.all.item('menuBar').style.visibility = "hidden";
	LeftBody.all.item('contentDiv').style.visibility = "hidden";
    LeftFrame.scrolling = "no";
    MainFrame.cols="12,*";
    HideShow.src = "images/next1.gif";
	HideShow.title = "Restore";
	TextFrame.focus();
	self.setTimeout("Left.fixSize()", 100);
  }
  else {
	LeftBody.all.item('menuBar').style.visibility = "visible";
	LeftBody.all.item('contentDiv').style.visibility = "visible";
    LeftFrame.scrolling = "auto";
    MainFrame.cols= LeftFrameWidth + ",*";
    HideShow.src = "images/prev1.gif";
	HideShow.title = "Maximize";
    HideShow.style.left = LeftFrameWidth - 20;
    LeftFrame.onresize = LeftOnResize;
	LeftFrame.focus();
	self.setTimeout("Left.fixSize()", 100);
  }
}
