### ----- USER MANAGEMENT ------------------------------------------------------

use constant COOKIE_EXPIRE => 900;

sub CookieExpireTime {
  my @tm = (qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec));
  my @td = (qw(Sun Mon Tue Wed Thu Fri Sat));

  my @g = localtime(shift);
  $g[5] += 1900;
  for (0..2) {
    $g[$_] = substr("0$g[$_]",-2);
  }
  return "$td[$g[6]], $g[3]-$tm[$g[4]]-$g[5] $g[2]:$g[1]:$g[0] GMT";
}

sub sql_convertdate {
  my $datepart = shift; my @g = split("/", $datepart);

  if ($dbdateformat eq "YMD") {
    return"$g[0]/$g[1]/$g[2]";
  }
  if ($dbdateformat eq "DMY") {
    $datepart = "$g[2]/$g[1]/$g[0]";
  }
  else {
    $datepart = "$g[1]/$g[2]/$g[0]";
  }
}

sub SqlCookieExpireTime {
  my @g = localtime(shift);
  $g[5] += 1900; $g[4] += 1; 
  for (0..2) {
    $g[$_] = substr("0$g[$_]",-2);
  } 

  return sql_convertdate("$g[5]/$g[4]/$g[3]")." $g[2]:$g[1]:$g[0]";
}

sub SetUserLocalSessionCookie {
  my ($cookie, $cookie_hex, $cookie_exp); require Digest::MD5;

  my $ca = $main::Application->{'cfg.authenticate'};
  my $lcluname = $main::Session->{$ca.'UserName'}; 
  my $AUTH_USER = "$ca\\$lcluname"; 
  my $ApplUser = $main::Session->{UserName};

  my $REMOTE_ADDR = $main::Request->ServerVariables('REMOTE_ADDR')->Item(); 

  debug2log("SetUserLocalSessionCookie - cookie id:$ca\\$lcluname/$REMOTE_ADDR;UserName:$ApplUser");
  
  my $ExpireTime = time()+(1*COOKIE_EXPIRE);
  
  $cookie = "$AUTH_USER/$REMOTE_ADDR"; 
  $cookie_hex = Digest::MD5::md5_hex($cookie);
  $cookie_exp = SqlCookieExpireTime($ExpireTime);
  
  my $dbr = dbExecute("
    SELECT * from tbl_UsersLocalSessionCookies
    WHERE CookieHexString = '$cookie_hex'
  ");
  if ( $dbr->eof() ) {
    dbExecute("
      INSERT into tbl_UsersLocalSessionCookies
      (CookieHexString, CookieExpireTime, UserName, AUTH_USER, REMOTE_ADDR)
      Values ('$cookie_hex', '$cookie_exp', '$ApplUser', '$AUTH_USER', '$REMOTE_ADDR')
    ");
  }
  else {
    dbExecute("
      UPDATE tbl_UsersLocalSessionCookies

      SET
        CookieHexString  = '$cookie_hex', 
        CookieExpireTime = '$cookie_exp', 
        UserName         = '$ApplUser', 
        AUTH_USER        = '$AUTH_USER', 
        REMOTE_ADDR      = '$REMOTE_ADDR'
        
      WHERE CookieHexString = '$cookie_hex'
    ");
  }
  $dbr->Close();

  $main::Response->{'Cookies'}->{'IXR_User'}->{'LocalSessionCookie'} = $cookie_hex;
  $main::Response->{'Cookies'}->{'IXR_User'}->{'expires'} = CookieExpireTime($ExpireTime);
}

sub GetUserLocalSessionCookie {
  my ($cookie, $cookie_hex, $cookie_exp); require Digest::MD5;

  my ($AUTH_USER, $REMOTE_ADDR);
  debug2log("GetUserLocalSessionCookie - User:$UserName");
  my $ca = $main::Application->{'cfg.authenticate'};
  $AUTH_USER = $ca."\\".$main::Session->{$ca.'UserName'};
  $REMOTE_ADDR = $main::Request->ServerVariables('REMOTE_ADDR')->Item();

  debug2log("GetUserLocalSessionCookie - cookie id:$AUTH_USER/$REMOTE_ADDR;UserName:$UserName");

  my $ExpireTime = time();
   
  if ( $main::Request->ServerVariables('AUTH_USER')->Item() and $ca eq 'NTLM') {
    $AUTH_USER = $main::Request->ServerVariables('AUTH_USER')->Item();
    
    $cookie = "$AUTH_USER/$REMOTE_ADDR";
    $cookie_hex = Digest::MD5::md5_hex($cookie);
  }
  else {
    $cookie_hex =  $main::Request->{'Cookies'}->{'IXR_User'}->{'LocalSessionCookie'};
  }

  $cookie_exp = SqlCookieExpireTime($ExpireTime);
	debug2log("Cookie Expire time is $cookie_exp");
	debug2log("Cookie HEX is $cookie_hex");
  
  my $dbr = dbExecute("
    SELECT *, CASE WHEN '$cookie_exp' < a.CookieExpireTime THEN 1 ELSE 0 END As CookieIsValid 
    from tbl_UsersLocalSessionCookies a WHERE CookieHexString = '$cookie_hex' AND REMOTE_ADDR = '$REMOTE_ADDR'
  ");
  if ( !$dbr->eof() ) {
    if ($dbr->Fields()->Item('CookieIsValid')->Value()) {
      $UserName = $dbr->Fields()->Item('UserName')->Value();
      $main::Session->{'UserName'} = $main::requestVars->{'UserName'} = $UserName;
      $cookie_exp = SqlCookieExpireTime(time()+(1*COOKIE_EXPIRE));
	  debug2log("Update Cookie Expire time is $cookie_exp");
    dbExecute("
      UPDATE tbl_UsersLocalSessionCookies

      SET
        CookieExpireTime = '$cookie_exp' 
        
      WHERE CookieHexString = '$cookie_hex'
    ");
    }
    else {
      dbExecute("DELETE from tbl_UsersLocalSessionCookies WHERE CookieHexString = '$cookie_hex'");
	  $UserName = "";
    }
  }
  else {
  }
  $dbr->Close(); 

  debug2log("GetUserLocalSessionCookie - cookie id: $AUTH_USER/$REMOTE_ADDR;User:$UserName");
  return $UserName;
}

#sub Check_NTLM {
#  return if $main::Application->{'cfg.authenticate'} ne 'NTLM';

#  if ( $main::Request->ServerVariables("AUTH_USER")->Item() eq "" ) {
#    $main::Response->Clear();
#    $main::Response->{'Buffer'} = 1;
#    $main::Response->{'Status'} = "401 Unauthorized";
#    $main::Response->AddHeader("WWW-Authenticate","NTLM");
  
#    TERMINATE_REQUEST();
#  }
#  @{$main::requestVars}{qw(NetName UserName)} 
#    = 
#  split(/\\/, $main::Request->ServerVariables("AUTH_USER")->Item() );
  
#  return $main::requestVars->{'UserName'}; # = $main::Request->ServerVariables("AUTH_USER")->Item();
#}

#sub Check_ISAPI {
#  return if $main::Application->{'cfg.authenticate'} ne 'ISAPI';

#  if ( $main::Request->ServerVariables("HTTP_AUTHORIZATION")->Item() eq "" ) {
#    $main::Response->Clear();
#    $main::Response->{'Buffer'} = 1;
#    $main::Response->{'Status'} = "401 Unauthorized";
#    $main::Response->AddHeader("WWW-Authenticate","NTLM");
  
#    TERMINATE_REQUEST();
#  }
#  @{$main::requestVars}{qw(NetName UserName)} 
##    = 
##  split(/\\/, $main::Request->ServerVariables("AUTH_USER")->Item() );
  
##  return $main::requestVars->{'UserName'}; # = $main::Request->ServerVariables("AUTH_USER")->Item();
#}

sub Check_AUTHENTICATION_DISABLED {
  my $ca = $main::Application->{'cfg.authenticate'};
  # my $UserName = $main::Session->{'UserName'};
  
  my ($domain, $user, $role) = (undef, undef, undef);
  debug2log("chkAUTH - User:$UserName;CA:$ca");
  
  if ($ca eq 'NTLM') {
    ($domain, $user) =  split( /\\/, $main::Request->ServerVariables("AUTH_USER")->Item(), 2 ) ;
    $main::Session->{$ca.'UserName'} = $user;
  }
  elsif ($ca eq 'ISAPI') {
    $user = $main::Request->ServerVariables("HTTP_AUTHORIZATION")->Item();
    $role = $main::Request->ServerVariables("HTTP_PROFILE")->Item() || '';
    $main::Session->{$ca.'UserName'} = $user;
#    die "in ca user ==> $user <== xx ==> $xx <== yy ==> $yy <==\n";
  }
  elsif ($ca eq 'ISAPIWAC') {
    $user = $main::Request->ServerVariables("HTTP_USER")->Item();
    $role = $main::Request->ServerVariables("HTTP_PROFILE")->Item() || '';
    $main::Session->{$ca.'UserName'} = $user;
#    die "in ca user ==> $user <== xx ==> $xx <== yy ==> $yy <==\n";
  }
  elsif ($ca eq 'RACF') {
    $user = $main::Session->{RACFUserName};
    $role = $main::Session->{RACFUserRole};
    unless ( $user ) {
      $main::Response->Write("\<DIV ID=\"EXEC\"\>\<script>\x0d\x0atop.LogonRACFUser()\x0d\x0a\</script\>\</DIV\>");
      TERMINATE_REQUEST();
    }
  }
  elsif ($ca eq 'PGE') {
       	
    ($domain, $user) =  split( /\\/, $main::Request->ServerVariables("AUTH_USER")->Item(), 2 ) ;
    EXIT_ERROR("User Name not defined for this session", 0) unless $user;
     
    $main::Session->{$ca.'UserName'} = $user;
    my $currentProf = $main::Session->{U_User_Profile};
    my $UAttrs = {};
    if ( $currentProf ) {
      @{$UAttrs}{qw(username torre filiale istituto banca nome cognome ruolo profilo)} = split /\t/, $currentProf;
      ($user, $role) = @{$UAttrs}{qw(username ruolo)};
    }
    else {
    	
      my $netuser = $user;
	  my $resp = {};
      if ( exists($XReport::cfg->{PGE}->{specialuser}->{uc($netuser)}) ) {
    	my $specialu = { %{$XReport::cfg->{PGE}->{specialuser}->{uc($netuser)}} };
    	$user = uc(delete $specialu->{UserName});
    	$resp = delete $specialu->{WSresp};
    	foreach ( keys %$specialu ) {
    		next unless $specialu->{$_};
    		$main::requestVars->{$_} = $specialu->{$_};
    	}
      }
  
	  @{$UAttrs}{qw(torre filiale istituto)} = ( $main::requestVars->{U_Torre}, 
	   			                                    substr("00000".$main::requestVars->{U_Filiale}, -5),
  	   			                                    substr("00".$main::requestVars->{U_Istituto}, -2) 
  	   			                                    );
  	   			                                    
	  $UAttrs->{istituto} = '01' if  !$main::requestVars->{U_Istituto} || $main::requestVars->{U_Istituto} =~ /^(?:52|83|86)$/;
	  $UAttrs->{torre} = 'UJ' unless $UAttrs->{torre};
	    
	  if ( !exists($XReport::cfg->{PGE}->{specialuser}->{uc($netuser)}) || !exists($resp->{PG_COD_AUTHOR}) ) {

	  	require SOAP::Lite;
	
        debug2log("chkAUTH - calling PGE for torre:$UAttrs->{torre};user:$user;applid:"
        		.$XReport::cfg->{'PGE'}->{applid});
        (my $PGEurl = $XReport::cfg->{'PGE'}->{url} ) =~ s/\#\{(.+?)\}\#/$UAttrs->{$1}/g;		
	  	$resp = SOAP::Lite
	    	-> uri('http://bfutil.pre.xframe.usi.it')
	    	-> proxy($PGEurl)
	    	-> getExtendPreUserInfo($UAttrs->{torre}, 
	    							$user, 
	    							$XReport::cfg->{'PGE'}->{applid})
	    	-> result();
	  } 
	  
	  debug2log("chkAUTH - PGE response:".Dumper($resp));
	  if ( !$resp || $resp->{PG_ERRORE}->{descError} ) {
	    $main::Response->Clear();
    	$main::Response->{'Buffer'} = 1;
    	EXIT_ERROR("PG Error upon request for\\n\\t" 
    	          . join('/', ($UAttrs->{torre},
    	         			$XReport::cfg->{'PGE'}->{applid}, 
    	         			$user))
    	         .  ":\\n" . ($resp  
    	                   ? $resp->{PG_ERRORE}->{descError}
    	                   : "undefined response")
    	         , 0);
	  }
	  
	  $UAttrs->{filiale} = substr("00000".($user =~ /^UP\./i 
                      ? $resp->{PG_COD_SPORT_CONT} 
                      : $resp->{PG_UFFICIO_INQ}), -5);
                      
	  @{$UAttrs}{qw(cognome nome ruolo profilo)} = 
	        map { $_ =~ s/\s+$//; $_ =~ s/'/''/g; $_; } @{$resp}{qw(PG_COGNOME_OPER PG_NOME_OPER PG_RUOLO PG_COD_AUTHOR)};
      $role = $UAttrs->{ruolo};
	  @{$UAttrs}{qw(banca torre)} = map { $_ =~ s/\s+$//; $_ =~ s/'/''/g; $_;} @{$resp->{PG_BANCA}}{qw(codAbi codTorre)};
  	  $UAttrs->{istituto} = substr("00$resp->{PG_BANCA}->{codBanca}", -2);
	  $UAttrs->{istituto} = '01' if $resp->{PG_BANCA}->{codBanca} =~ /^(?:52|83|86)$/;
	 
	  eval "\$user = $XReport::cfg->{PGE}->{assignuser};";
	  $UAttrs->{username} = $user;  

      $main::Session->{U_User_Profile} = join("\t", @{$UAttrs}{qw(username torre filiale istituto banca nome cognome ruolo profilo)});
	  
    }

	debug2log("chkAUTH - UAttrs:".Dumper($UAttrs));
    @{$main::requestVars}{qw(U_Torre U_Filiale U_Istituto U_Banca U_Nome U_Cognome U_Ruolo U_Profile )} =
    	   @{$UAttrs}{qw(torre filiale istituto banca nome cognome ruolo profilo)};
	@{$main::requestVars}{qw(U_IsMine)} = ('0');
	@{$main::requestVars}{qw(U_IsMine)} = ('1') if $UAttrs->{ruolo} =~ /^(?:RTRESROL|RTADDROL|BRRESROL|BRADDROL|BDRESROL|BDADDROL|RTARCTER)$/;
  }
  else {
    	$main::Response->Write("<DIV ID=\"EXEC\">\<script>\x0d\x0atop.LogonUser()\x0d\x0a\</script></DIV>");
    	TERMINATE_REQUEST();
  }
  
  debug2log("chkAUTH - User:$UserName;newuser:$user;role:$role");
  if ( !$user ) {
    $main::Response->Clear();
    $main::Response->{'Buffer'} = 1;
    EXIT_ERROR("Impossibile verificare credenziali - Contattare Help Desk", 0);
  }

  @{$main::requestVars}{qw(NetName UserName)} = ($domain, $user); 
  return $main::requestVars->{'UserName'}; 

}

sub VerifyUser {   
  
  my $ca = $main::Application->{'cfg.authenticate'};
  debug2log("VerifyUser - ca:$ca;User:$UserName;ioprogr=".$main::requestVars->{'ioprogr'});

  GetUserLocalSessionCookie() if ($main::requestVars->{'ioprogr'}) > 1; 
  return if $UserName;
  
  $UserName  = Check_AUTHENTICATION();
  $UserAlias = $UserName;
   
  if ( $main::Application->{'cfg.use_useraliases'} ) {
  
    my $dbr_al = dbExecute("SELECT * from tbl_UserAliases WHERE UserAlias='$UserAlias'");
    
    if ( $dbr_al->eof() ) {
      $dbr_al->Close();
      foreach ( qw(UserName RACFUserName NetName) ) { $main::Session->{$_} = ''; }
      EXIT_ERROR("UTENTE $UserName NON TROVATO", 0);
    }

    $UserName = $dbr_al->Fields()->Item("UserName")->Value(); 
    $dbr_al->Close();

  }
  my $dbr = dbExecute("SELECT * from tbl_Users WHERE UserName = '$UserName'");
  my $usrondb = ($dbr->eof() ? 0 : 1);
  $dbr->Close(); 
  debug2log("VerifyUser - User:$UserName;alias:$UserAlias;userondb:$usrondb");
  
  unless ( $usrondb ) {
    foreach ( qw(UserName RACFUserName NetName) ) { $main::Session->{$_} = ''; }
    EXIT_ERROR("UTENTE $UserName NON DEFINITO A XREPORT", 0);
  }
  else {
    $main::Session->{'UserName'} = $main::requestVars->{'UserName'} = $UserName;
  }
}

### --------------------------------------------------------------------------------------

1;