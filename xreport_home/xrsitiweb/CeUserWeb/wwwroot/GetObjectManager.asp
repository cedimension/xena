<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

### vim: set syn=perl: ###

use lib("$main::Application->{'XREPORT_HOME'}/perllib");

use strict;

BEGIN {
#  require XReport::LoginWin32;
#  XReport::LoginWin32::RevertToSelf();  
}

use constant EF_PDF => 1;
use constant EF_HTML => 2;

our ($Application, $Server, $Session, $Request, $Response);

my ($JobReportId, $FileFormat, $ElabFormat, $ObjectManager, $dbc, $dbr);

sub dbExecute {
  my $rs = $dbc->Execute( "$_[0]" );
  if ( $dbc->Errors()->Count() > 0 ) {
    my $msg = "$_[0]\n" . $dbc->Errors(0)->Description;
    $Response->Write($msg . "<br>");
	$Response->Write("Exiting ON DataBase ERROR !!!");
	$Response->Flush();
	die $msg;
  }
  return $rs;
}

sub INIT_ALL {
  $dbc = $Server->CreateObject('ADODB.Connection');
  $dbc->Open($Application->{'cfg.db.XREPORT'});

  $JobReportId = $Request->QueryString("JobReportId")->Item();
  $FileFormat = $Request->QueryString("FileFormat")->Item();

  $Response->{'Buffer'} = 0;
}

sub TERMINATE_ALL {
  $Server->Transfer($ObjectManager);
}

INIT_ALL();

$dbr = dbExecute("
  SELECT ElabFormat FROM tbl_JobReportsElabOptions
  WHERE JobReportId = $JobReportId
");
if (!$dbr->eof()) {
  $ElabFormat = $dbr->Fields->Item('ElabFormat')->Value();
}
else {
  $ElabFormat = EF_PDF;
}

if ( uc($FileFormat) eq 'EXCEL' ) {
  $ObjectManager = "ObjectManagerExcel.html";
}
else {
  $ObjectManager = "ObjectManagerPdf.html";
}

TERMINATE_ALL();

</SCRIPT>
