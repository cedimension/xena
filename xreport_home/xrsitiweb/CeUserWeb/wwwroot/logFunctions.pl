use POSIX qw(strftime);
use IO::Socket::INET;

$main::debugdir = $XReport::cfg->{logsdir} || $XReport::cfg->{workdir};
$main::debugdir .= "/$main::servername\_debug";
$main::DEBUG = $main::Application->{DEBUG} || $XReport::cfg->{DEBUG} || 0;
use constant DBGDEST => '226.1.1.9:3000';
#use constant LOGDEST => '226.1.1.9:2000';
$main::dbgsock = new IO::Socket::INET(Proto=>'udp',PeerAddr=>DBGDEST, Reuse => 1);
#$main::logsock = new IO::Socket::INET(Proto=>'udp',PeerAddr=>LOGDEST, Reuse => 1);

sub debug2log {
  $main::dbgsock->send($main::Session->{SessionID} . " " . strftime("%Y-%m-%d %H:%M:%S.%U", localtime) . " $main::ASPVER $main::servername\> " . join('', @_), 0, DBGDEST) if $main::dbgsock;
  return 1;

#  return 1 unless -d $main::debugdir;

#  i::logit(join('', @_));

#  my $mplog = new FileHandle '>>'.$main::debugdir.'/service.log';
#  print $mplog $main::Session->{SessionID} . " " . localtime() . " $main::ASPVER $main::servername: " . join('', @_)."\n";
#  $mplog->close();
}

sub write2log {
  return main::debug2log(@_);
}

sub TERMINATE_REQUEST {
  debug2log("TERMINATE_REQ called by: ".join('::', (caller())[0,2])." - ".$_[0]);
#  for (@main::TARGETs) {
#    ($OUTPUT[$_])->close() if $OUTPUT[$_]; unlink $xmlFile[$_] if $xmlFile[$_];
#  }
  $main::Response->Flush();
  # exit(0); #$main::Response->End(); exit(0);
}

sub WRITE_ERROR {
	debug2log("ERROR at ".join('::',(caller(1))[0,2]).': '.$_[0]);
	$main::Response->Write(
    "\<html\>\<body\>\<scr".
      "ipt type=\"text/javascript\" \>alert('".$_[0]."')\</scr".
      "ipt\>\</body\>\</html\>"
  	); 
  	return undef;
}

sub EXIT_ERROR {
  my ($msg, $restart) = @_;  $restart = 0 if !$restart;
  debug2log("EXIT_ERROR called by: ".join('::', (caller())[0,2])." - ".$msg);
  return WRITE_ERROR($msg); 
  $main::Response->Write(
    "<DIV ID=\"EXEC\">\<script>top.ERROR_MSG('$msg', $restart)\</script></DIV>"
  ); 
  
  TERMINATE_REQUEST();
}

1;