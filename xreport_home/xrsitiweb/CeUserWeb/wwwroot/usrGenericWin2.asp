<!--
 Nota Roberto CeDimension :
    Pagina Asp legata a XReport Oper (Visualizza Gui Utente)
	richiamata da UsrGenericWin
-->

<%@ LANGUAGE = PerlScript%>
<HTMl>
<head>
<title><%= $Request->querystring->user%>/<%= $Session->SessionID %></title>
</head>
 <script type="text/javascript" src="js/cereport.js"></script>
 <script type="text/javascript" src="js/cookie.js"></script>
 <script>ExpandRequest="FromUsers.asp?<%= $Request->querystring->user %>"</script>
</head>
<%
use lib ($ENV{XREPORT_HOME}."/perllib");
use PSlist;
use XReport::Util; 

sub Dominio_Utenti_e_Gruppi {

    # Dominio
	my $Dominio = getConfValues('XrOpers')
                                    ->{'Domain'};

	# Lista Utenti
	my @ListaUtenti;
    my $UsersList = getConfValues('XrOpers')
                                    ->{'UsersList'}
		  						         ->{'user'};

    my $refTest= ref $UsersList; 

       if (!$refTest){ @ListaUtenti=($UsersList);}
       else { @ListaUtenti=@$UsersList; }

    # Lista Gruppi
	my @ListaGruppi;
    my $GroupsList = getConfValues('XrOpers')
                                    ->{'GroupsList'}
		    						      ->{'group'};


    my $refTest= ref $GroupsList; 

       if (!$refTest){ @ListaGruppi=($GroupsList);}
       else { @ListaGruppi=@$GroupsList; }

return $Dominio,\@ListaUtenti,\@ListaGruppi;

}

sub securityCheck {

  my $AUTH_USER= shift @_;
  
   # Modulo Perl gestione gruppi e user di rete  
   use Win32::NetAdmin qw(LocalGroupGetMembers GetUsers GroupIsMember); 
                                                                       
 
   # dominio utenti e gruppi da leggere da xreport.cfg    
   my @usersList;my @usersGroup;
   my ($dominio,$ListaUtenti,$ListaGruppi)=&Dominio_Utenti_e_Gruppi;
   @usersList = @$ListaUtenti;
   @usersGroup= @$ListaGruppi;
   
 
 
   # In userConnected elemino il nome di dominio e lo rendo maiuscolo   
   my($domain,$userConnected); 
   
   if($AUTH_USER =~ /\\/){    
     ($domain,$userConnected)=split (/\\/, $AUTH_USER );
   }
   else{
	 $userConnected=$AUTH_USER;
   }
   
	$userConnected = uc $userConnected; 

    
    # Se Connected User appatiene al gruppo degli amministratori Cerepot $grpTest=1
   my $grpTest;  
       
	   for $usersGroup(@usersGroup){
          
		  $grpTest=GroupIsMember($dominio, $usersGroup, $userConnected);#  
          last if $grpTest

	   }                                                        


    # Se Connected User non � in grp Amm. verifico la sua presenza nella lista degli utenti permessi 
    my $usrTest;
    
	unless ($grpTest){
 
       for (@usersList){

	       $userPermitted= uc $_;
           if ($userPermitted eq $userConnected){
          
               $usrTest=1;
			   last;
		   }
    
	   }
    
	}

return $grpTest,$usrTest
}#end_sub


   # ricavo nome dell'utente connesso (es.dmnbpb03\xreport per Integrated Conn.  o xreport per base Conn.) 
   my $AUTH_USER = $Request->ServerVariables('AUTH_USER')->Item();
   my ($grpTest,$usrTest)=&securityCheck($AUTH_USER);

      unless ($grpTest or $usrTest){
   
         $Response->Write("<h3>EXIT 999.999</h3>\n");
	     exit;
		 
      }

#$Response->Write("<h3>Test $AUTH_USER $grpTest or $usrTest </h3>\n");
#exit;




%>
<frameset id="Main" cols="310,*" ONLOAD="INIT_ALL()">
 <frame id="Left" name="Left" src="LeftUser.html" marginwidth="0" scrolling="AUTO">
 <frame id="Text" name="Text" src='TextUser.html' marginwidth="0" scrolling="AUTO">
</frameset>

<!--
<body bgcolor=ffff00>
<h1>Luciana <%= $Request->querystring %> </h1>
</body>
-->

<%= $Session->Abandon %>


</HTMl>
