<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

### vim: set syn=perl: ###

use strict; 

use FileHandle; use URI::Escape; use Win32::OLE; use Digest::MD5;

our ($Server, $Application, $Session, $Request, $Response, $XREPORT_HOME);

my ($requestVars, $dbc);

sub CreateObject {
  return $Server->CreateObject($_[0]);
}

sub CONNECT {
  $dbc = Win32::OLE->new("ADODB.Connection");

  $dbc->{"ConnectionTimeout"} = 10;
  $dbc->{"CommandTimeout"} = 30;
	
  $dbc->Open($Application->{"Cfg.OMS"});
}

sub DISCONNECT {
  $dbc->Close();
}

sub dbExecute {
  my $sql = $_[0]; my $dbr = CreateObject("ADODB.Recordset");
    
  $dbr->Open($sql, $dbc, 3, 3);
  
  return $dbr;
}

sub getFormVars {
  return {} if $Request->ServerVariables("REQUEST_METHOD")->Item() ne "POST";
  
  my ($strVars, $FormVars) = ($Request->BinaryRead($Request->TotalBytes()), {}); 
  
  $strVars =~ s/\+/ /g;
  
  my @FormVars = map {uri_unescape($_)} split("&",$strVars);
  for (@FormVars) {
    my ($VarName, $VarValue) = $_ =~ /^(\w+)=(.*)$/;
    $FormVars->{$VarName} = $VarValue;
  }

  return $FormVars;
}

sub ChangeUserPassword {
  Win32::OLE->new('LoginAdmin.ImpersonateUser')->Logoff();

  if ( $requestVars->{'UserName'} eq "" ) {
    EXIT_ERROR("INSERIRE NOME UTENTE E PASSWORD");
  }

  if ( $requestVars->{'NewPassword'} eq "" ) {
    EXIT_ERROR("INSERIRE NUOVA PASSWORD");
  }

  for (qw(NewPassword ConfirmNewPassword)) {
    $requestVars->{$_} = Digest::MD5::md5_hex($requestVars->{$_})
      if
    $requestVars->{$_} ne "";
  }

  my ($UserName, $NewPassword, $ConfirmNewPassword, $PassMustBeChanged) 
    = 
  @{$requestVars}{qw(UserName NewPassword ConfirmNewPassword PassMustBeChanged)};

  my $dbr = dbExecute("SELECT * from tbl_Users WHERE UserName = '$UserName'");
  
  if ( !$dbr->eof() ) {
    if ($NewPassword eq $ConfirmNewPassword) {
      dbExecute("
        UPDATE tbl_Users set Password = '$NewPassword', PassMustBeChanged = 1
        WHERE UserName = '$UserName'
      ");
    }
    else {
      EXIT_ERROR("Conferma Nuova Password ERRATA");
    }
  }
  else {
    EXIT_ERROR("UTENTE NON CONOSCIUTO");
  }
 
  $dbr->Close();
  
  return $UserName;
}

sub EXIT_ERROR {
  my ($UserName, $MESSAGE) = ("$requestVars->{'UserName'}", shift);
  
  my $xml = CreateObject("MSXML2.DOMDocument.4.0"); $xml->{"async"} = 0;
  my $xslt = CreateObject("MSXML2.DOMDocument.4.0"); $xslt->{"async"} = 0;
   
  my $rc = $xml->loadXML(
    "<ChangeUserPassword UserName=\"$UserName\" MESSAGE=\"$MESSAGE\"/>"
  ); 

  $xslt->load($Server->MapPath("xsl/ChangeUserPassword.xsl"));

  $Response->Write($xml->transformNode($xslt)); exit();
}

sub EXIT_OK {
  my $UserName = "$requestVars->{'UserName'}";

  my $MESSAGE = "PASSWORD UTENTE $UserName CAMBIATA";
  
  my $xml = CreateObject("MSXML2.DOMDocument.4.0"); $xml->{"async"} = 0;
  my $xslt = CreateObject("MSXML2.DOMDocument.4.0"); $xslt->{"async"} = 0;
   
  my $rc = $xml->loadXML(
    "<ChangeUserPassword MESSAGE=\"$MESSAGE\"/>"
  ); 

  $xslt->load($Server->MapPath("xsl/ChangeUserPassword.xsl"));

  $Response->Write($xml->transformNode($xslt)); exit();
}

sub INIT_ALL {
  CONNECT(); 

  $requestVars = getFormVars();
}

sub TERMINATE_ALL {
  DISCONNECT();
}

INIT_ALL();

ChangeUserPassword(); EXIT_OK();

TERMINATE_ALL();
    
</SCRIPT>
