use lib($ENV{'XREPORT_HOME'}."/perllib");
use lib($ENV{'APPL_PHYSICAL_PATH'}."/perllib");

use strict;
use XReport::DBUtil;
use XReport::Util;
use PSlist;
use CGI qw/:standard *table /;
use File::Basename;

#--------------------------
# Cgi FileName
#--------------------------
my ($cgiFile) = fileparse($0);

#--------------------------
# SPEC. FILE CGI
#--------------------------
my $q = new CGI;
my $TableType      = $q->param('TableType');
my $ReportName     = $q->param('ReportName');
my $JobName        = $q->param('JobName');
my $lf             = $q->param('lf');

# Per PageLoading
my $TableType_Load = $q->param('TableType_Load');
my $Id             = $q->param('Id');

my $frame;
#============================================================
my $StyleHead= &StyleHead;

$frame=&Loading($ReportName,$Id,$TableType_Load,$lf) if $TableType eq 'Loading';
$frame=&JobNameList($JobName, $ReportName) if $TableType eq 'JobNameList' ;
$frame=&BrowseCtrl($lf,$ReportName,$Id) if $TableType eq 'BrowseCtrl'   ;

print <<EOF;
$frame
</body>
</html>
EOF
#----------------------------------
sub Loading {

   my ($ReportName,$Id,$TableType_Load,$lf)=@_;
 
  my $StyleHead= &StyleHead;
  my $frame = <<EOF;
refresh: 0; URL=$cgiFile?TableType=$TableType_Load&lf=$lf&ReportName=$ReportName&Id=$Id;
Content-Type: text/html; charset=ISO-8859-1

<html>
<head>
$StyleHead
</head>
<body>
<hr>
<br/>
<b><font color=red>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOADING... </font></b>  
</body>
</html>
EOF

return $frame
}


sub BrowseCtrl  {
my ($lf,$ReportName,$Id)=@_;


my $frame .= <<EOF;
Content-type: text/html

<html>
<head>
<title>Browse Ctrl</title>
  $StyleHead
</script>
</head>
<body>
<hr>
<br>
<b>$ReportName RepId: $Id</b>
<div style="height:75%;width:100%;overflow:auto; border :1px solid rgb(127,157,185);">
<pre style="margin: 5">
<b>CTRL FILE :</b>
  
EOF
$lf =~ s/file:\/\///;
 
  my $lf_cntrl = $lf;
  $lf_cntrl =~ s/DATA\.TXT\.gz/CNTRL.TXT/; 
  open (IN,"$lf_cntrl") 
		  
		  or 
  
     return "$frame not found or reading error \n\n$lf_cntrl\n";

  while(<IN>) {

	 $frame .= $_;	  
  
  }

return $frame; # to do :
               #
	       #  Gestionere delle prime 66 righe 
	       #   � da approfondire prima di darla
	       #
$frame .= 
"\n================================================================================================================================\n\n".
"<b>DATA FILE :</b>\n\n";

return "$frame not found or reading error \n\n$lf\n" unless -e $lf;

$frame .= &estrai($lf);
  
$frame .= <<EOF;

<b>FIRST 66 LINEs</b>
</pre>
</div>
</body>
</html>

EOF

return $frame ;
}


#----------------------------------
sub JobNameList {

   my ($JobName,$ReportName) = @_;

#my $frame ;
#my $frame = <<EOF;
   my $StyleHead= &StyleHead;


my $frame .= <<EOF;
Content-type: text/html

<html>
<head>
  <title>Joblist</title>
  $StyleHead



<script>

  old=null
  
 function colora(ele){
     
   if (old != null){
       old.style.background=''
	   old.style.color='#000080'
   }
	  
	    //ele.style.background='yellow'
	    ele.style.background='#0000aa'
	    ele.style.color='white'
   
   old=ele
   
 }
  
</script>

</head>
<body>
<br/> <br/>
<b>Report $ReportName :</b>
<div style="height:70%;width:100%;overflow:auto; border :1px solid rgb(127,157,185);">
EOF
   

   my $RS =  dbGetDynamicRs(   
       "select * from tbl_JobReports where JobReportName=\'$ReportName\' and JobName=\'$JobName\'"
   );
   

   if ( $RS->eof() ){ 
        $frame .= "<h1><font class=tref3>&nbsp$ReportName PrintOuts Not Found</font></h1>";
        $frame .= "</body></html>\n";
        exit;
   }
   
       $frame.= <<EOF;
		   <TABLE>
            <TBODY>
             <TR>
			   <TH Colspan=4></TH>
			 </TR>
			 <TR>
			   <th>&nbsp;&nbsp;</th>
			   <TH style='background-color: lavender;'>!!</TH>
               <TH style='background-color: lavender;'>JobName</TH>
               <TH style='background-color: lavender;'>DateTimeXfer</TH>
               <TH style='background-color: lavender;'>JobNum</TH>
               <TH style='background-color: lavender;'>RepId</TH>
             </TR>
EOF
 
   #return $frame; 
   while ( !$RS->eof() ) {


      my $JobReportId       = $RS->Fields->Item('JobReportId')->{'Value'};
      my $XferStartTime     = $RS->Fields->Item('XferStartTime')->{'Value'};
      my $JobNumber         = $RS->Fields->Item('JobNumber')->{'Value'};
      my $JobNamee          = $RS->Fields->Item('JobName')->{'Value'};
      my $lf                = $RS->Fields->Item('LocalFileName')->{'Value'};
      my $LocalPathId       = $RS->Fields->Item('LocalPathId_IN')->{'Value'};
 

      if ($LocalPathId eq "") {
          		
			  
	     my $initPath = getConfValues('LocalPath')->{'default'};
			$initPath = getConfValues('LocalPath')->{'L1'}  if $initPath eq "";
			
			$lf       =  $initPath."/IN/$lf";
      }
      else {
           $lf =  getConfValues('LocalPath')->{$LocalPathId}."/IN/$lf";
      }

	  
	  #$lf= "D:/xreport/storage/IN/".$lf; #BUCO NERO STO IMBROGLIANDO 

	  $frame .= <<EOF;
             <TR>
                 <TD>&nbsp;</TD> 
                 <TD>&nbsp;</TD> 
                 <TD><a target='CONTENUTO' onClick='colora(this)'
	                href=\'$cgiFile?ReportName=$ReportName\&Id=$JobReportId\&lf=$lf\&TableType=Loading\&TableType_Load=BrowseCtrl'>
				 $JobNamee</a></TD> 
                 <TD>$XferStartTime</TD> 
                 <TD>$JobNumber</TD> 
                 <TD>$JobReportId</TD> 
			 </TR>
EOF

	  
	  
	  
	  $RS->MoveNext;

    }

$frame .= <<EOF;

</TBODY>
</TABLE>
</div>
</body>
</html>

EOF

return $frame ;
}


#-----------------------------


sub StyleHead{

my ($second)=@_;


my $processesHead=<<EOF; 
  <link rel="stylesheet" type="text/css" href="./css/mozzilla.css" >
  <style type="text/css">
  
     body{border: 0; margin: 5; padding:0; background-color: white;}
  
  </style>

EOF

return $processesHead;
}

#-----------------------------------------------------------------
sub estrai {

   use Convert::EBCDIC qw(ebcdic2ascii);
   use Compress::Zlib ;
   use File::Basename; 
    
   #---------------------------------------
   my $file = shift;   
   my $dir_tmp = getConfValues('workdir')."/TEMP";
   mkdir($dir_tmp) unless -e $dir_tmp; 
   my $file_tmp = "$dir_tmp/".basename($file);
   $file_tmp =~ s/\.gz//;  
 
   #---------------------------------------------------------
   open(OU,">$file_tmp") or die ("Cannot open $file_tmp\n") ;
   #---------------------------------------------------------
   my $buffer ;
    
   my $gz = gzopen($file, "rb") 
         or die "Cannot open $file: $gzerrno\n" ;
    
   #-------------------------------------------------
   print OU $buffer while $gz->gzread($buffer) > 0 ;
   #-------------------------------------------------
    
   die "Error reading from $file: $gzerrno" . ($gzerrno+0) . "\n" 
         if $gzerrno != Z_STREAM_END ;
    
   $gz->gzclose() ;
	
  #---------------------------------------------------------
  close OU;
   
  my $frame.=&converti($file_tmp,$file);
  unlink($file_tmp);
  return "$frame";
  
}

#--------------------------------------------------------
sub converti {

   my $file_in = shift;
   my $file_ou = shift;

   #------------------------------------------
   my $FF=pack ('C', 12); 
   
   #------------------------------------------
   my $buffer;my $n;my $line; 
   open(IN,"$file_in") 
		   
		   		  or 
  
   return "$frame not found or reading error \n\n$file_in\n";
   
   my $frame;   
   #------------------------------------------
   # LINEA 1
   #------------------------------------------
   read(IN,$buffer,1);
     
     
       # Gestione File ASCII
       $n= unpack('C',$buffer);
       
       $frame .= "Carattere 1  ==> $n <===\n\n"; 
       #0 0 6 non � un file ascii
     
       unless($n == 0 or $n==6) {
         close IN;
         open(IN,"$file_in") or die ("Cannot open $file_in\n");
         my $j=1;
		 while(<IN>) {

             $frame .= $_;
		     ++$j;
	         last if $j > 66;		 
          }   
		 close IN;
		 return $frame;	   
       }   
   
   #gestione File EBCDIC
   read(IN,$buffer,1);

   $n= unpack('C',$buffer);
  
    $frame .= "Carattere 2  ==> $n <===\n\n"; 
    # se il secondo carattere � 90 ( esadecimale=5A) 
    return "$frame This is an AFP File\n\n" if $n==90;
    return "$frame This is a Font File\n\n" if $n==80;
   
   read(IN,$buffer,$n);
   $line = &ebcdic2ascii($buffer);

   $line =~ s/^1//;
   $line =~ s/^ //;
   $frame .= $line."\n";
   #print  $line."\n";

   #------------------------------------------
   # LINEE SUCCESSIVE
   #------------------------------------------
   read(IN,$buffer,1);
   read(IN,$buffer,1);

my $j=1;

   while (read(IN,$buffer,$n)){

	   ++$j; last if $j>66;
       $line = &ebcdic2ascii($buffer);
	   $line =~ s/^1/$FF/;
	   $line =~ s/^ //;
       $frame .= $line."\n";
	
	   read(IN,$buffer,1);
       read(IN,$buffer,1);
	   $n= unpack('C',$buffer);

   }

   close IN;
   return $frame;

}
