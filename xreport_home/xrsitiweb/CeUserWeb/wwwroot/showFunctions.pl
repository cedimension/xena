use constant IQUERY_VARS => 0;
use constant IQUERY_ELEMENT => 1;
use constant IQUERY_FIELDS => 2;
use constant IQUERY_STYLESHEET => 3;

use constant LEFT => 0;
use constant TEXT => 1;

our @TARGETs = (LEFT, TEXT);
our ($XREPORT_HOME, $dbdateformat);
our ($dbc, @OUTPUT, $xml, $xslt, @xmlFile, @xsltFile, $workdir, $userlib);
our ($UserName, $FoldersList);

sub CreateObject {
  return $main::Server->CreateObject($_[0]);
}

sub xmlquote {
  my $t = shift;
  
  $t =~ s/&/&amp;/g;
  $t =~ s/</&lt;/g;
  $t =~ s/>/&gt;/g;
  $t =~ s/\"/&quot;/g;
  $t =~ s/[\'\x92]/&apos;/g;
  
  $t =~ s/([\x80-\xff])/"\&\#".unpack("C",$1).";"/eg;
  
  return $t;
}

sub XmlAttr {
  my ($dbr, $fields) = @_; my ($f, $fi, $fv, $t);

  for $f (@$fields) {
    $fi = $dbr->Fields->Item($f) or die "???? $f";
    $fv = $fi->Value(); 
    $t .= " $f=\"".xmlquote($fv)."\""; 
  }
  
  return $t;
}

sub getFormVars { 
	use URI::Escape;
	my $reqmet   = $main::Request->ServerVariables('REQUEST_METHOD')->item();
	my (%formVars, %reqVars);
	my $tb = $main::Request->TotalBytes();
  	my $strVars = $main::Request->BinaryRead($tb); 
	my $pcount = $main::Request->{'Params'}->{'Count'};
	debug2log("getFormVars - "
				."caller: ". join('::', (caller(1))[0,2])
				." reqLen:". length($strVars)
				." pcount:$pcount\n"
				." strVars: ".($strVars || '__empty__')
				);
	
	if ( $reqmet eq 'POST' ) {
		for my $j ( 1..$pcount ) {
   			my $keyn = $main::Request->{'Params'}->Key($j);
   			my $keyv = $Request->Params($keyn)->Item();
   			$formVars{$keyn} = $keyv;
		}	
		debug2log("getFormVars - formVars:".Dumper(\%formVars)."\n");
	} 

	if ( $strVars ne "" ) {
  		$strVars =~ s/\+/ /g; 
 		%reqVars = ( map { my ($varn, $varv) = $_ =~  /^(\w+)=(.*)$/; 
  				$varn => ( $varn =~ /^(?:UserTimeRef)$/ ? sql_convertdate($varv)
  		                                    : $varv);} split("&",uri_unescape($strVars)) );
		debug2log("getFormVars - reqVars:".Dumper(\%reqVars)."\n");
	}
    
  	RESTART_REQUEST() unless (scalar(keys %formVars) || scalar(keys %reqVars));
  	return {%formVars, %reqVars};
}

sub add_to_xml {
  $OUTPUT[$_[1]]->print($_[0], "\n");
}

sub resolveSQLProc {
  my ($queryName, $values) = (shift, shift);
  use Data::Dumper;
  my ($vars, $ele, $fields) = @{$query_parameters->{$queryName}};
  my $queryExpr = $XReport::cfg->{sqlprocs}->{$queryName} 
    if ( exists($XReport::cfg->{sqlprocs}) && exists($XReport::cfg->{sqlprocs}->{$queryName}));
  $queryExpr =  
#	"execute ce_$queryName".(($ext ne "")?"_$ext " :" ")
    "execute ce_$queryName"
	.join(
      ",", map {"\@$_ = '$values->{$_}'"} @{$vars}
     ) unless $queryExpr;
  ;
  $queryExpr =~ s/\#\{(.+?)\}\#/$values->{$1}/g;
#  $main::Response->AppendToLog("UserName: $values->{UserName} SQL by ".(caller())[2] . ": ".$queryExpr);
  return $queryExpr;
}




sub FOR_XML {
  my ($lt, $queryName, $ext, $values) = @_;  my ($vars, $ele, $fields) = @{$query_parameters->{$queryName}};

  die "INVALID QUERY NAME $queryName" if !exists($query_parameters->{$queryName});

  debug2log("Executing $queryName - caller: ". join('::', (caller())[0,2])." Values:\n".Dumper($values));
  my $dbr = dbExecute(resolveSQLProc($queryName, $values));

  my $xmlid = $main::requestVars->{'ioprogr'};
	
  add_to_xml(
    "<${ele}sList xmlid=\"$xmlid\" QueryName=\"$queryName\" " .
	join(" ", map {"$_ = \"".xmlquote($values->{$_})."\""} @{$vars}) .">", $lt
  );
	
  while(!$dbr->eof()) {
	add_to_xml(
	  "<$ele ".XmlAttr($dbr, $fields)."/>", $lt
	);
    $dbr->MoveNext();
  }
	
  add_to_xml("</${ele}sList>", $lt);
  
  $xsltFile[$lt] = $main::Server->MapPath($query_parameters->{$queryName}->[IQUERY_STYLESHEET]);
}

sub putFoldersList {
  my $lcluname = $UserName; $lcluname =~ s/\\/_/g;
  my $fileName = "$workdir/UsersSaveArea/$lcluname\.FoldersList";
  open(FLIST, ">$fileName"); binmode(FLIST); print FLIST $_[0]; close(FLIST);
}

sub getFoldersList {
  my $lcluname = $UserName; $lcluname =~ s/\\/_/g;
  my $fileName = "$workdir/UsersSaveArea/$lcluname\.FoldersList";
  open(FLIST, "<$fileName"); binmode(FLIST); read(FLIST, $FoldersList, -s $fileName); close(FLIST);
}

sub LoadFoldersTreesXML {
  #if ( $FoldersTreesFrom eq 'XML' ) {
  if (!$xml->load(c::getValues('userlib')."/folderstrees/FoldersTrees.xml")) {
    die("XML LOAD ERROR: ". $xml->parseError()->reason());
  }
  #}
  return $xml;
}

sub LoadFoldersTreesXSL {
  if (!$xslt->load($main::Server->MapPath("xsl/FoldersTrees.xsl"))) {
    die("XSLT LOAD ERROR: ". $xslt->parseError()->reason());
  }
  return $xslt;
}

sub UserFoldersList_xml {
  my ($lcluname, $restart) = @_; my ($dbr, $docroot, $node, $root); $restart = 0 if !defined($restart); 
  
  LoadFoldersTreesXML(); LoadFoldersTreesXSL();

  $docroot = $xml->documentElement();

  $node = $xml->createNode(1, "user", "");
  $node->setAttribute("name", "$lcluname");
  $docroot->appendChild($node);
  
  $dbr = dbExecute(
		   "SELECT DISTINCT RootNode from tbl_ProfilesFoldersTrees "
		   . "WHERE ProfileName IN( " 
		   . "SELECT ProfileName from tbl_UsersProfiles "
		   . "WHERE UserName = '" . $main::requestVars->{'UserName'} ."' "
		   . ") ORDER BY RooTnode"
		  );
#		   "XRUserFoldersTrees '$main::requestVars->{'UserName'}'");

  while(!$dbr->eof()) {
    $root = $dbr->Fields()->Item("RootNode")->Value();
	
    $node = $xml->createNode(1, "usertree", "");
    $node->setAttribute("root", "$root");
    $docroot->appendChild($node);
	
	$dbr->MoveNext();
  }
  $dbr->Close(); my $t_list;  

  $t_list = $xml->transformNode($xslt); 
  #die "null tlist" unless $t_list;

  $t_list =~ s/([\x80-\xff])/"\&\#".unpack("C",$1).";"/eg;

  my %folders = map  { ($_, 1) } $t_list =~ /TreeNode name="([^"]+)" [^>]* folder="1"/ig ;
  
  putFoldersList(join(",", map {"'$_'"} keys(%folders)));

  add_to_xml($t_list, LEFT) if !$restart; 
  
  $xsltFile[LEFT] = $main::Server->MapPath("xsl/".$main::Application->{'cfg.FoldersTreesXsl'});
}

sub UserFoldersList_db {
my $dbr = dbExecute(resolveSQLProc('UserFoldersTrees', { UserName => $main::requestVars->{'UserName'} } ) );
#  my $dbr = dbExecute("XRUserFoldersTrees '$main::requestVars->{'UserName'}'"); $FoldersList = '';
  while(!$dbr->eof()) {
    $FoldersList .= ', ' if $FoldersList ne '';
    $FoldersList .= "'".$dbr->Fields()->Item('FolderName')->Value()."'";
  }
  continue {
    $dbr->MoveNext();
  }
  putFoldersList($FoldersList);

  add_to_xml("<root for=\"$main::requestVars->{'UserName'}\">");

  FOR_XML(LEFT, 'UserFoldersTrees', '', $main::requestVars);
 
  add_to_xml("</root>");
}

sub UserFoldersList { return ($main::Application->{'cfg.usefoldertreefile'} ? UserFoldersList_xml(@_) : UserFoldersList_db(@_)); }

sub VerifyFolder_xml {
  my $FolderName = shift; getFoldersList() if $FoldersList eq ""; # die $FoldersList;

  if ($FoldersList !~ /\'$FolderName\'/) {
	EXIT_ERROR("FolderName $FolderName NOT AUTHORIZED", 0);
  }
  else {
    #todo: verify FolderName not in table of excluded folders
  }
}

sub VerifyFolder_db {
  my $FolderName = shift; getFoldersList() if $FoldersList eq ""; 

  my $chkfn = quotemeta($FolderName);
  return 1 if $FoldersList =~ /\'$chkfn\'/;

  my $ParentFolder = $FolderName; 
  while(1) {
    my $dbr = dbExecute("
      SELECT ParentFolder from tbl_Folders WHERE FolderName = '$ParentFolder'
    ");
    last if $dbr->eof();
    $ParentFolder = $dbr->Fields()->Item(0)->Value();
    my $var = 1 if $FoldersList =~ /\Q'$ParentFolder)'\E/;

    do {
      putFoldersList("$FoldersList, '$FolderName'"); return 1;
    }
    if $FoldersList =~ /\Q'$ParentFolder'\E/;
  }

  EXIT_ERROR("FolderName $FolderName NOT AUTHORIZED", 0);
}

sub VerifyFolder { return ($main::Application->{'cfg.usefoldertreefile'} ? VerifyFolder_xml(@_) : VerifyFolder_db(@_)); }


sub UserFolderListRequest {
  my @frefVars = split(/(?:\/|\%2f)/i, $main::requestVars->{"fref"}); shift @frefVars; 
  
  VerifyUser();
  my $ext = $main::requestVars->{"EXTENSION"};

  my $reqType = shift @frefVars; 

  if (join("",@frefVars) ne "FolderList" and $main::Session->{'started'} eq "") {
    ### restart session -------------------------------- ///
    UserFoldersList($main::requestVars->{"UserName"}, 1) if 0;
  }
    
  if ( $reqType eq "FolderList" ) {
    my @FolderNames = map {my $t = substr($_, 3)} grep(/^fl(?:\%3a|:)/i, @frefVars); my $q;
    
    @frefVars = grep(!/^fl(?:\%3a|:)/i, @frefVars); unshift @frefVars, $FolderNames[-1];

    my ($FolderName, $ReportName, $JobReportId, $EXPRESSION) = @frefVars;
#    die "EXT: $ext FREF: ". $main::requestVars->{"fref"} . "frefvars: " . join('/', @frefVars) . "\n" if $ReportName;
        
    @{$main::requestVars}{qw(FolderName ReportName JobReportId EXPRESSION)} = @frefVars;

#    @{$main::requestVars}{qw(FolderName UserName)} =  ($FolderName, 'XREPORT');

    if ( $FolderName   ) {
      VerifyFolder($FolderName);

      FOR_XML(LEFT, 'FolderChildFolders', '', $main::requestVars) if $main::requestVars->{"eLeft"} eq "true";
      
    }
        
    if ($EXPRESSION eq "FILERANGES") {
      $q = "JobReportFileRanges";
    }
    elsif ($JobReportId ne "") {
      $q = "JobReportReports";
    }
    elsif ($ReportName ne "") {
      $q = "ReportNameJobReports"; 
    }
    elsif ($FolderName ne "") {
      my $activeBody = $main::requestVars->{"activeBody"};
      if ($activeBody eq "BodyOfListePerData") {
        $q = "FolderJobReportsOfDate";
      }
      elsif ($activeBody eq "BodyOfListeRecenti") {
        $q = "FolderActiveJobReports";
      }
      else {
        $q = "FolderReportNames"; 
      }
    }
    else {
      UserFoldersList($main::requestVars->{"UserName"}); 
      SetUserLocalSessionCookie($main::requestVars->{"UserName"}); 
      return;
    }
    FOR_XML(TEXT, $q, $ext, $main::requestVars) if $main::requestVars->{"eText"} eq "true";
  }
  else {
    die "INVALID REQUEST /".$main::requestVars->{"fref"}."/ Reqt: /".$reqType."/";
  }
}


sub BinaryWrite {
  my $fileName = shift; my $INPUT = new FileHandle; my ($rec, $recV, $lastLength);

  $INPUT->open("<$fileName"); binmode($INPUT);
  
  while(!$INPUT->eof()) {
    my $lrec = $INPUT->read($rec, 32768);
	if ( !$recV or $lastLength != $lrec ) {
      $recV = new Win32::OLE::Variant(VT_UI1, $rec);
	}
	else {
	  $recV->Put($rec);
	}
	$main::Response->BinaryWrite($recV); $lastLength = $lrec; 
  }
  
  $INPUT->close();
}


sub INIT_ALL {
  $main::XREPORT_HOME = $main::Application->{'XREPORT_HOME'}; 
  my $sessid = $main::Session->{"SessionId"}; 

  $main::Response->{'Buffer'} = 0;

  $workdir = $main::Application->{'cfg.workdir'} || "$ENV{TEMP}";
  $userlib = c::getValues('userlib') || "$ENV{TEMP}";
  $dbdateformat = uc($main::Application->{'cfg.db.dateformat'}) || "YMD";

  for (@TARGETs) {
    $xmlFile[$_] = "$workdir/UsersXML/$sessid.$_.xml";
    $OUTPUT[$_] = new FileHandle; $OUTPUT[$_]->open(">$xmlFile[$_]"); 
  }
  
  #CONNECT();

  $main::requestVars = getFormVars(); 
  debug2log("INIT_ALL - reqVars:".Dumper($main::requestVars)."\n");
  
  
  $main::requestVars->{"fref"} = "/FolderList" 
   if 
  $main::requestVars->{"fref"} eq "";
	
  VerifyUser();
  my $msxml = "MSXML2.DOMDocument.6.0";
  $xml = CreateObject($msxml);
  if ( !$xml ) {
    $msxml = "MSXML2.DOMDocument.4.0";
    $xml = CreateObject($msxml);
    unless ( $xml ) {    
      EXIT_ERROR("Impossibile caricare MSXML DOM - Contattare Help Desk", 0);
      TERMINATE_REQUEST();
    } 
   }
  $xml->{"async"} = 0;
  $xslt = CreateObject($msxml); $xslt->{"async"} = 0;
  if ( $msxml eq "MSXML2.DOMDocument.6.0" ) {
    $xml->{"validateOnParse"} = 0;
    $xml->setProperty("ProhibitDTD", 0);
    $xml->setProperty("AllowDocumentFunction", 1);
    $xml->setProperty("AllowXsltScript", 1);
    $xslt->{"validateOnParse"} = 0;
    $xslt->setProperty("ProhibitDTD", 0);
    $xslt->setProperty("AllowDocumentFunction", 1);
    $xslt->setProperty("AllowXsltScript", 1);
  }  
}

sub TERMINATE_ALL {
  debug2log("TERMINATE_ALL called by: ".join('::', (caller())[0,2])." - ".$_[0]);
  my $accept_encoding = $main::Request->ServerVariables->{'HTTP_ACCEPT_ENCODING'}->Item();
  
  my @html; my $htmlbuff;

  for (@TARGETs) {
    $OUTPUT[$_]->close(); next if $xsltFile[$_] eq '';
    copy($xmlFile[$_], $xmlFile[$_].'.save');
    $xml->load($xmlFile[$_]); 
    $xslt->load($xsltFile[$_]);
    $html[$_] = $xml->transformNode($xslt);
  }

  my $OUTPUT;

  $htmlbuff = 
   "<HTML>" .
    "<link type=\"text/css\" rel=\"stylesheet\" href=\"css/cereport.css\"/>" .
    "<script type=\"text/javascript\" src=\"js/tablesort.js\"></script>" .
    "<body>" .
	"<span id=\"LEFT\">$html[LEFT]</span>" .
	"<span id=\"FText\">$html[TEXT]</span>" .
    "</body>" .
	"<script> top.TreeExp.End() </script>" .
   "</HTML>"
  ;

  if ( 0 and $accept_encoding =~ /deflate/ ) {
  
    $OUTPUT->open(">$xmlFile[LEFT]"); binmode($OUTPUT);
    $OUTPUT->print(compress($htmlbuff)); 
	$OUTPUT->close();

    $main::Response->{'Content-Type'} = "text/html";
    $main::Response->AddHeader("Content-Encoding","deflate");
#    $main::Response->AddHeader("Content-Length", -s $xmlFile[LEFT]);

	BinaryWrite($xmlFile[LEFT]);
  }
  
  elsif ( 0 and $accept_encoding =~ /gzip/ ) {

    my $gz = gzopen("$xmlFile[LEFT]", "wb");
	$gz->gzwrite($htmlbuff);
    $gz->gzclose(); 

    $main::Response->{'Content-Type'} = "text/html";
    $main::Response->AddHeader("Content-Encoding", "gzip");
#    $main::Response->AddHeader("Content-Length", -s $xmlFile[LEFT]);

	BinaryWrite($xmlFile[LEFT]);
  }

  else {

    $main::Response->{'Content-Type'} = "text/html";
#    $main::Response->AddHeader("Content-Length", length($htmlbuff));

    $main::Response->Write($htmlbuff); 
  }
  
  TERMINATE_REQUEST();
}

sub RESTART_LOGON {
  if ( $main::Application->{'cfg.authenticate'} eq 'NTLM' ) {
    $main::Response->Clear();
    $main::Response->{'Buffer'} = 1;
    $main::Response->{'Status'} = "401 Unauthorized";
    $main::Response->AddHeader("WWW-Authenticate","NTLM");
  
    TERMINATE_REQUEST();
  }
  else {
    my ($msg, $restart) = @_; $restart = 0 if !$restart; 

    $main::Response->Write(
      "<DIV ID=\"EXEC\">\<script>top.INVALID_USERNAME('$msg', $restart)\</script></DIV>"
    ); 
  }
  
  TERMINATE_REQUEST();
}

sub RESTART_REQUEST {
  $main::Response->Write("
    <DIV ID=\"EXEC\">
      \<script> alert('restart_request'); //top.restart_tree_exp(); \</script>
    </DIV>
  ");

  TERMINATE_REQUEST();
}

1;