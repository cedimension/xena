<%@ LANGUAGE=PerlScript %>
<%
###-- vim: set syn=perl: --###

use lib("$main::Application->{XREPORT_HOME}/perllib");

use strict; 

use Compress::Zlib;
use File::Copy;
use FileHandle; use URI::Escape; 
use Win32;
use Win32::OLE; use Win32::OLE::Variant;

use XReport;
use XReport::DBUtil;

our $query_parameters = {

  UserFoldersTrees => [
    [qw(UserName)],
	"Folder", 
	[qw(
	  FolderName FolderDescr IsActive HasChilds HasReports
	)],
	"xsl/FolderChildFolders.xsl"
  ],

  FolderChildFolders => [
    [qw(FolderName UserName)],
	"Folder", 
	[qw(
	  FolderName FolderDescr IsActive HasChilds HasReports
	)],
	"xsl/FolderChildFolders.xsl"
  ],

  JobReportFileRanges => [
    [qw(FolderName ReportName JobReportId)],
	"FileRange", 
	[qw(
	  JobReportId FileId FileRangesVar FromValue ToValue TotPages
	)],
	"xsl/UserFolders.xsl"
  ],

  JobReportReports => [
    [qw(FolderName ReportName JobReportId UserName)],
	"Report", 
	[qw(
	  JobReportId ReportId FilterVar FilterValue TotPages
	)],
	"xsl/UserFolders.xsl"
  ],

  ReportNameJobReports => [ 
    [qw(FolderName ReportName UserName)],
    "JobReport", 
	[qw(
	  JobReportId ReportId XferStartTime FileRangesVar ExistTypeMap
	  UserRef UserTimeElab UserTimeRef
      JobName JobNumber TotReports TotPages isGRANTED
    )],
	"xsl/UserFolders.xsl"
  ],

  FolderReportNames => [
    [qw(FolderName UserName)],
	"ReportName", 
	[qw(
	  ReportName ReportDescr HasIndex isGRANTED
	)],
	"xsl/UserFolders.xsl"
  ],

  FolderActiveJobReports => [
    [qw(FolderName UserName UserTimeRef UserName)], 
    "JobReport", 
	[qw(
     JobName JobNumber UserTimeElab UserTimeRef UserRef
	 XferStartTime TotReports TotPages JobReportId 
	 ReportId ReportName ReportDescr 
    )],
	"xsl/UserJobReports.xsl"
  ],
  
  FolderJobReportsOfDate => [
    [qw(FolderName UserTimeRef UserName)],
    "JobReport",
    [qw( 
     JobReportId ReportId ReportName ReportDescr FileRangesVar ExistTypeMap
     JobName JobNumber UserTimeElab UserTimeRef UserRef
	 XferStartTime TotReports TotPages isGRANTED
    )],
	"xsl/UserJobReports.xsl"
  ],

  FolderJobReportsWithConds => [
    [qw(FolderName UserName DescrMask NameMask JobMask FromDate ToDate)],
	"JobReport",
    [qw(
     JobName JobNumber UserTimeElab UserTimeRef UserRef
	 XferStartTime TotReports TotPages JobReportId 
	 ReportId ReportName ReportDescr 
    )], 
	"xsl/UserJobReports.xsl"
  ],
  
};

my @routines = qw(logFunctions.pl showFunctions.pl authFunctions.pl); 
my $binpath = $main::Request->ServerVariables('APPL_PHYSICAL_PATH')->Item();
my $ca = $main::Application->{'cfg.authenticate'};
push @routines, "authFunctions_$ca.pl" if ( -f $binpath."authFunctions_$ca.pl");
for my $fname ( @routines ) {
	my $fpath = $binpath.$fname;
	require "$fpath";
}
debug2log("FromUsers.asp Initializing");

INIT_ALL();

if ( $main::requestVars->{"fref"} =~ /^(?:\/|\%2f)FolderList/i ) {
  debug2log("FromUsers.asp handling FolderList");
  UserFolderListRequest();
}
else {
  die "INVALID REQUEST /".$main::requestVars->{"fref"}."/";
}

debug2log("FromUsers.asp Terminating");
TERMINATE_ALL();
    
%>
