<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

###-- vim: set syn=perl: --###

use lib("$ENV{'XREPORT_HOME'}/perllib");

use strict vars; use bytes; 

#BEGIN {
  #require XReport::LoginWin32;
  #XReport::LoginWin32::RevertToSelf();  
#}

use Symbol;
use  Win32::OLE;
use  Win32::OLE::Variant;

use XReport::DBUtil;
use XReport::XMLUtil qw(SQL2XML HASH2XML);

our ($Server, $Application, $Session, $Request, $Response, $XREPORT_HOME, $UserName);

our ($Application, $Server, $Session, $Request, $Response);

my ($XREPORT_HOME, $INDEX_NAME, $activeBody, $dbr, $SESSIONID);

###todo: get elements and xsl program from external xml configuration

my $INDEXes = {
  RETE_ITALIA => {
    addressing_mode => 'PAGE', jobreport_info => 0, restrict => 'permit',
    index_vars => {
      MITT => 'T/14',
      DEST => 'T/14',
      DIVISA => 'T/3',
      IMPORTO => 'N+/16',
      DIREZIONE => 'T/1',
      TIPO => 'T/3',
      ORDINANTE => 'T/5',
	  BENEFICIARIO => 'T/5',
      DATAARRIVO => 'D+/10',
      CRO => 'T/11',
      CROORIG => 'T/11',
	},
    restrict_sub => sub {
      my $FilterList = shift;
      return "
        DIREZIONE = 'I' AND SUBSTRING(MITT,1,5) IN($FilterList)
        OR DIREZIONE = 'O' AND SUBSTRING(DEST,1,5) IN($FilterList)
      "; 
    },
	with_sub => sub { 
	  return 'WITH (INDEX(IX_tbl_IDX_RETE_ITALIA_DataArrivo))'; 
	},
	max_entries => 500, index_element => "Retemsg", xsl_script => "Retemsgs.xsl", FORMAT => 'DATA'
  },

  RETE_SWIFT => {
    addressing_mode => 'PAGE', jobreport_info => 0, restrict => 'permit',
    index_vars => {
      MITT => 'T/11',
      DEST => 'T/11',
      DIREZIONE => 'T/1',
      TIPO => 'T/3',
      TIPOLOGIA => 'LIKE+/TIPO',
      ORDINANTE => 'T/11',
	  BENEFICIARIO => 'T/11',
      DATAARRIVO => 'D+/10',
      RR => 'T/16',
      TRN => 'T/16',
      DIVISA => 'T/3',
      IMPORTO => 'N+/16',
	},
    restrict_sub => sub {
      my $FilterList = shift;
      return "
        DIREZIONE = 'I' AND SUBSTRING(MITT,1,8) IN($FilterList)
        OR DIREZIONE = 'O' AND SUBSTRING(DEST,1,8) IN($FilterList)
      "; 
    },
	with_sub => sub { 
	  return 'WITH (INDEX(IX_tbl_IDX_RETE_SWIFT_DataArrivo))'; 
	},
	max_entries => 500, index_element => "swiftmsg", xsl_script => "swiftmsgs.xsl", FORMAT => 'DATA'
  },

  THBONIFICI => {
    addressing_mode => 'LINE', jobreport_info => 1, restrict => 'permit',
    index_vars => {
      ABI_BENEFICIARIO => 'T/6',
      CAB_BENEFICIARIO => 'T/6',
      IMPORTO => 'N+/16',
      CRO => 'T/11',
      DIP_CARICO => 'T/6',
      DATA_CARICO => 'D+/10',
      DIP_AUTH => 'T/6',
      ORDINANTE => 'T/24',
      BENEFICIARIO => 'T/24',
	},
	with_sub => sub { 
	  return 'WITH (INDEX(IX_tbl_IDX_THBONIFICI_DATA_CARICO))'; 
	}, 
	max_entries => 500, index_element => "bonifico", xsl_script => "thbonifici.xsl", FORMAT => 'DATA'
  },

  GARANZIE => {
    addressing_mode => 'CLIPID', jobreport_info => 0, restrict => 'permit', db_name => 'XRCENTERA',
    index_vars => {
      GARANZIA => 'N/15',
      COGNOME => 'T/32',
      NOME => 'T/32',
      COD_FISCALE => 'T/16',
      COD_ISTITUTO => 'T/2' 
	},
	with_sub => sub { 
	}, 
	max_entries => 500, index_element => "garanzia", xsl_script => "garanzie.xsl", FORMAT => 'ex:garanzie'
  },

  DEPOSITI_PRIVATE => {
    addressing_mode => 'PAGE', jobreport_info => 0, restrict => 'permit', 
    index_vars => {
      ANAGRAFICA => 'T/64',
      DEPOSITO => 'N/15',
      DATA_RIF => 'D+/10',
	},
	with_sub => sub { 
	  return 'WITH (INDEX(IX_tbl_IDX_DEPOSITI_PRIVATE_DATA_RIF))'; 
	}, 
	max_entries => 500, index_element => "deposito", xsl_script => "depositi_private.xsl", FORMAT => 'PRINT'
  },

  CONTI_CORRENTI_PRIVATE => {
    addressing_mode => 'PAGE', jobreport_info => 0, restrict => 'permit', 
    index_vars => {
      ANAGRAFICA => 'T/64',
      PAESE => 'T/2',
      BANCA => 'N/13',
      SPORTELLO => 'N/14',
      CONTO => 'N/15',
      DATA_RIF => 'D+/10',
	},
	with_sub => sub { 
	  return 'WITH (INDEX(IX_tbl_IDX_CONTI_CORRENTI_PRIVATE_DATA_RIF))'; 
	}, 
	max_entries => 500, index_element => "ccorrente", xsl_script => "conti_correnti_private.xsl", FORMAT => 'PRINT'
  },

  KEY_BOLOGNA => {
    addressing_mode => 'PAGE', jobreport_info => 0, restrict => 'permit', 
    index_vars => {
      CONTO => 'N/15',
      DATA_RIF => 'D+/10',
	},
	with_sub => sub { 
	  return 'WITH (INDEX(IX_tbl_IDX_KEY_BOLOGNA_DATA_RIF))'; 
	}, 
	max_entries => 500, index_element => "key", xsl_script => "key_bologna.xsl", FORMAT => 'PRINT'
  },

  CC_CSE => {
    addressing_mode => 'PAGE', jobreport_info => 0, restrict => 'permit', 
    index_vars => {
      ABI => 'N/13',
      CDG => 'N/14',
      RAPPORTO => 'N/15',
      DATA_RIF => 'D+/10',
	},
	with_sub => sub { 
	  return 'WITH (INDEX(IX_tbl_IDX_CONTICORR_DATA_RIF))'; 
	}, 
	max_entries => 500, index_element => "ccorrente", xsl_script => "conticorr.xsl", FORMAT => 'PRINT'
  },
};

sub EvaluateAndRestrictions {
  my $INDEX_NAME = shift; my $INDEX = $INDEXes->{$INDEX_NAME}; my ($dbr, @FilterList);

  my $restrict=$INDEX->{'restrict'}; 
 
  $dbr = dbExecute("
    SELECT FilterValue from tbl_ProfilesIndexTables_LL1
    WHERE 
      IndexTableName = '$INDEX_NAME'
      AND ProfileName IN(
        SELECT ProfileName FROM tbl_UsersProfiles
        WHERE UserName = '$UserName'
      )
  ");

  return "" if ($dbr->eof() and $restrict eq 'permit');

  while(!$dbr->eof) {
    my $FilterValue = $dbr->GetFieldsValues('FilterValue');
    return "" if $FilterValue eq '';
    push @FilterList, "'$FilterValue'"; 
  }
  continue {
    $dbr->MoveNext();
  }

  return &{$INDEX->{'restrict_sub'}}(join(", ", @FilterList));
}

sub CreateObject {
  return $Server->CreateObject($_[0]);
}

sub respWrite {
  $Response->Write(@_);
}

sub ListsOfEntries {
  my $listMode = shift; my $INDEX = $INDEXes->{$INDEX_NAME}; my ($dbr, $sqlstmt); 
  
  my (
    $db_name,
    $index_vars, 
    $restrict,
    $with_sub, 
    $max_entries, 
    $index_element, 
    $xsl_script, 
    $jobreport_info
  ) = 
  @{$INDEX}{qw(
    db_name index_vars restrict with_sub max_entries index_element xsl_script jobreport_info
  )};

  $db_name = 'XRINDEX' if $db_name eq '';
  
  my @ListOfVars = keys(%$index_vars); my $ListOfConds = '';
  
  my ($ListOfConds, $and_restrictions, $with_conds) = ('', '', '');
  
  my @SimpleVars = grep($index_vars->{$_} =~ /\w\//, @ListOfVars);
  my @ComplexVars = grep($index_vars->{$_} !~ /\w\//, @ListOfVars);
  
  for my $var (@SimpleVars) {
    if ( (my $val = $Request->Form("$var")->Item()) ne "" ) {
	  my ($type, $maxlen) = $index_vars->{$var} =~ /([^\/])\/(\d+)/;
	  if ($type eq 'T') {
	    $val =~ s/ +$//g; $val .= "\%" if length($val) < $maxlen && $val !~ /\%$/;
	  }
      my $op = ($val !~ /[\%_]/) ? '=' : 'LIKE';
      $ListOfConds .= "AND $var $op '$val' ";
    }
  }
 
  for my $var (@ComplexVars) {
    if ( $index_vars->{$var} =~ /^LIKE\+/ ) {
      my ($like, $likevar) = $index_vars->{$var} =~ /([^\/]+).\/(\w+)/;
      my $Form = $Request->Form("$var"); my $orexp = '';
      for (1..$Request->Form("$var")->Count()) {
        if (my $likevalue = $Form->Item($_)) { 
          $orexp .= " OR " if $orexp ne '';
          $orexp .= " $likevar LIKE '$likevalue'" 
        }
      }
      $ListOfConds .= "AND ($orexp) " if $orexp ne '';
    }
    elsif ( my $valfm = $Request->Form("$var\.FM")->Item() ) {
      my $valto = $Request->Form("$var\.TO")->Item() || $valfm;
      $valfm =~ s/ +$//g; $valto =~ s/ +$//g;
      if ( $index_vars->{$var} !~ /^N/i ) {
        $ListOfConds .= "AND $var BETWEEN '$valfm' AND '$valto' ";
      }
      else {
	    $valfm =~ s/[^0-9.]//g; $valto =~ s/[^0-9.]//g;
        $ListOfConds .= "AND $var BETWEEN $valfm AND $valto ";
      }
    }
  }

  $and_restrictions = EvaluateAndRestrictions($INDEX_NAME) if $restrict ne '';
  $ListOfConds .= "AND ( $and_restrictions ) " 
   if 
  $and_restrictions ne "";
  $ListOfConds = substr($ListOfConds,4) if length($ListOfConds) > 0;

  $with_conds = &$with_sub($ListOfConds);
  
  if ( $jobreport_info ) {
    my $MAINDB = XReport::DBUtil->get_dbname();
    $sqlstmt = "
      SELECT TOP $max_entries i.*, j.JobReportName, j.XferStartTime, j.JobName, j.JobNumber 

      FROM
        tbl_IDX_$INDEX_NAME i $with_conds 
        INNER JOIN $MAINDB.dbo.tbl_JobReports j ON j.JobReportId=i.JobReportId

      WHERE $ListOfConds
    ";
  }
  else {
    $sqlstmt = "SELECT TOP $max_entries * FROM tbl_IDX_$INDEX_NAME $with_conds WHERE $ListOfConds";
  }
  
  my $dbc = XReport::DBUtil::->new( DBNAME => $db_name);

  my $dbr = $dbc->dbExecute($sqlstmt);

  my $xml = CreateObject("MSXML2.DOMDocument.4.0"); $xml->{"async"} = 0;
    $xml->loadXML( "<${index_element}List detail='0' max_entries=\"$max_entries\">\n". 
    SQL2XML($index_element, $dbr) ."</${index_element}List>")
  ;
  
  my $xslt = CreateObject("MSXML2.DOMDocument.4.0"); $xslt->{"async"} = 0;
  my $rc2 = $xslt->load("$XREPORT_HOME/userlib/xsl/$xsl_script"); 
  die $xml->XML();
  
  respWrite($xml->transformNode($xslt)); 
}

sub DetailsOfEntries {
  require Symbol; require XReport::EXTRACT; my ($objList, @PAGE_LIST, @LINE_LIST); 
  
  my ($query, $elist, $INPUT) = ('', '', Symbol::gensym());
  
  my $INDEX = $INDEXes->{$INDEX_NAME};

  my ($FORMAT, $FILTER_CODE) = @{$INDEX}{qw(FORMAT FILTER_CODE)};
  
  $query = XReport::EXTRACT->new();
  
  ### ------ if clip_id ------------------------------------------------------
  if ( $FORMAT eq 'ex:garanzie' ) {
    require XReport::APPLS::GARANZIE; require XReport::PDF::DOC;
    WriteBinary("c:/temp/cpp.pdf", "application/pdf"); return;

    my @clipList = split(/\s+/, $Request->Form("objList")->Item());
    my $fileName = $Server->MapPath('QUERY')."/EXTRACT/IX.$SESSIONID.pdf";

    my $doc = XReport::PDF::DOC->Create($fileName);
    my $gar = XReport::APPLS::GARANZIE->new(
      "10.254.15.25, 10.254.15.26", 
      'XRCENTERA'
    );

    for my $clip_id (@clipList) {
      $gar->add_clip_images_to_pdf($clip_id, $doc);
    }

    $doc->Close(); WriteBinary($fileName, "application/pdf"); unlink $fileName; return;
  }
  ### ------------------------------------------------------------------------


  $objList = $Request->Form("objList")->Item();
  $objList =~ s/^\D+//; $objList =~ s/\D+$//;


  ### ------ if FORMAT = PRINT -----------------------------------------------
  if ($FORMAT eq 'PRINT') {
    @PAGE_LIST = split(/\D+/, $objList);
    
    $elist = $query->ExtractPages(
      TO_DIR => $Server->MapPath('QUERY')."/EXTRACT",
      QUERY_TYPE => 'FROMPAGES', 
      PAGE_LIST => \@PAGE_LIST,
      FORMAT => "PRINT",
      REQUEST_ID => "IX.$SESSIONID"
    );
    my $fileName = $elist->[0]->[1];

    WriteBinary($fileName, "application/pdf"); unlink $fileName; return;
  }
  ### ------------------------------------------------------------------------


  ### ------ if FORMAT = DATA ------------------------------------------------

  if ( $INDEX->{'addressing_mode'} eq 'LINE' ) {
    @LINE_LIST = split(/\D+/, $objList);
    $elist = $query->ExtractLines(
      TO_DIR => $Server->MapPath('QUERY')."/EXTRACT",
      QUERY_TYPE => 'FROMLINES', 
      LINE_LIST => \@LINE_LIST,
      INDEX_NAME => "$INDEX_NAME",
      FORMAT => "$FORMAT",
      FILTER_CODE => $FILTER_CODE,
      REQUEST_ID => "IX.$SESSIONID"
    );
  }
  elsif ( $INDEX->{'addressing_mode'} eq 'PAGE' ) {
    @PAGE_LIST = split(/\D+/, $objList);
    $elist = $query->ExtractPages(
      TO_DIR => $Server->MapPath('QUERY')."/EXTRACT",
      QUERY_TYPE => 'FROMPAGES', 
      PAGE_LIST => \@PAGE_LIST,
      INDEX_NAME => "$INDEX_NAME",
      FORMAT => "$FORMAT",
      FILTER_CODE => $FILTER_CODE,
      REQUEST_ID => "IX.$SESSIONID"
    );
  }
  
  my ($index_element, $xsl_script, $parse_sub) = @{$INDEX}{qw(index_element xsl_script parse_sub)};

  my $xmlresp = "<${index_element}List detail='1'>\n";
  
  for (@$elist) {
    my ($JobReportId, $fileName)  = @$_; my ($msg, $hdr, $body, @hdr, %hdr);
	
    open($INPUT, "<$fileName") 
	 or 
	die("INPUT OPEN ERROR \"$fileName\" $!"); binmode($INPUT); 

	while(!eof($INPUT)) {
      my ($l, $rec); read($INPUT, $l, 2); $l = unpack("n", $l); 
      read($INPUT, $rec, $l); $rec = substr($rec,1);
      if ( $parse_sub ) {
        $xmlresp .= &$parse_sub($rec);
      }
      elsif ( $rec !~ /^<$index_element/ ) {
        $xmlresp .= "<$index_element><![CDATA[$rec]]></$index_element>\n" 
      }
      else {
        $xmlresp .= $rec . "\n";
      } 
    }
	
    close($INPUT); unlink $fileName;
  }
  $xmlresp .= "</${index_element}List>";

  my $xml = CreateObject("MSXML2.DOMDocument.4.0"); $xml->{"async"} = 0;
  my $rc1 = $xml->loadXML($xmlresp);
  
  my $xslt = CreateObject("MSXML2.DOMDocument.4.0"); $xslt->{"async"} = 0;
  my $rc2 = $xslt->load("$XREPORT_HOME/userlib/xsl/$xsl_script");
  
  respWrite($xml->transformNode($xslt));
}

sub WriteBinary {
  my ($fileName, $ContentType) = @_; my ($INPUT, $rec, $ll, $tl, $variant) = (gensym());

  $Response->{'ContentType'} = "$ContentType" if $ContentType;

  $Response->AddHeader("Content-Length", -s $fileName);
  #$Response->AddHeader("Cache-Control", "no-cache");

  open($INPUT, "<$fileName")
   or
  die("INPUT OPEN ERROR for \"$fileName\" rc=$!"); binmode($INPUT);

  while(1) {
    read($INPUT, $rec, 32768); last if $rec eq '';

	if ( !$variant or $ll != length($rec) ) {
      $variant = new Win32::OLE::Variant(VT_UI1, $rec);
	}
	else {
	  $variant->Put($rec);
	}
	$Response->BinaryWrite($variant); $tl += $ll = length($rec);
  }

  die "BYTE COUNT MISMATCH for FILE \"$fileName\"" if $tl != -s $fileName; return $tl;
}

sub Restart_HTLM {
  my $AUTH_USER = $Request->ServerVariables("AUTH_USER")->Item();
  $Response->Write("
   <html>
    <head>
     <META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\"/>
     <META HTTP-EQUIV=\"Expires\" CONTENT=\"-1\"/>
	 <style>
	  
	 </style>
    </head>
    <body>
	 <span id=\"CONTENT\">
      <table style=\"margin:10;\">
      <tr>
        <td>
         <h4 >UTENTE $AUTH_USER NON ABILITATO !!</h4></br>
        </td>
       </tr>
      </table
	 </span>
    </body>
	<"."script> top.TreeExp.End() </"."script>
   </html>
  ");
}

sub Restart_NTLM {
  $Response->Clear();
  $Response->{'Buffer'} = 1;
  $Response->{'Status'} = "401 Unauthorized";
  $Response->AddHeader("WWW-Authenticate","NTLM");
}

sub VerifyUser {
  $dbr = dbExecute("
    SELECT UserName FROM tbl_Users WHERE UserName = '$UserName'
  ");

  do {Restart_NTLM(); $Response->End(); exit;} if $dbr->eof(); $dbr->Close();
}

sub INIT_ALL {
  $UserName = $Request->ServerVariables("AUTH_USER")->Item(); 

  $UserName =~ s/^[^\\]+\\//;
  $SESSIONID = $Session->SessionID();

  $XREPORT_HOME = $ENV{'XREPORT_HOME'};

  $INDEX_NAME = $Request->Form("INDEX_NAME")->Item();
  $activeBody = $Request->Form("activeBody")->Item();

  $Response->{'Buffer'} = 0;

  #VerifyUser();
}

sub TERMINATE_ALL {
}

INIT_ALL();

if ( $activeBody eq "BodyOfLISTE" ) {
  ListsOfEntries();
}
elsif ( $activeBody eq "BodyOfDETTAGLI" ) {
  DetailsOfEntries();
}
else {
  die "INVALID activeBody value \"$activeBody\"";
}

TERMINATE_ALL();

</SCRIPT>
