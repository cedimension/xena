#!perl

package main;

use strict;
no warnings;

use FCGI (); # Imports the library; required line
use CGI qw(:standard :push);
#use CGI ();

use XReport (runmode => 'fastcgi');
use lib $main::Application->{ApplPath};
use XReport::SOAP::FCGI ('#'.$main::Application->{ApplName});

sub _SOAP_ERROR {
  my $args = { @_ };
  $args->{faultcode} = 'soap:Server' unless exists($args->{faultcode});
  $args->{faultstring} = 'Service Error' unless exists($args->{faultstring});
  my $respmsg = XReport::SOAP::FCGI::_build_soap_error($main::Application->{ApplName}, @_);	
	
  $main::Response->Status("500 Internal Server Error");
  $main::Response->AddHeader("Connection", "close");
  $main::Response->Write( $respmsg );
  return undef;
	
}

sub readContent {
  my ($reqident, $method) = @{$main::Request}{qw(reqid method)};
  my $buffsize = 32*1024;
  my ($RequestData, $TotalBytes) = ('', $main::Request->TotalBytes());
  if ( $TotalBytes ) {
    while($TotalBytes > 0) {
      my $bytes2read = ($TotalBytes > $buffsize ? $buffsize : $TotalBytes);
      $main::dotrace && i::traceit("${method} reading $bytes2read of $TotalBytes left - caller: ".join('::',(caller())[0,2]));
      my $buff = $main::Request->BinaryRead($bytes2read);
      $RequestData .= $buff if $buff;

      $TotalBytes -= $bytes2read;
    }
  }
  else {
    while (1) {
      my $buff = $main::Request->BinaryRead($buffsize);
      $RequestData .= $buff if $buff;
      last if !$buff || length($buff) < $buffsize;
    }
  }
  $main::dotrace && i::traceit("${method} returning ", length($RequestData), " to caller: ".join('::',(caller())[0,2]));
  return $RequestData;
}

sub returnFullDocument {
  my ($docid, $DocumentData) = (shift, shift);
  my ($JobReportId, $fileid) = split /\//, $docid, 2;
  $fileid = '#0' unless $fileid;
  
  my ($fhin, $fhout, $fherr) = @{$main::Request}{qw(fhin fhout fherr)};

  $DocumentData->{DocumentBody} = {content => 'BASE64ENCODEDGOESRIGHTHERE'};
  my ($msghdr, $msgtail) = (XReport::SOAP::buildResponse($DocumentData) =~ /^(.*)BASE64ENCODEDGOESRIGHTHERE(.*)$/s);
  $main::dolog && i::logit("returnFulldocument JROD: $JobReportId msghdr:\n$msghdr\nmsgtail:\n$msgtail\n-------") ;

  eval { require XReport::ARCHIVE::OUTPUT; }; 
  if ($@) {
    $main::dolog && i::logit("Error during require of $JobReportId - $@") ;
    return _SOAP_ERROR("Sender", "returnFullDocument/openError", "Error during require of $JobReportId - $@");
  }

  my $zip;
  eval {$zip = Open XReport::ARCHIVE::OUTPUT($JobReportId, wrapper => 1 ); };
  if ($@) {
    $main::dolog && i::logit("Error during open for $JobReportId - $@") ;
    return _SOAP_ERROR("Sender", "returnFullDocument/openError", "Error during open of $JobReportId - $@");
  }

  my ($JobReportName ) =  @{$zip}{qw(JobReportName)};
 
  my $document = $zip->FileContents("$JobReportName\.$JobReportId\.0\.$fileid\.pdf");
  my ($buffsize, $outbytes) = (57*71, 0);

  $main::dolog && i::logit("Size to be Encoded for $JobReportName $JobReportId: ", length($document)) ;
  $main::Request->Write($msghdr); # serializer
  while ($document) {
    (my $buffer, $document) = unpack("a$buffsize a*", $document);
    last unless length($buffer);
    $main::Request->Write(MIME::Base64::encode_base64($buffer));
    $outbytes += length($buffer);
    last unless  length($buffer) == $buffsize;
  }
  $main::Request->Write($msgtail);
  return $outbytes;
}

sub returnDocumentPages {
  my ($pagelist, $DocumentData) = (shift, shift);
  my ($fhin, $fhout, $fherr) = @{$main::Request}{qw(fhin fhout fherr)};
  $main::dolog && i::logit("returnDocumentPages pagelist:", XReport::SOAP::Dumper($pagelist), "documentdata\n", XReport::SOAP::Dumper($DocumentData)) ;

  my ($msghdr, $msgtail) = (XReport::SOAP::buildResponse($DocumentData) =~ /^(.*<\/IndexEntries>)(<\/RESPONSE>.*)$/s);

  require XReport::EXTRACT;
  require XReport::PDF::DOC;
  require MIME::Base64;

  my $query = XReport::EXTRACT->new();
  my $streamer = new XReport::SOAP::B64streamer();
  $query->{docTo} = XReport::PDF::DOC->Create($streamer) ;
  $main::Request->Write($msghdr . '<DocumentBody>');

  my $elist;
  eval { $elist = $query->ExtractPages(
                       QUERY_TYPE => 'FROMPAGES',
                       PAGE_LIST => \@$pagelist ,
                       FORMAT => "PRINT",
                       ENCRYPT => 'YES',
                       OPTIMIZE => 1,
                       REQUEST_ID => (caller())[0].$main::Session->{'SessionID'},
                       #                   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"}
                      );
    };

  if ( scalar(@$elist) == 0 ) {
    EXIT_SOAP_ERROR("Sender", "IdNotFound", "DOCUMENTI NON TROVATI IN ARCHIVIO per la chiave richiesta - $@");
    return 0;
  }
  
  $main::Request->Write('</DocumentBody>'.$msgtail);

  $main::dolog && i::logit("returnDocumentPages elist: ", scalar(@$elist), " InputDoc: ", $streamer->{InputBytes}, " B64Streamer: ", $streamer->{OutputBytes}) ;
  return $streamer->{OutputBytes};
  
}

sub _GET_TESTPAGE {
    $main::dolog && i::logit("Service ", $main::Request->REQUEST_ID(), "Serving TESTPAGE request") ;
     my $byteswritten = &{$main::Application->{samplehtml}}($main::Request, $main::Response, '') if $main::Application->{samplehtml};
     if ( !$main::Application->{samplehtml} ) {
        my $title = "FastCGI Demo Page (perl)";
     	my ( $servername, $remote ) = $main::Request->ServerVariables(qw(SERVER_NAME REMOTE_HOST));
        $main::Response->Status('200 OK');
        $main::Response->ContentType('text/html');
        $byteswritten = $main::Response->Write( CGI::start_html(-title => $title) 
              ,CGI::hr(),CGI::h1($title)
              ,CGI::hr(),CGI::blockquote("This is coming from a perl FastCGI SOAP server", CGI::br(), 
                         "Running on ", CGI::em($servername), " to ", CGI::em($remote), CGI::br(),
                         "This is connection number $main::Application->{reqcount}")
              ,CGI::hr()
              ,CGI::end_html()
              );
     } 
     return $byteswritten;
}

sub _UNKNOWN_REQUEST {
    my ( $reqmet, $servername, $remote ) = $main::Request->ServerVariables(qw(REQUEST_METHOD SERVER_NAME REMOTE_HOST));
    my ($serverurl, $querystr, $soapstr, $reststr) = @{$main::Request}{qw(serverurl querystr soapstr reststr)};
     my $infomsg = "Service  does not support request $serverurl by $main::Application->{perlclass}";
     $main::dolog && i::logit($infomsg) ;
     $main::Response->Status("402 $reqmet method not supported");
     $main::Response->ContentType('text/html');
     my $html = CGI::start_html(-title => 'Request Error') ;
     $html .= CGI::hr().CGI::h2('GET Method supported only for WSDL') if $reqmet =~ /^GET$/i;
     $html .= CGI::hr().CGI::h1("$reqmet REQUEST for $serverurl (".($querystr || $soapstr || $reststr || 'NO PARMS').") not supported")
            . CGI::hr().CGI::blockquote($infomsg)
            . CGI::hr().CGI::h1("Request Handler").CGI::hr().CGI::pre(XReport::SOAP::Dumper($main::Request)).CGI::br()
            . CGI::hr()
            . CGI::end_html();
    return $main::Response->Write( $html );
}

sub _SOAPError {
  $main::Response->Status($_[3] || '500 Internal Server Error');
  $main::Response->Write(XReport::SOAP::BUILD_SOAP_ERROR(@_));
  $main::dolog && i::logit("SOAP error: " . $_[2]) ;
}

sub _parseSOAPrequest { 
   my ($msub, $method, @parms) = ( @_ );
   i::traceit("_parseSOAPReq parms:", XReport::SOAP::Dumper(\@_)) if $main::dotrace;
   my $reqData = readContent();
   my $docdatastr = '';
   my ($ctype) = $main::Request->ServerVariables('CONTENT_TYPE');
   if ( $ctype =~ /multipart/i ) {
      my $contentInfo = { map { $_ =~ /\=/ ? (map { /^"?([^"]+)"?$/ ? $1 : $_;} split /\=/, $_, 2) : ($_ => (1 == 1)) } split /\; /, $ctype };
      my $bndry = qr/\-\-$contentInfo->{boundary}(?:\x0a|\-\-)/;
      my @msgparts = ();
      foreach my $part ( split /$bndry/, $reqData ) {
      	my ($header, $content) = split /\x0a\x0a/, $part, 2;
      	my @hinfo = ();
      	foreach my $hline ( split /\x0a/, $header ) {
      		my ($varname, $varvalue) = split /\: /, $hline;
#            $main::dolog && i::logit("Header ", scalar(@msgparts) + 1, " varname: $varname varval: $varvalue") ;
      		if ( $varvalue =~ /\; / ) {
               push @hinfo, ($varname, {map { $_ =~ /\=/ ? (map { /^"?([^"]+)"?$/ ? $1 : $_;} split /\=/, $_, 2) : ($_ => (1 == 1)) } split /\; /, $varvalue});
               
      		}
      		else { 
      		   push @hinfo, ($varname, $varvalue); 
      		}
      	}
        push @msgparts, {header => { @hinfo }, content => $content };
      }      	
#      $main::dolog && i::logit("Bndry: $bndry CINFO:", XReport::SOAP::Dumper($contentInfo), "msgParts: ", XReport::SOAP::Dumper(\@msgparts)) ;
      $docdatastr = ((grep {exists($_->{header}->{'Content-ID'}) && $_->{header}->{'Content-ID'} eq $contentInfo->{start} } @msgparts)[0])->{content};
   }
   else { 
#      $main::dolog && i::logit("NO Multipart data ctype: $ctype") ;
      $docdatastr = $reqData; 
   }
   if ( $docdatastr !~ /REQUEST.+IndexName/s ) {
      $main::dolog && i::logit("DocumentData: $docdatastr") ; 
      $main::dotrace && warn "DocumentData: $docdatastr", "\n";
      return main::_SOAP_ERROR(faultcode => '501', faultstring => "Invalid Format", detailmsg => "Invalid DocumentData");
   }
   i::traceit("_parseSOAPReq DATA: $docdatastr") if $main::dotrace;
   my $DocumentData;
   eval { $DocumentData = XReport::SOAP::parseSOAPreq($docdatastr); }; 
   if ( $@ ) {
      return main::_SOAP_ERROR(faultcode => 502, faultstring => "Parsing Error"
                              , detailmsg => "Request parser process abnormally ended", detaildesc => $@);      
   }
   i::traceit("SOAP parsesub default $method - parms: @parms - Content: ".XReport::SOAP::Dumper($DocumentData)) if $main::dotrace;
#   my $DocumentData = XReport::SOAP::parseSOAPreq($docdatastr)->{request}; 
   my $result;
   eval { $result = &$msub($DocumentData->{request}, $DocumentData->{soaphdrs}) };
   if ( $@ ) {
      return main::_SOAP_ERROR( faultcode => 502, faultstring => "Process Error"
                              , detailmsg => "Processing handler abnormally ended", detaildesc => $@);      
   }
}

sub _parseSOAPmultipart { 
   my ($msub, $method, @parms) = ( @_ );
   my $contentInfo = { map { $_ =~ /\=/ ? map { /^"?(.+)"?$/ ? $1 : $_;} split /\=/, $_, 2 : $_ => (1 == 1); } 
                           split /; /, $main::Request->ServerVariables('CONTENT_TYPE') };
   $main::dolog && i::logit("CINFO:", XReport::SOAP::Dumper($contentInfo)) ;
   my $fhin = $main::Request->{fhin};
   my $soap = 0; my $contentpart = 0;
   while ( <$fhin> ) {
   	  chomp;
   	  my $inrow = $_;
   	  if ( "\"$inrow\"" eq $contentInfo->{boundary} ) {
   	  	
   	  } 
   }
   my $DocumentData;
   eval { $DocumentData = XReport::SOAP::parseSOAPreq(readContent()) }; 
   if ( $@ ) {
      return main::_SOAP_ERROR(faultcode => 502, faultstring => "Parsing Error"
                              , detailmsg => "Request parser process abnormally ended", detaildesc => $@);      
   }
#   $main::dolog && i::logit("SOAP parsesub default $method - parms: @parms - Content: ".XReport::SOAP::Dumper($DocumentData)) ;
   my $result;
   eval { $result = &$msub($DocumentData) };
   if ( $@ ) {
      return main::_SOAP_ERROR(faultcode => 502, faultstring => "Process Error"
                              , detailmsg => "$method processing handler abnormally ended", detaildesc => $@);      
   }
}

sub _parseHTMLrequest { 
   my ($msub, $method, @parms) = ( @_ );
   $main::dolog && i::logit("HTML parsesub default $method - selection: @parms") ;
   my $result;
   eval { $result = &$msub({ request => { split(/[=&]/, readContent() ), @parms } }) };
   if ( $@ ) {
   	  $main::Response->Status("500 $method Error");
   	  $main::Response->Write(CGI::start_html(),CGI::pre($@),CGI::end_html());
   }
    
}

# Initialization code

#$main::FCGIreq = FCGI::Request();
#$main::Request  = new XReport::FCGI::Request($main::FCGIreq);
#i::logit("Out-Of-Loop Request initialized - mcast dest: $main::mcastdest_log (", ref($main::logsock), ")");

if ( my $initsub = $main::Application->{perlclass}->can('_ApplInit') ) {
    &$initsub($FCGI::global_request);    
}

# Request loop
i::logit("Begin accept loop - waiting for requests");
while ( XReport::SOAP::Request->Accept() ) {
  if ( !$main::Request->isFastCgi() ) {
#  	$main::dotrace = 1; $main::debug = 1; $main::veryverbose = 1; $main::verbose = 1; 
#  	warn "This is a batch request - ARGV: ", XReport::SOAP::Dumper(\@ARGV), "\n", " ENV: ", XReport::SOAP::Dumper(\%ENV), "\n";
#  	my $req = new CGI();
#  	warn "REQUEST: ", XReport::SOAP::Dumper($req), "\n";
#  	next;
  }  
  my ( $reqmet, $qrystr, $soapstr ) = $main::Request->ServerVariables(qw( REQUEST_METHOD QUERY_STRING HTTP_SOAPACTION ));
  my ($restparms, $SOAPparms, $qryparms, $method, $reststr) = @{$main::Request}{qw(restparms soapparms qryparms method reststr )};

  my $byteswritten = 0;
  my $msub = main->can('_'.uc(${reqmet}).'_'.uc($qrystr));
  my $parsesub = $main::Application->{parsesub};
  my $parseparms = [];

  if ( $msub ) {
     $parsesub = sub { my $msub = shift; return &$msub(@_); } unless $parsesub;
  }
  elsif ( $reqmet =~ /^POST$/i && $soapstr && ($msub = $main::Application->{perlclass}->can($SOAPparms->[0])) ) {
     $main::dolog && i::logit( "Service Calling $method of $main::Application->{perlclass}" ) ;
     $parsesub = \&_parseSOAPrequest unless $parsesub;
     $parseparms = $SOAPparms;
  }
  elsif ( $reqmet =~ /^GET$/i && $reststr && ($msub = $main::Application->{perlclass}->can("_REST_$restparms->[0]")) ) {
     $main::dolog && i::logit( "Service Calling _REST_$restparms->[0] of $main::Application->{perlclass}") ;
     $parsesub = \&_parseHTMLrequest unless $parsesub;
     $parseparms = $restparms;
  }
  elsif ( $reqmet =~ /^GET$/i && $qrystr && ($msub = $main::Application->{perlclass}->can("_GET_$qryparms->[0]")) ) {
     $main::dolog && i::logit( "Service Calling _GET_$qryparms->[0] of $main::Application->{perlclass}") ;
     $parsesub = \&_parseHTMLrequest unless $parsesub;
     $parseparms = $qryparms;
  }
  else {
     $msub = \&_UNKNOWN_REQUEST;
     $msub = \&_GET_SHOWSERVER if $main::veryverbose;
     if ( $main::veryverbose && (my $testsub = $main::Application->{perlclass}->can('_testDocList')) ) {
     	$msub = $testsub;
     }
     $parsesub = sub { my $msub = shift; return &$msub(@_); };
  }
  
  eval { $byteswritten = &$parsesub($msub, @{$parseparms} ); };
  $main::dolog && $@ && i::logit("Handler routine returned messages:\n", $@);
  
  $main::Request = undef;
}

1;

__END__
#------------------------------------------------------------
package XReport::FCGI;

use strict;

use FCGI; use IO::Handle;

use XReport; 

sub i::logit {
}

my $INPUT = new IO::Handle; my $OUTPUT = new IO::Handle; my $ERROR = new IO::Handle; my %env = ();

my $request = FCGI::Request($INPUT, $OUTPUT, $ERROR, \%env); 
#my $request = FCGI::Request(); 

my $aspi =  XReport::FCGI::ASPServer->new($INPUT, $OUTPUT, $ERROR, \%env, $request); 

while($request->Accept() >= 0) {
  eval {
    $aspi->handle_request();
  };
  if ($@) {
#    print $OUTPUT "HTTP/1.1 500 Internal Server Error\r\n\r\n$@";
    print "HTTP/1.1 500 Internal Server Error\r\n\r\n$@";
  }
}

1;

__END__

use FCGI;
use CGI qw(:standard);

my $cnt = 0;

my $req = FCGI::Request();
while ($req->Accept() >= 0) {   
    $cnt++;
    my ($fhin, $fhou, $fher) = $req->GetHandles();
    print $fhou header(-type=>'text/html').start_html().h1('Counter').hr().h3($cnt).hr().end_html();
}
__END__

#Request.ClientCertificate Collection
#The values of fields stored in the client certificate that is sent in the HTTP request.
#Request.Cookies Collection
#The values of cookies sent in the HTTP request.
#Request.Form Collection
#The values of form elements in the HTTP request body.
#Request.QueryString Collection
#The values of variables in the HTTP query string.
#Request.ServerVariables Collection
#The values of predetermined environment variables.
#Request.TotalBytes
# 
#Response.Buffer
#Indicates whether page output is buffered.
#Response.CacheControl
#Sets a header to notify proxy servers or other cache mechanisms whether they can cache the output generated by ASP.
#Response.Charset
#Appends the name of the character set to the content-type header. The character set specifies for the browser how to display characters.
#Response.CodePage
#Sets the code page for data in the intrinsic objects for one response. The code page specifies for the server how to encode characters for different languages.
#Response.ContentType
#Specifies the HTTP content type for the response.
#Response.Cookies Collection
#Specifies cookie values. Using this collection, you can set cookie values.
#Response.Expires
#Specifies the length of time before a page cached on a browser expires.
#Response.ExpiresAbsolute
#Specifies the date and time on which a page cached on a browser expires.
#Response.IsClientConnected
#Indicates whether the client has reset the connection to the server.
#Response.LCID
#Sets the LCID for data for one response. The LCID refers to how dates, times, and currency are formatted for a specific geographical locale.
#Response.PICS
#Set the value for the pics-label response header to indicate the PICS content rating.
#Response.Status
