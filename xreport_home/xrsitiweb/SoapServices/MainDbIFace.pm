package XReport::DocumentsIFace;

use strict;

require Data::Dumper;
require XReport::SOAP;
use CGI;

=begin :wsdldoc

=method getDBRows

=doc  IndexName: { tablename | #procname }
    an entry in XreportDB.tbl_IndexTables must be present for "Indexid"
    if #indexid.procname an entry in config.sqlprocs must be present for "procname"
    
    Additional doc to come
    
    May be in an other time 

=end :wsdldoc

=cut

sub getDBRows {
  my $selection = shift;
  i::logit("getDocList input:", _Dumper($selection) );# if $main::veryverbose;
  warn "getDocList input:", _Dumper($selection) if $main::veryverbose;

  my ($method, $reqident) = @{$main::Request}{qw(method reqid)};
  my $dfltTOP = $XReport::cfg->{defaulttop} || 100;

  my ($tblid, $orderby, $IndexEntries) = @{$selection}{qw(IndexName ORDERBY IndexEntries)};
  my $select = 'SELECT ';
  $select .= ( !exists($selection->{TOP}) ? 'TOP $dfltTOP' : $selection->{TOP} ne "-1" ? "TOP $selection->{TOP}" : '' );
  
  my ($dbh, $tblname);
  $dbh = XReport::DBUtil->new('XREPORT');
 
  if ( $tblid =~ /^#/ ) {
     my $sqlid = substr($tblid, 1);
     my $sql = $XReport::cfg->{sqlprocs}->{$sqlid};
     return _SOAP_ERROR( faultcode => 503, faultstring => "Client Request Error"
                             , detailmsg => "sql $sqlid not found"
                             , detaildesc => "$sqlid not in cfg list (". join(', ', keys %{$XReport::cfg->{sqlprocs}}) ) unless $sql;
     $select .= '('.$sql.')';
  }
  else {
     $select .= "tbl_".$tblid;
  } 
  
  $select .= 'WHERE (' . join(') OR (', XReport::SOAP::Server::buildClause(@{$selection}{qw(IndexName IndexEntries)}, '')).')'
                                                if (exists($selection->{IndexEntries}) && scalar(@{$selection->{IndexEntries}}));
  $select .= ' ORDER BY '.$orderby if $orderby;
  i::logit("${method} SELECT:", $select);
  warn "${method} SELECT:", $select if $main::veryverbose;
   
   my $where = 'WHERE (' . join(') OR (', XReport::SOAP::Server::buildClause(@{$selection}{qw(IndexName IndexEntries)}, '')).')';
   i::logit( "getDocList: ", _Dumper($selection), "Clause: $where");

   eval { $IndexEntries = $dbh->getTableRows($select)};
   return _SOAP_ERROR(faultcode => 503, faultstring => "Data Base Error", detailmsg => "getTableRows error", detaildesc => $@) if $@;

   my $DocumentData = { IndexName => $tblid,
                        IndexEntries => $dbh->getTableRows($select),
                        NewEntries => [],
                        DocumentBody => ''
                      };
           
    return $main::Response->Write( XReport::SOAP::buildResponse($DocumentData) );                    
}

=begin :wsdldoc 

=method getDBRows2

=doc  IndexName: { tablename | #procname }
    an entry in XreportDB.tbl_IndexTables must be present for "Indexid"
    se #indexid.procname an entry in config.sqlprocs must be present for "procname"

=end :wsdldoc

=cut

__PACKAGE__;
