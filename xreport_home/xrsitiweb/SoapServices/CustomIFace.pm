package XReport::CustomIFace;

use strict;

require Data::Dumper;
require XReport::SOAP;
require XReport::JobREPORT;
require XReport::ARCHIVE::INPUT;
require XReport::ARCHIVE::JobExtractor;
use CGI qw(:none:);

sub _ApplInit {
    my $FCGIenv = $_[0];
	#die "PARMS: ", Dumper($main::Request);
	if ( !$main::request->{serverurl} && (my ($us) = grep /--usersvc=/, @ARGV)) {
	   ($main::request->{serverurl} = "/$us/") =~ s/--usersvc=//;
    }	   
	my $user_methods = $main::Application->{XREPORT_SITECONF}.'/userlib/SOAPServices';
	($user_methods .= $main::request->{serverurl}) =~ s/\/$/_methods.pl/;
	if ( -e $user_methods && -f $user_methods ) {
	   eval { require $user_methods ; };
	   die "error loading $user_methods - $@" if $@;
	   no strict qw(refs subs);
	  
       my $methods = [ grep { $_ !~ /^[\(_]/ && XReport::CustomIFace->can($_) }
                                                    keys %{'XReport::CustomIFace::'} ];
       use strict qw(refs subs);
	   die "$user_methods loaded" . _Dumper($methods) 
	                              . "\nWSDL: " . $XReport::SOAP::FCGI::WSDLHDR;
	}
	else {
	  die "$user_methods not found";
    }	  
}

sub _buildListOfPages {
	my $reqdata = shift;
    my ($tblid, $orderby, $IndexEntries) = @{$reqdata}{qw(IndexName ORDERBY IndexEntries)};
	my $res = { LOP => [], RES => [], HNDLR => {} };
    my @colrequested = qw(JobReportId FromPage ForPages); 
    my @colretrieved = qw(JobReportName LocalFileName XferStartTime UserTimeRef);
    
    my $lastjr = {};
    if ( exists($reqdata->{JobReportId}) ) { 
        my ($JobReportId, $FromPage, $ForPages ) = @{$reqdata}{@colrequested};
        $res->{HNDLR}->{$JobReportId} = [ XReport::JobREPORT->Open($JobReportId) ];
        @{$lastjr}{@colrequested} = @{$reqdata}{@colrequested};
        @{$lastjr}{@colretrieved} = $res->{HNDLR}->{$JobReportId}->[0]->getValues(@colretrieved);
        if ( $FromPage && $ForPages && $lastjr->{JobReportName} ) {
#           push @{$res->{RES}}, $lastjr;
           push @{$res->{LOP}}, ($JobReportId, $FromPage, $ForPages );
        }
    }
    
#    foreach my $IndexEntry ( @{$IndexEntries} ) {
#       my $reqcols = ( exists($IndexEntry->{Columns}) ? $IndexEntry->{Columns} : $IndexEntry );
    foreach my $reqcols ( @{$IndexEntries} ) {
         my ($JobReportId, $FromPage, $ForPages ) = @{$reqcols}{@colrequested};
         next unless ( $FromPage && $ForPages );
         my $cols = {};
         @{$cols}{@colrequested} = @{$reqcols}{@colrequested};
         if ( !$JobReportId ) {
            next unless ( $lastjr->{JobReportId} );
            @{$cols}{'JobReportId', @colretrieved} = @{$lastjr}{'JobReportId', @colretrieved};
         }
         else {
            $res->{HNDLR}->{$JobReportId} = [ XReport::JobREPORT->Open($JobReportId) ]
                                                               if ( !exists($res->{HNDLR}->{$JobReportId}) );
            @{$cols}{@colretrieved} = $res->{HNDLR}->{$JobReportId}->[0]->getValues(@colretrieved);
            @{$lastjr}{'JobReportId', @colretrieved} = @{$cols}{'JobReportId', @colretrieved};
         }      
         push @{$res->{LOP}}, @{$cols}{@colrequested};
         push @{$res->{RES}}, { Columns => $cols };
    } 
    
    push @{$res->{RES}}, { Columns => [ map { { colname => $_, content => $lastjr->{$_} } } (@colrequested, @colretrieved) ] } 
                                                                                                   unless scalar(@{$res->{RES}});
    return $res;
}

sub _QueryXRDB {
	
  my $reqdata = shift;
  my ($method, $reqident) = @{$main::Request}{qw(method reqid)};
  
  my $dfltTOP = $XReport::cfg->{defaulttop} || 100;

  my ($tblid, $orderby, $IndexEntries) = @{$reqdata}{qw(IndexName ORDERBY IndexEntries)};
  my $select = 'SELECT ';
  $select .= ( !exists($reqdata->{TOP}) ? "TOP $dfltTOP" 
             : $reqdata->{TOP} ne "-1" ? "TOP $reqdata->{TOP}" : '' );
  $select .= ' * FROM ';
  if ( $tblid =~ /^#/ ) {
     my $sqlid = substr($tblid, 1);
     my $sql = $XReport::cfg->{sqlprocs}->{$sqlid};
     return _SOAP_ERROR( faultcode => 500, faultstring => "Client Request Error"
                             , detailmsg => "sql $sqlid not found"
                             , detaildesc => "$sqlid not in cfg list (". join(', ', keys %{$XReport::cfg->{sqlprocs}}) ) unless $sql;
     $select .= '('.$sql.') ';
  }
  
  my ($dbh, $tblname);
  if ( $tblid =~ /^_/ ) {
     $dbh = getDBHandle('XREPORT');
     $select .= "tbl".$tblid;
  }
  else {
     $dbh = XReport::DBUtil->get_ix_dbc($tblid);
     $select .= 'tbl_idx_'.$tblid;
  }
  
  my $where = '';
  $where = ' WHERE (' . join(') OR (', XReport::SOAP::buildClause(@{$reqdata}{qw(IndexName IndexEntries)}, '')).')'
                                                if (exists($reqdata->{IndexEntries}) && scalar(@{$reqdata->{IndexEntries}}));
  i::traceit( "getDocList: ", _Dumper($reqdata), "Clause: $where") if $main::dotrace;
  $select .= $where;
  
  $select .= ' ORDER BY '.$orderby if $orderby;

  i::traceit("${method} SELECT:", $select) if $main::dotrace;

   my $dbr; eval { $dbr = $dbh->dbExecute($select)};
   return _SOAP_ERROR(faultcode => 500
                      , faultstring => "Data Base Error"
                      , detailmsg => "_QueryXRDB error invoked by ".join('::', (caller())[0,2])
                      , detaildesc => $@. "Request: "._Dumper($reqdata)) if $@ || !$dbr;

   return _SOAP_ERROR(faultcode => 500
                      , faultstring => "No entries found"
                      , detailmsg => "no data from DB query invoked by ".join('::', (caller())[0,2])
                      , detaildesc => "Request: "._Dumper($reqdata). "Select: $select") 
                      if $dbr->eof();
   
   my $DocumentData = { IndexName => $tblid
#                     , sqlselect => $select
                      , IndexEntries => XReport::SOAP::fillIndexEntries($dbr)
                      , NewEntries => []
                      , DocumentBody => ''
                      };
                      
   return $DocumentData;

}

__PACKAGE__;
