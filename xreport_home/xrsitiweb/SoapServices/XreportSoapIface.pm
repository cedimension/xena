package XReport::XReportWebIface;

use strict;

use strict;

require Data::Dumper;
require XReport::SOAP;
require XReport::JobREPORT;
require XReport::ARCHIVE::INPUT;
require XReport::ARCHIVE::JobExtractor;
use CGI qw(:none:);

sub _verifyRACFPassword {
  my ($UserName, $Password, $result, $newUserName) = (shift, shift, "FAILED", undef);
  my $racfhost = $main::Application->{'cfg.racfhost'};
  return "ERROR: RACF host name not defined" unless $racfhost;
 
  main::debug2log("Try login to $racfhost");
  my $ftp = Net::FTP->new($racfhost, Debug => 0);
  if ( !$ftp ) {
    $result = "ERROR: connection to RACF host failed, retry later";
  }
  else {
    my $ftprc = $ftp->login($UserName, $Password);
    $newUserName = ($ftprc ? (split(/\s/, $ftp->message()))[0] : undef);
    $result = ($newUserName ? "SUCCESS" : "ERROR: Login to RACF failed for $UserName - ".$ftp->message());
    $ftp->quit();
  }
  main::debug2log("LogonRACFUser - User:$newUserName; RACFUser:$UserName");
  
  return $result;
}

sub _verifyUser {
    my ($userName, $password, $applName) = (shift, shift, shift);

    my $userSelect = 
            "SELECT a.UserName as UserName, b.ProfileName as ProfileName, c.UserAlias as UserId".
            " FROM tbl_Users as a INNER JOIN tbl_UsersProfiles as b ON a.UserName = b.UserName".
            " INNER JOIN tbl_UserAliases AS c ON a.UserName = c.UserName".
        " WHERE (b.ProfileName IN ('SUPER', '".uc($applName)."')".
        " AND a.UserName = '".uc($userName)."')".
        " OR (b.ProfileName IN ('SUPER', '".uc($applName)."')".
        " AND c.UserAlias = '".uc($userName)."')"
    ;
    main::debug2log("$userSelect");
    my $dbr = XReport::DBUtil::dbExecuteReadOnly($userSelect);
    main::debug2log("SELECT END");
    return "ERROR: User $userName hasn't access to this XReport application" if($dbr->eof() or !$dbr);
    return "SUCCESS";
}

sub getUserConfig {
    my ($method, $reqident, $selection) = (shift, shift, shift);

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    #main::debug2log(Dumper($IndexEntries));
    my $userName = $IndexEntries->[0]->{'Columns'}->{'UserName'}->{'content'};
    my $password = $IndexEntries->[0]->{'Columns'}->{'Password'}->{'content'};
    my $authMethod = $IndexEntries->[0]->{'Columns'}->{'AuthMethod'}->{'content'};
    my $applName = $IndexEntries->[0]->{'Columns'}->{'ApplName'}->{'content'};
    my $actionMessage = "FAILED";
    my $cfgFile = "n_o_t__f_o_u_n_d";
    my $cfgContent = "";
    main::debug2log("$method - Set UserName to $userName");
    main::debug2log("$method - Set Password to $password");
    main::debug2log("$method - Set Authentication method to $authMethod");
    main::debug2log("$method - Application name is $applName");

    $actionMessage = _verifyUser($userName, $password, $applName);
    main::debug2log("$method - _verifyUser return $actionMessage");
    if($authMethod =~ /RACF/ and $actionMessage =~ /SUCCESS/) {
        $actionMessage = _verifyRACFPassword($userName, $password);
        main::debug2log("$method - _verifyRACFPassword return $actionMessage");
    }

    if($actionMessage =~ /SUCCESS/) {
        $cfgFile = $main::Application->{XREPORT_SITECONF}."/".$userName."/xml/$applName.xml"  unless -e $cfgFile;
        $cfgFile = $main::Application->{XREPORT_SITECONF}."/".$main::Application->{ApplName}."/xml/$applName.xml"  unless -e $cfgFile;
        $cfgFile = $main::Application->{XREPORT_SITECONF}."/xml/$applName.xml" unless -e $cfgFile;
        main::debug2log("$method - Config file name is $cfgFile");

        my $cfgHandle = gensym();
        open($cfgHandle, "<$cfgFile") or die("Cannot open $cfgFile: $!");
        while(<$cfgHandle>) {
            $cfgContent .= $_;
        }
        close($cfgHandle);
        main::debug2log("$method - Config content is $cfgContent");

        my $buffsz = 57*76;
        $cfgContent = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $cfgContent ) );
    }

    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => [
            {
                Columns => [
                    {colname => 'UserName', content => $userName},
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ],
        DocumentType => 'text/xml',
        FileName => "$applName.xml",
        Identity => $applName,
        DocumentBody => {content => "<![CDATA[$cfgContent]]>"},
    }));
}

sub getHistoryReportsData {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    
    my $actionMessage = "FAILED";
    my $whereClause = "";
    my $user = "";my $currentRow = 0;
    my $type ="";
    my ($IndexEntries, $IndexName, $TOP, $ORDERBY, $DISTINCT) = @{$selection->{request}}{qw(IndexEntries IndexName TOP ORDERBY DISTINCT)};
    $TOP = 1000 if($TOP =~ /^$/);
    main::debug2log(Dumper($selection->{request}));
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;
        if($field_name =~ /^User$/){
            $user = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
            next;
        }
        if($field_name =~ /^Ambiente$/){
            $type = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
            next;
        }    
        if($field_name =~ /^CurrentRow$/){
            $currentRow = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
            next;
        }    
        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/
                    and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Min'} =~ /^$/
                and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Max'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        #$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date1$/) ? " Convert (varchar ,[$field_name] ,112) >=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
        #   if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
        #$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date2$/) ? " Convert (varchar ,[$field_name] ,112) <=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
        #   if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            
        $whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) >= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} , "'", "AND")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} !~ /^\s*$/);
        $whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) <= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} , "'", "AND")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} !~ /^\s*$/);  
        main::debug2log("ECCO".Dumper($whereClause));
        
        $whereClause = _addWhereClause($whereClause, "$field LIKE ",$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'", "AND") 
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} !~ /^date$/);
        } 
    my $sql2 ="";
            
    if(!(_checkRoot($user) != undef)){
        main::debug2log("USER_IS_NOT_ROOT");
            
        $sql2 = " inner join   tbl_profilesfolderstrees pft join tbl_usersprofiles up inner join tbl_useraliases ua on up.username = ua.username ".
               " on pft.profilename = up.profilename and rootnode <> 'root' and ua.useralias like '$user' on   B.user_name = pft.ProfileName ";   
    }
     my $sql = "select top $TOP ".
                "Restored, ReportEntryId, ReportName, Recipient, JobName, TimeRef, UserTimeElab, JobNumber, TotReports, DDName, UserTimeRef, TotPages ".
                ", totpages2, firstRBA, lastRBA, restore_id,".
                "( CASE  WHEN (MIN(Indexed) = MAX(Indexed)) THEN MAX(Indexed)  ELSE (MAX(Indexed)+';'+MIN(Indexed)) END ) as Indexed ".
                "from ( ".
                "select '' as Indexed, case when D.restore_id IS NULL  then 0 else 1 end as Restored ,  A.report_entry_id as ReportEntryId ".
                ",B.report_ext_name as ReportName  ".
                #",' ' as FolderDescr  ".
                ",B.user_name as Recipient ".
                ",C.job_name as JobName ".
                ",cidam_date_time as TimeRef ".
                ",cidam_date_time as UserTimeElab ".
                #",cidam_date_time as XFerStartTime ".
                #",cidam_file_name ".
                ",job_number as JobNumber ".
                ", '1' as TotReports  ".
                #",job_step_name ".
                #",proc_step_name ".
                #",step ".
                ",ddname as DDName".
                ",C.date_time as UserTimeRef ".
                #",C.tot_pages as Pagine ".
                #",C.tot_lines as Linee ".
                ", B.tot_pages as   TotPages ".
                ", C.tot_pages       AS totpages2 ".
                ", C.first_rba       AS firstRBA ".
                ", C.last_rba        AS lastRBA ".
                ", D.restore_id      AS restore_id ".                
                "from [ctd2xreport].[dbo].[tbl_ctd_report_ranges_$type] A INNER JOIN [ctd2xreport].[dbo].[tbl_ctd_cidam_entries_$type] C ON  A.cidam_id = C.cidam_id  ".
                "INNER JOIN [ctd2xreport].[dbo].[tbl_ctd_report_entries_$type] B ON B.report_entry_id = A.report_entry_id ".
                " LEFT OUTER JOIN [ctd2xreport].[dbo].[tbl_ctd_restore_entries_$type] D on D.file_id = C.cidam_id and  D.file_type = 'cidam'".
                $sql2 .
                "union all ".
                "select case when E.index_name IS NOT NULL  then lower(E.index_name+'-'+E.key_fields) end as Indexed, case when D.restore_id IS NULL  then 0 else 1 end as Restored , 0 as ReportEntryId, E.report_name as ReportName, '' as Recipient, C.job_name as JobName, C.cidam_date_time as TimeRef ,cidam_date_time as UserTimeElab , C.job_number as JobNumber, '1' as TotReports, ddname as DDName,C.date_time as UserTimeRef , E.tot_pages as TotPages, ".
                " E.tot_pages       AS TotPages2 , E.first_rba AS firstRBA, E.last_rba        AS lastRBA, D.restore_id      AS restore_id ".
                "from [ctd2xreport].[dbo].[tbl_ctd_cidam_entries_$type] C JOIN [ctd2xreport].[dbo].[tbl_ctd_index_entries_$type] E  ".
                "on C.cidam_id = E.index_id  ".
                " LEFT OUTER JOIN [ctd2xreport].[dbo].[tbl_ctd_restore_entries_A1] D on D.file_id = C.cidam_id and D.file_type = 'index' ".
                ") TAB ".
                $whereClause .
                "group by Restored, ReportEntryId, ReportName, Recipient, JobName, TimeRef, UserTimeElab, JobNumber, TotReports, DDName, UserTimeRef, TotPages, totpages2, firstRBA,lastRBA, restore_id ".
                "order by TAB.UserTimeRef desc ";

    main::debug2log("ECCO_QUERY".Dumper($sql));   
    my $result = _getDataFromSQL($sql);

    if(!$result) {
        $actionMessage = "No data found!";
    } else {
        $actionMessage = "SUCCESS";
    }            
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    main::debug2log("----------------------------------------------------------".Dumper(@{$result}[1]));
    push @{$respEntries}, @{$result} if($result);
#   my $h = gensym();
#   open($h, ">D:/out.log") or die("open error: $!");
#   print $h XReport::SOAP::buildResponse({
#       IndexName => 'XREPORTWEB',
#       IndexEntries => $respEntries
#   });
#   close($h);

    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));

}

sub getReportsDataOld {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";
    my $user = "";
    my $count = 0;
    my ($IndexEntries, $IndexName, $TOP, $ORDERBY, $DISTINCT) = @{$selection->{request}}{qw(IndexEntries IndexName TOP ORDERBY DISTINCT)};
    $TOP = 3000 if($TOP =~ /^$/);
    main::debug2log(Dumper($selection->{request}));
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;
        if($field_name =~ /^User$/){
            $user = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
            next;
        }   
        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/
                    and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Min'} =~ /^$/
                and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Max'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        #$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date1$/) ? " Convert (varchar ,[$field_name] ,112) >=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
        #   if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
        #$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date2$/) ? " Convert (varchar ,[$field_name] ,112) <=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
        #   if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            
        $whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) >= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} , "'", "AND")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} !~ /^\s*$/);
        $whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) <= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} , "'", "AND")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} !~ /^\s*$/);  
        main::debug2log("ECCO".Dumper($whereClause));
        if ($field =~ /^RepName$/)
        {
              if ($whereClause =~ /^\s*$/)
              {
                $whereClause = " where ";
              }
              else
              {
                 $whereClause .= " AND ";
              }
                $whereClause .= "(LR.UserRef like '".$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}."' OR JR.UserRef like '".$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}."')";
        }    
        else 
        {
              $whereClause = _addWhereClause($whereClause, "$field LIKE ",$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'", "AND") 
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} !~ /^date$/);
        } 
    }   
        

    my $sql = "BEGIN WITH Reports AS(".
        "select folderName, FolderDescr AS FolderDescr, JR.JobReportId as JobReport_ID ".
        "  ,MIN(LR.ReportId) As ReportId".
        " ,JR.XferStartTime As TimeRef".
        " ,COALESCE(LR.UserTimeRef, JR.UserTimeRef, JR.XferStartTime) As UserTimeRef".
        " ,COALESCE(JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime) As UserTimeElab".
        #"  ,CONVERT(varchar, JR.XferStartTime, 120) As TimeRef".
        #"  ,COALESCE(CONVERT(varchar,LR.UserTimeRef,103), CONVERT(varchar,JR.UserTimeRef,103)) As UserTimeRef".
        #"  ,CONVERT(varchar, JR.UserTimeElab , 103) As UserTimeElab".
        "  ,(CONVERT(varchar, JR.XferStartTime, 103) + SUBSTRING(CONVERT(varchar, JR.XferStartTime, 121),11,6)) As XferStartTime".
        "  ,COUNT(*) As TotReports".
        "  ,SUM(LR.TotPages) As TotPages".
        "  ,JR.JobName".
        "  ,JR.JobNumber".
        "  ,COALESCE(LR.UserRef, JR.UserRef) AS ReportName ".
        "  ,JR.FileRangesVar".
        "  ,JR.ExistTypeMap".
        "  ,1 As IsGRANTED".
        "  ,CASE WHEN charindex('\\',reverse(FolderName)) > 0 THEN (right([FolderName],charindex('\\',reverse(FolderName))-(1))) ELSE FolderName END AS Recipient".
        " from (".
        " SELECT fn1.FolderName AS FolderName, FolderDescr AS FolderDescr ".
        "              , rn1.ReportName as ReportName ".
        "               ,rn1.FilterVar AS FilterVar ".
        "               ,vv1.VarName AS VarName, vv1.varValue AS VarValue".
        "               ,nm1.FilterRule AS FilterRule, nm1.RecipientRule AS RecipientRule".
        "               ,lr1.JobReportID as JobReportID, lr1.ReportId as ReportID, lr1.TotPages As TotPages, ".
        "       lrr1.userTimeRef AS UserTimeRef,    lrt1.textString AS UserRef ".
            "".
        "               from tbl_Folders fn1".
        "               INNER JOIN dbo.tbl_FoldersRules fr1 ON fn1.FolderName = fr1.FolderName".
        "               INNER JOIN dbo.tbl_NamedReportsGroups nm1 ON fr1.ReportGroupId = nm1.ReportGroupId ".
        "               and nm1.FilterRule <> ''  and nm1.ReportRule <> ''".
        "               INNER JOIN tbl_VarSetsValues vr1 ON nm1.ReportRule = vr1.VarSetName and vr1.VarName = ':REPORTNAME' ".
        "               INNER JOIN tbl_ReportNames rn1 ON  rn1.ReportName = vr1.VarValue ".
        "                   and nm1.FilterRule <> ''  and nm1.ReportRule <> ''".
        "               INNER JOIN tbl_LogicalReports lr1 ON lr1.ReportName = rn1.ReportName".
        "  INNER JOIN tbl_LogicalReportsRefs lrr1 ON lr1.JobReportId = lrr1.JobReportId AND lr1.ReportId = lrr1.ReportId ".
        "  INNER JOIN tbl_VarSetsValues vv1 ON vv1.VarSetName = nm1.FilterRule and vv1.VarName = lrr1.FilterVar ".
        "  LEFT OUTER JOIN tbl_LogicalReportsTexts lrt1 ON lrr1.textId = lrt1.textId ".
        "   and lr1.XferRecipient = nm1.RecipientRule".
        "   and lr1.FilterValue = vv1.VarValue ".
        "union all ".
        "SELECT fn2.FolderName AS FolderName, FolderDescr AS FolderDescr ".
        "              , rn2.ReportName as ReportName ".
        "               ,rn2.FilterVar AS FilterVar ".
        "               ,'' AS VarName, '' AS VarValue ".
        "               ,nm2.FilterRule AS FilterRule, nm2.RecipientRule AS RecipientRule ".
        "               ,lr2.JobReportID as JobReportID, lr2.ReportId as ReportID, lr2.TotPages As TotPages, ".
        "       lrr2.userTimeRef AS UserTimeRef,    lrt2.textString AS UserRef ".
        "from tbl_Folders fn2 ".
        "   INNER JOIN dbo.tbl_FoldersRules fr2 ON fn2.FolderName = fr2.FolderName ".
        "   INNER JOIN dbo.tbl_NamedReportsGroups nm2 ON fr2.ReportGroupId = nm2.ReportGroupId and nm2.FilterRule = ''  and nm2.ReportRule <> '' ".
        "   INNER JOIN tbl_VarSetsValues vr2 ON nm2.ReportRule = vr2.VarSetName and vr2.VarName = ':REPORTNAME' ".
        "       INNER JOIN tbl_ReportNames rn2 ON  rn2.ReportName = vr2.VarValue ".
        "       INNER JOIN tbl_LogicalReports lr2 ON lr2.ReportName = rn2.ReportName ".
        "       INNER JOIN tbl_LogicalReportsRefs lrr2 ON lr2.JobReportId = lrr2.JobReportId AND lr2.ReportId = lrr2.ReportId ".
        "       LEFT OUTER JOIN tbl_LogicalReportsTexts lrt2 ON lrr2.textId = lrt2.textId".
        "               and lr2.XferRecipient = nm2.RecipientRule ".
        "UNION ALL ".
        "SELECT fn3.FolderName AS FolderName, FolderDescr AS FolderDescr ".
        "               ,rn3.ReportName as ReportName ".
        "               ,rn3.FilterVar AS FilterVar ".
        "               ,'' AS VarName, '' AS VarValue ".
        "               ,nm3.FilterRule AS FilterRule, nm3.RecipientRule AS RecipientRule ".
        "               ,lr3.JobReportID as JobReportID, lr3.ReportId as ReportID, lr3.TotPages As TotPages, ".
        "       lrr3.userTimeRef AS UserTimeRef,    lrt3.textString AS UserRef ".
        "from tbl_Folders fn3 ".
        "   INNER JOIN dbo.tbl_FoldersRules fr3 ON fn3.FolderName = fr3.FolderName ".
        "   INNER JOIN dbo.tbl_NamedReportsGroups nm3 ON fr3.ReportGroupId = nm3.ReportGroupId and nm3.ReportRule <> '' ".
        "   INNER JOIN tbl_VarSetsValues vr3 ON nm3.ReportRule = vr3.VarSetName and vr3.VarName = ':REPORTNAME' ".
        "       INNER JOIN tbl_ReportNames rn3 ON  rn3.ReportName = vr3.VarValue ".
        "       INNER JOIN tbl_LogicalReports lr3 ON lr3.ReportName = rn3.ReportName ".
        "       INNER JOIN tbl_LogicalReportsRefs lrr3 ON lr3.JobReportId = lrr3.JobReportId AND lr3.ReportId = lrr3.ReportId ".  
                "   LEFT OUTER JOIN tbl_LogicalReportsTexts lrt3 ON lrr3.textId = lrt3.textId ".
        "               and lr3.XferRecipient = nm3.RecipientRule ".
        
        ") LR INNER JOIN tbl_JobReports JR ON JR.JobReportId = LR.JobReportid ".
        #" $whereClause".
        "  GROUP BY FolderName, LR.ReportName, JR.JobReportId, XferStartTime, LR.UserTimeRef, JR.UserTimeRef".
        " ,JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime".
        " ,JobName, JobNumber, LR.UserRef, JR.UserRef, FileRangesVar, existTypeMap, FolderDescr ) ".
        "SELECT * FROM Reports".
        " $whereClause ".
        "END";
    my $sql3 = "SELECT * from dbo.vw_reportsSmartTestToRecipient $whereClause "; 
    main::debug2log($sql3."and__".$user);
    my $outResult;
    my $result = _getDataFromSQL($sql3);
    #main::debug2log("ECCO_QUELLO_CHE_MI_SERVE".Dumper($result));
    if(!$result) {
        $actionMessage = "No data found!";
    } else {
        $actionMessage = "SUCCESS";
        if(_checkRoot($user) != undef){
            main::debug2log("USER_IS_ROOT");
            if(scalar @{$result}  > 1000){
              my @intermedio = @{$result}[1..1000]; 
              $outResult = \@intermedio;
            }  
            else {
               $outResult = $result;
            }   
        }else{
          main::debug2log("user_not_is_root");  
          my $sql4 = "select distinct rootnode from tbl_profilesfolderstrees pft join tbl_usersprofiles up inner join tbl_useraliases ua on up.username = ua.username ".
                         "on pft.profilename = up.profilename and rootnode <> 'root' ".
                         "and ua.useralias = '$user' ";
          my $result4 = _getDataFromSQLNoFill($sql4); my @profiles;
          while($result4 != undef  && !$result4->eof()){
               push @profiles , $result4->Fields()->item('rootnode')->value();
               $result4->MoveNext();
               main::debug2log("rrrrrrrrrrrrrr----------------------------------------------------------".Dumper(@profiles));
          }
          main::debug2log("num:righe".Dumper(scalar(@{$result})));
          foreach my $line (@{$result}) {
            last if ($count >= 1000);    
            main::debug2log("rrrrrrrrrrrrrr----------------------------------------------------------".Dumper($line));
            #main::debug2log("riga_numeroarray__".dumper($line->[0]));
            
            if($line->{Columns}->[0]){
                #main::debug2log("colonna__".dumper($line->{'columns'}));
                my $folder1 = (grep { $_->{'colname'} =~ /foldername/i } @{$line->{Columns}})[0]->{content};
                main::debug2log("folder__".Dumper($folder1));
                
                #foreach my $profile (@profiles){
                #main::debug2log("profile".Dumper($profile));
                if((grep {$folder1 =~ /^\Q$_\E/i} @profiles)[0]  !~ /^$/){
                  #$profile = s/\\/\\\\/g;
                  
                   #if($folder1 =~/^\Q$profile\E/i){
                     main::debug2log("$count####################-------------------------------".Dumper($_)) if ($count % 100 == 0) ;
                     $count++;
                     push (@$outResult,$line);
                     next; 
                 }
                 #} 
                 # } 
            }
            } 
         } 
    }
    

    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];

    main::debug2log("@@@@@@@@@@@@@@@@@@------------------------------------------------------".Dumper(@{$outResult}[1])) if ref($outResult) eq "ARRAY";
    push @{$respEntries}, @{$outResult} if($outResult);
#   my $h = gensym();
#   open($h, ">D:/out.log") or die("open error: $!");
#   print $h XReport::SOAP::buildResponse({
#       IndexName => 'XREPORTWEB',
#       IndexEntries => $respEntries
#   });
#   close($h);

    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub getOldReportsData {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";
    my $user = ""; my $currentRow = 0;
    my $count = 0;
    my ($IndexEntries, $IndexName, $TOP, $ORDERBY, $DISTINCT) = @{$selection->{request}}{qw(IndexEntries IndexName TOP ORDERBY DISTINCT)};
    $TOP = 3000 if($TOP =~ /^$/);
    main::debug2log(Dumper($selection->{request}));
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;
        if($field_name =~ /^User$/){
            $user = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
            next;
        }
        if($field_name =~ /^CurrentRow$/){
            $currentRow = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
            next;
        }   
        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/
                    and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Min'} =~ /^$/
                and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Max'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        #$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date1$/) ? " Convert (varchar ,[$field_name] ,112) >=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
        #   if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
        #$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date2$/) ? " Convert (varchar ,[$field_name] ,112) <=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
        #   if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            
        $whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) >= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} , "'", "AND")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} !~ /^\s*$/);
        $whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) <= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} , "'", "AND")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} !~ /^\s*$/);  
        main::debug2log("ECCO".Dumper($whereClause));
        if ($field =~ /^RepName$/)
        {
              if ($whereClause =~ /^\s*$/)
              {
                $whereClause = " where ";
              }
              else
              {
                 $whereClause .= " AND ";
              }
                $whereClause .= "(LR.UserRef like '".$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}."' OR JR.UserRef like '".$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}."')";
        }    
        else 
        {
              $whereClause = _addWhereClause($whereClause, "$field LIKE ",$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'", "AND") 
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} !~ /^date$/);
        } 
    }   
    
    my $sql4 = " inner join tbl_profilesfolderstrees pft join tbl_usersprofiles up inner join tbl_useraliases ua on up.username = ua.username  on pft.profilename = up.profilename and rootnode <> 'root' 
                         and ua.useralias like '$user'   on SubString(FolderName, 1, len(pft.rootNode) ) = pft.rootNode ";
        
    if(!(_checkRoot($user) != undef)){
            main::debug2log("USER_IS_NOT_ROOT");
            
         $whereClause = $sql4.$whereClause." ";   
    }
    my $sql3 = "SELECT TOP 1000 * FROM ".
               "    ( SELECT ROW_NUMBER() OVER (ORDER BY UserTimeRef) AS ROW,  B.* ".
               "         FROM dbo.vw_reportsSmartTestToRecipient B $whereClause ) T ".
               "WHERE ROW > $currentRow ORDER BY RECIPIENT"; 
    main::debug2log($sql3."and__".$user);   
    my $result = _getDataFromSQL($sql3);
    #main::debug2log("ECCO_QUELLO_CHE_MI_SERVE".Dumper($result));
    my $outResult;
    if(!$result) {
        $actionMessage = "No data found!";
    } else {
        $actionMessage = "SUCCESS";
        
        if(scalar @{$result}  > 1000){
         my @intermedio = @{$result}[1..1000]; 
             $outResult = \@intermedio;
        } else {
            $outResult = $result;
        }  
    
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];

    main::debug2log("@@@@@@@@@@@@@@@@@@------------------------------------------------------".Dumper(@{$outResult}[1])) if ref($outResult) eq "ARRAY";
    push @{$respEntries}, @{$outResult} if($outResult);
#   my $h = gensym();
#   open($h, ">D:/out.log") or die("open error: $!");
#   print $h XReport::SOAP::buildResponse({
#       IndexName => 'XREPORTWEB',
#       IndexEntries => $respEntries
#   });
#   close($h);

    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub getReportsData {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";
    my $user = ""; my $currentRow = 0; my $jobName = "";
    my $count = 0;
    my ($IndexEntries, $IndexName, $TOP, $ORDERBY, $DISTINCT) = @{$selection->{request}}{qw(IndexEntries IndexName TOP ORDERBY DISTINCT)};
    $TOP = 3000 if($TOP =~ /^$/);
    main::debug2log(Dumper($selection->{request}));
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;
        if($field_name =~ /^User$/){
            $user = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
            next;
        }
        if($field_name =~ /^JobName$/){
            $jobName = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
            next;
        }
        if($field_name =~ /^CurrentRow$/){
            $currentRow = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
            next;
        }   
        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/
                    and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Min'} =~ /^$/
                and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Max'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        #$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date1$/) ? " Convert (varchar ,[$field_name] ,112) >=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
        #   if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
        #$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date2$/) ? " Convert (varchar ,[$field_name] ,112) <=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
        #   if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            
        $whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) >= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} , "'", "AND")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} !~ /^\s*$/);
        $whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) <= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} , "'", "AND")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} !~ /^\s*$/);  
        main::debug2log("ECCO".Dumper($whereClause));
        if ($field =~ /^RepName$/)
        {
              if ($whereClause =~ /^\s*$/)
              {
                $whereClause = " where ";
              }
              else
              {
                 $whereClause .= " AND ";
              }
                $whereClause .= "(LR.UserRef like '".$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}."' OR JR.UserRef like '".$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}."')";
        }    
        else 
        {
              $whereClause = _addWhereClause($whereClause, "$field LIKE ",$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'", "AND") 
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} !~ /^date$/);
        } 
    }   
    #my $sql4 =  " like '#{Recipient}#'";
    my $sql4 =  " ";
    if(!(_checkRoot($user) != undef)){
            main::debug2log("USER_IS_NOT_ROOT");
            
          $sql4 =  " and fn1.FolderName in (select FolderName from tbl_Folders fo".
            "      inner join tbl_ProfilesFoldersTrees pt on fo.FolderName like pt.RootNode+'%'".
            "       inner join tbl_UsersProfiles up on up.ProfileName = pt.ProfileName and rootnode <> 'root'  where UserName like '#{User}#'".
            "    )";   
    }

    my $sql6 = "select top 1000 * from (SELECT ROW_NUMBER() OVER (ORDER BY UserTimeRef) AS ROW , B.* from (select fn1.FolderName, FolderDescr AS FolderDescr, JR.JobReportId as JobReport_ID, lr1.ReportId As ReportId  , JR.XferStartTime As TimeRef  , COALESCE(lrr1.UserTimeRef, JR.UserTimeRef, JR.XferStartTime) As UserTimeRef, COALESCE(JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime) As UserTimeElab  , (CONVERT(varchar, JR.XferStartTime, 103) + SUBSTRING(CONVERT(varchar, JR.XferStartTime, 121),11,6)) As XferStartTime  ,".
    "    1 As TotReports  , sum(lr1.TotPages) As TotPages  , CONVERT(varchar, pr1.ListOfPages) as ListOfPages,".
    "      JR.JobName  ,JR.JobNumber  ,".
    "    COALESCE(lrt1.textString, JR.UserRef) AS ReportName   ,JR.FileRangesVar ".
    "      ,JR.ExistTypeMap  ,1 As IsGRANTED,".
    "    CASE WHEN charindex('\\',reverse(fn1.FolderName)) > 0 THEN (right(fn1.FolderName,".
    "    charindex('\\',reverse(fn1.FolderName))-(1))) ELSE fn1.FolderName END AS Recipient, lr1.CheckStatus, lr1.LastUsedToCheck, RN.CheckFlag".

    "       from tbl_Folders fn1".
    "       INNER JOIN dbo.tbl_FoldersRules fr1 ON fn1.FolderName = fr1.FolderName  ".
    
    $sql4.
    "       INNER JOIN dbo.tbl_NamedReportsGroups nm1 ON fr1.ReportGroupId = nm1.ReportGroupId".
    "       INNER JOIN tbl_VarSetsValues vr1 ON nm1.ReportRule = vr1.VarSetName".
    "       INNER JOIN tbl_VarSetsValues vv1 ON vv1.VarSetName = nm1.FilterRule".
    "        and vv1.VarName = nm1.FilterVar".
    #"        --INNER JOIN tbl_JobReports JR".
    "          ".
    "        INNER JOIN (Select * from tbl_JobReports where  JobName LIKE  '%#{JobName}#' ) JR".
    "        ON JR.JobReportName = vr1.VarValue".
    "        INNER JOIN tbl_LogicalReports lr1 ON lr1.JobReportId = ".
    "    JR.JobReportId and lr1.XferRecipient = nm1.RecipientRule       and".
    "        lr1.FilterValue = vv1.VarValue      INNER JOIN tbl_LogicalReportsRefs lrr1".
    "       ON lr1.JobReportId = lrr1.JobReportId AND lrr1.ReportId = lr1.ReportId  and lrr1.FilterVar = vv1.VarName".
    "        INNER JOIN tbl_LogicalReportsTexts lrt1 ON lrr1.textId = lrt1.textId".
    "        INNER JOIN dbo.tbl_ReportNames RN ON RN.ReportName = lr1.ReportName".
    "        INNER JOIN tbl_PhysicalReports pr1 ON pr1.JobReportId = lr1.JobReportId and pr1.ReportId = lr1.ReportId".
    "        where JR.Status = 18 and vr1.VarName = ':REPORTNAME'".
    
    "    GROUP BY fn1.FolderName, lr1.ReportName, JR.JobReportId, lr1.ReportID, XferStartTime, COALESCE(lrr1.UserTimeRef, JR.UserTimeRef, JR.XferStartTime), convert(varchar, pr1.ListOfPages) , JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime, JR.UserTimeRef, UserTimeElab ,JobName, JobNumber, lrt1.textString, JR.UserRef, FileRangesVar, existTypeMap,".
    "      FolderDescr, lr1.CheckStatus, lr1.LastUsedToCheck, RN.CheckFlag".
    "    ) B".
    "    $whereClause) T WHERE ROW > 0 ORDER BY $ORDERBY ";
    main::debug2log($sql6."and__".$user);
    $sql6 =~ s/#{([^\{]+)}#/$IndexEntries->[0]->{'Columns'}->{$1}->{'content'}/g;


    my $sql3 = "SELECT TOP 1000 * FROM ".
               "    ( SELECT ROW_NUMBER() OVER (ORDER BY UserTimeRef) AS ROW,  B.* ".
               "         FROM dbo.vw_reportsSmartTestToRecipient B $whereClause ) T ".
               "WHERE ROW > $currentRow ORDER BY RECIPIENT"; 
    main::debug2log($sql6."and__".$user);   
    my $result = _getDataFromSQL($sql6);
    #main::debug2log("ECCO_QUELLO_CHE_MI_SERVE".Dumper($result));
    my $outResult;
    if(!$result) {
        $actionMessage = "No data found!";
    } else {
        $actionMessage = "SUCCESS";
        
        if(scalar @{$result}  > 1000){
         my @intermedio = @{$result}[1..1000]; 
             $outResult = \@intermedio;
        } else {
            $outResult = $result;
        }  
    
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];

    main::debug2log("@@@@@@@@@@@@@@@@@@------------------------------------------------------".Dumper(@{$outResult}[1])) if ref($outResult) eq "ARRAY";
    push @{$respEntries}, @{$outResult} if($outResult);
#   my $h = gensym();
#   open($h, ">D:/out.log") or die("open error: $!");
#   print $h XReport::SOAP::buildResponse({
#       IndexName => 'XREPORTWEB',
#       IndexEntries => $respEntries
#   });
#   close($h);

    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub _checkRoot{
    my($user) =(shift);
    main::debug2log("PARA________".$user);
    
    my $sql = "select  RootNode from tbl_ProfilesFoldersTrees pft join tbl_UsersProfiles up ".
          "on pft.ProfileName = up.ProfileName 
            join tbl_UserAliases ua on up.UserName = ua.UserName and  ua.UserAlias = '$user' and RootNode = 'ROOT' ";
    main::debug2log($sql);
    my $result = _getDataFromSQLNoFill($sql);
    #main::debug2log("_CHECKROOT_".Dumper($result->Fields()->Item('RootNode')->Value()));
    return $result;
    
}

sub getReportsCount
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";
    my $user = ""; 
    #my $currentRow = 0;
    my $count = 0;
    my ($IndexEntries, $IndexName, $TOP, $ORDERBY, $DISTINCT) = @{$selection->{request}}{qw(IndexEntries IndexName TOP ORDERBY DISTINCT)};
    $TOP = 3000 if($TOP =~ /^$/);

    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;
        if($field_name =~ /^User$/){
            $user = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
            next;
        }
        if($field_name =~ /^CurrentRow$/){
           #$currentRow = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
            next;
        }
        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/
                    and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Min'} =~ /^$/
                and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Max'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }           
        $whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) >= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} , "'", "AND")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} !~ /^\s*$/);
        $whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) <= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} , "'", "AND")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} !~ /^\s*$/);  
        $whereClause = _addWhereClause($whereClause, "$field LIKE ",$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'", "AND") 
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} !~ /^date$/);
    }
    my $sql4 = " INNER JOIN tbl_profilesfolderstrees pft". 
               " JOIN tbl_usersprofiles up".
               " INNER JOIN tbl_useraliases ua".
               " ON up.username = ua.username ON pft.profilename = up.profilename AND rootnode <> 'root'".
               " AND ua.useralias LIKE '$user' ON SubString(FolderName, 1, len(pft.rootNode) ) = pft.rootNode ";
        
    if(!(_checkRoot($user) != undef)){
         main::debug2log("USER_IS_NOT_ROOT");       
         $whereClause = $sql4.$whereClause." ";   
    }

    #my $sql3 = "select top 1000 * from ( SELECT ROW_NUMBER() OVER (ORDER BY UserTimeRef) AS ROW,  B.* ,(select COUNT(*) from dbo.vw_ReportsSmartTestToRecipient A $whereClause ) as Totale from dbo.vw_reportsSmartTestToRecipient B $whereClause ) T WHERE ROW > $currentRow"; 
    my $sql3 = "SELECT COUNT(*) ".
               "FROM dbo.vw_reportsSmartTestToRecipient3 $whereClause";               
    main::debug2log($sql3."and__".$user);   
    my $result = _getDataFromSQL($sql3);
    #main::debug2log("ECCO_QUELLO_CHE_MI_SERVE".Dumper($result));
    my $outResult;
    if(!$result) {
        $actionMessage = "No data found!";
    } else {
        $actionMessage = "SUCCESS";
        $outResult = $result;   
    }

    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];

    push @{$respEntries}, @{$outResult} if($outResult);
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub getJobReportPdf {
    my ($method, $reqident, $selection) = (shift, shift, shift);

    my $IndexEntry = $selection->{request}->{IndexEntries}->[0];
    my $JobReportId = $IndexEntry->{'Columns'}->{'JobReportId'}->{'content'};
    my $FromPage = $IndexEntry->{'Columns'}->{'FromPage'}->{'content'};
    my $jex;
    eval {$jex =  XReport::ARCHIVE::JobExtractor->Open($JobReportId);};
    if ( $@ ) {
           return _SOAP_ERROR( faultcode => 500, faultstring => "Server Error"
                           , detailmsg => "Open Error"
                           , detaildesc => "Error during JobExtractor Open - $@" );
            
    }
    my $JobReportName = $jex->{JobReportName};
    my $pagexref = [$jex->getPageXref()];
#   warn "REQFID: $REQFID PAGEXREF: ", join('::', @{$pagexref}), "\n";
    my $fileid;
    while ( scalar(@{$pagexref}) && !$fileid ) {
      my ( $fid, $fp, $tp) = splice @{$pagexref}, 0, 3;
      $fileid = $fid if ( $FromPage >= $fp && $FromPage <= $tp  );
    }
        
    returnFullDocument("$JobReportId/$fileid", {
        IndexName => 'XREPORTWEB',
        IndexEntries => {
        	 Columns => [ {colname => 'ActionMessage', content => "SUCCESS"} ]
             },
        DocumentType => 'application/pdf',
        FileName => "$JobReportName\.$JobReportId.$fileid",
        Identity =>  $JobReportName.$JobReportId
     });
            
}



sub dbEscape{

 my $str = shift;
 $str =~ s/'/''/g;
 return $str;
}

sub _loadIndexAttrs {
    my ($dbh, $ixname) = (shift, shift);
    my $ixattrsTXT = $main::Application->{"index.$ixname"};
    if ( !$ixattrsTXT ) {
       my $dbhix = XReport::DBUtil->get_ix_dbc($ixname);
       my $dbr = $dbhix->ExecuteReadOnly("SELECT TOP 0 * from tbl_idx_$ixname");
       my $fields = $dbr->Fields();
       $main::Application->{"index.$ixname"} = 'columns=['.join(',',
            (grep !/^(?:JobReportId|FromPage|ForPages)$/, map { $fields->Item($_)->Name() } (0..$fields->Count()-1) )
               ).']' ;
       
    }
    return {map { map { $_ =~ /^\[([^\]]+)\]$/ ? [split /\,/, $1 ] : $_ } split /=/, $_ } split /\|/, $ixattrsTXT }; 
}


sub getIndexes {
  my $reqdata = shift;
  i::traceit("getIndexesList input:", _Dumper($reqdata) ) if $main::dotrace;
  
  my ($method, $reqident) = @{$main::Request}{qw(method reqid)};
  
  my $dfltTOP = $XReport::cfg->{defaulttop} || 100;

  my ($tblid, $orderby, $IndexEntries) = @{$reqdata}{qw(IndexName ORDERBY IndexEntries)};
  my ($dbh, $what);
  if ( !$tblid ) {
     $dbh = getDBHandle('XREPORT');
     $what= "tbl_JobReportsIndexTables";
  }
  if ( $tblid =~ /^#/ ) {
     my $sqlid = substr($tblid, 1);
     my $sql = $XReport::cfg->{sqlprocs}->{$sqlid};
     return _SOAP_ERROR( faultcode => 500, faultstring => "Client Request Error"
                             , detailmsg => "sql $sqlid not found"
                             , detaildesc => "$sqlid not in cfg list (". join(', ', keys %{$XReport::cfg->{sqlprocs}}) ) unless $sql;
     $what = '('.$sql.') ';
  }
  
  if ( $tblid =~ /^_/ ) {
     $dbh = getDBHandle('XREPORT');
     $what = "tbl".$tblid;
  }
  else {
     $what = 'tbl_idx_'.$tblid;
  }

  my $select = 'SELECT ';
  $select .= ( !exists($reqdata->{TOP}) ? 'TOP $dfltTOP' : $reqdata->{TOP} ne "-1" ? "TOP $reqdata->{TOP}" : '' );
  $select .= ' DISTINCT(' . ( exists($reqdata->{DISTINCT}) ? $reqdata->{DISTINCT} : 'IndexName' ). ')';

  $select .= ' FROM '. $what;
    
  my $where = '';
  $where = ' WHERE (' . join(') OR (', XReport::SOAP::buildClause(@{$reqdata}{qw(IndexName IndexEntries)}, '')).')'
                                                if (exists($reqdata->{IndexEntries}) && scalar(@{$reqdata->{IndexEntries}}));
  i::traceit( "getDocList: ", _Dumper($reqdata), "Clause: $where") if $main::dotrace;
  $select .= $where;
  
  $select .= ' ORDER BY '.$orderby if $orderby;

  i::traceit("${method} SELECT:", $select) if $main::dotrace;

   my $dbr; eval { $dbr = $dbh->dbExecute($select)};
   return _SOAP_ERROR(faultcode => 500, faultstring => "Data Base Error"
                      , detailmsg => "getIndexList error", detaildesc => $@. "Request: "._Dumper($reqdata)) if $@;
   my $outlist = XReport::SOAP::fillIndexEntries($dbr);
   
   if ( !$tblid ) {
      foreach my $ie ( @{$outlist} ) {
        $ie->{Columns}->{rootvar} = loadIndexAttrs($ie->{Columns}->{IndexName})->{columns}->[0];
      }
   }
   my $DocumentData = {IndexEntries => $outlist
                      , NewEntries => []
                      , DocumentBody => ''
   };
   $DocumentData->{IndexName} = $tblid if $tblid;
   $DocumentData->{DISTINCT} 
      = (grep { !exists($IndexEntries->[0]->{Columns}->{$_}) } @{loadIndexAttrs($tblid)->{columns}})[0];
       
   
                      
  return $main::Response->Write( XReport::SOAP::buildResponse($DocumentData) );                    
  
}

sub _getDataFromSQL {
    my ($sql) = (shift);

    my $dbr = XReport::DBUtil::dbExecuteReadOnly($sql);
    if($dbr->eof() or !$dbr) {
        return undef;
    } else {
        return XReport::SOAP::fillIndexEntries($dbr);
    }
}

sub _getDataFromSQLNoFill {
    my ($sql) = (shift);

    my $dbr = XReport::DBUtil::dbExecuteReadOnly($sql);
    if($dbr->eof() or !$dbr) {
        return undef;
    } else {
        return $dbr;
    }
}

sub _updateDataFromSQL {
    my ($sql) = (shift);

    my $dbr = XReport::DBUtil::dbExecute($sql);
}

sub _addWhereClause {
    my ($where, $name, $value, $sep, $cond) = (shift, shift, shift, shift, shift);
    $cond = " AND" if($cond !~ /[AND|OR]/);

    if($value !~ /^$/) {
        if($where =~ /^$/) {
            $where = " WHERE";
        } else {
            $where .= " $cond";
        }
        $where .= " $name $sep$value$sep";
    }

    return $where;
}

my $excelConfigLineCommands = {
    'R' => '_requeueJobreports',
    'P' => '_deleteJobreports',
    'L' => '_getJobReportLog',
    'VD' => '_downloadInput',
    'O' => '_downloadOutput',
};

sub getReports{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

#   my $sql = 
#       "SELECT TOP 100 vv.VarSetName            as ReportGroup,"
#       ."jr.JobReportName         as ReportName,"
#       ."jr.JobReportDescr        as ReportDescr,"
#       ."jr.TypeOfWork            as TypeOfWork,"
#       ."jr.HoldDays              as RetDays,"
#       ."jr.MailTo                as MailTo,"
#       ."jr.ParseFileName         as ParseFileName,"
#       ."jr.TargetLocalPathId_IN  as ArchivioInput,"
#       ."jr.TargetLocalPathId_OUT as ArchivioOutput,"
#       ."jr.CodePage              as CodePage,"
#       ."jr.ReportFormat          as ReportFormat,"
#       ."jr.ElabFormat            as ElabFormat,"
#       ."jr.WorkClass             as WorkClass,"
#       ."jr.HasCc             as HasCc,"
#       ."jr.CharsPerLine             as CharsPerLine,"
#       ."jr.LinesPerPage             as LinesPerPage,"
#       ."jr.PageOrient             as PageOrient,"
#       ."jr.FontSize             as FontSize,"
#       ."rn.FilterVar             as FilterVar,"
#       ."op.t1Fonts               as t1Fonts,"
#       ."op.LaserAdjust           as LaserAdjust,"
#       ."op.FormDef               as FormDef "
#       ."FROM         tbl_NamedReportsGroups AS rg "
#       ."INNER JOIN      tbl_VarSetsValues AS vv ON rg.ReportRule = vv.VarSetName AND vv.VarName = ':REPORTNAME' "
#       ."INNER JOIN      tbl_JobReportNames AS jr ON jr.JobReportName = vv.VarValue "
#       ."LEFT OUTER JOIN tbl_Os390PrintParameters AS op ON op.JobReportName = jr.JobReportName "
#       ."LEFT OUTER JOIN tbl_ReportNames AS rn ON rn.ReportName = jr.JobReportName "
#       ." $whereClause "
#       ."ORDER BY ReportGroup"
#   ;
    my $sql = 
        "SELECT TOP 1000 * FROM (SELECT DISTINCT vv.VarSetName            as ReportGroup,"
        ."jr.JobReportName         as ReportName,"
        ."jr.JobReportDescr        as ReportDescr,"
        ."jr.TypeOfWork            as TypeOfWork,"
        ."jr.HoldDays              as RetDays,"
        ."jr.MailTo                as MailTo,"
        ."jr.ParseFileName         as ParseFileName,"
        ."jr.TargetLocalPathId_IN  as ArchivioInput,"
        ."jr.TargetLocalPathId_OUT as ArchivioOutput,"
        ."jr.CodePage              as CodePage,"
        ."jr.ReportFormat          as ReportFormat,"
        ."jr.ElabFormat            as ElabFormat,"
        ."jr.WorkClass             as WorkClass,"
        ."jr.HasCc             as HasCc,"
        ."jr.CharsPerLine             as CharsPerLine,"
        ."jr.LinesPerPage             as LinesPerPage,"
        ."jr.PageOrient             as PageOrient,"
        ."jr.FontSize             as FontSize,"
        ."jr.FitToPage             as FitToPage,"
        ."jr.Priority             as Priority,"
        ."rn.FilterVar             as FilterVar,"
        ."op.t1Fonts               as t1Fonts,"
        ."op.LaserAdjust           as LaserAdjust,"
        ."op.PageDef as PageDef, "
        ."op.CharsDef as CharsDef, "
        ."op.FormDef  as FormDef, "
        ."op.ReplaceDef as ReplaceDef,"
        ."op.PrintControlFile as PrintControlFile, "
        ."op.PageSize as PageSize "
        ."FROM         tbl_JobReportNames AS jr "
        ."LEFT OUTER JOIN tbl_Os390PrintParameters AS op ON op.JobReportName = jr.JobReportName "
        ."LEFT OUTER JOIN tbl_ReportNames AS rn ON rn.ReportName = jr.JobReportName "
        ."LEFT OUTER JOIN      tbl_VarSetsValues AS vv ON vv.VarName = ':REPORTNAME' AND vv.VarValue = jr.JobReportName "
        ."LEFT OUTER JOIN      tbl_NamedReportsGroups AS rg ON rg.ReportRule = vv.VarSetName "

        #."FROM         tbl_NamedReportsGroups AS rg "
        #."INNER JOIN      tbl_VarSetsValues AS vv ON rg.ReportRule = vv.VarSetName AND vv.VarName = ':REPORTNAME' "
        #."INNER JOIN      tbl_JobReportNames AS jr ON jr.JobReportName = vv.VarValue "
        #."LEFT OUTER JOIN tbl_Os390PrintParameters AS op ON op.JobReportName = jr.JobReportName "
        #."LEFT OUTER JOIN tbl_ReportNames AS rn ON rn.ReportName = jr.JobReportName "

        ." $whereClause "
        .") as gr ORDER BY ReportName"
    ;

    main::debug2log($sql);
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub manageReports{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(!exists($IndexEntries->[$entryPos]->{'Columns'}->{'_action'})) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }

    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    #my $ReportGroup = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ReportGroup'}->{'content'});
    my @ReportGroups = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'ReportGroup'}->{'content'});
    my $ReportName = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ReportName'}->{'content'});
    my $ReportDescr = uc(dbEscape($IndexEntries->[$entryPos]->{'Columns'}->{'ReportDescr'}->{'content'}));
    my $TypeOfWork = uc($IndexEntries->[$entryPos]->{'Columns'}->{'TypeOfWork'}->{'content'}) || 1;
    my $RetDays = uc($IndexEntries->[$entryPos]->{'Columns'}->{'RetDays'}->{'content'}) || 180;
    my $MailTo = uc($IndexEntries->[$entryPos]->{'Columns'}->{'MailTo'}->{'content'});
    my $ParseFileName = $IndexEntries->[$entryPos]->{'Columns'}->{'ParseFileName'}->{'content'} || '*,NULL.XML';
    my $HasCc = $IndexEntries->[$entryPos]->{'Columns'}->{'HasCc'}->{'content'};
    my $CharsPerLine = uc($IndexEntries->[$entryPos]->{'Columns'}->{'CharsPerLine'}->{'content'}) || 133;
    my $LinesPerPage = uc($IndexEntries->[$entryPos]->{'Columns'}->{'LinesPerPage'}->{'content'}) || 66;
    my $PageOrient = uc($IndexEntries->[$entryPos]->{'Columns'}->{'PageOrient'}->{'content'}) || 'L';
    my $PageSize = uc($IndexEntries->[$entryPos]->{'Columns'}->{'PageSize'}->{'content'}) || '';
    my $FontSize = uc($IndexEntries->[$entryPos]->{'Columns'}->{'FontSize'}->{'content'}) || '[8.0 12.0]';
    my $FitToPage = $IndexEntries->[$entryPos]->{'Columns'}->{'FitToPage'}->{'content'} || 1;
    my $Priority = $IndexEntries->[$entryPos]->{'Columns'}->{'Priority'}->{'content'} || 1;
    my $ArchivioInput = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ArchivioInput'}->{'content'});
    my $ArchivioOutput = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ArchivioOutput'}->{'content'});
    my $CodePage = uc($IndexEntries->[$entryPos]->{'Columns'}->{'CodePage'}->{'content'});
    my $ReportFormat = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ReportFormat'}->{'content'}) || 2;
    my $ElabFormat = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ElabFormat'}->{'content'}) || 1;
    my $WorkClass = uc($IndexEntries->[$entryPos]->{'Columns'}->{'WorkClass'}->{'content'}) || 'NULL';
    my $FilterVar = uc($IndexEntries->[$entryPos]->{'Columns'}->{'FilterVar'}->{'content'});
    my $t1Fonts = uc($IndexEntries->[$entryPos]->{'Columns'}->{'t1Fonts'}->{'content'});
    my $LaserAdjust = uc($IndexEntries->[$entryPos]->{'Columns'}->{'LaserAdjust'}->{'content'});
    my $FormDef = uc($IndexEntries->[$entryPos]->{'Columns'}->{'FormDef'}->{'content'});
    my $PageDef = uc($IndexEntries->[$entryPos]->{'Columns'}->{'PageDef'}->{'content'});
    my $CharsDef = uc($IndexEntries->[$entryPos]->{'Columns'}->{'CharsDef'}->{'content'});
    my $ReplaceDef = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ReplaceDef'}->{'content'});
    my $PrintControlFile = uc($IndexEntries->[$entryPos]->{'Columns'}->{'PrintControlFile'}->{'content'});

    main::debug2log("action is $action");
    $main::msgtolog .= "$action report $ReportName";
    if($action =~ /add/i) {
        _updateDataFromSQL("BEGIN TRANSACTION REPORTADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_JobReportNames WHERE JobReportName = '$ReportName'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_JobReportNames"
                    ." (JobReportName"
                    ." ,JobReportDescr"
                    ." ,TypeOfWork"
                    ." ,HoldDays"
                    ." ,MailTo"
                    ." ,ParseFileName"
                    ." ,HasCc"
                    ." ,CharsPerLine"
                    ." ,LinesPerPage"
                    ." ,PageOrient"
                    ." ,FontSize"
                    ." ,FitToPage"
                    ." ,TargetLocalPathId_IN"
                    ." ,TargetLocalPathId_OUT"
                    ." ,CodePage"
                    ." ,ReportFormat"
                    ." ,ElabFormat"
                    ." ,IsActive"
                    ." ,PrintFlag"
                    ." ,ViewOnlineFlag"
                    ." ,ActiveDays"
                    ." ,ActiveGens"
                    ." ,HoldGens"
                    ." ,StorageClass"
                    ." ,WorkClass"
                    ." ,Priority)"
                    ." VALUES"
                    ." ('$ReportName'"
                    ." ,'$ReportDescr'"
                    ." ,$TypeOfWork"
                    ." ,$RetDays"
                    ." ,'$MailTo'"
                    ." ,'$ParseFileName'"
                    ." ,$HasCc"
                    ." ,$CharsPerLine"
                    ." ,$LinesPerPage"
                    ." ,'$PageOrient'"
                    ." ,'$FontSize'"
                    ." ,$FitToPage"
                    ." ,'$ArchivioInput'"
                    ." ,'$ArchivioOutput'"
                    ." ,'$CodePage'"
                    ." ,$ReportFormat"
                    ." ,$ElabFormat"
                    ." ,1"
                    ." ,'Y'"
                    ." ,'Y'"
                    ." ,5"
                    ." ,5"
                    ." ,30"
                    ." ,''"
                    ." ,$WorkClass"
                    ." ,$Priority)"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Report $ReportName already exists(JobReportNames)";
            }

            $sql = "SELECT * FROM tbl_ReportNames WHERE ReportName = '$ReportName'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_ReportNames"
                    ." (ReportName"
                    ." ,ReportDescr"
                    ." ,HoldDays"
                    ." ,MailTo"
                    ." ,FilterVar)"
                    ." VALUES"
                    ." ('$ReportName'"
                    ." ,'$ReportDescr'"
                    ." ,$RetDays"
                    ." ,'$MailTo'"
                    ." ,'$FilterVar')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Report $ReportName already exists(ReportNames)";
            }

            $sql = "SELECT * FROM tbl_Os390PrintParameters WHERE JobReportName = '$ReportName'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_Os390PrintParameters"
                    ." (JobReportName"
                    ." ,t1Fonts"
                    ." ,LaserAdjust"
                    ." ,FormDef "
                    ." ,PageDef "
                    ." ,CharsDef "
                    ." ,ReplaceDef "
                    ." ,PageSize "
                    ." ,PrintControlFile)"
                    ." VALUES"
                    ." ('$ReportName'"
                    ." ,$t1Fonts"
                    ." ,'$LaserAdjust'"
                    ." ,'$FormDef'"
                    ." ,'$PageDef'"
                    ." ,'$CharsDef'"
                    ." ,'$ReplaceDef'"
                    ." ,'$PageSize'"
                    ." ,'$PrintControlFile')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Report $ReportName already exists(OS390PrintParameters)";
            }

            foreach my $ReportGroup(@ReportGroups) {
                $sql = "SELECT * FROM tbl_NamedReportsGroups WHERE ReportGroupId = '$ReportGroup'";
                $result = _getDataFromSQL($sql);
                if(!$result) {
                    $sql = 
                        "INSERT INTO tbl_NamedReportsGroups"
                        ." (ReportGroupId"
                        ." ,FilterVar"
                        ." ,FilterRule"
                        ." ,RecipientRule"
                        ." ,ReportRule)"
                        ." VALUES"
                        ." ('$ReportGroup'"
                        ." ,''"
                        ." ,''"
                        ." ,''"
                        ." ,'$ReportGroup')"
                    ;
                    main::debug2log($sql);
                    $result = _updateDataFromSQL($sql); 
                }

                $sql = 
                    "INSERT INTO tbl_VarSetsValues"
                    ." (VarSetName"
                    ." ,VarName"
                    ." ,VarValue)"
                    ." VALUES"
                    ." ('$ReportGroup'"
                    ." ,':REPORTNAME'"
                    ." ,'$ReportName')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION REPORTADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION REPORTADD");
        }
    } elsif($action =~ /update/i) {
        _updateDataFromSQL("BEGIN TRANSACTION REPORTUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_JobReportNames WHERE JobReportName = '$ReportName'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "UPDATE tbl_JobReportNames SET"
                    ." JobReportDescr = '$ReportDescr'"
                    ." ,TypeOfWork = $TypeOfWork"
                    ." ,HoldDays = $RetDays"
                    ." ,MailTo = '$MailTo'"
                    ." ,ParseFileName = '$ParseFileName'"
                    ." ,HasCc = $HasCc"
                    ." ,FitToPage = $FitToPage"
                    ." ,CharsPerLine = $CharsPerLine"
                    ." ,LinesPerPage = $LinesPerPage"
                    ." ,PageOrient = '$PageOrient'"
                    ." ,FontSize = '$FontSize'"
                    ." ,TargetLocalPathId_IN = '$ArchivioInput'"
                    ." ,TargetLocalPathId_OUT = '$ArchivioOutput'"
                    ." ,CodePage = '$CodePage'"
                    ." ,ReportFormat = $ReportFormat"
                    ." ,ElabFormat = $ElabFormat"
                    ." ,WorkClass = $WorkClass"
                    ." ,Priority = $Priority"
                    ." WHERE JobReportName = '$ReportName'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Report $ReportName doesn't exists(JobReportNames)";
            }

            $sql = "SELECT * FROM tbl_ReportNames WHERE ReportName = '$ReportName'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "UPDATE tbl_ReportNames SET"
                    ." ReportDescr = '$ReportDescr'"
                    ." ,HoldDays = $RetDays"
                    ." ,MailTo = '$MailTo'"
                    ." ,FilterVar = '$FilterVar'"
                    ." WHERE ReportName = '$ReportName'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Report $ReportName doesn't exists(ReportNames)";
            }


            $sql = "SELECT * FROM tbl_Os390PrintParameters WHERE JobReportName = '$ReportName'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "UPDATE tbl_Os390PrintParameters SET"
                    ."  t1Fonts = '$t1Fonts'"
                    ." ,LaserAdjust = '$LaserAdjust'"
                    ." ,FormDef = '$FormDef'"
                    ." ,PageDef = '$PageDef'"
                    ." ,CharsDef = '$CharsDef'"
                    ." ,ReplaceDef = '$ReplaceDef'"
                    ." ,PageSize = '$PageSize'"
                    ." ,PrintControlFile = '$PrintControlFile'"
                    ." WHERE JobReportName = '$ReportName'"
                    
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                $sql = 
                    "INSERT INTO tbl_Os390PrintParameters"
                    ." (JobReportName"
                    ." ,t1Fonts"
                    ." ,LaserAdjust"
                    ." ,FormDef "
                    ." ,PageDef "
                    ." ,CharsDef "
                    ." ,ReplaceDef "
                    ." ,PageSize "
                    ." ,PrintControlFile)"
                    ." VALUES"
                    ." ('$ReportName'"
                    ." ,$t1Fonts"
                    ." ,'$LaserAdjust'"
                    ." ,'$FormDef'"
                    ." ,'$PageDef'"
                    ." ,'$CharsDef'"
                    ." ,'$ReplaceDef'"
                    ." ,'$PageSize'"
                    ." ,'$PrintControlFile')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql);
            }

            $sql = "DELETE FROM tbl_VarSetsValues WHERE VarValue = '$ReportName'";
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 

            foreach my $ReportGroup(@ReportGroups) {
                $sql = "SELECT * FROM tbl_NamedReportsGroups WHERE ReportGroupId = '$ReportGroup'";
                $result = _getDataFromSQL($sql);
                if(!$result) {
                    $sql = 
                        "INSERT INTO tbl_NamedReportsGroups"
                        ." (ReportGroupId"
                        ." ,FilterVar"
                        ." ,FilterRule"
                        ." ,RecipientRule"
                        ." ,ReportRule)"
                        ." VALUES"
                        ." ('$ReportGroup'"
                        ." ,''"
                        ." ,''"
                        ." ,''"
                        ." ,'$ReportGroup')"
                    ;
                    main::debug2log($sql);
                    $result = _updateDataFromSQL($sql); 
                }

                $sql = 
                    "INSERT INTO tbl_VarSetsValues"
                    ." (VarSetName"
                    ." ,VarName"
                    ." ,VarValue)"
                    ." VALUES"
                    ." ('$ReportGroup'"
                    ." ,':REPORTNAME'"
                    ." ,'$ReportName')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION REPORTUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION REPORTUPDATE");
        }
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION REPORTDELETE");
        eval {
            my $sql = "";
            my $result = undef;
    
            $sql = 
                "DELETE tbl_JobReportNames WHERE"
                ." JobReportName = '$ReportName'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 
        
            $sql = 
                "DELETE tbl_ReportNames WHERE"
                ." ReportName = '$ReportName'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 

            $sql = 
                "DELETE tbl_Os390PrintParameters WHERE"
                ." JobReportName = '$ReportName'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 

            $sql = 
                "DELETE tbl_VarSetsValues WHERE"
                ." VarValue = '$ReportName'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION REPORTDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION REPORTDELETE");
        }
    }
    $main::msgtolog .= " $actionMessage";

    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        #$selection = undef;
        #push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        #main::debug2log("selection is ".Dumper($selection));
        getReports($method, $reqident, $selection);
    }
}

sub getFolderReports {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "WHERE ff.FolderName not like 'ROOT' ";

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    my $sql = "SELECT TOP 1000 ff.FolderName as FolderPath, "
        ."ff.FolderDescr, ff.FolderAddr, ff.SpecialInstr, "
        ."fr.ReportGroupId as ReportGroups "
        ."FROM tbl_NamedReportsGroups AS rg "
        ."INNER JOIN       tbl_FoldersRules AS fr ON rg.ReportGroupId = fr.ReportGroupId "
        ."RIGHT OUTER JOIN tbl_Folders AS ff      ON fr.FolderName =    ff.FolderName "
        #."WHERE ff.FolderName not like 'ROOT' "
        
        #"SELECT TOP 100 ff.FolderName as FolderPath,"
        #."ff.FolderDescr,"
        #."fr.ReportGroupId as ReportGroups "
        #."FROM             tbl_NamedReportsGroups AS rg "
        #."INNER JOIN       tbl_FoldersRules       AS fr ON rg.ReportGroupId = fr.ReportGroupId "
        #."RIGHT OUTER JOIN tbl_Folders            AS ff ON fr.FolderName    = ff.FolderName "
        ." $whereClause "
        ."ORDER BY FolderPath, ReportGRoups"
    ;
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub manageFolderReports {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }

    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    my $FolderPath = uc($IndexEntries->[$entryPos]->{'Columns'}->{'FolderPath'}->{'content'});
    my $FolderDescr = dbEscape($IndexEntries->[$entryPos]->{'Columns'}->{'FolderDescr'}->{'content'});
    my @ReportGroups = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'ReportGroups'}->{'content'});

    my $parent = "ROOT";
    if($FolderPath =~ /^(.*)\\\w+$/) {
        $parent = $1;
    }

    $main::msgtolog .= "$action folder $FolderPath";
    if($action =~ /add/i) {
        _updateDataFromSQL("BEGIN TRANSACTION FOLDERADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_Folders WHERE FolderName = '$FolderPath' AND ParentFolder = '$parent'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_Folders"
                    ." (FolderName"
                    ." ,FolderDescr"
                    ." ,IsActive"
                    ." ,ParentFolder)"
                    ." VALUES"
                    ." ('$FolderPath'"
                    ." ,'$FolderDescr'"
                    ." ,1"
                    ." ,'$parent')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "FolderPath $FolderPath already exists";
            }

            foreach my $group(@ReportGroups) {
                $sql =
                    "INSERT INTO tbl_FoldersRules"
                    ." (FolderName"
                    ." ,ReportGroupId)"
                    ." VALUES"
                    ." ('$FolderPath'"
                    ." ,'$group')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION FOLDERADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION FOLDERADD");
        }
    } elsif($action =~ /update/i) {
        _updateDataFromSQL("BEGIN TRANSACTION FOLDERUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_Folders WHERE FolderName = '$FolderPath' AND ParentFolder = '$parent'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "UPDATE tbl_Folders SET"
                    ." FolderDescr = '$FolderDescr'"
                    ." WHERE FolderName = '$FolderPath' AND ParentFolder = '$parent'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "FolderPath $FolderPath doesn't exists";
            }

            $sql =
                "DELETE tbl_FoldersRules WHERE"
                ." FolderName = '$FolderPath'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql);

            foreach my $group(@ReportGroups) {
                $sql =
                    "INSERT INTO tbl_FoldersRules"
                    ." (FolderName"
                    ." ,ReportGroupId)"
                    ." VALUES"
                    ." ('$FolderPath'"
                    ." ,'$group')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION FOLDERUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION FOLDERUPDATE");
        }
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION FOLDERDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            foreach my $group(@ReportGroups) {
                $sql =
                    "DELETE tbl_FoldersRules WHERE"
                    ." FolderName = '$FolderPath'"
                    ." AND ReportGroupId = '$group'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }

            $sql = "SELECT * FROM tbl_Folders WHERE ParentFolder = '$FolderPath'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "DELETE tbl_Folders WHERE"
                    ." FolderName = '$FolderPath'"
                    ." AND FolderDescr = '$FolderDescr'"
                    ." AND ParentFolder = '$parent'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION FOLDERDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION FOLDERDELETE");
        }
    }

    $main::msgtolog .= " $actionMessage";
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getFolderReports($method, $reqident, $selection);
    }
}

sub getProfilesOperations {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause1 = " WHERE     (NOT (pr.ProfileName = 'SUPER')) AND (NOT (pf.RootNode = 'ROOT')) ";
    my $whereClause2 = "";
    my $whereClause3 = 
        "WHERE NOT profilename IN "
        ."(SELECT DISTINCT profilename FROM tbl_profilesfolderstrees "
        ."UNION ALL SELECT DISTINCT varsetname FROM tbl_varsetsvalues WHERE varname LIKE 'SVC:\%' ) "
    ;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        
        if($field_name =~ /ProfileAuthorities/) {
            $whereClause1 = _addWhereClause($whereClause1, "pf.RootNode like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            $whereClause2 = _addWhereClause($whereClause2, "vv.VarName like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            $whereClause3 = _addWhereClause($whereClause3, "'' like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
        } else {
            $whereClause1 = _addWhereClause($whereClause1, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            $whereClause2 = _addWhereClause($whereClause2, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            $whereClause3 = _addWhereClause($whereClause3, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
        }
    }

    my $sql = "SELECT TOP 1000 pr.ProfileName,"
        ."pr.ProfileDescr,"
        ."pf.RootNode AS ProfileAuthorities "
        ."FROM            tbl_Profiles             AS pr "
        ."INNER JOIN tbl_ProfilesFoldersTrees AS pf ON pr.ProfileName = pf.ProfileName "
        ." $whereClause1 "
        ."union all "
        ."SELECT     pr.ProfileName, pr.ProfileDescr,  DT.VarSetName AS ProfileAuthorities "
        ."FROM            tbl_Profiles             AS pr "
        ."INNER JOIN (select distinct VarsetName from tbl_VarSetsValues AS vv where (vv.VarName LIKE 'SVC:\%') ) DT "
        ."ON pr.ProfileName = dt.VarSetName "
        ." $whereClause2 "
        ."union all "
        ."SELECT     pr.ProfileName, pr.ProfileDescr, '' AS ProfileAuthorities "
        ."FROM            tbl_Profiles   AS pr "
        ." $whereClause3 "
        ."ORDER BY pr.ProfileName, ProfileAuthorities"
    ;


    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub manageProfilesOperations {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }
    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    my $ProfileName = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ProfileName'}->{'content'});
    my $ProfileDescr = dbEscape($IndexEntries->[$entryPos]->{'Columns'}->{'ProfileDescr'}->{'content'});
    my @ProfileAuthorities = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'ProfileAuthorities'}->{'content'});

    $main::msgtolog .= "$action profile $ProfileName";
    if($action =~ /add/i) {
        _updateDataFromSQL("BEGIN TRANSACTION PROFILEADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_Profiles WHERE ProfileName = '$ProfileName'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_Profiles"
                    ." (ProfileName"
                    ." ,ProfileDescr"
                    ." ,LookAtEverything)"
                    ." VALUES"
                    ." ('$ProfileName'"
                    ." ,'$ProfileDescr'"
                    ." ,NULL)"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Profile $ProfileName already exists";
            }

            foreach my $folder(@ProfileAuthorities) {
                $sql =
                    "INSERT INTO tbl_ProfilesFoldersTrees"
                    ." (ProfileName"
                    ." ,RootNode)"
                    ." VALUES"
                    ." ('$ProfileName'"
                    ." ,'".uc($folder)."')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION PROFILEADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION PROFILEADD");
        }
    } elsif($action =~ /update/i) {
        _updateDataFromSQL("BEGIN TRANSACTION PROFILEUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_Profiles WHERE ProfileName = '$ProfileName'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "UPDATE tbl_Profiles SET"
                    ." ProfileDescr = '$ProfileDescr'"
                    ." WHERE ProfileName = '$ProfileName'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Profile $ProfileName doesn't exists";
            }

            $sql =
                "DELETE tbl_ProfilesFoldersTrees WHERE"
                ." ProfileName = '$ProfileName'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql);

            foreach my $folder(@ProfileAuthorities) {
                $sql =
                    "INSERT INTO tbl_ProfilesFoldersTrees"
                    ." (ProfileName"
                    ." ,RootNode)"
                    ." VALUES"
                    ." ('$ProfileName'"
                    ." ,'".uc($folder)."')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION PROFILEUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION PROFILEUPDATE");
        }
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION PROFILEDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            foreach my $folder(@ProfileAuthorities) {
                $sql =
                    "DELETE tbl_ProfilesFoldersTrees WHERE"
                    ." ProfileName = '$ProfileName'"
                    ." AND RootNode = '".uc($folder)."'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }

            $sql = "SELECT * FROM tbl_ProfilesFoldersTrees WHERE ProfileName = '$ProfileName'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "DELETE tbl_Profiles WHERE"
                    ." ProfileName = '$ProfileName'"
                    ." AND ProfileDescr = '$ProfileDescr'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION PROFILEDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION PROFILEDELETE");
        }
    }

    $main::msgtolog .= " $actionMessage";
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getProfilesOperations($method, $reqident, $selection);
    }
}

sub getUsersProfiles {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }
    
    my $sql = 
        "SELECT TOP 1000 us.UserName as UserName,"
        ."us.UserDescr as NameDescr,"
        ."ua.UserAlias as UserId,"
        ."ua.UserAliasDescr as IDDescr,"
        ."us.EMailAddr as Mail,"
        ."pr.ProfileName as Profiles "
        ."FROM tbl_Users us "
        ."INNER JOIN tbl_UserAliases ua on ua.UserName = us.UserName "
        ."INNER JOIN tbl_UsersProfiles pr on pr.UserName = us.UserName"
        ." $whereClause"
    ;
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub manageUsersProfiles {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }

    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    my $UserName = uc($IndexEntries->[$entryPos]->{'Columns'}->{'UserName'}->{'content'});
    my $NameDescr = dbEscape($IndexEntries->[$entryPos]->{'Columns'}->{'NameDescr'}->{'content'});
    
    my $UserId = uc($IndexEntries->[$entryPos]->{'Columns'}->{'UserId'}->{'content'});
    my $IDDescr = dbEscape($IndexEntries->[$entryPos]->{'Columns'}->{'IDDescr'}->{'content'});
    
    my $Mail = $IndexEntries->[$entryPos]->{'Columns'}->{'Mail'}->{'content'};
    my @Profiles = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'Profiles'}->{'content'});

    main::debug2log("action is $action");
    $main::msgtolog .= "$action user $UserName";
    if($action =~ /add/i) {
        main::debug2log("Add User");
        _updateDataFromSQL("BEGIN TRANSACTION USERADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_Users WHERE UserName = '$UserName'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_Users"
                    ." (UserName"
                    ." ,UserDescr"
                    ." ,EMailAddr"
                    ." ,Password)"
                    ." VALUES"
                    ." ('$UserName'"
                    ." ,'$NameDescr'"
                    ." ,'$Mail'"
                    ." ,'')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "User $UserName already exists";
            }

            $sql = "SELECT * FROM tbl_UserAliases WHERE UserName = '$UserName' AND UserAlias = '$UserId'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql =
                    " INSERT INTO tbl_UserAliases"
                    ." (UserAlias"
                    ." ,UserName"
                    ." ,UseAlias"
                    ." ,UserAliasDescr)"
                    ." VALUES"
                    ." ('$UserId'"
                    ." ,'$UserName'"
                    ." ,NULL"
                    ." ,'$IDDescr')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Alias $UserId for $UserName already exists";
            }

            foreach my $profile(@Profiles) {
                $sql =
                    " INSERT INTO tbl_UsersProfiles"
                    ." (UserName"
                    ." ,ProfileName)"
                    ." VALUES"
                    ." ('$UserName'"
                    ." ,'".uc($profile)."')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION USERADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION USERADD");
        }
    } elsif($action =~ /update/i) {
        _updateDataFromSQL("BEGIN TRANSACTION USERUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_Users WHERE UserName = '$UserName'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "UPDATE tbl_Users SET"
                    ." UserDescr = '$NameDescr'"
                    ." ,EMailAddr = '$Mail'"
                    ." WHERE UserName = '$UserName'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "User $UserName doesn't exists";
            }

            $sql = "SELECT * FROM tbl_UserAliases WHERE UserName = '$UserName' AND UserAlias = '$UserId'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql =
                    " UPDATE tbl_UserAliases SET"
                    ." UserAliasDescr = '$IDDescr'"
                    ." WHERE UserName = '$UserName' AND UserAlias = '$UserId'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Alias $UserId for $UserName doesn't exists";
            }

            $sql =
                "DELETE tbl_UsersProfiles WHERE"
                ." UserName = '$UserName'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql);

            foreach my $profile(@Profiles) {
                $sql =
                    " INSERT INTO tbl_UsersProfiles"
                    ." (UserName"
                    ." ,ProfileName)"
                    ." VALUES"
                    ." ('$UserName'"
                    ." ,'".uc($profile)."')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION USERUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION USERUPDATE");
        }
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION USERDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            foreach my $profile(@Profiles) {
                $sql =
                    "DELETE tbl_UsersProfiles WHERE"
                    ." UserName = '$UserName'"
                    ." AND ProfileName = '".uc($profile)."'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
    
            $sql = "SELECT * FROM tbl_Users WHERE UserName = '$UserName'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "DELETE tbl_Users WHERE"
                    ." UserName = '$UserName'"
                    ." AND UserDescr = '$NameDescr'"
                    ." AND EmailAddr = '$Mail'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
        
                $sql = 
                    "DELETE tbl_UserAliases WHERE"
                    ." UserAlias = '$UserId'"
                    ." AND UserName = '$UserName'"
                    ." AND UserAliasDescr = '$IDDescr'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION USERDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION USERDELETE");
        }
    }

    $main::msgtolog .= " $actionMessage";
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getUsersProfiles($method, $reqident, $selection);
    }
}

sub downloadConfig {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $filter = undef;
    my $XLS;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $IndexEntry(@{$IndexEntries}) {
        if(!defined($IndexEntry->{'Columns'}->{'Command'})) {
            main::debug2log("Command no found: ".Dumper($IndexEntry));
            $filter = $IndexEntry;
            last;
        }
    }

    eval {
        require "$main::Application->{'XREPORT_HOME'}/bin/config2xls.pl";
        main::debug2log("$main::Application->{'XREPORT_HOME'}/bin/config2xls.pl");
        buildXLS('ALL', \$XLS);
        main::debug2log("Excel length: ".length($XLS));
    };
    if($@) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => "##########[ UPLOAD ERROR ]##########\n\n".$@}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getConfigReports($method, $reqident, $selection, $XLS);
    }
}

sub downloadConfig2 {
    my ($method, $reqident, $selection) = (shift, shift, shift);

    require "$main::Application->{'XREPORT_HOME'}/bin/config2xls.pl";
    main::debug2log("$main::Application->{'XREPORT_HOME'}/bin/config2xls.pl");
    my $XLS;
    buildXLS('ALL', \$XLS);

    main::debug2log("Excel length: ".length($XLS));
    my $buffsz = 57*76;
    my $XLS64 = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $XLS ) );

    my $actionMessage = "SUCCESS";
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];

    my $response = XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries,
        DocumentType => 'application/vnd.ms-excel',
        FileName => "downloadConfig.xls",
        Identity => "downloadConfig",
        DocumentBody => {content => "<![CDATA[$XLS64]]>"}
    });
  my $ctlen = length($response);
  $main::Response->Clear();
  $main::Response->{'buffer'} = 0;
  $main::Response->AddHeader('Content-Length' => $ctlen);
  
  my $t = 0;
  while ( $response ) {
    (my $buff, $response) = unpack("a32768 a*", $response);
    if($buff) {
        $main::Response->Write($buff);
        $t += length($buff);
    }
  }  
  main::debug2log("${method} $reqident $ctlen bytes of response sent to requester($t)");
  return $main::Response->Flush();
}

my $uploadResponse;

sub logIt {
  for (@_) {
    my $t = $_;
#    $t =~ s/\n/<br\/>/sg;
    $uploadResponse .= "$t\n";
  }
}

sub debug2log {
#   main::debug2log($_);
}

sub uploadConfig2 {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    $uploadResponse = "";
    my $uploadError = "";

    require "$main::Application->{'XREPORT_HOME'}/bin/loadExcelConfig.pl";
    main::debug2log("$main::Application->{'XREPORT_HOME'}/bin/loadExcelConfig.pl");
    my $DocumentBody = $selection->{request}->{DocumentBody};
    logIt("DB: ".length($DocumentBody));
    my $XLS = MIME::Base64::decode_base64($DocumentBody);
    logIt("Processing Started at ". localtime());
    main::debug2log("Processing Started at ". localtime());
    eval {
        parseXLSConfig('XReport::XReportWebIface',\$XLS);
        logIt("CeReport Upload Ended at " . localtime());
        main::debug2log("CeReport Upload Ended at " . localtime());
    };
#   if($@) {
#       $uploadError = "##########[ UPLOAD ERROR ]##########\n\n".$@;
#   }
    if($@) {
      $uploadError = "##########[ UPLOAD ERROR ]##########\n\n".$@;
    } else {
      $uploadResponse = "CONFIG APPLIED SUCCESSFULLY";
    }
    if($uploadError !~ /^\s*$/) {
        $uploadResponse = $uploadError."\n\n##########[ UPLOAD LOG ]##########\n\n".$uploadResponse;
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => 'SUCCESS'}
            ]
        },
        {
            Columns => [
                {colname => 'UploadResult', content => $uploadResponse}
            ]
        }
    ];

    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub _setXferFileA {
  my ($queuename, $progr, $datetime) = @_;

  $datetime = GetDateTime() unless ($datetime && $datetime =~ /^\d{14}$/);
  my ($curryear, $currday, undef) = unpack("a4a4a*", $datetime);

  my $storage = getConfValues('LocalPath')->{'L1'}."/IN";
  $storage =~ s/^file:\/\///;
  logIt("LocalPath is $storage");
  main::debug2log("LocalPath is $storage");
  if (! -e $storage."/".$curryear."/".$currday ) {
    if (! -e $storage."/".$curryear) {
        die "Unable to create $storage/$curryear ($!)\n" if(!mkdir $storage."/".$curryear);
    }
    if (! -e $storage."/".$curryear."/".$currday) {
        die "Unable to create $storage/$curryear/$currday ($!)\n" if(!mkdir $storage."/".$curryear."/".$currday);
    }
  }

  my $filen = "$curryear/$currday/$queuename.$datetime.$progr.DATA.TXT";
  
  return ($filen, $storage, $datetime);
}

sub uploadConfig {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;

    $uploadResponse = "";
    my $uploadError = "";
    my $JobReportName = $main::Application->{'cfg.xlscfgreportname'};
    main::debug2log("ReportName is $JobReportName");

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $blockDocumentBody = 0;
    foreach my $IndexEntry(@{$IndexEntries}) {
        if(!defined($IndexEntry->{'Columns'}->{'Command'})) {
            $filter = $IndexEntry;
            last;
        }
    }

    my $DocumentBody = $selection->{request}->{DocumentBody};
    logIt("DB: ".length($DocumentBody));
    my $XLS = MIME::Base64::decode_base64($DocumentBody);
    logIt("Processing Started at ". localtime());
    main::debug2log("Processing Started at ". localtime());
    eval {
        my $JobReportId = QCreate XReport::QUtil(
            SrvName        => "XReportWebIface",
            JobReportName  => $JobReportName,
            LocalFileName  => $JobReportName . $$ . '001',
            RemoteHostAddr => $main::Server->{'REMOTE_ADDR'},
            RemoteFileName => "UPLOADCONFIG.xls",
            XferStartTime  => "GETDATE()",
            XferMode       => 2,
            XferDaemon     => "XReportWebIface",
            Status         => 0,
            XferId         => 123,
        );
        my ($toFile, $todir, $datetime) = _setXferFileA($JobReportName, $JobReportId);
        my $toFile_ = "$todir/$toFile"; unlink glob("$toFile_\*");
        my $OUTPUT = gzopen("$toFile_\.PENDING", "wb") or die "ERROR AT OUTPUT GZOPEN for file \"$toFile\" $!";
        my $gzerr = $OUTPUT->gzwrite($XLS);
        $OUTPUT->gzclose();
        if($gzerr <= 0) {
            main::debug2log("GZ Write error: $gzerr");
            logIt("GZ Write error: $gzerr");
            die("GZ Write error: $gzerr");
        }

        my $rc = rename("$toFile_\.PENDING", "$toFile_\.gz");
        if ( !$rc ) {
          logIt("RENAME Error ($toFile_\.PENDING", "$toFile_\.gz) $!");
          main::debug2log("RENAME Error ($toFile_\.PENDING", "$toFile_\.gz) $!");
          die ("FileError: RENAME Error $!");
        }
        $toFile .= ".gz";
        
        main::debug2log("TRANSFER ENDED NORMALLY FROM UPLOADED FILE", " INTO \"$toFile_\.gz\""); 
        
        my $req = QUpdate XReport::QUtil(
            XferEndTime    => 'GETDATE()',
            Status         => 16,
            JobName        => 'XRUPLOADXLSCFG',
            JobNumber      => "UPLOAD$JobReportId",
            JobReportName  => $JobReportName,
            XferRecipient  => '',
            RemoteFileName => "UPLOADCONFIG.xls",
            LocalFileName  => $toFile,
            XferPages      => 0,
            XferInBytes    => length($XLS),
            XferMode       => 2,
            Id             => $JobReportId,
        );

        logIt("CeReport Upload Ended at " . localtime());
        main::debug2log("CeReport Upload Ended at " . localtime());
    };
    if($@) {
      $uploadError = "##########[ UPLOAD ERROR ]##########\n\n".$@;
    } else {
      $uploadResponse = "CONFIG SUCCESSFULLY QUEUED FOR PROCESSING";
      $actionMessage = "SUCCESS";
    }
    if($uploadError !~ /^\s*$/) {
        $uploadResponse = $uploadError."\n\n##########[ UPLOAD LOG ]##########\n\n".$uploadResponse;
    }

    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];

        my $respToBuild = {
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        };

        my $buffsz = 57*76;
        $uploadResponse = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $uploadResponse ) );
        $respToBuild->{'DocumentType'} = 'text/xml';
        $respToBuild->{'FileName'} = "xrmonitor.xml";
        $respToBuild->{'Identity'} = "ConfigReportsData";
        $respToBuild->{'DocumentBody'} = {content => "<![CDATA[$uploadResponse]]>"};

        $main::Response->Write(XReport::SOAP::buildResponse($respToBuild));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getConfigReports($method, $reqident, $selection, $uploadResponse);
    }
}

sub recoveryConfig {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    $uploadResponse = "";
    my $uploadError = "";

    require "$main::Application->{'XREPORT_HOME'}/bin/restoreLastConfig.pl";
    main::debug2log("$main::Application->{'XREPORT_HOME'}/bin/restoreLastConfig.pl");

    logIt("Processing Started at ". localtime());
    main::debug2log("Processing Started at ". localtime());
    eval {
        restoreLastConfig();
    };
    if($@) {
        $uploadError = "##########[ RECOVERY ERROR ]##########\n\n".$@;
    }
    logIt("CeReport Restore Ended at " . localtime());
    main::debug2log("CeReport Restore Ended at " . localtime());

    if($uploadError !~ /^\s*$/) {
        $uploadResponse = $uploadError."\n\n##########[ RECOVERY LOG ]##########\n\n".$uploadResponse;
    }

    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => 'SUCCESS'}
            ]
        },
        {
            Columns => [
                {colname => 'RecoveryResult', content => $uploadResponse}
            ]
        }
    ];

    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub getConfigReports {
    my ($method, $reqident, $selection, $documentBody) = (shift, shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";
    my $joinClause = "";
    my $JobReportName = $main::Application->{'cfg.xlscfgreportname'} || "XRXLSCFG";

    $whereClause = _addWhereClause($whereClause, "JobReportName like", $JobReportName, "'");
    $whereClause = _addWhereClause($whereClause, "tbl_JobReports.PendingOp NOT IN (", "12", "");
    $whereClause .= ")";
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;

        if($field_name =~ /^FromDate$/) {
            $whereClause = _addWhereClause($whereClause, "XferStartTime >=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            next;
        }
        if($field_name =~ /^ToDate$/) {
            $whereClause = _addWhereClause($whereClause, "XferStartTime <=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            next;
        }
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }
    
    my $sql = 
        "SELECT TOP 1000 '' AS Command, tbl_JobReports.* "
        ." FROM tbl_JobReports "
        ." $joinClause "
        ." $whereClause "
        ." ORDER BY JobReportId DESC"
    ;
    main::debug2log($sql);
    my $result;
    eval {
        $result = _getDataFromSQL($sql);
        if(!$result) {
            $actionMessage = "No configs found!";
        } else {
            $actionMessage = "SUCCESS";
        }
    };
    $actionMessage = "Search of configs failed: $@" if($@);
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
    my $respToBuild = {
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    };

    if($documentBody !~ /^\s*$/) {
        my $buffsz = 57*76;
        $documentBody = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $documentBody ) );
        $respToBuild->{'DocumentType'} = 'text/xml';
        $respToBuild->{'FileName'} = "xrmonitor.xml";
        $respToBuild->{'Identity'} = "JobReportsData";
        $respToBuild->{'DocumentBody'} = {content => "<![CDATA[$documentBody]]>"};
    }
    $main::Response->Write(XReport::SOAP::buildResponse($respToBuild));
}

sub manageConfigReports {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $documentBody = "";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $blockDocumentBody = 0;
    foreach my $IndexEntry(@{$IndexEntries}) {
        if(!defined($IndexEntry->{'Columns'}->{'Command'})) {
            main::debug2log("Command no found: ".Dumper($IndexEntry));
            $filter = $IndexEntry;
            next;
        }
        my $currentCommand = uc($IndexEntry->{'Columns'}->{'Command'}->{'content'});
        ($currentCommand) = split("/", $currentCommand) if($currentCommand =~ /\//);
        $currentCommand = uc($currentCommand);
        main::debug2log("Command is $currentCommand");
        my $jobReportId = $IndexEntry->{'Columns'}->{'JobReportId'}->{'content'};
        if(exists($excelConfigLineCommands->{$currentCommand})) {
            my $commandFunction = $excelConfigLineCommands->{$currentCommand};
            if(defined(&{$commandFunction})) {
                main::debug2log("Call function $commandFunction");
                my $func = \&{$commandFunction};
                ($actionMessage, my $data) = &$func($jobReportId, $IndexEntry);
                last if($actionMessage !~ /SUCCESS/);
                if($currentCommand =~ /^(VD|O)$/) {
                    $documentBody = $data;
                    $blockDocumentBody = 1;
                }
                $documentBody .= "\n############[JobReportId $jobReportId]############\n\n".$data."\n" if($data !~ /^\s*$/ and !$blockDocumentBody);
            }
        }
    }
    
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getConfigReports($method, $reqident, $selection, $documentBody);
    }
}

sub getPrintCopies{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";
    main::debug2log("FILTER_RULES ".Dumper($selection));
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    $IndexEntries->[0]->{'Columns'}->{'cmd_vv.VarSetName'}->{'content'} = 'PRTC:'.($IndexEntries->[0]->{'Columns'}->{'cmd_vv.VarSetName'} ? $IndexEntries->[0]->{'Columns'}->{'cmd_vv.VarSetName'}->{'content'}.'%' : "%");
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
    #if($field_name =~ /VarSetName/i){
    #   $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} = 'PRTC:'.$IndexEntries->[0]->{'Columns'}->{$field}->{'content'};    
    #}
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    my $sql = "SELECT  TOP 1000 substring(vv.VarSetName,6, len(vv.VarSetName)) as VarGroup, substring(vv.VarName, 0,CHARINDEX('/',vv.VarName)) as JobName , substring(vv.VarName,CHARINDEX('/',vv.VarName)+1, LEN(vv.VarName)) as ReportName, vv.VarValue FROM    tbl_VarSetsValues vv "
              ." $whereClause ";
     main::debug2log("SQL ".Dumper($sql));
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
    main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}


sub managePrintCopies {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;
    my $whereClause = "";
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }
    $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    my @VarSetNames =  split(/[;]/,$IndexEntries->[$entryPos]->{'Columns'}->{'VarSetName'}->{'content'});
    my @VarNames = split(/[;]/,$IndexEntries->[$entryPos]->{'Columns'}->{'VarName'}->{'content'});
   
    my @Clause;
    
    #foreach my $field(keys(%{$IndexEntries->[$entryPos]->{'Columns'}})) {
    #    my $field_name = $field;
    #
    #    $IndexEntries->[$entryPos]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
    #    $IndexEntries->[$entryPos]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
    #    $IndexEntries->[$entryPos]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;
    #
    #    if($IndexEntries->[$entryPos]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
    #        delete($IndexEntries->[$entryPos]->{'Columns'}->{$field_name});
    #        next;
    #    }
#   if($IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'} !~ /^$/){
#      next;
#   }
#        $field_name =~ s/^cmd_//;
#   main::debug2log($whereClause);
#   $whereClause = _addWhereClause($whereClause, "$field_name", $IndexEntries->[$entryPos]->{'Columns'}->{$field}->{'content'}, "'")
#            if($IndexEntries->[$entryPos]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
#    }
    
    my $NPrintCopies =  $IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'};
    #my @ReportGroups = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'});

    my $parent = "ROOT";
    

    if($action =~ /add/i) {
        my $count = 0; 
    my $result = undef;
    my $sql = "";
    foreach my $VarSetName (@VarSetNames){
            my $forClause = "";
        $forClause = "select 'PRTC:".$VarSetName."'" ;
        $forClause .= ", '".$VarNames[$count]."' " ;
        $forClause .= ", ' ".$NPrintCopies."' " ;
        push @Clause, $forClause;
        $sql = "SELECT * FROM tbl_VarSetsValues WHERE VarSetName = 'PRTC:$VarSetName' AND VarName = '".$VarNames[$count]."'";
            
        _updateDataFromSQL("BEGIN TRANSACTION COPIESADD");
        eval {
             $result = _getDataFromSQL($sql);
                     main::debug2log($sql);
             $whereClause = join (" UNION ALL ", @Clause); 
             main::debug2log("XXXXXXXXXXXXX".$whereClause);
        
            if(!$result) {
            $sql = "INSERT INTO tbl_VarSetsValues "
                ."$whereClause";
            main::debug2log($sql);
            $result= _updateDataFromSQL($sql); 
            } else {
            die "VarSetValues items PRTC:$VarSetName $VarNames[$count] already exists";
            }
        $count++;
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION COPIESADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION COPIESADD");
        }
    }
    
    } elsif($action =~ /update/i) {
         my $count = 0; 
     foreach my $VarSetName (@VarSetNames){
         my $forClause = "";
     $forClause = " ( VarSetName = 'PRTC:".$VarSetName."'" ;
     $forClause .= " AND  VarName = '".$VarNames[$count]."' )";
     push @Clause, $forClause;
     $count++;

    }
    $whereClause = _addWhereClause($whereClause, "" , join (" OR ", @Clause), "");
    _updateDataFromSQL("BEGIN TRANSACTION COPIESUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
        $sql = "UPDATE tbl_VarSetsValues SET VarValue = '$NPrintCopies'". $whereClause;
        main::debug2log($sql);
        #$sql = "SELECT * FROM tbl_VarSetsValues WHERE VarSetName = '$SetNameOld' AND VarName = '$NameOld' AND VarValue = '$ValueOld'";
        $result = _updateDataFromSQL($sql);
        
            
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION COPIESUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION COPIESUPDATE");
        }
    } elsif($action =~ /del/i) {
         my $count = 0; 
     foreach my $VarSetName (@VarSetNames){
            my $forClause = "";
        $forClause = " ( VarSetName = 'PRTC:".$VarSetName."'" ;
        $forClause .= " AND  VarName = '".$VarNames[$count]."' )";
        push @Clause, $forClause;
        $count++;

    }
    $whereClause = _addWhereClause($whereClause, "" , join (" OR ", @Clause), "");
        _updateDataFromSQL("BEGIN TRANSACTION COPIESDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            if(!$result) {
                $sql = "DELETE tbl_VarSetsValues $whereClause";
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION COPIESDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION COPIESDELETE");
        }
    }

    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getPrintCopies($method, $reqident, $selection);
    }
}

sub getFilterRules{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "WHERE   (NOT (VarName = ':REPORTNAME')) AND (NOT (VarName LIKE 'SVC:\%')) AND NOT VarSetName like 'PRTC:%' AND NOT VarName = ':BundleIncludePattern' AND NOT VarName = ':BundleExcludePattern' ";
    main::debug2log("FILTER_RULES ".Dumper($selection));
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    my $sql = "SELECT  TOP 1000 vv.VarSetName as VarGroup, vv.VarName, vv.VarValue "
          ."FROM    tbl_VarSetsValues vv "
              ." $whereClause ";
     main::debug2log("SQL ".Dumper($sql));
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
    main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}


sub manageFilterRules {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }

    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    my $SetName = uc($IndexEntries->[$entryPos]->{'Columns'}->{'SetName'}->{'content'});
    my $Name = $IndexEntries->[$entryPos]->{'Columns'}->{'Name'}->{'content'};
    my $Value =  $IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'};
    my $SetNameOld = uc($IndexEntries->[$entryPos]->{'Columns'}->{'_SetName'}->{'content'});
    my $NameOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_Name'}->{'content'};
    my $ValueOld =  $IndexEntries->[$entryPos]->{'Columns'}->{'_Value'}->{'content'};
    #my @ReportGroups = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'});

    my $parent = "ROOT";
    
    $main::msgtolog .= "$action filter rule $SetName;$Name;$Value";
    if($action =~ /add/i) {
        _updateDataFromSQL("BEGIN TRANSACTION FILTERADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_VarSetsValues WHERE VarSetName = '$SetName' AND VarName = '$Name' AND VarValue = '$Value'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_VarSetsValues"
                    ." (VarSetName"
                    ." ,VarName"
                    ." ,VarValue)"
                    ." VALUES"
                    ." ('$SetName'"
                    ." ,'$Name'"
                    ." ,'$Value')"          
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "VarSetValues item $SetName $Name $Value already exists";
            }

        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION FILTERADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION FILTERADD");
        }
    } elsif($action =~ /update/i) {
        _updateDataFromSQL("BEGIN TRANSACTION FILTERUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_VarSetsValues WHERE VarSetName = '$SetNameOld' AND VarName = '$NameOld' AND VarValue = '$ValueOld'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "DELETE tbl_VarSetsValues WHERE"
                    ." VarSetName = '$SetNameOld'"
                    ." AND VarName = '$NameOld'"
                    ." AND VarValue = '$ValueOld'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
            $sql = 
                    "INSERT INTO tbl_VarSetsValues"
                    ." (VarSetName"
                    ." ,VarName"
                    ." ,VarValue)"
                    ." VALUES"
                    ." ('$SetName'"
                    ." ,'$Name'"
                    ." ,'$Value')"
                ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 
            
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION FILTERUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION FILTERUPDATE");
        }
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION FILTERDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            if(!$result) {
                $sql = 
                    "DELETE tbl_VarSetsValues WHERE"
                    ." VarSetName = '$SetName'"
                    ." AND VarName = '$Name'"
                    ." AND VarValue = '$Value'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION FILTERDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION FILTERDELETE");
        }
    }

    $main::msgtolog .= " $actionMessage";
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getFilterRules($method, $reqident, $selection);
    }
}


sub getReportGroups{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    my $sql = "SELECT TOP 1000  rg.ReportGroupId as ReportGroup, rg.ReportRule as ReportSet, "
                  ."rg.FilterVar as VarName, rg.FilterRule as VarGroup, rg.RecipientRule as RecipientName "
          ."FROM    tbl_NamedReportsGroups rg"
              ." $whereClause ";
    
      main::debug2log(Dumper($whereClause));
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub manageReportGroups{

    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;
    main::debug2log("REPORT GROUPS ".Dumper($selection));
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }

    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    my $GroupOld = uc($IndexEntries->[$entryPos]->{'Columns'}->{'_Group'}->{'content'});
    my $SetOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_Set'}->{'content'};
    my $NameOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_Name'}->{'content'};
    my $RuleOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_Rule'}->{'content'};
    my $RecRuleOld =  $IndexEntries->[$entryPos]->{'Columns'}->{'_RecRule'}->{'content'};
    my $Group = uc($IndexEntries->[$entryPos]->{'Columns'}->{'Group'}->{'content'});
    my $Set = $IndexEntries->[$entryPos]->{'Columns'}->{'Set'}->{'content'};
    my $Name = $IndexEntries->[$entryPos]->{'Columns'}->{'Name'}->{'content'};
    my $Rule = $IndexEntries->[$entryPos]->{'Columns'}->{'Rule'}->{'content'};
    my $RecRule =  $IndexEntries->[$entryPos]->{'Columns'}->{'RecRule'}->{'content'};
    #my @ReportGroups = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'});

    my $parent = "ROOT";
    
    $main::msgtolog .= "$action reportgroup $Group;$Set;$Name;$Rule;$RecRule";
    if($action =~ /add/i) {
        _updateDataFromSQL("BEGIN TRANSACTION GROUPADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_NamedReportsGroups WHERE ReportGroupId = '$Group' AND ReportRule = '$Set' AND FilterVar = '$Name' 
                AND FilterRule = '$Rule' AND RecipientRule = '$RecRule'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_NamedReportsGroups"
                    ." (ReportGroupId"
                    ." ,ReportRule"
                    ." ,FilterVar"
                    ." ,FilterRule"
                    ." ,RecipientRule)"
                    ." VALUES"
                    ." ('$Group'"
                    ." ,'$Set'"
                    ." ,'$Name'"
                    ." ,'$Rule'"
                    ." ,'$RecRule')"
                    
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Report Group item $Group $Set $Name $Rule already exists";
            }

        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION GROUPADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION GROUPADD");
        }
    } elsif($action =~ /update/i) {
        _updateDataFromSQL("BEGIN TRANSACTION GROUPUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
                $sql = "SELECT * FROM tbl_NamedReportsGroups WHERE ReportGroupId = '$GroupOld' AND ReportRule = '$SetOld' AND FilterVar = '$NameOld' 
                AND FilterRule = '$RuleOld' AND RecipientRule = '$RecRuleOld'";
            $result = _getDataFromSQL($sql);
            if($result) {
                    $sql = "DELETE tbl_NamedReportsGroups WHERE
                        ReportGroupId = '$GroupOld' AND ReportRule = '$SetOld'
                        AND FilterVar = '$NameOld' AND FilterRule = '$RuleOld'
                        AND RecipientRule = '$RecRuleOld'" ;
                    main::debug2log($sql);
                $result = _updateDataFromSQL($sql);

                #if(!$result) {
                        $sql = 
                        "INSERT INTO tbl_NamedReportsGroups"
                        ." (ReportGroupId"
                        ." ,ReportRule"
                        ." ,FilterVar"
                        ." ,FilterRule"
                        ." ,RecipientRule)"
                        ." VALUES"
                        ." ('$Group'"
                        ." ,'$Set'"
                        ." ,'$Name'"
                        ." ,'$Rule'"
                        ." ,'$RecRule')"
                        
                    ;
                    main::debug2log($sql);
                    $result = _updateDataFromSQL($sql); 
                    #} else {
                    #die "Report Group item $Group $Set $Name $Rule $RecRule doesn't exists";
                    #}



            } else {
                die "Item $Group $Set $Name $Rule doesn't exists";
            }

        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION GROUPUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION GROUPUPDATE");
        }
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION GROUPDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            if(!$result) {
                $sql = "DELETE tbl_NamedReportsGroups WHERE
                    ReportGroupId = '$Group' AND ReportRule = '$Set'
                    AND FilterVar = '$Name' AND FilterRule = '$Rule'
                    AND RecipientRule = '$RecRule'" ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION GROUPDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION GROUPDELETE");
        }
    }

    $main::msgtolog .= " $actionMessage";
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getReportGroups($method, $reqident, $selection);
    }
}

sub getBundlesOper{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED" ; my $whereClause = "";
    main::debug2log("get BundlesOper OHOH ".Dumper($selection));
    
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
    my $field_name = $field;

    $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
    $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
    $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

    if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
    }
    $field_name =~ s/^cmd_//;
    $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    main::debug2log("get Clause OHOH ".Dumper($whereClause));
    my $sql = "select top 1000 SUBSTRING( MAX(VarValue), 22, len(MAX(VarValue))) as Include, case when MIN(VarValue) <> MAX(VarValue) THEN SUBSTRING( MIN(VarValue), 22, len(MIN(VarValue))) ELSE '' END as Exclude, ".
              "BundleSkel, BundleOrder, BundleCategory, BundleWriter, BundleUdata, BundleDescr, BundleUdest, InputMember, BundleName, BundleDest, BundleClass, BundleForm, BundlePrtopt, BundleKey from ".
          "( select VSV.VarSetName, VSV.VarName+' '+ ".
           " stuff(( ".
               "select distinct ';' + cast(t.VarValue as varchar(max)) ".
           "from tbl_VarSetsValues t ".
               "where t.VarName = VSV.VarName and t.VarSetName = VSV.VarSetName ".
               "for xml path('') ".
           "), 1, 1, '') as VarValue , ".
          " BundleSkel, BundleOrder, BundleCategory, BundleWriter, BundleUdata, BundleDescr, BundleUdest, InputMember, BundleName, BundleDest, BundleClass, BundleForm, BundlePrtopt, BundleKey ".
          " from tbl_bundlesNames BN ".
              " INNER JOIN tbl_VarSetsValues VSV on  VSV.VarSetName = BN.BundleKey ".
              " AND VarName like ':Bundle%Pattern'".
              " $whereClause ".
          " group by VSV.VarSetName, VSV.VarName ,BundleSkel, BundleOrder, BundleCategory, BundleWriter, BundleUdata, BundleDescr, BundleUdest, InputMember, BundleName, BundleDest, BundleClass, BundleForm, BundlePrtopt, BundleKey ) A ".
          " group by VarSetName ,BundleSkel, BundleOrder, BundleCategory, BundleWriter, BundleUdata, BundleDescr, BundleUdest, InputMember, BundleName, BundleDest, BundleClass, BundleForm, BundlePrtopt, BundleKey ";
    
    main::debug2log(Dumper($sql));
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
    ];
       
    push @{$respEntries}, @{$result} if($result);
    #main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
  
}


sub manageBundles{

    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;
    main::debug2log("BUNDLES ".Dumper($selection));
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }
    
    
    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    main::debug2log("*************ACTION ".$action);
    my $BundleSkelOld = uc($IndexEntries->[$entryPos]->{'Columns'}->{'_BundleSkel'}->{'content'});
    #my $BundleOrderOld = uc($IndexEntries->[$entryPos]->{'Columns'}->{'_BundleOrder'}->{'content'});
    my $BundleCategoryOld = uc($IndexEntries->[$entryPos]->{'Columns'}->{'_BundleCategory'}->{'content'});
    #my $BundleWriterOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleWriter'}->{'content'};
    #my $SetOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_Set'}->{'content'};
    #my $BundleUdataOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleUdata'}->{'content'};
    #my $BundleDescrOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleDescr'}->{'content'};
    #my $BundleUdestOld =  $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleUdest'}->{'content'};
    #my $InputMemberOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_InputMember'}->{'content'};
    my $BundleNameOld = uc($IndexEntries->[$entryPos]->{'Columns'}->{'_BundleName'}->{'content'});
    #my $BundleDestOld =  $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleDest'}->{'content'};
    #my $BundleClassOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleClass'}->{'content'};
    #my $BundleFormOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleForm'}->{'content'};
    #my $BundlePrtoptOld =  $IndexEntries->[$entryPos]->{'Columns'}->{'_BundlePrtopt'}->{'content'};
    my $BundleSkel = uc($IndexEntries->[$entryPos]->{'Columns'}->{'BundleSkel'}->{'content'});
    my $BundleOrder = $IndexEntries->[$entryPos]->{'Columns'}->{'BundleOrder'}->{'content'};
    my $BundleCategory = uc($IndexEntries->[$entryPos]->{'Columns'}->{'BundleCategory'}->{'content'});
    my $BundleWriter = $IndexEntries->[$entryPos]->{'Columns'}->{'BundleWriter'}->{'content'};
    my $BundleUdata = $IndexEntries->[$entryPos]->{'Columns'}->{'BundleUdata'}->{'content'};
    my $BundleDescr = $IndexEntries->[$entryPos]->{'Columns'}->{'BundleDescr'}->{'content'};
    my $BundleUdest =  $IndexEntries->[$entryPos]->{'Columns'}->{'BundleUdest'}->{'content'};
    my $InputMember = $IndexEntries->[$entryPos]->{'Columns'}->{'InputMember'}->{'content'};
    my $BundleName = uc($IndexEntries->[$entryPos]->{'Columns'}->{'BundleName'}->{'content'});
    my $BundleDest =  $IndexEntries->[$entryPos]->{'Columns'}->{'BundleDest'}->{'content'};
    my $BundleClass = $IndexEntries->[$entryPos]->{'Columns'}->{'BundleClass'}->{'content'};
    my $BundleForm = $IndexEntries->[$entryPos]->{'Columns'}->{'BundleForm'}->{'content'};
    my $BundlePrtopt =  $IndexEntries->[$entryPos]->{'Columns'}->{'BundlePrtopt'}->{'content'};
    
    #my $Set = $IndexEntries->[$entryPos]->{'Columns'}->{'Set'}->{'content'};
    #my $Name = $IndexEntries->[$entryPos]->{'Columns'}->{'Name'}->{'content'};
    #my @ValuesI = split(/[;\n]/, $IndexEntries->[$entryPos]->{'Columns'}->{'ValuesI'}->{'content'});
    #my @ValuesE = split(/[;\n]/ , $IndexEntries->[$entryPos]->{'Columns'}->{'ValuesE'}->{'content'});
    
    my $ValuesI =  $IndexEntries->[$entryPos]->{'Columns'}->{'ValuesI'}->{'content'};
    my $ValuesE =  $IndexEntries->[$entryPos]->{'Columns'}->{'ValuesE'}->{'content'};


    #my @ReportGroups = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'});

    my $parent = "ROOT";
     use Symbol;
    my $h = gensym();
    open($h, ">c:\\my.log") or die("Open error: $!\n");
    
    if($action =~ /add/i) {
        _updateDataFromSQL("BEGIN TRANSACTION BUNDLEADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_BundlesNames WHERE BundleKey =  '".$BundleName."_".$BundleCategory."_".$BundleSkel."'";
            main::debug2log($sql);
            print $h "INQUIRY".$sql;
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_BundlesNames"
                    ." ( BundleSkel,"
                    ." BundleOrder," 
                    ." BundleCategory,"
                    ." BundleWriter,"
                    ." BundleUdata,"
                    ." BundleDescr,"
                    ." BundleUdest,"
                    ." InputMember,"
                    ." BundleName,"
                    ." BundleDest,"
                    ." BundleClass,"
                    ." BundleForm,"
                    ." BundlePrtopt )" 
                    ." VALUES ( "
                    ." '$BundleSkel', "
                    ."  '$BundleOrder'," 
                    ."  '$BundleCategory', "
                    ."  '$BundleWriter' ,"
                    ."  '$BundleUdata', "
                    ."  '$BundleDescr', "
                    ."  '$BundleUdest', "
                    ."  '$InputMember', "
                    ."  '$BundleName', "
                    ."  '$BundleDest', "
                    ."  '$BundleClass', "
                    ."  '$BundleForm', "
                    ."  '$BundlePrtopt');";
                 
                
                main::debug2log($sql);
                print $h "PRIMA".$sql;
                
                        main::debug2log("*********".Dumper("CIAO"));    
                if (_updateDataFromSQL($sql)){
                    my $sqlin = "INSERT INTO tbl_VarSetsValues ";
                       $sqlin =  $sqlin.join( "\n UNION ALL " ,  map{ join("  " , "select", "'".$BundleName."_".$BundleCategory."_".$BundleSkel."' ," , "':BundleIncludePattern' , ", "'$_'" ) } grep !/^\s*$/ , split(/[;\n]+/ , $ValuesI)); 
                               $sqlin =  $sqlin."\n UNION ALL " if ($ValuesI && $ValuesE);
                       $sqlin =  $sqlin.join( "\n UNION ALL " , map{ join(" " , "select", "'".$BundleName."_".$BundleCategory."_".$BundleSkel."' ," , "':BundleExcludePattern' , ", "'$_'" )} grep !/^\s*$/ , split(/[;\n]+/ , $ValuesE )); 
                                      
                      print $h "DOPO_".Dumper($sqlin);
                      print $h "\nEND";
                
                
                      main::debug2log("############INSERTING PATTERNS VALUES###########\n".Dumper($sqlin)."\n###################################");
                     _updateDataFromSQL($sqlin);   
                
                }else {
                             die "Bundle item '$BundleName '_'$BundleCategory '_'$BundleSkel': unable to insert on db";
                        }
                 
            } else {
                die "Bundle item '$BundleName '_'$BundleCategory '_'$BundleSkel' already exists";
            }

        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION BUNDLEADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION BUNDLEADD");
        }
    } elsif($action =~ /update/i) {
    _updateDataFromSQL("BEGIN TRANSACTION BUNDLEUPD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_BundlesNames bn WHERE BundleKey =  '".$BundleNameOld."_".$BundleCategoryOld."_".$BundleSkelOld."'";
            main::debug2log($sql);
            print $h "INQUIRY".$sql;
            $result = _getDataFromSQL($sql);
            if($result) {
                main::debug2log("##################".Dumper($sql)."##################");
                $sql = "DELETE from tbl_BundlesNames where BundleKey =  '".$BundleNameOld."_".$BundleCategoryOld."_".$BundleSkelOld."' ;";
                $sql .= "DELETE from tbl_VarSetsValues where VarSetName = '".$BundleNameOld."_".$BundleCategoryOld."_".$BundleSkelOld."' ;";
                main::debug2log("##################".Dumper($sql)."##################");
                 _updateDataFromSQL($sql); 
            }
            $sql =  "INSERT INTO tbl_BundlesNames"
                    ." ( BundleSkel,"
                    ." BundleOrder," 
                    ." BundleCategory,"
                    ." BundleWriter,"
                    ." BundleUdata,"
                    ." BundleDescr,"
                    ." BundleUdest,"
                    ." InputMember,"
                    ." BundleName,"
                    ." BundleDest,"
                    ." BundleClass,"
                    ." BundleForm,"
                    ." BundlePrtopt )" 
                    ." VALUES ( "
                    ." '$BundleSkel', "
                    ."  '$BundleOrder'," 
                    ."  '$BundleCategory', "
                    ."  '$BundleWriter' ,"
                    ."  '$BundleUdata', "
                    ."  '$BundleDescr', "
                    ."  '$BundleUdest', "
                    ."  '$InputMember', "
                    ."  '$BundleName', "
                    ."  '$BundleDest', "
                    ."  '$BundleClass', "
                    ."  '$BundleForm', "
                    ."  '$BundlePrtopt');";
                 
                
            main::debug2log($sql);
            print $h "PRIMA\n".$sql;
            
            if (_updateDataFromSQL($sql)){
                  my $sqlin = "INSERT INTO tbl_VarSetsValues ";
                      
                  $sqlin =  $sqlin.join( "\n UNION ALL " ,  map{ join(" " , "select", "'".$BundleName."_".$BundleCategory."_".$BundleSkel."' ," , "':BundleIncludePattern' , ", "'$_'" ) }  grep !/^\s*$/ , split(/[;\n]+/ , $ValuesI)); 
                  $sqlin = $sqlin."\n UNION ALL " if ($ValuesI && $ValuesE);
                  $sqlin =  $sqlin.join( "\n UNION ALL " , map{ join(" " , "select", "'".$BundleName."_".$BundleCategory."_".$BundleSkel."' ," , "':BundleExcludePattern' , ", "'$_'" )}  grep !/^\s*$/ , split(/[;\n]+/ , $ValuesE)); 
                                  
                print $h "DOPO_\n".Dumper($sqlin);
                print $h "\nEND";
                
            
                 main::debug2log("##################".Dumper($sqlin)."##################");
                 _updateDataFromSQL($sqlin);   
                
            }else {
                         die "Bundle item '$BundleName '_'$BundleCategory '_'$BundleSkel': unable to insert on db";
                    }
                 
            # else {
            #   die "Bundle item '$BundleName '_'$BundleCategory '_'$BundleSkel' already exists";
            #}

        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION BUNDLEUPD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION BUNDLEUPD");
        }   
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION BUNDLEDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            main::debug2log("##################".Dumper($sql)."##################");
        $sql = "DELETE from tbl_BundlesNames where BundleKey =  '".$BundleName."_".$BundleCategory."_".$BundleSkel."' ;";
        $sql .= "DELETE from tbl_VarSetsValues where VarSetName = '".$BundleName."_".$BundleCategory."_".$BundleSkel."' ;";
                main::debug2log($sql);
        print $h "DELETE##########".$sql;
                _updateDataFromSQL($sql);
            
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION BUNDLEDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION BUNDLEDELETE");
        }
    }
    
    close($h);
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
     } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getBundlesOper($method, $reqident, $selection);
     }
}

sub getServices{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "WHERE (VarName LIKE 'SVC:%') ";

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    my $sql = "SELECT  TOP 1000 vv.VarSetName as ServiceGroup, SUBSTRING(vv.VarName, 5, len(vv.VarName)) AS ServiceName, vv.VarValue as Functions "
          ."FROM    tbl_VarSetsValues vv "
          
              ." $whereClause ";
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub getCommonReports {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    $whereClause = _addWhereClause($whereClause, "Status =", "18", "");
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;

        $field_name =~ s/^cmd_//;
        if($field_name =~ /^FromDate$/) {
            $whereClause = _addWhereClause($whereClause, "XferStartTime >=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            next;
        }
        if($field_name =~ /^ToDate$/) {
            $whereClause = _addWhereClause($whereClause, "XferStartTime <=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            next;
        }
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    my $sql = "SELECT TOP 300 * FROM tbl_jobreports $whereClause ORDER BY JobReportId DESC";
    main::debug2log("SQL is $sql");
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No reports found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

use XReport::Web::Service;

my $serviceStatusMapping = {
    -1 => "Not Available",
    0 => "Unknown",
    1 => "Stopped",
    2 => "Starting",
    3 => "Stopping",
    4 => "Running",
    5 => "Resuming",
    6 => "Pausing",
    7 => "Paused"
};

my $serviceStatusColor = {
    -1 => "0x4D6DF6",
    0 => "0x4D6DF6",
    1 => "0xF40707",
    2 => "0xF49D07",
    3 => "0xF49D07",
    4 => "0x07F41D",
    5 => "0xF49D07",
    6 => "0xF49D07",
    7 => "0xF49D07"
};

my $serviceLineCommands = {
    'S' => '_startService',
    'P' => '_stopService',
    'R' => '_recycleService',
    'Z' => '_resetService',
    'L' => '_getServiceLog'
};

my $workQueueStatusColor = {
    0 => "0x4D6DF6",
    1 => "0xF49D07",
    15 => "0xF40707",
    16 => "0x07F41D",
    17 => "0xF49D07",
    18 => "0x07F41D",
    31 => "0xF40707"
};

my $workQueueStatusMapping = {
    0 => "Accepted",
    1 => "Receiving",
    15 => "RecvError",
    16 => "Queued",
    17 => "Processing",
    18 => "Completed",
    31 => "Failed"
};

my $workQueueLineCommands = {
    'S' => '_suspendQueue',
    'A' => '_activateQueue',
    'P' => '_deleteQueue',
    'U' => '_updateRows'
};

my $jobReportsLineCommands = {
    'R' => '_requeueJobreports',
    'P' => '_deleteJobreports',
    'I' => '_defineJobreports',
#   'IC' => '_commitDefineJobreports',
    'L' => '_getJobReportLog',
    'V' => '_getASCIIPage',
    'VD' => '_downloadInput',
#   'VDC' => '_downloadInGz',
    'VX' => '_getHEXPage',
    'VA' => '_getCtrlFile',
    'O' => '_downloadOutput',
#   'E' => '_editJobreports',
#   'EC' => '_commitEditJobreports',
#   'EX' => '_editJobreportsTimes',
#   'EXC' => '_commitEditJobreportsTimes'
};

sub _getWorkQueueJob {
    my $serviceName = shift;

    my $sql = "SELECT tbl_WorkQueue.*, tbl_JobReports.JobReportName, tbl_JobReports.ElabStartTime FROM tbl_WorkQueue INNER JOIN tbl_JobReports ON tbl_WorkQueue.ExternalKey = tbl_JobReports.JobReportId WHERE tbl_WorkQueue.SrvName = '$serviceName'";
    my $result = "";

    my $dbr =  XReport::DBUtil::dbExecuteReadOnly($sql);

    if(!$dbr->eof()) {
        my $fields = $dbr->Fields();
        my $elabStartTime = "";
        $elabStartTime = $fields->Item('ElabStartTime')->Value()->Date('yyyy-MM-dd')." ".$fields->Item('ElabStartTime')->Value()->Time('hh:mm:ss tt') if($fields->Item('ElabStartTime')->Value());
        $result =
            $fields->Item('JobReportName')->Value()."(".
            $fields->Item('ExternalKey')->Value().") ".
            $elabStartTime;
#           $dbr->getFieldValue("JobReportName"). "(".
#           $dbr->getFieldValue("ExternalKey"). ") ".
#           $dbr->getFieldValue("ElabStartTime") if(!$dbr->eof())
        ;
    }

    return $result;
}

sub _startService {
    my ($host, $service) = (shift, shift);
    
    my $serviceHandler = XReport::Web::Service->new();
    my $result = $serviceHandler->changeServiceState($host, $service, 1);
    if($result == "") {
        return "SUCCESS";
    } else {
        return "Start of service $service on $host failed: $result";
    }
}

sub _stopService {
    my ($host, $service) = (shift, shift);
    
    my $serviceHandler = XReport::Web::Service->new();
    my $result = $serviceHandler->changeServiceState($host, $service, 4);
    if($result == "") {
        return "SUCCESS";
    } else {
        return "Stop of service $service on $host failed: $result";
    }
}

sub _recycleService {
    my ($host, $service) = (shift, shift);

    my $serviceHandler = XReport::Web::Service->new();
    my $details = $serviceHandler->getServiceDetails($host, $service);
    my $status = $details->{'status'};
    main::debug2log("Stop service $service on $host(status = $status)");
    my $result = _stopService($host, $service) if($status != 1 && $status != 3);
    main::debug2log("Stop result = $result");
    return $result if($result !~ /SUCCESS/ && $result !~ /^$/);
    main::debug2log("Start service $service on $host(status = $status)");
    $result = _startService($host, $service) if($status != 2 && $status != 4);
    main::debug2log("Start result = $result");
    return $result;
}

sub _resetService {
    my ($host, $service) = (shift, shift);

    my $serviceHandler = XReport::Web::Service->new();
    my $details = $serviceHandler->getServiceDetails($host, $service);
    my $status = $details->{'status'};

    main::debug2log("Stop service $service at host $host");
    my $result = _stopService($host, $service) if($status != 1 && $status != 3);
    return $result if($result !~ /SUCCESS/);

    my $logsDir = c::getValues('logsdir');
    main::debug2log("Log dir is $logsDir");
    if($logsDir =~ /^(.*)\/webappls$/) {
        $logsDir = $1;
        main::debug2log("Log dir changed, now is $logsDir");
    }
    my @now = localtime();
    main::debug2log("Rename $logsDir/$service.LOG to $logsDir/$service.LOG.".($now[5] + 1900).($now[4] + 1).$now[3]);
    return "Rotate of service log $logsDir/$service.LOG failed: $!" if(!rename($logsDir."/$service.LOG", $logsDir."/$service.LOG.".($now[5] + 1900).($now[4] + 1).$now[3]));

    main::debug2log("Start service $service at host $host");
    return _startService($host, $service) if($status != 2 && $status != 4);
}

sub getServices {
    my ($method, $reqident, $selection, $documentBody) = (shift, shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";
    my $selectedService = "";
    my $selectedServer = "";
    
    if($selection) {
        my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
        $selectedService = $IndexEntries->[0]->{'Columns'}->{'cmd_ServiceName'}->{'content'};
        $selectedServer = $IndexEntries->[0]->{'Columns'}->{'cmd_ServerName'}->{'content'};
    }

    $selectedService =~ s/^\s+//;
    $selectedService =~ s/\s+$//;
    $selectedServer =~ s/^\s+//;
    $selectedServer =~ s/\s+$//;

    my $serviceHandler = XReport::Web::Service->new();
    my $hosts = $serviceHandler->getHosts();
    my $result = undef;

    foreach my $host(sort(@{$hosts})) {
        next if($selectedServer !~ /^$/ and $host !~ /$selectedServer/i);
        my $services =  $serviceHandler->getServicesName($host);
    
        foreach my $service(sort(@{$services})) {
            next if($selectedService !~ /^$/ and $service !~ /$selectedService/i);
            my $details = $serviceHandler->getServiceDetails($host, $service);
            my $status = $details->{'status'};
            next if($status < 0);
            my $descr = $details->{'descr'};
            my $currentEntry = [
                {
                    Columns => [
                        {colname => 'Command', content => ""},
                        {colname => 'ServerName', content => $host},
                        {colname => 'ServiceName', content => $service},
                        {colname => 'ServiceDescr', content => $details->{'descr'}},
                        {colname => 'Status', content => $serviceStatusMapping->{$status}},
                        {colname => 'ActiveWork', content => _getWorkQueueJob($service)},
                        {colname => '_color', content => $serviceStatusColor->{$status}},
                    ]
                }
            ];
            
            $result = [] if(!$result);
            push @{$result}, @{$currentEntry};
        }
    }
    
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    my $respToBuild = {
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    };

    if($documentBody !~ /^\s*$/) {
        my $buffsz = 57*76;
        $documentBody = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $documentBody ) );
        $respToBuild->{'DocumentType'} = 'text/xml';
        $respToBuild->{'FileName'} = "xrmonitor.xml";
        $respToBuild->{'Identity'} = "ServicesLogs";
        $respToBuild->{'DocumentBody'} = {content => "<![CDATA[$documentBody]]>"};
    }
    $main::Response->Write(XReport::SOAP::buildResponse($respToBuild));
}

sub manageServices {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $documentBody = "";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $IndexEntry(@{$IndexEntries}) {
        if(!defined($IndexEntry->{'Columns'}->{'Command'})) {
            main::debug2log("Command no found: ".Dumper($IndexEntry));
            $filter = $IndexEntry;
            $actionMessage = "SUCCESS";
            next;
        }
        my $currentCommand = uc($IndexEntry->{'Columns'}->{'Command'}->{'content'});
        my $host = uc($IndexEntry->{'Columns'}->{'ServerName'}->{'content'});
        my $service = uc($IndexEntry->{'Columns'}->{'ServiceName'}->{'content'});
        if(exists($serviceLineCommands->{$currentCommand})) {
            my $commandFunction = $serviceLineCommands->{$currentCommand};
            if(defined(&{$commandFunction})) {
                my $func = \&{$commandFunction};
                ($actionMessage, my $data) = &$func($host, $service);
                last if($actionMessage !~ /SUCCESS/);
                $documentBody .= "\n############[LOG FOR $service ON $host]############\n\n".$data."\n" if($data !~ /^\s*$/);
            }
        }
    }
    
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getServices($method, $reqident, $selection, $documentBody);
    }
}

sub _getServiceLog {
    my ($host, $service) = (shift, shift);
    my $actionMessage = "FAILED";
    
    main::debug2log("view_log command");
    my $logsDir = c::getValues('logsdir');
    main::debug2log("Log dir is $logsDir");
    if($logsDir =~ /^(.*)\/webappls$/) {
        $logsDir = $1;
        main::debug2log("Log dir changed, now is $logsDir");
    }

    my $lines = _tail(100, $logsDir."/$service.LOG");
    $actionMessage = "SUCCESS";

    return ($actionMessage, $lines);
}

sub getServiceLog {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $service = $IndexEntries->[0]->{'Columns'}->{'ServiceName'}->{'content'};
    my $host = $IndexEntries->[0]->{'Columns'}->{'ServerName'}->{'content'};
    
    main::debug2log("view_log command");
    my $logsDir = c::getValues('logsdir');
    main::debug2log("Log dir is $logsDir");
    if($logsDir =~ /^(.*)\/webappls$/) {
        $logsDir = $1;
        main::debug2log("Log dir changed, now is $logsDir");
    }

    my $lines = _tail(100, $logsDir."/$service.LOG");
    my $buffsz = 57*76;
    $lines = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $lines ) );
        
    $actionMessage = "SUCCESS";
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage},
                {colname => 'ViewId', content => "servicesMenu"}
            ]
        }
    ];
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries,
        DocumentType => 'text/xml',
        FileName => "xrmonitor.xml",
        Identity => $host."_".$service,
        DocumentBody => {content => "<![CDATA[$lines]]>"}
    }));
}

sub _tail {
    my $lim = shift;
    my $file = shift;
    use Symbol;
    my $IN = gensym();

    my $offset = 2000;
    my $readLines;
    my @lines = qw();
    my $filesize = -s $file;

    main::debug2log("Opening file $file");
    if(!open($IN, "<$file")) {
        main::debug2log("Cannot open $file for input: $!");
        return "Log is empty";
    }
    
    while($#lines < $lim) {
        sysseek($IN, -$offset, 2);  
        sysread($IN, $readLines, $offset);
        @lines = split("\n", $readLines);
        last if($offset == $filesize);
        $offset *= 2;
        $offset = $filesize if($offset > $filesize);
    }

    close($IN);

    $lim = $#lines if($#lines < $lim);
    my @outputLines = qw();
    for(my $count = $#lines; $count >= ($#lines - $lim); $count--) {
        push @outputLines, $lines[$count];
    }

    return join("\n", reverse(@outputLines));
}

sub getWorkqueue {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        if($field_name =~ /^FromDate$/) {
            $whereClause = _addWhereClause($whereClause, "InsertTime >=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} . " 00:00:00", "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            next;
        }
        if($field_name =~ /^ToDate$/) {
            $whereClause = _addWhereClause($whereClause, "InsertTime <=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} . " 23:59:59", "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            next;
        }
        if($field_name =~ /^Suspend$/) {
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /false/) {
                $whereClause = _addWhereClause($whereClause, "(SrvName NOT LIKE 'SUSPENDED' OR SrvName IS", "NULL)", "");
                #$whereClause = _addWhereClause($whereClause, "SrvName IS", "NULL", "", "OR");
            }
            next;
        }
        next if($field_name =~ /^TargetName$/);
        next if($field_name =~ /^JobName$/);
        if($field_name =~ /^ExternalKey$/) {
            $whereClause = _addWhereClause($whereClause, "ExternalKey =", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            next;
        }
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    my $sql = "SELECT COUNT(*) as TotalRows FROM tbl_WorkQueue $whereClause";
    main::debug2log("$sql");
    my $result;
    my $totalRows = "";
    eval {
        $result = _getDataFromSQLNoFill($sql);
        if($result and !$result->eof()) {
            $totalRows = $result->Fields->Item("TotalRows")->{Value};
        }
    };
    $actionMessage = "<![CDATA[Search of queued jobs failed: $@]]>" if($@);

    $sql = 
        "SELECT TOP 3000 '' AS Command, "
        ."CASE "
        ." WHEN ExternalTableName = 'tbl_JobReports' THEN (SELECT JobReportName FROM tbl_JobReports WHERE JobReportId = ExternalKey) "
        ." WHEN ExternalTableName = 'tbl_JobSpool' THEN (SELECT PrintDest FROM tbl_JobSpool WHERE JobSpoolId = ExternalKey) "
        ." ELSE 'UNKNOWN' "
        ." END "
        ." AS TargetName, "
        ."CASE "
        ." WHEN ExternalTableName = 'tbl_JobReports' THEN (SELECT JobName FROM tbl_JobReports WHERE JobReportId = ExternalKey) "
        ." ELSE 'UNKNOWN' "
        ." END "
        ." AS JobName, * "
        ." FROM tbl_WorkQueue "
        ." $whereClause "
        ." ORDER BY Priority DESC, WorkId DESC"
    ;
    if(exists($IndexEntries->[0]->{'Columns'}->{'cmd_TargetName'})) {
        $sql = "SELECT * FROM ($sql) AS QueuedJobs WHERE TargetName like '".$IndexEntries->[0]->{'Columns'}->{'cmd_TargetName'}->{'content'}."'";
    }
    if(exists($IndexEntries->[0]->{'Columns'}->{'cmd_JobName'})) {
        if(exists($IndexEntries->[0]->{'Columns'}->{'cmd_TargetName'})) {
            $sql .= " AND JobName like '".$IndexEntries->[0]->{'Columns'}->{'cmd_JobName'}->{'content'}."'";
        } else {
            $sql = "SELECT * FROM ($sql) AS QueuedJobs WHERE JobName like '".$IndexEntries->[0]->{'Columns'}->{'cmd_JobName'}->{'content'}."'";
        }
    }
    $result;
    eval {
        $result = _getDataFromSQL($sql);
        if(!$result) {
            $actionMessage = "No jobs found!";
        } else {
            $actionMessage = "SUCCESS";
        }
    };
    $actionMessage = "<![CDATA[Search of queued jobs failed: $@]]>" if($@);
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage},
                {colname => 'TotalRows', content => $totalRows}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub manageWorkqueue {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $IndexEntry(@{$IndexEntries}) {
        if(!defined($IndexEntry->{'Columns'}->{'Command'})) {
            main::debug2log("Command no found: ".Dumper($IndexEntry));
            $filter = $IndexEntry;
            $actionMessage = "SUCCESS";
            next;
        }
        my $currentCommand = uc($IndexEntry->{'Columns'}->{'Command'}->{'content'});
        my $externalKey = $IndexEntry->{'Columns'}->{'ExternalKey'}->{'content'};
        my $externalTableName = $IndexEntry->{'Columns'}->{'ExternalTableName'}->{'content'};
        if(exists($workQueueLineCommands->{$currentCommand})) {
            my $commandFunction = $workQueueLineCommands->{$currentCommand};
            if(defined(&{$commandFunction})) {
                main::debug2log("Execute $commandFunction");
                my $func = \&{$commandFunction};
                $actionMessage = &$func($externalKey, $externalTableName, $IndexEntry);
                last if($actionMessage !~ /SUCCESS/);
            }
        } else {
            $actionMessage = "Command '$currentCommand' not defined";
        }
        
        main::debug2log($actionMessage);
    }
    
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getWorkqueue($method, $reqident, $selection);
    }
}

sub _suspendQueue {
    my ($externalKey, $externalTableName) = (shift, shift);
    
    my $sql = 
        "UPDATE tbl_WorkQueue SET SrvName = 'SUSPENDED' WHERE ".
        "ExternalKey = ".$externalKey.
        " AND ExternalTableName = '".$externalTableName."'"
    ;
    my $result = "";

    eval { $result = _updateDataFromSQL($sql); };
    if($@) {
        return "Suspend failed for externaKey $externalKey: $@";
    } else {
        return "SUCCESS";
    }
}

sub _activateQueue {
    my ($externalKey, $externalTableName) = (shift, shift);
        
    my $sql = 
        "UPDATE tbl_WorkQueue SET SrvName = NULL, Status = 16 WHERE ".
        "ExternalKey = ".$externalKey.
        " AND ExternalTableName = '".$externalTableName."'"
    ;
    my $result = "";

    eval {$result = _updateDataFromSQL($sql)};
    if($@) {
        return "Activate failed for externaKey $externalKey: $@";
    } else {
        return "SUCCESS";
    }
}

sub _deleteQueue {
    my ($externalKey, $externalTableName) = (shift, shift);
        
    my $sql = 
        "DELETE tbl_WorkQueue WHERE ".
        "ExternalKey = ".$externalKey.
        " AND ExternalTableName = '".$externalTableName."'"
    ;
    my $result = "";
    main::debug2log($sql);

    eval {$result = _updateDataFromSQL($sql)};
    if($@) {
        return "Delete failed for externaKey $externalKey: $@";
    } else {
        return "SUCCESS";
    }
}

sub _updateRows {
    my ($externalKey, $externalTableName, $data) = (shift, shift, shift);

    my $WorkClass = $data->{'Columns'}->{'WorkClass'}->{'content'};
    $WorkClass = "NULL" if($WorkClass =~ /^\s*$/);
    my $Priority = $data->{'Columns'}->{'Priority'}->{'content'};
    $Priority = "NULL" if($Priority =~ /^\s*$/);
    my $TOW = $data->{'Columns'}->{'TypeOfWork'}->{'content'};
    $TOW = "NULL" if($TOW =~ /^\s*$/);

    my $sql = 
        "UPDATE tbl_WorkQueue SET WorkClass = $WorkClass, Priority = $Priority, TypeOfWork = $TOW WHERE ".
        "ExternalKey = ".$externalKey.
        " AND ExternalTableName = '".$externalTableName."'"
    ;
    my $result = "";

    eval {$result = _updateDataFromSQL($sql)};
    if($@) {
        return "Update failed for externaKey $externalKey: $@";
    } else {
        return "SUCCESS";
    }
}

sub getJobReports {
    my ($method, $reqident, $selection, $documentBody) = (shift, shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";
    my $joinClause = "";

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        if($field_name =~ /^FromDate$/) {
            $whereClause = _addWhereClause($whereClause, "XferStartTime >=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} . " 00:00:00", "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            next;
        }
        if($field_name =~ /^ToDate$/) {
            $whereClause = _addWhereClause($whereClause, "XferStartTime <=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} . " 23:59:59", "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            next;
        }
        if($field_name =~ /^Undefined$/) {
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /true/) {
                $joinClause = "LEFT OUTER JOIN tbl_JobReportNames ON tbl_JobReports.JobReportName = tbl_JobReportNames.JobReportName";
                $whereClause = _addWhereClause($whereClause, "tbl_JobReportNames.JobReportName is", "NULL", "");
            }
            next;
        }
        if($field_name =~ /^OnError$/) {
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /true/) {
                $whereClause = _addWhereClause($whereClause, "tbl_JobReports.Status NOT IN (", "18,17,16,15,1,0", "");
                $whereClause .= ")";
            }
            next;
        }

        if($field_name =~ /^QueuedDelete$/) {
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /true/) {
                $whereClause = _addWhereClause($whereClause, "tbl_JobReports.PendingOp IN (", "12", "");
                $whereClause .= ")";
            } else {
                $whereClause = _addWhereClause($whereClause, "tbl_JobReports.PendingOp NOT IN (", "12", "");
                $whereClause .= ")";
            }
            next;
        }
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    $whereClause =~ s/\sJobReportName\slike/ tbl_JobReports.JobReportName like/ig;

    my $sql = "SELECT COUNT(*) as TotalRows FROM tbl_JobReports $whereClause";
    my $result;
    my $totalRows = "";
    eval {
        $result = _getDataFromSQLNoFill($sql);
        if($result and !$result->eof()) {
            $totalRows = $result->Fields->Item("TotalRows")->{Value};
        }
    };
    $actionMessage = "Search of queued jobs failed: $@" if($@);
    
    $sql = 
        "SELECT TOP 1000 '' AS Command, tbl_JobReports.* "
        ." FROM tbl_JobReports "
        ." $joinClause "
        ." $whereClause "
        ." ORDER BY JobReportId DESC"
    ;
    main::debug2log($sql);
    my $result;
    eval {
        $result = _getDataFromSQL($sql);
        if(!$result) {
            $actionMessage = "No jobs found!";
        } else {
            $actionMessage = "SUCCESS";
        }
    };
    $actionMessage = "Search of queued jobs failed: $@" if($@);
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage},
                {colname => 'TotalRows', content => $totalRows}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
    my $respToBuild = {
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    };

    if($documentBody !~ /^\s*$/) {
        my $buffsz = 57*76;
        $documentBody = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $documentBody ) );
        $respToBuild->{'DocumentType'} = 'text/xml';
        $respToBuild->{'FileName'} = "xrmonitor.xml";
        $respToBuild->{'Identity'} = "JobReportsData";
        $respToBuild->{'DocumentBody'} = {content => "<![CDATA[$documentBody]]>"};
    }
    $main::Response->Write(XReport::SOAP::buildResponse($respToBuild));
}

sub manageJobReports {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $documentBody = "";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $blockDocumentBody = 0;
    foreach my $IndexEntry(@{$IndexEntries}) {
        if(!defined($IndexEntry->{'Columns'}->{'Command'})) {
            main::debug2log("Command no found: ".Dumper($IndexEntry));
            $filter = $IndexEntry;
            $actionMessage = "SUCCESS";
            next;
        }
        my $currentCommand = uc($IndexEntry->{'Columns'}->{'Command'}->{'content'});
        ($currentCommand) = split("/", $currentCommand) if($currentCommand =~ /\//);
        $currentCommand = uc($currentCommand);
        main::debug2log("Command is $currentCommand");
        my $jobReportId = $IndexEntry->{'Columns'}->{'JobReportId'}->{'content'};
        if(exists($jobReportsLineCommands->{$currentCommand})) {
            my $commandFunction = $jobReportsLineCommands->{$currentCommand};
            if(defined(&{$commandFunction})) {
                main::debug2log("Call function $commandFunction");
                my $func = \&{$commandFunction};
                ($actionMessage, my $data) = &$func($jobReportId, $IndexEntry);
                last if($actionMessage !~ /SUCCESS/);
                if($currentCommand =~ /^(VD|O)$/) {
                    $documentBody = $data;
                    $blockDocumentBody = 1;
                }
                $documentBody .= "\n############[JobReportId $jobReportId]############\n\n".$data."\n" if($data !~ /^\s*$/ and !$blockDocumentBody);
            }
        }
    }
    
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getJobReports($method, $reqident, $selection, $documentBody);
    }
}

sub _requeueJobreports {
    my ($jobReportId, $data) = (shift, shift);
    
    my $sql =   
        "SELECT JobReportId, Status, TypeOfWork, LocalFileName FROM tbl_JobReports".
        " INNER JOIN tbl_JobReportNames ON tbl_JobReports.JobReportName = tbl_JobReportNames.JobReportName".
        " WHERE JobReportId = ".$jobReportId
    ;
    my $dbr;
    eval {$dbr = XReport::DBUtil::dbExecuteReadOnly($sql)};
    if($@) {
        return "Retrieve of JobReportName for JobReport $jobReportId failed: $@";
    }
    
    if($dbr and !$dbr->eof()) {
        my $status = $dbr->Fields->Item("Status")->{Value};
        my $localFileName = $dbr->Fields->Item("LocalFileName")->{Value};
        if($localFileName =~ /\.default/i) {
            return "Requeue failed for JobReport $jobReportId: filename shouldn't be .default";
        }
        if($status < 16) {
            return "Requeue failed for JobReport $jobReportId: status($status) must be greater than 15";
        }

        my $typeOfWork = $dbr->Fields->Item("TypeOfWork")->{Value};
        $sql = 
            "SELECT * FROM tbl_WorkQueue WHERE".
            " ExternalTableName = 'tbl_JobReports'".
            " AND ExternalKey = $jobReportId".
            " AND TypeOfWork = ".$typeOfWork
        ;
        $dbr = XReport::DBUtil::dbExecuteReadOnly($sql);
        if($dbr->eof() or !$dbr) {
            $sql =
                "INSERT INTO tbl_WorkQueue(".
                "ExternalTableName, ".
                "ExternalKey, ".
                "TypeOfWork, ".
                "InsertTime, ".
                "WorkClass, ".
                "Priority, ".
                "Status, ".
                "SrvName) VALUES(".
                "'tbl_JobReports', ".
                $jobReportId.", ".
                $typeOfWork.", ".
                "GETDATE(), ".
                "NULL, ".
                "1,".
                "16,".
                "NULL)"
            ;
            eval {my $result = _updateDataFromSQL($sql)};
            if($@) {
                return "Requeue failed for JobReport $jobReportId: $@";
            }
        } else {
            return "Requeue failed for JobReport $jobReportId: report is already queued";
        }
    } else {
        return "Requeue failed for JobReport $jobReportId: report is not defined";
    }

    return "SUCCESS";
}

sub _deleteJobreports {
    my ($jobReportId, $data) = (shift, shift);
    
    my $sql = 
        "UPDATE tbl_JobReports SET PendingOp = 12 WHERE ".
        "JobReportId = ".$jobReportId
    ;
    my $result = "";

    main::debug2log($sql);
    eval {$result = _updateDataFromSQL($sql)};
    if($@) {
        return "Update of PendingOp for JobReport $jobReportId failed: $@";
    }
    return _deleteQueue($jobReportId, 'tbl_JobReports');

#   my $jobreport = XReport::JobREPORT->Open($jobReportId, 0);
#   $jobreport->deleteAll();
}

sub _defineJobreports {
    my ($jobReportId, $data) = (shift, shift);

    my ($command, $keys) = split("/", $data->{'Columns'}->{'Command'}->{'content'});
    my @fields = split(",", $keys);
    my $reportData = {};
    foreach my $field(@fields) {
        my ($key, $value) = split("=", $field);
        $reportData->{$key} = $value;
    }
    main::debug2log(Dumper($reportData));

    _updateDataFromSQL("BEGIN TRANSACTION REPORTADD");
    eval {
        my $sql = "";
        my $result = undef;

        $sql = "SELECT JobReportName FROM tbl_JobReports WHERE JobReportId = $jobReportId";
        $result = _getDataFromSQLNoFill($sql);

        if(!$result or $result->eof()) {
            die "Cannot find JobReportName for JobReportId $jobReportId";
        }
        my $JobReportName = $result->Fields->Item("JobReportName")->{Value};
        my $LocalFileName = $result->Fields->Item("LocalFileName")->{Value};
        my $LocalPathId_IN = $result->Fields->Item("LocalPathId_IN")->{Value};
        my $LocalPath = c::getValues('LocalPath')->{$LocalPathId_IN ||= "L1"};
        
        my @ReportGroups = split(/[,;]/, $reportData->{'ReportGroup'}->{'content'});
        my $ReportName = uc($reportData->{'ReportName'}->{'content'}) || $JobReportName;
        my $ReportDescr = uc(dbEscape($reportData->{'ReportDescr'}->{'content'})) || "Description for Report $ReportName";
        my $TypeOfWork = uc($reportData->{'TypeOfWork'}->{'content'}) || 1;
        my $RetDays = uc($reportData->{'RetDays'}->{'content'}) || 180;
        my $MailTo = uc($reportData->{'MailTo'}->{'content'});
        my $ParseFileName = $reportData->{'ParseFileName'}->{'content'} || '*,NULL.XML';
        my $HasCc = $reportData->{'HasCc'}->{'content'} || 1;
        my $CharsPerLine = uc($reportData->{'CharsPerLine'}->{'content'}) || 133;
        my $LinesPerPage = uc($reportData->{'LinesPerPage'}->{'content'}) || 66;
        my $PageOrient = uc($reportData->{'PageOrient'}->{'content'}) || 'L';
        my $PageSize = uc($reportData->{'PageSize'}->{'content'}) || '';
        my $FontSize = uc($reportData->{'FontSize'}->{'content'}) || '[8.0 12.0]';
        my $FitToPage = $reportData->{'FitToPage'}->{'content'} || 1;
        my $ArchivioInput = uc($reportData->{'ArchivioInput'}->{'content'});
        my $ArchivioOutput = uc($reportData->{'ArchivioOutput'}->{'content'});
        my $CodePage = uc($reportData->{'CodePage'}->{'content'});
        my $ReportFormat = uc($reportData->{'ReportFormat'}->{'content'}) || 2;
        my $ElabFormat = uc($reportData->{'ElabFormat'}->{'content'}) || 1;
        my $WorkClass = uc($reportData->{'WorkClass'}->{'content'}) || 'NULL';
        my $FilterVar = uc($reportData->{'FilterVar'}->{'content'});
        my $t1Fonts = uc($reportData->{'t1Fonts'}->{'content'}) || 1;
        my $LaserAdjust = uc($reportData->{'LaserAdjust'}->{'content'});
        my $FormDef = uc($reportData->{'FormDef'}->{'content'});
        my $PageDef = uc($reportData->{'PageDef'}->{'content'});
        my $CharsDef = uc($reportData->{'CharsDef'}->{'content'});
        my $ReplaceDef = uc($reportData->{'ReplaceDef'}->{'content'});
        my $PrintControlFile = uc($reportData->{'PrintControlFile'}->{'content'});

        $sql = "SELECT * FROM tbl_JobReportNames WHERE JobReportName = '$ReportName'";
        $result = _getDataFromSQL($sql);
        if(!$result) {
            $sql = 
                "INSERT INTO tbl_JobReportNames"
                ." (JobReportName"
                ." ,JobReportDescr"
                ." ,TypeOfWork"
                ." ,HoldDays"
                ." ,MailTo"
                ." ,ParseFileName"
                ." ,HasCc"
                ." ,CharsPerLine"
                ." ,LinesPerPage"
                ." ,PageOrient"
                ." ,FontSize"
                ." ,FitToPage"
                ." ,TargetLocalPathId_IN"
                ." ,TargetLocalPathId_OUT"
                ." ,CodePage"
                ." ,ReportFormat"
                ." ,ElabFormat"
                ." ,IsActive"
                ." ,PrintFlag"
                ." ,ViewOnlineFlag"
                ." ,ActiveDays"
                ." ,ActiveGens"
                ." ,HoldGens"
                ." ,StorageClass"
                ." ,WorkClass)"
                ." VALUES"
                ." ('$ReportName'"
                ." ,'$ReportDescr'"
                ." ,$TypeOfWork"
                ." ,$RetDays"
                ." ,'$MailTo'"
                ." ,'$ParseFileName'"
                ." ,$HasCc"
                ." ,$CharsPerLine"
                ." ,$LinesPerPage"
                ." ,'$PageOrient'"
                ." ,'$FontSize'"
                ." ,$FitToPage"
                ." ,'$ArchivioInput'"
                ." ,'$ArchivioOutput'"
                ." ,'$CodePage'"
                ." ,$ReportFormat"
                ." ,$ElabFormat"
                ." ,1"
                ." ,'Y'"
                ." ,'Y'"
                ." ,5"
                ." ,5"
                ." ,30"
                ." ,''"
                ." ,$WorkClass)"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 
#       } else {
#           die "Report $ReportName already exists(JobReportNames)";
        }

        $sql = "SELECT * FROM tbl_ReportNames WHERE ReportName = '$ReportName'";
        $result = _getDataFromSQL($sql);
        if(!$result) {
            $sql = 
                "INSERT INTO tbl_ReportNames"
                ." (ReportName"
                ." ,ReportDescr"
                ." ,HoldDays"
                ." ,MailTo"
                ." ,FilterVar)"
                ." VALUES"
                ." ('$ReportName'"
                ." ,'$ReportDescr'"
                ." ,$RetDays"
                ." ,'$MailTo'"
                ." ,'$FilterVar')"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 
#       } else {
#           die "Report $ReportName already exists(ReportNames)";
        }

        $sql = "SELECT * FROM tbl_Os390PrintParameters WHERE JobReportName = '$ReportName'";
        my $result = _getDataFromSQL($sql);
        if(!$result) {
            $sql = 
                "INSERT INTO tbl_Os390PrintParameters"
                ." (JobReportName"
                ." ,t1Fonts"
                ." ,LaserAdjust"
                ." ,FormDef "
                ." ,PageDef "
                ." ,CharsDef "
                ." ,ReplaceDef "
                ." ,PageSize "
                ." ,PrintControlFile)"
                ." VALUES"
                ." ('$ReportName'"
                ." ,$t1Fonts"
                ." ,'$LaserAdjust'"
                ." ,'$FormDef'"
                ." ,'$PageDef'"
                ." ,'$CharsDef'"
                ." ,'$ReplaceDef'"
                ." ,'$PageSize'"
                ." ,'$PrintControlFile')"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 
#       } else {
#           die "Report $ReportName already exists(OS390PrintParameters)";
        }

        foreach my $ReportGroup(@ReportGroups) {
            $sql = "SELECT * FROM tbl_NamedReportsGroups WHERE ReportGroupId = '$ReportGroup'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_NamedReportsGroups"
                    ." (ReportGroupId"
                    ." ,FilterVar"
                    ." ,FilterRule"
                    ." ,RecipientRule"
                    ." ,ReportRule)"
                    ." VALUES"
                    ." ('$ReportGroup'"
                    ." ,''"
                    ." ,''"
                    ." ,''"
                    ." ,'$ReportGroup')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }

            $sql = 
                "INSERT INTO tbl_VarSetsValues"
                ." (VarSetName"
                ." ,VarName"
                ." ,VarValue)"
                ." VALUES"
                ." ('$ReportGroup'"
                ." ,':REPORTNAME'"
                ." ,'$ReportName')"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 
        }
        
        if($JobReportName !~ /^$ReportName$/i) {
            my $newLocalFileName = $LocalFileName;
            $newLocalFileName =~ s/$JobReportName/$ReportName/;
        
            $sql =
                "UPDATE tbl_JobReports SET JobReportName = '$ReportName', LocalFileName = '$newLocalFileName' ".
                "WHERE JobReportId = $jobReportId"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql);
        
            my $oldLocalFileName = "$LocalPath/IN/$LocalFileName";
            my $newLocalFileName = "$LocalPath/IN/$newLocalFileName";
        
            die "Cannot rename $oldLocalFileName to $newLocalFileName: $!" if(!rename($oldLocalFileName, $newLocalFileName));   
        }
    };
    if($@) {
        _updateDataFromSQL("ROLLBACK TRANSACTION REPORTADD");
        return "FAILED: $@";
    } else {
        _updateDataFromSQL("COMMIT TRANSACTION REPORTADD");
        return "SUCCESS";
    }
}

sub _getJobReportLog {
    my ($jobReportId, $data) = (shift, shift);
    my $actionMessage = "FAILED";
    my $result = "";

    my $log_contents;
    eval {
        my $jobReportHandle = XReport::JobREPORT->Open($jobReportId);
        my $output_archive = $jobReportHandle->get_OUTPUT_ARCHIVE();
        my $jobReportFiles = $output_archive->FileList();
        foreach(@{$jobReportFiles}) {
            my $currentFile = $_;
            next if(($currentFile !~ /log$/i) and ($currentFile !~ /stderr$/i) and ($currentFile !~ /stdout$/i));
            my $file_content = $output_archive->FileContents($currentFile);
            $log_contents->{$currentFile} = $file_content;
        }
    };
    if($@) {
        $actionMessage = "Log not available for JobReportId $jobReportId($@)";
    } else {
        main::debug2log("view_log command");
        $result =
            "Logs of JobReportId $jobReportId"
        ;

        foreach my $file(keys(%{$log_contents})) {
            $file =~ s/#/__/g;
            $result .= "\n\n".$file."\n\n";
            $result .= $log_contents->{$file}."\n";
        }
        main::debug2log($result);
        $actionMessage = "SUCCESS";
    }
    return ($actionMessage, $result);
}

sub getJobReportLog {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobReportId = $IndexEntries->[1]->{'Columns'}->{'JobReportId'}->{'content'};
    my $result = "";

    my $log_contents;
    eval {
        my $jobReportHandle = XReport::JobREPORT->Open($JobReportId);
        my $output_archive = $jobReportHandle->get_OUTPUT_ARCHIVE();
        my $jobReportFiles = $output_archive->FileList();
        foreach(@{$jobReportFiles}) {
            my $currentFile = $_;
            next if(($currentFile !~ /log$/i) and ($currentFile !~ /stderr$/i) and ($currentFile !~ /stdout$/i));
            my $file_content = $output_archive->FileContents($currentFile);
            $log_contents->{$currentFile} = $file_content;
        }
    };
    if($@) {
        $actionMessage = "Log not available for JobReportId $JobReportId($@)";
    } else {
        main::debug2log("view_log command");
        $result =
            "Logs of JobReportId $JobReportId"
        ;

        foreach my $file(keys(%{$log_contents})) {
            $file =~ s/#/__/g;
            $result .= "\n\n".$file."\n\n";
            $result .= $log_contents->{$file}."\n";
        }
        main::debug2log($result);
    
        my $buffsz = 57*76;
        $result = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $result ) );
            
        $actionMessage = "SUCCESS";
    }

    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage},
                {colname => 'ViewId', content => "jobreportsMenu"}
            ]
        }
    ];
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries,
        DocumentType => 'text/xml',
        FileName => "xrmonitor.xml",
        Identity => $JobReportId,
        DocumentBody => {content => "<![CDATA[$result]]>"}
    }));
}

sub _getASCIIPage {
    my ($jobReportId, $data) = (shift, shift);
    my $actionMessage = "FAILED";
    my $result = "";

    my $content;
    eval {
        $content = _getInputFile($jobReportId, \&_PutLine);
    };
    if($@) {
        $actionMessage = "Content not available for JobReportId $jobReportId($@)";
    } else {
        $actionMessage = "SUCCESS";
    }

    return ($actionMessage, $content);
}

sub getASCIIPage {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobReportId = $IndexEntries->[1]->{'Columns'}->{'JobReportId'}->{'content'};
    my $result = "";

    my $content;
    eval {
        $content = _getInputFile($JobReportId, \&_PutLine);
    };
    if($@) {
        $actionMessage = "Content not available for JobReportId $JobReportId($@)";
    } else {
        my $buffsz = 57*76;
        $result = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $content ) );
            
        $actionMessage = "SUCCESS";
    }

    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage},
                {colname => 'ViewId', content => "jobreportsMenu"}
            ]
        }
    ];
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries,
        DocumentType => 'text/plain',
        FileName => "asciipage.txt",
        Identity => $JobReportId,
        DocumentBody => {content => "<![CDATA[$result]]>"}
    }));
}

sub _PutLine {
    my ($page, $line) = (shift, shift);

    return ' '.substr($line->[0], 1);
}

sub _getInputFile {
    my $jobReportId = shift;
    my $outRtn = shift;
    return undef unless ref($outRtn) eq 'CODE';
    my $input_content;

    my $jobReportHandle = XReport::JobREPORT->Open($jobReportId);
    main::debug2log("JobReportId $jobReportId opened");
    my $jobReportINArchive = $jobReportHandle->get_INPUT_ARCHIVE();
    main::debug2log("JobReportINArchive opened");
    $jobReportHandle->setValues(ijrar => $jobReportINArchive, ojrar => undef, ElabContext => {});
    main::debug2log("JobReport setValues");
    my $input_handle = XReport::INPUT->new($jobReportHandle);
    main::debug2log("Input handle created");
    my $page = $input_handle->GetPage()->GetPageRectList(1,1,255,255);
    $input_content .= ($input_content ? "\x0c" : "");
    my @page_image = ();
    for ( @{$page} ) {
        my $newline = &$outRtn($page, $_);
        push @page_image, $newline;
    }
    $input_content .= join("\n", @page_image );
    $input_handle->Close();
  
    return $input_content;
}

sub _getCtrlFile {
    my ($jobReportId, $data) = (shift, shift);
    my $actionMessage = "FAILED";
    my $result = "";

    my $content;
    eval {
        my $jr = XReport::JobREPORT->Open($jobReportId);

        main::debug2log("JobReport opened");
        my ($LocalPathId_IN, $LocalFileName) = $jr->getValues(qw(LocalPathId_IN LocalFileName));
        main::debug2log("PathId_IN is $LocalPathId_IN");
        main::debug2log("LocalFileName is $LocalFileName");

        my $LocalPath = c::getValues('LocalPath')->{$LocalPathId_IN ||= "L1"};
        main::debug2log("LocalPath is $LocalPath");

        $LocalFileName = "$LocalPath/IN/".$jr->getFileName('INPUT');
        main::debug2log("LOCAL file name is $LocalFileName");
        if($LocalFileName =~ /^\s*file:\/\/(.*)$/i) {
            my $zipFileName = $1;
            $zipFileName =~ s/\.DATA\./\.CNTRL\./i;
            $zipFileName =~ s/\.gz$//i;
            my $zipHandle = gensym();
            main::debug2log("GZ file name is $zipFileName");
            open($zipHandle, "<$zipFileName");
            binmode($zipHandle);
            while(<$zipHandle>) {
                $content .= $_;
            }
            close($zipHandle);
        }
    };
    if($@) {
        $actionMessage = "Content not available for JobReportId $jobReportId($@)";
    } else {
        $actionMessage = "SUCCESS";
    }

    return ($actionMessage, $content);
}

sub getCtrlFile {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobReportId = $IndexEntries->[1]->{'Columns'}->{'JobReportId'}->{'content'};
    my $result = "";

    my $content;
    eval {
        $content = _getInputControlFile($JobReportId, \&_PutLine);
    };
    if($@) {
        $actionMessage = "Content not available for JobReportId $JobReportId($@)";
    } else {
        my $buffsz = 57*76;
        $result = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $content ) );
            
        $actionMessage = "SUCCESS";
    }

    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage},
                {colname => 'ViewId', content => "jobreportsMenu"}
            ]
        }
    ];
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries,
        DocumentType => 'text/plain',
        FileName => "ctrlfile.txt",
        Identity => $JobReportId,
        DocumentBody => {content => "<![CDATA[$result]]>"}
    }));
}

sub _getInputControlFile {
  my $jobReportId = shift;
  my $outRtn = shift;
  return undef unless ref($outRtn) eq 'CODE';
  
  my $input_content;
  
  eval {
    my $jobReportHandle = XReport::JobREPORT->Open($jobReportId);
    my $InGZObject = $jobReportHandle->get_INPUT_ARCHIVE();
    if($InGZObject->getFileName() =~ /^\s*file:\/\/(.*)$/i) {
      my $ctrl_file = $1;
      $ctrl_file =~ s/\.DATA\./\.CNTRL\./i;
      $ctrl_file =~ s/\.gz$//i;
      my $ctrl_handle = gensym();
      open($ctrl_handle, "<$ctrl_file") or die("Cannot open $ctrl_file: $!");
      while(<$ctrl_handle>) {
    my $newline = &$outRtn($_);
    $input_content .= $newline."<br>\n";
      }
      close($ctrl_handle);
    }
  };
  main::debug2log($@) if($@);
  
  return $input_content;
}

sub _getHEXPage {
    my ($jobReportId, $data) = (shift, shift);
    my $actionMessage = "FAILED";
    my $result = "";

    my $content;
    eval {
        $content = _getInputFile($jobReportId, \&_PutHexLine);
    };
    if($@) {
        $actionMessage = "Content not available for JobReportId $jobReportId($@)";
    } else {
        $actionMessage = "SUCCESS";
    }

    return ($actionMessage, $content);
}

sub getHEXPage {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobReportId = $IndexEntries->[1]->{'Columns'}->{'JobReportId'}->{'content'};
    my $result = "";

    my $content;
    eval {
        $content = _getInputFile($JobReportId, \&_PutHexLine);
    };
    if($@) {
        $actionMessage = "Content not available for JobReportId $JobReportId($@)";
    } else {
        my $buffsz = 57*76;
        $result = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $content ) );
            
        $actionMessage = "SUCCESS";
    }

    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage},
                {colname => 'ViewId', content => "jobreportsMenu"}
            ]
        }
    ];
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries,
        DocumentType => 'text/plain',
        FileName => "asciipage.txt",
        Identity => $JobReportId,
        DocumentBody => {content => "<![CDATA[$result]]>"}
    }));
}

sub _PutHexLine {
    my ($page, $line) = (shift, shift);

    my $rawline = substr($line->[0], 1);
    $line = join(" ", unpack((length($rawline) > 8 ? "(H8)*" : "H*"), $rawline)); 
    return "$line\n";
}

sub downloadInput {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobReportId = $IndexEntries->[0]->{'Columns'}->{'JobReportId'}->{'content'};
    my $result = "";

    my $content;
    eval {
        my $jr = XReport::JobREPORT->Open($JobReportId);

        main::debug2log("JobReport opened");
        my ($LocalPathId_IN, $LocalFileName) = $jr->getValues(qw(LocalPathId_IN LocalFileName));
        main::debug2log("PathId_IN is $LocalPathId_IN");
        main::debug2log("LocalFileName is $LocalFileName");

        my $LocalPath = c::getValues('LocalPath')->{$LocalPathId_IN ||= "L1"};
        main::debug2log("LocalPath is $LocalPath");

        $LocalFileName = "$LocalPath/IN/".$jr->getFileName('INPUT');
        main::debug2log("LOCAL file name is $LocalFileName");
        if($LocalFileName =~ /^\s*file:\/\/(.*)$/i) {
            my $zipFileName = $1;
            my $zipHandle = gensym();
            main::debug2log("GZ file name is $zipFileName");
            open($zipHandle, "<$zipFileName");
            binmode($zipHandle);
            while(<$zipHandle>) {
                $content .= $_;
            }
            close($zipHandle);
        }
    };
    if($@) {
        $actionMessage = "Content not available for JobReportId $JobReportId($@)";
    } else {
        my $buffsz = 57*76;
        $result = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $content ) );
            
        $actionMessage = "SUCCESS";
    }

    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries,
        DocumentType => 'application/x-gzip',
        FileName => "$JobReportId.IN.gz",
        Identity => $JobReportId,
        DocumentBody => {content => "<![CDATA[$result]]>"}
    }));
}

sub _downloadInput {
    my ($jobReportId, $data) = (shift, shift);
    my $actionMessage = "FAILED";
    my $result = "";

    my $content;
    eval {
        my $jr = XReport::JobREPORT->Open($jobReportId);

        main::debug2log("JobReport opened");
        my ($LocalPathId_IN, $LocalFileName) = $jr->getValues(qw(LocalPathId_IN LocalFileName));
        main::debug2log("PathId_IN is $LocalPathId_IN");
        main::debug2log("LocalFileName is $LocalFileName");

        my $LocalPath = c::getValues('LocalPath')->{$LocalPathId_IN ||= "L1"};
        main::debug2log("LocalPath is $LocalPath");

        $LocalFileName = "$LocalPath/IN/".$jr->getFileName('INPUT');
        main::debug2log("LOCAL file name is $LocalFileName");
            if($LocalFileName =~ /^\s*file:\/\/(.*)$/i) {
            my $zipFileName = $1;
            my $zipHandle = gensym();
            main::debug2log("GZ file name is $zipFileName");
            open($zipHandle, "<$zipFileName");
            binmode($zipHandle);
            while(<$zipHandle>) {
                $content .= $_;
            }
            close($zipHandle);
        }
    };
    if($@) {
        $actionMessage = "Content not available for JobReportId $jobReportId($@)";
    } else {
        $actionMessage = "SUCCESS";
    }

    return ($actionMessage, $content);
}

sub downloadOutput {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobReportId = $IndexEntries->[0]->{'Columns'}->{'JobReportId'}->{'content'};
    my $result = "";

    my $content;
    eval {
        my $jr = XReport::JobREPORT->Open($JobReportId);

        main::debug2log("JobReport opened");
        my ($LocalPathId_OUT, $LocalFileName) = $jr->getValues(qw(LocalPathId_OUT LocalFileName));
        main::debug2log("PathId_OUT is $LocalPathId_OUT");
        main::debug2log("LocalFileName is $LocalFileName");

        my $LocalPath = c::getValues('LocalPath')->{$LocalPathId_OUT ||= "L1"};
        main::debug2log("LocalPath is $LocalPath");

        $LocalFileName = "$LocalPath/OUT/".$jr->getFileName('ZIPOUT');
        main::debug2log("LOCAL file name is $LocalFileName");
        if($LocalFileName =~ /^\s*file:\/\/(.*)$/i) {
            my $zipFileName = $1;
            my $zipHandle = gensym();
            main::debug2log("ZIP file name is $zipFileName");
            open($zipHandle, "<$zipFileName");
            binmode($zipHandle);
            while(<$zipHandle>) {
                $content .= $_;
            }
            close($zipHandle);
        }
    };
    if($@) {
        $actionMessage = "Content not available for JobReportId $JobReportId($@)";
    } else {
        my $buffsz = 57*76;
        $result = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $content ) );
            
        $actionMessage = "SUCCESS";
    }

    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries,
        DocumentType => 'application/zip',
        FileName => "$JobReportId.OUT.zip",
        Identity => $JobReportId,
        DocumentBody => {content => "<![CDATA[$result]]>"}
    }));
}

sub _downloadOutput {
    my ($jobReportId, $data) = (shift, shift);
    my $actionMessage = "FAILED";
    my $result = "";

    my $content;
    eval {
        my $jr = XReport::JobREPORT->Open($jobReportId);

        main::debug2log("JobReport opened");
        my ($LocalPathId_OUT, $LocalFileName) = $jr->getValues(qw(LocalPathId_OUT LocalFileName));
        main::debug2log("PathId_OUT is $LocalPathId_OUT");
        main::debug2log("LocalFileName is $LocalFileName");

        my $LocalPath = c::getValues('LocalPath')->{$LocalPathId_OUT ||= "L1"};
        main::debug2log("LocalPath is $LocalPath");

        $LocalFileName = "$LocalPath/OUT/".$jr->getFileName('ZIPOUT');
        main::debug2log("LOCAL file name is $LocalFileName");
            if($LocalFileName =~ /^\s*file:\/\/(.*)$/i) {
            my $zipFileName = $1;
            my $zipHandle = gensym();
            main::debug2log("ZIP file name is $zipFileName");
            open($zipHandle, "<$zipFileName");
            binmode($zipHandle);
            while(<$zipHandle>) {
                $content .= $_;
            }
            close($zipHandle);
        }
    };
    if($@) {
        $actionMessage = "Content not available for JobReportId $jobReportId($@)";
    } else {
        $actionMessage = "SUCCESS";
    }

    return ($actionMessage, $content);
}

my @PageXref;
my $JobReportId;

sub _EXIT_SOAP_ERROR {
  my $respmsg = ('<?xml version="1.0" encoding="UTF-8"?>'
         . '<SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema/instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">'
         . '<SOAP-ENV:Body>'
         . '<SOAP-ENV:Fault>'
         . "<faultcode>$_[0]</faultcode>"
         . "<faultactor>".$main::servername.'/'.$_[1]."</faultactor>"
         . "<faultstring>$_[2]</faultstring>"
         . '</SOAP-ENV:Fault>'
         . '</SOAP-ENV:Body>'
         . '</SOAP-ENV:Envelope>'
        );
  main::debug2log("EXIT_SOAP_ERROR: ", $respmsg);

  $main::Response->{Status} = "500 Xreport Web Service Server Error";
  $main::Response->AddHeader("Connection", "close");
  $main::Response->Write( $respmsg );
  $main::Response->Flush();
  return undef;
}


sub _expListOfPages {
  use Data::Dumper;
  my $ListOfPages = shift; my (@ListOfPages, %ListOfPages);

#  my @LOP = split(/\D+/, $ListOfPages);
  for ( my @LOP = split(/\D+/, $ListOfPages); @LOP; ) {
#  while ( @LOP ) {
    my ($fm, $req) = (shift @LOP, shift @LOP); 

    my $to = $fm + $req - 1; my $found = 0;

#    for ($_=0; $_<scalar(@PageXref); $_+=3) {
#      if ( 
#        ( $fm >= $PageXref[$_+1] and $fm <= $PageXref[$_+2] ) or
#        ( $to >= $PageXref[$_+1] and $to <= $PageXref[$_+2] )
#      ) {
#        my $FileId = $PageXref[$_]; 
#        if ( !exists($ListOfPages{"$FileId"}) ) {
#          $ListOfPages{$FileId} = [$FileId];
#        }
#        my $fm_ = max($fm,$PageXref[$_+1]);
#        my $to_ = min($to,$PageXref[$_+2]);
#        push @{$ListOfPages{$FileId}}, $fm_, $to_- $fm + 1;
#        $found += $to_ - $fm_ + 1;
#      }
#    }
    foreach my $pel ( @PageXref ) {
      if ( ( $fm >= $pel->[1] and $fm <= $pel->[2] ) or
       ( $to >= $pel->[1] and $to <= $pel->[2] ) 
      ) {
        my $FileId = $pel->[0]; 
        $ListOfPages{$FileId} = [$FileId] if ( !exists($ListOfPages{"$FileId"}) );

        my $maxfm_ = max($fm,$pel->[1]);
        my $minto_ = min($to,$pel->[2]);
        push @{$ListOfPages{$FileId}}, $maxfm_, $minto_- $fm + 1;
        $found += $minto_ - $maxfm_ + 1;
      }
    }
#    die "$fm $to $req ====\n====". Dumper(\@PageXref) ."=====\n==== " . Dumper(\%ListOfPages) ."=====\n===== $found ======\n";
     die "NUMBER MISMATCH ". join("/", $JobReportId, $fm, $to, $req, $found) if ( $req != $found );
      _EXIT_SOAP_ERROR("Sender", "IdNotFound", "DOCUMENTI NON TROVATI IN ARCHIVIO per la chiave richiesta - $JobReportId $fm $to $req $found") if ( $req != $found ) ;
  }
  for my $FileId (sort(keys(%ListOfPages))) {
    my $fileList = $ListOfPages{$FileId};
    if ( 
      $fileList->[1] == $PageXref[$FileId]->[1]
      and $fileList->[2] ==  $PageXref[$FileId]->[2] - $PageXref[$FileId]->[1] + 1
     ) {
      push @ListOfPages, $FileId;
    }
    else {
      push @ListOfPages, $fileList;
    }
  }

#  die "====". $ListOfPages ."=====\n====". Dumper(\@PageXref) ."=====\n==== " . Dumper(\%ListOfPages) ."=====\n=====" . Dumper(\@ListOfPages) . "======\n";
  return \@ListOfPages
}

sub getReportEntryPdf {
      my ($method, $reqident, $selection) = (shift, shift, shift);

      my $respEntries = [];
      my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
      $JobReportId = $IndexEntries->[0]->{'Columns'}->{'JobReportId'}->{'content'};
      my $FromPage = $IndexEntries->[0]->{'Columns'}->{'FromPage'}->{'content'};
       
      my $ListOfPages =$IndexEntries->[0]->{'Columns'}->{'ListOfPages'}->{'content'}; 
      my $ReportID =$IndexEntries->[0]->{'Columns'}->{'ReportId'}->{'content'}; 
      my $JobReportHandle = XReport::JobREPORT->Open($JobReportId);
      my $output_archive = $JobReportHandle->get_OUTPUT_ARCHIVE();
      my $JobReportName = $JobReportHandle->getValues('JobReportName');
      
      # my $xfileName = "\$\$PageXref.$JobReportId.txt";
      # my $zipMember = $output_archive->ExistsFile( $xfileName ); 
      # my $ObjectLength;
      # $zipMember->desiredCompressionMethod('COMPRESSION_STORED');
      #  $ObjectLength = $zipMember->uncompressedSize();

      # $zipMember->rewindData();
      #my $lastChunkLength = 0; my $xrefV = "";
      #while( !$zipMember->readIsDone() ) {
      # my ($ref, $status) = $zipMember->readChunk( 32768 );
            # main::debug2log("xref: $ref");
            # if ( $status != AZ_OK and $status != AZ_STREAM_END ) {
              #        main::debug2log("READ ERROR rc=$status");
                    #        last;
                    #  }
            #  $xrefV .= $$ref;
            #  $lastChunkLength = length($$ref); 
            # }

            # $zipMember->endRead();
      # main::debug2log("---------------------------------- PageXref: $xrefV");
      # push @PageXref, ( map { $_ =~ /File=(\d+) +From=(\d+) +To=(\d+)/; [$1, $2, $3] } 
        # grep /File=\d+ +From=\d+ +To=\d+/, split("\n", $xrefV) );
            #main::debug2log("PAGE_XREF: ".Dumper(@PageXref));      
            # main::debug2log("LIST OF PAGES : ".$ListOfPages);
      #$ListOfPages = _expListOfPages($ListOfPages);
      #main::debug2log("getReportEntryPdf".$ListOfPages); 
      #  die "===>4 LOP: ". join('::', @{$ListOfPages->[0]}) . " <===\n";

      #if ( scalar(@$ListOfPages) == 1 and !ref($ListOfPages->[0]) ) {
      #    eval { ExtractAndSendObject( $ListOfPages->[0], $FileFormat ) };
      # }
      # else {
      #eval {
        
       require XReport::EXTRACT;
       require XReport::PDF::DOC;
       require MIME::Base64;

      my $DocumentData = XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $IndexEntries,
        DocumentType => 'application/pdf',
        FileName => "$JobReportName\.$JobReportId",
        Identity =>  $JobReportName.$JobReportId,
        DocumentBody => {content => "BASE64DOCUMENT"},
     });

       my ($msghdr, $msgtail) = (($DocumentData) =~ /^(.*<\/IndexEntries>).*(<\/RESPONSE>.*)$/s);
       my $query = XReport::EXTRACT->new();
       my $streamer = new B64streamer();
       $query->{docTo} = XReport::PDF::DOC->Create($streamer) ;
       $main::Response->Write($msghdr . '<DocumentBody>'); # serializer

       my $elist;
       my @list = split(/,/, $ListOfPages, 3);
        main::debug2log($DocumentData."////////////".substr($msghdr,0,20)."--------------PAGES: ".Dumper(\@list));   
       eval {$elist = $query->ExtractPages(
                       QUERY_TYPE => 'FROMPAGES',
                       PAGE_LIST => \@list ,
                       FORMAT => "PRINT",
                       ENCRYPT => 'YES',
                       OPTIMIZE => 1,
                       REQUEST_ID => "GetDocById.".$main::Session->{'SessionID'},
                       #                   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"}B64streamer
        
                     );
      };
      if ( scalar($elist) == 0 ) {
            _EXIT_SOAP_ERROR("Sender", "IdNotFound", "DOCUMENTI NON TROVATI IN ARCHIVIO per la chiave richiesta - $@");
            return 0;
         }
       
    main::debug2log("returnDocumentPages elist: ", " InputDoc: ", $streamer->{InputBytes}, " B64Streamer: ", $streamer->{OutputBytes});

    $main::Response->Write('</DocumentBody>'.$msgtail);
    $main::Response->Flush();
    #};
    #}
    #unlink("$spool/$FileName");
    #main::debug2log("len response:".length($pdfV));
    #my ($buffsize, $outbytes, $pdfContent) = (57*76, 0, undef);
    #main::debug2log("Start base64 encoding");
    #$pdfContent = $streamer->{OutputBytes};
    #$pdfContent = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsize)*", $pdfV ) );
    #$pdfContent = MIME::Base64::encode_base64($pdfV);
#   main::debug2log("End base64 encoding");
    
#   my $actionMessage = "SUCCESS";
#   my $respEntries = [
#       {
#           Columns => [
#               {colname => 'ActionMessage', content => $actionMessage}
#           ]
#       }
#   ];
    #$main::Response->AddHeader('Content-Length' => length($pdfContent));
#   $main::Response->Write(XReport::SOAP::buildResponse({
#       IndexName => 'XREPORTWEB',
#       IndexEntries => $respEntries,
#       DocumentType => 'application/pdf',
#       FileName => "$JobReportName\.$JobReportId",
#       Identity =>  $JobReportName.$JobReportId,
#       DocumentBody => {content => $pdfContent},
#    }));
    
     #my $response = XReport::SOAP::buildResponse({
     #  IndexName => 'XREPORTWEB',
     #  IndexEntries => $respEntries,
     #  DocumentType => 'application/pdf',
     #  FileName => "$JobReportName\.$JobReportId",
     #  Identity => $JobReportName.$JobReportId,
     #  DocumentBody => {content => "<![CDATA[$pdfContent]]>"},
     #});
#   main::debug2log("len response decoded:".length($pdfContent));
    #$main::Response->AddHeader("Content-Length", length($response));
    #my $t = 0;
    # while ( $response ) {
    #   (my $buff, $response) = unpack("a32768 a*", $response);
    #   if($buff) {
    #   $main::Response->Write($buff);
    #       $t += length($buff);
    #   }
    #   main::debug2log("len chunk:".$t);
    # }  
    # main::debug2log("${method} $reqident bytes of response sent to requester($t)");
    # return $main::Response->Flush();
        
}

sub GetNotes
{
    my ($method, $reqident, $selection) = (shift, shift, shift);

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    
    my $sql = 
        "SELECT JobReportId AS JobReport_ID, ReportId, NoteId, LastUpdateTime, AUTH_USER, textString FROM tbl_LogicalReportNotes ".
        "WHERE IsActive = 1 AND JobReportId = $JobReportID AND ReportId = $ReportID ".
        "ORDER BY JobReportId, ReportId, NoteId ASC";
    main::debug2log($sql);
    
    my $result = _getDataFromSQL($sql);
    my $actionMessage;
    if(!$result) {
        $actionMessage = "No data found!";
    } else {
        $actionMessage = "SUCCESS";
    }            
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'Notes',
        IndexEntries => $respEntries
    }));
}

sub InsertNote
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $Version = $IndexEntries->[0]->{'Columns'}->{'Version'}->{'content'};
    my $UpdateUser = $IndexEntries->[0]->{'Columns'}->{'UpdateUser'}->{'content'};
    my $Text = $IndexEntries->[0]->{'Columns'}->{'Text'}->{'content'};

    #my $sql = "SELECT COUNT(*) as TotalRows FROM tbl_LogicalReportNotes WHERE JobReportId = $JobReportID AND ReportId = $ReportID";
    #my $result;
    #my $totalRows = 0;
    #eval {
        #$result = _getDataFromSQLNoFill($sql);
        #if($result and !$result->eof()) {
            #$totalRows = $result->Fields->Item("TotalRows")->{Value};
        #}
    #};
    #$totalRows++;
    my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION INSNOTE");
    eval{
        my $sql = "INSERT INTO tbl_LogicalReportNotes (JobReportId, ReportId, NoteId, LastUpdateTime, AUTH_USER, textString, IsActive) ".
            "SELECT '$JobReportID', '$ReportID', MAX(NoteId) + 1, GETDATE(), '$UpdateUser', '$Text', 1 ".
            "FROM tbl_LogicalReportNotes WHERE JobReportId = '$JobReportID' AND ReportId = '$ReportID' ";
    
        #$sql = "INSERT INTO tbl_LogicalReportNotes (JobReportId, ReportId, NoteId, LastUpdateTime, AUTH_USER, textString, IsActive) VALUES ('$JobReportID', '$ReportID', '$totalRows', GETDATE(), '$UpdateUser', '$Text', 1) ";
        main::debug2log($sql);
        my $res = _updateDataFromSQL($sql);
     };
    if($@)
    {
        $actionMessage = "FAILED: $@";
        _updateDataFromSQL("ROLLBACK TRANSACTION INSNOTE");
    }
    else
    {
        $actionMessage = "SUCCESS";
        _updateDataFromSQL("COMMIT TRANSACTION INSNOTE");
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
  
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'Notes',
        IndexEntries =>  $respEntries
    }));
}

sub UpdateNote
{  
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $Version = $IndexEntries->[0]->{'Columns'}->{'Version'}->{'content'};
    my $Text = $IndexEntries->[0]->{'Columns'}->{'Text'}->{'content'};
    my $UpdateUser = $IndexEntries->[0]->{'Columns'}->{'UpdateUser'}->{'content'};

    my $DocumentData = {IndexName => 'SocietaCliente' };
    my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION UPDNOTE");
    eval{
        my $select = "SELECT JobReportId, ReportId, NoteId, LastUpdateTime, AUTH_USER, textString FROM tbl_LogicalReportNotes ".
                     "WHERE JobReportId = $JobReportID AND ReportId = $ReportID AND NoteId = $Version";

        my $result = _getDataFromSQL($select);
        if($result) {
            my $sql = "UPDATE tbl_LogicalReportNotes SET textString = '$Text', AUTH_USER = '$UpdateUser', LastUpdateTime = GETDATE() ".
            "WHERE JobReportId = $JobReportID AND ReportId = $ReportID AND NoteId = $Version";
            $result = _updateDataFromSQL($sql); 
        }
        else {
            die "Note doesn't exists($JobReportID, $ReportID, $Version)";
        }
    };
    if($@)
    {
        $actionMessage = "FAILED: $@";
        _updateDataFromSQL("ROLLBACK TRANSACTION UPDNOTE");
    }
    else
    {
        $actionMessage = "SUCCESS";
        _updateDataFromSQL("COMMIT TRANSACTION UPDNOTE");
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
  
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'Notes',
        IndexEntries => $respEntries
    }));
}

sub DeleteNote
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $Version = $IndexEntries->[0]->{'Columns'}->{'Version'}->{'content'};
    my $UpdateUser = $IndexEntries->[0]->{'Columns'}->{'UpdateUser'}->{'content'};
    my $VersionWhere = "";
    my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION DELNOTE");
    my $sql = "UPDATE tbl_LogicalReportNotes SET IsActive = 0, AUTH_USER = '$UpdateUser', LastUpdateTime = GETDATE() ".
              "WHERE JobReportId = $JobReportID AND ReportId = $ReportID ";
    $sql.= "AND NoteId = $Version" if ($Version != "-1");
    main::debug2log($sql);
    eval{ my $res = _updateDataFromSQL($sql); };
    if($@) {
        $actionMessage = "FAILED: $@";
        _updateDataFromSQL("ROLLBACK TRANSACTION DELNOTE");
    }
    else {
        $actionMessage = "SUCCESS";
        _updateDataFromSQL("COMMIT TRANSACTION DELNOTE");
    }
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'Notes',
        IndexEntries => [ { Columns => [ {colname => 'ActionMessage', content => $actionMessage} ] } ]
    }));
}

sub CheckReport
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $CheckUser = $IndexEntries->[0]->{'Columns'}->{'CheckUser'}->{'content'};
     my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION CHKREP");
    eval{
        my $sql = "UPDATE tbl_LogicalReports SET CheckStatus = 1, LastUsedToCheck = '$CheckUser' ".
                  "WHERE JobReportID = $JobReportID AND ReportID = $ReportID ";
    
        my $res = _updateDataFromSQL($sql);
    };
    if($@)
    {
        $actionMessage = "FAILED: $@";
        _updateDataFromSQL("ROLLBACK TRANSACTION CHKREP");
    }
    else
    {
        $actionMessage = "SUCCESS";
        _updateDataFromSQL("COMMIT TRANSACTION CHKREP");
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'Notes',
        IndexEntries => $respEntries
    }));
}

sub UpdateLastUserToView
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $CheckUser = $IndexEntries->[0]->{'Columns'}->{'CheckUser'}->{'content'};
     my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION UPDUSER");
    eval{
        my $sql = "UPDATE tbl_LogicalReports SET LastUsedToView = '$CheckUser' ".
                  "WHERE JobReportID = $JobReportID AND ReportID = $ReportID ";
        main::debug2log($sql);
        my $res = _updateDataFromSQL($sql)
      };
    if($@)
    {
        $actionMessage = "FAILED: $@";
        _updateDataFromSQL("ROLLBACK TRANSACTION UPDUSER");
    }
    else
    {
        $actionMessage = "SUCCESS";
        _updateDataFromSQL("COMMIT TRANSACTION UPDUSER");
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'Notes',
        IndexEntries => $respEntries
    }));
}

__PACKAGE__;