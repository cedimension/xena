
use XML::Simple;
use Pod::Simple::XMLOutStream;
use Data::Dumper;

my $fn = 'c:\home\Customers\Unicredit\workspace\xena\xreport_home\xrsitiweb\SoapServices\MainDbIFace.pm';

my $mm='';
my $p = Pod::Simple::XMLOutStream->new();
$p->accept_targets( qw(wsdldoc) );
$p->accept_directive_as_verbatim(qw(method doc));
$p->output_string(\$mm);
$p->parse_file($fn);
my @methods = qw(getDBRows getIndexRows);
#my $docstruct = XMLin($mm, ContentKey => '-content' );
my $wsdldata = { map { $_->{method}->{content} => $_->{doc}->{content} 
	             . ($_->{Verbatim}->{content} ? "\n\n" . $_->{Verbatim}->{content} : '' ) } 
	                               grep { $_->{target} =~ /^:wsdldoc/ } @{XMLin($mm, ContentKey => '-content' )->{for}} };

print 'MM: ', Dumper($docstruct), "\n";
print 'MDOC: ', Dumper($wsdldata), "\n"; 
print  '<wsdl:portType name="XRSOAPSERVERNAMEMethods">'
          . join('', map { '<wsdl:operation name="'.$_.'" >'
                         . ( !exists($wsdldata->{$_}) ? ''
                         : '<wsdl:documentation><![CDATA['.$wsdldata->{$_}.']]></wsdl:documentation>')
                         . '<wsdl:input name="'.$_.'Input" message="impl:RequestMsg" />'
                         . '<wsdl:output name="'.$_.'Output" message="impl:ResponseMsg" />'
                         . '<wsdl:fault name="'.$_.'Fault" message="impl:FaultMsg" />'
                         . '</wsdl:operation>' } @methods ) 
          . '</wsdl:portType>'
          . '<wsdl:binding name="XRSOAPSERVERNAMEBinding" type="impl:XRSOAPSERVERNAMEMethods" >'
          . '<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http" />'
          . join('', map { '<wsdl:operation name="'.$_.'" >'
                         . '<soap:operation soapAction="'.$main::Application->{ApplName}.'#'.$_.'" />'
                         . '<wsdl:input name="'.$_.'Input" ><soap:body use="literal" /></wsdl:input>'       
                         . '<wsdl:output name="'.$_.'Output" ><soap:body use="literal" /></wsdl:output>'       
                         . '<wsdl:fault name="'.$_.'Fault" ><soap:fault use="literal" name="'.$_.'Fault" /></wsdl:fault>'
                         . '</wsdl:operation>' } @methods )
           . '</wsdl:binding>'; 