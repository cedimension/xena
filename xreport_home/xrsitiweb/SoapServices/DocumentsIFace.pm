package XReport::DocumentsIFace;

use strict;

require Data::Dumper;
require XReport::SOAP;
require XReport::JobREPORT;
require XReport::ARCHIVE::INPUT;
require XReport::ARCHIVE::JobExtractor;
use CGI qw(:none:);

sub _buildListOfPages {
	my $reqdata = shift;
    my ($tblid, $orderby, $IndexEntries) = @{$reqdata}{qw(IndexName ORDERBY IndexEntries)};
	my $res = { LOP => [], RES => [], HNDLR => {} };
    my @colrequested = qw(JobReportId FromPage ForPages); 
    my @colretrieved = qw(JobReportName LocalFileName XferStartTime UserTimeRef);
    
    my $lastjr = {};
    if ( exists($reqdata->{JobReportId}) ) { 
        my ($JobReportId, $FromPage, $ForPages ) = @{$reqdata}{@colrequested};
        $res->{HNDLR}->{$JobReportId} = [ XReport::JobREPORT->Open($JobReportId) ];
        @{$lastjr}{@colrequested} = @{$reqdata}{@colrequested};
        @{$lastjr}{@colretrieved} = $res->{HNDLR}->{$JobReportId}->[0]->getValues(@colretrieved);
        if ( $FromPage && $ForPages && $lastjr->{JobReportName} ) {
#           push @{$res->{RES}}, $lastjr;
           push @{$res->{LOP}}, ($JobReportId, $FromPage, $ForPages );
        }
    }
    
#    foreach my $IndexEntry ( @{$IndexEntries} ) {
#       my $reqcols = ( exists($IndexEntry->{Columns}) ? $IndexEntry->{Columns} : $IndexEntry );
    foreach my $reqcols ( @{$IndexEntries} ) {
         my ($JobReportId, $FromPage, $ForPages ) = @{$reqcols}{@colrequested};
         next unless ( $FromPage && $ForPages );
         my $cols = {};
         @{$cols}{@colrequested} = @{$reqcols}{@colrequested};
         if ( !$JobReportId ) {
            next unless ( $lastjr->{JobReportId} );
            @{$cols}{'JobReportId', @colretrieved} = @{$lastjr}{'JobReportId', @colretrieved};
         }
         else {
            $res->{HNDLR}->{$JobReportId} = [ XReport::JobREPORT->Open($JobReportId) ]
                                                               if ( !exists($res->{HNDLR}->{$JobReportId}) );
            @{$cols}{@colretrieved} = $res->{HNDLR}->{$JobReportId}->[0]->getValues(@colretrieved);
            @{$lastjr}{'JobReportId', @colretrieved} = @{$cols}{'JobReportId', @colretrieved};
         }      
         push @{$res->{LOP}}, @{$cols}{@colrequested};
         push @{$res->{RES}}, { Columns => $cols };
    } 
    
    push @{$res->{RES}}, { Columns => [ map { { colname => $_, content => $lastjr->{$_} } } (@colrequested, @colretrieved) ] } 
                                                                                                   unless scalar(@{$res->{RES}});
    return $res;
}

sub _QueryXRDB {
	
  my $reqdata = shift;
  my ($method, $reqident) = @{$main::Request}{qw(method reqid)};
  
  my $dfltTOP = $XReport::cfg->{defaulttop} || 100;

  my ($tblid, $orderby, $IndexEntries) = @{$reqdata}{qw(IndexName ORDERBY IndexEntries)};
  my $select = 'SELECT ';
  $select .= ( !exists($reqdata->{TOP}) ? "TOP $dfltTOP" 
             : $reqdata->{TOP} ne "-1" ? "TOP $reqdata->{TOP}" : '' );
  $select .= ' * FROM ';
  if ( $tblid =~ /^#/ ) {
     my $sqlid = substr($tblid, 1);
     my $sql = $XReport::cfg->{sqlprocs}->{$sqlid};
     return _SOAP_ERROR( faultcode => 500, faultstring => "Client Request Error"
                             , detailmsg => "sql $sqlid not found"
                             , detaildesc => "$sqlid not in cfg list (". join(', ', keys %{$XReport::cfg->{sqlprocs}}) ) unless $sql;
     $select .= '('.$sql.') ';
  }
  
  my ($dbh, $tblname);
  if ( $tblid =~ /^_/ ) {
     $dbh = getDBHandle('XREPORT');
     $select .= "tbl".$tblid;
  }
  else {
     $dbh = XReport::DBUtil->get_ix_dbc($tblid);
     $select .= 'tbl_idx_'.$tblid;
  }
  
  my $where = '';
  $where = ' WHERE (' . join(') OR (', XReport::SOAP::buildClause(@{$reqdata}{qw(IndexName IndexEntries)}, '')).')'
                                                if (exists($reqdata->{IndexEntries}) && scalar(@{$reqdata->{IndexEntries}}));
  i::traceit( "getDocList: ", _Dumper($reqdata), "Clause: $where") if $main::dotrace;
  $select .= $where;
  
  $select .= ' ORDER BY '.$orderby if $orderby;

  i::traceit("${method} SELECT:", $select) if $main::dotrace;

   my $dbr; eval { $dbr = $dbh->dbExecute($select)};
   return _SOAP_ERROR(faultcode => 500
                      , faultstring => "Data Base Error"
                      , detailmsg => "_QueryXRDB error invoked by ".join('::', (caller())[0,2])
                      , detaildesc => $@. "Request: "._Dumper($reqdata)) if $@ || !$dbr;

   return _SOAP_ERROR(faultcode => 500
                      , faultstring => "No entries found"
                      , detailmsg => "no data from DB query invoked by ".join('::', (caller())[0,2])
                      , detaildesc => "Request: "._Dumper($reqdata). "Select: $select") 
                      if $dbr->eof();
   
   my $DocumentData = { IndexName => $tblid
#                     , sqlselect => $select
                      , IndexEntries => XReport::SOAP::fillIndexEntries($dbr)
                      , NewEntries => []
                      , DocumentBody => ''
                      };
                      
   return $DocumentData;

}

=begin :wsdldoc

=method getDocList

=doc  IndexName: { indexId | #procname | _tablename }
    an entry in XreportDB.tbl_IndexTables must be present for "Indexid"
    if _tablename query will be performed on table of main Xreport db 
    if #indexid.procname an entry in config.sqlprocs must be present for "procname"
    
=end :wsdldoc

=cut

sub getDocList {
  my $reqdata = shift;
  i::traceit("getDocList input:", _Dumper($reqdata) ) if $main::dotrace;

  my ($method, $reqident) = @{$main::Request}{qw(method reqid)};
  
  my $DocumentData = _QueryXRDB($reqdata);
  return undef unless defined($DocumentData);
  return $main::Response->Write( XReport::SOAP::buildResponse($DocumentData) );                    
  
#  my $dfltTOP = $XReport::cfg->{defaulttop} || 100;
#
#  my ($tblid, $orderby, $IndexEntries) = @{$reqdata}{qw(IndexName ORDERBY IndexEntries)};
#  my $select = 'SELECT ';
#  $select .= ( !exists($reqdata->{TOP}) ? 'TOP $dfltTOP' : $reqdata->{TOP} ne "-1" ? "TOP $reqdata->{TOP}" : '' );
#  $select .= ' * FROM ';
#  if ( $tblid =~ /^#/ ) {
#  	 my $sqlid = substr($tblid, 1);
#     my $sql = $XReport::cfg->{sqlprocs}->{$sqlid};
#     return _SOAP_ERROR( faultcode => 500, faultstring => "Client Request Error"
#                             , detailmsg => "sql $sqlid not found"
#                             , detaildesc => "$sqlid not in cfg list (". join(', ', keys %{$XReport::cfg->{sqlprocs}}) ) unless $sql;
#     $select .= '('.$sql.') ';
#  }
#  
#  my ($dbh, $tblname);
#  if ( $tblid =~ /^_/ ) {
#     $dbh = getDBHandle('XREPORT');
#     $select .= "tbl".$tblid;
#  }
#  else {
#     $dbh = XReport::DBUtil->get_ix_dbc($tblid);
#     $select .= 'tbl_idx_'.$tblid;
#  }
#  
#  my $where = '';
#  $where = ' WHERE (' . join(') OR (', XReport::SOAP::buildClause(@{$reqdata}{qw(IndexName IndexEntries)}, '')).')'
#                                                if (exists($reqdata->{IndexEntries}) && scalar(@{$reqdata->{IndexEntries}}));
#  i::traceit( "getDocList: ", _Dumper($reqdata), "Clause: $where") if $main::dotrace;
#  $select .= $where;
#  
#  $select .= ' ORDER BY '.$orderby if $orderby;
#
#  i::traceit("${method} SELECT:", $select) if $main::dotrace;
#
#   my $dbr; eval { $dbr = $dbh->dbExecute($select)};
#   return _SOAP_ERROR(faultcode => 500, faultstring => "Data Base Error"
#                      , detailmsg => "getDocList error", detaildesc => $@. "Request: "._Dumper($reqdata)) if $@;
#   
#   my $DocumentData = { IndexName => $tblid
##                     , sqlselect => $select
#   	                  , IndexEntries => ( $dbr ? XReport::SOAP::fillIndexEntries($dbr) : [] )
#                      , NewEntries => []
#                      , DocumentBody => ''
#                      };
#           
#    return $main::Response->Write( XReport::SOAP::buildResponse($DocumentData) );                    
}

#_extractRaw4ListOfPages $ListOfPages, $method, $reqident, $handlers;
sub _extractRaw4ListOfPages {

      my ( $ListOfPages, $method, $reqident, $handlers ) = @_;
        
    foreach my $JRID ( keys %{$handlers}) {
       my $iar;
       eval { $iar = XReport::ARCHIVE::INPUT->Open($handlers->{$JRID}->[0], wrapper => 1, random_access=> 1); };
       if ( $@ ) {
       	  die join("\t", ("Server Error", "Open Error", "Error during unput archive Open - $@"));
       }
       push @{$handlers->{$JRID}}, $iar;
    }

      my $streamer = new XReport::SOAP::B64Streamer();
      while ( scalar(@{$ListOfPages}) ) {
         my ($JobReportId, $FromPage, $ForPages) = splice @{$ListOfPages}, 0, 3;
         my ($pages, $lines);
         eval { ($pages, $lines) = $handlers->{$JobReportId}->[1]->extract_pages(
                                                       $FromPage, $ForPages, tostreamer => $streamer ); };
         if ( my $errmsg = $@ || !defined($lines) ) {
            die "Extract pages error for JR $JobReportId (pages $ForPages from $ForPages) ".($errmsg ? " - $errmsg" : '');
         }
      }

      i::traceit("returnDocumentPages elist: ", " InputDoc: ", $streamer->{InputBytes}
                                            , " B64Streamer: ", $streamer->{OutputBytes}) if $main::dotrace;

      foreach my $JRID ( keys %{$handlers}) {
         $handlers->{$JRID}->[1]->Close();
      }

}

#_extractPdf4ListOfPages $ListOfPages, $method, $reqident, $handlers;
sub _extractPdf4ListOfPages {

      my ( $ListOfPages, $method, $reqident, $handlers ) = @_;
      	
      require XReport::EXTRACT;
      require XReport::PDF::DOC;
      require MIME::Base64;

      my $query = XReport::EXTRACT->new();
      my $streamer = new XReport::SOAP::B64Streamer();
      $query->{docTo} = XReport::PDF::DOC->Create($streamer) ;

       my $elist;
       eval {$elist = $query->ExtractPages(
                       QUERY_TYPE => 'FROMPAGES',
                       PAGE_LIST => $ListOfPages,
                       FORMAT => "PRINT",
                       ENCRYPT => 'YES',
                       OPTIMIZE => 1,
                       REQUEST_ID => "$method.$$.$reqident",
                       #                   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"}B64streamer
                     );
      };
      if ( scalar($elist) == 0 ) {
      	 die join("\t", ("Client Request Error", "Document Not Found", 
      	          "DOCUMENTI NON TROVATI IN ARCHIVIO per la chiave richiesta (". join('::',@{$ListOfPages}).")"));
      }
       
    i::traceit("returnDocumentPages elist: ", " InputDoc: ", $streamer->{InputBytes}, " B64Streamer: ", $streamer->{OutputBytes}) if $main::dotrace;

}

=begin :wsdldoc

=method getReportPages

=doc  This method will return the pages indicated by the JobReportId, FromPage, ForPages
      specified either as attribute of the REQUEST block or in the elements of the indexentries array
      Is possible to specify the JobReportId in the request block and just the couple FromPage, ForPages
      in the IndexEntries array. Every time a new JobReportId is found in the IndexEntries array, that 
      will become the default for the followers.
      The result will be assembled from the INPUT archive if the IndexName attribute is set to "@INPUTARCHIVE"
      The result will be a collection of all the pages specified.

=end :wsdldoc

=cut

sub getReportPages {
    my $reqdata = shift;
    i::traceit("get ReportPages:", _Dumper($reqdata) ) if $main::dotrace;

    my ($method, $reqident) = @{$main::Request}{qw(method reqid)};
    my ($tblid, $orderby, $IndexEntries) = @{$reqdata}{qw(IndexName ORDERBY IndexEntries)};
    if ( scalar(@{$IndexEntries}) == 0 && !exists($reqdata->{JobReportId})
         && !exists($reqdata->{FromPage}) 
         && !exists($reqdata->{ForPages}) 
       ) {
         return _SOAP_ERROR( faultcode => 500, faultstring => "Client Request Error"
                           , detailmsg => "Empty Request list"
                           , detaildesc => "La richiesta non contiene alcun riferimento (".XReport::SOAP::Dumper($reqdata).")" );
    }
    
    my ($ListOfPages, $resultEntries, $handlers) = @{ _buildListOfPages($reqdata)}{qw(LOP RES HNDLR)};
    
    if ( scalar(@{$ListOfPages}) == 0 ) {
         return _SOAP_ERROR( faultcode => 500, faultstring => "Client Request Error"
                           , detailmsg => "Empty Request list"
                           , detaildesc => "Trovato nessun riferimento a Documento  (".XReport::SOAP::Dumper($reqdata).")" );
    }
    my $DocumentData = XReport::SOAP::buildResponse({
       IndexName => $tblid,
       IndexEntries => $resultEntries,
       DocumentType => ($tblid eq "\@INPUTARCHIVE" ? 'text/bin' : 'application/pdf'),
       DocumentBody => {content => "BASE64DOCUMENT"},
    } );

    my ($msghdr, $msgtail) = (($DocumentData) =~ /^(.*<\/IndexEntries[^\>]*>).*(<\/RESPONSE>.*)$/s);
    $main::Response->Write($msghdr . '<DocumentBody>'); # serializer

    my $extractor = ( $tblid eq "\@INPUTARCHIVE" ? \&_extractRaw4ListOfPages : \&_extractPdf4ListOfPages );
    eval { &$extractor( $ListOfPages, $method, $reqident, $handlers ); };
    if ( $@ ) {
          my ($fstring, $fdetail, $fdesc) = split /\t/, $@, 3; 
          return _SOAP_ERROR( faultcode => 500, faultstring => $fstring, detailmsg => $fdetail, detaildesc => $fdesc ); 
    }
    $main::Response->Write('</DocumentBody>'.$msgtail);
    return 1;

}

=begin :wsdldoc

=method getDocument

=doc  This is an alias of the getReportPages Method

=end :wsdldoc

=cut

sub getDocument { 
	my $datain = shift;
    i::traceit("getDocument:", _Dumper($datain) ) if $main::dotrace;
	my $reqdata = _QueryXRDB($datain);
	return undef unless defined($reqdata);
	return getReportPages($reqdata); 
}
#sub getPdfPages {
#    my $reqdata = shift;
#    i::traceit("getPdfPages input:", _Dumper($reqdata) ) if $main::dotrace;
#
#    my ($method, $reqident) = @{$main::Request}{qw(method reqid)};
#    my ($tblid, $orderby, $IndexEntries) = @{$reqdata}{qw(IndexName ORDERBY IndexEntries)};
#    if ( scalar(@{$IndexEntries}) == 0 && !exists($reqdata->{JobReportId})
#         && !exists($reqdata->{FromPage}) 
#         && !exists($reqdata->{ForPages}) 
#       ) {
#         return _SOAP_ERROR( faultcode => 500, faultstring => "Client Request Error"
#                           , detailmsg => "Empty Request list"
#                           , detaildesc => "La richiesta non contiene alcun riferimento (".XReport::SOAP::Dumper($reqdata).")" );
#    }
#    
#    my ($ListOfPages, $resultEntries, $handlers) = @{ _buildListOfPages($reqdata)}{qw(LOP RES HNDLR)};
#    
#    if ( scalar(@{$ListOfPages}) == 0 ) {
#         return _SOAP_ERROR( faultcode => 500, faultstring => "Client Request Error"
#                           , detailmsg => "Empty Request list"
#                           , detaildesc => "Trovato nessun riferimento a Documento  (".XReport::SOAP::Dumper($reqdata).")" );
#    }
#
#    my $DocumentData = XReport::SOAP::buildResponse({
#        IndexName => 'ARCHIVEDPDF',
#        IndexEntries => $resultEntries,
#        DocumentType => 'application/pdf',
#        DocumentBody => {content => "BASE64DOCUMENT"},
#    });
#
#    my ($msghdr, $msgtail) = (($DocumentData) =~ /^(.*<\/IndexEntries>).*(<\/RESPONSE>.*)$/s);
#    $main::Response->Write($msghdr . '<DocumentBody>'); # serializer
#
#    foreach my $JRID ( keys %{$handlers}) {
#         $handlers->{$JRID}->[1]->Close();
#    }
#
#      require XReport::EXTRACT;
#      require XReport::PDF::DOC;
#      require MIME::Base64;
#
#      my $query = XReport::EXTRACT->new();
#      my $streamer = new XReport::SOAP::B64Streamer();
#      $query->{docTo} = XReport::PDF::DOC->Create($streamer) ;
#
#       my $elist;
#       eval {$elist = $query->ExtractPages(
#				       QUERY_TYPE => 'FROMPAGES',
#				       PAGE_LIST => $ListOfPages,
#				       FORMAT => "PRINT",
#				       ENCRYPT => 'YES',
#				       OPTIMIZE => 1,
#				       REQUEST_ID => "$method.$$.$reqident",
#				       #				   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"}B64streamer
#                     );
#      };
#      if ( scalar($elist) == 0 ) {
#         return _SOAP_ERROR( faultcode => 503, faultstring => "Client Request Error"
#                           , detailmsg => "Document Not Found"
#                           , detaildesc => "DOCUMENTI NON TROVATI IN ARCHIVIO per la chiave richiesta ("
#                           . join('::',@{$ListOfPages}).")" );
#      }
#	   
#    i::traceit("returnDocumentPages elist: ", " InputDoc: ", $streamer->{InputBytes}, " B64Streamer: ", $streamer->{OutputBytes}) if $main::dotrace;
#
#    $main::Response->Write('</DocumentBody>'.$msgtail);
#    $main::Response->Flush();
#}

#sub getRawPages {
#    my $reqdata = shift;
#    i::traceit("getPdfPages input:", _Dumper($reqdata) ) if $main::dotrace;
#
#    my ($method, $reqident) = @{$main::Request}{qw(method reqid)};
#    my ($tblid, $orderby, $IndexEntries) = @{$reqdata}{qw(IndexName ORDERBY IndexEntries)};
#    if ( scalar(@{$IndexEntries}) == 0 && !exists($reqdata->{JobReportId})
#         && !exists($reqdata->{FromPage}) 
#         && !exists($reqdata->{ForPages}) 
#       ) {
#         return _SOAP_ERROR( faultcode => 500, faultstring => "Client Request Error"
#                           , detailmsg => "Empty Request list"
#                           , detaildesc => "La richiesta non contiene alcun riferimento (".XReport::SOAP::Dumper($reqdata).")" );
#    }
#    
#    my ($ListOfPages, $resultEntries, $handlers) = @{ _buildListOfPages($reqdata)}{qw(LOP RES HNDLR)};
#    foreach my $JRID ( keys %{$handlers}) {
#    	my $iar;
#    	eval { $iar = XReport::ARCHIVE::INPUT->Open($handlers->{$JRID}, wrapper => 1, random_access=> 1); };
#    	if ( $@ ) {
#    	   return _SOAP_ERROR( faultcode => 500, faultstring => "Server Error"
#                           , detailmsg => "Open Error"
#                           , detaildesc => "Error during unput archive Open - $@" );
#    		
#    	}
#    	push @{$handlers->{$JRID}}, $iar;
#    }
#    
#    if ( scalar(@{$ListOfPages}) == 0 ) {
#         return _SOAP_ERROR( faultcode => 500, faultstring => "Client Request Error"
#                           , detailmsg => "Empty Request list"
#                           , detaildesc => "Trovato nessun riferimento a Documento  (".XReport::SOAP::Dumper($reqdata).")" );
#    }
#       
#    my $DocumentData = XReport::SOAP::buildResponse({
#        IndexName => '#ARCHIVEDSTREAM',
#        IndexEntries => $resultEntries,
#        DocumentType => 'application/pdf',
#        DocumentBody => {content => "BASE64DOCUMENT"},
#    });
#
#    my ($msghdr, $msgtail) = (($DocumentData) =~ /^(.*<\/IndexEntries>).*(<\/RESPONSE>.*)$/s);
#
##    return _SOAP_ERROR(faultcode => 500, faultstring => "Method under development");
#
#    $main::Response->Write($msghdr . '<DocumentBody>'); # serializer
#    my $streamer = new XReport::SOAP::B64Streamer();
#
#    while ( scalar(@{$ListOfPages}) ) {
#       my ($JobReportId, $FromPage, $ForPages) = splice @{$ListOfPages}, 0, 3;
#       my ($pages, $lines);
#       eval { ($pages, $lines) = $handlers->{$JobReportId}->[1]->extract_pages(
#      	                                               $FromPage, $ForPages, tostreamer => $streamer ); };
#       if ( my $errmsg = $@ || !defined($lines) ) {
#            i::warnit("Extract pages error for JR $JobReportId (pages $ForPages from $ForPages) ".($errmsg ? " - $errmsg" : ''));
#            return _SOAP_ERROR(faultcode => 509, faultstring => "Method under development");
#       }
#    }
#
#    i::traceit("returnDocumentPages elist: ", " InputDoc: ", $streamer->{InputBytes}
#                                            , " B64Streamer: ", $streamer->{OutputBytes}) if $main::dotrace;
#
#    foreach my $JRID ( keys %{$handlers}) {
#         $handlers->{$JRID}->[1]->Close();
#    }
#
#    $main::Response->Write('</DocumentBody>'.$msgtail);
#    $main::Response->Flush();
#
#}

=begin :wsdldoc

=method getReportFileId

=doc  This method will return the content of the file archive indicated by the request attributes JobReportId, FileId

=end :wsdldoc

=cut

sub getReportFileId {
      my $reqdata = shift;
      my ($method, $reqident) = @{$main::Response}{qw(method reqid)};
      my ($tblid, $orderby, $IndexEntries) = @{$reqdata}{qw(IndexName ORDERBY IndexEntries)};
      my ($JRID, $REQFID) = @{$reqdata}{qw(JobReportId FileId)};
      my $jex;
      eval {$jex =  XReport::ARCHIVE::JobExtractor->Open($JRID);};
      if ( $@ ) {
           return _SOAP_ERROR( faultcode => 500, faultstring => "Server Error"
                           , detailmsg => "Open Error"
                           , detaildesc => "Error during JobExtractor Open - $@" );
            
      }
      my $pagexref = [$jex->getPageXref()];
#      warn "REQFID: $REQFID PAGEXREF: ", join('::', @{$pagexref}), "\n";
      my $ListOfPages = [];
      while ( scalar(@{$pagexref}) ) {
        my ( $fid, $fp, $tp) = splice @{$pagexref}, 0, 3;
        if ( $fid == $REQFID ) {
        	push @{$ListOfPages}, $JRID, $fp, ($tp - $fp);
        	last;
        }
      }
      my $handlers = { $JRID => [ XReport::JobREPORT->Open($JRID) ] };
      my $DocumentData = XReport::SOAP::buildResponse({
         IndexName => $tblid,
         IndexEntries => [],
         DocumentType => ($tblid eq "\@INPUTARCHIVE" ? 'text/bin' : 'application/pdf'),
         DocumentBody => {content => "BASE64DOCUMENT"},
      } );

    my ($msghdr, $msgtail) = (($DocumentData) =~ /^(.*<\/IndexEntries>).*(<\/RESPONSE>.*)$/s);
    $main::Response->Write($msghdr . '<DocumentBody>'); # serializer
    my $extractor = ( $tblid eq "\@INPUTARCHIVE" ? \&_extractRaw4ListOfPages : \&_extractPdf4ListOfPages );
    eval { &$extractor( $ListOfPages, $method, $reqident, $handlers ); };
    if ( $@ ) {
          my ($fstring, $fdetail, $fdesc) = split /\t/, $@, 3; 
          return _SOAP_ERROR( faultcode => 500, faultstring => $fstring, detailmsg => $fdetail, detaildesc => $fdesc ); 
    }
    $main::Response->Write('</DocumentBody>'.$msgtail);
    return 1;
      
}

=begin :wsdldoc

=method getLogicalReport

=doc  This method will return the logical report indicated by the request attributes JobReportId, ReportID

=end :wsdldoc

=cut

sub getLogicalReport {
      my $reqdata = shift;
      my ($method, $reqident) = @{$main::Response}{qw(method reqid)};
      my ($tblid, $orderby, $IndexEntries) = @{$reqdata}{qw(IndexName ORDERBY IndexEntries)};
      my ($JRID, $REPID) = @{$reqdata}{qw(JobReportId ReportId)};
      my $dbh = getDBHandle('XREPORT');
      my $sql = "SELECT * FROM tbl_LogicalReports LR INNER JOIN tbl_PhysicalReports PR on PR.JobReportId = LR.JobReportId "
              . " AND PR.ReportId = LR.ReportID WHERE LR.JobReportId = $JRID AND LR.ReportId = $REPID ";
  
      my $dbr; eval { $dbr = $dbh->dbExecute($sql)};
      my $prpages = [ split /[\, ]+/, $dbr->Fields()->{ListOfPages}->Value() ];
      my $ListOfPages = [];
      while ( scalar @{$prpages} ) {
      	push @{$ListOfPages}, $JRID, splice @{$prpages}, 0, 2;
      }
      my $handlers = { $JRID => [ XReport::JobREPORT->Open($JRID) ] };

    my $DocumentData = XReport::SOAP::buildResponse({
       IndexName => $tblid,
       IndexEntries => [],
       DocumentType => ($tblid eq "\@INPUTARCHIVE" ? 'text/bin' : 'application/pdf'),
       DocumentBody => {content => "BASE64DOCUMENT"},
    } );

    my ($msghdr, $msgtail) = (($DocumentData) =~ /^(.*<\/IndexEntries>).*(<\/RESPONSE>.*)$/s);
    $main::Response->Write($msghdr . '<DocumentBody>'); # serializer
    my $extractor = ( $tblid eq "\@INPUTARCHIVE" ? \&_extractRaw4ListOfPages : \&_extractPdf4ListOfPages );
    eval { &$extractor( $ListOfPages, $method, $reqident, $handlers ); };
    if ( $@ ) {
          my ($fstring, $fdetail, $fdesc) = split /\t/, $@, 3; 
          return _SOAP_ERROR( faultcode => 500, faultstring => $fstring, detailmsg => $fdetail, detaildesc => $fdesc ); 
    }
    $main::Response->Write('</DocumentBody>'.$msgtail);
    return 1;

}

=begin :wsdldoc

=method getArchiveFileList

=doc  This method will return the Output Archive file list merged with the infos contained in the 
      PageXref reference file ( filename, FromPage, ToPage ). The page info for the service files
      will be 0

=end :wsdldoc

=cut

sub getArchiveFileList {
      my $reqdata = shift;
      my ($method, $reqident) = @{$main::Response}{qw(method reqid)};
      my $JRID = $reqdata->{JobReportId};
      my $jex;
      eval {$jex =  XReport::ARCHIVE::JobExtractor->Open($JRID);};
      if ( $@ ) {
           return _SOAP_ERROR( faultcode => 500, faultstring => "Server Error"
                           , detailmsg => "Open Error"
                           , detaildesc => "Error during JobReport Open - $@" );
            
      }
      my $pagexref = [$jex->getPageXref()];
      my $fpages = {};
      while ( scalar(@{$pagexref}) ) {
      	my ( $fid, $fp, $tp) = splice @{$pagexref}, 0, 3;
      	$fpages->{$fid} = [ $fp, $tp ];
      }
      my $ojrar = $jex->{jr}->get_OUTPUT_ARCHIVE();
      my $archfiles = $ojrar->FileList();
      my $filelist = [];
      foreach my $fn ( @{$archfiles} ) {
      	my ($fp, $tp) = ( 0, 0 );
      	if ( $fn =~ /^.+\.$JRID\.0\.\#(\d+)\.([^\.]+)$/ ) {
      		my $ftype = $2;
      		($fp, $tp) = $fpages->{$1} if $ftype !~ /^(?:log|xref)$/i; 
      	}
      	push @{$filelist}, { Columns => { FileName => $fn, From => $fp, To => $tp} };
      }
      my $DocumentData = { IndexName => "#ARCHIVEFILES", JobReportId => $JRID
                      , IndexEntries => $filelist
                      , NewEntries => []
                      , DocumentBody => ''
                      };
      
    return $main::Response->Write( XReport::SOAP::buildResponse($DocumentData) );                    
}

=begin :wsdldoc

=method getPhysicalReports

=doc  This method will return the Physical report indicated by the request IndexEntry JobReportId, ListOfPages

=end :wsdldoc

=cut

sub getPhysicalReports {
      my $reqdata = shift;
      my ($method, $reqident) = @{$main::Response}{qw(method reqid)};
      my ($tblid, $orderby, $IndexEntries) = @{$reqdata}{qw(IndexName ORDERBY IndexEntries)};
      my ($JRID) = @{$reqdata}{qw(JobReportId)};
      my $ListOfPages = [];
      my $resultEntries = [];
      my $handlers = {};
      foreach my $ie ( @{$IndexEntries} ) {
      	  my ($iejrid, $ReportID, $lop) = @{$ie->{Columns}}{qw(JobReportId ReportId ListOfPages)};
          my $JobReportId = $iejrid || $JRID;
      	  my ($lopjrid, @plist) = split /\,/, $lop;
      	  $JobReportId = $lopjrid unless $JobReportId;
      	  $handlers->{$JobReportId} = [ XReport::JobREPORT->Open($JobReportId) ]  
      	                                                         unless exists($handlers->{$JobReportId});
      	  push @{$resultEntries}, {Columns => { JobReportId => $JobReportId, ReportId => $ReportID, PageList => join(',', @plist) }};
      	  while ( scalar(@plist) ) {
      	  	push @{$ListOfPages}, ($JobReportId, splice( @plist, 0, 2));
      	  } 
      }
    my $DocumentData = XReport::SOAP::buildResponse({
       IndexName => $tblid,
       IndexEntries => $resultEntries,
       DocumentType => ($tblid eq "\@INPUTARCHIVE" ? 'text/bin' : 'application/pdf'),
       DocumentBody => {content => "BASE64DOCUMENT"},
    } );

    my ($msghdr, $msgtail) = (($DocumentData) =~ /^(.*<\/IndexEntries>).*(<\/RESPONSE>.*)$/s);
    $main::Response->Write($msghdr . '<DocumentBody>'); # serializer

    my $extractor = ( $tblid eq "\@INPUTARCHIVE" ? \&_extractRaw4ListOfPages : \&_extractPdf4ListOfPages );
    eval { &$extractor( $ListOfPages, $method, $reqident, $handlers ); };
    if ( $@ ) {
          my ($fstring, $fdetail, $fdesc) = split /\t/, $@, 3; 
          return _SOAP_ERROR( faultcode => 500, faultstring => $fstring, detailmsg => $fdetail, detaildesc => $fdesc ); 
    }
    $main::Response->Write('</DocumentBody>'.$msgtail);
    return 1;
      
}

=begin :wsdldoc

=method getPhysicalReports

=doc  This is an alias of the getPhysicalReports Method


=end :wsdldoc

=cut

sub getReportEntryPdf {
    return getPhysicalReports(@_);
}

=begin :wsdldoc

=method getInputMetadata

=doc  This method will return the metadata collected during the receive process of the input stream 
      for a given JobReportId

=end :wsdldoc

=cut

sub getInputMetadata {
      my $reqdata = shift;
      my ($method, $reqident) = @{$main::Response}{qw(method reqid)};
      my $JRID = $reqdata->{JobReportId};
      my $jrh;
      eval {$jrh = XReport::JobREPORT->Open($JRID);};
      if ( $@ ) {
           return _SOAP_ERROR( faultcode => 500, faultstring => "Server Error"
                           , detailmsg => "Open Error"
                           , detaildesc => "Error during JobReport Open - $@" );
            
      }
      my $iar;
      eval {$iar = XReport::ARCHIVE::INPUT->Open($jrh, wrapper => 1, random_access=> 1);};
      if ( $@ ) {
           return _SOAP_ERROR( faultcode => 500, faultstring => "Server Error"
                           , detailmsg => "Open Error"
                           , detaildesc => "Error during input archive Open - $@" );
            
      }
#      return _SOAP_ERROR(faultcode => 509, faultstring => "Method under development");
      my $DocumentData = { IndexName => "#INPUTMETADATA", JobReportID => $JRID
                      , IndexEntries => [ { Columns => $iar->{blocks_input}->Cinfo() } ]
                      , NewEntries => []
                      , DocumentBody => ''
                      };
      $iar->Close();
                 
    return $main::Response->Write( XReport::SOAP::buildResponse($DocumentData) );                    
      
}

=begin :wsdldoc

=method getInputFirstBlock

=doc  To be developed - will return a soap error

=end :wsdldoc

=cut

sub getInputFirstBlock {
      my $reqdata = shift;
      my ($method, $reqident) = @{$main::Response}{qw(method reqid)};
      return _SOAP_ERROR(faultcode => 509, faultstring => "Method under development");
}


#sub _sampleSoapResponse {
#    i::logit( "DISPATCHER: " . XReport::SOAP::Dumper(\@_));
#    my $reqdata = shift;
#    my ($method, $reqident) = @{$main::Response}{qw(method reqid)};
#    my $DocumentData = { IndexName => 'pippo', TOP => 10, DocumentType => 'Application/x-pdf',
#             IndexEntries => [ { JobReportId => 12367, FromPage => 2, ForPages => '3',
#                                 Columns => [{colname => 'ResName', content => 'CICLADE' },
#                                             {colname => 'XFerStartTime', content => '2010-01-01' },
#                                            ],
#                               },
#                               { JobReportId => 12368, FromPage => 2, ForPages => '3',
#                                 Columns => [{colname => 'ResName', content => 'PIPPUZZO' },
#                                             {colname => 'XFerStartTime', content => '2010-01-01' },
#                                            ],
#                               },
#                            ],
#             NewEntries => [],
#             DocumentBody => ''
#           };
#           
#       return $main::Response->Write( XReport::SOAP::buildResponse($DocumentData) );                    
#}

#sub _testDocList {
#    $main::dolog && i::logit( "DISPATCHER: " . XReport::SOAP::Dumper(\@_)) if $main::veryverbose;
#    return getDocList({ IndexName => '_LOgicalReports', TOP => 10, ORDERBY => "XferStartTime" });
#}
#

__PACKAGE__;
