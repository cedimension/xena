use strict;
use XReport::ENUMS;

my @PageXref;
my $JobReportId;

sub _EXIT_SOAP_ERROR {
  my $respmsg = ('<?xml version="1.0" encoding="UTF-8"?>'
		 . '<SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema/instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">'
		 . '<SOAP-ENV:Body>'
		 . '<SOAP-ENV:Fault>'
		 . "<faultcode>$_[0]</faultcode>"
		 . "<faultactor>".$main::servername.'/'.$_[1]."</faultactor>"
		 . "<faultstring>$_[2]</faultstring>"
		 . '</SOAP-ENV:Fault>'
		 . '</SOAP-ENV:Body>'
		 . '</SOAP-ENV:Envelope>'
		);
  main::debug2log("EXIT_SOAP_ERROR: ", $respmsg);

  $main::Response->{Status} = "500 Xreport Web Service Server Error";
  $main::Response->AddHeader("Connection", "close");
  $main::Response->Write( $respmsg );
  $main::Response->Flush();
  return undef;
}


sub _expListOfPages {
  use Data::Dumper;
  my $ListOfPages = shift; my (@ListOfPages, %ListOfPages);

#  my @LOP = split(/\D+/, $ListOfPages);
  for ( my @LOP = split(/\D+/, $ListOfPages); @LOP; ) {
#  while ( @LOP ) {
    my ($fm, $req) = (shift @LOP, shift @LOP); 

    my $to = $fm + $req - 1; my $found = 0;

#    for ($_=0; $_<scalar(@PageXref); $_+=3) {
#      if ( 
#        ( $fm >= $PageXref[$_+1] and $fm <= $PageXref[$_+2] ) or
#        ( $to >= $PageXref[$_+1] and $to <= $PageXref[$_+2] )
#      ) {
#        my $FileId = $PageXref[$_]; 
#        if ( !exists($ListOfPages{"$FileId"}) ) {
#          $ListOfPages{$FileId} = [$FileId];
#        }
#        my $fm_ = max($fm,$PageXref[$_+1]);
#        my $to_ = min($to,$PageXref[$_+2]);
#        push @{$ListOfPages{$FileId}}, $fm_, $to_- $fm + 1;
#        $found += $to_ - $fm_ + 1;
#      }
#    }
    foreach my $pel ( @PageXref ) {
      if ( ( $fm >= $pel->[1] and $fm <= $pel->[2] ) or
	   ( $to >= $pel->[1] and $to <= $pel->[2] ) 
      ) {
        my $FileId = $pel->[0]; 
        $ListOfPages{$FileId} = [$FileId] if ( !exists($ListOfPages{"$FileId"}) );

        my $maxfm_ = max($fm,$pel->[1]);
        my $minto_ = min($to,$pel->[2]);
        push @{$ListOfPages{$FileId}}, $maxfm_, $minto_- $fm + 1;
        $found += $minto_ - $maxfm_ + 1;
      }
    }
#    die "$fm $to $req ====\n====". Dumper(\@PageXref) ."=====\n==== " . Dumper(\%ListOfPages) ."=====\n===== $found ======\n";
     die "NUMBER MISMATCH ". join("/", $JobReportId, $fm, $to, $req, $found) if ( $req != $found );
      _EXIT_SOAP_ERROR("Sender", "IdNotFound", "DOCUMENTI NON TROVATI IN ARCHIVIO per la chiave richiesta - $JobReportId $fm $to $req $found") if ( $req != $found ) ;
  }
  for my $FileId (sort(keys(%ListOfPages))) {
    my $fileList = $ListOfPages{$FileId};
    if ( 
      $fileList->[1] == $PageXref[$FileId]->[1]
      and $fileList->[2] ==  $PageXref[$FileId]->[2] - $PageXref[$FileId]->[1] + 1
     ) {
      push @ListOfPages, $FileId;
    }
    else {
      push @ListOfPages, $fileList;
    }
  }

#  die "====". $ListOfPages ."=====\n====". Dumper(\@PageXref) ."=====\n==== " . Dumper(\%ListOfPages) ."=====\n=====" . Dumper(\@ListOfPages) . "======\n";
  return \@ListOfPages
}

sub getReportEntryTxt { 
    my ($method, $reqident, $selection) = (shift, shift, shift);

      my $respEntries = [];
      my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
      $JobReportId = $IndexEntries->[0]->{'Columns'}->{'JobReportId'}->{'content'};
      my $FromPage = $IndexEntries->[0]->{'Columns'}->{'FromPage'}->{'content'};
       
      my $ListOfPages =$IndexEntries->[0]->{'Columns'}->{'ListOfPages'}->{'content'}; 
      my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportId'}->{'content'};
	  my $pageSep = $IndexEntries->[0]->{'Columns'}->{'PageSeparator'}->{'content'};
	  #main::debug2log("ecco il separator ".$pageSep. "e in esadecimale  ".unpack("H4", $pageSep)." stessi caratteri".unpack("H4", "AAAAAAAA"));
      #my $JobReportHandle = XReport::JobREPORT->Open($JobReportId);
      #my $output_archive = $JobReportHandle->get_OUTPUT_ARCHIVE();
	  #my $JobReportName = $JobReportHandle->getValues('JobReportName');
	  require XReport::EXTRACT;
      require XReport::PDF::DOC;
      require MIME::Base64;
      require XReport::ARCHIVE::INPUT; 
	  #my $readexit = sub {return $_[1]; };
	  #my $readexit = sub {return substr($_[1],1); }; 
	  my $readexit = sub {
						#return "\n".  substr($_[1],1) if(substr($_[1],0,1) eq '0');
						#return "\n\n".substr($_[1],1) if(substr($_[1],0,1) eq '-');
						my $retstr = substr($_[1],1);
						$retstr =~ s/(\r?\n|\r\n?)/\r\n/g; 
						$retstr =~ s/\x00/ /g;
					    return $retstr;
						}; 
	  my $jrref = $JobReportId;
      do {
      	require XReport::ARCHIVE::JobREPORT; 
      	$jrref = XReport::ARCHIVE::JobREPORT->Open($jrref)
      } 
      if !ref($jrref);
	  
	  my $jr;
	  if( $jrref->getValues(qw(ReportFormat)) eq XReport::ENUMS::RF_ASCII) 
	  {
	  	$readexit = sub { 
	  		my $retstr = $_[1]; 
	  		$retstr =~ s/\x00//g;  
	  		#$retstr =~ s/\x0d/\x0a/g; 
		    $retstr =~ s/(\r?\n|\r\n?)/\r\n/g; 
	  		return $retstr;
	  	};
	  	$jr = XReport::ARCHIVE::INPUT->Open($jrref, wrapper => 1, random_access => 1
	  	,recexit => $readexit
	  	);
	  }
	  else
	  {
		$jr = XReport::ARCHIVE::INPUT->Open($jrref, wrapper => 1, random_access => 1, 
                       outxlate => 'cp37:latin1', recexit => $readexit, change5A => 1, pagedelim => "\x31"
		);
	    if((exists $jr->{HasCc}) and ($jr->{HasCc} == 0))
	    {
		  $jr->{recexit} = sub {
		  					   my $retstr = $_[1];
		  					   $retstr =~ s/(\r?\n|\r\n?)/\r\n/g;
		  					   $retstr =~ s/\x00/ /g;
		  					   return $retstr;
		  					 }; 
	    }
	  }
	  
	  
	  my $JobReportName = $jr->{JobReportName};

      my $DocumentData = XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $IndexEntries,
		DocumentType => 'application/txt',
		FileName => "$JobReportName\.$JobReportId",
		Identity =>  $JobReportName.$JobReportId,
		DocumentBody => {content => "BASE64DOCUMENT"},
	    });

       my ($msghdr, $msgtail) = (($DocumentData) =~ /^(.*<\/IndexEntries>).*(<\/RESPONSE>.*)$/s);
       my $query = XReport::EXTRACT->new();
       my $streamer = new B64streamer();
       $main::Response->Write($msghdr . '<DocumentBody>'); # serializer
       my $elist;
       my (undef, @list ) = split(/,/, $ListOfPages);
       my $lop = []; 
       while(scalar(@list)){
            #push @$lop , $JobReportId, splice @list,0, 2;
            my ($frompage, $forpages) = splice @list,0, 2;
            my ($pages, $lines);
            main::debug2log("calling extract_pages fromp $frompage forp $forpages-------------------- ");   
            eval { ($pages, $lines) = $jr->extract_pages($frompage, $forpages, tostreamer => $streamer , $pageSep ? (pagesep => $pageSep) : undef) }; 
            _EXIT_SOAP_ERROR("Sender", "IdNotFound", "DOCUMENTI NON TROVATI IN ARCHIVIO per la chiave richiesta - $@ ") if($@);
       }
	   $streamer->close();
       main::debug2log($DocumentData."////////////Finished".substr($msghdr,0,20)."---------------------" );
       main::debug2log("returnDocumentPages elist: ", " InputDoc: ", $streamer->{InputBytes}, " B64Streamer: ", $streamer->{OutputBytes});

       $main::Response->Write('</DocumentBody>'.$msgtail);
       $main::Response->Flush();
		main::debug2log($DocumentData."////////////----------------------RESPONSE: ".Dumper($main::Response));

}

sub getReportEntryExcel {
	   my ($method, $reqident, $selection) = (shift, shift, shift);
       
	   my $respEntries = [];
	   my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	   $JobReportId = $IndexEntries->[0]->{'Columns'}->{'JobReportId'}->{'content'};
	   my $FromPage = $IndexEntries->[0]->{'Columns'}->{'FromPage'}->{'content'};
	    
	   my $ListOfPages =$IndexEntries->[0]->{'Columns'}->{'ListOfPages'}->{'content'}; 
	   my $ReportID =$IndexEntries->[0]->{'Columns'}->{'ReportId'}->{'content'}; 
	   my $JobReportHandle = XReport::JobREPORT->Open($JobReportId);
	   my $output_archive = $JobReportHandle->get_OUTPUT_ARCHIVE();
	   my $JobReportName = $JobReportHandle->getValues('JobReportName');
	   require XReport::EXTRACT;
	   #require XReport::PDF::DOC;
	   require MIME::Base64;

      my $DocumentData = XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $IndexEntries,
		DocumentType => 'application/pdf',
		FileName => "$JobReportName\.$JobReportId",
		Identity =>  $JobReportName.$JobReportId,
		DocumentBody => {content => "BASE64DOCUMENT"},
	 });

       my ($msghdr, $msgtail) = (($DocumentData) =~ /^(.*<\/IndexEntries>).*(<\/RESPONSE>.*)$/s);
       my $query = XReport::EXTRACT->new();
       my $streamer = new B64streamer();
       #$query->{docTo} = XReport::PDF::DOC->Create($streamer) ;
	   $query->{docTo} = $streamer;
       $main::Response->Write($msghdr . '<DocumentBody>'); # serializer

       my $elist;
       my (undef, @list ) = split(/,/, $ListOfPages);
       my $lop = []; 
       while(scalar(@list)){
        push @$lop , $JobReportId, splice @list,0, 2;
       }
       main::debug2log($DocumentData."////////////".substr($msghdr,0,20)."--------------PAGES: ".Dumper($lop));   
       eval {$elist = $query->ExtractPages(
				       QUERY_TYPE => 'FULLEXCEL',
				       PAGE_LIST => $lop ,
				       FORMAT => "EXCEL",
				       ENCRYPT => 'NO',
				       OPTIMIZE => 0,
				       REQUEST_ID => "GetDocById.".$main::Session->{'SessionID'},
				       #				   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"}B64streamer
		
                     );
      };
      if ( scalar($elist) == 0 ) {
            _EXIT_SOAP_ERROR("Sender", "IdNotFound", "DOCUMENTI NON TROVATI IN ARCHIVIO per la chiave richiesta - $@");
            return 0;
         }
	   
    main::debug2log("returnExcel elist: ", " InputDoc: ", $streamer->{InputBytes}, " B64Streamer: ", $streamer->{OutputBytes});

    $main::Response->Write('</DocumentBody>'.$msgtail);
    $main::Response->Flush();
}

sub getReportEntryPdf {
      my ($method, $reqident, $selection) = (shift, shift, shift);

      my $respEntries = [];
      my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
      $JobReportId = $IndexEntries->[0]->{'Columns'}->{'JobReportId'}->{'content'};
      my $FromPage = $IndexEntries->[0]->{'Columns'}->{'FromPage'}->{'content'};
       
      my $ListOfPages =$IndexEntries->[0]->{'Columns'}->{'ListOfPages'}->{'content'}; 
      my $ReportID =$IndexEntries->[0]->{'Columns'}->{'ReportId'}->{'content'}; 
      my $JobReportHandle = XReport::JobREPORT->Open($JobReportId);
	  my $output_archive = $JobReportHandle->get_OUTPUT_ARCHIVE();
	  my $JobReportName = $JobReportHandle->getValues('JobReportName');
	  
      # my $xfileName = "\$\$PageXref.$JobReportId.txt";
      # my $zipMember = $output_archive->ExistsFile( $xfileName ); 
      # my $ObjectLength;
      # $zipMember->desiredCompressionMethod('COMPRESSION_STORED');
      #  $ObjectLength = $zipMember->uncompressedSize();

      # $zipMember->rewindData();
      #my $lastChunkLength = 0; my $xrefV = "";
      #while( !$zipMember->readIsDone() ) {
      # my ($ref, $status) = $zipMember->readChunk( 32768 );
            # main::debug2log("xref: $ref");
            # if ( $status != AZ_OK and $status != AZ_STREAM_END ) {
              #        main::debug2log("READ ERROR rc=$status");
                    #        last;
                    #  }
            #  $xrefV .= $$ref;
            #  $lastChunkLength = length($$ref); 
            # }

            # $zipMember->endRead();
      # main::debug2log("---------------------------------- PageXref: $xrefV");
      # push @PageXref, ( map { $_ =~ /File=(\d+) +From=(\d+) +To=(\d+)/; [$1, $2, $3] } 
        # grep /File=\d+ +From=\d+ +To=\d+/, split("\n", $xrefV) );
            #main::debug2log("PAGE_XREF: ".Dumper(@PageXref));      
            # main::debug2log("LIST OF PAGES : ".$ListOfPages);
      #$ListOfPages = _expListOfPages($ListOfPages);
      #main::debug2log("getReportEntryPdf".$ListOfPages); 
      #  die "===>4 LOP: ". join('::', @{$ListOfPages->[0]}) . " <===\n";

      #if ( scalar(@$ListOfPages) == 1 and !ref($ListOfPages->[0]) ) {
      #    eval { ExtractAndSendObject( $ListOfPages->[0], $FileFormat ) };
      # }
      # else {
      #eval {
        
       require XReport::EXTRACT;
       require XReport::PDF::DOC;
       require MIME::Base64;

      my $DocumentData = XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $IndexEntries,
		DocumentType => 'application/pdf',
		FileName => "$JobReportName\.$JobReportId",
		Identity =>  $JobReportName.$JobReportId,
		DocumentBody => {content => "BASE64DOCUMENT"},
	 });

       my ($msghdr, $msgtail) = (($DocumentData) =~ /^(.*<\/IndexEntries>).*(<\/RESPONSE>.*)$/s);
       my $query = XReport::EXTRACT->new();
       my $streamer = new B64streamer();
       $query->{docTo} = XReport::PDF::DOC->Create($streamer) ;
       $main::Response->Write($msghdr . '<DocumentBody>'); # serializer

       my $elist; my $lop = []; 
       my (undef, @list ) = split(/,/, $ListOfPages);
       while(scalar(@list)){
			if( (not defined($ReportID)) or  ($ReportID > 0)){
        push @$lop , $JobReportId, splice @list,0, 2;
			}else{
				push @$lop , splice @list,0, 3;
       }
				
	   }
	   
       main::debug2log($DocumentData."////////////".substr($msghdr,0,20)."--------------PAGES: ".Dumper($lop));   
       eval {$elist = $query->ExtractPages(
				       QUERY_TYPE => 'FROMPAGES',
				       PAGE_LIST => $lop ,
				       FORMAT => "PRINT",
				       ENCRYPT => 'YES',
				       OPTIMIZE => 1,
				       REQUEST_ID => "GetDocById.".$main::Session->{'SessionID'},
				       #				   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"}B64streamer
		
                     );
      };
      if ( scalar($elist) == 0 ) {
            _EXIT_SOAP_ERROR("Sender", "IdNotFound", "DOCUMENTI NON TROVATI IN ARCHIVIO per la chiave richiesta - $@");
            return 0;
         }
	   
    main::debug2log("returnDocumentPages elist: ", " InputDoc: ", $streamer->{InputBytes}, " B64Streamer: ", $streamer->{OutputBytes});

    $main::Response->Write('</DocumentBody>'.$msgtail);
    $main::Response->Flush();
    #};
    #}
	#unlink("$spool/$FileName");
    #main::debug2log("len response:".length($pdfV));
    #my ($buffsize, $outbytes, $pdfContent) = (57*76, 0, undef);
    #main::debug2log("Start base64 encoding");
    #$pdfContent = $streamer->{OutputBytes};
	#$pdfContent = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsize)*", $pdfV ) );
    #$pdfContent = MIME::Base64::encode_base64($pdfV);
#	main::debug2log("End base64 encoding");
	
#	my $actionMessage = "SUCCESS";
#	my $respEntries = [
#		{
#			Columns => [
#				{colname => 'ActionMessage', content => $actionMessage}
#			]
#		}
#	];
	#$main::Response->AddHeader('Content-Length' => length($pdfContent));
#	$main::Response->Write(XReport::SOAP::buildResponse({
#		IndexName => 'XREPORTWEB',
#		IndexEntries => $respEntries,
#		DocumentType => 'application/pdf',
#		FileName => "$JobReportName\.$JobReportId",
#		Identity =>  $JobReportName.$JobReportId,
#		DocumentBody => {content => $pdfContent},
#	 }));
	
	 #my $response = XReport::SOAP::buildResponse({
	 #	IndexName => 'XREPORTWEB',
	 #	IndexEntries => $respEntries,
	 #	DocumentType => 'application/pdf',
	 #	FileName => "$JobReportName\.$JobReportId",
	 #	Identity => $JobReportName.$JobReportId,
	 #	DocumentBody => {content => "<![CDATA[$pdfContent]]>"},
	 #});
#	main::debug2log("len response decoded:".length($pdfContent));
	#$main::Response->AddHeader("Content-Length", length($response));
	#my $t = 0;
	# while ( $response ) {
	#   (my $buff, $response) = unpack("a32768 a*", $response);
	#	if($buff) {
	#	$main::Response->Write($buff);
	#		$t += length($buff);
	#	}
	#	main::debug2log("len chunk:".$t);
	# }  
	# main::debug2log("${method} $reqident bytes of response sent to requester($t)");
	# return $main::Response->Flush();
		
}

1;
