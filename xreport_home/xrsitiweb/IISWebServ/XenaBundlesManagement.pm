package XReport::XenaBundlesManagement;

use strict;

use FileHandle;
use Symbol;
use Net::FTP;
use Win32::OLE::Variant;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

use XReport::Util;
use XReport::SOAP;
use XReport::JobREPORT;
use XReport::DBUtil qw(:none);
use XReport::QUtil;
use XReport::Spool;

require Data::Dumper;

sub _Dumper { return Data::Dumper::Dumper(@_); } 

# 
# XReport::XenaBundlesManagement::_substituteVars(\{}, "SQL ", \{TOPLINES => "TOP 100", FromDate => });
sub _substituteVars {
    my ($info, $cfgstring) = (shift, shift, shift);
#   warn "subst called by ", join('::', (caller())[0,2]), "\n";
    my $prtvars = { %$info, @_ };

    use Data::Dumper;
    return join("\n", map { (my $l = $_) =~ s/\#\{(.+?)\}\#/$prtvars->{$1}/g; $l } 
                                                                  split /\n/, $cfgstring);   
}

sub _parseContent { 
  my ($method, @parms) = ( shift, @_ );
  return XReport::SOAP::parseSOAPreq(main::readContent('-none-', $method)); 
}

require($main::Request->ServerVariables('APPL_PHYSICAL_PATH')->Item()."/auth_functions.pl");

require($main::Request->ServerVariables('APPL_PHYSICAL_PATH')->Item()."/db_functions.pl");

require($main::Request->ServerVariables('APPL_PHYSICAL_PATH')->Item()."/bundles_functions.pl");

1;

