package B64streamer;

use MIME::Base64;

use strict;
sub new {
  my $class = shift;
  my $self = { buffsize => 57*76, bufflen => 0, InputBytes => 0, OutputBytes => 0, buffer => ''};
  bless $self, $class;

  return $self;
}

sub binmode {

    return 1;
}

sub write {
  my ($self, $buffer) = (shift, shift);
  $self->{buffer} .= $buffer; 
  while ( length($self->{buffer}) > $self->{buffsize} ) {
    ($buffer, $self->{buffer}) = unpack("a".$self->{buffsize}." a*", $self->{buffer}) ;
    my $b64buff = MIME::Base64::encode_base64($buffer);
    $main::Response->Write($b64buff) ;
    $self->{InputBytes} += length($buffer);
    $self->{OutputBytes} += length($b64buff);
  }
}

sub tell {
 my $self = shift;
 return $self->{InputBytes} + length($self->{buffer});
}

sub close {
  my $self = shift;
  $self->write('');
  my $b64buff = MIME::Base64::encode_base64($self->{buffer});
  $main::Response->Write($b64buff) ;
  $self->{InputBytes} += length($self->{buffer});
  $self->{OutputBytes} += length($b64buff);
  main::debug2log("Inputbytes: ", $self->{InputBytes}, " Outputbytes: ", $self->{OutputBytes});
#  return $main::Response->Flush();
}

package XReport::XReportWebIface;

use strict;

use FileHandle;
use Symbol;
use Net::FTP;
use Win32::OLE::Variant;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

use XReport::Util;
use XReport::SOAP;
use XReport::JobREPORT;
use XReport::DBUtil qw(:none);
use XReport::QUtil;
use XReport::Spool;

require Data::Dumper;

sub _Dumper { return Data::Dumper::Dumper(@_); } 

sub _parseContent { 
  my ($method, @parms) = ( shift, @_ );
  main::debug2log("XReportWebIface parsesub $method  - selection: @parms");
  my $rawreq = main::readContent('-none-', $method);
  main::debug2log("XReportWebIface parsesub $method  - rawdata: $rawreq");
  return XReport::SOAP::parseSOAPreq($rawreq); 
}
my $phpath = ($main::Request ? $main::Request->ServerVariables('APPL_PHYSICAL_PATH')->Item() : $main::apppath);
require($phpath."/auth_functions.pl");

require($phpath."/xr_util_functions.pl");

require($phpath."/db_functions.pl");

### defin ###

require($phpath."/defin_functions.pl");

### common ###

require($phpath."/common_functions.pl");

### oper ###

require($phpath."/oper_functions.pl");

### extractor pdf ###

require($phpath."/extr_functions.pl");

require($phpath."/search_functions.pl");

__PACKAGE__;
