<% @Language="PerlScript" %>
<%
use strict vars;
use POSIX qw(strftime);
use Time::HiRes qw(time);
use IO::Socket::INET;
use Win32::OLE::Variant;

$main::ASPVER = '1.1';
#$main::starttime = strftime "%Y-%m-%d %H:%M:%S.%U", localtime;
my $t = time; $main::starttime =  strftime '%Y-%m-%d %H:%M:%S', localtime $t ; $main::starttime.= sprintf '.%03d',($t-int($t))*1000 ;
$main::timesmark = [ times() ];

use lib($main::Application->{"XREPORT_HOME"}."/perllib");
use lib(split /;/, $main::Application->{"ApplPerllib"});
use lib($main::Request->ServerVariables('APPL_PHYSICAL_PATH')->Item());
use XReport;
use XReport::SOAP::Server ('#'.$main::Application->{ApplName});

$main::servername = $main::Application->{ApplName};
$main::debugdir = $XReport::cfg->{logsdir} || $XReport::cfg->{workdir};
$main::authUserName = '-';
$main::debugdir .= "/$main::servername\_debug";
$main::DEBUG = $main::Application->{DEBUG} || $XReport::cfg->{DEBUG} || 1;
use constant DBGDEST => '226.1.1.9:4000';
#use constant LOGDEST => '226.1.1.9:2000';
$main::dbgsock = new IO::Socket::INET(Proto=>'udp',PeerAddr=>DBGDEST, Reuse => 1);
#$main::logsock = new IO::Socket::INET(Proto=>'udp',PeerAddr=>LOGDEST, Reuse => 1);

sub debug2log {
  $main::dbgsock->send($main::Session->{SessionID} . " " . strftime("%Y-%m-%d %H:%M:%S.%U", localtime) . " $main::ASPVER $main::servername\> " . join('', @_), 0, DBGDEST) if $main::dbgsock;
  return 1;

#  return 1 unless -d $main::debugdir;

#  i::logit(join('', @_));

#  my $mplog = new FileHandle '>>'.$main::debugdir.'/service.log';
#  print $mplog $main::Session->{SessionID} . " " . localtime() . " $main::ASPVER $main::servername: " . join('', @_)."\n";
#  $mplog->close();
}

sub i::logit { return debug2log(@_); }

sub write2log {
  return main::debug2log(@_);

#  return main::debug2log(@_) if -d $main::debugdir;
#  return i::logit(join('', @_));

#  return 1 unless -d $main::debugdir;

#  my $mplog = new FileHandle '>>'.$main::debugdir.'/service.log';
#  print $mplog $main::Session->{SessionID} . " " . localtime() . " $main::ASPVER $main::servername: " . join('', @_)."\n";
#  $mplog->close();
}


sub _GET_SHOWLOG {
  my @html = ();
  my $debug_fn = $main::debugdir.'/service.log';
  if ( -e $debug_fn ) {
    my $debug_fs = -s $debug_fn;
    push @html, "File: $debug_fn size: $debug_fs at ". localtime()."\n";
    (my $iis_logfh = new FileHandle("<$debug_fn")) || push @html, "ERROR ACCESSING \"$debug_fn\" - $!";
    if ( $iis_logfh ) {
      $iis_logfh->binmode();
      $iis_logfh->seek(-63000, 2);
      $iis_logfh->read(my $buff, 63000);
      $iis_logfh->close();
      $buff =~ s/[\s\x00]+$//gs;
      $buff = substr($buff, (length($buff) - 32760)) if length($buff) > 32760;
      #    $buff =~ s/.*(.{1,32760})$/$1/;
      push @html, $buff."\n";
    }
    push @html, localtime()."\n";
  }
  else {
    push @html, "Debug Log File \"$debug_fn\" for site $main::servername not found\n";
  }
  return $main::Response->Write('<pre>'.join('',@html).'</pre>');
}



sub SOAPError {
  $main::Response->Write(XReport::SOAP::BUILD_SOAP_ERROR(@_));
  $main::Response->{Status} = $_[3] || 400;
  $main::msgtolog = "error: " . $_[2];
}

sub readContent {
  my ($reqident, $method) = (shift, shift);
  my $buffsize = 32*1024;
  my $TotalBytes = $main::Request->TotalBytes();
  (my $RequestData, $main::ContentLength) = ('', 0);
  if ( $TotalBytes ) {
    while($TotalBytes > 0) {
      my $bytes2read = ($TotalBytes > $buffsize ? $buffsize : $TotalBytes);
      main::debug2log("${method} $reqident request: reading $bytes2read of $TotalBytes left");
      my $buff = $main::Request->BinaryRead($bytes2read);
      $RequestData .= $buff if $buff;
      $main::ContentLength += length($buff) if $buff;

      $TotalBytes -= $bytes2read;
    }
  }
  else {
    while (1) {
      my $buff = $main::Request->BinaryRead($buffsize);
      $RequestData .= $buff if $buff;
      $main::ContentLength += length($buff) if $buff;
      last if !$buff || length($buff) < $buffsize;
    }
  }
  return $RequestData;
}

sub toUI1 { return new Win32::OLE::Variant(VT_UI1, $_[0]); }

sub sendBinary {
  my $binstream = new Win32::OLE::Variant(VT_UI1, $_[0]);
  $main::Response->BinaryWrite($binstream) ;
}

my ($reqident, $reqmet, $qrystr, $httpaction, $conttype, $httpserverpath, $https, $SOAPAct, $remote_addr, $user_agent) =
 map { $main::Request->ServerVariables($_)->item() } 
  qw(HTTP_REQUEST_ID REQUEST_METHOD QUERY_STRING 
     URL CONTENT_TYPE HTTP_HOST HTTPS HTTP_SOAPAction REMOTE_ADDR HTTP_USER_AGENT);

main::write2log("XreportWebIface V1 at $ENV{COMPUTERNAME} Processing request from $remote_addr using - $user_agent");
main::write2log("Application name is ".$main::Application->{ApplName});
my $class = 'XReport::'.$main::Application->{ApplName};
#*{$class.'::SOAPError'} = &SOAPError;
#*{$class.'::strftime'} = &XReport::SOAP::strftime;
#*{$class.'::encode_base64'} = &XReport::SOAP::_encode_base64; 
#*{$class.'::decode_base64'} = &XReport::SOAP::_decode_base64; 
 
$reqident = 0 unless $reqident;
my $proto = 'HTTP'.($https eq 'off' ? ':' : 'S:');
my $actRE = qr/^\/?("?)(?:(.*?)[#\/\@\?]+)?(.*)\1$/;
my ($pkgref, $method) = ($httpaction =~ /$actRE/)[1,2] if $httpaction;
($pkgref, $method) = ($main::Application->{ApplName}, $SOAPAct) if $SOAPAct;
$method = ($SOAPAct =~ /$actRE/)[-1] if $SOAPAct;
#my ($pkgref, $method) = ($httpaction =~ /^\/?(\"?)(.*)[#\/\@](.*)\1$/)[1,2] if $httpaction;

my $parsesub = $class->can('_parseContent');
main::write2log("init Method: $reqmet, httpaction: -$httpaction-, QRYSTR: -$qrystr-, SOAPAct: -$SOAPAct-, method: -$method-, class: -$class-, parsesub: $parsesub");
main::debug2log("${method} $reqident ALL_RAW:", $main::Request->ServerVariables('ALL_RAW')->item());
($qrystr, my %parms) = split /[\@\/\#\&=]/, $qrystr; #($qrystr =~ /^([\w\-\_]+)[\@\/\#\&](.*)$/g);
main::debug2log("${method} $reqident qrystr__:", $qrystr, " parms: ", %parms);

$parsesub = ($SOAPAct 
	     ? sub { my ($method, @parms) = ( shift, @_ );
	       main::debug2log("SOAP parsesub default $method  - selection: @parms");
	       return XReport::SOAP::parseSOAPreq(readContent($reqident, $method)); } 
	     : sub { my ($method, @parms) = ( shift, @_ );
		       main::debug2log("HTML parsesub $method  - selection: @parms");
		       return { 
			     request => { split(/[=&]/, readContent($reqident, $method) ), @parms } }; } )
  unless $parsesub;

$main::Response->Clear();
$main::Response->{'Buffer'} = 0;
#$main::Response->{'Buffer'} = 1;

my $msub;
if ( $reqmet eq 'GET' && $qrystr && $qrystr =~ /^WSDL$/i ) {
#  $main::Response->ClearContent();
#  $main::Response->AddHeader('Content-Type', 'text/xml');
  $main::Response->{ContentType} = 'text/xml; charset=iso-8859-1';

#  $main::Response->AddHeader('Content_type', 'text/xml');
  my $wsdl = $main::Application->{WSDL};
  unless ( $wsdl) {
    my ($reqident, $httpaction, $httpserverpath, $https) =
      map { $main::Request->ServerVariables($_)->item() } qw(HTTP_REQUEST_ID URL HTTP_HOST HTTPS);
    $reqident = 0 unless $reqident;
    my $proto = 'HTTP'.($https eq 'off' ? ':' : 'S:');
    $wsdl = XReport::SOAP::buildWSDL("$proto//${httpserverpath}$httpaction", $main::Application->{ApplName} ); 
    main::write2log("sendWSDL storing WSDL in APP pool for $main::servername");
    $main::Application->{WSDL} = $wsdl;
  }
  $main::Response->Write($wsdl);
}
elsif ( $reqmet =~ /^(?:GET|POST)$/ && $qrystr && $qrystr =~ /^ShowServer[^\w]*/i ) {
  (my $ss = $main::Application->{"XREPORT_HOME"}."/bin/ShowServer.pl") =~ s/\\/\//g;
  require $ss;
  main::ShowServer( skipXreport => 1, content => &$parsesub($method) );    
}
elsif ( $reqmet =~ /^(?:GET|POST)$/ && $qrystr && $qrystr =~ /^setDebug=([\d])[^\w]*/i ) {
  $main::Application->{DEBUG} = $1;
  $main::DEBUG = $1;
}
elsif ( $reqmet eq 'GET' && $qrystr && $qrystr =~ /^showlog$/i ) {
  $msub = main->can('_GET_'.uc($qrystr));
  $method = uc($qrystr);
} 
elsif ( $reqmet eq 'GET' && $qrystr && $qrystr =~ /^testpage$/i ) {
  $method = $qrystr;
  $msub = sub {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    
    my ($tbl, $maxlines, $IndexEntries, $orderby, $distinct, $newentries) = 
      @{$selection->{request}}{qw(IndexName TOP IndexEntries ORDERBY DISTINCT NewEntries)}
	if ref($selection);
    
    $main::Response->{ContentType} = 'text/html';
    my $html = [ '<HTML>' ];
    push @$html, ( '<h3>', "Hello From ", $class, '</h3>' );
    push @$html, "<pre>Selection:\n";
    push @$html, XReport::SOAP::Dumper($selection);
    push @$html, "</pre>";
    
    $main::Response->Write(join '', (@$html, '</HTML>') );
  }
}
elsif ( $reqmet eq 'POST' && $SOAPAct && ($msub = $class->can($method)) ) {
  $main::Response->{ContentType} = 'text/xml; charset=iso-8859-1';
#  $method = ($SOAPAct =~ /$actRE/)[-1] if $SOAPAct;
}
elsif ( $reqmet eq 'POST' && $qrystr && $qrystr =~ /^\w+$/i && ($msub = $class->can($qrystr)) ) {
  $method = $qrystr;
} 
elsif ( $reqmet eq 'GET' && $qrystr && $qrystr =~ /^\w+$/i && ($msub = $class->can('_GET_'.uc($qrystr))) ) {
  $method = uc($qrystr);
} 
else {
  $main::Response->{Status} = 400;
}

if ( $msub) {
  my $selection = &$parsesub($method, %parms);
  $main::authUserName = $selection->{soaphdrs}->{Authentication}->{userid};
  if($selection->{request}->{IndexEntries}[0]->{Columns}){
        foreach my $key (sort keys %{$selection->{request}->{IndexEntries}[0]->{Columns}}) { 
            $main::parameters .= "$key == $selection->{request}->{IndexEntries}[0]->{Columns}->{$key}->{content}::" if($selection->{request}->{IndexEntries}[0]->{Columns}->{$key}->{content});
            $main::parameters .= "$key == $selection->{request}->{IndexEntries}[0]->{Columns}->{$key}->{Min}||$selection->{request}->{IndexEntries}[0]->{Columns}->{$key}->{Max}::" if($selection->{request}->{IndexEntries}[0]->{Columns}->{$key}->{operation}) 
        }
        $main::parameters = substr($main::parameters, 0, -2) if($main::parameters);
  }
  main::debug2log("$reqmet $method Invoking Handling routine - selection: ", XReport::SOAP::Dumper($selection));
  eval { &$msub($method, $reqident, $selection); };
  main::SOAPError("$method", "Invoke", "$@", 500) if $@;
}
else {
  main::debug2log("$reqmet $method is an undefined code ");
}

my $timesnow = [ times() ];
for my $ix (0..3) {
  $timesnow->[$ix] = $timesnow->[$ix] - $main::timesmark->[$ix];
}
my $timesmsg = "Times(us::sy::cus::csy):_" . join('::', @{$timesnow} ) ;
#$main::endtime = strftime "%Y-%m-%d %H:%M:%S.%U", localtime;
my $t = time; $main::endtime =  strftime '%Y-%m-%d %H:%M:%S', localtime $t ; $main::endtime.= sprintf '.%03d',($t-int($t))*1000 ;
$main::Response->Flush();
main::write2log("$reqmet $method Request $reqident  ended: ", $timesmsg, " " . $main::authUserName . " ", ($main::msgtolog || ''));
$main::Response->AppendToLog("${method}_start_at_${main::starttime}_-ID_$reqident-_"
                 . $timesmsg . '_' . $main::authUserName . '_'."${main::endtime}_"
			     . ($main::msgtolog || $main::parameters)
			    );
$main::Session->Abandon();
%>
