use strict;

sub _addWhere
{
	my $field = shift;
}

sub GetAllBundles
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $BundleName = $IndexEntries->[0]->{'Columns'}->{'BundleName'}->{'content'};
	my $BundleCategory = $IndexEntries->[0]->{'Columns'}->{'BundleCategory'}->{'content'};
	my $JobName = $IndexEntries->[0]->{'Columns'}->{'JobName'}->{'content'};
	my $ReportName = $IndexEntries->[0]->{'Columns'}->{'ReportName'}->{'content'};
	my $ReportFrom = $IndexEntries->[0]->{'Columns'}->{'From'}->{'content'};
	my $ReportTo = $IndexEntries->[0]->{'Columns'}->{'To'}->{'content'};
    my $BundleFrom = $IndexEntries->[0]->{'Columns'}->{'BundleElabFrom'}->{'content'};
	my $BundleTo = $IndexEntries->[0]->{'Columns'}->{'BundleElabTo'}->{'content'};
	my $Recipient = $IndexEntries->[0]->{'Columns'}->{'Recipient'}->{'content'};
    my $Limit = $IndexEntries->[0]->{'Columns'}->{'Limit'}->{'content'};
    my $prtvars = {TOPLINES => $Limit, FromDate => $ReportFrom, ToDate => $ReportTo, BundleFrom=> $BundleFrom ,BundleTo=> $BundleTo,  RecipientName => $Recipient, JobName => $JobName, BundleName => $BundleName, BundleCategory => $BundleCategory, ReportName => $ReportName};   

    my @sqlArray;
    foreach my $l (split /\n/, $XReport::cfg->{sqlprocs}->{BundlesNames})
    {      
      $l =~ s/--REPORTDATA--// if($l =~ /^\s*--REPORTDATA--/  and $IndexEntries->[0]->{'Columns'}->{'From'}->{'content'} !~ /^\s*$/);
      $l =~  s/\#\{(.+?)\}\#/$prtvars->{$1}/g; 
      push @sqlArray, $l;
    }
    my $sqlString = join "\n", @sqlArray;

	main::debug2log($sqlString);
	
	my $result = _getDataFromSQL($sqlString);
    	my $actionMessage;
    	if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
   	}            
    	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
    	push @{$respEntries}, @{$result} if($result);
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'GetBundles',
		IndexEntries => $respEntries
	}));
}

sub GetAllBundlesElab
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $BundleName = $IndexEntries->[0]->{'Columns'}->{'BundleName'}->{'content'};
	my $BundleCategory = $IndexEntries->[0]->{'Columns'}->{'BundleCategory'}->{'content'};
	my $BundleTo = $IndexEntries->[0]->{'Columns'}->{'BundleElabTo'}->{'content'};
    my $BundleFrom = $IndexEntries->[0]->{'Columns'}->{'BundleElabFrom'}->{'content'};
    my $Limit = $IndexEntries->[0]->{'Columns'}->{'Limit'}->{'content'};
    my $prtvars = {TOPLINES => $Limit, BundleFrom=> $BundleFrom, BundleTo=> $BundleTo, BundleName => $BundleName, BundleCategory => $BundleCategory};   

    my @sqlArray;
    foreach my $l (split /\n/, $XReport::cfg->{sqlprocs}->{BundlesNamesElab})
    {      
      $l =~ s/--REPORTDATA--// if($l =~ /^\s*--REPORTDATA--/  and $IndexEntries->[0]->{'Columns'}->{'From'}->{'content'} !~ /^\s*$/);
      $l =~  s/\#\{(.+?)\}\#/$prtvars->{$1}/g; 
      push @sqlArray, $l;
    }
    my $sqlString = join "\n", @sqlArray;

	main::debug2log($sqlString);
	
	my $result = _getDataFromSQL($sqlString);
    	my $actionMessage;
    	if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
   	}            
    	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
    	push @{$respEntries}, @{$result} if($result);
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'GetBundles',
		IndexEntries => $respEntries
	}));
}


sub GetBundlesRecipient
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $BundleId = $IndexEntries->[0]->{'Columns'}->{'BundleId'}->{'content'};
	my $JobName = $IndexEntries->[0]->{'Columns'}->{'JobName'}->{'content'};
	my $Text = $IndexEntries->[0]->{'Columns'}->{'Text'}->{'content'};
    my $From = $IndexEntries->[0]->{'Columns'}->{'From'}->{'content'};
	my $To = $IndexEntries->[0]->{'Columns'}->{'To'}->{'content'};
	my $BundleFrom = $IndexEntries->[0]->{'Columns'}->{'BundleElabFrom'}->{'content'};
	my $BundleTo = $IndexEntries->[0]->{'Columns'}->{'BundleElabTo'}->{'content'};
    my $Recipient = $IndexEntries->[0]->{'Columns'}->{'Recipient'}->{'content'};
    my $Limit = $IndexEntries->[0]->{'Columns'}->{'Limit'}->{'content'};
    my $BundleName = $IndexEntries->[0]->{'Columns'}->{'BundleName'}->{'content'};
	my $BundleCategory = $IndexEntries->[0]->{'Columns'}->{'BundleCategory'}->{'content'};
    my $prtvars = {TOPLINES => $Limit, FromDate => $From, ToDate => $To,BundleFrom => $BundleFrom, BundleTo => $BundleTo, RecipientName => $Recipient, JobName => $JobName, Text => $Text, BundleId => $BundleId, BundleName => $BundleName, BundleCategory => $BundleCategory};   

    my @sqlArray;
    foreach my $l (split /\n/, $XReport::cfg->{sqlprocs}->{BundlesRecipient})
    {      
      $l =~ s/--REPORTDATA--// if($l =~ /^\s*--REPORTDATA--/  and $IndexEntries->[0]->{'Columns'}->{'From'}->{'content'} !~ /^\s*$/);
      $l =~  s/\#\{(.+?)\}\#/$prtvars->{$1}/g; 
      push @sqlArray, $l;
    }
    #foreach my $l (split /\n/, $XReport::cfg->{sqlprocs}->{BundlesRecipient})
    #{
      #$l =~  s/\#\{(.+?)\}\#/$prtvars->{$1}/g; 
      #push @sqlArray, $l;
    #}
    my $sqlString = join "\n", @sqlArray;

	main::debug2log($sqlString);
	
	my $result = _getDataFromSQL($sqlString);
    	my $actionMessage;
    	if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
   	}            
    	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
    	push @{$respEntries}, @{$result} if($result);
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'GetBundles',
		IndexEntries => $respEntries
	}));
}

sub GetBundlesNameRecipient
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportId = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
	my $ReportId = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $BundleName = $IndexEntries->[0]->{'Columns'}->{'BundleName'}->{'content'};
    my $prtvars = {BundleName => $BundleName};   

    my @sqlArray;
    foreach my $l (split /\n/, $XReport::cfg->{sqlprocs}->{BundlesNameRecipient})
    {
      $l =~  s/\#\{(.+?)\}\#/$prtvars->{$1}/g; 
      push @sqlArray, $l;
    }
    my $sqlString = join "\n", @sqlArray;

	main::debug2log($sqlString);
	if($JobReportId !~ /^\s*$/)
	{
		$sqlString .= " AND jr.JobReportId = '$JobReportId'";
	}
    if($ReportId !~ /^\s*$/)
	{
		$sqlString .= " AND lr.ReportId = '$ReportId'";
	}
	my $result = _getDataFromSQL($sqlString);
    	my $actionMessage;
    	if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
   	}            
    	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
    	push @{$respEntries}, @{$result} if($result);
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'GetBundles',
		IndexEntries => $respEntries
	}));
}

sub GetBundlesLog
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $BundleId = $IndexEntries->[0]->{'Columns'}->{'BundleId'}->{'content'};
	my $JobName = $IndexEntries->[0]->{'Columns'}->{'JobName'}->{'content'};
	my $Text = $IndexEntries->[0]->{'Columns'}->{'Text'}->{'content'};
    my $From = $IndexEntries->[0]->{'Columns'}->{'From'}->{'content'};
	my $To = $IndexEntries->[0]->{'Columns'}->{'To'}->{'content'};
	my $BundleFrom = $IndexEntries->[0]->{'Columns'}->{'BundleElabFrom'}->{'content'};
	my $BundleTo = $IndexEntries->[0]->{'Columns'}->{'BundleElabTo'}->{'content'};
    my $Recipient = $IndexEntries->[0]->{'Columns'}->{'Recipient'}->{'content'};
    my $Limit = $IndexEntries->[0]->{'Columns'}->{'Limit'}->{'content'};
    my $BundleName = $IndexEntries->[0]->{'Columns'}->{'BundleName'}->{'content'};
	my $BundleCategory = $IndexEntries->[0]->{'Columns'}->{'BundleCategory'}->{'content'};
main::debug2log(Dumper($IndexEntries));
    my $prtvars = {TOPLINES => $Limit, FromDate => $From, ToDate => $To,BundleFrom => $BundleFrom, BundleTo => $BundleTo, RecipientName => $Recipient, JobName => $JobName, Text => $Text, BundleId => $BundleId, BundleName => $BundleName, BundleCategory => $BundleCategory};   

    my @sqlArray;
    foreach my $l (split /\n/, $XReport::cfg->{sqlprocs}->{BundlesLog})
    {      
      $l =~ s/--REPORTDATA--// if($l =~ /^\s*--REPORTDATA--/  and $IndexEntries->[0]->{'Columns'}->{'From'}->{'content'} !~ /^\s*$/);
      $l =~  s/\#\{(.+?)\}\#/$prtvars->{$1}/g; 
      push @sqlArray, $l;
    }
    my $sqlString = join "\n", @sqlArray;

	main::debug2log($sqlString);
	
	my $result = _getDataFromSQL($sqlString);
    	my $actionMessage;
    	if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
   	}            
    	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
    	push @{$respEntries}, @{$result} if($result);
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'GetBundles',
		IndexEntries => $respEntries
	}));
}

sub GetBundlesQueue
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $BundleName = $IndexEntries->[0]->{'Columns'}->{'BundleName'}->{'content'};
	my $JobName = $IndexEntries->[0]->{'Columns'}->{'JobName'}->{'content'};
	my $ReportName = $IndexEntries->[0]->{'Columns'}->{'ReportName'}->{'content'};
	my $From = $IndexEntries->[0]->{'Columns'}->{'From'}->{'content'};
	my $To = $IndexEntries->[0]->{'Columns'}->{'To'}->{'content'};
    my $Limit = $IndexEntries->[0]->{'Columns'}->{'Limit'}->{'content'};
    main::debug2log(Dumper($IndexEntries));
    my $prtvars = {TOPLINES => $Limit, FromDate => $From, ToDate => $To, BundleName => $BundleName, JobName => $JobName, ReportName => $ReportName};   

    my @sqlArray;
    foreach my $l (split /\n/, $XReport::cfg->{sqlprocs}->{BundlesQueue})
    {
      $l =~  s/\#\{(.+?)\}\#/$prtvars->{$1}/g; 
      push @sqlArray, $l;
    }
    my $sqlString = join "\n", @sqlArray;

	main::debug2log($sqlString);
	
	my $result = _getDataFromSQL($sqlString);
    	my $actionMessage;
    	if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
   	}            
    	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
    	push @{$respEntries}, @{$result} if($result);
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'BundlesQueue',
		IndexEntries => $respEntries
	}));
}

sub GetBundlesQueueOld
{
   	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $BundleName = $IndexEntries->[0]->{'Columns'}->{'BundleName'}->{'content'};
	my $JobName = $IndexEntries->[0]->{'Columns'}->{'JobName'}->{'content'};
	my $ReportName = $IndexEntries->[0]->{'Columns'}->{'ReportName'}->{'content'};
	my $From = $IndexEntries->[0]->{'Columns'}->{'From'}->{'content'};
	my $To = $IndexEntries->[0]->{'Columns'}->{'To'}->{'content'};
    my $Limit = $IndexEntries->[0]->{'Columns'}->{'Limit'}->{'content'};

	my $whereClause = "";
	my $sql = "SELECT $Limit * FROM".
	"( SELECT ".
	"COALESCE(LRT.textString, JR.UserRef, JR.JobReportName) AS ReportName,  LR.FilterValue, LR.TotPages,  ".
	"LR.XferDateDiff, LR.XferRecipient,   ".
	"CASE WHEN LR.CheckStatus = 1 THEN 'Checked' ELSE 'Not Checked' END AS CheckStatus,   ".
	"CASE WHEN LR.HasNotes = 1 THEN 'Checked' ELSE 'Not Checked' END AS HasNotes,  LR.LastUsedToView, ". 
	"LR.LastUsedToCheck,  JR.JobReportName, JR.JobName, JR.JobNumber, ".
	"BQ.BundleName,BQ.JobReportId AS JobReport_ID,BQ.ReportId,BQ.ReportTimeRef, BQ.textId, BQ.PrintCopies AS PrintCopies ".
	"FROM tbl_BundlesQ BQ ".
	"INNER JOIN tbl_LogicalReports LR ON LR.JobReportId = BQ.JobReportId AND BQ.ReportId = LR.ReportId  ".
	"INNER JOIN tbl_LogicalReportsTexts LRT ON BQ.textId = LRT.textId  ".
	"INNER JOIN tbl_JobReports JR ON BQ.JobReportId = JR.JobReportId  ) B";

	if($BundleName !~ /^\s*$/)
	{
		$whereClause = _addWhereClause($whereClause, "B.BundleName LIKE ",$BundleName."%", "'", "AND") ;
	}
	if($JobName !~ /^\s*$/)
	{
		$whereClause = _addWhereClause($whereClause, "B.JobName LIKE ",$JobName."%", "'", "AND") ;
	}
	if($ReportName !~ /^\s*$/)
	{
		$whereClause = _addWhereClause($whereClause, "B.ReportName LIKE ",$ReportName."%", "'", "AND") ;
	}
	if($From !~ /^\s*$/)
	{
      #$whereClause = _addWhereClause($whereClause, "B.ReportTimeRef >= ",$From, "'", "AND") ;
        $whereClause = _addWhereClause($whereClause, " Convert (varchar ,B.ReportTimeRef ,112) >= ", $From , "'", "AND")
	}
	if($To !~ /^\s*$/)
	{
	    $whereClause = _addWhereClause($whereClause, " Convert (varchar ,B.ReportTimeRef ,112) <= ", $To , "'", "AND");
	}

	
	$sql .= $whereClause." ORDER BY B.ReportTimeRef, B.BundleName";
	main::debug2log($sql);
    my $result = _getDataFromSQL($sql);
    	my $actionMessage;
    	if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
    	}	            
    	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
    	push @{$respEntries}, @{$result} if($result);
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'GetBundlesQueue',
		IndexEntries => $respEntries
	}));
}


sub DeleteBundlesQueueEntry
{
    	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $BundleName = $IndexEntries->[0]->{'Columns'}->{'BundleName'}->{'content'};
	my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
	my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION DELBUNDLES");
	eval{	
	    my $sql = "DELETE FROM tbl_BundlesQ ". 
	    "WHERE BundleName = '$BundleName' AND JobReportId = '$JobReportID' AND ReportId = '$ReportID' ";

	    my $res = _updateDataFromSQL($sql);
    };
	if($@)
	{
		$actionMessage = "FAILED: $@";
		_updateDataFromSQL("ROLLBACK TRANSACTION DELBUNDLES");
	}
	else
	{
		$actionMessage = "SUCCESS";
		_updateDataFromSQL("COMMIT TRANSACTION DELBUNDLES");
	}

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'BUNDLES',
		IndexEntries => $respEntries
	}));
}

sub DeleteBundlesQueue
{
    	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $BundleName = $IndexEntries->[0]->{'Columns'}->{'BundleName'}->{'content'};
    my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION DELBUNDLES");
	eval{	
	    my $sql = "DELETE FROM tbl_BundlesQ ". 
	    "WHERE BundleName = '$BundleName' ";

	    my $res = _updateDataFromSQL($sql);
    };
	if($@)
	{
		$actionMessage = "FAILED: $@";
		_updateDataFromSQL("ROLLBACK TRANSACTION DELBUNDLES");
	}
	else
	{
		$actionMessage = "SUCCESS";
		_updateDataFromSQL("COMMIT TRANSACTION DELBUNDLES");
	}

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'BUNDLES',
		IndexEntries => $respEntries
	}));
}

sub RequeueBundle
{

    	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $BundleName = $IndexEntries->[0]->{'Columns'}->{'BundleName2'}->{'content'};
    my $BundleId = $IndexEntries->[0]->{'Columns'}->{'BundleId'}->{'content'};
	my $JobReportId = $IndexEntries->[0]->{'Columns'}->{'JobReportId'}->{'content'};
	my $ReportId = $IndexEntries->[0]->{'Columns'}->{'ReportId'}->{'content'};
	my $textId = $IndexEntries->[0]->{'Columns'}->{'textId'}->{'content'};
	my $ReportTimeRef = $IndexEntries->[0]->{'Columns'}->{'ReportTimeRef'}->{'content'};
	my $Copies = $IndexEntries->[0]->{'Columns'}->{'Copies'}->{'content'};
	my $actionMessage = "SUCCESS";

	_updateDataFromSQL("BEGIN TRANSACTION REQUEUE");
	eval{
      #my $sel = "SELECT * FROM tbl_BundlesQ WHERE BundleName = '$BundleName' AND JobReportId = '$JobReportId' AND ReportId = '$ReportId' AND textId = '$textId'";
      #my $result = _getDataFromSQL($sel);
      #if (!$result)
      #{
      	    my $sql = "INSERT INTO tbl_BundlesQ (BundleName,JobReportId,ReportId,textId,ReportTimeRef,PrintCopies) ".
            "SELECT TOP 1 '$BundleName', '$JobReportId', '$ReportId', '$textId', GETDATE(), CASE WHEN $Copies='-1' THEN BL.BQCopies ELSE $Copies END ".
            "FROM tbl_BundlesLog BL WHERE BundleId = '$BundleId' AND JobReportId = '$JobReportId' AND ReportId = '$ReportId' AND textId = '$textId'";
            #VALUES ('$BundleName','$JobReportId','$ReportId','$textId',GETDATE(),'$Copies') ";
			main::debug2log("RequeueBundle:INSERT_".$sql);	
			_updateDataFromSQL($sql);
            #}	
            #	else {die "This bundle in already requeued."}
	};
	if($@)
	{
		$actionMessage = "FAILED: This bundle in already requeued.";
		_updateDataFromSQL("ROLLBACK TRANSACTION REQUEUE");
	}
	else
	{
		$actionMessage = "Operation completed.";
		_updateDataFromSQL("COMMIT TRANSACTION REQUEUE");
	}

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'BUNDLES',
		IndexEntries => $respEntries
	}));		
}

sub RequeueBundles
{

    my ($method, $reqident, $selection) = (shift, shift, shift);
	main::debug2log("REQUEUE_Bundles_");
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $BundleId = $IndexEntries->[0]->{'Columns'}->{'BundleId'}->{'content'};
    my $Recipient = $IndexEntries->[0]->{'Columns'}->{'Recipient'}->{'content'};
    my $Copies = $IndexEntries->[0]->{'Columns'}->{'Copies'}->{'content'};
    my $BundleName = $IndexEntries->[0]->{'Columns'}->{'BundleName'}->{'content'};
    my $BundleName2 = $IndexEntries->[0]->{'Columns'}->{'BundleName2'}->{'content'};
	my $BundleCat = $IndexEntries->[0]->{'Columns'}->{'BundleCategory'}->{'content'};
	my $JobName = $IndexEntries->[0]->{'Columns'}->{'JobName'}->{'content'};
	my $ReportName = $IndexEntries->[0]->{'Columns'}->{'ReportName'}->{'content'};
	my $From = $IndexEntries->[0]->{'Columns'}->{'From'}->{'content'};
	my $To = $IndexEntries->[0]->{'Columns'}->{'To'}->{'content'};
    #my $BundleFrom = $IndexEntries->[0]->{'Columns'}->{'BundleElabFrom'}->{'content'};
	#my $BundleTo = $IndexEntries->[0]->{'Columns'}->{'BundleElabTo'}->{'content'};
    my $DateFrom = $IndexEntries->[0]->{'Columns'}->{'From'}->{'content'};
	my $DateTo = $IndexEntries->[0]->{'Columns'}->{'To'}->{'content'};

	#XferStartTime
	my $actionMessage = "SUCCESS";  

    my $sql = "INSERT INTO dbo.tbl_BundlesQ  ".
            "SELECT DISTINCT '$BundleName2', BL.JobReportID, BL.ReportId, BL.textId,  BL.ReportTimeRef, CASE WHEN $Copies='-1' THEN BL.BQCopies ELSE $Copies END, GETDATE() ".
            "FROM tbl_Bundles B ".
            "INNER JOIN tbl_BundlesLog BL ON B.BundleId = BL.BundleId ".
            "INNER JOIN tbl_JobReports JR ON BL.JobReportId = JR.JobReportId ".
            "INNER JOIN tbl_LogicalReports LR ON LR.JobReportId = BL.JobReportId AND BL.ReportId = LR.ReportId  ".
            "INNER JOIN tbl_LogicalReportsTexts LRT ON BL.textId = LRT.textId  ";     
    my $whereClause = "WHERE BL.BQCopies > 0 and B.BundleId = $BundleId";
    if($BundleName !~ /^\s*$/)
	{
		$whereClause = _addWhereClause($whereClause, "B.BundleName LIKE ",$BundleName."%", "'", "AND") ;
	}
	if($BundleCat !~ /^\s*$/)
	{
		$whereClause = _addWhereClause($whereClause, "B.BundleCategory LIKE ",$BundleCat."%", "'", "AND") ;
	}
	if($JobName !~ /^\s*$/)
	{
		$whereClause = _addWhereClause($whereClause, "JR.JobName LIKE ",$JobName."%", "'", "AND") ;
	}
	if($ReportName !~ /^\s*$/)
	{
		$whereClause = _addWhereClause($whereClause, "COALESCE(NULLIF(LRT.textString, '') , NULLIF(JR.UserRef, ''), JR.JobReportName) LIKE ",$ReportName."%", "'", "AND") ;
	}
	
#	if($BundleFrom !~ /^\s*$/)
#	{
#		if($BundleFrom !~ /^[0-9]{8}$/) 
#		{
#			$whereClause = _addWhereClause($whereClause, "B.ElabStartTime >= ",$BundleFrom, "'", "AND") ;
#		}
#        else
#		{
#			$whereClause = _addWhereClause($whereClause, " Convert (varchar ,B.ElabStartTime ,112) >= ", $BundleFrom , "'", "AND")
#		}
#        
#	}
#	
#	
#	if($BundleTo !~ /^\s*$/)
#	{
#		if($BundleTo !~ /^[0-9]{8}$/) 
#		{
#			$whereClause = _addWhereClause($whereClause, "B.ElabEndTime <= ",$BundleTo, "'", "AND") ;
#		}
#		else
#		{
#			$whereClause = _addWhereClause($whereClause, " Convert (varchar ,B.ElabStartTime ,112) <= ", $BundleTo , "'", "AND")			
#		}
#	}
	if($DateFrom !~ /^\s*$/)
	{
		if($DateFrom !~ /^[0-9]{8}$/) 
		{
			$whereClause = _addWhereClause($whereClause, "JR.XferStartTime >= ",$DateFrom, "'", "AND") ;
		}
        else
		{
			$whereClause = _addWhereClause($whereClause, " Convert (varchar ,JR.XferStartTime ,112) >= ", $DateFrom , "'", "AND")
		}
        
	}
	
	
	if($DateTo !~ /^\s*$/)
	{
		if($DateTo !~ /^[0-9]{8}$/) 
		{
			$whereClause = _addWhereClause($whereClause, "JR.XferStartTime <= ",$DateTo, "'", "AND") ;
		}
		else
		{
			$whereClause = _addWhereClause($whereClause, " Convert (varchar ,JR.XferStartTime ,112) <= ", $DateTo , "'", "AND")			
		}
	}
	
   	if($Recipient !~ /^\s*$/)
	{
		$whereClause = _addWhereClause($whereClause, "BL.RecipientName LIKE ",$Recipient."%", "'", "AND") ;
	}
 
	
    $sql .= $whereClause;
	main::debug2log("REQUEUE:INSERT_".$sql);	
    _updateDataFromSQL("BEGIN TRANSACTION REQUEUEALL");
	eval{
      _updateDataFromSQL($sql);
		main::debug2log("-------------");
	};
	if($@)
	{
		$actionMessage = "FAILED:";
		_updateDataFromSQL("ROLLBACK TRANSACTION REQUEUEALL");
	}
	else
	{
		$actionMessage = "Operation completed";
		_updateDataFromSQL("COMMIT TRANSACTION REQUEUEALL");
	}

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'BUNDLES',
		IndexEntries => $respEntries
	}));		
}

sub RequeueBundlesRecipient
{

    my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $BundleId = $IndexEntries->[0]->{'Columns'}->{'BundleId'}->{'content'};
    my $Recipient = $IndexEntries->[0]->{'Columns'}->{'Recipient'}->{'content'};
    my $Copies = $IndexEntries->[0]->{'Columns'}->{'Copies'}->{'content'};
    my $BundleName = $IndexEntries->[0]->{'Columns'}->{'BundleName'}->{'content'};
    my $BundleName2 = $IndexEntries->[0]->{'Columns'}->{'BundleName2'}->{'content'};
	my $BundleCat = $IndexEntries->[0]->{'Columns'}->{'BundleCategory'}->{'content'};
	my $JobName = $IndexEntries->[0]->{'Columns'}->{'JobName'}->{'content'};
	my $ReportName = $IndexEntries->[0]->{'Columns'}->{'ReportName'}->{'content'};
	my $From = $IndexEntries->[0]->{'Columns'}->{'From'}->{'content'};
	my $To = $IndexEntries->[0]->{'Columns'}->{'To'}->{'content'};
    #my $BundleFrom = $IndexEntries->[0]->{'Columns'}->{'BundleElabFrom'}->{'content'};
	#my $BundleTo = $IndexEntries->[0]->{'Columns'}->{'BundleElabTo'}->{'content'};
    my $DateFrom = $IndexEntries->[0]->{'Columns'}->{'From'}->{'content'};
	my $DateTo = $IndexEntries->[0]->{'Columns'}->{'To'}->{'content'};

	my $actionMessage = "SUCCESS";
    my $whereClause = "";
    my $sql = "INSERT INTO dbo.tbl_BundlesQ (BundleName, JobReportId, ReportId, textId, ReportTimeRef, PrintCopies) ".
            "SELECT DISTINCT '$BundleName2', BL.JobReportID, BL.ReportId, BL.textId, GETDATE(), CASE WHEN $Copies='-1' THEN BL.BQCopies ELSE $Copies END ".
            "FROM tbl_Bundles B ".
            "INNER JOIN tbl_BundlesLog BL ON B.BundleId = BL.BundleId ".
            "INNER JOIN tbl_JobReports JR ON BL.JobReportId = JR.JobReportId ".
            "INNER JOIN tbl_LogicalReports LR ON LR.JobReportId = BL.JobReportId AND BL.ReportId = LR.ReportId  ".
            "INNER JOIN tbl_LogicalReportsTexts LRT ON BL.textId = LRT.textId  ";           
           
    $whereClause = "WHERE BL.BQCopies > 0 AND B.BundleId = $BundleId AND BL.RecipientName = '$Recipient' ";
    if($BundleName !~ /^\s*$/)
	{
		$whereClause = _addWhereClause($whereClause, "B.BundleName LIKE ",$BundleName."%", "'", "AND") ;
	}
	if($BundleCat !~ /^\s*$/)
	{
		$whereClause = _addWhereClause($whereClause, "B.BundleCategory LIKE ",$BundleCat."%", "'", "AND") ;
	}
	if($JobName !~ /^\s*$/)
	{
		$whereClause = _addWhereClause($whereClause, "JR.JobName LIKE ",$JobName."%", "'", "AND") ;
	}
	if($ReportName !~ /^\s*$/)
	{
		$whereClause = _addWhereClause($whereClause, "COALESCE(NULLIF(LRT.textString, '') , NULLIF(JR.UserRef, ''), JR.JobReportName) LIKE ",$ReportName."%", "'", "AND") ;
	}
    #if($BundleFrom !~ /^\s*$/)
	#{
	#	if($BundleFrom !~ /^[0-9]{8}$/) 
	#	{
	#		$whereClause = _addWhereClause($whereClause, "B.ElabStartTime >= ",$BundleFrom, "'", "AND") ;
	#	}
	#	else
	#	{
	#		$whereClause = _addWhereClause($whereClause, " Convert (varchar ,B.ElabStartTime ,112) >= ", $BundleFrom , "'", "AND")
	#	}
	#}
	#if($BundleTo !~ /^\s*$/)
	#{
	#	if($BundleTo !~ /^[0-9]{8}$/) 
	#	{
	#		$whereClause = _addWhereClause($whereClause, "B.ElabEndTime <= ",$BundleTo, "'", "AND") ;
	#	}
	#	else
	#	{
	#		$whereClause = _addWhereClause($whereClause, " Convert (varchar ,B.ElabStartTime ,112) <= ", $BundleTo , "'", "AND")
	#	}
    #    
	#}
    if($DateFrom !~ /^\s*$/)
	{
		if($DateFrom !~ /^[0-9]{8}$/) 
		{
			$whereClause = _addWhereClause($whereClause, "JR.XferStartTime >= ",$DateFrom, "'", "AND") ;
		}
		else
		{
			$whereClause = _addWhereClause($whereClause, " Convert (varchar ,JR.XferStartTime ,112) >= ", $DateFrom , "'", "AND")
		}
	}
	if($DateTo !~ /^\s*$/)
	{
		if($DateTo !~ /^[0-9]{8}$/) 
		{
			$whereClause = _addWhereClause($whereClause, "JR.XferStartTime <= ",$DateTo, "'", "AND") ;
		}
		else
		{
			$whereClause = _addWhereClause($whereClause, " Convert (varchar ,JR.XferStartTime ,112) <= ", $DateTo , "'", "AND")
		}
        
	}
	
    $sql .= $whereClause;
	main::debug2log("RequeueBundlesRecipient:".$sql);	

	_updateDataFromSQL("BEGIN TRANSACTION REQUEUEALL");
	eval{
		my $result = _updateDataFromSQL($sql);
		main::debug2log("-------------");
	};
	if($@)
	{
		$actionMessage = "FAILED: $@";
		_updateDataFromSQL("ROLLBACK TRANSACTION REQUEUEALL");
	}
	else
	{
		$actionMessage = "Operation completed.";
		_updateDataFromSQL("COMMIT TRANSACTION REQUEUEALL");
	}

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'BUNDLES',
		IndexEntries => $respEntries
	}));		
}

sub UpdatePrintCopies{
  	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobName = $IndexEntries->[0]->{'Columns'}->{'JobName'}->{'content'};
	my $ReportName = $IndexEntries->[0]->{'Columns'}->{'ReportName'}->{'content'};
	my $Recipient = $IndexEntries->[0]->{'Columns'}->{'Recipient'}->{'content'};
    my $Copies = $IndexEntries->[0]->{'Columns'}->{'Copies'}->{'content'};
    my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION VARSETS");
	eval{     
      my $sel = "SELECT * FROM tbl_VarSetsValues WHERE VarName = '$JobName/$ReportName' AND VarSetName = 'PRTC:$Recipient'";
      main::debug2log($sel);
      my $result = _getDataFromSQL($sel);
      if (!$result)
      {
      	    my $sql = "INSERT INTO tbl_VarSetsValues (VarName,VarSetName,VarValue) VALUES ('$JobName/$ReportName','PRTC:$Recipient','$Copies') ";
            main::debug2log($sql);
			my $res = _updateDataFromSQL($sql);
      }	
      else {
            my $sql = "UPDATE tbl_VarSetsValues SET VarValue = '$Copies'  WHERE VarName = '$JobName/$ReportName' AND VarSetName = 'PRTC:$Recipient' ";
            main::debug2log($sql);
			my $res = _updateDataFromSQL($sql);
      }
	};
	if($@)
	{
		$actionMessage = "FAILED: Problems during the update.";
		_updateDataFromSQL("ROLLBACK TRANSACTION REQUEUE");
	}
	else
	{
		$actionMessage = "Operation completed.";
		_updateDataFromSQL("COMMIT TRANSACTION VARSETS");
	}

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'VARSETS',
		IndexEntries => $respEntries
	}));	
}

sub DeletePrintCopies{
  	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $JobName = $IndexEntries->[0]->{'Columns'}->{'JobName'}->{'content'};
	my $ReportName = $IndexEntries->[0]->{'Columns'}->{'ReportName'}->{'content'};
	my $Recipient = $IndexEntries->[0]->{'Columns'}->{'Recipient'}->{'content'};
    my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION VARSETS");
	eval{     
      my $sel = "SELECT * FROM tbl_VarSetsValues WHERE VarName = '$JobName/$ReportName' AND VarSetName = 'PRTC:$Recipient'";
      main::debug2log($sel);
      my $result = _getDataFromSQL($sel);
      if ($result)
      {
      	    my $sql = "DELETE tbl_VarSetsValues WHERE VarName = '$JobName/$ReportName' AND VarSetName = 'PRTC:$Recipient'";
            main::debug2log($sql);
			my $res = _updateDataFromSQL($sql);
      }	
      else {die "No records."}
	};
	if($@)
	{
		$actionMessage = "FAILED: No record to delete.";
		_updateDataFromSQL("ROLLBACK TRANSACTION REQUEUE");
	}
	else
	{
		$actionMessage = "Operation completed.";
		_updateDataFromSQL("COMMIT TRANSACTION VARSETS");
	}

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'VARSETS',
		IndexEntries => $respEntries
	}));	
}

sub GetBundlesNames
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};

    my $sqlString = "SELECT DISTINCT BundleName FROM tbl_BundlesNames";

	main::debug2log($sqlString);
	
	my $result = _getDataFromSQL($sqlString);
    	my $actionMessage;
    	if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
   	}            
    	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
    	push @{$respEntries}, @{$result} if($result);
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'GetBundles',
		IndexEntries => $respEntries
	}));
}

1;



