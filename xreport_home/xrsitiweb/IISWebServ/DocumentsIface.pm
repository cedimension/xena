package B64streamer;

use MIME::Base64;

use strict;
sub new {
  my $class = shift;
  my $self = { buffsize => 57*76, bufflen => 0, InputBytes => 0, OutputBytes => 0, buffer => ''};
  bless $self, $class;

  return $self;
}

sub binmode {

    return 1;
}

sub write {
  my ($self, $buffer) = (shift, shift);
  $self->{buffer} .= $buffer; 
  while ( length($self->{buffer}) > $self->{buffsize} ) {
    ($buffer, $self->{buffer}) = unpack("a".$self->{buffsize}." a*", $self->{buffer}) ;
    my $b64buff = MIME::Base64::encode_base64($buffer);
    $main::Response->Write($b64buff) ;
    $self->{InputBytes} += length($buffer);
    $self->{OutputBytes} += length($b64buff);
  }
}

sub tell {
 my $self = shift;
 return $self->{InputBytes} + length($self->{buffer});
}

sub close {
  my $self = shift;
  $self->write('');
  my $b64buff = MIME::Base64::encode_base64($self->{buffer});
  $main::Response->Write($b64buff) ;
  $self->{InputBytes} += length($self->{buffer});
  $self->{OutputBytes} += length($b64buff);
  main::debug2log("Inputbytes: ", $self->{InputBytes}, " Outputbytes: ", $self->{OutputBytes});
#  return $main::Response->Flush();
}

package XReport::DocumentsIface;

use strict;

use FileHandle;
use Symbol;
#use Net::FTP;
#use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

use XReport::Util;
use XReport::SOAP;
use XReport::JobREPORT;
use XReport::DBUtil qw(:none);
use XReport::QUtil;
use XReport::Spool;

require Data::Dumper;

sub _Dumper { return Data::Dumper::Dumper(@_); } 

sub _parseContent { 
  my ($method, @parms) = ( shift, @_ );
  main::debug2log("XReportWebIface parsesub $method  - selection: @parms");
  return XReport::SOAP::parseSOAPreq(main::readContent('-none-', $method)); 
}

require($main::Request->ServerVariables('APPL_PHYSICAL_PATH')->Item()."/auth_functions.pl");

#require($main::Request->ServerVariables('APPL_PHYSICAL_PATH')->Item()."/xr_util_functions.pl");

require($main::Request->ServerVariables('APPL_PHYSICAL_PATH')->Item()."/db_functions.pl");

### common ###

require($main::Request->ServerVariables('APPL_PHYSICAL_PATH')->Item()."/common_functions.pl");

### extractor pdf ###

sub _EXIT_SOAP_ERROR {
  my $respmsg = ('<?xml version="1.0" encoding="UTF-8"?>'
		 . '<SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema/instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">'
		 . '<SOAP-ENV:Body>'
		 . '<SOAP-ENV:Fault>'
		 . "<faultcode>$_[0]</faultcode>"
		 . "<faultactor>".$main::servername.'/'.$_[1]."</faultactor>"
		 . "<faultstring>$_[2]</faultstring>"
		 . '</SOAP-ENV:Fault>'
		 . '</SOAP-ENV:Body>'
		 . '</SOAP-ENV:Envelope>'
		);
  main::debug2log("EXIT_SOAP_ERROR: ", $respmsg);

  $main::Response->{Status} = "500 Xreport Web Service Server Error";
  $main::Response->AddHeader("Connection", "close");
  $main::Response->Write( $respmsg );
  $main::Response->Flush();
  return undef;
}

sub getReportEntryPdf {
      my ($method, $reqident, $selection) = (shift, shift, shift);

      my $respEntries = [];
      my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
      my $JobReportId = $IndexEntries->[0]->{'Columns'}->{'JobReportId'}; # ->{content};
      my $FromPage = $IndexEntries->[0]->{'Columns'}->{'FromPage'}; # ->{content};
       
      my $ListOfPages =$IndexEntries->[0]->{'Columns'}->{'ListOfPages'}; # ->{content}; 
      my $ReportID =$IndexEntries->[0]->{'Columns'}->{'ReportId'}; # ->{content}; 
      my $JobReportHandle = XReport::JobREPORT->Open($JobReportId);
	  my $output_archive = $JobReportHandle->get_OUTPUT_ARCHIVE();
	  my $JobReportName = $JobReportHandle->getValues('JobReportName');
	         
      require XReport::EXTRACT;
      require XReport::PDF::DOC;
      require MIME::Base64;

      my $DocumentData = XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $IndexEntries,
		DocumentType => 'application/pdf',
		FileName => "$JobReportName\.$JobReportId",
		Identity =>  $JobReportName.$JobReportId,
		DocumentBody => {content => "BASE64DOCUMENT"},
	 });

       my ($msghdr, $msgtail) = (($DocumentData) =~ /^(.*<\/IndexEntries>).*(<\/RESPONSE>.*)$/s);
       my $query = XReport::EXTRACT->new();
       my $streamer = new B64streamer();
       $query->{docTo} = XReport::PDF::DOC->Create($streamer) ;
       $main::Response->Write($msghdr . '<DocumentBody>'); # serializer

       my $elist;
       my $list = [split(/,/, $ListOfPages, 3)];
        main::debug2log($DocumentData."////////////".substr($msghdr,0,20)."--------------PAGES: ".Dumper($list));   
       eval {$elist = $query->ExtractPages(
				       QUERY_TYPE => 'FROMPAGES',
				       PAGE_LIST => $list ,
				       FORMAT => "PRINT",
				       ENCRYPT => 'YES',
				       OPTIMIZE => 1,
				       REQUEST_ID => "GetDocById.".$main::Session->{'SessionID'},
				       #				   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"}B64streamer
		
                     );
      };
      if ( scalar($elist) == 0 ) {
            _EXIT_SOAP_ERROR("Sender", "IdNotFound", "DOCUMENTI NON TROVATI IN ARCHIVIO per la chiave richiesta - $@");
            return 0;
         }
	   
    main::debug2log("returnDocumentPages elist: ", " InputDoc: ", $streamer->{InputBytes}, " B64Streamer: ", $streamer->{OutputBytes});

    $main::Response->Write('</DocumentBody>'.$msgtail);
    $main::Response->Flush();
}

1;
