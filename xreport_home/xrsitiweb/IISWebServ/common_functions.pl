use strict;

sub getCommonReports {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $whereClause = "";

	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	$whereClause = _addWhereClause($whereClause, "Status =", "18", "");
	foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
		my $field_name = $field;

		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;

		$field_name =~ s/^cmd_//;
		if($field_name =~ /^FromDate$/) {
			$whereClause = _addWhereClause($whereClause, "XferStartTime >=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
				if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
			next;
		}
		if($field_name =~ /^ToDate$/) {
			$whereClause = _addWhereClause($whereClause, "XferStartTime <=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
				if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
			next;
		}
		$whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
	}

	my $sql = "SELECT TOP 300 * FROM tbl_jobreports $whereClause ORDER BY JobReportId DESC";
	main::debug2log("SQL is $sql");
  	my $result = _getDataFromSQL($sql);
	if(!$result) {
		$actionMessage = "No reports found!";
	} else {
		$actionMessage = "SUCCESS";
	}
	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	push @{$respEntries}, @{$result} if($result);
#	main::debug2log(Dumper($respEntries));
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries
	}));
}

1;
