use strict;

sub GetNotes
{
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	if(exists $IndexEntries->[0]->{'Columns'}->{'FolderName'})
	{
		GetNotes_AUS($method, $reqident, $selection);
	}
	else
	{
		GetNotes_ITA($method, $reqident, $selection);
	}
}
sub DeleteNote
{
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	if(exists $IndexEntries->[0]->{'Columns'}->{'FolderName'})
	{
		DeleteNote_AUS($method, $reqident, $selection);
	}
	else
	{
		DeleteNote_ITA($method, $reqident, $selection);
	}
}
sub UpdateNote
{
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	if(exists $IndexEntries->[0]->{'Columns'}->{'FolderName'})
	{
		UpdateNote_AUS($method, $reqident, $selection);
	}
	else
	{
		UpdateNote_ITA($method, $reqident, $selection);
	}
}
sub InsertNote
{
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	if(exists $IndexEntries->[0]->{'Columns'}->{'FolderName'})
	{
		InsertNote_AUS($method, $reqident, $selection);
	}
	else
	{
		InsertNote_ITA($method, $reqident, $selection);
	}
}

sub GetNotes_AUS
{
	my ($method, $reqident, $selection) = (shift, shift, shift);

	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
	
	my $FolderName = $IndexEntries->[0]->{'Columns'}->{'FolderName'}->{'content'};
	my $Type = $IndexEntries->[0]->{'Columns'}->{'Type'}->{'content'};
	
	my $sql = 
		"SELECT JobReportId AS JobReport_ID, ReportId, NoteId, LastUpdateTime, AUTH_USER, textString FROM tbl_LogicalReportNotes_AUS ".
        "WHERE IsActive = 1 AND JobReportId = $JobReportID AND ReportId = $ReportID  AND ((FolderPath = '$FolderName') OR (FolderPath = '\$\$\$\$')) AND Type = '$Type' ".
        "ORDER BY JobReportId, ReportId, NoteId ASC";
	main::debug2log($sql);
	
	my $result = _getDataFromSQL_withBytesStream($sql);
	#my $result = _getDataFromSQL($sql);
    my $actionMessage;
    if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
    }            
    my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
    push @{$respEntries}, @{$result} if($result);
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'Notes',
		IndexEntries => $respEntries
	}));
}

sub InsertNote_AUS
{
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $Version = $IndexEntries->[0]->{'Columns'}->{'Version'}->{'content'};
    my $UpdateUser = $IndexEntries->[0]->{'Columns'}->{'UpdateUser'}->{'content'};
    my $Text = $IndexEntries->[0]->{'Columns'}->{'Text'}->{'content'};
	
	
	my $FolderName = $IndexEntries->[0]->{'Columns'}->{'FolderName'}->{'content'};
	my $Type = $IndexEntries->[0]->{'Columns'}->{'Type'}->{'content'};
    #my $sql = "SELECT COUNT(*) as TotalRows FROM tbl_LogicalReportNotes_AUS WHERE JobReportId = $JobReportID AND ReportId = $ReportID";
	#my $result;
	#my $totalRows = 0;
	#eval {
  		#$result = _getDataFromSQLNoFill($sql);
		#if($result and !$result->eof()) {
			#$totalRows = $result->Fields->Item("TotalRows")->{Value};
		#}
	#};
    #$totalRows++;
    my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION INSNOTE");
    eval{
        my $sql = "INSERT INTO tbl_LogicalReportNotes_AUS (JobReportId, ReportId, NoteId, LastUpdateTime, AUTH_USER, textString, IsActive, FolderPath, Type) ".
            #"SELECT '$JobReportID', '$ReportID', MAX(NoteId) + 1, GETDATE(), '$UpdateUser', '$Text', 1 ".
            "SELECT '$JobReportID', '$ReportID', COALESCE(MAX(NoteId), 0) + 1, GETDATE(), '$UpdateUser', '$Text', 1 , '$FolderName', '$Type'".
            "FROM tbl_LogicalReportNotes_AUS WHERE JobReportId = '$JobReportID' AND ReportId = '$ReportID' AND ((FolderPath = '$FolderName') OR (FolderPath = '\$\$\$\$')) AND Type = '$Type' ";
    
	    #$sql = "INSERT INTO tbl_LogicalReportNotes_AUS (JobReportId, ReportId, NoteId, LastUpdateTime, AUTH_USER, textString, IsActive) VALUES ('$JobReportID', '$ReportID', '$totalRows', GETDATE(), '$UpdateUser', '$Text', 1) ";
	    main::debug2log($sql);
	    my $res = _updateDataFromSQL($sql);
     };
	if($@)
	{
		$actionMessage = "FAILED: $@";
		_updateDataFromSQL("ROLLBACK TRANSACTION INSNOTE");
	}
	else
	{
		$actionMessage = "SUCCESS";
		_updateDataFromSQL("COMMIT TRANSACTION INSNOTE");
	}
    my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
  
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'Notes',
		IndexEntries =>  $respEntries
	}));
}

sub UpdateNote_AUS
{  
  	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $Version = $IndexEntries->[0]->{'Columns'}->{'Version'}->{'content'};
    my $Text = $IndexEntries->[0]->{'Columns'}->{'Text'}->{'content'};
    my $UpdateUser = $IndexEntries->[0]->{'Columns'}->{'UpdateUser'}->{'content'};
	my $IsRemark = $IndexEntries->[0]->{'Columns'}->{'IsRemark'}->{'content'};
	
	
	my $FolderName = $IndexEntries->[0]->{'Columns'}->{'FolderName'}->{'content'};
	my $Type = $IndexEntries->[0]->{'Columns'}->{'Type'}->{'content'};

	my $DocumentData = {IndexName => 'SocietaCliente' };
    my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION UPDNOTE");
	eval{
		if ($IsRemark eq "1")
		{
			my $select =   "SELECT JobReportId ,ReportId ,FolderPath ,Type ,NoteId ,IsActive ,AUTH_USER ,LastUpdateTime ,textString ".
                           "FROM tbl_LogicalReportNotes_AUS  WHERE JobReportId = $JobReportID AND ReportId = $ReportID AND NoteId = $Version ".
                           " AND ((((FolderPath = '$FolderName') OR (FolderPath = '\$\$\$\$'))) OR (FolderPath = '\$\$\$\$')) AND Type = '$Type' ";
			my $result = _getDataFromSQL($select);
			if($result) {
				my $sql = "UPDATE tbl_LogicalReportNotes_AUS SET FolderPath = '$FolderName', textString = '$Text', AUTH_USER = '$UpdateUser', LastUpdateTime = GETDATE(), IsActive = 1 ".
				"WHERE JobReportId = $JobReportID AND ReportId = $ReportID AND NoteId = $Version ".
                " AND ((FolderPath = '$FolderName') OR (FolderPath = '\$\$\$\$')) AND Type = '$Type' ";
				$result = _updateDataFromSQL($sql);	
			}
			else
			{
				my $sql = "INSERT INTO tbl_LogicalReportNotes_AUS (JobReportId, ReportId, FolderPath, Type, NoteId, IsActive, AUTH_USER, LastUpdateTime, textString) ".
				" VALUES ($JobReportID, $ReportID, '$FolderName', '$Type', 1, 1, '$UpdateUser', GETDATE(), '$Text' ) "; 
				$result = _updateDataFromSQL($sql);	
			}
			
			##my $sql_remark = "UPDATE tbl_LogicalReports SET Remark = '$Text' ".
			##	"WHERE JobReportId = $JobReportID AND ReportId = $ReportID";
			##my $result_remark = _updateDataFromSQL($sql_remark);	
		}
		else
		{
			my $select = "SELECT JobReportId, ReportId, NoteId, LastUpdateTime, AUTH_USER, textString FROM tbl_LogicalReportNotes_AUS ".
                     "WHERE JobReportId = $JobReportID AND ReportId = $ReportID AND NoteId = $Version AND ((FolderPath = '$FolderName') OR (FolderPath = '\$\$\$\$')) AND Type = '$Type' ";

			my $result = _getDataFromSQL($select);
			if($result) {
				my $sql = "UPDATE tbl_LogicalReportNotes_AUS SET FolderPath = '$FolderName', textString = '$Text', AUTH_USER = '$UpdateUser', LastUpdateTime = GETDATE() ".
				"WHERE JobReportId = $JobReportID AND ReportId = $ReportID AND NoteId = $Version AND ((FolderPath = '$FolderName') OR (FolderPath = '\$\$\$\$')) AND Type = '$Type' ";
				$result = _updateDataFromSQL($sql);	
			}
			else {
				die "Note doesn't exists($JobReportID, $ReportID, $Version)";
			}
		}
    };
	if($@)
	{
		$actionMessage = "FAILED: $@";
		_updateDataFromSQL("ROLLBACK TRANSACTION UPDNOTE");
	}
	else
	{
		$actionMessage = "SUCCESS";
		_updateDataFromSQL("COMMIT TRANSACTION UPDNOTE");
	}
    my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
  
  	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'Notes',
		IndexEntries => $respEntries
	}));
}

sub DeleteNote_AUS
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $Version = $IndexEntries->[0]->{'Columns'}->{'Version'}->{'content'};
    my $UpdateUser = $IndexEntries->[0]->{'Columns'}->{'UpdateUser'}->{'content'};
    my $VersionWhere = "";
    my $actionMessage = "";
	
	my $FolderName = $IndexEntries->[0]->{'Columns'}->{'FolderName'}->{'content'};
	my $Type = $IndexEntries->[0]->{'Columns'}->{'Type'}->{'content'};
	
    _updateDataFromSQL("BEGIN TRANSACTION DELNOTE");
	eval{
	    my $sql = "UPDATE tbl_LogicalReportNotes_AUS SET FolderPath = '$FolderName', IsActive = 0, AUTH_USER = '$UpdateUser', LastUpdateTime = GETDATE() ".
                  "WHERE JobReportId = $JobReportID AND ReportId = $ReportID  AND ((FolderPath = '$FolderName') OR (FolderPath = '\$\$\$\$')) AND Type = '$Type' ";
        if ($Version != "-1")
        {
            $sql.= "AND NoteId = $Version";
        }
	    main::debug2log($sql);
	    my $res = _updateDataFromSQL($sql);
    };
	if($@)
	{
		$actionMessage = "FAILED: $@";
		_updateDataFromSQL("ROLLBACK TRANSACTION DELNOTE");
	}
	else
	{
		$actionMessage = "SUCCESS";
		_updateDataFromSQL("COMMIT TRANSACTION DELNOTE");
	}
    my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'Notes',
		IndexEntries => $respEntries
	}));
}

sub GetNotes_ITA
{
	my ($method, $reqident, $selection) = (shift, shift, shift);

	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
	
	my $sql = 
		"SELECT JobReportId AS JobReport_ID, ReportId, NoteId, LastUpdateTime, AUTH_USER, textString FROM tbl_LogicalReportNotes ".
        "WHERE IsActive = 1 AND JobReportId = $JobReportID AND ReportId = $ReportID ".
        "ORDER BY JobReportId, ReportId, NoteId ASC";
	main::debug2log($sql);
	
	#my $result = _getDataFromSQL_withBytesStream($sql);
	my $result = _getDataFromSQL($sql);
    my $actionMessage;
    if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
    }            
    my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
    push @{$respEntries}, @{$result} if($result);
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'Notes',
		IndexEntries => $respEntries
	}));
}

sub InsertNote_ITA
{
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $Version = $IndexEntries->[0]->{'Columns'}->{'Version'}->{'content'};
    my $UpdateUser = $IndexEntries->[0]->{'Columns'}->{'UpdateUser'}->{'content'};
    my $Text = $IndexEntries->[0]->{'Columns'}->{'Text'}->{'content'};

    #my $sql = "SELECT COUNT(*) as TotalRows FROM tbl_LogicalReportNotes WHERE JobReportId = $JobReportID AND ReportId = $ReportID";
	#my $result;
	#my $totalRows = 0;
	#eval {
  		#$result = _getDataFromSQLNoFill($sql);
		#if($result and !$result->eof()) {
			#$totalRows = $result->Fields->Item("TotalRows")->{Value};
		#}
	#};
    #$totalRows++;
    my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION INSNOTE");
    eval{
        my $sql = "INSERT INTO tbl_LogicalReportNotes (JobReportId, ReportId, NoteId, LastUpdateTime, AUTH_USER, textString, IsActive) ".
            #"SELECT '$JobReportID', '$ReportID', MAX(NoteId) + 1, GETDATE(), '$UpdateUser', '$Text', 1 ".
            "SELECT '$JobReportID', '$ReportID', COALESCE(MAX(NoteId), 0) + 1, GETDATE(), '$UpdateUser', '$Text', 1 ".
            "FROM tbl_LogicalReportNotes WHERE JobReportId = '$JobReportID' AND ReportId = '$ReportID' ";
    
	    #$sql = "INSERT INTO tbl_LogicalReportNotes (JobReportId, ReportId, NoteId, LastUpdateTime, AUTH_USER, textString, IsActive) VALUES ('$JobReportID', '$ReportID', '$totalRows', GETDATE(), '$UpdateUser', '$Text', 1) ";
	    main::debug2log($sql);
	    my $res = _updateDataFromSQL($sql);
     };
	if($@)
	{
		$actionMessage = "FAILED: $@";
		_updateDataFromSQL("ROLLBACK TRANSACTION INSNOTE");
	}
	else
	{
		$actionMessage = "SUCCESS";
		_updateDataFromSQL("COMMIT TRANSACTION INSNOTE");
	}
    my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
  
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'Notes',
		IndexEntries =>  $respEntries
	}));
}

sub UpdateNote_ITA
{  
  	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $Version = $IndexEntries->[0]->{'Columns'}->{'Version'}->{'content'};
    my $Text = $IndexEntries->[0]->{'Columns'}->{'Text'}->{'content'};
    my $UpdateUser = $IndexEntries->[0]->{'Columns'}->{'UpdateUser'}->{'content'};
	my $IsRemark = $IndexEntries->[0]->{'Columns'}->{'IsRemark'}->{'content'};

	my $DocumentData = {IndexName => 'SocietaCliente' };
    my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION UPDNOTE");
	eval{
		if ($IsRemark eq "1")
		{
			my $sql_remark = "UPDATE tbl_LogicalReports SET Remark = '$Text' ".
				"WHERE JobReportId = $JobReportID AND ReportId = $ReportID";
			my $result_remark = _updateDataFromSQL($sql_remark);	
		}
		else
		{
        my $select = "SELECT JobReportId, ReportId, NoteId, LastUpdateTime, AUTH_USER, textString FROM tbl_LogicalReportNotes ".
                     "WHERE JobReportId = $JobReportID AND ReportId = $ReportID AND NoteId = $Version";

   	    my $result = _getDataFromSQL($select);
	    if($result) {
            my $sql = "UPDATE tbl_LogicalReportNotes SET textString = '$Text', AUTH_USER = '$UpdateUser', LastUpdateTime = GETDATE() ".
            "WHERE JobReportId = $JobReportID AND ReportId = $ReportID AND NoteId = $Version";
            $result = _updateDataFromSQL($sql);	
        }
        else {
	        die "Note doesn't exists($JobReportID, $ReportID, $Version)";
	    }
		}
    };
	if($@)
	{
		$actionMessage = "FAILED: $@";
		_updateDataFromSQL("ROLLBACK TRANSACTION UPDNOTE");
	}
	else
	{
		$actionMessage = "SUCCESS";
		_updateDataFromSQL("COMMIT TRANSACTION UPDNOTE");
	}
    my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
  
  	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'Notes',
		IndexEntries => $respEntries
	}));
}

sub DeleteNote_ITA
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $Version = $IndexEntries->[0]->{'Columns'}->{'Version'}->{'content'};
    my $UpdateUser = $IndexEntries->[0]->{'Columns'}->{'UpdateUser'}->{'content'};
    my $VersionWhere = "";
    my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION DELNOTE");
	eval{
	    my $sql = "UPDATE tbl_LogicalReportNotes SET IsActive = 0, AUTH_USER = '$UpdateUser', LastUpdateTime = GETDATE() ".
                  "WHERE JobReportId = $JobReportID AND ReportId = $ReportID ";
        if ($Version != "-1")
        {
            $sql.= "AND NoteId = $Version";
        }
	    main::debug2log($sql);
	    my $res = _updateDataFromSQL($sql);
    };
	if($@)
	{
		$actionMessage = "FAILED: $@";
		_updateDataFromSQL("ROLLBACK TRANSACTION DELNOTE");
	}
	else
	{
		$actionMessage = "SUCCESS";
		_updateDataFromSQL("COMMIT TRANSACTION DELNOTE");
	}
    my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'Notes',
		IndexEntries => $respEntries
	}));
}


sub CheckReport
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
  	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $CheckUser = $IndexEntries->[0]->{'Columns'}->{'CheckUser'}->{'content'};
     my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION CHKREP");
	eval{
        my $sql = "UPDATE tbl_LogicalReports SET CheckStatus = 1, LastUsedToCheck = '$CheckUser' ".
                  "WHERE JobReportID = $JobReportID AND ReportID = $ReportID ";
    
        my $res = _updateDataFromSQL($sql);
    };
	if($@)
	{
		$actionMessage = "FAILED: $@";
		_updateDataFromSQL("ROLLBACK TRANSACTION CHKREP");
	}
	else
	{
		$actionMessage = "SUCCESS";
		_updateDataFromSQL("COMMIT TRANSACTION CHKREP");
	}
    my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'Notes',
		IndexEntries => $respEntries
	}));
}

sub UpdateLastUserToView
{
    my ($method, $reqident, $selection) = (shift, shift, shift);
  	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportID = $IndexEntries->[0]->{'Columns'}->{'JobReportID'}->{'content'};
    my $ReportID = $IndexEntries->[0]->{'Columns'}->{'ReportID'}->{'content'};
    my $CheckUser = $IndexEntries->[0]->{'Columns'}->{'CheckUser'}->{'content'};
     my $actionMessage = "";
    _updateDataFromSQL("BEGIN TRANSACTION UPDUSER");
	eval{
        my $sql = "UPDATE tbl_LogicalReports SET LastUsedToView = '$CheckUser' ".
                  "WHERE JobReportID = $JobReportID AND ReportID = $ReportID ";
        main::debug2log($sql);
        my $res = _updateDataFromSQL($sql)
      };
	if($@)
	{
		$actionMessage = "FAILED: $@";
		_updateDataFromSQL("ROLLBACK TRANSACTION UPDUSER");
	}
	else
	{
		$actionMessage = "SUCCESS";
		_updateDataFromSQL("COMMIT TRANSACTION UPDUSER");
	}
    my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'Notes',
		IndexEntries => $respEntries
	}));
}

1;
