<html>
<body>
<table border="1">
   <tr>
      <td><b>Server Variable</b></td>
      <td><b>Value</b></td>
   </tr>
   <% For Each strKey In Request.ServerVariables %>
   <tr>
      <td><%= strKey %></td>
      <td><%= Request.ServerVariables(strKey) %></td>
   </tr>
   <% Next %>
</table>
</body>
</html>
