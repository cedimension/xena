use strict;
#use Encode qw(decode encode);

my $excelConfigLineCommands = {
    'R' => '_requeueJobreports',
    'P' => '_deleteJobreports',
    'L' => '_getJobReportLog',
    'VD' => '_downloadInput',
    'O' => '_downloadOutput',
};

sub getRSetFromGroup{
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $whereClause = " where ReportGroupId in ";
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my @ReportGroups = split(/[,;]/, $IndexEntries->[0]->{'Columns'}->{'ReportGroupId'}->{'content'});
        $whereClause .= join ("", (" ('", join ("', '" , @ReportGroups), "') "));
	
	delete($IndexEntries->[0]->{'Columns'}->{ReportGroupId});	
	foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
	        my $field_name = $field;

		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

		if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
		    delete($IndexEntries->[0]->{'Columns'}->{$field_name});
		    next;
        }
        $field_name =~ s/^cmd_//;
        $whereClause = _addWhereClause($whereClause, "$field_name = ", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
	 
    }
    my $sql = "SELECT ReportRule as SetName, VarValue as ReportName from tbl_NamedReportsGroups NRG join tbl_varSetsValues VSV on NRG.ReportRule = VSV.VarSetName and VarName = ':REPORTNAME'".
    		   $whereClause.
		   " ORDER By 1";
    main::debug2log($sql);
    my $result = undef;
    eval{ 
	
    	$result = _getDataFromSQL($sql);
	if($result){
        	$actionMessage = "SUCCESS";
    	}else{
		$actionMessage = "NO REPORSET FOUND";
	}
    };
    if($@){
    	$actionMessage = "FAILED: $@";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));	
}

sub getUsersFromGroup{
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $whereClause = "";
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
        foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
	        my $field_name = $field;

		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

		if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
		    delete($IndexEntries->[0]->{'Columns'}->{$field_name});
		    next;
        }
        $field_name =~ s/^cmd_//;
        $whereClause = _addWhereClause($whereClause, "$field_name = ", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
	 
    }
    my $sql = "SELECT A.USERALIAS , B.UserDescr as NameDescr from tbl_UserAliases  A join tbl_Users B on A.UserName = B.UserName  join tbl_UsersProfiles C on B.UserName = C.UserName ".
		   " join tbl_ProfilesFoldersTrees D on D.ProfileName = C.ProfileName ".
		   " join tbl_FoldersRules E on E.FolderName like D.RootNode+'%' ".
		   $whereClause.
		   " ORDER By 1";
    main::debug2log($sql);
    my $result = undef;
    eval{ 
	
    	$result = _getDataFromSQL($sql);
	if($result){
        	$actionMessage = "SUCCESS";
    	}else{
		$actionMessage = "NO USER FOUND";
	}
    };
    if($@){
    	$actionMessage = "FAILED: $@";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));	
}

sub getReports{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
		main::debug2log($field_name."OLE--".encode('iso-8859-1', $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}));
        main::debug2log($field_name."PRIMA--".unpack('H*', $whereClause));
		#$whereClause = encode('iso-8859-1', _addWhereClause($whereClause, "$field_name like", encode('iso-8859-1', $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}), "'"))
		$whereClause =  _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
		main::debug2log($field_name."DOPO--".unpack('H*', $whereClause));	
    }

#   my $sql = 
#       "SELECT TOP 100 vv.VarSetName            as ReportGroup,"
#       ."jr.JobReportName         as ReportName,"
#       ."jr.JobReportDescr        as ReportDescr,"
#       ."jr.TypeOfWork            as TypeOfWork,"
#       ."jr.HoldDays              as RetDays,"
#       ."jr.MailTo                as MailTo,"
#       ."jr.ParseFileName         as ParseFileName,"
#       ."jr.TargetLocalPathId_IN  as ArchivioInput,"
#       ."jr.TargetLocalPathId_OUT as ArchivioOutput,"
#       ."jr.CodePage              as CodePage,"
#       ."jr.ReportFormat          as ReportFormat,"
#       ."jr.ElabFormat            as ElabFormat,"
#       ."jr.WorkClass             as WorkClass,"
#       ."jr.HasCc             as HasCc,"
#       ."jr.CharsPerLine             as CharsPerLine,"
#       ."jr.LinesPerPage             as LinesPerPage,"
#       ."jr.PageOrient             as PageOrient,"
#       ."jr.FontSize             as FontSize,"
#       ."rn.FilterVar             as FilterVar,"
#       ."op.t1Fonts               as t1Fonts,"
#       ."op.LaserAdjust           as LaserAdjust,"
#       ."op.FormDef               as FormDef "
#       ."FROM         tbl_NamedReportsGroups AS rg "
#       ."INNER JOIN      tbl_VarSetsValues AS vv ON rg.ReportRule = vv.VarSetName AND vv.VarName = ':REPORTNAME' "
#       ."INNER JOIN      tbl_JobReportNames AS jr ON jr.JobReportName = vv.VarValue "
#       ."LEFT OUTER JOIN tbl_Os390PrintParameters AS op ON op.JobReportName = jr.JobReportName "
#       ."LEFT OUTER JOIN tbl_ReportNames AS rn ON rn.ReportName = jr.JobReportName "
#       ." $whereClause "
#       ."ORDER BY ReportGroup"
#   ;
    my $sql = 
        "SELECT TOP 1000 * FROM (SELECT DISTINCT vv1.VarSetName            as ReportGroup,"
        ."jr1.JobReportName         as ReportName,"
        ."jr1.JobReportDescr        as ReportDescr,"
        ."jr1.TypeOfWork            as TypeOfWork,"
        ."jr1.HoldDays              as RetDays,"
        ."jr1.MailTo                as MailTo,"
        ."jr1.ParseFileName         as ParseFileName,"
        ."jr1.TargetLocalPathId_IN  as ArchivioInput,"
        ."jr1.TargetLocalPathId_OUT as ArchivioOutput,"
        ."jr1.CodePage              as CodePage,"
        ."jr1.ReportFormat          as ReportFormat,"
        ."jr1.ElabFormat            as ElabFormat,"
        ."jr1.WorkClass             as WorkClass,"
        ."jr1.HasCc             as HasCc,"
        ."jr1.CharsPerLine             as CharsPerLine,"
        ."jr1.LinesPerPage             as LinesPerPage,"
        ."jr1.PageOrient             as PageOrient,"
        ."jr1.FontSize             as FontSize,"
        ."jr1.FitToPage             as FitToPage,"
        ."jr1.Priority             as Priority,"
        ."rn.FilterVar             as FilterVar,"
        ."op.t1Fonts               as t1Fonts,"
        ."op.LaserAdjust           as LaserAdjust,"
        ."op.PageDef as PageDef, "
        ."op.CharsDef as CharsDef, "
        ."op.FormDef  as FormDef, "
        ."op.ReplaceDef as ReplaceDef,"
        ."op.PrintControlFile as PrintControlFile, "
        ."op.PageSize as PageSize "
		."FROM tbl_jobReportNames as jr LEFT OUTER JOIN      tbl_VarSetsValues AS vv ON vv.VarName = ':REPORTNAME' AND vv.VarValue = jr.JobReportName  "
		."JOIN tbl_JobReportNames AS jr1 on jr.JobReportName = jr1.JobReportName "
		."LEFT OUTER JOIN tbl_Os390PrintParameters AS op ON op.JobReportName = jr1.JobReportName  "
		."LEFT OUTER JOIN tbl_ReportNames AS rn ON rn.ReportName = jr1.JobReportName  "
		."LEFT OUTER JOIN  tbl_VarSetsValues AS vv1 ON vv.VarName = ':REPORTNAME' AND vv1.VarValue = jr.JobReportName  "
		."LEFT OUTER JOIN  tbl_NamedReportsGroups AS rg ON rg.ReportRule = vv1.VarSetName  "
        #."FROM         tbl_JobReportNames AS jr "
        #."LEFT OUTER JOIN tbl_Os390PrintParameters AS op ON op.JobReportName = jr.JobReportName "
        #."LEFT OUTER JOIN tbl_ReportNames AS rn ON rn.ReportName = jr.JobReportName "
        #."LEFT OUTER JOIN      tbl_VarSetsValues AS vv ON vv.VarName = ':REPORTNAME' AND vv.VarValue = jr.JobReportName "
        #."LEFT OUTER JOIN      tbl_NamedReportsGroups AS rg ON rg.ReportRule = vv.VarSetName "

        ." $whereClause "
        .") as gr ORDER BY ReportName"
    ;

    main::debug2log($sql);
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub manageReports{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(!exists($IndexEntries->[$entryPos]->{'Columns'}->{'_action'})) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }

    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    #my $ReportGroup = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ReportGroup'}->{'content'});
    my @ReportGroups = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'ReportGroup'}->{'content'});
    my $ReportName = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ReportName'}->{'content'});
    my $ReportDescr = uc(dbEscape($IndexEntries->[$entryPos]->{'Columns'}->{'ReportDescr'}->{'content'}));
    my $TypeOfWork = uc($IndexEntries->[$entryPos]->{'Columns'}->{'TypeOfWork'}->{'content'}) || 1;
    my $RetDays = uc($IndexEntries->[$entryPos]->{'Columns'}->{'RetDays'}->{'content'}) || 180;
    my $MailTo = uc($IndexEntries->[$entryPos]->{'Columns'}->{'MailTo'}->{'content'});
    my $ParseFileName = $IndexEntries->[$entryPos]->{'Columns'}->{'ParseFileName'}->{'content'} || '*,NULL.XML';
    my $HasCc = $IndexEntries->[$entryPos]->{'Columns'}->{'HasCc'}->{'content'};
    my $CharsPerLine = uc($IndexEntries->[$entryPos]->{'Columns'}->{'CharsPerLine'}->{'content'}) || 133;
    my $LinesPerPage = uc($IndexEntries->[$entryPos]->{'Columns'}->{'LinesPerPage'}->{'content'}) || 66;
    my $PageOrient = uc($IndexEntries->[$entryPos]->{'Columns'}->{'PageOrient'}->{'content'}) || 'L';
    my $PageSize = uc($IndexEntries->[$entryPos]->{'Columns'}->{'PageSize'}->{'content'}) || '';
    my $FontSize = uc($IndexEntries->[$entryPos]->{'Columns'}->{'FontSize'}->{'content'}) || '[8.0 12.0]';
    my $FitToPage = $IndexEntries->[$entryPos]->{'Columns'}->{'FitToPage'}->{'content'} || 1;
    my $Priority = $IndexEntries->[$entryPos]->{'Columns'}->{'Priority'}->{'content'} || 1;
    my $ArchivioInput = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ArchivioInput'}->{'content'});
    my $ArchivioOutput = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ArchivioOutput'}->{'content'});
    my $CodePage = uc($IndexEntries->[$entryPos]->{'Columns'}->{'CodePage'}->{'content'});
    my $ReportFormat = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ReportFormat'}->{'content'}) || 2;
    my $ElabFormat = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ElabFormat'}->{'content'}) || 1;
    my $WorkClass = uc($IndexEntries->[$entryPos]->{'Columns'}->{'WorkClass'}->{'content'}) || 'NULL';
    my $FilterVar = uc($IndexEntries->[$entryPos]->{'Columns'}->{'FilterVar'}->{'content'});
    my $t1Fonts = uc($IndexEntries->[$entryPos]->{'Columns'}->{'t1Fonts'}->{'content'});
    my $LaserAdjust = uc($IndexEntries->[$entryPos]->{'Columns'}->{'LaserAdjust'}->{'content'});
    my $FormDef = uc($IndexEntries->[$entryPos]->{'Columns'}->{'FormDef'}->{'content'});
    my $PageDef = uc($IndexEntries->[$entryPos]->{'Columns'}->{'PageDef'}->{'content'});
    my $CharsDef = uc($IndexEntries->[$entryPos]->{'Columns'}->{'CharsDef'}->{'content'});
    my $ReplaceDef = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ReplaceDef'}->{'content'});
    my $PrintControlFile = uc($IndexEntries->[$entryPos]->{'Columns'}->{'PrintControlFile'}->{'content'});

    main::debug2log("action is $action");
    $main::msgtolog .= "$action report $ReportName";
    if($action =~ /add/i) {
        _updateDataFromSQL("BEGIN TRANSACTION REPORTADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_JobReportNames WHERE JobReportName = '$ReportName'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_JobReportNames"
                    ." (JobReportName"
                    ." ,JobReportDescr"
                    ." ,TypeOfWork"
                    ." ,HoldDays"
                    ." ,MailTo"
                    ." ,ParseFileName"
                    ." ,HasCc"
                    ." ,CharsPerLine"
                    ." ,LinesPerPage"
                    ." ,PageOrient"
                    ." ,FontSize"
                    ." ,FitToPage"
                    ." ,TargetLocalPathId_IN"
                    ." ,TargetLocalPathId_OUT"
                    ." ,CodePage"
                    ." ,ReportFormat"
                    ." ,ElabFormat"
                    ." ,IsActive"
                    ." ,PrintFlag"
                    ." ,ViewOnlineFlag"
                    ." ,ActiveDays"
                    ." ,ActiveGens"
                    ." ,HoldGens"
                    ." ,StorageClass"
                    ." ,WorkClass"
                    ." ,Priority)"
                    ." VALUES"
                    ." ('$ReportName'"
                    ." ,'$ReportDescr'"
                    ." ,$TypeOfWork"
                    ." ,$RetDays"
                    ." ,'$MailTo'"
                    ." ,'$ParseFileName'"
                    ." ,$HasCc"
                    ." ,$CharsPerLine"
                    ." ,$LinesPerPage"
                    ." ,'$PageOrient'"
                    ." ,'$FontSize'"
                    ." ,$FitToPage"
                    ." ,'$ArchivioInput'"
                    ." ,'$ArchivioOutput'"
                    ." ,'$CodePage'"
                    ." ,$ReportFormat"
                    ." ,$ElabFormat"
                    ." ,1"
                    ." ,'Y'"
                    ." ,'Y'"
                    ." ,5"
                    ." ,5"
                    ." ,30"
                    ." ,''"
                    ." ,$WorkClass"
                    ." ,$Priority)"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Report $ReportName already exists(JobReportNames)";
            }

            $sql = "SELECT * FROM tbl_ReportNames WHERE ReportName = '$ReportName'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_ReportNames"
                    ." (ReportName"
                    ." ,ReportDescr"
                    ." ,HoldDays"
                    ." ,MailTo"
                    ." ,FilterVar)"
                    ." VALUES"
                    ." ('$ReportName'"
                    ." ,'$ReportDescr'"
                    ." ,$RetDays"
                    ." ,'$MailTo'"
                    ." ,'$FilterVar')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Report $ReportName already exists(ReportNames)";
            }

            $sql = "SELECT * FROM tbl_Os390PrintParameters WHERE JobReportName = '$ReportName'";
            my $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_Os390PrintParameters"
                    ." (JobReportName"
                    ." ,t1Fonts"
                    ." ,LaserAdjust"
                    ." ,FormDef "
                    ." ,PageDef "
                    ." ,CharsDef "
                    ." ,ReplaceDef "
                    ." ,PageSize "
                    ." ,PrintControlFile)"
                    ." VALUES"
                    ." ('$ReportName'"
                    ." ,$t1Fonts"
                    ." ,'$LaserAdjust'"
                    ." ,'$FormDef'"
                    ." ,'$PageDef'"
                    ." ,'$CharsDef'"
                    ." ,'$ReplaceDef'"
                    ." ,'$PageSize'"
                    ." ,'$PrintControlFile')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Report $ReportName already exists(OS390PrintParameters)";
            }

            foreach my $ReportGroup(@ReportGroups) {
                $sql = "SELECT * FROM tbl_NamedReportsGroups WHERE ReportGroupId = '$ReportGroup'";
                $result = _getDataFromSQL($sql);
                if(!$result) {
                    $sql = 
                        "INSERT INTO tbl_NamedReportsGroups"
                        ." (ReportGroupId"
                        ." ,FilterVar"
                        ." ,FilterRule"
                        ." ,RecipientRule"
                        ." ,ReportRule)"
                        ." VALUES"
                        ." ('$ReportGroup'"
                        ." ,''"
                        ." ,''"
                        ." ,''"
                        ." ,'$ReportGroup')"
                    ;
                    main::debug2log($sql);
                    $result = _updateDataFromSQL($sql); 
                }

                $sql = 
                    "INSERT INTO tbl_VarSetsValues"
                    ." (VarSetName"
                    ." ,VarName"
                    ." ,VarValue)"
                    ." VALUES"
                    ." ('$ReportGroup'"
                    ." ,':REPORTNAME'"
                    ." ,'$ReportName')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION REPORTADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION REPORTADD");
        }
    } elsif($action =~ /update/i) {
        _updateDataFromSQL("BEGIN TRANSACTION REPORTUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_JobReportNames WHERE JobReportName = '$ReportName'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "UPDATE tbl_JobReportNames SET"
                    ." JobReportDescr = '$ReportDescr'"
                    ." ,TypeOfWork = $TypeOfWork"
                    ." ,HoldDays = $RetDays"
                    ." ,MailTo = '$MailTo'"
                    ." ,ParseFileName = '$ParseFileName'"
                    ." ,HasCc = $HasCc"
                    ." ,FitToPage = $FitToPage"
                    ." ,CharsPerLine = $CharsPerLine"
                    ." ,LinesPerPage = $LinesPerPage"
                    ." ,PageOrient = '$PageOrient'"
                    ." ,FontSize = '$FontSize'"
                    ." ,TargetLocalPathId_IN = '$ArchivioInput'"
                    ." ,TargetLocalPathId_OUT = '$ArchivioOutput'"
                    ." ,CodePage = '$CodePage'"
                    ." ,ReportFormat = $ReportFormat"
                    ." ,ElabFormat = $ElabFormat"
                    ." ,WorkClass = $WorkClass"
                    ." ,Priority = $Priority"
                    ." WHERE JobReportName = '$ReportName'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die " Report $ReportName doesn't exists (JobReportNames) use ADD Panel if usefull\n";
            }

            $sql = "SELECT * FROM tbl_ReportNames WHERE ReportName = '$ReportName'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "UPDATE tbl_ReportNames SET"
                    ." ReportDescr = '$ReportDescr'"
                    ." ,HoldDays = $RetDays"
                    ." ,MailTo = '$MailTo'"
                    ." ,FilterVar = '$FilterVar'"
                    ." WHERE ReportName = '$ReportName'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Report $ReportName doesn't exists (ReportNames)  use ADD Panel if usefull\n";
            }


            $sql = "SELECT * FROM tbl_Os390PrintParameters WHERE JobReportName = '$ReportName'";
            my $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "UPDATE tbl_Os390PrintParameters SET"
                    ."  t1Fonts = '$t1Fonts'"
                    ." ,LaserAdjust = '$LaserAdjust'"
                    ." ,FormDef = '$FormDef'"
                    ." ,PageDef = '$PageDef'"
                    ." ,CharsDef = '$CharsDef'"
                    ." ,ReplaceDef = '$ReplaceDef'"
                    ." ,PageSize = '$PageSize'"
                    ." ,PrintControlFile = '$PrintControlFile'"
                    ." WHERE JobReportName = '$ReportName'"
                    
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                $sql = 
                    "INSERT INTO tbl_Os390PrintParameters"
                    ." (JobReportName"
                    ." ,t1Fonts"
                    ." ,LaserAdjust"
                    ." ,FormDef "
                    ." ,PageDef "
                    ." ,CharsDef "
                    ." ,ReplaceDef "
                    ." ,PageSize "
                    ." ,PrintControlFile)"
                    ." VALUES"
                    ." ('$ReportName'"
                    ." ,$t1Fonts"
                    ." ,'$LaserAdjust'"
                    ." ,'$FormDef'"
                    ." ,'$PageDef'"
                    ." ,'$CharsDef'"
                    ." ,'$ReplaceDef'"
                    ." ,'$PageSize'"
                    ." ,'$PrintControlFile')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql);
            }

            $sql = "DELETE FROM tbl_VarSetsValues WHERE VarValue = '$ReportName'";
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 

            foreach my $ReportGroup(@ReportGroups) {
                $sql = "SELECT * FROM tbl_NamedReportsGroups WHERE ReportGroupId = '$ReportGroup'";
                $result = _getDataFromSQL($sql);
                if(!$result) {
                    $sql = 
                        "INSERT INTO tbl_NamedReportsGroups"
                        ." (ReportGroupId"
                        ." ,FilterVar"
                        ." ,FilterRule"
                        ." ,RecipientRule"
                        ." ,ReportRule)"
                        ." VALUES"
                        ." ('$ReportGroup'"
                        ." ,''"
                        ." ,''"
                        ." ,''"
                        ." ,'$ReportGroup')"
                    ;
                    main::debug2log($sql);
                    $result = _updateDataFromSQL($sql); 
                }

                $sql = 
                    "INSERT INTO tbl_VarSetsValues"
                    ." (VarSetName"
                    ." ,VarName"
                    ." ,VarValue)"
                    ." VALUES"
                    ." ('$ReportGroup'"
                    ." ,':REPORTNAME'"
                    ." ,'$ReportName')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION REPORTUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION REPORTUPDATE");
        }
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION REPORTDELETE");
        eval {
            my $sql = "";
            my $result = undef;
    
            $sql = 
                "DELETE tbl_JobReportNames WHERE"
                ." JobReportName = '$ReportName'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 
        
            $sql = 
                "DELETE tbl_ReportNames WHERE"
                ." ReportName = '$ReportName'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 

            $sql = 
                "DELETE tbl_Os390PrintParameters WHERE"
                ." JobReportName = '$ReportName'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 

            $sql = 
                "DELETE tbl_VarSetsValues WHERE"
                ." VarValue = '$ReportName'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION REPORTDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION REPORTDELETE");
        }
    }
    $main::msgtolog .= " $actionMessage";

    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        #$selection = undef;
        #push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        #main::debug2log("selection is ".Dumper($selection));
        getReports($method, $reqident, $selection);
    }
}

sub getFolderReports {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "WHERE ff.FolderName not like 'ROOT' ";

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    my $sql = "SELECT TOP 1000 ff.FolderName as FolderPath, "
        ."ff.FolderDescr, ff.FolderAddr, ff.SpecialInstr, "
        ."fr.ReportGroupId as ReportGroup "
        ."FROM tbl_FoldersRules AS fr "
	#."LEFT JOIN   tbl_NamedReportsGroups AS rg ON rg.ReportGroupId = fr.ReportGroupId "
        ."RIGHT OUTER JOIN tbl_Folders AS ff      ON fr.FolderName =    ff.FolderName "
        #."WHERE ff.FolderName not like 'ROOT' "
        
        #"SELECT TOP 100 ff.FolderName as FolderPath,"
        #."ff.FolderDescr,"
        #."fr.ReportGroupId as ReportGroups "
        #."FROM             tbl_NamedReportsGroups AS rg "
        #."INNER JOIN       tbl_FoldersRules       AS fr ON rg.ReportGroupId = fr.ReportGroupId "
        #."RIGHT OUTER JOIN tbl_Folders            AS ff ON fr.FolderName    = ff.FolderName "
        ." $whereClause "
        ."ORDER BY FolderPath, ReportGRoup"
    ;
    main::debug2log("Sql--$sql\n");
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub manageFolderReports {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }

    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    my $FolderPath = uc($IndexEntries->[$entryPos]->{'Columns'}->{'FolderPath'}->{'content'});
    my $FolderDescr = dbEscape($IndexEntries->[$entryPos]->{'Columns'}->{'FolderDescr'}->{'content'});
    my @ReportGroups = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'ReportGroups'}->{'content'});

    my $parent = "ROOT";
    if($FolderPath =~ /^(.*)\\[^\\]+$/) {
        $parent = $1;
    }

    $main::msgtolog .= "$action folder $FolderPath";
    if($action =~ /add/i) {
        _updateDataFromSQL("BEGIN TRANSACTION FOLDERADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_Folders WHERE FolderName = '$FolderPath' AND ParentFolder = '$parent'";
            main::debug2log($sql."\n");
            $result = _getDataFromSQL($sql);
            if(!$result) {
		$sql = "SELECT * FROM tbl_Folders WHERE FolderName = '$parent' ";
		main::debug2log($sql);
                $result = _getDataFromSQL($sql);
                if($result){
                $sql = 
                    "INSERT INTO tbl_Folders"
                    ." (FolderName"
                    ." ,FolderDescr"
                    ." ,IsActive"
                    ." ,ParentFolder)"
                    ." VALUES"
                    ." ('$FolderPath'"
                    ." ,'$FolderDescr'"
                    ." ,1"
                    ." ,'$parent')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
			die "Parent Folder $parent doesn't exists !!!  Add Folder not possible";
		}
            } else {
                die "FolderPath $FolderPath already exists";
            }

            foreach my $group(@ReportGroups) {
		    $sql = ""
		    ."IF NOT EXISTS (SELECT * FROM tbl_FoldersRules where FolderName = '$FolderPath' and ReportGroupId = '$group') "
                    ."INSERT INTO tbl_FoldersRules"
                    ." (FolderName"
                    ." ,FORBID"
                    ." ,ReportGroupId)"
                    ." VALUES"
                    ." ('$FolderPath'"
                    ." ,'0'"
                    ." ,'$group')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION FOLDERADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION FOLDERADD");
        }
    } elsif($action =~ /update/i) {
        _updateDataFromSQL("BEGIN TRANSACTION FOLDERUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_Folders WHERE FolderName = '$FolderPath' AND ParentFolder = '$parent'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "UPDATE tbl_Folders SET"
                    ." FolderDescr = '$FolderDescr'"
                    ." WHERE FolderName = '$FolderPath' AND ParentFolder = '$parent'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "FolderPath $FolderPath or ParentFolder $parent doesn't exists";
            }

            $sql =
                "DELETE tbl_FoldersRules WHERE"
                ." FolderName = '$FolderPath'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql);

            foreach my $group(@ReportGroups) {
                $sql = ""
		    ."IF NOT EXISTS (SELECT * FROM tbl_FoldersRules where FolderName = '$FolderPath' and ReportGroupId = '$group') "
                    ."INSERT INTO tbl_FoldersRules"
                    ." (FolderName"
                    ." ,FORBID"
                    ." ,ReportGroupId)"
                    ." VALUES"
                    ." ('$FolderPath'"
                    ." ,'0'"
                    ." ,'$group')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION FOLDERUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION FOLDERUPDATE");
        }
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION FOLDERDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            foreach my $group(@ReportGroups) {
                $sql =
                    "DELETE tbl_FoldersRules WHERE"
                    ." FolderName = '$FolderPath'"
                    ." AND ReportGroupId = '$group'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }

            $sql = "SELECT * FROM tbl_Folders WHERE ParentFolder = '$FolderPath'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "DELETE tbl_Folders WHERE"
                    ." FolderName = '$FolderPath'"
                    ." AND FolderDescr = '$FolderDescr'"
                    ." AND ParentFolder = '$parent'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION FOLDERDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION FOLDERDELETE");
        }
    }

    $main::msgtolog .= " $actionMessage";
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getFolderReports($method, $reqident, $selection);
    }
}

sub getProfilesOperations {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause1 = " WHERE     (NOT (pr.ProfileName = 'SUPER')) AND (NOT (pf.RootNode = 'ROOT')) ";
    my $whereClause2 = "";
    my $whereClause3 = 
        "WHERE NOT profilename IN "
        ."(SELECT DISTINCT profilename FROM tbl_profilesfolderstrees "
        ."UNION ALL SELECT DISTINCT varsetname FROM tbl_varsetsvalues WHERE varname LIKE 'SVC:\%' ) "
    ;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        
        if($field_name =~ /ProfileAuthorities/) {
            $whereClause1 = _addWhereClause($whereClause1, "pf.RootNode like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            $whereClause2 = _addWhereClause($whereClause2, "VarValue like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            $whereClause3 = _addWhereClause($whereClause3, "'' like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
        } else {
            $whereClause1 = _addWhereClause($whereClause1, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            $whereClause2 = _addWhereClause($whereClause2, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            $whereClause3 = _addWhereClause($whereClause3, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
        }
    }

    my $sql = "SELECT TOP 1000 pr.ProfileName,"
        ."pr.ProfileDescr,"
        ."pf.RootNode AS ProfileAuthorities "
        ."FROM            tbl_Profiles             AS pr "
        ."INNER JOIN tbl_ProfilesFoldersTrees AS pf ON pr.ProfileName = pf.ProfileName "
        ." $whereClause1 "
        ."union all "
        ."SELECT     pr.ProfileName, pr.ProfileDescr,  DT.VarSetName AS ProfileAuthorities "
        ."FROM            tbl_Profiles             AS pr "
        ."INNER JOIN (select distinct VarsetName , VarValue from tbl_VarSetsValues AS vv where (vv.VarName LIKE 'SVC:\%') ) DT "
        ."ON pr.ProfileName = dt.VarSetName "
        ."$whereClause2 "
        ."union all "
        ."SELECT     pr.ProfileName, pr.ProfileDescr, '' AS ProfileAuthorities "
        ."FROM            tbl_Profiles   AS pr "
        ." $whereClause3 "
        ."ORDER BY pr.ProfileName, ProfileAuthorities"
    ;


    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub manageProfilesOperations {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }
    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    my $ProfileName = uc($IndexEntries->[$entryPos]->{'Columns'}->{'ProfileName'}->{'content'});
    my $ProfileDescr = dbEscape($IndexEntries->[$entryPos]->{'Columns'}->{'ProfileDescr'}->{'content'});
    my @ProfileAuthorities = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'ProfileAuthorities'}->{'content'});

    $main::msgtolog .= "$action profile $ProfileName";
    if($action =~ /add/i) {
        _updateDataFromSQL("BEGIN TRANSACTION PROFILEADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_Profiles WHERE ProfileName = '$ProfileName'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_Profiles"
                    ." (ProfileName"
                    ." ,ProfileDescr"
                    ." ,LookAtEverything)"
                    ." VALUES"
                    ." ('$ProfileName'"
                    ." ,'$ProfileDescr'"
                    ." ,NULL)"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Profile $ProfileName already exists";
            }

            foreach my $folder(@ProfileAuthorities) {
                $sql =
                    "INSERT INTO tbl_ProfilesFoldersTrees"
                    ." (ProfileName"
                    ." ,RootNode)"
                    ." VALUES"
                    ." ('$ProfileName'"
                    ." ,'".uc($folder)."')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION PROFILEADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION PROFILEADD");
        }
    } elsif($action =~ /update/i) {
        _updateDataFromSQL("BEGIN TRANSACTION PROFILEUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_Profiles WHERE ProfileName = '$ProfileName'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "UPDATE tbl_Profiles SET"
                    ." ProfileDescr = '$ProfileDescr'"
                    ." WHERE ProfileName = '$ProfileName'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Profile $ProfileName doesn't exists";
            }

            $sql =
                "DELETE tbl_ProfilesFoldersTrees WHERE"
                ." ProfileName = '$ProfileName'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql);

            foreach my $folder(@ProfileAuthorities) {
                $sql =
                    "INSERT INTO tbl_ProfilesFoldersTrees"
                    ." (ProfileName"
                    ." ,RootNode)"
                    ." VALUES"
                    ." ('$ProfileName'"
                    ." ,'".uc($folder)."')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION PROFILEUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION PROFILEUPDATE");
        }
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION PROFILEDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            foreach my $folder(@ProfileAuthorities) {
                $sql =
                    "DELETE tbl_ProfilesFoldersTrees WHERE"
                    ." ProfileName = '$ProfileName'"
                    ." AND RootNode = '".uc($folder)."'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }

            $sql = "SELECT * FROM tbl_ProfilesFoldersTrees WHERE ProfileName = '$ProfileName'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "DELETE tbl_Profiles WHERE"
                    ." ProfileName = '$ProfileName'"
                    ." AND ProfileDescr = '$ProfileDescr'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION PROFILEDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION PROFILEDELETE");
        }
    }

    $main::msgtolog .= " $actionMessage";
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getProfilesOperations($method, $reqident, $selection);
    }
}

sub getUsersProfiles {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }
    
    my $sql = 
        "SELECT TOP 1000 us.UserName as UserName,"
        ."us.UserDescr as NameDescr,"
        ."ua.UserAlias as UserId,"
        ."ua.UserAliasDescr as IDDescr,"
        ."us.EMailAddr as Mail,"
        ."pr.ProfileName as Profiles, "
	."us.UserName as UserName1 "
        ."FROM tbl_Users us "
        ."INNER JOIN tbl_UserAliases ua on ua.UserName = us.UserName "
        ."LEFT JOIN tbl_UsersProfiles pr on pr.UserName = us.UserName"
        ." $whereClause"
    ;
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub manageUsersProfiles {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }

    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    my $UserName = uc($IndexEntries->[$entryPos]->{'Columns'}->{'UserName'}->{'content'});
    my $NameDescr = dbEscape($IndexEntries->[$entryPos]->{'Columns'}->{'NameDescr'}->{'content'});
    
    my $UserId = uc($IndexEntries->[$entryPos]->{'Columns'}->{'UserId'}->{'content'});
    my $IDDescr = dbEscape($IndexEntries->[$entryPos]->{'Columns'}->{'IDDescr'}->{'content'});
    
    my $Mail = $IndexEntries->[$entryPos]->{'Columns'}->{'Mail'}->{'content'};
    my @Profiles = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'Profiles'}->{'content'});

    main::debug2log("action is $action");
    $main::msgtolog .= "$action user $UserName";
    if($action =~ /add/i) {
        main::debug2log("Add User");
        _updateDataFromSQL("BEGIN TRANSACTION USERADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_Users WHERE UserName = '$UserName'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_Users"
                    ." (UserName"
                    ." ,UserDescr"
                    ." ,EMailAddr"
                    ." ,Password)"
                    ." VALUES"
                    ." ('$UserName'"
                    ." ,'$NameDescr'"
                    ." ,'$Mail'"
                    ." ,'')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "User $UserName already exists";
            }

            $sql = "SELECT * FROM tbl_UserAliases WHERE UserName = '$UserName' AND UserAlias = '$UserId'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql =
                    " INSERT INTO tbl_UserAliases"
                    ." (UserAlias"
                    ." ,UserName"
                    ." ,UseAlias"
                    ." ,UserAliasDescr)"
                    ." VALUES"
                    ." ('$UserId'"
                    ." ,'$UserName'"
                    ." ,NULL"
                    ." ,'$IDDescr')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Alias $UserId for $UserName already exists";
            }

            foreach my $profile(@Profiles) {
                $sql =
                    " INSERT INTO tbl_UsersProfiles"
                    ." (UserName"
                    ." ,ProfileName)"
                    ." VALUES"
                    ." ('$UserName'"
                    ." ,'".uc($profile)."')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION USERADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION USERADD");
        }
    } elsif($action =~ /update/i) {
        _updateDataFromSQL("BEGIN TRANSACTION USERUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_Users WHERE UserName = '$UserName'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "UPDATE tbl_Users SET"
                    ." UserDescr = '$NameDescr'"
                    ." ,EMailAddr = '$Mail'"
                    ." WHERE UserName = '$UserName'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "User $UserName doesn't exists";
            }

            $sql = "SELECT * FROM tbl_UserAliases WHERE UserName = '$UserName' AND UserAlias = '$UserId'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql =
                    " UPDATE tbl_UserAliases SET"
                    ." UserAliasDescr = '$IDDescr'"
                    ." WHERE UserName = '$UserName' AND UserAlias = '$UserId'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Alias $UserId for $UserName doesn't exists";
            }

            $sql =
                "DELETE tbl_UsersProfiles WHERE"
                ." UserName = '$UserName'"
            ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql);

            foreach my $profile(@Profiles) {
                $sql =
                    " INSERT INTO tbl_UsersProfiles"
                    ." (UserName"
                    ." ,ProfileName)"
                    ." VALUES"
                    ." ('$UserName'"
                    ." ,'".uc($profile)."')"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION USERUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION USERUPDATE");
        }
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION USERDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            foreach my $profile(@Profiles) {
                $sql =
                    "DELETE tbl_UsersProfiles WHERE"
                    ." UserName = '$UserName'"
                    ." AND ProfileName = '".uc($profile)."'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
    
            $sql = "SELECT * FROM tbl_Users WHERE UserName = '$UserName'";
            my $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "DELETE tbl_Users WHERE"
                    ." UserName = '$UserName'"
                    ." AND UserDescr = '$NameDescr'"
                    ." AND EmailAddr = '$Mail'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
        
                $sql = 
                    "DELETE tbl_UserAliases WHERE"
                    ." UserAlias = '$UserId'"
                    ." AND UserName = '$UserName'"
                    ." AND UserAliasDescr = '$IDDescr'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION USERDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION USERDELETE");
        }
    }

    $main::msgtolog .= " $actionMessage";
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getUsersProfiles($method, $reqident, $selection);
    }
}

sub downloadConfig {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $filter = undef;
    my $XLS;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $IndexEntry(@{$IndexEntries}) {
        if(!defined($IndexEntry->{'Columns'}->{'Command'})) {
            main::debug2log("Command no found: ".Dumper($IndexEntry));
            $filter = $IndexEntry;
            last;
        }
    }

    eval {
        require "$main::Application->{'XREPORT_HOME'}/bin/config2xls.pl";
        main::debug2log("$main::Application->{'XREPORT_HOME'}/bin/config2xls.pl");
        buildXLS('ALL', \$XLS);
        main::debug2log("Excel length: ".length($XLS));
    };
    if($@) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => "##########[ UPLOAD ERROR ]##########\n\n".$@}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getConfigReports($method, $reqident, $selection, $XLS);
    }
}

sub downloadConfig_old {
    my ($method, $reqident, $selection) = (shift, shift, shift);

    require "$main::Application->{'XREPORT_HOME'}/bin/config2xls.pl";
    main::debug2log("$main::Application->{'XREPORT_HOME'}/bin/config2xls.pl");
    my $XLS;
    buildXLS('ALL', \$XLS);

    main::debug2log("Excel length: ".length($XLS));
    my $xlsv = undef;
    for(my $i = 0; $i < length($XLS); $i += 32768) {
        my $len = 32768;
        $len = length($XLS) - $i if((length($XLS) - $i) < $len);
#       main::debug2log("Current length: $len(".(length($XLS) - $i).")");
        my $chunk = substr($XLS, $i, 32768);
        if(!$xlsv) {
            $xlsv = new Win32::OLE::Variant(VT_UI1, $chunk);
        } else {
            $xlsv->Put($chunk);
        }
        $main::Response->BinaryWrite($xlsv);
    }
}

sub downloadConfig2 {
    my ($method, $reqident, $selection) = (shift, shift, shift);

    require "$main::Application->{'XREPORT_HOME'}/bin/config2xls.pl";
    main::debug2log("$main::Application->{'XREPORT_HOME'}/bin/config2xls.pl");
    my $XLS;
    buildXLS('ALL', \$XLS);

#   my $handle = gensym();
#   open($handle, ">D:/downloadConfig.xls");
#   binmode($handle);
#   print $handle $XLS;
#   close($handle);

    main::debug2log("Excel length: ".length($XLS));
    my $buffsz = 57*76;
    my $XLS64 = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $XLS ) );

#   my $xlsv = undef;
#   for(my $i = 0; $i < length($XLS); $i += 32768) {
#       my $len = 32768;
#       $len = length($XLS) - $i if((length($XLS) - $i) < $len);
#       main::debug2log("Current length: $len(".(length($XLS) - $i).")");
#       my $chunk = substr($XLS, $i, 32768);
#       if(!$xlsv) {
#           $xlsv = new Win32::OLE::Variant(VT_UI1, $chunk);
#       } else {
#           $xlsv->Put($chunk);
#       }
#       $main::Response->BinaryWrite($xlsv);
#   }

    my $actionMessage = "SUCCESS";
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];

    my $response = XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries,
        DocumentType => 'application/vnd.ms-excel',
        FileName => "downloadConfig.xls",
        Identity => "downloadConfig",
        DocumentBody => {content => "<![CDATA[$XLS64]]>"}
    });
  my $ctlen = length($response);
  $main::Response->Clear();
  $main::Response->{'buffer'} = 0;
  $main::Response->AddHeader('Content-Length' => $ctlen);
#  $main::Response->{ContentType} = "text/xml";
  
    my $t = 0;
  while ( $response ) {
    (my $buff, $response) = unpack("a32768 a*", $response);
    if($buff) {
        $main::Response->Write($buff);
        $t += length($buff);
    }
#   main::debug2log("len response:".length($response));
  }  
  main::debug2log("${method} $reqident $ctlen bytes of response sent to requester($t)");
  return $main::Response->Flush();
#   main::debug2log(Dumper($respEntries));
#   $main::Response->Write($response);
#   $main::Response->Flush();
#   $main::Response->End();
}

my $uploadResponse;

sub logIt {
  for (@_) {
    my $t = $_;
#    $t =~ s/\n/<br\/>/sg;
    $uploadResponse .= "$t\n";
  }
}

sub debug2log {
#   main::debug2log($_);
}

sub uploadConfig2 {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    $uploadResponse = "";
    my $uploadError = "";

    require "$main::Application->{'XREPORT_HOME'}/bin/loadExcelConfig.pl";
    main::debug2log("$main::Application->{'XREPORT_HOME'}/bin/loadExcelConfig.pl");
    my $DocumentBody = $selection->{request}->{DocumentBody};
    logIt("DB: ".length($DocumentBody));
    my $XLS = MIME::Base64::decode_base64($DocumentBody);
    logIt("Processing Started at ". localtime());
    main::debug2log("Processing Started at ". localtime());
    eval {
        parseXLSConfig('XReport::XReportWebIface',\$XLS);
        logIt("CeReport Upload Ended at " . localtime());
        main::debug2log("CeReport Upload Ended at " . localtime());
    };
#   if($@) {
#       $uploadError = "##########[ UPLOAD ERROR ]##########\n\n".$@;
#   }
    if($@) {
      $uploadError = "##########[ UPLOAD ERROR ]##########\n\n".$@;
    } else {
      $uploadResponse = "CONFIG APPLIED SUCCESSFULLY";
    }
    if($uploadError !~ /^\s*$/) {
        $uploadResponse = $uploadError."\n\n##########[ UPLOAD LOG ]##########\n\n".$uploadResponse;
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => 'SUCCESS'}
            ]
        },
        {
            Columns => [
                {colname => 'UploadResult', content => $uploadResponse}
            ]
        }
    ];

    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub _setXferFileA {
  my ($queuename, $progr, $datetime) = @_;

  $datetime = GetDateTime() unless ($datetime && $datetime =~ /^\d{14}$/);
  my ($curryear, $currday, undef) = unpack("a4a4a*", $datetime);

  my $storage = getConfValues('LocalPath')->{'L1'}."/IN";
  $storage =~ s/^file:\/\///;
  logIt("LocalPath is $storage");
  main::debug2log("LocalPath is $storage");
  if (! -e $storage."/".$curryear."/".$currday ) {
    if (! -e $storage."/".$curryear) {
        die "Unable to create $storage/$curryear ($!)\n" if(!mkdir $storage."/".$curryear);
    }
    if (! -e $storage."/".$curryear."/".$currday) {
        die "Unable to create $storage/$curryear/$currday ($!)\n" if(!mkdir $storage."/".$curryear."/".$currday);
    }
  }

  my $filen = "$curryear/$currday/$queuename.$datetime.$progr.DATA.TXT";
  
  return ($filen, $storage, $datetime);
}

sub uploadConfig {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;

    $uploadResponse = "";
    my $uploadError = "";
    my $JobReportName = $main::Application->{'cfg.xlscfgreportname'};
    main::debug2log("ReportName is $JobReportName");

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $blockDocumentBody = 0;
    foreach my $IndexEntry(@{$IndexEntries}) {
        if(!defined($IndexEntry->{'Columns'}->{'Command'})) {
            $filter = $IndexEntry;
            last;
        }
    }

    my $DocumentBody = $selection->{request}->{DocumentBody};
    logIt("DB: ".length($DocumentBody));
    my $XLS = MIME::Base64::decode_base64($DocumentBody);
    logIt("Processing Started at ". localtime());
    main::debug2log("Processing Started at ". localtime());
    eval {
        my $JobReportId = QCreate XReport::QUtil(
            SrvName        => "XReportWebIface",
            JobReportName  => $JobReportName,
            LocalFileName  => $JobReportName . $$ . '001',
            RemoteHostAddr => $main::Server->{'REMOTE_ADDR'},
            RemoteFileName => "UPLOADCONFIG.xls",
            XferStartTime  => "GETDATE()",
            XferMode       => 2,
            XferDaemon     => "XReportWebIface",
            Status         => 0,
            XferId         => 123,
        );
        my ($toFile, $todir, $datetime) = _setXferFileA($JobReportName, $JobReportId);
        my $toFile_ = "$todir/$toFile"; unlink glob("$toFile_\*");
        my $OUTPUT = gzopen("$toFile_\.PENDING", "wb") or die "ERROR AT OUTPUT GZOPEN for file \"$toFile\" $!";
        my $gzerr = $OUTPUT->gzwrite($XLS);
        $OUTPUT->gzclose();
        if($gzerr <= 0) {
            main::debug2log("GZ Write error: $gzerr");
            logIt("GZ Write error: $gzerr");
            die("GZ Write error: $gzerr");
        }

        my $rc = rename("$toFile_\.PENDING", "$toFile_\.gz");
        if ( !$rc ) {
          logIt("RENAME Error ($toFile_\.PENDING", "$toFile_\.gz) $!");
          main::debug2log("RENAME Error ($toFile_\.PENDING", "$toFile_\.gz) $!");
          die ("FileError: RENAME Error $!");
        }
        $toFile .= ".gz";
        
        main::debug2log("TRANSFER ENDED NORMALLY FROM UPLOADED FILE", " INTO \"$toFile_\.gz\""); 
        
        my $req = QUpdate XReport::QUtil(
            XferEndTime    => 'GETDATE()',
            Status         => 16,
            JobName        => 'XRUPLOADXLSCFG',
            JobNumber      => "UPLOAD$JobReportId",
            JobReportName  => $JobReportName,
            XferRecipient  => '',
            RemoteFileName => "UPLOADCONFIG.xls",
            LocalFileName  => $toFile,
            XferPages      => 0,
            XferInBytes    => length($XLS),
            XferMode       => 2,
            Id             => $JobReportId,
        );

        logIt("CeReport Upload Ended at " . localtime());
        main::debug2log("CeReport Upload Ended at " . localtime());
    };
    if($@) {
      $uploadError = "##########[ UPLOAD ERROR ]##########\n\n".$@;
    } else {
      $uploadResponse = "CONFIG SUCCESSFULLY QUEUED FOR PROCESSING";
      $actionMessage = "SUCCESS";
    }
    if($uploadError !~ /^\s*$/) {
        $uploadResponse = $uploadError."\n\n##########[ UPLOAD LOG ]##########\n\n".$uploadResponse;
    }

    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];

        my $respToBuild = {
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        };

        my $buffsz = 57*76;
        $uploadResponse = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $uploadResponse ) );
        $respToBuild->{'DocumentType'} = 'text/xml';
        $respToBuild->{'FileName'} = "xrmonitor.xml";
        $respToBuild->{'Identity'} = "ConfigReportsData";
        $respToBuild->{'DocumentBody'} = {content => "<![CDATA[$uploadResponse]]>"};

        $main::Response->Write(XReport::SOAP::buildResponse($respToBuild));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getConfigReports($method, $reqident, $selection, $uploadResponse);
    }
}

sub recoveryConfig {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    $uploadResponse = "";
    my $uploadError = "";

    require "$main::Application->{'XREPORT_HOME'}/bin/restoreLastConfig.pl";
    main::debug2log("$main::Application->{'XREPORT_HOME'}/bin/restoreLastConfig.pl");

    logIt("Processing Started at ". localtime());
    main::debug2log("Processing Started at ". localtime());
    eval {
        restoreLastConfig();
    };
    if($@) {
        $uploadError = "##########[ RECOVERY ERROR ]##########\n\n".$@;
    }
    logIt("CeReport Restore Ended at " . localtime());
    main::debug2log("CeReport Restore Ended at " . localtime());

    if($uploadError !~ /^\s*$/) {
        $uploadResponse = $uploadError."\n\n##########[ RECOVERY LOG ]##########\n\n".$uploadResponse;
    }

    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => 'SUCCESS'}
            ]
        },
        {
            Columns => [
                {colname => 'RecoveryResult', content => $uploadResponse}
            ]
        }
    ];

    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub getConfigReports {
    my ($method, $reqident, $selection, $documentBody) = (shift, shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";
    my $joinClause = "";
    my $JobReportName = $main::Application->{'cfg.xlscfgreportname'} || "XRXLSCFG";

    $whereClause = _addWhereClause($whereClause, "JobReportName like", $JobReportName, "'");
    $whereClause = _addWhereClause($whereClause, "tbl_JobReports.PendingOp NOT IN (", "12", "");
    $whereClause .= ")";
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;

        if($field_name =~ /^FromDate$/) {
            $whereClause = _addWhereClause($whereClause, "XferStartTime >=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            next;
        }
        if($field_name =~ /^ToDate$/) {
            $whereClause = _addWhereClause($whereClause, "XferStartTime <=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
                if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
            next;
        }
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }
    
    my $sql = 
        "SELECT TOP 1000 '' AS Command, tbl_JobReports.* "
        ." FROM tbl_JobReports "
        ." $joinClause "
        ." $whereClause "
        ." ORDER BY JobReportId DESC"
    ;
    main::debug2log($sql);
    my $result;
    eval {
        $result = _getDataFromSQL($sql);
        if(!$result) {
            $actionMessage = "No configs found!";
        } else {
            $actionMessage = "SUCCESS";
        }
    };
    $actionMessage = "Search of configs failed: $@" if($@);
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
    my $respToBuild = {
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    };

    if($documentBody !~ /^\s*$/) {
        my $buffsz = 57*76;
        $documentBody = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $documentBody ) );
        $respToBuild->{'DocumentType'} = 'text/xml';
        $respToBuild->{'FileName'} = "xrmonitor.xml";
        $respToBuild->{'Identity'} = "JobReportsData";
        $respToBuild->{'DocumentBody'} = {content => "<![CDATA[$documentBody]]>"};
    }
    $main::Response->Write(XReport::SOAP::buildResponse($respToBuild));
}

sub manageConfigReports {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $documentBody = "";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $blockDocumentBody = 0;
    foreach my $IndexEntry(@{$IndexEntries}) {
        if(!defined($IndexEntry->{'Columns'}->{'Command'})) {
            main::debug2log("Command no found: ".Dumper($IndexEntry));
            $filter = $IndexEntry;
            next;
        }
        my $currentCommand = uc($IndexEntry->{'Columns'}->{'Command'}->{'content'});
        ($currentCommand) = split("/", $currentCommand) if($currentCommand =~ /\//);
        $currentCommand = uc($currentCommand);
        main::debug2log("Command is $currentCommand");
        my $jobReportId = $IndexEntry->{'Columns'}->{'JobReportId'}->{'content'};
        if(exists($excelConfigLineCommands->{$currentCommand})) {
            my $commandFunction = $excelConfigLineCommands->{$currentCommand};
            if(defined(&{$commandFunction})) {
                main::debug2log("Call function $commandFunction");
                my $func = \&{$commandFunction};
                ($actionMessage, my $data) = &$func($jobReportId, $IndexEntry);
                last if($actionMessage !~ /SUCCESS/);
                if($currentCommand =~ /^(VD|O)$/) {
                    $documentBody = $data;
                    $blockDocumentBody = 1;
                }
                $documentBody .= "\n############[JobReportId $jobReportId]############\n\n".$data."\n" if($data !~ /^\s*$/ and !$blockDocumentBody);
            }
        }
    }
    
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getConfigReports($method, $reqident, $selection, $documentBody);
    }
}

sub getPrintCopies{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";
    main::debug2log("FILTER_RULES ".Dumper($selection));
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    $IndexEntries->[0]->{'Columns'}->{'cmd_vv.VarSetName'}->{'content'} = 'PRTC:'.($IndexEntries->[0]->{'Columns'}->{'cmd_vv.VarSetName'} ? $IndexEntries->[0]->{'Columns'}->{'cmd_vv.VarSetName'}->{'content'}.'%' : "%");
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
    #if($field_name =~ /VarSetName/i){
    #   $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} = 'PRTC:'.$IndexEntries->[0]->{'Columns'}->{$field}->{'content'};    
    #}
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    my $sql = "SELECT  TOP 1000 substring(vv.VarSetName,6, len(vv.VarSetName)) as VarGroup, substring(vv.VarName, 0,CHARINDEX('/',vv.VarName)) as JobName , substring(vv.VarName,CHARINDEX('/',vv.VarName)+1, LEN(vv.VarName)) as ReportName, vv.VarValue FROM    tbl_VarSetsValues vv "
              ." $whereClause ";
     main::debug2log("SQL ".Dumper($sql));
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
    main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}


sub managePrintCopies {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;
    my $whereClause = "";
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    my @VarSetNames =  split(/[;]/,$IndexEntries->[$entryPos]->{'Columns'}->{'VarSetName'}->{'content'});
    my @VarNames = split(/[;]/,$IndexEntries->[$entryPos]->{'Columns'}->{'VarName'}->{'content'});
   
    my @Clause;
    
    #foreach my $field(keys(%{$IndexEntries->[$entryPos]->{'Columns'}})) {
    #    my $field_name = $field;
    #
    #    $IndexEntries->[$entryPos]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
    #    $IndexEntries->[$entryPos]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
    #    $IndexEntries->[$entryPos]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;
    #
    #    if($IndexEntries->[$entryPos]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
    #        delete($IndexEntries->[$entryPos]->{'Columns'}->{$field_name});
    #        next;
    #    }
#   if($IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'} !~ /^$/){
#      next;
#   }
#        $field_name =~ s/^cmd_//;
#   main::debug2log($whereClause);
#   $whereClause = _addWhereClause($whereClause, "$field_name", $IndexEntries->[$entryPos]->{'Columns'}->{$field}->{'content'}, "'")
#            if($IndexEntries->[$entryPos]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
#    }
    
    my $NPrintCopies =  $IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'};
    #my @ReportGroups = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'});

    my $parent = "ROOT";
    

    if($action =~ /add/i) {
        my $count = 0; 
    my $result = undef;
    my $sql = "";
    foreach my $VarSetName (@VarSetNames){
            my $forClause = "";
        $forClause = "select 'PRTC:".$VarSetName."'" ;
        $forClause .= ", '".@VarNames[$count]."' " ;
        $forClause .= ", ' ".$NPrintCopies."' " ;
        push @Clause, $forClause;
        $sql = "SELECT * FROM tbl_VarSetsValues WHERE VarSetName = 'PRTC:$VarSetName' AND VarName = '".@VarNames[$count]."'";
            
        _updateDataFromSQL("BEGIN TRANSACTION COPIESADD");
        eval {
             $result = _getDataFromSQL($sql);
                     main::debug2log($sql);
             $whereClause = join (" UNION ALL ", @Clause); 
             main::debug2log("XXXXXXXXXXXXX".$whereClause);
        
            if(!$result) {
            $sql = "INSERT INTO tbl_VarSetsValues "
                ."$whereClause";
            main::debug2log($sql);
            $result= _updateDataFromSQL($sql); 
            } else {
            die "VarSetValues items PRTC:$VarSetName @VarNames[$count] already exists";
            }
        $count++;
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION COPIESADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION COPIESADD");
        }
    }
    
    } elsif($action =~ /update/i) {
         my $count = 0; 
     foreach my $VarSetName (@VarSetNames){
         my $forClause = "";
     $forClause = " ( VarSetName = 'PRTC:".$VarSetName."'" ;
     $forClause .= " AND  VarName = '".@VarNames[$count]."' )";
     push @Clause, $forClause;
     $count++;

    }
    $whereClause = _addWhereClause($whereClause, "" , join (" OR ", @Clause), "");
    _updateDataFromSQL("BEGIN TRANSACTION COPIESUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
        $sql = "UPDATE tbl_VarSetsValues SET VarValue = '$NPrintCopies'". $whereClause;
        main::debug2log($sql);
        #$sql = "SELECT * FROM tbl_VarSetsValues WHERE VarSetName = '$SetNameOld' AND VarName = '$NameOld' AND VarValue = '$ValueOld'";
        $result = _updateDataFromSQL($sql);
        
            
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION COPIESUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION COPIESUPDATE");
        }
    } elsif($action =~ /del/i) {
         my $count = 0; 
     foreach my $VarSetName (@VarSetNames){
            my $forClause = "";
        $forClause = " ( VarSetName = 'PRTC:".$VarSetName."'" ;
        $forClause .= " AND  VarName = '".@VarNames[$count]."' )";
        push @Clause, $forClause;
        $count++;

    }
    $whereClause = _addWhereClause($whereClause, "" , join (" OR ", @Clause), "");
        _updateDataFromSQL("BEGIN TRANSACTION COPIESDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            if(!$result) {
                $sql = "DELETE tbl_VarSetsValues $whereClause";
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION COPIESDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION COPIESDELETE");
        }
    }

    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getPrintCopies($method, $reqident, $selection);
    }
}

sub getFilterRules{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "WHERE   (NOT (VarName = ':REPORTNAME')) AND (NOT (VarName LIKE 'SVC:\%')) AND NOT VarSetName like 'PRTC:%' AND NOT VarName = ':BundleIncludePattern' AND NOT VarName = ':BundleExcludePattern' ";
    main::debug2log("FILTER_RULES ".Dumper($selection));
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    my $sql = "SELECT  TOP 1000 vv.VarSetName as VarGroup, vv.VarName, vv.VarValue "
          ."FROM    tbl_VarSetsValues vv "
              ." $whereClause ";
     main::debug2log("SQL ".Dumper($sql));
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
    main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}


sub manageFilterRules {
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }

    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    my $SetName = uc($IndexEntries->[$entryPos]->{'Columns'}->{'SetName'}->{'content'});
    my $Name = $IndexEntries->[$entryPos]->{'Columns'}->{'Name'}->{'content'};
    my $Value =  $IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'};
    my $SetNameOld = uc($IndexEntries->[$entryPos]->{'Columns'}->{'_SetName'}->{'content'});
    my $NameOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_Name'}->{'content'};
    my $ValueOld =  $IndexEntries->[$entryPos]->{'Columns'}->{'_Value'}->{'content'};
    #my @ReportGroups = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'});

    my $parent = "ROOT";
    
    $main::msgtolog .= "$action filter rule $SetName;$Name;$Value";
    if($action =~ /add/i) {
        _updateDataFromSQL("BEGIN TRANSACTION FILTERADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_VarSetsValues WHERE VarSetName = '$SetName' AND VarName = '$Name' AND VarValue = '$Value'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_VarSetsValues"
                    ." (VarSetName"
                    ." ,VarName"
                    ." ,VarValue)"
                    ." VALUES"
                    ." ('$SetName'"
                    ." ,'$Name'"
                    ." ,'$Value')"          
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "VarSetValues item $SetName $Name $Value already exists";
            }

        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION FILTERADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION FILTERADD");
        }
    } elsif($action =~ /update/i) {
        _updateDataFromSQL("BEGIN TRANSACTION FILTERUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_VarSetsValues WHERE VarSetName = '$SetNameOld' AND VarName = '$NameOld' AND VarValue = '$ValueOld'";
            $result = _getDataFromSQL($sql);
            if($result) {
                $sql = 
                    "DELETE tbl_VarSetsValues WHERE"
                    ." VarSetName = '$SetNameOld'"
                    ." AND VarName = '$NameOld'"
                    ." AND VarValue = '$ValueOld'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
            $sql = 
                    "INSERT INTO tbl_VarSetsValues"
                    ." (VarSetName"
                    ." ,VarName"
                    ." ,VarValue)"
                    ." VALUES"
                    ." ('$SetName'"
                    ." ,'$Name'"
                    ." ,'$Value')"
                ;
            main::debug2log($sql);
            $result = _updateDataFromSQL($sql); 
            
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION FILTERUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION FILTERUPDATE");
        }
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION FILTERDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            if(!$result) {
                $sql = 
                    "DELETE tbl_VarSetsValues WHERE"
                    ." VarSetName = '$SetName'"
                    ." AND VarName = '$Name'"
                    ." AND VarValue = '$Value'"
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION FILTERDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION FILTERDELETE");
        }
    }

    $main::msgtolog .= " $actionMessage";
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getFilterRules($method, $reqident, $selection);
    }
}


sub getReportGroups{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "";

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    my $sql = "SELECT TOP 1000  rg.ReportGroupId as ReportGroup, rg.ReportRule as ReportSet, "
                  ."rg.FilterVar as VarName, rg.FilterRule as VarGroup, rg.RecipientRule as RecipientName "
          ."FROM    tbl_NamedReportsGroups rg"
              ." $whereClause ";
    
      main::debug2log(Dumper($whereClause));
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}

sub manageReportGroups{

    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;
    main::debug2log("REPORT GROUPS ".Dumper($selection));
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }

    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    my $GroupOld = uc($IndexEntries->[$entryPos]->{'Columns'}->{'_Group'}->{'content'});
    my $SetOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_Set'}->{'content'};
    my $NameOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_Name'}->{'content'};
    my $RuleOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_Rule'}->{'content'};
    my $RecRuleOld =  $IndexEntries->[$entryPos]->{'Columns'}->{'_RecRule'}->{'content'};
    my $Group = uc($IndexEntries->[$entryPos]->{'Columns'}->{'Group'}->{'content'});
    my $Set = $IndexEntries->[$entryPos]->{'Columns'}->{'Set'}->{'content'};
    my $Name = $IndexEntries->[$entryPos]->{'Columns'}->{'Name'}->{'content'};
    my $Rule = $IndexEntries->[$entryPos]->{'Columns'}->{'Rule'}->{'content'};
    my $RecRule =  $IndexEntries->[$entryPos]->{'Columns'}->{'RecRule'}->{'content'};
    #my @ReportGroups = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'});

    my $parent = "ROOT";
    
    $main::msgtolog .= "$action reportgroup $Group;$Set;$Name;$Rule;$RecRule";
    if($action =~ /add/i) {
        _updateDataFromSQL("BEGIN TRANSACTION GROUPADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_NamedReportsGroups WHERE ReportGroupId = '$Group' AND ReportRule = '$Set' AND FilterVar = '$Name' 
                AND FilterRule = '$Rule' AND RecipientRule = '$RecRule'";
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_NamedReportsGroups"
                    ." (ReportGroupId"
                    ." ,ReportRule"
                    ." ,FilterVar"
                    ." ,FilterRule"
                    ." ,RecipientRule)"
                    ." VALUES"
                    ." ('$Group'"
                    ." ,'$Set'"
                    ." ,'$Name'"
                    ." ,'$Rule'"
                    ." ,'$RecRule')"
                    
                ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            } else {
                die "Report Group item $Group $Set $Name $Rule already exists";
            }

        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION GROUPADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION GROUPADD");
        }
    } elsif($action =~ /update/i) {
        _updateDataFromSQL("BEGIN TRANSACTION GROUPUPDATE");
        eval {
            my $sql = "";
            my $result = undef;
                $sql = "SELECT * FROM tbl_NamedReportsGroups WHERE ReportGroupId = '$GroupOld' AND ReportRule = '$SetOld' AND FilterVar = '$NameOld' 
                AND FilterRule = '$RuleOld' AND RecipientRule = '$RecRuleOld'";
            $result = _getDataFromSQL($sql);
            if($result) {
                    $sql = "DELETE tbl_NamedReportsGroups WHERE
                        ReportGroupId = '$GroupOld' AND ReportRule = '$SetOld'
                        AND FilterVar = '$NameOld' AND FilterRule = '$RuleOld'
                        AND RecipientRule = '$RecRuleOld'" ;
                    main::debug2log($sql);
                $result = _updateDataFromSQL($sql);

                #if(!$result) {
                        $sql = 
                        "INSERT INTO tbl_NamedReportsGroups"
                        ." (ReportGroupId"
                        ." ,ReportRule"
                        ." ,FilterVar"
                        ." ,FilterRule"
                        ." ,RecipientRule)"
                        ." VALUES"
                        ." ('$Group'"
                        ." ,'$Set'"
                        ." ,'$Name'"
                        ." ,'$Rule'"
                        ." ,'$RecRule')"
                        
                    ;
                    main::debug2log($sql);
                    $result = _updateDataFromSQL($sql); 
                    #} else {
                    #die "Report Group item $Group $Set $Name $Rule $RecRule doesn't exists";
                    #}



            } else {
                die "Item $Group $Set $Name $Rule doesn't exists";
            }

        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION GROUPUPDATE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION GROUPUPDATE");
        }
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION GROUPDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            if(!$result) {
                $sql = "DELETE tbl_NamedReportsGroups WHERE
                    ReportGroupId = '$Group' AND ReportRule = '$Set'
                    AND FilterVar = '$Name' AND FilterRule = '$Rule'
                    AND RecipientRule = '$RecRule'" ;
                main::debug2log($sql);
                $result = _updateDataFromSQL($sql); 
            }
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION GROUPDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION GROUPDELETE");
        }
    }

    $main::msgtolog .= " $actionMessage";
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
    } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getReportGroups($method, $reqident, $selection);
    }
}

sub getBundlesOper{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED" ; my $whereClause = "";
    main::debug2log("get BundlesOper OHOH ".Dumper($selection));
    
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
    my $field_name = $field;

    $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
    $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
    $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

    if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
    }
    $field_name =~ s/^cmd_//;
    $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    main::debug2log("get Clause OHOH ".Dumper($whereClause));
    my $sql = "select top 1000 SUBSTRING( MAX(VarValue), 22, len(MAX(VarValue))) as Include, case when MIN(VarValue) <> MAX(VarValue) THEN SUBSTRING( MIN(VarValue), 22, len(MIN(VarValue))) ELSE '' END as Exclude, ".
              "BundleSkel, BundleOrder, BundleCategory, BundleWriter, BundleUdata, BundleDescr, BundleUdest, InputMember, BundleName, BundleDest, BundleClass, BundleForm, BundlePrtopt, BundleKey from ".
          "( select VSV.VarSetName, VSV.VarName+' '+ ".
           " stuff(( ".
               "select distinct ';' + cast(t.VarValue as varchar(max)) ".
           "from tbl_VarSetsValues t ".
               "where t.VarName = VSV.VarName and t.VarSetName = VSV.VarSetName ".
               "for xml path('') ".
           "), 1, 1, '') as VarValue , ".
          " BundleSkel, BundleOrder, BundleCategory, BundleWriter, BundleUdata, BundleDescr, BundleUdest, InputMember, BundleName, BundleDest, BundleClass, BundleForm, BundlePrtopt, BundleKey ".
          " from tbl_bundlesNames BN ".
              " INNER JOIN tbl_VarSetsValues VSV on  VSV.VarSetName = BN.BundleKey ".
              " AND VarName like ':Bundle%Pattern'".
              " $whereClause ".
          " group by VSV.VarSetName, VSV.VarName ,BundleSkel, BundleOrder, BundleCategory, BundleWriter, BundleUdata, BundleDescr, BundleUdest, InputMember, BundleName, BundleDest, BundleClass, BundleForm, BundlePrtopt, BundleKey ) A ".
          " group by VarSetName ,BundleSkel, BundleOrder, BundleCategory, BundleWriter, BundleUdata, BundleDescr, BundleUdest, InputMember, BundleName, BundleDest, BundleClass, BundleForm, BundlePrtopt, BundleKey ";
    
    main::debug2log(Dumper($sql));
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
    ];
       
    push @{$respEntries}, @{$result} if($result);
    #main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
  
}


sub manageBundles{

    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $filter = undef;
    main::debug2log("BUNDLES ".Dumper($selection));
    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    my $entryPos = 0;
    if(length(@{$selection->{request}}{qw(IndexEntries)}) > 1) {
        $filter = $IndexEntries->[$entryPos];
        $entryPos = 1;
    }
    
    
    my $action = $IndexEntries->[$entryPos]->{'Columns'}->{'_action'}->{'content'};
    main::debug2log("*************ACTION ".$action);
    my $BundleSkelOld = uc($IndexEntries->[$entryPos]->{'Columns'}->{'_BundleSkel'}->{'content'});
    #my $BundleOrderOld = uc($IndexEntries->[$entryPos]->{'Columns'}->{'_BundleOrder'}->{'content'});
    my $BundleCategoryOld = uc($IndexEntries->[$entryPos]->{'Columns'}->{'_BundleCategory'}->{'content'});
    #my $BundleWriterOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleWriter'}->{'content'};
    #my $SetOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_Set'}->{'content'};
    #my $BundleUdataOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleUdata'}->{'content'};
    #my $BundleDescrOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleDescr'}->{'content'};
    #my $BundleUdestOld =  $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleUdest'}->{'content'};
    #my $InputMemberOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_InputMember'}->{'content'};
    my $BundleNameOld = uc($IndexEntries->[$entryPos]->{'Columns'}->{'_BundleName'}->{'content'});
    #my $BundleDestOld =  $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleDest'}->{'content'};
    #my $BundleClassOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleClass'}->{'content'};
    #my $BundleFormOld = $IndexEntries->[$entryPos]->{'Columns'}->{'_BundleForm'}->{'content'};
    #my $BundlePrtoptOld =  $IndexEntries->[$entryPos]->{'Columns'}->{'_BundlePrtopt'}->{'content'};
    my $BundleSkel = uc($IndexEntries->[$entryPos]->{'Columns'}->{'BundleSkel'}->{'content'});
    my $BundleOrder = $IndexEntries->[$entryPos]->{'Columns'}->{'BundleOrder'}->{'content'};
    my $BundleCategory = uc($IndexEntries->[$entryPos]->{'Columns'}->{'BundleCategory'}->{'content'});
    my $BundleWriter = $IndexEntries->[$entryPos]->{'Columns'}->{'BundleWriter'}->{'content'};
    my $BundleUdata = $IndexEntries->[$entryPos]->{'Columns'}->{'BundleUdata'}->{'content'};
    my $BundleDescr = $IndexEntries->[$entryPos]->{'Columns'}->{'BundleDescr'}->{'content'};
    my $BundleUdest =  $IndexEntries->[$entryPos]->{'Columns'}->{'BundleUdest'}->{'content'};
    my $InputMember = $IndexEntries->[$entryPos]->{'Columns'}->{'InputMember'}->{'content'};
    my $BundleName = uc($IndexEntries->[$entryPos]->{'Columns'}->{'BundleName'}->{'content'});
    my $BundleDest =  $IndexEntries->[$entryPos]->{'Columns'}->{'BundleDest'}->{'content'};
    my $BundleClass = $IndexEntries->[$entryPos]->{'Columns'}->{'BundleClass'}->{'content'};
    my $BundleForm = $IndexEntries->[$entryPos]->{'Columns'}->{'BundleForm'}->{'content'};
    my $BundlePrtopt =  $IndexEntries->[$entryPos]->{'Columns'}->{'BundlePrtopt'}->{'content'};
    
    #my $Set = $IndexEntries->[$entryPos]->{'Columns'}->{'Set'}->{'content'};
    #my $Name = $IndexEntries->[$entryPos]->{'Columns'}->{'Name'}->{'content'};
    #my @ValuesI = split(/[;\n]/, $IndexEntries->[$entryPos]->{'Columns'}->{'ValuesI'}->{'content'});
    #my @ValuesE = split(/[;\n]/ , $IndexEntries->[$entryPos]->{'Columns'}->{'ValuesE'}->{'content'});
    
    my $ValuesI =  $IndexEntries->[$entryPos]->{'Columns'}->{'ValuesI'}->{'content'};
    my $ValuesE =  $IndexEntries->[$entryPos]->{'Columns'}->{'ValuesE'}->{'content'};


    #my @ReportGroups = split(/[,;]/, $IndexEntries->[$entryPos]->{'Columns'}->{'Value'}->{'content'});

    my $parent = "ROOT";
     use Symbol;
    my $h = gensym();
    open($h, ">c:\\my.log") or die("Open error: $!\n");
    
    if($action =~ /add/i) {
        _updateDataFromSQL("BEGIN TRANSACTION BUNDLEADD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_BundlesNames WHERE BundleKey =  '".$BundleName."_".$BundleCategory."_".$BundleSkel."'";
            main::debug2log($sql);
            print $h "INQUIRY".$sql;
            $result = _getDataFromSQL($sql);
            if(!$result) {
                $sql = 
                    "INSERT INTO tbl_BundlesNames"
                    ." ( BundleSkel,"
                    ." BundleOrder," 
                    ." BundleCategory,"
                    ." BundleWriter,"
                    ." BundleUdata,"
                    ." BundleDescr,"
                    ." BundleUdest,"
                    ." InputMember,"
                    ." BundleName,"
                    ." BundleDest,"
                    ." BundleClass,"
                    ." BundleForm,"
                    ." BundlePrtopt )" 
                    ." VALUES ( "
                    ." '$BundleSkel', "
                    ."  '$BundleOrder'," 
                    ."  '$BundleCategory', "
                    ."  '$BundleWriter' ,"
                    ."  '$BundleUdata', "
                    ."  '$BundleDescr', "
                    ."  '$BundleUdest', "
                    ."  '$InputMember', "
                    ."  '$BundleName', "
                    ."  '$BundleDest', "
                    ."  '$BundleClass', "
                    ."  '$BundleForm', "
                    ."  '$BundlePrtopt');";
                 
                
                main::debug2log($sql);
                print $h "PRIMA".$sql;
                
                        main::debug2log("*********".Dumper("CIAO"));    
                if (_updateDataFromSQL($sql)){
                    my $sqlin = "INSERT INTO tbl_VarSetsValues ";
                       $sqlin =  $sqlin.join( "\n UNION ALL " ,  map{ join("  " , "select", "'".$BundleName."_".$BundleCategory."_".$BundleSkel."' ," , "':BundleIncludePattern' , ", "'$_'" ) } grep !/^\s*$/ , split(/[;\n]+/ , $ValuesI)); 
                               $sqlin =  $sqlin."\n UNION ALL " if ($ValuesI && $ValuesE);
                       $sqlin =  $sqlin.join( "\n UNION ALL " , map{ join(" " , "select", "'".$BundleName."_".$BundleCategory."_".$BundleSkel."' ," , "':BundleExcludePattern' , ", "'$_'" )} grep !/^\s*$/ , split(/[;\n]+/ , $ValuesE )); 
                                      
                      print $h "DOPO_".Dumper($sqlin);
                      print $h "\nEND";
                
                
                      main::debug2log("############INSERTING PATTERNS VALUES###########\n".Dumper($sqlin)."\n###################################");
                     _updateDataFromSQL($sqlin);   
                
                }else {
                             die "Bundle item '$BundleName '_'$BundleCategory '_'$BundleSkel': unable to insert on db";
                        }
                 
            } else {
                die "Bundle item '$BundleName '_'$BundleCategory '_'$BundleSkel' already exists";
            }

        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION BUNDLEADD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION BUNDLEADD");
        }
    } elsif($action =~ /update/i) {
    _updateDataFromSQL("BEGIN TRANSACTION BUNDLEUPD");
        eval {
            my $sql = "";
            my $result = undef;
            $sql = "SELECT * FROM tbl_BundlesNames bn WHERE BundleKey =  '".$BundleNameOld."_".$BundleCategoryOld."_".$BundleSkelOld."'";
            main::debug2log($sql);
            print $h "INQUIRY".$sql;
            $result = _getDataFromSQL($sql);
            if($result) {
                main::debug2log("##################".Dumper($sql)."##################");
                $sql = "DELETE from tbl_BundlesNames where BundleKey =  '".$BundleNameOld."_".$BundleCategoryOld."_".$BundleSkelOld."' ;";
                $sql .= "DELETE from tbl_VarSetsValues where VarSetName = '".$BundleNameOld."_".$BundleCategoryOld."_".$BundleSkelOld."' ;";
                main::debug2log("##################".Dumper($sql)."##################");
                 _updateDataFromSQL($sql); 
            }
            $sql =  "INSERT INTO tbl_BundlesNames"
                    ." ( BundleSkel,"
                    ." BundleOrder," 
                    ." BundleCategory,"
                    ." BundleWriter,"
                    ." BundleUdata,"
                    ." BundleDescr,"
                    ." BundleUdest,"
                    ." InputMember,"
                    ." BundleName,"
                    ." BundleDest,"
                    ." BundleClass,"
                    ." BundleForm,"
                    ." BundlePrtopt )" 
                    ." VALUES ( "
                    ." '$BundleSkel', "
                    ."  '$BundleOrder'," 
                    ."  '$BundleCategory', "
                    ."  '$BundleWriter' ,"
                    ."  '$BundleUdata', "
                    ."  '$BundleDescr', "
                    ."  '$BundleUdest', "
                    ."  '$InputMember', "
                    ."  '$BundleName', "
                    ."  '$BundleDest', "
                    ."  '$BundleClass', "
                    ."  '$BundleForm', "
                    ."  '$BundlePrtopt');";
                 
                
            main::debug2log($sql);
            print $h "PRIMA\n".$sql;
            
            if (_updateDataFromSQL($sql)){
                  my $sqlin = "INSERT INTO tbl_VarSetsValues ";
                      
                  $sqlin =  $sqlin.join( "\n UNION ALL " ,  map{ join(" " , "select", "'".$BundleName."_".$BundleCategory."_".$BundleSkel."' ," , "':BundleIncludePattern' , ", "'$_'" ) }  grep !/^\s*$/ , split(/[;\n]+/ , $ValuesI)); 
                  $sqlin = $sqlin."\n UNION ALL " if ($ValuesI && $ValuesE);
                  $sqlin =  $sqlin.join( "\n UNION ALL " , map{ join(" " , "select", "'".$BundleName."_".$BundleCategory."_".$BundleSkel."' ," , "':BundleExcludePattern' , ", "'$_'" )}  grep !/^\s*$/ , split(/[;\n]+/ , $ValuesE)); 
                                  
                print $h "DOPO_\n".Dumper($sqlin);
                print $h "\nEND";
                
            
                 main::debug2log("##################".Dumper($sqlin)."##################");
                 _updateDataFromSQL($sqlin);   
                
            }else {
                         die "Bundle item '$BundleName '_'$BundleCategory '_'$BundleSkel': unable to insert on db";
                    }
                 
            # else {
            #   die "Bundle item '$BundleName '_'$BundleCategory '_'$BundleSkel' already exists";
            #}

        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION BUNDLEUPD");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION BUNDLEUPD");
        }   
    } elsif($action =~ /del/i) {
        _updateDataFromSQL("BEGIN TRANSACTION BUNDLEDELETE");
        eval {
            my $sql = "";
            my $result = undef;
            main::debug2log("##################".Dumper($sql)."##################");
        $sql = "DELETE from tbl_BundlesNames where BundleKey =  '".$BundleName."_".$BundleCategory."_".$BundleSkel."' ;";
        $sql .= "DELETE from tbl_VarSetsValues where VarSetName = '".$BundleName."_".$BundleCategory."_".$BundleSkel."' ;";
                main::debug2log($sql);
        print $h "DELETE##########".$sql;
                _updateDataFromSQL($sql);
            
        };
        if($@) {
            $actionMessage = "FAILED: $@";
            _updateDataFromSQL("ROLLBACK TRANSACTION BUNDLEDELETE");
        } else {
            $actionMessage = "SUCCESS";
            _updateDataFromSQL("COMMIT TRANSACTION BUNDLEDELETE");
        }
    }
    
    close($h);
    if($actionMessage !~ /SUCCESS/) {
        my $respEntries = [
            {
                Columns => [
                    {colname => 'ActionMessage', content => $actionMessage}
                ]
            }
        ];
        $main::Response->Write(XReport::SOAP::buildResponse({
            IndexName => 'XREPORTWEB',
            IndexEntries => $respEntries
        }));
     } else {
        $selection = undef;
        push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
        main::debug2log("selection is ".Dumper($selection));
        getBundlesOper($method, $reqident, $selection);
     }
}

sub getServices{
    my ($method, $reqident, $selection) = (shift, shift, shift);
    my $actionMessage = "FAILED";
    my $whereClause = "WHERE (VarName LIKE 'SVC:%') ";

    my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
        my $field_name = $field;

        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
        $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;

        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
            delete($IndexEntries->[0]->{'Columns'}->{$field_name});
            next;
        }
        $field_name =~ s/^cmd_//;
        $whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
            if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
    }

    my $sql = "SELECT  TOP 1000 vv.VarSetName as ServiceGroup, SUBSTRING(vv.VarName, 5, len(vv.VarName)) AS ServiceName, vv.VarValue as Functions "
          ."FROM    tbl_VarSetsValues vv "
          
              ." $whereClause ";
    my $result = _getDataFromSQL($sql);
    if(!$result) {
        $actionMessage = "No definitions found!";
    } else {
        $actionMessage = "SUCCESS";
    }
    my $respEntries = [
        {
            Columns => [
                {colname => 'ActionMessage', content => $actionMessage}
            ]
        }
    ];
    push @{$respEntries}, @{$result} if($result);
#   main::debug2log(Dumper($respEntries));
    $main::Response->Write(XReport::SOAP::buildResponse({
        IndexName => 'XREPORTWEB',
        IndexEntries => $respEntries
    }));
}
1;
