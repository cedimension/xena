use strict;

sub _verifyRACFPassword {
  my ($UserName, $Password, $result, $newUserName) = (shift, shift, "FAILED", undef);
  my $racfhost = $main::Application->{'cfg.racfhost'};
  return "ERROR: RACF host name not defined" unless $racfhost;
 
  main::debug2log("Try login to $racfhost");
  my $ftp = Net::FTP->new($racfhost, Debug => 0);
  if ( !$ftp ) {
    $result = "ERROR: connection to RACF host failed, retry later";
  }
  else {
    my $ftprc = $ftp->login($UserName, $Password);
    $newUserName = ($ftprc ? (split(/\s/, $ftp->message()))[0] : undef);
    $result = ($newUserName ? "SUCCESS" : "ERROR: Login to RACF failed for $UserName - ".$ftp->message());
    $ftp->quit();
  }
  main::debug2log("LogonRACFUser - User:$newUserName; RACFUser:$UserName");
  
  return $result;
}

sub _verifyUser {
	my ($userName, $password, $applName) = (shift, shift, shift);

  	my $userSelect = 
		"SELECT a.UserName as UserName, b.ProfileName as ProfileName, c.UserAlias as UserId".
	        " FROM tbl_Users as a INNER JOIN tbl_UsersProfiles as b ON a.UserName = b.UserName".
		" INNER JOIN tbl_UserAliases AS c ON a.UserName = c.UserName".
		" WHERE (b.ProfileName IN ('SUPER', '".uc($applName)."')".
		" AND a.UserName = '".uc($userName)."')".
	" OR (b.ProfileName IN ('SUPER', '".uc($applName)."')".
		" AND c.UserAlias = '".uc($userName)."')"
	;
	main::debug2log("$userSelect");
  	my $dbr = XReport::DBUtil::dbExecuteReadOnly($userSelect);
	main::debug2log("SELECT END");
	return "ERROR: User $userName hasn't access to this XReport application" if($dbr->eof() or !$dbr);
	return "SUCCESS";
}

sub getUserConfig {
	my ($method, $reqident, $selection) = (shift, shift, shift);

	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	#main::debug2log(Dumper($IndexEntries));
	my $userName = $IndexEntries->[0]->{'Columns'}->{'UserName'}->{'content'};
	my $password = $IndexEntries->[0]->{'Columns'}->{'Password'}->{'content'};
	my $authMethod = $IndexEntries->[0]->{'Columns'}->{'AuthMethod'}->{'content'};
	my $applName = $IndexEntries->[0]->{'Columns'}->{'ApplName'}->{'content'};
	my $actionMessage = "FAILED";
	my $cfgFile = "n_o_t__f_o_u_n_d";
	my $cfgContent = "";
	main::debug2log("$method - Set UserName to $userName");
	main::debug2log("$method - Set Password to $password");
	main::debug2log("$method - Set Authentication method to $authMethod");
	main::debug2log("$method - Application name is $applName");

	$actionMessage = _verifyUser($userName, $password, $applName);
	main::debug2log("$method - _verifyUser return $actionMessage");
	if($authMethod =~ /RACF/ and $actionMessage =~ /SUCCESS/) {
		$actionMessage = _verifyRACFPassword($userName, $password);
		main::debug2log("$method - _verifyRACFPassword return $actionMessage");
	}

	if($actionMessage =~ /SUCCESS/) {
		$cfgFile = $main::Application->{XREPORT_SITECONF}."/".$userName."/xml/$applName.xml"  unless -e $cfgFile;
		$cfgFile = $main::Application->{XREPORT_SITECONF}."/".$main::Application->{ApplName}."/xml/$applName.xml"  unless -e $cfgFile;
		$cfgFile = $main::Application->{XREPORT_SITECONF}."/xml/$applName.xml" unless -e $cfgFile;
		main::debug2log("$method - Config file name is $cfgFile");

		my $cfgHandle = gensym();
		open($cfgHandle, "<$cfgFile") or die("Cannot open $cfgFile: $!");
		while(<$cfgHandle>) {
			$cfgContent .= $_;
		}
		close($cfgHandle);
		main::debug2log("$method - Config content is $cfgContent");

		my $buffsz = 57*76;
		$cfgContent = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $cfgContent ) );
	}

	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => [
			{
				Columns => [
					{colname => 'UserName', content => $userName},
					{colname => 'ActionMessage', content => $actionMessage}
				]
			}
		],
		DocumentType => 'text/xml',
		FileName => "$applName.xml",
		Identity => $applName,
		DocumentBody => {content => "<![CDATA[$cfgContent]]>"},
	}));
}

1;
