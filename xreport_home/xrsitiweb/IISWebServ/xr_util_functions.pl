use strict;
use Time::HiRes;

sub getHistoryReportsData {
	my ($method, $reqident, $selection) = (shift, shift, shift);
    
    my $actionMessage = "FAILED";
	my $whereClause = ""; my $whereClause1 = ""; my $whereClause2 = "";
	my $user = "";my $currentRow = 0;
    my $type ="";
	my ($IndexEntries, $IndexName, $TOP, $ORDERBY, $DISTINCT) = @{$selection->{request}}{qw(IndexEntries IndexName TOP ORDERBY DISTINCT)};
	$TOP = 1000 if($TOP =~ /^$/);
	main::debug2log(Dumper($selection->{request}));
	foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
		my $field_name = $field;

		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;
		#TESTSAN
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~  s/\?/\_/g ;	
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~  s/\*/\%/g ;	
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~  s/'//g ;	
		
		if($field_name =~ /^User$/){
			$user = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
			next;
		}
    	if($field_name =~ /^Ambiente$/){
			$type = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
			next;
		}
        if($field_name =~ /^CurrentRow$/){
			$currentRow = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
			next;
		}    
		if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/
			       	and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Min'} =~ /^$/
				and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Max'} =~ /^$/) {
			delete($IndexEntries->[0]->{'Columns'}->{$field_name});
			next;
		}
		#TESTSAN - remove JobNameSuffix - historycal search
		#if($field_name =~ /^JobName$/){
		#	$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/%[^%]+$/%/;	
		#}
		#$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date1$/) ? " Convert (varchar ,[$field_name] ,112) >=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
		#	if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
		#$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date2$/) ? " Convert (varchar ,[$field_name] ,112) <=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
		#	if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
			

		$whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) >= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} , "'", "AND")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} !~ /^\s*$/);

		$whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) <= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} , "'", "AND")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} !~ /^\s*$/);	
        main::debug2log("ECCO".Dumper($whereClause));
		
		if($field_name !~ /^Recipient$/)
		{
			$whereClause = _addWhereClause($whereClause, "$field LIKE ",$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'", "AND") 
					if($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} !~ /^date$/);
		}
		else
		{
			$whereClause2 = _addWhereClause($whereClause2, "$field LIKE ",$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'", "AND") 
					if($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} !~ /^date$/);
		}
        $whereClause1 = _addWhereClause($whereClause1, " Convert (varchar ,[".$field."1] ,112) >= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} , "'", "AND")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} !~ /^\s*$/);
		$whereClause1 = _addWhereClause($whereClause1, " Convert (varchar ,[".$field."1] ,112) <= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} , "'", "AND")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} !~ /^\s*$/);	

        $whereClause1 = _addWhereClause($whereClause1, $field."1 LIKE ",$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'", "AND") 
			    if(($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} !~ /^date$/) && ($field_name !~ /^Recipient$/));
        } 
		
    my $sql2 ="";  
    if(!(_checkRoot($user) != undef)){
		main::debug2log("USER_IS_NOT_ROOT");
            
        $sql2 = " inner join   tbl_profilesfolderstrees pft join tbl_usersprofiles up inner join tbl_useraliases ua on up.username = ua.username ".
               " on pft.profilename = up.profilename and rootnode <> 'root' and ua.useralias like '$user' on   B.user_name = pft.ProfileName ";   
	}

     my $prtvars = { ENV => $type,  WhereClause => $whereClause, WhereClause1 => $whereClause1 , WhereClause2 => $whereClause2, RootSql => $sql2 };
    my @sqlArray;
	foreach my $l (split /\n/, $XReport::cfg->{sqlprocs}->{HistoryReports}){
     
      $l =~  s/\#\{(.+?)\}\#/$prtvars->{$1}/g;
      push @sqlArray, $l;
    }  
    my $sqlStringNew = join "\n" , @sqlArray;
    ####da  questo momento si possono usare due query questa � quella configurata nel xml che ha stesso nome del web-service
	#es: "\\usnas710.intranet.unicredit.it\usxena$\xena\xreport_siteconf\A1\xml\XReportWebIface.xml"
    #main::debug2log("NUOVA QUERY \n $sqlStringNew");  
     my $sql = "select top $TOP ".
                "Restored, ReportEntryId, ReportName, Recipient, JobName, TimeRef, UserTimeElab, JobNumber, TotReports, DDName, UserTimeRef, TotPages ".
                ", totpages2, firstRBA, lastRBA, restore_id,".
                "( CASE  WHEN (MIN(Indexed) = MAX(Indexed)) THEN MAX(Indexed)  ELSE (MAX(Indexed)+';'+MIN(Indexed)) END ) as Indexed ".
                "from ( ".
                "select '' as Indexed, case when D.restore_id IS NULL  then 0 else 1 end as Restored ,  A.report_entry_id as ReportEntryId ".
                ",B.report_ext_name as ReportName  ".
                #",' ' as FolderDescr  ".
                ",B.user_name as Recipient ".
                ",C.job_name as JobName ".
                ",cidam_date_time as TimeRef ".
                ",cidam_date_time as UserTimeElab ".
                #",cidam_date_time as XFerStartTime ".
                #",cidam_file_name ".
                ",job_number as JobNumber ".
                ", '1' as TotReports  ".
                #",job_step_name ".
                #",proc_step_name ".
                #",step ".
                ",ddname as DDName".
                ",C.date_time as UserTimeRef ".
                #",C.tot_pages as Pagine ".
                #",C.tot_lines as Linee ".
                ", B.report_tot_pages as   TotPages ".
                ", C.tot_pages       AS totpages2 ".
                ", C.first_rba	     AS firstRBA ".
                ", C.last_rba	     AS lastRBA ".
                ", D.restore_id      AS restore_id ".                
                "from [ctd2xreport].[dbo].[tbl_ctd_report_ranges_$type] A INNER JOIN [ctd2xreport].[dbo].[tbl_ctd_cidam_entries_$type] C ON  A.cidam_id = C.cidam_id  ".
                "INNER JOIN [ctd2xreport].[dbo].[tbl_ctd_report_entries_$type] B ON B.report_entry_id = A.report_entry_id ".
                " LEFT OUTER JOIN [ctd2xreport].[dbo].[tbl_ctd_restore_entries_$type] D on D.file_name = C.cidam_file_name and  D.file_type = 'cidam'".
                $sql2 .
                "union all ".
                "select case when E.index_name IS NOT NULL  then lower(E.index_name+'-'+E.key_fields) end as Indexed, case when D.restore_id IS NULL  then 0 else 1 end as Restored , 0 as ReportEntryId, E.report_name as ReportName, '' as Recipient, C.job_name as JobName, C.cidam_date_time as TimeRef ,cidam_date_time as UserTimeElab , C.job_number as JobNumber, '1' as TotReports, ddname as DDName,C.date_time as UserTimeRef , E.tot_pages as TotPages, ".
                " E.tot_pages       AS TotPages2 , E.first_rba AS firstRBA, E.last_rba        AS lastRBA, D.restore_id      AS restore_id ".
                "from [ctd2xreport].[dbo].[tbl_ctd_cidam_entries_$type] C JOIN [ctd2xreport].[dbo].[tbl_ctd_index_entries_$type] E  ".
                "on C.cidam_id = E.index_id  ".
                " LEFT OUTER JOIN [ctd2xreport].[dbo].[tbl_ctd_restore_entries_$type] D on D.file_name = E.index_file_name and D.file_type = 'index' ".
                "and (D.es_status is Null or  D.es_status = 1 or (D.es_status = 0  and not exists(select 1 from  [ctd2xreport].[dbo].[tbl_ctd_restore_entries_A1] int where int.file_id = D.file_id and int.es_status = 1)  )) ".
                ") TAB ".
                $whereClause .
                "group by Restored, ReportEntryId, ReportName, Recipient, JobName, TimeRef, UserTimeElab, JobNumber, TotReports, DDName, UserTimeRef, TotPages, totpages2, firstRBA,lastRBA, restore_id ".
                "order by TAB.UserTimeRef desc ";
	
 
   	main::debug2log("ECCO_QUERY_STORICO_NEW".Dumper($sqlStringNew)."_".__FILE__."::".__LINE__); 
   	#main::debug2log("ECCO_QUERY_STORICO_OLD".Dumper($sql));   
    my $result = _getDataFromSQL($sqlStringNew);

	if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
    }            
    my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	main::debug2log("----------------------------------------------------------".Dumper(@{$result}[1]));
	push @{$respEntries}, @{$result} if($result);
#	my $h = gensym();
#	open($h, ">D:/out.log") or die("open error: $!");
#	print $h XReport::SOAP::buildResponse({
#		IndexName => 'XREPORTWEB',
#		IndexEntries => $respEntries
#	});
#	close($h);

	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries
	}));

}

sub getReportsDataOld {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $whereClause = "";
	my $user = "";
    my $count = 0;
	my ($IndexEntries, $IndexName, $TOP, $ORDERBY, $DISTINCT) = @{$selection->{request}}{qw(IndexEntries IndexName TOP ORDERBY DISTINCT)};
	$TOP = 3000 if($TOP =~ /^$/);
	main::debug2log(Dumper($selection->{request}));
	foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
		my $field_name = $field;

		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;
		if($field_name =~ /^User$/){
			$user = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
			next;
		}	
		if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/
			       	and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Min'} =~ /^$/
				and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Max'} =~ /^$/) {
			delete($IndexEntries->[0]->{'Columns'}->{$field_name});
			next;
		}
		#$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date1$/) ? " Convert (varchar ,[$field_name] ,112) >=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
		#	if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
		#$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date2$/) ? " Convert (varchar ,[$field_name] ,112) <=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
		#	if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
			
		$whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) >= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} , "'", "AND")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} !~ /^\s*$/);
		$whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) <= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} , "'", "AND")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} !~ /^\s*$/);	
        main::debug2log("ECCO".Dumper($whereClause));
        if ($field =~ /^RepName$/)
        {
              if ($whereClause =~ /^\s*$/)
              {
                $whereClause = " where ";
              }
              else
              {
                 $whereClause .= " AND ";
              }
             	$whereClause .= "(LR.UserRef like '".$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}."' OR JR.UserRef like '".$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}."')";
        }    
        else 
        {
              $whereClause = _addWhereClause($whereClause, "$field LIKE ",$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'", "AND") 
			    if($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} !~ /^date$/);
        } 
	}	
		

	my $sql = "BEGIN WITH Reports AS(".
        "select folderName, FolderDescr AS FolderDescr, JR.JobReportId as JobReport_ID ".
		"  ,MIN(LR.ReportId) As ReportId".
        " ,JR.XferStartTime As TimeRef".
        " ,COALESCE(LR.UserTimeRef, JR.UserTimeRef, JR.XferStartTime) As UserTimeRef".
        " ,COALESCE(JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime) As UserTimeElab".
		#"  ,CONVERT(varchar, JR.XferStartTime, 120) As TimeRef".
		#"  ,COALESCE(CONVERT(varchar,LR.UserTimeRef,103), CONVERT(varchar,JR.UserTimeRef,103)) As UserTimeRef".
		#"  ,CONVERT(varchar, JR.UserTimeElab , 103) As UserTimeElab".
		"  ,(CONVERT(varchar, JR.XferStartTime, 103) + SUBSTRING(CONVERT(varchar, JR.XferStartTime, 121),11,6)) As XferStartTime".
		"  ,COUNT(*) As TotReports".
		"  ,SUM(LR.TotPages) As TotPages".
		"  ,JR.JobName".
		"  ,JR.JobNumber".
        "  ,COALESCE(LR.UserRef, JR.UserRef) AS ReportName ".
		"  ,JR.FileRangesVar".
		"  ,JR.ExistTypeMap".
		"  ,1 As IsGRANTED".
        "  ,CASE WHEN charindex('\\',reverse(FolderName)) > 0 THEN (right([FolderName],charindex('\\',reverse(FolderName))-(1))) ELSE FolderName END AS Recipient".
		" from (".
		" SELECT fn1.FolderName AS FolderName, FolderDescr AS FolderDescr ".
		"			   , rn1.ReportName as ReportName ".
		"               ,rn1.FilterVar AS FilterVar ".
		"               ,vv1.VarName AS VarName, vv1.varValue AS VarValue".
		"               ,nm1.FilterRule AS FilterRule, nm1.RecipientRule AS RecipientRule".
		"               ,lr1.JobReportID as JobReportID, lr1.ReportId as ReportID, lr1.TotPages As TotPages, ".
		"		lrr1.userTimeRef AS UserTimeRef,    lrt1.textString AS UserRef ".
	       	"".
		"               from tbl_Folders fn1".
		"				INNER JOIN dbo.tbl_FoldersRules fr1 ON fn1.FolderName = fr1.FolderName".
		"				INNER JOIN dbo.tbl_NamedReportsGroups nm1 ON fr1.ReportGroupId = nm1.ReportGroupId ".
		"				and nm1.FilterRule <> ''  and nm1.ReportRule <> ''".
		"				INNER JOIN tbl_VarSetsValues vr1 ON nm1.ReportRule = vr1.VarSetName and vr1.VarName = ':REPORTNAME' ".
		"				INNER JOIN tbl_ReportNames rn1 ON  rn1.ReportName = vr1.VarValue ".
		"					and nm1.FilterRule <> ''  and nm1.ReportRule <> ''".
		"				INNER JOIN tbl_LogicalReports lr1 ON lr1.ReportName = rn1.ReportName".
		"  INNER JOIN tbl_LogicalReportsRefs lrr1 ON lr1.JobReportId = lrr1.JobReportId AND lr1.ReportId = lrr1.ReportId ".
		"  INNER JOIN tbl_VarSetsValues vv1 ON vv1.VarSetName = nm1.FilterRule and vv1.VarName = lrr1.FilterVar ".
		"  LEFT OUTER JOIN tbl_LogicalReportsTexts lrt1 ON lrr1.textId = lrt1.textId ".
		"	and lr1.XferRecipient = nm1.RecipientRule".
		"	and lr1.FilterValue = vv1.VarValue ".
		"union all ".
		"SELECT fn2.FolderName AS FolderName, FolderDescr AS FolderDescr ".
		"			   , rn2.ReportName as ReportName ".
		"               ,rn2.FilterVar AS FilterVar ".
		"               ,'' AS VarName, '' AS VarValue ".
		"               ,nm2.FilterRule AS FilterRule, nm2.RecipientRule AS RecipientRule ".
		"               ,lr2.JobReportID as JobReportID, lr2.ReportId as ReportID, lr2.TotPages As TotPages, ".
		"		lrr2.userTimeRef AS UserTimeRef,    lrt2.textString AS UserRef ".
		"from tbl_Folders fn2 ".
		"	INNER JOIN dbo.tbl_FoldersRules fr2 ON fn2.FolderName = fr2.FolderName ".
		"	INNER JOIN dbo.tbl_NamedReportsGroups nm2 ON fr2.ReportGroupId = nm2.ReportGroupId and nm2.FilterRule = ''  and nm2.ReportRule <> '' ".
		"	INNER JOIN tbl_VarSetsValues vr2 ON nm2.ReportRule = vr2.VarSetName and vr2.VarName = ':REPORTNAME' ".
		"       INNER JOIN tbl_ReportNames rn2 ON  rn2.ReportName = vr2.VarValue ".
		"       INNER JOIN tbl_LogicalReports lr2 ON lr2.ReportName = rn2.ReportName ".
		"       INNER JOIN tbl_LogicalReportsRefs lrr2 ON lr2.JobReportId = lrr2.JobReportId AND lr2.ReportId = lrr2.ReportId ".
		"       LEFT OUTER JOIN tbl_LogicalReportsTexts lrt2 ON lrr2.textId = lrt2.textId".
		"               and lr2.XferRecipient = nm2.RecipientRule ".
		"UNION ALL ".
		"SELECT fn3.FolderName AS FolderName, FolderDescr AS FolderDescr ".
		"               ,rn3.ReportName as ReportName ".
		"               ,rn3.FilterVar AS FilterVar ".
		"               ,'' AS VarName, '' AS VarValue ".
		"               ,nm3.FilterRule AS FilterRule, nm3.RecipientRule AS RecipientRule ".
		"               ,lr3.JobReportID as JobReportID, lr3.ReportId as ReportID, lr3.TotPages As TotPages, ".
		"		lrr3.userTimeRef AS UserTimeRef,    lrt3.textString AS UserRef ".
		"from tbl_Folders fn3 ".
		"	INNER JOIN dbo.tbl_FoldersRules fr3 ON fn3.FolderName = fr3.FolderName ".
		"	INNER JOIN dbo.tbl_NamedReportsGroups nm3 ON fr3.ReportGroupId = nm3.ReportGroupId and nm3.ReportRule <> '' ".
		"	INNER JOIN tbl_VarSetsValues vr3 ON nm3.ReportRule = vr3.VarSetName and vr3.VarName = ':REPORTNAME' ".
		"       INNER JOIN tbl_ReportNames rn3 ON  rn3.ReportName = vr3.VarValue ".
		"       INNER JOIN tbl_LogicalReports lr3 ON lr3.ReportName = rn3.ReportName ".
		"       INNER JOIN tbl_LogicalReportsRefs lrr3 ON lr3.JobReportId = lrr3.JobReportId AND lr3.ReportId = lrr3.ReportId ".  
                "	LEFT OUTER JOIN tbl_LogicalReportsTexts lrt3 ON lrr3.textId = lrt3.textId ".
		"               and lr3.XferRecipient = nm3.RecipientRule ".
		
		") LR INNER JOIN tbl_JobReports JR ON JR.JobReportId = LR.JobReportid ".
		#" $whereClause".
		"  GROUP BY FolderName, LR.ReportName, JR.JobReportId, XferStartTime, LR.UserTimeRef, JR.UserTimeRef".
        " ,JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime".
		" ,JobName, JobNumber, LR.UserRef, JR.UserRef, FileRangesVar, existTypeMap, FolderDescr ) ".
        "SELECT * FROM Reports".
        " $whereClause ".
        "END";
    my $sql3 = "SELECT * from dbo.vw_reportsSmartTestToRecipient $whereClause "; 
	main::debug2log($sql3."and__".$user);
	my $outResult;
  	my $result = _getDataFromSQL($sql3);
	#main::debug2log("ECCO_QUELLO_CHE_MI_SERVE".Dumper($result));
	if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
	    if(_checkRoot($user) != undef){
		    main::debug2log("USER_IS_ROOT");
            if(scalar @{$result}  > 1000){
              my @intermedio = @{$result}[1..1000]; 
              $outResult = \@intermedio;
            }  
            else {
               $outResult = $result;
            }   
	    }else{
		  main::debug2log("user_not_is_root");  
          my $sql4 = "select distinct rootnode from tbl_profilesfolderstrees pft join tbl_usersprofiles up inner join tbl_useraliases ua on up.username = ua.username ".
						 "on pft.profilename = up.profilename and rootnode <> 'root' ".
                         "and ua.useralias = '$user' ";
          my $result4 = _getDataFromSQLNoFill($sql4); my @profiles;
          while($result4 != undef  && !$result4->eof()){
               push @profiles , $result4->Fields()->item('rootnode')->value();
               $result4->MoveNext();
               main::debug2log("rrrrrrrrrrrrrr----------------------------------------------------------".Dumper(@profiles));
          }
          main::debug2log("num:righe".Dumper(scalar(@{$result})));
          foreach my $line (@{$result}) {
            last if ($count >= 1000);    
            main::debug2log("rrrrrrrrrrrrrr----------------------------------------------------------".Dumper($line));
            #main::debug2log("riga_numeroarray__".dumper($line->[0]));
            
            if($line->{Columns}->[0]){
                #main::debug2log("colonna__".dumper($line->{'columns'}));
                my $folder1 = (grep { $_->{'colname'} =~ /foldername/i } @{$line->{Columns}})[0]->{content};
                main::debug2log("folder__".Dumper($folder1));
                
                #foreach my $profile (@profiles){
                #main::debug2log("profile".Dumper($profile));
                if((grep {$folder1 =~ /^\Q$_\E/i} @profiles)[0]  !~ /^$/){
                  #$profile = s/\\/\\\\/g;
                  
                   #if($folder1 =~/^\Q$profile\E/i){
                     main::debug2log("$count####################-------------------------------".Dumper($_)) if ($count % 100 == 0) ;
                     $count++;
					 push (@$outResult,$line);
					 next; 
                 }
                 #} 
                 # } 
            }
            } 
	     } 
	}
	

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];

	main::debug2log("@@@@@@@@@@@@@@@@@@------------------------------------------------------".Dumper(@{$outResult}[1])) if ref($outResult) eq "ARRAY";
	push @{$respEntries}, @{$outResult} if($outResult);
#	my $h = gensym();
#	open($h, ">D:/out.log") or die("open error: $!");
#	print $h XReport::SOAP::buildResponse({
#		IndexName => 'XREPORTWEB',
#		IndexEntries => $respEntries
#	});
#	close($h);

	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries
	}));
}

sub getOldReportsData {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $whereClause = "";
	my $user = ""; my $currentRow = 0;
    my $count = 0;
	my ($IndexEntries, $IndexName, $TOP, $ORDERBY, $DISTINCT) = @{$selection->{request}}{qw(IndexEntries IndexName TOP ORDERBY DISTINCT)};
	$TOP = 3000 if($TOP =~ /^$/);
	main::debug2log(Dumper($selection->{request}));
	foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
		my $field_name = $field;

		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;
		if($field_name =~ /^User$/){
			$user = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
			next;
		}
        if($field_name =~ /^CurrentRow$/){
			$currentRow = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
			next;
		}	
		if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/
			       	and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Min'} =~ /^$/
				and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Max'} =~ /^$/) {
			delete($IndexEntries->[0]->{'Columns'}->{$field_name});
			next;
		}
		#$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date1$/) ? " Convert (varchar ,[$field_name] ,112) >=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
		#	if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
		#$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date2$/) ? " Convert (varchar ,[$field_name] ,112) <=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
		#	if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
			
		$whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) >= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} , "'", "AND")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} !~ /^\s*$/);
		$whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) <= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} , "'", "AND")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} !~ /^\s*$/);	
        main::debug2log("ECCO".Dumper($whereClause));
        if ($field =~ /^RepName$/)
        {
              if ($whereClause =~ /^\s*$/)
              {
                $whereClause = " where ";
              }
              else
              {
                 $whereClause .= " AND ";
              }
             	$whereClause .= "(LR.UserRef like '".$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}."' OR JR.UserRef like '".$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}."')";
        }    
        else 
        {
              $whereClause = _addWhereClause($whereClause, "$field LIKE ",$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'", "AND") 
			    if($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} !~ /^date$/);
        } 
	}	
	
    my $sql4 = " inner join tbl_profilesfolderstrees pft join tbl_usersprofiles up inner join tbl_useraliases ua on up.username = ua.username  on pft.profilename = up.profilename and rootnode <> 'root' 
                         and ua.useralias like '$user'   on SubString(FolderName, 1, len(pft.rootNode) ) = pft.rootNode ";
        
    if(!(_checkRoot($user) != undef)){
		    main::debug2log("USER_IS_NOT_ROOT");
            
         $whereClause = $sql4.$whereClause." ";   
	}
    my $sql3 = "SELECT TOP 1000 * FROM ".
               "    ( SELECT ROW_NUMBER() OVER (ORDER BY UserTimeRef) AS ROW,  B.* ".
               "         FROM dbo.vw_reportsSmartTestToRecipient B $whereClause ) T ".
               "WHERE ROW > $currentRow ORDER BY RECIPIENT"; 
   	main::debug2log($sql3."and__".$user);   
  	my $result = _getDataFromSQL($sql3);
	#main::debug2log("ECCO_QUELLO_CHE_MI_SERVE".Dumper($result));
    my $outResult;
	if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
	    
        if(scalar @{$result}  > 1000){
         my @intermedio = @{$result}[1..1000]; 
             $outResult = \@intermedio;
        } else {
            $outResult = $result;
        }  
	
    }
	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];

	main::debug2log("@@@@@@@@@@@@@@@@@@------------------------------------------------------".Dumper(@{$outResult}[1])) if ref($outResult) eq "ARRAY";
	push @{$respEntries}, @{$outResult} if($outResult);
#	my $h = gensym();
#	open($h, ">D:/out.log") or die("open error: $!");
#	print $h XReport::SOAP::buildResponse({
#		IndexName => 'XREPORTWEB',
#		IndexEntries => $respEntries
#	});
#	close($h);

	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries
	}));
}

sub getReportsData {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $whereClause = "";
	my ($UserTimeRefMin, $UserTimeRefMax ) = ('0000-00-00 00:00:00.000','9999-99-99 99:99:99.999');
	my $JobNumber = "%";
	my $JobNumberCondition ="";
	my $Recipient = "%";
	my $RecipientCondition ="";
	my $ReportName = "%";
	my $ReportNameCondition ="";
	
	my $user = ""; my $currentRow = 0; my $jobName = "";
    my $count = 0;
	my ($IndexEntries, $IndexName, $TOP, $ORDERBY, $DISTINCT) = @{$selection->{request}}{qw(IndexEntries IndexName TOP ORDERBY DISTINCT)};
    main::SOAPError("$method", "Invoke", "Empty params request ", 500) && return unless($IndexEntries);
	$TOP = 3000 if($TOP =~ /^$/);
	main::debug2log("UFFA".Dumper($IndexEntries));
	foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
		my $field_name = $field;

		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;
		#TESTSAN
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\?/\_/g ;	
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\*/\%/g ;	
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/'/\%/g ;	
		if($field_name =~ /^User$/){
			$user = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
			next;
		}
        if($field_name =~ /^JobName$/){
			$jobName = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
			next;
        }
        if($field_name =~ /^CurrentRow$/){
			$currentRow = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
			next;
		}	
		if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/
			       	and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Min'} =~ /^$/
				and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Max'} =~ /^$/) {
			delete($IndexEntries->[0]->{'Columns'}->{$field_name});
			next;
		}
		#$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date1$/) ? " Convert (varchar ,[$field_name] ,112) >=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
		#	if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
		#$whereClause = _addWhereClause($whereClause, ($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} =~ /^date2$/) ? " Convert (varchar ,[$field_name] ,112) <=  " : "  [$field_name] = " , $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
		#	if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
			
		$whereClause = _addWhereClause($whereClause, " Convert (varchar ,".$field." ,112) >= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} , "'", "AND")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} !~ /^\s*$/);
		$whereClause = _addWhereClause($whereClause, " Convert (varchar ,".$field." ,112) <= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} , "'", "AND")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} !~ /^\s*$/);	
        main::debug2log("ECCO".Dumper($whereClause));
        if ($field =~ /^RepName$/)
        {
              if ($whereClause =~ /^\s*$/)
              {
                $whereClause = " where ";
              }
              else
              {
                 $whereClause .= " AND ";
              }
             	$whereClause .= "(LR.UserRef like '".$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}."' OR JR.UserRef like '".$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}."')";
              #$whereClause .= "(B.UserRef1 like '".$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}."')";
        }    
        else 
        {
			if ($field =~ /^UserTimeRef$/) {
				if($IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} =~ /^(\d{4})(\d{2})(\d{2})$/) {
					$UserTimeRefMax = "$1-$2-$3 23:59:59.999";
				}
				if($IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} =~ /^(\d{4})(\d{2})(\d{2})$/) {
					$UserTimeRefMin = "$1-$2-$3 00:00:00.000";
				}
			}
			#TRIM
			#$IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ s/(?:^\s*|\s*$)//;
			
			if ($field =~ /^JobNumber$/) { 
				if(($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /^job(\d+)\%*$/i ) 
				or 				
				($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /^(\d+)\%*$/i )) 
				{
					$JobNumber = 'JOB'.$1;				
					$JobNumberCondition = "        AND JobNumber like '%$JobNumber%' \n";
				}
			}
			if ($field =~ /^Recipient$/) { 
			    #$IndexEntries->[0]->{'Columns'}->{$field}->{'content'} = '%'.$1.'%' if ($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /^[^0-9]{2}([0-9]+)/);
				if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /^(.+)$/ ) {
					$Recipient = $1;				
					$RecipientCondition = "        AND B.Recipient like '%$Recipient%' \n";
				}
                delete($IndexEntries->[0]->{'Columns'}->{$field});
			}
			if ($field =~ /^ReportName$/) { 
				if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /^(.+)$/ ) {
					$ReportName = '%'.$1.'%';				
					$ReportNameCondition = "        AND COALESCE(lrt1.textString, JR.UserRef) like '$ReportName' \n";
				}
			}
			
			$whereClause = _addWhereClause($whereClause, "".$field." LIKE ",$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'", "AND") 
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} !~ /^date$/);
        } 
	}	
	
	#TESTSAN - remove JobNameSuffix - online search
	#$jobName =~ s/^(.+%)[^%]+$/$1/ if $user =~ /^X1NOPR$/;
	# "%C0" -> "______C0" 
	$jobName =~ s/^%(.{2})$/______$1/ ;
	$jobName =~ s/^(.{6})%(.*)$/$1$2/ ;
	$jobName =~ s/^(.{8}).*$/$1/ ;
	$jobName = '%' if ($jobName eq "");
	
    #my $sql4 =  " like '#{Recipient}#'";
    my $sql4 =  " ";
    if(!(_checkRoot($user) != undef)){
		    main::debug2log("USER_IS_NOT_ROOT");
            
          $sql4 =  " and fn1.FolderName in (select FolderName from tbl_Folders fo with (NOLOCK) ".
            "      inner join tbl_ProfilesFoldersTrees pt with (NOLOCK) on fo.FolderName like pt.RootNode+'%'".
            "       inner join tbl_UsersProfiles up with (NOLOCK) on up.ProfileName = pt.ProfileName and rootnode <> 'root'  where UserName like '$user'".
            #"       inner join tbl_UsersProfiles up with (NOLOCK) on up.ProfileName = pt.ProfileName and rootnode <> 'root'  where UserName like '#{User}#'".
            "    )";   
	}
	
	my $timestamp = Time::HiRes::time; 
	my $TempOriginalTable = '#TempOriginalTable'.$ENV{'COMPUTERNAME'}.$timestamp;
	$TempOriginalTable =~ s/[^#a-zA-Z0-9]//g;  
 
	my $sql6preStat = "IF OBJECT_ID('tempdb.dbo.".$TempOriginalTable."', 'U') IS NOT NULL DROP TABLE ".$TempOriginalTable.";\n". 
    "CREATE TABLE ".$TempOriginalTable."(            \n".
    "FolderName varchar(128) NULL,                   \n".
    "FolderDescr varchar(128) NULL,                  \n".
    "JobReport_ID int NULL,                          \n".
    "ReportId smallint NULL,                         \n".
    "TimeRef datetime NULL,                          \n".
    "UserTimeRef datetime NULL,                      \n".
    "UserTimeElab datetime NULL,                     \n".
    "XferStartTime varchar(36) NULL,                 \n".
    "TotReports int NULL,                            \n".
    "TotPages int NULL,                              \n".
    "ListOfPages varchar(500) NULL,                  \n".
    "JobNameOrig varchar(64) NULL,                   \n".
    "JobNumber varchar(16) NULL,                     \n".
    "JobReportDescr varchar(1) NULL,                 \n".
    "HasIndexes bit NULL,                            \n".
    "JobName varchar(8) NULL,                        \n".
    "ReportName varchar(128) NULL,                   \n".
    "FileRangesVar varchar(64) NULL,                 \n".
    "ExistTypeMap int NULL,                          \n".
    "IsGRANTED int NULL,                             \n".
    "Recipient varchar(128) NULL,                    \n".
    "CheckStatus bit NULL,                           \n".
    "LastUsedToCheck varchar(32) NULL,               \n".
    "CheckFlag bit NULL                              \n".
    ");                                              \n".
	"INSERT INTO ".$TempOriginalTable."            \n".
    "SELECT * FROM (select fn1.FolderName, FolderDescr AS FolderDescr, JR.JobReportId as JobReport_ID, lr1.ReportId As ReportId  , JR.XferStartTime As TimeRef  , COALESCE(lrr1.UserTimeRef, JR.UserTimeRef, JR.XferStartTime) As UserTimeRef, COALESCE(JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime) As UserTimeElab  , (CONVERT(varchar, JR.XferStartTime, 103) + SUBSTRING(CONVERT(varchar, JR.XferStartTime, 121),11,6)) As XferStartTime  ,\n".
#    my $sql6preStat = "IF OBJECT_ID('tempdb.dbo.#TempOriginalTable', 'U') IS NOT NULL DROP TABLE #TempOriginalTable;\n\n".
#    "SELECT * INTO #TempOriginalTable FROM (select fn1.FolderName, FolderDescr AS FolderDescr, JR.JobReportId as JobReport_ID, lr1.ReportId As ReportId  , JR.XferStartTime As TimeRef  , COALESCE(lrr1.UserTimeRef, JR.UserTimeRef, JR.XferStartTime) As UserTimeRef, COALESCE(JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime) As UserTimeElab  , (CONVERT(varchar, JR.XferStartTime, 103) + SUBSTRING(CONVERT(varchar, JR.XferStartTime, 121),11,6)) As XferStartTime  ,\n".
    "    1 As TotReports  , sum(lr1.TotPages) As TotPages  , CONVERT(varchar(500), pr1.ListOfPages) as ListOfPages,\n".
    "      JR.JobName AS JobNameOrig ,JR.JobNumber  , '' as JobReportDescr, JR.HasIndexes, \n".
    #" substring(JobName,1,8) as JobName, \n".
	" substring(JR.jobreportname,1,8) as JobName, \n".
    "    COALESCE(lrt1.textString, JR.UserRef) AS ReportName   ,JR.FileRangesVar \n".
    "      ,JR.ExistTypeMap  ,1 As IsGRANTED,\n".
    "    CASE WHEN charindex('\\',reverse(fn1.FolderName)) > 0 THEN (right(fn1.FolderName,\n".
    "    charindex('\\',reverse(fn1.FolderName))-(1))) ELSE fn1.FolderName END AS Recipient, lr1.CheckStatus, lr1.LastUsedToCheck, RN.CheckFlag\n".

    "       from tbl_Folders fn1\n".
    "       INNER JOIN dbo.tbl_FoldersRules fr1 ON fn1.FolderName = fr1.FolderName  \n".
	$RecipientCondition.
    
    $sql4.
    "       INNER JOIN dbo.tbl_NamedReportsGroups nm1 ON fr1.ReportGroupId = nm1.ReportGroupId\n".
    "       INNER JOIN tbl_VarSetsValues vr1 ON nm1.ReportRule = vr1.VarSetName\n".
    "       INNER JOIN tbl_VarSetsValues vv1 ON vv1.VarSetName = nm1.FilterRule\n".
    "        and vv1.VarName = nm1.FilterVar\n".
    "          \n". 
#    "        INNER JOIN (Select * from tbl_JobReports where  JobName LIKE  '$jobName%' \n".
#    "        INNER JOIN (Select * from tbl_JobReports where  substring(JobName,1,8)  LIKE  '$jobName' \n".
    "        INNER JOIN (Select * from tbl_JobReports where JobReportName <> 'CDAMFILE' and substring(jobreportname,1,8)  LIKE  '$jobName' \n".
	$JobNumberCondition.
	#"        AND JobNumber like '$JobNumber' \n".
	"        AND PendingOp <> 12             \n".
	"        AND COALESCE(UserTimeElab, JobExecutionTime, ElabStartTime)  between '$UserTimeRefMin' and '$UserTimeRefMax') JR\n".
    "        ON JR.JobReportName like vr1.VarValue\n".
    "        INNER JOIN tbl_LogicalReports lr1 ON lr1.JobReportId = \n".
    "    JR.JobReportId and lr1.XferRecipient = nm1.RecipientRule       and\n".
    "        lr1.FilterValue = vv1.VarValue 	 INNER JOIN tbl_LogicalReportsRefs lrr1\n".
    "       ON lr1.JobReportId = lrr1.JobReportId AND lrr1.ReportId = lr1.ReportId  and lrr1.FilterVar = vv1.VarName\n".
    "        INNER JOIN tbl_LogicalReportsTexts lrt1 ON lrr1.textId = lrt1.textId\n".
    "        INNER JOIN dbo.tbl_ReportNames RN ON RN.ReportName = lr1.ReportName\n".
    "        INNER JOIN tbl_PhysicalReports pr1 ON pr1.JobReportId = lr1.JobReportId and pr1.ReportId = lr1.ReportId\n".
    "        where JR.Status = 18 and vr1.VarName = ':REPORTNAME'\n". 
	$ReportNameCondition.
    #"    GROUP BY fn1.FolderName, lr1.ReportName, JR.JobReportId, lr1.ReportID, XferStartTime, COALESCE(lrr1.UserTimeRef, JR.UserTimeRef, JR.XferStartTime), convert(varchar(500), pr1.ListOfPages) , JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime, JR.UserTimeRef, UserTimeElab ,JobName, JobNumber, JR.HasIndexes, lrt1.textString, JR.UserRef, FileRangesVar, existTypeMap,\n".
    "    GROUP BY JR.jobreportname, fn1.FolderName, lr1.ReportName, JR.JobReportId, lr1.ReportID, XferStartTime, COALESCE(lrr1.UserTimeRef, JR.UserTimeRef, JR.XferStartTime), convert(varchar(500), pr1.ListOfPages) , JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime, JR.UserTimeRef, UserTimeElab ,JobName, JobNumber, JR.HasIndexes, lrt1.textString, JR.UserRef, FileRangesVar, existTypeMap,\n".
	"      FolderDescr, lr1.CheckStatus, lr1.LastUsedToCheck, RN.CheckFlag\n".
    "    ) XXX;\n";

    $sql6preStat =~ s/#{([^\{]+)}#/$IndexEntries->[0]->{'Columns'}->{$1}->{'content'}/g;

	my $sql6 = 
	" select top 1000 * from (SELECT ROW_NUMBER() OVER (ORDER BY UserTimeRef) AS ROW , B.* from\n".
    " (select OT.foldername,OT.FolderDescr,OT.JobReport_ID,OT.ReportId,OT.TimeRef,OT.UserTimeRef,OT.UserTimeElab,OT.XferStartTime,OT.TotReports,OT.TotPages,OT.ListOfPages,OT.JobNameOrig,OT.jobnumber,OT.JobReportDescr,OT.hasindexes,OT.JobName,OT.ReportName,OT.filerangesvar,OT.existtypemap,OT.IsGRANTED,OT.Recipient,OT.checkstatus,OT.lastusedtocheck,OT.checkflag from \n".
    " (select foldername,FolderDescr,JobReport_ID,TimeRef,UserTimeRef,UserTimeElab,XferStartTime,TotReports,TotPages,ListOfPages,JobNameOrig,jobnumber,JobReportDescr,hasindexes,JobName,filerangesvar,existtypemap,IsGRANTED,Recipient,checkstatus,lastusedtocheck,checkflag, max(ReportId) as maxReportId\n".
#    "  from #TempOriginalTable\n".
    "  from ".$TempOriginalTable."\n".
    "  group by foldername,FolderDescr,JobReport_ID,TimeRef,UserTimeRef,UserTimeElab,XferStartTime,TotReports,TotPages,ListOfPages,JobNameOrig,jobnumber,JobReportDescr,hasindexes,JobName,filerangesvar,existtypemap,IsGRANTED,Recipient,checkstatus,lastusedtocheck,checkflag) GroupedTable\n".
#    "  INNER JOIN #TempOriginalTable OT\n".
    "  INNER JOIN ".$TempOriginalTable." OT\n".
    "  ON GroupedTable.JobReport_ID = OT.JobReport_ID\n".
    "  AND GroupedTable.Recipient = OT.Recipient\n".
    "  AND GroupedTable.foldername = OT.foldername\n".
    "  AND GroupedTable.maxReportId = OT.ReportId\n".
    "  ) B\n".
    "    $whereClause) T WHERE ROW > 0 ORDER BY $ORDERBY ";
#TESTSAN 14/03/2016
#    my $sql6 = ";WITH OriginalTable AS (select fn1.FolderName, FolderDescr AS FolderDescr, JR.JobReportId as JobReport_ID, lr1.ReportId As ReportId  , JR.XferStartTime As TimeRef  , COALESCE(lrr1.UserTimeRef, JR.UserTimeRef, JR.XferStartTime) As UserTimeRef, COALESCE(JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime) As UserTimeElab  , (CONVERT(varchar, JR.XferStartTime, 103) + SUBSTRING(CONVERT(varchar, JR.XferStartTime, 121),11,6)) As XferStartTime  ,".
#    "    1 As TotReports  , sum(lr1.TotPages) As TotPages  , CONVERT(varchar(500), pr1.ListOfPages) as ListOfPages,\n".
#    "      JR.JobName AS JobNameOrig ,JR.JobNumber  , '' as JobReportDescr, JR.HasIndexes, ".
#    " substring(JobName,1,8) as JobName, ".
#    "    COALESCE(lrt1.textString, JR.UserRef) AS ReportName   ,JR.FileRangesVar ".
#    "      ,JR.ExistTypeMap  ,1 As IsGRANTED,".
#    "    CASE WHEN charindex('\\',reverse(fn1.FolderName)) > 0 THEN (right(fn1.FolderName,".
#    "    charindex('\\',reverse(fn1.FolderName))-(1))) ELSE fn1.FolderName END AS Recipient, lr1.CheckStatus, lr1.LastUsedToCheck, RN.CheckFlag".
#
#    "       from tbl_Folders fn1".
#    "       INNER JOIN dbo.tbl_FoldersRules fr1 ON fn1.FolderName = fr1.FolderName  ".
#    
#    $sql4.
#    "       INNER JOIN dbo.tbl_NamedReportsGroups nm1 ON fr1.ReportGroupId = nm1.ReportGroupId".
#    "       INNER JOIN tbl_VarSetsValues vr1 ON nm1.ReportRule = vr1.VarSetName".
#    "       INNER JOIN tbl_VarSetsValues vv1 ON vv1.VarSetName = nm1.FilterRule".
#    "        and vv1.VarName = nm1.FilterVar".
#    "          ".
##   "        INNER JOIN (Select * from tbl_JobReports where  JobName LIKE  '#{JobName}#%' ) JR".
#    "        INNER JOIN (Select * from tbl_JobReports where  JobName LIKE  '$jobName%' ) JR".
#    "        ON JR.JobReportName = vr1.VarValue".
#    "        INNER JOIN tbl_LogicalReports lr1 ON lr1.JobReportId = ".
#    "    JR.JobReportId and lr1.XferRecipient = nm1.RecipientRule       and".
#    "        lr1.FilterValue = vv1.VarValue 	 INNER JOIN tbl_LogicalReportsRefs lrr1".
#    "       ON lr1.JobReportId = lrr1.JobReportId AND lrr1.ReportId = lr1.ReportId  and lrr1.FilterVar = vv1.VarName".
#    "        INNER JOIN tbl_LogicalReportsTexts lrt1 ON lrr1.textId = lrt1.textId".
#    "        INNER JOIN dbo.tbl_ReportNames RN ON RN.ReportName = lr1.ReportName".
#    "        INNER JOIN tbl_PhysicalReports pr1 ON pr1.JobReportId = lr1.JobReportId and pr1.ReportId = lr1.ReportId".
#    "        where JR.Status = 18 and vr1.VarName = ':REPORTNAME'".
#    
#    "    GROUP BY fn1.FolderName, lr1.ReportName, JR.JobReportId, lr1.ReportID, XferStartTime, COALESCE(lrr1.UserTimeRef, JR.UserTimeRef, JR.XferStartTime), convert(varchar(500), pr1.ListOfPages) , JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime, JR.UserTimeRef, UserTimeElab ,JobName, JobNumber, JR.HasIndexes, lrt1.textString, JR.UserRef, FileRangesVar, existTypeMap,".
#    "      FolderDescr, lr1.CheckStatus, lr1.LastUsedToCheck, RN.CheckFlag".
#    "    )".
#	" select top 1000 * from (SELECT ROW_NUMBER() OVER (ORDER BY UserTimeRef) AS ROW , B.* from".
#    " (select OT.foldername,OT.FolderDescr,OT.JobReport_ID,OT.ReportId,OT.TimeRef,OT.UserTimeRef,OT.UserTimeElab,OT.XferStartTime,OT.TotReports,OT.TotPages,OT.ListOfPages,OT.JobNameOrig,OT.jobnumber,OT.JobReportDescr,OT.hasindexes,OT.JobName,OT.ReportName,OT.filerangesvar,OT.existtypemap,OT.IsGRANTED,OT.Recipient,OT.checkstatus,OT.lastusedtocheck,OT.checkflag from ".
#    " (select foldername,FolderDescr,JobReport_ID,TimeRef,UserTimeRef,UserTimeElab,XferStartTime,TotReports,TotPages,ListOfPages,JobNameOrig,jobnumber,JobReportDescr,hasindexes,JobName,filerangesvar,existtypemap,IsGRANTED,Recipient,checkstatus,lastusedtocheck,checkflag, max(ReportId) as maxReportId".
#    "  from OriginalTable".
#    "  group by foldername,FolderDescr,JobReport_ID,TimeRef,UserTimeRef,UserTimeElab,XferStartTime,TotReports,TotPages,ListOfPages,JobNameOrig,jobnumber,JobReportDescr,hasindexes,JobName,filerangesvar,existtypemap,IsGRANTED,Recipient,checkstatus,lastusedtocheck,checkflag) GroupedTable".
#    "  INNER JOIN OriginalTable OT".
#    "  ON GroupedTable.JobReport_ID = OT.JobReport_ID".
#    "  AND GroupedTable.Recipient = OT.Recipient".
#    "  AND GroupedTable.foldername = OT.foldername".
#    "  AND GroupedTable.maxReportId = OT.ReportId".
#    "  ) B".
#    "    $whereClause) T WHERE ROW > 0 ORDER BY $ORDERBY ";
#TESTSAN 14/03/2016 - FINE
##TESTSAN
#    my $sql6 = "select top 1000 * from (SELECT ROW_NUMBER() OVER (ORDER BY UserTimeRef) AS ROW , B.* from (select fn1.FolderName, FolderDescr AS FolderDescr, JR.JobReportId as JobReport_ID, lr1.ReportId As ReportId  , JR.XferStartTime As TimeRef  , COALESCE(lrr1.UserTimeRef, JR.UserTimeRef, JR.XferStartTime) As UserTimeRef, COALESCE(JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime) As UserTimeElab  , (CONVERT(varchar, JR.XferStartTime, 103) + SUBSTRING(CONVERT(varchar, JR.XferStartTime, 121),11,6)) As XferStartTime  ,".
#    "    1 As TotReports  , sum(lr1.TotPages) As TotPages  , CONVERT(varchar(500), pr1.ListOfPages) as ListOfPages,".
#    "      JR.JobName AS JobNameOrig ,JR.JobNumber  , '' as JobReportDescr, JR.HasIndexes, ".
#    " CASE WHEN CHARINDEX('.', JobName) > 0 THEN STUFF(JobName, CHARINDEX('.', JobName), LEN('.'), ' (') + ')' ELSE JobName END AS JobName, ".
#    "    COALESCE(lrt1.textString, JR.UserRef) AS ReportName   ,JR.FileRangesVar ".
#    "      ,JR.ExistTypeMap  ,1 As IsGRANTED,".
#    "    CASE WHEN charindex('\\',reverse(fn1.FolderName)) > 0 THEN (right(fn1.FolderName,".
#    "    charindex('\\',reverse(fn1.FolderName))-(1))) ELSE fn1.FolderName END AS Recipient, lr1.CheckStatus, lr1.LastUsedToCheck, RN.CheckFlag".
#
#    "       from tbl_Folders fn1".
#    "       INNER JOIN dbo.tbl_FoldersRules fr1 ON fn1.FolderName = fr1.FolderName  ".
#    
#    $sql4.
#    "       INNER JOIN dbo.tbl_NamedReportsGroups nm1 ON fr1.ReportGroupId = nm1.ReportGroupId".
#    "       INNER JOIN tbl_VarSetsValues vr1 ON nm1.ReportRule = vr1.VarSetName".
#    "       INNER JOIN tbl_VarSetsValues vv1 ON vv1.VarSetName = nm1.FilterRule".
#    "        and vv1.VarName = nm1.FilterVar".
#    #"        --INNER JOIN tbl_JobReports JR".
#    "          ".
#    #"       INNER JOIN (Select * from tbl_JobReports where  JobName LIKE  '#{JobName}#' ) JR".
#    "        INNER JOIN (Select * from tbl_JobReports where  JobName LIKE  '#{JobName}#%' ) JR".
#    "        ON JR.JobReportName = vr1.VarValue".
#    "        INNER JOIN tbl_LogicalReports lr1 ON lr1.JobReportId = ".
#    "    JR.JobReportId and lr1.XferRecipient = nm1.RecipientRule       and".
#    "        lr1.FilterValue = vv1.VarValue 	 INNER JOIN tbl_LogicalReportsRefs lrr1".
#    "       ON lr1.JobReportId = lrr1.JobReportId AND lrr1.ReportId = lr1.ReportId  and lrr1.FilterVar = vv1.VarName".
#    "        INNER JOIN tbl_LogicalReportsTexts lrt1 ON lrr1.textId = lrt1.textId".
#    "        INNER JOIN dbo.tbl_ReportNames RN ON RN.ReportName = lr1.ReportName".
#    "        INNER JOIN tbl_PhysicalReports pr1 ON pr1.JobReportId = lr1.JobReportId and pr1.ReportId = lr1.ReportId".
#    "        where JR.Status = 18 and vr1.VarName = ':REPORTNAME'".
#    
#    "    GROUP BY fn1.FolderName, lr1.ReportName, JR.JobReportId, lr1.ReportID, XferStartTime, COALESCE(lrr1.UserTimeRef, JR.UserTimeRef, JR.XferStartTime), convert(varchar(500), pr1.ListOfPages) , JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime, JR.UserTimeRef, UserTimeElab ,JobName, JobNumber, JR.HasIndexes, lrt1.textString, JR.UserRef, FileRangesVar, existTypeMap,".
#    "      FolderDescr, lr1.CheckStatus, lr1.LastUsedToCheck, RN.CheckFlag".
#    "    ) B".
#    "    $whereClause) T WHERE ROW > 0 ORDER BY $ORDERBY ";
    #main::debug2log($sql6."and__".$user);
	my $prtvars = { rootCond => $sql4, JobName=> $jobName, WhereClause => $whereClause, JobNumberCondition => $JobNumberCondition , ReportNameCondition => $ReportNameCondition, RecipientCondition => $RecipientCondition ,OrderBy => $ORDERBY};
    my @sqlArray;
	main::debug2log("Parametri -------" , Data::Dumper("parametri all ", $prtvars));
	foreach my $l (split /\n/, $XReport::cfg->{sqlprocs}->{OnLineReports}){
     
      $l =~  s/\#\{(.+?)\}\#/$prtvars->{$1}/g;
	  main::debug2log("Parametri -------" , $1."=".$prtvars->{$1});
      push @sqlArray, $l;
    }  
	my $sql7 = join "\n" , @sqlArray;
    $sql6 =~ s/#{([^\{]+)}#/$IndexEntries->[0]->{'Columns'}->{$1}->{'content'}/g;
	#main::debug2log("ECCO QUERY ONLINE MAXVERS:".$sql6."\nand__".$user);
    my $sql3 = "SELECT TOP 1000 * FROM \n".
               "    ( SELECT ROW_NUMBER() OVER (ORDER BY UserTimeRef) AS ROW,  B.* \n".
               "         FROM dbo.vw_reportsSmartTestToRecipient B $whereClause ) T \n".
               "WHERE ROW > $currentRow ORDER BY RECIPIENT"; 
   	#main::debug2log("ECCO QUERY ONLINE:".$sql6preStat.";\n".$sql6."\nand__".$user);   
  	my $result; 
	my $outResult;
	#eval { $result = _getDataFromSQLWithPreStatement($sql6preStat,$sql6); };
	
	my $QHTTPUSER = $user;
	my $QJOBNAME = $jobName or '%';
	my $QFROMDATE = join( '-', unpack('a4a2a2',$IndexEntries->[0]->{'Columns'}->{UserTimeRef}->{'Min'}));
	my $QTODATE =   join( '-', unpack('a4a2a2',$IndexEntries->[0]->{'Columns'}->{UserTimeRef}->{'Max'}));
	my $QRECIPIENT = $Recipient or '%';
	my $QJOBNUMBER = $JobNumber;
	my $QREPORTNAME = $IndexEntries->[0]->{'Columns'}->{ReportName}->{'content'};
	my $QWHERECONDITIONREPORTNAME = ''; 
	$QWHERECONDITIONREPORTNAME = "where COALESCE(lrt.textString, JR.UserRef) like '${QREPORTNAME}'" if( $QREPORTNAME );
	
	my $newquery = qq{
-------------	:SETVAR HTTPUSER "XREPORT"
---------------:SETVAR HTTPUSER "A29990"
---------------:SETVAR JOBNAME "%"
---------------:SETVAR FROMDATE "2017-05-01"
---------------:SETVAR TODATE "2017-05-15"
---------------:SETVAR RECIPIENT "%"
---------------:SETVAR JOBNUMBER :SETVAR HTTPUSER "XREPORT"
---------------:SETVAR HTTPUSER "A29990"
-------------:SETVAR JOBNAME "%"
-------------:SETVAR FROMDATE "2016-05-12 00:00:00.000"
-------------:SETVAR TODATE "2016-05-12 23:59:59.999"
-------------:SETVAR FDATECHAR "20160512"
-------------:SETVAR TDATECHAR "20160512"
---------------:SETVAR RECIPIENT "%09706%"
-------------:SETVAR RECIPIENT "%"
-------------:SETVAR JOBNUMBER "%"

with JNLIST AS (
SELECT DISTINCT JobReportName -- JobReportId, JobReportName, JobName, JobNumber, XferStartTime, Status -- DISTINCT top(1000) JobReportName
from tbl_JobReports with (INDEX([IX_JobReports_JobName_XferStart]))
where ((JobName IS NOT NULL) and (Status > 17) and status <> 31 and status <> 47 and status <> 63)
	  --AND jobreportname like '${QJOBNAME}%' and XferStartTime between '${QFROMDATE}' and '${QTODATE}'
	  AND jobreportname like '${QJOBNAME}%' and XferStartTime between  CONVERT ( datetime , '${QFROMDATE}' , (110) ) and  DATEADD(day,1,CONVERT( datetime , '${QTODATE}' , (110) )   ) 
)
, usrf as (
SELECT DISTINCT fo.FolderName, fo.FolderNameKey
	  From tbl_UserAliases usra
	  INNER JOIN dbo.tbl_UsersProfiles usrp on usrp.UserName = usra.UserName
	  INNER JOIN dbo.tbl_ProfilesFoldersTrees pft on pft.ProfileName = usrp.ProfileName
	  INNER JOIN dbo.tbl_folders fo on ((pft.RootNode = 'ROOT' and fo.FolderName like '%') OR ( pft.RootNode <> 'ROOT' AND fo.FolderName like pft.RootNode + '%' ))
	  WHERE usra.UserAlias = '${QHTTPUSER}'
	    AND fo.FolderNameKey like '${QRECIPIENT}'
)
, FRN AS ( 
SELECT usrf.FolderNameKey, usrf.FolderName, vr1.VarValue as JobReportName, vv1.VarName as FilterVar, vv1.VarValue as FilterValue, nrg.RecipientRule as XferRecipient 
	  , (select fn.FolderDescr from dbo.tbl_folders fn where fn.FolderName = fr.Foldername) as FolderDescr
	  From dbo.tbl_FoldersRules fr 
	  INNER JOIN usrf on usrf.FolderName = fr.FolderName
	  -- INNER JOIN dbo.tbl_Folders fn on fr.FolderName = fr.FolderName 
      INNER JOIN dbo.tbl_NamedReportsGroups nrg ON fr.ReportGroupId = nrg.ReportGroupId
      INNER JOIN dbo.tbl_VarSetsValues vr1 with (INDEX(IX_reportRules)) ON nrg.ReportRule = vr1.VarSetName and vr1.VarName = ':REPORTNAME'
      INNER JOIN dbo.tbl_VarSetsValues vv1 ON vv1.VarSetName = nrg.FilterRule and vv1.VarName = nrg.FilterVar and vv1.varname <> ':REPORTNAME'
	  INNER JOIN JNLIST on JNLIST.JobReportName = vr1.VarValue
)
, LRI AS (
select jrix.JobReportId, jrix.JobReportName, jrix.JobName, jrix.JobNumber, jrix.XferStartTime, jrix.Status 
     , lr.reportid, lr.UserTimeRef, lr.XferRecipient,  FRN.FolderName ,FRN.FoldernameKey, lr.textId, FRN.FolderDescr
	 , (select RN.CheckFlag from dbo.tbl_ReportNames RN where RN.reportname = jrix.jobreportname) as CheckFlag
	 , lr.CheckStatus, lr.LastUsedToCheck  
from tbl_JobReports jrix with (INDEX([IX_JobReports_JobName_XferStart]))
inner join vw_LogicalReports lr WITH (INDEX([IX_vw_LR_JRID_FN_FV_XR])) on jrix.JobReportId = lr.JobReportId  
inner join FRN on jrix.JobReportName = FRN.JobReportName 
       AND FRN.FilterVar = lr.FilterVar 
       AND FRN.FilterValue = lr.FilterValue 
	   AND FRN.XferRecipient = lr.XferRecipient
	   AND FRN.FolderNameKey like '${QRECIPIENT}'
where ((jrix.JobName IS NOT NULL) and (jrix.Status > 17) and jrix.status <> 31 and jrix.status <> 47 and jrix.status <> 63)
	  AND jrix.jobreportname like '${QJOBNAME}%' 
	  AND jrix.jobnumber like '${QJOBNUMBER}' 
	  --and jrix.XferStartTime between '${QFROMDATE}' and '${QTODATE}'
	  and jrix.XferStartTime between CONVERT ( datetime , '${QFROMDATE}' , (110) ) and  DATEADD(day,1,CONVERT( datetime , '${QTODATE}' , (110) )   ) 
)
, B AS (
SELECT LRI.JobReportId as JobReport_ID, LRI.JobReportName, substring(LRI.jobreportname,1,8) as JobName, LRI.JobNumber, LRI.XferStartTime, LRI.Status 
            , LRI.reportid, LRI.FolderName , LRI.FolderNameKey as Recipient, LRI.TextId, LRI.FolderDescr, LRI.CheckFlag, LRI.CheckStatus, LRI.LastUsedToCheck 
            , JR.DateTime, JR.LocalFileName, JR.LocalPathId_IN, JR.LocalPathId_OUT, JR.OrigJobReportName, JR.TarFileNumberIN, JR.TarRecNumberIN, JR.TarFileNumberOUT
            , JR.TarRecNumberOUT, JR.RemoteHostAddr, JR.RemoteFileName, JR.JobExecutionTime, JR.XferEndTime, JR.XferRecipient
			, JR.XferMode, JR.XferInBytes, JR.XferInLines, JR.XferInPages, JR.XferOutBytes, JR.XferDaemon, JR.XferId, JR.ElabStartTime, JR.ElabEndTime, JR.ElabOutBytes
			, JR.InputLines, JR.InputPages, JR.ParsedLines, JR.ParsedPages, JR.DiscardedLines, JR.DiscardedPages, JR.HasIndexes, JR.isAFP, JR.isFullAFP, JR.ExistTypeMap
			, JR.MayBeTypeMap, JR.FileRangesVar, JR.ViewLevel, JR.MailingStatus, JR.PendingOp, JR.SrvName, JR.UserRef, JR.XrefData
			, JR.HoldDays, JR.XferDestArea
			, COALESCE(LRI.UserTimeRef, JR.UserTimeRef, JR.XferStartTime) As UserTimeRef
            , COALESCE(JR.UserTimeElab, JR.JobExecutionTime, JR.ElabStartTime) As UserTimeElab
			, COALESCE(lrt.textString, JR.UserRef) AS ReportName
			, PR.TotPages, PR.ListOfPages 

FROM LRI
INNER JOIN dbo.tbl_JobReports JR on LRI.JobReportId = JR.JobReportId 
INNER JOIN dbo.tbl_PhysicalReports PR ON LRI.JobReportId = PR.JobReportId and LRI.ReportId = PR.ReportId
INNER JOIN dbo.tbl_LogicalReportsTexts LRT ON LRI.textId = LRT.TextId
${QWHERECONDITIONREPORTNAME}
)   select top 1000 * from (SELECT ROW_NUMBER() OVER (ORDER BY UserTimeRef) AS ROW , B.* from B ) T WHERE ROW > 0 ORDER BY ${ORDERBY}

	};
   	#main::debug2log("ECCO QUERY ONLINE MAXVERS:".$sql7."\nand__".$user);   
	$sql7 =~ s/%%/%/g ; 
	main::debug2log("ECCO QUERY ONLINE GIUVERS:".$sql7."\nand__".$user);
	eval { $result = _getDataFromSQL($sql7); };
	if($@) {
		main::debug2log("Data Base Error:".$@);
		$actionMessage = "Data Base Error".$@ ;
	}
	else
	{
		#main::debug2log("ECCO_QUELLO_CHE_MI_SERVE".Dumper($result));
		if(!$result) {
			$actionMessage = "No data found!";
		} else {
			$actionMessage = "SUCCESS";
			
			if(scalar @{$result}  > 1000){
			 my @intermedio = @{$result}[1..1000]; 
				 $outResult = \@intermedio;
			} else {
				$outResult = $result;
			}  
		
		}
	}
	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];

	main::debug2log("@@@@@@@@@@@@@@@@@@------------------------------------------------------".Dumper(@{$outResult}[1])) if ref($outResult) eq "ARRAY";
	push @{$respEntries}, @{$outResult} if($outResult);
#	my $h = gensym();
#	open($h, ">D:/out.log") or die("open error: $!");
#	print $h XReport::SOAP::buildResponse({
#		IndexName => 'XREPORTWEB',
#		IndexEntries => $respEntries
#	});
#	close($h);

	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries
	}));
}

sub _checkRoot{
	my($user) =(shift);
	main::debug2log("PARA________".$user);
    
    my $sql = "select  RootNode from tbl_ProfilesFoldersTrees pft join tbl_UsersProfiles up ".
		  "   with (NOLOCK)  on pft.ProfileName = up.ProfileName ".
          "  join tbl_UserAliases ua   with (NOLOCK) on up.UserName = ua.UserName and  ua.UserAlias = '$user' and RootNode = 'ROOT' ";
	main::debug2log($sql);
    my $result = _getDataFromSQLNoFill($sql);
    #main::debug2log("_CHECKROOT_".Dumper($result->Fields()->Item('RootNode')->Value()));
	return $result;
	
}

sub getReportsCount
{
  	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $whereClause = "";
    my $user = ""; 
    #my $currentRow = 0;
    my $count = 0;
	my ($IndexEntries, $IndexName, $TOP, $ORDERBY, $DISTINCT) = @{$selection->{request}}{qw(IndexEntries IndexName TOP ORDERBY DISTINCT)};
	$TOP = 3000 if($TOP =~ /^$/);

    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
		my $field_name = $field;

		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\\\\/\\/g;
		if($field_name =~ /^User$/){
			$user = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
			next;
		}
        if($field_name =~ /^CurrentRow$/){
           #$currentRow = $IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'};
			next;
		}
		if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/
			       	and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Min'} =~ /^$/
				and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Max'} =~ /^$/) {
			delete($IndexEntries->[0]->{'Columns'}->{$field_name});
			next;
		}			
		$whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) >= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} , "'", "AND")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'Min'} !~ /^\s*$/);
		$whereClause = _addWhereClause($whereClause, " Convert (varchar ,[$field] ,112) <= ", $IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} , "'", "AND")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'Max'} !~ /^\s*$/);	
        $whereClause = _addWhereClause($whereClause, "$field LIKE ",$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'", "AND") 
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'operation'} !~ /^date$/);
	}
    my $sql4 = " INNER JOIN tbl_profilesfolderstrees pft". 
               " JOIN tbl_usersprofiles up".
               " INNER JOIN tbl_useraliases ua".
               " ON up.username = ua.username ON pft.profilename = up.profilename AND rootnode <> 'root'".
               " AND ua.useralias LIKE '$user' ON SubString(FolderName, 1, len(pft.rootNode) ) = pft.rootNode ";
        
    if(!(_checkRoot($user) != undef)){
		 main::debug2log("USER_IS_NOT_ROOT");       
         $whereClause = $sql4.$whereClause." ";   
	}

    #my $sql3 = "select top 1000 * from ( SELECT ROW_NUMBER() OVER (ORDER BY UserTimeRef) AS ROW,  B.* ,(select COUNT(*) from dbo.vw_ReportsSmartTestToRecipient A $whereClause ) as Totale from dbo.vw_reportsSmartTestToRecipient B $whereClause ) T WHERE ROW > $currentRow"; 
   	my $sql3 = "SELECT COUNT(*) ".
	           "FROM dbo.vw_reportsSmartTestToRecipient3 $whereClause";               
    main::debug2log($sql3."and__".$user);   
  	my $result = _getDataFromSQL($sql3);
	#main::debug2log("ECCO_QUELLO_CHE_MI_SERVE".Dumper($result));
    my $outResult;
	if(!$result) {
		$actionMessage = "No data found!";
	} else {
	    $actionMessage = "SUCCESS";
	    $outResult = $result;	
    }

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];

	push @{$respEntries}, @{$outResult} if($outResult);
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries
	}));
}

sub getJobReportPdf {
	my ($method, $reqident, $selection) = (shift, shift, shift);

	my $respEntries = [];
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportId = $IndexEntries->[0]->{'Columns'}->{'JobReportId'}->{'content'};
	my $FromPage = $IndexEntries->[0]->{'Columns'}->{'FromPage'}->{'content'};
	;
	my $JobReportHandle = XReport::JobREPORT->Open($JobReportId);
	my $output_archive = $JobReportHandle->get_OUTPUT_ARCHIVE();
	my $JobReportName = $JobReportHandle->getValues('JobReportName');
	
	my $xfileName = "\$\$PageXref.$JobReportId.txt";
	my $zipMember = $output_archive->ExistsFile( $xfileName ); my $ObjectLength;
	$zipMember->desiredCompressionMethod(COMPRESSION_STORED);
	$ObjectLength = $zipMember->uncompressedSize();

	$zipMember->rewindData();
	my $lastChunkLength = 0; my $xrefV = "";
	while( !$zipMember->readIsDone() ) {
		my ($ref, $status) = $zipMember->readChunk( 32768 );
		if ( $status != AZ_OK and $status != AZ_STREAM_END ) {
				main::debug2log("READ ERROR rc=$status");
				last;
		}
		$xrefV .= $$ref;
		$lastChunkLength = length($$ref); 
	}

	$zipMember->endRead();
	main::debug2log("PageXref: $xrefV");
	my ($fid) = ($xrefV =~ /.*File=(\d+)\sFrom=$FromPage.*/m);

	my $fileName = "$JobReportName\.$JobReportId\.0\.#$fid\.pdf";
	#my $fileName = "$JobReportName\.$JobReportId\.0\.#0\.pdf";

	$zipMember = $output_archive->ExistsFile( $fileName );
	$zipMember->desiredCompressionMethod(COMPRESSION_STORED);
	$ObjectLength = $zipMember->uncompressedSize();
	#$main::Response->AddHeader("Content-Length", $ObjectLength);
	#$main::Response->{ContentType} = 'application/pdf';
	$main::Response->{Buffer} = 0;
	$zipMember->rewindData();
	my $lastChunkLength = 0; my $pdfV = undef;
	while( !$zipMember->readIsDone() ) {
		my ($ref, $status) = $zipMember->readChunk( 32768 );
		if ( $status != AZ_OK and $status != AZ_STREAM_END ) {
				main::debug2log("READ ERROR rc=$status");
				last;
		}
		#if ( !$pdfV or $lastChunkLength != length($$ref) ) {
			#$pdfV = new Win32::OLE::Variant(VT_UI1, $$ref);
			#}
		#else {
		
			#$pdfV->Put($$ref);
			#}
		$pdfV.= $$ref;
		#$main::Response->BinaryWrite($pdfV);
		$lastChunkLength = length($pdfV); 
	}

	$zipMember->endRead();
	

	 #unlink("$spool/$FileName");
	main::debug2log("len response:".length($pdfV));
	my ($buffsize, $outbytes, $pdfContent) = (57*76, 0, undef);
	main::debug2log("Start base64 encoding");
	#$pdfContent = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsize)*", $pdfV ) );
	$pdfContent = MIME::Base64::encode_base64($pdfV);
	main::debug2log("End base64 encoding");
	
	my $actionMessage = "SUCCESS";
	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	#$main::Response->AddHeader('Content-Length' => length($pdfContent));
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries,
		DocumentType => 'application/pdf',
		FileName => "$JobReportName\.$JobReportId",
		Identity =>  $JobReportName.$JobReportId,
		DocumentBody => {content => "<![CDATA[$pdfContent]]>"},
	 }));
	
	 #my $response = XReport::SOAP::buildResponse({
	 #	IndexName => 'XREPORTWEB',
	 #	IndexEntries => $respEntries,
	 #	DocumentType => 'application/pdf',
	 #	FileName => "$JobReportName\.$JobReportId",
	 #	Identity => $JobReportName.$JobReportId,
	 #	DocumentBody => {content => "<![CDATA[$pdfContent]]>"},
	 #});
	main::debug2log("len response decoded:".length($pdfContent));
	#$main::Response->AddHeader("Content-Length", length($response));
	#my $t = 0;
	# while ( $response ) {
	#   (my $buff, $response) = unpack("a32768 a*", $response);
	#	if($buff) {
	#	$main::Response->Write($buff);
	#		$t += length($buff);
	#	}
	#	main::debug2log("len chunk:".$t);
	# }  
	# main::debug2log("${method} $reqident bytes of response sent to requester($t)");
	# return $main::Response->Flush();
		
}

sub _loadIndexAttrs {
    my ($ixname) = (shift);
    my $ixattrsTXT = $main::Application->{"index.$ixname"};
    main::debug2log("Method _loadIndexAttrs ATTRIBUTES: $ixattrsTXT");
    if ( !$ixattrsTXT ) {
       main::debug2log("Method _loadIndexAttrs INPUT: $ixname");
       my $dbhix = XReport::DBUtil->get_ix_dbc($ixname);
       my $dbr = $dbhix->dbExecuteReadOnly("SELECT TOP 1 * from tbl_idx_$ixname");
       my $fields = $dbr->Fields();
       main::debug2log("Method _loadIndexAttrs RESULTS:\n ".Dumper( $fields->Item(3)));
       $ixattrsTXT  = 'columns=['.join(',',
            (grep !/^(?:JobReportId|FromPage|ForPages)$/, map { $fields->Item($_)->Name() } (0..$fields->Count()-1) )
               ).']' ;
	       #$main::Application->{"index.$ixname"} = 'columns=['.join(',',
	       #(grep !/^(?:JobReportId|FromPage|ForPages)$/, map { $fields->Item($_)->Name() } (0..$fields->Count()-1) )
	       #).']' ;
       $main::Application->{"index.$ixname"} = $ixattrsTXT;
    }
    main::debug2log("Method _loadIndexAttrs ATTRIBUTES: $ixname -- $ixattrsTXT");
    return {map { map { $_ =~ /^\[([^\]]+)\]$/ ? [split /\,/, $1 ] : $_ } split /=/, $_ } split /\|/, $ixattrsTXT }; 
}

sub _SOAP_ERROR {
  my $args = { @_ };
  $args->{faultcode} = 'soap:Server' unless exists($args->{faultcode});
  $args->{faultstring} = 'Service Error' unless exists($args->{faultstring});
  my $respmsg = XReport::SOAP::Server::_build_soap_error($main::Application->{ApplName}, @_);	
	
  $main::Response->Status("500 Internal Server Error");
  $main::Response->AddHeader("Connection", "close");
  $main::Response->Write( $respmsg );
  return undef;
	
}


sub getIndexes {
  my ($method, $reqident, $selection) = (shift, shift, shift);
  
  main::debug2log("Method getIndexes INPUT:", Dumper(@{$selection->{request}}{qw(IndexName IndexEntries)})); 
  
  my $dfltTOP = $XReport::cfg->{defaulttop} || 100;

  my ($tblid, $distinct, $top, $orderby, $IndexEntries) = @{$selection->{request}}{qw(IndexName DISTINCT TOP ORDERBY IndexEntries)};
  my ($dbh, $what);
  if ( !$tblid || $tblid =~ /^$/) {
     $dbh = XReport::DBUtil::getHandle('XREPORT');
     $what= "tbl_JobReportsIndexTables";
  }else {
     $dbh = XReport::DBUtil::getHandle('XRINDEX');
     $what = 'tbl_idx_'.$tblid;
  }
  if ( $tblid =~ /^#/ ) {
     my $sqlid = substr($tblid, 1);
     my $sql = $XReport::cfg->{sqlprocs}->{$sqlid};
     return _SOAP_ERROR( faultcode => 500, faultstring => "Client Request Error"
                             , detailmsg => "sql $sqlid not found"
                             , detaildesc => "$sqlid not in cfg list (". join(', ', keys %{$XReport::cfg->{sqlprocs}}) ) unless $sql;
     $what = '('.$sql.') ';
  }
  
  if ( $tblid =~ /^_/ ) {
     $dbh = XReport::DBUtil::getHandle('XREPORT');
     $what = "tbl".$tblid;
  }
	  
  my $indexAttr;
  if ($tblid  or $tblid !~ /^$/ ) {
        $indexAttr = _loadIndexAttrs($tblid);
	main::debug2log("Method getIndexes ATTRIBUTES: ".Dumper($indexAttr)); 	
	if (!$distinct or $distinct =~ /^$/)
	{
		$distinct = $indexAttr->{columns}[0];
	}
	else
	{
		my $index = 0;
		$index++ until $indexAttr->{columns}[$index] eq $distinct;
		$index++;
		if ($index < scalar(@{$indexAttr->{columns}}))
		{
			$distinct = $indexAttr->{columns}[$index];
		}
		else
		{
			$distinct = "*";
		}
        }	
	main::debug2log("Method getIndexes DISTINCT: ".Dumper($distinct)); 
	#$DocumentData->{DISTINCT} = (grep { !exists($IndexEntries->[0]->{Columns}->{$_}) } @{_loadIndexAttrs($tblid)}[0];
   	#$DocumentData->{DISTINCT} = (grep { !exists($IndexEntries->[0]->{Columns}->{$_}) } @{_loadIndexAttrs(@{$outlist}[0]->{Columns}[0]->{content})->{columns}})[0];   
  }
  
  main::debug2log("Method getIndexes INDEX_ENTRIES: ".Dumper(@{$IndexEntries}));

  my $select = 'SELECT DISTINCT ';
  $select .= ( !$top ? "TOP $dfltTOP" : $top ne "-1" ? "TOP $top" : '' );
  $select .= ' ' . ( $distinct ? $distinct : 'IndexName' ). ' ';
  $select .= ' FROM ['. $what.']';
    
  my $where = '';
  $where = ' WHERE (' . join(') OR (', XReport::SOAP::buildClause(@{$selection->{request}}{qw(IndexName IndexEntries)}, '')).')'
				  if (exists($selection->{request}->{IndexEntries}) && scalar({$selection->{request}->{IndexEntries}}));

  $select .= $where;  
  $select .= ' ORDER BY '.$orderby if $orderby;

  #i::traceit( "getDocList: ", _Dumper($selection), "Clause: $where") if $main::dotrace;
  main::debug2log("Method getIndexes SELECT QUERY: $select");

  my $dbr; eval { $dbr = $dbh->dbExecute($select)};
  return _SOAP_ERROR(faultcode => 500, faultstring => "Data Base Error"
                      , detailmsg => "getIndexList error", detaildesc => $@. "Request: "._Dumper($selection)) if $@;
  my $outlist = XReport::SOAP::fillIndexEntries($dbr);
  main::debug2log("Method getIndexes SELECT QUERY RESULTS: ".Dumper($outlist));
  
  #if ( $tblid  && $tblid !~ /^$/ ) {
   	 #foreach my $ie ( @{$outlist} ) {
   	  	#$ie->{Columns}->{rootvar} = _loadIndexAttrs($ie->{Columns}[0]->{colname});
   	  #}
  #}
     
  my $DocumentData = {IndexEntries => $outlist
                      , NewEntries => []
                      , DocumentBody => ''
  };
  $DocumentData->{IndexName} = $tblid if $tblid;
  $DocumentData->{DISTINCT} = $distinct;
    
  return $main::Response->Write( XReport::SOAP::buildResponse($DocumentData) );                      
}


sub getIndexesUnicredit {
  my ($method, $reqident, $selection) = (shift, shift, shift);
  
  main::debug2log("Method getIndexesUnicredit INPUT:", Dumper(@{$selection->{request}}{qw(IndexName IndexEntries)})); 
  
  my $dfltTOP = $XReport::cfg->{defaulttop} || 100;

  my ($tblid, $distinct, $top, $orderby, $IndexEntries) = @{$selection->{request}}{qw(IndexName DISTINCT TOP ORDERBY IndexEntries)};
  my ($dbh, $what);
  $dbh = XReport::DBUtil::getHandle('XRINDEX');
  $what = 'tbl_idx_'.$tblid;

	    
  main::debug2log("Method getIndexesUnicredit INDEX_ENTRIES: ".Dumper(@{$IndexEntries}));
  my $select = '';
  my $where = '';
  if ($distinct =~ /SUM/)
  {
	   if($orderby)
	   {
			$select = 'SELECT SUM (ForPages), '.$orderby;	
			$select .= ' FROM ['. $what.']';
            $where = ' WHERE (' . join(') OR (', XReport::SOAP::buildClause(@{$selection->{request}}{qw(IndexName IndexEntries)}, '')).')'
				  if (exists($selection->{request}->{IndexEntries}) && scalar({$selection->{request}->{IndexEntries}}));
		    $select .= $where;  
			$select .=  ' group by '.$orderby;	
	   }
	   else
	   {
	   $select = 'SELECT SUM (ForPages) ';
           $select .= ' FROM ['. $what.']';
           $where = ' WHERE (' . join(') OR (', XReport::SOAP::buildClause(@{$selection->{request}}{qw(IndexName IndexEntries)}, '')).')'
				  if (exists($selection->{request}->{IndexEntries}) && scalar({$selection->{request}->{IndexEntries}}));

           $select .= $where;  
  	   $select .= ' ORDER BY '.$orderby if $orderby;
  }
  }
  else  {
  	   $select = 'SELECT DISTINCT ';
 	   $select .= ( !$top ? "TOP $dfltTOP" : $top ne "-1" ? "TOP $top" : '' );
  	   $select .= ' ' . ( $distinct ? $distinct : 'IndexName' ). ' ';
           $select .= ' FROM ['. $what . ']';
           $where = ' WHERE (' . join(') OR (', XReport::SOAP::buildClause(@{$selection->{request}}{qw(IndexName IndexEntries)}, '')).')'
				  if (exists($selection->{request}->{IndexEntries}) && scalar({$selection->{request}->{IndexEntries}}));

           $select .= $where;  
           $select .= ' ORDER BY '.$orderby if $orderby;
  }
  #i::traceit( "getDocList: ", _Dumper($selection), "Clause: $where") if $main::dotrace;
  main::debug2log("Method getIndexesUnicredit SELECT QUERY before substitution: $select");
  
  #substitution for numeric fields. es:
  #AND ( [RAPPORTO] = '4130286' )
  #-->
  #AND ( ((isnumeric([RAPPORTO]) = 1) AND (CONVERT(bigint, [RAPPORTO]) = CONVERT(bigint, '0000004130286'))) OR ([RAPPORTO] = '0000004130286' ) ) 
  $select =~ s/AND\s*\(\s*\[([^\]]+)]\s*=\s*'(\d+)'\s*\)/AND \( \(\(isnumeric\([$1]\) = 1\) AND \(CONVERT\(bigint, [$1]\) = CONVERT\(bigint, '$2'\)\)\) OR \([$1] = '$2' \) \)/g;
  $select =~ s/AND\s*\(\s*\[([^\]]+)]\s*LIKE\s*'(\d+)'\s*\)/AND \( \(\(isnumeric\([$1]\) = 1\) AND \(CONVERT\(bigint, [$1]\) = CONVERT\(bigint, '$2'\)\)\) OR \([$1] LIKE '$2' \) \)/g;
  main::debug2log("Method getIndexesUnicredit SELECT QUERY after substitution: $select");




  my $dbr; eval { $dbr = $dbh->dbExecute($select)};
  return _SOAP_ERROR(faultcode => 500, faultstring => "Data Base Error"
                      , detailmsg => "getIndexList error", detaildesc => $@. "Request: "._Dumper($selection)) if $@;
  my $outlist = XReport::SOAP::fillIndexEntries($dbr);
  main::debug2log("Method getIndexesUnicredit SELECT QUERY RESULTS: ".Dumper($outlist));
  
  #if ( $tblid  && $tblid !~ /^$/ ) {
   	 #foreach my $ie ( @{$outlist} ) {
   	  	#$ie->{Columns}->{rootvar} = _loadIndexAttrs($ie->{Columns}[0]->{colname});
   	  #}
  #}
     
  my $DocumentData = {IndexEntries => $outlist
                      , NewEntries => []
                      , DocumentBody => ''
  };
  $DocumentData->{IndexName} = $tblid if $tblid;
  $DocumentData->{DISTINCT} = $distinct;
    
  return $main::Response->Write( XReport::SOAP::buildResponse($DocumentData) );                      
}

sub GetIndexStruct
{
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my ($IndexEntries, $IndexName, $TOP, $ORDERBY, $DISTINCT) = @{$selection->{request}}{qw(IndexEntries IndexName TOP ORDERBY DISTINCT)};
 	my $dbhix = XReport::DBUtil->get_ix_dbc($IndexName);
        my $dbr = $dbhix->dbExecuteReadOnly("SELECT TOP 1 * from [tbl_idx_$IndexName]");
        my $fields = $dbr->Fields();
	my $cols  = join(',',
            (grep !/^(?:JobReportId|FromPage|ForPages)/, map { $fields->Item($_)->Name().'|'.$fields->Item($_)->Type() } (0..$fields->Count()-1) )
               ) ;	
           
    	my $respEntries = [
		{
			Columns => [
				{colname => 'Fields', content => $cols}
			]
		}
	];
    	
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'Notes',
		IndexEntries => $respEntries
	}));
}

sub GetIndexExample
{
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my ($IndexEntries, $IndexName, $TOP, $ORDERBY, $DISTINCT) = @{$selection->{request}}{qw(IndexEntries IndexName TOP ORDERBY DISTINCT)};
 	my $dbhix = XReport::DBUtil->get_ix_dbc($IndexName);	my $whereClause = "";
    #my $JobReportId = $IndexEntries->[0]->{'JobReportId'};
    $whereClause = _addWhereClause($whereClause, "JobReportId = ",$IndexEntries->[0]->{'JobReportId'}, "'", "AND");
    foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
		my $field_name = $field;
        if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/
			       	and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Min'} =~ /^$/
				and $IndexEntries->[0]->{'Columns'}->{$field_name}->{'Max'} =~ /^$/) {
			delete($IndexEntries->[0]->{'Columns'}->{$field_name});
			next;
		}
        $whereClause = _addWhereClause($whereClause, $field_name." like ",$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'}."%", "'", "AND")

    }
    my $dbr = $dbhix->dbExecuteReadOnly("SELECT DISTINCT TOP 500 [$TOP] from [tbl_idx_$IndexName] $whereClause ");
    main::debug2log("Method getIndexExample ---whereCl --- $whereClause"); 
    my $outlist = XReport::SOAP::fillIndexEntries($dbr);         
    my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content =>  'SUCCESS'}
			]
		}
	];
    push @{$respEntries}, @{$outlist} if($outlist);    
	my $DocumentData = {IndexEntries => $respEntries
                      , NewEntries => []
                      , DocumentBody => ''
  };
    
  return $main::Response->Write( XReport::SOAP::buildResponse($DocumentData) );     
}

sub dbEscape{

 my $str = shift;
 $str =~ s/'/''/g;
 return $str;
}


1;
