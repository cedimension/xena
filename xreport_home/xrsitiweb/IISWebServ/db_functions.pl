use strict;
use Encode qw(decode encode); 
use constant CODE_PAGE => ($XReport::cfg->{DBCODEPAGE} ? $XReport::cfg->{DBCODEPAGE} : '');

sub _getDataFromSQL {
	my ($sql) = (shift);
	$sql = encode(CODE_PAGE, $sql) unless CODE_PAGE eq '';
  	my $dbr = XReport::DBUtil::dbExecuteReadOnly($sql);
	if($dbr->eof() or !$dbr) {
		return undef;
	} else {
		return XReport::SOAP::fillIndexEntries($dbr);
	}
}

sub _getDataFromSQL_withBytesStream {
	my ($sql) = (shift);
	$sql = encode(CODE_PAGE, $sql) unless CODE_PAGE eq '';
  	my $dbr = XReport::DBUtil::dbExecuteReadOnly($sql);
	if($dbr->eof() or !$dbr) {
		return undef;
	} else {
		return XReport::SOAP::fillIndexEntries_withBytesStream($dbr);
	}
}

sub _getDataFromSQLWithPreStatement {
	my ($sqlPreStat,$sql) = (shift,shift);

  	my $dbr = XReport::DBUtil::dbExecute($sqlPreStat);
  	$dbr = XReport::DBUtil::dbExecuteReadOnly($sql);
	if($dbr->eof() or !$dbr) {
		return undef;
	} else {
		return XReport::SOAP::fillIndexEntries($dbr);
	}
}

sub _getDataFromSQLNoFill {
	my ($sql) = (shift);
	$sql = encode(CODE_PAGE, $sql) unless CODE_PAGE eq '';
  	my $dbr = XReport::DBUtil::dbExecuteReadOnly($sql);
	if($dbr->eof() or !$dbr) {
		return undef;
	} else {
		return $dbr;
	}
}

sub _updateDataFromSQL {
	my ($sql) = (shift);
	$sql = encode(CODE_PAGE, $sql) unless CODE_PAGE eq '';
  	my $dbr = XReport::DBUtil::dbExecute($sql);
}

sub _addWhereClause {
	my ($where, $name, $value, $sep, $cond) = (shift, shift, shift, shift, shift);
	$cond = " AND" if($cond !~ /[AND|OR]/);

	if($value !~ /^$/) {
		if($where =~ /^$/) {
			$where = " WHERE";
		} else {
			$where .= " $cond";
		}
		$where .= " $name $sep$value$sep";
	}

	return $where;
}

1;
