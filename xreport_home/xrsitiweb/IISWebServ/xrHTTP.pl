#!/usr/bin/perl -w
use strict;
use lib "$ENV{XREPORT_HOME}\\perllib";

package getItem;

sub Item {
  return shift;
}

1;

package XReport::HTTPServ::Request;

our(@ISA, $VERSION, @EXPORT_OK, $AUTOLOAD);
use Exporter;

use Carp;
use HTTP::Daemon;
use HTTP::Status;

$VERSION = "1.00";

sub setup_environment_hash {
	my ($self) = (shift);
#	$self->{QUERY_STRING} = $self->{request}->uri()->as_string();
	(@{$self}{qw(PATH_INFO QUERY_STRING)}) = ($self->{request}->uri()->as_string()) =~ /^(.*)\?(.*)$/;
	$self->{URL} = $self->{'PATH_INFO'};
	$self->{REQUEST_METHOD} = $self->{request}->method();
	$self->{HTTP_USER_AGENT} = $self->{request}->header('User_Agent');
	my ($length) = $self->{request}->header('Content_Length');
	if ($length) {
		$self->{CONTENT_LENGTH} = $length;
	}
	my ($contentType) = $self->{request}->header('Content_Type');
	if ($contentType) {
		$self->{CONTENT_TYPE} = $contentType;
	}
	return $self;
}

sub AUTOLOAD {
	my $self = shift;
	my $type = ref($self)
		    or croak "$self is not an object";
	return if $AUTOLOAD =~ /::DESTROY$/;

	my ($module, $name) = $AUTOLOAD =~ /^(.*)::(\w+)$/;

	croak "Can't access method '$name' in class $type" unless $self->{request}->can($name);
	return $self->{request}->$name(@_);
}

sub new { 
  my ($class, $response) = (shift, shift); 
  croak "Argument 2 must be an HTTPServ::Response reference ref:" . ref($response) unless ref($response) eq 'XReport::HTTPServ::Response';
  
  i::logit("Accepting New Request for connection (" . ref($response) . ")");
  my $self = { '__c' => $response, @_ }; 
  $self->{RBuffSize} = 32*1024 unless $self->{RBuffSize};
  $self->{request} = $response->{ClientConn}->get_request(1) || croak "get Request failed - $!"; 
  bless $self, $class;
  return $self->setup_environment_hash();
}



sub ServerVariables {
  my ($self, $item) = (shift, shift);
  return $self->{$item} if exists $self->{$item};
  if ( $item =~ /^HTTP_(.*)$/ ) {
    return $self->header($1);
  }
}

sub TotalBytes {
  my $self = shift;
  i::logit("TotalBytes: " . $self->{CONTENT_LENGTH});
  return $self->{CONTENT_LENGTH};
}

sub BinaryRead {
  my $self = shift;
  my $nbytes = shift;
  my $c = $self->{'__c'}->{ClientConn};
  i::logit("Entering BinaryRead for $nbytes bytes ref: " . ref($c). " cbuff: " . length(${*$c}{'httpd_rbuf'}). " tmout: " . ${*$c}{'httpd_client_proto'});
  my $content = $self->{'__c'}->{ClientConn}->read_buffer('');
  if ($nbytes > length($content) ) {
    while (1) {
      my $blen = $nbytes - length($content);
      $blen = $self->{RBuffSize} unless $blen && $blen < $self->{RBuffSize};
      i::logit("BinaryRead reading $blen bytes contents: " . length($content) . " bytes");
      my ($result) = $self->{'__c'}->{ClientConn}->recv(my $buffer, $blen);
      
      die("Client closed connection:$^E") unless defined $result;
      last if $result == 0;
      $content .= $buffer;
      last unless $nbytes > length($content); 
    }
  }
  else {
    ($content, my $buffer) = unpack("a$nbytes a*", $content);
    $self->{'__c'}->read_buffer($buffer) if $buffer;
  }
  i::logit("returning " . length($content) . " bytes");
  return $content;
}

sub content {
  my ($self) = (shift);
  my $content = '';

  if ( !defined($self->{CONTENT_LENGTH}) || $self->{CONTENT_LENGTH} == 0 ) {
    i::logit("Entering content with no length");

    while (1) {
      my $buffer = $self->BinaryRead($self->{RBuffSize});
      $content .= $buffer if $buffer;
      last if (!$buffer || length($buffer) < $self->{RbuffSize});
    }
  }
  else {
    i::logit("Entering content for $self->{CONTENT_LENGTH} bytes");
    $content = $self->BinaryRead($self->{CONTENT_LENGTH});
    die("Client did not provide enough data") if length($content) < $self->{CONTENT_LENGTH};
  }
  return $content;
}

sub header {
  my ($self, $item) = (shift, shift);
  i::logit("HEADER requesting $item");
  return $self->{request}->header($item);
}

sub DESTROY {
  i::logit(__PACKAGE__ . " destruction begins");
}

#__PACKAGE__;

package XReport::HTTPServ::Response;

our(@ISA, $VERSION, @EXPORT_OK, $AUTOLOAD);
use Exporter;
#@ISA = qw(HTTP::Daemon::ClientConn);

use Carp;
use HTTP::Daemon;
use HTTP::Status;

$VERSION = "1.00";

sub new { my ($class, $server) = (shift, shift); 
	  croak "Argument 2 must be an HTTP::Daemon reference ref:" . ref($server) unless ref($server) eq 'HTTP::Daemon';
	  
	  my $self = { '__d' => $server, @_ }; 
	  $self->{ClientConn} = $server->accept() || return undef;
	  i::logit("Server accepted connection from " . $self->{ClientConn}->peerhost());
	  $self->{ClientConn}->autoflush(1);
	  return bless $self, $class;
	}

sub AUTOLOAD {
	my $self = shift;
	my $type = ref($self)
		    or croak "$self is not an object";
	return if $AUTOLOAD =~ /::DESTROY$/;

	my ($module, $name) = $AUTOLOAD =~ /^(.*)::(\w+)$/;

	croak "Can't access method '$name' in class $type" unless $self->{ClientConn}->can($name);
	return $self->{ClientConn}->$name(@_);
}

sub _send_hdrs {
  my $self = shift;
  $self->{HTTPresp} = HTTP::Response->new($self->{Status} ? split(/\s+/, $self->{Status}, 2) : (@_));
  $self->{HTTPresp}->header( @{$self->{Headers}} ) if $self->{Headers};
}

sub _send_content {
  my $self = shift;
  $self->{HTTPresp}->content(join('', @_)) if scalar(@_);
  $self->{ClientConn}->send_response($self->{HTTPresp});
}

sub Write {
  my $self = shift;
#  i::logit("Write Entered by " . join('::', (caller())[0,2]));
  _send_hdrs($self, RC_OK) unless ( $self->{HTTPresp} ); 
  _send_content( $self, @_ );
}

sub BinaryWrite {
  my $self = shift;
  _send_hdrs($self, RC_OK) unless ( $self->{HTTPresp} );
  _send_content( $self, @_ );
}

sub AddHeader { 
  my $self = shift; 
  push @{$self->{Headers}}, @_; 
}

sub End {
  my $self = shift;
  i::logit("END Entered by " . join('::', (caller())[0,2]));
  _send_hdrs($self, 999, "Interrupted") unless ( $self->{HTTPresp} );
  $self->{ClientConn}->send_response($self->{HTTPresp});
}

sub AppendToLog {   my $self = shift; i::logit(@_); }

1;

package main;

use XReport;

use lib $main::Application->{ApplPath};
use XReport::SOAP ('#'.$main::Application->{ApplName});

use HTTP::Daemon;
use HTTP::Status;

$main::servername = $main::Application->{ApplName} unless $main::servername;

my ($listenaddr, $listenport) = c::getValues(qw(ListenAddr ListenPort));
$listenaddr = c::getValues('ListenAddr') unless $listenaddr;
$listenport = c::getValues('ListenPort') unless $listenport;
$listenaddr = $ENV{COMPUTERNAME} unless $listenaddr;
$listenport = 60080 unless $listenport;

my $class = 'XReport::'.$main::Application->{ApplName};

my $thisserver = HTTP::Daemon
  ->new (LocalAddr => $listenaddr, LocalPort => $listenport, Reuse => 1)
  ;
if (my $initcode = $class->can('__INIT') ) { &$initcode($thisserver) };

$main::Application->{WSDL} =  XReport::SOAP::buildWSDL($thisserver->url(), $main::Application->{ApplName} ) unless exists($main::Application->{WSDL});
i::logit "$main::servername waiting for client requests at $listenaddr\:$listenport";

while ( $main::Response = XReport::HTTPServ::Response->new($thisserver) ) {
  i::logit("Looping over Response");
  while ($main::Request = XReport::HTTPServ::Request->new($main::Response)) {
    i::logit("Getting REQUEST_ID");
    my $httpreqid = $main::Request->ServerVariables('HTTP_REQUEST_ID') || 0;

    my $getreq = $main::Request->ServerVariables('QUERY_STRING');
    my $httpaction = $main::Request->ServerVariables('URL');
    my ($pkgref, $method) = ($httpaction =~ /^\/?("?)(.*)[#\/\@](.*)\1$/)[1,2] if $httpaction;

    i::logit("Processing client request url: " . $main::Request->ServerVariables('URL')
	     . " http_method: " . $main::Request->ServerVariables('REQUEST_METHOD') 
	     . " qstring: " . $main::Request->ServerVariables('QUERY_STRING') 
	     . " req: $getreq " # . "content: " . $main::Request->content() 
	     . " pkgref: $pkgref " 
	     . " method: $method " 
	     . " ct: " . $main::Request->ServerVariables('CONTENT_TYPE')
	     );

    if ($main::Request->method eq 'GET' && $getreq =~ /^WSDL$/i ) {
      $main::Response->AddHeader('Content_type', 'text/xml');
      $main::Response->Write($main::Application->{WSDL});
      $main::Response->End();
    } 
    elsif ($main::Request->method ne 'POST' or !$pkgref or $pkgref ne $main::Application->{ApplName} ) {
      $main::Response->{Status} = RC_BAD_REQUEST;
      $main::Response->End();
    }
    elsif ( my $msub = $class->can($method) ) {
    #      my $msub = $class->can($method);
    #      print "HTTP Action: ", $main::Request->uri(), " Data: ", length($main::Request->content), " pkgref: $pkgref method: $method sub: ", ref($msub), "CT: ", $main::Request->content_type(), "\n";
    #      die "POST REQUEST: $pkgref :: $method\n", Dumper($r);
      my $parsesub;
      if ( $parsesub = $class->can('_parseContent') ) {
	i::logit("Assigning parseContent handler for " . $main::Request->ServerVariables('CONTENT_TYPE'));
	
      }
      else {
	i::logit("Assigning default handler for " . $main::Request->ServerVariables('CONTENT_TYPE'));
	$parsesub = ($main::Request->ServerVariables('CONTENT_TYPE') eq 'text/xml' ? sub { return XReport::SOAP::parseSOAPreq($main::Request->content()) }
		    : sub { return { request => { split(/[=&]/, $main::Request->content() ) } } } );
      }
      eval { &$msub($method, $httpreqid, &$parsesub($method)); };
      if ( $@ ) {
	i::logit("$method ERROR: $@");
	$main::Response->{Status} = "500 $@" if $@;
	$main::Response->End();
      }
    }
    else {
      $main::Response->{Status} = RC_NOT_IMPLEMENTED;
      $main::Response->End();
    }
    undef $main::Request;
    last;
  } 
  
  UNIVERSAL::isa($main::Response, 'shutdown') ? $main::Response->shutdown(2) : $main::Response->close(); 
  undef $main::Response;
}

