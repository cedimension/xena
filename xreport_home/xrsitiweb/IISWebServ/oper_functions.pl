use strict;

use XReport::Web::Service;

my $serviceStatusMapping = {
	-1 => "Not Available",
	0 => "Unknown",
	1 => "Stopped",
	2 => "Starting",
	3 => "Stopping",
	4 => "Running",
	5 => "Resuming",
	6 => "Pausing",
	7 => "Paused"
};

my $serviceStatusColor = {
	-1 => "0x4D6DF6",
	0 => "0x4D6DF6",
	1 => "0xF40707",
	2 => "0xF49D07",
	3 => "0xF49D07",
	4 => "0x07F41D",
	5 => "0xF49D07",
	6 => "0xF49D07",
	7 => "0xF49D07"
};

my $serviceLineCommands = {
	'S' => '_startService',
	'P' => '_stopService',
	'R' => '_recycleService',
	'Z' => '_resetService',
	'L' => '_getServiceLog'
};

my $workQueueStatusColor = {
	0 => "0x4D6DF6",
	1 => "0xF49D07",
	15 => "0xF40707",
	16 => "0x07F41D",
	17 => "0xF49D07",
	18 => "0x07F41D",
	31 => "0xF40707"
};

my $workQueueStatusMapping = {
	0 => "Accepted",
	1 => "Receiving",
	15 => "RecvError",
	16 => "Queued",
	17 => "Processing",
	18 => "Completed",
	31 => "Failed"
};

my $workQueueLineCommands = {
	'S' => '_suspendQueue',
	'A' => '_activateQueue',
	'P' => '_deleteQueue',
	'X' => '_deleteJobreportsNow', 
	'U' => '_updateRows'
};

my $jobReportsLineCommands = {
	'R' => '_requeueJobreports',
	'P' => '_deleteJobreports',
	'X' => '_deleteJobreportsNow', 
	'I' => '_defineJobreports',
#	'IC' => '_commitDefineJobreports',
	'L' => '_getJobReportLog',
	'V' => '_getASCIIPage',
	'VD' => '_downloadInput',
#	'VDC' => '_downloadInGz',
	'VX' => '_getHEXPage',
	'VA' => '_getCtrlFile',
	'O' => '_downloadOutput',
	'MI' => 'migrateInput',
	'MO' => 'migrateOutput',
#	'E' => '_editJobreports',
#	'EC' => '_commitEditJobreports',
#	'EX' => '_editJobreportsTimes',
#	'EXC' => '_commitEditJobreportsTimes'
};

use constant DEBUG => 0;
use constant TOW_INPUT_TO_MIGRATE => 666;
use constant TOW_OUTPUT_TO_MIGRATE => 888;
use constant TOW_TO_DELETE => 999;
use constant WC_TO_DELETE => 748;

sub _getWorkQueueJob {
	my $serviceName = shift;

	my $sql = "SELECT tbl_WorkQueue.*, tbl_JobReports.JobReportName, tbl_JobReports.ElabStartTime FROM tbl_WorkQueue INNER JOIN tbl_JobReports ON tbl_WorkQueue.ExternalKey = tbl_JobReports.JobReportId WHERE tbl_WorkQueue.SrvName = '$serviceName'";
	my $result = "";

	my $dbr =  XReport::DBUtil::dbExecuteReadOnly($sql);

	if(!$dbr->eof()) {
		my $fields = $dbr->Fields();
		my $elabStartTime = "";
		$elabStartTime = $fields->Item('ElabStartTime')->Value()->Date('yyyy-MM-dd')." ".$fields->Item('ElabStartTime')->Value()->Time('hh:mm:ss tt') if($fields->Item('ElabStartTime')->Value());
		$result =
			$fields->Item('JobReportName')->Value()."(".
			$fields->Item('ExternalKey')->Value().") ".
			$elabStartTime;
#			$dbr->getFieldValue("JobReportName"). "(".
#			$dbr->getFieldValue("ExternalKey"). ") ".
#			$dbr->getFieldValue("ElabStartTime") if(!$dbr->eof())
		;
	}

	return $result;
}

sub _startService {
	my ($host, $service) = (shift, shift);
	
	my $serviceHandler = XReport::Web::Service->new();
	my $result = $serviceHandler->changeServiceState($host, $service, 1);
	if($result == "") {
		return "SUCCESS";
	} else {
		return "Start of service $service on $host failed: $result";
	}
}

sub _stopService {
	my ($host, $service) = (shift, shift);
	
	my $serviceHandler = XReport::Web::Service->new();
	my $result = $serviceHandler->changeServiceState($host, $service, 4);
	if($result == "") {
		return "SUCCESS";
	} else {
		return "Stop of service $service on $host failed: $result";
	}
}

sub _recycleService {
	my ($host, $service) = (shift, shift);

	my $serviceHandler = XReport::Web::Service->new();
	my $details = $serviceHandler->getServiceDetails($host, $service);
	my $status = $details->{'status'};
	main::debug2log("Stop service $service on $host(status = $status)");
	my $result = _stopService($host, $service) if($status != 1 && $status != 3);
	main::debug2log("Stop result = $result");
	return $result if($result !~ /SUCCESS/ && $result !~ /^$/);
	main::debug2log("Start service $service on $host(status = $status)");
	$result = _startService($host, $service) if($status != 2 && $status != 4);
	main::debug2log("Start result = $result");
	return $result;
}

sub _resetService {
	my ($host, $service) = (shift, shift);

	my $serviceHandler = XReport::Web::Service->new();
	my $details = $serviceHandler->getServiceDetails($host, $service);
	my $status = $details->{'status'};

	main::debug2log("Stop service $service at host $host");
	my $result = _stopService($host, $service) if($status != 1 && $status != 3);
	return $result if($result !~ /SUCCESS/);

	my $logsDir = c::getValues('logsdir');
	main::debug2log("Log dir is $logsDir");
	if($logsDir =~ /^(.*)\/webappls$/) {
		$logsDir = $1;
		main::debug2log("Log dir changed, now is $logsDir");
	}
	if ($logsDir =~ /localhost/i)
	{
		$logsDir =~ s/localhost/$host/i ; 
		main::debug2log("Log dir changed, now is $logsDir") ;
	} 
	my @now = localtime();
	main::debug2log("Rename $logsDir/$service.LOG to $logsDir/$service.LOG.".($now[5] + 1900).($now[4] + 1).$now[3]);
	return "Rotate of service log $logsDir/$service.LOG failed: $!" if(!rename($logsDir."/$service.LOG", $logsDir."/$service.LOG.".($now[5] + 1900).($now[4] + 1).$now[3]));

	main::debug2log("Start service $service at host $host");
	return _startService($host, $service) if($status != 2 && $status != 4);
}

sub getServices {
	my ($method, $reqident, $selection, $documentBody) = (shift, shift, shift, shift);
	my $actionMessage = "FAILED";
	my $whereClause = "";
	my $selectedService = "";
	my $selectedServer = "";
	
	if($selection) {
		my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
		$selectedService = $IndexEntries->[0]->{'Columns'}->{'cmd_ServiceName'}->{'content'};
		$selectedServer = $IndexEntries->[0]->{'Columns'}->{'cmd_ServerName'}->{'content'};
	}

	$selectedService =~ s/^\s+//;
	$selectedService =~ s/\s+$//;
	$selectedServer =~ s/^\s+//;
	$selectedServer =~ s/\s+$//;

	my $serviceHandler = XReport::Web::Service->new();
	my $hosts = $serviceHandler->getHosts();
	my $result = undef;

	foreach my $host(sort(@{$hosts})) {
		next if($selectedServer !~ /^$/ and $host !~ /$selectedServer/i);
		my $services =  $serviceHandler->getServicesName($host);
    
		foreach my $service(sort(@{$services})) {
			next if($selectedService !~ /^$/ and $service !~ /$selectedService/i);
           	my $details = $serviceHandler->getServiceDetails($host, $service);
            my $status = $details->{'status'};
			next if($status < 0);
			my $descr = $details->{'descr'};
			my $currentEntry = [
				{
					Columns => [
						{colname => 'Command', content => ""},
						{colname => 'ServerName', content => $host},
						{colname => 'ServiceName', content => $service},
						{colname => 'ServiceDescr', content => $details->{'descr'}},
						{colname => 'Status', content => $serviceStatusMapping->{$status}},
						{colname => 'ActiveWork', content => _getWorkQueueJob($service)},
						{colname => '_color', content => $serviceStatusColor->{$status}},
					]
				}
			];
			
			$result = [] if(!$result);
			push @{$result}, @{$currentEntry};
		}
	}
	
	if(!$result) {
		$actionMessage = "No definitions found!";
	} else {
		$actionMessage = "SUCCESS";
	}
	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
	push @{$respEntries}, @{$result} if($result);
#	main::debug2log(Dumper($respEntries));
	my $respToBuild = {
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries
	};

	if($documentBody !~ /^\s*$/) {
		my $buffsz = 57*76;
		$documentBody = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $documentBody ) );
		$respToBuild->{'DocumentType'} = 'text/xml';
		$respToBuild->{'FileName'} = "xrmonitor.xml";
		$respToBuild->{'Identity'} = "ServicesLogs";
		$respToBuild->{'DocumentBody'} = {content => "<![CDATA[$documentBody]]>"};
	}
	$main::Response->Write(XReport::SOAP::buildResponse($respToBuild));
}

sub manageServices {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $documentBody = "";
	my $filter = undef;

	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	foreach my $IndexEntry(@{$IndexEntries}) {
		if(!defined($IndexEntry->{'Columns'}->{'Command'})) {
			main::debug2log("Command no found: ".Dumper($IndexEntry));
			$filter = $IndexEntry;
			$actionMessage = "SUCCESS";
			next;
		}
		my $currentCommand = uc($IndexEntry->{'Columns'}->{'Command'}->{'content'});
		my $host = uc($IndexEntry->{'Columns'}->{'ServerName'}->{'content'});
		my $service = uc($IndexEntry->{'Columns'}->{'ServiceName'}->{'content'});
		if(exists($serviceLineCommands->{$currentCommand})) {
			my $commandFunction = $serviceLineCommands->{$currentCommand};
			if(defined(&{$commandFunction})) {
   		 		my $func = \&{$commandFunction};
   		 		($actionMessage, my $data) = &$func($host, $service);
				last if($actionMessage !~ /SUCCESS/);
				$documentBody .= "\n############[LOG FOR $service ON $host]############\n\n".$data."\n" if($data !~ /^\s*$/);
   		 	}
		}
	}
	
	if($actionMessage !~ /SUCCESS/) {
		my $respEntries = [
			{
				Columns => [
					{colname => 'ActionMessage', content => $actionMessage}
				]
			}
		];
		$main::Response->Write(XReport::SOAP::buildResponse({
			IndexName => 'XREPORTWEB',
			IndexEntries => $respEntries
		}));
	} else {
		$selection = undef;
		push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
		main::debug2log("selection is ".Dumper($selection));
		getServices($method, $reqident, $selection, $documentBody);
	}
}

sub _getServiceLog {
	my ($host, $service) = (shift, shift);
	my $actionMessage = "FAILED";
	
	main::debug2log("view_log command");
	my $logsDir = c::getValues('logsdir');
	main::debug2log("Log dir is $logsDir");
	if($logsDir =~ /^(.*)\/webappls$/) {
		$logsDir = $1;
		main::debug2log("Log dir changed, now is $logsDir");
	}

	if ($logsDir =~ /localhost/i)
	{
		$logsDir =~ s/localhost/$host/i ; 
		main::debug2log("Log dir changed, now is $logsDir") ;
	} 

	my $lines = _tail(100, $logsDir."/$service.LOG");
	$actionMessage = "SUCCESS";

	return ($actionMessage, $lines);
}

sub getServiceLog {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $service = $IndexEntries->[0]->{'Columns'}->{'ServiceName'}->{'content'};
	my $host = $IndexEntries->[0]->{'Columns'}->{'ServerName'}->{'content'};
	
	main::debug2log("view_log command");
	my $logsDir = c::getValues('logsdir');
	main::debug2log("Log dir is $logsDir");
	if($logsDir =~ /^(.*)\/webappls$/) {
		$logsDir = $1;
		main::debug2log("Log dir changed, now is $logsDir");
	}

	if ($logsDir =~ /localhost/i)
	{
		$logsDir =~ s/localhost/$host/i ; 
		main::debug2log("Log dir changed, now is $logsDir") ;
	} 

	my $lines = _tail(100, $logsDir."/$service.LOG");
	my $buffsz = 57*76;
	$lines = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $lines ) );
		
	$actionMessage = "SUCCESS";
	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage},
				{colname => 'ViewId', content => "servicesMenu"}
			]
		}
	];
#	main::debug2log(Dumper($respEntries));
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries,
		DocumentType => 'text/xml',
		FileName => "xrmonitor.xml",
		Identity => $host."_".$service,
		DocumentBody => {content => "<![CDATA[$lines]]>"}
	}));
}

sub _tail {
	my $lim = shift;
	my $file = shift;
	use Symbol;
	my $IN = gensym();

	my $offset = 2000;
	my $readLines;
	my @lines = qw();
	my $filesize = -s $file;

	main::debug2log("Opening file $file");
	if(!open($IN, "<$file")) {
		main::debug2log("Cannot open $file for input: $!");
		return "Log is empty";
	}
	
	while($#lines < $lim) {
		sysseek($IN, -$offset, 2);	
		sysread($IN, $readLines, $offset);
		@lines = split("\n", $readLines);
		last if($offset == $filesize);
		$offset *= 2;
		$offset = $filesize if($offset > $filesize);
	}

	close($IN);

	$lim = $#lines if($#lines < $lim);
	my @outputLines = qw();
	for(my $count = $#lines; $count >= ($#lines - $lim); $count--) {
		push @outputLines, $lines[$count];
	}

	return join("\n", reverse(@outputLines));
}

sub getWorkqueue {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $whereClause = "";

	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
		my $field_name = $field;

		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;

		if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
			delete($IndexEntries->[0]->{'Columns'}->{$field_name});
			next;
		}
		$field_name =~ s/^cmd_//;
		if($field_name =~ /^FromDate$/) {
			$whereClause = _addWhereClause($whereClause, "InsertTime >=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} . ($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /\d{2}:\d{2}:\d{2}$/ ? "" : " 00:00:00"), "'")
				if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
			next;
		}
		if($field_name =~ /^ToDate$/) {
			$whereClause = _addWhereClause($whereClause, "InsertTime <=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} . ($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /\d{2}:\d{2}:\d{2}$/ ? "" : " 23:59:59"), "'")
				if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
			next;
		}
		if($field_name =~ /^Suspend$/) {
			#if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /false/) {
			#	$whereClause = _addWhereClause($whereClause, "(SrvName NOT LIKE 'SUSPENDED' OR SrvName IS", "NULL)", "");
				#$whereClause = _addWhereClause($whereClause, "SrvName IS", "NULL", "", "OR");
				(my $cond,  $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}) =  $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~  /^\s*(AND|OR)\s+([\s\w\'\\\$\(\)\!\=]+)$/i;
				main::debug2log("ECCO \n$cond\n$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}");
				$whereClause = _addWhereClause($whereClause, "",  $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "" , $cond); 
			#}
			next;
		}
		if($field_name =~ /^SpecialMngFor/) {
				(my $cond,  $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}) =  $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~  /^\s*(AND|OR)\s+([,\s\w\'\\\$\(\)\!\=]+)$/i;
				main::debug2log("ECCO \n$cond\n$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}");
				$whereClause = _addWhereClause($whereClause, "",  $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "" , $cond); 
			next;
		}
		next if($field_name =~ /^TargetName$/);
		next if($field_name =~ /^JobName$/);
		if($field_name =~ /^ExternalKey$/) {
			$whereClause = _addWhereClause($whereClause, "ExternalKey =", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "")
				if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
			next;
		}
		$whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
	}

	my $sql = "SELECT COUNT(*) as TotalRows FROM tbl_WorkQueue $whereClause";
	main::debug2log("$sql\n");
	my $result;
	my $totalRows = "";
	eval {
  		$result = _getDataFromSQLNoFill($sql);
		if($result and !$result->eof()) {
			$totalRows = $result->Fields->Item("TotalRows")->{Value};
		}
	};
	$actionMessage = "<![CDATA[Search of queued jobs failed: $@]]>" if($@);

	$sql = 
		#"SELECT TOP 3000 '' AS Command, "
		"SELECT '' AS Command, "
		."CASE "
		." WHEN ExternalTableName = 'tbl_JobReports' THEN (SELECT JobReportName FROM tbl_JobReports WHERE JobReportId = ExternalKey) "
		." WHEN ExternalTableName = 'tbl_JobSpool' THEN (SELECT PrintDest FROM tbl_JobSpool WHERE JobSpoolId = ExternalKey) "
		." WHEN ExternalTableName = 'MIGRATION' THEN (SELECT JobReportName FROM tbl_JobReports WHERE JobReportId = ExternalKey) "
		." ELSE 'UNKNOWN' "
		." END "
		." AS TargetName, "
		."CASE "
		." WHEN ExternalTableName = 'tbl_JobReports' THEN (SELECT JobName FROM tbl_JobReports WHERE JobReportId = ExternalKey) "
		." WHEN ExternalTableName = 'MIGRATION' THEN 'MIGRATION' "
		." ELSE 'UNKNOWN' "
		." END "
		." AS JobName, * "
		." FROM tbl_WorkQueue "
		." $whereClause "
		#." ORDER BY Priority DESC, WorkId DESC"
	;
#TESTSAN - 26/11/2015
#	$sql = 
#		"SELECT TOP 3000 '' AS Command, "
#		."CASE "
#		." WHEN ExternalTableName = 'tbl_JobReports' THEN (SELECT JobReportName FROM tbl_JobReports WHERE JobReportId = ExternalKey) "
#		." WHEN ExternalTableName = 'tbl_JobSpool' THEN (SELECT PrintDest FROM tbl_JobSpool WHERE JobSpoolId = ExternalKey) "
#		." ELSE 'UNKNOWN' "
#		." END "
#		." AS TargetName, "
#		."CASE "
#		." WHEN ExternalTableName = 'tbl_JobReports' THEN (SELECT JobName FROM tbl_JobReports WHERE JobReportId = ExternalKey) "
#		." ELSE 'UNKNOWN' "
#		." END "
#		." AS JobName, * "
#		." FROM tbl_WorkQueue "
#		." $whereClause "
#		." ORDER BY Priority DESC, WorkId DESC"
#	;
	if(exists($IndexEntries->[0]->{'Columns'}->{'cmd_TargetName'})) {
#		$sql = "SELECT * FROM ($sql) AS QueuedJobs WHERE TargetName like '".$IndexEntries->[0]->{'Columns'}->{'cmd_TargetName'}->{'content'}."'";
		$sql = "SELECT * FROM ($sql) AS QueuedJobs WHERE TargetName like '".$IndexEntries->[0]->{'Columns'}->{'cmd_TargetName'}->{'content'}."'" ;
	}
	if(exists($IndexEntries->[0]->{'Columns'}->{'cmd_JobName'})) {
		if(exists($IndexEntries->[0]->{'Columns'}->{'cmd_TargetName'})) {
			#$sql .= " AND JobName like '".$IndexEntries->[0]->{'Columns'}->{'cmd_JobName'}->{'content'}."'";
			$sql = "SELECT TOP 3000 * FROM ($sql) AS QueuedJobs where JobName like '".$IndexEntries->[0]->{'Columns'}->{'cmd_JobName'}->{'content'}."'";
			$sql .= " ORDER BY Priority DESC, WorkId DESC";
		} else {
			$sql = "SELECT TOP 3000 * FROM ($sql) AS QueuedJobs WHERE JobName like '".$IndexEntries->[0]->{'Columns'}->{'cmd_JobName'}->{'content'}."'";
			$sql .= " ORDER BY Priority DESC, WorkId DESC";
		}
	}
	else
	{
		$sql = "SELECT TOP 3000 * FROM ($sql) AS QueuedJobs ";
		$sql .= " ORDER BY Priority DESC, WorkId DESC";
	}
	main::debug2log("$sql");
	$result;
	eval {
  		$result = _getDataFromSQL($sql);
		if(!$result) {
			$actionMessage = "No jobs found!";
		} else {
			$actionMessage = "SUCCESS";
		}
	};
	$actionMessage = "<![CDATA[Search of queued jobs failed: $@]]>" if($@);
	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage},
				{colname => 'TotalRows', content => $totalRows}
			]
		}
	];
	push @{$respEntries}, @{$result} if($result);
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries
	}));
}

sub manageWorkqueue {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $filter = undef;

	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	foreach my $IndexEntry(@{$IndexEntries}) {
		if(!defined($IndexEntry->{'Columns'}->{'Command'})) {
			main::debug2log("Command no found: ".Dumper($IndexEntry));
			$filter = $IndexEntry;
			$actionMessage = "SUCCESS";
			next;
		}
		my $currentCommand = uc($IndexEntry->{'Columns'}->{'Command'}->{'content'});
		my $externalKey = $IndexEntry->{'Columns'}->{'ExternalKey'}->{'content'};
		my $externalTableName = $IndexEntry->{'Columns'}->{'ExternalTableName'}->{'content'};
		if(exists($workQueueLineCommands->{$currentCommand})) {
			my $commandFunction = $workQueueLineCommands->{$currentCommand};
			if(defined(&{$commandFunction})) {
				main::debug2log("Execute $commandFunction");
   		 		my $func = \&{$commandFunction};
   		 		$actionMessage = &$func($externalKey, $externalTableName, $IndexEntry);
				last if($actionMessage !~ /SUCCESS/);
   		 	}
		} else {
			$actionMessage = "Command '$currentCommand' not defined";
		}
		
		main::debug2log($actionMessage);
	}
	
	if($actionMessage !~ /SUCCESS/) {
		my $respEntries = [
			{
				Columns => [
					{colname => 'ActionMessage', content => $actionMessage}
				]
			}
		];
		$main::Response->Write(XReport::SOAP::buildResponse({
			IndexName => 'XREPORTWEB',
			IndexEntries => $respEntries
		}));
	} else {
		$selection = undef;
		push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
		main::debug2log("selection is ".Dumper($selection));
		getWorkqueue($method, $reqident, $selection);
	}
}

sub _suspendQueue {
	my ($externalKey, $externalTableName) = (shift, shift);
	
	my $sql = 
		"UPDATE tbl_WorkQueue SET SrvName = 'SUSPENDED' WHERE ".
		"ExternalKey = ".$externalKey.
		" AND ExternalTableName = '".$externalTableName."'"
	;
	my $result = "";

	eval { $result = _updateDataFromSQL($sql); };
	if($@) {
		return "Suspend failed for externaKey $externalKey: $@";
	} else {
		return "SUCCESS";
	}
}

sub _activateQueue {
	my ($externalKey, $externalTableName) = (shift, shift);
		
	my $sql = 
		"UPDATE tbl_WorkQueue SET SrvName = NULL, Status = 16 WHERE ".
		"ExternalKey = ".$externalKey.
		" AND ExternalTableName = '".$externalTableName."'"
	;
	my $result = "";

	eval {$result = _updateDataFromSQL($sql)};
	if($@) {
		return "Activate failed for externaKey $externalKey: $@";
	} else {
		return "SUCCESS";
	}
}

sub _deleteQueue {
	my ($externalKey, $externalTableName) = (shift, shift);
		
	my $sql = 
		"DELETE tbl_WorkQueue WHERE ".
		"ExternalKey = ".$externalKey.
		" AND ExternalTableName = '".$externalTableName."'"
	;
	my $result = "";
	main::debug2log($sql);

	eval {$result = _updateDataFromSQL($sql)};
	if($@) {
		return "Delete failed for externaKey $externalKey: $@";
	} else {
		return "SUCCESS";
	}
}

sub _updateRows {
	my ($externalKey, $externalTableName, $data) = (shift, shift, shift);

	my $WorkClass = $data->{'Columns'}->{'WorkClass'}->{'content'};
	$WorkClass = "NULL" if($WorkClass =~ /^\s*$/);
	my $Priority = $data->{'Columns'}->{'Priority'}->{'content'};
	$Priority = "NULL" if($Priority =~ /^\s*$/);
	my $TOW = $data->{'Columns'}->{'TypeOfWork'}->{'content'};
	$TOW = "NULL" if($TOW =~ /^\s*$/);

	my $sql = 
		"UPDATE tbl_WorkQueue SET WorkClass = $WorkClass, Priority = $Priority, TypeOfWork = $TOW WHERE ".
		"ExternalKey = ".$externalKey.
		" AND ExternalTableName = '".$externalTableName."'"
		." AND ((TypeOfWork in (".TOW_OUTPUT_TO_MIGRATE.",".TOW_INPUT_TO_MIGRATE.") AND (TypeOfWork = '".$TOW."')) OR (TypeOfWork not in (".TOW_INPUT_TO_MIGRATE.",".TOW_OUTPUT_TO_MIGRATE.")))"
	;
	my $result = "";

	eval {$result = _updateDataFromSQL($sql)};
	if($@) {
		return "Update failed for externaKey $externalKey: $@";
	} else {
		return "SUCCESS";
	}
}

sub getJobReports {
	my ($method, $reqident, $selection, $documentBody) = (shift, shift, shift, shift);
	my $actionMessage = "FAILED";
	my $whereClause = "";
	my $joinClause = "";

	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	foreach my $field(keys(%{$IndexEntries->[0]->{'Columns'}})) {
		my $field_name = $field;

		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/^\s+//;
		$IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ s/\s+$//;

		if($IndexEntries->[0]->{'Columns'}->{$field_name}->{'content'} =~ /^$/) {
			delete($IndexEntries->[0]->{'Columns'}->{$field_name});
			next;
		}
		$field_name =~ s/^cmd_//;
		if($field_name =~ /^FromDate$/) {
			#$whereClause = _addWhereClause($whereClause, "XferStartTime >=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} . " 00:00:00", "'")
			#	if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/); 
			$whereClause = _addWhereClause($whereClause, "XferStartTime >=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} . ($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /\d{2}:\d{2}:\d{2}$/ ? "" : " 00:00:00"), "'")
				if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
			next;
		}
		if($field_name =~ /^ToDate$/) {
			$whereClause = _addWhereClause($whereClause, "XferStartTime <=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} . ($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /\d{2}:\d{2}:\d{2}$/ ? "" : " 23:59:59"), "'")
				if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
			next;
		}
		if($field_name =~ /^FromDateElab$/) {
			$whereClause = _addWhereClause($whereClause, "ElabStartTime >=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} . ($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /\d{2}:\d{2}:\d{2}$/ ? "" : " 00:00:00"), "'")
				if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
			next;
		}
		if($field_name =~ /^ToDateElab$/) {
			$whereClause = _addWhereClause($whereClause, "ElabStartTime <=", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} . ($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /\d{2}:\d{2}:\d{2}$/ ? "" : " 23:59:59"), "'")
				if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
			next;
		}
		if($field_name =~ /^Undefined$/) {
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /true/) {
				$joinClause = "LEFT OUTER JOIN tbl_JobReportNames ON tbl_JobReports.JobReportName = tbl_JobReportNames.JobReportName";
				$whereClause = _addWhereClause($whereClause, "tbl_JobReportNames.JobReportName is", "NULL", "");
			}
			next;
		}
		if($field_name =~ /^OnError$/) {
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /true/) {
				$whereClause = _addWhereClause($whereClause, "tbl_JobReports.Status NOT IN (", "18,17,16,15,1,0", "");
				$whereClause .= ")";
			}
			next;
		}

		if($field_name =~ /^QueuedDelete$/) {
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~ /true/) {
				$whereClause = _addWhereClause($whereClause, "tbl_JobReports.PendingOp IN (", "12", "");
				$whereClause .= ")";
			} else {
				$whereClause = _addWhereClause($whereClause, "tbl_JobReports.PendingOp NOT IN (", "12", "");
				$whereClause .= ")";
			}
			next;
		}
		if($field_name =~ /^SpecialMngFor/) {  
				(my $cond,  $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}) =  $IndexEntries->[0]->{'Columns'}->{$field}->{'content'} =~  /^\s*(AND|OR)\s+([,\s\w\'\\\$\(\)\!\=]+)$/i;
				main::debug2log("ECCO \n$cond\n$IndexEntries->[0]->{'Columns'}->{$field}->{'content'}");
				$whereClause = _addWhereClause($whereClause, "",  $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "" , $cond); 
			next;
		}
		
		$whereClause = _addWhereClause($whereClause, "$field_name like", $IndexEntries->[0]->{'Columns'}->{$field}->{'content'}, "'")
			if($IndexEntries->[0]->{'Columns'}->{$field}->{'content'} !~ /^\s*$/);
	}

	$whereClause =~ s/\sJobReportName\slike/ tbl_JobReports.JobReportName like/ig;

	my $sql = "SELECT COUNT(*) as TotalRows FROM tbl_JobReports $whereClause";
	main::debug2log("\nECCO QUERY=\n$sql\n");
				
	my $result;
	my $totalRows = "";
	eval {
  		$result = _getDataFromSQLNoFill($sql);
		if($result and !$result->eof()) {
			$totalRows = $result->Fields->Item("TotalRows")->{Value};
		}
	};
	$actionMessage = "Search of queued jobs failed: $@" if($@);
	
	$sql = 
		"SELECT TOP 1000 '' AS Command, tbl_JobReports.* "
		." FROM tbl_JobReports "
		." $joinClause "
		." $whereClause "
		." ORDER BY JobReportId DESC"
	;
	main::debug2log($sql);
	my $result;
	eval {
  		$result = _getDataFromSQL($sql);
		if(!$result) {
			$actionMessage = "No jobs found!";
		} else {
			$actionMessage = "SUCCESS";
		}
	};
	$actionMessage = "Search of queued jobs failed: $@" if($@);
	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage},
				{colname => 'TotalRows', content => $totalRows}
			]
		}
	];
	push @{$respEntries}, @{$result} if($result);
	my $respToBuild = {
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries
	};

	if($documentBody !~ /^\s*$/) {
		my $buffsz = 57*76;
		$documentBody = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $documentBody ) );
		$respToBuild->{'DocumentType'} = 'text/xml';
		$respToBuild->{'FileName'} = "xrmonitor.xml";
		$respToBuild->{'Identity'} = "JobReportsData";
		$respToBuild->{'DocumentBody'} = {content => "<![CDATA[$documentBody]]>"};
	}
	$main::Response->Write(XReport::SOAP::buildResponse($respToBuild));
}

sub manageJobReports {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $documentBody = "";
	my $filter = undef;
	
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	main::debug2log("Manage Job Reports : cosa arriva".Dumper(scalar@{$IndexEntries}));
	my $blockDocumentBody = 0;
	foreach my $IndexEntry(@{$IndexEntries}) {
		if(!defined($IndexEntry->{'Columns'}->{'Command'})) {
			main::debug2log("Command no found: ".Dumper($IndexEntry));
			$filter = $IndexEntry;
			$actionMessage = "SUCCESS";
			next;
		}
		my $currentCommand = uc($IndexEntry->{'Columns'}->{'Command'}->{'content'});
		($currentCommand) = split("%%", $currentCommand) if($currentCommand =~ /\%\%/);
		$currentCommand = uc($currentCommand);
		main::debug2log("Command is $currentCommand");
		my $jobReportId = $IndexEntry->{'Columns'}->{'JobReportId'}->{'content'};
		if(exists($jobReportsLineCommands->{$currentCommand})) {
			my $commandFunction = $jobReportsLineCommands->{$currentCommand};
			if(defined(&{$commandFunction})) {
				main::debug2log("Call function $commandFunction");
		 		my $func = \&{$commandFunction};
		 		($actionMessage, my $data) = &$func($jobReportId, $IndexEntry);
				last if($actionMessage !~ /SUCCESS/);
				if($currentCommand =~ /^(VD|O)$/) {
					$documentBody = $data;
					$blockDocumentBody = 1;
				}
				$documentBody .= "\n############[JobReportId $jobReportId]############\n\n".$data."\n" if($data !~ /^\s*$/ and !$blockDocumentBody);
		 	}
		}
	}
	
	if($actionMessage !~ /SUCCESS/) {
		my $respEntries = [
			{
				Columns => [
					{colname => 'ActionMessage', content => $actionMessage}
				]
			}
		];
		$main::Response->Write(XReport::SOAP::buildResponse({
			IndexName => 'XREPORTWEB',
			IndexEntries => $respEntries
		}));
	} else {
		$selection = undef;
		push @{$selection->{request}->{IndexEntries}}, $filter if($filter != undef);
		main::debug2log("selection is ".Dumper($selection));
		getJobReports($method, $reqident, $selection, $documentBody);
	}
}

sub _requeueJobreports {
	my ($jobReportId, $data) = (shift, shift);
	
	my $sql =	
		"SELECT JobReportId, Status, TypeOfWork, LocalFileName FROM tbl_JobReports".
		" INNER JOIN tbl_JobReportNames ON tbl_JobReports.JobReportName = tbl_JobReportNames.JobReportName".
		" WHERE JobReportId = ".$jobReportId
	;
	my $dbr;
	eval {$dbr = XReport::DBUtil::dbExecuteReadOnly($sql)};
	if($@) {
		return "Retrieve of JobReportName for JobReport $jobReportId failed: $@";
	}
	
	if($dbr and !$dbr->eof()) {
		my $status = $dbr->Fields->Item("Status")->{Value};
		my $localFileName = $dbr->Fields->Item("LocalFileName")->{Value};
		if($localFileName =~ /\.default/i) {
			return "Requeue failed for JobReport $jobReportId: filename shouldn't be .default";
		}
		if($status < 16) {
			return "Requeue failed for JobReport $jobReportId: status($status) must be greater than 15";
		}

		my $typeOfWork = $dbr->Fields->Item("TypeOfWork")->{Value};
		$sql = 
			"SELECT * FROM tbl_WorkQueue WHERE".
			" ExternalTableName = 'tbl_JobReports'".
			" AND ExternalKey = $jobReportId".
			" AND TypeOfWork = ".$typeOfWork
		;
		$dbr = XReport::DBUtil::dbExecuteReadOnly($sql);
		if($dbr->eof() or !$dbr) {
			$sql =
				"INSERT INTO tbl_WorkQueue(".
				"ExternalTableName, ".
				"ExternalKey, ".
				"TypeOfWork, ".
				"InsertTime, ".
				"WorkClass, ".
				"Priority, ".
				"Status, ".
				"SrvName) VALUES(".
				"'tbl_JobReports', ".
				$jobReportId.", ".
				$typeOfWork.", ".
				"GETDATE(), ".
				"NULL, ".
				"1,".
				"16,".
				"NULL)"
			;
			eval {my $result = _updateDataFromSQL($sql)};
			if($@) {
				return "Requeue failed for JobReport $jobReportId: $@";
			}
		} else {
			return "Requeue failed for JobReport $jobReportId: report is already queued";
		}
	} else {
		return "Requeue failed for JobReport $jobReportId: report is not defined";
	}

	return "SUCCESS";
}

sub migrateInput {
	my ($jobReportId, $data) = (shift, shift);
	my $typeOfWork = TOW_INPUT_TO_MIGRATE;
	return migrateData($jobReportId, $typeOfWork, $data);
}

sub migrateOutput {
	my ($jobReportId, $data) = (shift, shift);
	my $typeOfWork = TOW_OUTPUT_TO_MIGRATE;
	return migrateData($jobReportId, $typeOfWork, $data);
}

sub migrateData {
	my ($jobReportId, $typeOfWork, $data) = (shift, shift);


	
	my $sql =	
		"SELECT JobReportId, Status, TypeOfWork, LocalFileName FROM tbl_JobReports".
		" INNER JOIN tbl_JobReportNames ON tbl_JobReports.JobReportName = tbl_JobReportNames.JobReportName".
		" WHERE JobReportId = ".$jobReportId
	;
	my $dbr;
	eval {$dbr = XReport::DBUtil::dbExecuteReadOnly($sql)};
	if($@) {
		return "Retrieve of JobReportName for JobReport $jobReportId failed: $@";
	}
	
	if($dbr and !$dbr->eof()) {
		my $status = $dbr->Fields->Item("Status")->{Value};
		my $localFileName = $dbr->Fields->Item("LocalFileName")->{Value};
		if($localFileName =~ /\.default/i) {
			return "Requeue failed for JobReport $jobReportId: filename shouldn't be .default";
		}
		if($status != 18 ) {
			return "Requeue failed for JobReport $jobReportId: status($status) must be equal to 18";
		}	
		
		my ($lpTarget, $JobReportName) = GetLpTarget($jobReportId);
		if(!$lpTarget)
		{
			return "The parameter LPTARGET is missing in the configuration for $JobReportName, NO migration to do";
		}
		
		$sql = 
			"SELECT * FROM tbl_WorkQueue WHERE".
			" ExternalTableName = 'MIGRATION'".
			" AND ExternalKey = $jobReportId".
			" AND TypeOfWork = ".$typeOfWork
		;
		$dbr = XReport::DBUtil::dbExecuteReadOnly($sql);
		if($dbr->eof() or !$dbr) {
			$sql =
				"INSERT INTO tbl_WorkQueue(".
				"ExternalTableName, ".
				"ExternalKey, ".
				"TypeOfWork, ".
				"InsertTime, ".
				"WorkClass, ".
				"Priority, ".
				"Status, ".
				"SrvParameters, ".
				"SrvName) VALUES(".
				"'MIGRATION', ".
				$jobReportId.", ".
				$typeOfWork.", ".
				"GETDATE(), ".
				"NULL, ".
				"1,".
				"16,".
				"'$lpTarget',".
				"NULL)"
			;
			eval {my $result = _updateDataFromSQL($sql)};
			if($@) {
				return "Requeue of Migration failed for JobReport $jobReportId: $@";
			}
		} else {
			return "Requeue of Migration failed for JobReport $jobReportId: report is already queued";
		}
	} else {
		return "Requeue of Migration  failed for JobReport $jobReportId: report is not defined";
	}

	return "SUCCESS";
}



sub GetLpTarget {
	($main::veryverbose || DEBUG) and i::logit("$main::myName: call GetLpTarget()");
	my ($JobReportId, %args) = @_;
	my $lpTarget = XReport::Util::getConfValues('LPTARGET') or undef;
	my $confdir = $XReport::cfg->{confdir};
	$confdir = $XReport::cfg->{userlib}."/rptconf" unless $confdir && -d $confdir;
	my $jr = XReport::JobREPORT->Open($JobReportId);
	my ($ParseFileName,$JobReportName)= (split( /[\s,]+/ , $jr->getValues(qw(ParseFileName JobReportName))))[-1];
	my $rptconfXML = $confdir.'/'.$ParseFileName;

	($main::veryverbose || DEBUG) and ($lpTarget) and i::logit("$main::myName: - lpTarget(from xreport.cfg)=[$lpTarget]");
	if(-e $rptconfXML)
	{
		my $cfg_jobreport_rptconf = undef;
		$cfg_jobreport_rptconf =  XML::Simple::XMLin($rptconfXML);
		$lpTarget = $cfg_jobreport_rptconf->{'LPTARGET'} if( exists($cfg_jobreport_rptconf->{LPTARGET}) && $cfg_jobreport_rptconf->{LPTARGET});
		($main::veryverbose || DEBUG) and (exists($cfg_jobreport_rptconf->{LPTARGET})) and main::debug2log("$main::myName: - lpTarget(from rptconfXML)=[$lpTarget]");
		($main::veryverbose || DEBUG) and i::logit("$main::myName: - rptconfXML=[$rptconfXML] - cfg_jobreport_rptconf:".Dumper($cfg_jobreport_rptconf));
	}

	return $lpTarget, $JobReportName;
}

sub _deleteJobreports {
	my ($jobReportId, $data) = (shift, shift);
	
	my $sql = 
		"UPDATE tbl_JobReports SET PendingOp = 12, ElabEndTime = GETDATE() WHERE ".
		#"UPDATE tbl_JobReports SET PendingOp = 12 WHERE ".
		"JobReportId = ".$jobReportId
	;
	my $result = "";

	main::debug2log($sql);
	eval {$result = _updateDataFromSQL($sql)};
	if($@) {
		return "Update of PendingOp for JobReport $jobReportId failed: $@";
	}
	return _deleteQueue($jobReportId, 'tbl_JobReports');

#	my $jobreport = XReport::JobREPORT->Open($jobReportId, 0);
#	$jobreport->deleteAll();
}


sub _deleteJobreportsNow {
	my ($jobReportId, $data) = (shift, shift);  
	my $typeOfWork = TOW_TO_DELETE;
	my $WorkClass = WC_TO_DELETE;  
	my $ExternalTableName = 'MIGRATION'; 
	my $result = _deleteQueue($jobReportId, $ExternalTableName); 
	return $result if($result !~ /SUCCESS/); 
	my $sql =  
				"INSERT INTO tbl_WorkQueue(".
				"ExternalTableName, ".
				"ExternalKey, ".
				"TypeOfWork, ".
				"InsertTime, ".
				"WorkClass, ".
				"Priority, ".
				"Status, ".
				"SrvName) VALUES(".
				"'$ExternalTableName', ".
				$jobReportId.", ".
				$typeOfWork.", ".
				"GETDATE(), ".
				$WorkClass.", ".
				"1,".
				"16,".
				"NULL)"
			;    
	main::debug2log($sql);
	eval {$result = _updateDataFromSQL($sql)}; 
	if($@) {
		return "Delete failed for externaKey $jobReportId: $@";
	} else {
		return "SUCCESS";
	}

#	my $jobreport = XReport::JobREPORT->Open($jobReportId, 0);
#	$jobreport->deleteAll();
}

sub _defineJobreports {
	my ($jobReportId, $data) = (shift, shift);
	my $outmess = "";
	my ($command, $keys) = split("%%", $data->{'Columns'}->{'Command'}->{'content'});
	main::debug2log(Dumper("Dentro",$command, $keys));
	my @fields = split("\t", $keys);
	my $reportData = {};
	foreach my $field(@fields) {
		my ($key, $value) = split("=", $field);
		$reportData->{$key} = $value;
	}
	main::debug2log(Dumper($reportData));

	_updateDataFromSQL("BEGIN TRANSACTION REPORTADD");
	eval {
		my $sql = "";
		my $result = undef;

		$sql = "SELECT JobReportName, LocalFileName, LocalPathId_IN FROM tbl_JobReports WHERE JobReportId = $jobReportId";
  		$result = _getDataFromSQLNoFill($sql);

		if(!$result or $result->eof()) {
			die "Cannot find JobReportName for JobReportId $jobReportId";
		}
		my $JobReportName = $result->Fields->Item("JobReportName")->{Value};
		my $LocalFileName = $result->Fields->Item("LocalFileName")->{Value};
		my $LocalPathId_IN = $result->Fields->Item("LocalPathId_IN")->{Value};
		my $LocalPath = c::getValues('LocalPath')->{$LocalPathId_IN || "L1"};
		
		my @ReportGroups = split(/[,;]/, $reportData->{'ReportGroup'});
		my $ReportName = uc($reportData->{'ReportName'}) || $JobReportName;
		my $ReportDescr = uc(dbEscape($reportData->{'ReportDescr'})) || "Description for Report $ReportName";
		my $TypeOfWork = uc($reportData->{'TypeOfWork'}) || 1;
		my $RetDays = uc($reportData->{'RetDays'}) || 180;
		my $MailTo = uc($reportData->{'MailTo'});
		my $ParseFileName = $reportData->{'ParseFileName'} || '*,NULL.XML';
		my $HasCc = $reportData->{'HasCc'} || 1;
		my $CharsPerLine = uc($reportData->{'CharsPerLine'}) || 133;
		my $LinesPerPage = uc($reportData->{'LinesPerPage'}) || 66;
		my $PageOrient = uc($reportData->{'PageOrient'}) || 'L';
		my $PageSize = uc($reportData->{'PageSize'}) || '';
		my $FontSize = uc($reportData->{'FontSize'}) || '[8.0 12.0]';
		my $FitToPage = $reportData->{'FitToPage'} || 1;
		my $ArchivioInput = uc($reportData->{'ArchivioInput'});
		my $ArchivioOutput = uc($reportData->{'ArchivioOutput'});
		my $CodePage = uc($reportData->{'CodePage'});
		my $ReportFormat = uc($reportData->{'ReportFormat'}) || 2;
		my $ElabFormat = uc($reportData->{'ElabFormat'}) || 1;
		my $WorkClass = uc($reportData->{'WorkClass'}) || 'NULL';
		my $FilterVar = uc($reportData->{'FilterVar'});
		my $t1Fonts = uc($reportData->{'t1Fonts'}) || 1;
		my $LaserAdjust = uc($reportData->{'LaserAdjust'});
		my $FormDef = uc($reportData->{'FormDef'});
		my $PageDef = uc($reportData->{'PageDef'});
		my $CharsDef = uc($reportData->{'CharsDef'});
		my $ReplaceDef = uc($reportData->{'ReplaceDef'});
		my $PrintControlFile = uc($reportData->{'PrintControlFile'});

		$sql = "SELECT * FROM tbl_JobReportNames WHERE JobReportName = '$ReportName'";
		$result = _getDataFromSQLNoFill($sql);
		if(!$result) {
			$sql = 
				"INSERT INTO tbl_JobReportNames"
				." (JobReportName"
				." ,JobReportDescr"
				." ,TypeOfWork"
				." ,HoldDays"
				." ,MailTo"
				." ,ParseFileName"
				." ,HasCc"
				." ,CharsPerLine"
				." ,LinesPerPage"
				." ,PageOrient"
				." ,FontSize"
				." ,FitToPage"
				." ,TargetLocalPathId_IN"
				." ,TargetLocalPathId_OUT"
				." ,CodePage"
				." ,ReportFormat"
				." ,ElabFormat"
				." ,IsActive"
				." ,PrintFlag"
				." ,ViewOnlineFlag"
				." ,ActiveDays"
				." ,ActiveGens"
				." ,HoldGens"
				." ,StorageClass"
				." ,WorkClass)"
				." VALUES"
				." ('$ReportName'"
				." ,'$ReportDescr'"
				." ,$TypeOfWork"
				." ,$RetDays"
				." ,'$MailTo'"
				." ,'$ParseFileName'"
				." ,$HasCc"
				." ,$CharsPerLine"
				." ,$LinesPerPage"
				." ,'$PageOrient'"
				." ,'$FontSize'"
				." ,$FitToPage"
				." ,'$ArchivioInput'"
				." ,'$ArchivioOutput'"
				." ,'$CodePage'"
				." ,$ReportFormat"
				." ,$ElabFormat"
				." ,1"
				." ,'Y'"
				." ,'Y'"
				." ,5"
				." ,5"
				." ,30"
				." ,''"
				." ,$WorkClass)"
			;
			main::debug2log($sql);
			$result = _updateDataFromSQL($sql);
			$outmess .= "INSERTED JobReportName\n"; 	
#		} else {
#			die "Report $ReportName already exists(JobReportNames)";
		}

		$sql = "SELECT * FROM tbl_ReportNames WHERE ReportName = '$ReportName'";
		$result = _getDataFromSQLNoFill($sql);
		if(!$result) {
			$sql = 
				"INSERT INTO tbl_ReportNames"
				." (ReportName"
				." ,ReportDescr"
				." ,HoldDays"
				." ,MailTo"
				." ,FilterVar)"
				." VALUES"
				." ('$ReportName'"
				." ,'$ReportDescr'"
				." ,$RetDays"
				." ,'$MailTo'"
				." ,'$FilterVar')"
			;
			main::debug2log($sql);
			$result = _updateDataFromSQL($sql);
			$outmess .= "INSERTED ReportNames\n";	
#		} else {
#			die "Report $ReportName already exists(ReportNames)";
		}

		$sql = "SELECT * FROM tbl_Os390PrintParameters WHERE JobReportName = '$ReportName'";
		my $result = _getDataFromSQLNoFill($sql);
		if(!$result) {
			$sql = 
				"INSERT INTO tbl_Os390PrintParameters"
				." (JobReportName"
				." ,t1Fonts"
				." ,LaserAdjust"
				." ,FormDef "
				." ,PageDef "
				." ,CharsDef "
				." ,ReplaceDef "
				." ,PageSize "
				." ,PrintControlFile)"
				." VALUES"
				." ('$ReportName'"
				." ,$t1Fonts"
				." ,'$LaserAdjust'"
				." ,'$FormDef'"
				." ,'$PageDef'"
				." ,'$CharsDef'"
				." ,'$ReplaceDef'"
				." ,'$PageSize'"
				." ,'$PrintControlFile')"
			;
			main::debug2log($sql);
			$result = _updateDataFromSQL($sql);
			$outmess .= "INSERTED Os390PrintParameters\n";	
#		} else {
#			die "Report $ReportName already exists(OS390PrintParameters)";
		}
		### management of report group must be done in defin interface !!!
#		foreach my $ReportGroup(@ReportGroups) {
#			$sql = "SELECT * FROM tbl_NamedReportsGroups WHERE ReportGroupId = '$ReportGroup'";
#  			$result = _getDataFromSQL($sql);
#			if(!$result) {
#				$sql = 
#					"INSERT INTO tbl_NamedReportsGroups"
#					." (ReportGroupId"
#					." ,FilterVar"
#					." ,FilterRule"
#					." ,RecipientRule"
#					." ,ReportRule)"
##					." VALUES"
#					." ('$ReportGroup'"
#					." ,''"
#					." ,''"
#					." ,''"
#					." ,'$ReportGroup')"
#				;
#				main::debug2log($sql);
#				$result = _updateDataFromSQL($sql);	
#			}

#			$sql = 
#				"INSERT INTO tbl_VarSetsValues"
#				." (VarSetName"
#				." ,VarName"
#				." ,VarValue)"
#				." VALUES"
#				." ('$ReportGroup'"
#				." ,':REPORTNAME'"
#				." ,'$ReportName')"
#			;
#			main::debug2log($sql);
#			$result = _updateDataFromSQL($sql);	
#		}
		
		if($JobReportName !~ /^$ReportName$/i) {
			my $newLocalFileName = $LocalFileName;
			$newLocalFileName =~ s/$JobReportName/$ReportName/;
	        
            die "Files on PERSISTENCE STORAGE -- Cannot rename $LocalFileName to $newLocalFileName" if($LocalPath !~ /^file:\/\//i);
            $LocalPath =~ s/^file:\/\///;    
			$sql =
				"UPDATE tbl_JobReports SET JobReportName = '$ReportName', LocalFileName = '$newLocalFileName' ".
				"WHERE JobReportId = $jobReportId"
			;
			main::debug2log($sql);
			$result = _updateDataFromSQL($sql);
		        $outmess .= "UPDATED JobReports LocalFilename\n" unless ($result->eof() || !$result);
			my $oldLocalFileName = "$LocalPath/IN/$LocalFileName";
			my $newLocalFileName = "$LocalPath/IN/$newLocalFileName";
		
			die "Cannot rename $oldLocalFileName to $newLocalFileName: $!, $^E" if(!rename($oldLocalFileName, $newLocalFileName));	
		}
	};

	if($@) {
		$outmess .=  " FAILED: $@";
		_updateDataFromSQL("ROLLBACK TRANSACTION REPORTADD");
		
	} else {
		_updateDataFromSQL("COMMIT TRANSACTION REPORTADD");
		$outmess = "SUCCESS";
	}
	return ($outmess, "");
}

sub _getJobReportLog {
	my ($jobReportId, $data) = (shift, shift);
	my $actionMessage = "FAILED";
	my $result = "";

	my $log_contents;
	eval {
		my $jobReportHandle = XReport::JobREPORT->Open($jobReportId);
		my $output_archive = $jobReportHandle->get_OUTPUT_ARCHIVE();
		my $jobReportFiles = $output_archive->FileList();
		foreach(@{$jobReportFiles}) {
			my $currentFile = $_;
			next if(($currentFile !~ /log$/i) and ($currentFile !~ /stderr$/i) and ($currentFile !~ /stdout$/i));
			my $file_content = $output_archive->FileContents($currentFile);
			$log_contents->{$currentFile} = $file_content;
		}
	};
	if($@) {
		$actionMessage = "Log not available for JobReportId $jobReportId($@)";
	} else {
		main::debug2log("view_log command");
		$result =
			"Logs of JobReportId $jobReportId"
		;

		foreach my $file(keys(%{$log_contents})) {
			$file =~ s/#/__/g;
			$result .= "\n\n".$file."\n\n";
			$result .= $log_contents->{$file}."\n";
		}
		main::debug2log($result);
		$actionMessage = "SUCCESS";
	}
	return ($actionMessage, $result);
}

sub getJobReportLog {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportId = $IndexEntries->[1]->{'Columns'}->{'JobReportId'}->{'content'};
	my $result = "";

	my $log_contents;
	eval {
		my $jobReportHandle = XReport::JobREPORT->Open($JobReportId);
		my $output_archive = $jobReportHandle->get_OUTPUT_ARCHIVE();
		my $jobReportFiles = $output_archive->FileList();
		foreach(@{$jobReportFiles}) {
			my $currentFile = $_;
			next if(($currentFile !~ /log$/i) and ($currentFile !~ /stderr$/i) and ($currentFile !~ /stdout$/i));
			my $file_content = $output_archive->FileContents($currentFile);
			$log_contents->{$currentFile} = $file_content;
		}
	};
	if($@) {
		$actionMessage = "Log not available for JobReportId $JobReportId($@)";
	} else {
		main::debug2log("view_log command");
		$result =
			"Logs of JobReportId $JobReportId"
		;

		foreach my $file(keys(%{$log_contents})) {
			$file =~ s/#/__/g;
			$result .= "\n\n".$file."\n\n";
			$result .= $log_contents->{$file}."\n";
		}
		main::debug2log($result);
	
		my $buffsz = 57*76;
		$result = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $result ) );
			
		$actionMessage = "SUCCESS";
	}

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage},
				{colname => 'ViewId', content => "jobreportsMenu"}
			]
		}
	];
#	main::debug2log(Dumper($respEntries));
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries,
		DocumentType => 'text/xml',
		FileName => "xrmonitor.xml",
		Identity => $JobReportId,
		DocumentBody => {content => "<![CDATA[$result]]>"}
	}));
}

sub _getASCIIPage {
	my ($jobReportId, $data) = (shift, shift);
	my $actionMessage = "FAILED";
	my $result = "";

	my $content;
	eval {
		$content = _getInputFile($jobReportId, \&_PutLine);
	};
	if($@) {
		$actionMessage = "Content not available for JobReportId $jobReportId($@)";
	} else {
		$actionMessage = "SUCCESS";
	}

	return ($actionMessage, $content);
}

sub getASCIIPage {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportId = $IndexEntries->[1]->{'Columns'}->{'JobReportId'}->{'content'};
	my $result = "";

	my $content;
	eval {
		$content = _getInputFile($JobReportId, \&_PutLine);
	};
	if($@) {
		$actionMessage = "Content not available for JobReportId $JobReportId($@)";
	} else {
		my $buffsz = 57*76;
		$result = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $content ) );
			
		$actionMessage = "SUCCESS";
	}

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage},
				{colname => 'ViewId', content => "jobreportsMenu"}
			]
		}
	];
#	main::debug2log(Dumper($respEntries));
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries,
		DocumentType => 'text/plain',
		FileName => "asciipage.txt",
		Identity => $JobReportId,
		DocumentBody => {content => "<![CDATA[$result]]>"}
	}));
}

sub _PutLine {
	my ($page, $line) = (shift, shift);

	return ' '.substr($line->[0], 1);
}

sub _getInputFile {
	my $jobReportId = shift;
	my $outRtn = shift;
	return undef unless ref($outRtn) eq 'CODE';
	my $input_content;

	my $jobReportHandle = XReport::JobREPORT->Open($jobReportId);
	main::debug2log("JobReportId $jobReportId opened");
	my $jobReportINArchive = $jobReportHandle->get_INPUT_ARCHIVE();
	main::debug2log("JobReportINArchive opened");
	$jobReportHandle->setValues(ijrar => $jobReportINArchive, ojrar => undef, ElabContext => {});
	main::debug2log("JobReport setValues");
	my $input_handle = XReport::INPUT->new($jobReportHandle);
	main::debug2log("Input handle created");
	my $page = $input_handle->GetPage()->GetPageRectList(1,1,255,255);
	$input_content .= ($input_content ? "\x0c" : "");
	my @page_image = ();
	for ( @{$page} ) {
		my $newline = &$outRtn($page, $_);
		push @page_image, $newline;
	}
	$input_content .= join("\n", @page_image );
	$input_handle->Close();
  
	return $input_content;
}

sub _getCtrlFile {
	my ($jobReportId, $data) = (shift, shift);
	my $actionMessage = "FAILED";
	my $result = "";

	my $content;
	eval {
		my $jr = XReport::JobREPORT->Open($jobReportId);

		main::debug2log("JobReport opened");
		my ($LocalPathId_IN, $LocalFileName) = $jr->getValues(qw(LocalPathId_IN LocalFileName));
		main::debug2log("PathId_IN is $LocalPathId_IN");
		main::debug2log("LocalFileName is $LocalFileName");

		my $LocalPath = c::getValues('LocalPath')->{$LocalPathId_IN ||= "L1"};
		main::debug2log("LocalPath is $LocalPath");

		$LocalFileName = "$LocalPath/IN/".$jr->getFileName('INPUT');
		main::debug2log("LOCAL file name is $LocalFileName");
    	if($LocalFileName =~ /^\s*file:\/\/(.*)$/i) {
			my $zipFileName = $1;
      		$zipFileName =~ s/\.DATA\./\.CNTRL\./i;
		    $zipFileName =~ s/\.gz$//i;
			my $zipHandle = gensym();
			main::debug2log("GZ file name is $zipFileName");
			open($zipHandle, "<$zipFileName");
			binmode($zipHandle);
			while(<$zipHandle>) {
				$content .= $_;
			}
			close($zipHandle);
		}
	};
	if($@) {
		$actionMessage = "Content not available for JobReportId $jobReportId($@)";
	} else {
		$actionMessage = "SUCCESS";
	}

	return ($actionMessage, $content);
}

sub getCtrlFile {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportId = $IndexEntries->[1]->{'Columns'}->{'JobReportId'}->{'content'};
	my $result = "";

	my $content;
	eval {
		$content = _getInputControlFile($JobReportId, \&_PutLine);
	};
	if($@) {
		$actionMessage = "Content not available for JobReportId $JobReportId($@)";
	} else {
		my $buffsz = 57*76;
		$result = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $content ) );
			
		$actionMessage = "SUCCESS";
	}

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage},
				{colname => 'ViewId', content => "jobreportsMenu"}
			]
		}
	];
#	main::debug2log(Dumper($respEntries));
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries,
		DocumentType => 'text/plain',
		FileName => "ctrlfile.txt",
		Identity => $JobReportId,
		DocumentBody => {content => "<![CDATA[$result]]>"}
	}));
}

sub _getInputControlFile {
  my $jobReportId = shift;
  my $outRtn = shift;
  return undef unless ref($outRtn) eq 'CODE';
  
  my $input_content;
  
  eval {
    my $jobReportHandle = XReport::JobREPORT->Open($jobReportId);
    my $InGZObject = $jobReportHandle->get_INPUT_ARCHIVE();
    if($InGZObject->getFileName() =~ /^\s*file:\/\/(.*)$/i) {
      my $ctrl_file = $1;
      $ctrl_file =~ s/\.DATA\./\.CNTRL\./i;
      $ctrl_file =~ s/\.gz$//i;
      my $ctrl_handle = gensym();
      open($ctrl_handle, "<$ctrl_file") or die("Cannot open $ctrl_file: $!");
      while(<$ctrl_handle>) {
	my $newline = &$outRtn($_);
	$input_content .= $newline."<br>\n";
      }
      close($ctrl_handle);
    }
  };
  main::debug2log($@) if($@);
  
  return $input_content;
}

sub _getHEXPage {
	my ($jobReportId, $data) = (shift, shift);
	my $actionMessage = "FAILED";
	my $result = "";

	my $content;
	eval {
		$content = _getInputFile($jobReportId, \&_PutHexLine);
	};
	if($@) {
		$actionMessage = "Content not available for JobReportId $jobReportId($@)";
	} else {
		$actionMessage = "SUCCESS";
	}

	return ($actionMessage, $content);
}

sub getHEXPage {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportId = $IndexEntries->[1]->{'Columns'}->{'JobReportId'}->{'content'};
	my $result = "";

	my $content;
	eval {
		$content = _getInputFile($JobReportId, \&_PutHexLine);
	};
	if($@) {
		$actionMessage = "Content not available for JobReportId $JobReportId($@)";
	} else {
		my $buffsz = 57*76;
		$result = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $content ) );
			
		$actionMessage = "SUCCESS";
	}

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage},
				{colname => 'ViewId', content => "jobreportsMenu"}
			]
		}
	];
#	main::debug2log(Dumper($respEntries));
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries,
		DocumentType => 'text/plain',
		FileName => "asciipage.txt",
		Identity => $JobReportId,
		DocumentBody => {content => "<![CDATA[$result]]>"}
	}));
}

sub _PutHexLine {
	my ($page, $line) = (shift, shift);

	my $rawline = substr($line->[0], 1);
	$line = join(" ", unpack((length($rawline) > 8 ? "(H8)*" : "H*"), $rawline)); 
	return "$line\n";
}

sub downloadInput {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	my $JobReportId = $IndexEntries->[1]->{'Columns'}->{'JobReportId'}->{'content'};
	#my $JobReportId = $IndexEntries->[0]->{'Columns'}->{'JobReportId'}->{'content'};
	my $result = "";

    ($actionMessage, my $content) = _downloadInput($JobReportId, $IndexEntries->[1]);
    #($actionMessage, my $content) = _downloadInput($JobReportId, $IndexEntries->[0]);
    if ( $actionMessage eq "SUCCESS" ) {
		
		my $buffsz = 57*76;
		$result = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $content ) );
			
		$actionMessage = "SUCCESS";
	}
	

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
#	main::debug2log(Dumper($respEntries));
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries,
		DocumentType => 'application/x-tar',
		FileName => "$JobReportId.IN.gz.tar",
		Identity => $JobReportId,
		DocumentBody => {content => $result}
	}));
	
	
	 
}

sub _xxxdownloadInput {
	my ($jobReportId, $data) = (shift, shift);
	my $actionMessage = "FAILED";

    my $jr;
    eval {
        $jr = XReport::JobREPORT::get_OUTPUT_ARCHIVE($jobReportId);
        #$jr->Open();
    };
    my $openrc = $@;
    if($openrc) {
        $actionMessage = "Unable to open Output Archive for JobReportId $jobReportId($openrc)";
        return ($actionMessage, '');
    } 

    my $content;
    my $dest = IO::String->new($content);
    eval {
		my $jr = XReport::JobREPORT->Open($jobReportId);

		main::debug2log("JobReport opened");
		my ($LocalPathId_IN, $LocalFileName) = $jr->getValues(qw(LocalPathId_IN LocalFileName));
		main::debug2log("PathId_IN is $LocalPathId_IN");
		main::debug2log("LocalFileName is $LocalFileName");

		my $LocalPath = c::getValues('LocalPath')->{$LocalPathId_IN ||= "L1"};
		main::debug2log("LocalPath is $LocalPath");

		$LocalFileName = "$LocalPath/IN/".$jr->getFileName('INPUT');
		main::debug2log("LOCAL file name is $LocalFileName");
	    	if($LocalFileName =~ /^\s*file:\/\/(.*)$/i) {
			my $zipFileName = $1;
			my $zipHandle = gensym();
			main::debug2log("GZ file name is $zipFileName");
			open($zipHandle, "<$zipFileName");
			binmode($zipHandle);
			while(<$zipHandle>) {
				$content .= $_;
			}
			close($zipHandle);
		}
	};
	if($@) {
		$actionMessage = "Content not available for JobReportId $jobReportId($@)";
	} else {
		$actionMessage = "SUCCESS";
	}

	return ($actionMessage, $content);
}

sub downloadOutput {
	my ($method, $reqident, $selection) = (shift, shift, shift);
	my $actionMessage = "FAILED";
	my $IndexEntries = @{$selection->{request}}{qw(IndexEntries)};
	#my $JobReportId = $IndexEntries->[0]->{'Columns'}->{'JobReportId'}->{'content'};
	my $JobReportId = $IndexEntries->[1]->{'Columns'}->{'JobReportId'}->{'content'};
	my $result = "";
    #($actionMessage, my $content) = _downloadOutput($JobReportId, $IndexEntries->[0]);
    ($actionMessage, my $content) = _downloadOutput($JobReportId, $IndexEntries->[1]);
	if ( $actionMessage eq "SUCCESS" ) {
		my $buffsz = 57*76;
		$result = join('', map { XReport::SOAP::_encode_base64($_) } unpack("(a$buffsz)*", $content ) );
			
		$actionMessage = "SUCCESS";
	}

	my $respEntries = [
		{
			Columns => [
				{colname => 'ActionMessage', content => $actionMessage}
			]
		}
	];
#	main::debug2log(Dumper($respEntries));
	$main::Response->Write(XReport::SOAP::buildResponse({
		IndexName => 'XREPORTWEB',
		IndexEntries => $respEntries,
		DocumentType => 'application/zip',
		FileName => "$JobReportId.OUT.zip",
		Identity => $JobReportId,
		DocumentBody => {content => $result}
	}));
}

use IO::String qw();
use Archive::Zip qw();
sub _downloadInput { 
	my ($jobReportId, $data) = (shift, shift);
	my $actionMessage = "FAILED";

    my $jr;
	eval {
        $jr = XReport::JobREPORT->Open($jobReportId,0);
        #$jr->Open();
    };
    my $openrc = $@;
    if($openrc) {
        $actionMessage = "Unable to open Input Archive for JobReportId $jobReportId($openrc)";
        return ($actionMessage, '');
    } 

    my $content;
    my $dest = IO::String->new($content);
    eval {
        main::debug2log("JobReport $jobReportId Output archive opened");
		my $INPATH = $jr->getValues('$INPATH') or 'none';
		if($INPATH !~ /^centera:\/\//i )
		{
			$jr->setValues('line_iexec' => ''); 
			my $INPUT = $jr->get_INPUT_ARCHIVE(); 
			die "Unable to acquire INPUT handler -", Data::Dumper::Dumper($jr), "\n" unless $INPUT;
			my $is = $INPUT->get_INPUT_STREAM_GZ($jr, random_access => 0);  
			i::logit("INPUT STREAM acquired - INPUT REF: ".ref($INPUT)." IS ref: ".ref($is)."_Line[". __LINE__."]");
			i::logit("INPUT STREAM from local storage"."_Line[". __LINE__."]");
			$is->Open();
			my $buff = ' ' x 131072;
			while(1) { $is->read($buff, 131072); last if $buff eq ''; $dest->syswrite($buff); }
			$is->Close();
			$dest->close();
		}
		else
		{
			main::debug2log("INPUT STREAM from Centera"."_Line[". __LINE__."]"); 
			my $ijrar = $jr->get_INPUT_ARCHIVE();  
			die "Unable to acquire INPUT handler -", Data::Dumper::Dumper($jr), "\n" unless $ijrar; 
			my $pool = "";
			my $cdamfh = $ijrar->get_INPUT_ARCHIVE(\$pool);  
		
			my $clip = $cdamfh->{'clip'}; 
 
			my $FileTags = $cdamfh->FileTags();  
			
			foreach my $hash (@$FileTags)
			{  
				foreach my $key (sort keys %$hash)
				{ 
					if(( $key =~ /^md5$/i) && (!($hash->{$key})) )
					{
						$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 1;
						main::debug2log("noCheckMd5 on XReport::ARCHIVE::Centera::File::INPUT.pm"); 
					}
				}
			}
			
			$XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5 = 1;
			main::debug2log("INPUT STREAM from Centera"."_Line[". __LINE__."]");
			my $FileList = $cdamfh->FileList();
			foreach my $iFileName (grep /\.DATA\.TXT\.gz\.tar$/i, @$FileList) {
				main::debug2log("iFileName: ".$iFileName."_Line[". __LINE__."]");
				my $is = $cdamfh->LocateFile($iFileName);  
				$is->Open();  
				my $osize = $cdamfh->ExtractMember2FH($is, $dest);
				main::debug2log("osize: ".$osize); 
				$is->Close();
				$dest->close; 
				last;
			}
		}
    };    
	if($@) {
		$actionMessage = "Content not available for JobReportId $jobReportId($@)";
	} else {
		$actionMessage = "SUCCESS";
	}

	return ($actionMessage, $content);
}

sub _downloadOutput {
    my ($jobReportId, $data) = (shift, shift);
    my $actionMessage = "FAILED";
    my $result = "";

    my $content;
    my $dest = IO::String->new($content);
    my $jr;
    eval {
        $jr = XReport::JobREPORT::get_OUTPUT_ARCHIVE($jobReportId);
        #$jr->Open();
    };
    my $openrc = $@;
    if($openrc) {
        $actionMessage = "Unable to open Output Archive for JobReportId $jobReportId($openrc)";
        return ($actionMessage, '');
    } 
    eval {
        main::debug2log("JobReport $jobReportId Output archive opened");
#        my $tar = Archive::Tar::Stream->new(outfh => $dest);
        my $ozip = new Archive::Zip;
        my $FileList = $jr->FileList();
        my $osize = 0;
        foreach my $iFileName (@$FileList) {
           my $INPUT = $jr->LocateFile($iFileName);
           die("ExtractFile output archive $iFileName NOT Located") if !$INPUT; 

           my $member = ''; 
           my $OFH = new IO::String($member);

           $INPUT->Open();
           my $buff = ' ' x 131072;
           while(1) { $INPUT->read($buff, 131072); last if $buff eq ''; $OFH->syswrite($buff); }
           $INPUT->Close();
           $OFH->close();

           my $string_member = $ozip->addString( $member, $iFileName );
           $string_member->desiredCompressionMethod( COMPRESSION_DEFLATED );
#           my $isize = $INPUT->uncompressedSize();
#           my $tarhdr = $tar->CreateHeader($tar->BlankHeader(
#                            name => $iFileName,
#                            size => $isize,
#                            ));
#           $dest->syswrite($tarhdr);
#           my $wsize = $self->ExtractFile2FH($INPUT, $dest);
#           my $blockfiller = "\x00" x (512 - ($wsize % 512));
#           $osize += $wsize;
#           $dest->syswrite($blockfiller) if length($blockfiller) && length($blockfiller) != 512;    
        }
#    $dest->syswrite("\x00" x 512);
     $ozip->writeToFileHandle($dest);
#     $ozip->Close();
     $dest->close();
#        main::debug2log("JobReport opened");
#        my ($LocalPathId_OUT, $LocalFileName) = $jr->getValues(qw(LocalPathId_OUT LocalFileName));
#        main::debug2log("PathId_OUT is $LocalPathId_OUT");
#        main::debug2log("LocalFileName is $LocalFileName");
#
#        my $LocalPath = c::getValues('LocalPath')->{$LocalPathId_OUT ||= "L1"};
#        main::debug2log("LocalPath is $LocalPath");

#        $LocalFileName = "$LocalPath/OUT/".$jr->getFileName('ZIPOUT');
#        main::debug2log("LOCAL file name is $LocalFileName");
#         if($LocalFileName =~ /^\s*file:\/\/(.*)$/i) {
#            my $zipFileName = $1;
#            my $zipHandle = gensym();
#            main::debug2log("ZIP file name is $zipFileName");
#            open($zipHandle, "<$zipFileName");
#            binmode($zipHandle);
#            while(<$zipHandle>) {
#                $content .= $_;
#            }
#            close($zipHandle);
#         }
    };
    if($@) {
        $actionMessage = "Content not available for JobReportId $jobReportId($@)";
    } else {
        $actionMessage = "SUCCESS";
    }

    return ($actionMessage, $content);
}
1;
