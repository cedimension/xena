#/usr/bin/perl -w

use strict "vars";

use Data::Dumper;#$Data::Dumper::Terse = 1; $Data::Dumper::Indent = 1;
#use SOAP::Trace;
#use SOAP::Lite +trace;
use SOAP::Lite;

use MIME::Base64;
use XML::Simple;

sub xtractParm {
  my ($struct, $parm) = (shift, shift);
  my $result = undef;
  if (ref($struct) eq 'HASH') {
    foreach my $k ( keys %$struct ) {
      return  $struct->{$k} if $k =~ /^$parm$/i;
      $result = xtractParm($struct->{$k}, $parm);
      return $result if $result;
    }
  }
  elsif (ref($struct) eq 'ARRAY') {
    foreach my $el ( @$struct ) {
      $result = xtractParm($el, $parm);
      return $result if $result;
    }
  }
  return undef;
}

#my $PageList = SOAP::Lite->new({
#			       service => 'file://N2391989/xrsitiweb_cap/webservice/getDocument.wsdl'
#			      });
#my $soap =  SOAP::Lite
#  ->service('http://N239199b.bipop.it/CeWebService/getDocument.asp?wsdl');
   #, options => {location => 'http://N239199b.bipop.it/CeWebService/getDocument.asp'}) ;

#  ->service('file://N2391989/xrsitiweb_cap/webservice/getDocument.wsdl')

#print ref($soap->{_transport}->proxy()), "\n===================\n";
#$soap->{_transport}->proxy('http://N239199b.bipop.it/CeWebService/getDocument.asp');
#print $soap->{_transport}->proxy(), "\n===================\n";

#my $PageList = $soap->getDocBySel( {DocVarSet => [{VarName => 'pippo', VarValue => 'Pluto'}]},  );
#  ;

#my$PageListRes = $PageList
#  -> getDocBySel( {DocVarSet => [{VarName => 'pippo', VarValue => 'Pluto'}]} );
;#

#my $endp = $soap->proxy('http://N239199b.bipop.it/CeWebService/getDocument.asp');
#my $PageList = $srvc->getStateName(1)->result;
#my $PageList = SOAP::Lite 
# -> service('file://N2391989/xrsitiweb_cap/webservice/getDocument.wsdl')
# -> proxy('http://N239199b.bipop.it/CeWebService/getDocument.asp')
# -> getDocBySel( {DocVarSet => [{VarName => 'pippo', VarValue => 'Pluto'}]} );
# -> getPagesByIdx( {KeyList => ['LETCC_BPOP', "DOCFLDS LIKE '____30%538'"]} );
;

#print Data::Dumper::Dumper($PageList);
#print Data::Dumper::Dumper($PageList->fault());

#exit 0;

my $soap = SOAP::Lite 
  ->service('http://'.$ARGV[0].'/getDocument.asp?wsdl')
#  ->envprefix('xsd1')
#  ->encprefix('soapenc')
;


#my $SELECT = {Query => {
#			TOP => 5,
#			TableName => 'LETTCC_BPOP',
#			WHERE => { Row => [ {Key => [{Var => 'CDG', Type => 'S', Min => '193687'}, 
#						     {Var => 'DATA_RIF', Type => 'D', Min => '2005-01-01', Max => '2005-12-01'}, 
#						     {Var => 'JobReportId', Type => 'N', Min => 64, Max => '64'}, 
#						    ], },
#					    {Key => [{Var => 'DATARIF', Type => 'D', Min => '2005-01-01', Max => '2005-12-01'}, 
#						     {Var => 'JobReportId', Type => 'N', Min => 64, Max => '64'}, 
#						    ], },
#					  ], 
#				    },
#		       },
#	     };

my $SELECT = {Query => {
			TOP => 2,
			TableName => 'LETTCC_BPOP',
			WHERE => [ 
				  {Row => [ {Key => {Var => 'DOCFLDS', Type => 'S', Min => '193687'}}, 
					    {Key => {Var => 'DATARIF', Type => 'D', Min => '2005-01-01', Max => '2005-12-01'}}, 
					    {Key => {Var => 'JobReportId', Type => 'N', Min => 17366, Max => 17366}}, 
					  ], 
				  },
				  {Row => [
					   
					    {Key => {Var => 'JobReportId', Type => 'N', Min => 17366, Max => 17366}}, 
					  ], 
				  },
				 ]
		       },
	     };

my $lop = $soap
  ->new(namespace => 'soapenv',)
  ->encodingspace(undef)
  ->outputxml(1)
  ->encprefix(undef)
  ->getDocList(  SOAP::Data->type('xml' => 
				  XMLout($SELECT, 
					 keeproot => 1, 
					 NoAttr => 1, 
					 KeyAttr => [qw(Key)], NoIndent => 1))
	      );

my $DList = xtractParm(XMLin($lop, NoAttr=>1), 'Document');


my $lod = {ListOfDocuments => {Document => []}};
foreach my $doc ( @$DList ) {
  my $docel = {};
  @{$docel}{qw(JobReportId FromPage ForPages)} = @{$doc}{qw(JobReportId FromPage ForPages)};
  push @{$lod->{ListOfDocuments}->{Document}}, $docel;
  print "DOC: ", join(', ', (@{$doc}{qw(JobReportId FromPage ForPages)}, @{$doc->{KeyList}->{Key}->[0]}{qw(Var Min)})), "\n";
}
print "====>\n", Dumper($lod), "\n==========\n";

my $pdfFile = $soap
  ->new(namespace => 'soapenv',)
  ->encodingspace(undef)
  ->outputxml(1)
  ->encprefix(undef)
  ->getDocById(  SOAP::Data->type('xml' => 
				  XMLout($lod, 
					 keeproot => 1, 
					 NoAttr => 1, 
					 KeyAttr => [qw(Document)], 
					 NoIndent => 1))
	      );

open(PDFDOC,">BancaDoc.pdf"); binmode(PDFDOC);

my $buffer = MIME::Base64::decode(XMLin($pdfFile, NoAttr => 1)->{'soapenv:Body'}->{'xsd1:getDocByIdResponse'}->{'PdfElem'}->{'Pdf'});
print PDFDOC $buffer;

close PDFDOC;

exit 0;
__END__

#<ListOfDocuments>
#  <Document>
#  <JobReportId>415</JobReportId>
#  <FromPage>1</FromPage>
#  <ForPages>1</ForPages>
#  </Document>
#  <Document>
#  <JobReportId>415</JobReportId>
#  <FromPage>2</FromPage
#  <ForPages>1</ForPages>
#  </Document>
#</ListOfDocuments>
#<ListOfDocuments>
#  <Document>
#    <ForPages>3</ForPages>
#    <FromPage>1</FromPage>
#    <JobReportId>64</JobReportId>
#  </Document>
#</ListOfDocuments>
#<ListOfDocuments>
#  <Document>
#    <ForPages>3</ForPages>
#    <FromPage>1</FromPage>
#    <JobReportId>64</JobReportId>
#  </Document>
#</ListOfDocuments>
my $lod = {ListOfDocuments =>  
			       {Document => [{
					      JobReportId => 17366, 
					      FromPage => 1, 
					      ForPages => 3,
					     },
					     {JobReportId => 17366, 
					      FromPage => 4, 
					      ForPages => 1,
					     }
					    ]
			       },
			      
	  };
#my $lod =  [ 
#	    {Document => {
#			  JobReportId => 64, 
#			  FromPage => 1, 
#			  ForPages => 3,
#			  KeyList => [
#				      {Key => {Var => 'JobReportId', Type => 'N', Min => 64, Max => 65}}, 
#				     ], 
#			 }
#	    },
#	    {Document => {
#			  JobReportId => 65, 
#			  FromPage => 4, 
#			  ForPages => 1,
#			  KeyList => [
#				      {Key => {Var => 'JobReportId', Type => 'N', Min => 65, Max => 65}}, 
#				     ], 
#			 }
#	    },
#	   ],
#  ;

my $pdfFile = $soap
  ->new(namespace => 'soapenv',)
  ->encodingspace(undef)
  ->outputxml(1)
  ->encprefix(undef)
  ->getDocById(  SOAP::Data->type('xml' => 
				  XMLout($lod, 
					 keeproot => 1, 
					 NoAttr => 1, 
					 KeyAttr => [qw(Document)], 
					 NoIndent => 1))
	      );

#print length($pdfFile->result()), " ===> \n";
#print "pdfFile =>>\n", Dumper([keys %{ ${ XMLin($pdfFile, NoAttr => 1)}->{'soapenv:Body'}} ]), "\n";
#exit 0;
#print $pdfFile, "\n";


open(PDFDOC,">BancaDoc.pdf"); binmode(PDFDOC);

#foreach my $item ( sort keys %$pdfFile ) {
  my $buffer = MIME::Base64::decode(XMLin($pdfFile, NoAttr => 1)->{'soapenv:Body'}->{'xsd1:getDocByIdResponse'}->{'PdfElem'}->{'Pdf'});
  print PDFDOC $buffer;
#  print length($pdfFile->result()), " ===> ", length($buffer), "\n";;
#}

#print Data::Dumper::Dumper($pdfFile->result());
#print Data::Dumper::Dumper(*BancaDoc::elencaEstrattiContoDisponibili);
close PDFDOC;
exit 0;

__END__

exit 0;

__END__
 -> service('file:./getDocument.wsdl')

 -> getDocByList({
	        Document => 'ccindex',
                KeyArray => [{key => 64, FromPage => 1, ForPages => 3}],
              })
;

<Document val�ue="ccindex"><keyarray 
