<?xml version="1.0" ?> 
<xsl:stylesheet 
  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:soapenv="uri:skip" xmlns:ns1="uri:skip" xmlns:ns2="uri:skip" xmlns:xsi="uri:skip">

  <xsl:template match="/">
   <soapenv:Envelope 
     xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"
     xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
     xmlns:xsd1="urn:getDocument.xsd1"
     xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <soapenv:Body>
       <xsl:apply-templates />
     </soapenv:Body>
   </soapenv:Envelope>

  </xsl:template>

  <xsl:template match="elencaEstrattiContoDisponibiliResponse">

    <ns1:elencaEstrattiContoDisponibiliResponse 
       soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" 
       xmlns:ns1="http://webservices.bipop.it/BancaDoc">
     <result href="#id0"/>
    </ns1:elencaEstrattiContoDisponibiliResponse>

    <multiRef id="id0" soapenc:root="0" 
      soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" 
      xsi:type="ns2:EstrattiContoDisponibiliResult" 
      xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" 
      xmlns:ns2="http://webservices.bipop.it/BancaDoc.xsd1">

     <codFiliale xsi:type="xsd:string"><xsl:value-of select="codFiliale"/></codFiliale>
     <codContoCorrente xsi:type="xsd:string"><xsl:value-of select="codContoCorrente"/></codContoCorrente>
     <estrattiConto xsi:type="soapenc:Array">
      <xsl:attribute name="soapenc:arrayType">ns2:EstrattoContoDisponibile[<xsl:value-of select="count(./ec)"/>]</xsl:attribute>
       <xsl:for-each select="ec">
        <item>
         <xsl:attribute name="href">#id<xsl:value-of select="position()"/></xsl:attribute>
        </item>
       </xsl:for-each>
     </estrattiConto>

    </multiRef>

    <xsl:for-each select="ec">
     <multiRef soapenc:root="0" 
        soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" 
        xsi:type="ns3:EstrattoContoDisponibile" 
        xmlns:ns3="http://webservices.bipop.it/BancaDoc.xsd1" 
        xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
       <xsl:attribute name="id">id<xsl:value-of select="position()"/></xsl:attribute>

       <coordinate xsi:type="xsd:string"><xsl:value-of select="@refList"/></coordinate>
       <data xsi:type="xsd:date"><xsl:value-of select="@dateRef"/></data>
       <numpag xsi:type="xsd:short"><xsl:value-of select="@totPages"/></numpag>
     </multiRef>
    </xsl:for-each>

  </xsl:template>


  <xsl:template match="DocByIdResponse">

   <xsd1:getDocByIdResponse 
      soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" 
      xmlns:xsd1="urn:getDocument.xsd1">

     <PdfElem>
       <Pdf xsi:type="xsd:base64Binary"><xsl:value-of select="PdfStream"/></Pdf>
     </PdfElem>
   </xsd1:getDocByIdResponse>

  </xsl:template>

  <xsl:template match="DocListResponse">
    
    <xsd1:getDocListResponse 
      soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd1="urn:getDocument.xsd1">

    <getDocList xmlns="uri:skip">
      
      <!-- ListOfDocuments xsi:type="xsd1:DocumentsList" >
        <xsl:attribute name="xsi:arrayType">xsd1:LogicalReport[<xsl:value-of select="count(./LogicalReport)"/>]</xsl:attribute -->
        
        <xsl:for-each select="LogicalReport">
          <Document >
            <xsl:attribute name="xsi:type">xsd1:LogicalReport</xsl:attribute>
            <JobReportId xsi:type="xsd:int"><xsl:value-of select="@JobReportId"/></JobReportId>
            <FromPage xsi:type="xsd:int"><xsl:value-of select="@FromPage"/></FromPage>
            <ForPages xsi:type="xsd:int"><xsl:value-of select="@ForPages"/></ForPages>

            <KeyList xsi:type="xsd1:SELRow">
              <xsl:attribute name="xsi:arrayType">xsd1:COLAttrs[<xsl:value-of select="count(./COL)"/>]</xsl:attribute>

              <xsl:for-each select="COL">
                <Key >
                  <xsl:attribute name="xsi:type">xsd1:COLAttrs</xsl:attribute>
                  <Var xsi:type="xsd:string"><xsl:value-of select="@Var"/></Var>
                  <Min xsi:type="xsd:string"><xsl:value-of select="@Min"/></Min>
                  <Type xsi:type="xsd:string"><xsl:value-of select="@Type"/></Type>
                  <Max xsi:type="xsd:string"><xsl:value-of select="@Max"/></Max>
                </Key>
              </xsl:for-each>
              
            </KeyList>
          </Document>
        </xsl:for-each>

      <!-- /ListOfDocuments -->

    </getDocList>
   </xsd1:getDocListResponse>
    
  </xsl:template>

  <xsl:template match="faultResponse">
    <soapenv:Fault>
     <faultcode><xsl:value-of select="@faultcode"/></faultcode>
     <faultstring><xsl:value-of select="@faultstring"/></faultstring>
    </soapenv:Fault>
  </xsl:template>

</xsl:stylesheet>
