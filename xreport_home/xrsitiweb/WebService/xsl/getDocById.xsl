<?xml version="1.0" ?> 
<xsl:stylesheet 
  version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:soapenv="uri:skip" xmlns:ns1="uri:skip" xmlns:ns2="uri:skip" xmlns:xsi="uri:skip">

  <xsl:template match="/">
   <soapenv:Envelope 
      xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
      xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Body>
     <xsl:apply-templates />
    </soapenv:Body>
   </soapenv:Envelope>

  </xsl:template>

  <xsl:template match="DocByIdResponse">

   <ns1:XRWebsrvc 
      soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" 
      xmlns:ns1="XRWebsrvc">

    <Pdf xsi:type="xsd:base64Binary"><xsl:value-of select="PdfStream"/></Pdf>

   </ns1:XRWebsrvc>

  </xsl:template>

  <xsl:template match="faultResponse">
    <soapenv:Fault>
     <faultcode><xsl:value-of select="@faultcode"/></faultcode>
     <faultstring><xsl:value-of select="@faultstring"/></faultstring>
    </soapenv:Fault>
  </xsl:template>

</xsl:stylesheet>
