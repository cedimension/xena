#!/usr/bin/perl -w

use strict;
use FileHandle;
use XML::Simple;

use LWP::UserAgent;
use LWP::MediaTypes qw(media_suffix add_type);
add_type("text/xml" => qw(xml));
my $ua = LWP::UserAgent->new;
my $URL = 'http://'.$ARGV[0].'/getRawDocById.asp';

my $bytes_received = 0;

$ua->default_headers->push_header('Documents' => '<REQUEST><Document JobReportId="$ARGV[1]" FromPage="1" ForPages="4" /></REQUEST>') ;
my $res = $ua->post($URL
		    , Content_Type => 'text/xml' 
		    , Content => XMLout({REQUEST => {Document => [{JobReportId => $ARGV[1], FromPage => 1, ForPages => 4 },
#								  {JobReportId => 1362, FromPage => 3, ForPages => 2 },
								 ],
						    }
					}, KeepRoot=>1 )
);


my $data = $res->content();
my $outfn = "rawDocRequested.". media_suffix($res->content_type());
my $fh = new FileHandle(">$outfn");
binmode $fh;
$fh->syswrite($data, length($data));
close $fh;

print $res->status_line, " - Received ", length($data), " bytes in \"$outfn\"\n";

__END__



























my $res =
  $ua->request(HTTP::Request->new(POST => $URL),
               sub {
		 my($chunk, $res) = @_;
		 my $fh;
		 unless ($main::bytes_received) {
		   $fh = new FileHandle(">Requested.pdf");
		   binmode $fh;
		 }
		 unless (defined $main::expected_length) {
		   $main::expected_length = $res->content_length || 0;
		 }
		 if ($main::expected_length) {
		   printf STDERR "%d%% - ",
		     100 * $main::bytes_received / $main::expected_length;
		 }
		 
		 $main::bytes_received += length($chunk);
		 
		 print STDERR "$main::bytes_received received bytes received\n";
		 
		 # XXX Should really do something with the chunk itself
		 print $fh $chunk;
	       });

print $res->status_line, "\n";

