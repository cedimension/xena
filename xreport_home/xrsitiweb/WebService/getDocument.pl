#!/usr/bin/perl -w





use strict "vars";

use FileHandle;
use Data::Dumper;

use Win32::OLE qw(in);
use Win32::OLE::Variant;

#use XReport::DBUtil;
#use XReport::XMLUtil qw(SQL2XML);
use MIME::Base64;

our ($Server, $Application, $Session, $Request, $Response, $XREPORT_HOME);
$XREPORT_HOME = $ENV{'XREPORT_HOME'};

my $reader  = Win32::OLE->new("MSSOAP.SoapReader30");
my $xmlmgr  = Win32::OLE->new("MSXML2.DOMDocument.4.0"); $xmlmgr->{"async"} = 0; 
my $xsltmgr = Win32::OLE->new("MSXML2.DOMDocument.4.0"); $xsltmgr->{"async"} = 0; 

#my $parser = Win32::OLE->new("MSSOAP.WSDLReader30");
$| = 1;

$Response = new FileHandle '>>getDocument.response';
sub FileHandle::AddHeader {my $self = shift; $self->print("hdr:", @_);}
sub FileHandle::End {my $self = shift; $self->close()};
 
sub xmlquote {
  my $t = shift; $t =~ s/ +$//g;
  
  $t =~ s/&/&amp;/g;
  $t =~ s/</&lt;/g;
  $t =~ s/>/&gt;/g;
  $t =~ s/\"/&quot;/g;
  $t =~ s/\'/&apos;/g;
  
  $t =~ s/([\x80-\xff])/"\&\#".unpack("C",$1).";"/eg;
  
  return $t;
}


sub xtractParm {
  my ($struct, $parm) = (shift, shift);
  my $result = undef;
  if (ref($struct) eq 'HASH') {
    foreach my $k ( keys %$struct ) {
      return  $struct->{$k} if $k =~ /^$parm$/i;
      $result = xtractParm($struct->{$k}, $parm);
      return $result if $result;
    }
  }
  elsif (ref($struct) eq 'ARRAY') {
    foreach my $el ( @$struct ) {
      $result = xtractParm($el, $parm);
      return $result if $result;
    }
  }
  return undef;
}

sub ReqStruct {
  my $array; # = [];
  my $node = shift;
  my $daddy = $node->baseName();
  my $nspace = $node->prefix();
  my $tname = $node->tagName;
  print "node:". substr($node->NodeTypeString()." type: ". $daddy ." ns: $nspace tag: $tname ". "-" x 150, 0, 150), "\n";
  
  my $childs = $node->childNodes();
  for my $z ( 0..($childs->length()-1) ) {
    my $child = $childs->Item($z);
    my $type = $child->NodeTypeString();
    (my $ctag) = (split(/:+/, $child->tagName()))[-1] unless ($type =~ /^text/i);
    $ctag = $daddy if ($type =~ /^text/i);
    print "($z)", $child->prefix, '==>',$child->NodeName(), " (Type: $type) tag: $ctag value: ", $child->text(), "\n" if $type =~ /^text/i;
    if ($type !~ /^text/i) {
      if ( $ctag =~ /^item/i ) {
	push @{$array}, ReqStruct($child) ;
      }
      else {
	$array->{$ctag} = ReqStruct($child) ;
      }
    }
    elsif ($ctag !~ /^item/i) {
      $array = $child->text();
    }
    else {
      push @{$array->{$daddy}}, $child->text();
    }
  }
  print "-" x 150, "\n";
  return $array;
}

sub EXIT_ERROR {
  my $respmsg = "<faultResponse faultcode=\"".xmlquote($_[0])."\" faultstring=\"".xmlquote($_[1])."\"/>";
  
  $Response->{Status} = "500 Server Error"; $Response->AddHeader("Connection", "close");
  
  if ( !$xmlmgr->loadXML($respmsg)  ) {
    $Response->Write( $respmsg ); 
    $Response->End();
    return;
  }
      
  my $xmlout = ($xsltmgr ? $xmlmgr->transformNode($xsltmgr) : $respmsg);

  $Response->Write( $xmlout ); 
  $Response->End();
  return;
   
}

sub getDocBySelect {
  EXIT_ERROR("soapenv:Server", "getDocByVarList non ancora implementato");
}

sub getDocById {
   my ($pageList, $ExtractList, @PAGE_LIST, $pdf, $pdf64);
   my $reqdata = shift;
   print Dumper($reqdata), "\n";

  @PAGE_LIST = map { @{$_}{qw(JobReportId FromPage ForPages)} } @{ xtractParm($reqdata, 'PageList') };

#  require XReport::EXTRACT; require MIME::Base64; 
  
#  my $query = XReport::EXTRACT->new();

#  sub FileHandle::write {my $self = shift; my $databuff = join('', @_); $bytecnt += length($databuff); print($databuff);}
#  sub FileHandle::tell {return $bytecnt;}

#  my $outfile = new FileHandle "../extractions/test.pdf", "w" || die "Unable to open outpdf - $!\n";
#  my $outfile = new FileHandle || die "Unable to open outpdf - $!\n";

#  $outfile->open(">../extractions/test.pdf");
#  binmode($outfile);

#  my $docTo = XReport::PDF::DOC->Create($outfile) ;
#  $query->{'docTo'} = $docTo;
  
   EXIT_ERROR("soapenv:Server", "XML LOADXML ERROR: ". $xmlmgr->parseError()->reason())
     if
       !$xmlmgr->loadXML("<DocByIdResponse><PdfStream>PDF GOES HERE</PdfStream></DocByIdResponse>");
   
   (my $xmlout = $xmlmgr->transformNode($xsltmgr)) =~ s/\s[\w:]+="uri:skip"//g;
   my ($xmlhdr, $xmltail) = $xmlout =~ /^(.*)PDF GOES HERE(.*)$/;
   print $xmlhdr;
   print Dumper({    TO_DIR => $Application->{"cfg.workdir"},
		     QUERY_TYPE => 'FROMPAGES',
		     PAGE_LIST => \@PAGE_LIST,
		     FORMAT => "PRINT",
		     REQUEST_ID => 1, #$Session->SessionID(),
		     ENCRYPT => "YES"}
	       ), "\n";
   print $xmltail;
   
   
   return ("");
   
##  my $elist = $query->ExtractPages(
#				   TO_DIR => "../extractions",
#				   QUERY_TYPE => 'FROMPAGES', 
#				   PAGE_LIST => \@PAGE_LIST,
#				   FORMAT => "PRINT",
#				   ENCRYPT => 'YES',
#				   OPTIMIZE => 1,
#				   REQUEST_ID => "filippi", 
#				   #				 REQUEST_ID => "filippi_${fil}_${ncc}", 
#				   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"} 
#				  );


#  if ( !$ExtractList ) {
#    EXIT_ERROR("soapenv:Client", "ESTRATTO CONTO NON TROVATO");
#  }
##  my $fileName = $ExtractList->[0]->[1]; $IN = FileHandle->new();

##  open($IN, "<$fileName") 
##    or 
# # EXIT_ERROR("soapenv:Server", "OPEN ERROR FOR file \"$fileName\" $!"); binmode($IN);

##  read($IN, $pdf, -s $fileName); close($IN); unlink $fileName;
  
##  $pdf64 = MIME::Base64::encode($pdf); 
   $Response->write($xmlhdr);
#   $Response->write($pdf64);
   $Response->write(MIME::Base64::encode(Dumper({
				      TO_DIR => $Application->{"cfg.workdir"},
				      QUERY_TYPE => 'FROMPAGES',
				      PAGE_LIST => \@PAGE_LIST,
				      FORMAT => "PRINT",
				      REQUEST_ID => $Session->SessionID(),
				      ENCRYPT => "YES"}
				    )));
   
   
   $Response->write($xmltail);
 
}

EXIT_ERROR("soapenv:Server", "XSLT LOADXML ERROR: ". $xsltmgr->parseError()->reason())
  if
  !$xsltmgr->load("./xsl/getDocument.xsl");

#my $SOAPStream = $reader->LoadXML(  $Request->BinaryRead( $Request->TotalBytes() ) );

#$Response->Clear(); $Response->{'Buffer'} = 1;
#$Response->{'ContentType'} = "text/xml";
print "Reading XMLIN\n";

open XMLINFH, "<getDocument.source.xml" || die "Unable to open XMLIN\n";
sysread XMLINFH, my $xmlin, -s 'getDocument.source.xml';
close XMLINFH;

print "loading XMLIN (" . length($xmlin) . ") using soap\n";
$xmlin =~ s/\s*[\n\r]+\s*/ /gm;
$xmlin =~ s/> </></gm;
print "XML:\n", $xmlin, "\n";

$reader->LoadXML( ''.$xmlin.'' ) ;

my $soapreq = $reader->RpcStruct();
my $function = $soapreq->prefix();
my $method = $soapreq->baseName();
my $respmsg = &$method( ReqStruct($soapreq) );

$Response->End();
