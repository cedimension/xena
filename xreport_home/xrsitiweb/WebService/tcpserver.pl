#!/usr/bin/perl -w

use IO::Socket::INET;
use IO::Select;
use SOAP::Transport::IO;
my $SrvName = $0;

my $logrRtn = sub {print @_, "\n";};
#SOAP::Transport::IO::Server

  # you may specify as parameters for new():
  # -> new( in => 'in_file_name' [, out => 'out_file_name'] )
  # -> new( in => IN_HANDLE      [, out => OUT_HANDLE] )
  # -> new( in => *IN_HANDLE     [, out => *OUT_HANDLE] )
  # -> new( in => \*IN_HANDLE    [, out => \*OUT_HANDLE] )

  # -- OR --
  # any combinations
  # -> new( in => *STDIN, out => 'out_file_name' )
  # -> new( in => 'in_file_name', => \*OUT_HANDLE )

  # -- OR --
  # use in() and/or out() methods
  # -> in( *STDIN ) -> out( *STDOUT )

  # -- OR --
  # use default (when nothing specified):
  #      in => *STDIN, out => *STDOUT

  # don't forget, if you want to accept parameters from command line
  # \*HANDLER will be understood literally, so this syntax won't work 
  # and server will complain

  # -> new(@ARGV)

  # specify path to My/Examples.pm here
#  -> new( in => $Cli      [, out => $Cli] )
#  -> dispatch_to('/Your/Path/To/Deployed/Modules', 'Module::Name', 'Module::method') 
#  -> handle
#;
  my $SrvSock = new IO::Socket::INET(Listen => 5, 
				     LocalPort => 8888,
				     LocalAddr => '0.0.0.0',
				     Proto => 'tcp', 
				     Reuse => 1) || &$logrRtn("unable to listen on 8888 - $!");

  &$logrRtn("Listener now waiting on port 8888") if ($SrvSock);

  my $sel = new IO::Select( $SrvSock );

  &$logrRtn("now entering main loop") if ($SrvSock);
  for (my $con = 0; ; $con++) {
    $sock = $sel->can_read(15);
    next unless ($sock);
    $Cli = $SrvSock->accept || &$logrRtn("unable to accept connection - $! - $?");
    if ($Cli) {
      my ($cliport, $iaddr) = sockaddr_in(getpeername($Cli));
      my $peer = inet_ntoa($iaddr);
      &$logrRtn($SrvName . " accepted connection from " . $peer);
      SOAP::Transport::IO::Server
	  -> new( in => $Cli  , out => $Cli )
	    -> dispatch_to('/Your/Path/To/Deployed/Modules', 'Module::Name', 'Module::method') 
	      -> handle
		;
    } else {
      return 1;
    }
  }
  &$logrRtn("now leaving main loop: $currSt ", (($sock) ? "Cli pending" : "Cli Exhausted"));
  $SrvSock->close;
  return 0;


