<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

#!/usr/bin/perl -w

use constant WSDL => <<'ENDWSDL'
<?xml version="1.0" encoding="UTF-8"?>
<wsdl:definitions name="XRSOAPSERVERNAME"
	targetNamespace="http://cereport.org/WebService"
	xmlns:apachesoap="http://xml.apache.org/xml-soap"
	xmlns:impl="http://cereport.org/WebService"
	xmlns:intf="http://cereport.org/WebService"
	xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
	xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
	xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	<wsdl:types>
		<xsd:schema elementFormDefault="qualified"
			targetNamespace="http://cereport.org/WebService"
			xmlns="http://www.w3.org/2001/XMLSchema"
			xmlns:apachesoap="http://xml.apache.org/xml-soap"
			xmlns:intf="http://cereport.org/WebService"
			xmlns:impl="http://cereport.org/WebService"
			xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
			
			<xsd:element name="binaryDoc" type="xsd:base64Binary">
			  <xsd:annotation>
			    <xsd:documentation>Attachment - Anche se il type indica la codifica 
			                       base64 l'allegato gira in binario
			    </xsd:documentation>
			  </xsd:annotation>
			</xsd:element>
			
			<xsd:element name="documentID" type="xsd:string"/>
			
			<xsd:complexType name="keyT">
			  <xsd:annotation>
			    <xsd:documentation>Chiave</xsd:documentation>
			  </xsd:annotation>
			  <xsd:simpleContent>
			    <xsd:extension base="xsd:string">
			      <xsd:attribute name="name" type="xsd:string"/>
			    </xsd:extension>
			  </xsd:simpleContent>
			</xsd:complexType>

			<xsd:complexType name="documentKeyT">
				<xsd:sequence>
					<xsd:element name="TableName" type="xsd:string" />
					<xsd:element maxOccurs="unbounded" minOccurs="1"
						name="Key" type="impl:keyT" />
				</xsd:sequence>
			</xsd:complexType>
			
			<element name="PutDocumentData">
			  <xsd:annotation>
			    <xsd:documentation>Dati per la richiesta di archiviazione di un documento</xsd:documentation>
			  </xsd:annotation>
				<xsd:complexType>
					<xsd:sequence>
						<xsd:element name="documentKey" type="impl:documentKeyT" />
						<xsd:element name="temporary" type="xsd:boolean" default="false" minOccurs="0"/>
						<xsd:element name="overwrite" type="xsd:boolean" default="false" minOccurs="0"/>
					</xsd:sequence>
				</xsd:complexType>
			</element>			
			
			<element name="GetDocumentData">
				<xsd:complexType>
					<xsd:sequence>
						<xsd:element name="documentKey" type="impl:documentKeyT" />
					</xsd:sequence>
				</xsd:complexType>
			</element>		

			<element name="DeleteDocumentData">
				<xsd:complexType>
					<xsd:sequence>
						<xsd:element name="documentKey" type="impl:documentKeyT" />
					</xsd:sequence>
				</xsd:complexType>
			</element>		

			<element name="PersistDocumentData">
				<xsd:complexType>
					<xsd:sequence>
						<xsd:element name="documentKey" type="impl:documentKeyT" />
					</xsd:sequence>
				</xsd:complexType>
			</element>						
		</xsd:schema>
	</wsdl:types>
	<wsdl:message name="PutDocumentRequest">
		<wsdl:part element="intf:PutDocumentData" name="PutDocument" />
		<wsdl:part element="intf:binaryDoc" name="binaryDoc" />
	</wsdl:message>
	<wsdl:message name="PutDocumentResponse">
		<wsdl:part element="intf:documentID" name="documentID" />
	</wsdl:message>
	<wsdl:message name="GetDocumentRequest">
		<wsdl:part element="intf:GetDocumentData" name="GetDocument" />
	</wsdl:message>
	<wsdl:message name="GetDocumentResponse">
		<wsdl:part element="intf:binaryDoc" name="binaryDoc" />
	</wsdl:message>
	<wsdl:message name="DeleteDocumentRequest">
		<wsdl:part element="intf:DeleteDocumentData" name="DeleteDocument" />
	</wsdl:message>
	<wsdl:message name="DeleteDocumentResponse">
		<wsdl:part element="intf:documentID" name="documentID" />
	</wsdl:message>
	<wsdl:message name="PersistDocumentRequest">
		<wsdl:part element="intf:PersistDocumentData" name="PersistDocument" />
	</wsdl:message>
	<wsdl:message name="PersistDocumentResponse">
		<wsdl:part element="intf:documentID" name="documentID" />
	</wsdl:message>
	<wsdl:portType name="XRSOAPSERVERNAMEMethods">
		<wsdl:operation name="putDocument">
			<wsdl:input message="intf:PutDocumentRequest" />
			<wsdl:output message="intf:PutDocumentResponse" />
		</wsdl:operation>
		<wsdl:operation name="getDocument">
			<wsdl:input message="intf:GetDocumentRequest" />
			<wsdl:output message="intf:GetDocumentResponse" />
		</wsdl:operation>
		<wsdl:operation name="deleteDocument">
			<wsdl:input message="intf:DeleteDocumentRequest" />
			<wsdl:output message="intf:DeleteDocumentResponse" />
		</wsdl:operation>
		<wsdl:operation name="persistDocument">
			<wsdl:input message="intf:PersistDocumentRequest" />
			<wsdl:output message="intf:PersistDocumentResponse" />
		</wsdl:operation>
	</wsdl:portType>
	<wsdl:binding name="XRSOAPSERVERNAMEBinding"
		type="intf:XRSOAPSERVERNAMEMethods">
        <soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
		<wsdl:operation name="putDocument">
			<soap:operation soapAction="putDocument" />
			<wsdl:input>
                <mime:multipartRelated>
                    <mime:part>
                        <soap:body use="literal" parts="PutDocumentR" />
                    </mime:part>
                    <mime:part>
                        <mime:content part="binaryDoc" type="*/*"/>
                    </mime:part>
                </mime:multipartRelated>
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal" />
			</wsdl:output>
		</wsdl:operation>		
		<wsdl:operation name="getDocument">
			<soap:operation soapAction="getDocument" />
			<wsdl:input>
				<soap:body use="literal" />
			</wsdl:input>
			<wsdl:output>
                <mime:multipartRelated>
                    <mime:part>
                        <mime:content part="binaryDoc" type="*/*"/>
                    </mime:part>
                </mime:multipartRelated>
			</wsdl:output>
		</wsdl:operation>		
		<wsdl:operation name="deleteDocument">
			<soap:operation soapAction="deleteDocument" />
			<wsdl:input>
				<soap:body use="literal" />
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal" />
			</wsdl:output>
		</wsdl:operation>		
		<wsdl:operation name="persistDocument">
			<soap:operation soapAction="persistDocument" />
			<wsdl:input>
				<soap:body use="literal" />
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal" />
			</wsdl:output>
		</wsdl:operation>		
	</wsdl:binding>
	<wsdl:service name="XRSOAPSERVERNAME">
		<wsdl:port binding="intf:XRSOAPSERVERNAMEBinding"
			name="XRSOAPSERVERNAMEPort">
			<soap:address
				location="HTTP://10.15.34.64:8080/CeWebService/XRSOAPSERVERNAME.asp" />
		</wsdl:port>
	</wsdl:service>
</wsdl:definitions>
ENDWSDL
;

use strict "vars";

our ($Server, $Application, $Session, $Request, $Response, $XREPORT_HOME, $ASPVER, $ScriptHost);
$main::debugdir = "$ENV{TEMP}/CeWebService_Debug";

my $reqmet   = $Request->ServerVariables('REQUEST_METHOD'    )->item();
my $SOAPAct  = $Request->ServerVariables('HTTP_SOAPAction'   )->item();
my $qrystr   = $Request->ServerVariables('QUERY_STRING'      )->item();
my $reqident = $Request->ServerVariables('HTTP_REQUEST_ID'   )->item();
my $appldir  = $Request->ServerVariables('APPL_PHYSICAL_PATH')->item();
my @req = ((split /[\/.:]/, $Request->ServerVariables('URL')->item())[-2,-1]);
$main::servername = ($req[1] =~ /asp/ ? $req[0] : $req[1]);

use POSIX qw(strftime);
$main::starttime = strftime "%Y-%m-%d %H:%M:%S", localtime;

use lib("$ENV{'XREPORT_HOME'}/perllib");


use FileHandle;
use File::Path;
use Data::Dumper;

#use Win32::OLE qw(in);
#use Win32::OLE::Variant;

use XReport::DBUtil;
#use XReport::XMLUtil qw(SQL2XML);
use MIME::Base64;
use XML::Simple;

$XREPORT_HOME = $ENV{'XREPORT_HOME'};
$ASPVER = '1.1';
$appldir = '.\\' unless $appldir;
require "${appldir}webservice.pl";

sub getDocList {

  my $reqdata = shift;
  my $reqident = shift || 'NO-IDENT';

  write2log("Request Started: ", join('::', times() ));
  my $query = xtractParm($reqdata, 'Query');
  write2log("getDocList Query $reqident:", Dumper($query));

  my ($tbl, $maxlines, $where) = @{$query}{qw(TableName TOP WHERE)};
  my $orderby = exists($query->{'ORDERBY'}) ? $query->{'ORDERBY'} : 'JobReportId DESC';

  my $dbc = initDBConn($tbl) || return;

  my $select = "SELECT TOP " . ($maxlines || 100) . " * FROM tbl_IDX_$tbl ";

  my @conds = ( buildClause($tbl, $where->[0]) ); 
  return unless defined($conds[0]);

  $select .= 'WHERE (' . join(') OR (', @conds ) .')' if scalar(@conds);
  $select .= ' ORDER BY ' . $orderby . ' ';
  write2log("SELECT:", $select);

  my $dbr = $dbc->dbExecute($select);
#  return EXIT_SOAP_ERROR("Sender", "getDocList/QryResult", "Nessun Documento trovato per i criteri specificati") if $dbr->eof();

  my $result;
  my $idxlines = 0;
  while (!$dbr->eof()) {
    my $fields = $dbr->Fields();
    $result .= '<Document><JobReportId>' . $fields->{JobReportId}->Value() . '</JobReportId>'
      . '<FromPage>'. $fields->{FromPage}->Value() . '</FromPage>'
	. '<ForPages>'. $fields->{ForPages}->Value() . '</ForPages><KeyList>';
    $idxlines++;
    for my $fldn (0..$fields->Count()-1) {
      my $var = $fields->Item($fldn)->Name();
      next if $var =~ /JobReportId|FromPage|ForPages/;
      my $codeType = $fields->{$var}->Type();
      my $type = XReport::DBUtil::MSSQLTypes->getCodeAttr($codeType)->{Type};
#      my $tname = XReport::DBUtil::MSSQLTypes->getCodeAttr($codeType)->{Name};
#      my $type1 = XReport::DBUtil::MSSQLTypes->getCodeAttr("$codeType")->{Type};
#      my $tname1 = XReport::DBUtil::MSSQLTypes->getCodeAttr("$codeType")->{Name};
#     write2log("code: ", $codeType, " type1: -", $type1, "-", " tname1: -", $tname1, "-");
      my $val = $fields->{$var}->Value();
#      write2log("code: -$codeType- tname: -$tname- Var=$var Type= -$type- Val=$val ref=-",ref($val), '-');
      $val = ($val->can('Date') ? $val->Date('yyyy-MM-dd') : '')." ".($val->can('Time') ? $val->Time('hh:mm:ss tt') : '') if ref($val);
      $result .= "<Key><Var>$var</Var><Min>$val</Min></Key>";
    }

    $result .= '</KeyList></Document>';
    $dbr->MoveNext()
  }

  write2log("result:", $result);
#  return EXIT_SOAP_ERROR("Sender", "getDocList/ResultEmpty", "Nessun Documento Trovato") unless $result;

  $Response->Write(SOAPMSGHDR
		   . '<ListOfDocuments xmlns="http://cereport.org/WebService">'
		   . $result
		   . '</ListOfDocuments>'
		   . SOAPMSGTAIL);

  write2log("Request ended: ", join('::', times() ));
  $Response->Flush();
  return $Response->AppendToLog("GetDocList_start_at_${main::starttime}" . "_-ID_$reqident-_"
				. "Times(us::sy::cus::csy):_" .  join('::', times() ) . "_-_"
				. "${idxlines}_lines_from_${tbl}" . getSOAPHDR($reqdata)
			       );


}

sub getDocById {
  my $reqdata = shift;
  my $reqident = shift || 'NO-IDENT';

  my $DocList = ( $reqdata->{ListOfDocuments}->[0]->{Document} );

  write2log("Request Started: $reqident ", join('::', times() ));

  my @PAGE_LIST = map { @{$_}{qw(JobReportId FromPage ForPages)} } @$DocList;
  write2log("page_list:", Dumper(@PAGE_LIST));
  my $outbytes = ( (scalar(@PAGE_LIST) == 3 && $PAGE_LIST[1] eq "0" && $PAGE_LIST[2] eq "-1") ?
		   extractDocument( $PAGE_LIST[0] , '<PdfDocument xmlns="http://cereport.org/WebService">', '</PdfDocument>') : 
		   extractPages(\@PAGE_LIST, '<PdfDocument xmlns="http://cereport.org/WebService">', '</PdfDocument>' )
		 );
  return 0 unless $outbytes;

  write2log("Document bytes written: $outbytes ", join('::', times() ));
  return $Response->AppendToLog("GetDocById_start_at_${main::starttime}_-ID_$reqident-_"
				. "Times(us::sy::cus::csy):_" .  join('::', times() ) . '_-_'
				. scalar(@$DocList) . "_Documents Retrieved_-_"
				. "${outbytes}_sent_in_base64_encoded_format" . getSOAPHDR($reqdata)
			       );

}

sub getDocument {

  my $reqdata = shift;
  my $reqident = shift || 'NO-IDENT';

  write2log("Request Started: ", join('::', times() ));
  my $query = xtractParm($reqdata, 'Query');
  write2log("getDocList Query $reqident:", Dumper($query));

  my ($tbl, $maxlines, $where) = @{$query}{qw(TableName TOP WHERE)};
  my $orderby = exists($query->{'ORDERBY'}) ? $query->{'ORDERBY'} : 'JobReportId DESC';

  my $dbc = initDBConn($tbl) || return;

  my $select = "SELECT TOP 1 * FROM tbl_IDX_$tbl ";

  my @conds = ( $tbl, buildClause($where->[0]) ); 
  return unless defined($conds[0]);

  $select .= 'WHERE (' . join(') OR (', @conds ) .')' if scalar(@conds);
  $select .= ' ORDER BY ' . $orderby . ' ';
  write2log("SELECT:", $select);

  my $dbr = $dbc->dbExecute($select);
#  return EXIT_SOAP_ERROR("Sender", "getDocList/QryResult", "Nessun Documento trovato per i criteri specificati") if $dbr->eof();

  my $result;
  my $idxlines = 0; 
  my $JobReportId;
  while (!$dbr->eof()) {
    my $fields = $dbr->Fields();
    $JobReportId = $fields->{JobReportId}->Value();
    $result .= '<Document><JobReportId>' . $fields->{JobReportId}->Value() . '</JobReportId>'
      . '<FromPage>'. $fields->{FromPage}->Value() . '</FromPage>'
	. '<ForPages>'. $fields->{ForPages}->Value() . '</ForPages><KeyList>';
    $idxlines++;
    for my $fldn (0..$fields->Count()-1) {
      my $var = $fields->Item($fldn)->Name();
      next if $var =~ /JobReportId|FromPage|ForPages/;
      my $codeType = $fields->{$var}->Type();
      my $type = XReport::DBUtil::MSSQLTypes->getCodeAttr($codeType)->{Type};
#      my $tname = XReport::DBUtil::MSSQLTypes->getCodeAttr($codeType)->{Name};
#      my $type1 = XReport::DBUtil::MSSQLTypes->getCodeAttr("$codeType")->{Type};
#      my $tname1 = XReport::DBUtil::MSSQLTypes->getCodeAttr("$codeType")->{Name};
#     write2log("code: ", $codeType, " type1: -", $type1, "-", " tname1: -", $tname1, "-");
      my $val = $fields->{$var}->Value();
#      write2log("code: -$codeType- tname: -$tname- Var=$var Type= -$type- Val=$val ref=-",ref($val), '-');
      $val = ($val->can('Date') ? $val->Date('yyyy-MM-dd') : '')." ".($val->can('Time') ? $val->Time('hh:mm:ss tt') : '') if ref($val);
      $result .= "<Key><Var>$var</Var><Min>$val</Min></Key>";
    }

    $result .= '</KeyList></Document>';
    $dbr->MoveNext()
  }

  

  write2log("result:", $result);
#  return EXIT_SOAP_ERROR("Sender", "getDocList/ResultEmpty", "Nessun Documento Trovato") unless $result;
  extractDocument( $JobReportId , '' 
		   . '<ListOfDocuments xmlns="http://cereport.org/WebService">'
		   . $result
		   . '</ListOfDocuments>'
		   . '<PdfDocument xmlns="http://cereport.org/WebService">', '</PdfDocument>') ; 

  write2log("Request ended: ", join('::', times() ));
  $Response->Flush();
  return $Response->AppendToLog("GetDocument_start_at_${main::starttime}" . "_-ID_$reqident-_"
				. "Times(us::sy::cus::csy):_" .  join('::', times() ) . "_-_"
				. "${idxlines}_lines_from_${tbl}" . getSOAPHDR($reqdata)
			       );


}

sub deleteDocument {
  my $reqdata = shift;
  my $reqident = shift || 'NO-IDENT';

  write2log("Request Started: ", join('::', times() ));
  my $selection = xtractParm($reqdata, 'Query');
  write2log("deleteDocument $reqident:", Dumper($selection));

  my ($tbl, $maxlines, $where) = @{$selection}{qw(TableName TOP WHERE)};

  my @conds = ( $tbl, buildClause($where->[0]) ); 
  return unless defined($conds[0]);

  my $select = "SELECT TOP " . ($maxlines || 100) . " * FROM tbl_IDX_$tbl ";
  $select .= 'WHERE (' . join(') OR (', @conds ) .')' if scalar(@conds);
  write2log("SELECT:", $select);

  my $dbc = initDBConn($tbl) || return;

  my $dbr = $dbc->dbExecute($select);
#  return EXIT_SOAP_ERROR("Sender", "getDocList/QryResult", "Nessun Documento trovato per i criteri specificati") if $dbr->eof();

  my $result;
  my $idxlines = 0;
  while (!$dbr->eof()) {
    my $fields = $dbr->Fields();
    $result .= '<Document><JobReportId>' . $fields->{JobReportId}->Value() . '</JobReportId>'
      . '<FromPage>'. $fields->{FromPage}->Value() . '</FromPage>'
	. '<ForPages>'. $fields->{ForPages}->Value() . '</ForPages><KeyList>';
    $idxlines++;
    for my $fldn (0..$fields->Count()-1) {
      my $var = $fields->Item($fldn)->Name();
      next if $var =~ /JobReportId|FromPage|ForPages/;
      my $codeType = $fields->{$var}->Type();
      my $type = XReport::DBUtil::MSSQLTypes->getCodeAttr($codeType)->{Type};
#      my $tname = XReport::DBUtil::MSSQLTypes->getCodeAttr($codeType)->{Name};
#      my $type1 = XReport::DBUtil::MSSQLTypes->getCodeAttr("$codeType")->{Type};
#      my $tname1 = XReport::DBUtil::MSSQLTypes->getCodeAttr("$codeType")->{Name};
#     write2log("code: ", $codeType, " type1: -", $type1, "-", " tname1: -", $tname1, "-");
      my $val = $fields->{$var}->Value();
#      write2log("code: -$codeType- tname: -$tname- Var=$var Type= -$type- Val=$val ref=-",ref($val), '-');
      $val = ($val->can('Date') ? $val->Date('yyyy-MM-dd') : '')." ".($val->can('Time') ? $val->Time('hh:mm:ss tt') : '') if ref($val);
      $result .= "<Key><Var>$var</Var><Min>$val</Min></Key>";
    }

    $result .= '</KeyList></Document>';
    $dbr->MoveNext()
  }

  write2log("result:", $result);
#  return EXIT_SOAP_ERROR("Sender", "getDocList/ResultEmpty", "Nessun Documento Trovato") unless $result;

  $Response->Write(SOAPMSGHDR
		   . '<ListOfDocuments xmlns="http://cereport.org/WebService">'
		   . $result
		   . '</ListOfDocuments>'
		   . SOAPMSGTAIL);

  write2log("Request ended: ", join('::', times() ));
  $Response->Flush();
  return $Response->AppendToLog("DeleteDocument_start_at_${main::starttime}" . "_-ID_$reqident-_"
				. "Times(us::sy::cus::csy):_" .  join('::', times() ) . "_-_"
				. "${idxlines}_lines_from_${tbl}" . getSOAPHDR($reqdata)
			       );


}

sub persistDocument {
  my $reqdata = shift;
  my $reqident = shift || 'NO-IDENT';

  write2log("Request Started: ", join('::', times() ));
  my $query = xtractParm($reqdata, 'Query');
  write2log("getDocList Query $reqident:", Dumper($query));

  my ($tbl, $maxlines, $where) = @{$query}{qw(TableName TOP WHERE)};

  my @conds = ( $tbl, buildClause($where->[0]) ); 
  return unless defined($conds[0]);

  my $select = "SELECT TOP " . ($maxlines || 100) . " * FROM tbl_IDX_$tbl ";
  $select .= 'WHERE (' . join(') OR (', @conds ) .')' if scalar(@conds);
  write2log("SELECT:", $select);

  my $dbc = initDBConn($tbl) || return;

  my $dbr = $dbc->dbExecute($select);
#  return EXIT_SOAP_ERROR("Sender", "getDocList/QryResult", "Nessun Documento trovato per i criteri specificati") if $dbr->eof();

  my $result;
  my $idxlines = 0;
  while (!$dbr->eof()) {
    my $fields = $dbr->Fields();
    $result .= '<Document><JobReportId>' . $fields->{JobReportId}->Value() . '</JobReportId>'
      . '<FromPage>'. $fields->{FromPage}->Value() . '</FromPage>'
	. '<ForPages>'. $fields->{ForPages}->Value() . '</ForPages><KeyList>';
    $idxlines++;
    for my $fldn (0..$fields->Count()-1) {
      my $var = $fields->Item($fldn)->Name();
      next if $var =~ /JobReportId|FromPage|ForPages/;
      my $codeType = $fields->{$var}->Type();
      my $type = XReport::DBUtil::MSSQLTypes->getCodeAttr($codeType)->{Type};
#      my $tname = XReport::DBUtil::MSSQLTypes->getCodeAttr($codeType)->{Name};
#      my $type1 = XReport::DBUtil::MSSQLTypes->getCodeAttr("$codeType")->{Type};
#      my $tname1 = XReport::DBUtil::MSSQLTypes->getCodeAttr("$codeType")->{Name};
#     write2log("code: ", $codeType, " type1: -", $type1, "-", " tname1: -", $tname1, "-");
      my $val = $fields->{$var}->Value();
#      write2log("code: -$codeType- tname: -$tname- Var=$var Type= -$type- Val=$val ref=-",ref($val), '-');
      $val = ($val->can('Date') ? $val->Date('yyyy-MM-dd') : '')." ".($val->can('Time') ? $val->Time('hh:mm:ss tt') : '') if ref($val);
      $result .= "<Key><Var>$var</Var><Min>$val</Min></Key>";
    }

    $result .= '</KeyList></Document>';
    $dbr->MoveNext()
  }

  write2log("result:", $result);
#  return EXIT_SOAP_ERROR("Sender", "getDocList/ResultEmpty", "Nessun Documento Trovato") unless $result;

  $Response->Write(SOAPMSGHDR
		   . '<ListOfDocuments xmlns="http://cereport.org/WebService">'
		   . $result
		   . '</ListOfDocuments>'
		   . SOAPMSGTAIL);

  write2log("Request ended: ", join('::', times() ));
  $Response->Flush();
  return $Response->AppendToLog("persistDocument_start_at_${main::starttime}" . "_-ID_$reqident-_"
				. "Times(us::sy::cus::csy):_" .  join('::', times() ) . "_-_"
				. "${idxlines}_lines_from_${tbl}" . getSOAPHDR($reqdata)
			       );


}

sub putDocument {
  my $reqdata = shift;
  my $reqident = shift || 'NO-IDENT';

  write2log("Request Started: ", join('::', times() ));
  my $IRows = xtractParm($reqdata, 'IndexRows');
  my $pdf = xtractParm($reqdata, 'Pdf');
  my ($tbl, $where) = @{$IRows}{qw(TableName WHERE)};

  my $dbc = initDBConn($tbl) || return;
  my $idxlines = 0;
  write2log("getDocument Query $reqident:", Dumper($IRows), "\n", "PdfDocument bytes: ", length($pdf));
  write2log("getDocument Query $reqident:", "\n", "PdfDocument bytes: ", length($pdf), "\n", $main::HTTPStream, "\n");

  return $Response->AppendToLog("putDocument_start_at_${main::starttime}" . "_-ID_$reqident-_"
				. "Times(us::sy::cus::csy):_" .  join('::', times() ) . "_-_"
				. "${idxlines}_lines_from_${tbl}" . getSOAPHDR($reqdata)
			       );
}

return $Session->Abandon() if ( !$Application->{'cfg.processed'} );


write2log("init $main::servername: Method: $reqmet, QSTRING: -$qrystr-, SOAPAct: $SOAPAct");

$Response->Clear();
$Response->{'Buffer'} = 1;

if ( $reqmet =~ /^GET$/i ) {
  if ($qrystr =~ /^wsdl$/i) {
    sendWSDL();
  }
  elsif (($qrystr =~ /^testpdfou$/i)) {
    $Response->Write(SOAPMSGHDR
		     . '<PdfDocument xmlns="http://cereport.org/WebService"><Pdf>PDF GOES HERE</Pdf></PdfDocument>'
		     . SOAPMSGTAIL
		    );
  }
  elsif (($qrystr =~ /^testdoclist$/i)) {
    $Response->Write(SOAPMSGHDR
		     . '<ListOfDocuments xmlns="http://cereport.org/WebService">'
		     . '<Document>'
		     . '<JobReportId>1</JobReportId><FromPage>1</FromPage><ForPages>1</ForPages>'
		     . '<KeyList>'
		     . '<Key><Var>NDG</Var><Min>01230078056</Min></Key>'
		     . '<Key><Var>DATARIF</Var><Min>2005-11-11</Min></Key>'
		     . '</KeyList>'
		     . '</Document>'
		     . '</ListOfDocuments>'
		     . SOAPMSGTAIL
		    );
  } else {
    EXIT_HTTP_ERROR("Client", "HTTP GET $qrystr not supported");
  }
  write2log("Request ended: ", join('::', times() ));
  return $Response->Flush();
}

return EXIT_HTTP_ERROR("Client", "HTTP POST not supported without SOAPAction") unless $SOAPAct;
write2log("SOAPAction ", $SOAPAct);

$Response->{'ContentType'} = "text/xml";

my $HTTPStream = $Request->BinaryRead( $Request->TotalBytes() );
write2log("HTTPstream ", Dumper($HTTPStream));

my $reqdata = xtractParm(XMLin($HTTPStream,
			       NoAttr => 1,
			       ForceArray => [ qw(WHERE KeyList Key ListOfDocuments Document) ],
#			       KeyAttr => { Key => 'Var'},
			      ),
			 qr/\w+:Body/);

$reqdata->{soaphdrs} = xtractParm(XMLin($HTTPStream, NoAttr => 1, ), qr/soapenv:Header/);

write2log("reqdata ", Dumper($reqdata));

#my $method = [ keys %$reqdata ]->[0];
my $method = $Request->ServerVariables("HTTP_SOAPAction")->item();
$method =~ s/\W*(\w+)\W*/$1/;
write2log("XMLin SOAPStruct for -$method-:", Dumper( $reqdata ));

return EXIT_SOAP_ERROR("Client", "invAction", "SOAP action $method not supported") unless defined &$method;

return &$method( $reqdata , $reqident) ;

#$HTTPStream = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><getDocList xmlns=""><Query><TOP>100</TOP><TableName>CCINDEX</TableName><WHERE><Key><Var>CDG</Var><Min>193687</Min><Max>193687</Max></Key><Key><Var>DATA_RIF</Var><Min>2005-01-01</Min><Max>2005-12-01</Max></Key><Key><Var>JobReportId</Var><Min>64</Min><Max>64</Max></Key></WHERE></Query></getDocList></soapenv:Body></soapenv:Envelope>';

</SCRIPT>
