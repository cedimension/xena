#!/usr/bin/perl -w

use constant WSDLOPERATIONS => qw(getDocument updateIndex deleteDocument persistDocument storeDocument );
#use constant WSDLOPERATIONS => qw(getDocument updateIndex removeIndex deleteDocument persistDocument storeDocument );
use strict "vars";


sub getDocument {
  my $method = shift;  
  my $reqident = shift || 'NO-IDENT';
  SOAPreq2HASH($method);

  my $selection = xtractParm($main::reqdata, 'REQUEST');
  main::debug2log("${method} $reqident:", Dumper($selection));

  my ($tbl, $IndexEntries) = @{$selection}{qw(IndexName IndexEntries)};
  
  my $dbc = initDBConn($tbl) || return;

  my @conds = ( buildClause($tbl, $IndexEntries) ); 
  return unless defined($conds[0]);

  my $select = "SELECT TOP 1 * FROM tbl_IDX_$tbl ";
  $select .= 'WHERE (' . join(') OR (', @conds ) .')' if scalar(@conds);
  $select .= ' ORDER BY JobReportId DESC';
  main::debug2log("${method} SELECT:", $select);


  my $dbr = $dbc->dbExecute($select);
  return EXIT_SOAP_ERROR("Sender", "$method/QryResult", "Nessun Documento trovato per i criteri specificati") if $dbr->eof();
#  my $outbytes = returnDocumentPages([$dbr->Fields()->{JobReportId}->Value(), 1, 9999999], 
  my $outbytes = returnDocument([$dbr->Fields()->{JobReportId}->Value(), 
				 $dbr->Fields()->{FromPage}->Value(), 
				 $dbr->Fields()->{ForPages}->Value()], 
				{IndexName => $tbl, 
				 DocumentType => 'PDF', 
				 IndexEntries => fillIndexEntries($dbr), 
				});

  $main::msgtolog = "${outbytes}_sent_in_base64_encoded_format";

  return;
}

sub _GET_DOCUMENT {
	$main::Response->Write(buildResponse({DocumentType => 'PDF', DocumentBody => {content => 'PDF GOES HERE'}} ));
}

sub removeIndex {
  my $method = shift;
  SOAPreq2HASH($method);
  my $reqident = shift || 'NO-IDENT';

  my $selection = xtractParm($main::reqdata, 'REQUEST');

  return _removeIndexEntries($method, $selection, $reqident);
}

sub updateIndex {
  my $method = shift;
  SOAPreq2HASH($method);
  my $reqident = shift || 'NO-IDENT';

  my $selection = xtractParm($main::reqdata, 'REQUEST');
  return _updateIndexEntries($method, $selection, $reqident);
}

sub persistDocument {
  my $method = shift;
  SOAPreq2HASH($method);
  my $reqident = shift || 'NO-IDENT';

  my $selection = xtractParm($main::reqdata, 'REQUEST');
  $selection->{IndexName} = "ReportStatus";
  $selection->{IndexEntries} = [ { JobReportId => $selection->{IndexEntries}->[0]->{JobReportId}, 
				   Columns => { JobReportId => { content => $selection->{IndexEntries}->[0]->{JobReportId} }, } } ];
  $selection->{NewEntry} = [ { Columns => { Status => {content => "1"} } } ];

  return _updateIndexEntries($method, $selection, $reqident);
}

sub deleteDocument {
  my $method = shift;
  SOAPreq2HASH($method);
  my $reqident = shift || 'NO-IDENT';

  my $selection = xtractParm($main::reqdata, 'REQUEST');
  $selection->{IndexName} = "ReportStatus";
  $selection->{IndexEntries} = [ { JobReportId => $selection->{IndexEntries}->[0]->{JobReportId}, 
				   Columns => { JobReportId => { content => $selection->{IndexEntries}->[0]->{JobReportId} }, } } ];

# CSE 2006-12-11
# $selection->{NewEntry} = { Columns => { Status => {content => "9"} } };
  $selection->{NewEntry} = [ { Columns => { Status => {content => "9"} } } ];


  return _updateIndexEntries($method, $selection, $reqident);
}

sub storeDocument {
  use Compress::Zlib;

  my $method = shift;
  my $reqident = shift || 'NO-IDENT';

  my $TotalBytes = $main::Request->TotalBytes();
  my ($RequestData, $RequestDocument) = ('', '');
  my $buffref = \$RequestData;
  while($TotalBytes > 0) {
    my $bytes2read = ($TotalBytes > 4096 ? 4096 : $TotalBytes);
    main::debug2log("${method} $reqident request: reading $bytes2read of $TotalBytes left");
    
    $RequestData .= $main::Request->BinaryRead($bytes2read); 
    $TotalBytes -= $bytes2read;
    main::debug2log("${method} $reqident request: checking contents data: ", length($RequestData), " bytes ");
    if ( $RequestData =~ /^(.*)\<DocumentBody\s*\>(.*)$/is ) {
      ($RequestData, $RequestDocument) = ($1, $2);
      main::debug2log("${method} $reqident request: switching to Document ",
		"data: ", length($RequestData), " bytes ",
		"document: ", length($RequestDocument), " bytes "
	       );
      last
    }
  }
  ($RequestDocument, undef) = ($RequestDocument =~ /^(.*)(\<\/DocumentBody\s*\>.*){0,1}$/is);
  main::debug2log("${method} $reqident request: switching to Document ",
		"document: ", length($RequestDocument), " bytes "
	       );
  return EXIT_SOAP_ERROR("Receiver", "Receive", "Request contains no document ") 
    unless ($TotalBytes || length($RequestDocument)) ;
  
  my @soaptags = ( $RequestData =~ /(?:\<([^\/][\w\:]+)[^>]*?\>)/sg );
  my $tagsended = { '?xml' => '>', ( $RequestData =~ /(?:\<\/([\w\:]+)[^>]*?(\>))/sg ) };
  $RequestData .= '<DocumentBody>REFERTOIN</DocumentBody>'.
    join('', map { (exists($tagsended->{"$_"}) ?  '' : '</'."$_".'>' ) }  reverse @soaptags);
  
  parseSOAPreq($RequestData);
  main::debug2log("${method} $reqident request:", Dumper($main::reqdata));

  my $selection = xtractParm($main::reqdata, 'REQUEST');
  my ($tbl, $IndexEntries, $doctype, $rmtfile, $Recipient) = @{$selection}{qw(IndexName IndexEntries DocumentType FileName DocRecipient)};
  $doctype = $tbl unless $doctype;
  return EXIT_SOAP_ERROR("Receiver", "QCreate", "No Document Type or Index Name specified") unless $doctype;
  
  my ($reqid, $TargetLocalPathId_IN); eval {($reqid, $TargetLocalPathId_IN) = QCreate XReport::QUtil(
				     SrvName        => $main::servername,
				     JobReportName  => $doctype,
#				     XferRecipient  => $Recipient,
				     RemoteFileName => $rmtfile || 'MIME::Application/pdf',
				     LocalFileName  => '.',
				     RemoteHostAddr => $main::Request->ServerVariables('REMOTE_ADDR')->item(),
				     XferStartTime  => $main::starttime_local,
				     XferMode       => '9',
				     XferDaemon     => $main::servername, #.'@'.$ENV{COMPUTERNAME},
				     JobOrigin      => $main::Request->ServerVariables('REMOTE_HOST')->item() || 'UNKNOWN',
				     JobName        => $method,
				     JobNumber      => $main::Request->ServerVariables('INSTANCE_ID')->item(),
				     JobExecutionTime => $main::starttime_local,
				     Status         => $CD::stAccepted,
				     XferId         => $reqident,
				     );
		 };
  return EXIT_SOAP_ERROR("Receiver", "QCreate", "$@") if $@;
  my $ofil = "$doctype." . strftime('%Y%m%d%H%M%S', localtime ). ".$reqid";
  my $odir = strftime('%Y/%m%d', localtime);

  (my $destpath= $main::Application->{'cfg.localpath.'.$TargetLocalPathId_IN}."/IN/$odir") =~ s/^file:\/\///;
  eval { mkpath($destpath) };
  if ($@) {
    return EXIT_SOAP_ERROR("Receiver", "dircreate", "$@") if $@;
  }

  my $ofh = gzopen("$destpath/$ofil.DATA.TXT.gz", "wb");
  my ($buffsize, $databytes) = (4056, 0);

  while($TotalBytes > 0) {
    my $bytes2read = ($TotalBytes > $buffsize ? ($buffsize - length($RequestDocument)) : $TotalBytes);

    my $buffer = $main::Request->BinaryRead($bytes2read); 
    $TotalBytes -= $bytes2read;

    if ( ($RequestDocument.$buffer) =~ /^(.*)(<\/DocumentBody\s*\>.*)$/is ) {
      $RequestDocument = $1;
    }
    else {
      $RequestDocument .= $buffer;
    }
    $RequestDocument =~ s/[\r\n]//gs; 
    my @b64lines = unpack("(a76)*", $RequestDocument);
    
    $RequestDocument = ( length($b64lines[-1]) < 76 ? pop @b64lines : '') ;
#    main::debug2log("${method} $reqident buffer base64 is ", scalar(@b64lines), " 76 bytes lines + ", length($RequestDocument), " bytes");
#    main::debug2log("${method} $reqident first line buffer base64:\n", $b64lines[0]);
#    main::debug2log("${method} $reqident first line buffer decoded:\n", unpack("H*", MIME::Base64::decode_base64($b64lines[0])));
    my $decoded_buffer = MIME::Base64::decode_base64(join('', @b64lines));
    main::debug2log("${method} $reqident decoded buffer is ", length($decoded_buffer), " bytes");
    my $wb = $ofh->gzwrite($decoded_buffer);
    $databytes += $wb; 
  }
  if ($RequestDocument) {
    if ( $RequestDocument =~ /^(.*)(<\/DocumentBody\s*\>.*)$/is ) {
      ($RequestDocument = $1) =~ s/[\r\n]//gs;
    }
 #   main::debug2log("${method} $reqident last buffer base64 is ", length($RequestDocument), " bytes");
 #   main::debug2log("${method} $reqident buffer base64:\n", $RequestDocument);
    my $lastbuff = MIME::Base64::decode_base64($RequestDocument);
 #   main::debug2log("${method} $reqident buffer decoded:\n", unpack("H*", $lastbuff));
    my $wb = $ofh->gzwrite($lastbuff);
    $databytes += $wb;
  }
  $ofh->gzclose();
  main::debug2log("${method} $reqident Document is ", $databytes, " bytes");

  main::debug2log("${method} $reqident ALL_HTTP:", $main::Request->ServerVariables('ALL_HTTP')->item(), " destpath: ", $destpath);

  $ofh = new FileHandle(">$destpath/$ofil.CNTRL.TXT");
  my ($rqsthdr, $rqsttail) = ($RequestData =~ /^(\<\?xml.*?\?\>)(.*)$/osi);
  my $wb = $ofh->syswrite($rqsthdr.'<CNTLDATA>'.$main::Request->ServerVariables('ALL_RAW')->item()."\n".$rqsttail.'</CNTLDATA>' );
  $ofh->close();

  my $req;eval {$req = QUpdate XReport::QUtil(
					      SrvName        => $main::servername,
					      LocalFileName  => "$odir/$ofil.DATA.TXT.gz",
					      JobReportName  => $doctype,
					      ReportName     => $doctype,
					      XferInBytes    => $main::Request->TotalBytes(),
					      XferOutBytes   => (-s "$destpath/$ofil.DATA.TXT.gz") + (-s "$destpath/$ofil.CNTRL.TXT"),
					      XferEndTime    => strftime("%Y-%m-%d %H:%M:%S", localtime),
					      Status         => $CD::stQueued,
					      Id             => $reqid
				  );
	      };
  return EXIT_SOAP_ERROR("Receiver", "QUpdate", "$@") if $@;
#  map { $_->{JobReportId} = $reqid } @$IndexEntries;
  my $DocumentData = {IndexName => $tbl, DocumentType => $doctype,
		      IndexEntries => [ {JobReportId => $reqid} ],
		    };
  my $result = buildResponse($DocumentData);
  $main::Response->Write( $result );
  
  main::debug2log("${method} result:", $result);
  $main::msgtolog = "${reqid}_JobReportId_created_".scalar(@{$DocumentData->{IndexEntries}})
    ."_".$DocumentData->{IndexName}."_entries_tobe_inserted";
  return;
}

1;
