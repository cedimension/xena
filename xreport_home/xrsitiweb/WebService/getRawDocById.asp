<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

### vim: set syn=perl: ###

use strict "vars";
use POSIX qw(strftime);

my $XREPORT_HOME = $ENV{'XREPORT_HOME'};
my $ASPVER = '1.1';

$main::debugdir = "$ENV{TEMP}/CeWebService_Debug";
##$main::debugdir = "c:/CeWebService_Debug";

use lib("$ENV{'XREPORT_HOME'}/perllib");

use Data::Dumper;

use Win32::OLE::Variant;
use XML::Simple;

my $appldir  = $main::Request->ServerVariables('APPL_PHYSICAL_PATH')->item();
require "${appldir}webservice.pl";

$main::timesmark = [ times() ];

$main::starttime = strftime "%Y-%m-%d %H:%M:%S", localtime;
$main::starttime_local = strftime "%Y-%m-%d %H:%M:%S", localtime;

my $reqmet   = $main::Request->ServerVariables('REQUEST_METHOD'    )->item();
my $reqident = $main::Request->ServerVariables('HTTP_REQUEST_ID'   )->item();

my @req = ((split /[\/.:]/, $main::Request->ServerVariables('URL')->item())[-2,-1]);
$main::method = $main::servername = ($req[1] =~ /asp/ ? $req[0] : $req[1]);

$main::Response->Clear();
$main::Response->{'Buffer'} = 1;

return EXIT_HTTP_ERROR("Client", "HTTP GET not supported") unless $reqmet eq 'POST';

my $hdrs = parseHTTPHDRS();
#my $DocList = ( exists($hdrs->{DOCUMENTS}) ? XMLin( parseHTTPHDRS()->{DOCUMENTS}, ForceArray => [qw(Document)] )->{Document} : [] );
my $DocList = [];

write2log("init $main::servername: Method: $reqmet, REQUEST: ", Dumper($DocList) );
write2log("${main::method} $reqident ALL_RAW:", $main::Request->ServerVariables('ALL_RAW')->item());
my $REQUEST;
my $reqlen = $main::Request->{TotalBytes};
if ($reqlen) {
  eval { $REQUEST = XMLin( $main::Request->BinaryRead( $reqlen ), ForceArray => [qw(Document)] ) };
  return EXIT_HTTP_ERROR("Sender", "getRawDocById/XMLError", $@) if $@;
  return EXIT_HTTP_ERROR("Sender", "getRawDocById/REQUESTerror", "INCORRECT REQUEST") unless exists($REQUEST->{Document});
  write2log("init $main::servername: Method: $reqmet, XMLCONTENT: ", Dumper($REQUEST) );
  $DocList = [ @{$REQUEST->{Document}} ];
}
else {
  return EXIT_HTTP_ERROR("Sender", "getRawDocById/NoRequest", "NO REQUEST SPECIFIED");
}


my @PAGE_LIST = map { @{$_}{qw(JobReportId FromPage ForPages)} } @{$DocList};
if ( scalar(@PAGE_LIST) == 0 ) {
  return EXIT_HTTP_ERROR("Sender", "getRawDocById/NoPages", "NO PAGES FOUND IN RANGE");
}

my $JobReportId;
write2log("REQUEST Documents list size:", scalar(@PAGE_LIST), " content: $PAGE_LIST[2] ", Dumper(\@PAGE_LIST));
if (scalar(@PAGE_LIST) < 4 and $PAGE_LIST[2] == -1 ) {
  $JobReportId = $PAGE_LIST[0];

  eval { require XReport::ARCHIVE::OUTPUT; };
  if ($@) {
    main::write2log("Error during require of $JobReportId - $@");
    return EXIT_HTTP_ERROR("Sender", "returnFullDocument/openError", "Error during require of $JobReportId - $@");
  }

  my $JRhandle;
  eval {$JRhandle = Open XReport::ARCHIVE::OUTPUT($JobReportId, wrapper => 1 ); };
  if ($@) {
    main::write2log("Error during open for $JobReportId - $@");
    return EXIT_HTTP_ERROR("Sender", "returnFullDocument/openError", "Error during open of $JobReportId - $@");
  }
#  my $tmpdir = $ENV{USERPROFILE}.'/Application Data/CeDim/XRIDX';
#  write2log("JobReport Handle:", Dumper($JRhandle));
#-------------------------------------------
  my ($JobReportName, $RemoteFileName, $ElabFormat ) =  @{$JRhandle}{qw(JobReportName RemoteFileName ElabFormat)};
  my ($document, $mimetype, $fname) = ('', 'application/x-pdf', '');
  main::write2log("Retrieving $RemoteFileName (ID $PAGE_LIST[1] of $JobReportId)");
  if ($RemoteFileName =~ /^MIME::(.*)$/) {
    $mimetype = $1;
  }
  else {
    if ( $RemoteFileName =~ /^.*\.((?i:xml|jpe?g|tif{1,2}|od[tsp]|bmp))$/ ) {
      my $exts = $1;
      $mimetype = ($exts =~ /^xml$/i ? 'text/xml' : $exts =~ /od[tsp]/i ? 'application/oo-document' : "image/$exts");
    }
    $main::Response->AddHeader("Content-Disposition", "attachment; filename=".$RemoteFileName)
  }
  main::write2log("Retrieving $RemoteFileName (ID $PAGE_LIST[1] of $JobReportId EF: $ElabFormat) as $mimetype");
  $document = $JRhandle->getIdContent(($PAGE_LIST[1] - 1), $ElabFormat == 1 ? 'pdf' : 'dat') ||
    return EXIT_HTTP_ERROR("Sender", "getRawDocById/openIdError", "Error during extract of ID $PAGE_LIST[1] of $JobReportId - $@");
#  $document = $JRhandle->{archiver}->FileContents("$JobReportName\.$JobReportId\.0\.#0\.pdf");
#  $document = $JRhandle->{archiver}->FileContents("$JobReportName\.$JobReportId\.0\.#0\.dat") unless $document;
  $main::Response->AddHeader('Content-Length' => length($document));
#  main::write2log("Retrieved" . length($document) . "bytes HEAD: " . unpack("a40", $document));

  $main::Response->{ContentType} = $mimetype;

  my $binstream = new Win32::OLE::Variant(VT_UI1, $document);
  $main::Response->BinaryWrite($binstream) ;
  $main::msgtolog = length($document) . "_bytesRetrieved_-_";


}
else {

  require XReport::EXTRACT;
  require XReport::PDF::DOC;

  my $query = XReport::EXTRACT->new();
  my $streamer = BINstreamer->new('Application/pdf');

  $query->{docTo} = XReport::PDF::DOC->Create($streamer) ;
  #$main::Response->AddHeader('Content-Length' => "58189");

  my $elist;
  eval { $elist = $query->ExtractPages(
				     QUERY_TYPE => 'FROMPAGES',
				     PAGE_LIST => \@PAGE_LIST,
				     FORMAT => "PRINT",
				     ENCRYPT => 'YES',
				     OPTIMIZE => 1,
				     REQUEST_ID => "GetRawDocById.".$main::Session->{'SessionID'},
				     #				   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"}
				    );
     };
  my $errmsg = $@ || 'unknown';

  if ( scalar(@$elist) == 0 ) {
    return EXIT_HTTP_ERROR("Sender", "getRawDocById/IdNotFound", "IMPOSSIBILE ESTRARRE DA ARCHIVIO per la/e chiavi richiesta/e - $errmsg");
  }

  $main::msgtolog = scalar(@$elist) . "_DocumentsRetrieved_-_" . $streamer->{InputBytes} . "_BytesRetrieved_". $streamer->{OutputBytes} . "_BytesSent_as_OLEVariant";
}

my $timesnow = [ times() ];
for my $ix (0..3) {
  $timesnow->[$ix] = $timesnow->[$ix] - $main::timesmark->[$ix];
}
my $timesmsg = "Times(us::sy::cus::csy):_" . join('::', @{$timesnow} );

write2log("ended $reqmet $main::method Request $reqident  ended: ", $timesmsg, " - ", $main::msgtolog);
$main::Response->AppendToLog("${main::method}_start_at_${main::starttime}_-ID_$reqident-_"
			     . $timesmsg  . '_-_'
			     . $main::msgtolog
			     . getHTTPHDR($main::reqdata)
			    );
return $main::Response->Flush();

</SCRIPT>
