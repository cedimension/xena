#!/usr/bin/perl -w
use strict "vars";

use Data::Dumper;#$Data::Dumper::Terse = 1; $Data::Dumper::Indent = 1;
#use SOAP::Lite +trace => debug;
use SOAP::Lite;

use MIME::Base64;
use XML::Simple;

sub xtractParm {
  my ($struct, $parm) = (shift, shift);
  my $result = undef;
  if (ref($struct) eq 'HASH') {
    foreach my $k ( keys %$struct ) {
      return  $struct->{$k} if $k =~ /^$parm$/i;
      $result = xtractParm($struct->{$k}, $parm);
      return $result if $result;
    }
  }
  elsif (ref($struct) eq 'ARRAY') {
    foreach my $el ( @$struct ) {
      $result = xtractParm($el, $parm);
      return $result if $result;
    }
  }
  return undef;
}

sub XRwebRequest {
 my $parms = { @_ };

 my $server = $parms->{server} || '127.0.0.1:80';
 delete $parms->{server};
 my $service = $parms->{service} || 'xrIdxQuery';
 delete $parms->{service};
 my $method = $parms->{method} || 'getDocList';
 delete $parms->{method};

 my $RQSTattr = {};
 for my $attr (qw(IndexName TOP ORDERBY DocumentType FileName temporary)) {
   if (exists($parms->{$attr})) {
     $RQSTattr->{$attr} = $parms->{$attr};
     delete $parms->{$attr};
   }
 }
 my $DocumentData = {};
 foreach my $valname ( qw(IndexEntries NewEntries) ) {
   next unless exists $parms->{$valname};
   foreach my $ie ( @{$parms->{$valname}}) {
     my @attrn = [ grep !/^Columns$/, keys %{$ie} ];
     my $attrs = {};
     @{$attrs}{@attrn} = @{$ie}{@attrn};
     push @{$DocumentData->{$valname}}, SOAP::Data->value({Columns => $ie->{Columns}});
   }
 }
 

 my $client = ( SOAP::Lite->uri($parms->{server})
		->service('http://'.$server.'/'.$service.'.asp?wsdl')
		->proxy('http://'.$server.'/'.$service.'.asp')
		->on_action(sub{'"' . join('', @_). '"'})
		->namespace('soapenv')
		->encprefix('soapenc')
		->envprefix('soapenv')
		->autotype(0)
		->on_debug(sub{print@_})
		->outputxml(1));

 my $result = $client->call($method,
		       SOAP::Data->name('REQUEST')->attr( $RQSTattr )
		       ->value(XMLout({'IndexEntries' => $parms->{IndexEntries}}, KeepRoot => 1, NoIndent => 1)
			       . XMLout({'NewEntries' => $parms->{NewEntries}}, KeepRoot => 1, NoIndent => 1)
			      )
		      )
 ;
 return $result;
}

my ($server, $uri, $method) = @ARGV[0,1,2];

my $search = [{Columns => [{colname => 'JobReportId', Min => 8204, Max => 8204},
			   {colname => 'CDG', Min => 20904, Max => 20904}]}];
my $update = [];

my $result = XRwebRequest( server => $ARGV[0], service => $ARGV[1], method => $ARGV[2], 
			   IndexName => 'LC05080', TOP => 10, ORDERBY => 'JobReportId', DocumentType => 'PDF',
			   IndexEntries => $search ,  NewEntries => $update,
			 );

print join ', ',
  $result->faultcode(),
  $result->faultactor(),
  $result->faultstring(),
  $result->faultdetail() if $result && $result->fault();

print "=========>\n", Dumper($result), "\n<===========\n" if $result;
print "No Results returned\n" unless $result;
exit 0;

