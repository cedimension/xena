<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

use strict "vars";
$main::ASPVER = '1.1';

$main::debugdir = "C:/CeWebService_Debug";
###$main::debugdir = "C:/CeWebService_Debug";

my $appldir  = $main::Request->ServerVariables('APPL_PHYSICAL_PATH')->item();
my @req = ((split /[\/.:]/, $main::Request->ServerVariables('URL')->item())[-2,-1]);
$main::servername = ($req[1] =~ /asp/ ? $req[0] : $req[1]);

use POSIX qw(strftime);
$main::starttime = strftime "%Y-%m-%d %H:%M:%S", localtime;

$appldir = '.\\' unless $appldir;
require "${appldir}${main::servername}_methods.pl";
require "${appldir}Testwebservice.pl";

return processRequest();

</SCRIPT>
