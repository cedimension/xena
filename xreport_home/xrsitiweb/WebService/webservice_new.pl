#!/usr/bin/perl -w

# --- webservice.pl
package XReport::DBUtil::MSSQLTypes;

use strict "vars";

use constant typeCode => {
	  adEmpty => 0,
	  adTinyInt => 16,
	  adSmallInt => 2,
	  adInteger => 3,
	  adBigInt => 20,
	  adUnsignedTinyInt => 17,
	  adUnsignedSmallInt => 18,
	  adUnsignedInt => 19,
	  adUnsignedBigInt => 21,
	  adSingle => 4,
	  adDouble => 5,
	  adCurrency => 6,
	  adDecimal => 14,
	  adNumeric => 131,
	  adVarNumeric => 139,
	  adBoolean => 11,
	  adError => 10,
	  adUserDefined => 132,
	  adVariant => 12,
	  adIDispatch => 9,
	  adIUnknown => 13,
	  adGUID => 72,
	  adDate => 7,
	  adDBDate => 133,
	  adDBTime => 134,
	  adDBTimeStamp => 135,
	  adFileTime => 64,
	  adDBFileTime => 137,
	  adBSTR => 8,
	  adChar => 129,
	  adVarChar => 200,
	  adLongVarChar => 201,
	  adWChar => 130,
	  adVarWChar => 202,
	  adLongVarWChar => 203,
	  adBinary => 128,
	  adVarBinary => 204,
	  adLongVarBinary => 205,
	  adChapter => 136,
	  adPropVariant => 138,
};

use constant codeAttr => {
	  0 => { Name => 'adEmpty', Type => 'S' },
	  16 => { Name => 'adTinyInt', Type => 'N' },
	  2 => { Name => 'adSmallInt', Type => 'N' },
	  3 => { Name => 'adInteger', Type => 'N' },
	  20 => { Name => 'adBigInt', Type => 'N' },
	  17 => { Name => 'adUnsignedTinyInt', Type => 'N' },
	  18 => { Name => 'adUnsignedSmallInt', Type => 'N' },
	  19 => { Name => 'adUnsignedInt', Type => 'N' },
	  21 => { Name => 'adUnsignedBigInt', Type => 'N' },
	  4 => { Name => 'adSingle', Type => 'N' },
	  5 => { Name => 'adDouble', Type => 'N' },
	  6 => { Name => 'adCurrency', Type => 'N' },
	  14 => { Name => 'adDecimal', Type => 'N' },
	  131 => { Name => 'adNumeric', Type => 'N' },
	  139 => { Name => 'adVarNumeric', Type => 'N' },
	  11 => { Name => 'adBoolean', Type => 'N' },
	  10 => { Name => 'adError', Type => 'S' },
	  132 => { Name => 'adUserDefined', Type => 'S' },
	  12 => { Name => 'adVariant', Type => 'S' },
	  9 => { Name => 'adIDispatch', Type => 'S' },
	  13 => { Name => 'adIUnknown', Type => 'S' },
	  72 => { Name => 'adGUID', Type => 'S' },
	  7 => { Name => 'adDate', Type => 'D' },
	  133 => { Name => 'adDBDate', Type => 'D' },
	  134 => { Name => 'adDBTime', Type => 'D' },
	  135 => { Name => 'adDBTimeStamp', Type => 'D' },
	  64 => { Name => 'adFileTime', Type => 'D' },
	  137 => { Name => 'adDBFileTime', Type => 'D' },
	  8 => { Name => 'adBSTR', Type => 'S' },
	  129 => { Name => 'adChar', Type => 'S' },
	  200 => { Name => 'adVarChar', Type => 'S' },
	  201 => { Name => 'adLongVarChar', Type => 'S' },
	  130 => { Name => 'adWChar', Type => 'S' },
	  202 => { Name => 'adVarWChar', Type => 'S' },
	  203 => { Name => 'adLongVarWChar', Type => 'S' },
	  128 => { Name => 'adBinary', Type => 'S' },
	  204 => { Name => 'adVarBinary', Type => 'S' },
	  205 => { Name => 'adLongVarBinary', Type => 'S' },
	  136 => { Name => 'adChapter', Type => 'S' },
	  138 => { Name => 'adPropVariant', Type => 'S' },
};

sub getTypeCode {
  my ($class, $tn) = (shift, shift);
  return undef unless exists typeCode->{$tn};
  return typeCode->{$tn};
}

sub getCodeAttr {
  my ($class, $cd) = (shift, shift);
  return codeAttr->{13} unless exists codeAttr->{$cd};
  return codeAttr->{$cd};
}

1;

package B64streamer;

use MIME::Base64;

sub new {
  my $class = shift;
  my $self = { buffsize => 57*76, bufflen => 0, InputBytes => 0, OutputBytes => 0, buffer => ''};
  bless $self, $class;

  return $self;
}

sub write {
  my ($self, $buffer) = (shift, shift);
  $self->{buffer} .= $buffer; 
  while ( length($self->{buffer}) > $self->{buffsize} ) {
    ($buffer, $self->{buffer}) = unpack("a".$self->{buffsize}." a*", $self->{buffer}) ;
    my $b64buff = MIME::Base64::encode_base64($buffer);
    $main::Response->Write($b64buff) ;
    $self->{InputBytes} += length($buffer);
    $self->{OutputBytes} += length($b64buff);
  }
}

sub tell {
 my $self = shift;
 return $self->{InputBytes} + length($self->{buffer});
}

sub close {
  my $self = shift;
  $self->write('');
  my $b64buff = MIME::Base64::encode_base64($self->{buffer});
  $main::Response->Write($b64buff) ;
  $self->{InputBytes} += length($self->{buffer});
  $self->{OutputBytes} += length($b64buff);
  main::write2log("Inputbytes: ", $self->{InputBytes}, " Outputbytes: ", $self->{OutputBytes});
#  return $main::Response->Flush();
}

1;

package BINstreamer;

use Win32::OLE::Variant;
use IO::String;

sub new {
  my $class = shift;
 
  my $self = { buffsize => 57*76, bufflen => 0, InputBytes => 0, OutputBytes => 0, buffer => '', content => shift};
  $self->{response_data} = '';
  bless $self, $class;
  
  return $self;
}

sub write {
  my ($self, $buffer) = (shift, shift);
  $self->{buffer} .= $buffer; 
  $main::Response->{ContentType} = $self->{content} unless $self->{OutputBytes};
  while ( length($self->{buffer}) > $self->{buffsize} ) {
    ($buffer, $self->{buffer}) = unpack("a".$self->{buffsize}." a*", $self->{buffer}) ;
    my $binstream = new Win32::OLE::Variant(VT_UI1, $buffer);
    $main::Response->BinaryWrite($binstream) ;
    $self->{InputBytes} += length($buffer);
    $self->{OutputBytes} += length($binstream);
  }
}

sub tell {
 my $self = shift;
 return $self->{InputBytes} + length($self->{buffer});
}

sub close {
  my $self = shift;
  $self->write('');
  my $binstream = new Win32::OLE::Variant(VT_UI1, $self->{buffer});
  $main::Response->BinaryWrite($binstream) ;
  $self->{InputBytes} += length($self->{buffer});
  $self->{OutputBytes} += length($binstream);
  main::write2log("Inputbytes: ", $self->{InputBytes}, " Outputbytes: ", $self->{OutputBytes});
#  return $main::Response->Flush();
}

1;

package main;

use lib("$ENV{'XREPORT_HOME'}/perllib");

use FileHandle;
use File::Path;
use Data::Dumper;

use XReport::DBUtil;
use XReport::QUtil;

use MIME::Base64;
use XML::Simple;

use POSIX qw(strftime);

use constant WSDLHDR => '<?xml version="1.0" encoding="UTF-8"?>'
  . '<wsdl:definitions name="XRSOAPSERVERNAME"'
  . '	targetNamespace="http://cereport.org/WebService"'
  . '	xmlns:apachesoap="http://xml.apache.org/xml-soap"'
  . '	xmlns:impl="http://cereport.org/WebService"'
  . '	xmlns:intf="http://cereport.org/WebService"'
  . '	xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"'
  . '	xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"'
  . '	xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/"'
  . '	xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
  . '   <wsdl:types>'
  . '       <xsd:schema'
  . '            elementFormDefault="qualified"'
  . '          targetNamespace="http://cereport.org/WebService"'
  . '            xmlns="http://www.w3.org/2001/XMLSchema"'
  . '            xmlns:apachesoap="http://xml.apache.org/xml-soap"'
  . '           xmlns:impl="http://cereport.org/WebService"'
  . '            xmlns:intf="http://cereport.org/WebService"'
  . '            xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">'
#  . '            <xsd:complexType name="column">'
#  . '			  <xsd:annotation>'
#  . '			    <xsd:documentation>Column Attrbutes</xsd:documentation>'
#  . '			  </xsd:annotation>'
#  . '			  <xsd:sequence>'
#  . '                            <xsd:element maxOccurs="unbounded" minOccurs="0" name="ValuesArray" type="xsd:string"/>'
#  . '                            <xsd:element maxOccurs="1" minOccurs="0" name="dummy4axis" type="xsd:string"/>'
#  . '			  </xsd:sequence>'
#  . '			  <xsd:attribute name="colname" type="xsd:string"/>'
#  . '			  <xsd:attribute name="operation" type="xsd:string"/>'
#  . '			  <xsd:attribute name="Value" type="xsd:string"/>'
#  . '	         </xsd:complexType>'
  . '            <xsd:complexType name="column">'
  . '			  <xsd:annotation>'
  . '			    <xsd:documentation>Column Attrbutes</xsd:documentation>'
  . '			  </xsd:annotation>'
  . '			  <xsd:simpleContent>'
  . '			    <xsd:extension base="xsd:string">'
  . '			      <xsd:attribute name="colname" type="xsd:string"/>'
  . '			      <xsd:attribute name="operation" type="xsd:string"/>'
  . '			      <xsd:attribute name="Min" type="xsd:string"/>'
  . '			      <xsd:attribute name="Max" type="xsd:string"/>'
  . '			    </xsd:extension>'
  . '			  </xsd:simpleContent>'
  . '	         </xsd:complexType>'
  . '            <xsd:complexType name="IndexEntry">'
  . '			  <xsd:annotation>'
  . '			    <xsd:documentation>Definisce parte o tutto un documento. Viene indicata la parte di documento da attribuire alle chiavi specificate in termini di pagina di partenza (FromPage) e numero di pagine consecutive (ForPages). Nell\'array "Columns" vengono specificate le colonne di indici pertinenti al documento ed il loro valore. In caso di interrogazione deve essere valorizzato solo l\'array "Columns" da cui viene costruita la where clause per la query. In caso di inserimento devono essere specificate anche le pagine pertinenti alle chiavi specificate</xsd:documentation>'
  . '			  </xsd:annotation>'
  . '			  <xsd:sequence>'
  . '                            <xsd:element maxOccurs="unbounded" minOccurs="0" name="Columns" type="impl:column"/>'
  . '                            <xsd:element maxOccurs="1" minOccurs="0" name="dummy4axis" type="xsd:string"/>'
  . '			  </xsd:sequence>'
  . '			      <xsd:attribute name="JobReportId" type="xsd:string"/>'
  . '			      <xsd:attribute name="FromPage"    type="xsd:string"/>'
  . '			      <xsd:attribute name="ForPages"    type="xsd:string"/>'
  . '            </xsd:complexType>'
  . '            <xsd:complexType name="DocumentData">'
  . '		   <xsd:annotation>'
  . '		      <xsd:documentation>Document Data Structure. Viene usata sia nelle richieste che nelle risposte. Contiene i seguenti campi: IndexName - Nome simbolico dell\'indice da accedere; DocumentType - Typo documento da trattare (PDF per il momento); temporary - non so; TOP - numero massimo di righe indice da ritornare alla richiesta; ORDERBY - statement da utilizzare per l\'ordinamento delle righe indice; IndexEntries - array di righe indice da trattare; DocumentBody - rappresentazione in BASE64 del contenuto del documento</xsd:documentation>'
  . '		   </xsd:annotation>'
  . '	           <xsd:sequence>'
  . '                  <xsd:element maxOccurs="unbounded" minOccurs="0" name="IndexEntries" type="impl:IndexEntry"/>'
  . '                  <xsd:element maxOccurs="1" minOccurs="0" name="NewEntry" type="impl:IndexEntry"/>'
  . '                  <xsd:element maxOccurs="1" minOccurs="0" name="DocumentBody" type="xsd:base64Binary"/>'
  . '              </xsd:sequence>'
  . '		   <xsd:attribute name="IndexName" type="xsd:string"/>'
  . '		   <xsd:attribute name="DocumentType" type="xsd:string"/>'
  . '		   <xsd:attribute name="FileName" type="xsd:string"/>'
  . '		   <xsd:attribute name="temporary" default="false" type="xsd:boolean"/>'
  . '		   <xsd:attribute name="TOP"       type="xsd:string"/>'
  . '		   <xsd:attribute name="ORDERBY"   type="xsd:string"/>'
  . '            </xsd:complexType>'
  . '            <element type="impl:DocumentData" name="REQUEST" />'
  . '            <element type="impl:DocumentData" name="RESPONSE" />'
  . '        </xsd:schema>'
  . '    </wsdl:types>'
  . '    <wsdl:message name="RequestMsg">'
  . '        <wsdl:part element="intf:REQUEST" name="REQUEST"/>'
  . '    </wsdl:message>'
  . '    <wsdl:message name="ResponseMsg">'
  . '        <wsdl:part element="intf:RESPONSE" name="RESPONSE"/>'
  . '    </wsdl:message>'
  ;

use constant WSDLTAIL => '	<wsdl:service name="XRSOAPSERVERNAME">'
  . '		<wsdl:port binding="intf:XRSOAPSERVERNAMEBinding"'
  . '			name="XRSOAPSERVERNAMEPort">'
  . '			<soap:address'
  . '				location="HTTP://127.0.0.1:8085/CeWebService/XRSOAPSERVERNAME.asp" />'
  . '		</wsdl:port>'
  . '	</wsdl:service>'
  . '</wsdl:definitions>'
  ;

use constant SOAPMSGHDR => '<?xml version="1.0" encoding="UTF-8"?>'
  . '<soap:Envelope '
  . 'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" '
  . 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
  .' xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
  . '<soap:Body>'
  ;

use constant SOAPMSGTAIL => '</soap:Body >'
  . '</soap:Envelope>'
  ;

sub soaphdr {
  return SOAPMSGHDR;
}

sub soaptail {
  return SOAPMSGTAIL;
}

sub write2log {
  return 1 unless -d $main::debugdir;
  my $mplog = new FileHandle '>>'.$main::debugdir.'/'.$main::servername.'.srvc.log';
  $mplog->write($main::Session->{SessionID} . " " . localtime() . " $main::ASPVER $main::servername: " . join('', @_)."\n");
  $mplog->close;
}

sub xmlquote {
  my $t = shift; $t =~ s/ +$//g;

  $t =~ s/&/&amp;/g;
  $t =~ s/</&lt;/g;
  $t =~ s/>/&gt;/g;
  $t =~ s/\"/&quot;/g;
  $t =~ s/\'/&apos;/g;

  $t =~ s/([\x80-\xff])/"\&\#".unpack("C",$1).";"/eg;

  return $t;
}

sub buildResponse {
  my $DocumentData = shift;
  $DocumentData->{xmlns} = 'http://cereport.org/WebService';
  $DocumentData->{comment} = 'version 1.1';
  return XMLout($DocumentData, RootName => 'RESPONSE',
		NoIndent => 1, NoSort => 1, # SuppressEmpty => 1,
		KeyAttr => {'Columns' => 'colname'},
		XMLDecl => SOAPMSGHDR) . SOAPMSGTAIL;
}

sub xtractParm {
  my ($struct, $parm) = (shift, shift);
  my $result = undef;
  if (ref($struct) eq 'HASH') {
    foreach my $k ( keys %$struct ) {
      return  $struct->{$k} if $k =~ /^$parm$/i;
      $result = xtractParm($struct->{$k}, $parm);
      return $result if $result;
    }
  }
  elsif (ref($struct) eq 'ARRAY') {
    foreach my $el ( @$struct ) {
      $result = xtractParm($el, $parm);
      return $result if $result;
    }
  }
  return undef;
}

sub EXIT_HTTP_ERROR {
  my $respmsg = "<faultResponse faultcode=\"".xmlquote($_[0])."\" faultstring=\"".xmlquote($_[1])."\"/>";
  write2log("EXIT_HTTP_ERROR: ", $respmsg);

  $main::Response->{'ContentType'} = "text/xml";
  $main::Response->{Status} = "500 Xreport Web Service Server Error";
  $main::Response->AddHeader("Connection", "close");
  $main::Response->Write( $respmsg );
  $main::Response->Flush();
#  $main::Response->End();
  return undef;

}

sub EXIT_SOAP_ERROR {
  my $respmsg = ('<?xml version="1.0" encoding="UTF-8"?>'
		 . '<SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema/instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">'
		 . '<SOAP-ENV:Body>'
		 . '<SOAP-ENV:Fault>'
		 . "<faultcode>$_[0]</faultcode>"
		 . "<faultactor>".$main::servername.'/'.$_[1]."</faultactor>"
		 . "<faultstring>$_[2]</faultstring>"
		 . '</SOAP-ENV:Fault>'
		 . '</SOAP-ENV:Body>'
		 . '</SOAP-ENV:Envelope>'
		);
  write2log("EXIT_SOAP_ERROR: ", $respmsg);

  $main::Response->{Status} = "500 Xreport Web Service Server Error";
  $main::Response->AddHeader("Connection", "close");
  $main::Response->Write( $respmsg );
  $main::Response->Flush();
  return undef;
}

sub getHTTPHDR {
  my $httphdr = '';
  for my $j ( 1..$main::Request->{'ServerVariables'}->{'Count'} ) {
    my $keyn = $main::Request->{'ServerVariables'}->Key($j);
    next if $keyn !~ /^HTTP_(.*)$/;
    $httphdr .= '_hdr-' . $1 .'|' . $main::Request->ServerVariables($keyn)->Item();
 }

  return $httphdr;

}

sub getSOAPHDR {
  my $request = shift;
  return '' unless $request;
  my $hdrs = $request->{soaphdrs};
  my $soaphdr = '';
  foreach my $keyn ( keys %$hdrs ) {
    (my $hdrid = $keyn) =~ s/:/_/g;
    $soaphdr .= '_hdr-' . $hdrid .'|' . $hdrs->{$keyn};
  }
  return $soaphdr;

}

sub parseHTTPHDRS {
  my $httphdr = {};
  for my $j ( 1..$main::Request->{'ServerVariables'}->{'Count'} ) {
    my $keyn = $main::Request->{'ServerVariables'}->Key($j);
    next if $keyn !~ /^HTTP_(.*)$/;
    $httphdr->{$1} = $main::Request->ServerVariables($keyn)->Item();
 }

  return { %$httphdr };

}

sub WSDLdeclaremethods {
  my $portTypeOPS = { 'wsdl:input' =>  {message => "intf:RequestMsg", }, 
		      'wsdl:output' => {message => "intf:ResponseMsg"},};
  my $bindingOPS = { 'wsdl:input' =>  {'soap:body' => {use => 'literal'}, content => ''}, 
		     'wsdl:output' => {'soap:body' => {use => 'literal'}, content => ''},};
  my $portType = {
		  name => "XRSOAPSERVERNAMEMethods", 
		  'wsdl:operation' => [ map { { 'name' => $_, %$portTypeOPS } } @_ ],
		 };

  my $binding = {
		 name => "XRSOAPSERVERNAMEBinding", 
		 type => "intf:XRSOAPSERVERNAMEMethods",
		 'soap:binding' => { style => 'document',  transport => 'http://schemas.xmlsoap.org/soap/http', }, 
		 'wsdl:operation' => [ map { { 'name' => $_ , 'soap:operation' => { soapAction => $_ }, %$bindingOPS } } @_ ],
		};

  return XMLout( $portType, RootName => 'wsdl:portType' ) . XMLout( $binding, RootName => 'wsdl:binding' );

}

sub sendWSDL {

  if ( !$main::Application->{$main::servername.'.'.'WSDL'} ) {
    
    write2log("sendWSDL storing WSDL in APP pool for $main::servername");
    my $wsdlhost = $main::Application->{$main::servername.'.wsdlhost'} = $main::Request->ServerVariables('SERVER_NAME')->item();
    my $serverpath = ($main::Request->ServerVariables('HTTPS')->item() =~ /^on$/i ? 
		      'HTTPS://' . $wsdlhost . ':' . $main::Request->ServerVariables('SERVER_PORT_SECURE')->item() :
		      'HTTP://' .$wsdlhost . ':'   . $main::Request->ServerVariables('SERVER_PORT')->item() )
      . $main::Request->ServerVariables('URL')->item();
    ;
    
    (my $wsdl = WSDLHDR . WSDLdeclaremethods( @_ ) . WSDLTAIL) =~ s/<soap:address\s+location=".*"\s*\/>/<soap:address location="$serverpath" \/>/im;
    my $wsdlspace = lc($main::servername);
    $wsdl =~ s/XRSOAPSERVERNAME/$wsdlspace/isg;
    $main::Application->{$main::servername.'.'.'WSDL'} = "$wsdl";   
    write2log("sendWSDL WSDL stored in APP pool for $main::servername");
  }
  return EXIT_HTTP_ERROR("Client", "unable to initialize WSDL") unless $main::Application->{$main::servername.'.'.'WSDL'} ;
  $main::Response->{'ContentType'} = "text/xml";
  $main::Response->Write($main::Application->{$main::servername.'.'.'WSDL'});
  return $main::Response->Flush();

}


sub initDBConn {
  my $tbl = shift;
  write2log("initDBConn started");
  my $dbc; eval {$dbc = XReport::DBUtil->get_ix_dbc($tbl);};
  return EXIT_SOAP_ERROR("Receiver", "DBInit", "$@") if $@;

  return EXIT_SOAP_ERROR("Receiver", "DBConnect", "ApplInit: DataBase Connector Lost") unless $dbc;

  if ( !$main::Application->{'getDocument.'.$tbl.'ColTypes'} ) {
    write2log("initDBConn initializing APP pool");
    my $dbr = $dbc->dbExecute("SELECT TOP 1 * FROM tbl_IDX_$tbl");
    if ($dbr->eof) {
      $main::Application->{'getDocument.'.$tbl.'ColTypes'} = '__EMPTY__';
    }
    else {
      my $fields = $dbr->Fields();
      my @COLAttrs;
      for my $fldn (0..$fields->Count()-1) {
	push @COLAttrs, my $var = $fields->Item($fldn)->Name();
	push @COLAttrs, XReport::DBUtil::MSSQLTypes->getCodeAttr($fields->{$var}->Type())->{Type};
      }
      $main::Application->{'getDocument.'.$tbl.'ColTypes'} = join(' ', @COLAttrs );
    }
    write2log("initDBConn terminated initialization for $tbl:", Dumper(\{split / /, $main::Application->{'getDocument.'.$tbl.'ColTypes'}}));
  }
  return EXIT_SOAP_ERROR("Receiver", "DBAccess", "Tabella indici vuota") if ($main::Application->{'getDocument.'.$tbl.'ColTypes'} eq '__EMPTY__');
  return $dbc;
}

sub buildCond {
      my ($var, $min, $max, $typ) = (shift, shift, shift, shift);
# insert date format validation 'yyyy-mm-gg hh:mm:ss'
      $min = "'$min'" unless (!defined($min) or $typ =~ /^N$/i);
      $max = "'$max'" unless (!defined($max) or $typ =~ /^N$/i);
      write2log("KEY: $var $typ $min $max");

      return " $var IS NULL " if !defined($min) && !defined($max);

      return " $var <= $max " if !defined($min);

      return " $var >= $min " if !defined($max);

      return " $var = 0 "     if ( "$min" eq "0" and "$max" eq "0");

      return " $var = '' "    if ( "$min" eq "''" and "$max" eq "''");

      return " $var = $min "  if ( $min eq $max );

      if ($min =~ /^'{0,1}LIKE'{0,1}$/) {
	return " $var LIKE $max " unless $typ =~ /^N$/i;
	write2log( "LIKE not supported for numeric column $var ");
	die "LIKE not supported for numeric column $var ";
      }

      if ($min =~ /^'{0,1}IN'{0,1}$/) {
	my $qt = ($typ =~ /^N$/i ? '' : "'");
	my $regex = ($typ =~ /^N$/i ? qr/[\x09\s]+/ : qr/\s*\x09\s*/);
	return " $var IN (" . join($qt.', '.$qt, split(/$regex/, $max)) . ") "; 	
      }

      return " $var BETWEEN $min AND $max ";
}

sub buildClause {
  my ($tbl, $IndexEntries) = (shift, shift);

  my $idxcoltypes = {split / /, $main::Application->{'getDocument.'.$tbl.'ColTypes'} };
  write2log("columns $tbl:", Dumper($idxcoltypes));

  my @or;
  write2log("buildClause request: ", Dumper($IndexEntries));
  foreach my $row ( @{$IndexEntries} ) {
    my @and;
    foreach my $var ( keys %{$row->{Columns}} ) {
      return EXIT_SOAP_ERROR("Sender", "IDXAccess", "Variable $var Richiesta non in tabella $tbl") unless exists $idxcoltypes->{$var};
      my $typ = $idxcoltypes->{$var};
      my $qt = ( $typ eq 'N' ? '' : "'" );
      my ($min, $max, $content) = @{ $row->{Columns}->{$var} }{qw(Min Max content)};
      write2log("ForEach: Checking now $var");
      if (!defined($content) && !defined($min) && !defined($max) ) {
	push @and, " $var IS NULL "
      }
      elsif ( !defined($content) ) {
	eval { push @and, buildCond($var, $min, $max, $idxcoltypes->{$var}) };
      }
      elsif (!defined($min) && !defined($max) ) {
	push @and, " $var = ${qt}${content}${qt} "
      }
      else {
	eval { push @and, buildCond($var, $min, (defined($max) ? $max : $content), $idxcoltypes->{$var}) };
      }
      return EXIT_SOAP_ERROR("Sender", "QuerySyntax", $@) if $@;
    }
    push @or, '(' .join(') AND (', @and) . ')' if scalar(@and);
  }
  return @or;
}

sub fillIndexEntries {
  my $dbr = shift;
  write2log("filling IndexEntries from ", ref($dbr));

  my @IndexEntries;
  write2log("fillIndexEntries no rows in RESULTSET") if $dbr->eof();

  while (!$dbr->eof()) {
    my $fields = $dbr->Fields();
    my $IndexEntry = { JobReportId => $fields->{JobReportId}->Value(),  };
    for my $fldn (0..$fields->Count()-1) {
      my $var = $fields->Item($fldn)->Name();
      next if $var =~ /^JobReportId$/i;
      if ( $var =~ /^(?:FromPage|ForPages)$/i ) {
	$IndexEntry->{$var} = $fields->{$var}->Value();
      }
      else {
#	my $codeType = $fields->{$var}->Type();
#	my $type = XReport::DBUtil::MSSQLTypes->getCodeAttr($codeType)->{Type};
	my $val = $fields->{$var}->Value();
	$val = ($val->can('Date') ? $val->Date('yyyy-MM-dd') : '')." ".($val->can('Time') ? $val->Time('hh:mm:ss tt') : '') if ref($val);
	push @{$IndexEntry->{Columns}}, { colname => $var, content => $val, };
      }
    }
    push @IndexEntries, $IndexEntry;
    $dbr->MoveNext();
  }
  write2log("fillIndexEntries IndexEntries: ", Dumper(\@IndexEntries)) if scalar(@IndexEntries);

  return [ @IndexEntries ];
}


sub returnDocumentPages {
  my ($pagelist, $DocumentData) = (shift, shift);
  write2log("returnDocumentPages pagelist:", Dumper($pagelist), "documentdata\n", Dumper($DocumentData));

  my ($msghdr, $msgtail) = (buildResponse($DocumentData) =~ /^(.*<\/IndexEntries>)(<\/RESPONSE>.*)$/s);

  require XReport::EXTRACT;
  require XReport::PDF::DOC;
  require MIME::Base64;

  my $query = XReport::EXTRACT->new();
  my $streamer = new B64streamer();
  $query->{docTo} = XReport::PDF::DOC->Create($streamer) ;
  $main::Response->Write($msghdr . '<DocumentBody>'); # serializer

  my $elist;
  eval { $elist = $query->ExtractPages(
				       QUERY_TYPE => 'FROMPAGES',
				       PAGE_LIST => \@$pagelist ,
				       FORMAT => "PRINT",
				       ENCRYPT => 'YES',
				       OPTIMIZE => 1,
				       REQUEST_ID => "GetDocById.".$main::Session->{'SessionID'},
				       #				   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"}
				      );
	};

  if ( scalar(@$elist) == 0 ) {
    EXIT_SOAP_ERROR("Sender", "IdNotFound", "DOCUMENTI NON TROVATI IN ARCHIVIO per la chiave richiesta - $@");
    return 0;
  }
  
  $main::Response->Write('</DocumentBody>'.$msgtail);
  $main::Response->Flush();
  my $outbytes = 33333;

  write2log("returnDocumentPages elist: ", scalar(@$elist), " InputDoc: ", $streamer->{InputBytes}, " B64Streamer: ", $streamer->{OutputBytes});
  return $streamer->{OutputBytes};
  
}

sub extractDocument {
  require XReport::ARCHIVE::OUTPUT; 

  my ($JobReportId, $msghdr, $msgtail) = (shift, shift, shift);
  my $zip = Open XReport::ARCHIVE::OUTPUT($JobReportId, wrapper => 1 );
  my $tmpdir = $ENV{USERPROFILE}.'/Application Data/CeDim/getDocument';
#-------------------------------------------
  my ($JobReportName ) =  @{$zip}{qw(JobReportName)};
 
  my $document = $zip->FileContents("$JobReportName\.$JobReportId\.0\.#0\.pdf");
  my ($buffsize, $outbytes) = (57*71, 0);

  write2log("extractDocument Size to be Encoded: ", length($document));
  $main::Response->Write(SOAPMSGHDR . $msghdr); # serializer
  while ($document) {
    (my $buffer, $document) = unpack("a$buffsize a*", $document);
    last unless length($buffer);
    $main::Response->Write(MIME::Base64::encode_base64($buffer));
    $outbytes += length($buffer);
    last unless  length($buffer) == $buffsize;
  }
  $main::Response->Write($msgtail . SOAPMSGTAIL );
  $main::Response->Flush();
  return $outbytes;
}

sub returnFullDocument {
  my ($JobReportId, $DocumentData) = (shift, shift);

  $DocumentData->{DocumentBody} = {content => 'BASE64ENCODEDGOESRIGHTHERE'};
  my ($msghdr, $msgtail) = (buildResponse($DocumentData) =~ /^(.*)BASE64ENCODEDGOESRIGHTHERE(.*)$/s);
  write2log("returnFulldocument JROD: $JobReportId msghdr:\n$msghdr\nmsgtail:\n$msgtail\n-------");

  eval { require XReport::ARCHIVE::OUTPUT; }; 
  if ($@) {
    write2log("Error during require of $JobReportId - $@");
    return EXIT_SOAP_ERROR("Sender", "returnFullDocument/openError", "Error during require of $JobReportId - $@");
  }

  my $zip;
  eval {$zip = Open XReport::ARCHIVE::OUTPUT($JobReportId, wrapper => 1 ); };
  if ($@) {
    write2log("Error during open for $JobReportId - $@");
    return EXIT_SOAP_ERROR("Sender", "returnFullDocument/openError", "Error during open of $JobReportId - $@");
  }
  my $tmpdir = $ENV{USERPROFILE}.'/Application Data/CeDim/getDocument';
#-------------------------------------------
  my ($JobReportName ) =  @{$zip}{qw(JobReportName)};
 
  my $document = $zip->FileContents("$JobReportName\.$JobReportId\.0\.#0\.pdf");
  my ($buffsize, $outbytes) = (57*71, 0);

  write2log("Size to be Encoded for $JobReportName $JobReportId: ", length($document));
  $main::Response->Write($msghdr); # serializer
  while ($document) {
    (my $buffer, $document) = unpack("a$buffsize a*", $document);
    last unless length($buffer);
    $main::Response->Write(MIME::Base64::encode_base64($buffer));
    $outbytes += length($buffer);
    last unless  length($buffer) == $buffsize;
  }
  $main::Response->Write($msgtail);
  $main::Response->Flush();
  return $outbytes;
}

sub returnDocument {
  my $DocumentData = shift;
  my $pagelist = ( shift );
  $DocumentData->{DocumentBody => {content => 'PDF GOES HERE'}};

  write2log("return page_list:", Dumper(@$pagelist));
  my $outbytes = ( (scalar(@$pagelist) == 3 && $pagelist->[1] eq "0" && $pagelist->[2] eq "-1") ?
		   returnFullDocument( $pagelist->[0] , $DocumentData) : 
		   returnDocumentPages($pagelist, $DocumentData ) );
  return $outbytes || 0;
}

sub _updateIndexEntries {
  my ($method, $selection, $reqident) = (shift, shift, shift);
  write2log("${method} $reqident:", Dumper($selection));

  my ($tbl, $maxlines, $IndexEntries, $orderby, $newentry) = @{$selection}{qw(IndexName TOP IndexEntries ORDERBY NewEntry)};
  my $orderby = 'JobReportId DESC' unless $orderby;

  my $dbc = initDBConn($tbl) || return;

  my @conds = ( buildClause($tbl, $IndexEntries) ); 
  return unless defined($conds[0]);

  my $select = "SELECT * FROM tbl_IDX_$tbl ";
  $select .= 'WHERE (' . join(') OR (', @conds ) .')' if scalar(@conds);
  $select .= ' ORDER BY ' . $orderby;
  write2log("SELECT:", $select);
  my $dbr; eval { $dbr = $dbc->dbExecute($select); };
  return EXIT_SOAP_ERROR("Receiver", "$method/DBSelect", "$@") if $@;

  my $idxlines = fillIndexEntries($dbr);
  
  my $DocumentData = {IndexName => $tbl }; 
  if (scalar(@$idxlines)) {
    my $idxcoltypes = {split / /, $main::Application->{'getDocument.'.$tbl.'ColTypes'} };
    my $setvars = 'SET '. join(',', map { "$_ =" . ($idxcoltypes->{$_} eq 'N' ? 
					    $newentry->[0]->{Columns}->{$_}->{content} : 
					    "'$newentry->[0]->{Columns}->{$_}->{content}'" 
					   ) 
				} keys %{$newentry->[0]->{Columns}} );
    
    $select =~ s/^SELECT\s+\*\s+FROM\s+(\w+)\s+(WHERE.*?)(?: ORDER BY .*){0,1}$/UPDATE $1 $setvars $2/; 
    write2log("UPDATE:", $select);
    my $dbr; eval { $dbr = $dbc->dbExecute($select); };
    return EXIT_SOAP_ERROR("Receiver", "$method/DBUPdate", "$@") if $@;
  }
  $DocumentData->{IndexEntries} = [ @$idxlines ] if scalar(@$idxlines) > 0;
  
  my $result = buildResponse($DocumentData);
  write2log("${method} result:", $result);
  
  $main::Response->Write( $result );
  
  $main::msgtolog = scalar(@$idxlines)."_updated_in_$tbl";
  return;
}

sub _removeIndexEntries {
  my ($method, $selection, $reqident) = (shift, shift, shift);
  write2log("${method} $reqident:", Dumper($selection));

  my ($tbl, $maxlines, $IndexEntries, $orderby, $newentry) = @{$selection}{qw(IndexName TOP IndexEntries ORDERBY NewEntry)};
  my $orderby = 'JobReportId DESC' unless $orderby;

  my $dbc = initDBConn($tbl) || return;

  my @conds = ( buildClause($tbl, $IndexEntries) ); 
  return unless defined($conds[0]);

  my $select = "SELECT * FROM tbl_IDX_$tbl ";
  $select .= 'WHERE (' . join(') OR (', @conds ) .')' if scalar(@conds);
  $select .= ' ORDER BY ' . $orderby;
  write2log("SELECT:", $select);
  my $dbr; eval { $dbr = $dbc->dbExecute($select); };
  return EXIT_SOAP_ERROR("Receiver", "$method/DBSelect", "$@") if $@;

  my $idxlines = fillIndexEntries($dbr);
  
  my $DocumentData = {IndexName => $tbl }; 
  if (scalar(@$idxlines)) {
    my $idxcoltypes = {split / /, $main::Application->{'getDocument.'.$tbl.'ColTypes'} };
    my $setvars = join(',', map { "$_ =" . ($idxcoltypes->{$_} eq 'N' ? 
					    $newentry->[0]->{Columns}->{$_}->{content} : 
					    "'$newentry->[0]->{Columns}->{$_}->{content}'" 
					   ) 
				} keys %{$newentry->[0]->{Columns}} );
    
    $select =~ s/^SELECT\s+\*\s+FROM\s+(\w+)\s+(WHERE.*?)(?: ORDER BY .*){0,1}$/DELETE FROM $1 $2/; 
    write2log("UPDATE:", $select);
    $dbr = $dbc->dbExecute("BEGIN TRANSACTION");
    my $dbr; eval { $dbr = $dbc->dbExecute($select); };
    if ($@) {
      $dbr = $dbc->dbExecute("ROLLBACK TRANSACTION");
      return EXIT_SOAP_ERROR("Receiver", "$method/DBDelete", "$@") if $@;
    } else {
      $dbr = $dbc->dbExecute("COMMIT TRANSACTION");
    }
  }
  $DocumentData->{IndexEntries} = [ @$idxlines ] if scalar(@$idxlines) > 0;
  
  my $result = buildResponse($DocumentData);
  write2log("${method} result:", $result);
  
  $main::Response->Write( $result );
  
  $main::msgtolog = scalar(@$idxlines)."_deleted_from_$tbl";
  return;
}

sub strip{
	my $str = $_[0];

	$str =~ s/</</g;
	$str =~ s/>/>/g;
	$str =~ s/[\x00-\x1F]/<b>.<\/b>/g;
	return $str;
}

sub TableHdr {
  my ($title) = (shift);
  $title .= " ".$main::Application->{'ApplName'};
  $main::Response->Write("<h2>$title</h2>");
  $main::Response->Write('<table title="'.$title.'" cellpadding=3 cellspacing=2 WIDTH="98%">');
  $main::Response->Write("<tr><th>Name</th><th>Value</th></tr>\n");
}

sub TableHdr3 {
  my ($title) = (shift);
  $main::Response->Write("<h3>$title</h3>");
  $main::Response->Write('<table title="'.$title.'" cellpadding=3 cellspacing=2 WIDTH="99%">');
  $main::Response->Write("<tr><th>Name</th><th>Value</th></tr>\n");
}

sub DumpNames(\%$)
{
  our ($sym, %sym, @sym);
	my ($package,$packname) =  @_;
	my $symname = 0;
	my $value = 0; 
	my $key = 0;
	my $i = 0;

# 	if ($packname eq 'main::') {

#		$main::Response->write("<H2>Packages</H2>\n");

#		foreach $symname (sort keys %$package) {
#			local *sym = $$package{$symname};
#			$main::Response->write("\t<A HREF=#Package_$symname>$symname</A>\n") if ((defined %sym) && ($symname =~/::/));
#		}
#	}

	$main::Response->write("<tr><td COLSPAN=2><h3><a name=\"Package_$packname\"::>Package $packname</a><h3>"); #Scalars Lists Hashes Functions
#	$main::Response->Write("<h2>$title</h2>");
	$main::Response->Write('<table cellpadding=3 cellspacing=2 WIDTH="99%">');

	$main::Response->Write("<tr><td colspan=2>"); TableHdr3 "Scalars";
	foreach $symname (sort keys %$package) {
		local *sym = $$package{$symname};
		if (defined $sym and strip($symname) !~ /Config_SH|summary/i) {
			#$value = ' 'x(length($symname) - 30)."\$$symname";
			#$value = $value."\t=".((length($sym) > 40) ? sprintf("%37.37s...",$sym) : $sym);
			$value = "<tr><td WIDTH=\"300\">\$".strip($symname)."</td><td WIDTH=\"600\">".strip($sym)."</td></tr>\n"; 
			$main::Response->write($value);
		}
	}
	$main::Response->write("</table></td></tr>");

	$main::Response->Write("<tr><td colspan=2>"); TableHdr3 "Functions";
	foreach $symname (sort keys %$package) {
		local *sym = $$package{$symname};
		$main::Response->write("<tr><td>$symname()</td><td/></tr>\n") if defined &sym;
	}
	$main::Response->write("</table></td></tr>");

	$main::Response->Write("<tr><td colspan=2>"); TableHdr3 "Lists";
	foreach $symname (sort keys %$package) {
		local *sym = $$package{$symname};
		$main::Response->write("<tr><td>\@$symname</td><td/></tr>\n") if defined @sym;
	}
	$main::Response->write("</table></td></tr>");

	$main::Response->Write("<tr><td colspan=2>"); TableHdr3 "Hashes";
	foreach $symname (sort keys %$package) {
		local *sym = $$package{$symname};
		$main::Response->write("<tr><td>\%$symname</td><td/></tr>\n") if ((defined %sym) && !($symname =~/::/));
	}
	$main::Response->write("</table></td></tr>");

 	if ($packname ne 'main::') {
	  $main::Response->write("</table></td></tr>");
	  return;
	}

	foreach $symname (sort keys %$package) {
		local *sym = $$package{$symname};
		DumpNames(\%sym,$symname) if ((defined %sym) && ($symname =~/::/) && ($symname ne 'main::'));
	}
	$main::Response->write("</table></td></tr>");
	$main::Response->Flush();
}

sub parseSOAPreq {
  my $stream = shift;
  write2log("parseSOAPreq request: ", $stream);

  $main::reqdata = xtractParm(XMLin($stream,
				 ForceArray => [ qw(NewEntry IndexEntries Columns ValuesArray) ],
				 KeyAttr => {'Columns' => 'colname'},
				),
			   qr/\w+:Body/);
  
  $main::reqdata->{soaphdrs} = xtractParm(XMLin($stream, NoAttr => 1, ), qr/soapenv:Header/);
}

sub SOAPreq2Hash {
  my $method = shift;
  my $HTTPStream = $main::Request->BinaryRead( $main::Request->TotalBytes() );

  parseSOAPreq $HTTPStream;
  
  write2log("$method SOAPreq2Hash XMLin SOAPStruct for -$method-:", Dumper( $main::reqdata ));

}

sub _GET_WSDL {
  return sendWSDL(WSDLOPERATIONS);
}

sub _GET_PERLNAMES {
  return DumpNames(%main::,'main::');
}

sub processRequest {
  $main::starttime = strftime "%Y-%m-%d %H:%M:%S", localtime;
  $main::starttime_local = strftime "%Y-%m-%d %H:%M:%S", localtime;
  $main::timesmark = [ times() ];


  my $reqmet   = $main::Request->ServerVariables('REQUEST_METHOD'    )->item();
  my $SOAPAct  = $main::Request->ServerVariables('HTTP_SOAPAction'   )->item();
  my $qrystr   = $main::Request->ServerVariables('QUERY_STRING'      )->item();
  my $reqident = $main::Request->ServerVariables('HTTP_REQUEST_ID'   )->item();
  (my $method = $SOAPAct || $qrystr) =~ s/\W*?(\w+)\W*/$1/;

  write2log("init Method: $reqmet, QSTRING: -$qrystr-, SOAPAct: $SOAPAct");
  write2log("${method} $reqident ALL_RAW:", $main::Request->ServerVariables('ALL_RAW')->item());

  if ( $main::Application->{'cfg.processed'} ) {
    $main::Response->Clear();
    $main::Response->{'Buffer'} = 1;
    
    write2log("$reqmet $method Request $reqident Started: ", join('::', times() ));
    $main::msgtolog = 'Function_aborted';
    if ( $reqmet =~ /^GET$/i ) {
      my $getsub = '_GET_'.uc($qrystr);
      return EXIT_HTTP_ERROR("Client", "HTTP GET $qrystr not supported") unless main->can($getsub);
      &$getsub();
    }
    else {
      $main::Response->{'ContentType'} = "text/xml";
      return EXIT_HTTP_ERROR("Client", "HTTP POST not supported without SOAPAction") unless $SOAPAct;
      return EXIT_SOAP_ERROR("Client", "invAction", "SOAP action $method not supported") unless main->can($method);
      &$method( $method , $reqident) ;
    }
    my $timesnow = [ times() ];
    for my $ix (0..3) {
      $timesnow->[$ix] = $timesnow->[$ix] - $main::timesmark->[$ix];
    }
    my $timesmsg = "Times(us::sy::cus::csy):_" . join('::', @{$timesnow} ) ;

    write2log("$reqmet $method Request $reqident  ended: ", $timesmsg, " - ", $main::msgtolog);
    $main::Response->AppendToLog("${method}_start_at_${main::starttime}_-ID_$reqident-_"
				      . $timesmsg . '_-_'
				      . $main::msgtolog 
				      . getSOAPHDR($main::reqdata)
				     );
    return $main::Response->Flush();
    
  }

  return $main::Session->Abandon() if ( !$main::Application->{'cfg.processed'} );
}

# -- webservice.pl

