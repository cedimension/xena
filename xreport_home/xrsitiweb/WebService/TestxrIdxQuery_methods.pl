#!/usr/bin/perl -w

use constant WSDLOPERATIONS => qw(getDocument getDocList getAllDoc);
use strict;

sub getDocList {
  my $method = shift;  
  my $reqident = shift || 'NO-IDENT';
  SOAPreq2HASH($method);

  my $selection = xtractParm($main::reqdata, 'REQUEST');
  write2log("${method} $reqident:", Dumper($selection));

  my ($tbl, $maxlines, $orderby, $IndexEntries) = @{$selection}{qw(IndexName TOP ORDERBY IndexEntries)};

  my $dbc = initDBConn($tbl) || return;

  my @conds = ( buildClause($tbl, $IndexEntries) ); 
  return unless defined($conds[0]);

  my $select = "SELECT TOP " . ($maxlines || 100) . " * FROM tbl_IDX_$tbl ";
  $select .= 'WHERE (' . join(') OR (', @conds ) .')' if scalar(@conds);
  $select .= ' ORDER BY '.$orderby if $orderby;

  write2log("${method} SELECT:", $select);
  my $dbr; eval { $dbr = $dbc->dbExecute($select); };
  return EXIT_SOAP_ERROR("Receiver", "$method/DBSelect", "$@") if $@;

#  return EXIT_SOAP_ERROR("Sender", "getDocList/QryResult", "Nessun Documento trovato per i criteri specificati") if $dbr->eof();
  my $idxlines = fillIndexEntries($dbr);
 
  my $DocumentData = {IndexName => $tbl }; 
  $DocumentData->{IndexEntries} = [ @$idxlines ] if scalar(@$idxlines) > 0;
		
  my $result = buildResponse($DocumentData);
  write2log("${method} result:", $result);
  
  $main::Response->Write( $result );
  $main::msgtolog = scalar(@$idxlines) . "_lines_from_${tbl}";

  return;
}

sub _GET_DOCLIST {
	$main::Response->Write(buildResponse({ IndexName => 'pippo',  TOP => 100,  ORDERBY => 'JobReportId', DocumentType =>'PDF',
					       IndexEntries => [ { JobReportId => '12345', FromPage => '2', ForPages => '1',
								   Columns => [
									       {colname => 'pippo', Min => 'string', content => 'pluto' },
									       {colname => 'topo', Min => 'string', content => 'paper'  },
									      ],
								 },
								 { JobReportId => '6789', FromPage => 3, ForPages => 7,
								   Columns => [
									       { colname => 'col1', Min => 'string', content => 'val1' },
									       { colname => 'col2', Min => 'string', content => 'val2' },
									      ],
								 },
							       ],
					       DocumentBody => { content => 'BASE 64 ENCODED STREAM', },
					     }));

  $main::msgtolog = "2_lines_from_pippo";
}

sub getDocById {
  my $method = shift;  
  my $reqident = shift || 'NO-IDENT';
  SOAPreq2HASH($method);

  my ($tbl, $DocList) = @{xtractParm($main::reqdata, 'REQUEST')}{qw(IndexName IndexEntries)};
  write2log("${method} $reqident:", Dumper($DocList));

  my @PAGE_LIST = map { @{$_}{qw(JobReportId FromPage ForPages)} } @$DocList;
  write2log("${method} page_list:", Dumper(@PAGE_LIST));

  return EXIT_SOAP_ERROR("Sender", "$method/NoPages", "NO PAGES FOUND IN RANGE") unless scalar(@PAGE_LIST);

#  my $outbytes = returnDocumentPages(
  my $outbytes = returnDocument(
				     [ @PAGE_LIST ], 
				     {IndexName => $tbl, 
				      DocumentType => 'PDF', 
				      IndexEntries => [ @$DocList ], 
				     });

  $main::msgtolog = scalar(@$DocList) . "_Documents Retrieved_-_" . "${outbytes}_sent_in_base64_encoded_format";

  return;
}

sub getDocument {
  my $method = shift;  
  my $reqident = shift || 'NO-IDENT';
  SOAPreq2HASH($method);

  my $selection = xtractParm($main::reqdata, 'REQUEST');
  write2log("${method} $reqident:", Dumper($selection));

  my ($tbl, $IndexEntries) = @{$selection}{qw(IndexName IndexEntries)};

 ##################################################################################################
 # CSE20070808 - Funzione getDocument prende in input anche l'intervallo di pagine se specificato  
 #               Altrimenti prende FromPage e ForPages dalla select effettuata  
 # Inizio personalizzazione
 ##################################################################################################
  
  my $FromPage = xtractParm($main::reqdata, 'FromPage');
  my $ForPages = xtractParm($main::reqdata, 'ForPages');
  
  my $DaPagina = 0;
  my $PerPagine = -1;
  
  $DaPagina = $FromPage if $FromPage > "";
  $PerPagine = $ForPages if $ForPages > "";
  
 
  ##################################################################################################
  # Fine personalizzazione
  ##################################################################################################
  
  my $dbc = initDBConn($tbl) || return;

  my @conds = ( buildClause($tbl, $IndexEntries) ); 
  return unless defined($conds[0]);

  my $select = "SELECT TOP 1 * FROM tbl_IDX_$tbl ";
  $select .= 'WHERE (' . join(') OR (', @conds ) .')' if scalar(@conds);
  $select .= ' ORDER BY JobReportId DESC';
  write2log("${method} SELECT:", $select);

  my $dbr = $dbc->dbExecute($select);
  return EXIT_SOAP_ERROR("Sender", "$method/QryResult", "Nessun Documento trovato per i criteri specificati") if $dbr->eof();
#  my $outbytes = returnDocumentPages([$dbr->Fields()->{JobReportId}->Value(), 1, 9999999], 
#				    {IndexName => $tbl, 
#				    DocumentType => 'PDF', 
#				    IndexEntries => fillIndexEntries($dbr), 
#				  });

 ##################################################################################################
 # CSE20070808 - Funzione getDocument prende in input anche l'intervallo di pagine se specificato  
 #               Altrimenti prende FromPage e ForPages dalla select effettuata
 # Inizio personalizzazione
 ##################################################################################################
  #my $outbytes = returnDocument([$dbr->Fields()->{JobReportId}->Value(), 0, -1], 
  $DaPagina  = $dbr->Fields->Item('FromPage')->Value() if $FromPage == "";
  $PerPagine = $dbr->Fields->Item('ForPages')->Value() if $ForPages == "";
  
  my $outbytes = returnDocument([$dbr->Fields()->{JobReportId}->Value(), $DaPagina, $PerPagine], 
 ##################################################################################################
 # Fine personalizzazione
 ##################################################################################################
				    {IndexName => $tbl, 
				    DocumentType => 'PDF',
				    IndexEntries => fillIndexEntries($dbr), 
				  });

  $main::msgtolog = "${outbytes}_sent_in_base64_encoded_format";

  return;
}
################################################################################
# Metodo: getAllDoc     [by CSE Cipollone]                                     #
# Restituisce tutti i documenti trovati dato un set di criteri di ricerca      #
################################################################################
sub getAllDoc {
  my $method = shift;
  my $reqident = shift || 'NO-IDENT';
  SOAPreq2HASH($method);

  # estraggo i criteri di ricerca dalla richiesta
  my $selection = xtractParm($main::reqdata, 'REQUEST');
  main::debug2log("${method} $reqident:", Dumper($selection));

  my ($tbl, $IndexEntries) = @{$selection}{qw(IndexName IndexEntries)};

  # connessione al db
  my $dbc = initDBConn($tbl) || return;

  # creo la clausola di where per estrazione da db
  my @conds = ( buildClause($tbl, $IndexEntries) );
  return unless defined($conds[0]);

  # preparo la query
  my $select = "SELECT * FROM tbl_IDX_$tbl ";
  $select .= 'WHERE (' . join(') OR (', @conds ) .')' if scalar(@conds);
  $select .= ' ORDER BY JobReportId DESC';
  main::debug2log("${method} SELECT:", $select);

  # eseguo la query su db
  my $dbr = $dbc->dbExecute($select);
  return EXIT_SOAP_ERROR("Sender", "$method/QryResult", "Nessun Documento trovato per i criteri specificati") if $dbr->eof();

  # preparo i risultati ottenuti
  my $idxlines = fillIndexEntries($dbr);

  # preparo i puntatori per estrarre i documenti richiesti
  my @PAGE_LIST = map { @{$_}{qw(JobReportId FromPage ForPages)} } @$idxlines;
  main::debug2log("${method} page_list:", Dumper(@PAGE_LIST));

  # estraggo i documenti richiesti
  my $outbytes = returnDocument(
				     [ @PAGE_LIST ],
				     {IndexName => $tbl,
				      DocumentType => 'PDF',
				      IndexEntries => [ @$idxlines ],
				     });

  $main::msgtolog = "${outbytes}_sent_in_base64_encoded_format";

  return;
}

sub _GET_DOCUMENT {
	$main::Response->Write(buildResponse({DocumentType => 'PDF', DocumentBody => {content => 'PDF GOES HERE'}} ));
	$main::msgtolog = "14_sent_in_base64_encoded_format";
}


1;
