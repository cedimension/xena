<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

use strict "vars";
$main::ASPVER = '1.1';

###$main::debugdir = "$ENV{TEMP}/CeWebService_Debug";
$main::debugdir = "$ENV{TEMP}/CeWebService_Debug";
$main::debugdir = "c:/CeWebService_Debug";

my $appldir  = $main::Request->ServerVariables('APPL_PHYSICAL_PATH')->item();
my @req = ((split /[\/.:]/, $main::Request->ServerVariables('URL')->item())[-2,-1]);
$main::servername = ($req[1] =~ /asp/ ? $req[0] : $req[1]);

$appldir = '.\\' unless $appldir;
require "${appldir}${main::servername}_methods.pl";
require "${appldir}webservice_new.pl";

return processRequest();

</SCRIPT>
