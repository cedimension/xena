<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

### vim: set syn=perl: ###

package XReport::DBUtil::MSSQLTypes;

use strict "vars";

use constant typeCode => {
	  adEmpty => 0,
	  adTinyInt => 16,
	  adSmallInt => 2,
	  adInteger => 3,
	  adBigInt => 20,
	  adUnsignedTinyInt => 17,
	  adUnsignedSmallInt => 18,
	  adUnsignedInt => 19,
	  adUnsignedBigInt => 21,
	  adSingle => 4,
	  adDouble => 5,
	  adCurrency => 6,
	  adDecimal => 14,
	  adNumeric => 131,
	  adVarNumeric => 139,
	  adBoolean => 11,
	  adError => 10,
	  adUserDefined => 132,
	  adVariant => 12,
	  adIDispatch => 9,
	  adIUnknown => 13,
	  adGUID => 72,
	  adDate => 7,
	  adDBDate => 133,
	  adDBTime => 134,
	  adDBTimeStamp => 135,
	  adFileTime => 64,
	  adDBFileTime => 137,
	  adBSTR => 8,
	  adChar => 129,
	  adVarChar => 200,
	  adLongVarChar => 201,
	  adWChar => 130,
	  adVarWChar => 202,
	  adLongVarWChar => 203,
	  adBinary => 128,
	  adVarBinary => 204,
	  adLongVarBinary => 205,
	  adChapter => 136,
	  adPropVariant => 138,
};

use constant codeAttr => {
	  0 => { Name => 'adEmpty', Type => 'S' },
	  16 => { Name => 'adTinyInt', Type => 'N' },
	  2 => { Name => 'adSmallInt', Type => 'N' },
	  3 => { Name => 'adInteger', Type => 'N' },
	  20 => { Name => 'adBigInt', Type => 'N' },
	  17 => { Name => 'adUnsignedTinyInt', Type => 'N' },
	  18 => { Name => 'adUnsignedSmallInt', Type => 'N' },
	  19 => { Name => 'adUnsignedInt', Type => 'N' },
	  21 => { Name => 'adUnsignedBigInt', Type => 'N' },
	  4 => { Name => 'adSingle', Type => 'N' },
	  5 => { Name => 'adDouble', Type => 'N' },
	  6 => { Name => 'adCurrency', Type => 'N' },
	  14 => { Name => 'adDecimal', Type => 'N' },
	  131 => { Name => 'adNumeric', Type => 'N' },
	  139 => { Name => 'adVarNumeric', Type => 'N' },
	  11 => { Name => 'adBoolean', Type => 'N' },
	  10 => { Name => 'adError', Type => 'S' },
	  132 => { Name => 'adUserDefined', Type => 'S' },
	  12 => { Name => 'adVariant', Type => 'S' },
	  9 => { Name => 'adIDispatch', Type => 'S' },
	  13 => { Name => 'adIUnknown', Type => 'S' },
	  72 => { Name => 'adGUID', Type => 'S' },
	  7 => { Name => 'adDate', Type => 'D' },
	  133 => { Name => 'adDBDate', Type => 'D' },
	  134 => { Name => 'adDBTime', Type => 'D' },
	  135 => { Name => 'adDBTimeStamp', Type => 'D' },
	  64 => { Name => 'adFileTime', Type => 'D' },
	  137 => { Name => 'adDBFileTime', Type => 'D' },
	  8 => { Name => 'adBSTR', Type => 'S' },
	  129 => { Name => 'adChar', Type => 'S' },
	  200 => { Name => 'adVarChar', Type => 'S' },
	  201 => { Name => 'adLongVarChar', Type => 'S' },
	  130 => { Name => 'adWChar', Type => 'S' },
	  202 => { Name => 'adVarWChar', Type => 'S' },
	  203 => { Name => 'adLongVarWChar', Type => 'S' },
	  128 => { Name => 'adBinary', Type => 'S' },
	  204 => { Name => 'adVarBinary', Type => 'S' },
	  205 => { Name => 'adLongVarBinary', Type => 'S' },
	  136 => { Name => 'adChapter', Type => 'S' },
	  138 => { Name => 'adPropVariant', Type => 'S' },
};

sub getTypeCode {
  my ($class, $tn) = (shift, shift);
  return undef unless exists typeCode->{$tn};
  return typeCode->{$tn};
}

sub getCodeAttr {
  my ($class, $cd) = (shift, shift);
  return codeAttr->{13} unless exists codeAttr->{$cd};
  return codeAttr->{$cd};
}

1;

package main;
$main::debugdir = "$ENV{TEMP}/CeWebService_Debug";
###$main::debugdir = "c:/CeWebService_Debug";

use POSIX qw(strftime);

use lib("$ENV{'XREPORT_HOME'}/perllib");

use strict "vars";

use FileHandle;
use File::Path;
use Data::Dumper;

#use Win32::OLE qw(in);
#use Win32::OLE::Variant;

use XReport::DBUtil;
#use XReport::XMLUtil qw(SQL2XML);
use MIME::Base64;
use XML::Simple;

our ($Server, $Application, $Session, $Request, $Response, $XREPORT_HOME, $ASPVER, $ScriptHost);
$XREPORT_HOME = $ENV{'XREPORT_HOME'};
$ASPVER = '1.1';

use constant WSDL => <<'ENDWSDL'
<?xml version="1.0" encoding="UTF-8"?>
<wsdl:definitions
    name="XRSOAPSERVERNAME"
    targetNamespace="http://cereport.org/WebService"
    xmlns:apachesoap="http://xml.apache.org/xml-soap"
    xmlns:impl="http://cereport.org/WebService"
    xmlns:intf="http://cereport.org/WebService"
    xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"
    xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <wsdl:types>
        <xsd:schema
            elementFormDefault="qualified"
            targetNamespace="http://cereport.org/WebService"
            xmlns="http://www.w3.org/2001/XMLSchema"
            xmlns:apachesoap="http://xml.apache.org/xml-soap"
            xmlns:impl="http://cereport.org/WebService"
            xmlns:intf="http://cereport.org/WebService"
            xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
            <element name="PdfDocument">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element maxOccurs="1" minOccurs="1" name="Pdf" type="xsd:base64Binary"/>
                    </xsd:sequence>
                </xsd:complexType>
            </element>
            <xsd:complexType name="COLAttrs">
                <xsd:sequence>
                    <xsd:element maxOccurs="1" minOccurs="1" name="Var" type="xsd:string"/>
                    <xsd:element maxOccurs="1" minOccurs="1" name="Min" type="xsd:string"/>
                    <xsd:element maxOccurs="1" minOccurs="0" name="Max" type="xsd:string"/>
                </xsd:sequence>
            </xsd:complexType>
            <xsd:complexType name="SELRow">
                <xsd:sequence>
                    <xsd:element maxOccurs="unbounded" minOccurs="1" name="Key" type="impl:COLAttrs"/>
                </xsd:sequence>
            </xsd:complexType>
            <xsd:complexType name="LogicalReport">
                <xsd:sequence>
                    <xsd:element maxOccurs="1" minOccurs="1" name="JobReportId" type="xsd:int"/>
                    <xsd:element maxOccurs="1" minOccurs="1" name="FromPage" type="xsd:int"/>
                    <xsd:element maxOccurs="1" minOccurs="1" name="ForPages" type="xsd:int"/>
                    <xsd:element maxOccurs="1" minOccurs="0" name="KeyList" type="impl:SELRow"/>
                </xsd:sequence>
            </xsd:complexType>
            <xsd:complexType name="RowsList">
                <xsd:sequence>
                    <xsd:element maxOccurs="unbounded" minOccurs="1" name="KeyList" type="impl:SELRow"/>
                </xsd:sequence>
            </xsd:complexType>
            <element name="Query">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element maxOccurs="1" minOccurs="1" name="TableName" type="xsd:string"/>
                        <xsd:element maxOccurs="1" minOccurs="1" name="TOP" type="xsd:int"/>
                        <xsd:element maxOccurs="1" minOccurs="1" name="WHERE" type="impl:RowsList"/>
                        <xsd:element maxOccurs="1" minOccurs="0" name="ORDERBY" type="xsd:string"/>
                    </xsd:sequence>
                </xsd:complexType>
            </element>
            <element name="ListOfDocuments">
                <xsd:complexType>
                    <xsd:sequence>
                        <xsd:element
                            maxOccurs="unbounded"
                            minOccurs="1"
                            name="Document"
                            type="impl:LogicalReport"/>
                    </xsd:sequence>
                </xsd:complexType>
            </element>
        </xsd:schema>
    </wsdl:types>
    <wsdl:message name="PdfResult">
        <wsdl:part element="intf:PdfDocument" name="Pdf"/>
    </wsdl:message>
    <wsdl:message name="QueryMsg">
        <wsdl:part element="intf:Query" name="SELECT"/>
    </wsdl:message>
    <wsdl:message name="DocumentListMsg">
        <wsdl:part element="intf:ListOfDocuments" name="ListOfDocuments"/>
    </wsdl:message>
    <wsdl:portType name="XRSOAPSERVERNAMEMethods">
        <wsdl:operation name="getDocList">
            <wsdl:input message="intf:QueryMsg"/>
            <wsdl:output message="intf:DocumentListMsg"/>
        </wsdl:operation>
        <wsdl:operation name="getDocById">
            <wsdl:input message="intf:DocumentListMsg"/>
            <wsdl:output message="intf:PdfResult"/>
        </wsdl:operation>
    </wsdl:portType>
    <wsdl:binding name="XRSOAPSERVERNAMEBinding" type="intf:XRSOAPSERVERNAMEMethods">
        <soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
        <wsdl:operation name="getDocList">
            <soap:operation soapAction="getDocList"/>
            <wsdl:input>
                <soap:body use="literal"/>
            </wsdl:input>
            <wsdl:output>
                <soap:body use="literal"/>
            </wsdl:output>
        </wsdl:operation>
        <wsdl:operation name="getDocById">
            <soap:operation soapAction="getDocById"/>
            <wsdl:input>
                <soap:body use="literal"/>
            </wsdl:input>
            <wsdl:output>
                <soap:body use="literal"/>
            </wsdl:output>
        </wsdl:operation>
    </wsdl:binding>
    <wsdl:service name="XRSOAPSERVERNAME">
        <wsdl:port binding="intf:XRSOAPSERVERNAMEBinding" name="XRSOAPSERVERNAMEPort">
            <soap:address location="HTTP://n239199b:80/CeWebService/XRSOAPSERVERNAME.asp" />
        </wsdl:port>
    </wsdl:service>
</wsdl:definitions>
ENDWSDL
;

use constant SOAPMSGHDR =>
  '<?xml version="1.0" encoding="UTF-8"?>'
  . '<soap:Envelope '
  . 'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" '
  . 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
  .' xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
  . '<soap:Body >'
  ;

use constant SOAPMSGTAIL =>
  '</soap:Body >'
  . '</soap:Envelope >'
  ;

sub soaphdr {
  return SOAPMSGHDR;
}

sub soaptail {
  return SOAPMSGTAIL;
}

sub soapwsdl {
  return WSDL;
}

sub write2log {
  return 1 unless -d $main::debugdir;
  my $mplog = new FileHandle '>>'.$main::debugdir.'/'.$main::servername.'.srvc.log';
  $mplog->write($Session->{SessionID} . " " . localtime() . " $ASPVER: " . join('', @_)."\n");
  $mplog->close;
}

sub xmlquote {
  my $t = shift; $t =~ s/ +$//g;

  $t =~ s/&/&amp;/g;
  $t =~ s/</&lt;/g;
  $t =~ s/>/&gt;/g;
  $t =~ s/\"/&quot;/g;
  $t =~ s/\'/&apos;/g;

  $t =~ s/([\x80-\xff])/"\&\#".unpack("C",$1).";"/eg;

  return $t;
}

sub xtractParm {
  my ($struct, $parm) = (shift, shift);
  my $result = undef;
  if (ref($struct) eq 'HASH') {
    foreach my $k ( keys %$struct ) {
      return  $struct->{$k} if $k =~ /^$parm$/i;
      $result = xtractParm($struct->{$k}, $parm);
      return $result if $result;
    }
  }
  elsif (ref($struct) eq 'ARRAY') {
    foreach my $el ( @$struct ) {
      $result = xtractParm($el, $parm);
      return $result if $result;
    }
  }
  return undef;
}

sub EXIT_HTTP_ERROR {
  my $respmsg = "<faultResponse faultcode=\"".xmlquote($_[0])."\" faultstring=\"".xmlquote($_[1])."\"/>";
  write2log("EXIT_HTTP_ERROR: ", $respmsg);

  $Response->{'ContentType'} = "text/xml";
  $Response->{Status} = "500 Xreport Web Service Server Error";
  $Response->AddHeader("Connection", "close");
  $Response->Write( $respmsg );
  $Response->Flush();
#  $Response->End();
  return;

}

sub EXIT_SOAP_ERROR {
  my $respmsg = ('<?xml version="1.0" encoding="UTF-8"?>'
		 . '<SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema/instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">'
		 . '<SOAP-ENV:Body>'
		 . '<SOAP-ENV:Fault>'
		 . "<faultcode>$_[0]</faultcode>"
		 . "<faultactor>".$main::servername.'/'.$_[1]."</faultactor>"
		 . "<faultstring>$_[2]</faultstring>"
		 . '</SOAP-ENV:Fault>'
		 . '</SOAP-ENV:Body>'
		 . '</SOAP-ENV:Envelope>'
		);
  write2log("EXIT_SOAP_ERROR: ", $respmsg);

  $Response->{Status} = "500 Xreport Web Service Server Error";
  $Response->AddHeader("Connection", "close");
  $Response->Write( $respmsg );
  $Response->Flush();
  return;
}

sub getHTTPHDR {
  my $httphdr = '';
  for my $j ( 1..$Request->{'ServerVariables'}->{'Count'} ) {
    my $keyn = $Request->{'ServerVariables'}->Key($j);
    next if $keyn !~ /^HTTP_(.*)$/;
    $httphdr .= '_hdr-' . $1 .'|' . $Request->ServerVariables($keyn)->Item();
 }

  return $httphdr;

}

sub getSOAPHDR {
  my $hdrs = shift->{soaphdrs};
  my $soaphdr = '';
  foreach my $keyn ( keys %$hdrs ) {
    (my $hdrid = $keyn) =~ s/:/_/g;
    $soaphdr .= '_hdr-' . $hdrid .'|' . $hdrs->{$keyn};
  }
  return $soaphdr;

}

sub sendWSDL {

    my $wsdlhost = $Application->{$main::servername.'.wsdlhost'} = $Request->ServerVariables('SERVER_NAME')->item();
    my $serverpath = ($Request->ServerVariables('HTTPS')->item() eq 'on' ? ''
		      . 'HTTPS://' . $wsdlhost . ':'
		      . $Request->ServerVariables('SERVER_PORT_SECURE')->item() :
		      'HTTP://' .$wsdlhost . ':'
		      . $Request->ServerVariables('SERVER_PORT')->item() )
                      . $Request->ServerVariables('URL')->item();
	     ;

    (my $wsdl = WSDL) =~ s/<soap:address\s+location=".*"\s*\/>/<soap:address location="$serverpath" \/>/im;
    $wsdl =~ s/XRSOAPSERVERNAME/$main::servername/img;
    $Response->{'ContentType'} = "text/xml";
    $Response->Write($wsdl);
    return $Response->Flush();

}

sub buildCond {
      my ($var, $min, $max, $typ) = (shift, shift, shift, shift);
# insert date format validation 'yyyy-mm-gg hh:mm:ss'
      $min = "'$min'" unless (!$min or $typ =~ /^N$/i);
      $max = "'$max'" unless (!$max or $typ =~ /^N$/i);
      write2log("KEY: $var $typ $min $max");

      if ($min ne '' and $max ne '') {
	if ($min =~ /^'{0,1}LIKE'{0,1}$/) {
	  return " $var LIKE $max ";
	}
	elsif ($min =~ /^'{0,1}IN'{0,1}$/) {
          my $qt = ($typ =~ /^N$/i ? '' : "'");
          return " $var IN (" . join($qt.', '.$qt, split(/\s*\x09/, $max)) . ") ";
	}
	else {
	  return " $var BETWEEN $min AND $max";
	}
      }
      elsif ($min ne '') {
	return " $var >= $min ";
      }
      elsif ($max ne '') {
	return " $var <= $max ";
      }
      return undef;
}

sub getDocList {

  my $reqdata = shift;
  my $reqident = shift || 'NO-IDENT';

  write2log("Request Started: ", join('::', times() ));
  my $query = xtractParm($reqdata, 'Query');
  write2log("getDocList Query $reqident:", Dumper($query));

  my ($tbl, $maxlines, $where) = @{$query}{qw(TableName TOP WHERE)};
  my $orderby = exists($query->{'ORDERBY'}) ? $query->{'ORDERBY'} : 'JobReportId DESC';
  my $dbc; eval {$dbc = XReport::DBUtil->get_ix_dbc($tbl);};
  return EXIT_SOAP_ERROR("Receiver", "getDocList/DBInit", "$@") if $@;

  return EXIT_SOAP_ERROR("Receiver", "getDocList/DBConnect", "ApplInit: DataBase Connector Lost") unless $dbc;

  if ( !$Application->{'getDocument.'.$tbl.'ColTypes'} ) {
    my $dbr = $dbc->dbExecute("SELECT TOP 1 * FROM tbl_IDX_$tbl");
    if ($dbr->eof) {
      $Application->{'getDocument.'.$tbl.'ColTypes'} = '__EMPTY__';
    }
    else {
      my $fields = $dbr->Fields();
      my @COLAttrs;
      for my $fldn (0..$fields->Count()-1) {
	push @COLAttrs, my $var = $fields->Item($fldn)->Name();
	push @COLAttrs, XReport::DBUtil::MSSQLTypes->getCodeAttr($fields->{$var}->Type())->{Type};
      }
      $Application->{'getDocument.'.$tbl.'ColTypes'} = join(' ', @COLAttrs );
    }
  }

  return EXIT_SOAP_ERROR("Receiver", "getDocList/DBAccess", "Tabella indici vuota") if ($Application->{'getDocument.'.$tbl.'ColTypes'} eq '__EMPTY__');

  my $idxcoltypes = {split / /, $Application->{'getDocument.'.$tbl.'ColTypes'} };
  write2log("COLAttrs $tbl:", Dumper($idxcoltypes));

  my $select = "SELECT TOP " . ($maxlines || 100) . " * FROM tbl_IDX_$tbl ";
  write2log("Parsing Request: ", join('::', times() ));

  my @or;
  foreach my $row ( @{$where->[0]->{KeyList}} ) {
    my @and;
    foreach my $key ( @{$row->{Key}} ) {
      my ($var, $min, $max) = @{$key}{qw(Var Min Max)};
      write2log("ForEach: Checking now $var");

      return EXIT_SOAP_ERROR("Sender", "getDocList/IDXAccess", "Variable $var Richiesta non in tabella $tbl") unless exists $idxcoltypes->{$var};

      push @and, buildCond($var, $min, $max, $idxcoltypes->{$var}) ||
	return EXIT_SOAP_ERROR("Sender", "getDocList/QuerySyntax", "Valori di selezione non specificati per $var");
    }
    push @or, '(' .join(') AND (', @and) . ')' if scalar(@and);
  }
  $select .= 'WHERE (' . join(') OR (', @or) .')' if scalar(@or);
  $select .= ' ORDER BY ' . $orderby . ' ';
  write2log("SELECT:", $select);

  my $dbr = $dbc->dbExecute($select);
#  return EXIT_SOAP_ERROR("Sender", "getDocList/QryResult", "Nessun Documento trovato per i criteri specificati") if $dbr->eof();

  my $result;
  my $idxlines = 0;
  while (!$dbr->eof()) {
    my $fields = $dbr->Fields();
    $result .= '<Document><JobReportId>' . $fields->{JobReportId}->Value() . '</JobReportId>'
      . '<FromPage>'. $fields->{FromPage}->Value() . '</FromPage>'
	. '<ForPages>'. $fields->{ForPages}->Value() . '</ForPages><KeyList>';
    $idxlines++;
    for my $fldn (0..$fields->Count()-1) {
      my $var = $fields->Item($fldn)->Name();
      next if $var =~ /JobReportId|FromPage|ForPages/;
      my $codeType = $fields->{$var}->Type();
      my $type = XReport::DBUtil::MSSQLTypes->getCodeAttr($codeType)->{Type};
#      my $tname = XReport::DBUtil::MSSQLTypes->getCodeAttr($codeType)->{Name};
#      my $type1 = XReport::DBUtil::MSSQLTypes->getCodeAttr("$codeType")->{Type};
#      my $tname1 = XReport::DBUtil::MSSQLTypes->getCodeAttr("$codeType")->{Name};
#     write2log("code: ", $codeType, " type1: -", $type1, "-", " tname1: -", $tname1, "-");
      my $val = $fields->{$var}->Value();
#      write2log("code: -$codeType- tname: -$tname- Var=$var Type= -$type- Val=$val ref=-",ref($val), '-');
      $val = ($val->can('Date') ? $val->Date('yyyy-MM-dd') : '')." ".($val->can('Time') ? $val->Time('hh:mm:ss tt') : '') if ref($val);
      $result .= "<Key><Var>$var</Var><Min>$val</Min></Key>";
    }

    $result .= '</KeyList></Document>';
    $dbr->MoveNext()
  }

  write2log("result:", $result);
#  return EXIT_SOAP_ERROR("Sender", "getDocList/ResultEmpty", "Nessun Documento Trovato") unless $result;

  $Response->Write(SOAPMSGHDR
		   . '<ListOfDocuments xmlns="http://cereport.org/WebService">'
		   . $result
		   . '</ListOfDocuments>'
		   . SOAPMSGTAIL);

  write2log("Request ended: ", join('::', times() ));
  $Response->Flush();
  return $Response->AppendToLog("GetDocList_start_at_${main::starttime}" . "_-ID_$reqident-_"
				. "Times(us::sy::cus::csy):_" .  join('::', times() ) . "_-_"
				. "${idxlines}_lines_from_${tbl}" . getSOAPHDR($reqdata)
			       );


}

sub getDocById {
  my $reqdata = shift;
  my $reqident = shift || 'NO-IDENT';

  my ($pageList, $ExtractList, @PAGE_LIST, $pdf, $pdf64);

  my $DocList = ( $reqdata->{ListOfDocuments}->[0]->{Document} );

  write2log("Request Started: $reqident ", join('::', times() ));
  @PAGE_LIST = map { @{$_}{qw(JobReportId FromPage ForPages)} } @$DocList;
  write2log("page_list:", Dumper(@PAGE_LIST));

  require XReport::EXTRACT;
  require XReport::PDF::DOC;
  require MIME::Base64;

  if ( scalar(@PAGE_LIST) == 0 ) {
    return EXIT_SOAP_ERROR("Sender", "getDocById/NoPages", "NO PAGES FOUND IN RANGE");
  }

  my $query = XReport::EXTRACT->new();

  my $tmpdir = $ENV{USERPROFILE}.'/Application Data/CeDim/getDocument';
  eval { mkpath($tmpdir) } unless -e $tmpdir;;
  return EXIT_SOAP_ERROR("Sender", "getDocById/DirCreate", "Couldn't create $tmpdir: $@") if $@;

  my $elist;
   eval { $elist = $query->ExtractPages(
					TO_DIR => $tmpdir,
					QUERY_TYPE => 'FROMPAGES',
					PAGE_LIST => \@PAGE_LIST,
					FORMAT => "PRINT",
					ENCRYPT => 'YES',
					OPTIMIZE => 1,
					REQUEST_ID => "GetDocById.".$Session->{'SessionID'},
					#				   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"}
				       );
	};

  if ( scalar(@$elist) == 0 ) {
    return EXIT_SOAP_ERROR("Sender", "getDocById/IdNotFound", "DOCUMENTI NON TROVATI IN ARCHIVIO per la chiave richiesta");
  }

  my $fname = $elist->[0]->[1];
  my $IN = FileHandle->new();

  open($IN, "<$fname")
    or
      return EXIT_SOAP_ERROR("Sender", "getDocById/TmpFileOperError", "OPEN ERROR FOR file \"$fname\" - $!");
  binmode($IN);

  write2log("Encoded Size: ", -s $fname);

  my ($buffsize, $outbytes, $j) = (57*71, 0, 0);

  $Response->Write(SOAPMSGHDR . '<PdfDocument xmlns="http://cereport.org/WebService"><Pdf>'); # serializer

  while (1) {
    my $bread = sysread($IN, my $buffer, $buffsize);
    last unless $bread;
        $Response->Write(MIME::Base64::encode_base64($buffer));
    $outbytes += length($buffer);
    last unless $bread == $buffsize;
  }
  $IN->close();
  unlink $fname;
  write2log("bytes written: $outbytes ", join('::', times() ));

  $Response->Write('</Pdf></PdfDocument>' . SOAPMSGTAIL );
  $Response->Flush();
  return $Response->AppendToLog("GetDocById_start_at_${main::starttime}_-ID_$reqident-_"
				. "Times(us::sy::cus::csy):_" .  join('::', times() ) . '_-_'
				. scalar(@$DocList) . "_Documents Retrieved_-_"
				. "${outbytes}_sent_in_base64_encoded_format" . getSOAPHDR($reqdata)
			       );

}

$main::starttime =
    strftime "%Y-%m-%d %H:%M:%S", localtime;
return $Session->Abandon() if ( !$Application->{'cfg.processed'} );

my $reqmet = $Request->ServerVariables('REQUEST_METHOD')->item();
my $SOAPAct = $Request->ServerVariables("HTTP_SOAPAction")->item();
my $qrystr = $Request->ServerVariables('QUERY_STRING')->item();
my $appldir = $Request->ServerVariables('APPL_PHYSICAL_PATH')->item();
my $reqident = $Request->ServerVariables('HTTP_REQUEST_ID')->item();
my @req = ((split /[\/.:]/, $Request->ServerVariables('URL')->item())[-2,-1]);
$main::servername = ($req[1] =~ /asp/ ? $req[0] : $req[1]);
$main::debug = 1 if $main::servername =~ /debug/i;

write2log("init $main::servername: Method: $reqmet, QSTRING: -$qrystr-, SOAPAct: $SOAPAct");

$Response->Clear();
$Response->{'Buffer'} = 1;

#return sendWSDL() if $reqmet.$qrystr =~ /^GETWSDL$/i;

if ( $reqmet =~ /^GET$/i ) {
  if ($qrystr =~ /^wsdl$/i) {
    return sendWSDL();
  }
  elsif (($qrystr =~ /^testpdfou$/i)) {
    $Response->Write(SOAPMSGHDR
		     . '<PdfDocument xmlns="http://cereport.org/WebService"><Pdf>PDF GOES HERE</Pdf></PdfDocument>'
		     . SOAPMSGTAIL
		    );
    write2log("Request ended: ", join('::', times() ));
    return $Response->Flush();
  }
  elsif (($qrystr =~ /^testdoclist$/i)) {
    $Response->Write(SOAPMSGHDR
		     . '<ListOfDocuments xmlns="http://cereport.org/WebService">'
		     . '<Document>'
		     . '<JobReportId>1</JobReportId><FromPage>1</FromPage><ForPages>1</ForPages>'
		     . '<KeyList>'
		     . '<Key><Var>NDG</Var><Min>01230078056</Min></Key>'
		     . '<Key><Var>DATARIF</Var><Min>2005-11-11</Min></Key>'
		     . '</KeyList>'
		     . '</Document>'
		     . '</ListOfDocuments>'
		     . SOAPMSGTAIL
		    );
    write2log("Request ended: ", join('::', times() ));
    return $Response->Flush();
  }
  return EXIT_HTTP_ERROR("Client", "HTTP GET $qrystr not supported");
}

return EXIT_HTTP_ERROR("Client", "HTTP POST not supported without SOAPAction") unless $SOAPAct;
write2log("SOAPAction ", $SOAPAct);

$Response->{'ContentType'} = "text/xml";

my $HTTPStream = $Request->BinaryRead( $Request->TotalBytes() );
write2log("HTTPstream ", Dumper($HTTPStream));

my $reqdata = xtractParm(XMLin($HTTPStream,
			       NoAttr => 1,
			       ForceArray => [ qw(WHERE KeyList Key ListOfDocuments Document) ],
#			       KeyAttr => { Key => 'Var'},
			      ),
			 qr/\w+:Body/);

$reqdata->{soaphdrs} = xtractParm(XMLin($HTTPStream, NoAttr => 1, ), qr/soapenv:Header/);


write2log("reqdata ", Dumper($reqdata));

#my $method = [ keys %$reqdata ]->[0];
my $method = $Request->ServerVariables("HTTP_SOAPAction")->item();
$method =~ s/\W*(\w+)\W*/$1/;
write2log("XMLin SOAPStruct for -$method-:", Dumper( $reqdata ));

return EXIT_SOAP_ERROR("Client", "invAction", "SOAP action $method not supported") unless defined &$method;

return &$method( $reqdata , $reqident) ;

#$HTTPStream = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><getDocList xmlns=""><Query><TOP>100</TOP><TableName>CCINDEX</TableName><WHERE><Key><Var>CDG</Var><Min>193687</Min><Max>193687</Max></Key><Key><Var>DATA_RIF</Var><Min>2005-01-01</Min><Max>2005-12-01</Max></Key><Key><Var>JobReportId</Var><Min>64</Min><Max>64</Max></Key></WHERE></Query></getDocList></soapenv:Body></soapenv:Envelope>';

</SCRIPT>
