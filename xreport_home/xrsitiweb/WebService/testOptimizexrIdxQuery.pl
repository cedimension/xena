#!/usr/bin/perl -w
use strict "vars";

use Data::Dumper;#$Data::Dumper::Terse = 1; $Data::Dumper::Indent = 1;
#use SOAP::Lite +trace => debug;
use SOAP::Lite;

use MIME::Base64;
use XML::Simple;

sub xtractParm {
  my ($struct, $parm) = (shift, shift);
  my $result = undef;
  if (ref($struct) eq 'HASH') {
    foreach my $k ( keys %$struct ) {
      return  $struct->{$k} if $k =~ /^$parm$/i;
      $result = xtractParm($struct->{$k}, $parm);
      return $result if $result;
    }
  }
  elsif (ref($struct) eq 'ARRAY') {
    foreach my $el ( @$struct ) {
      $result = xtractParm($el, $parm);
      return $result if $result;
    }
  }
  return undef;
}

sub XRwebRequest {
 my $parms = { @_ };

 my $server = $parms->{server} || '127.0.0.1:80';
 delete $parms->{server};
 my $service = $parms->{service} || 'xrIdxQuery';
 delete $parms->{service};
 my $method = $parms->{method} || 'getDocList';
 delete $parms->{method};

 my $RQSTattr = {};
 for my $attr (qw(IndexName TOP ORDERBY DocumentType FileName temporary)) {
   if (exists($parms->{$attr})) {
     $RQSTattr->{$attr} = $parms->{$attr};
     delete $parms->{$attr};
   }
 }
 my $DocumentData = {};
 foreach my $valname ( qw(IndexEntries NewEntries) ) {
   next unless exists $parms->{$valname};
   foreach my $ie ( @{$parms->{$valname}}) {
     my @attrn = [ grep !/^Columns$/, keys %{$ie} ];
     my $attrs = {};
     @{$attrs}{@attrn} = @{$ie}{@attrn};
     push @{$DocumentData->{$valname}}, SOAP::Data->value({Columns => $ie->{Columns}});
   }
 }
 

my $client = ( SOAP::Lite->uri($parms->{server})
		->service('http://'.$server.'/'.$service.'.asp?wsdl')
		->proxy('http://'.$server.'/'.$service.'.asp')
		->on_action(sub{'"' . join('', @_). '"'})
#		->namespace('soapenv')
		->encprefix('soapenc')
		->envprefix('soapenv')
		->autotype(0)
#		->on_debug(sub{print@_})
		->outputxml(1));

my $result = $client->call($method,
		       SOAP::Data->name('REQUEST')->attr( $RQSTattr )
		       ->value(XMLout({'IndexEntries' => $parms->{IndexEntries}}, KeepRoot => 1, NoIndent => 1)
			       . XMLout({'NewEntries' => $parms->{NewEntries}}, KeepRoot => 1, NoIndent => 1)
			      )
		      )
 ;
 return $result;
}

my ($server) = $ARGV[0];

#my $search = [{Columns => [
#			   {colname => 'ABI', Min => '03209', Max => '03209'},
#			   {colname => 'DOC', Min => '01000', Max => '01000'},
#			   {colname => 'CDG', Min => '9011825', Max => '9011825'},
#			  ]
#	      },
#	      {Columns => [
#			   {colname => 'ABI', Min => '03209', Max => '03209'},
#			   {colname => 'DOC', Min => '01000', Max => '01000'},
#			   {colname => 'CDG', Min => '9014129', Max => '9014129'},
#			  ]
#	      },
#	      {Columns => [
#			   {colname => 'ABI', Min => '03209', Max => '03209'},
#			   {colname => 'DOC', Min => '01000', Max => '01000'},
#			   {colname => 'CDG', Min => '9014354', Max => '9014354'},
#			  ]
#	      },
#	     ];
my $search = [
	    #{JobreportId => '10765', FromPage => '1', ForPages => '1', Columns => [ ] },
	    #  {JobReportId => '10769', FromPage => '1', ForPages => '1', Columns => [ ] },
	    #  {JobReportId => '10771', FromPage => '1', ForPages => '1', Columns => [ ] },
{JobReportId => '16134', FromPage => '1', ForPages => '402', Columns=>[]},
{JobReportId => '16135', FromPage => '1', ForPages => '804', Columns=>[]},
{JobReportId => '16136', FromPage => '1', ForPages => '2', Columns=>[]},
{JobReportId => '16137', FromPage => '1', ForPages => '1', Columns=>[]},

#{JobReportId => '16089', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16088', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16087', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16086', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16085', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16084', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16083', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16082', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16081', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16080', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16079', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16078', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16077', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16076', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16075', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16074', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16073', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16072', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16071', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16070', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16069', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16068', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16067', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16066', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16065', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16064', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16063', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16062', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16061', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16060', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16059', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16058', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16057', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16056', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16055', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16054', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16053', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16052', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16051', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16050', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16049', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16048', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16047', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16046', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16045', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16044', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16043', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16042', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16041', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16040', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16039', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16038', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16037', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16036', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16035', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16034', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16033', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16032', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16031', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16030', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16029', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16028', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16027', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16026', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16025', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16024', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16023', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16022', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16021', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16020', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16019', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16018', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16017', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16016', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16015', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16014', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16013', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16012', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16011', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16010', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16009', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16008', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16007', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16006', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16005', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16004', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16003', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16002', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16001', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '16000', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '15999', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '15998', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '15997', FromPage => '1', ForPages => '1', Columns=>[]},
#{JobReportId => '15996', FromPage => '1', ForPages => '1', Columns=>[]},
	     ];
my $update = [];

my $pdfbase64 = {};
foreach my $uri ( qw(xrIdxQuery TestxrIdxquery) ) {
#foreach my $uri ( qw(TestxrIdxquery) ) {
	print "1: ", time(), "\n";
  my $xmlresult = '<resp>'.XRwebRequest( server => $server, service => $uri, method => 'getDocById', 
			     IndexName => 'LC03209', TOP => 10, ORDERBY => 'JobReportId', DocumentType => 'PDF',
			     IndexEntries => $search ,  NewEntries => $update,
			   ).'</resp>';
  print "=========> \n", substr($xmlresult, 0, 200), "\n", substr($xmlresult, -200, 200), "\n";

	print "2: ", time(), "\n";
	use Symbol;
	my $handle = gensym();
	open($handle, ">out.xml") or die "FDFDDDDD: $!\n";
	print $handle $xmlresult;
	close($handle);
	print "3: ", time(), "\n";
  my $result = XMLin($xmlresult);
	print "4: ", time(), "\n";

   $pdfbase64->{$uri} =
   (exists($result->{'soap:Body'}) ? delete $result->{'soap:Body'}->{'RESPONSE'}->{'DocumentBody'}
				: $result->{'DocumentBody'});
	print "5: ", time(), "\n";

  print "=========> \n", join('::', keys %$result
#    Dumper($result
#			       ->{'soap:Body'}
#			       ->{'RESPONSE'}
#			       ->{'DocumentBody'}
			      ), "\n<===========\n" if $result;



  
  print join ', ',
    $result->faultcode(),
      $result->faultactor(),
	$result->faultstring(),
	  $result->faultdetail() if $result && ref($result ne 'HASH') && $result->fault();
  
  print "No Results returned\n" unless $result;
}
use FileHandle;

while (my ($uri, $pdf64) = each %$pdfbase64 ) {
  my $pdfdata = decode_base64($pdf64);  
  print "$uri \t===> ", length($pdfdata), "\n";
  my $fh = new FileHandle(">$uri.pdf");
  binmode $fh;
  $fh->syswrite($pdfdata, length($pdfdata));
  close $fh;
}
exit 0;

