#!/usr/bin/perl -w

use constant WSDLOPERATIONS => qw(getDocument deleteIndex deleteDocument updateIndex persistDocument storeDocument );
use strict "vars";


sub getDocument {
  my $method = shift;  
  my $reqident = shift || 'NO-IDENT';
  SOAPreq2HASH($method);

  my $selection = xtractParm($main::reqdata, 'REQUEST');
  write2log("${method} $reqident:", Dumper($selection));

  my ($tbl, $IndexEntries) = @{$selection}{qw(IndexName IndexEntries)};

  my $dbc = initDBConn($tbl) || return;

  my @conds = ( buildClause($tbl, $IndexEntries) ); 
  return unless defined($conds[0]);

  my $select = "SELECT TOP 1 * FROM tbl_IDX_$tbl ";
  $select .= 'WHERE (' . join(') OR (', @conds ) .')' if scalar(@conds);
  $select .= ' ORDER BY JobReportId DESC';
  write2log("${method} SELECT:", $select);


  my $dbr = $dbc->dbExecute($select);
  return EXIT_SOAP_ERROR("Sender", "$method/QryResult", "Nessun Documento trovato per i criteri specificati") if $dbr->eof();
  my $outbytes = returnDocumentPages([$dbr->Fields()->{JobReportId}->Value(), 1, 9999999], 
				    {IndexName => $tbl, 
				    DocumentType => 'PDF', 
				    IndexEntries => fillIndexEntries($dbr), 
				  });

  $main::msgtolog = "${outbytes}_sent_in_base64_encoded_format";

  return;
}

sub _GET_DOCUMENT {
	$main::Response->Write(buildResponse({DocumentType => 'PDF', DocumentBody => {content => 'PDF GOES HERE'}} ));
}

sub _deleteIndexEntries {
  my ($method, $selection, $reqident) = (shift, shift, shift);
  write2log("${method} $reqident:", Dumper($selection));

  my ($tbl, $maxlines, $IndexEntries, $orderby) = @{$selection}{qw(IndexName TOP IndexEntries ORDERBY)};
  $orderby = 'JobReportId DESC' unless $orderby;

  my $dbc = initDBConn($tbl) || return;

  my @conds = ( buildClause($tbl, $IndexEntries) ); 
  return unless defined($conds[0]);

  my $select = "SELECT * FROM tbl_IDX_$tbl ";
  $select .= 'WHERE (' . join(') OR (', @conds ) .')' if scalar(@conds);
  $select .= ' ORDER BY ' . $orderby;
  write2log("SELECT:", $select);

  my $idxlines = fillIndexEntries($dbc->dbExecute($select));
  return EXIT_SOAP_ERROR("Sender", "$method/QryResult", "too many rows to be deleted from index table $tbl") if scalar(@$idxlines) > 200;
  
  my $DocumentData = {IndexName => $tbl }; 
  if (scalar(@$idxlines)) {
    $select =~ s/^SELECT\s+\*\s+FROM/DELETE/; 
    write2log("DELETE:", $select);
  }
  $DocumentData->{IndexEntries} = [ @$idxlines ] if scalar(@$idxlines) > 0;
		
  my $result = buildResponse($DocumentData);
  write2log("${method} result:", $result);
  
  $main::Response->Write( $result );
  $main::msgtolog = scalar(@$idxlines)."_deleted_from_$tbl";

  return;
}

sub deleteIndex {
  my $method = shift;
  SOAPreq2HASH($method);
  my $reqident = shift || 'NO-IDENT';

  my $selection = xtractParm($main::reqdata, 'REQUEST');

  return _deleteIndexEntries($method, $selection, $reqident);
}

sub deleteDocument {
  my $method = shift;
  SOAPreq2HASH($method);
  my $reqident = shift || 'NO-IDENT';

  my $selection = xtractParm($main::reqdata, 'REQUEST');
  @{$selection}{qw(IndexName)} = ("GS03570");
  return _deleteIndexEntries($method, $selection, $reqident);
}

sub _updateIndexEntries {
  my ($method, $selection, $reqident) = (shift, shift, shift);
  write2log("${method} $reqident:", Dumper($selection));

  my ($tbl, $maxlines, $IndexEntries, $orderby, $newentry) = @{$selection}{qw(IndexName TOP IndexEntries ORDERBY NewEntry)};
  $orderby = 'JobReportId DESC' unless $orderby;

  my $dbc = initDBConn($tbl) || return;

  my @conds = ( buildClause($tbl, $IndexEntries) ); 
  return unless defined($conds[0]);

  my $select = "SELECT * FROM tbl_IDX_$tbl ";
  $select .= 'WHERE (' . join(') OR (', @conds ) .')' if scalar(@conds);
  $select .= ' ORDER BY ' . $orderby;
  write2log("SELECT:", $select);

  my $idxlines = fillIndexEntries($dbc->dbExecute($select));
  
  my $DocumentData = {IndexName => $tbl }; 
  if (scalar(@$idxlines)) {
    my $setvars = Dumper($newentry);
    $select =~ s/^SELECT\s+\*\s+FROM\s+(\w+)\s+(WHERE.*)$/UPDATE $1 $setvars $2/; 
    write2log("UPDATE:", $select);
  }
  $DocumentData->{IndexEntries} = [ @$idxlines ] if scalar(@$idxlines) > 0;
		
  my $result = buildResponse($DocumentData);
  write2log("${method} result:", $result);
  
  $main::Response->Write( $result );
  
  $main::msgtolog = scalar(@$idxlines)."_updated_in_$tbl";
  return;
}

sub updateIndex {
  my $method = shift;
  SOAPreq2HASH($method);
  my $reqident = shift || 'NO-IDENT';

  my $selection = xtractParm($main::reqdata, 'REQUEST');
  return _updateIndexEntries($method, $selection, $reqident);
}

sub persistDocument {
  my $method = shift;
  SOAPreq2HASH($method);
  my $reqident = shift || 'NO-IDENT';

  my $selection = xtractParm($main::reqdata, 'REQUEST');
  $selection->{IndexName} = "GS03570";
  $selection->{NewEntry}->[0]->{Columns}->[0]->{VAR1}->{content} = "1";
  return _updateIndexEntries($method, $selection, $reqident);
}

sub storeDocument {
  use Compress::Zlib;

  my $method = shift;
  my $reqident = shift || 'NO-IDENT';

  my $TotalBytes = $main::Request->TotalBytes();
  my ($RequestData, $RequestDocument) = ('', '');
  my $buffref = \$RequestData;
  while($TotalBytes > 0) {
    my $bytes2read = ($TotalBytes > 4096 ? 4096 : $TotalBytes);
    write2log("${method} $reqident request: reading $bytes2read of $TotalBytes left");
    
    $RequestData .= $main::Request->BinaryRead($bytes2read); 
    $TotalBytes -= $bytes2read;
    write2log("${method} $reqident request: checking contents data: ", length($RequestData), " bytes ");
    if ( $RequestData =~ /^(.*)\<DocumentBody\s*\>(.*)$/is ) {
      ($RequestData, $RequestDocument) = ($1, $2);
      write2log("${method} $reqident request: switching to Document ", 
		"data: ", length($RequestData), " bytes ",
		"document: ", length($RequestDocument), " bytes "
	       );
      last
    }
  }
  ($RequestDocument, undef) = ($RequestDocument =~ /^(.*)(\<\/DocumentBody\s*\>.*){0,1}$/is);
  write2log("${method} $reqident request: switching to Document ", 
		"document: ", length($RequestDocument), " bytes "
	       );
  return EXIT_SOAP_ERROR("Receiver", "Receive", "Request contains no document ") unless ($TotalBytes || length($RequestDocument)) ;
  $RequestData .= '<DocumentBody>REFERTOIN</DocumentBody></REQUEST></soap:Body></soap:Envelope>';
  
  parseSOAPreq($RequestData);
  write2log("${method} $reqident request:", Dumper($main::reqdata));

  my $selection = xtractParm($main::reqdata, 'REQUEST');
  my ($tbl, $IndexEntries, $doctype, $rmtfile, $Recipient) = @{$selection}{qw(IndexName IndexEntries DocumentType FileName DocRecipient)};
  $doctype = $tbl unless $doctype;

  my $reqid; eval {$reqid = QCreate XReport::QUtil(
				     SrvName        => $main::servername,
				     JobReportName  => $doctype,
				     XferRecipient  => $Recipient,
				     RemoteFileName => $rmtfile || 'UNKNOWN',
				     LocalFileName  => '.',
				     RemoteHostAddr => $main::Request->ServerVariables('REMOTE_ADDR')->item(),
				     XferStartTime  => $main::starttime_local,
				     XferMode       => 100,
				     XferDaemon     => $main::servername,
				     JobOrigin      => $main::Request->ServerVariables('REMOTE_HOST')->item() || 'UNKNOWN',
				     JobName        => $method,
				     JobNumber      => $main::Request->ServerVariables('INSTANCE_ID')->item(),
				     JobExecutionTime => $main::starttime_local,
				     Status         => $CD::stAccepted,
				     XferId         => $reqident,
				     );
		 };
  return EXIT_SOAP_ERROR("Receiver", "QCreate", "$@") if $@;
  my $ofil = "$doctype." . strftime('%Y%m%d%H%M%S', localtime ). ".$reqid";
  my $odir = strftime('%Y/%m%d', localtime);

  (my $destpath= $main::Application->{'cfg.localpath.L1'}."/IN/$odir") =~ s/^file:\/\///;
  eval { mkpath($destpath) };
  if ($@) {
    return EXIT_SOAP_ERROR("Receiver", "dircreate", "$@") if $@;
  }

  write2log("${method} $reqident ALL_RAW:", $main::Request->ServerVariables('ALL_HTTP')->item(), " destpath: ", $destpath);

  my $ofh = gzopen("$destpath/$ofil.DATA.TXT.gz", "wb");
  my ($buffsize, $databytes) = (4056, length($RequestDocument));

  while($TotalBytes > 0) {
    my $bytes2read = ($TotalBytes > $buffsize ? ($buffsize - length($RequestDocument)) : $TotalBytes);

    my $buffer = $main::Request->BinaryRead($bytes2read); 
    $TotalBytes -= $bytes2read;

    if ( ($RequestDocument.$buffer) =~ /^(.*)(<\/DocumentBody\s*\>.*)$/is ) {
      $RequestDocument .= $1;
    }
    else {
      $RequestDocument .= $buffer;
    }
    my @b64lines = split(/[\n\r][\n\r]{0,1}/, $RequestDocument);
    $RequestDocument = ( length($b64lines[-1]) < 76 ? pop @b64lines : '') ;
    my $wb = $ofh->gzwrite(MIME::Base64::decode_base64(join('', @b64lines)));
    $databytes += $wb;

  }
  if ($RequestDocument) {
    my $lastbuff = MIME::Base64::decode_base64($RequestDocument);
    my $wb = $ofh->gzwrite($lastbuff);
    $databytes += $wb;
  }
  $ofh->gzclose();
  write2log("${method} $reqident Document is ", $databytes, " bytes");

  $ofh = new FileHandle(">$destpath/$ofil.CNTRL.TXT");
  my $wb = $ofh->syswrite( $main::Request->ServerVariables('ALL_HTTP')->item()."\n".$RequestData );
  $ofh->close();

  my $req;eval {$req = QUpdate XReport::QUtil(
					      SrvName        => $main::servername,
					      LocalFileName  => "$odir/$ofil.DATA.TXT.gz",
					      JobReportName  => $doctype,
					      ReportName     => $doctype,
					      XferInBytes    => $main::Request->TotalBytes(),
					      XferOutBytes   => (-s "$destpath/$ofil.DATA.TXT.gz") + (-s "$destpath/$ofil.CNTRL.TXT"),
					      Status         => $CD::stAccepted,
					      Id             => $reqid
				  );
	      };
  return EXIT_SOAP_ERROR("Receiver", "QUpdate", "$@") if $@;
  map { $_->{JobReportId} = $reqid } @$IndexEntries;
  my $DocumentData = {IndexName => $tbl, DocumentType => $doctype,
		      IndexEntries => [ @$IndexEntries ],
		    };
  my $result = buildResponse($DocumentData);
  $main::Response->Write( $result );
  
  write2log("${method} result:", $result);
  $main::msgtolog = "${reqid}_JobReportId_created_".scalar(@{$DocumentData->{IndexEntries}})
    ."_".$DocumentData->{IndexName}."_entries_tobe_inserted";
  return;
}

1;
