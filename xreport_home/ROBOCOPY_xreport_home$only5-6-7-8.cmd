echo off
setlocal EnableDelayedExpansion
IF /I [%1]==[dryrun] (
echo.dryrun mode activated
SET NO_A1TEST=Y
SET only_LOG=/L
) ELSE (
echo.normal mode activated
SET only_LOG=
)
SET dirFrom=%~dp0
SET filesToExclude= /XF log_moveBackup.txt ^
 /XF log_moveBackup.txt ^
 /XF __Thumbs.db ^
 /XF _Thumbs.db ^
 /XF __BUNDLE.pm ^
 /XF log_MIG.txt/XF unxmit.log ^
 /XF .project ^
 /XF BUNDLE.pm ^
 /XF Centera.pm.20170111 ^
 /XF defin_functions.pl_OLD ^
 /XF doAssemblyBundles.pl ^
 /XF FTP.pm ^
 /XF JUtil.pm ^
 /XF log_MIG.txt ^
 /XF MVS.pm ^
 /XF MVSJES.pm ^
 /XF STANDARD.pl ^
 /XF log_START_ALL-XN_SERVICES.txt ^
 /XF xrDispatcher.pl.new ^
 /XF xrMigrate.pl ^
 /XF XReport.pm ^
 /XF XREPORTN_A1.xml ^
 /XF CTD00061.xml ^
 /XF CTD2UPNC_A1.xml ^
 /XF CTDXA4NC_A1.xml ^
 /XF doPrintMission.cmd ^
 /XF doPrintMissionEnv.cmd ^
 /XF ReportLoadedList.cmd ^
 /XF Send_Bundle_Manual.cmd ^
 /XF *.20170217 ^
 /XF XREPORTN_A1.xml
:: /XF BUNDLE.pm /XF JUtil.pm /XF STANDARD.pl /XF FTP.pm /XF MVS.pm /XF MVSJES.pm /XF doAssemblyBundles.pl 
:: /XF JUtil.pm
::set lastFolder with the name of the last folder
SET lastFolder=xreport_home$\
SET lastFolder2=\xreport_home\
cd C:\Windows\system32
for /F "usebackq tokens=1,2,3,4,5,6,7 delims=-//.:, " %%a IN (`echo %%date%%-%%time%%`) do set my_datetime=%%a-%%b-%%c-%%d-%%e-%%f
echo %my_datetime%
SET currentLastFolder=%dirFrom:~-14%
IF /I NOT [%lastFolder%]==[%currentLastFolder%] (
IF /I NOT [%lastFolder2%]==[%currentLastFolder%] (
echo The current last folder [%currentLastFolder%] is not [%lastFolder%]
pause
exit -1
)
)
SET log=log_robocopy_
SET fileLog=LOG\%log%_%my_datetime%.txt
SET MAX_INDEX_SERVER=9
IF /I [%NO_A1TEST%]==[Y] (
echo NO_A1TEST mode
SET MAX_INDEX_SERVER=8
)
FOR /L %%G IN (5,1,!MAX_INDEX_SERVER!) DO (
SET dirTo=\\C0CTLPW00%%G\%lastFolder%
IF /I [%%G]==[9] (
SET dirTo=\\C0ctlpw004\d$\xena\xreport_home_A1TEST
)
robocopy "%dirFrom% " "!dirTo! " /E /MAX:10485760 /XO /XD backup /XD old /XD LOG /XD .svn /XF %log%* /XF *.txt /XF *.swp /XF *.7z /XF *.swn /XF *.swo /XF *.back /XF *.bac* /XF *.old /XF *.Copy.* /XF *.Copia.* /XF *.baK /XF *TESTSAN* %only_LOG% %filesToExclude% /LOG+:%dirFrom%%fileLog%
)
echo.---------------------------------------------------------------------- >> %dirFrom%%fileLog%
type "%dirFrom%%fileLog%" | findstr /I /c:"newer" /c:"New File" /c:"EXTRA File" /c:"Dest " >> "%dirFrom%%fileLog%"
echo.type "%dirFrom%%fileLog%" ^| findstr /I /c:"newer" /c:"Dest " ^>^> "%dirFrom%%fileLog%"
notepad %dirFrom%%fileLog%
::max 10 MB /MAX:10485760
::cartelle backup escluse /XD backup
::cartelle old escluse /XD old
::cartelle .svn escluse /XD .svn
::file *.back esclusi /XF *.back /XF *.old
::file log esclusi /XF %log%*  /XD LOG
::/L :: List only - don't copy, timestamp or delete any files.