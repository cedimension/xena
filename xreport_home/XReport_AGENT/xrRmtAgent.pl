#!/usr/local/bin/perl -w
#######################################################################
# @(#) $Id: xrDaemon.pl 2250 2008-07-02 12:50:03Z mpezzi $ 
#  
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
use strict;
use Data::Dumper;
#use MIME::Base64;
use XML::Simple;
use FileHandle;
use File::Path;
use File::HomeDir;
use Socket;

sub xtractParm {
  my ($struct, $parm) = (shift, shift);
  my $result = undef;
  if (ref($struct) eq 'HASH') {
    foreach my $k ( keys %$struct ) {
      #      main::debug2log("checking struct '", $k, "' for '$parm' result: ", $k =~ /^$parm$/i);
      return  $struct->{$k} if $k =~ /^$parm$/i;
      $result = xtractParm($struct->{$k}, $parm);
      return $result if $result;
    }
  } elsif (ref($struct) eq 'ARRAY') {
    foreach my $el ( @$struct ) {
      $result = xtractParm($el, $parm);
      return $result if $result;
    }
  }
  return undef;
}

BEGIN {
  ($main::spath = $0) =~ s/[\/\\][^\\\/]+$//;
#  print "SPATH: ", $main::spath, "\n";
}

use lib($main::spath);

my $version = do { my @r = (q$Revision: 1.3 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

my @parms = @ARGV;
while ( @parms ) {
  #  print "PARMS: ", join("::", @parms), "\n";
  $_ = shift @parms;
  ( 
   /^-N$/ ? do { $main::servername   = shift @parms; }
   #   : /^-U$/ ? do { $main::xruri      = shift @parms; }
   : /^-S$/ ? do { $main::xrhost     = shift @parms; }
   #   : /^-P$/ ? do { $main::thisaddr = shift @parms; }
   #   : /^-msgmax$/ ? do { $main::msgmax = shift @parms; }
   : /^-testrun$/ ? do { $main::testrun = 1; }
   : 1
  );
}

use Win32;
use Win32::OLE;
use Win32::Daemon;
use Win32::Console;
use Win32::Process;
use Win32::EventLog;

$main::EventLog = Win32::EventLog->new("Application", '.') or die "Can't open Application EventLog\n";
sub doEventLog {
  my $eid = shift;
  my $typ = ($eid =~ /9$/ ? EVENTLOG_ERROR_TYPE : EVENTLOG_INFORMATION_TYPE);
  my @es = ($main::servername || 'N/A', $0, (caller($eid =~ /9$/ ? 1 : 0))[0,2], @_);
  $main::EventLog->Report(my $ei = {
				    Source => "XReport",
				    Computer => '.',
				    EventType => $typ,
				    Category => 0,
				    EventID => $eid,
				    Strings => join("\0", @es),
				    Data => "$es[0] Error at $es[2]:$es[3] in $es[1] - $es[4] -",
#				    Data => "Failure of $es[1] detected by Service $es[0] - WorkQueue key $es[2] - ExternalKey key $es[3] -cfg ref: ". ref($cfg) ."\n" 
#				    .Dumper($cfg),
				   });
}

sub error_exit {
 warn sprintf("error at %s:%s", (caller())[0,2]), " - ", @_, "\n";
 doEventLog("10009", @_);
 die "10009 - ", @_;
}

sub SaveFile {
  my ($hashref, $ele) = (shift, shift);
  my $pfx = ($_[0] ? "$_[0]_" : '');
  return unless exists($hashref->{$ele});

  my ($fname, $content, $encoding, $type) = @{$hashref->{$ele}}{qw(name content encoding type)};
  return unless $fname && $content;

  $XReport::cfg->{$ele} = "$main::workdir/$pfx$fname";
#  print "ELE: $ele -> $main::workdir/$pfx$fname CFG: ", Dumper($XReport::cfg), "\n";
  
  my $codefh = new FileHandle(">$XReport::cfg->{$ele}");
  binmode $codefh;
#  my $fsz = 0;
  my $buffsz = 57 * 76; 
  while ( $content ) {
#    print "ref: $buffsz - content size: ", length($content), "\n";
    (my $buff, $content) = (length($content) le $buffsz ? ($content, '') : unpack("a$buffsz a*", $content));
    if ( $encoding && $encoding eq 'base64' ) {
      my @rows = split /\n/, $buff;
      $content = pop(@rows) . $content if length($rows[-1])%4 ;
      $buff = join('', map { tr#A-Za-z0-9+/##cd;                   # remove non-base64 chars
                             tr#A-Za-z0-9+/# -_#;                  # convert to uuencoded format
                             unpack("u", pack("c", 32 + 0.75*length($_) ) . $_) } @rows);
#      $buff = join('', map { MIME::Base64::decode_base64($_) } @rows);
    }
#    $fsz += length($buff);
    print $codefh $buff;
  } 
  close $codefh;
#  print "Written $fsz bytes to $ele\n";
  delete $hashref->{$ele};
}

unless ($main::servername) {
  my $objWMIService = Win32::OLE->GetObject
    ("winmgmts:{impersonationLevel=impersonate}\\\\.\\root\\CIMV2") or error_exit "WMI connection failed";
  my $colItems = $objWMIService->ExecQuery
    ("Select * from Win32_Service Where State <> 'Stopped' and ProcessId = $$","WQL", 0x10 | 0x20 ); #  wbemFlagReturnImmediately => 0x10; wbemFlagForwardOnly => 0x20;
  $main::servicename = '';
  for my $item (in $colItems){
    last if $main::servicename = $item->{Name};
    #  my $properties = {map { $_ => $item->{$_} } qw(name ProcessId DisplayName Status)};
    #  print "ITEM ", $main::ct++, Dumper($properties), "\n";
    #  last;
  }
  $main::servername = $main::servicename;
}

$main::lclcfg = { servername => $main::servername, servicename => $main::servicename || 'N/A', xrhost => $main::xrhost,  debug => 0 };
my $lclcfgfn = "$main::spath\\$main::servername.cfg"; 
if ( -e $lclcfgfn ) {
  my $lclcfg = {};
  eval { $lclcfg = XMLin($lclcfgfn, KeepRoot => 1, ForceArray => [qw(daemon dbase ReqredCode RunAtRefresh)], Variables => $main::lclcfg ); };
  unless ($@) {
    
    $main::lclcfg = { %{$main::lclcfg}, %{$lclcfg->{daemon}->{$main::servername}} } if exists($lclcfg->{daemon}->{$main::servername});
  }
  $main::lclcfg->{RefreshInterval} = 120 unless $main::lclcfg->{RefreshInterval};
}
#die Dumper($main::lclcfg);
$main::xrhost = $main::lclcfg->{xrhost} unless $main::xrhost;
$main::debug  = $main::lclcfg->{debug}  unless $main::debug;
$main::servername  = $main::lclcfg->{servername}  unless $main::servername;
#($main::servername, $main::xrhost, $main::debug) = @{$main::lclcfg}{qw(servername xrhost debug)};

error_exit "xrDaemon Server name unspecified - ARGS: ", @ARGV unless $main::servername;
#$main::testrun = !($main::servicename) & 1 unless $main::testrun;

unless ( $main::xrhost ) {
  my $ip_address = inet_aton('xrconfigserver');
  $main::xrhost = "$ip_address\:60080" if $ip_address;
}
error_exit "XReport Config server unspecified" unless $main::xrhost;


print "Running now in command line mode\n" if $main::testrun;
#error_exit("testing eventlog: $main::testrun");

sub refreshConfig {
  use LWP;

  my $r = LWP::UserAgent->new;
  my $cfgdatafn = "$main::workdir/$main::servername\_CONFIG.xml";
  
  my $cfgresp = $r->post( "http://$main::xrhost/ConfigServer/getCentralConfig", 
			  [ server => $main::servername, 
			    cfguser => $ENV{USERNAME}, 
			  ] );

# die Dumper($cfgresp);
  i::logit("Error response from server - ", $cfgresp->status_line) unless $cfgresp->is_success();
  return undef if !$cfgresp->is_success() && !$XReport::cfg;

  unless ( !$cfgresp->is_success || ($XReport::cfg && $main::cfgmtime && $main::cfgmtime == $cfgresp->header('ConfigMtime')) ) {

    $main::cfgmtime = $cfgresp->header('ConfigMtime');

    my $servercfg = XMLin($cfgresp->content(), KeepRoot=>1, ForceArray => [qw(daemon dbase ReqredCode RunAtRefresh)]);
    # die Dumper($cfgresp->content());
    # die Dumper($XReport::cfg);
    my $daemoncfg = $servercfg->{daemon};
    $daemoncfg = $daemoncfg->[0] if ref($daemoncfg) eq 'ARRAY';
    error_exit "Unable to find $main::servername section" unless exists $daemoncfg->{$main::servername};
    #die Dumper($daemoncfg);
    $daemoncfg->{$main::servername} = { %{$main::lclcfg}, %{$daemoncfg->{$main::servername}} };
=pod

ServerCode config item contains the perl code to be invoked as server

=cut
    SaveFile $daemoncfg->{$main::servername}, 'ServerCode', $main::servername;
=pod

ClientCode config item contains the perl code that the server will eventually invoke

=cut
    SaveFile $daemoncfg->{$main::servername}, 'ClientCode', $main::servername;
    #  die Dumper($daemoncfg);
=pod

ReqrdCode config item contains all the files nedded by the server 

=cut
    while ( my ($fn, $finfo) = each %{$daemoncfg->{$main::servername}->{ReqredCode}} ) {
      #    print "REQUIRED: $fn LL: ", length($finfo->{content} || ''), "\n";
      #    print Dumper(\{'dummy' => { name => $fn, %$finfo } }), "\n";
      SaveFile {$fn => { name => $fn, %$finfo } }, $fn, ''; 
    }
    delete $daemoncfg->{$main::servername}->{ReqredCode};
    
=pod

RunAtRefresh config item contains all the perl scripts that has to be invoked at refresh time 

=cut
    $XReport::cfg->{"RunAtRefresh"} = [];
    while ( my ($fn, $finfo) = each %{$daemoncfg->{$main::servername}->{RunAtRefresh}} ) {
#      die "FN: $fn INFO: ", Dumper($finfo), "\n";
      
      SaveFile {$fn => { name => $fn, %$finfo } }, $fn, ''; 
#      @{$XReport::cfg->{"RunAtRefresh"}} = grep { $_->{name} ne $XReport::cfg->{$fn} } @{$XReport::cfg->{"RunAtRefresh"}};
      push @{$XReport::cfg->{"RunAtRefresh"}}, 
	{ name => $XReport::cfg->{$fn}, 
	  cmd => "perl -e "
	  . "\"use XML::Simple;\$XReport::cfg = XMLin('$cfgdatafn', ForceArray => [qw(daemon dbase ReqredCode RunAtRefresh)]);" #$xmlfn');"
	  . "require '$main::preCodefn';"
	  . "do '$XReport::cfg->{$fn}';\" -- -N $main::servername" 
	};
      
    }
    delete $daemoncfg->{$main::servername}->{RunAtRefresh};
    $XReport::cfg->{daemon} = $daemoncfg;
    
    $XReport::cfg->{cfgdatafn} = $cfgdatafn;
    
    my $cfgdatafh = new FileHandle(">$cfgdatafn");
    binmode $cfgdatafh;
    print $cfgdatafh XMLout($XReport::cfg, KeepRoot => 1);
    close $cfgdatafh;
    
    $XReport::cfg->{daemon}->{$main::servername}->{RefreshInterval} = 120 unless $XReport::cfg->{daemon}->{$main::servername}->{RefreshInterval};

    $XReport::cfg->{launchcmd} = "perl -e "
      . "\"use XML::Simple;\$XReport::cfg = XMLin('$XReport::cfg->{cfgdatafn}', ForceArray => [qw(daemon dbase ReqredCode RunAtRefresh)]);" #$xmlfn');"
	. "require '$main::preCodefn';"
	  . "do '$XReport::cfg->{ServerCode}';\" -- -N $main::servername";
    #  die Dumper($XReport::cfg);
  }

  if ( exists( $XReport::cfg->{RunAtRefresh}) ) {
    foreach my $attach ( @{$XReport::cfg->{RunAtRefresh}} ) {
#      print "Invoking refresh code name: $attach->{name} cmd: $attach->{cmd}\n";
      i::logit( "xrRmtAgent Invoking $attach->{name} for $main::servername refresh" );
      Win32::Process::Create( my $PrObj, "$^X", $attach->{cmd}, 1, Win32::Process->NORMAL_PRIORITY_CLASS, "." )
	  || i::logit("ERROR Launching $attach->{cmd} - $!");
      i::logit("Invoked Refresh Code for $main::servername pid: " .$PrObj->GetProcessID()) if $PrObj;

    }
  }
  return $XReport::cfg;
}


$main::workdir = File::HomeDir->my_data()."/XReport/$main::servername";
mkpath $main::workdir unless -d $main::workdir;
error_exit "unable to create \"$main::workdir\" - $!" unless -d $main::workdir; 

doEventLog("10001", "xrDaemon started ", "PID: ", $$, " workdir: ", $main::workdir, " ARGS: ", @ARGV);
($main::logfn = "$main::workdir/$main::servername\_.log") =~ s/\\/\//g;

($main::preCodefn = "$main::workdir/$main::servername\_precode.pl") =~ s/\\/\//g;

$main::preCodefh = new FileHandle(">$main::preCodefn") || error_exit "unable to open file - $!";
print $main::preCodefh join("\n", 
#			    "use XML::Simple;",
#			    "\$XReport::cfg = XMLin('$XReport::cfg->{cfgdatafn}', KeepRoot => 1, ForceArray => [qw(daemon dbase files)]);",
			    "use FileHandle;",
			    "use constant DBGDEST => '226.1.1.9:3000';",
			    "use IO::Socket::INET;",
			    "\$main::logfh = new FileHandle(\">>$main::logfn\") || die \"unable to open file - \$!\";",
			    "(\$main\:\:workdir = '$main::workdir') =~ s/
			    "my \$oldfh = select \$main::logfh;",
			    "\$| = 1;",
			    "select \$oldfh;",
			    "sub i\::logit { ",
			    "\$main::xreport_dbgsock = new IO\::Socket\::INET(Proto=>'udp',PeerAddr=>DBGDEST, Reuse => 1) unless \$main\::xreport_dbgsock;",  
			    "\$main\::xreport_dbgsock->send(localtime() .\" - \$main\::servername \>\" .join('', \@_), 0, DBGDEST) if \$main\::xreport_dbgsock;",
			    "print \$main::logfh ''.localtime(), \"(\$\$): \", \@_, \"\\n\"; print \@_, \"\\n\";} ",
			    " 1;",
			   );

$main::preCodefh->close();

require $main::preCodefn;

#print Dumper($XReport::cfg);
my $sh_obj = Win32::OLE->new("WScript.Shell");
my $env_obj = $sh_obj->Environment("Process");
$env_obj->{'XREPORT_HOME'} = $main::spath."\\..\\";
$env_obj->{'XREPORT_SITECONF'} = $main::spath."\\..\\";

my $SLEEP_TIMEOUT = 1;
my $SERVICE_BITS = USER_SERVICE_BITS_8;

Win32::Daemon::StartService() || error_exit "Service not Started - $! - $? - $@\n";
i::logit("xrDaemon Service Started main loop HOME: $main::workdir XRHOST: $main::xrhost");
Win32::Daemon::SetServiceBits( $SERVICE_BITS ) || error_exit "xrDaemon Service not set bits\n";
i::logit("xrDaemon Service Service bits set to ", unpack("H*", $SERVICE_BITS));
doEventLog("10001", "xrDaemon server started name: ", $main::servername);

$main::cfgmtime = 0;

refreshConfig() || error_exit "Unable to get config for ", $main::servername, " from ", $main::xrhost;

#error_exit Dumper($cfgresp);
#(my $srvcmd = "perl -e \"use XML::Simple; use IO::File; my \$xmlfh = IO::File->new('$xmlfn');\$XReport::cfg = XMLin(\$xmlfh); undef \$xmlfh;do '$XReport::cfg->{srvcodefn}';\""); 
#my $srvcmd = "perl -e "
#  . "\"use XML::Simple;\$XReport::cfg = XMLin('$XReport::cfg->{cfgdatafn}', ForceArray => [qw(daemon dbase ReqredCode)]);" #$xmlfn');"
#  . "require '$main::preCodefn';"
#  . "do '$XReport::cfg->{ServerCode}';\" -- -N $main::servername"; 
#print "CMD: $srvcmd\n";
my $ProcessObj = 0;

$main::refreshtime = time();
#if ( $main::testrun ) {
#  i::logit("performing a test run");
# START_LOOP: while ( 1 ) {
#    Win32::Process::Create( $ProcessObj, "$^X", $XReport::cfg->{launchcmd}, 1, Win32::Process->NORMAL_PRIORITY_CLASS, "." )
#	|| error_exit "ERROR Launching $XReport::cfg->{launchcmd} - $!";
#    i::logit("Started Server Daemon for $main::servername pid: " .$ProcessObj->GetProcessID());
#    while ( !$ProcessObj->Wait(0) ) { 
#      my $currmtime = $main::cfgmtime;
#      my $refresh_interval = time() - $main::refreshtime;
#      if ( $refresh_interval > $XReport::cfg->{daemon}->{RefreshInterval} ) {
#	#         i::logit("Refreshing config");  
# 	$main::refreshtime = time();
# 	next START_LOOP unless refreshConfig();
#      }
#      if ( $currmtime != $main::cfgmtime ) {
# 	$ProcessObj->Kill(9999);
# 	$ProcessObj->Wait(5000);
# 	next START_LOOP;
#      }  
#      sleep( $SLEEP_TIMEOUT );
#    }
#  }
#}

#use XReport;
#use XReport::Util;
#use XReport::SUtil;

#my ($SrvName, $debug, $basedir) = getConfValues(qw(SrvName debugLevel basedir));
my ($SrvName, $debug, $basedir) = ($main::servername, $main::debug, $main::workdir);

my $cfg = $XReport::cfg->{'daemon'}->{$main::servername};
$main::debug = 1 if $cfg->{debug};
#my $pgmName = $cfg->{'process'} || error_exit "Program Name to start missing";
#my $procAff = $cfg->{$SrvName}->{'procaff'} if ($cfg->{'procaff'});
#$pgmName = "$basedir/bin/$pgmName";

my $State;
 
Win32::Daemon::Timeout( 5 );

#if (!Win32::Daemon::ShowService()) { 
#  i::logit ("xrDaemon ShowService Failed");
#  error_exit "xrDaemon ShowService Failed\n";
#}
$State = Win32::Daemon::QueryLastMessage();
i::logit ("last Message $State");

i::logit("xrDaemon Service Loop main loop");
while ( SERVICE_START_PENDING != Win32::Daemon::State() ) {
  i::logit("xrDaemon Start pending state detected sleeping for $SLEEP_TIMEOUT seconds");
  sleep( $SLEEP_TIMEOUT);
  last if $main::testrun;
}
	
doEventLog("10001", "$main::servername entering control messages loop - current state: ", $State);
i::logit("performing a test run interval: $XReport::cfg->{daemon}->{$main::servername}->{RefreshInterval}") if $main::testrun;
while ( 1 ) {
  my $currmtime = $main::cfgmtime;
  my $refresh_interval = time() - $main::refreshtime;
  if ( $refresh_interval > $XReport::cfg->{daemon}->{$main::servername}->{RefreshInterval} ) {
    i::logit("Refreshing config interval: $refresh_interval");  
    $main::refreshtime = time();
    error_exit "Service $main::servername configuration refresh failed - aborting" unless refreshConfig();
#    Win32::Daemon::State( SERVICE_STOP_PENDING, 30 ) unless refreshConfig();
  }
  if ( $currmtime != $main::cfgmtime && $ProcessObj && !$ProcessObj->Wait(0)) {
    $ProcessObj->Kill(0);
    sleep( $SLEEP_TIMEOUT );
  }

#  error_exit "Service $main::servername configuration changed - aborting" if ( $currmtime != $main::cfgmtime );
#  Win32::Daemon::State( SERVICE_STOP_PENDING, 30 ) if ( $currmtime != $main::cfgmtime );

  if (!$ProcessObj || $ProcessObj->Wait(0) ) {
    i::logit( "xrDaemon Starting SERVER for $SrvName" );
    Win32::Process::Create( $ProcessObj, "$^X", $XReport::cfg->{launchcmd}, 1, Win32::Process->NORMAL_PRIORITY_CLASS, "." )
	|| error_exit "ERROR Launching $XReport::cfg->{launchcmd} - $!";
    i::logit("Started Server Daemon for $main::servername pid: " .$ProcessObj->GetProcessID()) if $ProcessObj && !$ProcessObj->Wait(0);
  }

  if ( $main::testrun ) {
#    i::logit("performing a test run interval: $refresh_interval");
    sleep( $SLEEP_TIMEOUT );
    next;
  }

  $State = Win32::Daemon::State();

  if ( SERVICE_START_PENDING == $State  ) {
    unless ( !$ProcessObj || $ProcessObj->Wait(0) ) {
      i::logit("xrDaemon Service pending. Setting state to Running." );
      Win32::Daemon::State( SERVICE_RUNNING );
    }
  } 
  elsif ( SERVICE_PAUSE_PENDING == $State ) {
    i::logit( "xrDaemon Pausing $SrvName..." );
    unless ( !$ProcessObj || $ProcessObj->Wait(0) ) {
      $ProcessObj->Suspend();
    }
    Win32::Daemon::State( SERVICE_PAUSED );
    # next;
  } 
  elsif ( SERVICE_CONTINUE_PENDING == $State ) {
    i::logit( "xrDaemon resuming $SrvName... " );
    unless ( !$ProcessObj || $ProcessObj->Wait(0) ) {
      $ProcessObj->Resume() ;
      Win32::Daemon::State( SERVICE_RUNNING );
    }
    #next;
  } 
  elsif ( SERVICE_CONTROL_SHUTDOWN == $State ) {
    i::logit( "xrDaemon Shutting Down $SrvName... " );
    Win32::Daemon::State( SERVICE_STOP_PENDING, 30 );
    #next;
  } 
  elsif ( SERVICE_STOP_PENDING == $State ) {
    i::logit( "xrDaemon Stopping $SrvName..." );
    $ProcessObj->Kill(0) unless ( !$ProcessObj || $ProcessObj->Wait(0) );
    last;
  }
  elsif ( SERVICE_CONTROL_INTERROGATE == $State ) {
    Win32::Daemon::State( SERVICE_RUNNING ) unless ( !$ProcessObj || $ProcessObj->Wait(0) );
  } 
  else {
    i::logit( "xrDaemon Unknown state $State for $SrvName... " );
    Win32::Daemon::State( SERVICE_RUNNING ) unless ( !$ProcessObj || $ProcessObj->Wait(0) );
  }
  sleep( $SLEEP_TIMEOUT );
}

Win32::Daemon::State( SERVICE_STOPPED );
sleep( $SLEEP_TIMEOUT );
Win32::Daemon::StopService();

exit 0;

__END__

