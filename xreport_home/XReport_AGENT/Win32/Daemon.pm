#//////////////////////////////////////////////////////////////////////////////
#//
#//  Daemon.pm
#//  Win32::Daemon Perl extension package file
#//
#//  Copyright (c) 1998-2002 Dave Roth
#//  Courtesy of Roth Consulting
#//  http://www.roth.net/
#//
#//  This file may be copied or modified only under the terms of either 
#//  the Artistic License or the GNU General Public License, which may 
#//  be found in the Perl 5.0 source kit.
#//
#//  2002.06.06  :Date
#//  20020606    :Version
#//////////////////////////////////////////////////////////////////////////////

package Win32::Daemon;

$PACKAGE = $Package = "Win32::Daemon";

$VERSION = 20020606;
require Exporter;
require DynaLoader;

my @OSVerInfo = Win32::GetOSVersion();
my $OSVersion = "$OSVerInfo[1].$OSVerInfo[2]";
my $RECOGNIZED_CONTROLS;

@ISA= qw( Exporter DynaLoader );
    # Items to export into callers namespace by default. Note: do not export
    # names by default without a very good reason. Use EXPORT_OK instead.
    # Do not simply export all your public functions/methods/constants.
@EXPORT = qw(

    SERVICE_CONTROL_USER_DEFINED
    SERVICE_NOT_READY
    SERVICE_STOPPED
    SERVICE_RUNNING
    SERVICE_PAUSED
    SERVICE_START_PENDING
    SERVICE_STOP_PENDING
    SERVICE_CONTINUE_PENDING
    SERVICE_PAUSE_PENDING

    SERVICE_CONTROL_NONE
    SERVICE_CONTROL_STOP
    SERVICE_CONTROL_PAUSE
    SERVICE_CONTROL_CONTINUE
    SERVICE_CONTROL_INTERROGATE
    SERVICE_CONTROL_SHUTDOWN
    SERVICE_CONTROL_PARAMCHANGE
    SERVICE_CONTROL_NETBINDADD
    SERVICE_CONTROL_NETBINDREMOVE
    SERVICE_CONTROL_NETBINDENABLE
    SERVICE_CONTROL_NETBINDDISABLE

    SERVICE_ACCEPT_HARDWAREPROFILECHANGE
    SERVICE_ACCEPT_POWEREVENT
    SERVICE_ACCEPT_SESSIONCHANGE

    USER_SERVICE_BITS_1
    USER_SERVICE_BITS_2
    USER_SERVICE_BITS_3
    USER_SERVICE_BITS_4
    USER_SERVICE_BITS_5
    USER_SERVICE_BITS_6
    USER_SERVICE_BITS_7
    USER_SERVICE_BITS_8
    USER_SERVICE_BITS_9
    USER_SERVICE_BITS_10

    SERVICE_ACCEPT_STOP
    SERVICE_ACCEPT_PAUSE_CONTINUE
    SERVICE_ACCEPT_SHUTDOWN    
    SERVICE_ACCEPT_PARAMCHANGE  
    SERVICE_ACCEPT_NETBINDCHANGE

    SERVICE_WIN32_OWN_PROCESS
    SERVICE_WIN32_SHARE_PROCESS
    SERVICE_KERNEL_DRIVER
    SERVICE_FILE_SYSTEM_DRIVER
    SERVICE_INTERACTIVE_PROCESS

    SERVICE_BOOT_START
    SERVICE_SYSTEM_START
    SERVICE_AUTO_START
    SERVICE_DEMAND_START
    SERVICE_DISABLED

    SERVICE_DISABLED
    SERVICE_ERROR_NORMAL
    SERVICE_ERROR_SEVERE
    SERVICE_ERROR_CRITICAL

    SC_GROUP_IDENTIFIER

    NO_ERROR
);


@EXPORT_OK = qw(
);      

sub new{
    my( $type, @Options ) = @_;
    my $self = bless {};

    $Result = Win32::Perms::New( $self, @Options );
    if( ! $Result )
    {
        undef %self;
        return undef;
    }
    return( $self );
}

bootstrap $Package;

sub AUTOLOAD 
{
    # This AUTOLOAD is used to 'autoload' constants from the constant()
    # XS function.  If a constant is not found then control is passed
    # to the AUTOLOAD in AutoLoader.

    my( $Constant ) = $AUTOLOAD;
    my( $Result, $Value );
    $Constant =~ s/.*:://;

    $Result = Constant( $Constant, $Value );

    if( 0 == $Result )
    {
        # The extension could not resolve the constant...
        $AutoLoader::AUTOLOAD = $AUTOLOAD;
            goto &AutoLoader::AUTOLOAD;
        return;
    }
    elsif( 1 == $Result )
    {
        # $Result == 1 if the constant is valid but not defined
        # that is, the extension knows that the constant exists but for
        # some wild reason it was not compiled with it.
        $pack = 0; 
        ($pack,$file,$line) = caller;
        print "Your vendor has not defined $Package macro $constname, used in $file at line $line.";
    }
    elsif( 2 == $Result )
    {
        # If $Result == 2 then we have a string value
        $Value = "'$Value'";
    }
        # If $Result == 3 then we have a numeric value

    eval "sub $AUTOLOAD { return( $Value ); }";
    goto &$AUTOLOAD;
}

# Set the default accepted controls...
$RECOGNIZED_CONTROLS = &SERVICE_ACCEPT_STOP
                       | &SERVICE_ACCEPT_PAUSE_CONTINUE
                       | &SERVICE_ACCEPT_SHUTDOWN;

# If we are talking about Windows 5.1 (Windows XP/.NET Server) or higher
# then add the addtional controls...
if( 5.1 <= $OSVersion )
{
    $RECOGNIZED_CONTROLS |= &SERVICE_ACCEPT_PARAMCHANGE | &SERVICE_ACCEPT_NETBINDCHANGE;
}
AcceptedControls( $RECOGNIZED_CONTROLS );

# For a module, you *always* return TRUE...
return( 1 );


END
{
    Win32::Daemon::StopService();
}

__END__

=head1 NAME

Win32::Daemon - Extension enabling Win32 Perl scripts to run as a true Win32 service.

=head1 SYNOPSIS

        use Win32::Daemon;
    Win32::Daemon::StartService();
        I<...process Perl code...>
    Win32::Daemon::StopService();

=head1 DESCRIPTION

This extension enables a Win32 Perl script to act as a true Win32 service.

=head1 FUNCTIONS

    AcceptedControls()
    CreateService()
    ConfigureService()
    QueryServiceConfig()
    DeleteService() 
    GetLastError()
    GetServiceHandle()
    HideService()
    QueryLastMessage()
    RestoreService()
    SetServiceBits()
    ShowService()
    StartService()
    State()
    StopService()   
    Timeout()
    
=over 4

=item AcceptedControls( [$NewControls] )

This function queries (and optionally sets) the current list of controls that the service registers for.
By registering for a control the script is notifying the SCM that it is accepting the specified
control messages. For example, if you specify the SERVICE_ACCEPT_PAUSE_CONTINUE control then
the SCM knows that the script will accept and process any attempt to pause and continue (resume 
from paused state) the service.

Recognized accepted controls:

    SERVICE_ACCEPT_STOP...............The service accepts messages to stop.
    SERVICE_ACCEPT_PAUSE_CONTINUE.....The service accepts messages to pause and continue.
	SERVICE_ACCEPT_SHUTDOWN...........The service accepts messages to shutdown the system.
                                      When the OS is shutting down the service will be notified
                                      when it has accepted this control.

Following controls are only recognized on Windows 2000 and higher:

    SERVICE_ACCEPT_PARAMCHANGE........The service accepts messages notifying it of any 
                                      parameter change made to the service.
    SERVICE_ACCEPT_NETBINDCHANGE......The service accepts messages notifying it of any 
                                      network binding changes.

By default all of these controls are accepted. To change this pass in a value consisting of
any of these values OR'ed together. 

B<NOTE> that you can query and set these controls at any time. However it is only supported to
set them before you start the service (calling the C<StartService()> function).


=item StartService()

This starts a new service thread. The script should call this as soon as possible.  When
the service manager starts the service Perl is started and the script is loaded.

=item StopService()

This will instruct the service to terminate.

=item QueryLastMessage( [$fResetMessage] )

This function returns the last message that the service manager has sent to the service.

Pass in a non zero value to reset the pending message to SERVICE_CONTROL_NONE. This way
your script can tell when two of the same messages come in.

Occasionally the service manager will send messages to the service. These messages 
typically request the service to change from one state to another.  It is important that
the Perl script responds to each message otherwise the service manager becomes confused
about the current state of the service. For example, if the service manager is submits
a C<SERVICE_PAUSE_PENDING> then it expects the Perl script to recognize the change to a paused
state and submit the new state by calling C<State( SERVICE_PAUSED )>.

You can update the service manager with the current status using the C<State()> function.

Possible values returned are:

    Valid Service Control Messages:
	-------------------------------
	SERVICE_CONTROL_NONE..............No message is pending.
	SERVICE_CONTROL_STOP..............The SCM is requesting the service to stop.
                                      This results in State() reporting SERVICE_STOP_PENDING.
	SERVICE_CONTROL_PAUSE.............The SCM is requesting the service to pause.
                                      This results in State() reporting SERVICE_PAUSE_PENDING.
	SERVICE_CONTROL_CONTINUE..........The SCM is requesting the service to continue from a 
                                      paused state.
                                      This results in State() reporting SERVICE_CONTINUE_PENDING.
    SERVICE_CONTROL_INTERROGATE.......The service manager is querying the service's state

    Windows 2000 specific messages:
    SERVICE_CONTROL_SHUTDOWN..........The machine is shutting down. This indicates that
                                      the service has roughly 20 seconds to clean up
                                      and terminate. This time can be extended by
                                      submitting SERVICE_STOP_PENDING via the State() function.
    SERVICE_CONTROL_PARAMCHANGE.......Service parameters have been modified.
    SERVICE_CONTROL_NETBINDADD........A network binding as been added.
    SERVICE_CONTROL_NETBINDREMOVE.....A network binding has been removed.
    SERVICE_CONTROL_NETBINDENABLE.....A network binding has been enabled.
    SERVICE_CONTROL_NETBINDDISABLE....A network binding has been disabled.

    SERVICE_CONTROL_USER_DEFINED......This is a user defined control. There are 127 of these
                                      beginning with SERVICE_CONTROL_USER_DEFINED as the base.

B<Note:> When the system shuts down it will send a C<SERVICE_CONTROL_SHUTDOWN> message. The
Perl script has approximately 20 seconds to perform any shutdown activities before the
Control Manger stops the service. If more time is needed call the C<State()> function
passing in the C<SERVICE_STOP_PENDING> control message along with how many seconds it will 
take to shutdown the service. This time value is only an estimate. When the service is 
finally ready to stop it must submit the C<SERVICE_STOPPED> message as in:

    if( SERVICE_CONTROL_SHUTDOWN == State() )
    {
        Win32::Daemon::State( SERVICE_STOP_PENDING, 30 );
        I<...process code...>
        Win32::Daemon::State( SERVICE_STOPPED );
    }


=item State([$NewState [, $Hint ] || \%Hash ] )

This function returns the current state of the service.  It can optionally update the
status of the service as well.  This is the last status reported to the service manager.

Optionally you can pass in a value that will be sent to the service manager.
Optionally you can pass in a numeric value indicating the "hint". This is the number of
milliseconds the SCM can expect to wait before the service responds to the request. For example,
if your service script reports a hint of 30,000 milliseconds means that the SCM will have to wait
for 30 seconds for the script to change the service's state before deciding that the
script is non responsive.

If you are setting/updating the state instead of passing in the state and wait hint you could 
pass in a hash reference. This allows you to specify the state, wait hint and error state. You
can use the following keys:

    Hash Key
    --------
    state..........Valid service state (see table below).
    waithint.......A wait hint explained above. This is in milliseconds.
    error..........Any 32 bit error code. This is what will be reported if an application 
                   queries the error state of the service. It is also what is reported if
                   a call to start the services fails.
                   To reset an error state pass in NO_ERROR.
                   The only invalid error value is 0xFFFFFFFF.

Example of passing in an error:

  Win32::Daemon::State( { error => 0x12345678 } );
  # Later to reset the error:
  Win32::Daemon::State( { error => NO_ERROR } );


Possible values returned (or submitted):

	Valid Service States:
	---------------------
    SERVICE_NOT_READY..........The SCM has not yet been initialized. If the SCM is slow or busy
                               then this value will result from a call to State().
                               If you get this value, just keep calling State() until you get 
                               SERVICE_START_PENDING.
    SERVICE_STOPPED............The service is stopped
    SERVICE_RUNNING............The service is running
    SERVICE_PAUSED.............The service is paused
    SERVICE_START_PENDING......The service manager is attempting to start the service
    SERVICE_STOP_PENDING.......The service manager is attempting to stop the service
    SERVICE_CONTINUE_PENDING...The service manager is attempting to resume the service
    SERVICE_PAUSE_PENDING......The service manager is attempting to pause the service


=back

=head2 Example: Simple Service

This example service will delete all .tmp files from the c:\temp directory every
time it starts.  It will immediately terminate.

    use Win32::Daemon;

    # Tell the OS to start processing the service...
    Win32::Daemon::StartService();

    # Wait until the service manager is ready for us to continue...
    while( SERVICE_START_PENDING != Win32::Daemon::State() )
    {
        sleep( 1 );
    }

    # Now let the service manager know that we are running...
    Win32::Daemon::State( SERVICE_RUNNING );

    # Okay, go ahead and process stuff...
    unlink( glob( "c:\\temp\\*.tmp" ) );

    # Tell the OS that the service is terminating...
    Win32::Daemon::StopService();

This particular example does not really illustrate the capabilities of a Perl based service.

=head2 Example: Typical skeleton code

  use Win32;
  use Win32::Daemon;
  $SERVICE_SLEEP_TIME = 20; # 20 milliseconds
  $PrevState = SERVICE_START_PENDING;
  while( SERVICE_STOPPED != ( $State = Win32::Daemon::State() ) )
  {
    if( SERVICE_START_PENDING == $State )
    {
        # Initialization code
        Win32::Daemon::State( SERVICE_RUNNING );
        $PrevState = SERVICE_RUNNING;
    }
    elseif( SERVICE_STOP_PENDING == $State )
    {
      Win32::Daemon::State( SERVICE_STOPPED );
    }
    elsif( SERVICE_PAUSE_PENDING == $State )
    {
      # "Pausing...";
      Win32::Daemon::State( SERVICE_PAUSED );
      $PrevState = SERVICE_PAUSED;
      next;
    }
    elsif( SERVICE_CONTINUE_PENDING == $State )
    {
      # "Resuming...";
      Win32::Daemon::State( SERVICE_RUNNING );
      $PrevState = SERVICE_RUNNING;
      next;
    }
    elsif( SERVICE_STOP_PENDING == $State )
    {
      # "Stopping...";
      Win32::Daemon::State( SERVICE_STOPPED );
      $PrevState = SERVICE_STOPPED;
      next;
    }
    elsif( SERVICE_RUNNING == $State )
    {
      # The service is running as normal...
      # ...add the main code here...
	  
    }
    else
    {
      # Got an unhandled control message. Set the state to
      # whatever the previous state was.
      Win32::Daemon::State( $PrevState );
    }

	# Check for any outstanding commands. Pass in a non zero value
	# and it resets the Last Message to SERVICE_CONTROL_NONE.
	if( SERVICE_CONTROL_NONE != ( my $Message = Win32::Daemon::QueryLastMessage( 1 ) ) )
	{
          if( SERVICE_CONTROL_INTERROGATE == $Message )
          {
            # Got here if the Service Control Manager is requesting
            # the current state of the service. This can happen for
            # a variety of reasons. Report the last state we set.
            Win32::Daemon::State( $PrevState );
          }
        elsif( SERVICE_CONTROL_SHUTDOWN == $Message )
        {
          # Yikes! The system is shutting down. We had better clean up
          # and stop.
          # Tell the SCM that we are preparing to shutdown and that we expect
          # it to take 25 seconds (so don't terminate us for at least 25 seconds)...
          Win32::Daemon::State( SERVICE_STOP_PENDING, 25000 );
        }
      }
      # Snooze for awhile so we don't suck up cpu time...
      Win32::Sleep( $SERVICE_SLEEP_TIME );
   }
   # We are done so close down...
   Win32::Daemon::StopService();


=head2 Example: Install the service

For the 'path' key the $^X equates to the full path of the 
perl executable.
Since no user is specified it defaults to the LocalSystem.

    use Win32::Daemon; 
    %Hash = (
        machine =>  '',
        name    =>  'PerlTest',
        display =>  'Oh my GOD, Perl is a service!',
        path    =>  $^X,
        user    =>  '',
        pwd     =>  '',
        description => 'Some text description of this service',
        parameters =>'c:\perl\scripts\service.pl -param1 -param2 "c:\Param2Path"',
    );
    if( Win32::Daemon::CreateService( \%Hash ) )
    {
        print "Successfully added.\n";
    }
    else
    {
        print "Failed to add service: " . Win32::FormatMessage( Win32::Daemon::GetLastError() ) . "\n";
    }



NOTES:
    - ConfigureService:
        - If you specify a 'parameters' key you MUST specify a 'path' key.

HISTORY:
	- 20011205 rothd
		-Fixed bug where "invalid service state 80" is reported in the Win32 event log.
		-Added initial support for SCM request callbacks (this is not implemented fully and
		 is not supported yet).
		-Added SERVICE_CONTROL_NONE constant
		-Fixed State() so it only returns states, not commands.
		-Modified QueryLastMessage(). It now allows you to reset the last message to SERVICE_CONTROL_NONE.
        -Added SERVICE_NOT_READY state
        -Cleaned up some states.
        -Added AcceptedControls() function.

    - 20011217 rothd
        - Added support for .Net Server accept control contants:
            SERVICE_ACCEPT_HARDWAREPROFILECHANGE
            SERVICE_ACCEPT_POWEREVENT
            SERVICE_ACCEPT_SESSIONCHANGE

    - 20011221 rothd@roth.net
        - Fixed bug where service doesn't work properly with Windows NT 4. We were 
          defaulting by acccepting the SERVICE_ACCEPT_PARAMCHANGE and 
          SERVICE_ACCEPT_NETBINDCHANGE controls. However, they were introduced in 
          Win2k so NT 4 coughed up blood with them.
    
    - 20020108 rothd@roth.net
        - Fixed another bug wwhere servie wouldn't work on Win2k machines. We
          were treating Win2k (version 5.0) the same as WinXP (version 5.1) and
          adding WinXP specific controls. This rendered the service unable to
          start, pause or stop. Fix was only in the Daemon.pm file.

    - 20020114 marc.pijnappels@nec-computers.com
        - Fixed another bug where service wouldn't work on WinXP machines. Variable 
          recognized_controls was wrongly initialized for WinXP systems. This 
          rendered the service unable to start, pause or stop. Fix was only in 
          the Daemon.pm file.

    - 20020605 rothd@roth.net
        - Added support for reporting service errors. You can now pass in a
          hash reference into State(). More details in the POD docs.
          
    
