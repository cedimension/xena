
package XReport::ObjCache::Manager;

use strict;

my $objcache = {}; my $objmarks = {}; my $lcacheid = 0;

sub mark {
  shift; my ($cacheid, $obj, $tier) = @_; $objmarks->{"$obj"} = [$cacheid, $tier];
}

sub deletemark {
}

sub get {
  shift; my $cacheid = shift; my $oc = $objcache->{$cacheid}; (shift @$oc, shift @$oc);
}

sub dispose {
  shift; my $obj = shift; my ($cacheid, $tier) = @{$objmarks->{"$obj"}};
  
  push @{$objcache->{$cacheid}}, $obj, $tier; 
}

sub getid {
  shift; $lcacheid += 1; $objcache->{$lcacheid} = []; $lcacheid;
}

sub deleteid {
}

$c::cm = bless {}, __PACKAGE__;

1;
