
=h2. CTD Print Mission Emulator

This module tries to provide the same output that CTD customers are used to have. 
Since that is not ment to be our standard, this is subject to move to siteconf/userlib/BUNDLESBIN 
and provide a standard module that produces a printer stream suitable for network   

=cut

sub queryFoldersLogicalReports {
    my ($self, $info) = (shift, shift);
    die "NO Valid DB HANDLE exists " unless exists($self->{dbhandle}) 
                                         && ref($self->{dbhandle}) eq 'XReport::DBUtil';
    my $query = $self->substituteVars($info, $XReport::cfg->{sqlprocs}->{FoldersLogicalReports}, @_ );
    $query .= ' WHERE '.$self->{UserSpec} if $self->{UserSpec} && $self->{UserSpec} !~ /^\s*$/;
#    warn "FoldersLOgicalReports: ", $query, "\n" unless $main::logged;
#        $main::logged = 1;
    return $self->{dbhandle}->dbExecute_NORETRY($query);
    
}

sub queryBundlesLogicalReports {
    my ($self, $info) = (shift, shift);
    die "NO Valid DB HANDLE exists " unless exists($self->{dbhandle}) 
                                         && ref($self->{dbhandle}) eq 'XReport::DBUtil';
#    warn "query info: ", Dumper($info), "\n";
#    $self->{UserSpec} = ' AND '.$self->{UserSpec} if $self->{UserSpec} && $self->{UserSpec} !~ /^\s*$/;
    my $query = $self->substituteVars($info, $XReport::cfg->{sqlprocs}->{BundlesLogicalReports}, @_ );
    if ( exists($XReport::cfg->{sqlprocs}->{BundlesLR2}) ) {
        $main::verbose && i::warnit("BundlesLogicalReports Part1: ", $query);
        i::warnit("BundlesLogicalReports Part1: ", $query);#TESTSAN
         $self->{dbhandle}->dbExecute_NORETRY($query);
        $query = $self->substituteVars($info, $XReport::cfg->{sqlprocs}->{BundlesLR2}, @_ );
    }
    $query .= ' WHERE '.$self->{UserSpec} if $self->{UserSpec} && $self->{UserSpec} !~ /^\s*$/;
    $query .= ' ORDER BY JobReportId, ReportId ';
    $main::verbose && i::warnit("BundlesLogicalReports: ", $query);
    i::warnit("BundlesLogicalReports: ", $query);#TESTSAN
    return  $self->{dbhandle}->dbExecute_NORETRY($query);

}

use XReport::ARCHIVE::INPUT;

sub initIOFiles {
  my $self = shift;
  my ($info, $JRID, $lrkey) = (shift, shift, shift);
  $JRID = $info->{JobReportId} unless $JRID;
#  my $lrkey = join('.', @{$info}{qw(JobReportId ReportId)});
  my $rfilename = $self->{workdir}."\\REP.$lrkey.bin";
#  warn "initIOFiles extract finfo ($JRID) for $rfilename: ", Dumper($info), "\n";
  $main::veryverbose && i::warnit("initIOFiles extract finfo ($JRID) for $rfilename: ");
  
  return undef if exists($info->{rfilename}) && -e $rfilename;
  if ( !exists( $self->{currjr} ) || $self->{currjr}->{JobReportId} != $JRID ) { 
    $self->{currjr}->Close() if exists( $self->{currjr} );
    $self->{currjr} = XReport::ARCHIVE::INPUT->Open($JRID, wrapper => 1, random_access => 1);
  }
  my $jr = $self->{currjr};
  return undef unless $jr;
  
  my $streamer = new IO::File ">$rfilename";
  if ( !$streamer ) {
    @{$rinfo}{qw(InputPages Lines)} = (0, 0);
    $jr->Close();
    warn "Unable to open \"$rfilename\" - $!\n";
    return undef;
  }
  @{$info}{qw(extract streamer rfilename lrkey)} = ($jr, $streamer, $rfilename, $lrkey);
  return $info;
}

use Encode;

sub xlate1047 {
#   warn "xlate called by ", join('::', (caller(1))[0,2]), " Case: $_[1] STR: $_[0] "
#   , (scalar(@_) > 2 ? 'P: '.join('::', $_[2,3,4]) : '')
#   , " HEX: ", unpack('H*', $_[0])
#   , "\n";
    return encode('CP1047', $_[0]); 
}


sub initBundleOutput {
    my ($self, $binfo) = (shift, shift);

    $self->outputSetup($binfo);

#todo prevedere code pages differenti (magari da config)
    $self->{bannerxlator} = \&xlate1047;
    $binfo->{encoding} = 'E';
#    $binfo->{outmode} = 'R';
    $binfo->{outmode} = 'B';

    $binfo->{outfile} = new IO::File(">$binfo->{outfilename}") 
                              || die "Unable to open \"$binfo->{outfilename}\" - $!";
    $binfo->{outfile}->binmode();
    return $binfo->{outfile};
}

sub writeBuff {
    my ($self, $info, $buffer) = (shift, shift, shift);
    $main::veryverbose && i::warnit("Writing now ", length($buffer), " bytes to $info->{outfilename} as requested by ", join('::', (caller())[0,2]));
    substr($buffer, 2, 1) = "\xF1" if (!exists($info->{bufferinit}) && $buffer !~ /^..\xF1/);
    $info->{bufferinit} = 1;
    return $info->{outfile}->write($buffer);
}

sub addReport2Bundle {
  my $self = shift;
  my $infile = shift;
  my ($jinfo, $streamer, $binfo) = (shift,shift,shift);
  my $inhndr = IO::File->new("<$infile") || die "Unable to open \"$infile\" - $!";
  $inhndr->binmode(); 
  i::logit("Now adding \"$infile\" to OUTFILE for $jinfo->{RecipientName} at ".$streamer->tell());
  my $leftovers = '';
  while ( !$inhndr->eof() ) { 
    $inhndr->read(my $buff, 32760);
    $self->writeBuff($binfo, $buff ) if length($buff);
  }
  return $inhndr->close();
}

sub buildReportIndex {
   my ($self, $info, $hdr) = (shift, shift, shift);
#   warn "bri called by ", (caller())[0,2], " INFO\n", Dumper($info), "\n";
   my $lines = [];
   push @$lines, ( ref($hdr) eq 'ARRAY' ? @$hdr : split("\n", $XReport::cfg->{ReportIndex}->{header}) ) if $hdr;
   push @$lines, map { (my $l = $_) =~ s/\&lt/\</g; $l =~ s/\&gt/\>/g; $l  } split /\n/, 
                       form( $XReport::cfg->{ReportIndex}->{form}->{template},
                            ,{ cols => [split(' ', $XReport::cfg->{ReportIndex}->{form}->{cols})] 
                               ,from => [ @$info ]
                             } ) if defined($info);
  return @$lines;
} 

sub createBanner {
#i::warnit("TESTSAN - sub createBanner(".ref($_[0]).",".$_[1].",".ref($_[2]).")(line:".__LINE__);
   my ($self, $bannerType, $info) = (shift, shift, shift);
   my $bannerCfg = $XReport::cfg->{$bannerType};
   $main::veryverbose && i::warnit("banner $bannerType: \n"
#        , "banner cfg: ", Dumper($bannerCfg));
#        , "Banner infos: ", Dumper($info), "\n"
        );
    
    my $head =
        [map { @{$self->formatBannerRec($_, $info)} } split /\n/, $bannerCfg->{head} ]; 
#    die "---------";
    push @$head, map { @{$self->formatBannerRec($_, $info)} } 
         ( $self->buildReportIndex(delete $info->{IndexArray}, 1 )) if exists($info->{IndexArray});
         
    my $tail =
        [map { @{$self->formatBannerRec($_, $info)} } split /\n/, $bannerCfg->{tail} ]; 
	#i::warnit("TESTSAN - createBanner(line:".__LINE__.")ref(\$tail):".ref($tail));
	#i::warnit("TESTSAN - bannerTail dentro create--$bannerType\n", Dumper($tail));
    $info->{BannerPages} = scalar(grep /^\xF1/, (@{$head},@{$tail})); 
    $info->{BannerLines} = scalar(@{$head})+scalar(@{$tail});
    $info->{$bannerType.'Head'} = $head;
    $info->{$bannerType.'Tail'} = $tail;
#    die "Formatted Banner: ", Dumper(\{HEAD => $head, TAIL => $tail}), "\n";
    return $info;
}

1;
