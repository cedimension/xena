sub queryBundleFolders {
    my ($self, $info) = (shift, shift);
    my $varset = ($_[0] =~ /^include/i ? ':BundleIncludePattern' : ':BundleExcludePattern');
    my $query = $self->substituteVars($info, <<'EOSQL', VarSet => $varset, BundleName => $self->{bundle} );
SELECT distinct case when charindex(CHAR(92),EFR.[FolderName]) > 0 then right(EFR.[FolderName],charindex(CHAR(92),reverse(EFR.[FolderName]))-(1)) else EFR.FolderName end 
   as Recipient
 ,EBF.BundleName
 ,EBF.BundleSkel
 ,EBF.BundleDest
 ,EFR.FolderName
FROM tbl_FoldersRules EFR
INNER JOIN (
  SELECT EBN.BundleName
     ,EBN.BundleSkel
     ,EBN.BundleDest
     ,EFN.FolderName as FolderPath
  FROM tbl_BundlesNames EBN 
  INNER JOIN tbl_testVSV EVS on EVS.VarSetName = EBN.BundleName and EVS.VarName = '#{VarSet}#'
  INNER JOIN tbl_Folders EFN on
       case when charindex(CHAR(92),EFN.[FolderName]) > 0 then right(EFN.[FolderName],charindex(CHAR(92),reverse(EFN.[FolderName]))-(1)) else EFN.FolderName end like EVS.VarValue 
  WHERE EBN.BundleName like '#{BundleName}#%'
) EBF on (EFR.FolderName = EBF.FolderPath or EFR.FolderName like EBF.FolderPath + CHAR(92) + '%')
INNER JOIN tbl_testNRG NRG on EFR.ReportGroupId = NRG.ReportGroupId
EOSQL
    return XReport::DBUtil::dbExecute_NORETRY($query);
}

sub queryFoldersLogicalReports {
    my ($self, $info) = (shift, shift);
    my $query = $self->substituteVars($info, <<'EOSQL', @_ );
SELECT fn.FolderName     AS FolderName
      ,case when charindex(char(92),fn.[FolderName]) > 0 then right(fn.[FolderName],charindex(char(92),reverse(fn.[FolderName]))-(1)) else fn.FolderName end 
                         AS RecipientName 
      ,LR0.ReportName    AS JobReportName
      ,LRR.FilterVar     AS FilterVar 
      ,LR0.FilterValue   AS FilterValue
      ,LR0.JobReportID   AS JobReportID
      ,LR0.ReportId      AS ReportID 
      ,JR0.JobName       AS JobName
      ,JR0.JobNumber     AS JobNumber
      ,JR0.XferStartTime AS XferStartTime
      ,JR0.UserTimeElab  AS UserTimeElab
      ,JR0.FileRangesVar AS FileRangesVar
      ,JR0.ExistTypeMap  AS ExistTypeMap
      ,case when LR0.ReportId > 1 then LR0.TotPages else 0 end 
                         AS TotPages 
      ,case when LR0.ReportId = 1 then LR0.TotPages else 0 end 
                         AS AllPages
      ,CONVERT(varchar(1024), PHR.ListOfPages) 
                         AS ListOfPages
      ,COALESCE(LRT.textString, JR0.UserRef, LR0.ReportName, JR0.JobReportName)  
                         AS UserRef
      ,COALESCE(LRR.UserTimeRef, JR0.UserTimeRef, JR0.UserTimeElab, JR0.XferStartTime) 
                         AS UserTimeRef
      ,nrg.FilterRule    AS FilterRule
      ,nrg.RecipientRule AS RecipientRule
      ,nrg.ReportRule    AS ReportRule
   FROM tbl_Folders fn
      INNER JOIN tbl_FoldersRules        fr  ON  fr.FolderName = fn.FolderName
      INNER JOIN tbl_testNRG             nrg ON  fr.ReportGroupId = nrg.ReportGroupId
      INNER JOIN tbl_testVSV             vr  ON  vr.VarSetName = nrg.ReportRule and vr.VarName = ':REPORTNAME' 
      INNER JOIN tbl_testVSV             vv  ON  vv.VarSetName = nrg.FilterRule and vv.VarName = nrg.FilterVar
      INNER JOIN tbl_JobReports          JR0 ON JR0.JobReportName = vr.VarValue
      INNER JOIN tbl_LogicalReports      LR0 ON LR0.JobReportId = JR0.JobReportId and LR0.XferRecipient = nrg.RecipientRule and LR0.FilterValue = vv.VarValue
      INNER JOIN tbl_LogicaLReportsRefs  LRR ON LRR.JobReportId = LR0.JobReportId and LRR.ReportId = LR0.ReportId and LRR.FilterVar = vv.VarName 
      INNER JOIN tbl_LogicalReportsTexts LRT ON LRT.textId = LRR.textId
      INNER JOIN tbl_PhysicalReports     PHR ON PHR.JobReportId = LR0.JobReportId and PHR.ReportId = LR0.ReportId
WHERE JR0.Status = 18 AND LR0.ReportId > 0 AND LRR.Printable = 1 AND
  fn.FolderName IN ('#{FolderName}#') 
EOSQL
        $query .= ($self->{UserSpec} ? ' AND '.$self->{UserSpec} : '') ;
#        warn "FoldersLOgicalReports: ", $query, "\n" unless $main::logged;
#        $main::logged = 1;
        return XReport::DBUtil::dbExecute_NORETRY($query);
	
}

sub extractReportPages {
  my $self = shift;
  my ($rinfo, $pagesptr) = (shift, shift);
  my $pagelist = [ @$pagesptr ];
  # my $finfo = $rinfo->{bundle};
  my $JRID = $pagelist->[0];
  
  (my $rfilename .= join('.', ('REP', @{$rinfo}{qw(JobReportID ReportID)}, 'bin'))) =~ s/[\\\/]/_/g;
  $rfilename = $self->{workdir}."\\".$rfilename;
#  warn "extract finfo for $rfilename: ", Dumper($rinfo), "\n";
  
  my ($totpages, $totlines) = (0, 0);
  unless ( -e $rfilename ) {
	  my $jr = XReport::ARCHIVE::INPUT->Open($JRID, wrapper => 1, random_access => 1);
	  my $streamer = new IO::File ">$rfilename";
	  if ( !$streamer ) {
        @{$rinfo}{qw(Pages Lines)} = (0, 0);
        $jr->Close();
	    warn "Unable to open \"$rfilename\" - $!\n";
	    return undef;
	  }
	  
	  while ( scalar(@$pagelist) ) {
	    my ($JobReportId, $FromPage, $ForPages) = splice @$pagelist, 0, 3;
	    my ($pages, $lines);
	    eval { ($pages, $lines) = $jr->extract_pages($FromPage, $ForPages, tostreamer => $streamer ) };
	    if ( my $errmsg = $@ ) {
            @{$rinfo}{qw(Pages Lines)} = (0, 0);
	        warn "Extract pages error for $JobReportId - $errmsg";
	        $streamer->close();
	        unlink $rfilename;
	        return undef;
	    }
#        warn "extract pages returned $pages pages $lines lines\n";
        $totpages += $pages;
        $totlines += $lines;
	  }
	  
	  $streamer->close();
  }
  $self->{logicalreports}->{$rfilename} = [$rfilename, $totpages, $totlines ] if !exists($self->{logicalreports}->{$rfilename});
  @{$rinfo}{qw(Pages Lines)} = @{$self->{logicalreports}->{$rfilename}}[1,2];
  return $self->{logicalreports}->{$rfilename}->[0];
  
}

sub outOpen {
	my ($self, $binfo) = (shift, shift);
	$self->{dspfx} = 'XRBUNDLE' unless $self->{dsnpfx};
	$self->{outname} = $self->{workdir}.'/'.$self->{dsnpfx}.'.'.$binfo->{BundleName}.'.bin';
	$self->{outfile} = new IO::File(">$self->{outname}") 
	                          || die "Unable to open \"$self->{outname}\" - $!";
#todo prevedere code pages differenti (magari da config)
	$self->{xlator} = new Convert::EBCDIC();
	return $self->{outfile};
}

sub writeBuff {
    my ($self, $rinfo, $buffer) = (shift, shift, shift);
    if (!exists($rinfo->{bufferinit}) && $buffer !~ /^..\xF1/) {
    	$rinfo->{bufferinit} = 1;
    	my ($fline, $btail) = unpack('n/a a*', $buffer);
    	my $newfline = pack('n/a*', "\xF1".substr($fline, 1));
        return $self->{outfile}->write($newfline.$btail);
    }
    else {
        $rinfo->{bufferinit} = 1;
        return $self->{outfile}->write($buffer);
    	
    }
}

use Net::FTP;

sub deliveryWrite {
	my $self  = shift;
	if (!exists($self->{ftpc}) || !defined($self->{ftpc}) ) {
		my $ftphost = $XReport::cfg->{ftp}->{host};
		$ftphost = $self->{ftphost} if exists($self->{ftphost}) && $self->{ftphost};
		
		print "Connecting to FTP HOST $ftphost to deliver \"$self->{outname}\"\n";
		$self->{ftpc} = Net::FTP->new($ftphost, Debug => 1) or die "Cannot connect to zOS: $@";
        $self->{ftpc}->login("xreport","euriskom") or die "Cannot login.", $self->{ftpc}->message;
        $self->{ftpc}->site("PRI=10","SEC=50",
           "TRacks",
           "RECfm=VB",
           "LRecl=32760",
           "BLocksize=0",
           ) or die "Cannot set SITE command. $!";

        $self->{ftpc}->quot("mode","b") or die "Cannot issue MODE 'b' command. $!";
        (my $remotename = $self->{outname}) =~ s/^.*?[\\\/]?([^\\\/]+)$/$1/;
        $remotename = "'$remotename'"; 
        $self->{ftpd} = $self->{ftpc}->stor($remotename) 
                               or die "Unable to acquire data handle for \"$remotename\" - $!";
	}
    $self->{ftpd}->write($self->{outbuff}, length $self->{outbuff})	
                               or die "Error during store \"'$self->{outname}'\" - $!";
    @{$self}{qw(outbuff outlen)} = ('', 0);
}

sub deliveryClose {
    my $self  = shift;
    return undef unless $self->{ftpc};
    $self->{ftpd}->write($self->{outbuff}, length $self->{outbuff}) 
                               or die "Error during store \"'$self->{outname}'\" - $!"
                               if length($self->{outbuff}) && $self->{ftpd};
    $self->{ftpd}->quit() or die "Can't close ftp connections: $!" if $self->{ftpd};
    $self->{ftpc}->quit() or die "Can't quit ftp connections: $!";
}

sub bundleDelivery {
	my $self = shift;
    my $outfh = new IO::File("<$self->{outname}") 
                              || die "Unable to open \"$self->{outname}\" - $!";
    while ( 1 ) {
    	$outfh->read(my $buff, 32767) unless $outfh->eof();
    	last unless length($self->{inbuff}) || length($buff);
    	$self->{inbuff} .= $buff;
    	
    	while ( length($self->{inbuff}) > 1 && (my $reclen = unpack('n', $self->{inbuff}) < length($self->{inbuff})+1 ) ) { 
    		(my $rec, $self->{inbuff}) = unpack('n/a a*', $self->{inbuff});
    		my $outrec = "\x80".pack('n/a*', $rec);
    		$self->deliveryWrite() if ( ($self->{outlen} + length($outrec)) > 32767 );
    		$self->{outbuff} .= $rec; 
            $self->{outlen} += length($outrec);
    	}
    }
    $outfh->close();
    $self->deliveryClose();
}

sub buildReportIndex {
   my ($self, $info, $hdr) = (shift, shift, shift);
#   warn "bri called by ", (caller())[0,2], " INFO\n", Dumper($info), "\n";
   my $lines = [];
   push @$lines, (
     "-+-----------------+-------------------------------+---------+----------+-----------+------------+"
    ,"++-----------------+-------------------------------+---------+----------+-----------+------------+"
    ," | UTENTE          | TABULATO                      |  PAGINE |    LINEE | JOB NAME  | JOB ID     |"
    ," +-----------------+-------------------------------+---------+----------+-----------+------------+"
    ,"++-----------------+-------------------------------+---------+----------+-----------+------------+"
    ) if $hdr;
  push @$lines, split /\n/, form( 
     " | <<<<<<<<<<<<<<< | <<<<<<<<<<<<<<<<<<<<<<<<<<<<< | >>>>>>> | >>>>>>>> | <<<<<<<<< | <<<<<<<<<< |",
     { cols =>[qw(RecipientName UserRef Pages Lines JobName JobNumber)] 
     	,from => $info
     } ) if defined($info);
  return @$lines;
} 

sub createBundleBanner {
   my ($self, $info) = (shift, shift);
#   warn "createBundleBanner INFO: ", Dumper($info), "\n";
   my $tail = "1\n\n\n"
 ."                       FFFFFFFFFF        IIIIII         NN       NN        EEEEEEEEEE   \n"
 ."                       FFFFFFFFFF        IIIIII         NNN      NN        EEEEEEEEEE   \n"
 ."                       FF                  II           NNNN     NN        EE   \n"
 ."                       FFFFFFFFFF          II           NN NN    NN        EEEEEEEEEE   \n"
 ."                       FFFFFFFFFF          II           NN  NN   NN        EEEEEEEEEE   \n"
 ."                       FF                  II           NN   NN  NN        EE   \n"
 ."                       FF                  II           NN    NN NN        EE   \n"
 ."                       FF                IIIIII         NN     NNNN        EEEEEEEEEE   \n"
 ."                       FF                IIIIII         NN      NNN        EEEEEEEEEE   \n"
 ."   \n"
 ."   \n"
 ."   \n"
 ."   \n"
 ."                PPPPPPPPP           AAAAAAAAA          CCCCCCCCCC         CCCCCCCCCC          OOOOOOO   \n"
 ."                PPPPPPPPPP         AAAAAAAAAAA        CCCCCCCCCCC        CCCCCCCCCCC         OOOOOOOOO   \n"
 ."                PP      PP         AA       AA        CCC                CCC                OOO     OOO   \n"
 ."                PP      PP         AA       AA        CC                 CC                 OO       OO   \n"
 ."                PPPPPPPPPP         AAAAAAAAAAA        CC                 CC                 OO       OO   \n"
 ."                PPPPPPPPP          AAAAAAAAAAA        CC                 CC                 OO       OO   \n"
 ."                PP                 AA       AA        CCC                CCC                OOO     OOO   \n"
 ."                PP                 AA       AA        CCCCCCCCCCC        CCCCCCCCCCC         OOOOOOOOO   \n"
 ."                PP                 AA       AA         CCCCCCCCCC         CCCCCCCCCC          OOOOOOO   \n"
 ."   \n"
 ."   \n"
 ."   \n"
 ."   \n"
 ."          CCCCCCCCCC   OOOOOOO   NN       NN TTTTTTTTTTT RRRRRRRRRR    OOOOOOO   LL                 // DDDDDDDD   \n"
 ."         CCCCCCCCCCC  OOOOOOOOO  NNN      NN TTTTTTTTTTT RRRRRRRRRRR  OOOOOOOOO  LL                //  DDDDDDDDD   \n"
 ."         CCC         OOO     OOO NNNN     NN     TTT     RR       RR OOO     OOO LL               //   DD     DDD   \n"
 ."         CC          OO       OO NN NN    NN     TTT     RRRRRRRRRRR OO       OO LL              //    DD      DDD   \n"
 ."         CC          OO       OO NN  NN   NN     TTT     RRRRRRRRRR  OO       OO LL             //     DD      DDD   \n"
 ."         CC          OO       OO NN   NN  NN     TTT     RR   RR     OO       OO LL            //      DD      DDD   \n"
 ."         CCC         OOO     OOO NN    NN NN     TTT     RR    RR    OOO     OOO LL           //       DD     DDD   \n"
 ."         CCCCCCCCCCC  OOOOOOOOO  NN     NNNN     TTT     RR      RR   OOOOOOOOO  LLLLLLLLLL  //        DDDDDDDDD   \n"
 ."          CCCCCCCCCC   OOOOOOO   NN      NNN     TTT     RR       RR   OOOOOOO   LLLLLLLLLL //         DDDDDDDD   \n"
;

   return (
    [map { pack('n/a',$self->{xlator}->toebcdic($_)) }
     '1      XR - BUNDLES'
    ,'-      I N D I C E      '
    ,'+      I N D I C E      '
    ,' '.'-' x 50 
    ,'+'.'-' x 50 
    ,map { $_ } $self->buildReportIndex(undef, 1)
    ,map { map { map { $_ } ($self->buildReportIndex($_->{jobs}) ) } 
    	                                          values %{$_->{reports}} } values %{$info->{folders}}
    ], 
    [ map { pack('n/a',$self->{xlator}->toebcdic($_)) } split /\n/, $tail ]
   );

}

sub createUserBanner {
   my ($self, $info) = (shift, shift);
#   warn "createUserBanner INFO: ", Dumper($info), "\n";
   my $tail = "1\n \n \n"
 ."                       FFFFFFFFFF        IIIIII         NN       NN        EEEEEEEEEE   \n"
 ."                       FFFFFFFFFF        IIIIII         NNN      NN        EEEEEEEEEE   \n"
 ."                       FF                  II           NNNN     NN        EE   \n"
 ."                       FFFFFFFFFF          II           NN NN    NN        EEEEEEEEEE   \n"
 ."                       FFFFFFFFFF          II           NN  NN   NN        EEEEEEEEEE   \n"
 ."                       FF                  II           NN   NN  NN        EE   \n"
 ."                       FF                  II           NN    NN NN        EE   \n"
 ."                       FF                IIIIII         NN     NNNN        EEEEEEEEEE   \n"
 ."                       FF                IIIIII         NN      NNN        EEEEEEEEEE   \n"
 ."   \n"
 ."   \n"
 ."   \n"
 ."   \n"
 ."   SSSSSSSSS         TTTTTTTTTTT         AAAAAAAAA         MM      MM        PPPPPPPPP          EEEEEEEEEE   \n"
 ."  SSSSSSSSSSS        TTTTTTTTTTT        AAAAAAAAAAA        MMM    MMM        PPPPPPPPPP         EEEEEEEEEE   \n"
 ."  SS       SS            TTT            AA       AA        MMMM  MMMM        PP      PP         EE   \n"
 ."    SS                   TTT            AA       AA        MM MMMM MM        PP      PP         EEEEEEEEEE   \n"
 ."      SS                 TTT            AAAAAAAAAAA        MM  MM  MM        PPPPPPPPPP         EEEEEEEEEE   \n"
 ."        SS               TTT            AAAAAAAAAAA        MM      MM        PPPPPPPPP          EE   \n"
 ."  SS      SS             TTT            AA       AA        MM      MM        PP                 EE   \n"
 ."  SSSSSSSSSSS            TTT            AA       AA        MM      MM        PP                 EEEEEEEEEE   \n"
 ."   SSSSSSSSS             TTT            AA       AA        MM      MM        PP                 EEEEEEEEEE   \n"
;
#   my @ilines = ();
#   my $recipientname;
#   while ( my ($userref, $rinfo) = each %{$info} ) { 
#   warn "UserBanner info:", Dumper($rinfo), "\n";
#   	$recipientname = $rinfo->{reports}->[0]->{RecipientName} unless $recipientname;
#   	push @ilines, @{$rinfo->{jobs}};
#   }
   return (
    [ map { pack('n/a',$self->{xlator}->toebcdic($_)) } (
     '1'.'*' x 120
    ,' '.'*' x 120
    ,' '.'*' x 120
    ,' '.'*' x 120
    ,'-     STAMPE PER: ' . $info->{name}
    ,'     *'.('*' x 47).'*      '.('*' x 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* PAGINE  : '. $info->{Pages}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* LINEE   : '. $info->{Lines}.(' ' x 40), 0, 40).'*'
    ,'     *'.('*' x 47).'*      '.('*' x 40).'*'
    ,'-      I N D I C E      '
    ,'+      I N D I C E      '
    ,'-'.'-' x 80
    ,'+'.'-' x 80 
    ,map { $_ } $self->buildReportIndex(undef, 1)
    ,map { map { $_ } $self->buildReportIndex($_->{jobs}) } values %{$info->{reports}}
#    ,($self->buildReportIndex(\@ilines))
    )
    ], 
    [ map { pack('n/a',$self->{xlator}->toebcdic($_)) } (
    map { $_ } split(/\n/, $tail)
    ,'-     ' . $info->{name}
    ,'     *'.('*' x 47).'*      '.('*' x 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* PAGINE  : '. $info->{Pages}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* LINEE   : '. $info->{Lines}.(' ' x 40), 0, 40).'*'
    ,'     *'.('*' x 47).'*      '.('*' x 40).'*'
    ) 
    ]
   );
}

sub createJobBanner {
=pod
 #leggere da file di configurazione
 $hdrre = ''
  .'PAGES[ \:]+(\d+)'
  .'.*?LINES[ \:]+(\d+)'
  .'.*?JOBNAME[ \:]+(\S+)'
  .'.*?JOB ID[ \:]+(\d+)'
  .'.*?DATE[ \:]+([\d\/]{8})'
  .'.*?TIME[ \:]+([\d:]{8})'
  .''
 ;
 $hdrre = ''
 .'PAGINE +: +(\d+)'
  .'.*?LINEE +: +(\d+)'
  .'.*?TABULATO +: (.{20})'
  .'.*?JOBNAME +: +(\S+)'
  .'.*?JOB ID +: +(\d+)'
  .'.*?DATA +: +([\d\/]{8})'
  .'.*?ORA +: +([\d:]{8})'
  .''
 ;

 #leggere da file di configurazione
 sub getJobEntry {
   my ($descr, $lines, $pages, $jobName, $jobNumber)
     = 
   unpack("\@19a20\@58a10\@45a10\@71a8\@86a5", $_[0]);
   return ($descr, 1, $lines, $pages, $jobName, $jobNumber);
 }

 sub getJobEntryToro {
   my ($descr, $copies, $lines, $pages, $jobName, $jobNumber)
     = 
   unpack("\@12a20\@35a8\@45a10\@58a9\@72a8\@85a5", $_[0]);
   return ($descr, $copies, $lines, $pages, $jobName, $jobNumber);
 }

 sub splitJobExecTime {
   my ($jobExecDate, $jobExecTime) = (shift, shift);

   my ($day, $month, $year) = unpack("a2xa2xa2", $jobExecDate);
   my ($hour, $minute, $seconds) = unpack("a2xa2xa2", $jobExecTime);
   
   return ($year, $month, $day, $hour, $minute, $seconds);
 }
 #---------------------------------------------------------------
]]></code>

=cut
   my ($self, $info) = (shift, shift);
   return (
    [ map { pack('n/a',$self->{xlator}->toebcdic($_)) } 
     '1'.'*' x 120
    ,' '.'*' x 120
    ,' '.'*' x 120
    ,' '.'*' x 120
    , ' ', ' ', ' '
    ,'     *'.('*' x 47).'*      '.('*' x 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* PAGINE  : '. $info->{Pages}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* LINEE   : '. $info->{Lines}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* '.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* TABULATO: '. $info->{UserRef}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* JOBNAME : '. $info->{JobName}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* JOB ID  : '. $info->{JobNumber}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* '.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* DATA    : '. substr($info->{UserTimeRef},  0, 10).(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* ORA     : '. substr($info->{UserTimeRef}, 10, 10).(' ' x 40), 0, 40).'*'
    ,'     *'.('*' x 47).'*      '.('*' x 40).'*'
   ], 
   []
   );
}

1;