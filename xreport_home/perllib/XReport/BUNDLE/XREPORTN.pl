sub buildReportIndex {
   my ($self, $info, $hdr) = (shift, shift, shift);
#   warn "bri called by ", (caller())[0,2], " INFO\n", Dumper($info), "\n";
   my $lines = [];
   push @$lines, ( ref($hdr) eq 'ARRAY' ? @$hdr : (
     "-+-----------------+-------------------------------+---------+----------+-----------+------------+"
    ,"++-----------------+-------------------------------+---------+----------+-----------+------------+"
    ," | UTENTE          | TABULATO                      |  PAGINE |    LINEE | JOB NAME  | JOB ID     |"
    ," +-----------------+-------------------------------+---------+----------+-----------+------------+"
    ,"++-----------------+-------------------------------+---------+----------+-----------+------------+"
#   .' | [[[[[[[[[[[[[[[ | [[[[[[[[[[[[[[[[[[[[[[[[[[[[[ | ]]]]]]] | ]]]]]]]] | [[[[[[[[[ | [[[[[[[[[[ |'
    )) if $hdr;
  push @$lines, map { (my $l = $_) =~ tr/\^\�/\<\>/; $l } split /\n/, form( 
#     { header => ''
#     .' +-----------------+-------------------------------+---------+----------+-----------+------------+'."\n"
#     .'++-----------------+-------------------------------+---------+----------+-----------+------------+'."\n"
#     .' | USER            |REPORT                         | PAGES   | LINES    | JOBNAME   | JOBID      |'."\n"
#     .' +-----------------+-------------------------------+---------+----------+-----------+------------+'."\n"
#     .'++-----------------+-------------------------------+---------+----------+-----------+------------+'
#     ,pagelen => 0
#     }
#     ,''
     ''
     .' | [[[[[[[[[[[[[[[ | [[[[[[[[[[[[[[[[[[[[[[[[[[[[[ | ]]]]]]] | ]]]]]]]] | [[[[[[[[[ | [[[[[[[[[[ |'
     ,{ cols => [qw(JobName JobNumber RecipientName InputPages InputLines UserRef RecipientName UserRef InputPages InputLines JobName JobNumber)] 
        ,from => [ @$info ]
     } 
     ) if defined($info);
  return @$lines;
} 

sub createBundleBanner {
   my ($self, $info) = (shift, shift);
#   warn "createBundleBanner INFO: ", Dumper($info), "\n";
   my $tailstr = "1\n\n\n"
 ."                       FFFFFFFFFF        IIIIII         NN       NN        EEEEEEEEEE   \n"
 ."                       FFFFFFFFFF        IIIIII         NNN      NN        EEEEEEEEEE   \n"
 ."                       FF                  II           NNNN     NN        EE   \n"
 ."                       FFFFFFFFFF          II           NN NN    NN        EEEEEEEEEE   \n"
 ."                       FFFFFFFFFF          II           NN  NN   NN        EEEEEEEEEE   \n"
 ."                       FF                  II           NN   NN  NN        EE   \n"
 ."                       FF                  II           NN    NN NN        EE   \n"
 ."                       FF                IIIIII         NN     NNNN        EEEEEEEEEE   \n"
 ."                       FF                IIIIII         NN      NNN        EEEEEEEEEE   \n"
 ."   \n"
 ."   \n"
 ."   \n"
 ."   \n"
 ."                PPPPPPPPP           AAAAAAAAA          CCCCCCCCCC         CCCCCCCCCC          OOOOOOO   \n"
 ."                PPPPPPPPPP         AAAAAAAAAAA        CCCCCCCCCCC        CCCCCCCCCCC         OOOOOOOOO   \n"
 ."                PP      PP         AA       AA        CCC                CCC                OOO     OOO   \n"
 ."                PP      PP         AA       AA        CC                 CC                 OO       OO   \n"
 ."                PPPPPPPPPP         AAAAAAAAAAA        CC                 CC                 OO       OO   \n"
 ."                PPPPPPPPP          AAAAAAAAAAA        CC                 CC                 OO       OO   \n"
 ."                PP                 AA       AA        CCC                CCC                OOO     OOO   \n"
 ."                PP                 AA       AA        CCCCCCCCCCC        CCCCCCCCCCC         OOOOOOOOO   \n"
 ."                PP                 AA       AA         CCCCCCCCCC         CCCCCCCCCC          OOOOOOO   \n"
 ."   \n"
 ."   \n"
 ."   \n"
 ."   \n"
 ."          CCCCCCCCCC   OOOOOOO   NN       NN TTTTTTTTTTT RRRRRRRRRR    OOOOOOO   LL                 // DDDDDDDD   \n"
 ."         CCCCCCCCCCC  OOOOOOOOO  NNN      NN TTTTTTTTTTT RRRRRRRRRRR  OOOOOOOOO  LL                //  DDDDDDDDD   \n"
 ."         CCC         OOO     OOO NNNN     NN     TTT     RR       RR OOO     OOO LL               //   DD     DDD   \n"
 ."         CC          OO       OO NN NN    NN     TTT     RRRRRRRRRRR OO       OO LL              //    DD      DDD   \n"
 ."         CC          OO       OO NN  NN   NN     TTT     RRRRRRRRRR  OO       OO LL             //     DD      DDD   \n"
 ."         CC          OO       OO NN   NN  NN     TTT     RR   RR     OO       OO LL            //      DD      DDD   \n"
 ."         CCC         OOO     OOO NN    NN NN     TTT     RR    RR    OOO     OOO LL           //       DD     DDD   \n"
 ."         CCCCCCCCCCC  OOOOOOOOO  NN     NNNN     TTT     RR      RR   OOOOOOOOO  LLLLLLLLLL  //        DDDDDDDDD   \n"
 ."          CCCCCCCCCC   OOOOOOO   NN      NNN     TTT     RR       RR   OOOOOOO   LLLLLLLLLL //         DDDDDDDD   \n"
;
   
    my $head =
        [map { $self->formatEbcdicBlockRec($_) } (
         '1         XR-BUNDLE'
        ,'          ** $$BNDLST *************************************'
        ,'          *  MISSION '.substr($info->{BundleName}.' ' x 10, 0, 10).'                            *'
        ,'          *  ODATE '.substr($info->{odate}.' ' x 10, 0, 10).'                             *'
        ,'          *************************************************'
#        ,'-         I N D I C E - $$UINDXH'
#        ,'+         I N D I C E - $$UINDXH'
#        ,' '.'-' x 50 
#        ,'+'.'-' x 50 
#        ,map { $_ } $self->buildReportIndex(undef, 1)
#        ,map { map { map { $_ } ($self->buildReportIndex($_->{jobs}) ) } 
#                                sort { $a->{UserRef} cmp $b->{UserRef} } values %{$_->{reports}} } 
#                                     sort { $a->{RecipientName} cmp $b->{RecipientName} } values %{$info->{folders}}
        )];
    push @$head, map { $self->formatEbcdicBlockRec($_) } ( $self->buildReportIndex(delete $info->{IndexArray},
                                        [ 
     '-         I N D I C E - $$UINDXH'
    ,'+         I N D I C E - $$UINDXH'
    ,' '.'-' x 50 
    ,'+'.'-' x 50 
    ,"-+-----------------+-------------------------------+---------+----------+-----------+------------+"
    ,"++-----------------+-------------------------------+---------+----------+-----------+------------+"
    ," | UTENTE          | TABULATO                      |  PAGINE |    LINEE | JOB NAME  | JOB ID     |"
    ," +-----------------+-------------------------------+---------+----------+-----------+------------+"
    ,"++-----------------+-------------------------------+---------+----------+-----------+------------+"
                                        ])) if exists($info->{IndexArray});
    my $tail = [ map { pack('n/a',$self->{xlator}->toebcdic($_)) } split /\n/, $tailstr ];
   
    $info->{BannerPages} = scalar(grep /^\xF1/, (@{$head},@{$tail})); 
    $info->{BannerLines} = scalar(@{$head})+scalar(@{tail});

    return ($head, $tail);

}

sub createUserBanner {
   my ($self, $info) = (shift, shift);
#   warn "createUserBanner INFO: ", Dumper($info), "\n";
   my $tailstr = "1\n \n \n"
 ."                       FFFFFFFFFF        IIIIII         NN       NN        EEEEEEEEEE   \n"
 ."                       FFFFFFFFFF        IIIIII         NNN      NN        EEEEEEEEEE   \n"
 ."                       FF                  II           NNNN     NN        EE   \n"
 ."                       FFFFFFFFFF          II           NN NN    NN        EEEEEEEEEE   \n"
 ."                       FFFFFFFFFF          II           NN  NN   NN        EEEEEEEEEE   \n"
 ."                       FF                  II           NN   NN  NN        EE   \n"
 ."                       FF                  II           NN    NN NN        EE   \n"
 ."                       FF                IIIIII         NN     NNNN        EEEEEEEEEE   \n"
 ."                       FF                IIIIII         NN      NNN        EEEEEEEEEE   \n"
 ."   \n"
 ."   \n"
 ."   \n"
 ."   \n"
 ."   SSSSSSSSS         TTTTTTTTTTT         AAAAAAAAA         MM      MM        PPPPPPPPP          EEEEEEEEEE   \n"
 ."  SSSSSSSSSSS        TTTTTTTTTTT        AAAAAAAAAAA        MMM    MMM        PPPPPPPPPP         EEEEEEEEEE   \n"
 ."  SS       SS            TTT            AA       AA        MMMM  MMMM        PP      PP         EE   \n"
 ."    SS                   TTT            AA       AA        MM MMMM MM        PP      PP         EEEEEEEEEE   \n"
 ."      SS                 TTT            AAAAAAAAAAA        MM  MM  MM        PPPPPPPPPP         EEEEEEEEEE   \n"
 ."        SS               TTT            AAAAAAAAAAA        MM      MM        PPPPPPPPP          EE   \n"
 ."  SS      SS             TTT            AA       AA        MM      MM        PP                 EE   \n"
 ."  SSSSSSSSSSS            TTT            AA       AA        MM      MM        PP                 EEEEEEEEEE   \n"
 ."   SSSSSSSSS             TTT            AA       AA        MM      MM        PP                 EEEEEEEEEE   \n"
;
    my $head = [ map { $self->formatEbcdicBlockRec($_) } (
     '1'.'*' x 120
    ,' '.'*' x 120
    ,' '.'*' x 120
    ,' '.'*' x 120
    ,'{D3AF5F000000}S1ST7000{00000B}{000077}'
    ,'-     STAMPE PER: ' . $info->{RecipientName}
    ,'     *'.('*' x 47).'*      '.('*' x 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* PAGINE  : '. $info->{InputPages}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* LINEE   : '. $info->{InputLines}.(' ' x 40), 0, 40).'*'
    ,'     *'.('*' x 47).'*      '.('*' x 40).'*'
#        ,'-         I N D I C E - $$UINDXH'
#        ,'+         I N D I C E - $$UINDXH'
#        ,' '.'-' x 50 
#        ,'+'.'-' x 50 
#    ,map { $_ } $self->buildReportIndex(undef, 1)
#    ,map { map { $_ } $self->buildReportIndex($_->{jobs}) } values %{$info->{reports}}
#    ,($self->buildReportIndex(\@ilines))
    )
    ]; 
    push @$head, map { $self->formatEbcdicBlockRec($_) } ( $self->buildReportIndex(delete $info->{IndexArray},
                                        [ 
     '-         I N D I C E - $$UINDXH'
    ,'+         I N D I C E - $$UINDXH'
    ,' '.'-' x 50 
    ,'+'.'-' x 50 
    ,"-+-----------------+-------------------------------+---------+----------+-----------+------------+"
    ,"++-----------------+-------------------------------+---------+----------+-----------+------------+"
    ," | UTENTE          | TABULATO                      |  PAGINE |    LINEE | JOB NAME  | JOB ID     |"
    ," +-----------------+-------------------------------+---------+----------+-----------+------------+"
    ,"++-----------------+-------------------------------+---------+----------+-----------+------------+"
                                        ])) if exists($info->{IndexArray});
    my $tail = [ map { pack('n/a',$self->{xlator}->toebcdic($_)) } (
    map { $_ } split(/\n/, $tailstr )
    ,'-     ' . $info->{RecipientName}
    ,'     *'.('*' x 47).'*      '.('*' x 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* PAGINE  : '. $info->{InputPages}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* LINEE   : '. $info->{InputLines}.(' ' x 40), 0, 40).'*'
    ,'     *'.('*' x 47).'*      '.('*' x 40).'*'
    ) 
    ];

    $info->{BannerPages} = scalar(grep /^\xF1/, (@{$head},@{$tail})); 
    $info->{BannerLines} = scalar(@{$head})+scalar(@{tail});

    return ($head, $tail);

}

sub createJobBanner {
   my ($self, $info) = (shift, shift);
#   warn "createJobBanner INFO: ", Dumper($info), "\n";
   my $head = [ map { $self->formatEbcdicBlockRec($_) } ( 
     '1'.'*' x 120
    ,' '.'*' x 120
    ,' '.'*' x 120
    ,' '.'*' x 120
    , ' ', ' ', ' '
    ,'     *'.('*' x 47).'*      '.('*' x 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* PAGINE  : '. $info->{InputPages}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* LINEE   : '. $info->{InputLines}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* '.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* TABULATO: '. $info->{UserRef}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* JOBNAME : '. $info->{JobName}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* JOB ID  : '. $info->{JobNumber}.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* '.(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* DATA    : '. substr($info->{UserTimeRef},  0, 10).(' ' x 40), 0, 40).'*'
    ,'     *'.(' ' x 47).'*      '.substr('* ORA     : '. substr($info->{UserTimeRef}, 10, 10).(' ' x 40), 0, 40).'*'
    ,'     *'.('*' x 47).'*      '.('*' x 40).'*'
   )]; 
   
   my $tail = [];

   $info->{BannerPages} = scalar(grep /^\xF1/, (@{$head},@{$tail})); 
   $info->{BannerLines} = scalar(@{$head})+scalar(@{tail});

   return ($head, $tail);

}

1;