package XReport::Matrix;

sub TRANSPOSE {
  my ($ti , @cols) = @_; my $to = [];
  
  for(my $i = 0; $i < @{$ti}; $i++){
    for(my $j = 0; $j < @cols; $j++){
      $to->[$j][$i] = $ti->[$i][$cols[$j]];
      #print "$i/$j $to->[$j][$i]\n";
    }
  }
 
  return $to;
}
