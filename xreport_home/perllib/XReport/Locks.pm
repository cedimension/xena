package XReport::Locks;

use strict;

use Symbol;
use Fcntl qw(:flock);

sub getLock {
  my ($self, $lockName) = @_; $lockName =~ s/[:\/\\]/_/g; my $lockTable = $self->{'lockTable'}; 
  my $lckFh = gensym();
  my $LocksHome = $XReport::cfg->{'LocksHome'};
  die "LocksHome config directive not found or empty" unless $LocksHome;
  open($lckFh, ">", "$LocksHome/$lockName.lck") 
   or 
  die("Unable to open LockFile $lockName \"$LocksHome/$lockName.lck\"- $!\n");

  $lockTable->{$lockName} = $lckFh; flock($lckFh, LOCK_EX);
}

sub relLock {
  my ($self, $lockName) = @_; $lockName =~ s/[:\/\\]/_/g; my $lockTable = $self->{'lockTable'};
  my $lckFh = $lockTable->{$lockName};
  my $LocksHome = $XReport::cfg->{'LocksHome'};
  die "LocksHome config directive not found or empty" unless $LocksHome;
  
  flock($lckFh, LOCK_UN); 
  close($lckFh);
  
  unlink "$LocksHome/$lockName.lck"; delete $lockTable->{$lockName}; 
}

sub relAllLocks {
  my $self = shift; my $lockTable = $self->{'lockTable'};
  my $LocksHome = $XReport::cfg->{'LocksHome'};
  die "LocksHome config directive not found or empty" unless $LocksHome;
  for my $lockName (keys(%$lockTable)) {
    my $lckFh = $lockTable->{$lockName};
    flock($lckFh, LOCK_UN); 
    close($lckFh);
	unlink "$LocksHome/$lockName.lck"; delete $lockTable->{$lockName}; 
  }
}

sub new {
  my $class = shift; bless { lockTable => {} }, $class;
}

sub DESTROY {
  my $self = shift; $self->relAllLocks(); return undef;
}

1;
