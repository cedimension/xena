package XReport::Spool;

use Carp;
require Exporter;


##############################################################################
# Define some constants
#

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

@ISA               = qw(Exporter);
@EXPORT            = qw();
@EXPORT_OK         = qw();
$VERSION           = '1.00';

use File::Copy;
use POSIX qw(strftime);

use XReport;
use XReport::DBUtil;

use constant MANDATORY_VALS => {
				JobReportId => '0' 
				,SpoolPathId  => 'L1'
				,InputFileName  => '.'
				,PrintDest  => '.'
				,PrintDuplex  => '0'
				,PrintParameters => '.'
				,LocalFileName  => '.'
				,ContentType => 'application/pdf'
				,SrvName => $main::Application->{ApplName}
			       };
use constant { 
         MANDATORY_COLS => [ keys %{+MANDATORY_VALS} ],
		 INFO_COLS => [ qw(
				      RemoteHostName
				      RemoteHostAddr
				      RemoteQueueName
				      JobName
				      JobNumber
				      XferDaemon
				      XferStartTime
				      XferEndTime
				      XferRecipient
				      XferMode
				      XferInBytes
				      XferOutBytes
				      XferId
				      Status
				      RetryCount
				      SrvName
				   ) ]
		   , IDENTITY => 'JobSpoolId'
		   , TBNAME => 'tbl_JobSpool'
    };

use constant { PRINTER_ATTRS => [ qw(
				      PrtName
				      PrtSrvAddress
				      Caption
				      Comment
				      Description
				      DeviceID
				      DriverName
				      ExtendedPrinterStatus
				      HorizontalResolution
				      InstallDate
				      LanguagesSupported
				      Location
				      MaxSizeSupported
				      PaperSizesSupported
				      PortName
				      PrinterState
				      PrinterStatus
				      Queued
				      ServerName
				      Status
				      StatusInfo
				      SystemName
				   ) ],
				 };
sub import {
  # Handle the :strict tag
  $main::thisservice = (grep /^#[^\s]+$/, @_)[0];
  @_ = grep(!/^#[^\s]+$/, @_);
  if ( $main::thisservice ) {
    $main::thisservice = substr($main::thisservice, 1).'.pm';
    require $main::thisservice;
  }
  goto &Exporter::import;
}

sub new {

  my $class = shift;
  my $self = { };
  my %args = @_;
  eval {$self->{dbc} = XReport::DBUtil->new( DBNAME => 'XRSPOOL', %args );} unless exists($self->{dbc}) && ref($self->{dbc});
  croak "Unable to get DB Connection handle - $@" if $@;
  bless $self, $class;
  if ( $self->{+IDENTITY} = delete $args->{+IDENTITY}  ) {
    my $sdbr = $self->{dbc}->dbExecute("SELECT * from ".TBNAME." WHERE ".IDENTITY." = ".$self->{+IDENTITY});
    if ( $sdbr->eof() ) {
      delete $self->{+IDENTITY};
    }
    else {
      $self->getFldsVals( $sdbr->Fields() );
      my $wdbr = dbExecute("SELECT TOP 1 * from tbl_WorkQueue where ExternalTableName = 'tbl_JobSpool' AND ExternalKey = ".$self->{+IDENTITY});
      if ( $wdbr->eof() ) {
	delete $self->{+IDENTITY};
      }
      else {
	$self->getFldsVals( $wdbr->Fields() )
      }
    }
  }
  else {
    delete $self->{+IDENTITY};
  }
#  if ( !$main::Application->{'XRSPOOL.ColTypes'} ) {
#    my $dbr = $self->{dbc}->dbExecute("SELECT TOP 0 * FROM tbl_JobSpool");
#    my $fields = $dbr->Fields();
#    croak "Unable to fetch columns attributes for tbl_JobSpool" unless $fields;
#    my $COLAttrs = { map { $_->Name(), unpack("H*",$_->Attributes()) } in $fields }; 
#    print Dumper($COLAttrs);
#    $main::Application->{'XRSPOOL.ColTypes'} = join(' ', %$COLAttrs );
#  }
  while ( my ($k, $v) = each %$args ) {
    $self->{$k} = $v;
  }
  return $self;
}

sub UpdateEntry {
  my $self = shift;
  my %args = @_;
  $id = ( $args{+IDENTITY} ? delete $args{+IDENTITY} : $self->{+IDENTITY} );   
  croak "Entry ID to update not specified " unless $id;

  my $sqlst = "UPDATE tbl_JobSpool SET " 
    . join( ', ', map { "$_ = '" . $args{$_} . "'" } keys %args )
      . " WHERE " . IDENTITY . " = '$id'";

  $self->{dbc}->dbExecute($sqlst);

}

sub UpdateQEntry {
  my $self = shift;
  my %args = @_;
  $id = ( $args{WorkId} ? delete $args{WorkId} : $self->{WorkId} );   
  croak "Entry ID to update not specified " unless $id;

  my $sqlst = "UPDATE tbl_WorkQueue SET " 
    . join( ', ', map { "$_ = '" . $args{$_} . "'" } keys %args )
      . " WHERE WorkId= '$id'";

  $self->{dbc}->dbExecute($sqlst);

}

sub QueueEntry {
  my ($self) = (shift);
  $args = { @_ };

  @{$self}{qw(ExternalTableName ExternalKey Status)} = ('tbl_JobSpool', $self->{+IDENTITY}, $CD::stQueued );
  if($self->{InputFileName} !~ /NOFILE/) {
    $self->{Status} = $CD::stQueued;
  } else {
    $self->{Status} = $CD::stReceiving;
  }
  $self->{TypeOfWork} = 515 unless exists($self->{TypeOfWork});
  $self->{SrvParameters} = '' unless exists($self->{SrvParameters});
  my @wqcols = ( grep /^(?:ExternalTableName|ExternalKey|TypeOfWork|WorkClass|Priority|Status|SrvParameters)$/, keys %$self );
  
  $self->{dbc}->dbExecute("BEGIN TRANSACTION");

#  print Dumper($self), "\nWQCOLS:", Dumper(\@wqcols);
  $sqlst = ("INSERT INTO tbl_WorkQueue " 
			  . "(" . join(',', @wqcols ) . ") "
			  . "VALUES ( '" . join("','", (map { $self->{$_} } @wqcols ) ) . "' )"
			 );
  $self->{dbc}->dbExecute($sqlst);
    
  $dbr = $self->{dbc}->dbExecute( "SELECT \@\@IDENTITY AS WorkId" );
  $self->{WorkId} = $dbr->Fields()->{WorkId}->Value();

  $self->UpdateEntry( Status => $self->{Status} );

  $self->{dbc}->dbExecute("COMMIT TRANSACTION");
#  print Dumper($self);
  return $self->{WorkId};

}

sub getFldsVals {
  my ($self, $fields) = (shift, shift);
  $args = { @_ };
  croak "Unable to fetch columns attributes for table" unless $fields;
  my $row = {};
  foreach my $fld ( in $fields ) {
    my ($name, $val) = ($fld->Name(), $fld->Value());
    next unless defined($val);
    $val = $val->Date() . $val->Time() if ref($val) && $val->can('Date');
    $row->{$name} = ($args->{$name} ? $args->{$name} : $self->{$name} ? $self->{$name} : $val);
    $self->{$name} = $row->{$name};
  }
  return $row;
}

sub getSpoolPath {
  my ($self, $create) = (shift, shift);
  my ($pathid, $fqn) = @{XReport::ARCHIVE::newTargetPath($self->{SpoolPathId} || '', "spool")}{qw(LocalPathId fqn)};
  $self->{SpoolPathId} = $pathid;
  return $fqn;
#  my $lpath = c::getValues('LocalPath') || croak "LocalPath element missing in config";
#  $lpath = $lpath->{$self->{SpoolPathId}} 
#           || croak "LocalPath element has no entry \"$self->{SpoolPathId}\" in config";
#  ($lpath .= "\\spool") =~ s/\//\\/g;
#  $lpath =~ s/^file:\\\\//i;
#  mkdir $lpath if !-d $lpath && $create;
#  croak "spool path \"$lpath\" not accessible " unless -d $lpath;
#  return $lpath;
}

sub getDestInfos {
  my $self = shift;
  my $args = { @_ };
  $dbr = $self->{dbc}->dbExecute("select TOP 1 * from " 
				 . " (SELECT b.PrintDest, a.*, b.TypeOfWork, b.WorkClass, b.Priority FROM tbl_PrintersAliases b "
				 . " LEFT OUTER JOIN tbl_Printers a on a.PrtName = b.PrtName and a.PrtSrvAddress = b.PrtSrvAddress "
				 . " union all "
				 . " SELECT a.PrtName as PrintDest, a.*, 515 as TypeOfWork, NULL as WorkClass, 0 as Priority FROM tbl_Printers a "
				 . " where not a.PrtName in (select PrintDest from tbl_PrintersAliases) "
				 . " ) DERIVEDTBL "
				 . " WHERE PrintDest = '$args->{PrintDest}'");
  return undef if $dbr->eof();
  return $self->getFldsVals( $dbr->Fields() );
  
}

sub setPrinterAttrs {
  my $soapstruct = shift;
#  print Dumper($soapstruct);
  my $attrs = {};
  @{$attrs}{@{+PRINTER_ATTRS}} = @{ { map { $_ => (exists $soapstruct->{Columns}->{$_}->{content} ? 
						   $soapstruct->{Columns}->{$_}->{content} : 
						   undef )  
					  } keys %{$soapstruct->{Columns}} } }{@{+PRINTER_ATTRS}} ;
#  print Dumper($attrs);
  return $attrs;
}

sub AddFile {
  my $self = shift;
  my $args = { @_ };
  
  croak "Input filename not specified for function" unless exists($args->{InputFileName});
  if($args->{InputFileName} !~ /NOFILE/) {
    croak "Input filename specified not accessible" unless -e $args->{InputFileName};
  }
  $self = __PACKAGE__->new(@_) unless ref($self);
  croak "LocalFileName replace not allowed" if ( exists($self->{+IDENTITY}) && $self->{LocalFileName} !~ /NOFILE/i );

  croak "Print Destination not specified" unless exists($args->{PrintDest});

  my $fn = (split(/[\\\/]/, $args->{InputFileName}))[-1];
  my $fext = (split(/\./, $fn))[-1];

  my $addValues = { map { $_ => (exists($args->{$_}) ? delete($args->{$_}) :  MANDATORY_VALS->{$_} ) } @{+MANDATORY_COLS} }; 
  $self->{SpoolPathId} = $addValues->{SpoolPathId};
  my $lpath = $self->getSpoolPath(1);

  if($args->{InputFileName} !~ /NOFILE/) {
    File::Copy::cp $addValues->{InputFileName}, $lpath."\\$fn" || croak "Unable to copy \"$addValues->{InputFileName}\" to \"$lpath\" - $!";  
  }

#  croak "Unable to get DB Connection handle" unless exists($self->{$dbc}) && ref($self->{dbc}) eq 'XReport::DBUtil';
  @{$self}{@{+MANDATORY_COLS}} = @{$addValues}{@{+MANDATORY_COLS}};
  $self->{Status} = $CD::stAccepted if($args->{InputFileName} !~ /NOFILE/);
  $self->{TypeOfWork} = $args->{TypeOfWork} if(exists($args->{TypeOfWork}));
  $self->{SrvParameters} = $args->{SrvParameters} if(exists($args->{SrvParameters}));
  $self->{+IDENTITY} = $args->{+IDENTITY} if(exists($args->{+IDENTITY}));
  $self->{WorkId} = $args->{WorkId} if(exists($args->{WorkId}));

  $self->{dbc}->dbExecute("BEGIN TRANSACTION");

  if ( !exists($self->{+IDENTITY}) ) {
    my $sqlstmt = ("INSERT INTO tbl_JobSpool " 
		   . "(" . join(',', @{+MANDATORY_COLS} ) . ") "
		   . "VALUES ( '" . join("','", (map { $self->{$_} } @{+MANDATORY_COLS} ) ) . "' )"
		  );
    print "SQL: $sqlstmt\n";
    $self->{dbc}->dbExecute($sqlstmt);
    #  $self->{dbc}->dbExecute("INSERT INTO tbl_JobSpool " 
    #			  . "(" . join(',', @{+MANDATORY_COLS} ) . ") "
    #			  . "VALUES ( '" . join("','", (map { $self->{$_} } @{+MANDATORY_COLS} ) ) . "' )"
    #			 );
    
    my $dbr = $self->{dbc}->dbExecute( "SELECT \@\@IDENTITY AS " . IDENTITY );
    $self->{+IDENTITY} = $dbr->Fields()->{+IDENTITY}->Value();
  }
  my $lfn = join('.', $self->{PrintDest}, strftime("%Y%m%d%H%M%S", localtime(time())), $self->{+IDENTITY}, $fext);
  unlink "$lpath\\$lfn" if -e "$lpath\\$lfn";
  rename "$lpath\\$fn", "$lpath\\$lfn";
  if($self->{InputFileName} !~ /NOFILE/) {
    croak "Unable to rename \"$lpath\\$fn\" to \"$lpath\\$lfn\" - $!" unless -e "$lpath\\$lfn";
    $self->{Status} = $CD::stQueued;
  }

  $self->UpdateEntry( InputFileName => "$lfn", LocalFileName => "$lfn", Status => $self->{Status} );

  $self->{dbc}->dbExecute("COMMIT TRANSACTION");

  $dbr = $self->{dbc}->dbExecute("SELECT TOP 1 * FROM tbl_PrintersAliases where PrintDest = '$self->{PrintDest}'");
#  return undef if $dbr->eof();
  $self->getFldsVals( $dbr->Fields() ) unless $dbr->eof();
  return $self->UpdateQEntry(Status => $self->{Status}) if $self->{WorkId};

  return $self->QueueEntry();

}

sub OpenDS {
  my $self = shift;
  my $args = { @_ };

  croak "Input filename not specified for function" unless exists($args->{+IDENTITY});
  $self = __PACKAGE__->new(@_) unless ref($self);
  $self->{buffsize} = delete $args->{buffsize} || 4096;
  my $sqlcm = "SELECT TOP 1 * from tbl_JobSpool "
				    . " WHERE " . IDENTITY . " = '" . $args->{+IDENTITY}. "'";
  my $dbr = $self->{dbc}->dbExecute($sqlcm);

  croak "unable to fetch attributes from ID " .$args->{+IDENTITY} if !$dbr || $dbr->eof();

  $self->getFldsVals( $dbr->Fields() );
  $self->getDestInfos(PrintDest => $self->{PrintDest});
#  print Dumper($self);
  my $lpath = $self->getSpoolPath();

  my $spoolfn = "$lpath\\$self->{LocalFileName}";
  croak "spool file \"$spoolfn\" not accessible " unless -e $spoolfn;

  $self->{dbc}->dbExecute("BEGIN TRANSACTION");

  $self->{DSFH} = new FileHandle("<$spoolfn") || 
      croak "open error for spool file \"$spoolfn\" - $!";
  binmode $self->{DSFH};
  $self->{DSFN} = $spoolfn;

  $self->UpdateEntry( Status => $CD::stProcessing );

  $self->{dbc}->dbExecute("COMMIT TRANSACTION");

  $self->{Status} = $CD::stProcessing;

  return $self;

}

sub ReadDS {
  my $self = shift;
  my $buff = shift;
  $self->{buffsize} = 4096 unless $self->{buffsize};
  return $self->{DSFH}->read($$buff, $self->{buffsize});
}

sub eof { my $self = shift; return $self->{DSFH}->eof(); }

sub CloseDS {
  my $self = shift;
  my $args = { @_ };
  my $purge = delete $args->{Purge};

  my $fh = delete($self->{DSFH});
  $fh->close if $fh;
  return $self->{DSFN} unless exists($args->{Status});

  i::logit "Updating Spool Entry $self->{JobSpoolId} with status: $args->{Status}";
  $self->UpdateEntry( %$args );
  $self->{Status} = $args->{Status};

  if ($args->{Status} == $CD::stCompleted && $purge && $purge !~ /^N/i ) {
    i::logit "Deleting Spool DS \"$self->{DSFN}\" of Entry $self->{JobSpoolId}";
    my $fn = delete($self->{DSFN});
    unlink $fn if $fn;
    return $fn; 
  }
}

1;
