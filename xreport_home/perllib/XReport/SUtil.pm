#######################################################################
# @(#) $Id: SUtil.pm,v 1.2 2002/10/23 22:50:19 Administrator Exp $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
package XReport::SUtil;

use strict;
use Carp;
use warnings;

use Date::Calc;
use FileHandle;

use XReport;
use XReport::Util;
use XReport::Logger;

use Exporter;
our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

# must be all one line, for MakeMaker
$VERSION = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r }; 

@ISA = qw(Exporter);

@EXPORT = qw(
  &spawnProcess &getFileToken &openFileToken
);

@EXPORT_OK = qw( 
  &spawnProcess &getFileToken &openFileToken
);

%EXPORT_TAGS = (
  DAEMON => [qw(
    &spawnProcess &getFileToken &openFileToken
  )],
);  


use File::Basename qw();
use Win32;
use Win32API::File 0.08 qw(:ALL);
use Win32::Process qw(INFINITE NORMAL_PRIORITY_CLASS CREATE_NO_WINDOW STILL_ACTIVE DETACHED_PROCESS);

sub spawnProcessNotPerl {

  my $pgm = shift;
  my @inargs = @_;

  my $pgmid = (split(/\./, File::Basename::basename($pgm)))[0];
  
  Win32::Process::Create(
			 my $ProcessObj,
			 "$pgm",
			 "$pgmid @inargs",
			 1,
             NORMAL_PRIORITY_CLASS|DETACHED_PROCESS,
			 "."
            )|| &$logrRtn("ERROR Launching $pgm - ".Win32::FormatMessage( Win32::GetLastError() ));
			
  return $ProcessObj;
}

sub spawnProcess {

  my $pgm = shift;
  my @inargs = @_;

#  my $basedir = getConfValues('basedir');
  my $incopt = join(' -I ', split /;/, $main::Application->{'ApplPerllib'});
  $incopt = '-I' . $incopt if $incopt;
  &$logrRtn("perl $incopt $pgm @inargs\n");

  Win32::Process::Create(
			 my $ProcessObj,
			 "$^X",
			 "perl $incopt $pgm @inargs",
			 1,
             NORMAL_PRIORITY_CLASS|DETACHED_PROCESS,
			 "."
			)|| &$logrRtn("ERROR Launching $0");
			
  return $ProcessObj;
}

sub getFileToken {

  my $fh = shift;
#  &$logrRtn("getting file token for", $fh->fileno);
  return FdGetOsFHandle( $fh->fileno );

}

sub openFileToken {
  my $fh = shift;
  my $hfno = shift;
#  &$logrRtn("opening file token", $hfno, " to ", ref($fh));
  OsFHandleOpen($fh, $hfno, "rw") or die("CliSock OPEN ERROR <$!>\n");
}

sub spawnProcessAndWait {
  my ($pgm, $timeout, @inargs) = @_;
  $timeout = INFINITE unless $timeout;

  my $pgmid = (split(/\./, File::Basename::basename($pgm)))[0];
  my $pgmstring = join(' ', $pgmid, @inargs);
  &$logrRtn("Starting $pgm as $pgmstring");
  Win32::Process::Create(
             my $ProcessObj,
             $pgm,
             $pgmstring,
             1,
             NORMAL_PRIORITY_CLASS|DETACHED_PROCESS,
             "."
            )|| &$logrRtn("ERROR Launching $pgm - ".Win32::FormatMessage( Win32::GetLastError() ));
  
  return undef unless $ProcessObj;

  my $pid = $ProcessObj->GetProcessID();
  
  $ProcessObj->Wait($timeout);
  $ProcessObj->GetExitCode(my $exitcode);
  if ( $exitcode == STILL_ACTIVE ) {
     $exitcode = 4096;
     $ProcessObj->Kill($exitcode);
     &$logrRtn("KILLED $pgm($pid) due to exceeded run time of $timeout msec");
     return 4096;
  }
  &$logrRtn("$pgm ended - RC: $exitcode");
  return $exitcode;
}

1;

__END__

$Log: SUtil.pm,v $
Revision 1.2  2002/10/23 22:50:19  Administrator
edit

Revision 1.5  2001/04/10 04:12:52  mpezzi
Fixed script name parsing to support backslashes
Added load code handler
fixed child launcher to include ../perllib

Revision 1.4  2001/03/22 13:10:03  mpezzi
Aggiunte versioni ai file

