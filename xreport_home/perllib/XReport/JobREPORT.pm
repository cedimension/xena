package XReport::JobREPORT;

use strict qw(vars);
no warnings 'deprecated';

use Carp;
use Symbol;
use Carp::Assert;

use File::Basename;
use File::Path;
use FileHandle;

use Compress::Zlib;

use POSIX qw(strftime);
use XML::Simple;
use Digest::SHA1;

use XReport;
#use XReport::DBUtil;
use XReport::XMLUtil qw(SQL2XML);
use XReport::Util qw($logger $logrRtn setConfValues);


use XReport::INPUT;
use XReport::Userlib;

#------------------------------------------------------------

use constant RF_ASCII => 1;
use constant RF_EBCDIC => 2;
use constant RF_AFP => 3;
use constant RF_VIPP => 4;

use constant EF_PRINT => 1;
use constant EF_DATA => 2;
use constant EF_EXCEL_QMF => 3;
use constant EF_EXCEL_CSV => 4;

use constant MAX_USER_ELAB_FORMAT => 100;

use constant EF_AFPRESOURCES => 101;
use constant EF_BUNDLE => 102;

use constant XF_LPR => 1;
use constant XF_PSF => 2;
use constant XF_FTPB => 3;
use constant XF_FTPC => 4;

use constant ST_RECEIVED => 16;
use constant ST_QUEUED => 16;
use constant ST_INPROCESS => 17;
use constant ST_COMPLETED => 18;
use constant ST_PROCERROR => 31;

#------------------------------------------------------------

my %FormatterClasses = (
  1 => 'XReport::Parser', 2 => 'XReport::Parser',
  3 => 'XReport::EFORMATs::ExcelQMF', 4 => 'XReport::EFORMATs::ExcelCSV',
);

#------------------------------------------------------------


#------------------------------------------------------------

my %SUFFIX = (
  PDFOUT => 'pdf', PSOUT => 'ps'
);

#------------------------------------------------------------
#todo: mettere new per poi obj->Open(....)

sub Variant_DateTime {
 return $_[0]->Date('yyyy/MM/dd')." ".$_[0]->Time('HH:mm:ss tt'); # 24 hours clock and AM/PM
}

sub JobReportBaseStruct {
  my $className = shift;

  #$self->[0] = tbl_JobReports Fields
  #$self->[1] = tbl_JobReportNames Fields 
  #  OUTER JOIN tbl_Os390PrintParameters Fields
  #$self->[2] = Previous Elab
  #$self->[3] = changed and internal Fields

  my $self = [ {}, {}, {}, {}, {} ]; 
  
  bless $self, $className;
}

sub ChangeJobReportValues {
  my ($self, $NewJobReportName, $NewXferRecipientName) = (shift, shift, shift);
  
  my ($JobReportName, $JobReportId, $LocalFileName) 
   =
  $self->getValues(qw(JobReportName JobReportId LocalFileName));

  if ( $NewJobReportName ne $JobReportName ) {
    my $dataFile = $self->getFileName('INPUT'); my $cntrlFile = $dataFile;

    $cntrlFile =~ s/\.DATA\./\.CNTRL\./i;
    $cntrlFile =~ s/\.gz$//i;

    dbExecute(
     "UPDATE tbl_JobReports set OrigJobReportName = '$JobReportName', PendingOp = 11 " .
     "where JobReportId = $JobReportId "
    );


	my $INPATH = getConfValues('LocalPath')->{$self->getValues('LocalPathId_IN') || "L1"}."/IN";
    die "Unable to change names to Archives not on File System" unless $INPATH =~ /^(?:file:\/\/|[a-zA-Z]:[\/\\]|[\/\\]{2})/i;
    $INPATH =~ s/^file:\/\///i;
    for my $fm ($dataFile, $cntrlFile) {
      my $to = $fm; $to =~ s/\/$JobReportName\./\/$NewJobReportName\./i;
      my $fmfull = "$INPATH/$fm";
      $to = "$INPATH/$to";
      next if ($fm eq $cntrlFile and !-e $fmfull);
      croak("ApplicationError: file \"$fmfull\" NOT FOUND") if !-e $fmfull;
      rename($fmfull, $to) or croak("ApplicationError: RENAME ERROR $! From \"$fm\" To \"$to\"");
    }

    $LocalFileName =~ s/\/$JobReportName\./\/$NewJobReportName\./i;
  }
  
  dbExecute(
   "UPDATE tbl_JobReports set JobReportName = '$NewJobReportName', XferRecipient = '$NewXferRecipientName', " .
   "LocalFileName = '$LocalFileName', PendingOp = 0 " .
   "where JobReportId = $JobReportId "
  );

  $self->setValues('JobReportName', $NewJobReportName, 'XferRecipient', $NewXferRecipientName);
}

sub VerifyJobReportValues {
  my $self = shift; my ($JobReportName, $JobReportId) = $self->getValues(qw(JobReportName JobReportId));


  $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
  my $dbr = $main::xrdbc->dbExecute(
    "SELECT * from vw_JobReportNamesQrexp WHERE fmJobReportName = '$JobReportName' ORDER BY TestOrder "
  );
  return 1 if $dbr->eof();
  
  my ($NewJobReportName, $NewXferRecipientName) = ("", "");

  my @TestFields = (); 
  
  for (0..($dbr->Fields->Count()-1)) {
    my $fieldName = $dbr->Fields->Item($_)->Name();
    if ( $fieldName !~ /JobReportName|TestOrder|MaxPageTests|toXferRecipientName/ ) {
      push @TestFields, $fieldName;
    }
  }

  JOB_REPORT_TEST: {
    my @JobReportTests = (); my $MaxPageReads = 1;
    
    DB_READ: while(!$dbr->eof()) {    
      my $MaxPageTests = $dbr->Fields->Item("MaxPageTests")->Value();
      my $toJobReportName = $dbr->Fields->Item("toJobReportName")->Value();
      my $toXferRecipientName = $dbr->Fields->Item("toXferRecipientName")->Value();
  
      my $PageTextTest = "";
      for my $field ( @TestFields ) {
        my $test = $dbr->Fields->Item("$field")->Value();
        next if $test =~ /^ *$/;
        if ( $field !~ /PageTextTest/i ) {
          next DB_READ 
           if 
          $self->getValues($field) !~ /$test/i;
        }
        else {
          $PageTextTest = qr/$test/si; # do not use m 
        }
      }
      $MaxPageReads = $MaxPageTests if $MaxPageTests > $MaxPageReads;
      push 
        @JobReportTests, 
       [qr/$PageTextTest/, $MaxPageTests, $toJobReportName, $toXferRecipientName]
      ;
    } continue {
      $dbr->MoveNext()
    }
    $dbr->Close();
    
    my $INPUT = XReport::INPUT->new($self); $INPUT->Open();
    
    my %UpdValues = ();
    
    READ_PAGE_TEXT:
    for (my $j=0; $j<$MaxPageReads; $j++ ) {
    
      my $PageRef = $INPUT->GetPage(); last if !$PageRef; 
      my $PageText = join("\n", map {$_->AsciiValue()} @{$PageRef->lineList()});
      
      for my $JobReportTest ( @JobReportTests ) {
      
        my (
          $PageTextTest, 
          $MaxPageTests, 
          $toJobReportName, 
          $toXferRecipientName
        ) = 
        @$JobReportTest;
        
        if ( $j<$MaxPageTests and ($PageTextTest eq '' or $PageText =~ /$PageTextTest/si) ) {
        
          $UpdValues{'JobReportName'} = eval($toJobReportName); die "eval error <$@>" if $@;
          if ( !defined( $UpdValues{'JobReportName'} ) ) {
            next;
          }
          $UpdValues{'XferRecipientName'} = eval($toXferRecipientName); die "eval error <$@>" if $@;
          
          if ( scalar( keys(%UpdValues) ) ) {
            ($NewJobReportName, $NewXferRecipientName) = @UpdValues{qw(JobReportName XferRecipientName)};
            &$logrRtn(
              "JobReport CHANGED / $NewJobReportName / $NewXferRecipientName / $PageTextTest /"
            );
          }
          
        }
      }
    }
    $INPUT->Close();
  }
  return 1 if !$NewJobReportName;

  if ($NewJobReportName eq '$$DELETE$$') {
    $self->setValues('to_delete', 'YES INDEED');
    return 1;
  }

  TEST_NEW_JOBREPORTNAME: {
    $NewJobReportName ne $JobReportName && do {
	  $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
      $dbr = $main::xrdbc->dbExecute(
       "SELECT * from tbl_JobReportNames WHERE JobReportName = '$NewJobReportName' "
      );
      if ( $dbr->eof() ) {
        $NewJobReportName = $JobReportName; last TEST_NEW_JOBREPORTNAME;
        croak("UserError: Target JobReportName $NewJobReportName must be listed in tbl_JobReportNames"); 
      }
  

      $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
      $dbr = $main::xrdbc->dbExecute(
       "SELECT * from vw_JobReportNamesQrexp WHERE fmJobReportName = '$NewJobReportName' ORDER BY TestOrder desc "
      );
      if ( !$dbr->eof() ) {
        croak("UserError: Target JobReportName $NewJobReportName cannot be listed in tbl_JobReportNamesQrexp"); 
      }
    };
  }
  $self->ChangeJobReportValues($NewJobReportName, $NewXferRecipientName);
  
  return $NewJobReportName eq $JobReportName;
}

sub get_INPUT_ARCHIVE {
  require XReport::ARCHIVE;
  return XReport::ARCHIVE::get_INPUT_ARCHIVE(@_);
}

sub get_OUTPUT_ARCHIVE {
  require XReport::ARCHIVE;
  return XReport::ARCHIVE::get_OUTPUT_ARCHIVE(@_);
}

sub Open {
  my ($className, $JobReportId, $FULLMODE) = @_; my ($dbr, $JobReportName); $FULLMODE = 1 if !defined($FULLMODE);


  my $self = JobReportBaseStruct($className); $self->getFields($JobReportId, $FULLMODE);
  
  croak "RESTARTABLE WARNING: JOBREPORT NOT active JobReportId=$JobReportId\n"
    if
  $FULLMODE && !$self->getValues('IsActive');

  my $LocalFileName = basename($self->getValues('LocalFileName'));
#  warn "LocalFileName: $LocalFileName\n";  
  my ($dateTime) = $LocalFileName =~ /[^.]+\.(\d+)\.[\d#]+\.DATA.TXT/i ; $dateTime =~ s/[\/ :]//g;
  
  $self->setValues('$dateTime'=> $dateTime); return $self;
}

sub UPDATE {
}

sub Close {
  my $self = shift; @$self = ();
}

sub NewXferStart {
  my $className = shift; my %args = @_;
  
  my ($SrvName, $JobReportName, $jobName, $jobNumber, $dateTime, $XferId, $PROGR, $XrefData) 
   = 
  @args{qw(SrvName JobReportName JobName JobNumber dateTime XferId PROGR XrefData)}; 

  $dateTime = GetDateTime() if !$dateTime; 

  assert( JobReportName ne '' ); assert( $dateTime =~ /^\d{14}$/ );

  my $XferStartTime = sprintf("\%s/\%s/\%s \%s:\%s:\%s", unpack("a4a2a2 a2a2a2", $dateTime));

  my $self = JobReportBaseStruct($className);

  my ($JobReportId, $jobExecDate, $jobExecTime, $PeerHost, $NjobExecTime, $lines, $pages);

  require XReport::OUTPUT::OStream;

  my $LocalFileName = getConfValues('XferArea')->{'Work'} .
    "/\$$JobReportName.$jobName.$jobNumber.$dateTime.$XferId.$PROGR.DATA.TXT.gz"
  ; 
  my $OUTPUT = new XReport::OUTPUT::OStream($LocalFileName); $OUTPUT->Open();

  $self->setValues( 
    LocalFileName => $LocalFileName, 
    JobReportName => $JobReportName, 
    JobReportId   => $JobReportId,
    JobName       => $jobName, 
    JobNumber     => $jobNumber,
    SrvName       => $SrvName,
    XrefData      => $XrefData,
   '$OUTPUT'      => $OUTPUT,
   '$dateTime'    => $dateTime, 
    XferStartTime => $XferStartTime
  );

  return $self;
}

sub NewXferEnd {
  my $self = shift; my $OUTPUT = $self->getValues('$OUTPUT'); my ($JobReportId, $dbr);
  
  $OUTPUT->Close();
  
  my ($JobReportName, $LocalFileName, $SrvName) 
    = 
  $self->getValues(qw(JobReportName LocalFileName SrvName));
  
  $self->setValues(
    'XferEndTime'  =>  GetDateTime('SQL'),
    'XferOutBytes' => -s $self->getValues('LocalFileName')
  );

  #dbExecute("BEGIN TRANSACTION");
    
  dbExecute(
    "INSERT INTO tbl_JobReports " .
    "( " .
      "JobReportName, LocalFileName, SrvName, Status " .
    ") " .
    "VALUES " .
    "( " .
      "'$JobReportName', '$LocalFileName', '$SrvName', 15 " .
    ") "
  );


  $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
  $dbr = $main::xrdbc->dbExecute("SELECT \@\@IDENTITY AS Id");
  
  if ( !$dbr->eof() ) {
    $JobReportId = $dbr->GetFieldsValues('Id');
    $self->setValues('JobReportId',$JobReportId);
  }
  else {
    croak "ApplicationError: EOF RETRIEVING INSERTED DATA!";
  }
  $dbr->Close();
  
  do {  
#    my $INPATH = getConfValues('LocalPath')->{L1}."/IN"; $INPATH =~ s/file:\/\///; 
    
    my $fmFile = $LocalFileName;

    my $dateTime = $self->getValues('$dateTime');

    my ($YEAR, $MONTH_DAY) = unpack("a4a4", $dateTime);
    
#    $LocalFileName = "$YEAR/$MONTH_DAY/$JobReportName.$dateTime.$JobReportId.DATA.TXT.gz";
#
#    my $todir = "$INPATH/$YEAR/$MONTH_DAY";
#  
#    -d $todir or mkpath($todir)
#      or
#    die("ApplicationError: UNABLE TO CREATE inputdir \"$todir\" DIRECTORY $!");
#
#    my $toFile = "$INPATH/$LocalFileName";

    my $tgt = XReport::ARCHIVE::newTargetPath('L1', "IN/$YEAR/$MONTH_DAY");
    my $toFile = $tgt->{fqn}."/$JobReportName.$dateTime.$JobReportId.DATA.TXT.gz";
    rename($fmFile, $toFile) 
          or croak("ApplicationError: RENAME ERROR From \"$fmFile\" To \"$toFile\" $!");
    
    (my $INPATH, $LocalFileName) = ($toFile = ~ /^(.+[\/\\]IN)\/([^\/\\]+)$/ );
    $self->setValues(
     'LocalFileName' => $LocalFileName,
     '$INPATH' => $INPATH,
     LocalPathId_IN => $tgt->{LocalPathId},
    );
  };

  $self->writeControlFile();

  $self->setValues('Status', ST_RECEIVED); $self->putFields(); 

  #dbExecute("COMMIT TRANSACTION");
}

sub creIBMcontrolFile {
  my $self = shift; return if $self->getValues('$controlFile') ne '';
  
  my ($SrvName, $jobName, $jobNumber, $xferMode) 
    =
  $self->getValues('SrvName', 'JobName', 'JobNumber', 'XferMode');
  
  $self->setValues('$controlFile', 
   "HIT0H" .
   "\nP$SrvName" .
   "\nJJES2.$SrvName.$jobName.JOB$jobNumber.?" .
   "\nfdf$jobNumber.IT0H" .
   "\nNJES2.$SrvName.$SrvName.A.JOB$jobNumber.?" .
   (($xferMode == 2) ? "\n-ofileformat=record" : "")
  );
}

sub readControlFile {
  use Time::Local qw(timegm_nocheck timelocal_nocheck);
  my $self = shift; my $fileName = join("/", $self->getValues(qw($INPATH LocalFileName))); 

  $fileName =~ s/\.DATA\.TXT\.gz$/.CNTRL.TXT/;
  
  my $cf = FileHandle->new("<$fileName")
   or die "OPEN FILE ERROR FOR CONTROL FILE \"$fileName\" rc=$!"; 
  binmode($cf);

  $cf->read(my $cfbuff, -s $fileName); 
  $cf->close();
  my $cinfo = { map { my ($f, $v) = ($_ =~ /^\-oJ(\w+)=(\w+).*$/  ? ($1 => $2)
               : $_ =~ /^\-o(\w+)=(\w+).*$/ ? ($1 => $2)
               : $_ =~ /^J([\w\.\?]+)$/     ? ('J' => $1)
               : $_ =~ /^(\w)(.*)$/         ? ($1 => $2)
               : ('NULL' => 'NULL'));
              $f => $v
            } split/\n/, $cfbuff };

  if ($cinfo->{'TIME'} && $cinfo->{'DATE'}) {
    my ($jcent,$jyr,$jdays) = ($cinfo->{'DATE'} =~ /^(\d{2})(\d{2})(\d{3})$/);
    my ($nsec, $hsec) = ($cinfo->{'TIME'} =~ /^(\d{5})(\d{2})$/);
#    print "c=$jcent, y=$jyr, d=$jdays, n=$nsec, h=$hsec\n";
    if ($jcent && $jyr && $jdays && defined($nsec) && defined($hsec)) {
      $cinfo->{'CRDTIM'} = strftime("%Y-%m-%dT%H:%M:%S.$hsec", 
                    localtime timelocal_nocheck $nsec, 0, 0, $jdays, 0, (($jcent + 19) * 100) + $jyr)
    }
  }
  return $cinfo;
}

sub writeControlFile {
  my $self = shift; my $fileName = join("/", $self->getValues(qw($INPATH LocalFileName))); 

  $fileName =~ s/\.DATA\.TXT\.gz$/.CNTRL.TXT/;

  my  $XferMode = $self->getValues('XferMode'); 

  $self->creIBMcontrolFile()
   if
  !$self->getValues('$controlFile') and ($XferMode == 1 or $XferMode == 2 or $XferMode == 3); 
  
  my $cf = FileHandle->new(">$fileName")
   or
  die "OPEN FILE ERROR FOR CONTROL FILE \"$fileName\" rc=$!"; binmode($cf);

  print $cf $self->getValues('$controlFile'); $cf->close();
}

### todo: move internal code to worqueue
sub QueueForElab {
  my $self = shift; my $JobReportId = $self->getValues('JobReportId');
  dbExecute(
    "INSERT into tbl_WorkQueue " .
    "(ExternalTableName, ExternalKey, InsertTime, TypeOfWork, WorkClass, Status) " .
    "VALUES " .
    "('tbl_JobReports', $JobReportId, GETDATE(), 1, NULL, 16) " 
  );
}

sub getFields {
  my ($self, $JobReportId, $FULLMODE) = (shift, shift, shift);
  my $dbr; 
  my $HIST = ''; 

  setConfValues('db_HIST', $HIST);
  
  while(1) {
    @$self = ({}, {}, {}, {});
	$main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
    $dbr = $main::xrdbc->dbExecute( "SELECT * from tbl_JobReports where JobReportId = $JobReportId" );
    if ( $dbr->eof() ) {
      $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
      $dbr = $main::xrdbc->dbExecute( "SELECT * from tbl_JobReports_HIST where JobReportId = $JobReportId" );
      $HIST = "_HIST";
    }
    if ( !$dbr->eof() ) {
      my $Fields = $dbr->Fields();
      for (0..($Fields->Count()-1)) {
        my $k = $Fields->Item($_)->Name();
        my $v = $Fields->Item($_)->Value();
        if (  ref($v) ) {
           $v = [];
           push @$v, $Fields->Item($_)->Value()->Date('yyyy-MM-dd') if $Fields->Item($_)->Value()->can('Date');
           push @$v, $Fields->Item($_)->Value()->Time('HH:mm:ss') if $Fields->Item($_)->Value()->can('Time');
           push @$v, $Fields->Item($_)->Value() unless scalar(@$v);
           $v = join(' ', @$v);
        }
#        if (ref($v) && $Fields->Item($_)->Type() == 135) {
#          $v = Variant_DateTime($v);
#        }
#        $v = "" if !defined($v);
        $self->[0]->{$k} = $v if !exists($self->[0]->{$k});
      }
      setConfValues('db_HIST', $HIST);
    }
    else {
      croak "RESTARTABLE ApplicationError: JOBREPORT NOT FOUND JobReportId=$JobReportId\n";
    }
    $self->[0]->{rdbmsdbname} = $dbr->get_dbname();
    $dbr->Close();
  
    my $JobReportName = $self->[0]->{'JobReportName'} = uc($self->[0]->{'JobReportName'});


	my $INPATH = getConfValues('LocalPath')->{$self->getValues('LocalPathId_IN') || "L1"}."/IN";
#    die "Unsupported Input path on ($INPATH)" unless $INPATH =~ /file:\/\//i;
    $INPATH =~ s/file:\/\///;
    
    $self->setValues('$INPATH', $INPATH); 

    return if !$FULLMODE;
	
	$main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
    $dbr = $main::xrdbc->dbExecute(
      "SELECT * FROM tbl_JobReportNames a LEFT OUTER JOIN tbl_Os390PrintParameters b " .
      "ON a.JobReportName = b.JobReportName WHERE (a.JobReportName = '$JobReportName') "
    );
    if ( !$dbr->eof() ) {
      for (0..($dbr->Fields->Count()-1)) {
        my $k = $dbr->Fields->Item($_)->Name();
        my $v = $dbr->Fields->Item($_)->Value();
    if (  ref($v) ) {
      $v = [];
      push @$v, $dbr->Fields->Item($_)->Value()->Date('yyyy-MM-dd') if $dbr->Fields->Item($_)->Value()->can('Date');
      push @$v, $dbr->Fields->Item($_)->Value()->Time('HH:mm:ss') if $dbr->Fields->Item($_)->Value()->can('Time');
      push @$v, $dbr->Fields->Item($_)->Value() unless scalar(@$v);
      $v = join(' ', @$v);
    }
#        if (ref($v) && $dbr->Fields->Item($_)->Type() == 135) {
#          $v = Variant_DateTime($v);
#        }
#        $v = "" if !defined($v);
        $self->[0]->{$k} = $v if !exists($self->[0]->{$k});
      }
    }
    else {
      croak "UserError: Unable to get tbl_JobReportNames data for \"$JobReportName\"($JobReportId) ???\n";
    }
    $dbr->Close();
#    die "VARSETS: ", Dumper($self), "\n";
    $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
    $dbr = $main::xrdbc->dbExecute( "SELECT ElabOptions FROM tbl_JobReportsElabOptions WHERE JobReportId = $JobReportId " );
    if ( !$dbr->eof() ) {
      my $ElabOptions = $dbr->Fields()->Item(0)->Value();
      $self->[2] = {}; #XMLin($ElabOptions);
    }
    $dbr->Close();

    $self->setValues("to_delete", ""); 
    last if $self->VerifyJobReportValues();
  }

  my ($rformat, $xfrmode, $elabformat) = $self->getValues(qw(ReportFormat XferMode ElabFormat));

###mpezzi  $self->SetValues(ReportFormat => RF_EBCDIC) 
### il nome della routine inizia con minuscolo
  $self->setValues(ReportFormat => RF_EBCDIC) if ($rformat == RF_ASCII and $xfrmode == XF_PSF);

  $self->setValues(
      ElabFormat => $elabformat
    , FileRangeVar => "" 
    , HasIndexes => 0
    , isAfp => 0
    , isFullAfp => 0
    , ExistTypeMap => 0
    , MaybeTypeMap => 0
  )
  if $FULLMODE;
}

sub createSetList {
    
  my $self = shift;
  my $TableName = shift;
  my $JobReportId = $self->getValues('JobReportId');
  
  $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
  my $dbr = $main::xrdbc->dbExecute("SELECT * from $TableName where JobReportId = $JobReportId ");
  my $setList = []; 

  if ($dbr->eof()) {
    return $setList if $TableName ne 'tbl_JobReports';
    croak "ApplicationError: ERROR RETRIEVING DATA for JobReportId = <$JobReportId>" 
  }
    
  my $ch = $self->[3]; 
  my $Fields = $dbr->Fields(); 
    
  for (0..($Fields->Count()-1)) {
    my ($k, $t) = ($Fields->Item($_)->Name(), $Fields->Item($_)->Type());
      
    if (exists($ch->{$k}) and $k ne 'JobReportId') {
      my $v = ($ch->{$k} ne 'NULL') ? $ch->{$k} : 'NULL';
      if ($t == 135 or $t == 200) {
        $v =~ s/'//g;
        $v = "'$v'" if $v ne "NULL";
      }
      push @{$setList}, "$k = $v";
    }
  }
  $dbr->Close();
  i::logit("UPDATE setList for $TableName", join(", ", @{$setList}), "<<<") 
                                                             if $TableName eq 'tbl_JobReports';
  return $setList;
}


sub putFields2SQLFile {
  my $self = shift;
  my $JobReportId = $self->getValues('JobReportId');
  my $cur_work_dir = $self->getFileName('WORKDIR');
  my $ppsqlfn = "$cur_work_dir/\$\$POST.\$\$PROCESS\$\$.$JobReportId.sql";
  my $ppsqlfh;

  for my $TableName ( qw( tbl_JobReportsElabOptions tbl_JobReports ) ) {
    my $setList = $self->createSetList($TableName);   
    
    return 1 unless scalar(@{$setList});
    
    if ( !$ppsqlfh ) {
        $ppsqlfh = new FileHandle(">$ppsqlfn")
          || die "Unable to open Post Process SQL file - $! - detected for \"$ppsqlfn\"";
    }
    
    print $ppsqlfh "UPDATE $TableName set " . join(", ", @{$setList}) . " WHERE JobReportId = $JobReportId\n";
                                                                                              
  }
  print $ppsqlfh " INSERT INTO tbl_JobReportsPendingXfers (JobReportId, TimeRef, HostName, RemoteFileName) "
				." select  JobReportId, "
				." convert(varchar(12),JobExecutionTime,105) as TimeRef, " 
				." JobReportName as HostName , " 
				#." RemoteFileName as RemoteFileName"
				." left(RTRIM  (remotefilename) + '|' + convert(varchar(26),jobexecutiontime,121),255) as RemoteFileName" 
				." from tbl_JobReports "
				." where JobReportName not in ('CDAMFILE', 'W3SVC', 'XRRENAME') "
				." AND JobReportName  is not NULL  "
				." AND JobExecutionTime  is not NULL  "
				." AND RemoteFileName  is not NULL  "
				." AND JobReportId = $JobReportId \n";  #2018-02-28
				
  
  my ($JobExecutionTime, $JobReportName) = $self->getValues(qw(JobExecutionTime JobReportName)); 
  
  my $flagManageLastExecLogicalReports = (getConfValues('ManageLastExecLogicalReports') or 'N');
  if ($flagManageLastExecLogicalReports =~ /^[ys]$/i)
  { 
	  print $ppsqlfh " INSERT INTO tbl_LastExecLogicalReports ( JobReportName, FilterValue , textId , JobReportId, ReportId, JobExecutionTime ) "
					." SELECT  '$JobReportName' as JobReportName, lr.FilterValue , lrr.textId , lr.JobReportId, lr.ReportId, '$JobExecutionTime' as JobExecutionTime "
					." FROM  tbl_logicalReports lr "
					." ,tbl_LogicalReportsRefs lrr "
					." where lrr.JobReportId = lr.JobReportId "
					." and  lrr.ReportId = lr.ReportId "
					." and lrr.FilterVar = 'CUTVAR' "
					." and lr.JobReportId = $JobReportId "
					." and not exists(select 1 from  tbl_LastExecLogicalReports lelr "
					." where lelr.JobReportName = '$JobReportName' "
					." and lelr.FilterValue = lr.FilterValue "
					." and lelr.textId = lrr.textId "
					." ) \n";
	  print $ppsqlfh " update lelr "
					." set JobReportId = lr.JobReportId "
					." ,   ReportId =    lr.ReportId "
					." ,   JobExecutionTime = '$JobExecutionTime'   "
					." FROM tbl_logicalReports lr "
					." ,tbl_LogicalReportsRefs lrr "
					." ,tbl_LastExecLogicalReports lelr "
					." where lr.JobReportId = $JobReportId  "
					." and lrr.JobReportId = lr.JobReportId  "
					." and lrr.ReportId = lr.ReportId  "
					." and lrr.FilterVar = 'CUTVAR'  "
					." and lelr.JobReportName = '$JobReportName' "
					." and lelr.FilterValue = lr.FilterValue "
					." and lelr.textId = lrr.textId  "
					." and  "
					." ( "
					." (     '$JobExecutionTime' > lelr.JobExecutionTime) "
					." or ( ('$JobExecutionTime' = lelr.JobExecutionTime) and (lr.JobReportId > lelr.JobReportId) ) "
					." or ( ('$JobExecutionTime' = lelr.JobExecutionTime) and (lr.JobReportId = lelr.JobReportId) and (lr.ReportId > lelr.ReportId) ) "
					." ) \n";
					
  } 
	
  if ( $ppsqlfh ) { $ppsqlfh->close(); }
}


sub putFields {
  my $self = shift;
  my $JobReportId = $self->getValues('JobReportId');

  for my $TableName ( qw( tbl_JobReportsElabOptions tbl_JobReports ) ) {
    my $setList = $self->createSetList($TableName);   
    
    return 1 unless scalar(@{$setList});

    dbExecute( "UPDATE $TableName set " . join(", ", @{$setList}) . " WHERE JobReportId = $JobReportId " );
  }
  
  setConfValues(db_HIST => '');
}


sub getValues {
  my $self = shift; my ($r, $jrn, $prev, $set) = @$self; my @r;
  
  for (@_) {
    if ( exists($set->{$_}) ) {
      push @r, $set->{$_};
    }
    elsif ( exists($prev->{$_}) ) {
      push @r, $prev->{$_};
    }
    elsif ( exists($r->{$_}) ) {
      push @r, $r->{$_};
    }
    elsif ( exists($jrn->{$_}) ) {
      push @r, $jrn->{$_};
    } 
    else {
      print join(",", %{$self->[2]}), "\n";
      croak "ApplicationError: REPORT getValue INVALID FieldName \"$_\"\n" if $_ !~ /^\$/;
    }
    $main::veryverbose && i::logit("got $_ for ", join('::', (caller())[0,2]), " val: ", $r[-1]) if $_ =~ /localpath/i;
  }
  
  return wantarray ? @r : $r[0];
}

sub setValues {
  my $self = shift;
  my $set = $self->[3];
  while(@_) {
    my ($k, $v) = (shift, shift);
    $set->{$k} = $v;
    $main::veryverbose && i::logit("SETLIST - $k => $v - caller: ".join('::', (caller())[0,2])) if $k =~ /localpath/i;
  }
}

sub NewElabStart {
  my $self = shift; 

  my ($SrvName, $workdir) = getConfValues('SrvName', 'workdir'); 
  my $dbr; 
  
  my ($JobReportName, $JobReportId, $LocalFileName, $XferStartTime, $XferEndTime,  $descr, $userRef)  =
                                $self->getValues(qw(JobReportName JobReportId LocalFileName 
                                                    XferStartTime XferEndTime JobReportDescr UserRef));

  my $baseFileName = basename($LocalFileName);

  my $restarting_pdf = 0;

  for (glob("$workdir/$SrvName/*")) {
    next if !-f $_;
    if ( $_ !~ /\/\w+\.$JobReportId\..*\.pdf$/i ) {
      unlink "$_" || croak("UnlinkError: Unable to delete $_ $!") 
    }
    else {
      $restarting_pdf = 1;
    }
  }
  $self->setValues('restarting_pdf', $restarting_pdf);

  map { unlink $_ if $_ !~ /\.ps$/i || !$restarting_pdf } glob($self->getFileName('LOCALRES')."/*");

  $logger->AddFile( 'REPORTLOG', $self->getFileName('LOGFILE') );

  i::logit("ELAB for Report $JobReportName\/$JobReportId \"$baseFileName\" STARTED");
  print "ELAB for Report $JobReportName\/$JobReportId \"$baseFileName\" STARTED\n";
  
  dbExecute(
    "UPDATE tbl_JobReports SET SrvName='$SrvName', Status=".ST_INPROCESS.", ".
    "ElabStartTime=GETDATE(), ElabEndTime=NULL " .
    "where JobReportId = $JobReportId"
  );
  
  $self->deleteElab();
  
  $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
  
  #check if the report has been already loaded previously 
  $dbr = $main::xrdbc->dbExecute(""
	." select jrpx.JobReportId from  "
	." tbl_JobReportsPendingXfers jrpx,  tbl_JobReports jr "
	." where jr.RemoteFileName is not NULL  "
	." and jr.JobExecutionTime is not NULL  "
	." and jrpx.RemoteFileName = left(RTRIM (jr.RemoteFileName) + '|' + convert(varchar(26),jr.jobexecutiontime,121),255)  "
	." and jrpx.HostName = jr.JobReportName "
	." AND jrpx.TimeRef = convert(varchar(12),jr.JobExecutionTime,105)  "
	." and jr.JobReportName not in ('CDAMFILE', 'W3SVC', 'XRRENAME')  "
	." AND jr.JobReportId =   $JobReportId ");
  die("Violation of PRIMARY KEY constraint 'PK_tbl_JobReportsPendingXfers' "
	."- report already loaded with JobReportId[".$dbr->GetFieldsValues('JobReportId')."]") if (!$dbr->eof());
  
  $dbr = $main::xrdbc->dbExecute("SELECT dbo.XferDateDiff('$XferStartTime') As XferDateDiff");

  my $XferDateDiff = $dbr->GetFieldsValues('XferDateDiff') 
    or
  die("INVALID XferStartTime DETECTED. \"$XferStartTime\"");

  my $ijrar = $self->get_INPUT_ARCHIVE() ||
                       die "get_INPUT_ARCHIVE Failed";
  
  $userRef = ($descr && $descr ne '' ? $descr : $JobReportName) unless ($userRef && $userRef ne '');
     
  $self->setValues(
    SrvName => $SrvName,
    ElabStartTime => GetDateTime('SQL'), 
    XferDateDiff => $XferDateDiff, 
    ijrar => $ijrar, 
    ojrar => undef
  );

  my ( $LocalPathId_IN, $TargetLocalPathId_IN ) = 
      $self->getValues(qw(LocalPathId_IN TargetLocalPathId_IN)); $LocalPathId_IN ||= 'L1';

	  my $INPATH = getConfValues('LocalPath')->{$LocalPathId_IN};

	  my $TGTINPATH = getConfValues('LocalPath')->{$TargetLocalPathId_IN} || '';
#  die "Unsupported Input path on $LocalPathId_IN ($INPATH)" unless $INPATH =~ /file:\/\//i;
  
  if ( $TGTINPATH =~ /centera:\/\//i and $INPATH ne $TGTINPATH ) {
  #if ( substr($LocalPathId_IN,0,1) eq 'L' and $LocalPathId_IN ne $TargetLocalPathId_IN ) {
    $self->getValues('ijrar')->Close(); 
    $self->setValues('ijrar', undef); 

    require XReport::ARCHIVE::Util;
    XReport::ARCHIVE::Util::Migrate_INPUT($self, $TargetLocalPathId_IN);

    $self->setValues('LocalPathId_IN', $TargetLocalPathId_IN);
    dbExecute("
      update tbl_JobReports set LocalPathId_IN = '$TargetLocalPathId_IN'
      where jobreportid=$JobReportId
    ");

    $self->setValues('ijrar', $self->get_INPUT_ARCHIVE());
  }

  my $OUTPUT = gensym(); 
  my $INPUT = gensym(); 

  my $fileName = $self->getFileName('LOGPARSE');
  open($OUTPUT, ">$fileName") 
    or die("FileError: OPEN OUTPUT ERROR for LOGPARSE \"$fileName\" $!"); 

  $fileName = $self->getFileName('PARSE');
  open($INPUT, "<$fileName")
    or die("FileError: OPEN INPUT ERROR for PARSE \"$fileName\" $!"); 
	
  binmode($OUTPUT);
  binmode($INPUT);
  
  while(<$INPUT>) { print $OUTPUT $_; }
  close($INPUT);
  close($OUTPUT);
  
}

sub getReportDefs() {
  my ($self, @ReportNames) = @_; my $JobReportName = $self->getValues('JobReportName');

  @ReportNames = ($JobReportName) if !scalar(@ReportNames);
  warn "Retrieving report definitions for: ", join(', ', @ReportNames), "\n";
  #TODO aggiungere ReportDescr Column
  $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
  my $dbr = $main::xrdbc->dbExecute(
    "SELECT ReportName, ReportDescr, FilterVar, CheckFlag from tbl_ReportNames " .
    "where ReportName IN " .
    "( " . 
      join(",", map {"'$_'"} @ReportNames) .
    ") "
  );
  
  my $ReportDefs = {};
  warn "No definitions found for report name list\n" if $dbr->eof();
  while ( !$dbr->eof() ) {
    my $ReportName = $dbr->Fields->Item('ReportName')->Value();
    my $FilterVar = $dbr->Fields->Item('FilterVar')->Value();
    my $CheckFlag = $dbr->Fields->Item('CheckFlag')->Value();
    
    $ReportDefs->{$ReportName} = {
      FilterVar => $FilterVar, CheckFlag => $CheckFlag, CheckStatus => 0
    };

    $dbr->MoveNext();
  }

  return $ReportDefs;
}

sub NewFileRange {
  my $self = shift; my ($FileId, $FromValue, $ToValue, $TotPages) = @_; 

  my $JobReportId = $self->getValues('JobReportId');

  dbExecute("
    INSERT INTO tbl_JobReportsFileRanges
    (JobReportId, FileId, FromValue, ToValue, TotPages)
    VALUES ($JobReportId, $FileId, '$FromValue', '$ToValue', $TotPages)
  ");
}
  
sub FilterValue_IN_TABLE {
  my ($self, $FilterVar, $FilterValue, $AtPage) = @_; $FilterValue =~ s/'/''/g; $AtPage ||= 0;
  
  $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
  
  my $dbr = $main::xrdbc->dbExecute("
    SELECT * FROM tbl_VarSetsValues WHERE VarName = '$FilterVar' AND VarValue = '$FilterValue'
  ");

  return 1 if !$dbr->eof(); $dbr->Close(); 

  my ($JobReportName, $JobReportId) = $self->getValues(qw(JobReportName JobReportId));
  
  $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
  
  $dbr = $main::xrdbc->dbExecute("
    SELECT * FROM tbl_JobReportsInvalidFilterValues 
    WHERE JobReportName = '$JobReportName' AND  FilterVar='$FilterVar' AND FilterValue='$FilterValue'
  ");

  return $dbr->GetFieldsValues('IsAcceptable') if !$dbr->eof(); $dbr->Close();

  dbExecute("
    INSERT INTO tbl_JobReportsInvalidFilterValues
    (JobReportName, FilterVar, FilterValue, JobReportId, AtPage)
    VALUES ('$JobReportName', '$FilterVar', '$FilterValue', $JobReportId, $AtPage)
  ");
  
  return 0;
}

sub setDiscardPageList {
  my ($self, $discardList) = @_; my $JobReportId = $self->getValues('JobReportId'); 

  assert(@$discardList); my $ListOfPages = join(",", @$discardList); 

  dbExecute("
    INSERT INTO tbl_JobReportsDiscardedPages
    (JobReportId, ListOfPages) 
    VALUES ($JobReportId, '$ListOfPages')
  ");
}
  
sub NewPhysicalReport {
  my $self = shift; my ($ReportId, $TotPages, $ListOfPages) = @_;

  my ($JobReportName, $JobReportId) 
   = 
  $self->getValues(qw(JobReportName JobReportId));
#  warn "NewPR JRN: $JobReportName JRID: $JobReportId, RID: $ReportId, TOTP: $TotPages LOP: ", $$ListOfPages, "\n";
#  &$logrRtn("Creating New PhysicalReport entry for $JobReportId as requested by ". join('::', (caller())[0,2]));
# warn "Creating New PhysicalReport entry for $JobReportId as requested by ". join('::', (caller())[0,2]), "\n";
  $main::veryverbose && 
  i::logit("NewPR JRN: $JobReportName JRID: $JobReportId, RID: $ReportId, TOTP: $TotPages LOP: ", $ListOfPages,
                                   "as requested by ". join('::', (caller())[0,2]));
  dbExecute( 
    "INSERT INTO tbl_PhysicalReports (JobReportId, ReportId, TotPages, ListOfPages) " .
    "VALUES($JobReportId, $ReportId, $TotPages, '$$ListOfPages')"
  ); 
}

sub addPhysicalReports {
  my $self = shift;
  my $PRList = shift;
  while ( scalar(@{$PRList}) ) {
    my $PRentry = shift @{$PRList}; 
    my ($ReportId, $TotPages, $ListOfPages) = @{$PRentry}{qw(rid totp lop)};

    my ($JobReportName, $JobReportId) = $self->getValues(qw(JobReportName JobReportId));
    $main::veryverbose && 
    i::logit("AddPR JRN: $JobReportName JRID: $JobReportId, RID: $ReportId, TOTP: $TotPages LOP: ", $ListOfPages,
                                   "as requested by ". join('::', (caller())[0,2]));
# warn "Creating New PhysicalReport entry for $JobReportId as requested by ". join('::', (caller())[0,2]), "\n";
    $ListOfPages =~ s/^1,,//;
    dbExecute( 
        "INSERT INTO tbl_PhysicalReports (JobReportId, ReportId, TotPages, ListOfPages) "
        . "VALUES($JobReportId, $ReportId, $TotPages, '$ListOfPages')"
            ); 
  }
  return [];
}

sub addLogicalReportsTexts {
    my $self = shift;
    my $txts = shift;
    $main::veryverbose && i::warnit("addLogicalReportsTexts textstrings: ".Dumper($txts));
    while (scalar(@{$txts}) ) {
        my @strings = splice @{$txts}, 0, 50;
        $main::veryverbose && i::warnit("addLogicalReportsTexts arraystrings: ".Dumper(map {  "SELECT '".$_."' as textString" } @strings));
        my $SQL = "INSERT INTO dbo.tbl_LogicalReportsTexts (textString) "
          . 'SELECT * FROM ('. join(' UNION ALL ',  map { (my $str = $_) =~ s/'/''/g; "SELECT '$str' as textString" }  @strings ) . ') ts '
          . 'WHERE not exists(SELECT * from dbo.tbl_LogicalReportsTexts TX where TX.textString = ts.textString) '
#         . "\nGO\n"
          ;
        $main::veryverbose && i::logit("addLogicalReportsTexts SQL: $SQL");
        dbExecute($SQL);
    }
    return $txts;
}

sub addLogicalReports {
  my $self = shift; 
  my $LRList = shift; 
  while ( scalar(@{$LRList}) ) {
      my $LRentry = shift @{$LRList};
      my ($ReportName, $fV, $TotPages, $ReportId, $Progr, $dbReportDefs, $lrbundles, $FilterVar) = 
                                                 @{$LRentry}{qw(rn fv totp rid progr rdef bundles fn)};
      $main::veryverbose && i::logit("LRENTRY :".Dumper($LRentry));
#      my ($JobReportId, $UserTimeElab, $XferDateDiff, 
#          $XferRecipient, $XferStartTime, $userRef, $userTimeRef, $descr) = $self->getValues( 
#                              qw(JobReportId UserTimeElab XferDateDiff XferRecipient
#                                 XferStartTime UserRef UserTimeRef JobReportDescr));
      my ($JobReportId, $UserTimeElab, $XferDateDiff, 
          $XferRecipient, $XferStartTime, $userRefOLD, $userTimeRef, $descr) = $self->getValues( 
                              qw(JobReportId UserTimeElab XferDateDiff XferRecipient
                                 XferStartTime UserRef UserTimeRef JobReportDescr));
      my ($CheckFlag, $CheckStatus) = @{$dbReportDefs->{$ReportName}}{qw(CheckFlag CheckStatus)};
    
      $UserTimeElab = ($UserTimeElab) ? "'$UserTimeElab'" : "NULL";
      $userTimeRef = ($userTimeRef && $userTimeRef ne 'NULL' ? "'$userTimeRef'" : "'$XferStartTime'");
      my ($FilterValue, $userRef) = split /\|/, $fV, 2;
      $userRef = ($descr && $descr ne '' ? $descr : $ReportName) unless ($userRef && $userRef ne '');
      
      $CheckStatus ||= 0;
#  warn "NLR ENV:", Dumper(\{ReportName => $ReportName, Descr => $descr, UserRef => $userRef}), "\n";

### mpezzi  $CheckStatus = 1 if ($CheckFlag == 1 && ($FilterVar eq '')==($FilterValue eq ''));
### assegna sempre un valore ( 1 o 0) 
      $CheckStatus = (($CheckFlag == 1 && ($FilterVar eq '')==($FilterValue eq '')) ? 1 : 0);
  
=tbl_LogicalReportsRefs
this insert assumes that the textStrings have been already inserted 
(addLogicalReportsTexts called before NewLogicaReport) 
=cut  
      (my $fv = $FilterValue) =~ s/'/''/g;
      (my $escUserRef = $userRef) =~ s/'/''/g;
      my $sqlstring = '' # "BEGIN TRANSACTION\n" 
        . "INSERT INTO tbl_LogicalReports "
        . "(ReportName, FilterValue, TotPages, CheckStatus, JobReportId, ReportId, XferDateDiff, XferRecipient) "
        . " VALUES "
        . "('$ReportName', '$fv', $TotPages, $CheckStatus, $JobReportId, $ReportId, $XferDateDiff, '$XferRecipient')"
        . "\n"
        . "INSERT INTO tbl_LogicalReportsRefs ( JobReportId, ReportId, UserTimeRef, FilterVar, TextId) "
        . " VALUES ( $JobReportId, $ReportId, $userTimeRef, '$FilterVar', "
        . " (SELECT textid FROM dbo.tbl_LogicalReportsTexts where textString = '$escUserRef')"
        . " )";

      $main::veryverbose && i::logit("INSERT LRR: $sqlstring");
      dbExecute( $sqlstring  );      
      my $bundlesPackage = $self->getValues('bundlesPackage');
      if ( $bundlesPackage ne '' && ( my $bundleProc = $bundlesPackage->can('NewLogicalReport') ) ) {
        my $bpparms = {};
        @{$bpparms}{qw(ReportName FilterValue TotPages CheckStatus 
                    JobReportId ReportId XferDateDiff XferRecipient
                    UserRef UserTimeRef FilterVar bundles)} = ($ReportName, $FilterValue, $TotPages, $CheckStatus,
                                                              $JobReportId, $ReportId, $XferDateDiff, $XferRecipient,
                                                              $userRef, $userTimeRef, $FilterVar, $lrbundles );
        &{$bundleProc}($bundlesPackage, %{$bpparms});
      }
      elsif ($bundlesPackage ne '') { 
        i::logit("BP: $bundlesPackage unable to handle \"NewLogicalReport\""); 
      }
  }
  return [];
}

sub LoadEmailTable {
  my ($self, $FileName, $TotEmails) = (shift, shift, shift); my $JobReportId = $self->getValues('JobReportId');
  
  &$logrRtn("LOADING EMAIL_PENDING_QUEUE TABLE BEGIN");

  for (1..10) {
    eval { dbExecute("BULK INSERT tbl_EMailPendingQueue FROM '$FileName' WITH ( FIRSTROW = 2) "); };
    last if !$@ || $@ !~ /could not be read/i;
    &$logrRtn("RETRYING BULK INSERT ERROR: $@"); sleep(60);
  } 
  die $@ if $@;

  dbExecute("
    INSERT INTO tbl_EmailJobReportsStat (JobReportId, TotEmails, SentEmails, PendingEmails)
    VALUES ($JobReportId, $TotEmails, 0, $TotEmails)
  ");

  &$logrRtn("LOADING EMAIL_PENDING_QUEUE TABLE END");

  $self->setValues('HasEMails', '1');
}

sub LoadIndexTable_BULK {
  my ($dbc, $FileName, $TableName, $sqlserv) = (shift, shift, shift, shift); 
  $FileName =~ s/\\/\//g;

  for (1..10) {
    eval { 
      $dbc->dbExecute("
       BULK INSERT $TableName FROM '$FileName' 
        WITH 
       (FIRSTROW = 2, ROWS_PER_BATCH = 300) 
     ");
    };
    last if !$@ || ($@ !~ /could not be read/ && $@ !~ /Command error/);
    &$logrRtn("HOOPS!? NOW RETRYING BULK INSERT ERROR: $@"); 
    sleep(60);
  }
  die $@ if $@;
  
}

sub addrows2tbl { 
  my ($dbc, $tbl, $columns, $rows) = (shift, shift, shift, shift);
  my $cmd = "INSERT INTO $tbl ([". join('], [', @$columns) ."]) " ."\n" . join("\n UNION ALL ", @$rows) . "\n";
  &$logrRtn("Starting to insert " . scalar(@$rows) . " rows into $tbl");
  $dbc->dbExecute($cmd);
}

sub LoadIndexTable_SQL {
  my ($dbc, $FileName, $TableName, $sqlserv) = (shift, shift, shift, shift); 
  $FileName =~ s/\\/\//g;

#  print "DBC: ", ref($dbc), " FN: $FileName, TN: $TableName, SERV: $sqlserv\n";
  my $ixfh = new FileHandle("<$FileName") or die "Unable to open Index File \"$FileName\" to load into $TableName";
  
  my (@tblcols, @rows) = ((), ());
  while ( <$ixfh> ) {
    chomp;
    $_ =~ s/'/''/g;
    @tblcols = split(/\t/, $_), next if (!scalar(@tblcols));
    
    push @rows, "SELECT '"  . join("', '",  split /\t/) . "'";
    addrows2tbl($dbc, "$TableName", \@tblcols, \@rows), @rows = () if scalar(@rows) > 299; 
  }
  addrows2tbl($dbc, "$TableName", \@tblcols, \@rows) if scalar(@rows);
  
  $ixfh->close();
}

sub LoadIndexTable_BCP {
  my ($dbc, $FileName, $TableName, $sqlserv) = (shift, shift, shift, shift); 
  $FileName =~ s/\//\\/g;

    # mpezzi BULK copy gives access denied - will fix it later
    my @bcp = ("bcp", "$TableName", "in", $FileName, 
           '-F', '2', '-b', '1000', '-c', '-S', $sqlserv, '-T');
    print "Calling: ", join(' ', @bcp), "\n";
    eval {
      my $rc = system(@bcp); 
      die "Command ended with error ($rc) - $! - $@\n" unless $rc == 0; 
    };
    die $@ if $@;
}
  
sub LoadIndexTable {
  my ($self, $IndexName, $FileName, $TotEntries) = @_; my $dbc; $TotEntries ||= 0;
#  print "FN: $FileName\n";
  my ($JobReportId, $userTimeRef) = $self->getValues(qw(JobReportId UserTimeRef));
  
  $userTimeRef = $self->getValues("XferStartTime") if !$userTimeRef || $userTimeRef eq "NULL";
  my $loadindexmode = $XReport::cfg->{loadindexmode} || 'SQL';
  
  &$logrRtn("LOADING INDEX TABLE $IndexName/$JobReportId BEGIN using $loadindexmode");
  
  $dbc = XReport::DBUtil->get_ix_dbc($IndexName);
  
  #stay on the safe side ------------------------------------------------------//
  $dbc->dbExecute( "DELETE from [tbl_IDX_$IndexName] WHERE JobReportId = $JobReportId ");
  
  dbExecute(
        "INSERT INTO tbl_JobReportsIndexTables (IndexName, JobReportId, UserTimeRef, TotEntries)
    VALUES ('$IndexName', $JobReportId, '$userTimeRef', $TotEntries)
   "
       );
  #----------------------------------------------------------------------------//

  my ($sqlserv, $dbname) = $dbc->{dbc}->ConnectionString() =~ /^.*Data\sSource=([\w\\]+);.*(?:DATABASE|Initial Catalog)=([\w\_]+);.*$/i;
  
#  print "FN 2: $FileName\n";
  eval "LoadIndexTable_$loadindexmode(\$dbc, \$FileName, \"$dbname..[tbl_IDX_$IndexName]\", \"$sqlserv\");";
  die $@ if $@;
  &$logrRtn("LOADING INDEX TABLE $IndexName/$JobReportId END");
  
  $self->setValues('HasIndexes', '1');
}

use XReport::BTree::OUTPUT;
use Data::Alias;

sub loadPageBtree {
    my $self = shift;
    my $pofile = $self->getFileName('IXFILE', '$$PageOffsets$$');
    return undef unless -e $pofile;
    my $JobReportId = $self->getValues(qw(JobReportId));
    
    my $potsv = IO::File->new("<$pofile");
    &$logrRtn("INIT CREATING BTREE METADATA");
    my $btree = XReport::BTree::OUTPUT->new(
      # from_page from_line from_offset
  
      btree_def => {
          JobReportId  => $JobReportId,
          id           => 2,
        name         => 'output_pages', 
        key_lengths  => [4, 6, 6, 2], 
        entry_length => 18, 
        column_names => [ qw(from_page from_line from_offset for_lines) ],
      },
    );
    my $page_offsets = [];
    while (<$potsv>) {
        chomp;
        next if $_ =~ /^from_page/;
        my $values = [split /\t/, $_ ];
 #       warn "Page Offset entry: ", join('::', @{$values}), "\n";
        push @$page_offsets, $values if scalar(@{$values});
        if (@$page_offsets >= 100) {
#          warn "at_page => ", join(",", @{$page_offsets->[-1]}), "\n";
          $btree->add_leaf_entries($page_offsets); 
          $page_offsets = [];
        }
    }
    $btree->add_leaf_entries($page_offsets) if scalar(@{$page_offsets});
    my ($status, $btree_meta_data) = $btree->close();
    $btree_meta_data = unpack("H*", compress($btree_meta_data));
    &$logrRtn("Inserting btree metadata type \"2\" into DB");
#    dbExecute("
#      update tbl_JobReportsBTrees set btree_meta_data = 0x$btree_meta_data where JobReportId = $JobReportId and btree_id = 2
#    ");
    dbExecute("
      INSERT INTO tbl_JobReportsBTrees (btree_id, JobReportId, btree_meta_data, btree_io, btree_name) values(2, $JobReportId, 0x$btree_meta_data, '', 'input_pages')
    ");
    &$logrRtn("Inserted btree metadata type \"2\" into DB");
    
}

sub loadBlockBtree {
    my $self = shift;
    my ($JobReportId, $INPUT) = (shift, shift);
    warn "LoadBlockBTree INPUT:", ref($INPUT), "\n";
    &$logrRtn("Starting LoadBlockBTree");
    my $btree = XReport::BTree::OUTPUT->new(
      # from_page from_line from_offset
  
      btree_def => {
          JobReportId  => $JobReportId,
          id           => 1,
        name         => 'input_blocks',
        entry_length => 58, 
        key_lengths => [6, 6, 6],
        column_names => [ qw(from_offset from_line to_gz_offset sha1_hex_block) ],
      },
    );
    my $block_offsets = [];
    foreach my $values (@{$INPUT->{block_offsets}}) {
        next unless scalar(@{$values});
        next if $values->[0] =~ /^from_offset/;
#        warn "Block Offset entry: ", join('::', @$values), "\n";
        push @$block_offsets, [ @{$values}[0..$#{$btree->{btree_def}->{column_names}}] ];
        if (@$block_offsets >= 100) {
 #         warn "at_page => ", join(",", @{$block_offsets->[-1]}), "\n";
          $btree->add_leaf_entries($block_offsets); 
          $block_offsets = [];
        }
    }
    $btree->add_leaf_entries($block_offsets) if scalar(@{$block_offsets});
    my ($status, $btree_meta_data) = $btree->close();
    warn "inserting input_blocks btree_meta_data for $JobReportId\n";
    &$logrRtn("Inserting btree metadata type \"1\" into DB");
    $btree_meta_data = unpack("H*", compress($btree_meta_data));
#    dbExecute("
#      update tbl_JobReportsBTrees set btree_meta_data = 0x$btree_meta_data where JobReportId = $JobReportId and btree_id = 1
#    ");
    dbExecute("
      INSERT INTO tbl_JobReportsBTrees (btree_id, JobReportId, btree_meta_data, btree_io, btree_name) values(1, $JobReportId, 0x$btree_meta_data, '', 'input_blocks')
    ");
    &$logrRtn("Inserted btree metadata type \"1\" into DB");
}

sub LoadExcelSheets {
  my ($self, $ExcelSheets) = (shift, shift); require Data::Dumper; my $OUTPUT = gensym();
     
  my $ExcelFileName = $self->getFileName('Excel');

  my $jobFile = $self->getFileName('JOB'); 

  open($OUTPUT, ">$jobFile");

  $ExcelSheets = Data::Dumper::Dumper( $ExcelSheets );
  $ExcelSheets =~ s/^[^\[]+//; $ExcelSheets =~ s/;\s+$//;

  print $OUTPUT "
   use lib(\"\$ENV{'XREPORT_HOME'}/perllib\");
   use XReport::Excel::Util;

   XReport::Excel::Util::TsvFile2Excel(
    ExcelSheets => $ExcelSheets,
    ExcelFileName => '$ExcelFileName'
   );
  ";
  close($OUTPUT);
  system("perl $jobFile");
      
  my ($JobReportId, $ExistTypeMap) 
    = 
  $self->getValues(qw(JobReportId ExistTypeMap));
      
  $self->setValues('ExistTypeMap', $ExistTypeMap||2);
}

sub LoadExcelSheets_Leak {
  my ($self, $ExcelSheets) = (shift, shift); require XReport::Excel::Util;
     
  my $ExcelFileName = $self->getFileName('Excel');

  XReport::Excel::Util::TsvFile2Excel(
    ExcelSheets => $ExcelSheets,
    ExcelFileName => $ExcelFileName
  );
      
  my ($JobReportId, $ExistTypeMap) 
    = 
  $self->getValues(qw(JobReportId ExistTypeMap));
      
  $self->setValues('ExistTypeMap', $ExistTypeMap||2);
}

sub cre_OUTPUT_ARCHIVE {
  my $self = shift; 
  require XReport::ARCHIVE;


  my ($SrvName, $workdir) = getConfValues('SrvName', 'workdir'); 
  my ($JobReportName, $JobReportId) = $self->getValues(qw(JobReportName JobReportId));
  my ( $LocalPathId_OUT, $LocalFileName ) = $self->getValues(qw(LocalPathId_OUT LocalFileName));

#  $LocalPathId_OUT = "L1" if $LocalPathId_OUT !~ /^(?:|L.*)$/i;
  
#  my $LocalPath = getConfValues('LocalPath')->{$LocalPathId_OUT || "L1"};
#  die "Unsupported Output path on $LocalPathId_OUT ($LocalPath)" unless $LocalPath =~ /file:\/\//i;
#  $LocalPath =~ s/file:\/\///i; 
#
#  $LocalFileName = "$LocalPath/OUT/".$self->getFileName('ZIPOUT');

  my $ar = XReport::ARCHIVE::cre_OUTPUT_ARCHIVE($self, @_);
  i::logit("INIT CREATING OUTPUT Archive \"$LocalFileName\" using ".ref($ar) );

  my @fileList = glob("$workdir/$SrvName/*\.$JobReportId\.*");
  for ( @fileList ) {
    #todo: test not to compress big or pdf files and from config
    if ($_ !~ /.*ps$/i and $_ !~ /^$JobReportName\.$JobReportId\.0\.#[^1-9]\d*\.log$/i) {
      my ($infn, $zipfn) = ($_, basename($_));
      $ar->AddFile($infn, $zipfn); 
    }
  }
  $ar->Close();

  i::logit("END CREATING OUTPUT Archive");

  my $fsz = -s $LocalFileName;
  die "Creation of Output archive \"$LocalFileName\" failed - no valid size" unless $fsz;
  dbExecute("
   UPDATE tbl_JobReports set ElabOutBytes = $fsz 
    WHERE JobReportId = $JobReportId
  ");
  ### todo: copy to tar lib
}

sub SendMail {
  my ($MailSubject, $MailTo, $MailMessage) = (shift, shift, shift);

  my ($MailFromUser, $MailServer) = getConfValues('MailFromUser', 'MailServer');

  my $MailDate = strftime("%a %b %e %H:%M:%S %Y",localtime());

  require Net::SMTP;
  
  my $smtp = Net::SMTP->new($MailServer);

  my @MailMessage = (
     "From: $MailFromUser\n"
    ,"To: <" . join(',', split(/\n/, $MailTo)) .  ">\n"
    ,"Subject: $MailSubject\n"
    ,"Date: $MailDate\n"
    ,"\n"
    ,"$MailMessage"
  );

  return (0, "UNABLE TO CONNECT TO SERVER")  if !$smtp;

  return (0, "MAIL code=".$smtp->code())     if !$smtp->mail($MailFromUser);
  return (0, "TO code=".$smtp->code())       if !$smtp->to(split(/\n/, $MailTo));
  return (0, "DATA code=".$smtp->code())     if !$smtp->data();
  
  for my $MailMessage (@MailMessage) { 
    if ( !$smtp->datasend($MailMessage) ) {
      return (0, "DATASEND code=".$smtp->code());
    }
  }
  return (0, "DATAEND code=".$smtp->code())  if !$smtp->dataend();
  return (0, "DATA code=".$smtp->code())     if !$smtp->quit();
  
  return (1, "COMPLETED code=".$smtp->code());
}

sub SendElabEndNotifications {
  my $self = shift; my $MailingStatusMessage; my @recipients = ();
  my $subj2send = shift;
  my $msg2send = shift;
  my ($MailTo, $MailingStatus) = $self->getValues('MailTo', 'MailingStatus');

  if ( !$MailTo ) { 
    $MailingStatus = 1;
    $self->setValues("MailingStatus", 1);
    return;
  }
#  return if $MailingStatus == 1;

  my ($JobReportName, $JobReportDescr, $InputPages, $userTimeRef, $UserTimeElab)
   =
  $self->getValues(qw(JobReportName JobReportDescr InputPages UserTimeRef UserTimeElab));

  &$logrRtn("MAIL: Preparing end notification to be send to $MailTo");
  
  foreach my $recAddr( split(/\,/, $MailTo ) ) {
    if ( my ($profile) = $recAddr =~ /^\x21\s*(.*\S)\s*$/ ) {

      &$logrRtn("MAIL: Retrieving Addr for $profile");
      
      eval {
	    $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
        my $dbr = $main::xrdbc->dbExecute("
          SELECT dbo.tbl_Users.EMailAddr 
          FROM   
            dbo.tbl_UsersProfiles INNER JOIN  
            dbo.tbl_Users ON dbo.tbl_UsersProfiles.UserName = dbo.tbl_Users.UserName 
          WHERE (dbo.tbl_UsersProfiles.ProfileName = '$profile')
        ");
        while (!$dbr->eof()) {
          my $EMailAddr = $dbr->Fields()->Item('EMailAddr')->Value();
          push @recipients, $EMailAddr if $EMailAddr;
          &$logrRtn("MAIL: Notification will be sent to $EMailAddr") if $EMailAddr;
          $dbr->MoveNext();
        }
      }
    } elsif (my ($EMailAddr) = $recAddr =~ /^mailto:(.*)\s*$/) {
      push @recipients, $EMailAddr if $EMailAddr;
      &$logrRtn("MAIL: Notification will be sent to $EMailAddr") if $EMailAddr;
    } 
    else {
      eval {
	  $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
      my $dbr = $main::xrdbc->dbExecute("
          SELECT dbo.tbl_Users.EMailAddr 
          FROM   
            dbo.tbl_Users
          WHERE (dbo.tbl_Users.UserName = '$recAddr')
        ");
    while (!$dbr->eof()) {
      my $EMailAddr = $dbr->Fields()->Item('EMailAddr')->Value();
      push @recipients, $EMailAddr if $EMailAddr;
      &$logrRtn("MAIL: Notification will be sent to $EMailAddr") if $EMailAddr;
      $dbr->MoveNext();
    }
  }
    }
  }

  ($MailingStatus, $MailingStatusMessage) = 
   SendMail(
     "REPORT \"$JobReportName/$JobReportDescr\" $userTimeRef disponibile per la visualizzazione",
     join("\n", @recipients),
     "Il Report $JobReportName - \"$JobReportDescr\"\n"
    ." di pagine: $InputPages\n"
    ." TimeRef: $userTimeRef TimeElab: $UserTimeElab\n"
    ."e' ora disponible per la visualizzazione da ambiente CEREPORT.\n"
   )
  ;
  &$logrRtn("SEND MAIL to $MailTo ENDED rc=$MailingStatus msg=$MailingStatusMessage");
  
  $self->setValues("MailingStatus", $MailingStatus);
}

sub LogElabInfo {
  my $self = shift; 
  my $JobReportId = $self->getValues('JobReportId');
  
  my %ElabOptions = (%{$self->[0]}, %{$self->[1]}, %{$self->[2]}, %{$self->[3]});
  #for ( (grep(/^\$/, keys(%ElabOptions)), qw(OS400attrs cinfo PSPageExit ElabContext AfpResources) ) ) {
  for ( (grep(/^\$/, keys(%ElabOptions)), qw(OS400attrs cinfo PSPageExit PageMKExcel ElabContext AfpResources) ) ) {
    delete $ElabOptions{$_};
  }

#  delete $ElabOptions{OS400attrs};
#  delete $ElabOptions{PSPageEXit};
#  my $ElabOptions = XMLout(\%ElabOptions, rootname => 'ElabOptions'); $ElabOptions =~ s/'/''/g;
  my $ElabOptions = join("'\n+'", 
                    map { $_ =~ s/'/''/g; $_ } unpack('(a150)*', XMLout(\%ElabOptions, rootname => 'ElabOptions') ));

  $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
  my $dbr = $main::xrdbc->dbExecute("SELECT * from tbl_JobReportsElabOptions WHERE JobReportId = $JobReportId");
  if ($dbr->eof()) {
    dbExecute("INSERT INTO tbl_JobReportsElabOptions (JobReportId, ElabOptions) "
                                                        . "VALUES($JobReportId, ''\n+'$ElabOptions')" );
  }
  else {
    $dbr->Close();
    dbExecute( "UPDATE tbl_JobReportsElabOptions set ElabOptions=''\n+'$ElabOptions' "
                                                    . "WHERE JobReportId = $JobReportId" );
  }
  
  my ($OUTPUT, $fileName);
  
  $OUTPUT = gensym(); $fileName = $self->getFileName('LOGTABLES');
  open($OUTPUT, ">$fileName") 
    or die("FileError: OPEN OUTPUT ERROR for LOGTABLES \"$fileName\" $!"); 
  binmode($OUTPUT);
  
  print $OUTPUT "<?xml version=\"1.0\"?>\n";
  print $OUTPUT "<LOGTABLES>\n";
  for my $tblName (qw(
    tbl_JobReports 
    tbl_PhysicalReports 
    tbl_LogicalReports 
    tbl_JobReportsElabOptions
  )) {
    $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
    my $dbr = $main::xrdbc->dbExecute( "SELECT * from $tblName where JobReportId = $JobReportId" );
    print $OUTPUT SQL2XML("$tblName", $dbr), "\n"; $dbr->Close();
  }
  print $OUTPUT "</LOGTABLES>\n";
  
  close($OUTPUT);

}

sub NewElabEnd {
  my ($self, $Status) = @_; 
  
  my ($JobReportName, $JobReportId, $LocalFileName) =
                    $self->getValues('JobReportName', 'JobReportId', 'LocalFileName');

  my $baseFileName = basename($LocalFileName);

  if ($Status == ST_COMPLETED) {
    $self->SendElabEndNotifications();
    $self->loadPageBtree(); 
    $self->setValues(ViewLevel=>1);
  }

  my ($ijrar, $ojrar) = $self->getValues(qw(ijrar ojrar));  
  $ijrar->Close() if $ijrar; 
  $ojrar->Close() if $ojrar; 

  $self->setValues(ijrar => '', ojrar => '');

  $self->LogElabInfo()  if ($Status == ST_COMPLETED);
  
  i::logit("ELAB for Report $JobReportName\/$JobReportId Status=$Status ENDED");
  print "ELAB for Report $JobReportName\/$JobReportId Status=$Status ENDED\n";

  $logger->RemoveFile('REPORTLOG') if $logger && ref($logger); 

#  my $createArchive = getConfValues('createoutputarchive');
#  $createArchive = 1 unless defined($createArchive);
#  if ( $createArchive ) { 
#    $self->cre_OUTPUT_ARCHIVE();
#    $self->setValues( Status => $Status, ElabEndTime => GetDateTime('SQL') );
#  }

  my ($SrvName, $workdir) = getConfValues('SrvName', 'workdir');

  #$self->putFields();
  setConfValues(GHOST => '');
  
}

sub doSingleParserElab {
    
  my $self = shift; 
  my ($JobReportName, $JobReportId) = $self->getValues('JobReportName', 'JobReportId');
  
  $self->setValues('ElabFull' => 1, ElabContext => {});
  my @evalList = ();
  my $ElabFormat = $self->getValues('ElabFormat');
  my $ret_status = ST_COMPLETED;
  my $bundlesPackage;
  eval "\$bundlesPackage = require XReport::BUNDLE;";
  $bundlesPackage = '' unless $bundlesPackage;
  $bundlesPackage = 'XReport::BUNDLE' if $bundlesPackage == 1;
  warn "BP REQUIRE: $bundlesPackage\n";
  $self->setValues(bundlesPackage => $bundlesPackage);

  my $formatter = undef;
  my $EndingMessage;
  if ( $ElabFormat == EF_AFPRESOURCES ) {
       &$logrRtn("BEGINNING CONVERSION OF AFP RESOURCES JobReportName=$JobReportName/$JobReportId");
       $self->NewElabStart('AFPRESOURCES_TYPE');

       require XReport::AFP::CONVERT;
       push @evalList, 'XReport::AFP::CONVERT';

       &$logrRtn("LocalFile: ".$self->getValues('LocalFileName'));
       eval {
          XReport::AFP::CONVERT::IMPORT_FILE(
             AfpSubSysId => 1,
             FileId => $self,
             UPD_MODE => 2,
             VERBOSE => 1
           );
       };
       $EndingMessage = $@;
       &$logrRtn("ENDING CONVERSION OF AFP RESOURCES JobReportName=$JobReportName/$JobReportId");
  }
  else {
      &$logrRtn("BEGINNING XReport::Parser Processing for $JobReportName/$JobReportId");
      $self->NewElabStart('USER_TYPE');
      $main::verbose and i::warnit("Loading XReport::Parser");
      require XReport::Parser;
      $formatter = XReport::Parser->new( $self );
      eval {
         $formatter->doElab(); 
         $formatter->Close(); 
       }; #END EVAL
       $EndingMessage = $@;
       &$logrRtn("ENDING XReport::Parser Processing for $JobReportName/$JobReportId");
  }
  $ret_status = ( $EndingMessage ? ST_PROCERROR : ST_COMPLETED );  
  i::logit("ERROR CATCHED : $EndingMessage") if $EndingMessage;

  $self->NewElabEnd( $ret_status );
  $self->putFields2SQLFile();
  $formatter->TERMINATE() if( $formatter && $formatter->can('TERMINATE') );    
  croak("UNRECOVERABLE ERROR: TERMINATING NOW !! - MSG: $EndingMessage") if $EndingMessage;
  return $ret_status;
}


sub doElab {
  my $self = shift; my ($JobReportName, $JobReportId) = $self->getValues('JobReportName', 'JobReportId');
  
  $self->setValues('ElabFull' => 1, ElabContext => {}); my @evalList = ();

  ### elab jobreport -----------------------------------------------
  ### test if to move to different filepath
  ### or test todo table

  my $ElabFormat = $self->getValues('ElabFormat');
  my $bundlesPackage;
  eval "\$bundlesPackage = require XReport::BUNDLE;";
  $bundlesPackage = '' unless $bundlesPackage;
  $bundlesPackage = 'XReport::BUNDLE' if $bundlesPackage == 1;
  warn "BP REQUIRE: $bundlesPackage\n";
  $self->setValues(bundlesPackage => $bundlesPackage);
  ### elab jobreport -----------------------------------------------
  eval { 

    if ( $ElabFormat == EF_AFPRESOURCES ) {
      $self->NewElabStart('AFPRESOURCES_TYPE'); require XReport::AFP::CONVERT;

      &$logrRtn("BEGINNING CONVERSION OF AFP RESOURCES JobReportName=$JobReportName/$JobReportId");

      &$logrRtn("LocalFile: ".$self->getValues('LocalFileName'));
      require XReport::AFP::CONVERT;
      push @evalList, 'XReport::AFP::CONVERT';

      XReport::AFP::CONVERT::IMPORT_FILE(
        AfpSubSysId => 1,
        FileId => $self,
        UPD_MODE => 2,
        VERBOSE => 1
      );

      &$logrRtn("ENDING CONVERSION OF AFP RESOURCES JobReportName=$JobReportName/$JobReportId");
    }

    elsif ( $ElabFormat == EF_BUNDLE && $bundlesPackage ne '') {
      $self->NewElabStart('BUNDLE_TYPE');

      my $bundler = $bundlesPackage->new(
                     ###mpezzi       $JobReportName, $self
                     ### new di BUNDLE vuole come primo parametro il JobReport HANDLE
                     $self, $JobReportName
      );

      push @evalList, $bundler;

      $bundler->Create(); $bundler->Close();
    }

    elsif ( $ElabFormat <= MAX_USER_ELAB_FORMAT ) {
      $self->NewElabStart('USER_TYPE');
	  
      if ( $self->getValues('to_delete') eq "YES INDEED" ) {
        dbExecute(
         "UPDATE tbl_JobReports set OrigJobReportName = '$JobReportName', PendingOp = 12 " .
         "where JobReportId = $JobReportId "
        );
        $self->NewElabEnd( ST_PROCERROR ); 
        $self->cre_OUTPUT_ARCHIVE();
        $self->setValues( Status => ST_PROCERROR, ElabEndTime => GetDateTime('SQL') );
        $self->putFields();
        return;
      }
      my $FormatterClass = $FormatterClasses{$ElabFormat};
      $main::verbose and i::warnit("Loading $FormatterClass");
      eval "require $FormatterClass;";
      $main::verbose and i::warnit("Error catched during require of $FormatterClass: $@") if $@;
      
      my $formatter = $FormatterClass->new( $self );
      $main::verbose and i::warnit("Formatter class is ".ref($formatter));

      push @evalList, $formatter; 

      $formatter->doElab(); $formatter->Close(); $formatter = undef;
    }

    ### todo: manage for other formats
    ### tbl_elabformats -> perl to include
    else {
      die "UserError: INVALID ElabFormat \"$ElabFormat\" REQUESTED";
    }

  };
  
  my $ret_status = ST_COMPLETED;
  ### signal completion state --------------------------------------
  if ( my $EndingMessage = $@ ) {
    i::logit("ERROR CATCHED : $EndingMessage");

    $self->NewElabEnd( ST_PROCERROR );
    $self->cre_OUTPUT_ARCHIVE();
    $self->setValues( Status => ST_PROCERROR, ElabEndTime => GetDateTime('SQL') );
    $self->putFields();

    eval { 
      for (@evalList) { $_->TERMINATE(); } 
      @evalList = (); 
    }; 

	my ($dieonerror) = getConfValues('dieonerror');
#    if ( $EndingMessage =~ /User *Error|Restartable/i ) {
    if ( !$dieonerror && $EndingMessage =~ /User *Error|Restartable/i ) {
      i::logit("RECOVERABLE ERROR DETECTED");
    }
    else {
        # &$logrRtn("UNRECOVERABLE ERROR DETECTED");
        #carp "UNRECOVERABLE ERROR DETECTED";
      croak("UNRECOVERABLE ERROR: TERMINATING NOW !!");
    }
    $ret_status = ST_PROCERROR;
  }
  else {
    $self->NewElabEnd( ST_COMPLETED );
    $self->cre_OUTPUT_ARCHIVE();
    $self->setValues( Status => ST_COMPLETED, ElabEndTime => GetDateTime('SQL') );
    $self->putFields();
  };

  @$self = ();
  return $ret_status;
}

sub deleteElab {
  my $self = shift; my ($JobReportName, $JobReportId) = $self->getValues('JobReportName', 'JobReportId');

  my $baseFileName = basename($self->getValues('LocalFileName')); 
  
  &$logrRtn( "Deleting REPORT links for $JobReportName\/$JobReportId \"$baseFileName\"" );
  
  $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
  my $dbr = $main::xrdbc->dbExecute( "SELECT IndexName from tbl_JobReportsIndexTables where JobReportId = $JobReportId " );
  if ( !$dbr->eof() ) {
    while( !$dbr->eof() ) { 
      my $IndexName = $dbr->Fields()->Item("IndexName")->Value();
      &$logrRtn("DELETING INDEX ENTRIES from \"$IndexName\" ");
      my $dbc = XReport::DBUtil->get_ix_dbc($IndexName);
      $dbc->dbExecute( "DELETE from [tbl_IDX_$IndexName] where JobReportId = $JobReportId" );
    } continue {
      $dbr->MoveNext();
    }
    dbExecute( "DELETE from tbl_JobReportsIndexTables where JobReportId = $JobReportId" );
  }
  
  $main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
  $dbr = $main::xrdbc->dbExecute( "SELECT * from tbl_JobReportsLocalFileCache where JobReportId = $JobReportId " );
  
  if (!$dbr->eof()) {
    my $LocalBasket = substr("0000".$dbr->Fields()->Item('LocalBasket')->Value(), -5);
    
	my $filecache = getConfValues('filecache');

    
    unlink glob( "$filecache/$LocalBasket/$JobReportName\.$JobReportId\.*");
    $dbr->Close();
  }

  dbExecute( "UPDATE tbl_JobReports SET TarFileNumberOUT=0, TarRecNumberOUT=0 where JobReportId = $JobReportId" );
  
  dbExecute( "DELETE from tbl_WorkQueue WHERE ExternalTableName = 'MIGRATION' " 
	   . " AND ExternalKey = $JobReportId AND TypeOfWork in (555,666,888) ");

  my $OUTPATH = getConfValues('LocalPath')->{$self->getValues('LocalPathId_OUT') || "L1"}."/OUT";

  
  my $ZipFileName = "$OUTPATH/".$self->getFileName('ZIPOUT');
 
  if ( $ZipFileName =~ /file:\/\//i ) {
    $ZipFileName = substr($ZipFileName, 7); 
    if ( -e $ZipFileName ) {
      &$logrRtn("DELETING FILE \"$ZipFileName\"");
      unlink $ZipFileName or die("ERROR DELETING FILE \"$ZipFileName\". $!");
    }
    else {
      &$logrRtn("FILE \"$ZipFileName NOT FOUND.");
    }
  }
  elsif ( $ZipFileName =~ /centera:\/\//i ) {
  	dbExecute( "IF EXISTS ( SELECT * from tbl_JobReportsCenteraClipIDs_DELETED where JobReportId = $JobReportId "
  	   . " and CentClipId = (select OUTFile_ClipId from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId ) )"
       . " UPDATE tbl_JobReportsCenteraClipIDs_DELETED set XferStartTime = NULL where JobReportId = $JobReportId "
       . " and CentClipId = (select OUTFile_ClipId from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId )"
       . " ELSE INSERT into tbl_JobReportsCenteraClipIDs_DELETED ( JobReportId, CentClipId, ElabStartTime, PoolRef, XferStartTime ) "
       . " select cid.JobReportId as JobReportId, cid.OUTFile_ClipId as OUTFile_ClipId, jr.ElabStartTime as ElabStartTime"
       . ", jr.LocalPathId_OUT as PoolRef, NULL as XferStarTime "
       . " from tbl_JobReportsCenteraClipIds as cid INNER JOIN tbl_JobReports as jr ON cid.JobReportId = jr.JobReportId "
       . " where jr.JobReportId = $JobReportId "
       . "\n" );
#	dbExecute( 
#		 " UPDATE tbl_JobReportsCenteraClipIds "
#		."    SET PoolRef = ''                 "
#		."       ,OUTFile_ClipId = ''          "
#		."       ,OUTFile_TotalSize = NULL     "
#		."       ,LocalPathId_OUT = ''         "
#		."  WHERE JobReportId = $JobReportId   "
#		."\n" );
#	
#    dbExecute(
#     "DELETE from tbl_JobReportsCenteraClipIds WHERE (JobReportId = $JobReportId AND INFile_ClipId = '')"
#    ."\n" );
  }
  else {
    die("DELETE OF $ZipFileName NOT MANAGED !!");
  }
  warn "Starting to delete tables entries for $JobReportId\n";
#  for ( qw(
#      tbl_JobReportsDiscardedPages
#      tbl_JobReportsFileRanges 
#      tbl_LogicalReports
#      tbl_LogicalReportsRefs
#      tbl_PhysicalReports
#      tbl_JobReportsMixedReportIds 
#      tbl_JobReportsLocalFileCache 
#      tbl_JobCtlInput 
#      tbl_JobReportsBTrees
#      tbl_JobReportsBTreesBlobs
#      tbl_BundlesQ
#    )
#  ) {
#    dbExecute(
#	 "IF EXISTS (SELECT * from $_ WHERE JobReportId = $JobReportId) DELETE from $_ WHERE JobReportId = $JobReportId"
#    );
#  }
#  for ( qw(
#        tbl_JobReportsDiscardedPages
#        tbl_JobReportsFileRanges;FileId;smallint
#        tbl_LogicalReports;ReportId;smallint;ReportName;varchar(32);FilterValue;varchar(32)
#        tbl_LogicalReportsRefs;FilterVar;varchar(32);ReportId;int;textId;int
#        tbl_PhysicalReports;ReportId;smallint
#        tbl_JobReportsMixedReportIds;MD5Hash;char(32)
#        tbl_JobReportsLocalFileCache 
#        tbl_JobCtlInput;JobCtlHash;varchar(64)
#        tbl_JobReportsBTrees;btree_id;int
#        tbl_JobReportsBTreesBlobs;blob_sha1;varbinary(40);btree_id;int
#        tbl_BundlesQ;BundleQId;int
#      )
#    ) 
  if ($JobReportName !~ /^CDAMFILE$/ )
  {
	  my @listOfTablesToDelete =  qw(
			tbl_JobReportsDiscardedPages
			tbl_JobReportsFileRanges;FileId;smallint
			tbl_LogicalReports;ReportId;smallint;ReportName;varchar(32);FilterValue;varchar(32)
			tbl_LogicalReportsRefs;FilterVar;varchar(32);ReportId;int;textId;int
			tbl_PhysicalReports;ReportId;smallint
			tbl_JobReportsMixedReportIds;MD5Hash;char(32)
			tbl_JobReportsLocalFileCache
			tbl_JobReportsPendingXfers;HostName;varchar(32);RemoteFileName;varchar(255)
			tbl_JobReportsBTrees;btree_id;int
			tbl_JobReportsBTreesBlobs;blob_sha1;varbinary(40);btree_id;int
			tbl_BundlesQ;BundleQId;int 
			);
	  my $flagManageLastExecLogicalReports = (getConfValues('ManageLastExecLogicalReports') or 'N');
      if ($flagManageLastExecLogicalReports =~ /^[ys]$/i)
	  {
		my $sql = "DELETE from $_ WHERE JobReportId = $JobReportId";
		my $pre_sql = ""
                     ." DECLARE \@JRID int, \@JobReportName varchar(32), \@FilterValue varchar(32), \@textId int "
                     ." DECLARE JRCursor CURSOR FOR SELECT JobReportId, JobReportName, FilterValue, textId FROM tbl_LastExecLogicalReports WITH (NOLOCK)  "
                     ." where JobReportId = $JobReportId; "
                     ." OPEN JRcursor; "
                     ." FETCH NEXT FROM JRCursor INTO \@JRID, \@JobReportName, \@FilterValue, \@textId "
                     ." WHILE \@\@FETCH_STATUS = 0 BEGIN; "
                     ." update lelr set JobExecutionTime = jr.JobExecutionTime,  JobReportId = lr.JobReportId,  ReportId = lr.ReportId "
                     ." FROM tbl_JobReports   jr "
                     ." ,tbl_logicalReports lr "
                     ." ,tbl_LogicalReportsRefs lrr "
                     ." ,tbl_LastExecLogicalReports lelr "
                     ." where jr.JobReportId = lr.JobReportId  "
                     ." and  lrr.JobReportId = lr.JobReportId  "
                     ." and  lrr.ReportId = lr.ReportId   "
                     ." and lrr.FilterVar = 'CUTVAR'  "
                     ." and jr.status = 18	 "
                     ." and jr.pendingop <> 12 "
                     ." and jr.JobReportName = \@JobReportName "
                     ." and lr.FilterValue	 =  \@FilterValue "
                     ." and lrr.textId 		 = \@textId "
                     ." and lelr.JobReportName = \@JobReportName "
                     ." and lelr.FilterValue   =  \@FilterValue "
                     ." and lelr.textId        = \@textId "
                     ." and lelr.JobReportId = \@JRID  "
                     ." and lr.JobReportId <> \@JRID   "
                     ." and not exists( select 1    "
                     ." FROM tbl_JobReports   jr2 "
                     ." ,tbl_logicalReports lr2 "
                     ." ,tbl_LogicalReportsRefs lrr2 "
                     ." where jr2.JobReportId = lr2.JobReportId  "
                     ." and  lrr2.JobReportId = lr2.JobReportId  "
                     ." and  lrr2.ReportId = lr2.ReportId   "
                     ." and lrr2.FilterVar = 'CUTVAR'  "
                     ." and jr2.status = 18 "
                     ." and jr2.pendingop <> 12 "
                     ." and jr2.JobReportName = \@JobReportName "
                     ." and lr2.FilterValue	 =  \@FilterValue "
                     ." and lrr2.textId 		 = \@textId "
                     ." and lr2.JobReportId <> \@JRID  "
                     ." and lr2.JobReportId > lr.JobReportId  "
                     ." ); "
                     ." FETCH NEXT FROM JRCursor INTO \@JRID, \@JobReportName, \@FilterValue, \@textId "
                     ." END; "
                     ." CLOSE JRCursor; "
                     ." DEALLOCATE JRCursor; ";
		  dbExecute($pre_sql);
		  push(@listOfTablesToDelete,'tbl_LastExecLogicalReports;JobReportName;varchar(32);FilterValue;varchar(32);textId;int');
	  }
	  #for ( qw(
		#	tbl_JobReportsDiscardedPages
		#	tbl_JobReportsFileRanges;FileId;smallint
		#	tbl_LogicalReports;ReportId;smallint;ReportName;varchar(32);FilterValue;varchar(32)
		#	tbl_LogicalReportsRefs;FilterVar;varchar(32);ReportId;int;textId;int
		#	tbl_PhysicalReports;ReportId;smallint
		#	tbl_JobReportsMixedReportIds;MD5Hash;char(32)
		#	tbl_JobReportsLocalFileCache
		#	tbl_JobReportsPendingXfers;HostName;varchar(32);RemoteFileName;varchar(255)
		#	tbl_JobReportsBTrees;btree_id;int
		#	tbl_JobReportsBTreesBlobs;blob_sha1;varbinary(40);btree_id;int
		#	tbl_BundlesQ;BundleQId;int
		#	tbl_LastExecLogicalReports;JobReportName;varchar(32);FilterValue;varchar(32);textId;int
		#  )
		#) 
	  for(@listOfTablesToDelete)
	  {
		  my ($tbl, %tbkeys) = split /;/;
		  my $sql = "DELETE from $_ WHERE JobReportId = $JobReportId";
		  if (scalar(keys %tbkeys)) {
			 $sql = '';
			 $sql .= 'DECLARE @JRID int, ' . join(', ', map { "\@$_ $tbkeys{$_}" } keys %tbkeys). '; ';
			 $sql .= 'DECLARE JRCursor CURSOR FOR SELECT JobReportId, ' . join(', ', keys %tbkeys);
			 $sql .= " FROM $tbl WITH (NOLOCK) where JobReportId = $JobReportId; ";
			 $sql .= 'OPEN JRcursor; ';
			 $sql .= 'FETCH NEXT FROM JRCursor INTO @JRID, ' . join(', ', map { "\@$_" } keys %tbkeys). '; ';
			 $sql .= 'WHILE @@FETCH_STATUS = 0 BEGIN; ';
			 $sql .= "DELETE FROM $tbl WHERE JobReportId = \@JRID AND " .
										 join('AND ', map { "$_ = \@$_ " } keys %tbkeys). '; ';
			 $sql .= 'FETCH NEXT FROM JRCursor INTO @JRID, ' . join(', ', map { "\@$_" } keys %tbkeys). '; ';
			 $sql .= 'END; ';
			 $sql .= 'CLOSE JRCursor; ';
			 $sql .= 'DEALLOCATE JRCursor; ';
		  }
		  dbExecute($sql);
	  }
  }
  my $logmsg = "END of Deleting REPORT links for $JobReportName\/$JobReportId \"$baseFileName\"";
  warn $logmsg, "\n";
  &$logrRtn($logmsg);
}

sub getPageXref {
  my $self = shift; my $PageXrefFile = $self->getFileName('PAGEXREF'); 

  my $INPUT = gensym(); my @PageXref;

  open($INPUT, "<$PageXrefFile") 
   or 
  die("PAGEXREF INPUT OPEN ERROR \"$PageXrefFile\" $!");

  read($INPUT, my $PageXref, -s $PageXrefFile);
 
  close($INPUT);

  for(split("\n",$PageXref)) {
    next if $_ !~ /File=(\d+) +From=(\d+) +To=(\d+)/;
    push @PageXref, $1, $2, $3;
  }

  return \@PageXref;
}

sub getParseFileNames {
  my $self = shift; 
  my ($JobReportName, $parseFileName) = $self->getValues('JobReportName', 'ParseFileName');
  my ($fileNames, $parseParametersString) = split /\//, $parseFileName, 2;
  $parseParametersString = '' unless $parseParametersString;
  $main::debug && i::logit("parsefilename parse parms: $parseParametersString");
  $self->setValues('parseparametersstring', $parseParametersString);
  my $ReportMask = substr($JobReportName,0,3).".".substr($JobReportName,4);
  return "$fileNames, $JobReportName\.XML, $ReportMask\.XML";
}

sub getFileName {
  my ($self, $fileType, @args) = @_;

  my ($SrvName, $workdir, $confdir, $userlib) = getConfValues(qw(SrvName workdir confdir userlib));

  
  #my ($JobReportName, $JobReportId) = $self->getValues(qw(JobReportName JobReportId));
  my ($JobReportName, $JobReportId, $RemoteFileName) = $self->getValues(qw(JobReportName JobReportId RemoteFileName));

  my $FileId = $args[0]; $FileId = "0" if !defined($FileId);

  my $Category = $args[1]; $Category = "" if !defined($Category);

  my $FileSuffix = "$JobReportId\.0\.#$FileId";

  local $_ = $fileType;

  SWITCH: {
    /^INPUT$/i and do {
      return $self->getValues('LocalFileName'); 
    };

    /^PARSE$/i and do {
      my $fileNames = $self->getParseFileNames();
	  
	  #ONLY FOR TEST
	  #$_ = "$confdir/".$JobReportId.".XML";
      #if ( -e $_ ) {
      #  &$logrRtn("ParseFile is \"$_\"");
      #  return $_; 
      #}
      my @fileNames = split( /[\s,]+/ , ($fileNames or "NULL.xml"));
      for ( @fileNames ) {
        $_ = "$JobReportName\.XML" if $_ eq '*'; 
		if($_ eq '#')
		{
			$_ = "$1\.XML"  if($RemoteFileName =~ /^.*?\.([^\.]+)\.J/i)  
		} 
		$_ = "$confdir/$_";
        if ( -e $_ ) {
          &$logrRtn("ParseFile is \"$_\"");
          return $_; 
        }
      }
      croak "UserError: ParseFileName <", join(",", @fileNames), "> NOT FOUND"; 
    };
    
    /^OUTLFILE$/i and do {
      return "$workdir/$SrvName/\$\$IT.$args[0]\.$JobReportId\.tsv";
    };
    
    /^PSOUT$/i and do {
      return "$workdir/$SrvName/$JobReportName\.$FileSuffix\.ps";
    };
    
    /^PDFOUT$/i and do {
      return "$workdir/$SrvName/$JobReportName\.$FileSuffix\.pdf";
    };
    
    /^DATAOUT$/i and do {
      return "$workdir/$SrvName/$JobReportName\.$FileSuffix\.dat";
    };
    
    /^ZIPOUT$/i and do {
      my $dateTime = $self->getValues('$dateTime'); my ($dtYear, $dtDay) = unpack("a4a4", $dateTime);
      return "$dtYear/$dtDay/$JobReportName.$dateTime.$JobReportId\.OUT.#0.zip";
    };
    
    /^LOGFILE$/i and do {
      return "$workdir/$SrvName/\$\$LogFile\.$JobReportId\.LOG";
    };
    
    /LOGTABLES/i and do {
      return "$workdir/$SrvName/\$\$LogTables\.$JobReportId\.xml";
    };
    
    /^LOGPARSE/i and do {
      return "$workdir/$SrvName/\$\$LogParse\.$JobReportId\.xml";
    };
    
    /^LOCALRES$/i and do {
      return "$workdir/$SrvName/localres";
    };
    
    /^IXFILE$/i and do {
      return "$workdir/$SrvName/\$\$IX.$args[0]\.$JobReportId\.tsv";
    };
    
    /^EXCEL$/i and do {
      return "$workdir/$SrvName/$JobReportName\.$JobReportId\.0\.#0\.excel";
    };
    
    /^FILXREF$/i and do {
      return "$workdir/$SrvName/\$\$$Category.FilXref\.$JobReportId\.txt";
    };
    
    /^PAGEXREF$/i and do {
      return "$workdir/$SrvName/\$\$PageXref\.$JobReportId\.txt";
    };
    
    /^IBMZLIB$/i and do {
      my $dateTime = $self->getValues('$dateTime');
      return "$JobReportName\.$dateTime\.$JobReportId\.zxr";
    };

    /^TRANSLATE/i and do {
      return "$userlib/translate.tables/$args[0]\.tbl";
    };
    
    /^JOB$/i and do {
      return "$workdir/$SrvName/\$\$Job\.$JobReportId\.txt";
    };
    
    /^TEMPFILE$/i and do {
      return "$workdir/$SrvName/\$\$l$args[0]\.$JobReportId\.".time()."\.txt";
    };
    
    /^WORKDIR$/i and do {
      return "$workdir/$SrvName";
    };
    
    /^DISCARDED$/i and do {
      return "$workdir/$SrvName/\$\$DISCARDED\.$JobReportId\.txt";
    };

    die "ApplicatioError: getFileName INVALID INPUT $_";
  }
}

sub VerifyOutputFile  {
}

sub PRINT_EXTRACT {
  my $self = shift; $self->setValues('ElabFull', 0);
  
  my $jrx = XReport::JobREPORT::PART->new($self);
  
  $jrx->PRINT(@_);
}

sub deleteAll {
  my $self = shift;
  my $inputDataIsMigratedInCentera = '0';
  my $PoolRef;

  my ($JobReportName, $JobReportId, $LocalFileName, $pathIdIN) 
           = $self->getValues(qw(JobReportName JobReportId LocalFileName LocalPathId_IN));
  $pathIdIN = 'L1' unless $pathIdIN;
  

  my $inrootpath = getConfValues('LocalPath')->{$pathIdIN};

  if  ( $inrootpath !~ /^file:/i ) 
  {
	if( $inrootpath =~ /^\s*centera:\/\/.*\/$/ ) 
	{
		#INPUT IN CENTERA
		$inputDataIsMigratedInCentera = '1';
		$inrootpath =~ /^\s*centera:\/\/(.*)\/$/i ;
		$PoolRef = $1;
		$inrootpath = 'centera//'.$PoolRef;
		&$logrRtn("INPUT job migrated in centera $JobReportName\/$JobReportId\/[$PoolRef]\/[$inrootpath]"); 	
	}
	else 
	{
		die "Delete not allowed for archives not allocated on files - PATH($pathIdIN): $inrootpath\n";  	
	}
  }

  &$logrRtn("BEGIN of complete delete for $JobReportName\/$JobReportId");

#  dbExecute( "DELETE from tbl_WorkQueue WHERE ExternalTableName = 'tbl_JobReports' " 
#           . " AND CONVERT(int, ExternalKey) = $JobReportId ");
  dbExecute( "DELETE from tbl_WorkQueue WHERE ExternalTableName in('tbl_JobReports', 'MIGRATION') " 
           . " AND CONVERT(int, ExternalKey) = $JobReportId ");
  
  $self->deleteElab();
  
  dbExecute(
   "UPDATE tbl_JobReports set OrigJobReportName = '$JobReportName', PendingOp = 12 " .
   "where JobReportId = $JobReportId "
  );
  
  
  if($inputDataIsMigratedInCentera)
  {
		&$logrRtn("inputDataIsMigratedInCentera");
		my $sql = 
		 " INSERT INTO tbl_JobReportsCenteraClipIDs_DELETED (                   "
		." 	JobReportId                                                         "
		." 	,CentClipId                                                         "
		." 	,ElabStartTime                                                      "
		." 	,PoolRef                                                            "
		." 	,XferStartTime                                                      "
		." 	)                                                                   "
		." SELECT cid.JobReportId AS JobReportId                                "
		." 	,cid.INFile_ClipId AS CentClipId                                    "
		." 	,jr.ElabStartTime AS ElabStartTime                                  "
		." 	,cid.PoolRef_IN AS PoolRef                                          "
		." 	,NULL AS XferStarTime                                               "
		." FROM tbl_JobReportsCenteraClipIds AS cid                             "
		." INNER JOIN tbl_JobReports AS jr ON cid.JobReportId = jr.JobReportId  "
		." WHERE jr.JobReportId = $JobReportId									"
		. "\n";
	  	dbExecute($sql);
		
		$sql = 
		 "DELETE from tbl_JobReportsCenteraClipIds WHERE (JobReportId = $JobReportId)"
		."\n";
	  	dbExecute($sql);
  }
  else
  {
	my $INPATH = $inrootpath."/IN";

	my $dataFile = substr("$INPATH/".$self->getFileName('INPUT'), 7); 
	(my $cntrlFile = $dataFile) =~ s/\.DATA\./\.CNTRL\./i;
	$cntrlFile =~ s/\.(?:gz|zip)$//i;

	for my $file ($dataFile, $cntrlFile) {
	### croak("ApplicationError: file \"$file\" NOT FOUND") if !-e $file;
	unlink $file or warn("ApplicationError: UNLINK ERROR $! file \"$file\"") if $file ne ($cntrlFile);
	}
  }
  
  #dbExecute("DELETE from tbl_JobReports where JobReportId = $JobReportId");
  
  
  #write tbl_JobReports_HIST
  my $dbr_HIST = $main::xrdbc->dbExecute(
	     " INSERT INTO  tbl_JobReports_HIST"
		." (JobReportName, JobReportId, LocalPathId, LocalFileName, OrigJobReportName, TarRecNumberIN, TarRecNumberOUT, RemoteHostAddr, RemoteFileName, JobName, JobNumber, JobExecutionTime, XferStartTime, XferEndTime, XferRecipient, XferMode, XferInBytes, XferInLines, XferInPages, XferOutBytes, XferDaemon, XferId, ElabStartTime, ElabEndTime, ElabOutBytes, InputLines, InputPages, ParsedLines, ParsedPages, DiscardedLines, DiscardedPages, Status, ViewLevel, MailingStatus, PendingOp, SrvName, UserTimeRef, UserTimeElab, UserRef, XrefData) "
		." OUTPUT INSERTED.*"
		." select    JobReportName, -1 * JobReportId, LocalPathId_IN, LocalFileName, OrigJobReportName, TarRecNumberIN, TarRecNumberOUT, RemoteHostAddr, RemoteFileName, RIGHT(JobName,16), JobNumber, JobExecutionTime, XferStartTime, XferEndTime, XferRecipient, XferMode, XferInBytes, XferInLines, XferInPages, XferOutBytes, XferDaemon, XferId, ElabStartTime, ElabEndTime, ElabOutBytes, InputLines, InputPages, ParsedLines, ParsedPages, DiscardedLines, DiscardedPages, Status, ViewLevel, MailingStatus, PendingOp, RIGHT(SrvName,16), UserTimeRef, UserTimeElab, UserRef, XrefData "
		." from tbl_JobReports jr where JobReportId = $JobReportId"
		." and not exists (select 1 from tbl_JobReports_HIST jrh"
		." where jrh.JobReportId = -1 * jr.JobReportId) ;") ; 
  &$logrRtn("INSERT INTO  tbl_JobReports_HIST made with success for $JobReportName\/$JobReportId") if (!$dbr_HIST->eof());
  $dbr_HIST->Close();
  
  
    
  for ( qw(
      tbl_JobCtlInput;JobCtlHash;varchar(64)
      tbl_JobReports 
    )
  ) 
  {
      my ($tbl, %tbkeys) = split /;/;
      my $sql = "DELETE from $_ WHERE JobReportId = $JobReportId";
      if (scalar(keys %tbkeys)) {
         $sql = '';
         $sql .= 'DECLARE @JRID int, ' . join(', ', map { "\@$_ $tbkeys{$_}" } keys %tbkeys). '; ';
         $sql .= 'DECLARE JRCursor CURSOR FOR SELECT JobReportId, ' . join(', ', keys %tbkeys);
         $sql .= " FROM $tbl WITH (NOLOCK) where JobReportId = $JobReportId; ";
         $sql .= 'OPEN JRcursor; ';
         $sql .= 'FETCH NEXT FROM JRCursor INTO @JRID, ' . join(', ', map { "\@$_" } keys %tbkeys). '; ';
         $sql .= 'WHILE @@FETCH_STATUS = 0 BEGIN; ';
         $sql .= "DELETE FROM $tbl WHERE JobReportId = \@JRID AND " .
                                     join('AND ', map { "$_ = \@$_ " } keys %tbkeys). '; ';
         $sql .= 'FETCH NEXT FROM JRCursor INTO @JRID, ' . join(', ', map { "\@$_" } keys %tbkeys). '; ';
         $sql .= 'END; ';
         $sql .= 'CLOSE JRCursor; ';
         $sql .= 'DEALLOCATE JRCursor; ';
      }
	  dbExecute($sql);
  } 

  &$logrRtn("END of complete delete for $JobReportName\/$JobReportId");
}

sub queueForElab {
  my $self = shift; my %args = @_; my $WorkClass = $args{'WorkClass'} || 64;

  my $JobReportId = $self->getValues('JobReportid');
 
  dbExecute("
    INSERT INTO tbl_WorkQueue  
    (  
      TypeOfWork, WorkClass, Priority, Status, InsertTime, ExternalKey, ExternalTableName  
    )  
    VALUES  
    (  
      1, $WorkClass, 0, ".ST_QUEUED.", GETDATE(), $JobReportId, 'tbl_JobReports'  
    )
  "); 

}

sub Iterator {
  my ($className, %args) = @_; my ($sqlHdr, $sqlTrl); 

  my ($JobReportMask, $XferRecipientMask, $fromdate, $todate, $JobReportIdList) 
    = 
  @args{qw(JobReportMask XferRecipientMask fromdate todate JobReportIdList)};

  $sqlHdr = "SELECT j.* from tbl_JobReports j where j.JobReportName LIKE '$JobReportMask' ";

  $sqlTrl .= "AND j.XferStartTime >= '$fromdate' " if $fromdate;

  $sqlTrl .= "AND j.XferStartTime <= '$todate' " if $todate;

  if ( $JobReportIdList ) {
    my $sqlHdr = "SELECT j.JobReportId from tbl_JobReports j where j.JobReportName LIKE '$JobReportMask' ";
    my %inList = map { $_, 1 } @$JobReportIdList;
    my $inList = join(",", @$JobReportIdList);
    my $sql = $sqlHdr.$sqlTrl;
	
	$main::xrdbc = XReport::DBUtil->new() unless $main::xrdbc && ref($main::xrdbc) eq 'XReport::DBUtil';
	
    my $dbr = $main::xrdbc->dbExecute("
      SELECT j.* from tbl_JobReports j where j.JobReportId IN( $inList ) AND j.JobReportId IN( $sql )
    ");

    if ( !$dbr->eof() ) {
      delete $inList{scalar($dbr->GetFieldsValues('JobReportId'))};
      $dbr->MoveNext();
    }
    $dbr->Close();

    #!scalar(keys(%inList)) or die %inList;

    $sqlTrl .= "AND j.JobReportId IN( $inList ) ";
  }

  $sqlTrl .= "AND j.XferRecipient like '$XferRecipientMask' " if $XferRecipientMask;

  bless { dbrj => dbExecute($sqlHdr.$sqlTrl) }, "XReport::JobREPORT::ITERATOR"; 
}

#----------------------------------------------------------------
package XReport::JobREPORT::PART;

use strict qw(vars);

use constant EF_PRINT => 1;
use constant EF_DATA => 2;
use constant EF_CONVERTAFP => 101;
use constant EF_BUNDLE => 102;

sub getValues {
  my $self = shift; my $jr = $self->{jr}; my @r;

  for my $k (@_) {
    if (exists($self->{$k})) {
      push @r, $self->{$k};
    }
    else {
      my $v = $jr->getValues($k);
      push @r, $v;
    }
  }
  return wantarray ? @r : $r[0]; 
}

sub setValues {
  my $self = shift;
  while(@_) {
    my ($k, $v) = (shift, shift);
    $self->{$k} = $v;
  }
}

sub getReportDefs {
  my $self = shift; my $jr = $self->{jr};
  return $jr->getReportDefs(@_);
}

sub getFileName {
  my ($self, $fileType, @args) = @_; my $jr = $self->getValues('jr');
  
  my ($JobReportId, $REQUEST_ID, $ToDir) 
   = 
  $self->getValues(qw(JobReportId REQUEST_ID ToDir));

  $_ = $fileType;
  SWITCH: {
    /PARSE/i and do {
      return $jr->getFileName($fileType, @args);
    };
    my $FileSuffix = $SUFFIX{$fileType} || 'dat';
    return "$ToDir/$REQUEST_ID\.$fileType\.$JobReportId\.$FileSuffix";
  }
}

sub PRINT {
  my $self = shift; my %args = @_;

  my ($ExtractedFile, $REQUEST_ID, $ToDir) = @args{qw(ExtractedFile REQUEST_ID ToDir)};
  
  @{$self}{qw(LocalFileName XferMode ToDir REQUEST_ID ElabFormat ElabFull jr)} 
    = 
  ($ExtractedFile, 2, $ToDir, $REQUEST_ID, EF_PRINT, 0, $self->{jr});
  
  #fill the LocalRes Directory /copy or call the output routine

  #if ( $jr->getValues('isAfp') ) {
  #  $AfpResDir = "$ToDir/REQUEST_ID\.localres.$JobReportId"; mkdir $AfpResDir;
  #  XReport::AFP::EXTRACT::CrePsLocalResDir(
  #  $AfpResDir, @AfpResList
  #    );
  #}

  require XReport::Parser;
  my $parser = new XReport::Parser( $self );
  $parser->doParsing(); $parser->Close(); $parser = undef;
}

sub new {
  my ($className, $jr) = (shift, shift); 
  
  my $self = {
    jr => $jr
  };

  bless $self, $className;
}


#----------------------------------------------------------------
package XReport::JobREPORT::ITERATOR;

sub getNext {
  my ($self, $FULLMODE) = (shift, shift); my $dbrj = $self->{'dbrj'}; return undef if $dbrj->eof();

  my $JobReportId = $dbrj->GetFieldsValues('JobReportId'); $dbrj->MoveNext();

  XReport::JobREPORT->Open($JobReportId, $FULLMODE);
}

sub eof {
  my $self = shift; my $dbrj = $self->{'dbrj'};  return $dbrj->eof();
}

1;
