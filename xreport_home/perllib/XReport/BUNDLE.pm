package XReport::BUNDLE;

use POSIX qw(strftime);

use Convert::EBCDIC;

use Text::Reform qw(form columns);
use Text::FIGlet;
use File::Spec;

use strict; use bytes;

use XReport::DBUtil;

#use XReport::PDF::BUNDLE;

use constant ST_QUEUED => 16;
use constant ST_INPROCESS => 17;
use constant ST_COMPLETED => 18;
use constant ST_PROCERROR => 31;

sub Prepare_lFilterTargets {
  my $self = shift;
  
  my (
    $FilterTargetsVar, $FilterTargetsMask, $lReportNames, $lPendingJobReports, 
    $fmXfer, $toXfer, $MaxDaysBefore
  ) =
  @{$self}{qw(
    FilterTargetsVar FilterTargetsMask ListOfReportNames ListOfPendingJobReports fmXfer toXfer MaxDaysBefore
  )};

    
  my ($lFilterTargets, $dbr) = ({}, undef); 
  
  $dbr = dbExecute("
    SELECT a.FilterTarget, a.FilterTargetValue, b.FilterVar, b.FilterValue
     from tbl_FilterTargets a 
     LEFT OUTER JOIN tbl_FilterTargetsFilterVars b 
     ON b.FilterTargetVar = a.FilterTargetVar AND b.FilterTarget = a.FilterTarget
    WHERE
     a.FilterTargetVar = '$FilterTargetsVar' AND a.FilterTarget LIKE '$FilterTargetsMask'
  ");

  my %lSearchedFilterValues = ();

  while(!$dbr->eof()) {
    my (
      $FilterTarget, $FilterTargetValue, $FilterVar, $FilterValue
    ) = 
    $dbr->GetFieldsValues(qw(FilterTarget FilterTargetValue FilterVar FilterValue));

    $FilterVar = $FilterTargetsVar if $FilterVar eq '';
    $FilterValue = $FilterTargetValue if $FilterValue eq '';

    $lSearchedFilterValues{$FilterVar}->{$FilterValue} = 1;
    
    if (!exists($lFilterTargets->{$FilterTarget})) {
      $lFilterTargets->{$FilterTarget} = {
        kFilterTarget => $FilterTarget,
        lFilterVars => {},
        lReports => [],
        FilterTargetValue => $FilterTargetValue
      }
    }
    
    my $lFilterVars = $lFilterTargets->{$FilterTarget}->{lFilterVars};
    
    if (!exists($lFilterVars->{$FilterVar})) {
      $lFilterVars->{$FilterVar} = {
        lFilterValues => []
      }
    }
    my $lFilterValues = $lFilterVars->{$FilterVar}->{lFilterValues};
  
    push @{$lFilterValues}, $FilterValue;
  
    $dbr->MoveNext();
  } 
  
  my ($lJobReportIds, $NewlPendingJobReports) = ([], []);
  
  my $SQL = ("
    SELECT DISTINCT j.JobReportId, j.XferStartTime, j.Status 
    
    FROM tbl_LogicalReports l INNER JOIN tbl_JobReports j ON j.JobReportId = l.JobReportId
      
    WHERE 
      l.ReportName IN(".join(', ', map {"'$_'"} @$lReportNames).") AND l.FilterValue = ''
      AND l.XferDateDiff 
        BETWEEN (dbo.XferDateDiff('$fmXfer') - 1) 
        AND (dbo.XferDateDiff('$toXfer') + 1)
      AND 
        (j.XferStartTime BETWEEN '$fmXfer' AND '$toXfer') ".
        (scalar(@$lPendingJobReports)
          ? "OR (
             j.JobReportId IN (". join(",", @$lPendingJobReports).")
             AND j.XferStartTime <= '$toXfer'
            )"
          : " "
        )
      ."
      AND DATEDIFF(DAY, j.XferStartTime, '$toXfer') <= $MaxDaysBefore
    ORDER BY j.XferStartTime
  ");
  #print "$SQL\n";
  $dbr = dbExecute($SQL);

  #todo: if status <> ST_COMPLETED and time < $maxstarttime restart after 10 min
  
  while(!$dbr->eof()) {
    my ($JobReportId, $Status) = $dbr->GetFieldsValues(qw(JobReportId Status));
    if ( $Status == ST_COMPLETED ) {
      push @$lJobReportIds, $JobReportId;
    }
    else {
      #todo: make some logic on XferStartTime
      push @$NewlPendingJobReports, $JobReportId;
    }
    $dbr->MoveNext();
  }
  $dbr->Close(); 

  return if !@$lJobReportIds;

  $self->{NewListOfPendingJobReports} = $NewlPendingJobReports;

  my %lExistingFilterValues = ();

  for my $FilterVar (keys(%lSearchedFilterValues)) {
    #my $lFilterValues = [sort(keys(%{$lSearchedFilterValues{$FilterVar}}))];
    $dbr = dbExecute("
      SELECT DISTINCT l.FilterValue
       FROM
        tbl_LogicalReports l INNER JOIN tbl_ReportNames r ON r.ReportName = l.ReportName
        INNER JOIN tbl_PhysicalReports p 
          ON p.JobReportId = l.JobReportId AND p.ReportId = l.ReportId
        INNER JOIN tbl_JobReports j 
          ON j.JobReportId = l.JobReportId
            
        WHERE
         l.JobReportId IN(".join(",",@$lJobReportIds).")
         AND r.FilterVar = '$FilterVar'
         AND l.ReportName IN(".join(",", map {"'$_'"} @$lReportNames).") 
    ");
    #AND l.FilterValue IN(".join(",", map {"'$_'"} @$lFilterValues).")
    
    while(!$dbr->eof()) {
      $lExistingFilterValues{$FilterVar}->{$dbr->GetFieldsValues('FilterValue')} = 1;
    }
    continue {
      $dbr->MoveNext();
    }
  }
  
  for my $kFilterTarget ( sort(keys(%$lFilterTargets)) ) { 
    my $FilterTarget = $lFilterTargets->{$kFilterTarget};

    for my $kFilterVar ( sort(keys(%{$FilterTarget->{lFilterVars}})) ) {
      my $FilterVar = $FilterTarget->{lFilterVars}->{$kFilterVar};

      my $lExistingFilterValues = $lExistingFilterValues{$kFilterVar};
      my $lFilterValues = [
        grep {
          exists($lExistingFilterValues->{$_})
        }
        @{$FilterVar->{lFilterValues}}
      ];
      next if !@$lFilterValues;

      #todo: if Report without filtervar include all of it (blank)
      
      print "QUERY FOR : ", join(",", @$lFilterValues), "\n";
      my $dbr = dbExecute("
        SELECT
          l.ReportName, r.ReportDescr, l.ReportId, p.TotPages, p.ListOfPages,
          CONVERT(varchar, j.XferStartTime, 120) AS XferStartTime, 
          CONVERT(varchar, j.UserTimeElab, 120) AS UserTimeElab, 
          CONVERT(varchar, j.UserTimeRef, 120) AS UserTimeRef, 
          j.JobName, j.JobNumber, j.JobReportId,
          r.FilterVar, l.FilterValue
        
         FROM
          tbl_LogicalReports l INNER JOIN tbl_ReportNames r ON r.ReportName = l.ReportName
          INNER JOIN tbl_PhysicalReports p 
            ON p.JobReportId = l.JobReportId AND p.ReportId = l.ReportId
          INNER JOIN tbl_JobReports j 
            ON j.JobReportId = l.JobReportId
            
         WHERE
          l.JobReportId IN(".join(",",@$lJobReportIds).")
          AND r.FilterVar = '$kFilterVar'
          AND l.ReportName IN(".join(",", map {"'$_'"} @$lReportNames).") 
          AND l.FilterValue IN(".join(",", map {"'$_'"} @$lFilterValues).")
          
         ORDER BY j.XferStartTime, j.JobReportId, r.ReportName, l.FilterValue
      ");
  
      my $lReports = $FilterTarget->{lReports}; my $ltref = undef;
      while (!$dbr->eof()) {
        my $tref = $dbr->GetFieldsHash( 
          qw(
            JobReportId XferStartTime UserTimeElab UserTimeRef JobName JobNumber 
            ReportName ReportDescr ReportId TotPages ListOfPages
          )
        );
        $tref->{KEYFIELD} = join("/", @{$tref}{qw(JobReportId ReportName)});
        if ( !$ltref or $ltref->{KEYFIELD} ne $tref->{KEYFIELD} ) {
          ### memory performance problems if lReports hashes saved as is
          push @$lReports, join("\x00", %$ltref) if $ltref; $ltref = $tref;
        }
        else {
          $ltref->{ListOfPages} .= ",$tref->{ListOfPages}";
          $ltref->{TotPages} += $tref->{TotPages};
        }
      } 
      continue {
        $dbr->MoveNext(); 
      }
      push @$lReports, join("\x00", %$ltref) if $ltref; $dbr->Close();
    }
  }

  return  $self->{'ListOfFilterTargets'} = $lFilterTargets;
}

sub Create {
  my $self = shift; $self->Prepare_lFilterTargets(); my $jr = $self->{'jr'}; 

  my $fileName = $jr->getFileName('PAGEXREF'); my $PAGEXREF = gensym();

  open($PAGEXREF, ">$fileName") 
   or 
  die("PAGEXREF OPEN OUTPUT ERROR \"$fileName \"$!");
  
  my ($ReportBundleName, $dbReportDefs, $lFilterTargets) 
    = 
  @{$self}{qw(ReportBundleName dbReportDefs ListOfFilterTargets)}; 

  my ($atFile, $ReportId) = (-1, 0);

  my ($FromPage, $ToPage, $TotPages) = (1, 0, 0); 

  for my $kFilterTarget ( sort(keys(%$lFilterTargets)) ) {

    my $FilterTarget = $lFilterTargets->{$kFilterTarget};
    my $lReports = $FilterTarget->{lReports}; 

    next if !@$lReports;

    $FromPage = $ToPage+1; $TotPages = 0; 

    ### memory performance problems if lReports hashes saved as is
    $lReports = [ map { {split("\x00", $_)} } @$lReports ];

    my $FilterTargetValue = $FilterTarget->{FilterTargetValue};
  
    $atFile += 1; $ReportId += 1; 
    
    ## todo: manage INPUT/text
    my $fileName = $jr->getFileName("PDFOUT", $atFile);

    print "CREATING PDF FOR $kFilterTarget FILE: $fileName\n";

    $ToPage += $TotPages = XReport::PDF::BUNDLE->Create(
      $fileName, $kFilterTarget, $lReports
    );
    @{$FilterTarget}{qw(ReportId FromPage TotPages)} = ($ReportId, $FromPage, $TotPages);

    print $PAGEXREF "File=$atFile From=$FromPage To=$ToPage\n";

    ### todo change to pagexfer ????
    $jr->NewPhysicalReport($ReportId, $TotPages, \"$FromPage,$TotPages"); 

    $jr->NewLogicalReport($ReportBundleName, $FilterTargetValue, $TotPages, $ReportId, 0, $dbReportDefs); 
  }

  $TotPages = $ToPage;
  $jr->NewPhysicalReport(0, $TotPages, \"1,$TotPages"); 
  $jr->NewLogicalReport($ReportBundleName, "", $TotPages, 0, 0, $dbReportDefs); 
  
  XReport::PDF::BUNDLE->reset(); close($PAGEXREF); return $self;
}

sub QuoteField {
  my ($self, $FieldName) = (shift, shift); my $FieldValue = $self->{$FieldName};
  
  $FieldValue = join(",", @$FieldValue) if ref($FieldValue);

  $FieldValue =~ s/'/''/g; 

  $FieldValue = "'$FieldValue'"; return $FieldValue;
}

sub Close {
  my $self = shift; my ($jr, $rebuild) = @{$self}{qw(jr rebuild)};
  
  $self->{XferStartTime} = $jr->getValues('XferStartTime');
  
  my ($ReportBundleName, $JobReportId) = @{$self}{qw(ReportBundleName JobReportId)};

  if ( !$rebuild ) {
    my @fields = qw(
      ReportBundleName JobReportId StartJobReportId XferStartTime 
      FilterTargetsVar FilterTargetsMask ListOfReportNames 
      ListOfPendingJobReports
    );
    dbExecute("
      INSERT INTO tbl_ReportBundles (". join(",", @fields) .")
       VALUES
      (".  join(",", map {$self->QuoteField($_)} @fields ) .")
    ");
  }
  else {
    my @fields = qw(
      StartJobReportId XferStartTime FilterTargetsMask 
      ListOfReportNames ListOfPendingJobReports
    );
    dbExecute("
      UPDATE tbl_ReportBundles SET ". join(",", map {"$_ = ".$self->QuoteField($_) } @fields) ."
      WHERE ReportBundleName = '$ReportBundleName' AND JobReportId = $JobReportId
    ");
  }

  my $dbr = dbExecute("
    SELECT JobReportId From tbl_ReportBundles
    WHERE 
     ReportBundleName = '$ReportBundleName'
     AND XferStartTime = (
       SELECT MAX(XferStartTime) from tbl_ReportBundles
       WHERE ReportBundleName = '$ReportBundleName'
     )
  ");
  my $Last_jrid = $dbr->GetFieldsValues('JobReportId'); $dbr->Close();
 
  if ($JobReportId == $Last_jrid ) {
    print "UPDATING ReportBundleNames\n";
    dbExecute("
      UPDATE tbl_ReportBundleNames set ListOfPendingJobReports = ".
      $self->QuoteField( 'ListOfPendingJobReports' ) ."
      WHERE ReportBundleName = '$ReportBundleName'
    ");
  }
}


sub newBundleElab {
    my ($self, $info ) = (shift, shift);
=Bundle Destination

La Destination del bundle pu� essere definita in diverse location 
considerate nell'ordine seguente:

* XML di Skeleton ({BundleSkel}.xml ) in BUNDLESKEL
* XML di Bundle ({BundleName}.xml) in BUNDLESKEL
* parametro di esecuzione (-dest )
* XML di applid ({-N parameter}.xml in XML )
* DB: nella tabella tbl_BundlesNames colonna BundleDest
* se mancante il processo terminer� alla close del file di bundle
 
=cut
    # info contiene la row del db
    my $dbdest = delete $info->{BundleDest};
    $dbdest = undef if defined($dbdest) && $dbdest =~ /^\s*$/;
    
    # $self contiene i parametri di esecuzione + cfg xml     
    my $appldest = delete $self->{BundleDest} if exists($self->{BundleDest});
    $appldest = undef if defined($appldest) && $appldest =~ /^\s*$/;
    
    my $execdest = $self->{execdest} if exists($self->{execdest});
    $execdest = undef if defined($execdest) && $execdest =~ /^\s*$/;
    $info->{Destination} = $execdest if $execdest;
    
    #load overrides per BundleName        
    if ( $info->{BundleName} ne $self->{BundleName} ) {
            $self->processUserConfig($info->{BundleName});
            $self->{BundleName} = $info->{BundleName};
    }
    else { $self->{BundleDest} = $self->{prevbdest}; }
    my $bcfgdest = delete $self->{BundleDest} if exists($self->{BundleDest});
    $bcfgdest = undef if defined($bcfgdest) && $bcfgdest =~ /^\s*$/;
    $self->{prevbdest} = $bcfgdest if $bcfgdest;
    
    #load overrides per BundleSkel        
    if ( $info->{BundleSkel} ne $self->{BundleSkel} ) {
            $self->processUserConfig($info->{BundleSkel});
            $self->{BundleSkel} = $info->{BundleSkel};
    }
    else { $self->{BundleDest} = $self->{prevsdest}; }
    my $scfgdest = delete $self->{BundleDest} if exists($self->{BundleDest});
    $scfgdest = undef if defined($scfgdest) && $scfgdest =~ /^\s*$/;
    $self->{prevsdest} = $scfgdest if $scfgdest;
    
    my $destination = ( $scfgdest ? $scfgdest
            : $bcfgdest ? $bcfgdest 
            : $execdest ? $execdest
            : $appldest ? $appldest
            : $dbdest   ? $dbdest
            : 'JUSTKEEP://.');
            
    i::logit("Bundle Destination set to \"$destination\" as for "
        . ( $scfgdest ? "Skeleton CFG "
            : $bcfgdest ? "Bundle CFG" 
            : $execdest ? "Execution Parameters"
            : $appldest ? "$self->{cfgname} CFG"
            : $dbdest   ? "$info->{BundleKey} DB entry"
            : " missing destination specification"
            )
    );
    $self->{BundleDest} = $appldest;
    
    return setPrintInfo( $info, {
                ElabParameters => join(' ', @ARGV),
                ElabStartTime => XReport::BUNDLE::strftime("%Y-%m-%d %H:%M:%S.001", localtime),
                odate => XReport::BUNDLE::strftime("%d/%m/%Y", localtime),
                otime => XReport::BUNDLE::strftime("%H:%M:%S", localtime),
                dtstring => 'D'. substr( XReport::BUNDLE::strftime("%Y%m%d.T%H%M%S", localtime), 1 ),
                Status => 2, 
                InputPages => 0, 
                InputLines => 0, 
                Status => 2,
                Destination => $destination,
                BundleDest => $appldest || $dbdest
    } );
                              
}

sub new {
  my ($className, $jr) = @_; my ($self, $dbrd, $dbrj); my $ReportBundleName;
  
  my (
    $JobReportName, $JobReportId, $StartJobReportId, $MaxStartTime, $MaxDaysBefore, 
    $FilterTargetsVar, $FilterTargetsMask, $lReportNames, $lPendingJobReports, $rebuild
  );

  ($JobReportName, $JobReportId) = $jr->getValues(qw(JobReportName JobReportId));

  $dbrd = dbExecute("
    SELECT * from tbl_ReportBundleNames WHERE JobReportName = '$JobReportName'
  ");
  if ( $dbrd->eof() ) {
    die "UserError: REPORT BUNDLEs FOR JOBREPORTNAME \"$JobReportName\" NOT DEFINED";
  }

  (
    $ReportBundleName, $MaxStartTime, $MaxDaysBefore, 
    $FilterTargetsVar, $FilterTargetsMask, 
    $lReportNames, $lPendingJobReports
  ) =
  $dbrd->GetFieldsValues(qw(
    ReportBundleName MaxStartTime MaxDaysBefore 
    FilterTargetsVar FilterTargetsMask ListOfReportNames ListOfPendingJobReports
  ));

  $dbrj = dbExecute("
    SELECT *, 1 AS rebuild from tbl_ReportBundles
    WHERE ReportBundleName = '$ReportBundleName' AND JobReportId = $JobReportId
  ");
  
  if ( !$dbrj->eof() ) {
    (
      $StartJobReportId, $FilterTargetsVar, $FilterTargetsMask, 
      $lReportNames, $lPendingJobReports, $rebuild
    ) =
    $dbrj->GetFieldsValues(qw(
      StartJobReportId FilterTargetsVar FilterTargetsMask 
      ListOfReportNames ListOfPendingJobReports 
      rebuild
    ));
  }
  else {
    $dbrj = dbExecute("
      SELECT TOP 1 JobReportId, 0 As rebuild from tbl_ReportBundles WHERE 
       ReportBundleName = '$ReportBundleName' 
       AND XferStartTime < (
         SELECT XferStartTime from tbl_JobReports
         WHERE JobReportId = $JobReportId
       )
      ORDER BY XferStartTime DESC
    ");
    if ( !$dbrj->eof() ) {
      $StartJobReportId = $dbrj->GetFieldsValues("JobReportId");
    }
    else {
      $StartJobReportId = $JobReportId;
    }
  }

  $dbrj->Close(); $dbrd->Close();

  my ($fmXfer, $toXfer) = ();

  if ( $StartJobReportId != $JobReportId ) {
    $dbrj = dbExecute("
      SELECT MIN(XferStartTime) As fmXfer, MAX(XferStartTime) As toXfer
      from tbl_JobReports WHERE JobReportId IN($StartJobReportId, $JobReportId)
    ");
    if ( $dbrj->eof() ) {
      die "ApplicationError: REFERENCE BUNDLE JobReports NOT FOUND";
    }
    ($fmXfer, $toXfer) = $dbrj->GetFieldsValues(qw(fmXfer toXfer)); $dbrj->Close();
  }
  else {
    $toXfer = $jr->getValues('XferStartTime');
    $dbrj = dbExecute("
      SELECT DATEADD(DAY, -1, '$toXfer') AS fmXfer
    ");
    $fmXfer = $dbrj->GetFieldsValues('fmXfer');
  }

  my $dbReportDefs = $jr->getReportDefs($ReportBundleName);

  $self = {
    ReportBundleName => $ReportBundleName, dbReportDefs => $dbReportDefs,
    jr => $jr, rebuild => $rebuild,
    JobReportId => $JobReportId,
    StartJobReportId => $StartJobReportId,
    MaxStartTime => $MaxStartTime,
    MaxDaysBefore => $MaxDaysBefore,
    fmXfer => $fmXfer, toXfer => $toXfer,
    FilterTargetsVar => $FilterTargetsVar,
    FilterTargetsMask => $FilterTargetsMask,
    ListOfReportNames => [split(/[\s,]+/, $lReportNames)],
    ListOfPendingJobReports => [split(/[\s,]+/, $lPendingJobReports)]
  };

  bless $self, $className;
}

sub rec5a {
    my ($subt5a, $data5a) = (shift, shift);
    return "\x5a". pack("n H12 a*", length($data5a) + 8, $subt5a."000000", $data5a); 
}

sub packrec {
	my $self = shift;
    return pack('n/a', $_[0] );
}

sub formatBannerRec {
    my $self = shift;
    my $info = $_[1];
    my $match0 = qr/\^\#[0-9A-Fa-f]+\#\^/;
    my $match1 = qr/\^\{[^\}]+\}\^/;
    my $match2 = qr/\^\{[^\}]+\}\%[^\^]+\^/;
    my $match3 = qr/\^\[[^\[\]]+\]\%[^\^]+\^/;
    my $case0 = qr/\^\#([0-9A-Fa-f]+)\#\^/;
    my $case1 = qr/\^\{([^\}]+)\}\^/;
    my $case2 = qr/\^\{([^\}]+)\}(\%[^\^]+)\^/;
    my $case3 = qr/\^\[([^\[\]]+)\](\%[^\^]+)\^/;
    $info = {} unless $info;
#   warn "processing string: $_[0] HEX: ", unpack('H*', $_[0]), "\n";
# ^�*�%*47.47s^*      ^�*�%*40.40s^*    
    if ($_[0] =~ /^\^\&(figlet[\d]?):([\{\[])([^\}\]]+)[\}\]]:(.+)\^\s*$/ ) {
        my ($type, $reqt, $reqs, $reqo) = ($1, $2, $3, $4);
        my $str = ($reqt eq '{' ? $info->{$reqs} : $reqs );
        my $bigstring = [ split /\x0a/, $self->{$type}->figify(-A=>$str, split /[ ,=]/, $reqo )];
        i::logit("BIGSTRING:\n".join("\n", @{$bigstring}));
        return [ map { &{$self->{bannerxlator}}(' '.$_, 5) } @{$bigstring} ]
    }
    my $str = join('',  map { 
#        warn 'ESC: ', unpack('H*', '^�#{'), " STR FOUND: $_ HEX: ", unpack('H*', $_), "\n";
                 # ^#0123456ABCDEF#^ HEX STRING
            $_ =~ /^\^\#([0-9A-Fa-f]+)\#\^$/ ? pack('H*', $1)
                 # ^{varname}^  $info key value
          : $_ =~ /^\^\{([^\}]+)\}\^$/ ?  &{$self->{bannerxlator}}($info->{$1}, 1)
                 # ^{varname}%3.3s^   $info key value with sprintf mask
          : $_ =~ /^\^\{([^\}]+)\}(\%[^\^]+)\^$/ ?  &{$self->{bannerxlator}}(sprintf($2, $info->{$1}), 2) 
                 # ^[costant]%c3.3s^  constant string with optional sprintf mask and fill char
          : $_ =~ /^\^\[([^\[\]]+)\]\%([^\d\-])?([^\^]+)\^$/ ?  &{$self->{bannerxlator}}(
                     do { # warn "P: ".join('::', $_, $1, $2, $3), "\n";
                        my ($p, $s) = ($2, sprintf('%'.$3, $1)); $s =~ s/ /$p/g if $p; $s}, 3) 
          : &{$self->{bannerxlator}}($_, 4) } 
                              split /(\^[^\^]+\^)/, $_[0]);
#    warn "bannerRec STR: ", unpack('H40', $str), "\n";                           
    return [ ($str !~ /^\xD3[^\x40]{2}/ ? $str : "\x5a".pack('n a*', length($str) + 2, $str)) ]; 
} 

# TODO: verify validity of all fields
sub insertBundleEntry {
    my ($self, $info) = (shift, shift);
    die "NO Valid DB HANDLE exists " unless exists($self->{dbhandle}) 
                                         && ref($self->{dbhandle}) eq 'XReport::DBUtil';
                                         
    my $sql = "INSERT INTO tbl_Bundles (BundleName, BundleCategory, BundleSkel, BundleDest, Status"
               . ", ElabStartTime, Destination)" 
               . "SELECT BundleName, BundleCategory, BundleSkel, BundleDest, 17 as Status"
               . ", '$info->{ElabStartTime}' as ElabStartTime, '$info->{Destination}' as Destination"
               . " FROM tbl_BundlesNames WHERE BundleKey = '$info->{BundleKey}'"
               ;
    i::warnit("insertBundleEntry:\n".$sql);
    $self->{dbhandle}->dbExecute_NORETRY($sql);
    $sql = "SELECT * FROM tbl_Bundles where BundleId = SCOPE_IDENTITY() ";
    i::warnit("insertBundleEntry2:\n".$sql);
    my $rs =  $self->{dbhandle}->dbExecute_NORETRY($sql);
               ;                  
    if ($rs->eof()) {
    warn "No results returned from query - Bundle Log not Created to be processed\n";
       return undef;
    }
    else {
        my $flds = $rs->GetFieldsHash();
        @{$info}{qw(BundleId BundleStartTime)} = @{$flds}{qw(BundleId ElabStartTime)};
        return $info;
    }
    return $info;
}
    
sub updateBundleEntry {
    my ($self, $info) = (shift, shift);
    die "NO Valid DB HANDLE exists " unless exists($self->{dbhandle}) 
                                         && ref($self->{dbhandle}) eq 'XReport::DBUtil';
    
    return undef unless exists($info->{BundleId});   

    my $args = { @_ };
    my $updates = {};

    for my $vn ( qw(BundleDest ElabStartTime ElabEndTime InputLines InputPages ElabLines ElabPages
                    Destination XferStartTime XferEndTime XferOutBytes Status ElabParameters) ) {
        $updates->{$vn} = $info->{$vn} if exists($info->{$vn});
        $updates->{$vn} = $args->{$vn} if exists($args->{$vn});
    }
    my $sql = "UPDATE tbl_Bundles SET "
            . join(', ', map { (my $v = ($updates->{$_} || '')) =~ s/'/''/g; "$_ = ".(defined($v) ? "'$v'" : 'NULL')} keys %{$updates})
            . " WHERE BundleId = $info->{BundleId}" 
               if exists($info->{BundleId});
#    i::warnit("insertBundleEntry:\n".$sql);
    my $rs = $self->{dbhandle}->dbExecute_NORETRY($sql);
    
    return $self;
}
    
sub updateBundleInfo {
    my ($self, $info) = (shift, shift);
    die "NO Valid DB HANDLE exists " unless exists($self->{dbhandle}) 
                                         && ref($self->{dbhandle}) eq 'XReport::DBUtil';
    my $sql = $self->substituteVars($info, $XReport::cfg->{sqlprocs}->{UpdateBundles}, @_ );
#    i::warnit("updateBundleInfo:\n".$sql);
    my $rs =  $self->{dbhandle}->dbExecute_NORETRY($sql);
    return $info;   
}   

sub updateQItem {
    my ($self, $info) = (shift, shift);
    die "NO Valid DB HANDLE exists " unless exists($self->{dbhandle}) 
                                         && ref($self->{dbhandle}) eq 'XReport::DBUtil';
    my $sql = $self->substituteVars($info, $XReport::cfg->{sqlprocs}->{UpdateQItem}, @_ );
#    i::warnit("updateQItem  :\n".$sql);
    my $rs =  $self->{dbhandle}->dbExecute_NORETRY($sql);
    return $info;   
}   

sub closeBundleLog {
    my ($self, $info) = (shift, shift);
    die "NO Valid DB HANDLE exists " unless exists($self->{dbhandle}) 
                                         && ref($self->{dbhandle}) eq 'XReport::DBUtil';
#    my $sql = $XReport::cfg->{sqlprocs}->{UpdateBundlesLog};
#    i::warnit("closeBundleLog:\n".$sql);
    my $sql = "INSERT INTO tbl_BundlesLog (BundleId, JobReportId, ReportId, RecipientName"
       . ", ElabLines, ElabPages, ElabStartTime, ElabEndTime, ReportTimeRef, textId, BQCopies)"
       . " SELECT $info->{BundleId} as BundleId, JobReportId, ReportId, RecipientName" 
       . ", PrintedLines as ElabLines, PrintedPages as ElabPages"
       . ", PrintStartTime as ElabStartTime, UpdateTime as ElabEndTime, ReportTimeRef, textId, BQCopies"
          . " FROM #LOGREPS";
     $sql .= " WHERE $self->{UserSpec} " if $self->{UserSpec} && $self->{UserSpec} !~ /^\s*$/;  
       ;
    $self->{dbhandle}->dbExecute_NORETRY($sql);
	
	#--delete of reportid = 1
	$sql =  " DELETE BQR1 "
           ." FROM tbl_BundlesQ BQR1 "
           ." WHERE BQR1.ReportId = 1  "
           ." AND exists (SELECT 1 from tbl_BundlesQ BQR2 "
           ." WHERE BQR2.BundleQId in (SELECT DISTINCT BundleQId from #LOGREPS )  "
           ." AND BQR1.BundleName =  BQR2.BundleName "
           ." AND BQR1.JobReportId =  BQR2.JobReportId "
           ." AND BQR1.ReportTimeRef =  BQR2.ReportTimeRef "
           ." AND BQR1.textId =  BQR2.textId) ";
	$main::verbose && i::warnit("query delete reportid1:\n".$sql);
    $self->{dbhandle}->dbExecute_NORETRY($sql);	
    $sql = "DELETE FROM tbl_BundlesQ WHERE BundleQId in (SELECT DISTINCT BundleQId from #LOGREPS ";
    $sql .= "WHERE $self->{UserSpec} " if $self->{UserSpec} && $self->{UserSpec} !~ /^\s*$/;
    $sql .= " )";
	$main::verbose && i::warnit("query delete:\n".$sql);
    $self->{dbhandle}->dbExecute_NORETRY($sql);
    $self->updateBundleEntry($info, Status => 18, 
                             ElabEndTime => XReport::BUNDLE::strftime("%Y-%m-%d %H:%M:%S.001", localtime));
    $self->{dbhandle}->DISCONNECT();
    return $info;   
}

sub updateWrkDir{
    my ($self, $binfo) = (shift, shift);
    #$self->{workdir} = $self->{workdir}."/".$binfo->{BundleId};
    $self->{cfgwrk} = $self->{workdir} unless exists($self->{cfgwrk});
    $self->{workdir} = $self->{cfgwrk}."\\".$binfo->{BundleId};
    i::logit("ADDING__$self->{workdir}\n");
    system("mkdir $self->{workdir}") unless -d $self->{workdir};
    #mkpath($self->{workdir}) unless  -d $self->{workdir};
    return $self;
}

sub clearOutPut{
    my ($self, $binfo) = (shift, shift);
    $self->{workdir} =~ s/\//\\/g;
    i::logit("CLEARING__$self->{workdir}\n");
    system("del /s /q $self->{workdir} && rmdir /S /Q $self->{workdir}") unless ! -d $self->{workdir};
    $self->{workdir} =~ s/^(\\\\.*|\w.*)?\\\d+$/$1/;
    i::logit("MATCH__$1\n");
    
    return $self;
}


sub processUserConfig {
    my ($self, $cfgname) = (shift, shift);
    $main::verbose && i::warnit("processUserConfig called by ", join('::', (caller())[0,2]), " to load $cfgname");
#    $self->loadUserMods($cfgname);
        #todo check if exists BundleName config and load it if true
        #todo check if config requires Usermods and load it if true
    (my $cfgfnam = $XReport::cfg->{userlib}.'/BUNDLESKEL/'.$cfgname.'.xml') =~ s/\\/\//g;

    return $self unless ( -e $cfgfnam );    

    my $usrcfg = c::parseXmlConfig($cfgfnam);
    XReport::BUNDLE::setPrintInfo($usrcfg, $self);
    
    c::mergeStruct($XReport::cfg, $usrcfg);
    $self->loadUserMods($usrcfg->{proclib}) 
                 if ( exists($usrcfg->{proclib}) && $usrcfg->{proclib} !~ /^\s*$/ );
    return $self;
}

sub loadUserMods {
    my ($self, $modname) = (shift, shift);
    return undef unless $modname;
    my @modlibs = ();
    
#    ($moddist = $self->{distlib}."/$modname.pl") =~ s/\//\\/g;
#    ($modusr = $XReport::cfg->{userlib}."/$modname.pl") =~ s/\//\\/g;
    foreach my $modfn ( ($self->{distlib}."/$modname.pl", 
                        $XReport::cfg->{userlib}."/BUNDLESBIN/$modname.pl"
                      ) ) {
                        $modfn =~ s/\//\\/g;
                        next unless -e $modfn;

                        push @modlibs, $modfn;
                        warn "loading $modfn\n";
                        require "$modfn";
                      }
                      warn "User Mod $modname not found\n" unless scalar(@modlibs);
                      return scalar(@modlibs);
}

sub getBundlesList {
    my $self = shift;
    die "NO Valid DB HANDLE exists " unless exists($self->{dbhandle}) 
                                         && ref($self->{dbhandle}) eq 'XReport::DBUtil';
    my $blist = [];
#    $bundle->{dbhandle} = XReport::DBUtil->new(DBNAME => 'XREPORT') unless exists($bundle->{dbhandle}) 
#                                         && ref($bundle->{dbhandle}) eq 'XReport::DBUtil';
#    my $query = $bundle->substituteVars($bundle, 
#        "SELECT *, convert(char(10), GETDATE(), 121) as odate"
#        . ", SUBSTRING(convert(char(30), GETDATE(), 121), 12, 8) as otime"
#        . " FROM tbl_BundlesNames WHERE BundleName LIKE '#{BundleName}#'"
#        . " AND BundleCategory LIKE '#{BundleCategory}#' AND BundleSkel LIKE '#{BundleSkel}#'" );
    my $query = $self->substituteVars($self, $XReport::cfg->{sqlprocs}->{BundlesNames}, @_ );
    i::warnit("Retrieving Bundle Names:\n$query");
    my $dbr = $self->{dbhandle}->dbExecute_NORETRY($query);
    while (!$dbr->eof()) {
        my $row = $dbr->GetFieldsHash();
#        $row->{dtstring} = join(''
#                         ,'D',  unpack('xa3xa2xa2', $row->{odate}) # 2013-07-23
#                         ,'.T', unpack('a2xa2xa2',  $row->{otime})
#                         );
        push @{$blist}, $row;
    }
    continue { $dbr->MoveNext(); }
#    warn "BLIST: ", Dumper($blist), "\n";
    return $blist;
}

sub extractBundlesFolders {
    my $self = shift; my $binfo = shift;
    die "NO Valid DB HANDLE exists " unless exists($self->{dbhandle}) 
                                         && ref($self->{dbhandle}) eq 'XReport::DBUtil';
    my ($flist, $fcount, $query) = ({}, 0, '');
    my $bkey = $binfo->{BundleKey};
    
#     $bundle->{dbhandle} = XReport::DBUtil->new(DBNAME => 'XREPORT') unless exists($bundle->{dbhandle}) 
#                                         && ref($bundle->{dbhandle}) eq 'XReport::DBUtil';
    $query =  join ("\n"
      , "SELECT EFR.*, EFR.FolderNameKey as RecipientName"
      , " FROM tbl_Folders AS EFR WITH (NOLOCK) "
      , " WHERE "
      , " exists(SELECT 1 FROM tbl_Folders AS EFN WITH (INDEX(IX_tbl_FolderRecipientName), NOLOCK)"
      , "    INNER JOIN tbl_VarSetsValues AS EVS WITH (NOLOCK) ON EFN.FolderNameKey like EVS.VarValue"
      , "    WHERE EVS.VarSetName = '$binfo->{BundleKey}' and EVS.VarName = ':BundleIncludePattern'"
      , "    and left(EFR.FolderName, LEN(EFN.FolderName)) = EFN.FolderName)"
      , " AND NOT exists(SELECT 1 FROM tbl_Folders AS EFN WITH (INDEX(IX_tbl_FolderRecipientName), NOLOCK)"
      , "    INNER JOIN tbl_VarSetsValues AS EVS WITH (NOLOCK) ON EFN.FolderNameKey like EVS.VarValue"
      , "    WHERE EVS.VarSetName = '$binfo->{BundleKey}' and EVS.VarName = ':BundleExcludePattern'"
      , "    and left(EFR.FolderName, LEN(EFN.FolderName)) = EFN.FolderName)"
        );

#    warn "extractBundlesFolders query:\n$query\n";    
    my $dbr =  $self->{dbhandle}->dbExecute_NORETRY($query);;
    while (!$dbr->eof()) {
        my $row = $dbr->GetFieldsHash();
        # warn "Found ", join('::', @{$row}{qw(BundleKey )}), "\n";
        $flist->{$bkey} = XReport::BUNDLE::setPrintInfo($row, 
               XReport::BUNDLE::setPrintInfo($binfo, 
               XReport::BUNDLE::setPrintInfo($self, { InputPages => 0, InputLines => 0, folders => {} } ) ) ) 
                                                                    if ( !exists($flist->{$bkey}) );
        $fcount++ unless exists($flist->{$bkey}->{folders}->{$row->{RecipientName}});
        $flist->{$bkey}->{folders}->{$row->{RecipientName}} = XReport::BUNDLE::setPrintInfo( $row, 
                                                              XReport::BUNDLE::setPrintInfo( $binfo, 
                                                              XReport::BUNDLE::setPrintInfo( $self, 
                                                     { InputPages => 0, InputLines => 0, reports => {} } ) ) );
    }
    continue { $dbr->MoveNext(); }
    
    my @bnames = keys %{$flist};
#    warn "processing List Contains ".scalar(@bnames)."(recipients: $fcount) Elements\n";
    return $flist;
}

=huge fonts

in order to make strings as huge fonts use Text::Figlets
colossal font will suite the need

my @lines = split /\x0a/, 
      Text::FIGlet->new(-f=>'colossal', -d=>"C:/Users/mpezzi/prova/figfonts",-m=>0)
                 ->figify(-A=>$RecipientName, -w=>133);

=cut

sub bundleInit {
    my $class = shift;
    
    my $parms = { @ARGV };
    my $self = { 
                dbhandle => XReport::DBUtil->new()
                ,dbcols => [qw( BundleName  BundleCategory BundleSkel BundleDest Destination ElabParameters
                           ElabStartTime ElabEndTime XferStartTime XferEndTime InputLines InputPages
                           ElabPages ElabLines XferOutBytes Status ) ]
                };
    ($self->{distlib} = __FILE__) =~ s/\.[^\.]+?$//;
    $self->{ElabParameters} = join(' ', @ARGV);
    $self->{ElabStartTime} = XReport::BUNDLE::strftime("%Y-%m-%d %H:%M:%S.001", localtime);
    $self->{odate} = XReport::BUNDLE::strftime("%d/%m/%Y", localtime);
    $self->{otime} = XReport::BUNDLE::strftime("%H:%M:%S.001", localtime);
    $self->{dtstring} = 'D'. substr( XReport::BUNDLE::strftime("%Y%m%d.T%H%M%S", localtime), 1 );
    $self->{Status} = 0;
    
    @{$self}{qw(workdir UserSpec dsnpfx cfgname BundleName execdest StartTimeOut)} 
                                        = @{$parms}{qw(-wrk -sel -pfx -N -bundle -dest -timeout)};
    die "Unable to access $self->{workdir} - $!" unless -d $self->{workdir};

    $self->{BundleName} = $self->{cfgname} unless defined($self->{BundleName}); 
    @{$self}{qw(BundleCategory BundleSkel)} = map { defined($_) ? $_ : '%' } @{$parms}{qw(-cat -skel)};
    
    $self->{BannerType} = $XReport::cfg->{BannerType};
    $self->{changeFirstASAchar} = (exists($XReport::cfg->{changeFirstASAchar}) ? $XReport::cfg->{changeFirstASAchar} : 0);
    $self->{maxPrintCopies} = (exists($XReport::cfg->{maxPrintCopies}) ? $XReport::cfg->{maxPrintCopies} : 4); 
	
    $self->{StartTimeOut} = '120' unless defined($self->{StartTimeOut}); 
    
    bless $self, $class;

    $self->loadUserMods('STANDARD');
    $self->loadUserMods($self->{cfgname});
#    if ( exists($XReport::cfg->{figletfonts}) ) {
#       foreach my $fnam ( split / /, $XReport::cfg->{figletfonts} ) {
            (my $figlib = $XReport::cfg->{userlib}.'/fonts/figlet') =~ s/\\/\//g;
            i::logit("Init FIGLET Module - FONTDIR: \"$figlib\"");
            $self->{figlet} = Text::FIGlet->new(-f=>$figlib.'/colossal.flf', -m=>0);
			$self->{figlet2} = Text::FIGlet->new(-f=>$figlib.'/banner3.flf', -m=>0);
#       }
#    }
#    warn "bundleInit SELF: ", Dumper($self), "\n";                            
    return $self;
}

sub bundleDelivery {
    my ($self, $binfo, $remotefn) = (shift, shift, shift);
#    warn "BINFO for BundleDelivery: ", Dumper($binfo), "\n";

    my ($destType, $destAddress) = ($binfo->{Destination} =~ /^\s*([^\s\/\:]+):\/\/(.+)$/);
    if ($destType eq 'NULLFILE' || $destType eq "JUSTKEEP") {
        return unlink $self->{outfilename} if $destType eq 'NULLFILE';
        return 1;
    }
    i::logit("Trying to load $destType handler to deliver Bundle(".(-s $binfo->{outfilename})." bytes) to $destAddress ($remotefn)");
    my $destclass = "XReport::Deliver::".$destType;
    
    eval "require $destclass;"; die $@ if $@;
    
    $binfo->{XferStartTime} = strftime("%Y-%m-%d %H:%M:%S.001", localtime);
    $binfo->{Status} = 33;
    $self->updateBundleEntry($binfo);

    if ( my $fulldelivery = $destclass->can('sendfile') ) {
        &{$fulldelivery}($destclass, %{$binfo}
                                , remote => $destAddress.'/'.$remotefn
                                , 'local' => $binfo->{outfilename}
        #                        , inputfh => $outfh
                                , inbuffcnt => 0
                                 );
        $binfo->{XferOutBytes} = -s $binfo->{outfilename};
    }
    else {    
        my $outfh = new IO::File("<$binfo->{outfilename}") 
                              || die "Unable to open \"$binfo->{outfilename}\" - $!";
    
        $binfo->{destfh} = $destclass->open(%{$binfo}
                                , remote => $destAddress.'/'.$remotefn
                                , 'local' => $binfo->{outfilename}
                                , inputfh => $outfh
                                , inbuffcnt => 0
                                 );
        my $buffcnt = 0;                                
        while ( !$outfh->eof() ) {
           $main::veryverbose and i::warnit("Writing now buffer ".(++$buffcnt)." of ".$binfo->{outfilename}." at ", $outfh->tell());
           $outfh->read(my $buff, 32760);
           $binfo->{destfh}->write($buff) if length($buff);
        }
        $main::veryverbose and i::warnit("Closing now Out Dest - buffer read: ".(++$buffcnt)." last Pos: ", $outfh->tell());
        $binfo->{XferOutBytes} = $binfo->{destfh}->{XferOutBytes};
        $binfo->{destfh}->close();
        $outfh->close();
    }
    $binfo->{XferEndTime} = strftime("%Y-%m-%d %H:%M:%S.001", localtime);
    $binfo->{Status} = 34;
    $self->updateBundleEntry($binfo);
    return $self;
}

sub endElab {
    my ($self, $binfo) = (shift, shift);
    $binfo->{ElabEndTime} = XReport::BUNDLE::strftime("%Y-%m-%d %H:%M:%S.001", localtime);
    $binfo->{Status} = 18;
    $self->updateBundleEntry($binfo);
    
}

=CINFO NOT NEEDED 
sub substituteVars {
    my ($self, $cinfo, $cfgstring) = (shift, shift, shift);
#   warn "subst called by ", join('::', (caller())[0,2]), "\n";
    my $prtvars = {};
    foreach my $k ( keys %$cinfo ) {
        next if ( $k eq 'finfo' ) ;
#       warn "setting $k to $cinfo->{$k}\n";
=solve path name
        $prtvars->{$k} = ( !$cinfo->{$k} ? '' 
                   : $cinfo->{$k} =~ /^(.*?\\)?([^\\]+\\[^\\]+)$/ ? $1 : $cinfo->{$k} );
#       my @val = (split /\\/, $cinfo->{$k});
#       $prtvars->{$k} = (scalar(@val) > 1 ? 
#           join("\\", @val[-2,-1]) : join('', @val));
    } 
    foreach my $k ( keys %{$cinfo->{'finfo'}} ) {
#        warn "setting $k to $cinfo->{finfo}->{$k}\n";
        $prtvars->{$k} = ( $cinfo->{'finfo'}->{$k} =~ /^(.*?\\)?([^\\]+\\[^\\]+)$/ ? $1 
                                                                   : $cinfo->{'finfo'}->{$k} );
#       my @val = (split /\\/, $cinfo->{'finfo'}->{$k});
#       $prtvars->{$k} = (scalar(@val) > 1 ? 
#       join("\\", @val[-2,-1]) : join('', @val));
    }
    
    while ( scalar(@_) ) {
        my ( $k, $v) = (shift, shift);
 #       warn "setting $k to $v\n";
        $prtvars->{$k} = $v;
    }
    use Data::Dumper;
#    warn "cinfo:", Dumper($cinfo), "\nprtvars:", Dumper($prtvars), "\n";
    my @outlines = ();
    foreach my $line ( split /\n/, $cfgstring ) {
        if ( $line =~ /\#\{(.+?)\}\#/ ) {
      warn "line tochange: ", $line, "\n";
          $line =~ s/\#\{(.+?)\}\#/$prtvars->{$1}/g;
#          warn "line changed: ", $line, "\n";
        }
        push @outlines, $line;
    }
    return join("\n", ( @outlines ));
}
=cut

sub substituteVars {
    my ($self, $info, $cfgstring) = (shift, shift, shift);
#   warn "subst called by ", join('::', (caller())[0,2]), "\n";
    my $prtvars = { %$info, @_ };

    use Data::Dumper;
    return join("\n", map { (my $l = $_) =~ s/\#\{(.+?)\}\#/$prtvars->{$1}/g; $l } 
                                                                  split /\n/, $cfgstring);   
}

sub dsnpfx {
    my ($self, $binfo) = (shift, shift);
    return (
      exists($binfo->{dsnpfx}) && defined($binfo->{dsnpfx}) && $binfo->{dsnpfx} !~ /^\s*$/ ? $binfo->{dsnpfx} 
       : exists($self->{dsnpfx}) && defined($self->{dsnpfx}) && $self->{dsnpfx} !~ /^\s*$/ ? $self->{dsnpfx}
       : 'XRBUNDLE' );
}

sub BundleFileSfx {
    my ($self, $binfo) = (shift, shift);
#    warn "DSN BINFO: ", Dumper($binfo), "\n";
    my ($namecat, $skedtime) = split /\/T/, $binfo->{BundleCategory}, 2;
    $skedtime = '0000' unless defined($skedtime);
    my $dtstring = $binfo->{dtstring};
    $dtstring = join('', unpack( 'axa7a9',
        XReport::BUNDLE::strftime("D%Y%m%d.T%H%M%S", localtime) )) unless $dtstring;
        
#    return join('.', $binfo->{BundleName}, $namecat, 'SKED'.$skedtime, $self->{dtstring});
#    return join('.', $binfo->{BundleName}, $binfo->{BundleSkel}, $binfo->{dtstring});
#   return join('.', $binfo->{BundleName}, $binfo->{BundleSkel}, $dtstring);
    return join('.', $binfo->{BundleName}, $dtstring);
}

sub LocalFileName {
    my ($self, $binfo) = (shift, shift);
    my ($namecat, $skedtime) = split /\/T/, $binfo->{BundleCategory}, 2;
    $skedtime = '0000' unless defined($skedtime);
    
    return $self->dsnpfx($binfo).'.SKED'.$skedtime.'.'.$self->BundleFileSfx($binfo);
}

=h2. Encodings

CodePages EBCDIC
    EURO  Countries
037 1140    Australia, Brazil, Canada, New Zealand, Portugal, South Africa, USA
273 1141    Austria, Germany
277 1142    Denmark, Norway
278 1143    Finland, Sweden
280 1144    Italy
284 1145    Latin America, Spain
285 1146    Ireland, United Kingdom
297 1147    France
500 1148    International
871 1149    Iceland
1047 924    Open Systems ( MVS C compiler )

Codepages ASCII
437 (US)
720 (Arabic)
737 (Greek)
775 (Baltic)
850 (Multilingual Latin I)
852 (Latin II)
855 (Cyrillic)
857 (Turkish)
858 (Multilingual Latin I + Euro)
862 (Hebrew)
866 (Russian)
1250 (Central Europe)
1251 (Cyrillic)
1252 (Latin I)
1253 (Greek)
1254 (Turkish)
1255 (Hebrew)
1256 (Arabic)
1257 (Baltic)
1258 (Vietnam)
874 (Thai)
 

=cut

sub outputSetup {
    my ($self, $binfo) = (shift, shift);

    $binfo->{outfilename} = $self->{workdir}.'/'.$self->LocalFileName($binfo);
    $binfo->{xlator} = sub { return $_[0]; }; # new Convert::EBCDIC();
    $binfo->{encoding} = 'E';
    $binfo->{outmode} = 'B';

    return $self;
}

sub addReport2Bundle {
  my $self = shift;
  my $infile = shift;
  my ($jinfo, $streamer, $binfo) = (shift,shift,shift);
  my $inhndr = IO::File->new("<$infile") || die "Unable to open \"$infile\" - $!";
  $inhndr->binmode(); 
  i::logit("Now adding \"$infile\" to OUTFILE for $jinfo->{RecipientName} at ".$streamer->tell());
  my $leftovers = '';
  while ( !$inhndr->eof() ) { 
    $inhndr->read(my $buff, 32760);
    my ($parsedlen, $outdata, @inrows) = (0, '', unpack('(n X2 n/a)*', $leftovers.$buff));
    while (scalar(@inrows) ) {
      if ( (scalar(@inrows) == 1) or (scalar(@inrows) == 2 && $inrows[0] > length($inrows[1]) ) ) { 
        $leftovers = substr($leftovers.$buff, $parsedlen);
        last;
      }
      my ($rowlen, $rowdata) = splice @inrows, 0, 2;
      $parsedlen += $rowlen + 2;
      $outdata .= packrec($rowdata);
    }
    $self->writeBuff($binfo, $outdata ) if length($outdata);
  }
  return $inhndr->close();
}

sub extractReportPages {
  my $self = shift;
  my ($rinfo, $pagesptr) = (shift, shift);
  my $pagelist = [ @$pagesptr ];

  my $JRID = $pagelist->[0];
  
  @{$rinfo}{qw(InputPages InputLines)} = (0, 0);
  my $lrkey = join('.', @{$rinfo}{qw(JobReportId ReportId)});
  if ( exists($self->{logicalreports}->{$lrkey}) ) {
    @{$rinfo}{qw(rfilename InputPages InputLines)} = @{$self->{logicalreports}->{$lrkey}};
    $main::verbose && i::warnit("$lrkey already extracted from ARCHIVE returning $rinfo->{rfilename}");
    return $rinfo->{rfilename};
  }
  my $newinfo = $self->initIOFiles($rinfo, $JRID, $lrkey);
  return undef unless $newinfo;
  my ($jr, $streamer) = @{$newinfo}{qw(extract streamer)};
  
  my ($totpages, $totlines) = (0, 0);
  while ( scalar(@$pagelist) ) {
    my ($JobReportId, $FromPage, $ForPages) = splice @$pagelist, 0, 3;
    die "ERROR: Unsupported Extract from multiple JobReportIDs - FIRST JRID: $JRID Current: $JobReportId" 
                                                                               unless $JobReportId == $JRID;
    my ($pages, $lines);
#    eval { ($pages, $lines) = &{$rinfo->{extract}}($jr, $FromPage, $ForPages, tostreamer => $rinfo->{streamer} ) };

	#i::warnit("TESTGIU - BundleName=".$self->{BundleName});
    eval {  
		#my $changeFirstASAchar = 1 if($self->{BundleName} =~ /^XREPORTN$/i);
		my $changeFirstASAchar = $self->{changeFirstASAchar};
		#i::warnit("TESTGIU - changeFirstASAchar=".$changeFirstASAchar);
		($pages, $lines) = $jr->extract_pages($FromPage, $ForPages, tostreamer => $rinfo->{streamer}, changeFirstASAchar => $changeFirstASAchar ) 
	};
    if ( my $errmsg = $@ || !defined($lines) ) {
            i::warnit("Extract pages error for JR $JRID (pages $ForPages from $ForPages) ".($errmsg ? " - $errmsg" : ''));
        $rinfo->{streamer}->close();
#        $jr->Close();
        unlink $rinfo->{rfilename};
        return undef;
    }
#   warn "extract pages returned $pages pages $lines lines\n";
    $totpages += $pages;
    $totlines += $lines;
  }
  
  $rinfo->{InputPages} = $totpages;
  $rinfo->{InputLines} = $totlines;

  $rinfo->{streamer}->close();
#  $jr->Close();
  if ( $totpages == 0 && $totlines == 0 ) { unlink $rinfo->{rfilename}; return undef };
  
  $self->{logicalreports}->{$rinfo->{lrkey}} = [@{$rinfo}{qw(rfilename InputPages InputLines)} ] if !exists($self->{logicalreports}->{$rinfo->{lrkey}});
  return $rinfo->{rfilename};
}

sub setPrintInfo {
    my ($instruct, $outstruct) = (shift, shift);
    my @initcols = qw(BundleName BundleCategory BundleSkel BundleKey
                      FolderName RecipientName UserRef
                      ElabParameters
                      ReportName JobReportId ReportId XferStartTime 
                      UserTimeElab UserTimeRef TotPages JobTimeRef
                      JobName JobNumer odate otime dtstring
                      FolderAddr.1 FolderAddr.2 FolderAddr.3 
                      FolderAddr.4 FolderAddr.5 FolderAddr.6
                      FolderAddr.7 FolderAddr.8 FolderAddr.9
                      SpecialInstr.1 SpecialInstr.2 SpecialInstr.3 
                      SpecialInstr.4 SpecialInstr.5 SpecialInstr.6
                      SpecialInstr.7 SpecialInstr.8 SpecialInstr.9
                      outfilename BundleDest Destination
                      );

    for ( @initcols ) {
        $outstruct->{$_} = $instruct->{$_} if exists($instruct->{$_} );
    }
#    warn "setInfo struct: ", Dumper($outstruct), "\n";
    return $outstruct;
}

sub insertBundleQ {
    my $self = shift;
#    die "NO Valid DB HANDLE exists " unless exists($self->{dbhandle}) 
#                                         && ref($self->{dbhandle}) eq 'XReport::DBUtil';
    my $args = { @_ };
    $main::veryverbose && i::logit("insertBundleQ args: ", join('::', @_), "\n");
    $args->{UserTimeRef} = 'GETDATE()' if (!exists($args->{UserTimeRef}) 
                                           || !$args->{UserTimeRef} 
                                           || $args->{UserTimeRef} eq 'NULL' );
    $args->{UserTimeRef} =~ s/^'|'$//g;
#TODO TimeREf must be computed from a WorkShift definition from main config 
    $args->{UserRef} =~ s/'/''/g;
    my $sql = "INSERT INTO tbl_BundlesQ (BundleName, JobReportId, ReportId, ReportTimeRef, PrintCopies, TextId) VALUES("
     . join(',', map { $_ ? "'".$_."'" : 'NULL' } @{$args}{qw(name JobReportId ReportId UserTimeRef copies)} )
     . ", (SELECT TextId from dbo.tbl_LogicalReportsTexts where textString = '".$args->{UserRef}."'))";
#    warn "BUNDLESQ SQL: $sql\n";
    my $dbexec = XReport::DBUtil->can('dbExecute_NORETRY');
    my $rs = &$dbexec((ref($self) && exists($self->{dbhandle}) && ref($self->{dbhandle}) eq 'XReport::DBUtil') 
            ? ( $self->{dbhandle}, $sql ) : $sql ); 
    
}


sub NewLogicalReport {
    my $self = shift;
    my $args = { @_ };
     $main::veryverbose && i::logit( "Bundle NewLogicalReport caller: ", join('::', (caller())[0,2]), "\n");
    #warn "Bundle NewLogicalReport ARGS:", Dumper(\{ @_ });
    foreach my $bundle ( @{$args->{bundles}} ) {
        my ($name, $copies) = split /\//, $bundle, 2;
        insertBundleQ($self, name => $name, copies => $copies, @_ );
    }
}
__PACKAGE__;

