package XReport::ghostscript;

use strict;
use IO::File;
use File::Basename;

use XReport::SUtil;

our $VERSION = '0.10';

sub FileToPdf {
  my ($PSFileName, $PdfFileName) = (shift, shift);
  my ($basedir, $afpdir) = @{$XReport::cfg}{qw(basedir afpdir)}; 
  i::warnit("afpdir: $afpdir - basedir: $basedir");
  $afpdir = "$basedir/afp" if !$afpdir;
  $basedir =~ s/\//\\/g; $afpdir =~ s/\\/\//g;
  my $mode = ($XReport::cfg->{distiller}->{mode} or '_UNDEF_');
  i::warnit("mode: $mode afpdir: $afpdir - basedir: $basedir");
  
  my $origin = $basedir;
  $origin = $ENV{XREPORT_HOME} unless -e "$basedir\\bin\\xreport.ps";

  ($PdfFileName = $PSFileName) =~ s/\.\w+$/.pdf/ unless $PdfFileName;
  if ( -e $PSFileName && (my $PSINPUT = IO::File->new(">>$PSFileName")) ) {
    print $PSINPUT
    "(\$main::currentsystemparams = {\\n) print\n"
          ," currentsystemparams { \%forall\n"
      ,"    exch dup \% name first\n"
      ,"    dup length string cvs print ( => [qw�) print\n"
      ,"    exch ==\n"
      ,"    (�],\\n) print\n"
      ,"  } forall\n"
      ,"  (vmusage => { qw�val1 ) print vmstatus 10 string cvs print \n"
      ,"  ( val2 ) print 10 string cvs print\n"
      ,"  ( val3 ) print 10 string cvs print (� },\\n) print\n"
      ,"(};\\n) print flush\n"
    ;
    close $PSINPUT;  
  }

  #my @infiles = ( @_, $PSFileName);
  my @infiles = ( @_ );
  push @infiles , $PSFileName unless $mode eq "NO_IN_PS";
  my $progr = $XReport::cfg->{distiller}->{gsexec};
  die "XXXXX: ", Dumper($XReport::cfg->{distiller}) unless $progr;
  i::warnit("GS: $progr - switches: $XReport::cfg->{distiller}->{gsargs} F1: $PSFileName OTHERS: ". join('::', @_));
  return -1 unless ($progr && -e $progr);
  
  my $psdir = dirname($PSFileName);
  my @filelvls = split(/\./, basename($PdfFileName));
  pop @filelvls;pop @filelvls;pop @filelvls;
  my $jrid = pop @filelvls;
  my $jrname = join('.', @filelvls);
  (my $PSLOGName = $PSFileName) =~ s/\.ps$/\.log/i;
  my @gsParms = (); # ('/F') for v8+
  push @gsParms, ( (qw(-dBATCH -dNOPAUSE -sDEVICE=pdfwrite ), split(/ /, ($XReport::cfg->{distiller}->{gsargs} || '')))
#   , "-sLocalResources=$psdir/localres/"
#   , "-sAfpDir=$afpdir"
   , "-sOutputFile=\"$PdfFileName\""
#   , "-sFONTPATH=$psdir/localres/;$afpdir/fonts/userfonts/charsets.outline/;$afpdir/fonts/userfonts/charsets.bitmap/;$afpdir/fonts/userfonts/;"
#   , "-I$psdir/localres/;$afpdir/FDEFLIB/;$afpdir/PDEFLIB/;$afpdir/OVERLIB/;$afpdir/PSEGLIB/"
   , "-sstdout=\"$PSLOGName\""
   , map { "\"$_\""} @infiles
  );                   
  i::warnit("GS PARMS: ".join(' ', @gsParms));
  my $DistRetCode = XReport::SUtil::spawnProcessAndWait($progr, 10800000, @gsParms);
#  CORE::open(my $savout, ">&STDOUT") or die "error: save original STDOUT: $!";
#  CORE::open(SAVERR, ">&", \*STDERR ) or die "error: save original STDERR: $!";
#  CORE::open(STDOUT, '>', $PSLOGName) or die "error: create  '$PSLOGName': $!";
#  CORE::open(STDERR, '>&STDOUT')    or die "error: redirect '$PSLOGName': $!";
#  select STDERR; $| = 1;  # make unbuffered
#  select STDOUT; $| = 1;  # make unbuffered
#  system { $progr } @gsParms;
#  my $DistRetCode = $? >> 8;
#  CORE::open(STDOUT, ">&", $savout) or die "error: restore STDOUT: $!";
#  CORE::open(STDERR, ">&SAVERR") or die "error: restore STDERR: $!";
#  CORE::close(SAVERR) or die "error: close SAVERR: $!";
#  CORE::close(SAVOUT) or die "error: close SAVOUT: $!";
  if ( -e $PSLOGName && ( my $PSLOG = IO::File->new( "<$PSLOGName" )) ) {
     my $stats = ''; my $stats_begin = qr/^\s*\$main::currentsystemparams\s*=\s*\{\s*$/;
     while ( <$PSLOG> ) {
         $stats = $_ if ($_ =~ /$stats_begin/ );
         warn "DISTILLER LOG: $_" unless $stats;;
         $stats .= $_ if $stats && $_ !~ /$stats_begin/;
     }
     if ($stats) {
       $stats =~ s/^\s*\$main::currentsystemparams\s*=\s*(\{.+\}).*$/$1/s;
       eval "\$main::distllr_stats = $stats";
     }
     close $PSLOG;
 } 
  i::warnit("GS ENDED with RC: $DistRetCode ");
  return $DistRetCode;
  
}
