
package XReport::Excel::UtilHtml;

use strict;

use Symbol;

my $lx_xrvars = qr/(?:JobReportid|FromPage|ForPages|FromLine|ForLines)/i;

sub getIsoDateTime {
  my @g = localtime(time());
  $g[5] += 1900; $g[4] += 1; 
  for (0..2) {
    $g[$_] = substr("0$g[$_]",-2);
  }
  return "$g[5]-$g[4]-$g[3]T$g[2]:$g[1]:$g[0]Z";
}

sub Header { 
  my %args = @_; 

  my (
    $ExcelCols, 
    $o_Created, $o_LastSaved, 
    $x_Name, $x_Name_HRef
  ) = 
  @args{qw(ExcelCols o_Created o_LastSaved x_Name x_Name_HRef)};

  my $x_FormatSettings = "<x:FormatSettings>\n";
  for (1..$ExcelCols) {
    $x_FormatSettings .= "<x:FieldType>AutoFormat</x:FieldType>\n";
  }
  $x_FormatSettings .= "</x:FormatSettings>";

<<EOF;
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>

<meta http-equiv="Content-Type" content="text/html;" charset="windows-1252"/>
<meta name="ProgId" content="Excel.Sheet"/>
<meta name="Generator" content="Microsoft Excel 9"/>

<xml>
 <o:DocumentProperties>
  <o:Author>Walter Borchia</o:Author>
  <o:LastAuthor>Walter Borchia</o:LastAuthor>
  <o:Created>$o_Created</o:Created>
  <o:LastSaved>$o_LastSaved</o:LastSaved>
  <o:Company>Euriskom s.r.l.</o:Company>
  <o:Version>0.8</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:DownloadComponents/>
 </o:OfficeDocumentSettings>
</xml>

<style>
  table {
    mso-displayed-decimal-separator:"\.";
    mso-displayed-thousand-separator:"\,";
  }
  
  \@page {
    margin:1.0in .75in 1.0in .75in;
    mso-header-margin:.5in;
    mso-footer-margin:.5in;
  }
  tr {
    mso-height-source:auto;
  }
  col {
    mso-width-source:auto;
  }
  br {
    mso-data-placement:same-cell;
  }
  
  .style0 { 
    mso-number-format:General;
    text-align:general;
    vertical-align:bottom;
    white-space:nowrap;
    mso-rotate:0;
    mso-background-source:auto;
    mso-pattern:auto;
    color:windowtext;
    font-size:10.0pt;
    font-weight:400;
    font-style:normal;
    text-decoration:none;
    font-family:Tahoma, Arial, sans-serif;
    mso-generic-font-family:auto;
    mso-font-charset:0;
    border:none;
    mso-protection:locked visible;
    mso-style-name:Normal;
    mso-style-id:0;
  }
  td {
    mso-style-parent:style0;
    padding-top:1px;
    padding-right:1px;
    padding-left:1px;
    mso-ignore:padding;
    color:windowtext;
    font-size:10.0pt;
    font-weight:400;
    font-style:normal;
    text-decoration:none;
    font-family:Tahoma, Arial, sans-serif;
    mso-generic-font-family:auto;
    mso-font-charset:0;
    mso-number-format:General;
    text-align:general;
    vertical-align:bottom;
    border:none;
    mso-background-source:auto;
    mso-pattern:auto;
    mso-protection:locked visible;
    white-space:nowrap;
    mso-rotate:0;
  }
  .xlbold {
    mso-style-parent:style0;
    font-weight:bold;
    font-family:Tahoma, Arial, sans-serif;
  }
  .xlplain {
    mso-style-parent:style0;
    mso-number-format:Standard;
    font-family:Tahoma, Arial, sans-serif;
  }
  .xldec2 {
    mso-style-parent:style0;
    font-family:Tahoma, Arial, sans-serif;
	mso-number-format:"\#\,\#\#\#.00";
  }
</style>

<xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
    <x:Name>CCG00490</x:Name>
    <x:WorksheetOptions>
     <x:Print>
      <x:ValidPrinterInfo/>
      <x:PaperSizeIndex>9</x:PaperSizeIndex>
      <x:HorizontalResolution>600</x:HorizontalResolution>
      <x:VerticalResolution>0</x:VerticalResolution>
     </x:Print>
     <x:Selected/>
     <x:LeftColumnVisible>0</x:LeftColumnVisible>
     <x:Panes>
      <x:Pane>
       <x:Number>1</x:Number>
       <x:ActiveCol>0</x:ActiveCol>
       <x:RangeSelection>1:1</x:RangeSelection>
      </x:Pane>
     </x:Panes>
     <x:ProtectContents>False</x:ProtectContents>
     <x:ProtectObjects>False</x:ProtectObjects>
     <x:ProtectScenarios>False</x:ProtectScenarios>
    </x:WorksheetOptions>
    <x:QueryTable>
     <x:Name>$x_Name</x:Name>
     <x:AutoFormatFont/>
     <x:AutoFormatPattern/>
     <x:QuerySource>
      <x:QueryType>Text</x:QueryType>
      <x:TextWizardSettings>
       <x:Name HRef="$x_Name_HRef"/>
       <x:Decimal>.</x:Decimal>
       <x:ThousandSeparator>,</x:ThousandSeparator>
        $x_FormatSettings 
       <x:Delimiters>
        <x:Tab/>
       </x:Delimiters>
      </x:TextWizardSettings>
     </x:QuerySource>
    </x:QueryTable>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>9765</x:WindowHeight>
  <x:WindowWidth>15135</x:WindowWidth>
  <x:WindowTopX>120</x:WindowTopX>
  <x:WindowTopY>90</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml>

</head>


<body link="blue" vlink="purple">

<table x:str="1">

EOF
}

sub Trailer {
 "</table></body></html>"; 
}

### todo: create format classes dynamically

sub TsvFile2ExcelHtml {
  my %args = @_; 

  my (
    $JobReportName, $FieldFormats,
    $TsvFileName, $ExcelHtmlFileName, 
  ) = 
  @args{qw(JobReportName FieldFormats TsvFileName ExcelHtmlFileName)};

  my ($INPUT, $OUTPUT) = (gensym(), gensym());

  open($INPUT, "<$TsvFileName") 
    or 
  die("EXCELHTML INPUT FILE OPEN ERROR \"$TsvFileName\" rc = $!"); binmode($INPUT);

  open($OUTPUT, ">$ExcelHtmlFileName") 
   or 
  die("EXCELHTML OUTPUT FILE OPEN ERROR \"$ExcelHtmlFileName\" rc = $!"); binmode($OUTPUT);

  my $header = <$INPUT>; $header =~ s/\s+$//;

  my @HeaderFields = grep {$_ !~ /^$lx_xrvars$/i} split("\t", $header);

  my $ExcelCols = scalar(@HeaderFields); 

  my $dateTime = getIsoDateTime();

  DO_HEADER: do {
    print $OUTPUT Header(
      ExcelCols   => $ExcelCols,
      o_Created   => $dateTime,
      o_LastSaved => $dateTime,  
      x_Name      => $JobReportName,
      x_Name_HRef => "http://xreport/getObject.asp",
    );
  
    my $tableHeader = "<tr>\n";
  
    for (@HeaderFields) {
      $tableHeader .= "<td class=\"xlbold\">$_</td>\n";
    }
    $tableHeader .= "</tr>";
  
    for my $Field (@HeaderFields) {
      my $Format = $FieldFormats->{$Field};
      my $Class = ($Format =~ /^D/) ? "class=\"xldec2\" " : "" ;
  
      if ($Format =~ /^[IDZ]/) {
        print $OUTPUT "<col $Class align=\"right\"/>\n";
      }
      else {
        print $OUTPUT "<col align=\"left\"/>\n";
      }
  
    }
    print $OUTPUT $tableHeader, "\n";
  };

  DO_ROW: while(!eof($INPUT)) {
    my $tx = <$INPUT>; chomp $tx; my $tr = "<tr class=\"xlplain\">\n";
    my @Values = split(/\t/, $tx); shift @Values;
    for (0..$ExcelCols-1) {
      my $Field = $HeaderFields[$_];
      my $Value = shift @Values; 
      my $Format = $FieldFormats->{$Field};

      if ($Format =~ /^[IDZ]\d/) {
        my $Class = ($Format =~ /^D/) ? "class=\"xldec2\"" : "" ;
        $tr .= "<td $Class x:num>$Value</td>\n";
      }
      elsif ($Format =~ /^[C]/) {
        $tr .= "<td x:str>$Value</td>\n";
      }
      else {
        $tr .= "<td x:str>$Value</td>\n";
      }

    } 
    $tr .= "</tr>";
    print $OUTPUT "$tr\n";
  }

  DO_TRAILER: do {
    print $OUTPUT Trailer(), "\n"; 
  };

  close($OUTPUT);
  close($INPUT);
}

1;
