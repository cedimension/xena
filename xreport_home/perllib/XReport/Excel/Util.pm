
package XReport::Excel::Util;

use strict;

use Symbol;

use Spreadsheet::WriteExcel;

my $lx_xrvars = qr/(?:JobReportid|FromPage|ForPages|FromLine|ForLines)/i;

sub MAX {
  $_[0] >= $_[1] ? $_[0] : $_[1];
}

sub CreFieldFormats {
  my ($workbook, $FieldFormats) = @_; 

  my %FieldFormats = %$FieldFormats;

  for my $Field (keys(%$FieldFormats)) {
    my $Format = $FieldFormats->{$Field};
    my ($c, $l, $p) = $Format =~ /^([^\d]+)(\d+)\.(\d+)?/;
    my ($isNumber, $hdrFormat, $fldFormat) = (0, undef, undef);
    if ($c =~ /^[I]/) {
      $hdrFormat = $workbook->add_format(bold => 1, align => 'right');
      $isNumber = 1;
    }
    elsif ($c =~ /^[Z]/) {
      $isNumber = 1;
      $hdrFormat = $workbook->add_format(bold => 1, align => 'right');
      $fldFormat = $workbook->add_format(num_format => '0'x$l);
    }
    elsif ($c =~ /^[D]/) {
      $isNumber = 1; 
      $hdrFormat = $workbook->add_format(bold => 1, align => 'right');
      $fldFormat = $workbook->add_format(num_format => '#,##0.'.('0'x$p));
    }
    else {
      $hdrFormat = $workbook->add_format(bold => 1);
    }

    $FieldFormats{$Field} = { 
       length => $l, isNumber => $isNumber, hdrFormat => $hdrFormat, fldFormat => $fldFormat 
    };
  }

  return %FieldFormats;
}

sub AddWorkSheet {
  my ($workbook, $ExcelName, $HeaderFields, $FieldFormats) = @_; 

  my $worksheet   = $workbook->addworksheet($ExcelName);

  my $col = 0;
  
  for my $Field (@$HeaderFields) { 
    my ($length, $hdrFormat) = @{$FieldFormats->{$Field}}{qw(length hdrFormat)};
    $worksheet->write(0, $col, $Field, $hdrFormat); 
    $worksheet->set_column($col, $col, MAX(length($Field),$length)); $col += 1;
  };

  return $worksheet;
}

sub AddTsvFile {
  my ($workbook, $args) = (shift, shift); my $INPUT = gensym();

  my (
    $ExcelName, $FieldFormats, $TsvFileName 
  ) = 
  @{$args}{qw(ExcelName FieldFormats TsvFileName)};
  
  open($INPUT, "<$TsvFileName") 
    or 
  die("EXCEL WRITE INPUT FILE OPEN ERROR \"$TsvFileName\" rc = $!"); binmode($INPUT);
  
  my $header = <$INPUT>; $header =~ s/\s+$//;

  my $HeaderFields = [grep {$_ !~ /^$lx_xrvars$/i} split("\t", $header)];

  my $ExcelCols = scalar(@$HeaderFields); 

  my %FieldFormats = CreFieldFormats($workbook, $FieldFormats);
  
  my $worksheet = AddWorkSheet($workbook, $ExcelName, $HeaderFields, \%FieldFormats);

  my ($next, $row, $col) = (2, 0, 0);

  while(!eof($INPUT)) {
    my $tx = <$INPUT>; chomp $tx; 
    my @Values = split(/\t/, $tx); shift @Values;
	
    $row += 1;
	if ( $row > 65535 ) {
      $worksheet = AddWorkSheet($workbook, "$ExcelName\#$next", $HeaderFields, \%FieldFormats);
      $next += 1; $row = 1;
	}
	$col=0;

    for (0..$ExcelCols-1) {
      my $Field = $HeaderFields->[$_];
      my $Value = shift @Values; 
      my ($isNumber, $fldFormat) = @{$FieldFormats{$Field}}{qw(isNumber fldFormat)};

      if ( $isNumber ) {
        $worksheet->write_number($row, $col, $Value, $fldFormat); 
      }
      else {
        $worksheet->write_string($row, $col, $Value); 
      }

      $col+=1;
    }
  }

  close($INPUT);
}

sub TsvFile2Excel {
  my %args = @_; my ($ExcelSheets, $ExcelFileName) = @args{qw(ExcelSheets ExcelFileName)};

  my ($workbook, $tsvsize) = (0, undef);
  
  for (@$ExcelSheets) {
     $tsvsize += -s $_->{'TsvFileName'}
  }
  if ($tsvsize <= 1700000) {
    $workbook = Spreadsheet::WriteExcel->new($ExcelFileName)
  }
  else {
    require Spreadsheet::WriteExcel::Big;
    $workbook = Spreadsheet::WriteExcel::Big->new($ExcelFileName)
  }

  #$workbook->set_tempdir("c:/temp");

  for (@$ExcelSheets) {
    AddTsvFile($workbook, $_);
  }
  
  $workbook->close();
}

1;
