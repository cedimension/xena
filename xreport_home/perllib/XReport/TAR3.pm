

#------------------------------------------------------------
package XReport::TAR::IZlibGZREAD;

use strict; use bytes;

use IO::File;
use Compress::Zlib;

sub Open {
  my $self = shift; my ($GZINPUT, $INPUT, $libFileName)  = @{$self}{qw(GZINPUT INPUT libFileName)};
  
  $INPUT->seek($self->{fileOffset}, 0);
  
  $GZINPUT = gzopen($INPUT, "rb") 
   or 
  die "GZOPEN GZINPUT OPEN ERROR \"$libFileName\" $!\n"; 
  
  $self->{GZINPUT} = $GZINPUT; $self->{bytesAvail} = $self->{fileSize}; return $self;
}

sub Close {
  my $self = shift; my ($GZINPUT, $INPUT) = @{$self}{qw(GZINPUT INPUT)};

  my $rc = $GZINPUT->gzclose(); 
  
  @{$self}{qw(GZINPUT bytesAvail)} = (undef, 0); return $rc;
}

sub read {
  my $self = shift; my $length = $_[1]; my $l1 = $length;
  
  if ( $length > $self->{bytesAvail} ) {
    $length = $self->{bytesAvail}; 
  }
  return 0 if $length == 0;
  
  $length = $self->{GZINPUT}->gzread($_[0], $length);
  
  $self->{bytesAvail} -= $length;
  
  return $length;
}

sub tell {
  my $self = shift; my $GZINPUT = $self->{GZINPUT}; return $GZINPUT->gztell();
}

sub seek {
  my $self = shift; my $GZINPUT = $self->{GZINPUT};
  my $before = $GZINPUT->gztell();
  my $rc = $GZINPUT->gzseek($_[0], $_[1]);
  my $after = $GZINPUT->gztell();
  $self->{bytesAvail} += $before - $after;
  return $rc;
}

sub binmode {
}

sub desiredCompressionMethod {
}

sub eof {
  return $_[0]->{bytesAvail} == 0;
}

sub fileName {
  return $_[0]->{fileName};
}

sub new {
  my ($class, $INPUT, $file, $libFileName) = @_; my $self = {};
  
  @{$self}{qw(INPUT libFileName fileName fileOffset fileSize bytesAvail)} 
    = 
  ($INPUT, $libFileName, @{$file}{qw(fileName fileOffset fileSize)}, 0); bless $self;
}

#------------------------------------------------------------
package XReport::TAR::IZlibINFLATE;

use strict; use bytes;

use Symbol;
use Carp::Assert;
use Compress::Zlib;

sub Open {
  my $self = shift; my ($INPUT, $libFileName)  = @{$self}{qw(INPUT libFileName)};
  
  $INPUT->seek($self->{fileOffset}, 0);
  
  $self->{buffer} = "";
  $self->{bytesAvail} = $self->{fileSize};
  $self->{bytesAvailIN} = $self->{fileCSize};
  
  ($self->{inflater}, my $zstatus) = inflateInit( '-WindowBits' => -MAX_WBITS() ); return $self;
}

sub Close {
  my $self = shift; $self->{buffer} = ""; $self->{bytesAvail} = 0; return $self;
}

sub read {
  my $self = shift; $_[0] = ""; my $length = $_[1]; my ($INPUT, $inflater) = @{$self}{qw(INPUT inflater)};
  
  if ( $length > $self->{bytesAvail} ) {
    $length = $self->{bytesAvail}; 
  }
  return 0 if $length == 0;
  
  my $bytesAvailIN = $self->{bytesAvailIN}; my $ref = \$self->{buffer};
  
  while( length($$ref) < $length and $bytesAvailIN > 0 ) {
    my $toread = ($bytesAvailIN > 4096) ? 4096 : $bytesAvailIN;
    my $rc = $INPUT->read(my $buffer, $toread); assert($rc == $toread);
	my ($buffer, $zstatus) = $inflater->inflate($buffer);
	$$ref .= $buffer;
	$bytesAvailIN -= $toread;
  }
  assert(length($$ref)>=$length);
  
  $_[0] = substr($$ref, 0, $length); $$ref = substr($$ref, $length);
  
  $self->{bytesAvail} -= $length;
  
  return $length;
}

sub tell {
  my $self = shift; return $self->{fileSize} - $self->{bytesAvail};
}

sub seek_UNIMPLEMENTED {
}

sub binmode {
}

sub desiredCompressionMethod {
}

sub uncompressedSize {
  return $_[0]->{fileSize};
}

sub eof {
  return $_[0]->{bytesAvail} == 0;
}

sub new {
  my ($class, $INPUT, $file, $libFileName) = @_; my $self = {};
  
  @{$self}{qw(
    INPUT libFileName fileOffset fileCSize fileSize bytesAvail
  )} = 
  ($INPUT, $libFileName, @{$file}{qw(fileOffset fileCSize fileSize)}, 0); bless $self, $class;
}

#------------------------------------------------------------
package XReport::TAR::IPlain;

use strict; use bytes;

use Symbol;

sub Open {
  my $self = shift; my ($INPUT, $libFileName, $TEXT) = @{$self}{qw(INPUT libFileName TEXT)};

  if ($TEXT ne "") {
    $INPUT->open($TEXT), $INPUT->binmode();
  }
  
  $self->{bytesAvail} = $self->{fileSize};
  
  $INPUT->seek($self->{fileOffset}, 0); return $self;
}

sub Close {
  my $self = shift; $self->{bytesAvail} = 0; return 1;
}

sub read {
  my $self = shift; my $INPUT = $self->{INPUT}; my $length = $_[1];
  
  if ( $length > $self->{bytesAvail} ) {
    $length = $self->{bytesAvail}; 
  }
  return 0 if $length == 0;
  
  $length = $INPUT->read($_[0], $length);
  
  $self->{bytesAvail} -= $length;
  
  return $length;
}

sub tell {
  my $self = shift; $self->{INPUT}->tell();
}

sub seek {
  my $self = shift; my $INPUT = $self->{INPUT};
  my $before = $INPUT->tell();
  my $rc = $INPUT->seek($_[0], $_[1]);
  my $after = $INPUT->tell();
  $self->{bytesAvail} += $before - $after; return $rc;
}

sub binmode {
}

sub desiredCompressionMethod {
}

sub eof {
  my $self = shift; return $self->{bytesAvail} == 0 or $self->{INPUT}->eof();
}

sub new {
  my ($class, $INPUT, $file, $libFileName, $TEXT) = @_; my $self = {};
  
  @{$self}{qw(INPUT libFileName fileOffset fileSize bytesAvail TEXT)} 
    = 
  ($INPUT, $libFileName, @{$file}{qw(fileOffset fileSize)}, 0, $TEXT); bless $self;
}

#------------------------------------------------------------
package XReport::TAR::IZIP;

use strict; use bytes;

use Symbol;
use Compress::Zlib;

use constant COMPRESSION_STORED => 0;
use constant COMPRESSION_DEFLATED => 8;	

use constant LOCAL_FILE_HEADER_FORMAT		=> "v3 V4 v2";
use constant LOCAL_FILE_HEADER_LENGTH		=> 26;

use constant LOCAL_FILE_HEADER_SIGNATURE	=> "\x50\x4b\x03\x04";
use constant CENTRAL_DIRECTORY_FILE_HEADER_SIGNATURE => "\x50\x4b\x01\x02";

sub listMembers {
  my ($self) = @_; my ($INPUT, $bof) = @{$self}{'INPUT', 'bof'}; my $files = {};

  $INPUT->seek($bof, 0);
  
  while(!$INPUT->eof()) {
    my ($rc, $header, $fileName, $fileOffset, $fileSize, $fileCSize, $fileCMethod);
	
    $INPUT->read($header, 4); last if $header eq CENTRAL_DIRECTORY_FILE_HEADER_SIGNATURE;
	
    if ( $header ne LOCAL_FILE_HEADER_SIGNATURE ) {
      die unpack("H*", $header);
    }
    my ($fileNameLength, $extraField, $extraFieldLength);
    $INPUT->read($header, LOCAL_FILE_HEADER_LENGTH);
    (
	  undef, 	    # $self->{'versionNeededToExtract'},
	  undef,	    # $self->{'bitFlag'},
	  $fileCMethod,	# $self->{'compressionMethod'},
	  undef,	    # $self->{'lastModFileDateTime'},
	  undef,	    # $crc32,
	  $fileCSize,	# $compressedSize,
	  $fileSize,	# $uncompressedSize,
	  $fileNameLength,
	  $extraFieldLength 
	)
	= unpack(LOCAL_FILE_HEADER_FORMAT, $header);
	
    $INPUT->read($fileName, $fileNameLength);
    $INPUT->read($extraField, $extraFieldLength);

	$fileOffset = $INPUT->tell(); 

	print "==> $fileName, $fileOffset\n";
	   
	$files->{$fileName} = { 
	  fileName => $fileName, 
      fileOffset => $fileOffset,
	  fileCSize => $fileCSize, 
	  fileSize => $fileSize,
	  fileCMethod => $fileCMethod
	};
	
    $INPUT->seek($fileCSize, 1);
  }

  return $files;
}

sub locateMember {
  my ($self, $fileName_) = @_; my ($INPUT, $bof) = @{$self}{'INPUT', 'bof'};

  $INPUT->seek($bof, 0); $fileName_ = uc($fileName_);
  
  while(!$INPUT->eof()) {
    my ($rc, $header, $fileName, $fileOffset, $fileSize, $fileCSize, $fileCMethod);
	
    $INPUT->read($header, 4); last if $header eq CENTRAL_DIRECTORY_FILE_HEADER_SIGNATURE;
	
    if ( $header ne LOCAL_FILE_HEADER_SIGNATURE ) {
      die unpack("H*", $header);
    }
    my ($fileNameLength, $extraField, $extraFieldLength);
    $INPUT->read($header, LOCAL_FILE_HEADER_LENGTH);
    (
	  undef, 	    # $self->{'versionNeededToExtract'},
	  undef,	    # $self->{'bitFlag'},
	  $fileCMethod,	# $self->{'compressionMethod'},
	  undef,	    # $self->{'lastModFileDateTime'},
	  undef,	    # $crc32,
	  $fileCSize,	# $compressedSize,
	  $fileSize,	# $uncompressedSize,
	  $fileNameLength,
	  $extraFieldLength 
	)
	= unpack(LOCAL_FILE_HEADER_FORMAT, $header);
	
    $INPUT->read($fileName, $fileNameLength);
    $INPUT->read($extraField, $extraFieldLength);
	
	if ( uc($fileName) eq $fileName_ ) {
	  $fileOffset = $INPUT->tell(); 
	   
	  return { 
	   	fileName => $fileName, 
		fileOffset => $fileOffset,
		fileCSize => $fileCSize, 
		fileSize => $fileSize,
		fileCMethod => $fileCMethod
	  };
	}	
	
    $INPUT->seek($fileCSize, 1);
  }

  return undef; 
}

sub checkMember {
}

sub extractMember {
  my ($self, $file) = (shift, shift); my $INPUT = $self->{'INPUT'}; my $result;

  $file = $self->locateMember($file) if !ref($file);

  my ($fileOffset, $fileCSize, $fileSize, $fileCMethod) 
   =
  @{$file}{qw(fileOffset fileCSize fileSize fileCMethod)};

  $INPUT->seek($file->{fileOffset}, 0);

  if ( $fileCMethod == COMPRESSION_DEFLATED ) {
    my ($i, $zstatus) = inflateInit( '-WindowBits' => -MAX_WBITS() );
	
	$i or die "INFLATEINIT ERROR for MEMBER \"$file->{fileName}\" status=$zstatus\n";

    my $rest = $fileCSize;
    while($rest>0) {
	  my ($rc, $ibuffer, $obuffer);
      $rc = $INPUT->read($ibuffer, $rest > 32768 ? 32768 : $rest);
	  ($obuffer, $zstatus) = $i->inflate($ibuffer);
      $result .= $obuffer; $rest -= $rc;
    }
  }
  else {
  }

  return $result;
}

sub getMember {
  my ($self, $fileName) = @_; my ($INPUT, $libFileName) = @{$self}{qw(INPUT libFileName)};

  my $file = $self->locateMember($fileName);
  
  return undef if !$file;
  
  my $fileCMethod = $file->{fileCMethod};
  
  if ( $fileCMethod == COMPRESSION_DEFLATED ) {
    return XReport::TAR::IZlibINFLATE->new($INPUT, $file, $libFileName);
  }
  else {
    return XReport::TAR::IPlain->new($INPUT, $file, $libFileName);
  }
}

*LocateFile = *getMember;

sub fileName {
  my $self = shift; return $self->{fileName};
}

sub Open {
  my ($class, $file, $fileName) = @_; my ($self, $INPUT, $bof) = ({}, undef, 0);

  if ( !ref($file) ) {
    $INPUT = IO::File->new(); $INPUT->open("<$file") or die("INPUT OPEN ERROR $file $!"); $fileName = $file;
  }
  else {
    $INPUT = $file;
  }
  $bof = $INPUT->tell(); $INPUT->binmode();

  #todo: check eyecatcher;

  @{$self}{'INPUT', 'bof', fileName => $fileName} = ($INPUT, $bof); bless $self;
}

#------------------------------------------------------------
package XReport::TAR;

use strict; use bytes;

use Symbol;
use FileHandle;
use File::Basename;
use Compress::Zlib;

use constant TAR_READ_MODE => 1;
use constant TAR_WRITE_MODE => 2;
use constant TAR_APPEND_MODE => 3; 

use constant TAR_HEADER_LENGTH => 512;

use constant GZIP_MAGIC	=> "\x1F\x8B";

use constant COMPRESSION_STORED => 0;
use constant COMPRESSION_DEFLATED => 8;	

sub format_tar_hdr {
  my ($fref) = shift; my ($str, $from_file, $file, $prefix, $pos, $size);

  ($from_file, $file, $size) = @{$fref}{qw(from_file name size)};
  
  if (length($file)>99) {
    $pos = index $file, "/",(length($file) - 100);
    die "Filename longer than 100 chars! \"$file\"" if ( $pos == -1 );
    $prefix = substr($file, 0, $pos);
    $file = substr($file, $pos+1);
    substr($prefix,0,-155) = "" if length($prefix) > 154;
  }
  else {
    $prefix="";
  }
  $str = pack("a100a8a8a8a12a12a8a1a100",
    $file,
    sprintf("%6o ",$fref->{'mode'}),
    sprintf("%6o ",$fref->{'uid'}),
    sprintf("%6o ",$fref->{'gid'}),
    sprintf("%11o ",$fref->{'size'}),
    sprintf("%11o ",$fref->{'mtime'}),
    "        ",
    $fref->{'typeflag'},
    $fref->{'linkname'}
  );
  $str .= pack("a6", $fref->{'magic'});
  $str .= '00';
  $str .= pack("a32",$fref->{'uname'});
  $str .= pack("a32",$fref->{'gname'});
  $str .= pack("a8",sprintf("%6o ",$fref->{'devmajor'}));
  $str .= pack("a8",sprintf("%6o ",$fref->{'devminor'}));
  $str .= pack("a155",$prefix);
  substr($str,148,6) = sprintf("%6o", unpack("%16C*",$str));
  substr($str,154,1) = "\0";
  
  $str .= "\0" x (TAR_HEADER_LENGTH-length($str));
  
  return ($from_file, $size, $str);
}

sub write_tar_file {
  my ($self, $OUTPUT) = (shift, shift); my ($INPUT, $close_output) = (IO::File->new(), 1);

  ### manage Scalar -> create Tar Trailer XREF
  
  if ( $OUTPUT ) {
    if ( ref($OUTPUT) ) {
      $OUTPUT->binmode(); $close_output = 0;
    }
    elsif ( $OUTPUT ne ":Scalar" ) {
      my $fileName = $OUTPUT; $OUTPUT = IO::File->new(); 
      $OUTPUT->open(">$fileName")
        or
      die("TAR OUTPUT OPEN ERROR \"$fileName\" $!"); $OUTPUT->binmode(); 
    }
    else {
      $OUTPUT = IO::File->new(); 
      $OUTPUT->open(">", $_[0]) 
        or 
      die("TAR SCALAR OUTPUT OPEN ERROR $_[0] $!"); $OUTPUT->binmode();
    } 
  }
  else {
    if ( $self->{'open_mode'} != TAR_APPEND_MODE ) {
      die("INVALID OPEN MODE ($self->{'open_mode'}) OR MISSING OUTPUT FILE NAME");
    }
    my $append_offset = $self->{'append_offset'}; 
    $OUTPUT = $self->{'INPUT'}; $OUTPUT->seek($append_offset, 0); $OUTPUT->binmode();
  }

  my $tarfiles = $self->{'append_tar_files'};
    
  foreach my $fref (@$tarfiles) {
    my ($file, $size, $tarHdr) = format_tar_hdr($fref); print $OUTPUT $tarHdr;
	$INPUT->open("<$file") or die("OPEN INPUT ERROR \"$file\" $!"); $INPUT->binmode();
	
	my ($begOffset, $buff) = ($OUTPUT->tell(), "");
    while(!$INPUT->eof()) {
	  $INPUT->read($buff, 32768) or die("read error $!");
	  $OUTPUT->print($buff) or die("write error $!");
	}
	
	my $endOffset = $OUTPUT->tell();
	if ( $INPUT->tell() != $size ) {
	  die "COPY ERROR INVALID INFILE READ (".$INPUT->tell().") ($size)";
	}
	if ( ($endOffset-$begOffset) != $size ) {
	  die "COPY ERROR INVALID INFILE WRITE (".($endOffset-$begOffset).") ($size)";
	}
	$INPUT->close() or die("input close error $!");

	$OUTPUT->print("\0" x ((512-($endOffset%512)) % 512)) or die("write error $!");
  }

  $OUTPUT->print("\0" x 1024) or die("write error $!");
  
  if ($close_output) {
    $OUTPUT->close() or die("tar close error $!");
  }
}

sub addFiles {
  my ($self, $base_dir, @files) = @_; my ($added_files, $skipped_files) = ([], []); 

  ### die if $tar_fileSize < 0;
  $base_dir .= "/" if $base_dir ne '' and $base_dir !~ /[\/\\]$/; $base_dir =~ s/\\/\//g;

  # use test operating system;
  my ($has_getpwuid, $has_getgrgid) = (0, 0);
  
  foreach my $file (@files) {
    my ($mode,$uid,$gid,$rdev,$size,$mtime,$linkname,$typeflag, $stat);
    if ( ref($file) ) { 
      ($file, $stat) = @$file 
    } 
    else { 
      @$stat = stat($file)
    }
    if (@$stat) {
     (undef,undef,$mode,undef,$uid,$gid,$rdev,$size,undef,$mtime,undef,undef,undef) = @$stat;
     #if (!-f $file) { # Plain file only
       #die "TAR add_files INVALID FILE TYPE \"$file\"";
     #}
      my ($fileName, $fileOffset, $fileSize) = ($base_dir.basename($file), $self->{tar_fileSize}, $size);
      push(@{$self->{'append_tar_files'}}, {
        name => $fileName,           
        mode => $mode,
        uid => $uid,
        gid => $gid,
        size => $size,
        mtime => $mtime,
        chksum => "      ",
        typeflag => $typeflag, 
        linkname => $linkname,
        magic => "ustar\0",
        version => "00",
		
        # WinNT protection
        uname => $has_getpwuid ?(getpwuid($uid))[0] :"unknown",
        gname => $has_getgrgid ?(getgrgid($gid))[0] :"unknown",
		
        devmajor => 0, 
        devminor => 0,
        prefix => "",

        from_file => $file,
      });
      # Successfully added file
	  push @$added_files, {
        fileName =>$fileName, 
        fileOffset => $fileOffset, 
        fileSize => $fileSize, 
      }; 
	  $self->{tar_fileSize} += 512 + $size + ((512-$size%512)%512);
    }
    else {
      push @$skipped_files, $file; # stat failed
    }
  }

  return (scalar(@$skipped_files), $added_files, $skipped_files);
}

sub Members {
  my $self = shift; return $self->{'tar_files'} if exists($self->{tar_files});

  return $self->listMembers();
}

sub listMembers {
  my $self = shift; my ($INPUT, $bof) = @{$self}{'INPUT', 'bof'}; my $tar_files = [];
  
  my ($rc, $header, $fileName, $fileOffset, $fileSize, $tar_fileSize);
  
  $INPUT->seek($bof, 0); $tar_fileSize = 0; $fileOffset = 0;

  while (1) {
    $rc = $INPUT->read($header, 512); last if $header =~ /^\x00*$/;
    ($fileName,$fileSize) = unpack("a100x8x8x8a12", $header);
	
	$fileName =~ s/\x00+$//g; $fileSize= oct($fileSize); 
    print "aaa $fileName $fileSize aaa\n";

    $tar_fileSize += $fileSize + 512 + (512-$fileSize%512)%512;

	push @$tar_files, {
	  fileName => $fileName, 
      fileOffset => $fileOffset, 
      fileSize => $fileSize, 
	};
	
    $INPUT->seek($bof+$tar_fileSize, 0) or die "WWWWW $!"; $fileOffset = $INPUT->tell() - $bof;
  }
  die "!!!!!!!!!!!!!! ggg \n" if $header !~ /^\x00{512}$/;

  @{$self}{qw(tar_files tar_fileSize append_offset)} = ($tar_files, $tar_fileSize, $fileOffset); 
  
  return $self->{'tar_files'}; 
}

sub locateMember {
  my ($self, $fileName_) = @_; my ($INPUT, $bof, $eof) = @{$self}{qw(INPUT bof eof)};
  
  $INPUT->seek($bof, 0); $fileName_ = lc($fileName_); 

  my $locate_offset = -1;

  if ( $fileName_ =~ /offset:(\d+)/ ) {
    $locate_offset = $1;
  }
  elsif ( $fileName_ =~ /block:(\d+)/ ) {
    $locate_offset = $1*512;
  }
  $INPUT->seek($bof+$locate_offset,0) if $locate_offset > 0;

  while ( !$INPUT->eof() ) {
    my ($rc, $header, $fileName, $fileOffset, $fileSize, $fileCSize, $fileCMethod, $fileMagic);
	
    $rc = $INPUT->read($header, 512); last if $header =~ /^\x00+$/;
	
    ($fileName, $fileSize) = unpack("a100x8x8x8a12", $header);

	$fileName =~ s/\x00+$//g; $fileSize = oct($fileSize);
    
    if ( lc($fileName) eq $fileName_ or $locate_offset != -1) {
	   $fileOffset = $INPUT->tell(); $fileCSize = $fileSize;
	   
	   $INPUT->read($fileMagic, 2); $INPUT->seek(-2, 1);
	   
	   if ( $fileMagic eq GZIP_MAGIC ) {
	     $INPUT->seek($fileCSize-4, 1);
         $rc = $INPUT->read($header, 4);
         $fileSize = unpack("I", $header);
         $INPUT->seek(-$fileCSize, 1);
		 $fileCMethod = COMPRESSION_DEFLATED;
	   }
	   else {
	     $fileCMethod = COMPRESSION_STORED;
	   }
	   
	   return { 
	   	 fileName => $fileName, 
		 fileOffset => $fileOffset,
		 fileCSize => $fileCSize, 
		 fileSize => $fileSize,
		 fileCMethod => $fileCMethod
	   };
	}

	$INPUT->seek($fileSize + ((512-$fileSize%512)%512), 1);
  }
  
  return undef;
}

sub extractMember {
  my ($self, $file) = (shift, shift); my $INPUT = $self->{'INPUT'}; my $result;

  $file = $self->locateMember($file) if !ref($file);
  
  my ($fileOffset, $fileCSize, $fileSize, $fileCMethod) 
   =
  @{$file}{qw(fileOffset fileCSize fileSize $fileCMethod)};

  if ( $fileCMethod == COMPRESSION_DEFLATED ) {
    my $gz = gzopen($INPUT, "r") 
     or 
    die "GZOPEN ERROR for MEMBER \"$file->{fileName}\"\n" ;

    $gz->gzseek($file->{fileOffset}, 0);

    my ($rest, $buf, $rc) = ($fileSize, '', 0);
    while($rest>0) {
      $rc = $gz->gzread($buf, $rest > 32768 ? 32768 : $rest);
      $result .= $buf; $rest -= $rc;
    }
    $gz->gzclose();
  }
  else {
    $INPUT->seek($file->{fileOffset}, 0);
    my ($rest, $buf, $rc) = ($fileSize, '', 0);
    while($rest>0) {
      $rc = $INPUT->read($buf, $rest > 32768 ? 32768 : $rest);
      $result .= $buf; $rest -= $rc;
    }
  }

  return $result;
}

sub getMember {
  my ($self, $fileName) = @_; my ($INPUT, $libFileName) = @{$self}{qw(INPUT libFileName)};

  my $file = $self->locateMember($fileName); 

  return undef if !$file;

  my $fileCMethod = $file->{'fileCMethod'};
  
  if ( $fileCMethod == COMPRESSION_DEFLATED ) {
    return XReport::TAR::IZlibGZREAD->new($INPUT, $file, $libFileName);
  }
  else {
    return XReport::TAR::IPlain->new($INPUT, $file, $libFileName);
  }
}

sub checkMember {
}

sub deleteMember {
}

sub tar_fileSize {
  my ($self, $fileName) = (shift, shift); my $fileSize;

  $self->listMembers() if !exists($self->{tar_fileSize});

  if ( $fileName eq '') {
    return $self->{tar_fileSize};
  }

  if (!-f $fileName) { 
    die "TAR fileSize INVALID FILE TYPE \"$fileName\"";
  }

  $fileSize = -s $fileName;

  return $self->{tar_fileSize} + 512 + ((512-$fileSize%512)%512);
}

sub Create {
  my $class = shift; 

  my $self = {
    open_mode => TAR_WRITE_MODE,
    tar_files => [],
    tar_fileSize => 0
  };

  bless $self, $class;
}

sub OpenInFileUrl {
  my ($class, $FileUrl, $LocalFileName, $FileSize) = @_;
  my (
    $TarDateRef, $TarDateProgr, $TarFileProgr, $FileOffset, $FileSize
  ) =
  $FileUrl =~ /^tar[^:]*:\/\/[^\/]+\/[^\/]+\/(\d{8})\.(\d+)(?:\.(\d+))?\/(\d+)\/(\d+)/; $TarFileProgr ||= 0;

  die("INVALID TAR FILE URL DETECTED ($FileUrl)") if $TarDateRef eq "";
  
  my $dbr = dbExecute("
    SELECT * from tbl_TarFiles where TarDateRef='$TarDateRef' and TarDateProgr=$TarDateProgr and TarFileProgr=$TarFileProgr and TarINOUT_Flag='I'
  ");
  my ($TarLocalFileName, $TarUrl) = $dbr->getValues(qw(TarLocalFileName TarUrl));

  my ($TarProtocol, $TarLocalId, $TarLocator) = $TarUrl =~ /^([^:]+):\/\/([^\/]+)\/(.*)$/;

  die "INVALID PROTOCOL DETECTED ($TarProtocol)\n" if $TarProtocol ne "centera"; 
  
  require XReport::ARCHIVE::Centera;

  my $cpool = XReport::ARCHIVE::Centera::FPPool->new($TarLocalId);

  my $cclip =  $cpool->Open_Clip($TarLocalFileName, $TarLocator);

  my $INPUT = $cclip->LocateFile($TarLocalFileName)->Open();

  my ($header, $buf, $rc, $LocalFileName_, $FileSize_); 

  $INPUT->seek($FileOffset);
  $rc = $INPUT->read($header, 512); die if $header =~ /^\x00*$/;
  ($LocalFileName_,$FileSize_) = unpack("a100x8x8x8a12", $header);
  $LocalFileName_ =~ s/\x00+$//g; $FileSize_ = oct($FileSize_);

  die("FILENAME MISMATCH expected($LocalFileName) found($LocalFileName_) !!") 
   if 
  lc($LocalFileName_) ne lc($LocalFileName); 

  my $istream = ''; my $ostream = ''; my $buf = ''; my $rest = $FileSize;
  while($rest > 0) {
    $INPUT->read($buf, $rest > 4096 ? 4096 : $rest); last if $buf eq ""; $istream .= $buf; $rest -= length($buf);
  }
  die "aaaaaa " if length($istream) != $FileSize;

  $ostream = Compress::Zlib::memGunzip($istream) or die "!!!!!!!!!"; $FileSize = length($ostream);
  
  my $File = {
    fileName => $LocalFileName, fileOffset => 0, fileSize => $FileSize 
  };

  require IO::String; $INPUT = IO::String->new();

  return XReport::TAR::IPlain->new($INPUT, $File, "$TarLocalFileName/$LocalFileName", $ostream); 
}

sub OpenOutFileUrl {
  my ($class, $FileUrl, $LocalFileName, $FileSize) = @_;
  my (
    $TarDateRef, $TarDateProgr, $TarFileProgr, $FileOffset, $FileSize
  ) =
  $FileUrl =~ /^tar[^:]*:\/\/[^\/]+\/[^\/]+\/(\d{8})\.(\d+)(?:\.(\d+))?\/(\d+)\/(\d+)/; $TarFileProgr ||= 0;

  die("INVALID TAR FILE URL DETECTED ($FileUrl)") if $TarDateRef eq "";
  
  my $dbr = dbExecute("
    SELECT * from tbl_TarFiles where TarDateRef='$TarDateRef' and TarDateProgr=$TarDateProgr and TarFileProgr=$TarFileProgr and TarINOUT_Flag='O'
  ");
  my ($TarLocalFileName, $TarUrl) = $dbr->getValues(qw(TarLocalFileName TarUrl));

  my ($TarProtocol, $TarLocalId, $TarLocator) = $TarUrl =~ /^([^:]+):\/\/([^\/]+)\/(.*)$/;

  die "INVALID PROTOCOL DETECTED ($TarProtocol)\n" if $TarProtocol ne "centera"; 
  
  require XReport::ARCHIVE::Centera;

  my $cpool = XReport::ARCHIVE::Centera::FPPool->new($TarLocalId);

  my $cclip =  $cpool->Open_Clip($TarLocalFileName, $TarLocator);

  my $INPUT = $cclip->LocateFile($TarLocalFileName)->Open();

  my ($header, $buf, $rc, $LocalFileName_, $FileSize_); 

  $INPUT->seek($FileOffset);
  $rc = $INPUT->read($header, 512); die if $header =~ /^\x00*$/;
  ($LocalFileName_,$FileSize_) = unpack("a100x8x8x8a12", $header);
  $LocalFileName_ =~ s/\x00+$//g; $FileSize_ = oct($FileSize_);

  die("FILENAME MISMATCH expected($LocalFileName) found($LocalFileName_) !!") 
   if 
  lc($LocalFileName_) ne lc($LocalFileName);

  return XReport::TAR::IZIP->Open($INPUT, "$TarLocalFileName/$LocalFileName"); 
}

sub Open {
  my ($class, $tarFile, $open_mode) = @_; my ($self, $INPUT, $libFileName, $bof, $tar_fileSize, $rec); $INPUT = IO::File->new();

  if ( !ref($tarFile) ) {
    $libFileName = $tarFile; $open_mode = $open_mode =~ /a|\+/i ? TAR_APPEND_MODE : TAR_READ_MODE;
    if ($libFileName =~ /^id:(\d+)/i) {
      my $TarFileId = $1; my $dbr; 
      $dbr = dbExecute("
        SELECT * from tbl_TarFiles where TarFileId = $TarFileId
      ");
      $libFileName = $dbr->GetFieldsValues('TarLocalFileName');
    }
    $INPUT->open(($open_mode == TAR_APPEND_MODE ? '+<' : '<').$libFileName) 
     or 
    die("TAR FILE INPUT OPEN ERROR \"$libFileName\" $!"); 
  }
  else {
    $INPUT = $tarFile; $open_mode = TAR_READ_MODE;
  }

  $bof = $INPUT->tell(); $INPUT->binmode(); $tar_fileSize = 0; 

  if ($open_mode == TAR_APPEND_MODE) {
    $INPUT->seek(-1024, 2) or die "WWWW $!"; $tar_fileSize = $INPUT->tell();
    die("INVALID TAR FILE SIZE $tar_fileSize $tarFile\n") if $tar_fileSize%512; 
    $INPUT->read($rec, 1024);
    die("INVALID TAR FILE END DETECTED ($tarFile) !!!") if $rec ne "\x00"x1024; 
    print "??????????????????????? $tar_fileSize\n";
  }

  $self = {
     libFileName => $libFileName,
     open_mode => $open_mode,
     INPUT => $INPUT,
     bof => $bof,
     tar_fileSize => $tar_fileSize,
     append_offset => $tar_fileSize
  }; 

  bless $self, $class;
}

sub Close {
  my $self = shift; %$self = ();
}

1;

__END__

package main;

#my $tar = XReport::TAR->Open("c:/temp/zz/zz1.tar", "a");
my $tar = XReport::TAR->Open("id:1", "a");

#print "==1 ", $tar->tar_fileSize(), "\n";

#my $member = $tar->getMember("in/2003/0910/zzGENERALI.20030910164146.115108.DATA.TXT.gz");
#my $member = $tar->getMember("offset:62976");
my $member = $tar->getMember("block:123");


for (1..2) {
$member->Open(); 
open(OUT, ">c:/temp/zz/zzout.txt"); binmode(OUT);
while($member->read(my $rec, 32768)) {
  print OUT $rec;
}
close(OUT);
$member->Close(); 
}

die $member;

if (0) {
$tar->addFiles('IN/2003/0910/', "c:/xreport/storage/in/2003/0910/ZZGENERALI.20030910164146.115108.DATA.TXT.gz");

print "==2 ", $tar->tar_fileSize(), "\n";

$tar->write_tar_file();

}

for (@{$tar->Members()}) {
  print join("\n", %$_), "\n";
}
