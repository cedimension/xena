package XReport::METABASE;

use strict;

use Data::Dumper;
use XML::Simple;
use SQL::Translator;

sub readMetaData {
	my $self = shift;
	my $mdfn = shift;
	return XML::Simple::XMLin( $mdfn,
                               ForceArray => [qw(initrows tables columns)],
                               KeyAttr    => { tables => '+name', columns => '+name' },
                               ContentKey => '-content' )
}

sub readTablesMetaData {
    my $self = shift;
    my $args = { @_ };
#    warn "readTablesMetaData: ", Dumper($args), "\n";
    my $dirn = $self->{metabase_loc};
    $dirn = delete $args->{metabase_loc} if exists($args->{metabase_loc}); 
     
    my $tables = {};
    my $tblre = qr/^.+$/;
    if ( exists($args->{include}) && $args->{include} ne '1' ) {
    	return $tables unless $args->{include};
        if ( ref($args->{include}) eq 'ARRAY' ) {
    	   my $re = '^(?:'.join('|', @{$args->{include}}).')$';
           $main::veryverbose && warn "READTABLES: compiling $re\n";
    	   $tblre = qr/$re/;
        }
        else {
            $tblre = qr/$args->{include}/;
        }
        delete $args->{include};
    }
    my $init = (exists($args->{initrows}) ? delete $args->{initrows} : 0 );
    $main::veryverbose && warn "READTABLES: applying $tblre to dir $dirn\n";
    opendir(my $dh, $dirn) || die;
 
    while(readdir $dh) {
        my $fn = "$dirn/$_";
 #       print "processing $fn\n";

        next if $_ =~ /^(?:\..*|dictionary.xml|.+\.sql|.+\.bak)$/i;
        my $href = $self->readMetaData( "$fn" );
        
        my @tables = grep { $_ =~ /$tblre/ } keys %{$href->{tables}};
        next unless scalar(@tables);
               
#        @{$self->{xrdb}->{tables}}{@tables} = @{$href->{tables}}{@tables};
        foreach my $tblref ( @{$href->{tables}}{@tables} ) {
            my $tblname = $tblref->{name};
            if (exists($self->{xrdb}->{usrtables}->{$tblname})) {
               my $usrtab = $self->{xrdb}->{usrtables}->{$tblname};
               foreach my $colname ( keys %{$usrtab->{columns}} ) {
                  my $newattrs = $usrtab->{columns}->{$colname};
                  if (exists($tblref->{columns}->{$colname})) {
                     my $oldattrs = delete $tblref->{columns}->{$colname};
                     $tblref->{columns}->{$colname} = { %{$oldattrs}, %{$newattrs} };
                  }
               }
               for ( qw(initrows keep) ) {
                  $tblref->{$_} = $usrtab->{$_} if exists($usrtab->{$_});
               }
            }
            
            my $tbcolumns = { map { $_->{name} => $_ } grep { !$_->{isidentity} && !$_->{computed} }
                                                      values %{$tblref->{columns}} };
    
            my $keycol = [ sort map { $_->{name} } sort { $a->{iskey} <=> $b->{iskey} } grep { $_->{iskey} }
                                                                                        values %{ $tbcolumns } ];
            my @missing  = map { $_ } grep { !exists($self->{xrdb}->{dictionary}->{columns}->{$_}) } keys %{ $tbcolumns };
            die "$tblname columns ", join(', ', @missing), " does not exists in Dictionary" if scalar(@missing);
            my $columns = { map { $_ => { %{ $self->{xrdb}->{dictionary}->{columns}->{$_} }
            	                        , %{ $tbcolumns->{$_} } }
                                } keys %$tbcolumns };
            my $rowcols = [ sort keys %{$columns} ];
            
            @{$tables->{$tblname}}{qw(name keycol columns rowcols)} 
                    = ($tblname, $keycol, $columns, $rowcols);
            $tables->{$tblname}->{keep} = $tblref->{keep} if exists($tblref->{keep});
            # print "TBLREF: ", Dumper($tblref), "\n";
            next unless my $initrows = delete $tblref->{initrows};
            $main::debug && i::logit("initrows for $tblname", Dumper($initrows));
            $self->addRows2Table($tables->{$tblname},
                   ( map { { map { $_->{name} => $_->{content} } values %{$_->{columns}} } } @{$initrows} ) )
                                                                             if ( $init && scalar(@{$initrows}) );
#           $tables->{$tblname}->{initrows} = 
#              [ map { { map { $_->{name} => $_->{content} } values %{$_->{columns}} } } @{$initrows} ] 
#                                                                    if $initrows && scalar(@{$initrows});
#           if ( $init && exists($tblref->{initrows}) && scalar(@{$tblref->{initrows}}) ) {
#              $self->addRows2Table($tblref, @{delete $tblref->{initrows}});
#           }
        }
   
    }
    closedir $dh;
    
    return $tables;
}

sub new {
    my $class = shift;
    my $self = { metabase_loc => "$main::Application->{XREPORT_HOME}/bin/xrdb_metabase",
        @_ };
    bless $self, $class; 

    my $dirn = $self->{metabase_loc};
    $self->{xrdb} = $self->readMetaData( "$dirn/dictionary.xml" );
    my $local_defaults = "$main::Application->{XREPORT_SITECONF}/xml/metabase_defaults.xml";
    my $usrdefaults = ( -e $local_defaults ? $self->readMetaData( $local_defaults) 
                                           : { dictionary => {columns => {}}, tables => {} } );
    foreach my $colname ( keys %{$usrdefaults->{dictionary}->{columns}} ) {
    	my $newattrs = $usrdefaults->{dictionary}->{columns}->{$colname};
    	if (exists($self->{xrdb}->{dictionary}->{columns}->{$colname})) {
    		my $oldattrs = delete $self->{xrdb}->{dictionary}->{columns}->{$colname};
    		$self->{xrdb}->{dictionary}->{columns}->{$colname} = { %{$oldattrs}, %{$newattrs} };
    	}
    }
    $self->{xrdb}->{usrtables} = delete $usrdefaults->{tables};
    my $include = delete $self->{loadtables};
    return $self unless $include;
    $main::veryverbose && warn "METABASE new requests to load $include tables\n";
    
    $self->{xrdb}->{tables} = 
        $self->readTablesMetaData(@_, include => $include);
    return $self;
    
}

sub parse {
    my $tr = $_[0];
    my $args = $tr->parser_args();
#    die "parser ARGS: ", Dumper($args), "\n";
#    warn "Producing SQL for: ", Dumper(\{p1 => $_[1], MB => $args->{table}});
    my $schema = $tr->schema;
    
    	my $xrtbl = (exists($args->{tblpfx}) ? $args->{tblpfx} : 'tbl_');
    	$xrtbl .= $args->{table}->{name};
        $xrtbl .= (exists($args->{tblsfx}) ? $args->{tblsfx} : '');
        #print "TABLE: ", Dumper($xrtbl), "\n";
        my $tb = $schema->add_table( name => $xrtbl );
#        foreach my $xrcol ( values %{ $args->{table}->{columns} } ) {
        foreach my $colname ( @{ $args->{table}->{rowcols} } ) {
            my $xrcol = $args->{table}->{columns}->{$colname};
            if ( exists( $args->{dictionary}->{columns}->{ $colname } ) ) {
                my $fld = { %{ $args->{dictionary}->{columns}->{ $xrcol->{name} } }, %{$xrcol} };

                #print "COL: ", Dumper($fld), "\n";
                my $f = $tb->add_field( name => $fld->{name}, data_type => $fld->{type} );
                if ( exists( $fld->{default} ) ) {
                    (my $default = $fld->{default} ) =~ s/^'(.+)'$/$1/ ;
                    $f->default_value( $default );
                }
                #$f->is_primary_key( $fld->{iskey} ) if (exists($fld->{iskey}) );
                $f->size( $fld->{length} )                  if ( exists( $fld->{length} ) );
                $f->is_auto_increment( $fld->{isidentity} ) if ( exists( $fld->{isidentity} ) );
            }
            else { print "MISSING XRCOL: ", Dumper($xrcol), "\n"; }
        }
        return 1 if exists($args->{nopk}) && $args->{nopk};
        my $pk =
          [ map { $_->{name} }
            sort { $a->{iskey} <=> $b->{iskey} } grep { exists( $_->{iskey} ) } values %{ $args->{table}->{columns} } ];
        $tb->primary_key($pk) if scalar( @{$pk} );

    return 1;
}


sub rowKey {
    my ($tbl, $row) = (shift, shift);
    my $keycol = $tbl->{keycol};
    return join("\t", map { $_ ? $_ : '' } @{$row}{@$keycol});
}

sub addRows2Table {
	my $self = shift;
    my $tbl = shift;
    
    while ( @_ ) {
      my $inrow = shift;
      i::logit("Adding row to $tbl->{name}: ", Dumper($inrow)) if $main::debug;
#    die "add varsetvalue VSN: $inrow->{VarSetName} VN: $inrow->{VarName} VV: $inrow->{VarValue}", 
#           " - called by ", join('::', (caller())[0,2]), "\n"
#           if $tbl->{name} eq 'VarSetsValues' && $inrow->{VarName} =~ /[^ ]+/ && $inrow->{VarValue} =~ /^\s*$/; 
      if ( exists($tbl->{keycol}) && scalar(@{$tbl->{keycol}}) ) {
        my $keyval = rowKey($tbl, $inrow);
        die "Column Key $tbl->{keycol} missing or missing value for keyed table $tbl->{name} missing - process aborted"
             unless defined($keyval);

        my $dfltrow = exists($tbl->{rows}->{$keyval}) ? $tbl->{rows}->{$keyval} 
                        : { map { $_ => $tbl->{columns}->{$_}->{default} } @{$tbl->{rowcols}} };

        #warn( "row key $keyval replaced in table") if exists($tbl->{rows}->{$keyval});
        $tbl->{rows}->{$keyval} =
            { map { $_ => ( exists( $inrow->{$_} ) ? $inrow->{$_} : $dfltrow->{$_} ) }  @{ $tbl->{rowcols} } }
      }     
      else {
        push @{$tbl->{rows}}, { map { $_ => ( exists( $inrow->{$_} ) ? $inrow->{$_} : $tbl->{columns}->{$_}->{default} ) }
              @{ $tbl->{rowcols} } }; 
      }
    }
    i::logit("AddRows: ", Dumper($tbl)) if $main::debug;
    return $tbl->{rows};
}

sub SQLCreate {
	my $self = shift;
	my $args = { @_ };
#    warn "genSQL data: ", Dumper($args), "\n";
    
    my $tables = delete($args->{tables}); 
    my $tblpfx = (exists($args->{tblpfx}) ? delete $args->{tblpfx} : 'tbl_' );
    my $tblsfx = (exists($args->{tblsfx}) ? delete $args->{tblsfx} : '' );
    my $dropt = (exists($args->{droptable}) ? delete $args->{droptable} : 0 );
    my $insert = (exists($args->{insertrows}) ? delete $args->{insertrows} : 0 );
	my $sql = '';
	foreach my $tbl ( @{$tables} ) {
    	my $generator = SQL::Translator->new(
            debug               => 1,   # Print debug info
            trace               => 1,   # Print Parse::RecDescent trace
            no_comments         => 1,   # Don't include comments in output
            show_warnings       => 1,   # Print name mutations, conflicts
#           add_drop_table      => 1,   # Add "drop table" statements
            quote_identifiers   => 1,   # to quote or not to quote, thats the question
            validate            => 1,   # Validate schema object
            );
        
        $generator->parser( __PACKAGE__ );
        $generator->producer( $self->{RDBMS} );
        $generator->parser_args(dictionary => $self->{xrdb}->{dictionary}, 
                                table => $tbl, tblpfx => $tblpfx, tblsfx => $tblsfx, 
                                %{$args} );
        $sql .= $self->dropTable(table => $tbl, tblpfx => $tblpfx, tblsfx => $tblsfx ) if $dropt; 
        $sql .= $generator->translate();
        if ( $insert && ((exists($tbl->{initrows}) && scalar(@{$tbl->{initrows}}))
                     || (exists($tbl->{rows}) && scalar(@{$tbl->{rows}}))) ) {
            $self->addRows2Table($tbl, @{delete $tbl->{initrows}}) 
                                if (exists($tbl->{initrows}) && scalar(@{$tbl->{initrows}}));
            $sql .= $self->genInsert(table => $tbl, tblpfx => $tblpfx, tblsfx => $tblsfx );
        }
	}
#	warn "GENSQL SQL: ", $sql, "\n";
	return $sql;
}


1;
