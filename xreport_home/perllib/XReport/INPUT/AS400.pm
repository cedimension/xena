
use lib("$ENV{XREPORT_HOME}\\perllib");

use strict;

package XReport::INPUT::AS400::data_record;

use strict;

use Convert::IBM390 qw(unpackeb);

sub getValues {
  my $self = shift->{'data_values'}; my @r; for (@_) { die("???? $_") if !exists($self->{$_}); push @r, $self->{$_}; }; return wantarray ? @r : $r[0];
}

sub new {
  my ($class, $data_string, $data_type, $unpack_string, $data_fields) = @_; my $data_values = {};
  my @data_values = unpackeb($unpack_string, $data_string);
#  warn "data values:\n", join("\n", @data_values), "\n";

  for (0..$#data_values) {
    $data_values[$_] =~ s/ +$//;
    $data_values->{$data_fields->[$_]} = $data_values[$_];
  }
  bless {data_type => $data_type, data_values => $data_values}, $class;
}

1;

package XReport::INPUT::AS400::data_unpacker;

use Convert::IBM390 qw(unpackeb);
use IO::File;

use Data::Dumper qw(Dumper);

my @SPLA0100_def = split("\n", <<EOF
0 0 BINARY(4) Bytes returned
4 4 BINARY(4) Bytes available
8 8 CHAR(16) Internal job identifier
24 18 CHAR(16) Internal spooled file identifier
40 28 CHAR(10) Job name
50 32 CHAR(10) User name
60 3C CHAR(6) Job number
66 42 CHAR(10) Spooled file name
76 4C BINARY(4) Spooled file number
80 50 CHAR(10) Form type
90 5A CHAR(10) User-specified data
100 64 CHAR(10) Status
110 6E CHAR(10) File available
120 78 CHAR(10) Hold file before written
130 82 CHAR(10) Save file after written
140 8C BINARY(4) Total pages
144 90 BINARY(4) Page or record being written
148 94 BINARY(4) Starting page
152 98 BINARY(4) Ending page
156 9C BINARY(4) Last page printed
160 A0 BINARY(4) Restart printing
164 A4 BINARY(4) Total copies
168 A8 BINARY(4) Copies left to produce
172 AC BINARY(4) Lines per inch
176 B0 BINARY(4) Characters per inch
180 B4 CHAR(2) Output priority
182 B6 CHAR(10) Output queue name
192 C0 CHAR(10) Output queue library name
202 CA CHAR(7) Date file opened
209 D1 CHAR(6) Time file opened
215 D7 CHAR(10) Device file name
225 E1 CHAR(10) Device file library name
235 EB CHAR(10) Program that opened file name
245 F5 CHAR(10) Program that opened file library name
255 FF CHAR(15) Accounting code
270 10E CHAR(30) Print text
300 12C BINARY(4) Record length
304 130 BINARY(4) Maximum records
308 134 CHAR(10) Device type
318 13E CHAR(10) Printer device type
328 148 CHAR(12) Document name
340 154 CHAR(64) Folder name
404 194 CHAR(8) System/36 procedure name
412 19C CHAR(10) Print fidelity
422 1A6 CHAR(1) Replace unprintable characters
423 1A7 CHAR(1) Replacement character
424 1A8 BINARY(4) Page length
428 1AC BINARY(4) Page width
432 1B0 BINARY(4) Number of separators
436 1B4 BINARY(4) Overflow line number
440 1B8 CHAR(10) DBCS data
450 1C2 CHAR(10) DBCS extension characters
460 1CC CHAR(10) DBCS shift-out shift-in (SO/SI) spacing
470 1D6 CHAR(10) DBCS character rotation
480 1E0 BINARY(4) DBCS characters per inch
484 1E4 CHAR(10) Graphic character set
494 1EE CHAR(10) Code page
504 1F8 CHAR(10) Form definition name
514 202 CHAR(10) Form definition library name
524 20C BINARY(4) Source drawer
528 210 CHAR(10) Printer font
538 21A CHAR(6) System/36 spooled file identifier
544 220 BINARY(4) Page rotation
548 224 BINARY(4) Justification
552 228 CHAR(10) Print on both sides (duplex)
562 232 CHAR(10) Fold records
572 23C CHAR(10) Control character
582 246 CHAR(10) Align forms
592 250 CHAR(10) Print quality
602 25A CHAR(10) Form feed
612 264 CHAR(71) Volumes (array)
683 2AB CHAR(17) File label identifier
700 2BC CHAR(10) Exchange type
710 2C6 CHAR(10) Character code
720 2D0 BINARY(4) Total records
724 2D4 BINARY(4) Multiple up (pages per side)
728 2D8 CHAR(10) Front overlay name
738 2E2 CHAR(10) Front overlay library name
748 2EC PACKED(15,5) Front overlay offset down
756 2F4 PACKED(15,5) Front overlay offset across
764 2FC CHAR(10) Back overlay name
774 306 CHAR(10) Back overlay library name
784 310 PACKED(15,5) Back overlay offset down
792 318 PACKED(15,5) Back overlay offset across
800 320 CHAR(10) Unit of measure
810 32A CHAR(10) Page definition name
820 334 CHAR(10) Page definition library name
830 33E CHAR(10) Line spacing
840 348 PACKED(15,5) Point size
848 350 PACKED(15,5) Front margin offset down
856 358 PACKED(15,5) Front margin offset across
864 360 PACKED(15,5) Back margin offset down
872 368 PACKED(15,5) Back margin offset across
880 370 PACKED(15,5) Length of page
888 378 PACKED(15,5) Width of page
896 380 CHAR(10) Measurement method
906 38A CHAR(1) Advanced Function Printing (AFP) resource
907 38B CHAR(10) Character set name
917 395 CHAR(10) Character set library name
927 39F CHAR(10) Code page name
937 3A9 CHAR(10) Code page library name
947 3B3 CHAR(10) Coded font name
957 3BD CHAR(10) Coded font library name
967 3C7 CHAR(10) DBCS-coded font name
977 3D1 CHAR(10) DBCS-coded font library name
987 3DB CHAR(10) User-defined file
997 3E5 CHAR(10) Reduce output
1007 3EF CHAR(1) Constant back overlay
1008 3F0 BINARY(4) Output bin
1012 3F4 BINARY(4) CCSID
1016 3F8 CHAR(100) User-defined text
1116 45C CHAR(8) System where file created
1124 464 CHAR(8) ID where file created
1132 46C CHAR(10) User who created file
1142 476 CHAR(2) Reserved
1144 478 BINARY(4) Offset to user-defined options
1148 47C BINARY(4) Number of user-defined options returned
1152 480 BINARY(4) Length of each user-defined option entry
1156 484 CHAR(255) User-defined data
1411 583 CHAR(10) User-defined object name
1421 58D CHAR(10) User-defined object library name
1431 597 CHAR(10) User object type
1441 5A1 CHAR(3) Reserved
1444 5A4 PACKED(15,5) Character set point size
1452 5AC PACKED(15,5) Coded font point size
1460 5B4 PACKED(15,5) DBCS-coded font point size
1468 5BC BINARY(4) Auxiliary storage pool
1472 5C0 BINARY(4) Spooled file size
1476 5C4 BINARY(4) Spooled file size multiplier
1480 5C8 BINARY(4) Internet print protocol job identifier
1484 5CC CHAR(1) Spooled file creation security method
1485 5CD CHAR(1) Spooled file creation authentication method
1486 5CE CHAR(7) Date writer began processing spooled file
1493 5D5 CHAR(6) Time writer began processing spooled file
1499 5DB CHAR(7) Date writer completed processing spooled file
1506 5E2 CHAR(6) Time writer completed processing spooled file
EOF
);

my @SPLA0200_def = split("\n", <<EOF
0 0 BINARY(4) Bytes returned
4 4 BINARY(4) Bytes available
8 8 CHAR(8) Format name
16 10 CHAR(16) Internal job identifier
32 20 CHAR(16) Internal spooled file identifier
48 30 CHAR(10) Job name
58 3A CHAR(10) User name
68 44 CHAR(6) Job number
74 4A CHAR(10) Spooled file name
84 54 BINARY(4) Spooled file number
88 58 CHAR(10) Form type
98 62 CHAR(10) User-specified data
108 6C CHAR(10) Status
118 76 CHAR(10) File available
128 80 CHAR(10) Hold file before written
138 8A CHAR(10) Save file after written
148 94 BINARY(4) Total pages
152 98 BINARY(4) Page or record being written
156 9C BINARY(4) Starting page
160 A0 BINARY(4) Ending page
164 A4 BINARY(4) Last page printed
168 A8 BINARY(4) Restart printing
172 AC BINARY(4) Total copies
176 B0 BINARY(4) Copies left to produce
180 B4 BINARY(4) Lines per inch
184 B8 BINARY(4) Characters per inch
188 BC CHAR(2) Output priority
190 BE CHAR(10) Output queue name
200 C8 CHAR(10) Output queue library name
210 D2 CHAR(7) Date file opened
217 D9 CHAR(6) Time file opened
223 DF CHAR(10) Device file name
233 E9 CHAR(10) Device file library name
243 F3 CHAR(10) Program that opened file name
253 FD CHAR(10) Program that opened file library name
263 107 CHAR(15) Accounting code
278 116 CHAR(30) Print text
308 134 BINARY(4) Record length
312 138 BINARY(4) Maximum records
316 13C CHAR(10) Device type
326 146 CHAR(10) Printer device type
336 150 CHAR(12) Document name
348 15C CHAR(64) Folder name
412 19C CHAR(8) System/36 procedure name
420 1A4 CHAR(10) Print fidelity
430 1AE CHAR(1) Replace unprintable characters
431 1AF CHAR(1) Replacement character
432 1B0 BINARY(4) Page length
436 1B4 BINARY(4) Page width
440 1B8 BINARY(4) Number of separators
444 1BC BINARY(4) Overflow line number
448 1C0 CHAR(10) DBCS data
458 1CA CHAR(10) DBCS extension characters
468 1D4 CHAR(10) DBCS shift-out shift-in (SO/SI) spacing
478 1DE CHAR(10) DBCS character rotation
488 1E8 BINARY(4) DBCS characters per inch
492 1EC CHAR(10) Graphic character set
502 1F6 CHAR(10) Code page
512 200 CHAR(10) Form definition name
522 20A CHAR(10) Form definition library name
532 214 BINARY(4) Source drawer
536 218 CHAR(10) Printer font
546 222 CHAR(6) System/36 spooled file identifier
552 228 BINARY(4) Page rotation
556 22C BINARY(4) Justification
560 230 CHAR(10) Print on both sides (duplex)
570 23A CHAR(10) Fold records
580 244 CHAR(10) Control character
590 24E CHAR(10) Align forms
600 258 CHAR(10) Print quality
610 262 CHAR(10) Form feed
620 26C CHAR(71) Volumes (array)
691 2B3 CHAR(17) File label identifier
708 2C4 CHAR(10) Exchange type
718 2CE CHAR(10) Character code
728 2D8 BINARY(4) Total records
732 2DC BINARY(4) Multiple up (pages per side)
736 2E0 CHAR(10) Front overlay name
746 2EA CHAR(10) Front overlay library name
756 2F4 PACKED(15,5) Front overlay offset down
764 2FC PACKED(15,5) Front overlay offset across
772 304 CHAR(10) Back overlay name
782 30E CHAR(10) Back overlay library name
792 318 PACKED(15,5) Back overlay offset down
800 320 PACKED(15,5) Back overlay offset across
808 328 CHAR(10) Unit of measure
818 332 CHAR(10) Page definition name
828 33C CHAR(10) Page definition library name
838 346 CHAR(10) Line spacing
848 350 PACKED(15,5) Point size
856 358 BINARY(4) Maximum spooled data record size
860 35C BINARY(4) Spooled file buffer size
864 360 CHAR(6) Spooled file level
870 366 ARRAY(16,4) Coded font array
886 376 CHAR(10) Channel mode
896 380 ARRAY(48,12) Channel value array
944 3B0 CHAR(8) Graphics token
952 3B8 CHAR(10) Record format
962 3C2 CHAR(2) Reserved
964 3C4 PACKED(15,5) Height of drawer 1
972 3CC PACKED(15,5) Width of drawer 1
980 3D4 PACKED(15,5) Height of drawer 2
988 3DC PACKED(15,5) Width of drawer 2
996 3E4 BINARY(4) Number of buffers
1000 3E8 BINARY(4) Maximum forms width
1004 3EC BINARY(4) Alternate forms width
1008 3F0 BINARY(4) Alternate forms length
1012 3F4 BINARY(4) Alternate lines per inch
1016 3F8 CHAR(2) System/38 Text Utility flags
1018 3FA CHAR(1) File open
1019 3FB CHAR(1) Page count estimated
1020 3FC CHAR(1) File stopped on page boundary
1021 3FD CHAR(1) TRC for 1403
1022 3FE CHAR(1) Define characters
1023 3FF CHAR(1) Characters per inch changes
1024 400 CHAR(1) Transparency
1025 401 CHAR(1) Double-wide characters
1026 402 CHAR(1) DBCS character rotation commands
1027 403 CHAR(1) Extended code page
1028 404 CHAR(1) FFT emphasis
1029 405 CHAR(1) 3812 SCS
1030 406 CHAR(1) Set Line Density command
1031 407 CHAR(1) Graphics error actions
1032 408 CHAR(1) 5219 commands
1033 409 CHAR(1) 3812 SCS commands
1034 40A CHAR(1) Field outlining
1035 40B CHAR(1) Final form text
1036 40C CHAR(1) Bar code
1037 40D CHAR(1) Color
1038 40E CHAR(1) Drawer change
1039 40F CHAR(1) Character ID
1040 410 CHAR(1) Lines per inch changes
1041 411 CHAR(1) Font
1042 412 CHAR(1) Highlight
1043 413 CHAR(1) Page rotate
1044 414 CHAR(1) Subscript
1045 415 CHAR(1) Superscript
1046 416 CHAR(1) DDS
1047 417 CHAR(1) Final form feed
1048 418 CHAR(1) SCS data
1049 419 CHAR(1) User-generated data stream
1050 41A CHAR(1) Graphics
1051 41B CHAR(1) Unrecognizable data
1052 41C CHAR(1) ASCII transparency
1053 41D CHAR(1) IPDS transparent data
1054 41E CHAR(1) OfficeVision
1055 41F CHAR(1) Lines-per-inch (lpi) value not supported
1056 420 CHAR(1) CPA3353 message
1057 421 CHAR(1) Set exception
1058 422 CHAR(1) Carriage control characters
1059 423 CHAR(1) Page position
1060 424 CHAR(1) Character not valid
1061 425 CHAR(1) Lengths present
1062 426 CHAR(1) 5A present
1063 427 CHAR(1) Reserved
1064 428 BINARY(4) Number of font array entries
1068 42C BINARY(4) Number of resource library entries
1072 430 CHAR(1153) Font equivalence array
2225 8B1 CHAR(631) Resource library array
2856 B28 CHAR(1) AS/400-created AFPDS
2857 B29 CHAR(1) Job character ID specified
2858 B2A CHAR(1) S36 CONTINUE-YES
2859 B2B CHAR(10) Decimal format
2869 B35 CHAR(7) Date file last used
2876 B3C CHAR(1) Page groups
2877 B3D CHAR(1) Group level index tags
2878 B3E CHAR(1) Page level index tags
2879 B3F CHAR(1) IPDS pass-through
2880 B40 BINARY(4) Offset to user resource library list
2884 B44 BINARY(4) Number of user resource library list entries
2888 B48 BINARY(4) Length of user resource library list entry
2892 B4C CHAR(2) Reserved
2894 B4E CHAR(1) Corner staple
2895 B4F CHAR(1) Edge-stitch reference edge
2896 B50 PACKED(15,5) Offset from edge-stitch reference edge
2904 B58 BINARY(4) Edge-stitch number of staples
2908 B5C BINARY(4) Offset to edge-stitch staple offset list
2912 B60 BINARY(4) Number of edge-stitch staple offset entries
2916 B64 BINARY(4) Length of edge-stitch staple offset entry
2920 B68 CHAR(10) Font resolution for formatting
2930 B72 CHAR(1) Record format name present in data stream
2931 B73 CHAR(1) Saddle-stitch reference edge
2932 B74 BINARY(4) Saddle-stitch number of staples
2936 B78 BINARY(4) Offset to saddle-stitch staple offset list
2940 B7C BINARY(4) Number of saddle-stitch staple offset entries
2944 B80 BINARY(4) Length of saddle-stitch staple offset entry
2948 B84 PACKED(15,0) Data stream size
2956 B8C BINARY(4) Offset to library list
2960 B90 BINARY(4) Number of library list entries
2964 B94 BINARY(4) Length of library list entry
2968 B98 BINARY(4) Offset to Internet Print Protocol spooled file attributes
2972 B9C CHAR(180) Reserved
3152 C50 PACKED(15,5) Front margin offset down
3160 C58 PACKED(15,5) Front margin offset across
3168 C60 PACKED(15,5) Back margin offset down
3176 C68 PACKED(15,5) Back margin offset across
3184 C70 PACKED(15,5) Length of page
3192 C78 PACKED(15,5) Width of page
3200 C80 CHAR(10) Measurement method
3210 C8A CHAR(1) Advanced Function Printing (AFP) resource
3211 C8B CHAR(10) Character set name
3221 C95 CHAR(10) Character set library name
3231 C9F CHAR(10) Code page name
3241 CA9 CHAR(10) Code page library name
3251 CB3 CHAR(10) Coded font name
3261 CBD CHAR(10) Coded font library name
3271 CC7 CHAR(10) DBCS-coded font name
3281 CD1 CHAR(10) DBCS-coded font library name
3291 CDB CHAR(10) User-defined file
3301 CE5 CHAR(10) Reduce output
3311 CEF CHAR(1) Constant back overlay
3312 CF0 BINARY(4) Output bin
3316 CF4 BINARY(4) CCSID
3320 CF8 CHAR(100) User-defined text
3420 D5C CHAR(8) System where file created
3428 D64 CHAR(8) Net ID where file created
3436 D6C CHAR(10) User who created file
3446 D76 CHAR(2) Reserved
3448 D78 BINARY(4) Offset to user-defined options
3452 D7C BINARY(4) Number of user-defined options returned
3456 D80 BINARY(4) Length of each user-defined option entry
3460 D84 CHAR(255) User-defined data
3715 E83 CHAR(10) User-defined object name
3725 E8D CHAR(10) User-defined object library name
3735 E97 CHAR(10) User object type
3745 EA1 CHAR(3) Reserved
3748 EA4 PACKED(15,5) Character set point size
3756 EAC PACKED(15,5) Coded font point size
3764 EB4 PACKED(15,5) DBCS-coded font point size
3772 EBC BINARY(4) Auxiliary storage pool
3776 EC0 BINARY(4) Spooled file size
3780 EC4 BINARY(4) Spooled file size multiplier
3784 EC8 BINARY(4) Internet print protocol job identifier
3788 ECC CHAR(1) Spooled file creation security method
3789 ECD CHAR(1) Spooled file creation authentication method
3790 ECE CHAR(7) Date writer began processing spooled file
3797 ED5 CHAR(6) Time writer began processing spooled file
3803 EDB CHAR(7) Date writer completed processing spooled file
3810 EE2 CHAR(6) Time writer completed processing spooled file
EOF
);


#Generic Header Section
#The following table shows the generic header information returned for the SPFR0100, SPFR0200, and SPFR0300 formats.
#For more details about the fields, see Field Descriptions.

my @gen_header_def = split("\n", <<EOF
0 0 CHAR(64) Generic user area
64 40 BINARY(4) Size of header
68 44 CHAR(4) Structure level
72 48 CHAR(6) Spooled file level
78 4E CHAR(8) Format of the information returned
86 56 CHAR(1) Information complete indicator
87 57 CHAR(1) Reserved
88 58 BINARY(4) Size of user space used
92 5C BINARY(4) Offset to first buffer
96 60 BINARY(4) Number of buffers requested
100 64 BINARY(4) Number of buffers returned
104 68 BINARY(4) Size of print data
108 6C BINARY(4) Number of complete pages
112 70 BINARY(4) Number of first page
116 74 BINARY(4) Offset to first page
120 78 CHAR(8) Reserved
EOF
);

#Buffer Information Section

my @buffer_info_def = split("\n", <<EOF
0 0 BINARY(4) Length of all buffer information
4 4 BINARY(4) Ordinal number of the buffer
8 8 BINARY(4) Offset to general information section
12 C BINARY(4) Size of general information section
16 10 BINARY(4) Offset to page data section
20 14 BINARY(4) Size of page data section
24 18 BINARY(4) Number of page entries
28 1C BINARY(4) Size of page entry
32 20 BINARY(4) Offset to print data section
36 24 BINARY(4) Size of print data section
EOF
);

#General Information Section
#The general information section of formats SPFR0100 and SPFR0200 has the following structure.
#For more details about the fields in the following table, see Field Descriptions.

my @gen_info_def = split("\n", <<EOF
0 0 BINARY(4) Nonblank lines in buffer
4 4 BINARY(4) Nonblank lines in first page
8 8 BINARY(4) Buffer number of error information
12 C BINARY(4) Offset to error recovery information
16 10 BINARY(4) Size of print data
20 14 CHAR(10) State
30 1E CHAR(1) Last page continues
31 1F CHAR(1) Advanced print function file
32 20 CHAR(1) LAC command array in buffer
33 21 CHAR(1) Any buffer had LAC
34 22 CHAR(1) Error recovery information contains LAC
35 23 CHAR(1) Error recovery information
36 24 CHAR(1) Zero pages
37 25 CHAR(1) Load font
38 26 CHAR(1) IPDS data
39 27 CHAR(5) Reserved
EOF
);


#Page Data Section

my @page_data_def = split("\n", <<EOF
0 0 BINARY(4) Text data start
4 4 BINARY(4) Any data start
8 8 BINARY(4) Page offset
EOF
);

sub _create_unpacker_definition {
  my $def_lines = shift; my $unpack_string = ""; my $unpack_field; my @unpack_fields = (); my $line;

  for $line (@$def_lines) {
    my ($dec_off, $hex_off, $data_type, $data_length, $data_precision, $data_descr) = $line =~ /(\d+) ([\d,A-F]+) ([A-Z]+)\((\d+)(?:,(\d+))?\) (.*)$/ 
      or 
    die("??????? $line\n"); 
  
    $data_precision ||= 0; $unpack_field = ""; #$data_descr =~ s/[ ]/_/g; 

    if ($data_type eq "CHAR") {
      $unpack_field = "e$data_length";
    }
    elsif ($data_type eq "BINARY") {
      $unpack_field = "m$data_length";
    }
    elsif ($data_type eq "PACKED") {
      $data_length += $data_length%2; $data_length /= 2;
      $unpack_field = "p$data_length.$data_precision";
    }
    elsif ($data_type eq "ARRAY") {
      $unpack_field = "e$data_length";
    }
    else {
      die "WHAT FORMAT TYPE ?? $data_type $line ??\n";
    }

    $unpack_string .= ((0 and $dec_off) ? "\@$dec_off " : "")."$unpack_field "; push @unpack_fields, $data_descr;
  }

  return (unpack_string => $unpack_string, unpack_fields => \@unpack_fields);
}

my $unpacker_table = undef;

sub _init {
  $unpacker_table = {};

  my @def_list = (
    SPLA0100 => \@SPLA0100_def, 
    SPLA0200 => \@SPLA0200_def,
    gen_header => \@gen_header_def,
    buffer_info => \@buffer_info_def,
    gen_info => \@gen_info_def, 
    page_data => \@page_data_def
  );

  while(@def_list) {
    my ($lname, $ldef) = splice(@def_list, 0, 2);
    $unpacker_table->{$lname} = {_create_unpacker_definition($ldef)};
  }
}

sub _get_unpacker {
  my ($lformat, $lrec) = @_; _init() if !defined($unpacker_table); 
  
  my $unpacker_def = $unpacker_table->{$lformat}; 
  my (
    $unpack_string, $unpack_fields
  ) = 
  @{$unpacker_def}{qw(unpack_string unpack_fields)}; 

  return XReport::INPUT::AS400::data_record->new($lrec, $lformat, $unpack_string, $unpack_fields);
}


sub get_print_data_list {
  my $self = shift; my $lrec; my $page_data_list = [];
  
  my (
    $INPUT, $print_tot_buffers, $print_read_buffers
  ) = 
  @{$self}{qw(INPUT print_tot_buffers print_read_buffers)}; return $page_data_list if $print_read_buffers >= $print_tot_buffers;

  if ($INPUT->eof()) {
    die("eof while read_buffers($print_read_buffers) < tot_buffers($print_tot_buffers) !!\n");
  }

  $INPUT->seek(-64, 1);
#  warn "REF: ", ref($INPUT), "gen_header (POS: ", $INPUT->{loffset}, "):", "\n";
  $INPUT->read($lrec, 128); my $gen_header_offset = 128; my $buff_length = 0;
#  warn "REF: ", ref($INPUT), "gen_header (POS: ", $INPUT->{loffset}, "):", unpack("H*", $lrec), "\n";
  my $gen_header = _get_unpacker('gen_header', $lrec); push @$page_data_list, $gen_header;
  my (
    $header_type, $off_first_buffer, $block_tot_buffers
  ) = 
  $gen_header->getValues('Format of the information returned', 'Offset to first buffer', 'Number of buffers returned');

  die "NOT Tot Buffers ????? $header_type, $off_first_buffer, $block_tot_buffers ???\n" if !$block_tot_buffers || $block_tot_buffers <= 0;

  for (1..$block_tot_buffers) {
    $INPUT->read($lrec, 4); $INPUT->seek(-4, 1); $buff_length = unpack("N", $lrec); my ($buffer_info, $gen_info, $page_data, $print_data);

    $INPUT->read($lrec, $buff_length); $buffer_info = _get_unpacker('buffer_info', $lrec);

    my (
      $gen_info_offset, $page_data_offset, $print_data_offset,
      $gen_info_size, $page_data_size, $print_data_size
    ) =
    $buffer_info->getValues(
      'Offset to general information section', 'Offset to page data section', 'Offset to print data section',
      'Size of general information section', 'Size of page data section', 'Size of print data section'
    );

    $gen_info = _get_unpacker('gen_info', substr($lrec, $gen_info_offset-$gen_header_offset, $gen_info_size)) if $gen_info_size;
    $page_data = _get_unpacker('page_data', substr($lrec, $page_data_offset-$gen_header_offset, $page_data_size)) if $page_data_size;

    $print_data = substr($lrec, $print_data_offset-$gen_header_offset, $print_data_size);

    if ($page_data) {
      #print join(',', $page_data->getValues('Text data start', 'Any data start', 'Page offset'), $print_data_size, length($print_data)), "\n";
    }

    push @$page_data_list, [$gen_info, $page_data, $print_data];
  }
  continue {
    $print_read_buffers += 1; last if $print_read_buffers == $print_tot_buffers; $gen_header_offset += $buff_length; 
  }

  @{$self}{qw(print_read_buffers)} = ($print_read_buffers); return $page_data_list
}

sub getValues {
  my $self = shift; my $print_header = $self->{'print_header'}; $print_header->getValues(@_);
}

sub eof {
  my $self = shift; $self->{'print_read_buffers'} >= $self->{'print_tot_buffers'};
}

sub new {
  my ($class, $INPUT) = @_;  $INPUT->seek(0, 0); my ($lrec);

  $INPUT->read($lrec, 16); $INPUT->seek(0, 0);

  my ($lreturned, $lavailable, $lformat) = unpackeb("m4m4e8", $lrec);

  $INPUT->read($lrec, $lreturned);

  my $print_header = _get_unpacker($lformat, $lrec);

  my ($print_tot_buffers) = $print_header->getValues('Number of buffers');

  my $self = {
    INPUT => $INPUT, print_header => $print_header, print_tot_buffers => $print_tot_buffers, print_read_buffers => 0
  };
  i::logit("AS400::data_unpacker: accessing ".ref($INPUT)." upon request of ".join('::',(caller())[0,2]));
  bless $self, $class;
}

1;

package XReport::INPUT::AS400::reader;

use strict;

sub getValues {
  my $self = shift; my $data_unpacker = $self->{'data_unpacker'}; $data_unpacker-> getValues(@_);
}

sub Open {
  my $self = shift; my ($INPUT, $byte_cache) = ($self->{'INPUT'}, ""); $INPUT->Open();  
 
  @{$self}{qw(data_unpacker byte_cache print_data_offset)} = (XReport::INPUT::AS400::data_unpacker->new($INPUT), \$byte_cache, 0);
}

sub Close {
  my $self = shift; my ($INPUT, $byte_cache) = ($self->{'INPUT'}, ""); 

  @{$self}{qw(data_unpacker byte_cache print_data_offset)} = (undef, \$byte_cache, 0); $INPUT->Close(); 
}

sub eof {
  my $self = shift; my ($data_unpacker, $byte_cache) = @{$self}{qw(data_unpacker byte_cache)};

  return $$byte_cache eq "" && $data_unpacker->eof();
}

sub read {
  my ($self, undef, $length) = @_; my ($data_unpacker, $byte_cache) = @{$self}{qw(data_unpacker byte_cache)};

  my ($print_data_list, $gen_buff, $gen_info, $page_data, $print_data);

  while ($length > length($$byte_cache)) {
    $print_data_list = $data_unpacker->get_print_data_list(); last if !@$print_data_list;
    $gen_buff = shift(@$print_data_list);
    for (@$print_data_list) {
      ($gen_info, $page_data, $print_data) = @$_;
      $$byte_cache .= $print_data;
    }
  }

  $length = length($$byte_cache) if length($$byte_cache) < $length;
    
  $_[1] = substr($$byte_cache, 0, $length); 
  
  $$byte_cache = substr($$byte_cache, $length); $self->{print_data_offset} += $length; return $length;
}

sub tell {
  my $self = shift; return $self->{print_data_offset};
}


use constant { FLDS_TO_SAVE => [ 
                                    'Front margin offset across',
                                    'User object type',
                                    'Save file after written',
                                    'User-specified data',
                                    'User name',
                                    'Page position',
                                    'ASCII transparency',
                                    'DBCS shift-out shift-in (SO/SI) spacing',
                                    'Drawer change',
                                    'Lines-per-inch (lpi) value not supported',
                                    'Transparency',
                                    'DBCS extension characters',
                                    'Code page',
                                    'Align forms',
                                    'Corner staple',
                                    'Alternate forms length',
                                    'Character set name',
                                    'Character not valid',
                                    'Back overlay name',
                                    'Device file name',
                                    'Page groups',
                                    'User who created file',
                                    'Back overlay offset down',
                                    'Front overlay offset across',
                                    'Control character',
                                    'Set Line Density command',
                                    'Alternate forms width',
                                    'System where file created',
                                    'Line spacing',
                                    'Page rotation',
                                    'Graphics',
                                    'Job number',
                                    'Form definition name',
                                    'Edge-stitch number of staples',
                                    'Double-wide characters',
                                    'DBCS-coded font library name',
                                    'Document name',
                                    'Height of drawer 2',
                                    'DBCS-coded font name',
                                    'Font resolution for formatting',
                                    'Overflow line number',
                                    'Back margin offset across',
                                    'Graphics token',
                                    'SCS data',
                                    'Bar code',
                                    'Width of drawer 2',
                                    'Accounting code',
                                    'Length of saddle-stitch staple offset entry',
                                    'Replace unprintable characters',
                                    'Spooled file name',
                                    'Measurement method',
                                    'User-defined object name',
                                    'User-defined file',
                                    'Page level index tags',
                                    'Point size',
                                    'Page definition name',
                                    'IPDS transparent data',
                                    'Internet print protocol job identifier',
                                    'Form feed',
                                    'Subscript',
                                    'Font equivalence array',
                                    'Field outlining',
                                    'Width of page',
                                    'Folder name',
                                    'Color',
                                    'Height of drawer 1',
                                    'Length of page',
                                    'Offset from edge-stitch reference edge',
                                    'Saddle-stitch number of staples',
                                    'Number of edge-stitch staple offset entries',
                                    'Carriage control characters',
                                    'Front overlay name',
                                    'Starting page',
                                    'Saddle-stitch reference edge',
                                    'Fold records',
                                    'Source drawer',
                                    'Reduce output',
                                    'Page length',
                                    'Constant back overlay',
                                    'Font',
                                    'Device type',
                                    'Job character ID specified',
                                    'Character ID',
                                    'Printer device type',
                                    'User-defined data',
                                    'Extended code page',
                                    'Character set point size',
                                    'Unit of measure',
                                    'Total copies',
                                    'Channel value array',
                                    'Output queue name',
                                    'Characters per inch',
                                    'Coded font name',
                                    'Program that opened file name',
                                    'Justification',
                                    'Lengths present',
                                    'TRC for 1403',
                                    'Front overlay offset down',
                                    'Maximum forms width',
                                    'DBCS character rotation commands',
                                    'Decimal format',
                                    'Record format',
                                    'Graphic character set',
                                    'Printer font',
                                    'Alternate lines per inch',
                                    'Define characters',
                                    'Character code',
                                    'Back overlay offset across',
                                    'Lines per inch',
                                    'Coded font array',
                                    'Final form text',
                                    '5A present',
                                    'Multiple up (pages per side)',
                                    'Code page name',
                                    'Group level index tags',
                                    'Spooled file number',
                                    'Page rotate',
                                    'Ending page',
                                    'Replacement character',
                                    'Print text',
                                    'Job name',
                                    'Edge-stitch reference edge',
                                    'Front margin offset down',
                                    'User-defined text',
                                    'Print quality',
                                    'Width of drawer 1',
                                    'Superscript',
                                    'System/36 spooled file identifier',
                                    'Output priority',
                                    'DBCS character rotation',
                                    'Output bin',
                                    'Page width',
                                    'Form type',
                                    'Print on both sides (duplex)',
                                    'DBCS characters per inch',
                                    'Program that opened file library name',
                                    'DBCS-coded font point size',
                                    'Back margin offset down',
                                    'Coded font point size',
                                    'Net ID where file created',
			       ],
			       };

sub spoolFields { return @{+FLDS_TO_SAVE}; }

sub new {
  my ($class, $INPUT, $jr) = @_[0,1,2];

  my $self = { INPUT => $INPUT };
  bless $self, $class;

 $self->Open(); 
  my ( $LinesPerPage, $CharsPerLine, $CPI, $LPI,  $PageLen) = 
    $self->getValues('Page length', 'Width of page', 'Characters per inch', 'Lines per inch', 'Overflow line number');
#  ($CPI, $LPI) = map { 720 / $_  } ($CPI, $LPI) if ($CPI && $LPI);
  
  my ($FontX, $FontY) = map { 720 / $_  } ($CPI, $LPI) if ($CPI && $LPI);
#  my ($FontX, $FontY) = ($CPI, $LPI) if ($CPI && $LPI);
  $FontX = ((($FontX * $CharsPerLine) - 60) / $CharsPerLine); 
  $FontY = ((($FontY * $LinesPerPage) - 56) / $LinesPerPage); 
  $FontX /= 0.6;
  i::logit("AS400 reader: Page Size set to Lines: $LinesPerPage Chars: $CharsPerLine FontSize: [ $FontX $FontY ] CPI-LPI: [ $CPI $LPI ]");
  $jr->setValues(LinesPerPage => $LinesPerPage) if $LinesPerPage;
  $jr->setValues(CharsPerLine => $CharsPerLine) if $CharsPerLine;
  $jr->setValues(FontSize => "[ $FontX $FontY ]") if ($FontX && $FontY);
  my $os400attrs = { };
  @{$os400attrs}{@{+FLDS_TO_SAVE}} = $self->getValues(@{+FLDS_TO_SAVE});
  $jr->setValues(OS400attrs => $os400attrs);
  i::logit("AS400 reader: Spool Attrs: Total copies: " . $os400attrs->{'Total copies'});

  return $self;
}

1;


__END__

$VAR1 = bless( {
                 'data_values' => {
                                    'Front margin offset across' => '0',
                                    'User object type' => '',
                                    'Save file after written' => '*YES',
                                    'User-specified data' => 'TOR07R',
                                    'User name' => 'XFTEST1',
                                    'Page position' => 'N',
                                    'Form definition library name' => '',
                                    'ASCII transparency' => 'N',
                                    'DBCS shift-out shift-in (SO/SI) spacing' => '*YES',
                                    'Drawer change' => 'N',
                                    'Lines-per-inch (lpi) value not supported' => 'N',
                                    'Transparency' => 'N',
                                    'DBCS extension characters' => '*YES',
                                    'Output queue library name' => 'QUSRSYS',
                                    'Code page' => '*DEVD',
                                    'Align forms' => '*NO',
                                    'Corner staple' => '0',
                                    'Alternate forms length' => '0',
                                    'Character set name' => '*FONT',
                                    'Offset to Internet Print Protocol spooled file attributes' => '0',
                                    'Character not valid' => 'N',
                                    'Back overlay name' => '*FRONTOVL',
                                    'Last page printed' => '0',
                                    'Reserved' => '   ',
                                    'Device file name' => 'TOR05RP',
                                    'Page groups' => 'N',
                                    'User-generated data stream' => 'N',
                                    'User who created file' => 'XFTEST1',
                                    'Back overlay offset down' => '0',
                                    'Front overlay offset across' => '0',
                                    'Control character' => '*NONE',
                                    'Set Line Density command' => 'N',
                                    'Restart printing' => '-1',
                                    'File available' => '*FILEEND',
                                    'Alternate forms width' => '0',
                                    'System where file created' => 'BACSAVE',
                                    'Line spacing' => '*CTLCHAR',
                                    'Front overlay library name' => '',
                                    'Code page library name' => '',
                                    'Set exception' => 'N',
                                    'Date file last used' => '1090514',
                                    'Offset to user resource library list' => '0',
                                    'User-defined object library name' => '',
                                    'Page rotation' => '-1',
                                    'Graphics' => 'N',
                                    'System/38 Text Utility flags' => '  ',
                                    'Job number' => '117940',
                                    'Page count estimated' => 'N',
                                    'Form definition name' => '',
                                    'Edge-stitch number of staples' => '0',
                                    'Double-wide characters' => 'N',
                                    'DBCS-coded font library name' => '',
                                    'Number of user-defined options returned' => '0',
                                    'Document name' => '',
                                    'Height of drawer 2' => '0',
                                    'Auxiliary storage pool' => '1',
                                    'DBCS-coded font name' => '*SYSVAL',
                                    'Length of each user-defined option entry' => '10',
                                    'Font resolution for formatting' => '*DEVD',
                                    'Overflow line number' => '60',
                                    'Back margin offset across' => '0',
                                    '3812 SCS' => 'N',
                                    'Spooled file size multiplier' => '4096',
                                    'Graphics token' => '',
                                    'Volumes (array)' => '',
                                    'Time file opened' => '121627',
                                    'Number of user resource library list entries' => '-1', 
                                    'SCS data' => 'N',
                                    'Status' => '*SENDING',
                                    'Bar code' => 'N',
                                    'Width of drawer 2' => '0',
                                    'Exchange type' => '          ',
                                    'Accounting code' => '',
                                    'Length of saddle-stitch staple offset entry' => '8',
                                    'Total pages' => '1',
                                    'Replace unprintable characters' => 'Y',
                                    'Spooled file name' => 'TOR05RP',
                                    'CCSID' => '65535',
                                    'Measurement method' => '*ROWCOL',
                                    'User-defined object name' => '*NONE',
                                    'User-defined file' => '*NO',
                                    'Page level index tags' => 'N',
                                    'Point size' => '0',
                                    'Page definition name' => '',
                                    'Data stream size' => '1037',
                                    'IPDS pass-through' => '0',
                                    'Number of library list entries' => '17',
                                    'IPDS transparent data' => 'N',
                                    'Total records' => '0',
                                    'Internet print protocol job identifier' => '31',
                                    'Form feed' => '*DEVD',
                                    'Maximum records' => '0',
                                    'Subscript' => 'N',
                                    'Length of user resource library list entry' => '10',
                                    'Font equivalence array' => '',
                                    'Field outlining' => 'N',
                                    'Width of page' => '80',
                                    'DBCS data' => '*NO',
                                    'Record format name present in data stream' => 'N',
                                    'Folder name' => '',
                                    'Page or record being written' => '0',
                                    'Unrecognizable data' => 'N',
                                    'OfficeVision' => 'N',
                                    'Advanced Function Printing (AFP) resource' => 'N',
                                    'Number of resource library entries' => '17',
                                    'Color' => 'N',
                                    'Height of drawer 1' => '0',
                                    'Length of page' => '66',
                                    'Offset from edge-stitch reference edge' => '-1',
                                    'Offset to user-defined options' => '0',
                                    'Hold file before written' => '*NO',
                                    'Number of separators' => '0',
                                    'Saddle-stitch number of staples' => '0',
                                    'Maximum spooled data record size' => '4079',
                                    'Time writer began processing spooled file' => '100604',
                                    'Character set library name' => '',
                                    'Number of edge-stitch staple offset entries' => '0',
                                    'Carriage control characters' => 'N',
                                    'Final form feed' => 'Y',
                                    'Front overlay name' => '*NONE',
                                    'Date file opened' => '1090513',
                                    'Starting page' => '0',
                                    'Internal spooled file identifier' => '�g{� ? �)���)�-',
                                    'Spooled file creation authentication method' => ' ',
                                    'Saddle-stitch reference edge' => '0',
                                    'Number of buffers' => '1',
                                    'Fold records' => '*NO',
                                    'Source drawer' => '1',
                                    'Reduce output' => '*TEXT',
                                    'Page length' => '66',
                                    'Constant back overlay' => '0',
                                    'Length of edge-stitch staple offset entry' => '8',
                                    'System/36 procedure name' => '',
                                    'Resource library array' => 'QSYS      QSYS2     QHLPSYS   QUSRSYS   PERS_CBRS SIBPER_PTFSIBPER    SIBEV5_PTFSIBEV5_OBJSIBXV5_PTFSIBXV5_OBJDATALIBT  PFTOOL    SIBEV5INSTSIBXV5INSTQGPL      MIGRXFRSM',
                                    'Font' => 'N',
                                    'Spooled file buffer size' => '4079',
                                    'Length of library list entry' => '10',
                                    'Offset to library list' => '3841',
                                    'Spooled file size' => '8',
                                    'Device type' => 'PRINTER',
                                    'Job character ID specified' => 'N',
                                    'Character ID' => 'N',
                                    'Printer device type' => '*SCS',
                                    'User-defined data' => '*NONE',
                                    'Extended code page' => 'N',
                                    'Character set point size' => '0',
                                    'Offset to saddle-stitch staple offset list' => '0',
                                    'Unit of measure' => '*INCH',
                                    'Lines per inch changes' => 'N',
                                    'Total copies' => '1',
                                    'Characters per inch changes' => 'N',
                                    'Channel value array' => ' ',
                                    'Record length' => '-1',
                                    'Date writer completed processing spooled file' => '',
                                    'Output queue name' => 'TEST_SPOOL',
                                    'Characters per inch' => '100',
                                    'CPA3353 message' => 'N',
                                    'Device file library name' => 'SIBXV5_OBJ',
                                    'Coded font name' => '*FNTCHRSET',
                                    'Program that opened file name' => 'TOR07R',
                                    'Justification' => '0',
                                    'Number of font array entries' => '0',
                                    'Lengths present' => 'N',
                                    'File open' => 'N',
                                    'TRC for 1403' => 'N',
                                    'Front overlay offset down' => '0',
                                    'Maximum forms width' => '80',
                                    'DBCS character rotation commands' => 'N',
                                    'Decimal format' => '*JOB',
                                    'Record format' => '*FIXED',
                                    'AS/400-created AFPDS' => 'N',
                                    'Graphic character set' => '*DEVD',
                                    'Printer font' => '*CPI',
                                    'Alternate lines per inch' => '0',
                                    'Define characters' => 'N',
                                    'S36 CONTINUE-YES' => 'N',
                                    'Character code' => '          ',
                                    'Back overlay library name' => '',
                                    'Back overlay offset across' => '0',
                                    'Graphics error actions' => 'N',
                                    '3812 SCS commands' => 'N',
                                    'Lines per inch' => '60',
                                    'Highlight' => 'N',
                                    'Coded font array' => '',
                                    'Final form text' => 'N',
                                    '5A present' => 'N',
                                    'Multiple up (pages per side)' => '1',
                                    'Spooled file level' => 'V5R4M0',
                                    'File stopped on page boundary' => 'N',
                                    'Channel mode' => '*NORMAL',
                                    'Code page name' => '',
                                    'Group level index tags' => 'N',
                                    'Bytes returned' => '4011',
                                    'Spooled file number' => '1',
                                    'Page rotate' => 'N',
                                    'Ending page' => '0',
                                    'Replacement character' => '',
                                    'Print text' => '',
                                    'Job name' => 'DSPEDU03B',
                                    'Edge-stitch reference edge' => '0',
                                    'Front margin offset down' => '-2',
                                    'User-defined text' => '',
                                    'Print quality' => '*STD',
                                    'Page definition library name' => '',
                                    'FFT emphasis' => 'N',
                                    'DDS' => 'Y',
                                    'Width of drawer 1' => '0',
                                    'Coded font library name' => '',
                                    'Superscript' => 'N',
                                    'System/36 spooled file identifier' => 'B80477',
                                    'Output priority' => ' 5',
                                    'DBCS character rotation' => '*NO',
                                    'Internal job identifier' => ' ) ? ?+ ��:-��0 ',
                                    'Date writer began processing spooled file' => '1090514',
                                    'Output bin' => '0',
                                    'Page width' => '80',
                                    'Form type' => '*STD',
                                    'Print on both sides (duplex)' => '*NO',
                                    'File label identifier' => '                 ',
                                    'Number of saddle-stitch staple offset entries' => '0', 
                                    'DBCS characters per inch' => '-1',
                                    'Program that opened file library name' => 'SIBXV5_OBJ',
                                    'Spooled file creation security method' => ' ',
                                    'DBCS-coded font point size' => '0',
                                    'Back margin offset down' => '-1',
                                    'Coded font point size' => '0',
                                    'Bytes available' => '4011',
                                    'Print fidelity' => '*CONTENT',
                                    '5219 commands' => 'N',
                                    'Offset to edge-stitch staple offset list' => '0',
                                    'Net ID where file created' => 'NWCRRO00',
                                    'Time writer completed processing spooled file' => '',
                                    'Format name' => 'SPLA0200',
                                    'Copies left to produce' => '1'
                                  },
                 'data_type' => 'SPLA0200'
               }, 'XReport::INPUT::AS400::data_record' );
