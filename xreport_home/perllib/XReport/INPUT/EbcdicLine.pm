package XReport::INPUT::EbcdicLine;

use strict; use bytes;

sub cleanLine(\$) {
  my $ref = shift; my ($cc, $line) = (substr($$ref,0,1), $$ref);
  if ( $cc eq "\x5a" ) {
    ### delete trailing spaces of afp records
    $line = substr($line, 0, unpack("n", substr($line,1,2))+1);
  }
  elsif ( length($line) > 2 ) {
	substr($line,2) =~ s/[\x00-\x3f]/\x40/g;
    substr($line,2) =~ s/\x40+$//;
  }
  $$ref = $line;
}

sub overwriteLine {
  my ($self, $line) = (shift, shift); return if $line =~ /^.\x40*$/; my $eraser = $line; $self->[1] = "";

  $eraser =~ s/[^\x40]/\x00/g; $eraser =~ s/[^\x00]/\xff/g; $self->[0] &= $eraser; 
  $line =~ s/\x40/\x00/g; $self->[0] |= $line; $self->[0] =~ s/\x00/\x40/g; 

}

sub AsciiValue {
  my $self = shift;
  
  if ( defined($_[0]) ) {
    $self->[1] = $_[0];
  }
  if ( $self->[1] eq "" ) {
    my $line = $self->[0]; my $cc = substr($line,0,1);
    if ($cc eq "\x5a" ) {
      $self->[1] = unpack("H24", $line);
    }
    else {
      $self->[1] = $cc.$translator->toascii(substr($line,1)); $self->[1] =~ s/\x00/ /g;
    }
  }
  
  return $self->[1];
}

sub Value {
  my $self = shift;
  
  if ( defined($_[0]) ) {
    $self->[0] = $_[0];
  }
  return $self->[0];
}

sub atLine {
  my ($self, $beg) = (shift, shift || 1); 
  
  if ( defined($_[3]) ) {
    $self->[3] = $_[0];
  }
  return $self->[3] + ($beg == 1 ? 0 : $self->[2]);
}

sub new {
  my ($className, $page, $atLine, $line) = @_; cleanLine($line);
  
  my $self = [$line, "", $page->atLine(), $atLine];
  
  bless $self, $className;
}


#------------------------------------------------------------
1;
