package XReport::INPUT::IPlain;

use strict; use bytes;

use Symbol;

#manage using original file size for read and seek;

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  if (!$INPUT) {
    $INPUT = gensym();
  }
  else {
    $self->Close();
  }
  open($INPUT,"<",$self->{fileName}); binmode($INPUT);
  
  return $self->{INPUT} = $INPUT;;
}

sub Close {
  return CORE::close($_[0]->{INPUT});
}

sub read {
  return CORE::read($_[0]->{INPUT}, $_[1], $_[2]);
}

sub tell {
  return CORE::tell($_[0]->{INPUT});
}

sub eof {
  return CORE::eof($_[0]->{INPUT});
}

sub new {
  my ($className, $fileName) = @_; my $self = {};
  
  $self->{fileName} = $fileName;
  
  bless $self;
}

#------------------------------------------------------------
1;
