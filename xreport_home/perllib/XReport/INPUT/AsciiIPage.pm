package XReport::INPUT::AsciiIPage;

our @ISA = ('XReport::INPUT::IPage');

use strict; use bytes;

sub NewPage {
  my $self = shift; $self->{atPage} += 1;  
  
  my $page = { 
    INPUT  => $self,
    overflowed => 0,
    atPage => $self->{atPage},
    atLine => $self->{atLine},
    lineList => [],
    atOffset => $self->tell()
  };

  bless $page, 'XReport::INPUT::AsciiIPage';
}

sub overflowed {
  my $self = shift; return $self->{overflowed} if !scalar(@_); $self->{overflowed} = shift;
}

sub totLines {
  my $self = shift; my $lineList = $self->{lineList};
  return scalar(@$lineList);
}

sub atPage {
  my $self = shift;
  return $self->{atPage};
}

sub atLine {
  my $self = shift;
  return $self->{atLine};
}

sub atOffset {
  my $self = shift;
  return $self->{atOffset};
}

sub appendLine {
  my $self = shift; my ($INPUT, $lineList) = @{$self}{qw(INPUT lineList)};
  push @$lineList, XReport::INPUT::AsciiLine->new($self, scalar(@$lineList)+1, @_); $INPUT->{atLine} += 1; 
}

sub overwriteLastLine {
  my $self = shift; my $page = $self->{lineList};
  $page->[$#$page]->overwriteLine(@_);
}

sub lineList {
  my $self = shift; 
  return $self->{lineList};
}

sub getLine {
  my $self = shift; 

  my ($lineOffsets, $lineCache) 
   = 
  @{$self}{qw(lineOffsets lineCache)}; 

  if ( !@$lineCache ) {
    my $INPUT = $self->{INPUT};
    $INPUT->getLines($lineOffsets, $lineCache , 77);
  }

  return if !scalar(@$lineCache);

  if ( wantarray ) {
    return (shift @$lineOffsets, shift @$lineCache);
  }
  else {
    shift @$lineOffsets; return shift @$lineCache;
  } 
}

sub tell {
  return $_[0]->{lineOffset};
}

sub eof {
  return (!@{$_[0]->{lineCache}} and $_[0]->{INPUT}->eof() and $_[0]->{lastLine} eq "");
}

sub GetPage {
  my $self = shift; return undef if $self->eof(); my ($INPUT, $lpp) = @{$self}{qw(INPUT lpp)};
  
  my ($lineOffset, $cc, $line) = @{$self}{qw(lineOffset lastcc lastLine)};
  
  my $page = $self->NewPage(); 

  if ( $line ne "" ) {
    $page->appendLine($line); ($cc, $line) = ();
  }
  
  my $totLines = $page->totLines();

  while(1) {
    ($lineOffset, $line) = $self->getLine(); $cc = substr($line,0,1); last if $line eq ""; 
	if ( ($cc eq "1" and $totLines > 0) or ($lpp and $totLines >= $lpp) ) {
	  last;
	}
	$page->appendLine($line); $totLines += 1; ($cc, $line) = (); 
  }
  @{$self}{qw(lineOffset lastcc lastLine)} = ($lineOffset, $cc, $line);
  
  return $page;
}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  @{$self}{qw(lineOffset lastcc lastLine)} = (0, '', '');
  
  @{$self}{qw(lineOffsets lineCache)} = ([], []);

  @{$self}{qw(atPage atLine)} = (0, 0);

  $INPUT->Open(@_); 
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  @{$self}{qw(lineOffset lastcc lastLine)} = (0, '', '');
  
  @{$self}{qw(lineOffsets lineCache)} = ([], []);

  @{$self}{qw(atPage atLine)} = (0, 0);
  
  $INPUT->Close(@_); 
}

sub new {
  my ($className, $self) = (shift, shift); 
  
  bless $self, $className;

  $self->setJobReportValues('cc','asa');

  $self->Open(@_);

  return $self;
}

#------------------------------------------------------------
1;
