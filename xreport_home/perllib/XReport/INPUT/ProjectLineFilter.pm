
package XReport::INPUT::ProjectLineFilter;

use strict;

use Symbol;

require MIME::Base64;

sub PrepareProject {
  my ($self, $ProjectName) = @_; my ($userlib, $workdir, $Projects) = @{$self}{qw(userlib workdir Projects)}; 
  
  my ($ProjectFileName, $lProjectFileName, $lProjectPdfFileName, $lxml);
  
  my ($INPUT, $OUTPUT) = (gensym(), gensym()); 

  $ProjectFileName = "$userlib/merge.projects/$ProjectName.xml"; 
  open($INPUT, "<$ProjectFileName")
   or 
  die("INPUT OPEN ERROR for \"$ProjectFileName\" $!"); binmode($INPUT); 
  
  read($INPUT, $lxml, -s "$ProjectFileName"); close($INPUT);

  my ($FlowQueue) = $lxml =~ /\<FlowQueue\><!\[CDATA\[(.*?)]]/mso;
  $lxml =~ s/\<FlowQueue\><!\[CDATA\[(.*?)]]/\<FlowQueue\><!\[CDATA\[\]\]/mso;
  my ($PDFBackgroundSource) = $lxml =~ /\<PDFBackgroundSource\><!\[CDATA\[(.*?)]]/mso;
  $lxml =~ s/\<PDFBackgroundSource\><!\[CDATA\[(.*?)]]/\<PDFBackgroundSource\><!\[CDATA\[\]\]/mso;
  my ($BackgroundFileName) = $lxml =~ /\<BackgroundFileName\><!\[CDATA\[(.*?)]]/mso;
  $lxml =~ s/\<BackgroundFileName\><!\[CDATA\[(.*?)]]/\<BackgroundFileName\><!\[CDATA\[\]\]/mso;

  for ($FlowQueue, $PDFBackgroundSource, $BackgroundFileName) {
    $_ = MIME::Base64::decode_base64($_);
  }

  mkdir "$workdir/localres" unless -d "$workdir/localres";
  $lProjectFileName = "$workdir/localres/$ProjectName.small.xml";
  open($OUTPUT, ">$lProjectFileName")
   or
  die("OUTPUT OPEN ERROR for \"$lProjectFileName\" $!"); binmode($OUTPUT);

  print $OUTPUT $lxml; close($OUTPUT);

  $lProjectPdfFileName = "$workdir/localres/$ProjectName.pdf";
  open($OUTPUT, ">$lProjectPdfFileName")
   or
  die("OUTPUT OPEN ERROR for \"$lProjectPdfFileName\" $!"); binmode($OUTPUT);

  print $OUTPUT $PDFBackgroundSource; close($OUTPUT);

  my $dom = XML::DOM::Parser->new()->parse($lxml);

  my $lineEditor = $dom->getElementsByTagName('LineEditor-Project')->[0];

  my $Project = $lineEditor->getElementsByTagName('Project')->[0];

  my $FrameCountBackGrounds = $Project->getAttribute("FrameCountBackGround");

  $Projects->{$ProjectName} = {
    lProjectFileName => $lProjectFileName, 
    lProjectPdfFileName => $lProjectPdfFileName, 
    lProjectBackGrounds => $FrameCountBackGrounds, 
  };
}

sub Open {
  my $self = shift; my ($INPUT, $tFileName) = @{$self}{qw(INPUT tFileName)};
  i::logit("ProjectLineFilter: opening \"$tFileName\" upon request of ".join('::', (caller())[0,2]));
  open($INPUT, "<$tFileName") 
    or 
  die("OUTPUT OPEN ERROR FOR \"$tFileName\" $!");
}

sub getLine {
  my $self = shift; my $INPUT = $self->{'INPUT'}; scalar(<$INPUT>);
}

sub getLines {
  my ($self, $olineOffsets, $olineCache, $omaxLines) = @_; my $Projects = $self->{'Projects'};
  my (
    $INPUT, $lineOffsets, $lineCache
  ) = 
  @{$self}{qw(INPUT lineOffsets lineCache)}; 
  
  warn "ProjectLineFilter getLines called by ".join('::', (caller(1))[0,2])." INPUT: ". ref($INPUT)."\n";  
  goto FILL if @$lineCache >= $omaxLines or $INPUT->eof();
  
  my ($ilineOffsets, $ilineCache, $imaxLines) = ([], [], $omaxLines); 
  
  while($imaxLines>0) {
    my $offset = tell($INPUT); my $line = <$INPUT>; 
	if ( $line eq "" ) {
	  last;
	}
	push @$ilineOffsets, $offset; push @$ilineCache, $line; $imaxLines -= 1;
  }

  while (@$ilineCache) {
    my ($ioffset, $iline) = (shift @$ilineOffsets, shift @$ilineCache); 
    
    next if $iline =~ /^\s*\x1a?\s*$/; 

    my $ProjectName = 'testProject';

    if (!exists($Projects->{$ProjectName})) {
      $self->PrepareProject($ProjectName); 
    }

    my $Project = $Projects->{$ProjectName};
    
    my  ($ProjectFileName, $FormFileName, $ForPages) = @{$Project}{qw(lProjectFileName lProjectPdfFileName lProjectBackGrounds)};
    warn "adding header\n";
    my @lines = map { $self->{xlator}->toebcdic($_) } (
      "1 --- MERGE=YES ----------------------------------",
      " ", 
      " ProjectFileName=$ProjectFileName", 
      " FormFileName=$FormFileName",
      " FromPage=1", 
      " ForPages=$ForPages", 
    );
    push @lines, $self->{xlator}->toebcdic(" DataRecord=").$iline; 
    for(1..$ForPages-1) {
      push @lines, map { $self->{xlator}->toebcdic($_) } (
      "1 --- MERGE=NO -----------------------------------",
      " ", 
      ); 
    }
    for (@lines) {
      push @$lineOffsets, $ioffset; push @$lineCache, $_;
    }
  }

  FILL:
  while($omaxLines > 0 and @$lineCache) {
    my ($offset, $line) = (shift @$lineOffsets, shift @$lineCache);
    push @$olineOffsets, $offset; push @$olineCache, $line;
    $omaxLines -= 1;
  }
  warn "ProjectLineFilter getLines returning ".scalar(@$olineCache)." lines\n";  
  
  return scalar(@$olineCache); 
}

sub eof {
  my $self = shift; my $INPUT = $self->{'INPUT'}; eof($INPUT);
}

sub Close {
  my $self = shift; my $INPUT = $self->{'INPUT'}; close($INPUT);
}

sub new {
  my ($class, $INPUT, $jr) = @_; my $OUTPUT = gensym();  my $itlines = 0;
  my ($userlib, $workdir, $SrvName) = c::getValues(qw(userlib workdir SrvName)); $workdir = "$workdir/$SrvName";

  my $tFileName = $jr->getFileName('TEMPFILE','Project');
  i::logit("$class new called by ".join('::', (caller())[0,2])." - INPUT is ".ref($INPUT));

  $INPUT->Open();
  open($OUTPUT, ">$tFileName") 
    or 
  die("OUTPUT OPEN ERROR FOR \"$tFileName\" $!"); 

  while(1) {
    my $line = $INPUT->getLine(); last if $line eq ''; print $OUTPUT substr($line, 1), "\n"; $itlines += 1;
  }
  close($OUTPUT); $INPUT->Close(); 

  my $self = {
    INPUT => $OUTPUT, lineOffsets => [], lineCache => [], 
    tFileName => $tFileName, itlines => $itlines, 
    userlib => $userlib, workdir => $workdir, Projects => {},
    xlator => $INPUT->{xlator}
  };
  i::logit("$class initialized - INPUT is ".ref($INPUT)." classINPUT is ".ref($self->{INPUT})." OUTFILE is \"$tFileName\"");

  bless $self, $class;
}

__PACKAGE__;

