#------------------------------------------------------------
package XReport::INPUT::IZlib;

use strict; use bytes;

use Symbol;
use Compress::Zlib;

#manage using original file size for read and seek;

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  if ( ref($INPUT) eq 'XReport::INPUT::IZlib' ) {
    $self->Close();
  }
  
  $INPUT = gzopen($self->{fileName}, "rb") 
   or 
  die("GZOPEN INPUT ERROR for $self->{fileName} rc=$!");

  $self->{INPUT} = $INPUT;

  return $INPUT;
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};

  $INPUT->gzclose();
  
  $self->{INPUT} = $INPUT = undef; 
}

sub read {
  my $self = shift; my $INPUT = $self->{INPUT};
  $main::reads += 1; $main::fpos = $INPUT->gztell();
  i::logit("IZlib call read invoked by ".join('::', (caller())[0,2]));
  my $br = $INPUT->gzread($_[0], $_[1]);

  return $br if $br >= 0;

  die("GZREAD ERROR DETECTED (rc=$br). FileName=$self->{fileName} gzerror=".$INPUT->gzerror());
}

sub tell {
  my $self = shift; my $INPUT = $self->{INPUT};

  my $bo = $INPUT->gztell();

  return $bo if $bo >= 0 or $bo < 0; 
  # $bo < 0 (-1) if fileSize > 2GigaBytes

  die("GZTELL ERROR DETECTED (rc=$bo). FileName=$self->{fileName} gzerror=".$INPUT->gzerror());
}

sub seek {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  my $bo = $INPUT->gzseek($_[0], $_[1]);
  
  return $bo if $bo >= 0;

  die("GZSEEK ERROR DETECTED (rc=$bo). FileName=$self->{fileName} gzerror=".$INPUT->gzerror());
}

sub eof {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  return $INPUT->gzeof();
}

sub lastModTime {
  my $self = shift; (stat($self->{fileName}))[9];
}

sub new {
  my ($className, $fileName) = @_; my $self = {};
  
  $self->{fileName} = $fileName;

  bless $self;
}

#------------------------------------------------------------
1;
