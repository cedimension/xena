package XReport::INPUT::RasterLine;

use Carp;

sub new {
  my ($className, $line, $atLine) = @_;

  my $self = [$line, $atLine];
  
  bless $self, $className;
}

sub AsciiValue {
  my $self = shift;
  return $self->[0];
}

sub Value {
  my $self = shift;
  return $self->[0];
}

sub atLine {
  my $self = shift;
  if ( defined($_[0]) ) {
    $self->[1] = $_[0];
  }
  return $self->[1];
}


#------------------------------------------------------------
1;
