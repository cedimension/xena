package XReport::INPUT::VippIPage;

our @ISA = ('XReport::INPUT::IPage');

use strict; use bytes;

sub NewPage {
  my $self = shift; $self->{atPage} += 1;
  
  my $page = { 
    INPUT  => $self,
    atPage => $self->{atPage},
    atLine => $self->{atLine},
    lineList => [],
    atOffset => $self->tell()
  };

  bless $page, 'XReport::INPUT::VippIPage';
}

sub totLines {
  my $self = shift; my $lineList = $self->{lineList};
  return scalar(@$lineList);
}

sub atPage {
  my $self = shift;
  return $self->{atPage};
}

sub atLine {
  my $self = shift;
  return $self->{atLine};
}

sub atOffset {
  my $self = shift;
  return $self->{atOffset};
}

sub appendLine {
  my $self = shift; my ($INPUT, $lineList) = @{$self}{qw(INPUT lineList)};
  push @$lineList, XReport::INPUT::AsciiLine->new($self, scalar(@$lineList)+1, @_); $INPUT->{atLine} += 1;
}

sub overwriteLastLine {
  my $self = shift; my $page = $self->{lineList};
  $page->[$#$page]->overwriteLine(@_);
}

sub lineList {
  my $self = shift;
  return $self->{lineList};
}

sub getLine {
  my $self = shift; 

  my ($lineOffsets, $lineCache) 
   = 
  @{$self}{qw(lineOffsets lineCache)}; 

  if ( !@$lineCache ) {
    my $INPUT = $self->{INPUT};
    $INPUT->getLines($lineOffsets, $lineCache , 88);
  }

  return if !scalar(@$lineCache);

  if ( wantarray ) {
    return (shift @$lineOffsets, shift @$lineCache);
  }
  else { 
    shift @$lineOffsets; return shift @$lineCache;
  } 
}

sub tell {
  return $_[0]->{lineOffset};
}

sub eof {
  return (!@{$_[0]->{lineCache}} and !exists($_[0]->{lastLine}) and $_[0]->{INPUT}->eof());
}

sub GetPage {
  my $self = shift;  return undef if $self->eof(); my $INPUT = $self->{INPUT};
  
  my ($lineOffset, $line) = ($self->{lineOffset}, '');
  
  my $page = $self->NewPage(); 

  if ( exists($self->{lastLine}) ) {
    $page->appendLine($self->{lastLine});
	delete $self->{lastLine};
  }

  while(1) {
   ($lineOffset, $line) = $self->getLine(); last if $line eq "";
	last if /^(?:C001 )|(\%\%EOF)/;
	chomp;
	$page->appendLine($line);
  }
  if  ( $line !~ /^\%\%EOF/ ) {
    chomp;
    $self->{lastLine} = $line;
  }
  else {
    my $trailer = $line;
	while(1) {
      $line = $self->getLine(); last if $line eq "";
	  $trailer .= $line;
	}
	$self->{TRAILER} = $trailer;
    $self->{lastLine} = ();
  }
  
  $self->{lineOffset} = $lineOffset;

  return $page;
}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  @{$self}{qw(lineOffset lineOffsets lineCache)} = (0, [], []);

  delete $self->{lastLine};

  @{$self}{qw(atPage atLine)} = (0, 0);
  
  return $INPUT->Open(@_);
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  @{$self}{qw(lineOffset lineOffsets lineCache)} = (0, [], []);

  @{$self}{qw(atPage atLine)} = (0, 0);

  return $INPUT->Close(@_);
}

sub new {
  my ($className, $self) = (shift, shift); 

  bless $self, $className;

  $self->Open(@_);

  return $self;
}

#------------------------------------------------------------
1;
