package XReport::INPUT::EbcdicIPage;

our @ISA = ('XReport::INPUT::IPage');

use strict; use bytes;

sub NewPage {
  my $self = shift; $self->{atPage} += 1;
  
  my $page = { 
    INPUT  => $self,
    overflowed => 0,
    atPage => $self->{atPage},
    atLine => $self->{atLine},
    lineList => [],
    atOffset => $self->tell()
  };

  bless $page, 'XReport::INPUT::EbcdicIPage';
}

sub overflowed {
  my $self = shift; return $self->{overflowed} if !scalar(@_); $self->{overflowed} = shift;
}

sub totLines {
  my $self = shift; my $lineList = $self->{lineList};
  return scalar(@$lineList);
}

sub atPage {
  my $self = shift;
  return $self->{atPage};
}

sub atLine {
  my $self = shift;
  return $self->{atLine};
}

sub atOffset {
  my $self = shift;
  return $self->{atOffset};
}

sub appendLine {
  my $self = shift; my ($INPUT, $lineList) = @{$self}{qw(INPUT lineList)};
  push @$lineList, XReport::INPUT::EbcdicLine->new($self, scalar(@$lineList)+1, @_); $INPUT->{atLine} += 1;
}

sub overwriteLastLine {
  my $self = shift; my $lineList = $self->{lineList};
  $lineList->[$#$lineList]->overwriteLine(@_);
}

sub lineList {
  my $self = shift;
  return $self->{lineList};
}

sub getLine {
  my $self = shift; 

  my ($lineOffsets, $lineCache) 
   = 
  @{$self}{qw(lineOffsets lineCache)}; 

  if ( !@$lineCache ) {
    my $INPUT = $self->{INPUT};
    $INPUT->getLines($lineOffsets, $lineCache , 88);
  }

  return if !scalar(@$lineCache);

  if ( wantarray ) {
    return (shift @$lineOffsets, shift @$lineCache);
  }
  else { 
    shift @$lineOffsets; return shift @$lineCache;
  } 
}

sub getAsciiLine {
  my $self = shift; my ($cc, $line) = $self->getLine();
  return $translator->toascii($line);
}

sub tell {
  return $_[0]->{lineOffset};
}

sub eof {
  return (!@{$_[0]->{lineCache}} and $_[0]->{lastccExec} eq "" and $_[0]->{INPUT}->eof());
}

sub GetPage {
  my $self = shift;  return undef if $self->eof(); my $INPUT = $self->{INPUT}; 
  warn "==========================> INPUT GetPage from ", __PACKAGE__, " for package ", ref($self), " INPUT: ", ref($INPUT), "\n" if $main::debug;
  
  my ($lineOffset, $lastcc, $lastLine, $lastccExec, $HasCc, $fcb) 
    = 
  @{$self}{qw(lineOffset lastcc lastLine lastccExec HasCc fcb)}; 

  my ($started, $cc, $line, $preExec, $printIt, $postExec, $curPos) = ();

  my $page = $self->NewPage();

  $started = 1 if $self->{lastcc} and $self->{lastcc} ne "\x5a";
  
  READLINES:
  while(1) {
    if ( $lastccExec eq "" ) {
      ($lineOffset, $line) = $self->getLine(); $cc = substr($line,0,1); last if ( $line eq "" );

#      print "=b4 SCS 34======> ", unpack("H8", $line ), " ", length($line), " $preExec $printIt $postExec<===========\n"  if $main::debug;
      if ($cc =~ /^\x34/) {
	if (substr($line, 1, 1) ne "\xC4") {
	  $self->LogIt( "INVALID SCS control character ".unpack("H*",$cc)." FOUND") if ( (++$self->{ccErrors}) < 10 );
	  $cc = "\xff"; # set cc to a default value (0x40 or 0x09)
	  #	  $line = "\x40".substr($line, 1);
	}
	else {
	  $cc .= substr($line, 1, 2);
	  my ($ppv) = unpack("C", substr($line, 2, 1));
	  #	  if ($ppv == 1) {
	  #	    ($preExec, $printIt, $postExec) = @{$ExecTable{"\x8b"}};
#	  }
	  #	  else {
	    my $currLines = $page->totLines();
	  last READLINES if $ppv == 1 and $ppv <= $currLines;
	  my $lines2skip = ($currLines > $ppv ? scalar(@$fcb) : 0 ) + $ppv - $currLines;
	$line = "\x40".substr($line,3);$cc = "\x40";	  
	  ($preExec, $printIt, $postExec) = ($lines2skip, 1, 0);
	  #	  $curPos = $page->totLines() ;
#	  for ($curPos = $page->totLines()+1; $curPos < $ppv; $curPos++ ) {
#	    last READLINES if ( $page->totLines() == scalar(@$fcb) );
#	    $page->appendLine("\x40");
#	  }
#	  next READLINES;
#	  }
	}
      }
      else {
      if (!$HasCc and $cc eq "\x0c") {
	    $line= "\x8b".substr($line,1); 
        $cc = "\x8b";
	  }
	  elsif (!$HasCc and $cc ne "\xf1") {
	    $line= "\x40".$line; $cc = "\x40";
	  }
	
	  if ( !exists($ExecTable{$cc}) ) {
		if ( (++$self->{ccErrors}) < 10 ) {
	    $self->LogIt( "INVALID control character ".unpack("H2",$cc)." FOUND in ".unpack("H40",$line))
		}
		$cc = "\xff"; # set cc to a default value (0x40 or 0x09)
	  }

	  ($preExec, $printIt, $postExec) = @{$ExecTable{$cc}};
	}
    }
	else {
	 ($preExec, $printIt, $postExec) = @$lastccExec;
	  $lastccExec = "";
	  $line = $lastLine; $cc = substr($line,0,1);
      $page->overflowed(1) if $cc ne "\xf1";
	}

	#do preProcessing
	if ( $preExec > 0 ) {  # space lines
	  while( $preExec>0 ) {
		if ( $page->totLines == scalar(@$fcb) ) { 
		  last READLINES;
		}
	    $page->appendLine("\x40");
		$preExec -= 1;
	  }
	}
	elsif ( $preExec < 0 ) {  # skip lines
	  if ( $preExec == -1 and $page->totLines() >= 1 ) {
	    $preExec = 0; last READLINES; 
	  }
	  $page->appendLine("\x40") if !$page->totLines();	
	  $curPos = $page->totLines()-1;
	  while( $fcb->[$curPos] != -$preExec ) {
		if ( $page->totLines == scalar(@$fcb) ) {
		  last READLINES;
		}
		$page->appendLine("\x40");	
		$curPos += 1;
	  }
	  $preExec = 0;
	}
	#test for filled page

	#push line
	if ( $printIt ) {
	  if ( $page->totLines() == 0 ) { 
	    #required (for example X89)	
	    $page->appendLine("\x40") 
	  }
	  $page->overwriteLastLine($line); #overwrite
	  $printIt = 0;
	  ($cc, $line) = ();
	}

	#do postProcessing
	if ( $postExec > 0 ) {  # space lines
	  while( $postExec>0 ) {
		if ( $page->totLines() == scalar(@$fcb) ) {
		  last READLINES;
		}
	    $page->appendLine("\x40");
		$postExec -= 1;
	  }
	}
	elsif ( $postExec < 0 ) {  # skip lines
	  if ( $postExec == -1 and $page->totLines() > 1 ) {
	    $postExec = 0; last READLINES; 
	  }
	  $page->appendLine("\x40") if !$page->totLines();	
	  $curPos = $page->totLines()-1;
	  while( $fcb->[$curPos] != -$postExec ) {
		if ( $page->totLines == scalar(@$fcb) ) {
		  last READLINES;
		}
		$page->appendLine("\x40");	
		$curPos += 1;
	  }
	  $postExec = 0;
	}
  }
  @{$self}{qw(lineOffset lastcc lastLine)} = ($lineOffset, $cc, $line);

  if (abs($preExec)+abs($printIt)+abs($postExec)) {
    $self->{lastccExec} 
	 = 
	[$preExec, $printIt, $postExec];
  }
  else {
    $self->{lastccExec} = ""; 
  }

  return $page;
}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};

  @{$self}{qw(lineOffset lastcc lastLine lastccExec ccErrors)} = (0, '', '', '', 0);

  my ($lpp, $fcb) = ($self->{lpp}, [@fcb2st60]);

  if ( $lpp > scalar(@$fcb) ) {
    for (1..($lpp-scalar(@$fcb))) {
      push @$fcb, 0
    }
  }
  elsif ($lpp < scalar(@$fcb)) {
    @$fcb = @{$fcb}[0..($lpp-1)]
  }

  @{$self}{qw(lineOffsets lineCache fcb atPage atLine)} = ([], [], $fcb, 0, 0);

  $INPUT->Open(@_);
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};

  @{$self}{qw(lineOffset lastcc lastLine lastccExec ccErrors)} = (0, '', '', '', 0);

  @{$self}{qw(lineOffsets lineCache fcb atPage atLine)} = ([], [], [], 0);

  return $INPUT->Close(@_);
}

sub new {
  my ($className, $self) = (shift, shift); my $INPUT = $self->{INPUT}; 

  bless $self, $className; 

  $self->Open(); my $lineOffset;
  
  if ($self->{HasCc}) {
    while( !$INPUT->eof() ) {
     ($lineOffset, $self->{lastLine}) = $INPUT->getLine();
      $self->{lastcc} = substr($self->{lastLine},0,1);
	
      my $cc = $self->{lastcc};
	  next if $cc eq "\x5a";

	  if ( exists($asaExecTable{$cc}) ) {
	    $self->{ExecTable} = \%asaExecTable;
	    $self->setJobReportValues('cc','asa');
	  }
	  else {
	    $self->{ExecTable} = \%machineExecTable;
	    $self->setJobReportValues('cc','machine');
	  }
	  last;
    }
    $self->Close(); $self->Open(@_);
  }
  else {
	$self->setJobReportValues('cc','asa');
  }

  return $self;
}

#------------------------------------------------------------
1;
