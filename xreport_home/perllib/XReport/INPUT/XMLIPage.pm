package XReport::INPUT::XMLIPage;

our @ISA = ('XReport::INPUT::IPage');

use strict; use bytes;

use XReport::Util qw($logger $logrRtn);

use constant EL_NAME => 0;
use constant EL_ATTRS => 1;
use constant EL_STRING => 2;

sub NewPage {
  my $self = shift; $self->{atPage} += 1;
  
  my $page = { 
    INPUT  => $self,
    atPage => $self->{atPage},
    overflowed => 0,   #### mpezzi overflowed was missing
    atLine => $self->{atLine},
    lineList => [],
    atOffset => $self->tell()
  };
  
  bless $page, 'XReport::INPUT::XMLIPage';
}

sub overflowed { return 0 }

sub totLines {
  my $self = shift; my $lineList = $self->{lineList};
  return scalar(@$lineList);
}

sub atPage {
  my $self = shift;
  return $self->{atPage};
}

sub atLine {
  my $self = shift;
  return $self->{atLine};
}

sub atOffset {
  my $self = shift;
  return $self->{atOffset};
}

sub appendLine {
  my $self = shift; my ($INPUT, $lineList) = @{$self}{qw(INPUT lineList)};
  push @$lineList, XReport::INPUT::AsciiLine->new($self, scalar(@$lineList)+1, @_); $INPUT->{atLine} += 1;
}

sub overwriteLastLine {
  my $self = shift; my $page = $self->{lineList};
  $page->[$#$page]->overwriteLine(@_);
}

sub lineList {
  my $self = shift;
  return $self->{lineList};
}

sub GetAttributes {
  my ($self, $elName) = (shift, shift); my $el = $self->{el}; my @r = ();

# print "QQQ $elName QQQ\n";

  return undef if $elName ne $el->[EL_NAME];

  my $elAttrs = $el->[EL_ATTRS]; 
  for (@_) {
	$elAttrs->{$_} =~ s/ +$//;
    push @r, $elAttrs->{$_};
  }

  return wantarray ? @r : $r[0];
}

sub setElement {
  my ($self, $el) = (shift, shift); my ($elName, $elAttrs, $elString) = @$el;

  $self->{el} = $el;

  print XMLOUT $el->[EL_STRING], "\n" if $self->atPage() <= 1000;

  $self->appendLine(" ".$el->[EL_STRING]);
}

sub getLine {
  my $self = shift; 

  my ($lineOffsets, $lineCache) 
   = 
  @{$self}{qw(lineOffsets lineCache)}; 

  if ( !@$lineCache ) {
    my $INPUT = $self->{INPUT};
    $INPUT->getLines($lineOffsets, $lineCache , 77);
  }

  return if !scalar(@$lineCache);

  if ( wantarray ) {
    return (shift @$lineOffsets, shift @$lineCache);
  }
  else {
    shift @$lineOffsets; return shift @$lineCache;
  } 
}

sub tell {
  return $_[0]->{lineOffset};
}

sub eofLines {
  return (!@{$_[0]->{lineCache}} and !exists($_[0]->{lastLine}) and $_[0]->{INPUT}->eof());
}

sub eof {
  #return ($_[0]->atPage >= 30000 or (!@{$_[0]->{elCache}} and $_[0]->eofLines()) );
  return (!@{$_[0]->{elCache}} and $_[0]->eofLines());
}

sub checkElString {
  my $self = shift;

  if (length($self->{elString}) > 100000) {
    print $self->{elString};
	die "MAXLEN DETECTED";
  }
}

sub start_handler
{
  my ($self, $parser, $elName) = (shift, shift, shift);

  if ( (my $depth = $parser->depth()) <= 1 ) {
    if ( $depth == 0 ) {
	  my $elCache = $self->{elCache};
	  push @$elCache, [
	    $elName, {@_} , $parser->original_string() 
	  ];
	}
    @{$self}{qw(elAttrs elString)} = ({@_}, '');
  }
  
  $self->{elString} .= $parser->original_string();
  
  $self->checkElString();
}
 
sub end_handler
{
  my ($self, $parser, $elName) = @_; my $elCache = $self->{elCache};
  
  if ( (my $depth = $parser->depth()) <= 1 ) {
    if ( $depth == 1 ) {
      push @$elCache, [
	    $elName, $self->{elAttrs} , $self->{elString} . $parser->original_string() 
	  ]; 
	}
	@{$self}{qw(elAttrs elString)} = ('', '');
  }
  else {
	$self->{elString} .= $parser->original_string();  
  
    $self->checkElString();
  }
}

sub default_handler { 
  my ($self, $parser) = (shift, shift); 
 
  $self->{elString} .= join('', @_);
  
  $self->checkElString();
}

sub fill_elCache {
  my $self = shift; my ($parser, $elCache) = @{$self}{qw(parser elCache)};

  while(1) {
    my ($lineOffset, $line) = $self->getLine(); last if $line eq ""; 

	$parser->parse_more($line);

	last if ( scalar(@$elCache) > 15 );
  }

  return scalar(@$elCache);
}

sub GetPage {
  my $self = shift;  return undef if $self->eof(); my $INPUT = $self->{INPUT};
  
  my ($elCache, $page)  = ($self->{elCache}, $self->NewPage());
  
  $self->fill_elCache() if !@$elCache; 
  
  my $el = shift @$elCache || return undef; $page->setElement($el);

  return $page;
}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  @{$self}{qw(lineOffset lineOffsets lineCache elCache)} = (0, [], [], []);

  require XML::Parser::Expat;

  my $parser = XML::Parser::ExpatNB->new( 
    ProtocolEncoding => 'US-ASCII',
	NoExpand => 1
  );
  
  $parser->setHandlers(
    'Start'    => sub {$self->start_handler(@_)},
    'End'      => sub {$self->end_handler(@_)},
    'Default'  => sub {$self->default_handler(@_)}
  );

  @{$self}{qw(parser elString elAttrs atPage atLine)} = ($parser, '', undef, 0, 0);

  open(XMLOUT, ">c:/temp/zzxml.xml");

  my $rc = $INPUT->Open(@_);
  
  return $rc; 
}

sub Close {
  my $self = shift; my ($INPUT, $parser) = @{$self}{qw(INPUT parser)};
  
  @{$self}{qw(lineOffset lineOffsets lineCache elCache)} = (0, [], [], []);

  eval { 
    $parser->parse_done(); 
  };

  delete $self->{parser}; 
  
  @{$self}{qw(elString elAttrs atPage atLine)} = ('', undef, 0, 0);

  close(XMLOUT);

  return $INPUT->Close(@_);
}

sub new {
  my ($className, $self) = (shift, shift); 

  bless $self, $className;

  $self->Open(@_);

  return $self;
}

#------------------------------------------------------------
1;
