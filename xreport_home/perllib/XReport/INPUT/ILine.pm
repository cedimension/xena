package XReport::INPUT::ILine;

use strict; use bytes;

use constant XF_LPR => 1;
use constant XF_PSF => 2;
use constant XF_FTPB => 3;
use constant XF_FTPC => 4;
use constant XF_ASPSF => 5;
use constant XF_AFPSTREAM => 6;

my $cs_asciidelim = quotemeta("\r\n\f\x0c");

my $lx_ascii = qr/([$cs_asciidelim]|[^$cs_asciidelim]+)/o;

sub getAsciiILine {
  my $self = shift; my ($INPUT, $byteCache) = @{$self}{qw(INPUT byteCache)};
  
  if ($INPUT->eof() and length($byteCache) == 0) {
    return undef;
  }
  my ($at, $at_max) = (0, length($byteCache));

  my ($c, $outrec) = ("", " ", 0); 

  my $lastc = '';

  while( 1 ) {
    if ( ($at_max-$at) < 2048 && !$INPUT->eof() ) {
	  $byteCache = substr($byteCache, $at);
	  $INPUT->read($_, 2048);
	  ### todo: translate to ascii if required
      $byteCache .= $_;
	  $at=0; $at_max = length($byteCache);
    }
    last if $at >= $at_max; 
	
    ($c) = substr($byteCache, $at, 64) =~ /^($lx_ascii)/o; $at += length($c);
	
    if ( $c eq "\r" ) {
	  if ( $outrec =~ /^.[ \x00]*$/ ) {
		substr($outrec,1) = "";
	  }
	}
	elsif ( $c eq "\n" ) {
	  last;
	}
	elsif ( $c eq "\f" or $c eq "\x0c") {
	  if ( length($outrec) > 1 ) {
		$at--; last;
	  }
	  else {
		$outrec = "1";
	  }
	}
	else {
      substr($outrec, 1) = "" if $lastc eq "\r";
      $outrec .= $c; 
	}

    $lastc = $c;
  }
  $self->{byteCache} = substr($byteCache, $at);

  $outrec =~ tr/\x00/ /;

  return $outrec;
}

my $scsdelim = quotemeta("\x34\x15\x0D\x0C");
my $lx_scs = qr/([$scsdelim]|[^$scsdelim]+)/o;

sub getSCSILine {
  my $self = shift; my ($INPUT, $byteCache) = @{$self}{qw(INPUT byteCache)};
  
  if ($INPUT->eof() and length($byteCache) == 0) {
    return undef;
  }

  my ($at, $at_max) = (0, length($byteCache));

  my ($c, $outrec) = ("", "\x40");

  my $lastc = '';
  my $hpos = 0;
  while( 1 ) {
    if ( ($at_max-$at) < 1024 && !$INPUT->eof() ) {
	  $byteCache = substr($byteCache, $at) if $at > 0;
	  $INPUT->read($_, 2048);
	  $byteCache .= $_;
	  $at=0; $at_max = length($byteCache);
    }
    last if $at >= $at_max; 
	
    ($c) = substr($byteCache, $at, 1024) =~ /^($lx_scs)/o; 
    $at += length($c);
    if ($c =~ /[$scsdelim]/) {
      if ( $c eq "\x34" ) {
	my ($ppc, $ppv) = unpack("CC", substr($byteCache,$at,2));
#	print "Start to parse escape code at $at ($ppc , $ppv) ", unpack("H*", substr($byteCache,$at,2)), "\n";
	$at += 2;
	if ($ppc == 0xC0 ) {     # Absolute Hor move
# mpezzi == here we truncate/expand outrec to the specified position,
# that means that we do not support overprint - may be in the future
# For as400 a good idea would be to support PCL format, since AS400 can 
# convert to this format.
	  my $currl = length($outrec);
#	  print "==Horz Move=====> $at $currl ", unpack("H*", substr($byteCache, $at-3, 8)), " $ppc  $ppv <===========\n";
	  $outrec .= "\x40" x ($ppv - $currl) if $currl < $ppv;
	  $outrec = substr($outrec, 0, $ppv) if $currl > $ppv;
#	  print "==============> ", length($outrec), " ", unpack("H*", $outrec), " <===========\n";
#	  $outrec .= "\x40" x ($ppv - 1);
#	  print "OutRec expanded of $ppv blank bytes\n";
        } 
	elsif ($ppc == 0xC4 ) { # Absolute Vert Move
#	  print "==Vert Move=====> $at ", unpack("H*", substr($byteCache, $at-3, 8)), " $ppc  $ppv <===========\n";
	  if (length($outrec) > 1) {
#	    $outrec = "\x8b\x40" if ((my $prevppv ) = unpack("C", $outrec =~ /^\x34\xC4(.)$/) && $prevppv > $ppv );
	    $at -= 3;
	    last;
	  }
	  elsif ($outrec eq "\xF1") {
	    $at -= 3;
	    $outrec = "\xF1";
	  last;
	} 
	else {
	    $outrec = "\x34\xC4".pack("C", $ppv);
	    #	  $outrec = ( $ppv eq "\x03" ? "\x60" : 
	    #	              $ppv eq "\x02" ? "\xF0" :
	    #	              "\x40" );  	
	    
	  }
	} 
	else {
#            print "==============> ", unpack("H*", substr($byteCache, $at-3, 8)), " <===========\n";
#            die;
	# some warning for a Presentation position command ignored  
	  #	  die "Start to parse escape code ", unpack("H*", substr($byteCache,$at,2)), "\n";
	}
      } 
      elsif ( $c eq "\x0D" ) {
#	print "=CR =========> $at ", length($outrec), " ", unpack("H*", $c.$outrec), " <===========\n";
	(my $ncc) = substr($byteCache, $at, 2) =~ /(\x00?\x15)/;
        if ($ncc =~ /\x00?\x15/) {
	  $at += length($ncc);
	  last;
	}
	$outrec = substr($outrec, 0, 1);
	#	if ( $outrec =~ /^.[\x40\x00]*$/ ) {
	#	  substr($outrec,1) = "";
	#	}
      } 
      elsif ( $c eq "\x15" ) {
#	print "=Line Feed======> $at ", length($outrec), " ", unpack("H40", $c.$outrec), " <===========\n";
	(my $ncc) = substr($byteCache, $at, 2) =~ /(\x00?\x0D)/;
        if ($ncc =~ /\x00?\x0D/) {
	  $at += length($ncc);
	}
	last;
      } 
      elsif ( $c eq "\x0C") { # and $at < $at_max and !$INPUT->eof()
#	print "=Form Feed======> $at ", length($outrec), " ", unpack("H*", $c.$outrec), " <===========\n";
	if ( length($outrec) > 1 ) {
	  $at--; 
	  last;
	} else {
	  $outrec = "\xF1";
	}
      }
    } 
    else {
      #      print "==============> ", unpack("H*", $lastc), " <<>> ", unpack("H*", $c), " <===========\n";
      #      die;
      #      substr($outrec, 1) = "" if $lastc  "\x0D";
      $c =~ s/^\x00+//;
      
      $outrec .= $c;
    }
    
    $lastc = $c;
  }

  $self->{byteCache} = substr($byteCache, $at);

  $outrec =~ tr/\x00/\x40/; 
#  print "OREC ==============> ", unpack("H20", $outrec), " <===========\n";
  return $outrec;
}


sub getModebILine {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  my ($rc, $d3, $b1, $d2, $outrec, $cc, $data) = ();

  $rc = $INPUT->read($d3,3);
  ($b1, $d2) = unpack("an", $d3);
  $rc = $INPUT->read($outrec, $d2);

  if ( $b1 eq "\x10" ) {
    return getModebLine();
  }

  return $outrec;
}

sub getModecILine {
  my $self = shift; my ($INPUT, $byteCache) = @{$self}{qw(INPUT byteCache)};
  
  my ($inrec, $incntrl, $outrec, $b1, $rc); $incntrl = "";

  my ($at, $at_max) = (0, length($byteCache)); 

  
  while( 1 ) {
    if ( ($at_max-$at) < 2048 && !$INPUT->eof() ) {
	  $byteCache = substr($byteCache, $at);
	  $INPUT->read($_, 2048);
      $byteCache .= $_;
	  $at=0; $at_max = length($byteCache);
    }
    last if $at >= $at_max;
	
    $b1 = substr($byteCache,$at++,1); $b1 = unpack("C", $b1); 

    if ( $b1 == 0 ) {
	  $incntrl = substr($byteCache,$at++,1);
	  if ( $incntrl ne "\x80" and $incntrl ne "\x40" ) {
	    $inrec = substr($byteCache,$at,10); $at += 10;
	    die("UserError: INVALID MODEC $at CONTROL BYTE ".unpack("H*", $incntrl.$inrec));
	  }
      next;
    }
    elsif ( ($b1 & 0x80) == 0 ) {
	  $inrec = substr($byteCache,$at,$b1); $at += $b1;
  	  $outrec .= $inrec;
    }
    elsif ( ($b1 & 0xc0) == 0x80 ) {
	  $inrec = substr($byteCache,$at++,1);
      $rc = $b1 & 0x3f;
  	  $outrec .= $inrec x $rc;
    }
    elsif ( ($b1 & 0xc0) == 0xc0 ) {
      $rc = $b1 & 0x3f;
  	  $outrec .= "\x00" x $rc; #FTC si comporta in modo diverso a seconda se chiamato in modo EBCDIC o BINARY
  	  #$outrec .= "\x40" x $rc;
    }
    else {
	  $inrec = substr($byteCache,$at,10); $at += 10;
  	  die("UserError: INVALID MODEC MASK ".unpack("H*", $inrec));
    }
    last if $incntrl ne "";
  }
  $self->{byteCache} = substr($byteCache, $at);
  
  return $outrec;
}

sub getPsfILine {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  my ($rc, $lr, $outrec) = ();
  
  $rc = $INPUT->read($lr,2); 
  $lr = unpack("n", $lr);
  $rc = $INPUT->read($outrec, $lr);

  return $outrec;
}

sub getAfpILine {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  my ($rc, $lr, $outrec) = ();
  
  $rc = $INPUT->read($lr,3);
  $rc = $INPUT->read(
	$outrec, unpack("xn", $lr)-2
  );

  return $lr.$outrec;
}

sub getASPsfILine {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  my ($rc, $lr, $outrec) = ();
  
  $rc = $INPUT->read($lr,8); 
  $lr = unpack("x2n", $lr);
  $rc = $INPUT->read($outrec, $lr);

  return $outrec;
}

sub getAFPStreamILine {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  my ($rc, $c5a, $outrec) = ();
  
  $rc = $INPUT->read($c5a,3);
###mpezzi  return undef if $c5a eq '';
### consider also read errors
  return undef unless ($c5a and (length($c5a) == 3));
  
###mpezzi  die("Input Stream Error: INVALID AFP Record Header ".unpack("H*", $c5a)) 
#   if 
#  substr($c5a,0,1) ne "\x5a";
#  
#  $rc = $INPUT->read($outrec, unpack("xn", $c5a) - 2);
###
  my ($cc, $lr) = unpack("an", $c5a);
  # print ">>", unpack("H*", $c5a), "<<\n";
  die("Input Stream Error: INVALID AFP Record Header ".unpack("H*", $c5a)) if $cc ne "\x5a";
  $lr -= 2;
  $rc = $INPUT->read($outrec, $lr);

  return $c5a . $outrec;
}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};
  
  $self->{byteCache} = "";

  $INPUT->Open();
}

sub getLine {
  return &{$_[0]->{getLine}}($_[0]);
}

sub getLines {
  my ($self, $lineOffsets, $lineCache, $maxLines) = @_; my ($offset, $line);
  
  while($maxLines>0) {
    $offset = $self->tell(); $line = &{$self->{getLine}}($self); 
	if ( $line eq "" ) {
	  last;
	}
	push @$lineOffsets, $offset; push @$lineCache, $line; $maxLines -= 1;
  }
}

sub tell {
  return $_[0]->{INPUT}->tell() - length($_[0]->{byteCache});
}

sub eof {
  if ( !$_[0]->{INPUT}->eof() ) {
    return 0;
  }
  return( length($_[0]->{byteCache}) == 0 );
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};

  $self->{byteCache} = "";

  $INPUT->Close();
}

sub new {
  my ($className, $INPUT, $XferMode) = @_; my $self = {}; my $getLine;

  $self->{byteCache} = "";
  
  if ( $XferMode == XF_LPR ) {
    $getLine = \&getAsciiILine;
  }
  elsif ( $XferMode == XF_PSF ) {
    $getLine = \&getPsfILine;
  }
  elsif ( $XferMode == XF_FTPB ) {
    $getLine = \&getModebILine;
  }
  elsif ( $XferMode == XF_FTPC ) {
    $getLine = \&getModecILine;
  }
  elsif ( $XferMode == XF_ASPSF ) {
    $getLine = \&getSCSILine;
#    $getLine = \&getASPsfILine;
  }
  elsif ( $XferMode == XF_AFPSTREAM ) {
    $getLine = \&getAFPStreamILine;
  }
  else {
    die("UserError: Invalid XferTransferMode: $XferMode\.");
  }

  @{$self}{qw(INPUT getLine XferMode)} = ($INPUT, $getLine, $XferMode);
  
  bless $self, $className;
}

#------------------------------------------------------------
1;
