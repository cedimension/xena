package XReport::INPUT::PostscriptIPage;

sub new {
  my ($className, $self) = (shift, shift); my $INPUT = $self->{INPUT};

  require XReport::Postscript::IOStream;

  return XReport::Postscript::IOStream->new($self);
}

#------------------------------------------------------------
1;
