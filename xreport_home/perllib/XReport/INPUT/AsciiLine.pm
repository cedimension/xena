package XReport::INPUT::AsciiLine;

use strict; use bytes;

sub log {
  my $self = shift; require XReport::Util; 

  &{$XReport::Util::logrRtn}(
   "Begin log of Line ", $self->AsciiValue() ,"End log of Line."
  );
}

sub cleanLine(\$) {
  my $ref = shift; my ($cc, $line) = (substr($$ref,0,1), $$ref);
  if ( length($line) > 2 ) {
    substr($line,2) =~ s/ +$//;
    $$ref = $line;
  }
}

sub overwriteLine {
  my ($self, $line) = (shift, shift); return if $line =~ /^. *$/; my $eraser = $line; 

  $eraser =~ s/[^ ]/\x00/g; $eraser =~ s/[^\x00]/\xff/g; $self->[0] &= $eraser; 
  $line =~ s/ /\x00/g; $self->[0] |= $line; $self->[0] =~ s/\x00/ /g; 

  $self->[1] = undef;
}

sub AsciiValue {
  my $self = shift;
  
  if ( defined($_[0]) ) {
    $self->[0] = $_[0];
  }
  return $self->[0];
}

sub Value {
  my $self = shift;
  
  if ( defined($_[0]) ) {
    $self->[0] = $_[0];
  }
  return $self->[0];
}

sub atLine {
  my ($self, $beg) = (shift, shift || 1); 
  
  if ( defined($_[3]) ) {
    $self->[3] = $_[0];
  }
  return $self->[3] + ($beg == 1 ? 0 : $self->[2]);
}

sub new {
  my ($className, $page, $atLine, $line) = @_; cleanLine($line);
  
  my $self = [$line, "", $page->atLine(), $atLine];
  
  bless $self, $className;
}

#------------------------------------------------------------
1;
