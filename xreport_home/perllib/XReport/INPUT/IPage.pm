package XReport::INPUT::IPage; ### superclass ###

our @ISA = ('XReport::INPUT');

use strict; use bytes;

sub log {
  my $page = shift; my $lineList = $page->lineList(); require XReport::Util; 

  &{$XReport::Util::logrRtn}(
   "Begin log of Page ".$page->atPage().".", (map {$_->AsciiValue} @$lineList), "End log of Page."
  );
}

sub GetPageRect {
  my ($page, $fr, $fc, $tr, $tc) = @_; my $t = ""; my $j;

  my $lineList = $page->lineList(); 

  my $lLen = scalar(@$lineList);

  for ($fr, $fc, $tr, $tc) {
    $_-- if $_ > 0;
  } 

  $fr = -$lLen if $fr < 0 && abs($fr) > $lLen; 
  $tr = $#$lineList if $tr > $#$lineList;

  return '' if $fr > $tr;

  for ( $j=$fr;$j<=$tr;$j++ ) {
	last if $j>$#$lineList;
    $t .= substr($lineList->[$j]->AsciiValue(),$fc,$tc)."\n";
  }

  return $t;
}

sub GetPageFields {
  my $page = shift; my $lineList = $page->lineList(); my @fields;
  
  while(@_) {
    my ($l, $c, $ll, $field) = (shift, shift, shift, undef); $l--; $c--; 
	if ($l = $lineList->[$l] and length($l=$l->AsciiValue()) > $c) {
	  $field = substr( $l, $c, $ll);
	}
	push @fields, $field;
  }

  return @fields;
}

sub GetLineRect {
  my ($page, $line, $fr, $fc, $tr, $tc) = @_; my $t = ""; my $j;

  my $atLine = $line->atLine();
  my $lineList = $page->lineList($page);

  $fr+=$atLine-2; $fc--; $tr+=$atLine-2; $tc--;

  for ( $j=$fr;$j<=$tr;$j++ ) {
	last if $j>$#$lineList;
    $t .= substr($lineList->[$j]->AsciiValue(),$fc,$tc)."\n";
  }

  return $t;
}

sub GetLineRectList {
}

sub setLineFilter {
  my ($self, $FilterClass) = (shift, shift); my $INPUT = $self->{INPUT};
  if ( $FilterClass ) {
    $self->{INPUT} = $FilterClass->new($INPUT, @_);
  }
  else {
	$self->{INPUT} = $INPUT->{INPUT};
  }
}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};
  return $INPUT->Open(@_);
}

sub getLine {
  return $_[0]->{INPUT}->getLine();
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};
  return $INPUT->Close(@_);
}

sub SetElabOptions {
}

sub new {
  my ($className, $self, $INPUT) = @_; 
  
  $self->{INPUT} = $INPUT; 

  bless $self, $className;

  return $self;
}

#------------------------------------------------------------
1;
