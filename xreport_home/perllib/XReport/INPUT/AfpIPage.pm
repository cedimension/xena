package XReport::INPUT::AfpIPage;

our @ISA = ('XReport::INPUT::EbcdicIPage');

use strict; use bytes;
use Carp;
use Symbol;
use XReport::Util;

my $lx_idm = qr/^\x5a..\xd3\xab\xca/;
my $lx_nop = qr/^\x5a..\xd3\xee\xee/;

my $TLE = undef;
my $NOP = undef; ### mpezzi nop handling

sub NewPage {
  my $self = shift; $self->{atPage} += 1;
  
  my $page = { 
    INPUT  => $self,
    atPage => $self->{atPage},
    atLine => $self->{atLine},
    lineList => [],
    atOffset => $self->tell()
  };

  bless $page, 'XReport::INPUT::AfpIPage';
}

sub totLines {
  my $self = shift; my $lineList = $self->{lineList};
  return scalar(@$lineList);
}

sub atPage {
  my $self = shift;
  return $self->{atPage};
}

sub atLine {
  my $self = shift;
  return $self->{atLine};
}

sub atOffset {
  my $self = shift;
  return $self->{atOffset};
}

sub appendLine {
  my $self = shift; my ($INPUT, $lineList) = @{$self}{qw(INPUT lineList)};
  push @$lineList, XReport::INPUT::EbcdicLine->new($self, scalar(@$lineList)+1, @_); $INPUT->{atLine} += 1;
}

sub overwriteLastLine {
  my $self = shift; my $lineList = $self->{lineList};
  $lineList->[$#$lineList]->overwriteLine(@_);
}

sub lineList {
  my $self = shift;
  return $self->{lineList};
}

sub getLine {
  my $self = shift; 

  my ($lineOffsets, $lineCache) 
   = 
  @{$self}{qw(lineOffsets lineCache)}; 

  if ( !@$lineCache ) {
    my $INPUT = $self->{INPUT};
    $INPUT->getLines($lineOffsets, $lineCache , 88);
  }

  return if !scalar(@$lineCache);

  if ( wantarray ) {
    return (shift @$lineOffsets, shift @$lineCache);
  }
  else { 
    shift @$lineOffsets; return shift @$lineCache;
  } 
}

sub tell {
  return $_[0]->{lineOffset};
}

sub eof {
  return (!@{$_[0]->{lineCache}} and $_[0]->{lastcc} =~ /^\x8b?$/ and $_[0]->{INPUT}->eof());
}

sub GetAttributes {
  my ($self, $elName) = (shift, shift); my @r = ();

### mpezzi nop handling
##  if ($elName eq 'NOP' and defined($NOP) and join('', @$NOP) ne '') {
##    my $re = shift;
##    print "NOP INPUT:", ($NOP ? join(":::", @$NOP) : "__undefined__"), "\n"; 
##    return wantarray ? $NOP : join("\x20", @$NOP) unless $re;
##    @r =  join("\x20", @$NOP) =~ /$re/mso;
##	print "GetAttributes NOP ==> ", join(':::', @r), "\n";
##	return wantarray ? @r : $r[0];
##  }
###

  return unless $elName eq "TLE" and defined($TLE) and keys(%$TLE);

  for (@_) {
    return () if !exists($TLE->{$_});
    push @r, $TLE->{$_};
  }

  return wantarray ? @r : $r[0];
}

sub check_nop {
  my ($self, $line) = @_; $line = $translator->toascii(substr($line,9));
  warn "check_nop from ", __PACKAGE__, " for package ", ref($self), " line: $line \n"  if $main::debug;
  return if $line !~ /^XRTLE\t/;
  $line =~ s/\s+$//; my %TLE = split("\t", substr($line,6)); 
  @{$TLE}{keys(%TLE)} = map {
	$_ =~ s/\s+$//; $_
  } 
  values(%TLE);
}

#sub handle5A {
#  my ($self, $line) = (shift, shift);
#  if ($line =~ /$lx_idm/o) {
#    my $dm = $translator->toascii(substr($line,9,8));
#    $c1 = $self->{'c1'} = $self->{'dm_c1'}->{$dm};
#  }
#  elsif ($line =~ /$lx_nop/o) {
#    $self->check_nop($line);
#  }
#  return '';
#}

sub cleanLineList {
  my ($self, $page) = @_; my $lineList = $page->lineList(); my $ll = scalar(@$lineList)-1; 
  
  my @l5a = (0,0,1); my $toclean = 0;

  for (0..$ll) {
    my $line = $lineList->[$ll-$_]->Value(); my $cc = substr($line,0,1); my $todel = 0;
    if ($cc eq "\x5a") {
	  my $sf = substr($line,3,3);
      my $ix = $sf eq "\xd3\xab\xcc" ? 0 : $sf eq "\xd3\xab\xca" ? 1 : 2 ;
      $todel = $l5a[$ix] == 1 ? 1 : 0; $l5a[$ix] = 1; 
    }
    else {
      $todel = 1;
    }
    do {delete $lineList->[$ll-$_]; $toclean += 1} if $todel;
  }
  
  @$lineList = grep {defined($_)} @$lineList if $toclean;
}

sub GetPage_CLEAN {
  my $self = shift;  return undef if $self->eof(); my $INPUT = $self->{INPUT}; $TLE = undef; 

  my (
   $lineOffset, 
   $cc, $line, $c1, $Formatted
  ) = 
  @{$self}{qw(lineOffset lastcc lastLine c1 Formatted)}; 
  
  my $page = $self->NewPage(); my $started = 0;
  
  if ( $cc ne "" and $cc ne "\x8b" ) {
	if ($line =~ /$lx_idm/o) {
	  my $dm = $translator->toascii(substr($line,9,8));
      $c1 = $self->{'c1'} = $self->{'dm_c1'}->{$dm} || "\xf1"; 
	}
	elsif ($line =~ /$lx_nop/o) {
	  $self->check_nop($line);
	}
    $page->appendLine($line); 
  }

  $started = 1 if $cc ne '' and $cc ne "\x5a" and ($cc ne $c1 or length($line) > 2);
  
  while(1) {
    ($lineOffset, $line) = $self->getLine(); last if $line eq ""; 
    
    $cc = substr($line,0,1); substr($line,2) =~ s/\x40+$//; 

	last if ( $started and ($cc eq $c1 and $started >= 1 or $cc eq "\x8b") ); 

	if ( $started and $cc eq "\x5a" ) {
	  my $sf = substr($line,3,3);
	  last if $sf eq "\xd3\xab\xcc" or $sf eq "\xd3\xab\xca";
	}

	if ($cc ne "\x5a" and ($cc ne $c1 or length($line) > 2)) {
      $self->cleanLineList($page) if !$started; 
      $started += 1; 
	}
	elsif ($line =~ /^$lx_idm/o) {
	  my $dm = $translator->toascii(substr($line,9,8));
      $c1 = $self->{'c1'} = $self->{'dm_c1'}->{$dm} || "\xf1"; 
	}
	elsif ($line =~ /$lx_nop/o) {
	  $self->check_nop($line);
	}

	$page->appendLine($line); ($cc, $line) = ();
  }
  
  @{$self}{qw(lineOffset lastcc lastLine)} = ($lineOffset, $cc, $line);

  return $page;
}


sub GetPage {
  my $self = shift;  return undef if $self->eof(); my $INPUT = $self->{INPUT}; $TLE = undef; 
  $NOP = undef; ### mpezzi nop handling 
  warn "==========================> INPUT GetPage from ", __PACKAGE__, " for package ", ref($self), " INPUT: ", ref($INPUT), "\n"  if $main::debug;
  my ($lineOffset, $cc, $line, $c1, $Formatted ) = 
  @{$self}{qw(lineOffset lastcc lastLine c1 Formatted)};
  
  my $page = $self->NewPage(); my $started = 0;
  
  if ( $cc ne "" and $cc ne "\x8b" ) {
	if ($line =~ /$lx_idm/o) {
      (my $dm = $translator->toascii(substr($line,9,8))) =~ s/\W//g;
      $c1 = $self->{'c1'} = $self->{'dm_c1'}->{$dm};
	}
	elsif ($line =~ /$lx_nop/o) {
	  $self->check_nop($line);
	}
    $page->appendLine($line); # if $cc ne "\x5a"; 
  }

  $started = 1 if $cc ne '' and $cc ne "\x5a";
  
  while(1) {

    ($lineOffset, $line) = $self->getLine(); $cc = substr($line,0,1); last unless $line; 

	#do { $line = ($cc="\xf1").$line; last; } if !$Formatted;
	#todo build skip page frompagedef

    #    last if ( $started and ($cc eq $c1 and $started>=1 or $cc eq $self->{FFcode}) );
    #    last if ( $started and ($cc eq $c1 and $started>=1 or $cc eq "\x8b") );
    last if ( $started and ($cc eq $c1 or $cc eq "\x8b") );
    last if ( $started and $cc eq "\x5a" and $line =~ /^\x5a..\xd3\xab[\xca\xcc]/ );
    
    #    if ( $started and $cc eq "\x5a" ) {
    #      my $sf = substr($line,3,3);
    #      last if $sf eq "\xd3\xab\xcc" or $sf eq "\xd3\xab\xca";
    #    }

	if ($cc ne "\x5a") {
	  $started += 1 
	}
	elsif ($line =~ /^$lx_idm/o) {
      (my $dm = $translator->toascii(substr($line,9,8))) =~ s/\W//g;
      $c1 = $self->{'c1'} = $self->{'dm_c1'}->{$dm};
	}
	elsif ($line =~ /$lx_nop/o) {
	  $self->check_nop($line);
	}

	$page->appendLine($line); ($cc, $line) = ();
  }
  
  @{$self}{qw(lineOffset lastcc lastLine)} = ($lineOffset, $cc, $line);

  return $page;
}


sub Open {
  my $self= shift; $self->SUPER::Open(@_);
  $self->{'c1'} = $self->{'dm_c1'}->{$self->{'dm_1'}} || "\xf1"; 
}

sub Close {
  my $self= shift; $self->SUPER::Close(@_);
  $self->{'c1'} = $self->{'dm_c1'}->{$self->{'dm_1'}} || "\xf1"; 
}

sub new {
  my ($className, $self) = (shift, shift); my $INPUT = $self->{INPUT}; 
  
  bless $self, $className; $self->setJobReportValues('isAfp', 1);
  $self->setJobReportValues('isAfpCSF',0); ##   #mpezzi csf files could omit endof document
  
  $self->setJobReportValues(
	'LastFormDef' => '', 'LastMediumMap' => '', 'LastDataMap' => '',
  );

  $self->Open(); my $lineOffset;
  
  while( !$INPUT->eof() ) {
   ($lineOffset, $self->{lastLine}) = $self->getLine();
    $self->{lastcc} = substr($self->{lastLine},0,1);
	
    my $cc = $self->{lastcc};
	
   if ( $self->{lastLine} =~ /^\x5a..(...)...(.{0,11})/ ) {
     my ($fc5a, $sig5a) = ($1, $2);
# \x53\x74\x72\x65\x61\x6D\x53\x65\x72\x76\x65 StreamServe signature
# \xc3\xe2\xc6                                 CSF Signature
     next unless $fc5a =~ /^\xd3(?:\xee\xee|\xa8[\xa7\xa8\xc6])$/;
     $self->setJobReportValues('isAfpCSF',1) if $fc5a =~ /^\xd3\xee\xee$/ && $sig5a =~ /^\xc3\xe2\xc6.*$/; # CSF Ebcdic
     next if $fc5a =~ /^\xd3\xee\xee$/ && $sig5a !~ /^\x53\x74\x72\x65\x61\x6D\x53\x65\x72\x76\x65.*$/; # StreamServe ascii

      $self->setJobReportValues('isFullAfp',1);
      if ( $self->getJobReportValues('FormDef') eq '') {
        $self->setJobReportValues('FormDef', 'NULL');
      }
	  $INPUT->Close(); 

      return XReport::AFP::IOStream->new($self);
	}

	if ( exists($asaExecTable{$cc}) ) {
	  $self->{ExecTable} = \%asaExecTable;
	  $self->setJobReportValues('cc','asa');
     $self->{FFcode} = "\xf1";
	}
	else {
	  $self->{ExecTable} = \%machineExecTable;
	  $self->setJobReportValues('cc','machine');
     $self->{FFcode} = "\x8b";
	}

	last;
  }
  $self->Close(); my $jr = $self->{'jr'}; 
  require XReport::AFP::EXTRACT;

  my ($XferStartTime, $PageDef) = $jr->getValues(qw(XferStartTime PageDef)); return $self if $PageDef eq '';

  XReport::AFP::EXTRACT::ToLocalResources( 
	my $LocalResources = $jr->getFileName('LOCALRES'), 
	$XferStartTime, $PageDef	
  );

  my $INPUT = gensym(); 
  
  my $FileName = "$LocalResources/$PageDef\.ps";

  $FileName = getConfValues("afpdir") . "/pdeflib/$PageDef\.ps" if !-e $FileName;

  open($INPUT, "<$FileName")
    or
  die("INPUT OPEN ERROR FOR FILE \"$FileName\" $!");

  my ($dm_1, $dm, %dm_c1);

  while(<$INPUT>) {
    if ( $_ =~ /\/FirstDataMap +\((\w+)\)/ ) {
	  $dm_1 = $1; last;
    }
  }
  while(<$INPUT>) {
    if ( $_ =~ /\/DataMap\.(\w+)/ ) {
	  $dm = $1;
    }
    elsif ($_ =~ /^ *\[ *1 +(\d+) /) {
	  $dm_c1{$dm} = $translator->toebcdic($1);
    }
  }
  close($INPUT); 

  @{$self}{qw(dm_1 dm_c1)} = ($dm_1, \%dm_c1); 
  my $c1 = $self->{'c1'} = $self->{'dm_c1'}->{$self->{'dm_1'}};
  
  return $self;
}

#------------------------------------------------------------
1;
