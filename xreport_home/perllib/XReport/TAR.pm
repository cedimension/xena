

#------------------------------------------------------------
package XReport::ARCHIVE::IZlibGZREAD;

use strict; use bytes;

use Symbol;
use Compress::Zlib;

sub Open {
  my $self = shift; my ($INPUT, $libFileName)  = @{$self}{qw(INPUT libFileName)};
  
  if ( ref($INPUT) eq 'XReport::ARCHIVE::IZlibGZREAD' ) {
    $self->Close();
  }

  my $LIBINPUT = gensym();

  open($LIBINPUT, "<$libFileName") 
   or 
  die("GZOPEN LIBINPUT OPEN ERROR \"$libFileName\" $!"); binmode($LIBINPUT);
  
  CORE::seek($LIBINPUT, $self->{fileOffset}, 0);
  
  $INPUT = gzopen($LIBINPUT, "rb") 
   or 
  die "GZOPEN INPUT OPEN ERROR \"$libFileName\"$!\n";

  @{$self}{qw(INPUT LIBINPUT)} = ($INPUT, $LIBINPUT);
  
  $self->{bytesAvail} = $self->{fileSize};

  return $self;
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};

  $INPUT->gzclose(); CORE::close($self->{LIBINPUT});

  $self->{bytesAvail} = 0;
  
  @{$self}{qw(INPUT LIBINPUT)} = ();
}

sub read {
  my $self = shift; my $length = $_[1]; my $l1 = $length;
  
  if ( $length > $self->{bytesAvail} ) {
    $length = $self->{bytesAvail}; 
  }
  return 0 if $length == 0;
  
  $length = $self->{INPUT}->gzread($_[0], $length);
  
  $self->{bytesAvail} -= $length;
  
  return $length;
}

sub tell {
  my $self = shift; return $self->{INPUT}->gztell();
}

sub seek {
  my $self = shift; my $INPUT = $self->{INPUT};
  my $before = $INPUT->gztell();
  my $rc = $INPUT->gzseek($_[0], $_[1]);
  my $after = $INPUT->gztell();
  $self->{bytesAvail} += $before - $after;
  return $rc;
}

sub eof {
  return $_[0]->{bytesAvail} == 0;
}

sub fileName {
  return $_[0]->{fileName};
}

sub new {
  my ($className, $libFileName, $file) = @_; my $self = {};
  
  @{$self}{qw(libFileName fileName fileOffset fileSize bytesAvail)} 
    = 
  ($libFileName, @{$file}{qw(fileName fileOffset fileSize)}, 0);

  bless $self;
}

#------------------------------------------------------------
package XReport::ARCHIVE::IZlibINFLATE;

use strict; use bytes;

use Symbol;
use Carp::Assert;
use Compress::Zlib;

sub Open {
  my $self = shift; my ($INPUT, $libFileName)  = @{$self}{qw(INPUT libFileName)};
  
  if ( ref($INPUT) eq 'XReport::ARCHIVE::IZlibINFLATE' ) {
    $self->Close();
  }
  my $INPUT = gensym();

  open($INPUT, "<$libFileName") 
   or 
  die("IZlibINFLATE NPUT OPEN ERROR \"$libFileName\" $!"); binmode($INPUT);
  
  CORE::seek($INPUT, $self->{fileOffset}, 0);

  $self->{INPUT} = $INPUT;
  
  $self->{buffer} = "";
  $self->{bytesAvail} = $self->{fileSize};
  $self->{bytesAvailIN} = $self->{fileCSize};
  
  ($self->{inflater}, my $zstatus) = inflateInit( '-WindowBits' => -MAX_WBITS() );

  return $self;
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};

  CORE::close($INPUT);

  $self->{buffer} = "";
  $self->{bytesAvail} = 0;
  
  $self->{INPUT} = $INPUT = undef; 
}

sub read {
  my $self = shift; my $length = $_[1]; my ($INPUT, $inflater) = @{$self}{qw(INPUT inflater)};
  
  if ( $length > $self->{bytesAvail} ) {
    $length = $self->{bytesAvail}; 
  }
  return 0 if $length == 0;
  
  my $bytesAvailIN = $self->{bytesAvailIN}; my $ref = \$self->{buffer};
  
  while( length($$ref) < $length and $bytesAvailIN > 0 ) {
    my $toread = ($bytesAvailIN > 4096) ? 4096 : $bytesAvailIN;
    my $rc = read($INPUT, my $buffer, $toread); assert($rc == $toread);
	my ($buffer, $zstatus) = $inflater->inflate($buffer);
	$$ref .= $buffer;
	$bytesAvailIN -= $toread;
  }
  assert(length($$ref)>=$length);
  
  $_[0] = substr($$ref, 0, $length); $$ref = substr($$ref, $length);
  
  $self->{bytesAvail} -= $length;
  
  return $length;
}

sub tell {
  my $self = shift; return $self->{fileSize} - $self->{bytesAvail};
}

sub seek_UNIMPLEMENTED {
}

sub eof {
  return $_[0]->{bytesAvail} == 0;
}

sub new {
  my ($className, $libFileName, $file) = @_; my $self = {};
  
  @{$self}{qw(INPUTLIB fileOffset fileCSize fileSize bytesAvail)} 
    = 
  ($libFileName, @{$file}{qw(fileOffset fileCSize fileSize)}, 0);

  bless $self;
}

#------------------------------------------------------------
package XReport::ARCHIVE::IPlain;

use strict; use bytes;

use Symbol;

sub Open {
  my $self = shift; my ($INPUT, $INPUTLIB) = @{$self}{qw(INPUT INPUTLIB)};

  if ( ref($INPUT) eq 'XReport::ARCHIVE::IPlain' ) {
    $self->Close();
  }
  
  $self->{bytesAvail} = $self->{fileSize};
  
  CORE::seek($INPUTLIB, $self->{fileOffset}, 0);

  $self->{INPUT} = $INPUTLIB;

  return $self;
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};

  $self->{bytesAvail} = 0;
  
  $self->{INPUT} = $INPUT = undef; 
}

sub read {
  my $self = shift; my $INPUT = $self->{INPUT}; my $length = $_[1];
  
  if ( $length > $self->{bytesAvail} ) {
    $length = $self->{bytesAvail}; 
  }
  return 0 if $length == 0;
  
  $length = CORE::read($INPUT, $_[0], $length);
  
  $self->{bytesAvail} -= $length;
  
  return $length;
}

sub tell {
  my $self = shift;
  return CORE::tell($self->{INPUT});
}

sub seek {
  my $self = shift; my $INPUT = $self->{INPUT};
  my $before = tell($INPUT);
  my $rc = CORE::seek($INPUT,$_[0], $_[1]);
  my $after = tell($INPUT);
  $self->{bytesAvail} += $before - $after;
  return $rc;
}

sub eof {
  my $self = shift;
  return $self->{bytesAvail} == 0 or CORE::eof($self->{INPUT});
}

sub new {
  my ($className, $INPUTLIB, $file) = @_; my $self = {};
  
  @{$self}{qw(INPUTLIB fileOffset fileSize bytesAvail)} 
    = 
  ($INPUTLIB, @{$file}{qw(fileOffset fileCSize)}, 0);

  bless $self;
}

#------------------------------------------------------------
package XReport::ARCHIVE::ZIP;

use strict; use bytes;

use Symbol;
use Compress::Zlib;

use constant COMPRESSION_STORED => 0;
use constant COMPRESSION_DEFLATED => 8;	

use constant LOCAL_FILE_HEADER_FORMAT		=> "v3 V4 v2";
use constant LOCAL_FILE_HEADER_LENGTH		=> 26;

use constant LOCAL_FILE_HEADER_SIGNATURE	=> "\x50\x4b\x03\x04";
use constant CENTRAL_DIRECTORY_FILE_HEADER_SIGNATURE => "\x50\x4b\x01\x02";

sub listMembers {
  my ($self) = @_; my ($INPUT, $bof) = @{$self}{'INPUT', 'bof'}; my $files = {};

  CORE::seek($INPUT, $bof, 0);
  
  while(!eof($INPUT)) {
    my ($rc, $header, $fileName, $fileOffset, $fileSize, $fileCSize, $fileCMethod);
	
    read($INPUT, $header, 4); last if $header eq CENTRAL_DIRECTORY_FILE_HEADER_SIGNATURE;
	
    if ( $header ne LOCAL_FILE_HEADER_SIGNATURE ) {
      die unpack("H*", $header);
    }
    my ($fileNameLength, $extraField, $extraFieldLength);
    read($INPUT, $header, LOCAL_FILE_HEADER_LENGTH);
    (
	  undef, 	    # $self->{'versionNeededToExtract'},
	  undef,	    # $self->{'bitFlag'},
	  $fileCMethod,	# $self->{'compressionMethod'},
	  undef,	    # $self->{'lastModFileDateTime'},
	  undef,	    # $crc32,
	  $fileCSize,	# $compressedSize,
	  $fileSize,	# $uncompressedSize,
	  $fileNameLength,
	  $extraFieldLength 
	)
	= unpack(LOCAL_FILE_HEADER_FORMAT, $header);
	
    read($INPUT, $fileName, $fileNameLength);
    read($INPUT, $extraField, $extraFieldLength);

	$fileOffset = tell($INPUT); 

	print "==> $fileName, $fileOffset\n";
	   
	$files->{$fileName} = { 
	  fileName => $fileName, 
      fileOffset => $fileOffset,
	  fileCSize => $fileCSize, 
	  fileSize => $fileSize,
	  fileCMethod => $fileCMethod
	};
	
    seek($INPUT, $fileCSize, 1);
  }

  return $files;
}

sub locateMember {
  my ($self, $fileName_) = @_; my ($INPUT, $bof) = @{$self}{'INPUT', 'bof'};

  CORE::seek($INPUT, $bof, 0); $fileName_ = uc($fileName_);
  
  while(!eof($INPUT)) {
    my ($rc, $header, $fileName, $fileOffset, $fileSize, $fileCSize, $fileCMethod);
	
    read($INPUT, $header, 4); last if $header eq CENTRAL_DIRECTORY_FILE_HEADER_SIGNATURE;
	
    if ( $header ne LOCAL_FILE_HEADER_SIGNATURE ) {
      die unpack("H*", $header);
    }
    my ($fileNameLength, $extraField, $extraFieldLength);
    read($INPUT, $header, LOCAL_FILE_HEADER_LENGTH);
    (
	  undef, 	    # $self->{'versionNeededToExtract'},
	  undef,	    # $self->{'bitFlag'},
	  $fileCMethod,	# $self->{'compressionMethod'},
	  undef,	    # $self->{'lastModFileDateTime'},
	  undef,	    # $crc32,
	  $fileCSize,	# $compressedSize,
	  $fileSize,	# $uncompressedSize,
	  $fileNameLength,
	  $extraFieldLength 
	)
	= unpack(LOCAL_FILE_HEADER_FORMAT, $header);
	
    read($INPUT, $fileName, $fileNameLength);
    read($INPUT, $extraField, $extraFieldLength);
	
	if ( uc($fileName) eq $fileName_ ) {
	  $fileOffset = tell($INPUT); 
	   
	  return { 
	   	fileName => $fileName, 
		fileOffset => $fileOffset,
		fileCSize => $fileCSize, 
		fileSize => $fileSize,
		fileCMethod => $fileCMethod
	  };
	}	
	
    seek($INPUT, $fileCSize, 1);
  }
  return undef; 
}

sub checkMember {
}

sub extractMember {
  my ($self, $file) = (shift, shift); my $INPUT = $self->{'INPUT'}; my $result;

  $file = $self->locateMember($file) if !ref($file);

  my ($fileOffset, $fileCSize, $fileSize, $fileCMethod) 
   =
  @{$file}{qw(fileOffset fileCSize fileSize fileCMethod)};

  CORE::seek($INPUT, $file->{fileOffset}, 0);

  if ( $fileCMethod == COMPRESSION_DEFLATED ) {
    my ($i, $zstatus) = inflateInit( '-WindowBits' => -MAX_WBITS() );
	
	$i or die "INFLATEINIT ERROR for MEMBER \"$file->{fileName}\" status=$zstatus\n";

    my $rest = $fileCSize;
    while($rest>0) {
	  my ($rc, $ibuffer, $obuffer);
      $rc = CORE::read($INPUT, $ibuffer, $rest > 32768 ? 32768 : $rest);
	  ($obuffer, $zstatus) = $i->inflate($ibuffer);
      $result .= $obuffer; $rest -= $rc;
    }
  }
  else {
  }

  return $result;
}

sub getMember {
  my ($self, $fileName) = @_; my $INPUT = $self->{'INPUT'};

  my $file = $self->locateMember($fileName);
  
  return undef if !$file;
  
  my $fileCMethod = $file->{fileCMethod};
  
  if ( $fileCMethod == COMPRESSION_DEFLATED ) {
    return XReport::ARCHIVE::IZlibINFLATE->new($INPUT, $file);
  }
  else {
    return XReport::ARCHIVE::IPlain->new($INPUT, $file);
  }
}

sub Open {
  my ($className, $file) = @_;
  
  my ($self, $INPUT, $bof) = ({}, gensym(), 0);

  if ( !ref($file) ) {
    open($INPUT, "<$file") or die("INPUT OPEN ERROR $file $!"); 
  }
  else {
    $INPUT = $file;
  }
  $bof = tell($INPUT); binmode($INPUT);

  #todo: check eyecatcher;

  @{$self}{'INPUT', 'bof'} = ($INPUT, $bof);

  bless $self;
}

#------------------------------------------------------------
package XReport::ARCHIVE::TAR;

use strict; use bytes;

use Symbol;
use FileHandle;
use File::Basename;
use Compress::Zlib;

use constant TAR_READ_MODE => 1;
use constant TAR_WRITE_MODE => 2;
use constant TAR_APPEND_MODE => 3; 

use constant TAR_HEADER_LENGTH => 512;

use constant GZIP_MAGIC	=> "\x1F\x8B";

use constant COMPRESSION_STORED => 0;
use constant COMPRESSION_DEFLATED => 8;	

sub format_tar_hdr {
  my ($fref) = shift; my ($str, $from_file, $file, $prefix, $pos, $size);

  ($from_file, $file, $size) = @{$fref}{qw(from_file name size)};
  
  if (length($file)>99) {
    $pos = index $file, "/",(length($file) - 100);
    die "Filename longer than 100 chars! \"$file\"" if ( $pos == -1 );
    $prefix = substr($file, 0, $pos);
    $file = substr($file, $pos+1);
    substr($prefix,0,-155) = "" if length($prefix) > 154;
  }
  else {
    $prefix="";
  }
  $str = pack("a100a8a8a8a12a12a8a1a100",
    $file,
    sprintf("%6o ",$fref->{'mode'}),
    sprintf("%6o ",$fref->{'uid'}),
    sprintf("%6o ",$fref->{'gid'}),
    sprintf("%11o ",$fref->{'size'}),
    sprintf("%11o ",$fref->{'mtime'}),
    "        ",
    $fref->{'typeflag'},
    $fref->{'linkname'}
  );
  $str .= pack("a6", $fref->{'magic'});
  $str .= '00';
  $str .= pack("a32",$fref->{'uname'});
  $str .= pack("a32",$fref->{'gname'});
  $str .= pack("a8",sprintf("%6o ",$fref->{'devmajor'}));
  $str .= pack("a8",sprintf("%6o ",$fref->{'devminor'}));
  $str .= pack("a155",$prefix);
  substr($str,148,6) = sprintf("%6o", unpack("%16C*",$str));
  substr($str,154,1) = "\0";
  
  $str .= "\0" x (TAR_HEADER_LENGTH-length($str));
  
  return ($from_file, $size, $str);
}

sub write_tar_file {
  my ($self, $OUTPUT) = (shift, shift); my ($INPUT, $close_output) = (gensym(), 1);

  ### manage Scalar -> create Tar Trailer XREF
  
  if ( $OUTPUT ) {
    if ( ref($OUTPUT) ) {
      binmode($OUTPUT); $close_output = 0;
    }
    elsif ( $OUTPUT ne ":Scalar" ) {
      my $fileName = $OUTPUT; $OUTPUT = gensym(); 
      open($OUTPUT, ">$fileName")
        or
      die("TAR OUTPUT OPEN ERROR \"$fileName\" $!"); binmode($OUTPUT); 
    }
    else {
      $OUTPUT = gensym(); 
      open($OUTPUT, ">", $_[0]) 
        or 
      die("TAR SCALAR OUTPUT OPEN ERROR $_[0] $!"); binmode($OUTPUT);
    } 
  }
  else {
    if ( $self->{'mode'} != TAR_APPEND_MODE ) {
      die("INVALID OPEN MODE OR MISSING OUTPUT FILE NAME");
    }
    my $append_offset = $self->{'append_offset'}; 
    $OUTPUT = $self->{'INPUT'}; seek($OUTPUT, $append_offset, 0); binmode($OUTPUT);
  }

  my $tarfiles = $self->{'append_tar_files'};
    
  foreach my $fref (@$tarfiles) {
    my ($file, $size, $tarHdr) = format_tar_hdr($fref); print $OUTPUT $tarHdr;
	open($INPUT, "<$file") or die("OPEN INPUT ERROR \"$file\" $!"); binmode($INPUT);
	
	my ($begOffset, $buff) = (tell($OUTPUT), "");
    while(!eof($INPUT)) {
	  read($INPUT, $buff, 32768);
	  print $OUTPUT $buff;
	}
	
	my $endOffset = tell($OUTPUT);
	if ( tell($INPUT) != $size ) {
	  die "COPY ERROR INVALID INFILE READ (".tell($INPUT).") ($size)";
	}
	if ( ($endOffset-$begOffset) != $size ) {
	  die "COPY ERROR INVALID INFILE WRITE (".($endOffset-$begOffset).") ($size)";
	}
	close($INPUT);

	print $OUTPUT ("\0" x ((512-($endOffset%512)) % 512));
  }

  print $OUTPUT "\0" x 1024;
  
  close($OUTPUT) if $close_output;
}

sub addFiles {
  my ($self, $base_dir, @files) = @_; my ($added_files, $skipped_files) = ([], []);

  ### die if $tar_fileSize < 0;
  $base_dir .= "/" if $base_dir ne '' and $base_dir !~ /[\/\\]$/;
  $base_dir =~ s/\\/\//g;

  # use test operating system;
  my ($has_getpwuid, $has_getgrgid) = (0, 0);
  
  foreach my $file (@files) {
    my ($mode,$uid,$gid,$rdev,$size,$mtime,$linkname,$typeflag);
    if ((undef,undef,$mode,undef,$uid,$gid,$rdev,$size,
         undef,$mtime,undef,undef,undef) = stat($file)) {
      if (!-f $file) { # Plain file only
        die "TAR add_files INVALID FILE TYPE \"$file\"";
      }
      push(@{$self->{'append_tar_files'}}, {
        name => $base_dir.basename($file),           
        mode => $mode,
        uid => $uid,
        gid => $gid,
        size => $size,
        mtime => $mtime,
        chksum => "      ",
        typeflag => $typeflag, 
        linkname => $linkname,
        magic => "ustar\0",
        version => "00",
		
        # WinNT protection
        uname => $has_getpwuid ?(getpwuid($uid))[0] :"unknown",
        gname => $has_getgrgid ?(getgrgid($gid))[0] :"unknown",
		
        devmajor => 0, 
        devminor => 0,
        prefix => "",

        from_file => $file,
      });
	  push @$added_files, $file; # Successfully added file
	  $self->{tar_fileSize} += 512 + $size + ((512-$size%512)%512);
    }
    else {
      push @$skipped_files, $file;; # stat failed
    }
  }
  return (scalar(@$skipped_files), $added_files, $skipped_files);
}

sub Members {
  my $self = shift; return $self->{'tar_files'} if exists($self->{tar_files});

  return $self->listMembers();
}

sub listMembers {
  my $self = shift; my ($INPUT, $bof) = @{$self}{'INPUT', 'bof'}; my $tar_files = [];
  
  my ($rc, $header, $fileName, $fileOffset, $fileSize, $tar_fileSize);
  
  seek($INPUT, $bof, 0); $tar_fileSize = 0; $fileOffset = 0;

  while ( !eof($INPUT) ) {
    $rc = read($INPUT, $header, 512); last if $header =~ /^\x00+$/;
    ($fileName,$fileSize) = unpack("a100x8x8x8a12", $header);
	
	$fileName =~ s/\x00+$//g; $fileSize= oct($fileSize); 

    $tar_fileSize += $fileSize + 512 + (512-$fileSize%512)%512;

	push @$tar_files, {
	  fileName => $fileName, 
      fileOffset => $fileOffset, 
      fileSize => $fileSize, 
	};
	
	seek($INPUT, $tar_fileSize, 0); $fileOffset = tell($INPUT) - $bof;
  }

  @{$self}{qw(tar_files tar_fileSize append_offset)} = ($tar_files, $tar_fileSize, $fileOffset);

  return $self->{'tar_files'} = $tar_files;
}

sub locateMember {
  my ($self, $fileName_) = @_; my ($INPUT, $bof, $eof) = @{$self}{qw(INPUT bof eof)};
  
  seek($INPUT, $bof, 0); $fileName_ = lc($fileName_); 

  my $locate_offset = -1;

  if ( $fileName_ =~ /offset:(\d+)/ ) {
    $locate_offset = $1;
  }
  elsif ( $fileName_ =~ /block:(\d+)/ ) {
    $locate_offset = $1*512;
  }
  seek($INPUT, $bof+$locate_offset,0) if $locate_offset > 0;

  while ( !eof($INPUT) ) {
    my ($rc, $header, $fileName, $fileOffset, $fileSize, $fileCSize, $fileCMethod, $fileMagic);
	
    $rc = read($INPUT, $header, 512); last if $header =~ /^\x00+$/;
	
    ($fileName, $fileSize) = unpack("a100x8x8x8a12", $header);

	$fileName =~ s/\x00+$//g; $fileSize = oct($fileSize);
    
    if ( lc($fileName) eq $fileName_ or $locate_offset != -1) {
	   $fileOffset = tell($INPUT); $fileCSize = $fileSize;
	   
	   read($INPUT, $fileMagic, 2); seek($INPUT, -2, 1);
	   
	   if ( $fileMagic eq GZIP_MAGIC ) {
	     seek($INPUT, $fileCSize-4, 1);
         $rc = read($INPUT ,$header, 4);
         $fileSize = unpack("I", $header);
         seek($INPUT, -$fileCSize, 1);
		 $fileCMethod = COMPRESSION_DEFLATED;
	   }
	   else {
	     $fileCMethod = COMPRESSION_STORED;
	   }
	   
	   return { 
	   	 fileName => $fileName, 
		 fileOffset => $fileOffset,
		 fileCSize => $fileCSize, 
		 fileSize => $fileSize,
		 fileCMethod => $fileCMethod
	   };
	}

	seek($INPUT, $fileSize + ((512-$fileSize%512)%512), 1);
  }
  
  return undef;
}

sub extractMember {
  my ($self, $file) = (shift, shift); my $INPUT = $self->{'INPUT'}; my $result;

  $file = $self->locateMember($file) if !ref($file);
  
  my ($fileOffset, $fileCSize, $fileSize, $fileCMethod) 
   =
  @{$file}{qw(fileOffset fileCSize fileSize $fileCMethod)};

  if ( $fileCMethod == COMPRESSION_DEFLATED ) {
    my $gz = gzopen($INPUT, "r") 
     or 
    die "GZOPEN ERROR for MEMBER \"$file->{fileName}\"\n" ;

    $gz->gzseek($file->{fileOffset}, 0);

    my ($rest, $buf, $rc) = ($fileSize, '', 0);
    while($rest>0) {
      $rc = $gz->gzread($buf, $rest > 32768 ? 32768 : $rest);
      $result .= $buf; $rest -= $rc;
    }
    $gz->gzclose();
  }
  else {
    seek($INPUT, $file->{fileOffset}, 0);
    my ($rest, $buf, $rc) = ($fileSize, '', 0);
    while($rest>0) {
      $rc = read($INPUT, $buf, $rest > 32768 ? 32768 : $rest);
      $result .= $buf; $rest -= $rc;
    }
  }

  return $result;
}

sub getMember {
  my ($self, $fileName) = @_; my ($INPUT, $tarFileName) = @{$self}{qw(INPUT tarFileName)};

  my $file = $self->locateMember($fileName); 

  return undef if !$file;

  my $fileCMethod = $file->{'fileCMethod'};
  
  if ( $fileCMethod == COMPRESSION_DEFLATED ) {
    return XReport::ARCHIVE::IZlibGZREAD->new($tarFileName, $file);
  }
  else {
    return XReport::ARCHIVE::IPlain->new($tarFileName, $file);
  }
}

sub checkMember {
}

sub deleteMember {
}

sub tar_fileSize {
  my ($self, $fileName) = (shift, shift); my $fileSize;

  $self->listMembers() if !exists($self->{tar_fileSize});

  if ( $fileName eq '') {
    return $self->{tar_fileSize};
  }

  if (!-f $fileName) { 
    die "TAR fileSize INVALID FILE TYPE \"$fileName\"";
  }

  $fileSize = -s $fileName;

  return $self->{tar_fileSize} + 512 + ((512-$fileSize%512)%512);
}

sub Create {
  my $className = shift; my $self = {};

  $self->{
    open_mode => TAR_WRITE_MODE,
    tar_files => [],
    tar_fileSize => 512
  };

  bless $self, $className;
}

sub Open {
  my ($className, $tarFile, $mode) = @_; my ($self, $INPUT, $tarFileName, $bof); $INPUT = gensym();

  if ( !ref($tarFile) ) {
    $tarFileName = $tarFile; $mode = $mode =~ /a|\+/i ? TAR_APPEND_MODE : TAR_READ_MODE;
    if ($tarFileName =~ /^id:(\d+)/i) {
      my $TarFileId = $1; my $dbr; 
      $dbr = dbExecute("
        SELECT * from tbl_TarFiles where TarFileId = $TarFileId
      ");
      $tarFileName = $dbr->GetFieldsValues('TarLocalFileName');
    }
    open($INPUT, ($mode == TAR_APPEND_MODE ? '+<' : '<').$tarFileName) 
     or 
    die("TAR FILE INPUT OPEN ERROR \"$tarFileName\" $!"); 
  }
  else {
    $INPUT = $tarFile; $mode = TAR_READ_MODE;
  }

  $bof = tell($INPUT); binmode($INPUT); 

  $self = {
     tarFileName => $tarFileName,
     open_mode => $mode,
     INPUT => $INPUT,
     bof => $bof
  }; 

  bless $self, $className;
}

sub Close {
  my $self = shift; %$self = ();
}

1;

__END__

package main;

#my $tar = XReport::ARCHIVE::TAR->Open("c:/temp/zz/zz1.tar", "a");
my $tar = XReport::ARCHIVE::TAR->Open("id:1", "a");

#print "==1 ", $tar->tar_fileSize(), "\n";

#my $member = $tar->getMember("in/2003/0910/zzGENERALI.20030910164146.115108.DATA.TXT.gz");
#my $member = $tar->getMember("offset:62976");
my $member = $tar->getMember("block:123");


for (1..2) {
$member->Open(); 
open(OUT, ">c:/temp/zz/zzout.txt"); binmode(OUT);
while($member->read(my $rec, 32768)) {
  print OUT $rec;
}
close(OUT);
$member->Close(); 
}

die $member;

if (0) {
$tar->addFiles('IN/2003/0910/', "c:/xreport/storage/in/2003/0910/ZZGENERALI.20030910164146.115108.DATA.TXT.gz");

print "==2 ", $tar->tar_fileSize(), "\n";

$tar->write_tar_file();

}

for (@{$tar->Members()}) {
  print join("\n", %$_), "\n";
}
