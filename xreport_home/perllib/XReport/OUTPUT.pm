#######################################################################
# @(#) $Id: Output.pm,v 1.4 2001/03/22 13:10:03 mpezzi Exp $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
package XReport::OUTPUT::Postscript;

use strict; use bytes;

use Carp;
use Symbol;

use Convert::EBCDIC;
#use Win32::Registry;

use XReport::Util;
use XReport::Distillr;
use XReport::ghostscript;

my $translator = new Convert::EBCDIC;

#######################################################################
#
#######################################################################

use constant ERROR_DISTILLER_NOT_INSTALLED	=> 255+1;
use constant ERROR_DISTILLER_NOT_FOUND      => 255+5;

use constant ERROR_DISTILLER_NOT_ACTIVE     => 255+2;
use constant ERROR_REGISTER_CLASS_EX        => 255+3;
use constant ERROR_CREATE_WINDOW            => 255+6;
use constant ERROR_SEND_MESSAGE             => 255+7;
use constant ERROR_CREATE_PROCESS_FAILED    => 255+8;
use constant ERROR_TERMINATE_PROCESS_FAILED => 255+9;
use constant ERROR_DISTILLER_IS_TERMINATED	=> 255+10;

my $Distiller_Initialized = 0;

BEGIN {
#  XReport::Distillr::Kill("DISTILLER Server for ".getConfValues('SrvName'))
}

END {
#  TERMINATE_Distiller();
}

package XReport::OUTPUT::Postscript;

use strict; use bytes;
use File::Copy qw();

sub psify {
  my $str = $_[0];$str =~ s/([\(\)\\\n])/\\$1/g;
  return $str;
}

sub DIRECT_MODE {
  my $self = shift; $self->{DIRECT_MODE} = @_;
}

sub _doPrint {
   my $self = shift;
    my $OUTPUT = shift || $self->{OUTPUT};
   my $np = grep /[\>\)] [^ ]+Line/, @_;
   $main::debug && i::logit( "_doPrint called by ".join('::', (caller())[0,2])
           . " Pagelen so far: ".$self->{pagelength}
           . " Lines: ($np)".scalar(@_)
		   );
   $self->{pagelength} += $np;
   unless ( exists($self->{OUTPUT}) && ref($self->{OUTPUT}) ) {
        i::warnit("PS Print called with undefined file target by ".join('::', (caller(1))[0,2]));
        die "PSPrint Error";
   }
   print $OUTPUT @_;
}

sub Print {
  my $self = shift;
  #die "SELF: ", ref($self), "\n";
  $main::veryverbose && i::logit("PS Print called  by ".join('::', (caller(1))[0,2]));
  my $linedata = join("", @_);
  my $linelen = $self->{MaxOutLength};
#  if ($linedata =~ /\s*\(([^\)]*?) *\).+$/ ) {
  if ($linedata =~ /^(?:\((?:.*?)\) \((?:.*?)\) +| *)\((.*?)\) [^ ]+Line$/ ) {
  	my $textstr = $1;
    (my $stripped = $textstr) =~ s/ *$//;
    $linelen = length($stripped);
    if ( $textstr !~ /^\s*$/ ) {
       my $hexstr = unpack('H*', $stripped);
       $linedata =~ s/\(\Q$textstr\E\)/<$hexstr>/;
    }
    
    #warn "LL decreasing to $linelen for line at $self->{atPage}(line $self->{pagelength})\n - LINE: $stripped\n" if $self->{MaxOutLength} < $linelen;
    $self->{MaxOutLength} = $linelen if $self->{MaxOutLength} < $linelen;
  }
  elsif ($linedata =~ /^(?:\((?:.*?)\) \((?:.*?)\) +| *)\<(.*?)\> [^ ]+Line$/ ) {
    my $textstr = $1;
    $linelen = length($textstr)/2;
    #warn "LL decreasing to $linelen for line at $self->{atPage}(line $self->{pagelength})\n - LINE: $stripped\n" if $self->{MaxOutLength} < $linelen;
    $self->{MaxOutLength} = $linelen if $self->{MaxOutLength} < $linelen;
    if ( !$linelen ) { $linedata =~ s/\<$textstr\>/( )/; }
  }
  if ( !$self->{DIRECT_MODE} ) {
    push @{$self->{Print}}, $linedata;
  }
  else {
    $self->_doPrint(undef, $linedata);
  }
}

sub FlushPrintLines {
    my $self = shift;
    my $OUTPUT = shift || $self->{OUTPUT};
    $main::debug && i::logit("FlushPrintPage called by ".join('::', (caller())[0,2])
                                  ." Lines: ".scalar(@{$self->{Print}}));
    my $jr = $self->{jr};
    my $pspgexit = $jr->getValues('PSPageExit');
    if (scalar(@{$self->{Print}})) {
	   my $linelist = $self->{Print};
       if ($pspgexit && (my $rtn = $pspgexit->can('Print')) ) {
          $linelist = &$rtn($pspgexit, $self->{Print});
       } 
       $self->_doPrint($OUTPUT, @{$linelist});
       $self->{Print} = [];
    }  
}

sub Header {
  my $self = shift; my $OUTPUT = $self->{OUTPUT}; my $jr = $self->{jr};
  
  my $LocalResources = $jr->getFileName('LOCALRES');

  $LocalResources =~ s/\\/\//g;
  (my $OutFileName = $self->{OutFileName}) =~ s/\//\\/g;
  my $jrname = $self->{jr}->getValues('JobReportName');
  $main::veryverbose && i::logit("Header --- $jrname");
  (my $xreportstartfile = $OutFileName) =~ s/\\\Q$jrname\E\./\\\$\$xreportstartfile\$\$\./;
  $xreportstartfile =~ s/\\/\//g;
  $main::veryverbose && i::logit("Header --- $xreportstartfile");
  i::warnit("INSERT STATEMENT to read process parameters from $xreportstartfile");
  return $self->Print(
    "%!PS-Adobe-2.0 \n"
   ,"\%\%EndComments \n"
   ,"\%\%EndProlog \n\n"
  );

#  $self->Print(
#    "%!PS-Adobe-2.0 \n"
#   ,"\%\%EndComments \n"
#   ,"/LocalResources ($LocalResources/) def \n"
#   ,"<< \n"
#  );
#  
#  if ($jr->getValues('isAfp')) {
#    for (qw(LastFormDef LastMediumMap)) {
#      $self->{$_} = $jr->getValues($_);
#    }
#  }
#
#  if (!exists($self->{'Chars'}) and $self->{'CharsDef'} =~ /\w/) {
#    my @chars = split(/[ ,]+/, $self->{'CharsDef'}); my $chars = '';
#    for my $i (1..scalar(@chars)) {
#      $chars .= "  /F$i /$chars[$i-1] 1 a.fdef\n";
#    }
#    $self->{'Chars'} = $chars;
#  }
#
#  $self->{'PageSize'} = ucfirst(lc($self->{'PageSize'}));
#  warn "PAGE DEFAULTS:"
#       ," PAGESIZE: $self->{PageSize}"
#       ," MOL: $self->{MaxOutLength}"
#       ," MPL: $self->{MaxPageLength}"
#       ," LPP: $self->{LinesPerPage}"
#       ," CPL: $self->{CharsPerLine}\n";
#  
#  my ($currlpp, $currcpl, $currmol, $currmpl) = @{$self}{qw(LinesPerPage CharsPerLine MaxOutLength MaxPageLength)};
#  $self->{LinesPerPage} = $currmpl if ($currlpp > $currmpl);
#  $self->{CharsPerLine} = $currmol if ($currcpl > $currmol); 
#  warn "PAGE ATTRS:"
#       ," GT: $currlpp :: $currcpl :: $currmol :: $currmpl", ($currcpl > $currmol ? '1' : '0')
#       ," PAGESIZE: $self->{PageSize}"
#       ," MOL: $self->{MaxOutLength}"
#       ," MPL: $self->{MaxPageLength}"
#       ," LPP: $self->{LinesPerPage}"
#       ," CPL: $self->{CharsPerLine}"
#       ,"\n";
#  
#  for (qw(
#    ReportFormat
#    CharsPerLine 
#    LinesPerPage 
#    PageOrient 
#    PageSize 
#    FontSize
#    FitToPage 
#    CodePage 
#    Trc 
#    FormDef LastFormDef LastMediumMap
#    PageDef LastPageDef LastDataMap
#    t1Fonts LaserAdjust ReplaceDef 
#  )) {  
#    print $OUTPUT " /$_ (", $self->{$_}, ") \n";
#  }
#  
#  print $OUTPUT " /CharsTable {\n", $self->{'Chars'}, " }\n" if exists($self->{'Chars'});
#  
#  ### todo: add resources from ----------------------------------------------////
#  if ( $jr->getValues('isAfp') and $jr->getValues('FormDef') ne "" ) {
#    $self->Print(" /UseGlobalLibs false");
#  }
#  else {
#    $self->Print(" /UseGlobalLibs true");
#  }
#  ### -----------------------------------------------------------------------////
#  
#  $self->Print(
#   "\n>>\n",
#   "XreportStart \n" .
#   "\%\%EndProlog \n\n"
#  );
}

sub writeXreportStart {
  my $self = shift; my $OUTPUT = shift; my $jr = $self->{jr};
  my $cinfo = $jr->getValues('cinfo');
  $main::veryverbose && i::warnit("OUTPUT WriteXreportStart - jr: ".Dumper($cinfo));
  my %kmap = ( FormDef => join('|', qw(formdef form) )
             , PageDef => join('|', qw(pagedef fcbnm) )
             , Chars => join('|', qw(chars) )
             );

  my %kprocs = ( FormDef => sub { return ($_[0] =~ /^F\d.{3,6}/i ? '' : 'F1').$_[0]; }
               , PageDef => sub { return ($_[0] =~ /^P\d.{3,6}/i ? '' : 'P1').$_[0]; }
               , Chars   => sub { my $chrID = 0;
                        return join("\n", map { 
                                                my $resN = ($_ =~ /^X/i ? '' : 'X0').$_; 
                                                $jr->[3]->{AfpResources}->{$resN} = 1; $chrID++; "/F$chrID /$resN 0 0 a.fdef" 
                                              } split(/[ ,]+/, $_[0]) ); }
               ); 

  while ( my ($selfkey, $infokl) = each %kmap ) {
     my $infokre = qr/^(?i:$infokl)$/;
     my $infok = (grep /$infokre/, keys %{$cinfo})[0];
     if ($infok && $cinfo->{$infok}) {
        $self->{$selfkey} = &{$kprocs{$selfkey}}($cinfo->{$infok}) ;
        $main::debug && i::warnit("OUTPUT WriteXreportStart - Attribute $selfkey set to $self->{$selfkey}");
     }
  }

  my ($fdef, $pdef, $cdef, $isafp) = map { my $v = $jr->getValues($_); $self->{$_} = $v if $v; $self->{$_} } 
  qw(FormDef PageDef CharsDef isAfp);
  $self->{FormDef} = 'F1'.$self->{FormDef} if ($self->{FormDef} && $self->{FormDef} !~ /^F1/i);
  $self->{PageDef} = 'P1'.$self->{PageDef} if ($self->{PageDef} && $self->{PageDef} !~ /^P1/i);

  my $LocalResources = $jr->getFileName('LOCALRES');

  $LocalResources =~ s/\\/\//g;
  
  print $OUTPUT "<< \n";
  
  if ( $isafp ) {
    for (qw(LastFormDef LastMediumMap)) {
      $self->{$_} = $jr->getValues($_);
    }
  }
  
  if (!exists($self->{'Chars'}) and $self->{'CharsDef'} =~ /\w/) {
#    my @chars = split(/[ ,]+/, $self->{'CharsDef'}); my $chars = '';
#    for my $i (1..scalar(@chars)) {
#      $chars .= "  /F$i /$chars[$i-1] 1 a.fdef\n";
#    }
#    $self->{'Chars'} = $chars;
    $self->{Chars} = &{$kprocs{'Chars'}}($self->{'CharsDef'});
  }

  $self->{'PageSize'} = ucfirst(lc($self->{'PageSize'}));
  $main::debug && i::warnit("PAGE DEFAULTS:"
       ," PAGESIZE: $self->{PageSize}"
       ," MOL: $self->{MaxOutLength}"
       ," MPL: $self->{MaxPageLength}"
       ," LPP: $self->{LinesPerPage}"
       ," CPL: $self->{CharsPerLine}"
       );
  
  my ($currlpp, $currcpl, $currmol, $currmpl) = @{$self}{qw(LinesPerPage CharsPerLine MaxOutLength MaxPageLength)};
  $self->{LinesPerPage} = $currmpl if ($currlpp > $currmpl);
  $self->{CharsPerLine} = $currmol if ($currcpl < $currmol);

  i::warnit("PAGE RUN VALUES:"
       ," ORIG LPP: $currlpp CPL: $currcpl MOL: $currmol MPL: $currmpl CPL::MOL?", ($currcpl > $currmol ? 'GT' : 'LE'), ' -- CURR - '
       ," PSZ: $self->{PageSize}"
       ," MOL: $self->{MaxOutLength}"
       ," MPL: $self->{MaxPageLength}"
       ," LPP: $self->{LinesPerPage}"
       ," CPL: $self->{CharsPerLine}"
       );
  
       ##--------------------------------------------------------- x generali
       if ( my $ReplaceDef = $jr->getValues('ReplaceDef') ) {
          my $ReplFile = getConfValues('afpdir')."/REPLLIB/$ReplaceDef.ps"; 
          if (-e $ReplFile) { 
             open(my $INPUT = gensym(), "<$ReplFile") 
               or die("INPUT OPEN ERROR for ReplaceDef \"$ReplFile\" $!"); 
             my $list = <$INPUT>; close($INPUT);
             if ( $list =~ /^\%INCLUDE:(.*)/i ) {
                $list = $1;
                for my $resName ( split(/\W+/, $list) ) {
#                  $self->{AfpResources}->{$resName} = 1 if $_ ne '';  ## mpezzi more afp resources 
                   $jr->[3]->{AfpResources}->{$resName} = 1 if $resName;  ## mpezzi more afp resources 
                }
             }
          } 
          else {
             $jr->setValues('ReplaceDef', '');
             $self->{ReplaceDef} = '';
          }
       }
    ##--------------------------------------------------------- x generali

  for (qw(
    ReportFormat
    CharsPerLine 
    LinesPerPage 
    PageOrient 
    PageSize 
    FontSize
    FitToPage 
    CodePage 
    Trc 
    FormDef LastFormDef LastMediumMap
    PageDef LastPageDef LastDataMap
    t1Fonts LaserAdjust ReplaceDef 
  )) {  
    print $OUTPUT " /$_ (", $self->{$_}, ") \n";
  }
  $self->{LinesPerPage} = $currlpp;
  $self->{CharsPerLine} = $currcpl;
  
  print $OUTPUT " /CharsTable {\n", $self->{'Chars'}, " }\n" if exists($self->{'Chars'}) && (!$XReport::cfg->{IgnoreChars}) ;
  
  ### todo: add resources from ----------------------------------------------////
  if ( $jr->getValues('isAfp') and $jr->getValues('FormDef') ne "" ) {
    print $OUTPUT " /UseGlobalLibs false\n";
  }
  else {
    print $OUTPUT " /UseGlobalLibs true\n";
  }
  ### -----------------------------------------------------------------------////
  
  print $OUTPUT ">>\nXreportStart\n";

  ### todo: add resources from ----------------------------------------------////
  #  if ($jr->getValues('isAfp') and !$jr->getValues('isFullAfp') and $jr->getValues('FormDef') ne "" ) {  
  #mpezzi se formdef specificato, cercarlo in ogni caso
  # if ( $jr->getValues('isAfp') 
  #       and $jr->getValues('FormDef') ne "" and $jr->getValues('FormDef') ne "NULL" 
    #
    #  if ( $jr->getValues('isAfp') and !$jr->getValues('isFullAfp') 
    #               and $jr->getValues('FormDef') ne "" ) { 
    #
    
    i::warnit("RUN VALUES: FDEF: $self->{FormDef} PDEF: $self->{PageDef} JR VALUES: FDEF: $fdef PDEF: $pdef CDEF: $cdef");
#    if ( $fdef and $fdef ne "NULL" ) { 
       my $TimeRef = $self->{XferStartTime};
       
       $jr->[3]->{AfpResources}->{$self->{FormDef}} = 1 
           if ($self->{FormDef} && $self->{FormDef} ne 'NULL');
       $jr->[3]->{AfpResources}->{$self->{PageDef}} = 1 
           if ($self->{PageDef} && $self->{PageDef} ne 'NULL');
       my @afp_resList = grep( !-e "$LocalResources/$_\.ps", keys(%{$jr->[3]->{AfpResources}})
               #      (@{$self}{qw(FormDef PageDef)}, keys(%{$self->{AfpResources}}))  ## mpezzi more afp resources 
#                         ($self->{FormDef} =~ /^NULL$/i ? (keys(%{$jr->[3]->{AfpResources}}))  
#                                                      : (@{$self}{qw(FormDef PageDef)}, keys(%{$jr->[3]->{AfpResources}})))  ## mpezzi more afp resources 
                             );
       if ( scalar(@afp_resList) ) {
          $main::debug && i::warnit("RESLIST: ".join('::', @afp_resList));
          require XReport::AFP::EXTRACT;
  
          XReport::AFP::EXTRACT::ToLocalResources($LocalResources, $TimeRef, @afp_resList);
       }
       else {
          $main::debug && i::warnit("RESLIST IS EMPTY");
       }
    ### -----------------------------------------------------------------------////
  
 #   }

}

sub Trailer {
  my $self = shift; 
  my ($OUTPUT, $jr) = @{$self}{qw(OUTPUT jr)};
  $main::debug && i::warnit("OUTPUT Trailer - caller:". join('::', (caller())[0,2]));


  $self->Print(
   "\n\%\%Trailer \n",
   "\%\%EOF \n"
  );
}

sub InsertOutlineTarget {
  my ($self, $TProgr, $TType) = @_; my $OUTPUT = $self->{OUTPUT};

  $self->Print("[/Dest /t.$TProgr " .
   (( $TType eq 'LINE' )
     ? "/View [/XYZ -5 curY 20 add null] "
	 : "/Page AtPage "
   ).
   "/DEST pdfmark " .
   "\n"
  );
}

sub PutOutlineTree {
  my ($self, $OName, $OType, $OutlineNode, @VarValues) = (shift, shift, shift, ,shift ());;

  my $OUTPUT = $self->{OUTPUT};

  $OutlineNode = $OutlineNode->{$OName};
  @VarValues = sort( keys(%$OutlineNode) );
  
  # The real page number is not known till after distilling
  # and during distilling more pages could be generated.
  # For example with AFP.
  # So the Page Dest has not to be used.
  
  if ( $#VarValues > 0 ) { # tree internal node
    # maybe an internal node also a leaf node ?? (incorrout in this case)
	print $OUTPUT 
      "[/Title (". psify($OName) .") " .
      "/Count -$#VarValues " .
	  (( 1 or $OType eq 'LINE' ) 
	    ? "/Dest /t." . $OutlineNode->{'->'}->[0] . " " 
	    : "/Page " . $OutlineNode->{'->'}->[0] . " " 
	  ). 
      "/OUT pdfmark " .
	  "\n"
    ;
    for my $VarValue ( @VarValues ) {
      next if $VarValue eq '->';
	  $self->PutOutlineTree($VarValue, $OType, $OutlineNode); 
    }
  }
  else { # tree leaf node
    my $aref = $OutlineNode->{'->'};
    if ( $#$aref > 0 ) { 
	  #more than one entries with the same value
      print $OUTPUT
        "[/Title (". psify($OName) .") " .
        "/Count -".($#$aref+1)." " .
	    (( 1 or $OType eq 'LINE' )
	      ? "/Dest /t." . $OutlineNode->{'->'}->[0] . " " 
	      : "/Page " . $OutlineNode->{'->'}->[0] . " " 
	    ). 
        "/OUT pdfmark " .
	    "\n"
      ;
	  for my $j ( 0..$#$aref ) { 
	    print $OUTPUT 
          "[/Title (". psify($OName) ."\.".($j+1).") " .
	      (( 1 or $OType eq 'LINE' )
	        ? "/Dest /t." . $OutlineNode->{'->'}->[$j] . " "
	        : "/Page " . $OutlineNode->{'->'}->[0] . " " 
	      ). 
          "/OUT pdfmark " .
	      "\n"
        ;
	  }
	}
	else {
	  print $OUTPUT
        "[/Title (". psify($OName) .") " .
	    (( 1 or $OType eq 'LINE' )
	      ? "/Dest /t." . $OutlineNode->{'->'}->[0] . " "
	      : "/Page " . $OutlineNode->{'->'}->[0] . " " 
	    ). 
        "/OUT pdfmark " .
	    "\n"
      ;
	}
  }
  $self->{ThereAreOutlines} = 1;
}

sub NewPhysicalReport {
  my ($self, $ReportId, $ListOfPages) = @_;

  my ($PdDocFm, $PdDocTo) = ($self->{PdDocFm}, $self->{PdDocTo});
  
  if ( !$self->{PdDocFm} ) {
    $PdDocFm = Win32::OLE->CreateObject("AcroExch.PDDOC");
    if ( !$PdDocFm or !$PdDocFm->Open($self->{PdfFileName}) ) {
	  croak("AcrobatError: PDFLibrary Open() ".$self->{PdfFileName}." ERROR");
	}
	$self->{PdDocFm} = $PdDocFm;
    $PdDocTo = Win32::OLE->CreateObject("AcroExch.PDDOC");
    if ( !$PdDocTo ) {
	  croak("AcrobatError: Unable to Create a new AcroExch.PDDOC COM Object");
	}
	$self->{PdDocTo} = $PdDocTo;
  }
  my $PdfFileName = $self->{jr}->getFileName('PDFEXTRACT', $ReportId); unlink $PdfFileName;
  if ( !$PdDocTo or !$PdDocTo->Create() ) {
    croak("AcrobatError: PDFLibrary Create() ERROR");
  }

  my @ListOfPages = split(',', $$ListOfPages);

  my ($toAtPage, $fm, $to) = (0, 0, 0);

  for (my $j=0; $j<$#ListOfPages; $j+=2) {
    ($fm,$to) = ($ListOfPages[$j] , $ListOfPages[$j+1]);
    &$logrRtn("$ReportId from=$fm to=$to");
    $PdDocTo->InsertPages($toAtPage-1, $PdDocFm, $fm-1, $to-$fm+1, 0)
	 or
	croak("AcrobatError: PDFlibrary InsertPages() ERROR");
    $toAtPage += $to-$fm+1;
  }

  $PdDocTo->Save(1+4+32, $PdfFileName) or croak("AcrobatError: PDFlibrary Save() $PdfFileName ERROR");
  $PdDocTo->Close() or croak("AcrobatError: PDFlibrary Close() $PdfFileName ERROR");
}

sub EndCreatePhysicalFiles {
  my ($self, $PdDocFm, $PdDocTo) = (shift, '', '');

  if ( $PdDocFm = $self->{PdDocFm} ) {
    $PdDocFm->Close();
	$self->{PdDocFm} = undef;
  }

  if ( $PdDocTo = $self->{PdDocTo} ) {
    $PdDocTo->Close();
	$self->{PdDocTo} = undef;
  }
  
  ($PdDocFm, $PdDocTo) = (undef, undef);
}

#######################################################################
#
#######################################################################

use constant MAX_FILE_SIZE => 1500;

use constant RF_ASCII => 1;
use constant RF_EBCDIC => 2;
use constant RF_POSTSCRIPT => 6;

sub NewFile {
  my $self = shift; my $OUTPUT = $self->{OUTPUT}; my $jr = $self->{jr};
  $main::veryverbose && i::logit("Init New OUT File");

  my $lFile = $self->{atFile} || 0;

  $self->EndFile() if ( $self->{atFileSize} > 0 );
  $self->{atFile} += 1 if $self->{atPage} > 1;
  $self->{atFileSize} = 0.1;
   
  $self->{OutFileName} = $jr->getFileName('PSOUT',$self->{atFile});
  $self->{PdfFileName} = $jr->getFileName('PDFOUT',$self->{atFile});
  $main::veryverbose && i::logit("OutFileName is --- $self->{OutFileName}");
  open($OUTPUT, ">".$self->{OutFileName}) 
   or 
  croak("OUTPUT OPEN ERROR $!"); binmode($OUTPUT);
  $self->{OUTPUT} = $OUTPUT;
  $self->{DIRECT_MODE} = 1;

  $self->Header();

  $self->{DIRECT_MODE} = 0;

  $self->{FromPage} = $self->{atPage};
  $self->{atPage} -= 1 if $self->{atPage} > 1;

  return $lFile;
}

sub EndFile {
  my $self = shift; return undef if !($self->{atFileSize} >= 1);
  
  my ($OUTPUT, $PAGEXREF) = @{$self}{qw(OUTPUT PAGEXREF)}; 
  
#  warn ref($self), " EndFile atFileSize: $self->{atFileSize}", 
#       sprintf(" called by %s@%s", (caller( ))[0,2]), 
#       sprintf(" called by %s@%s", (caller(1))[0,2]), "\n" if $main::debug;
  
  my $jr = $self->{jr};
  $self->FlushPrintLines($OUTPUT);
#  if (scalar(@{$self->{Print}})) {
#    $main::veryverbose && i::logit("Printing ".Dumper($self->{Print}));
#    print $OUTPUT @{$self->{Print}}; 
#    @{$self->{Print}} = ();
#  }  
#  $main::veryverbose && i::logit("Printed on OUTPUT");
  $self->{DIRECT_MODE} = 1;

  if ( $self->{ThereAreOutlines} ) {
    print $OUTPUT
     "[/PageMode /UseOutlines " .
     "/Page 1 /View [/XYZ null null null] " .
     "/DOCVIEW pdfmark \n"
    ;
  }
  $self->{ThereAreOutlines} = 0;
  
  $self->Trailer();

  close($OUTPUT); $self->{OUTPUT} = undef;

  $self->{DIRECT_MODE} = 0;

  my ($atFile, $FromPage, $atPage) = @{$self}{qw(atFile FromPage atPage)};
  i::logit("END PDF Build - atFile: $atFile FromPage: $FromPage atPage: $atPage");

  my $ToPage = ($self->{Closing}) ? $atPage : $atPage-1;
#  my $ToPage = $atPage-1;

  print $PAGEXREF "File=$atFile From=$FromPage To=$ToPage\n";
  
  my ($basedir, $afpdir) = getConfValues(qw(basedir afpdir)); 
  my  $EXIT_ON_MISMATCH = ( getConfValues(qw(EXIT_ON_MISMATCH)) =~ /^([YNyn])$/i? $1 : 'Y');
  
  $afpdir = "$basedir/afp" if !$afpdir;
  $basedir =~ s/\//\\/g; $afpdir =~ s/\//\\/g;

  (my $psdir = File::Basename::dirname($self->{OutFileName})) =~ s/\//\\/g;
  (my $OutFileName = $self->{OutFileName}) =~ s/\//\\/g;
  (my $PdfFileName = $self->{PdfFileName}) =~ s/\//\\/g;
  $main::veryverbose && i::logit("out filename --- $OutFileName ");
  my $DistRetCode = -1;
  
  croak("PROCESS ERROR - NO DISTILLER INPUT FILENAME FOUND !!") unless $OutFileName; 
  croak("PROCESS ERROR - DISTILLER INPUT FILE \"$OutFileName\" NOT FOUND !!") unless -e $OutFileName; 
  my $jrname = $self->{jr}->getValues('JobReportName');
  my $jrid = $self->{jr}->getValues('JobReportId');
  
  #TESTSAN  - force ghostscript
  #$XReport::cfg->{distiller}->{use}="ghostscript"                                   ;
  #$XReport::cfg->{distiller}->{acroexec}=""                                         ;
  #$XReport::cfg->{distiller}->{acroargs}=""                                         ;
  #$XReport::cfg->{distiller}->{gsexec}="//localhost/ghostscript\$/bin/gswin64.exe"  ;
  #$XReport::cfg->{distiller}->{gsargs}="-dUsePrologue -dSTRICT"                     ;
  #$XReport::cfg->{distiller}->{gsargsDEBUG}="-dUsePrologue -dSTRICT -ZdDmnI -dDEBUG";
  #$XReport::cfg->{distiller}->{mode}="_NO_IN_PS"  ;
  #&$logrRtn("TESTSAN - force ghostscript_\$XReport::cfg->{distiller}=".Dumper($XReport::cfg->{distiller}));
  #
		  
  my $distillery = 'XReport::'.($XReport::cfg->{distiller}->{use} || 'Distillr');
  &$logrRtn("DISTILLER ENVIROMENT SETUP for $jrname($jrid) DISTILLERY: $distillery PSDIR: $psdir BASEDIR: $basedir");
  my $file2pdf = $distillery->can('FileToPdf');
  croak("PROCESS ERROR - FileToPdf method not in $distillery") unless $file2pdf; 
  my $origin = $basedir;
  $origin = $ENV{XREPORT_HOME} unless -e "$basedir\\bin\\xreport.ps";
  my $lcl_xreport_ps = "$psdir\\xreport.ps";
  unless ( -e $lcl_xreport_ps ) {
  	  my $infile = "$origin\\bin\\${distillery}_init.ps";
  	  $infile = "$origin\\bin\\xreport.ps" unless (-e $infile);
      &$logrRtn("COPYING \"$infile\" FILE into init (xreport.ps) file");
      File::Copy::cp($infile, $lcl_xreport_ps);
  }
  croak("PROCESS ERROR - DISTILLER CONTROL FILE \"$lcl_xreport_ps\" NOT FOUND !!") unless -e $lcl_xreport_ps; 

  &$logrRtn("COPYING DIST FILEs FROM $origin TO PSDIR: $psdir");
  my $lcl_initafp_ps = "$psdir\\xreport.ps";
  File::Copy::cp("$origin\\bin\\initafp.ps", $lcl_initafp_ps) unless -e $lcl_initafp_ps;

  (my $xreportstartfile = $OutFileName) =~ s/\\\Q$jrname\E\./\\\$\$xreportstartfile\$\$\./;
#  my $xreportstartfile = "$psdir\\\$\$xreportstartfile\$\$.$jrid.ps";
  unless ( -e $xreportstartfile ) {
    my $OUTPUT = gensym();

    i::warnit("BEGIN WRITING process parameters to $xreportstartfile");
    open($OUTPUT, ">$xreportstartfile") or croak("ApplicationError: $xreportstartfile open error $!\n");

    $self->writeXreportStart($OUTPUT);

    close($OUTPUT);
    $OUTPUT = undef;
    i::warnit("END WRITING process parameters to $xreportstartfile");
  }  
  $OUTPUT = gensym();

  my $psout = "$psdir\\\$\$convert.$jrid.ps";
  open($OUTPUT, ">$psout") or croak("ApplicationError: afpdir $psout open error $!\n");

  print $OUTPUT join("\n", map { (my $xx = $_) =~ s/\\/\//g; $xx } ( 
     "%true () startjob pop"
    ,"/AfpDir ($afpdir\\) def"
    ,"/LocalResources ($psdir/localres/) def"
    ,"($psdir\\xreport.ps) run" 
    ,"/a.fontpaths [ LocalResources"
	,"AfpDir (fonts/userfonts/charsets.outline/) concatenate"
	,"AfpDir (fonts/userfonts/charsets.bitmap/) concatenate"
	,"AfpDir (fonts/userfonts/) concatenate"
	,"AfpDir (fonts/charsets/charsets.outline/) concatenate"
	,"AfpDir (fonts/charsets/charsets.bitmap/) concatenate"
	,"AfpDir (fonts/charsets/) concatenate"
	,"] def" 
    ,"($xreportstartfile) run" )
   ), "\n"
  ;
  
  my $mode = ($XReport::cfg->{distiller}->{mode} or '_UNDEF_');
  
  if( $mode eq "NO_IN_PS" )
  {
	  print $OUTPUT join("\n", map { (my $xx = $_) =~ s/\\/\//g; $xx } ( 
		"($OutFileName) run" )
   ), "\n"
  ;
  }

  close($OUTPUT);
  
  &$logrRtn("DISTILLER CONVERSION BEGIN for $OutFileName to produce $PdfFileName using $distillery");
  $DistRetCode = &$file2pdf($OutFileName, $PdfFileName, $psout);
  &$logrRtn("DISTILLER CONVERSION END -RC: $DistRetCode from $distillery");

  croak("UserError: Distiller Error DETECTED $DistRetCode !!") if ( $DistRetCode != 0 ); 

  &$logrRtn("DISTILLER: Pages Produced: $main::distllr_stats->{PageCount}->[0]") 
                         if $main::distllr_stats && exists($main::distllr_stats->{PageCount});
  
  require XReport::PDF::DOC; 
  my $doc = XReport::PDF::DOC->Open($PdfFileName); 

  my ($tPgmPages, $tPdfPages) = (($ToPage-$FromPage+1), $doc->TotPages()); 
  &$logrRtn(__PACKAGE__." PDF Produced $tPdfPages - Expecting $tPgmPages");
  
    
  #die "MISMATCH FOR NUMBER OF PAGES in $OutFileName: PGM=$tPgmPages PDF=$tPdfPages" if $tPdfPages != $tPgmPages;#TESTSAN
  die "MISMATCH FOR NUMBER OF PAGES in $OutFileName: PGM=$tPgmPages PDF=$tPdfPages" if (($EXIT_ON_MISMATCH =~ /^Y$/i) and ($tPdfPages != $tPgmPages) );
  &$logrRtn("MISMATCH FOR NUMBER OF PAGES in $OutFileName: PGM=$tPgmPages PDF=$tPdfPages") if $tPdfPages != $tPgmPages;#TESTSAN

  $doc->PrepareXrefFileLengths();
  $doc->Close();
  
  unlink $OutFileName or croak("UNLINK $OutFileName ERROR $!") if $atFile > 0;

  $self->{atFileSize} = 0; 
  $jr->setValues(PSHeaderDone => 0);
  $self->{totPage} = $tPdfPages;

  return $atFile;
}

sub BREAKABLE_PAGE {
  my ($self, $max_file_size) = (shift, shift || MAX_FILE_SIZE); 
  $main::debug && i::logit(__PACKAGE__." checking max pages limit $max_file_size at_page: $self->{atFileSize} caller:".join('::',(caller(1))[0,2]));
  
  if ( $self->{atFileSize} >= $max_file_size ) {
    return 1;
  }
  return 0;
}

sub BeginPage {
  my ($self, $page) = @_; my $OUTPUT = $self->{OUTPUT};

  $self->{atPage} += 1;
  $main::veryverbose && i::logit("Begin Page $self->{OUTPUT}");
  if ( !$self->{atFileSize} ) {
    $self->NewFile();
    $self->{atFileSize} = 0;
  }

  $self->Print("NewPage\n\n"); 
}

sub EndPage {
  my ($self, $page) = @_; my $OUTPUT = $self->{OUTPUT};
  $self->FlushPrintLines($OUTPUT);
#  print $OUTPUT @{$self->{Print}};
#  $main::veryverbose && i::logit("End page ".Dumper($OUTPUT)."\n -- \nEnd Page ".join ( "\n", @{$self->{Print}})); 
#  @{$self->{Print}} = ();

  print $OUTPUT "EndPage\n\n"; 
  $self->{atFileSize} += 1; 
  $self->{MaxPageLength} = $self->{pagelength} if $self->{MaxPageLength} < $self->{pagelength};
  $self->{pagelength} = 0; 
}

sub Open {
  my $self = shift; my $jr = $self->{jr}; my $fileName;
  
  $fileName = $self->{XrefFileName} = $jr->getFileName('FILXREF'); 
  
  open($self->{FILXREF}, ">$fileName") 
   or croak("FILXREF OPEN ERROR \"$fileName \"$!");

  $fileName = $self->{PageXrefFileName} = $jr->getFileName('PAGEXREF'); 
  
  open($self->{PAGEXREF}, ">$fileName") 
   or croak("PAGEXREF OPEN ERROR \"$fileName \"$!");

  $self->{atFile} = 0; $self->{atFileSize} = 0; $self->{atPage} = 0; 

  $self->{ThereAreOutlines} = 0;

  $self->{Print} = [];

#  $self->{AfpResources} = {}; ## mpezzi more afp resources 
  $self->{jr}->[3]->{AfpResources} = {}; ## mpezzi more afp resources 
}

sub Close {
  my $self = shift; $self->{Closing} = 1;

  $self->EndFile();
  
  CORE::close($self->{FILXREF}); CORE::close($self->{PAGEXREF}); $self->{Closing} = 0;
}

sub new {
  my ($className, $jr) = (shift, shift); my $self = {}; $self->{jr} = $jr;
  $jr->setValues(PSHeaderDone => 0);
  
#  INIT_Distiller() if !$Distiller_Initialized;
  
  @{$self}{qw(OUTPUT FILXREF PAGEXREF)} = ( gensym(), gensym(), gensym() );
  $self->{MaxPageLength} = 55;
  $self->{MaxOutLength} = 80;

  my @VarList = (
    "ReportFormat", "ElabFormat", "PageOrient", "LinesPerPage", "FitToPage", "CodePage",
    "FormDef", "PageDef", "cc", "Fcb", "Trc", "CharsDef", "PageSize", "FontSize",
	"t1Fonts", "LaserAdjust", "ReplaceDef", "XferStartTime"
  );
  for (@VarList) { $self->{$_} = $jr->getValues($_)};
  
  my $charsperline = $jr->getValues('CharsPerLine');
  my $maxll = $jr->getValues('MaxLineLength');
  $charsperline = $maxll if $charsperline > $maxll;
  $jr->setValues(CharsPerLine => $charsperline);

  $self->{CharsPerLine} = $charsperline;
  $self->{MaxLineLength} = $maxll;
  
  i::warnit("OUTPUT ATTRIBUTES MAXLL: $self->{MaxLineLength} CPL: $self->{CharsPerLine} LPP: $self->{LinesPerPage}");
  
  if (((my $controlFile = uc($jr->getValues('PrintControlFile')))) =~ s/.cntl$//i ne '' && (my $INPUT = gensym())) {
    # mpezzi added support for control file
    require XReport::AFP::EXTRACT;
    my $LocalResources = $jr->getFileName('LOCALRES');

    $LocalResources =~ s/\\/\//g;
    my $inresname = 'CNTL.'.$controlFile;
    XReport::AFP::EXTRACT::ToLocalResources($LocalResources, $self->{XferStartTime}, ($inresname));

    open($INPUT, "<". $LocalResources.'/'.$inresname);
    my (@lines, $line) = (); $line = '';
    while(!eof($INPUT)) {
      my $line_ = <$INPUT>; chomp $line_;
      next if $line_ =~ /^\s*$/;
      ($line .= substr($line_, 0, 71)) =~ s/\s+$//;
      if ($line =~ /\)$/ ) {
        push @lines, uc($line); $line = ''; 
      }
    }
    close($INPUT);
    
    for (@lines) {
      my ($larg, $lvalue) = $_ =~ /^\s*(\w+)\((.*)\)\s*$/; 
      if ($larg eq 'TRCTYPE' and $lvalue eq 'SNI') {
        $self->{'Trc'} = "Y";
      }
      elsif ($larg eq 'FORMDEF') {
        $self->{'FormDef'} =  "F1$lvalue"; 
        $jr->setValues('FormDef', "F1$lvalue", 'isAfp', 1);
      }
      elsif ($larg eq 'PAGEDEF') {
        $self->{'PageDef'} = "P1$lvalue";
        $jr->setValues('PageDef', "P1$lvalue");
      }
      elsif ($larg eq 'CHARS') {
        $self->{'CharsDef'} = join(",", map {"X0$_"} split(/\W+/, $lvalue));
      }
    }
  }

  if ( $jr->getValues('ReportFormat') == RF_POSTSCRIPT ) {
    bless $self, 'XReport::OUTPUT::IPostscript';
  }
  elsif ( $jr->getValues('isFullAfp') ) {
    bless $self, 'XReport::OUTPUT::FullAfp'; 
  }
  elsif ( $jr->getValues('isAfp') ) {
    bless $self, 'XReport::OUTPUT::PlainAfp';
  }
  elsif ( $jr->getValues('CodePage') ne '') {
    bless $self, 'XReport::OUTPUT::PlainCodePage';
  }
  else {
    bless $self, 'XReport::OUTPUT::PlainText';
    #bless $self, 'XReport::OUTPUT::AsciiAfp' if $self->{FormDef} || $self->{CharsDef};
    #bless $self, 'XReport::OUTPUT::PlainAfp' if $self->{FormDef} || $self->{CharsDef};
	bless $self, 'XReport::OUTPUT::PlainAfp' if $self->{FormDef} || ($self->{CharsDef} and ($self->{'CharsDef'} =~ /\w/));
  }
  i::logit("OUTPUT Handler is now ".ref($self));

## mpezzi more afp resource handling
##  @{$self}{qw(ThereAreOutlines Print AfpResources)} = (0, [], {}); return $self;
  @{$self}{qw(ThereAreOutlines Print)} = (0, []);  # , {}); 
  $self->{jr}->[3]->{AfpResources} = {};
  $main::debug && i::warnit(ref($self), " new INPUT: ", ref($self->{INPUT}), " OUTPUT: ", ref($self->{OUTPUT}));
  
#  print "OUTPUT TYPE is $self\n";

  @{$self}{qw(ThereAreOutlines Print AfpResources)} = (0, [], {});
  for my $rt ( qw(FormDef PageDef CharsDef) ) {
    if ( exists($self->{$rt}) && $self->{$rt} ) {
        $self->{jr}->[3]->{AfpResources}->{$self->{$rt}} = 1;
        $self->{AfpResources}->{$self->{$rt}} = 1;
    }
    
  }
 return $self;
}

#######################################################################
#
#######################################################################
package XReport::OUTPUT::PlainText;

use strict; use bytes;

use Carp;

our @ISA = qw(XReport::OUTPUT XReport::OUTPUT::Postscript);

sub PutPage {
  my ($self, $page ) = (shift, shift);

  for (@{$page->lineList()}) {
    $self->PutLine($_);
  }
}

sub PutLine {
  my ($self, $line) = (shift, shift);  
  my $OUTPUT = $self->{OUTPUT};
 
  my $lines = $line->AsciiValueList();

  if (@$lines == 1) { 
    $line = substr(shift @$lines, 1); #$line =~ s/([\(\)\\])/\\$1/g; 
#    $self->Print("($line) PutLine\n");
#    $self->Print("(40) () ($line) PutLine\n");
    $line =~ s/ +$//;
    $self->Print("(40) () <".unpack('H*', $line)."> PutLine\n");
  } 
  else {
    # compute the line bold part
    $line = substr($lines->[0], 1); $line =~ s/ +$//; my $boldLine = $line; my ($boldMask, $lmask) = ("", 0);
    for $line (@$lines) {
      $line = substr($line, 1); $line =~ s/ +$//; $boldLine ^= $line; 
      $boldLine =~ s/\x00/\xff/g; $boldLine =~ s/[^\xff]/\x00/g; 
      $boldLine &= $line; 
      $lmask = length($line) if length($line) > $lmask;
    }
    $boldLine =~ s/\x00/ /g; $boldLine =~ s/ +$//;

    $boldMask = $boldLine; $boldMask =~ s/ /\xff/g; $boldMask =~ s/[^\xff]/\x00/g; 
    if (length($boldMask) < $lmask) {
      $boldMask .= "\xff" x ($lmask - length($boldMask)); 
    }

    # print the non bold part
    $line = shift @$lines; $line &= $boldMask; $line =~ s/\x00/ /g; $line =~ s/ +$//;
 #   $line =~ s/([\(\)\\])/\\$1/g; $self->Print("($line) PutLine\n");
#    $line =~ s/([\(\)\\])/\\$1/g; $self->Print("(40) () ($line) PutLine\n");
    $self->Print("(40) () <".unpack('H*', $line)."> PutLine\n");
    while (@$lines) {
      $line = shift @$lines;  $line &= $boldMask; $line =~ s/\x00/ /g; $line =~ s/ +$//; 
      if ($line ne "") { 
#        $line =~ s/([\(\)\\])/\\$1/g; $self->Print("($line) OverLine\n")
        $self->Print("<".unpack('H*', $line)."> OverLine\n")
      } 
    }

    # print the bold part
    if ($boldLine ne "") {
#      $boldLine =~ s/([\(\)\\])/\\$1/g; $self->Print("($boldLine) BoldOverLine\n");
      $self->Print("<".unpack('H*', $boldLine)."> BoldOverLine\n");
    }
  } 
}

sub atPage { return shift->{atPage}; }

#######################################################################
#
#######################################################################
package XReport::OUTPUT::PlainCodePage;

use strict; use bytes;

our @ISA = qw(XReport::OUTPUT XReport::OUTPUT::PlainText);

sub PutLine {
  my ($self, $line) = (shift, shift);  my $OUTPUT = $self->{OUTPUT};

  $line = substr($line->Value(), 1); 
    
  $self->Print("<", unpack("H*",$line), "> PutLine\n");
}


#######################################################################
#
#######################################################################
package XReport::OUTPUT::PlainAfp;

use strict; use bytes;

use Carp;
use XReport::Util;
use Convert::IBM390 qw(:all);

our @ISA = qw(XReport::OUTPUT XReport::OUTPUT::PlainText);

sub PutPage {
  my ($self, $page ) = (shift, shift);

  for (@{$page->lineList()}) {
    $self->PutLine($_);
  }
}

sub rtrim {
  $_[0] =~ s/\s+$//;
  return $_[0];
}

sub rtrimz {
  $_[0] =~ s/[\s\000]+$//;
  return $_[0];
}

sub ltrim {
  $_[0] =~ s/^\s+//;
  return $_[0];
}

sub QuotePsString {
  my $t = shift;  $t =~ s/\s/ /sg; $t =~ s/([(\)\{\}\r\n\f\x00-\x1f])/\\$1/sg; $t;
}

sub PutAsComment {
  # my ($self, $t) = (shift, shift); $t = $translator->toascii($t) if 1; $self->Print("\%".QuotePsString($t)."\n");
}

sub showPtocaSeqs {
  my ($self, $cseqs, $atX, $atY, $XEXP, $YEXP) = @_; $XEXP ||= 1; $YEXP ||= 1; $self->Print("a.init.ptocaseqs\n");
  
  my ($flg, $fc, $fl, $f, $fd, $fm, $toright) = (0, 0, 0, 0, 0, "", 0);
  
  while (length($cseqs)>0) {
    if (($f % 2) == 0) {
      ($flg, $fc, $fl, $f) = unpack("CCCC", $cseqs);
      $cseqs = substr($cseqs, 2);
    }
    else {
      ($fl, $f) = unpack("CC", $cseqs);
    }
    last if $fl<2;
	
    $fd = substr($cseqs,2,$fl-2); $fm = pack("C", $f - $f % 2);
	
	if ( $toright > 0 ) {
	  if ( $fm =~ /[\xc6\xd8]/ ) {
		$toright = 0;
	  }
	  elsif ( $fm !~ /[\xd2\xd4\xee\xd0\xf0]/ ) {
		$self->Print("ILNI $toright mul a.rmi\n"); $toright = 0;
	  }
	}
    
    if ($fm eq "\xda") { # Transparent Data 		
	  $fd =~ s/\x00/\x40/g; $self->PutAsComment($fd); 
      $self->Print("<".unpack("H*",$fd)."> a.trn\n") if $fd ne '';
    }
    elsif ($fm eq "\xd2") { # Absolute Move Baseline
      $atY = unpackeb("s", $fd)*$YEXP;
      $self->Print("$atY a.amb\n");
    }
    elsif ($fm eq "\xc6") { # Absolute Move Inline
      $atX = unpackeb("s", $fd)*$XEXP;
      $self->Print("$atX a.ami\n");
    }
    elsif ($fm eq "\xd8") { # Begin Line
      $atX = 0; $atY += 15; #todo: better atY increment
      $self->Print("a.bln\n"); 
    }
    elsif ($fm eq "\xf2") { # Begin Suppression
      $self->Print("/".unpack("C", $fd)." a.bsu\n");
    }
    elsif ($fm eq "\xe6") { # Draw B-axis Rule DBR
      #my @t = unpackeb("s2m1", $fd);
	  my ($fdnums,  $mus) = unpack('a4 C', $fd);
	  my @t = (unpackeb('s2', $fdnums), $mus);
      if ($t[0] > 0) {
        $t[1] += $t[2] /= 256 ;
      }
      else {
        $t[1] -= $t[2] /= 256 ;
      }
      $#t = 1;
      $t[0]*=$YEXP; $t[1]*=$XEXP;
      $self->Print(join("  ", @t)." a.dbr\n");
    }
    elsif ($fm eq "\xe4") { # Draw I-axis Rule DIR
      #my @t = unpackeb("s2m1", $fd);
	  my ($fdnums,  $mus) = unpack('a4 C', $fd);
	  my @t = (unpackeb('s2', $fdnums), $mus);
      if ($t[0] > 0) {
        $t[1] += $t[2] /= 256 ;
      }
      else {
        $t[1] -= $t[2] /= 256 ;
      }
      $#t = 1;
      $t[0]*=$XEXP; $t[1]*=$YEXP;
      $self->Print(join("  ", @t)." a.dir\n");
    }
    elsif ($fm eq "\xf4") { # End Suppression
      $self->Print("/".unpack("C", $fd)." a.esu\n");
    }
    elsif ($fm eq "\xf8") { # No Operation
    }
    elsif ($fm eq "\x72") { # Overstrike
      $self->Print(">> a.ovs\n");
    }
    elsif ($fm eq "\xd4") { # Relative Move Baseline
      my $off = unpackeb("s", $fd)*$YEXP;
      $self->Print("$off a.rmb\n");
      $atY += $off;
    }
    elsif ($fm eq "\xc8") { # Relative Move Inline
      my $off = unpackeb("s", $fd)*$XEXP;
      $self->Print("$off a.rmi\n");
      $atX += $off;
    }
    elsif ($fm eq "\xee") { # Repeat String 
      my ($ll, $t) = unpack("na*", $fd); $t = "\x40" if $t eq '';

      if ( length($t) == 1 ) {
        $t = $t x $ll;
      }
	  else {
        while(length($t) < $ll) {
          $t .= $t; 
        }
        $t = substr($t,0,$ll);
      }

	  if ( $t !~ /^\x40+$/ ) {
        if ( $toright > 0 ) {
		  $self->Print("ILNI $toright mul a.rmi\n"); $toright = 0;
        }
		PutAsComment($t); $self->Print("<".unpack("H*", $t)."> a.trn\n");
	  }
	  else {
		$toright += length($t);
	  }
    }
    elsif ($fm eq "\xd0") { # Set Baseline Increment
      my $bi = unpackeb("s", $fd);
      $self->Print($bi*$YEXP." a.sbi\n");
    }
    elsif ($fm eq "\xf0") { # Set Coded Font Local
      my $FontIdx = unpack("C", $fd);
      $self->Print("F$FontIdx a.setfont\n");
    }
    elsif ($fm eq "\x80") { # Set Extended Text Color
	  my @sec = unpack("H2H2H8C4H8", $fd);
	  if ( $sec[1] eq '40' ) {
		$self->Print("($sec[7]) (01) a.stc\n");
	  }
	  else {
        $self->Print(">> a.sec\n");
	  }
    }
    elsif ($fm eq "\xc2") { # Set Intercharacter Adjustement
      $self->Print(">> a.sia\n");
    }
    elsif ($fm eq "\xc0") { # Set Inline Margin
      $self->Print((unpackeb("s", $fd)*$XEXP)." a.sim\n");
    }
    elsif ($fm eq "\x74") { # Set Text Color
      my @stc = unpack("H4H2", $fd);
      $self->Print("($stc[0]) ($stc[1]) a.stc\n");
    }
    elsif ($fm eq "\xf6") { # Set Text Orientation
      my @sto = unpack("H4H4", $fd);
      my $TextOr = "($sto[0]) ($sto[1])";
      $self->Print("$TextOr a.sto\n");
    }
    elsif ($fm eq "\xc4") { # Set Variable Space Character Increment
      $self->Print((unpackeb("s", $fd)*$XEXP)." a.svi\n");
    }
    elsif ($fm eq "\x78") { # Temporary Baseline Movement
      $self->Print(">> a.tbm\n");
    }
    elsif ($fm eq "\x76") { # Underscore
      $self->Print(unpack("C", $fd)." a.usc\n");
    }
    else {
      $self->Print(">> a.??? ".unpack("H*", $fm.$fd));
    }
    $cseqs = substr($cseqs, $fl);
  }

  $self->Print("a.terminate.ptocaseqs \%OUTPUT.pm\n");
}

sub showPtocaSeqs_ {
  my ($self, $cseqs) = (shift, shift); my $OUTPUT = $self->{OUTPUT};

  my ($flg, $fc, $fl, $f, $fd, $fm) = (0, 0, 0, 0, 0, "");

  while (length($cseqs)>0) {
	if (($f % 2) == 0) {
	  ($flg, $fc, $fl, $f) = unpack("CCCC", $cseqs);
	  $cseqs = substr($cseqs, 2);
	}
	else {
	  ($fl, $f) = unpack("CC", $cseqs);
	}
	last if $fl<2;
	$fd = substr($cseqs,2,$fl-2);
	$fm = $f - $f % 2;
	if ($fm == 0xda) { # Transparent Data
      # $self->Print("%(", $translator->toascii($fd), ") a.trn\n");
      $self->Print("<", unpack("H*", $fd), "> a.trn\n") if $fd ne "";
	}
	elsif ($fm == 0xd2) { # Absolute Move Baseline
	  $self->Print(join("  ", unpackeb("s", $fd)), " a.amb\n");
	}
	elsif ($fm == 0xc6) { # Absolute Move Inline
	  $self->Print(join("  ", unpackeb("s", $fd)), " a.ami\n");
	}
	elsif ($fm == 0xd8) { # Begin Line
	  $self->Print("a.bln\n");
	}
	elsif ($fm == 0xf2) { # Begin Suppression
	  $self->Print(">> a.bsu\n");
	}
	elsif ($fm == 0xe6) { # Draw B-axis Rule DBR
#	  my @t = unpackeb("s2m1", $fd);
	  my ($fdnums,  $mus) = unpack('a4 C', $fd);
	  my @t = (unpackeb('s2', $fdnums), $mus);
	  $t[0] -= 65536 if $t[0] > 32767;
	  if ($t[0] > 0) {
	    $t[1] += $t[2] /= 256 ;
	  }
	  else {
	    $t[1] -= $t[2] /= 256 ;
	  }
	  $t[1] -= 65536 if $t[1] > 32767;
	  $#t = 1;
	  $self->Print(join("  ", @t), " a.dbr\n");
	}
	elsif ($fm == 0xe4) { # Draw I-axis Rule DIR
	  #my @t = unpackeb("s2m1", $fd);
	  my ($fdnums,  $mus) = unpack('a4 C', $fd);
	  my @t = (unpackeb('s2', $fdnums), $mus);
	  $t[0] -= 65536 if $t[0] > 32767;
	  if ($t[0] > 0) {
	    $t[1] += $t[2] /= 256 ;
	  }
	  else {
	    $t[1] -= $t[2] /= 256 ;
	  }
	  $t[1] -= 65536 if $t[1] > 32767;
	  $#t = 1;
	  $self->Print(join("  ", @t), " a.dir\n");
	}
	elsif ($fm == 0xf4) { # End Suppression
	  $self->Print(">> a.esu\n");
	}
	elsif ($fm == 0xf8) { # No Operation
	  print OUTPUT "NOP  ", join("  ", unpack("H*", $fd)), "\n";
	}
	elsif ($fm == 0x72) { # Overstrike
	  $self->Print(">> a.ovs\n");
	}
	elsif ($fm == 0xd4) { # Relative Move Baseline
	  my $off = unpackeb("s", $fd);
	  $off -= 65536 if $off > 32767;
	  $self->Print("$off a.rmb\n");
	}
	elsif ($fm == 0xc8) { # Relative Move Inline
	  my $off = unpackeb("s", $fd);
	  $off -= 65536 if $off > 32767;
	  $self->Print("$off a.rmi\n");
	}
	elsif ($fm == 0xee) { # Repeat String
	  $self->Print(">> a.rps\n");
	}
	elsif ($fm == 0xd0) { # Set Baseline Increment
	  my $bi = unpackeb("s", $fd);
	  $bi -= 65536 if $bi > 32767;
	  $self->Print("$bi a.sbi\n");
	}
	elsif ($fm == 0xf0) { # Set Coded Font Local
	  $self->Print("F", unpack("C", $fd), " a.setfont\n");
	}
	elsif ($fm == 0x80) { # Set Extended Text Color
	  $self->Print(">> a.sec\n");
	}
	elsif ($fm == 0xc2) { # Set Intercharacter Adjustement
	  $self->Print(">> a.sia\n");
	}
	elsif ($fm == 0xc0) { # Set Inline Margin
	  $self->Print(">> a.sim\n");
	}
	elsif ($fm == 0x74) { # Set Text Color
	  $self->Print(">> a.stc\n");
	}
	elsif ($fm == 0xf6) { # Set Text Orientation
	  my @sto = unpack("H4H4", $fd);
	  my $TextOr = "($sto[0]) ($sto[1])";
	  $self->Print("\n$TextOr a.sto\n");
	}
	elsif ($fm == 0xc4) { # Set Variable Space Character Increment
	  $self->Print(join("  ", unpackeb("s", $fd)), " a.svi\n");
	}
	elsif ($fm == 0x78) { # Temporary Baseline Movement
	  $self->Print(">> a.tbm\n");
	}
	elsif ($fm == 0x76) { # Underscore
	  $self->Print(">> a.usc\n");
	}
	else {
	  $self->Print(">> a.??? $fl ", unpack("H*", substr($cseqs,0,$fl+2)), "\n");
	}
	$cseqs = substr($cseqs, $fl);
  }
}

sub PutLine5A {
  my ($self, $line) = (shift, shift) ; my $OUTPUT = $self->{OUTPUT};
  
  my $trp = unpack("H6",substr($line,0,3));
  my $trd = substr($line,6);

  if ($trp eq "d3abcc") { # Invoke Medium Map (IMM)
    if ( $trd ne "" ) {
      my $mm = $translator->toascii(substr($trd,0,8));
      $mm =~ s/ +$//;
      $self->Print("($mm) a.setMediumMap\n"); $self->{'LastMediumMap'} = $mm;
    }
  }
  elsif ($trp eq "d3abca") { # Invoke Data Map (IDM)
    if ( $trd ne "" ) {
      my $dm = $translator->toascii(substr($trd,0,8));
      $dm =~ s/ +$//;
      $self->Print("($dm) a.setDataMap\n"); $self->{'LastDataMap'} = $dm;
    }
  }
  elsif ($trp eq "d3afd8") { # Include Page Overlay (IPO)

    #my @IPO = unpackeb("e8m3m3H4c*",$trd);
    my @IPO = unpackeb("e8xsxsH4c*",$trd);
    # my ($rnam, $iponums, $ipotail) = unpackeb('a8a6a*', $trd);
    # my @IPO = ( unpackeb('e*', $rnam)
	          # , ( map { my $f = (substr($_, 0, 1) x 3).$_; unpack('s>', substr($f, -4) ) } unpack('x8a3a3', $iponums) )
			  # , unpackeb('H4c*', $ipotail)
			  # );

    my $po = $IPO[0]; $po =~ s/ +$//;
    $self->Print("\%$po  << BEGIN (INCLUDE PAGE OVERLAY)\n");
    $self->Print("$IPO[1] $IPO[2] ($po) a.IncOverlay\n");
    $self->Print("\%$po  << END\n");
##	$self->{AfpResources}->{$po} = 1; ## mpezzi more afp resources
	$self->{jr}->[3]->{AfpResources}->{$po} = 1; ## mpezzi more afp resources 

  }
  elsif ($trp eq "d3af5f") { # Include Page Segment (IPS)

    #my @IPS = unpackeb("e8m3m3c*",$trd);
    my @IPS = unpackeb("e8xsxsc*",$trd);
    # my ($rnam, $iponums, $ipotail) = unpackeb('a8a6a*', $trd);
    # my @IPO = ( unpackeb('e*', $rnam)
	          # , ( map { my $f = (substr($_, 0, 1) x 3).$_; unpack('s>', substr($f, -4) ) } unpack('x8a3a3', $iponums) )
			  # , unpackeb('c*', $ipotail)
			  # );

    my $ps = $IPS[0]; $ps =~ s/ +$//;
    $self->Print("\%$ps  << BEGIN (INCLUDE PAGE SEGMENT)\n");
    $self->Print("$IPS[1] $IPS[2] ($ps) a.IncPSegment\n");
    $self->Print("\%$ps  << END\n");
#	$self->{AfpResources}->{$ps} = 1; ## mpezzi more afp resources 
	$self->{jr}->[3]->{AfpResources}->{$ps} = 1; ## mpezzi more afp resources 

  }
  elsif ($trp eq "d3ee9b") { # Presentation Text Data
	$self->showPtocaSeqs($trd, 0, 0, 1, 1);
  }
  elsif ($trp eq "d3eeee") { # NOP
  }
  else {
    $self->Print("\%INVALID AFP CMD: $trp ". unpack("H*", $trd). "\n");
    &$logrRtn("UserError DETECTED: INVALID AFP COMMAND for MIXED STREAMS $trp ". unpack("H*", $trd));
  }
}

sub PutLine {
  my ($self, $line) = (shift, shift);
  $line = $line->Value();
  $main::veryverbose && i::warnit(ref($self)." PutLine TRC: $self->{Trc} LINEDATA: ".unpack('H40', $line));
  my $OUTPUT = $self->{OUTPUT};

  if ( $self->{Trc} =~ /^[1Y]$/i ) {
    my ($cc, $trc, $lineData) = unpack("H2Ca*", $line); 
    if ( $cc ne "5a" ) {
	  #$self->Print("%", $translator->toascii($lineData), "\n"); #change newlines
#      $self->Print("($cc) (F".(($trc & 63) + 1).") <", unpack("H*",$lineData), "> PutLine\n");
      $self->Print("(40) (F".(($trc & 63) + 1).") <", unpack("H*",$lineData), "> PutLine\n");
    }
    else {
	  $self->PutLine5A( substr($line,3) );
    }
  }
  else {
    my ($cc, $lineData) = unpack("H2a*", $line);
    if ( $cc ne "5a" ) {
	    #$self->Print("%", $translator->toascii($lineData), "\n"); #change newlines
	 # $self->Print("%", $lineData, "\n"); #change newlines
#      $self->Print("($cc) () <", unpack("H*",$lineData), "> PutLine\n");
      $self->Print("(40) () <", unpack("H*",$lineData), "> PutLine\n");
    }
    else {
	  $self->PutLine5A( substr($line,3) );
    }
  }
}

#######################################################################
#
#######################################################################
package XReport::OUTPUT::AsciiAfp;

use strict; use bytes;

use Carp;
use XReport::Util;
use Convert::IBM390 qw(:all);

our @ISA = qw(XReport::OUTPUT XReport::OUTPUT::PlainText);

sub PutPage {
  my ($self, $page ) = (shift, shift);
  
  #$self->PutLine("a.init.ptocaseqs\n");
  for (@{$page->lineList()}) {
    $self->PutLine($_);
  }
  #$self->PutLine("a.terminate.ptocaseqs\n");
}

sub rtrim {
  $_[0] =~ s/\s+$//;
  return $_[0];
}

sub rtrimz {
  $_[0] =~ s/[\s\000]+$//;
  return $_[0];
}

sub ltrim {
  $_[0] =~ s/^\s+//;
  return $_[0];
}

sub QuotePsString {
  my $t = shift;  $t =~ s/\s/ /sg; $t =~ s/([(\)\{\}\r\n\f\x00-\x1f])/\\$1/sg; $t;
}

sub PutLine {
  my ($self, $line) = (shift, shift);  my $OUTPUT = $self->{OUTPUT};
#print "caller::", join('', caller()), "line: ", ref($line), "\n"; 
  my $lines = $line->AsciiValueList();

  if (@$lines == 1) { 
	  # $self->Print("%", $lines->[0], "\n"); #change newlines
      my ($cc, $outline ) = unpack("H2H*",$translator->toebcdic($lines->[0]));
      #$outline = substr($outline, 2) if $outline;
      $self->Print(sprintf("(%s) () <%s> PutLine\n", ($cc, $outline)));
  } 
  else {
    # compute the line bold part
    $line = substr($lines->[0], 1); $line =~ s/ +$//; my $boldLine = $line; my ($boldMask, $lmask) = ("", 0);
    for $line (@$lines) {
      $line = substr($line, 1); $line =~ s/ +$//; $boldLine ^= $line; 
      $boldLine =~ s/\x00/\xff/g; $boldLine =~ s/[^\xff]/\x00/g; 
      $boldLine &= $line; 
      $lmask = length($line) if length($line) > $lmask;
    }
    $boldLine =~ s/\x00/ /g; $boldLine =~ s/ +$//;

    $boldMask = $boldLine; $boldMask =~ s/ /\xff/g; $boldMask =~ s/[^\xff]/\x00/g; 
    if (length($boldMask) < $lmask) {
      $boldMask .= "\xff" x ($lmask - length($boldMask)); 
    }

    # print the non bold part
    $line = shift @$lines; $line &= $boldMask; $line =~ s/\x00/ /g; $line =~ s/ +$//;
	  # $self->Print("%", $line, "\n"); #change newlines
      $self->Print(sprintf("(40) () <%s%s> PutLine\n", unpack("H2H*",$translator->toebcdic($line))));
    while (@$lines) {
      $line = shift @$lines;  $line &= $boldMask; $line =~ s/\x00/ /g; $line =~ s/ +$//; 
      if ($line ne "") { 
	  # $self->Print("%", $line, "\n"); #change newlines
      $self->Print(sprintf("(4e) () <%s%s> OverLine\n", unpack("H2H*",$translator->toebcdic($line))));
      } 
    }

    # print the bold part
    if ($boldLine ne "") {
	  # $self->Print("%", $line, "\n"); #change newlines
      $self->Print(sprintf("(4e) () <%s%s> BoldOverLine\n", unpack("H2H*",$translator->toebcdic($line))));
    }
  } 
}

#######################################################################
#
#######################################################################
package XReport::OUTPUT::FullAfp;

use strict; use bytes;

use Carp;

our @ISA = qw(XReport::OUTPUT::Postscript XReport::OUTPUT::PlainText);

sub PutPage {
}

sub PutLine {
}

sub EndPage {
  my ($self, $page) = (shift, shift); my $OUTPUT = $self->{OUTPUT};
    $main::veryverbose && i::warnit("FullAfp::EndPage called by ".join('::', (caller())[0,2])
                                   ." Lines: ".scalar(@{$page->{PsLines}}));
    my $jr = $self->{jr};
    my $pspgexit = $jr->getValues('PSPageExit');
    if (scalar(@{$page->{PsLines}})) {
       if ($pspgexit && (my $rtn = $pspgexit->can('Print')) ) {
          $self->Print(join("\n", @{&$rtn($pspgexit, $page->{PsLines})}), "\n");
       }
       else {
          $self->Print(join("\n", @{$page->{PsLines}}), "\n");
       } 
    }  
#  $self->Print(join("\n", @{$page->{PsLines}}), "\n");
  $self->SUPER::EndPage($page, @_);
}

#######################################################################
#
#######################################################################
package XReport::OUTPUT::IPostscript;

use strict; use bytes;

use Carp;

our @ISA = qw(XReport::OUTPUT::Postscript XReport::OUTPUT::PlainText);

sub PutPage {
}

sub PutLine {
}

sub EndPage {
  my ($self, $page) = (shift, shift); my $OUTPUT = $self->{OUTPUT};
    $main::veryverbose && i::warnit("IPostscript::EndPage called by ".join('::', (caller())[0,2])
                                   ." Lines: ".scalar(@{$page->{PsLines}}));
    my $jr = $self->{jr};
    my $pspgexit = $jr->getValues('PSPageExit');
    if (scalar(@{$self->{Print}})) {
       if ($pspgexit && (my $rtn = $pspgexit->can('Print')) ) {
          $self->Print(join("\n", @{&$rtn($pspgexit, $page->{PsLines})}), "\n");
       }
       else {
          $self->Print(join("\n", @{$page->{PsLines}}), "\n");
       } 
    }  
#  $self->Print(join("\n", @{$page->{PsLines}}), "\n");
  $self->SUPER::EndPage($page, @_);
}

#######################################################################
#
#######################################################################
package XReport::OUTPUT::DATA;

use strict; use bytes;

our @ISA = qw(XReport::OUTPUT);

use Carp; 
use Symbol;

use constant MAX_FILE_SIZE => 1500;

sub NewFile {
  my $self = shift; my $OUTPUT = $self->{OUTPUT}; my $jr = $self->{jr};

  my $lFile = $self->{atFile} || 0;

  if ( $self->{atFileSize} > 0 ) {
    $self->EndFile();
  }
  $self->{atFile} += 1 if $self->{atPage} > 1; $self->{atFileSize} = 0.1; 

  $self->{PageOffsets} = [];
  
  my $fileName = $self->{OutFileName} = $jr->getFileName('DATAOUT', $self->{atFile});
 
  open($OUTPUT, ">$fileName") 
    or 
  croak("OUTPUT OPEN ERROR \"$fileName \"$!"); binmode($OUTPUT); $self->Header();

  push @{$self->{PageOffsets}}, tell($OUTPUT) if $self->{atFile} > 0;

  $self->{FromPage} = $self->{atPage};

  return $lFile;
}

sub EndFile {
  my $self = shift ; return undef if !($self->{atFileSize} >= 1); 
  
  my ($OUTPUT, $PAGEXREF) = @{$self}{qw(OUTPUT PAGEXREF)};

  my ($atFile, $FromPage, $atPage) = @{$self}{qw(atFile FromPage atPage)};

  my $ToPage = ($self->{Closing}) ? $atPage : $atPage-1;

  if ( $self->{PageOffsets}->[-1] != tell($OUTPUT) ) {
    die "OFFSET MISMATCH $self->{PageOffsets}->[-1] ". tell($OUTPUT);
  }

  print $OUTPUT "xref", pack("NN", $FromPage, $atPage);
  for my $offset (@{$self->{PageOffsets}}) {
	print $OUTPUT pack("N",$offset);
  } 
  print $OUTPUT "%EOF";

  $self->{PageOffsets} = [];
  $self->{atFileSize} = 0;

  my $PAGEXREF = $self->{PAGEXREF};

  print $PAGEXREF "File=$atFile From=$FromPage To=$ToPage\n";

  return $atFile;
}

sub BREAKABLE_PAGE {
  my ($self, $max_file_size) = (shift, shift || MAX_FILE_SIZE);   
  if ( $self->{atFileSize}/1536 >= $max_file_size ) {
    return 1;
  }
  return 0;
}

sub BeginPage {
  my ($self, $page) = @_; my $OUTPUT = $self->{OUTPUT};

  $self->{atPage} += 1;

  if ( !$self->{atFileSize} ) {
    $self->NewFile();
  }
  
  push @{$self->{PageOffsets}}, tell($OUTPUT);
}

sub EndPage {
  my $self = shift; my $OUTPUT = $self->{OUTPUT};

  print $OUTPUT $self->{buffer};

  $self->{atFileSize} += length($self->{buffer}); $self->{buffer} = "";
}

sub PutPage {
  my ($self, $page ) = (shift, shift); my $OUTPUT = $self->{OUTPUT}; 

  for (@{$page->lineList()}) {
    my $line = $_->AsciiValue();

    $self->{buffer} .= pack("n",length($line)) . $line;
  }
}

sub PutLine {
  my ($self, $line) = (shift, shift); my $OUTPUT = $self->{OUTPUT};
  
  $line = $line->AsciiValue();
  
  $self->{buffer} .= pack("n",length($line)). $line;
}

sub Header {
}

sub Trailer {
}

sub InsertOutlineTarget {
}

sub EndCreatePhysicalFiles {
}

sub Open {
  my $self = shift; my $jr = $self->{jr}; my $fileName;
  
  $fileName = $self->{XrefFileName} = $jr->getFileName('FILXREF'); 
  
  open($self->{FILXREF}, ">$fileName") 
   or croak("FILXREF OPEN ERROR \"$fileName \"$!");

  $fileName = $self->{PageXrefFileName} = $jr->getFileName('PAGEXREF'); 
  
  open($self->{PAGEXREF}, ">$fileName") 
   or croak("PAGEXREF OPEN ERROR \"$fileName \"$!");

  $self->{Closing} = 0;

  $self->{atFile} = 0; $self->{atFileSize} = 0;
  $self->{atPage} = 0; $self->{PageOffsets} = [];
}

sub Close {
  my $self = shift; my $OUTPUT = $self->{OUTPUT}; $self->{Closing} = 1;

  push @{$self->{PageOffsets}}, tell($OUTPUT);
  
  $self->EndFile();
  
  CORE::close($self->{FILXREF}); CORE::close($self->{PAGEXREF}); $self->{Closing} = 0;
}

sub new {
  my ($className, $jr) = (shift, shift);

  my $self = {}; 

  $self->{jr} = $jr;
  
  $self->{OUTPUT} = gensym(); 
  $self->{FILXREF} = gensym();
  $self->{PageOffsets} = [];

  $self->{atFile} = 0; $self->{atFileSize} = 0;
  $self->{atPage} = 0; $self->{PageOffsets} = [];

  my @VarList = (
    "ReportFormat", "ElabFormat", "PageOrient", "CharsPerLine", "LinesPerPage", "FitToPage", "CodePage",
    "FormDef", "PageDef", "cc", "Fcb", "Trc", "CharsDef", "PageSize", "FontSize",
	"t1Fonts", "LaserAdjust", "ReplaceDef", "XferStartTime"
  );
  for (@VarList) { $self->{$_} = $jr->getValues($_)};
  
  bless $self, $className;
}

#######################################################################
#
#######################################################################
package XReport::OUTPUT;

use strict; use bytes;

use Carp;
use Symbol;

use constant EF_PRINT => 1;
use constant EF_DATA_ORIG => 2;
use constant EF_DATA_ASCII => 3;

sub totPages {
  my $self = shift;
  return $self->{totPage};
}

sub DESTROY {
  $main::debug && i::warnit("DESTROY OUTPUT", " called by ", join('@', (caller())[1,2]));
}

sub new {
  my ($className, $jr) = (shift, shift);
  $main::debug and i::warnit("Initializing new instance of $className");
  my $ElabFormat = $jr->getValues('ElabFormat');

  my $self;
  
  if ( $ElabFormat eq EF_PRINT ) {
    $self = new XReport::OUTPUT::Postscript($jr);
  }
  elsif ( $ElabFormat eq EF_DATA_ORIG or $ElabFormat eq EF_DATA_ASCII) {
    $self = new XReport::OUTPUT::DATA($jr);
  }
  else {
    croak("ApplicationError: OUTPUT FORMAT INVALID <$ElabFormat>.");
  }
  $main::debug and i::warnit("OUTPUT HANDLED by " . ref($self));
  return $self;
}

1;
