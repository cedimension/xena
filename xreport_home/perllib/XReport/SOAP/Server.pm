package XReport::SOAP::Server;

use Carp;
require Exporter;
use base "XReport::SOAP";


##############################################################################
# Define some constants
#

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

@ISA               = qw(Exporter);
@EXPORT            = qw();
@EXPORT_OK         = qw();
$VERSION           = '1.00';

use XML::Simple;

use constant WSDLHDR => '<?xml version="1.0" encoding="UTF-8"?>'
  . '<wsdl:definitions name="XRSOAPSERVERNAME"'
  . ' targetNamespace="http://cereport.org/WebService"' 
  . ' xmlns:apachesoap="http://xml.apache.org/xml-soap"'
  . ' xmlns:impl="http://cereport.org/WebService"'
  . ' xmlns:intf="http://cereport.org/WebService"'
  . ' xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"'
  . ' xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"'
  . ' xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/"'
  . ' xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
  . '  <wsdl:types>'
  . '  <xsd:schema'
  . '   elementFormDefault="qualified"'
  . '   targetNamespace="http://cereport.org/WebService"'
  . '   xmlns="http://www.w3.org/2001/XMLSchema"'
  . '   xmlns:apachesoap="http://xml.apache.org/xml-soap"'
  . '   xmlns:impl="http://cereport.org/WebService"'
  . '   xmlns:intf="http://cereport.org/WebService"'
  . '   xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">'
#  . '  <xsd:complexType name="column">'
#  . '	  <xsd:annotation>'
#  . '	    <xsd:documentation>Column Attributes</xsd:documentation>'
#  . '	  </xsd:annotation>'
#  . '	  <xsd:sequence>'
#  . '      <xsd:element maxOccurs="unbounded" minOccurs="0" name="ValuesArray" type="xsd:string"/>'
#  . '      <xsd:element maxOccurs="1" minOccurs="0" name="dummy4axis" type="xsd:string"/>'
#  . '	  </xsd:sequence>'
#  . '	  <xsd:attribute name="colname" type="xsd:string"/>'
#  . '	  <xsd:attribute name="operation" type="xsd:string"/>'
#  . '	  <xsd:attribute name="Value" type="xsd:string"/>'
#  . '	</xsd:complexType>'
  . '   <xsd:complexType name="column">'
  . '	  <xsd:annotation>'
  . '	    <xsd:documentation>Column Attrbutes</xsd:documentation>'
  . '	  </xsd:annotation>'
  . '	  <xsd:simpleContent>'
  . '	    <xsd:extension base="xsd:string">'
  . '	      <xsd:attribute name="colname" type="xsd:string"/>'
  . '	      <xsd:attribute name="operation" type="xsd:string"/>'
  . '         <xsd:attribute name="Min" type="xsd:string"/>'
  . '         <xsd:attribute name="Max" type="xsd:string"/>'
  . '       </xsd:extension>'
  . '     </xsd:simpleContent>'
  . '   </xsd:complexType>'
  . '   <xsd:complexType name="IndexEntry">'
  . '     <xsd:annotation>'
  . '       <xsd:documentation>Definisce parte o tutto un documento. Viene indicata la parte di documento da attribuire alle chiavi specificate in termini di pagina di partenza (FromPage) e numero di pagine consecutive (ForPages). Nell\'array "Columns" vengono specificate le colonne di indici pertinenti al documento ed il loro valore. In caso di interrogazione deve essere valorizzato solo l\'array "Columns" da cui viene costruita la where clause per la query. In caso di inserimento devono essere specificate anche le pagine pertinenti alle chiavi specificate</xsd:documentation>'
  . '     </xsd:annotation>'
  . '     <xsd:sequence>'
  . '       <xsd:element maxOccurs="unbounded" minOccurs="0" name="Columns" type="impl:column"/>'
  . '       <xsd:element maxOccurs="1" minOccurs="0" name="dummy4axis" type="xsd:string"/>'
  . '     </xsd:sequence>'
  . '     <xsd:attribute name="JobReportId" type="xsd:string"/>'
  . '     <xsd:attribute name="FromPage"    type="xsd:string"/>'
  . '     <xsd:attribute name="ForPages"    type="xsd:string"/>'
  . '   </xsd:complexType>'
  . '   <xsd:complexType name="DocumentData">'
  . '     <xsd:annotation>'
  . '       <xsd:documentation>Document Data Structure. Viene usata sia nelle richieste che nelle risposte. Contiene i seguenti campi: IndexName - Nome simbolico dell\'indice da accedere; DocumentType - Typo documento da trattare (PDF per il momento); temporary - non so; TOP - numero massimo di righe indice da ritornare alla richiesta; ORDERBY - statement da utilizzare per l\'ordinamento delle righe indice; IndexEntries - array di righe indice da trattare; DocumentBody - rappresentazione in BASE64 del contenuto del documento</xsd:documentation>'
  . '     </xsd:annotation>'
  . '     <xsd:sequence>'
  . '       <xsd:element maxOccurs="unbounded" minOccurs="0" name="IndexEntries" type="impl:IndexEntry"/>'
  . '       <xsd:element maxOccurs="1" minOccurs="0" name="NewEntries" type="impl:IndexEntry"/>'
  . '       <xsd:element maxOccurs="1" minOccurs="0" name="DocumentBody" type="xsd:base64Binary"/>'
  . '     </xsd:sequence>'
  . '     <xsd:attribute name="IndexName" type="xsd:string"/>'
  . '     <xsd:attribute name="DocumentType" type="xsd:string"/>'
  . '     <xsd:attribute name="FileName" type="xsd:string"/>'
  . '     <xsd:attribute name="temporary" default="false" type="xsd:boolean"/>'
  . '     <xsd:attribute name="TOP"       type="xsd:string"/>'
  . '     <xsd:attribute name="ORDERBY"   type="xsd:string"/>'
  . '     <xsd:attribute name="DISTINCT"  type="xsd:string"/>'
  . '     <xsd:attribute name="ReportId"  type="xsd:string"/>'
  . '     <xsd:attribute name="FileId"  type="xsd:string"/>'
  . '   </xsd:complexType>'
  . '   <xsd:complexType final="extension" name="FaultData">'
  . '    <xsd:annotation>'
  . '     <xsd:documentation>Fault reporting structure</xsd:documentation>'
  . '    </xsd:annotation>'
  . '    <xsd:sequence>'
  . '     <xsd:element maxOccurs="1" minOccurs="0" name="faultcode" type="xsd:string" />'
  . '     <xsd:element maxOccurs="1" minOccurs="0" name="faultstring" type="xsd:string" />'
  . '     <xsd:element maxOccurs="1" minOccurs="0" name="faultactor" type="xsd:anyURI" />'
  . '     <xsd:element maxOccurs="1" minOccurs="0" name="detail">'
  . '      <xsd:complexType>'
  . '       <xsd:sequence>'
  . '        <xsd:element maxOccurs="1" minOccurs="0" name="message" type="xsd:string" />'
  . '        <xsd:element maxOccurs="1" minOccurs="0" name="description" type="xsd:string" />'
  . '        <xsd:element maxOccurs="1" minOccurs="0" name="errorId" type="xsd:string" />'
  . '       </xsd:sequence>'
  . '      </xsd:complexType>'
  . '     </xsd:element>'
  . '    </xsd:sequence>'
  . '   </xsd:complexType>'
  . '   <element type="impl:DocumentData" name="REQUEST" />'
  . '   <element type="impl:DocumentData" name="RESPONSE" />'
  . '   <element type="impl:FaultData" name="FAULT" />'
  . ' </xsd:schema>'
  . '</wsdl:types>'
  . '<wsdl:message name="RequestMsg"><wsdl:part element="intf:REQUEST" name="REQUEST"/></wsdl:message>'
  . '<wsdl:message name="ResponseMsg"><wsdl:part element="intf:RESPONSE" name="RESPONSE"/></wsdl:message>'
  . '<wsdl:message name="FaultMsg"><wsdl:part element="intf:FAULT" name="FAULT"/></wsdl:message>'
  ;

use constant WSDLTAIL => ''
  . '  <wsdl:service name="XRSOAPSERVERNAME">'
  . '    <wsdl:port name="XRSOAPSERVERNAMEPort" binding="intf:XRSOAPSERVERNAMEBinding" >'
  . '	   <soap:address location="HTTP://127.0.0.1:8085/CeWebService/XRSOAPSERVERNAME.asp" />'
  . '    </wsdl:port>'
  . '  </wsdl:service>'
  . '</wsdl:definitions>'
  ;

sub _Dumper { return XReport::SOAP::Dumper(@_); }

sub _build_soap_error {
	my $class = shift;
	$class = ref($class) if ref($class);
	my $args = { @_ }; 
	$args->{faultactor} = join('/', $main::Request->ServerVariables(qw(URL HTTP_SOAPACTION))) 
	      if (!exists($args->{faultactor}) && $main::Request && $main::Request->can('ServerVariables'));
	$args->{faultactor} = $class.'/'.(caller())[0] unless exists($args->{faultactor});
	      
	my $respmsg = '<?xml version="1.0" encoding="UTF-8"?>'
         . '<SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema/instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">'
         . '<SOAP-ENV:Body>'
         . '<SOAP-ENV:Fault>'
         . "<faultcode>$args->{faultcode}</faultcode>"
         . "<faultactor>$args->{faultactor}</faultactor>"
         . "<faultstring>$args->{faultstring}</faultstring>"
         ;
   if ( exists($args->{detailmsg}) || exists($args->{detailcode}) || exists($args->{detaildesc}) ) {
   	  $respmsg .= '<detail>';
   	  $respmsg .= '<message><![CDATA['.$args->{detailmsg}.']]></message>' if exists($args->{detailmsg});
      $respmsg .= '<description><![CDATA['.$args->{detaildesc}.']]></description>' if exists($args->{detaildesc});
      $respmsg .= '<errorId>'.$args->{detailcode}.'</errorId>' if exists($args->{detailcode});
      $respmsg .= '</detail>';
   }
   $respmsg .= '</SOAP-ENV:Fault>'
         . '</SOAP-ENV:Body>'
         . '</SOAP-ENV:Envelope>'
         ;
   i::logit("_SOAP_ERROR: ", $respmsg);
   return $respmsg;
#  $main::Response->{Status} = "$args->{faultcode} $args->{faultstring}";
#  $main::Response->AddHeader("Connection", "close");
#  $main::Response->Write( $respmsg );
#  $main::Response->Flush();
#  return undef;
}

sub import {
  # Handle the :strict tag
  $main::thisservice = (grep /^#[^\s]+$/, @_)[0];
  @_ = grep(!/^#[^\s]+$/, @_);
  if ( $main::thisservice ) {
    my $servername = substr($main::thisservice, 1);
    $main::thisservice = $servername.'.pm';
    my $class = require $main::thisservice;
    $class = 'XReport::'.$servername if $class =~ /\d+/;
    (my $wsdl = WSDLHDR . WSDLdeclaremethods( methods($class, 'private') ) . WSDLTAIL ) =~ s/\s+/ /sg;
    $wsdl =~ s/\> \</\>\</sg; 
    $wsdl =~ s/XRSOAPSERVERNAME/$servername/isg;
    my $classrtn = "package $class; sub WSDL { "
                . " my \$serverpath = shift; "
                . " my \$wsdl = q($wsdl); "
                . " \$wsdl =~ s/<soap:address\\s+location=\".*\"\\s*\\/>/<soap:address location=\"\$serverpath\" \\/>/im; " 
                . " return \$wsdl; "
                . "}\n";
#    $classrtn .= "sub _BUILD_SOAP_ERROR { return XReport\:\:SOAP\:\:Server\:\:_build_soap_error(\"$class\", \@_); }\n";
    $classrtn .= "sub _Dumper { return XReport\:\:SOAP\:\:Dumper(\@_); }\n";
    $classrtn .= "'$class';\n";
                
    eval "$classrtn";
    my $outmsg = $@;
    warn "EVAL Result: $@\n" if ($main::veryverbose && $@);
  }
  goto &Exporter::import;
}

sub WSDLdeclaremethods {

   return  '<wsdl:portType name="XRSOAPSERVERNAMEMethods">'
          . join('', map { '<wsdl:operation name="'.$_.'" >'
                         . '<wsdl:input name="'.$_.'Input" message="impl:RequestMsg" />'
                         . '<wsdl:output name="'.$_.'Output" message="impl:ResponseMsg" />'
                         . '<wsdl:fault name="'.$_.'Fault" message="impl:FaultMsg" />'
                         . '</wsdl:operation>' } @_ ) 
          . '</wsdl:portType>'
          . '<wsdl:binding name="XRSOAPSERVERNAMEBinding" type="impl:XRSOAPSERVERNAMEMethods" >'
          . '<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http" />'
          . join('', map { '<wsdl:operation name="'.$_.'" >'
                         . '<soap:operation soapAction="'.$main::Application->{ApplName}.'#'.$_.'" />'
                         . '<wsdl:input name="'.$_.'Input" ><soap:body use="literal" /></wsdl:input>'       
                         . '<wsdl:output name="'.$_.'Output" ><soap:body use="literal" /></wsdl:output>'       
                         . '<wsdl:fault name="'.$_.'Fault" ><soap:fault use="literal" name="'.$_.'Fault" /></wsdl:fault>'
                         . '</wsdl:operation>' } @_ )
           . '</wsdl:binding>';       

}

sub buildWSDL {
 my $serverpath = shift;
 my $servername = shift;
# my $methods = [ methods("XReport\::$servername", 'private') ];

 my $classname = 'XReport::'.$servername;
 my $methods = $main::Application->{SOAPMethods};
 if ( !ref($methods) ) {
    $methods = [ grep { $_ !~ /^[\(_]/ && $classname->can($_) } keys %{$classname.'::'} ];
    $main::Application->{SOAPMethods} = $methods;
 }
 $main::debug && i::logit("$servername methods: ", @$methods);
 (my $wsdl = WSDLHDR . WSDLdeclaremethods( @$methods, @_ ) . WSDLTAIL) 
               =~ s/<soap:address\s+location=".*"\s*\/>/<soap:address location="$serverpath" \/>/im;  
 my $wsdlspace = lc($servername);
 $wsdl =~ s/XRSOAPSERVERNAME/$wsdlspace/isg;
 $wsdl =~ s/\s+/ /sg;
 $wsdl =~ s/\> \</\>\</sg;
 return $wsdl;
}

sub buildCond {
      my ($var, $min, $max, $typ) = (shift, shift, shift, shift);
      # insert date format validation 'yyyy-mm-gg hh:mm:ss'
#      i::logit("KEY: $var $typ MIN:$min(".defined($min).") MAX:$max(".defined($max).") caller " .join('::',(caller())[0,2]) );
      $min = "'$min'" unless !defined($min); #(!defined($min) or $typ =~ /^N$/i);
      $max = "'$max'" unless !defined($max); #(!defined($max) or $typ =~ /^N$/i);

      return " [$var] IS NULL " if !defined($min) && !defined($max);

      return " [$var] <= $max " if !defined($min);

      return " [$var] >= $min " if !defined($max);

      return " [$var] = 0 "     if ( "$min" eq "0" and "$max" eq "0");

      return " [$var] = '' "    if ( "$min" eq "''" and "$max" eq "''");

      return " [$var] = $min "  if ( $min eq $max );

      if ($min =~ /^'{0,1}LIKE'{0,1}$/) {
	return " [$var] LIKE $max " unless $typ =~ /^N$/i;
	i::logit( "LIKE not supported for numeric column $var ");
	die "LIKE not supported for numeric column $var ";
      }

      if ($min =~ /^'{0,1}IN'{0,1}$/) {
	my $qt = ($typ =~ /^N$/i ? '' : "'");
	my $regex = ($typ =~ /^N$/i ? qr/[\x09\s]+/ : qr/\s*\x09\s*/);
	return "  [$var] IN (" . join($qt.', '.$qt, split(/$regex/, $max)) . ") "; 	
      }

      return " [$var] BETWEEN $min AND $max ";
}

sub buildClause {
  my ($tbl, $IndexEntries, $prefix) = (shift, shift, shift);

#  my $idxcoltypes = {split / /, $main::Application->{'XRIDX.'.$tbl.'ColTypes'} };
#  i::logit("columns $tbl:", Dumper($idxcoltypes));

  my @or;
  $main::debug && i::logit("buildClause request: ", _Dumper($IndexEntries));
  foreach my $row ( @{$IndexEntries} ) {
    my @and; 
    if ( $row->{JobReportId} ) {
      $row->{Columns}->{JobReportId}->{content} = $row->{JobReportId} unless (exists($row->{Columns}->{JobReportId}));
    }
    foreach my $var ( keys %{$row->{Columns}} ) {
#      return EXIT_SOAP_ERROR("Sender", "IDXAccess", "Variable $var Richiesta non in tabella $tbl") unless exists $idxcoltypes->{$var};
      my $typ = ''; #$idxcoltypes->{$var};
      my $qt = ( $typ eq 'N' ? '' : "'" );
      my ($min, $max, $content) = @{ $row->{Columns}->{$var} }{qw(Min Max content)};
#      i::logit("ForEach: Checking now $var for MIN:$min(".defined($min).") MAX:$max(".defined($max).") -$content-");
      if (!defined($content) && !defined($min) && !defined($max) ) {
	push @and, " [$prefix$var] IS NULL "
      }
      elsif ( !defined($content) ) {
	eval { push @and, buildCond($prefix.$var, $min, $max, '')}; # $idxcoltypes->{$var}) };
      }
      elsif (!defined($min) && !defined($max) ) {
	push @and, " [$prefix$var] = ${qt}${content}${qt} "
      }
      else {
	eval { push @and, buildCond($prefix.$var, $min, (defined($max) ? $max : $content), '') }; #$idxcoltypes->{$var}) };
      }
#      return EXIT_SOAP_ERROR("Sender", "QuerySyntax", $@) if $@;
      die $@ if $@;
    }
    push @or, '(' .join(') AND (', @and) . ')' if scalar(@and);
  }
  return @or;
}



use MIME::Base64 ; 
sub fillIndexEntries_withBytesStream {
  my $dbr = shift;
#  i::logit("filling IndexEntries from " . join('::', (caller())[0,2]). " " . ref($dbr));

  my @IndexEntries;
  i::logit("fillIndexEntries_withBytesStream no rows in RESULTSET") if $dbr->eof();

  while (!$dbr->eof()) {
    my $fields = $dbr->Fields();
    my $IndexEntry = { };
    for my $fldn (0..$fields->Count()-1) {
      my $ftype = $fields->Item($fldn)->Type();
      next if ($ftype == 205 || $ftype == 128);
      my $var = $fields->Item($fldn)->Name();
      my $val = $fields->{$var}->Value();
#      print "fillIndexEntries_withBytesStream: $var => ".(ref($val) && $val->can('Date') ? $val->Date('yyyy-MM-dd').' '.$val->Time('hh:mm:ss tt') : $val || '')."\n";
      if ( $var =~ /^(?:JobReportId|FromPage|ForPages)$/i ) {
         $IndexEntry->{$var} = ($val || $val == 0 ? $val : '');
      }
      else {
    $val = ($val->can('Date') ? $val->Date('yyyy-MM-dd') : '')." ".($val->can('Time') ? $val->Time('HH:mm:ss tt') : '') if ref($val);
	if ( $var =~ /^(?:textString)$/i ) {
		$val = MIME::Base64::encode_base64($val) ;
	}
	
	
	push @{$IndexEntry->{Columns}}, { colname => $var, content => $val || '', };
      }
    }
    push @IndexEntries, $IndexEntry;
    $dbr->MoveNext();
  }
#  i::logit("fillIndexEntries_withBytesStream IndexEntries: ", Dumper(\@IndexEntries)) if scalar(@IndexEntries);

  return [ @IndexEntries ];
}

sub fillIndexEntries {
  my $dbr = shift;
#  i::logit("filling IndexEntries from " . join('::', (caller())[0,2]). " " . ref($dbr));

  my @IndexEntries;
  i::logit("fillIndexEntries no rows in RESULTSET") if $dbr->eof();

  while (!$dbr->eof()) {
    my $fields = $dbr->Fields();
    my $IndexEntry = { };
    for my $fldn (0..$fields->Count()-1) {
      my $ftype = $fields->Item($fldn)->Type();
      next if ($ftype == 205 || $ftype == 128);
      my $var = $fields->Item($fldn)->Name();
      my $val = $fields->{$var}->Value();
#      print "fillIndexEntry: $var => ".(ref($val) && $val->can('Date') ? $val->Date('yyyy-MM-dd').' '.$val->Time('hh:mm:ss tt') : $val || '')."\n";
      if ( $var =~ /^(?:JobReportId|FromPage|ForPages)$/i ) {
         $IndexEntry->{$var} = ($val || $val == 0 ? $val : '');
      }
      else {
    $val = ($val->can('Date') ? $val->Date('yyyy-MM-dd') : '')." ".($val->can('Time') ? $val->Time('HH:mm:ss tt') : '') if ref($val);
	push @{$IndexEntry->{Columns}}, { colname => $var, content => $val || '', };
      }
    }
    push @IndexEntries, $IndexEntry;
    $dbr->MoveNext();
  }
#  i::logit("fillIndexEntries IndexEntries: ", Dumper(\@IndexEntries)) if scalar(@IndexEntries);

  return [ @IndexEntries ];
}

#sub _HTTP_Server {
#  use HTTP::Daemon;
#  use HTTP::Status;

#  my ($listenaddr, $listenport) = (shift, shift);
#  $listenaddr = c::getValues('ListenAddr') unless $listenaddr;
#  $listenport = c::getValues('ListenPort') unless $listenport;
#  $listenaddr = $ENV{COMPUTERNAME} unless $listenaddr;
#  $listenport = 60080 unless $listenport;

#  my $thisserver = HTTP::Daemon
#    ->new (LocalAddr => $listenaddr, LocalPort => $listenport, Reuse => 1)
#      ;

#  $main::WSDL =  XReport::SOAP::buildWSDL($thisserver->url(), $main::Application->{ApplName} );
#  print "Contact to SOAP server at ", $thisserver->url(), "\n";
  
  
#  while (my $c = $thisserver->accept) {
#    while (my $r = $c->get_request) {
#      (my $getreq = $r->url()) =~ s/^.*\?(.*)$/$1/;
#      print "Processing client request url: ", $thisserver->url(), " method: ", $r->method, " req: $getreq\n";
#      my $resp = '';
#      my $soapaction = $r->header('SOAPAction');
#      my $httpreqid = $r->header('REQUEST_ID');
#      if ($r->method eq 'GET' && $getreq =~ /^WSDL$/i ) {
#	$resp = HTTP::Response->new(RC_OK);
#	#      $resp->content_type('text/xml');
#	$resp->content($main::WSDL);
#	#     $c->send_response($resp);
#      }
#      elsif ($r->method eq 'POST') {
#	$resp = HTTP::Response->new(RC_BAD_REQUEST) unless $soapaction;
#      my ($pkgref, $method) = ($r->header('SOAPAction') =~ /^("?)(.*)[#\/\@](.*)\1$/)[1,2];
#	$resp = HTTP::Response->new(RC_BAD_REQUEST) if $pkgref && $pkgref ne 'XReport::'.$main::Application->{ApplName};
	
	
#	my $class = 'XReport::'.$main::Application->{ApplName};
#	my $msub = $class->can($method);
#	print "SOAP Action: ", $r->header('SOAPAction'), " Data: ", length($r->content), " pkgref: $pkgref method: $method sub: ", ref($msub), "\n";
#	$pkgref = 'printiface' unless $pkgref;
#	$resp = (ref($msub) eq 'CODE' 
#		 ? HTTP::Response->new(&$msub($method, $httpreqid, XReport::SOAP::parseSOAPreq($r->content())))
#		 : HTTP::Response->new(RC_NOT_IMPLEMENTED)
#		);
#      }
#      else {
#	$resp = HTTP::Response->new(RC_BAD_REQUEST);
#      }
#      print "sending response\n";
#      $resp->content_type('text/xml');
#      $c->send_response($resp);
#    }
#    UNIVERSAL::isa($c, 'shutdown') ? $c->shutdown(2) : $c->close(); 
#    undef $c;
#  }
  
#}

sub methods {
  my ($rclass, $types) = @_;
  $rclass = ref $rclass || $rclass;
  $types ||= '';
  my %classes_seen;
  my %methods;
  my @class = ($rclass);
  
  no strict 'refs';
  while (my $class = shift @class) {
    next if $classes_seen{$class}++;
    unshift @class, @{"${class}::ISA"} if $types eq 'all';
    # Based on methods_via() in perl5db.pl
    for my $method (grep {not /^[(_]/ and defined &{${"${class}::"}{$_}} && $_ ne 'can' } keys %{"${class}::"}) { 
    	$methods{$method} = wantarray ? undef : $class->can($method);
    }
  }
#  print "Methods supported by $rclass: ", join('::', keys %methods), "\n";  
  wantarray ? keys %methods : \%methods;
}

BEGIN {
  *XReport::SOAP::buildWSDL = *XReport::SOAP::Server::buildWSDL;
  *XReport::SOAP::buildClause = *XReport::SOAP::Server::buildClause;
  *XReport::SOAP::fillIndexEntries = *XReport::SOAP::Server::fillIndexEntries;
  *XReport::SOAP::fillIndexEntries_withBytesStream = *XReport::SOAP::Server::fillIndexEntries_withBytesStream;  
}

__PACKAGE__;
