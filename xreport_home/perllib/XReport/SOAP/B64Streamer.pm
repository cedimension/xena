package XReport::SOAP::B64Streamer;

use MIME::Base64;

use strict;

sub new {
  my $class = shift;
  my $self = { buffsize => 57*76, bufflen => 0, InputBytes => 0, OutputBytes => 0, buffer => ''};
  bless $self, $class;

  return $self;
}

sub binmode {

    return 1;
}

sub write {
  my ($self, $buffer) = (shift, shift);
  $self->{buffer} .= $buffer; 
  while ( length($self->{buffer}) > $self->{buffsize} ) {
    ($buffer, $self->{buffer}) = unpack("a".$self->{buffsize}." a*", $self->{buffer}) ;
    my $b64buff = MIME::Base64::encode_base64($buffer);
    $main::Response->Write($b64buff) ;
    $self->{InputBytes} += length($buffer);
    $self->{OutputBytes} += length($b64buff);
  }
}

sub tell {
 my $self = shift;
 return $self->{InputBytes} + length($self->{buffer});
}

sub close {
  my $self = shift;
  $self->write('');
  my $b64buff = MIME::Base64::encode_base64($self->{buffer});
  $main::Response->Write($b64buff) ;
  $self->{InputBytes} += length($self->{buffer});
  $self->{OutputBytes} += length($b64buff);
  i::traceit("Inputbytes: ", $self->{InputBytes}, " Outputbytes: ", $self->{OutputBytes}) if $main::dotrace;
#  return $main::Response->Flush();
}

sub print {
	 my $self = shift; return $self->write(join('', @_)); 
}

__PACKAGE__;