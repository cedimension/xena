package XReport::DBUtil;

use strict;
use DBI;

sub connect {
  my ($self, $dbid) = (shift, shift);
  $dbid = 'XREPORT' unless $dbid;
  $self = new(__PACKAGE__, $dbid) if !ref($self);

  my $cstring = $self->{cstring};
  $self->{dbh} = DBI->connect("dbi:ODBC:$cstring", undef, undef,
                  { RaiseError => 1, odbc_cursortype => 2}) ;
  return $self;
}

sub disconnect {
  return shift->{dbh}->disconnect;
}

sub new {
  my ($class, $dbid) = (shift, shift);
  $dbid = 'XREPORT' unless $dbid;
  die "$dbid db access not configured" unless exists($XReport::cfg->{odbcdb}->{$dbid});
  my $cstring = $XReport::cfg->{odbcdb}->{$dbid};
  my $self = { dbid => $dbid, cstring => $cstring }; 
  bless $self, $class;
  return $self;
}

sub dbExecute {
  my $self = shift;
  my ($sql, $selparms) = (shift, shift);
  $selparms = {} unless ref($selparms);
#  $selparms->{Slice} = {};

  i::logit("Invoking SELECT: $sql");
  @{$self->{dbh}}{keys %$selparms} = @{$selparms}{keys %$selparms};
  my $rowset = $self->{dbh}->selectall_arrayref($sql, {Slice => {}});
#  i::logit("Returned ROWSET: ". Dumper($rowset));
  return $rowset;
}

sub getTableRows {
  my $self = shift;
  my ($sql, $selparms, $idkeys) = (shift, shift, shift);
  $idkeys = [] unless defined $idkeys;
  $selparms = {} unless ref($selparms);

  i::logit("Invoking SELECT: $sql");
  @{$self->{dbh}}{keys %$selparms} = @{$selparms}{keys %$selparms};
  $self->{dbh}->{RaiseError} = 1; # do this, or check every call for errors
  my $sth = $self->{dbh}->prepare($sql);
  $sth->execute;
  my %row;
  my $rowset = [];
  $sth->bind_columns( \( @row{ @{$sth->{NAME} } } ));
  while ($sth->fetch) {
    my $Columns = [];
    while ( my ($col, $val) = each %row ) { 
      push @$Columns, { content => "$val", colname => "$col" }; 
    }
    my $id = {};
    $id = { map { $_ => $row{$_} } @$idkeys } if scalar(@$idkeys); 
    push @{$rowset}, { Columns => $Columns, %$id };
  }

#  my $rowset = $self->{dbh}->selectall_arrayref($sql, {Slice => {}});

  i::logit("Returned ROWSET: ". Dumper($rowset));
  return $rowset;
}

sub retrieveBlob {
  require Encode;

  my ($self, $sql, $fldname) = (shift, shift, shift);
  my (%row, $sth, $resource);
  $sql =~ s/^SELECT.*?\s(FROM\s.*)$/SELECT *, DataLength($fldname) as bloblength $1/;
  @{$self->{dbh}}{qw(LongReadLen LongTruncOk RaiseError)} = (0, 1, 1);
  $sth = $self->{dbh}->prepare($sql);
  $sth->execute();
  $sth->bind_columns( \( @row{ @{$sth->{NAME} } } ));
  $sth->fetch;
  $sth->finish;
  my $result = { IndexEntry => { %row } };
  $sql =~ s/^SELECT.*?\s(FROM\s.*)$/SELECT $fldname $1/;
  my $fldlen = $row{bloblength} + 1;
  @{$self->{dbh}}{qw(LongReadLen LongTruncOk)} = ($fldlen, 0);
  $sth = $self->{dbh}->prepare($sql);
  $sth->execute;
  $sth->bind_columns( \$resource );
  $sth->fetch;
  $sth->finish;
  $result->{DocumentBody}->{content} = $resource;
  return $result;
}

sub storeBlob {
#  require Encode;

  my ($self, $tbl, $fldname, $inrow) = (shift, shift, shift, shift);
  my (%row, $sth, $identity);
  my @cols = keys %$inrow;
  my $sql = "INSERT INTO $tbl ( "
    . join(',', @cols ) . " ) VALUES( "
    . join(',', ("?")x@cols ) . ")"
    . ";UPDATE $tbl SET XferEndTime = GETDATE() where IDENTITYCOL = SCOPE_IDENTITY()"
    . ";SELECT *, DataLength($fldname) as bloblength from $tbl where IDENTITYCOL = SCOPE_IDENTITY()";
  @{$self->{dbh}}{qw(LongReadLen LongTruncOk RaiseError)} = (0, 1, 1);
#  i::logit("STORE SQL: ". $sql);
  $sth = $self->{dbh}->prepare($sql);
#  $sth->bind_param(1, $blob, SQL_LONGVARBINARY);
  $sth->execute(@{$inrow}{@cols});
  $sth->bind_columns( \( @row{ @{$sth->{NAME} } } ));
  $sth->fetch;
  $sth->finish;
  my $result = { IndexEntry => { %row } };
#  i::logit(Dumper($result));
  return $result;
}

package Response;

sub new {
  my ($class, $Request) = (shift, shift);
  bless { response => $Request }, $class;
}

sub Write {
  my $self = shift;
  $self->{response}->content_type($self->{ContentType}) if exists($self->{ContentType});
  $self->{response}->print(@_);
}

sub AddHeader {
  my $self = shift;
  my ($hdr, $value) = (shift, shift);
  $self->{response}->headers_out->{$hdr} = $value;
  return $self;
}

sub Flush {
  1;
}

sub Clear {
  my $self = shift;
  $self->{response}->headers_out->clear();
  return $self;
}

package Request;

use base 'Apache2::RequestRec';

sub new {
  my ($class, $Request) = (shift, shift);
  bless { request => $Request }, $class;
}

sub BinaryRead {
  my $self = shift;
  my $bytes2read = shift;
  my $bytes = $self->{request}->read(my $buff, $bytes2read);
  return $buff;
}

sub TotalBytes {
  my $self = shift;
  return $self->{request}->headers_in->{"Content-Length"};
}

sub ServerVariables {
  my $self = shift; my $item = shift;
  if ( $item =~ /^ALL_RAW$/ ) {
    my @all_raw;
    while ( my ($var, $val) = each %{ $self->{request}->headers_in() }) {
      push @all_raw, '$var: $val';
    }
    return join("\n", @all_raw);
  }
  return $ENV{$item};
}

package XReport::SOAP::ApacheServer;

#use Apache2::Reload;
use mod_perl2;

use Apache2::RequestIO ();
use Apache2::RequestRec ();
use Apache2::RequestUtil ();
use Apache2::ServerUtil ();
use Apache2::PerlSections ();

use Apache2::Const -compile => qw(OK);

$XReport::ApacheStatus::VERSION = '4.00'; # mod_perl 2.0
our $VERSION = "3.00";

use CGI qw(:standard);

use constant IS_WIN32 => ($^O eq "MSWin32");

use lib ("$ENV{XREPORT_SITECONF}/userlib/perllib",
	 "$ENV{XREPORT_HOME}/bin/webservices");

use XReport ();
use XReport::SOAP::Server ('#'.$main::Application->{ApplName});

sub main::debug2log {
#  i::logit( "$main::ASPVER $main::Application->{ApplName}\> " . join('', @_));
  i::logit(join('', @_));
#  $main::dbgsock->send($main::Session->{SessionID} . " " . strftime("%Y-%m-%d %H:%M:%S.%U", localtime) . " $main::ASPVER $main::servername\> " . join('', @_), 0, DBGDEST) if $main::dbgsock;
  return 1;

#  return 1 unless -d $main::debugdir;

#  i::logit(join('', @_));

#  my $mplog = new FileHandle '>>'.$main::debugdir.'/service.log';
#  print $mplog $main::Session->{SessionID} . " " . localtime() . " $main::ASPVER $main::servername: " . join('', @_)."\n";
#  $mplog->close();
}

sub main::write2log {
  return main::debug2log(@_);

#  return main::debug2log(@_) if -d $main::debugdir;
#  return i::logit(join('', @_));

#  return 1 unless -d $main::debugdir;

#  my $mplog = new FileHandle '>>'.$main::debugdir.'/service.log';
#  print $mplog $main::Session->{SessionID} . " " . localtime() . " $main::ASPVER $main::servername: " . join('', @_)."\n";
#  $mplog->close();
}

use DBI;

#sub main::getDBC {
#  my $dbid = shift;
#  $dbid = 'XREPORT' unless $dbid;
#  die "$dbid db access not configured" unless exists($XReport::cfg->{odbcdb}->{$dbid});
#  my $cstring = $XReport::cfg->{odbcdb}->{$dbid};
#  return DBI->connect("dbi:ODBC:$cstring", undef, undef,
#                  { RaiseError => 1, odbc_cursortype => 2}) ;
#}

#sub main::dbExecute {
#  my ($sql, $dbid) = (shift, shift);
#  my $selparms = shift;
#  my ($selparms, $dbid) = ($dbid, 'XREPORT') if (ref($dbid) && !defined($selparms));
#  $dbid = 'XREPORT' unless $dbid;
#  $selparms = {} unless ref($selparms);
#  $selparms->{Slice} = {};
##  $selparms->{RaiseError} = 0;

#  die "$dbid db access not configured" unless exists($XReport::cfg->{odbcdb}->{$dbid});
#  my $cstring = $XReport::cfg->{odbcdb}->{$dbid};
#  i::logit("SELECT - SELPARMS:\n".Dumper($selparms)."\nSELECT:\n".$sql);
#  my $dbh = DBI->connect("dbi:ODBC:$cstring", undef, undef,
##                  { RaiseError => 1, odbc_cursortype => 2}) ;
#                  { LongReadLen => 32*1024*1024, LongTruncOk => 1, RaiseError => 1, odbc_cursortype => 2}) ;
##  @{$dbh}{keys %$selparms} = @{$selparms}{keys %$selparms};
##  my $rowset = $dbh->selectall_arrayref($sql, {Slice => {}});
#  my $rowset = $dbh->selectall_arrayref($sql, $selparms);
#  i::logit("Returned ROWSET: ". Dumper($rowset));
#  $dbh->disconnect;
#  return $rowset;
#}

#sub main::get_ix_dbname {
#  my $IndexName = shift;
#  my $dbr = main::dbExecute("SELECT * from tbl_IndexTables WHERE IndexName = '$IndexName'",
#			    'XREPORT');
#  return $dbr->[0]->{'DatabaseName'};
#}

sub _GET_SHOWLOG {
  my @html = ();
  my $debug_fn = $main::debugdir.'/service.log';
  if ( -e $debug_fn ) {
    my $debug_fs = -s $debug_fn;
    push @html, "File: $debug_fn size: $debug_fs at ". localtime()."\n";
    (my $iis_logfh = new FileHandle("<$debug_fn")) || push @html, "ERROR ACCESSING \"$debug_fn\" - $!";
    if ( $iis_logfh ) {
      $iis_logfh->binmode();
      $iis_logfh->seek(-63000, 2);
      $iis_logfh->read(my $buff, 63000);
      $iis_logfh->close();
      $buff =~ s/[\s\x00]+$//gs;
      $buff = substr($buff, (length($buff) - 32760)) if length($buff) > 32760;
      #    $buff =~ s/.*(.{1,32760})$/$1/;
      push @html, $buff."\n";
    }
    push @html, localtime()."\n";
  }
  else {
    push @html, "Debug Log File \"$debug_fn\" for site $main::servername not found\n";
  }
  return print header(),start_html(),pre(join('',@html)),end_html();
}



sub main::SOAPError {
  $main::Response->AddHeader('Status', $_[3] || 400);
  $main::Response->{ContentType} = 'text/xml';
  $main::Response->Write(XReport::SOAP::BUILD_SOAP_ERROR(@_));
  $main::msgtolog = "error: " . $_[2];
}

sub main::readContent {
  my ($reqident, $method) = (shift, shift);
  my $buffsize = 32*1024;
  my $TotalBytes = $main::Request->TotalBytes();
  (my $RequestData, $main::ContentLength) = ('', 0);
  if ( $TotalBytes ) {
    while($TotalBytes > 0) {
      my $bytes2read = ($TotalBytes > $buffsize ? $buffsize : $TotalBytes);
      main::debug2log("${method} $reqident request: reading $bytes2read of $TotalBytes left");
      my $buff = $main::Request->BinaryRead($bytes2read);
#      my $bytes = $main::Request->read(my $buff, $bytes2read);
      $RequestData .= $buff if $buff;
      $main::ContentLength += length($buff) if $buff;

      $TotalBytes -= $bytes2read;
    }
  }
  else {
    while (1) {
      my $buff = $main::Request->BinaryRead($buffsize);
#      my $bytes = $main::Request->read(my $buff, $buffsize);
      $RequestData .= $buff if $buff;
      $main::ContentLength += length($buff) if $buff;
      last if !$buff || length($buff) < $buffsize;
    }
  }
  main::debug2log("${method} $reqident request: returning $main::ContentLength");
  return $RequestData;
}

sub main::splitSoapRequest {
  my ($method, $reqident) = (shift, shift);
  my ($buffsize, $databytes) = (32*1024, 0);
  my $TotalBytes = $main::Request->TotalBytes();
  my ($RequestData, $RequestDocument) = ('', '');
  my $buffref = \$RequestData;
  my $cache = '';
  my $dbhdr_re = qr/\<DocumentBody(?i:\sxsi\:type\=[^\s]+?)?\s*\>/;
  while( 1 ) {
    my $bytes2read = $buffsize;
    main::debug2log(ref($method) . " ${method} $reqident request: reading $bytes2read of $TotalBytes left");
    
    my $buffer = $main::Request->BinaryRead($bytes2read);
#    my $bytes = $main::Request->read(my $buffer, $bytes2read);
    $TotalBytes -= length($buffer) if $buffer;
    $cache .= $buffer;
    if ( $cache =~ /^(.*$dbhdr_re)(.*)$/is ) {
      $RequestData .= $1 . 'REFERTOIN';
      ($RequestDocument = $2) =~ s/[\r\n]//sg;
      main::debug2log("${method} $reqident request: switching to Document " );
      $buffref = \$RequestDocument;
      $cache = '';
      last;
    }
    elsif ( $cache =~ /^(.*)(\<\/DocumentBody\s*\>.*)$/is ) {
      $RequestData .= $2;;
      ($RequestDocument .= $1) =~ s/[\r\n]//sg;
      main::debug2log("${method} $reqident request: switching to Data " );
      $buffref = \$RequestData;
      $cache = '';
    }
    elsif  ( length($cache) > $buffsize || length($buffer) < $bytes2read) {
      $$buffref .= $cache;
      $cache = '';
    }

    main::debug2log("${method} $reqident request: re-looping "
		    . " data: ", length($RequestData). " bytes "
		    . " document: " . length($RequestDocument) . " bytes "
		    . " TotalBytes: " . $TotalBytes
		    . " cache: " . length($cache)
		    . " bytes2Read: " . $bytes2read
		    . " buffer: " . length($buffer)
#		    . "data:\n$buffer"
		   );
    last if length($buffer) < $bytes2read;
  }

  main::debug2log("${method} $reqident request: Received Document "
		  , " TotalBytes: " . $main::Request->TotalBytes()
		  , " Bytes Received: " . (length($RequestData) + length($RequestDocument))
		  , " data: ". length($RequestData). " bytes "
		  , " document: ". length($RequestDocument) . " bytes "
		 );
  return main::SOAPError("Receiver", "Receive", "Request contains no document ")
    unless ( $TotalBytes || length($RequestDocument) ) ;
  i::logit("RequestData:\n". $RequestData);
  i::logit("fixed RequestData:\n" . XReport::SOAP::closeXmlStruct($RequestData));
  my $usrreq = XReport::SOAP::parseSOAPreq( XReport::SOAP::closeXmlStruct($RequestData), 'REQUEST') 
    || return main::SOAPError("Receiver", "Receive", " Unable to find REQUEST element inside SOAP Envelope ");
  
  my ($tbl, $doctype, $rmtfile, $Recipient) = @{$usrreq->{request}}{qw(IndexName DocumentType FileName DocRecipient)};
  my $IndexEntry = (XReport::SOAP::polishEntries(delete($usrreq->{request}->{IndexEntries})))->[0];
  delete($usrreq->{request}->{NewEntries});
#  print "usrreq: ", Dumper($usrreq), "\n", "INDEXENTRIES: ", Dumper($IndexEntry);
  $Recipient = '' unless $Recipient;
  $doctype = 'xml' unless $doctype;

  my $ContentType = ($doctype =~ /^PDF/i ? 'Application-vnd/x-pdf'
		     : $doctype =~ /^(?:prj|xml|cfg)$/i ? 'text/xml'
		     : $doctype =~ /^(?:pl|pm)$/i ? 'text/xml'
		     : 'application/octet-stream');

  return main::SOAPError("Receiver", "QCreate", "No Document Type or Index Name specified") if ( !$doctype );
  my $docdata = '';
  my $ofh = new IO::String($docdata);
  binmode $ofh;

  while($TotalBytes > 0) {
    my $bytes2read = ($TotalBytes > $buffsize ? ($buffsize - length($RequestDocument)) : $TotalBytes);

#    my $bytes = $main::Request->read(my $buffer, $bytes2read);
    my $buffer = $main::Request->BinaryRead($bytes2read);
    $TotalBytes -= $bytes2read;

    if ( ($RequestDocument.$buffer) =~ /^(.*)(<\/DocumentBody\s*\>.*)$/is ) {
      ($RequestDocument = $1) =~ s/[\r\n]//gs; 
      $RequestData .= $2;
      last;
    }
    else {
      $RequestDocument .= $buffer;
    }
    $RequestDocument =~ s/[\r\n]//gs; 
    my @b64lines = unpack("(a76)*", $RequestDocument);
    
    $RequestDocument = ( length($b64lines[-1]) < 76 ? pop @b64lines : '') ;
    my $decoded_buffer = MIME::Base64::decode_base64(join('', @b64lines));
    my $wb = $ofh->syswrite($decoded_buffer, length($decoded_buffer));
    $databytes += $wb; 
  }

  if ($RequestDocument) {
    if ( $RequestDocument =~ /^(.*)(<\/DocumentBody\s*\>.*)$/is ) {
      ($RequestDocument = $1) =~ s/[\r\n]//gs;
      $RequestData .= $2;
    }
    my $lastbuff = MIME::Base64::decode_base64($RequestDocument);
    my $wb = $ofh->syswrite($lastbuff, length($lastbuff));
    $databytes += $wb;
  }
  $ofh->close();
  
  main::debug2log("${method} $reqident Input Request contains $databytes data bytes - Document is ". length($docdata). " bytes");

  return ({%{$usrreq->{request}}, IndexEntry => $IndexEntry, ContentType => $ContentType }, $docdata); 
}

sub handler {
#  die "Request ref: " .ref($_[0]);
  $main::Request = new Request($_[0]);
  $main::Server = $_[0]->server();
  $main::Response = new Response($_[0]);
  $main::servername = $main::Application->{ApplName};

  my ($reqident, $reqmet, $qrystr, 
      $httpaction, $conttype, $httpserverpath, $https, $SOAPAct, $remote_addr, $user_agent) =
    @{%ENV}{
      qw( HTTP_REQUEST_ID REQUEST_METHOD QUERY_STRING 
	 REQUEST_URI CONTENT_TYPE HTTP_HOST HTTPS HTTP_SOAPAction REMOTE_ADDR HTTP_USER_AGENT)};

  main::write2log("Processing request from $remote_addr using - $user_agent");
  my $class = 'XReport::'.$main::Application->{ApplName};
  #*{$class.'::SOAPError'} = &SOAPError;
  #*{$class.'::strftime'} = &XReport::SOAP::strftime;
  #*{$class.'::encode_base64'} = &XReport::SOAP::_encode_base64; 
  #*{$class.'::decode_base64'} = &XReport::SOAP::_decode_base64; 
 
  $reqident = 0 unless $reqident;
  my $proto = 'HTTP'.($https =~ /^ON$/i ? 'S:' : ':');
  my $actRE = qr/^(?:\/$main::Application->{ApplName})?\/?("?)(?:(.*?)[#\@\?]+)?(.*)\1$/;
  my ($pkgref, $method) = ($httpaction =~ /$actRE/)[1,2] if $httpaction;
  ($pkgref, $method) = ($main::Application->{ApplName}, $SOAPAct) if $SOAPAct;
  $method = ($SOAPAct =~ /$actRE/)[-1] if $SOAPAct;

  my $parsesub = $class->can('_parseContent');
  main::write2log("init Method: $reqmet, httpaction: -$httpaction-, QRYSTR: -$qrystr-, SOAPAct: -$SOAPAct-, method: -$method-, class: -$class-, parsesub: $parsesub");
#  main::debug2log("${method} $reqident ALL_RAW:", $main::Request->ServerVariables('ALL_RAW')->item());
  ($qrystr, my %parms) = split /[\@\/\#\&=]/, $qrystr; #($qrystr =~ /^([\w\-\_]+)[\@\/\#\&](.*)$/g);
  main::debug2log("${method} $reqident qrystr:", $qrystr, " parms: ", %parms);

  $parsesub = ($SOAPAct 
	       ? sub { my ($method, @parms) = ( shift, @_ );
		       main::debug2log("SOAP parsesub default $method  - selection: @parms");
		       return XReport::SOAP::parseSOAPreq(main::readContent($reqident, $method)); } 
	       : sub { my ($method, @parms) = ( shift, @_ );
		       main::debug2log("HTML parsesub $method  - selection: @parms");
		       return { request => { split(/[=&]/, main::readContent($reqident, $method) ), @parms } }; } 
	      )
    unless $parsesub;

  $main::Response->Clear();
#  $main::Response->{buffer} = 0;
#  $main::Response->{'Buffer'} = 1;

  my $msub;
  if ( $reqmet eq 'GET' && $qrystr && $qrystr =~ /^WSDL$/i ) {
    #  $main::Response->ClearContent();
    #  $main::Response->AddHeader('Content-Type', 'text/xml');

    #  $main::Response->AddHeader('Content-Type', 'text/xml');
    my $wsdl = $main::Application->{WSDL};
    unless ( $wsdl) {
      (my $uri = $httpaction) =~ s/\?$qrystr$//;
      $wsdl = XReport::SOAP::buildWSDL("$proto//${httpserverpath}$uri", $main::Application->{ApplName} ); 
      main::write2log("sendWSDL storing WSDL in APP pool for $main::servername httpaction: $httpaction URI: $uri");
      $main::Application->{WSDL} = $wsdl;
    }
    $main::Response->{ContentType} = 'text/xml';
    $main::Response->Write($wsdl);
  } elsif ( $reqmet =~ /^(?:GET|POST)$/ && $qrystr && $qrystr =~ /^setDebug=([\d])[^\w]*/i ) {
    $main::Application->{DEBUG} = $1;
    $main::DEBUG = $1;
  } elsif ( $reqmet eq 'GET' && $qrystr && $qrystr =~ /^status$/i ) {
    use XReport::ApacheStatus;
    return XReport::ApacheStatus::handler(@_);
  } elsif ( $reqmet eq 'GET' && $qrystr && $qrystr =~ /^showlog$/i ) {
    $msub = main->can('_GET_'.uc($qrystr));
    $method = uc($qrystr);
  } elsif ( $reqmet eq 'GET' && $qrystr && $qrystr =~ /^testpage$/i ) {
    $method = $qrystr;
    $msub = sub {
      my ($method, $reqident, $selection) = (shift, shift, shift);
    
      my ($tbl, $maxlines, $IndexEntries, $orderby, $distinct, $newentries) = 
	@{$selection->{request}}{qw(IndexName TOP IndexEntries ORDERBY DISTINCT NewEntries)}
	  if ref($selection);
    
      $main::Response->{ContentType} = 'text/html';
      $main::Response->print(start_html(), 
			     h3("Hello From ", $class),
			     pre("Selection:\n" . XReport::SOAP::Dumper($selection)),
			     end_html()
			    );
    
    }
  } elsif ( $reqmet eq 'POST' && $SOAPAct && ($msub = $class->can($method)) ) {
    $main::Response->{ContentType} = 'text/xml';
    #  $method = ($SOAPAct =~ /$actRE/)[-1] if $SOAPAct;
  } elsif ( $reqmet eq 'POST' && ($qrystr && $qrystr =~ /^\w+$/i && ($msub = $class->can($qrystr)) ) ) {
    $method = $qrystr;
  } elsif ( $reqmet eq 'GET' && $qrystr && $qrystr =~ /^\w+$/i && ($msub = $class->can('_GET_'.uc($qrystr))) ) {
    $method = uc($qrystr);
  } else {
    return Apache2::Const::FORBIDDEN;
  }

  if ( $msub) {
    my $selection = &$parsesub($method, %parms);
    main::debug2log("$reqmet $method Invoking Handling routine - selection: ", XReport::SOAP::Dumper($selection));
    eval { &$msub($method, $reqident, $selection); };
    main::SOAPError("$method", "Invoke", "$@", 500) if $@;
  }

  my $timesnow = [ times() ];
  for my $ix (0..3) {
    $timesnow->[$ix] = $timesnow->[$ix] - $main::timesmark->[$ix];
  }
  my $timesmsg = "Times(us::sy::cus::csy):_" . join('::', @{$timesnow} ) ;

#  $main::Response->Flush();
  main::write2log("$reqmet $method Request $reqident  ended: ", $timesmsg, " - ", ($main::msgtolog || ''));
#  $main::Response->AppendToLog("${method}_start_at_${main::starttime}_-ID_$reqident-_"
#			       . $timesmsg . '_-_'
#			       . ($main::msgtolog || '')
#			      );
    Apache2::Const::OK;

}
__PACKAGE__;
