package XReport::SOAP::BINStreamer;

use Win32::OLE::Variant;
use IO::String;

sub new {
  my $class = shift;
 
  my $self = { buffsize => 57*76, bufflen => 0, InputBytes => 0, OutputBytes => 0, buffer => '', content => shift};
  $self->{response_data} = '';
  bless $self, $class;
  
  return $self;
}

sub write {
  my ($self, $buffer) = (shift, shift);
  $self->{buffer} .= $buffer; 
  $main::Response->{ContentType} = $self->{content} unless $self->{OutputBytes};
  while ( length($self->{buffer}) > $self->{buffsize} ) {
    ($buffer, $self->{buffer}) = unpack("a".$self->{buffsize}." a*", $self->{buffer}) ;
    my $binstream = new Win32::OLE::Variant(VT_UI1, $buffer);
    $main::Response->BinaryWrite($binstream) ;
    $self->{InputBytes} += length($buffer);
    $self->{OutputBytes} += length($binstream);
  }
}

sub print {
     my $self = shift; return $self->write(join('', @_)); 
}

sub tell {
 my $self = shift;
 return $self->{InputBytes} + length($self->{buffer});
}

sub close {
  my $self = shift;
  $self->write('');
  my $binstream = new Win32::OLE::Variant(VT_UI1, $self->{buffer});
  $main::Response->BinaryWrite($binstream) ;
  $self->{InputBytes} += length($self->{buffer});
  $self->{OutputBytes} += length($binstream);
  main::debug2log("Inputbytes: ", $self->{InputBytes}, " Outputbytes: ", $self->{OutputBytes});
#  return $main::Response->Flush();
}

__PACKAGE__;
