package XReport::SOAP::Server;

sub new {
    my $class = shift;
    my $self = bless {}, $class;
    
    return $self;
}

#package XReport::DBUtil;
#
#use strict;
#use DBI;
#
#sub connect {
#  my ($self, $dbid) = (shift, shift);
#  if ( !ref($self) ) {
#     $dbid = 'XREPORT' unless $dbid;
#     $self = new('XReport::DBUtil', $dbid);
#  }
#  $dbid = $self->{dbid};
#  if ( !exists($main::Application->{odbcdb}->{$dbid}->{dbh}) || !$main::Application->{odbcdb}->{$dbid}->{dbh}->ping() ) {
#    i::logit("Connecting to $dbid using $self->{cstring}");
#    $main::Application->{odbcdb}->{$dbid}->{dbh} = DBI->connect("dbi:ODBC:$self->{cstring}", undef, undef,
#                                                                 { RaiseError => 1, PrintError => 0, odbc_cursortype => 2, @_}) ;
#  }
#  return $main::Application->{odbcdb}->{$dbid}->{dbh};
#}
#
##sub disconnect {
##  return shift->{dbc}->disconnect;
##}
##
#sub get_ix_dbc {
#   my $self = shift;
#   unless ( ref($self) ) {
#      $self = new('XReport::DBUtil', 'XREPORT'); 
#   } 
#  my $IndexName = $_[-1];
#  my $dbName = '';
#  my $rowset = $self->dbExecute("SELECT * from tbl_IndexTables WHERE IndexName = '$IndexName'");
#  if ( ref($rowset) && scalar(@{$rowset})  ) { $dbName = $rowset->[0]->{'DatabaseName'}; }
#  else {
#    die("UserError: INDEX \"$IndexName\" NOT DEFINED IN tbl_IndexTables")
#  }
#
#  return new('XReport::DBUtil', $dbName);
#}
#
#
#sub new {
#  my ($class, $dbid) = (shift, shift);
#  $dbid = 'XREPORT' unless $dbid;
#  #<XRINDEX>PROVIDER=SQLOLEDB;DATA SOURCE=CESQL;DATABASE=xrindex;User Id=xreport;password=euriskom;</XRINDEX>
#  #<XREPORT>PROVIDER=SQLOLEDB;Network Library=DBNMPNTW;DATA SOURCE=CESQL;Initial Catalog=xreport;Integrated Security=SSPI;</XREPORT>
#  if ( !exists($main::Application->{odbcdb}) ) {
#    while ( my ($dbkey, $dbstr) = each %{$XReport::cfg->{dbase}} ) {
#      my $cparms = { split /[\=;]/, $dbstr };
#      $cparms->{'DATABASE'} = delete $cparms->{'Initial Catalog'} if exists($cparms->{'Initial Catalog'});
#      $cparms->{'ACCESS'} = "Trusted_Connection=yes" if exists($cparms->{'Trusted_Connection'});
#      $cparms->{'ACCESS'} = "Trusted_Connection=yes" if exists($cparms->{'Integrated Security'});
#      my ($ukey) = grep /^user id$/i, keys %$cparms;
#      my ($pkey) = grep /^password$/i, keys %$cparms;
#      my ($skey) = grep /^data source$/i, keys %$cparms;
#      $cparms->{'ACCESS'} = "Uid=$cparms->{$ukey};Pwd=$cparms->{$pkey}" if $ukey;
#      my ($sname, $port) = ( split /\,/, $cparms->{$skey} );
#      my ($server, $instance) = ( split /[\\]/, $sname, 2 );
#      my $hostname = (split /\./, $server)[0]; 
#
#      $port = '1433' unless $port;
#
#      $main::Application->{odbcdb}->{$dbkey} = { dbid => $dbkey, cstring => "Driver=\{SQL Server\}"
##        . ";Server=".($instance ? "$hostname\\$instance" : "$hostname")
##        . ";Address=$server,$port"
#        . ";Server=$server,$port"
#        . ";Database=".$cparms->{'DATABASE'}
##        . ";Network=DBMSSOCN"
#        . ";".$cparms->{'ACCESS'}
#        . ";APP=".$main::Application->{ApplName}
#        . ";" };
#      $main::dotrace && i::traceit("Processed cparms: ", XReport::SOAP::Dumper($cparms), 
#               " ODBCSTR: ", XReport::SOAP::Dumper($main::Application->{odbcdb}->{$dbkey}));
#      $main::dolog && i::logit("DB $dbkey connection string set to $main::Application->{odbcdb}->{$dbkey}->{cstring}");
#    }
#  }
#  die "$dbid db access not configured" unless exists($main::Application->{odbcdb}->{$dbid});
#  my $self = { %{$main::Application->{odbcdb}->{$dbid}} };
##  &connect($self, $dbid);             
#  $main::dotrace && i::traceit("Connection for \"$dbid\" initialized") ;
#  return bless $self, $class;
#
#}
#
#=pod
#
#The following is suppose to take care of caching connection, but it does not work
#
#=cut
#
#sub connect_cached {
#    
#    my $self = shift;
#    return DBI->connect_cached("dbi:ODBC:$self->{cstring}", undef, undef, 
#              {RaiseError => 1
#              , PrintError => 0
#              , odbc_cursortype => 2
#              , AutoCommit => 1
#              , Callbacks => { "connect_cached.new" => 
#                sub {
#                   i::logit("new connection - ARGS:", join('::', @_), " : {", join('}{', keys %{$_[-1]}), '}');
#                                                           #$_[4]->{private_is_new} = 1;
#                                                           return 1;
#                                                         }
#                             , "connect_cached.reused" => sub {
#                                                           $main::dolog && i::logit("reused connection - ARGS:", join('::', @_));
#                                                           return 1;
#                                                         }
#                             , "connected" => sub {
#                                                           my $dbh = shift;
#                                                           (my $server) = ( $_[0] =~ /Server=([^;]+);/i);
#                                                           $main::dolog && i::logit("Connection to $server completed");# if delete $dbh->{private_is_new};
#                                                           return;
#                                                  }
#                             
#                             }
#              , @_
#              });
#}
#
#
#sub dbExecute {
#  my $self = shift;
#  my ($sql, $selparms) = (shift, shift);
#  $selparms = {} unless ref($selparms);
#  my $dbid = delete $selparms->{dbid};
#  if ( !ref($self) ) {
#     $dbid = 'XREPORT' unless $dbid;
#     $self = new('XReport::DBUtil', $dbid);
#  }
##  my $dbh = $self->connect_cached();
#  my $dbh = $self->connect();
#
#  $main::dotrace && i::traceit("dbExecute Invoked as requested by caller ", join('::', (caller())[0,2]), " SELECT: $sql") ;
#  @{$dbh}{keys %$selparms} = @{$selparms}{keys %$selparms};
#  my $rowset = $dbh->selectall_arrayref($sql, {Slice => {}});
#  $main::dotrace && i::traceit("Returned ROWSET: ". XReport::SOAP::Dumper($rowset)) ;
#  return $rowset;
#}
#
#sub getTableRows {
#  my $self = shift;
#  my ($sql, $selparms, $idkeys) = (shift, shift, shift);
#  $idkeys = [] unless defined $idkeys;
#  $selparms = {} unless ref($selparms);
#  my $dbid = delete $selparms->{dbid};
#  if ( !ref($self) ) {
#     $dbid = 'XREPORT' unless $dbid;
#     $self = new('XReport::DBUtil', $dbid);
#  }
##  my $dbh = DBI->connect_cached("dbi:ODBC:$self->{cstring}", undef, undef, {RaiseError => 1, PrintError => 0, odbc_cursortype => 2});
##  my $dbh = $self->connect_cached();
#  my $dbh = $self->connect();
#
#  $main::dolog && i::logit("getTableRows Invoking as requested by caller", join('::', (caller())[0,2]), " SELECT: $sql") ;
#  @{$dbh}{keys %$selparms} = @{$selparms}{keys %$selparms};
#  $dbh->{RaiseError} = 1; # do this, or check every call for errors
#  my $sth = $dbh->prepare($sql);
#  $sth->execute();
#  my %row;
#  my $rowset = [];
#  $sth->bind_columns( \( @row{ @{$sth->{NAME} } } ));
#  while ($sth->fetch) {
#    my $Columns = [];
#    while ( my ($col, $val) = each %row ) { 
#      push @$Columns, { content => $val || '', colname => "$col" }; 
#    }
#    my $id = {};
#    $id = { map { $_ => $row{$_} } @$idkeys } if scalar(@$idkeys); 
#    push @{$rowset}, { Columns => $Columns, %$id };
#  }
#
##  my $rowset = $self->{dbh}->selectall_arrayref($sql, {Slice => {}});
#
#  $main::dotrace && i::traceit("Returned ROWSET: ". XReport::SOAP::Dumper($rowset));
#  return $rowset;
#}
#
#sub retrieveBlob {
#  require Encode;
#
#  my ($self, $sql, $fldname) = (shift, shift, shift);
#  my (%row, $sth, $resource);
#  $sql =~ s/^SELECT.*?\s(FROM\s.*)$/SELECT *, DataLength($fldname) as bloblength $1/;
#  @{$self->{dbh}}{qw(LongReadLen LongTruncOk RaiseError PrintError)} = (0, 1, 1, 0);
#  $sth = $self->{dbh}->prepare($sql);
#  $sth->execute();
#  $sth->bind_columns( \( @row{ @{$sth->{NAME} } } ));
#  $sth->fetch;
#  $sth->finish;
#  my $result = { IndexEntry => { %row } };
#  $sql =~ s/^SELECT.*?\s(FROM\s.*)$/SELECT $fldname $1/;
#  my $fldlen = $row{bloblength} + 1;
#  @{$self->{dbh}}{qw(LongReadLen LongTruncOk)} = ($fldlen, 0);
#  $sth = $self->{dbh}->prepare($sql);
#  $sth->execute;
#  $sth->bind_columns( \$resource );
#  $sth->fetch;
#  $sth->finish;
#  $result->{DocumentBody}->{content} = $resource;
#  return $result;
#}
#
#sub storeBlob {
##  require Encode;
#
#  my ($self, $tbl, $fldname, $inrow) = (shift, shift, shift, shift);
#  my (%row, $sth, $identity);
#  my @cols = keys %$inrow;
#  my $sql = "INSERT INTO $tbl ( " . join(',', @cols ) . " ) VALUES( " . join(',', ("?")x@cols ) . ");"
#          . "UPDATE $tbl SET XferEndTime = GETDATE() where IDENTITYCOL = SCOPE_IDENTITY();"
#          . "SELECT *, DataLength($fldname) as bloblength from $tbl where IDENTITYCOL = SCOPE_IDENTITY();";
#  @{$self->{dbh}}{qw(LongReadLen LongTruncOk RaiseError PrintError)} = (0, 1, 1, 0);
##  $main::dolog && i::logit("STORE SQL: ". $sql) ;
#  $sth = $self->{dbh}->prepare($sql);
##  $sth->bind_param(1, $blob, SQL_LONGVARBINARY);
#  $sth->execute(@{$inrow}{@cols});
#  $sth->bind_columns( \( @row{ @{$sth->{NAME} } } ));
#  $sth->fetch;
#  $sth->finish;
#  my $result = { IndexEntry => { %row } };
##  $main::dolog && i::logit(XReport::SOAP::Dumper($result)) ;
#  return $result;
#}
#
package XReport::SOAP::Response;

use Time::HiRes ();

sub new {
    my $self = { 
        fcgireq => $_[1] 
        ,header => {-type => 'text/xml', -status => '200 OK'}
        ,byteswritten => 0
    };
    @{$self}{qw(fhin fhout fherr)} = $FCGI::global_request->GetHandles();
    $self->{fcgienv} = $FCGI::global_request->GetEnvironment();
    bless $self, $_[0];
}

sub Write {
    my $self = shift;
    my $fhout = $self->{fhout};
#    $main::dolog && i::logit("WRITE HEADER INPUT:", XReport::SOAP::Dumper($self->{header})) ;
#   $main::dolog && i::logit("WRITE HEADER:", CGI::header(%{$self->{header}}), " string: ", @_) ;
    $self->{_headerdone} = print $fhout CGI::header(%{$self->{header}})
                                              if ( !exists($self->{_headerdone}) && !$self->{_donotheader} );
    print $fhout @_;
    $main::dolog && i::logit("RESPONSE Write ", length("@_"), " bytes written to out channel") ;
    
}

sub BinaryWrite {
    return Write(@_);
}

sub toggleheader {
    $_[0]->{_donotheader} = ($_[0]->{_donotheader} ? 0 : 1);
}

sub Status {
    $_[0]->{header}->{'-status'} = $_[1];
}

sub ContentType {
    $_[0]->{header}->{'-type'} = $_[1];
}

sub ContentLength {
    $_[0]->{header}->{'-content-length'} = $_[1];
}

sub Buffer { return 1; }
sub CacheControl { return 1; }
sub Charset { return 1; }
sub CodePage { return 1; }
sub CookiesCollection { return 1; }
sub Expires { return 1; }
sub ExpiresAbsolute { return 1; }
sub IsClientConnected { return 1; }
sub LCID { return 1; }
sub PICS { return 1; }

sub AddHeader { 
        my $self = shift;
        my $args = { @_ };
        $self->{header} = {} unless exists($self->{header});
#        $main::dolog && i::logit("AddHeader b4:", XReport::SOAP::Dumper($self->{header}), "args: ", XReport::SOAP::Dumper($args)) ;
        while ( my ($header, $value) = each %{$args} ) {
            $self->{header}->{'-'.$header} = $value;
        }
#        $main::dolog && i::logit("AddHeader af:", XReport::SOAP::Dumper($self->{header})) ;
}

sub Flush {
    return $FCGI::global_request->Flush();
}

package XReport::SOAP::Request;

use Time::HiRes ();

DESTROY {
    my $self = shift;
  $self->Flush();
  $FCGI::global_request->Finish();
  my $timesnow = [ times(), Time::HiRes::time() ];
  for my $ix (0..4) {
      $timesnow->[$ix] = $timesnow->[$ix] - $self->{timesmark}->[$ix];
  }
  my $timesmsg = "Times(us::sy::cus::csy::elapsed):_" . join('::', @{$timesnow} ) ;
  my $method = $self->{requestmeth} || '__UNKNOWN__';
  $main::dolog && i::logit($self, "$method REQUEST handler destroyed - $timesmsg");
  
}

sub isFastCgi { return $FCGI::global_request->IsFastCGI() };

sub Accept {
	if (!$FCGI::global_request ) {
        $main::dolog && i::logit("FCGI global_request not defined - Accept aborted");
        return undef;		
	}
	if ( $FCGI::global_request->Accept() >= 0 ) {
      $main::Request = XReport::SOAP::Request->new($FCGI::global_request);
      foreach my $k ( keys %{$main::origenv} ) { $ENV{$k} = $main::origenv->{$k} unless exists($ENV{$k}); }
      $main::Response = new XReport::SOAP::Response($FCGI::global_request);
      $main::dotrace && i::traceit("New Request Accepted");
      return $main::Request;
	}
    i::logit("FCGI Accept failed ");
	return undef;
}


sub new {
#	return undef unless $_[1];
    my $self = { fcgireq => $_[1], perlclass => $main::Application->{perlclass} };
    
    $self->{timesmark} = [ times(), Time::HiRes::time() ];
    $main::Application->{reqcount} = 0 if !exists($main::Application->{reqcount});
    $main::Application->{reqcount} += 1;
    @{$self}{qw(fhin fhout fherr)} = $FCGI::global_request->GetHandles();
    $self->{fcgienv} = $FCGI::global_request->GetEnvironment();

    bless $self, $_[0];
    @{$self}{qw(serverhost serverurl https requestmeth querystr soapstr remote_addr remote_agent vdirname)} =
                                 $self->ServerVariables(qw( HTTP_HOST URL HTTPS REQUEST_METHOD QUERY_STRING HTTP_SOAPACTION 
                                                            REMOTE_ADDR HTTP_USER_AGENT INSTANCE_NAME ) );
    $self->{https} = 'off' unless $self->{https};
    $self->{totalbytes} = $self->{fcgienv}->{CONTENT_LENGTH} || 0;                                                            
    my $proto = 'HTTP'.($self->{https} eq 'off' ? ':' : 'S:');
    $self->{requri} = "$proto//$self->{serverhost}$self->{serverurl}";
    $self->{reqid} = " $self->{vdirname} $main::Application->{ApplName} $$.$main::Application->{reqcount} $self->{remote_addr} ";

#    $main::dolog && i::logit( $self, sprintf("New Request Initialized - URL: %s CONTENT_LENGTH: %d - request agent %s", 
#                                                                @{$self}{qw(requri totalbytes remote_agent)} ) ) ; 

    my ($physclpath, $xlatedpath ) = $self->ServerVariables(qw(APPL_PHYSICAL_PATH PATH_TRANSLATED));
    ($self->{reststr} = ( length($xlatedpath) > length($physclpath) ? substr($xlatedpath, length($physclpath)) : '')) =~ s/[\\\/]$//;

    my ($restparms, $SOAPparms, $qryparms, $method, $pkgref) = ([], [], [], '', $main::Application->{ApplName}, '' );

    if ( $self->{reststr} ) {
       $restparms = [ split /[\\\/]/, $self->{reststr} ];
       $method = $restparms->[0];
    }

    $qryparms = [ split /\&/, $self->{querystr} ] if $self->{querystr};
    if ( scalar(@{$qryparms}) ) {
       $method = $qryparms->[0];
    }

    if ( $self->{soapstr} =~ /^\"([^\"]+)\"/ ) { $self->{soapstr} = $1; }
    $SOAPparms = [ reverse(split /[\\\/\#]/, $self->{soapstr}) ] if $self->{soapstr};
    if ( scalar(@{$SOAPparms}) ) {
       $method = $SOAPparms->[0];
       $pkgref = $SOAPparms->[-1];
    }
    @{$self}{qw(method restparms soapparms qryparms)} = ($method, $restparms, $SOAPparms, $qryparms);
    
    $main::dolog && i::logit($self, sprintf("New Request - Agent: %s Method: %s, httpaction: %s, class: %s, method: %s, bytes: %d, URL: %s"
                                              ,@{$self}{qw(remote_agent requestmeth serverurl perlclass method totalbytes requri)})
            ,($self->{querystr} ? (" QRYSTR : -$self->{querystr}-  parms: ", join('::', @{$self->{qryparms}}), ",") : (''))
            ,($self->{soapstr} ?(" SOAPSTR: -$self->{soapstr}- parms: ", join('::', @{$self->{soapparms}}), ",") : (''))
            ,($self->{reststr} ?(" RESTSTR: -$self->{reststr}- parms: ", join('::', @{$self->{restparms}}), ",") : (''))
            ," PHPATH: $physclpath, XLPATH: $xlatedpath, parsesub: ", ref($main::Application->{parsesub}) ) ;
    return $self;
}

sub Flush {
    my $self = shift;
    my $ret = $FCGI::global_request->Flush() if !$self->{flushed};
    $self->{flushed} = 1;
    return $ret || $self->{flushed}; 
}


sub REQUEST_ID {
    return shift->{reqid};
}

sub Read {
   my $self = shift;
   my $fhin = $self->{fhin};
   my $len2read = shift;
    
   my $cl = $self->TotalBytes();
   $cl = $self->{bytesleft} if exists($self->{bytesleft});
   $self->{bytesleft} = $cl unless exists($self->{bytesleft});
   $cl = $len2read if $cl > $len2read;
#   $main::dolog && i::logit("Reading $cl bytes of $self->{bytesleft} - requested: $len2read") ;
#   return '' unless $cl;
   $cl = $len2read if !defined($cl);
   $main::dotrace && i::traceit( "Reading $cl bytes of $self->{bytesleft} - requested: $len2read");
   $fhin->read(my $reqbuffer, $cl);
   $self->{bytesleft} -= length($reqbuffer);
   return $reqbuffer;
}

sub BinaryRead {
    return Read(@_);
}

sub TotalBytes {
    return shift->{fcgienv}->{CONTENT_LENGTH};
}

sub ServerVariables {
    my $self = shift;
    return $self->{fcgienv} unless scalar(@_);
#    $main::dolog && i::logit('Retrieving variables:', join('::', @_), 'values: ', @{$self->{fcgienv}}{@_}) ;
    return $self->{fcgienv}->{$_[0]} if ( scalar(@_) == 1 );
    return (map { $_  ? $_ : '' } @{$self->{fcgienv}}{@_});  
}

sub bytesleft { return shift->{bytesleft}; }

sub FCGIreq { return $FCGI::global_request; }

package i;

use POSIX ();
=pod 

=head1 Logging

=head2 Logging Config Items
Logging is driven by 2 config attributes:

=item log2mcast => default true, this parameter tell the process to send log entries using multicast
  which address is defined by the mcastaddr attribute ( default to 226.1.1.9:4000 )

=item log2file => default true, this parameter tell the process to write log entries in a file located 
  into the directory named XRW3SVC{Application Identity} under the location specified by the attribute logsdir
  of configuration. The file will be named {Application Name (-N argument)}.{current date (YYYYMMDD)}.log
  Since {Application Identity} is provided by FCGI only after the first REQUEST logging to file for the 
  init phase is not provided

By default log messages are sent always to the multicast address 226.1.1.9:3000.
This can be disabled setting the config attribute log2mcast to 0. 
The same attribute can be used to set the multicast destination to a different address than 226.1.1.9:3000

E.G. <webappl name="DocumentsIface" log2mcast="226.1.1.9:5000" log2file="0" >  

=head1 Tracing
  Is possible to enable detailed trace creating a directory trace under the directory named 
    XRW3SVC{Application Identity}
  In this dir trace infos will be written to a file named 
    {Application Name (-N argument)}.{current date (YYYYMMDD)}.trace
  As for the log, since the {Application Identity} info is provided by FCGI only after the first REQUEST,
  tracing during INIT phase will not be provided. 
  Furthermore trace will not be performed before a first log request

  Tracing via multicast is disabled by default. It can be enabled setting the config attribute 
  trace2mcast to a value of 1 (which indicates to use the default multicast address of 226.1.1.9:5000 ) 
  or to the desired multicast address.   

=cut

sub formatLogMsg {
   my $req = (ref($_[0]) eq 'XReport::SOAP::Request' ? shift : $main::Request);
   my $reqid = $req->REQUEST_ID() if $req;
#   warn "REQ: ", XReport::SOAP::Dumper($req), "\n";
#   warn "formatLM: $req REQUESTID: $reqid", "caller ", join('::', caller(1)), "\nargs: ", join('::', @_), "\n";
   my $t0 = Time::HiRes::gettimeofday();
   return join('', POSIX::strftime("%Y-%m-%d %H:%M:%S", localtime(int($t0))),sprintf(".%03d", int(($t0-int($t0))*1000))
              , " ", $main::Application->{serverhost}
              , " ", ($reqid ? $reqid : "$main::Application->{ApplName}.$$.INIT ").">"
              , @_);
} 

no warnings qw(redefine);
sub logit {
    my $req = (ref($_[0]) eq 'XReport::SOAP::Request' ? $_[0] : $main::Request);
    if ( $main::logmcast ) {
       $main::logsock->send(formatLogMsg(@_), 0, $main::mcastdest_log) if $main::logsock;
    }
    
    if ( $main::log2file && !$main::logsdir ) {
      return undef unless($req);
      my $applid = $req->ServerVariables('INSTANCE_ID');
      return undef unless $applid;
      
      $main::logsdir = $main::Application->{'cfg.logsdir'}.'/XRW3SVC'.$applid;
      mkdir $main::logsdir if (!-e $main::logsdir);
      if (!-e $main::logsdir || !-d $main::logsdir ) {
        $main::log2file = 0;
        $main::logsdir = undef;
        return undef;
      } 
    }
    return undef unless ( $main::log2file );
    my $logfn = $main::logsdir.'/'.$main::Application->{ApplName}.'.'.POSIX::strftime("%Y%m%d", localtime()).'.log';
    if ( !defined($main::log) || $logfn ne $main::currlogfn ) {
       $main::log->close() if $main::log;
       $main::log = new IO::File(">>$logfn");
       if ( $main::log ) {
          $main::currlogfn = $logfn;
          $main::log->binmode();
          my $xx = select $main::log;
          $|=1;
          select $xx;
       }
       else { 
          $main::log2file = 0; 
          $main::dotrace = 0;
          $main::currlogfn = undef;
          $main::logsdir = undef;
          return undef 
       }
    }
    print $main::log ''.formatLogMsg(@_), "\n";
}

sub traceit {
#   warn "LOGIT:", Dumper(\@_), " CALLER: ", (caller()), "\n", Dumper($env), "\n";
    if ( $main::tracemcast ) {
       $main::logsock->send(formatLogMsg(@_), 0, $main::mcastdest_trace) if $main::tracesock;
    }
    if ( !$main::logsdir ) {
        $main::trace2file = 0;
        $main::dotrace = ($main::tracemcast ? 1 : 0);
        return undef;
    }
    if ( !defined($main::trace2file) ){ 
       my $tracedir = $main::logsdir.'/trace';
       if (-e $tracedir && -d $tracedir) {
          $main::trace2file = 1;
          $main::tracedir = $tracedir;
       }
       else {
          $main::trace2file = 0;
          $main::dotrace = ($main::tracemcast ? 1 : 0);
       }
    }
    elsif ( $main::trace2file == 0 ) {
       return undef;
    }
    
    my $tracefn = $main::tracedir.'/'.$main::Application->{ApplName}.'.'.POSIX::strftime("%Y%m%d", localtime()).".$$.trace";
    if ( !defined($main::trace) || $tracefn ne $main::currtracefn ) {
       $main::trace->close() if $main::trace;
       $main::trace = new IO::File(">>$tracefn");
       if ( $main::trace ) {
          $main::currtracefn = $tracefn;
          $main::trace->binmode();
          my $xx = select $main::trace;
          $|=1;
          select $xx;
       }
       else { 
          $main::trace2file = 0;
          $main::dotrace = ($main::tracemcast ? 1 : 0);
          $main::currtracefn = undef;
          $main::tracedir = undef;
          return undef
       }
    }
    print $main::trace ''.formatLogMsg(@_), "\n";
}

package XReport::Util;

sub _strip{
    my $str = $_[0];

    $str =~ s/</</g;
    $str =~ s/>/>/g;
    $str =~ s/[\x00-\x1F]/<b>.<\/b>/g;
    return $str;
}

sub _PerlNameSpace(\%$) {
  no strict 'refs';
  no warnings 'deprecated';
  my ($package,$packname) =  @_;
  my $collection = {};
  
  foreach my $symname (sort keys %$package) {
    our ($sym, %sym, @sym);
    local *sym = $$package{$symname};
    $collection->{Functions}->{_strip($symname).'()'} = ref(\&sym)  if (defined &sym);
    $collection->{Arrays}->{"\@"._strip($symname)}    = ref(\@sym)  if (defined @sym);
    $collection->{Scalars}->{"\$"._strip($symname)}   = _strip($sym) if ((defined $sym) && (_strip($symname) !~ /Config_SH|summary/i)); 
    $collection->{Hashes}->{"\%"._strip($symname)}    = ref(\%sym)  if ((defined %sym) && !($symname =~/::/));
    $collection->{Packages}->{_strip($symname)}       = &XReport::Util::_PerlNameSpace(\%sym, $symname) if (($packname =~ /main::/i) 
                                        && (defined %sym) 
                                        && ($symname =~ /::/) 
                                        && ($symname !~ /^main::/i) );
  }
  return $collection;
}


package XReport::SOAP::FCGI;

use Carp;
require Exporter;
use base "XReport::SOAP";

#use XReport::FCGI::DBUtil ();

use FCGI (); # Imports the library; required line
use CGI ();

##############################################################################
# Define some constants
#

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

@ISA               = qw(Exporter);
@EXPORT            = qw();
@EXPORT_OK         = qw();
$VERSION           = '1.00';

use IO::Socket::INET ();

use constant WSDLHDR => '<?xml version="1.0" encoding="UTF-8"?>'
  . '<wsdl:definitions name="XRSOAPSERVERNAME"'
  . ' targetNamespace="http://cereport.org/WebService"' 
  . ' xmlns:apachesoap="http://xml.apache.org/xml-soap"'
  . ' xmlns:impl="http://cereport.org/WebService"'
  . ' xmlns:intf="http://cereport.org/WebService"'
  . ' xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"'
  . ' xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"'
  . ' xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/"'
  . ' xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
  . '  <wsdl:types>'
  . '  <xsd:schema'
  . '   elementFormDefault="qualified"'
  . '   targetNamespace="http://cereport.org/WebService"'
  . '   xmlns="http://www.w3.org/2001/XMLSchema"'
  . '   xmlns:apachesoap="http://xml.apache.org/xml-soap"'
  . '   xmlns:impl="http://cereport.org/WebService"'
  . '   xmlns:intf="http://cereport.org/WebService"'
  . '   xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">'
#  . '  <xsd:complexType name="column">'
#  . '    <xsd:annotation>'
#  . '      <xsd:documentation>Column Attributes</xsd:documentation>'
#  . '    </xsd:annotation>'
#  . '    <xsd:sequence>'
#  . '      <xsd:element maxOccurs="unbounded" minOccurs="0" name="ValuesArray" type="xsd:string"/>'
#  . '      <xsd:element maxOccurs="1" minOccurs="0" name="dummy4axis" type="xsd:string"/>'
#  . '    </xsd:sequence>'
#  . '    <xsd:attribute name="colname" type="xsd:string"/>'
#  . '    <xsd:attribute name="operation" type="xsd:string"/>'
#  . '    <xsd:attribute name="Value" type="xsd:string"/>'
#  . '  </xsd:complexType>'
  . '   <xsd:complexType name="column">'
  . '     <xsd:annotation>'
  . '       <xsd:documentation>Column Attrbutes</xsd:documentation>'
  . '     </xsd:annotation>'
  . '     <xsd:simpleContent>'
  . '       <xsd:extension base="xsd:string">'
  . '         <xsd:attribute name="colname" type="xsd:string"/>'
  . '         <xsd:attribute name="operation" type="xsd:string"/>'
  . '         <xsd:attribute name="Min" type="xsd:string"/>'
  . '         <xsd:attribute name="Max" type="xsd:string"/>'
  . '       </xsd:extension>'
  . '     </xsd:simpleContent>'
  . '   </xsd:complexType>'
  . '   <xsd:complexType name="IndexEntry">'
  . '     <xsd:annotation>'
  . '       <xsd:documentation>'
  . 'Definisce parte o tutto un documento. '
  . ' Viene indicata la parte di documento da attribuire alle chiavi specificate in termini di pagina di partenza' 
  . ' (FromPage) e numero di pagine consecutive (ForPages).'
  . ' Nell\'array "Columns" vengono specificate le colonne di indici pertinenti al documento ed il loro valore.'
  . ' In caso di interrogazione deve essere valorizzato solo l\'array "Columns" da cui viene costruita la where'
  . ' clause per la query. In caso di inserimento devono essere specificate anche le pagine pertinenti alle chiavi'
  . ' specificate'
  . '</xsd:documentation>'
  . '     </xsd:annotation>'
  . '     <xsd:sequence>'
  . '       <xsd:element maxOccurs="unbounded" minOccurs="0" name="Columns" type="impl:column"/>'
#  . '       <xsd:element maxOccurs="1" minOccurs="0" name="dummy4axis" type="xsd:string"/>'
  . '     </xsd:sequence>'
  . '     <xsd:attribute name="JobReportId" type="xsd:string"/>'
  . '     <xsd:attribute name="FromPage"    type="xsd:string"/>'
  . '     <xsd:attribute name="ForPages"    type="xsd:string"/>'
  . '   </xsd:complexType>'
  . '   <xsd:complexType name="DocumentData">'
  . '     <xsd:annotation>'
  . '       <xsd:documentation>Document Data Structure. Viene usata sia nelle richieste che nelle risposte. Contiene i seguenti campi: IndexName - Nome simbolico dell\'indice da accedere; DocumentType - Typo documento da trattare (PDF per il momento); temporary - non so; TOP - numero massimo di righe indice da ritornare alla richiesta; ORDERBY - statement da utilizzare per l\'ordinamento delle righe indice; IndexEntries - array di righe indice da trattare; DocumentBody - rappresentazione in BASE64 del contenuto del documento</xsd:documentation>'
  . '     </xsd:annotation>'
  . '     <xsd:sequence>'
  . '       <xsd:element maxOccurs="unbounded" minOccurs="0" name="IndexEntries" type="impl:IndexEntry"/>'
  . '       <xsd:element maxOccurs="1" minOccurs="0" name="NewEntries" type="impl:IndexEntry"/>'
  . '       <xsd:element maxOccurs="1" minOccurs="0" name="DocumentBody" type="xsd:base64Binary"/>'
  . '     </xsd:sequence>'
  . '     <xsd:attribute name="IndexName" type="xsd:string"/>'
  . '     <xsd:attribute name="DocumentType" type="xsd:string"/>'
  . '     <xsd:attribute name="FileName" type="xsd:string"/>'
  . '     <xsd:attribute name="temporary" default="false" type="xsd:boolean"/>'
  . '     <xsd:attribute name="TOP"       type="xsd:string"/>'
  . '     <xsd:attribute name="ORDERBY"   type="xsd:string"/>'
  . '     <xsd:attribute name="DISTINCT"  type="xsd:string"/>'
  . '     <xsd:attribute name="OPTYPE"    default="QUERY" type="xsd:string"/>'
  . '   </xsd:complexType>'
  . '   <xsd:complexType final="extension" name="FaultData">'
  . '    <xsd:annotation>'
  . '     <xsd:documentation>Fault reporting structure</xsd:documentation>'
  . '    </xsd:annotation>'
  . '    <xsd:sequence>'
  . '     <xsd:element maxOccurs="1" minOccurs="0" name="faultcode" type="xsd:string" />'
  . '     <xsd:element maxOccurs="1" minOccurs="0" name="faultstring" type="xsd:string" />'
  . '     <xsd:element maxOccurs="1" minOccurs="0" name="faultactor" type="xsd:anyURI" />'
  . '     <xsd:element maxOccurs="1" minOccurs="0" name="detail">'
  . '      <xsd:complexType>'
  . '       <xsd:sequence>'
  . '        <xsd:element maxOccurs="1" minOccurs="0" name="message" type="xsd:string" />'
  . '        <xsd:element maxOccurs="1" minOccurs="0" name="description" type="xsd:string" />'
  . '        <xsd:element maxOccurs="1" minOccurs="0" name="errorId" type="xsd:string" />'
  . '       </xsd:sequence>'
  . '      </xsd:complexType>'
  . '     </xsd:element>'
  . '    </xsd:sequence>'
  . '   </xsd:complexType>'
  . '   <element type="impl:DocumentData" name="REQUEST" />'
  . '   <element type="impl:DocumentData" name="RESPONSE" />'
  . '   <element type="impl:FaultData" name="FAULT" />'
  . ' </xsd:schema>'
  . '</wsdl:types>'
  . '<wsdl:message name="RequestMsg"><wsdl:part element="intf:REQUEST" name="REQUEST"/></wsdl:message>'
  . '<wsdl:message name="ResponseMsg"><wsdl:part element="intf:RESPONSE" name="RESPONSE"/></wsdl:message>'
  . '<wsdl:message name="FaultMsg"><wsdl:part element="intf:FAULT" name="FAULT"/></wsdl:message>'
  ;

use constant WSDLTAIL => ''
  . '  <wsdl:service name="XRSOAPSERVERNAME">'
  . '    <wsdl:port name="XRSOAPSERVERNAMEPort" binding="intf:XRSOAPSERVERNAMEBinding" >'
  . '      <soap:address location="HTTP://127.0.0.1:8085/CeWebService/XRSOAPSERVERNAME.asp" />'
  . '    </wsdl:port>'
  . '  </wsdl:service>'
  . '</wsdl:definitions>'
  ;

use Pod::Simple::XMLOutStream;

sub _Dumper { return XReport::SOAP::Dumper(@_); }

sub _build_soap_error {
    my $class = shift;
    $class = ref($class) if ref($class);
    my $args = { @_ }; 
    $args->{faultactor} = join('/', ($0, $main::Request->ServerVariables(qw(HTTP_SOAPACTION)))) 
          if (!exists($args->{faultactor}) && $main::Request && $main::Request->can('ServerVariables'));
    $args->{faultactor} = $class.'/'.(caller())[0] unless exists($args->{faultactor});
          
    my $respmsg = '<?xml version="1.0" encoding="UTF-8"?>'
         . '<SOAP-ENV:Envelope xmlns:xsi="http://www.w3.org/1999/XMLSchema/instance" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">'
         . '<SOAP-ENV:Body>'
         . '<SOAP-ENV:Fault>'
         . "<faultcode>$args->{faultcode}</faultcode>"
         . "<faultactor>$args->{faultactor}</faultactor>"
         . "<faultstring>$args->{faultstring}</faultstring>"
         ;
   if ( exists($args->{detailmsg}) || exists($args->{detailcode}) || exists($args->{detaildesc}) ) {
      $respmsg .= '<detail>';
      $respmsg .= '<message><![CDATA['.$args->{detailmsg}.']]></message>' if exists($args->{detailmsg});
      $respmsg .= '<description><![CDATA['.$args->{detaildesc}.']]></description>' if exists($args->{detaildesc});
      $respmsg .= '<errorId>'.$args->{detailcode}.'</errorId>' if exists($args->{detailcode});
      $respmsg .= '</detail>';
   }
   $respmsg .= '</SOAP-ENV:Fault>'
         . '</SOAP-ENV:Body>'
         . '</SOAP-ENV:Envelope>'
         ;
   $main::dolog && i::logit("_SOAP_ERROR - ", $respmsg);
   return $respmsg;
#  $main::Response->{Status} = "$args->{faultcode} $args->{faultstring}";
#  $main::Response->AddHeader("Connection", "close");
#  $main::Response->Write( $respmsg );
#  $main::Response->Flush();
#  return undef;
}


#$SIG{__WARN__} = sub {
#   if ( exists($XReport::cfg->{logfile}) ) {
#       if ( !defined($main::log) ) {
#             my $logfn = $XReport::cfg->{logfile};
#             $logfn = 'd:/xena/'.$main::Application->{ApplName}.'.log' unless $logfn;
#         $main::log = new IO::File(">>$logfn");
#         if ( $main::log ) {
#             $main::log->binmode();
#             my $xx = select $main::log;
#             $|=1;
#             select $xx;
#         }
#      }
#      print $main::log @_, "\n" if $main::log;
#   }
#}
#
#sub i::logit { return warn @_; }
#

sub import {
  # Handle the :strict tag
  delete $main::Application->{'iis.cfg.perl'} if exists($main::Application->{'iis.cfg.perl'});
  $main::logger->RemoveFile('ServerLog') if $main::logger && ref($main::logger) eq 'XReport::logger';
  $main::xreport_dbgsock = undef; 
  
  $main::origenv = { ORIGIN => "copy of perl ENV", %ENV };
  $main::logmcast = 1;
  $main::logmcast = 0 if (exists($main::Application->{'cfg.'}) && $main::Application->{'cfg.log2mcast'} eq '0');
  $main::tracemcast = 0;
  $main::tracemcast = 1 if (exists($main::Application->{'cfg.trace2mcast'}) && $main::Application->{'cfg.trace2mcast'} ne '0');
  $main::dolog = 1;
  $main::dolog = 0 if (exists($main::Application->{'cfg.donotlog'}) && !$main::Application->{'cfg.donotlog'});
  $main::log2file = 1;
  $main::log2file = 0 if (exists($main::Application->{'cfg.log2file'}) && !$main::Application->{'cfg.log2file'});
  $main::dotrace = 1;
  $main::trace2file = undef;
  if ($main::logmcast) {
     $main::mcastdest_log = ( !exists($main::Application->{'cfg.log2mcast'}) || $main::Application->{'cfg.log2mcast'} eq '1' 
                              ? '226.1.1.9:3000' : $main::Application->{'cfg.log2mcast'});
     $main::logsock = new IO::Socket::INET(Proto=>'udp', PeerAddr => $main::mcastdest_log, Reuse => 1);
  }
  if ($main::tracemcast) {
     $main::mcastdest_trace = ( $main::Application->{'cfg.trace2mcast'} eq '1' ? '226.1.1.9:5000' : $main::Application->{'cfg.trace2mcast'});
     $main::tracesock = new IO::Socket::INET(Proto=>'udp', PeerAddr => $main::mcastdest_trace, Reuse => 1);
  }
  $main::logrRtn = sub { return i::logit(@_) };

  $main::thisservice = (grep /^#[^\s]+$/, @_)[0];
  @_ = grep(!/^#[^\s]+$/, @_);
  if ( $main::thisservice ) {
    my $servername = substr($main::thisservice, 1);
    $main::thisservice = $servername.'.pm';
    my $svcfqn = $main::Application->{ApplPath}.'/'.$main::thisservice;
    my $class = require $main::thisservice;
    $class = 'XReport::'.$servername if $class =~ /\d+/;
    (my $wsdl = WSDLHDR . WSDLdeclaremethods( $svcfqn, methods($class, 'private') ) . WSDLTAIL ) =~ s/\s+/ /sg;
    $wsdl =~ s/\> \</\>\</sg; 
#    $wsdl =~ s/XRSOAPSERVERNAME/$servername/isg;
    $wsdl =~ s/XRSOAPSERVERNAME/xrfcgiserv/isg;
    my $classrtn = "package $class; ";
    $classrtn   .= "use XReport\:\:DBUtil (); ";
    $classrtn   .= "sub WSDL { "
                . " my \$serverpath = shift; "
                . " my \$wsdl = q($wsdl); "
                . " \$wsdl =~ s/<soap:address\\s+location=\".*\"\\s*\\/>/<soap:address location=\"\$serverpath\" \\/>/im; " 
                . " return \$wsdl; "
                . "}\n";
    $classrtn .= "sub _SOAP_ERROR { return main::_SOAP_ERROR( \@_ ); }\n";
    $classrtn .= "sub _Dumper { return XReport\:\:SOAP\:\:Dumper(\@_); }\n";
    $classrtn .= "sub dbExecute { return XReport\:\:DBUtil\:\:dbExecute(\@_); }\n";
    $classrtn .= "sub getDBHandle { return XReport\:\:DBUtil->new(\@_); }\n";
#    $classrtn .= "sub dbExecute { return XReport\:\:FCGI\:\:DBUtil\:\:dbExecute(\@_); }\n";
    $classrtn .= "'$class';\n";
                
    eval "$classrtn";
    my $outmsg = $@;
    i::traceit("EVAL Result: $@\n") if ($main::traceit && $@);
    $main::Application->{perlclass}  = $class;
    $main::Application->{wsdlsub}    = $class->can('WSDL');
    $main::Application->{parsesub}   = $class->can('_parseContent');
    $main::Application->{samplehtml} = $class->can('_sampleHtmlResponse');
    $main::Application->{samplesoap} = $class->can('_sampleSoapResponse');
    $main::Application->{serverhost} = $main::origenv->{COMPUTERNAME} || 'localhost';
    
    require XReport::SOAP::B64Streamer;
    require XReport::SOAP::BINStreamer;
    
  }
  i::logit("preliminary int ended- Main INIT begins");
#  push @ISA, 'XReport::DBUtil';
  goto &Exporter::import;
}

sub WSDLdeclaremethods {
   my $fn = shift;
   my $mm='';
   my $p = Pod::Simple::XMLOutStream->new();
   $p->accept_targets( qw(wsdldoc) );
   $p->accept_directive_as_verbatim(qw(method doc));
   $p->output_string(\$mm);
   $p->parse_file($fn);
   my $ifaceDoc = ($p->content_seen ? { map { $_->{method}->{content} => $_->{doc}->{content} 
                 . ($_->{Verbatim}->{content} ? "\n\n" . $_->{Verbatim}->{content} : '' ) } 
                                   grep { $_->{target} =~ /^:wsdldoc/ } 
                                   @{XReport::SOAP::XMLin($mm)->{for}} } : {});
                                   
   return  '<wsdl:portType name="XRSOAPSERVERNAMEMethods">'
          . join('', map { '<wsdl:operation name="'.$_.'" >'
                         . ( !exists($ifaceDoc->{$_}) ? ''
                         : '<wsdl:documentation><![CDATA['.$ifaceDoc->{$_}.']]></wsdl:documentation>')
                         . '<wsdl:input name="'.$_.'Input" message="impl:RequestMsg" />'
                         . '<wsdl:output name="'.$_.'Output" message="impl:ResponseMsg" />'
                         . '<wsdl:fault name="'.$_.'Fault" message="impl:FaultMsg" />'
                         . '</wsdl:operation>' } @_ ) 
          . '</wsdl:portType>'
          . '<wsdl:binding name="XRSOAPSERVERNAMEBinding" type="impl:XRSOAPSERVERNAMEMethods" >'
          . '<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http" />'
          . join('', map { '<wsdl:operation name="'.$_.'" >'
                         . '<soap:operation soapAction="'.$main::Application->{ApplName}.'#'.$_.'" />'
                         . '<wsdl:input name="'.$_.'Input" ><soap:body use="literal" /></wsdl:input>'       
                         . '<wsdl:output name="'.$_.'Output" ><soap:body use="literal" /></wsdl:output>'       
                         . '<wsdl:fault name="'.$_.'Fault" ><soap:fault use="literal" name="'.$_.'Fault" /></wsdl:fault>'
                         . '</wsdl:operation>' } @_ )
           . '</wsdl:binding>';       

}

sub buildWSDL {
 my $serverpath = shift;
 my $servername = shift;
# my $methods = [ methods("XReport\::$servername", 'private') ];

 my $classname = 'XReport::'.$servername;
 my $methods = $main::Application->{SOAPMethods};
 if ( !ref($methods) ) {
    $methods = [ grep { $_ !~ /^[\(_]/ && $classname->can($_) } keys %{$classname.'::'} ];
    $main::Application->{SOAPMethods} = $methods;
 }
 #$main::dolog && i::logit("$servername methods: ", @$methods);
 (my $wsdl = WSDLHDR . WSDLdeclaremethods( @$methods, @_ ) . WSDLTAIL) 
               =~ s/<soap:address\s+location=".*"\s*\/>/<soap:address location="$serverpath" \/>/im;  
 my $wsdlspace = lc($servername);
 #$wsdl =~ s/XRSOAPSERVERNAME/$wsdlspace/isg;
 $wsdl =~ s/XRSOAPSERVERNAME/xrfcgiserv/isg;
 $wsdl =~ s/\s+/ /sg;
 $wsdl =~ s/\> \</\>\</sg;
 return $wsdl;
}

sub buildCond {
      my ($var, $min, $max, $typ) = (shift, shift, shift, shift);
      # insert date format validation 'yyyy-mm-gg hh:mm:ss'
#      $main::dolog && i::logit("KEY: $var $typ MIN:$min(".defined($min).") MAX:$max(".defined($max).") caller " .join('::',(caller())[0,2]) );
      $min = "'$min'" unless !defined($min); #(!defined($min) or $typ =~ /^N$/i);
      $max = "'$max'" unless !defined($max); #(!defined($max) or $typ =~ /^N$/i);

      return " $var IS NULL " if !defined($min) && !defined($max);

      return " $var <= $max " if !defined($min);

      return " $var >= $min " if !defined($max);

      return " $var = 0 "     if ( "$min" eq "0" and "$max" eq "0");

      return " $var = '' "    if ( "$min" eq "''" and "$max" eq "''");

      return " $var = $min "  if ( $min eq $max );

      if ($min =~ /^'{0,1}LIKE'{0,1}$/) {
    return " $var LIKE $max " unless $typ =~ /^N$/i;
    $main::dolog && i::logit( "LIKE not supported for numeric column $var ");
    die "LIKE not supported for numeric column $var ";
      }

      if ($min =~ /^'{0,1}IN'{0,1}$/) {
    my $qt = ($typ =~ /^N$/i ? '' : "'");
    my $regex = ($typ =~ /^N$/i ? qr/[\x09\s]+/ : qr/\s*\x09\s*/);
    return " $var IN (" . join($qt.', '.$qt, split(/$regex/, $max)) . ") ";     
      }

      return " $var BETWEEN $min AND $max ";
}

sub buildClause {
  my ($tbl, $IndexEntries, $prefix) = (shift, shift, shift);

#  my $idxcoltypes = {split / /, $main::Application->{'XRIDX.'.$tbl.'ColTypes'} };
#  $main::dolog && i::logit("columns $tbl:", Dumper($idxcoltypes));

  my @or;
  i::traceit("buildClause request: ", _Dumper($IndexEntries)) if $main::dotrace;
  foreach my $row ( @{$IndexEntries} ) {
    my @and; 
    for my $attr (qw(JobReportId FromPage ForPages)) {
          $row->{Columns}->{$attr} = { content => $row->{$attr} } 
                               if ($row->{$attr} && !exists($row->{Columns}->{$attr}));
    }
    foreach my $var ( keys %{$row->{Columns}} ) {
#      return EXIT_SOAP_ERROR("Sender", "IDXAccess", "Variable $var Richiesta non in tabella $tbl") unless exists $idxcoltypes->{$var};
      my $typ = ''; #$idxcoltypes->{$var};
      my $qt = ( $typ eq 'N' ? '' : "'" );
      if ( !ref($row->{Columns}->{$var}) && $row->{Columns}->{$var} ) {
      	 my $val = delete $row->{Columns}->{$var};
      	 $row->{Columns}->{$var} = { content => $val };
      }
      my ($min, $max, $content) = @{ $row->{Columns}->{$var} }{qw(Min Max content)};
#      $main::dolog && i::logit("ForEach: Checking now $var for MIN:$min(".defined($min).") MAX:$max(".defined($max).") -$content-");
      if (!defined($content) && !defined($min) && !defined($max) ) {
         push @and, " $prefix$var IS NULL "
      }
      elsif ( !defined($content) ) {
         eval { push @and, buildCond($prefix.$var, $min, $max, '')}; # $idxcoltypes->{$var}) };
      }
      elsif (!defined($min) && !defined($max) ) {
         push @and, " $prefix$var = ${qt}${content}${qt} "
      }
      else {
         eval { push @and, buildCond($prefix.$var, $min, (defined($max) ? $max : $content), '') }; #$idxcoltypes->{$var}) };
      }
#      return EXIT_SOAP_ERROR("Sender", "QuerySyntax", $@) if $@;
      die $@ if $@;
    }
    push @or, '(' .join(') AND (', @and) . ')' if scalar(@and);
  }
  return @or;
}

sub fillIndexEntries {
  my $dbr = shift;
#  $main::dolog && i::logit("filling IndexEntries from " . join('::', (caller())[0,2]). " " . ref($dbr));

  my $IndexEntries = [];
  $main::dolog && i::logit("fillIndexEntries no rows in RESULTSET") if $dbr->eof();
  while (!$dbr->eof()) {
    my $fields = $dbr->Fields();
    my $IndexEntry = { };
    for my $fldn (0..$fields->Count()-1) {
      my $ftype = $fields->Item($fldn)->Type();
      next if ($ftype == 205 || $ftype == 128);
      my $var = $fields->Item($fldn)->Name();
      my $val = $fields->{$var}->Value();
#      print "fillIndexEntry: $var => ".(ref($val) && $val->can('Date') ? $val->Date('yyyy-MM-dd').' '.$val->Time('hh:mm:ss tt') : $val || '')."\n";
      if ( $var =~ /^(?:JobReportId|FromPage|ForPages)$/i ) {
         $IndexEntry->{$var} = $val || '';
      }
      else {
         $val = ($val->can('Date') ? $val->Date('yyyy-MM-dd') : '')." ".($val->can('Time') ? $val->Time('HH:mm:ss tt') : '') if ref($val);
         push @{$IndexEntry->{Columns}}, { colname => $var, content => $val || '', };
      }
    }
    push @{$IndexEntries}, $IndexEntry;
    $dbr->MoveNext();
  }
#  $main::dolog && i::logit("fillIndexEntries IndexEntries: ", Dumper($IndexEntries)) if scalar(@{$IndexEntries});

  return $IndexEntries;
}

#sub _HTTP_Server {
#  use HTTP::Daemon;
#  use HTTP::Status;

#  my ($listenaddr, $listenport) = (shift, shift);
#  $listenaddr = c::getValues('ListenAddr') unless $listenaddr;
#  $listenport = c::getValues('ListenPort') unless $listenport;
#  $listenaddr = $ENV{COMPUTERNAME} unless $listenaddr;
#  $listenport = 60080 unless $listenport;

#  my $thisserver = HTTP::Daemon
#    ->new (LocalAddr => $listenaddr, LocalPort => $listenport, Reuse => 1)
#      ;

#  $main::WSDL =  XReport::SOAP::buildWSDL($thisserver->url(), $main::Application->{ApplName} );
#  print "Contact to SOAP server at ", $thisserver->url(), "\n";
  
  
#  while (my $c = $thisserver->accept) {
#    while (my $r = $c->get_request) {
#      (my $getreq = $r->url()) =~ s/^.*\?(.*)$/$1/;
#      print "Processing client request url: ", $thisserver->url(), " method: ", $r->method, " req: $getreq\n";
#      my $resp = '';
#      my $soapaction = $r->header('SOAPAction');
#      my $httpreqid = $r->header('REQUEST_ID');
#      if ($r->method eq 'GET' && $getreq =~ /^WSDL$/i ) {
#   $resp = HTTP::Response->new(RC_OK);
#   #      $resp->content_type('text/xml');
#   $resp->content($main::WSDL);
#   #     $c->send_response($resp);
#      }
#      elsif ($r->method eq 'POST') {
#   $resp = HTTP::Response->new(RC_BAD_REQUEST) unless $soapaction;
#      my ($pkgref, $method) = ($r->header('SOAPAction') =~ /^("?)(.*)[#\/\@](.*)\1$/)[1,2];
#   $resp = HTTP::Response->new(RC_BAD_REQUEST) if $pkgref && $pkgref ne 'XReport::'.$main::Application->{ApplName};
    
    
#   my $class = 'XReport::'.$main::Application->{ApplName};
#   my $msub = $class->can($method);
#   print "SOAP Action: ", $r->header('SOAPAction'), " Data: ", length($r->content), " pkgref: $pkgref method: $method sub: ", ref($msub), "\n";
#   $pkgref = 'printiface' unless $pkgref;
#   $resp = (ref($msub) eq 'CODE' 
#        ? HTTP::Response->new(&$msub($method, $httpreqid, XReport::SOAP::parseSOAPreq($r->content())))
#        : HTTP::Response->new(RC_NOT_IMPLEMENTED)
#       );
#      }
#      else {
#   $resp = HTTP::Response->new(RC_BAD_REQUEST);
#      }
#      print "sending response\n";
#      $resp->content_type('text/xml');
#      $c->send_response($resp);
#    }
#    UNIVERSAL::isa($c, 'shutdown') ? $c->shutdown(2) : $c->close(); 
#    undef $c;
#  }
  
#}

sub methods {
  my ($rclass, $types) = @_;
  $rclass = ref $rclass || $rclass;
  $types ||= '';
  my %classes_seen;
  my %methods;
  my @class = ($rclass);
  
  no strict 'refs';
  while (my $class = shift @class) {
    next if $classes_seen{$class}++;
    unshift @class, @{"${class}::ISA"} if $types eq 'all';
    # Based on methods_via() in perl5db.pl
    for my $method (grep {not /^[(_]/ && $_ ne 'import' && $_ ne 'can' and defined &{${"${class}::"}{$_}} } keys %{"${class}::"}) { 
        $methods{$method} = wantarray ? undef : $class->can($method);
    }
  }
#  print "Methods supported by $rclass: ", join('::', keys %methods), "\n";  
  wantarray ? keys %methods : \%methods;
}

sub _GET_WSDL {
    $main::dolog && i::logit("Serving WSDL request") ;
     $main::Response->Status('200 OK');
     $main::Response->ContentType('text/xml');
     $main::Response->Write( &{$main::Application->{wsdlsub}}($main::Request->{requri}));
}

sub _GET_SHOWSERVER {
    $main::dolog && i::logit("Service ", $main::Request->REQUEST_ID(), "Serving Showserver request") ;
    my $fcgienv = $main::Request->ServerVariables();
    my $title = "FastCGI ShowServer Page";
    my $glblreqenv = $FCGI::global_request->GetEnvironment();
    $main::Response->Status('200 OK');
    $main::Response->ContentType('text/html');
    return $main::Response->Write( CGI::start_html(-title => $title) 
              ,CGI::hr(),CGI::h1($title)
              ,CGI::hr(),CGI::a({name=>"TOC"}
              ,CGI::ul([CGI::a({-href=>"#conninfo"},"Connection Infos")
                      , CGI::a({-href=>"#statinfo"},"Server Status")
                      , CGI::a({-href=>"#mainenv"},"fastcgi process environment")
                      , CGI::a({-href=>"#fcgienv"},"REQUEST Enviroment")
                      , CGI::a({-href=>"#applset"},"Application settings")
                      , CGI::a({-href=>"#reqhset"},"Request Handler contents")
                      , CGI::a({-href=>"#CGIenv"},"FCGI global req env")
                      , CGI::a({-href=>"#perlns"},"Perl Name Space")
                      ] ))
              ,CGI::hr(),CGI::a({-name=>'conninfo'},CGI::span({-style=>"font-size: 25px; color: red;"},CGI::em("Info from server"))
                        ,CGI::center(CGI::a({-href=>"#TOC"},"Back To top"))
              ,CGI::hr(),CGI::blockquote("This is coming from a perl FastCGI SOAP server"
                        ,CGI::br(),"Running on ", CGI::em($fcgienv->{SERVER_NAME}), " to ", CGI::em($fcgienv->{REMOTE_HOST})
                        ,CGI::br(),"This is connection number $main::Application->{reqcount}")),CGI::br()
              ,CGI::hr(),CGI::a({-name=>'statinfo'},CGI::span({-style=>"font-size: 25px; color: red;"},CGI::em("Current Status"))
                        ,CGI::center(CGI::a({-href=>"#TOC"},"Back To top"))
              ,CGI::hr(),CGI::blockquote("Logging is ".($main::dolog ? "ON $main::currlogfn " : 'OFF')
                        ,CGI::br(),"Tracing is ".($main::dotrace ? "ON $main::currtracefn" : 'OFF')
                        ,CGI::br(),"Logging is ".($main::log ? '' : 'not ')."currently active".( $main::log ? ' - '.ref($main::log) : '' )
                        ,CGI::br(),"tracing is ".($main::trace ? '' : 'not ')."currently active")),CGI::br()
              ,CGI::hr(),CGI::a({-name=>'mainenv'},CGI::span({-style=>"font-size: 25px; color: red;"},CGI::em("Main ENV"))
                        ,CGI::center(CGI::a({-href=>"#TOC"},"Back To top"))
              ,CGI::hr(),CGI::pre(XReport::SOAP::Dumper($main::origenv))),CGI::br()
              ,CGI::hr(),CGI::a({-name=>'fcgienv'},CGI::span({-style=>"font-size: 25px; color: red;"},CGI::em("FCGI ENV"))
                        ,CGI::center(CGI::a({-href=>"#TOC"},"Back To top"))
              ,CGI::hr(),CGI::pre(XReport::SOAP::Dumper($fcgienv))),CGI::br()
              ,CGI::hr(),CGI::a({-name=>'applset'},CGI::span({-style=>"font-size: 25px; color: red;"},CGI::em("Application Settings"))
                        ,CGI::center(CGI::a({-href=>"#TOC"},"Back To top"))
              ,CGI::hr(),CGI::pre(XReport::SOAP::Dumper($main::Application))),CGI::br()
              ,CGI::hr(),CGI::a({-name=>'reqhset'},CGI::span({-style=>"font-size: 25px; color: red;"},CGI::em("Request Handler"))
                        ,CGI::center(CGI::a({-href=>"#TOC"},"Back To top"))
              ,CGI::hr(),CGI::pre(XReport::SOAP::Dumper($main::Request))),CGI::br()
              ,CGI::hr(),CGI::a({-name=>'CGIEnv'},CGI::span({-style=>"font-size: 25px; color: red;"},CGI::em("CGI global req env"))
                        ,CGI::center(CGI::a({-href=>"#TOC"},"Back To top"))
              ,CGI::hr(),CGI::pre(XReport::SOAP::Dumper( CGI->new() ))),CGI::br()
              ,CGI::hr(),CGI::a({-name=>'perlns'},CGI::span({-style=>"font-size: 25px; color: red;"},CGI::em("Perl Namespace"))
                        ,CGI::center(CGI::a({-href=>"#TOC"},"Back To top"))
              ,CGI::hr(),CGI::pre(XReport::SOAP::Dumper( {'Perl Includes' => \@INC } )),CGI::br()
              ,CGI::hr(),CGI::pre(XReport::SOAP::Dumper( XReport::Util::_PerlNameSpace(%main::, 'main::') ))),CGI::br()
              ,CGI::end_html()
              );
}

BEGIN {
  *XReport::SOAP::buildWSDL = *XReport::SOAP::FCGI::buildWSDL;
  *XReport::SOAP::buildClause = *XReport::SOAP::FCGI::buildClause;
  *XReport::SOAP::fillIndexEntries = *XReport::SOAP::FCGI::fillIndexEntries;
  *main::debug2log = *i::traceit;
  *main::_PerlNameSpace = *XReport::Util::_PerlNameSpace;
  *main::_strip = *XReport::Util::_strip;
  *main::_GET_SHOWSERVER = *XReport::SOAP::FCGI::_GET_SHOWSERVER;
  *main::_GET_WSDL = *XReport::SOAP::FCGI::_GET_WSDL;
}

__PACKAGE__;
