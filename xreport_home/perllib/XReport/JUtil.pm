#######################################################################
# @(#) $Id: JUtil.pm 2159 2007-09-27 14:56:17Z MPEZZI $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
package XReport::JUtil;

use strict;
use Carp;

use Digest::MD5;
use XReport;
use XReport::DBUtil;

use vars qw($VERSION @ISA @EXPORT_OK);
our ($VERSION, @ISA, @EXPORT, @EXPORT_OK);

require Exporter;

@ISA = qw(Exporter);

@EXPORT = qw(
	    );
@EXPORT_OK = qw( );
$VERSION = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r }; 


our $tblName = 'tbl_JobCtlInput';

sub new {
  my $ClassName = shift;
  my $self = {'dbu' => new XReport::DBUtil(@_) };
  bless $self, $ClassName;
  return $self;
}

sub JCExist {
  my ($this, $stream) = (shift, shift);
  my $dbu = $this->{'dbu'};
  my $md5 = Digest::MD5::md5_hex($stream);
  my $r = $dbu->dbExecute("SELECT COUNT(*) AS RCOUNT FROM $tblName WHERE JobCtlHash = '" . 
		    $md5 . "'");
#		    unpack("H*", $md5) . "'");
  if ($r->GetFieldsValues('RCOUNT')) {
    my $r = $dbu->dbExecute("SELECT JobReportID AS ID FROM $tblName WHERE JobCtlHash = '" . 
		    $md5 . "'");
    return $r->GetFieldsValues('ID')
    
  }
  return undef;
}

sub JCAdd {
  my ($this, $id, $stream) = (shift, shift, shift);
  my $dbu = $this->{'dbu'};
  my $md5 = Digest::MD5::md5_hex($stream);

  my $r = $dbu->dbExecute("INSERT INTO $tblName (JobCtlHash, JobReportId) VALUES ( '" . 
			  $md5 . "', $id)");
#			  unpack("H*", $md5) . "', $id)");
  my $dbc = $dbu->{'dbc'};
  
  return undef unless ($r and ($dbc->Errors()->Count() == 0));

  return 1;

}

sub Errors {shift->{'dbu'}->{'dbc'}->Errors(@_)};

1;
