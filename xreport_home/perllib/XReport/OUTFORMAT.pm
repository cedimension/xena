package XReport::OUTFORMAT;

sub Data01 {
  
  return $_[0] unless $_[0] =~ /^0+[\.\/\-]0+[\.\/\-]0+$/;
  return ' ';
}

sub DDMMYY {
  return $_[0];
}

sub YYMMDD {
  return $_[0];
}

sub MMDDYY {
  return $_[0];
}

sub COMMA {
  return $_[0];
}

sub STRIP {
  return $_[0];
}

sub CHAR {
  return $_[0];
}

sub UNDERSCORE {
  return $_[0];
}

sub DECIMALCOMMA {
  my ($hdr, $int, $dec) = (( $_[1] ? sprintf($_[1], $_[0]) : $_0) =~ /^([^\d]*)(\d*)(?:\.(.*))?$/);
  my $st = length($int)%3;
  my @th = unpack(($st ? "a$st" : '') . (length($int) > $st ? "(a3)*" : ''), $int);
  
  return (length($hdr) > $#th ? substr($hdr, $#th) : '').join('.', @th).($dec ? ",$dec" : '');
}

1;
