
package XReport::logger::udp_server;

use warnings;
use strict;

use POE;
use IO::Socket::INET;

use constant DATAGRAM_MAXLEN => 1024;

my $t2;

POE::Session->create(
  inline_states => {
    _start       => \&server_start,
    get_datagram => \&server_read,
  }
);

POE::Kernel->run();
exit;

sub switch_log {
   close(LOG); my $t = shift || time(); my $dday = 24*3600;  

   $t2 = $t - ($t % $dday) - 7200;
   
   my @t2 = reverse((localtime($t2))[0..5]); 
   $t2[0] += 1900; 
   $t2[1] += 1; 
   for (1..5) {
     $t2[$_] = substr("0$t2[$_]", -2);
   }
   my $date = join("", @t2[0..2]);

   open(LOG, ">>d:/logs/xreport/zzudp_server.$date.log"); 
   
   my $select = select(LOG); $| = 1; select($select); $t2 += $dday;
}

sub server_start {
    my $kernel = $_[KERNEL];

    my $socket = IO::Socket::INET->new(
        Proto     => 'udp',
        LocalPort => '8675',
    );

    die "Couldn't create server socket: $!" unless $socket; 

    $kernel->select_read( $socket, "get_datagram" ); switch_log();
}

sub server_read {
    my ( $kernel, $socket ) = @_[ KERNEL, ARG0 ];

    my $remote_address = recv( $socket, my $message = "", DATAGRAM_MAXLEN, 0 );
    
    return unless defined $remote_address;

    my ( $peer_port, $peer_addr ) = unpack_sockaddr_in($remote_address);

    my $human_addr = inet_ntoa($peer_addr);

    switch_log() if time() > $t2;

    print LOG "$human_addr : $peer_port : $message\n";
}

1;
