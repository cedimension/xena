
package XReport::logger::udp_client;

use strict;
#use warnings;

use IO::Socket::INET;

use Time::HiRes qw (time sleep);

sub log {
  my ($self, $message, $class, $severity) = @_; 
 
  my (
    $socket, $ident
  ) = 
  @{$self}{qw(socket ident)};

  $socket->send("$ident : ". time(). " : $message");
}

sub new {
  my ($class, $ident, $PeerAddr, $PeerPort) = @_; my $self;

  $PeerAddr ||= "localhost"; $PeerPort ||= 8675;

  my $socket = IO::Socket::INET->new(
    PeerAddr  => $PeerAddr,
    PeerPort  => $PeerPort,
    Proto     => 'udp',
    Broadcast => 1 
  )
  or die "INET NEW ERROR DETECTED : $@\n";

  $self = { 
    PeerAddr => $PeerAddr, PeerPort => $PeerPort, ident => $ident, socket => $socket, actions => {}
  };
  
  bless $self, $class;
}

1;

__END__

sub begin_action {
  my ($self, $action, @largs) = @_; my $actions = $self->{actions};

  push @{$actions->{$action}}, ['begin', time(), @largs];
}

sub end_action {
  my ($self, $action, @largs) = @_; my $actions = $self->{actions}; my $laction = $actions->{$action};

  return if !$laction; my $le = $laction->[-1]; return if !$le or $le->[0] ne "begin"; 
  
  pop @$laction; 
  
  my (undef, $t1) = splice(@$le, 0, 2);

  unshift @{$actions->{$action}}, ['el', time()-$t1, @$le, @largs];
}

sub flush {
  my $self = shift; my $actions = $self->{actions}; my $serialized = "\n";

  for my $action (keys(%$actions)) {
    $serialized .= "$action ==> ";
    my $la = $actions->{$action};
    for (@$la) {
      $serialized .= join(",", @$_);
    }
    $serialized .= "\n"; 
  }

  $self->log($serialized); print $serialized;
}

__END__
package main;

use Time::HiRes qw (time sleep);

my $logger = XReport::logger::udp_client->new();

$logger->begin_action("primaazione", pippo => 'pluto');
sleep(3.2);
$logger->end_action("primaazione", pippone => 'plutone');


$logger->flush();




