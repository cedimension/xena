package XReport::Deliver::WIN32SPOOL;

=h2 WIN32SPOOL Delivery module

=cut

use POSIX qw(strftime);
sub sendfile {
    my $class = shift;
    my $info = { @_ };
    $main::verbose and i::warnit("FTP processing open to $info->{remote}");
    my $destparms = $info->{remote};
    my $outfname = $info->{'local'};
    
    my $self = {};
    @{$self}{qw(outbuff outlen XferOutBytes local inputfh inbuffcnt)} = ('', 0, 0, @{$info}{qw(local inputfh inbuffcnt)});
#   $self->{inbuffcnt} = 0 unless defined($self->{inbuffcnt});
    
    bless $self, $class;

    my ($login, $rest) = ( $destparms =~ /\@/ ? split /\@/, $destparms, 2 : (undef, $destparms));
    my ($user, $pass) = ( $login && $login =~ /\:/ ? split /\:/, $login, 2 : ($login, undef));
    
    my ($host, $prinfo) = ($rest =~ /:/ ? split /:/, $rest, 2 : ($rest, undef));
    my ($gsdevice, $prname) = ($prinfo =~ /:/ ? split /:/, $rest, 2 : ('pswriter', $prinfo));

#    $main::verbose and i::warnit("Begin delivery to $ftphost login $ftpuser");
    my $lpcmd = "\"".$main::Application->{XREPORT_HOME}."\\sbin\\spool.exe\" \"$prname\" \"$outfname\"";
    warn "LPCMD: ", $lpcmd, "\n";
    system $lpcmd;
    
}

1;