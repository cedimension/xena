package XReport::Deliver::FTP;

=h2 FTP Delivery module

=cut

use POSIX qw(strftime);
use Net::FTP;
use Data::Dumper;
sub _parse_FTP_request {
    my $info = shift;
    my $config = shift;
	$main::verbose and warn "parsing FTP Request - ", Dumper($info), "\n";
    my ($destparms, $localfqn) = @{$info}{qw(remote local)};
    my ($login, $rest) = ( $destparms =~ /\@/ ? split /\@/, $destparms, 2 : (undef, $destparms));
    my ($ftpuser, $ftppass) = ( $login && $login =~ /\:/ ? split /\:/, $login, 2 : ($login, undef));
    
    my ($ftphost, $remotefqn) = ($rest =~ /:/ ? split /:/, $rest, 2 : ($rest, undef));
    $ftpuser = $info->{ftpuser} if (!$ftpuser && exists($info->{ftpuser}) && $info->{ftpuser});
    $ftpuser = $config->{user}  if (!$ftpuser && exists($config->{user}) && $config->{user});

    $ftppass = $info->{ftppass}    if (!$ftppass && exists($info->{ftppass}) && $info->{ftppass});
    $ftppass = $config->{password} if (!$ftppass && exists($config->{password}) && $config->{password});

    $ftphost = $info->{ftphost} if (!$ftphost && exists($info->{ftphost}) && $info->{ftphost});
    $ftphost = $config->{host}  if (!$ftphost && exists($config->{ftphost}) && $config->{ftphost});
    die "FTP Server address not specified in \"$destparms\"" unless $ftphost;
    
    return { localfqn => $localfqn, ftphost => $ftphost
           , remotefqn => $remotefqn, ftpuser => $ftpuser, ftppass => $ftppass };
}

sub new {
    my $class = shift;
    my $info = { @_ };

    my $config = $XReport::cfg->{ftp};
    $config = {} unless ref($config);

    $main::verbose and i::warnit("FTP processing init to $info->{remote}");
    my $self = {};
    @{$self}{qw(outbuff outlen XferOutBytes local inputfh inbuffcnt)} =
                                ('', 0, 0, @{$info}{qw(local inputfh inbuffcnt)});
#   $self->{inbuffcnt} = 0 unless defined($self->{inbuffcnt});
    $self->{sizeinp} = ( $self->{local} && -e $self->{local} ? -s $self->{local} : (32760 * 16)); 
    $self->{chunksize} = ($info->{chunksize} || (127*1024)); 
    
    bless $self, $class;

    my ($localfqn, $ftphost, $remotefqn, $ftpuser, $ftppass) = 
       @{_parse_FTP_request($info, $config)}{qw(localfqn ftphost remotefqn ftpuser ftppass)};
    
    my $encoding = $info->{encoding} || 'E';
    my $outmode = $info->{outmode} || 'R'; 
    
    @{$self}{qw(ftphost ftpuser ftppass encoding outmode remotefqn)} 
                             = ( $ftphost, $ftpuser, $ftppass, $encoding, $outmode, $remotefqn);
# (?:\/(.+?))?\/([^\/]+)$/
    my $ftpc = Net::FTP->new($ftphost, Debug => 1) or die "Cannot connect to server: $@";
    $self->{XferStartTime} = strftime("%Y-%m-%d %H:%M:%S.001", localtime);
    $ftpc->quot('SYST');
    ( $self->{rmttype} ) = map { uc($_) } ($ftpc->message() =~ /^([^\s]+)\s/);
    $ftpc->quit() or die "Can't quit ftp connections: $!";
    my $initproc = $self->can('_init'.$self->{rmttype} );
    $initproc = $self->can('_initgenascii') unless $initproc;
    return &{$initproc}($self) if $initproc;
    return $self;   
}

sub open {
    my $class = shift;
    my $self = $class->new(@_) unless ref($class);
    return &{$self->{openproc}}($self);
}

sub close {
    my $self = shift;
    return &{$self->{closeproc}}($self, @_);
}

sub _writebuff {
    my $self  = shift;
    return 0 unless 
            my $writelen = length($self->{outbuff});
#    $self->{ftpd}->write($self->{outbuff}, $writelen)   
    $self->{ftpd}->send($self->{outbuff})   
                               or die "Error during store \"'$self->{local}'\" - $!";
#    $self->{datacopy}->write($self->{outbuff}, $writelen) if $self->{datacopy};
    $self->{XferOutBytes} += $writelen;
    $main::veryverbose 
      and i::warnit(ref($self)." $writelen bytes sent ($self->{XferOutBytes} so far) to $self->{ftphost}");
    @{$self}{qw(outbuff outlen)} = ('', 0);
    return $writelen;
}


sub write {
    my $self = shift;
    return &{$self->{writeproc}}($self, @_);
}

=MVS 3390 Disk Tracks occupancy

The optimal blksize for a 3390 is 27998 - a VB record at max can be 27994
BLKSIZE=0 specification instruct the allocation routines of MVS to compute 
the optimal BLKSIZE for the LRECL specified in the target DISK (e.g. 3390 ) 

=cut

sub _tracks_MVS_3390 {
    my $fsize = shift;  
    my $pritracks = ($fsize + ( 27994 - ( $fsize % 27994 )) ) / 27994;
    $pritracks = (($pritracks + ($pritracks % 2)) / 2) + 1;
    return $pritracks; 
} 

sub _blocks_MVS_3390 {
    my $fsize = shift;  
    my $totbytes = ((($fsize + ( 40 - ( $fsize % 40 )) ) / 40) * 2) + $fsize; 
    my $blocks = ($totbytes + ( 27994 - ( $totbytes % 27994 )) ) / 27994;
    return $blocks; 
} 

sub _buildMVSFileName {
    (my $remotename 
        = join('.', map { sprintf('%-8.8s', $_) } split /[\/\.]/, $_[0])) =~ s/\s//g;
    return "'".(length($remotename) > 44 ? substr($remotename, 0, 44) : $remotename)."'";   
}

=IBM MVS FTP 
    MODE B is intended a stream composed by a collection of records prefixed by X'80' followed
    by 2 bytes representing the record len that follows. The last record is prefixed by x'40'
    
    STRU R is intended a stream composed by a collection of records suffixed by x'FF01' and with 
    x'FF' inside records represented by a double x'FF'. EOF must be flagged by x'FF02'
=cut 

sub _openMVS {
    my ($self) = (shift);
    my ($ftphost, $ftpuser, $ftppass, $enc, $outmode, $localfqn, $remotefqn) 
                       = (@{$self}{qw(ftphost ftpuser ftppass encoding outmode local remotefqn)});   
     
    $main::verbose 
       and i::warnit("Connecting to FTP HOST $ftphost to deliver \""
                     , $self->{local}, "\" into ", $self->{remotename}, "($self->{numblocks} blocks)");
    $self->{ftpc} = Net::FTP->new($ftphost, Debug => 1) or die "Cannot connect to server: $@";
    i::logit("FTP Connection initialized - ".(split /\n/,$self->{ftpc}->message())[0]);            
    $self->{ftpc}->login($ftpuser,$ftppass) or die "Cannot login.", $self->{ftpc}->message;
    foreach ( @{$self->{ftpquotes}} ) { $self->{ftpc}->quot(@{$_}) }
    $self->{ftpd} = $self->{ftpc}->stor($self->{remotename}) 
                or die "Cannot store - " . $self->{ftpc}->message();
    i::logit("FTP Data transfer initialized - ".(split /\n/,$self->{ftpc}->message())[0]);            
    $self->{inbuff} = '';
    $self->{lastrec} = 0;                       
    return $self;
} 

sub _initMVS {
	my $self = shift;
    my ($ftphost, $ftpuser, $ftppass, $enc, $outmode, $localfqn, $remotefqn) 
                       = @{$self}{qw(ftphost ftpuser ftppass encoding outmode local remotefqn)};
    $self->{numblocks} = _blocks_MVS_3390(-s $localfqn);                      
    $main::verbose and i::warnit("Initialize FTP delivery to MVS $ftphost ENC: $enc MODE: $outmode");
    die "Unable to find write routine for encoding $enc and mode $outmode" 
          unless $self->{writeproc} = $self->can('_write_MVS_'.$enc);
    die "Unable to find close routine for encoding $enc and mode $outmode" 
          unless $self->{closeproc} = $self->can('_close_MVS_'.$enc);
    die "Unable to find record routine for encoding $enc and mode $outmode" 
          unless $self->{recproc} = $self->can('_format_REC_'.$outmode);
    die "Unable to find flush routine for encoding mode $outmode" 
          unless $self->{flushproc} = $self->can('_flush_BUFF_'.$outmode);
    die "Unable to find open routine for encoding $enc and mode $outmode" 
          unless $self->{openproc} = $self->can('_openMVS');
    $self->{remotename} = _buildMVSFileName($remotefqn);    
#    $self->{datacopy} = new IO::File(">C:\\users\\xreport\\testBundle\\$remotename.modeb");
    i::logit("Initialize FTP delivery to MVS type MVS");
    $self->{ftpquotes} 
    = [ ['SITE', "BLocks VARrecfm RECFM=VBA LRECL=27994 BLOCKsize=0 PRI=$self->{numblocks} SEC=50 DSNTYPE=LARGE"] ];
    push @{$self->{ftpquotes}}, ['TYPE', ($self->{encoding} eq 'E' ? 'I' : 'A') ] ;
    push @{$self->{ftpquotes}}, ($self->{outmode} eq 'R' ? ['STRU', 'R'] : ['MODE', 'B']); 
    $main::veryverbose && push @{$self->{ftpquotes}}, ["STAT"];

    return $self;          
}

sub _flush_BUFF_R {
    my $self = shift;
    $self->{outbuff} .= "\xFF\x02";
    $self->{outlen} += 2; 
    return $self->{outbuff};
}

sub _flush_BUFF_B {
    my $self = shift;
    return "\x40\x00\x00" unless length($self->{outbuff});
    $self->{outlen} += 3; 
    substr($self->{outbuff}, $self->{lastrec}, 1) = "\x40";
    return $self->{outbuff};
}

sub _close_MVS_E {
    my $self  = shift;
    $main::veryverbose && i::warnit( "Closing now MVS_E_$self->{outmode} FTP Conn - lastrec: ".$self->{lastrec}
                                   . " CMD Conn: ".ref($self->{ftpc}) );
    die "Not empty cache found LEN:(".length($self->{inbuff}).") DATA: ".unpack('H*', $self->{inbuff})
        if length($self->{inbuff});                                   
    return undef unless $self->{ftpc};
    i::logit("FTP data Transfer interrupted ". (split /\n/,$self->{ftpc}->message())[0]) unless ( $self->{ftpd} );
    if ( $self->{ftpd} ) {
        &{$self->{flushproc}}($self);
        $main::veryverbose && i::warnit( "Writing out last MVES_E_R - outbuff: ".length($self->{outbuff})
           . " data: " . unpack('H*', substr($self->{outbuff}, (length($self->{outbuff}) > 20 ? -20 : 0)))
                );
        $self->_writebuff();
        $self->{ftpd}->close() or die "Can't close ftp data connections: $!";
        i::logit("FTP data Transfer completed ". join(' - ', split(/\n/,$self->{ftpc}->message())) );
    } 
    $self->{ftpc}->quit() or die "Can't quit ftp connections: ".(split /\n/,$self->{ftpc}->message())[0];
    $self->{XferEndTime} = strftime("%Y-%m-%d %H:%M:%S.001", localtime);
    return $self;
}

sub _format_REC_R {
	(my $rowdata = $_[0] ) =~ s/\xFF/\xFF\xFF/g;
     return $rowdata."\xFF\x01";        
}

sub _format_REC_B {
     return "\x80".pack('n/a*', $_[0]);
}

sub _write_MVS_E {
	my ($self, $buff) = (shift, shift);
	$self->{inbuffcnt} += 1;

    my $inputfh = $self->{inputfh};
    $main::veryverbose and i::warnit(ref($self)."::MVS_E"
    . " Processing now buffer $self->{inbuffcnt}(".length($buff)." bytes) ending at ".$inputfh->tell()." of INFILE");

    $main::veryverbose and i::warnit( " LASTREC(".$self->{lastrec}.") TOTINREC: $self->{numrecs} "
            . "BUFF(".length($buff)."): ". unpack('H30', $buff)
            . " INBUFF(".length($self->{inbuff})."): ". unpack('H30', $self->{inbuff})
            . " OUTBUFF(".length($self->{outbuff})."): ". unpack('H30', $self->{outbuff})
            );
    my @inrows = unpack('(a2 X2 n X2 n/a)*', $self->{inbuff}.$buff);
    $self->{inbuff} = '';
    $main::veryverbose and i::warnit( "REC Array will add ".scalar(@inrows)." elements to $self->{numrecs} "
            . " rec[-3]: ". unpack('H*', $inrows[-3])
            . " rec[-1]($inrows[-2]/".length($inrows[-1])."): ". unpack('H32', $inrows[-1])
            );

    while (scalar(@inrows) ) {
        my ($lpfx, $rowlen, $rowdata) = splice(@inrows, 0, 3);
        die "incongruent len and record size len: $rowlen size: ".length($rowdata) 
                if (scalar(@inrows) && (length($lpfx) < 2 || $rowlen != length($rowdata) ) );
        if ( length($lpfx) < 2 || $rowlen != length($rowdata) ) {
               $self->{inbuff} = $lpfx;
               $self->{inbuff} .= $rowdata if length($lpfx) == 2;
               last;
        }
        next if ( $lpfx eq "\x00\x00");
        $self->{lastrec} = length($self->{outbuff});
        my $outrec = &{$self->{recproc}}($rowdata);
        $self->{numrecs}++;
        $self->_writebuff() if ( ($self->{outlen} + length($outrec)) > $self->{chunksize} );
        $self->{outbuff} .= $outrec;
        $self->{outlen} += length($outrec); 
    }	

    return length($buff); 
}

sub _sendfile_MVS {
    my $self = shift;
    if ( ref($self) ) {
    	my $local = {@_}->{local};
    	$self->{local} = $local if $local;
    }
    else {
       $self = $self->new(@_);
    }
    my ($localfqn, $ftphost, $remotefqn, $ftpuser, $ftppass) = 
              @{$self}{qw(local ftphost remotename ftpuser ftppass)};

    my @ftpclist = ( "user $ftpuser $ftppass", 'quote PASV', 'bin'
                   , (map { 'quote '. join(' ', @{$_}) } @{$self->{ftpquotes}} ) 
                   , 'STAT'
                   , "put $localfqn $remotefqn"
                   , 'quit');
    
    my $wincmd = '( echo '. join(' && echo ', @ftpclist).') | ftp -d -n -v '.$ftphost;
#    i::logit("Now invoking: $wincmd");
    system "$wincmd";
}

sub _openCEREPORT {
    my ($self, $binfo) = (shift, shift);
    my ($ftpuser, $ftppass, $ftphost, $remotefqn) =  (@{$binfo}{qw(ftphost ftpuser ftppass)}, shift);   
    die "Destination type \"CEREPORT\" not handled" 
} 

sub _opengenascii {
    my ($self, $binfo) = (shift, shift);
    my ($ftpuser, $ftppass, $ftphost, $remotefqn) =  (@{$binfo}{qw(ftphost ftpuser ftppass)}, shift);   
    die "Destination type \"genascii\" not handled" 
} 

1;
