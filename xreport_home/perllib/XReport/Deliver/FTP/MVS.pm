package XReport::Deliver::FTP::MVS;

use base 'XReport::Deliver::FTP';

sub _init {
    my $self = shift;
    my ($ftphost, $ftpuser, $ftppass, $enc, $outmode, $localfqn, $remotefqn) 
                       = @{$self}{qw(ftphost ftpuser ftppass encoding outmode local remotefqn)};
    $self->{numblocks} = _blocks_MVS_3390(-s $localfqn);                      
    $main::verbose and i::warnit("Initialize FTP delivery to MVS $ftphost ENC: $enc MODE: $outmode");
    die "Unable to find write routine for encoding $enc and mode $outmode" 
          unless $self->{writeproc} = $self->can('_write_MVS_'.$enc);
    die "Unable to find close routine for encoding $enc and mode $outmode" 
          unless $self->{closeproc} = $self->can('_close_MVS_'.$enc);
    die "Unable to find record routine for encoding $enc and mode $outmode" 
          unless $self->{recproc} = $self->can('_format_REC_'.$outmode);
    die "Unable to find flush routine for encoding mode $outmode" 
          unless $self->{flushproc} = $self->can('_flush_BUFF_'.$outmode);
    die "Unable to find open routine for encoding $enc and mode $outmode" 
          unless $self->{openproc} = $self->can('_openMVS');
    $self->{remotename} = _buildMVSFileName($remotefqn);    
#    $self->{datacopy} = new IO::File(">C:\\users\\xreport\\testBundle\\$remotename.modeb");
    i::logit("Initialize FTP delivery to MVS type MVS11");
    $self->{ftpquotes} 
    = [ ['SITE', "BLocks VARrecfm RECFM=VBA LRECL=27994 BLOCKsize=0 PRI=$self->{numblocks} SEC=50 DSNTYPE=LARGE"] ];
    push @{$self->{ftpquotes}}, ['TYPE', ($self->{encoding} eq 'E' ? 'I' : 'A') ] ;
    push @{$self->{ftpquotes}}, ($self->{outmode} eq 'R' ? ['STRU', 'R'] : ['MODE', 'B']); 
    push @{$self->{ftpquotes}}, ['SITE', "FILEtype=SEQ"]; 
    $main::veryverbose && push @{$self->{ftpquotes}}, ["STAT"];

    return $self;          
}

sub _sendfile {
    my $self = shift;
    if ( ref($self) ) {
        my $local = {@_}->{local};
        $self->{local} = $local if $local;
    }
    else {
       $self = SUPER::new($self, @_);
    }
    my ($localfqn, $ftphost, $remotefqn, $ftpuser, $ftppass) = 
              @{$self}{qw(local ftphost remotename ftpuser ftppass)};

    my @ftpclist = ( "user $ftpuser $ftppass"
                   , 'quote PASV'
                   , 'bin'
                   , (map { 'quote '. join(' ', @{$_}) } @{$self->{ftpquotes}} ) 
                   , 'STAT'
                   , "put $localfqn $remotefqn"
                   , 'quit');
    
    my $wincmd = '( echo '. join(' && echo ', @ftpclist).') | ftp -d -n -v '.$ftphost;
    system "$wincmd";
}

1;
