package XReport::OUTPUT::OPlain;

use strict;
use Carp;
use FileHandle;

use Symbol;

sub Open {
  my $self = shift; my $OUTPUT; my $fileName = $self->{fileName};
  
  $OUTPUT = new FileHandle $fileName, "w"
  or croak("XReport::OUTPUT::IPlain Open ERROR $fileName $!");
  binmode($OUTPUT);

  $self->{OUTPUT} = $OUTPUT;

  return $OUTPUT;
}

sub Close {
  my $self = shift; my $OUTPUT = $self->{OUTPUT};
  return close($OUTPUT);
}

sub write {
  my $self = shift; my $OUTPUT = $self->{OUTPUT};
  return syswrite($OUTPUT, $_[0], $_[1]);
}


sub print {
  my $self = shift; my $OUTPUT = $self->{OUTPUT};
  map {syswrite($OUTPUT, $_)} @_;
}

sub putLine {
  my $self = shift;
  return &{$self->{putLine}}($self);
}

sub new {
  my ($className, $fileName, $putLine) = @_; my $self = {};
  $self->{fileName} = $fileName;
  $self->{putLine} = $putLine;
  bless $self;
}

1;
