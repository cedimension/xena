package XReport::OUTPUT::OStream;

use strict;
use Carp;
use Convert::EBCDIC;
#use Convert::IBM390;

use XReport::OUTPUT::OZlib;
use XReport::OUTPUT::OPlain;

my $translator = new Convert::EBCDIC;

#------------------------------------------------------------
sub putPsfLine {
  my ($self, $line) = (shift, shift);
  $self->write(pack("n",length($line)) . $line);
}

sub putPsfPage {
  my ($self, $page) = (shift, shift);
  #map { putPsfLine($self, $_) } @$page;
  map { $self->write(pack("n",length($_)).$_) } @$page;
}

sub putAfpLine {
  my $self = shift;

  my $trp = unpack("H6",substr($_[0],0,3));
  my $trd = substr($_[0],6);

  if ($trp eq "d3abcc") { # Invoke Medium Map (IMM)
    my $mm = $translator->toascii(substr($trd,0,8));
    $mm =~ s/ +$//;
    $self->write("($mm) a.setMediumMap\n") if $trd ne "";
  }
  elsif ($trp eq "d3abca") { # Invoke Data Map (IDM)
    my $dm = $translator->toascii(substr($trd,0,8));
    $dm =~ s/ +$//;
    $self->write("($dm) a.setDataMap\n") if $trd ne "";
  }
  elsif ($trp eq "d3afd8") { # Include Page Overlay (IPO)
    my @d3afd8 = unpack("H16H6H6H4H*",$trd);
    $d3afd8[0] = $translator->toascii(substr($trd,0,8));
    my $t1 = substr($trd, 8, 3);
    my $t2 = substr($trd, 11, 3);
    $t1 = unpack("C",$t1)*256+unpack("xn",$t1);
    $t2 = unpack("C",$t2)*256+unpack("xn",$t2);
    @d3afd8[1..2] = ($t1, $t2);
    my $po = $d3afd8[0]; $po =~ s/ +$//;
    $self->write("\%$po  << BEGIN (INCLUDE PAGE OVERLAY)\n");
    $self->write("$t1 $t2 ($po) a.IncOverlay\n");
    $self->write("\%$po  << END\n");
    #parseTripletGroup(@d3afd8[4]) if @d3afd8[4] ne "";
  }
  elsif ($trp eq "d3af5f") { # Include Page Segment (IPS)
    my @d3af5f = unpack("H16H6H6H*",$trd);
    $d3af5f[0] = $translator->toascii(substr($trd,0,8));
    my $t1 = substr($trd, 8, 3);
    my $t2 = substr($trd, 11, 3);
    $t1 = unpack("C",$t1)*256+unpack("xn",$t1);
    $t2 = unpack("C",$t2)*256+unpack("xn",$t2);
    @d3af5f[1..2] = ($t1, $t2);
    my $ps = $d3af5f[0]; $ps =~ s/ +$//;
    $self->write("\%$ps  << BEGIN (INCLUDE PAGE SEGMENT)\n");
    $self->write("$t1 $t2 ($ps) a.IncPSegment\n");
    $self->write("\%$ps  << END\n");
    #parseTripletGroup(@d3af5f[3]) if @d3af5f[3] ne "";
  }
  else {
    $self->write(unpack("H*",$_[0])."\n");
    die "??? ", unpack("H*", $_[0]), "\n";
  }
}

sub putLine {
  my ($self, $line) = (shift, shift);
  my ($cc, $lineData) = unpack("H2H*",$line);
  if ( $cc ne "5a" ) {
	$self->write("%".$translator->toascii(substr($line,0))."\n");
    #print $OUTPUT "($cc) <$lineData> a.asaLine\n";
  }
  else {
	putAfpLine( substr($_,3) );
  }
}

sub putPage {
  my ($self, $page) = (shift, shift);
  #print $OUTPUT "?? >>>>>>>>>>>>>>> $atPage\n";
  map { putLine($self, $_) } @$page;
}

#------------------------------------------------------------

my %putLine = (
);

sub new {
  my ($className, $fileName, $debug) = @_;

  my $putPage = ($debug) ? \&putPage : \&putPsfPage;
  my $putLine = ($debug) ? \&putLine : \&putPsfLine;
 
  my $self = ( $fileName =~ /\.gz$/ ) 
    ? new XReport::OUTPUT::OZlib($fileName, $putPage, $putLine)
    : new XReport::OUTPUT::OPlain($fileName, $putPage, $putLine)
  ;
}

1;
