
package XReport::OUTPUT::Extractor;

use strict;

use Symbol;

use Carp::Assert qw(assert);

use XReport::Util;
use XReport::PDF::DOC;

use constant MAX_FILE_SIZE => 1500;

sub NewFile {
  my $self = shift; my $OUTPUT = $self->{OUTPUT}; my $jr = $self->{jr};

  my $lFile = $self->{atFile} || 0;

  if ( $self->{atFileSize} > 0 ) {
    $self->EndFile();
  }
  $self->{atFile} += 1 if $self->{atPage} > 1; $self->{atFileSize} = 0.1;
   
  $self->{OutFileName} = $jr->getFileName('PSOUT',$self->{atFile});
  $self->{PdfFileName} = $jr->getFileName('PDFOUT',$self->{atFile});

  $self->{docTo} = XReport::PDF::DOC->Create(
    $self->{PdfFileName}, 
    {
      CheckForAliases => 1
    }
  );

  open($OUTPUT, ">".$self->{OutFileName}) 
   or 
  croak("OUTPUT OPEN ERROR $!"); binmode($OUTPUT);

  $self->{FromPage} = $self->{atPage}; 

  return $lFile;
}

sub EndFile {
  my $self = shift; return undef if !($self->{atFileSize} >= 1);
  
  my ($OUTPUT, $PAGEXREF) = @{$self}{qw(OUTPUT PAGEXREF)}; 

  close($OUTPUT);

  $self->{docTo}->Close(); delete $self->{docTo};

  my ($atFile, $FromPage, $atPage) = @{$self}{qw(atFile FromPage atPage)};

  my $ToPage = ($self->{Closing}) ? $atPage : $atPage-1;

  print $PAGEXREF "File=$atFile From=$FromPage To=$ToPage\n";
  
  &$logrRtn("File=$atFile From=$FromPage To=$ToPage");

  my $OutFileName = $self->{OutFileName}; $OutFileName =~ s/\//\\/g;
  my $PdfFileName = $self->{PdfFileName}; $PdfFileName =~ s/\//\\/g;
  
  my $doc = XReport::PDF::DOC->Open($PdfFileName); 

  my ($tPgmPages, $tPdfPages) = (($ToPage-$FromPage+1), $doc->TotPages()); 

  die "MISMATCH FOR NUMBER OF PAGES: PGM=$tPgmPages PDF=$tPdfPages\n"
   if
  $tPdfPages != $tPgmPages;

  $doc->PrepareXrefFileLengths(); $doc->Close();
  
  unlink $OutFileName or croak("UNLINK $OutFileName ERROR $!") if $atFile > 0;

  $self->{atFileSize} = 0; 

  return $atFile;
}

sub BeginPage {
  my ($self, $page) = (shift, shift); my $atPage = $self->{atPage} += 1; 
  
  my (
    $docFromPage, $docForPages, $docTable,
    $lbasedir 
  ) = 
  @{$self}{qw(docFromPage docForPages docTable lbasedir)}; 
  
  if ( !$self->{atFileSize} ) {
    $self->NewFile();
  }

  my $lineList = $page->lineList(); my $line = $lineList->[0]; $self->{totPages} += 1;

  if ($line->AsciiValue() =~ /EXTRACT=YES/i) {
    my @lines = grep(
      /^\s*(?:\w+)=/, 
      map {$_->AsciiValue()} @$lineList
    );
    my %largs = map {$_ =~ /(\w+)=(.*)/;($1,$2)} @lines;

    my (
      $lFileName, $docFromPage, $docForPages
    ) = 
    @largs{qw(FileName FromPage ForPages)}; $lFileName = "$lbasedir/$lFileName";

    if (!exists($docTable->{$lFileName})) {
      if (-e $lFileName) {
        $docTable->{$lFileName} = XReport::PDF::DOC->Open($lFileName); 
      }
      else {
        die("INPUT FILE UNKNOWN \"$lFileName\"");
      }
    }

    @{$self}{qw(doc docFromPage docForPages)} = ($docTable->{$lFileName}, $docFromPage, $docForPages);
  }
}

sub EndPage {
  my $self = shift; $self->{atFileSize} += 1; 
  
  my (
    $docTo, $doc, $docFromPage, $docForPages
  ) =
  @{$self}{qw(docTo doc docFromPage docForPages)}; 

  assert($docForPages > 0, "INTERNAL ERROR INVALID NUMBER OF PAGES TO COPY");
  
  $docFromPage <= $doc->TotPages() 
   or
  die("INVALID FROMPAGE DETECTED FromPage:$docFromPage TotPage:". $doc->TotPages(). " !!\n");

  $docTo->CopyPages($doc, $docFromPage, 1); 

  $docFromPage += 1; $docForPages -= 1;  

  @{$self}{qw(docFromPage docForPages)} = ($docFromPage, $docForPages);
}

sub PutPage {
  my ($self, $page) = @_; 
}

sub PutLine {
  my ($self, $line) = @_;
}

sub BREAKABLE_PAGE {
  my ($self, $max_file_size) = (shift, shift || MAX_FILE_SIZE); 
  if ( $self->{atFileSize} >= $max_file_size ) {
    return 1;
  }
  return 0;
}

sub totPages {
  my $self = shift; return $self->{totPages};
}

sub Open {
  my ($self, $parser) = (shift, shift); my ($jr) = @{$self}{qw(jr)};

  my $FileName = $self->{PageXrefFileName} 
   = 
  $jr->getFileName('PAGEXREF'); 
  
  open($self->{PAGEXREF}, ">$FileName") 
   or 
  croak("PAGEXREF OPEN ERROR \"$FileName \"$!");

  @{$self}{qw(atFile atPage atLine)} = (0, 0, 0); return $self;
}

sub Close {
  my $self = shift; $self->{Closing} = 1;

  $self->EndFile();

  close($self->{PAGEXREF}); $self->{Closing} = 0;
}

sub new {
  my ($class, $jr, $dom) = @_; my $self; 
  
  my ($SrvName, $workdir) = getConfValues('SrvName', 'workdir'); 
  
  my $lbasedir = "$workdir/$SrvName/localres";

  $self = {
    jr => $jr, dom => $dom, 
    lbasedir => $lbasedir, docTable => {},
    OUTPUT => gensym(), PAGEXREF => gensym(),
  };

  bless $self, $class;
}

1;

__END__

/Properties 
if (0) {
        my $Parser = XReport::PDF::Parser->new();

        my $cpage = $Parser->ParseCatalog($pgobj->[1]);
        my ($resid) = $cpage->{'Resources'} =~ /(\d+)/;
        my $resobj = $doc->getObj($resid) if $resid;
        if ($resobj->[1] =~ /\/Properties\s*/so) {
          $resobj->[1] =~ s/\/Properties\s*<<[^>]*>>\s*//so;
          $self->CopyObjs($doc, $resobj)
        }
      }
