package XReport::OUTPUT::OZlib;

use strict;
use Carp;
use Symbol;
use Compress::Zlib;

sub Open {
  my $self = shift; my $OUTPUT = $self->{OUTPUT};
  
  if (!$OUTPUT) {
    $OUTPUT = gensym();
  }
  else {
    $self->Close();
  }
  
  $OUTPUT = gzopen($self->{fileName}, "wb");
  $self->{OUTPUT} = $OUTPUT;

  return $OUTPUT;
}

sub Close {
  my $self = shift; my $OUTPUT = $self->{OUTPUT};
  return $OUTPUT->gzclose();
}

sub write {
  my $self = shift; my $OUTPUT = $self->{OUTPUT};
  return $OUTPUT->gzwrite($_[0]);
}

sub tell {
  my $self = shift; my $OUTPUT = $self->{OUTPUT};
  return $OUTPUT->gztell();
}

sub print {
  my $self = shift; my $OUTPUT = $self->{OUTPUT};
  map {$OUTPUT->gzwrite($_)} @_;
}

sub putLine {
  my $self = shift;
  return &{$self->{getLine}}($self, @_);
}

sub putPage {
  my $self = shift;
  return &{$self->{putPage}}($self, @_);
}

sub new {
  my ($className, $fileName, $putPage, $putLine) = @_; my $self = {};
  
  $self->{fileName} = $fileName;
  $self->{putPage} = $putPage;
  $self->{putLine} = $putLine;

  bless $self;
}

1;
