
package XReport::OUTPUT::ProjectMerger;

use strict;

use Symbol;

use Carp::Assert qw(assert);

require MIME::Base64;
require XML::DOM;

use XReport::Project;
use XReport::Util;
use XReport::PDF::DOC;
use XReport::Resources;

use constant MAX_FILE_SIZE => 1500;
$|=1;
sub FlushProject {
  my $self = shift; 
  my (
    $lProject, $docTo, $lWorkInFlow, $lIWorkInPage, $atPage, $lProjectName
  ) = 
  @{$self}{qw(lProject docTo lWorkInFlow lIWorkInPage atPage lProjectName)}; 
  
  assert($lIWorkInPage == $lWorkInFlow);
  
  my $ProjectClass = $self->{jr}->getValues('ProjectClass');
  if ( $ProjectClass =~ /^FormEditor$/i ) {
  i::logit("INITIALIZING for PRTDEST");
  my $PrtDestField = $self->{jr}->getValues('PrinterDestField');
  if ( $self->{lProject} ) {
    my $prtdest = '';
    if ( $PrtDestField ne '--DONTPRINT--' ) {
      $prtdest = $self->{lPage}->{'FieldsExtractor'}->getValues($PrtDestField);
      $prtdest =~ s/^\s*(\S+)\s*$/$1/;
      $prtdest = '' unless $prtdest;
      i::logit("PRTDEST set to $prtdest");
    }
    $self->{jr}->setValues(OS400attrs => {'Output queue name' => $prtdest });
  }
  }
#  print "flushing2 ".ref($lProject)."\n";
  $atPage += $lProject->FlushProject($docTo); 
#  print "flushing3\n";
  $lProject->FlushValues();
#  print "flushing4\n";
  i::logit("Project $lProjectName Flushed");

  @{$self}{qw(lProject lProjectName lWorkInFlow lIWorkInPage atPage)} = (undef, '', 0, 0, $atPage);
}

sub NewFile {
  my $self = shift; my $OUTPUT = $self->{OUTPUT}; my $jr = $self->{jr};

  my $lFile = $self->{atFile} || 0;

  if ( $self->{atFileSize} > 0 ) {
    $self->EndFile();
  }
  $self->{atFile} += 1 if $self->{atPage} > 1; $self->{atFileSize} = 0.1;
   
  $self->{OutFileName} = $jr->getFileName('PSOUT', $self->{atFile});
  $self->{PdfFileName} = $jr->getFileName('PDFOUT', $self->{atFile});

  my $docTo = $self->{docTo} = XReport::PDF::DOC->Create(
    $self->{PdfFileName}, 
    {
      CheckForAliases => 0
    }
  );

  open($OUTPUT, ">".$self->{OutFileName}) 
   or 
  croak("OUTPUT OPEN ERROR $!"); binmode($OUTPUT);

  $self->{FromPage} = $self->{atPage}; @{$self}{qw(FontAliases FontObjs)} = ({}, {}); return $lFile;
}

sub EndFile {
  my $self = shift; return undef if !($self->{atFileSize} >= 1);
  i::logit("closing file at size $self->{atFileSize}");
  
  my (
    $lProject, $docTo, $OUTPUT, $PAGEXREF
  ) = 
  @{$self}{qw(lProject docTo OUTPUT PAGEXREF)}; assert(!defined($lProject)); 

  close($OUTPUT); $docTo->Close(); delete $self->{docTo};

  my ($atFile, $FromPage, $atPage) = @{$self}{qw(atFile FromPage atPage)};

  my $ToPage = ($self->{'Closing'}) ? $atPage-1 : $atPage;

  print $PAGEXREF "File=$atFile From=$FromPage To=$ToPage\n";
  
  &$logrRtn("File=$atFile From=$FromPage To=$ToPage");

  my $OutFileName = $self->{'OutFileName'}; $OutFileName =~ s/\//\\/g;
  my $PdfFileName = $self->{'PdfFileName'}; $PdfFileName =~ s/\//\\/g;
  
  my $doc = XReport::PDF::DOC->Open($PdfFileName); 

  my ($tPgmPages, $tPdfPages) = (($ToPage-$FromPage+1), $doc->TotPages()); 

  die "MISMATCH FOR NUMBER OF PAGES: PGM=$tPgmPages PDF=$tPdfPages\n"
   if
  $tPdfPages != $tPgmPages;

  $doc->PrepareXrefFileLengths(); $doc->Close();
  
  unlink $OutFileName or croak("UNLINK $OutFileName ERROR $!") if $atFile > 0;

  $self->{atFileSize} = 0; return $atFile;
}

sub BeginPage {
  my $self = shift; $self->{'lPage'} = shift;  
  
  if ( !$self->{atFileSize} ) {
    $self->NewFile();
  }
}

sub ActivePageDefIs {
  my ($self, $ActivePageDef) = @_;  
  my (
    $Projects, $lProjectName, $lPage
  ) = 
  @{$self}{qw(Projects lProjectName lPage)};

  my $ProjectName = $ActivePageDef->getAttr("Project");
  
  if ($ProjectName eq '') {
    my $ProjectField = $ActivePageDef->getAttr("ProjectField");
    my $FieldsExtractor = $lPage->{'FieldsExtractor'};
    $ProjectName =$FieldsExtractor->getValues($ProjectField);
    $ProjectName = c::varxlate($ProjectField, $ProjectName);
  }
  i::logit("Current Project: $lProjectName - Page Def Project: $ProjectName");

  if ($lProjectName and $lProjectName ne $ProjectName) {
    i::logit("Now Flushing $lProjectName and closing file");
    $self->FlushProject();
  }

  if ($ProjectName ne $lProjectName) {
    my $oldprj = $lProjectName;
    
    if (!exists($Projects->{$ProjectName})) {
      $Projects->{$ProjectName} = XReport::Project->Open($ProjectName, jr => $self->{jr});
    }
    my $lProject = $Projects->{$ProjectName}; 
    @{$self}{qw(
      lProject lProjectName lWorkInFlow
    )} = 
    ($lProject, $ProjectName, $lProject->getValues('lWorkInFlow')); 
    i::logit("Switched from $oldprj to $self->{lProjectName}");
  }
}

sub EndPage {
  my ($self, $lPage) = @_; $self->{'atFileSize'} += 1; $self->{lIWorkInPage} += 1;
  my (
    $lProject, $lWorkInFlow, $lIWorkInPage 
  ) = 
  @{$self}{qw(lProject lWorkInFlow lIWorkInPage)};
  assert(defined($lProject));
  $lProject->ExtractFieldValues($lPage, $lIWorkInPage);
  
  if ($lIWorkInPage == $lWorkInFlow) {
    $self->FlushProject(); 
  }
}

sub PutPage {
  my ($self, $page) = @_; 
}

sub PutLine {
  my ($self, $line) = @_;
}

sub BREAKABLE_PAGE {
  my ($self, $max_file_size) = (shift, shift || MAX_FILE_SIZE); 
  if ( $self->{atFileSize} >= $max_file_size ) {
    return 1;
  }
  return 0;
}

sub atPage {
  my $self = shift; return $self->{atPage};
}

sub totPages {
  my $self = shift; return $self->{atPage};
}

sub Open {
  my ($self, $parser) = (shift, shift); my ($jr) = @{$self}{qw(jr)};

  my $FileName = $self->{PageXrefFileName} 
   = 
  $jr->getFileName('PAGEXREF'); 
  
  open($self->{PAGEXREF}, ">$FileName") 
   or 
  croak("PAGEXREF OPEN ERROR \"$FileName \"$!");

  @{$self}{qw(atFile atPage atLine)} = (0, 1, 1); return $self;
}

sub Close {
  my $self = shift; $self->{Closing} = 1;

  $self->EndFile();

  close($self->{PAGEXREF}); $self->{Closing} = 0;
}

sub new {
  my ($class, $jr, $dom) = @_; my $self; 
  i::logit("New ". __PACKAGE__ ."called by ".join('::', (caller())[0,2]));
  my $jobReportDef = $dom->getElementsByTagName("jobreport")->[0];
  if (my $ProjectClass = $jobReportDef->getAttribute("ProjectClass")) {
    $jr->setValues(ProjectClass => $ProjectClass);
  }
  else {
    $jr->setValues(ProjectClass => 'LineEditor');
  }    

  my ($userlib, $workdir, $SrvName) = c::getValues(qw(userlib workdir SrvName)); 

  $self = {
    jr => $jr, dom => $dom, userlib => $userlib, workdir => "$workdir/$SrvName", 
    
    OUTPUT => gensym(), PAGEXREF => gensym(),

    Projects => {}, lWorkInFlow => 0, lIWorkInPage => 0
  };

  bless $self, $class;
}

1;
