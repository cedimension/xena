package XReport::BTree::INPUT;

use strict;

use Compress::Zlib;
use Data::Alias qw(alias);
use Data::Serializer;

use Math::Int64 qw(uint64_to_number net_to_uint64);
use Math::Int64::die_on_overflow;

#use XReport::AUTOLOAD;

my %unpack_table = (
  1 => 'unpack("c", shift @t)',
  2 => 'unpack("n", shift @t)',
  3 => 'unpack("N", "\x00".(shift @t))',
  4 => 'unpack("N", shift @t)',
  5 => 'uint64_to_number(net_to_uint64("\x00\x00\x00".(shift @t)))',
  6 => 'uint64_to_number(net_to_uint64("\x00\x00".(shift @t)))',
  7 => 'uint64_to_number(net_to_uint64("\x00".(shift @t)))',
);

sub _locate_key {
  my ($low, $high, $target, $get_value) = @_; 

  while ( $low < $high ) {
    use integer;
    my $cur = ($low+$high)/2;
    if (&$get_value($cur) < $target) {
      $low  = $cur + 1;  
    } 
    else {
      $high = $cur;    
    } 
  }
  return $low;
}

sub _get_blob_data {
}

sub _locate_entry {
  my ($self, $key_pos, $key_value) = @_; 
  alias my (
    $btree_id, $btree_def, $leafBucketList, $entry_length, $entry_unpacker
  ) = 
  @$self{qw(
    btree_id btree_def leafBucketList entry_length entry_unpacker
  )};

  my $leafIndex = _locate_key(
    0, scalar(@$leafBucketList), $key_value, sub {$leafBucketList->[$_[0]][1][$key_pos]} # here 1 upper value
  );
  my $blob_sha1 = $leafBucketList->[$leafIndex]->[-1];
  
  
  if ( !$blob_sha1 ) {
        i::warnit("locate error - No SHA1 in Bucket list at $leafIndex (entry ".scalar(@{$leafBucketList->[$leafIndex]}).")");
        return undef;
  }
  if ( $blob_sha1 =~ /[^A-Fa-f0-9]/ ) {
      i::warnit("locate error - Inavlid SHA1 in Bucket list at $leafIndex (entry ".$blob_sha1.")");
      return undef;
  } 
  $main::veryverbose and i::warnit("locating blob_data for btree_id=$self->{'btree_id'} and blob_sha1=$blob_sha1");
  
  
  my $query = "select top 1 * from tbl_JobReportsBTreesBlobs where JobReportId=$self->{jobreportid} and btree_id=$self->{'btree_id'} and blob_sha1=0x$blob_sha1";
  my $dbr = dbExecute($query);
  die "No record found in tbl_JobReportsBTreesBlobs - query:$query" if $dbr->eof();
  my $blob_data = Compress::Zlib::uncompress($dbr->getValues(qw(blob_data))); my ($entry_data, $entry_pos);
  $dbr->Close();
  
  my $entryIndex = _locate_key( 
    0, length($blob_data)/$entry_length, $key_value, sub {
      (&$entry_unpacker(substr($blob_data,$_[0]*$entry_length, $entry_length)))[$key_pos];
    }
  );
  $entry_pos = $entryIndex*$entry_length; $entry_data = substr($blob_data, $entry_pos, $entry_length);

  $entry_pos -= $entry_length if  $key_value < (&$entry_unpacker($entry_data))[$key_pos]; 

  return ($leafIndex, $entry_pos, $blob_data);
}

sub get_entry {
  my ($self, $key_pos, $key_value) = @_;
  alias my ($leafIndex, $entry_pos, $blob_data) = $self->_locate_entry($key_pos, $key_value);

  my ($entry_length, $entry_unpacker) = @$self{qw(entry_length entry_unpacker)};

  &$entry_unpacker(substr($blob_data, $entry_pos, $entry_length))
}

sub get_next_entry {
  my $self = shift;
  alias my ($entry_pos, $entry_length, $entry_unpacker, $blob_data) = 
                                   @$self{qw(entry_pos entry_length entry_unpacker blob_data)}; 

  if ($entry_pos >= length($blob_data)) {
    alias my ($btree_id, $leafIndex, $leafBucketList) = @$self{qw(btree_id leafIndex leafBucketList)};

    my $blob_sha1 = $leafBucketList->[$leafIndex+=1]->[-1]; 
    if ( !$blob_sha1 ) {
    	i::warnit("getnext error - No SHA1 in Bucket list at $leafIndex (entry ".scalar(@{$leafBucketList->[$leafIndex]}).")");
    	return undef;
    }
    if ( $blob_sha1 =~ /[^A-Fa-f0-9]/ ) {
        i::warnit("getnext error - Inavlid SHA1 in Bucket list at $leafIndex (entry ".$blob_sha1.")");
        return undef;
    } 
    $entry_pos = 0;

    $main::veryverbose and i::warnit("get_next_entry blob_data for btree_id=$btree_id and blob_sha1=$blob_sha1");
	my $query = "select top 1 * from tbl_JobReportsBTreesBlobs where JobReportId=$self->{jobreportid} and btree_id=$btree_id and blob_sha1=0x$blob_sha1";
    my $dbr = dbExecute($query);
    die "No record found in tbl_JobReportsBTreesBlobs - query:$query" if $dbr->eof();

    $blob_data = Compress::Zlib::uncompress($dbr->getValues(qw(blob_data)));
	$dbr->Close();
  } 

  my $entry_data = substr($blob_data, $entry_pos, $entry_length);
  $entry_pos += $entry_length;
  return &$entry_unpacker($entry_data);
}

sub get_iterator {
  my ($self, $key_pos, $key_value) = @_; 
  alias my ($leafIndex, $entry_pos, $blob_data) = $self->_locate_entry($key_pos, $key_value);

  my ($entry_length, $entry_unpacker) = @$self{qw(entry_length entry_unpacker)};

  my $iterator = {
    %$self, entry_length => $entry_length, blob_data => $blob_data, leafIndex => $leafIndex, entry_pos => $entry_pos, key_pos => $key_pos
  };

  bless $iterator, ref($self);
}

use XReport::DBUtil;

{
    no warnings 'redefine';
	sub new {
	  my ($class, $JobReportId, $btree_name) = @_; 
	  my $dbr = dbExecute(
		"select top 1 * from tbl_JobReportsBTrees where JobReportId=$JobReportId and btree_name='$btree_name'"
	  );
	  die("btree index $JobReportId/$btree_name not found !!") if $dbr->eof();
	  my (
		$btree_id, $btree_meta_data
	  ) = 
	  $dbr->getValues(qw(btree_id btree_meta_data)); $btree_meta_data = Compress::Zlib::uncompress($btree_meta_data);
	  $dbr->Close();


	  my $serializer = Data::Serializer->new(serializer => 'JSON', compress => 0);

	  my $treeBucket = $serializer->deserialize($btree_meta_data); 

	  my ($btree_root, $btree_def, $leafBucketList) = @$treeBucket{qw(btree_root btree_def leafBucketList)};
		
	  my ($key_lengths, $entry_length) = @$btree_def{qw(key_lengths entry_length)}; my $unpack_expr = '';

	  for (@$key_lengths) {
		$unpack_expr .= $unpack_table{$_} . ','; 
	  }
	  $unpack_expr = "sub { my \@t = unpack('". join("", map {"a$_"} @$key_lengths,'*'). "', \$_[0]); ($unpack_expr); }";
	  
	  my $entry_unpacker = eval($unpack_expr) or die("eval error ($unpack_expr) $@\n");

	  my $self = {
		btree_id => $btree_id, btree_def => $btree_def, leafBucketList => $leafBucketList, 
		entry_length => $entry_length, entry_unpacker => $entry_unpacker, jobreportid => $JobReportId
	  };

	  bless $self, $class;
	}
}
1;
