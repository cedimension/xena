package XReport::BTree::OUTPUT;

use strict;

use Compress::Zlib;
use Data::Alias qw(alias);
use Data::Serializer;

use Math::Int64 qw(uint64 uint64_to_net);
use Math::Int64::die_on_overflow;

#use XReport::AUTOLOAD;

my %pack_table = (
  1 => 'pack("c", shift)',
  2 => 'pack("n", shift)',
  3 => 'substr(pack("N", shift), 1)',
  4 => 'pack("N", shift)',
  5 => 'substr(uint64_to_net(uint64(shift)),3)',
  6 => 'substr(uint64_to_net(uint64(shift)),2)',
  7 => 'substr(uint64_to_net(uint64(shift)),1)',
);

sub write_bucket_blob  {
	my $self = shift;
	alias my ($bucket) = @_;
#	warn "write_bucket_blob page xref level=($bucket->[-3]) blob_length=", length($bucket->[-1]), "\n";
	#print unpack("H*", substr($_[0], 14*30)), "\n"; 
	my ($btid, $jrid) = @{$self->{btree_def}}{qw(id JobReportId)};
	my $blob_sha1 = Digest::SHA1::sha1_hex($bucket->[-1]);
	my $blob_data = unpack("H*", Compress::Zlib::compress($bucket->[-1]));
    XReport::DBUtil::dbExecute("
	    insert into tbl_JobReportsBTreesBlobs (blob_sha1, JobReportId, btree_id, blob_data, blob_metadata)
	    values 
	    (0x$blob_sha1, $jrid, $btid, 0x$blob_data, NULL)
	");
	return $blob_sha1;
}
      
sub add_tree_entry {
  my ($self, $level, $treeEntry, $sha1_hex) = @_; 
  alias my (
    $treeBuckets, $entry_packer,
    $tree_max_entries, $tree_max_bsize, 
  ) =
  @$self{qw(treeBuckets entry_packer tree_max_entries tree_max_bsize)}; 

  $treeBuckets->[$level] = [$treeEntry, $treeEntry, $level, 0, ''] if !$treeBuckets->[$level];
  my $treeBucket = $treeBuckets->[$level];

  if ( $treeBucket->[-2] >= $tree_max_entries or length($treeBucket->[-1]) >= $tree_max_bsize ) {
    my $sha1_hex = $self->write_bucket_blob( $treeBucket);
    $self->add_tree_entry( $level + 1, $treeBucket->[0], $sha1_hex);
    @$treeBucket = ($treeEntry, $treeEntry, $level, 0, '');
  }

  $treeBucket->[1] = $treeEntry;
  $treeBucket->[-2] += 1;
  $treeBucket->[-1] .= &$entry_packer(@$treeEntry, $sha1_hex); 
}

sub add_leaf_entries {
  my ($self, $leafEntries) = @_; return if !@$leafEntries; 
  alias my (
    $leafBucket, $leafBucketList, $entry_packer, $entry_length, $leaf_max_entries, $leaf_max_bsize
  ) =
  @$self{qw(
    leafBucket leafBucketList entry_packer entry_length leaf_max_entries leaf_max_bsize
  )}; 

  for my $leafEntry (@$leafEntries) {
    @$leafBucket = ($leafEntry, $leafEntry, 0, 0, '') if !@$leafBucket; 
    if (  $leafBucket->[-2] >= $leaf_max_entries or length($leafBucket->[-1]) >= $leaf_max_bsize ) {
      my $sha1_hex = $self->write_bucket_blob( $leafBucket);
      my ($leafFirstEntry, $leafLastEntry) = (@{$leafBucket}[0,1]);
      $self->add_tree_entry( 1, $leafFirstEntry, $sha1_hex );
      @$leafBucket = ($leafEntry, $leafEntry, 0, 0, '');
      push @$leafBucketList, [$leafFirstEntry, $leafLastEntry, $sha1_hex]; 
    }
    $leafBucket->[1] = $leafEntry; 
    $leafBucket->[-2] += 1; 
    $leafBucket->[-1] .= &$entry_packer(@$leafEntry);
    die(join(",", @$leafEntry), "\n") if length($leafBucket->[-1]) % $entry_length;
  }
}

sub close {
  my $self = shift; 
  alias my (
    $btree_def, $leafBucket, $leafBucketList, $treeBuckets, $entry_packer
  ) = 
  @$self{qw(
    btree_def leafBucket leafBucketList treeBuckets entry_packer
  )}; 

  my $serializer = Data::Serializer->new(serializer => 'JSON', compress => 0);

  my $sha1_hex = $self->write_bucket_blob( $leafBucket); 
  my $leafFirstEntry = $leafBucket->[0];
  my $leafLastEntry = $leafBucket->[1];
  
  $self->add_tree_entry( 1, $leafFirstEntry, $sha1_hex ); 
  @$leafBucket = ();
  push @$leafBucketList, [$leafFirstEntry, $leafLastEntry, $sha1_hex]; 

  my $level = 1; my $treeBucket = undef; my $btree_root;
  while (1) {
    $treeBucket = $treeBuckets->[$level]; my $sha1_hex = $self->write_bucket_blob( $treeBucket); $level += 1;
    last if ($level >= @$treeBuckets);
    
    $self->add_tree_entry($level, $treeBucket->[0], $sha1_hex);
  }

  $btree_root = $sha1_hex; $treeBucket = [$level, 1, '']; 
  $treeBucket->[-1] .= $serializer->serialize({
    btree_root => $btree_root,
    btree_def => $btree_def,
    leafBucketList => $leafBucketList,
  });

#  warn "treeBucket: ", Dumper($serializer->deserialize($treeBucket->[-1])), "\n";

  return (0, $treeBucket->[-1]);
}

sub new {
  my ($class, %largs) = @_;  
  my (
    $btree_def, $leaf_max_entries, $leaf_max_bsize, $tree_max_entries, $tree_max_bsize 
  ) =
  @largs{qw(
    btree_def leaf_max_entries leaf_max_bsize tree_max_entries tree_max_bsize
  )};
  $leaf_max_entries ||= 1000; $leaf_max_bsize ||= 100_000;
  $tree_max_entries ||= 1000; $tree_max_bsize ||= 100_000;

  my ($key_lengths, $entry_length) = @$btree_def{qw(key_lengths entry_length)}; my $pack_expr = '';

  for (@$key_lengths) {
    $pack_expr .= $pack_table{$_} . ','; 
  }
  $pack_expr = "sub { my \$entry = join('', $pack_expr); \$entry .= join('', \@_) }";

  my $entry_packer = eval($pack_expr) or die("eval error ($pack_expr) $@\n");

  my $self = {
    btree_def => $btree_def, 
    entry_length => $entry_length,
    leafBucket => [], 
    leafBucketList => [], 
    treeBuckets => [], 
    entry_packer => $entry_packer, 
    leaf_max_entries => $leaf_max_entries, 
    leaf_max_bsize => $leaf_max_bsize,
    tree_max_entries => $tree_max_entries, 
    tree_max_bsize => $tree_max_bsize,
  };

  bless $self, $class;
}

1;

