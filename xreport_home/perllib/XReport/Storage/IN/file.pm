package XReport::Storage::IN::file;

use strict;

use base 'IO::File';

sub Open {
	my $self = shift;
	my ( $LocalPathId_IN, $LocalPath, $LocalFileName ) = @_;
	my ($ptype, $fqp) = split /:\/\//, $LocalPath, 2;
	my $FileName = "$fqp/$LocalFileName";
	return IO::File->new("<$FileName");
}

__PACKAGE__;