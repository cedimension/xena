package XReport::Storage::IN::centera;

use strict;

use Compress::Zlib;
use Digest::MD5 qw(md5_hex);
require XReport::ARCHIVE::Centera; 

sub size {
  my $self = shift; my ($Tag_file) = @{$self}{qw(Tag_file)};
  
  return $Tag_file->getAttributes('size');
}

sub ctime {
  my $self = shift; my ($Tag_file) = @{$self}{qw(Tag_file)};
  
  return $Tag_file->getAttributes('ctime');
}

sub mtime {
  my $self = shift; my ($Tag_file) = @{$self}{qw(Tag_file)};
  
  return $Tag_file->getAttributes('mtime');
}

sub Tag_file {
  my $self = shift; my ($Tag_file) = @{$self}{qw(Tag_file)}; return $Tag_file;
}

sub Open {
   my $class = shift;
   my ( $LocalPathId_IN, $LocalPath, $LocalFileName, $ClipId ) = @_;
   my $filename = lc((split /[\/\\]/, $LocalFileName, 2)[1]);
   $main::veryverbose && i::logit(__PACKAGE__." Open Trying to open $filename ($ClipId) as requested by ".join('::', (caller())[0,2])); 
   my $self = {};
   bless $self, $class;
   
   my $pool  = XReport::ARCHIVE::Centera::FPPool->new( $LocalPathId_IN );
   die "Open error for \"$filename\"(\"$LocalPath\") - $LocalPathId_IN open error - $EMC::Centera::LastErrorDescr\n"
             unless $pool;
   my $cliph = $pool->Open_Clip($filename, $ClipId, 'INPUT');

   die "Open error for \"$filename\"(\"$LocalPath\") - $ClipId open error - $EMC::Centera::LastErrorDescr\n"
             unless $cliph;
   my $tagfile;
   my $tag = $cliph->GetTopTag();
   my $ttagname = 'unknown'; #$tag->GetTagName();
   my $fnattr = lc(($tag->getAttributes('filename') || ''));
   $main::veryverbose && i::logit("TOP TAG ($ttagname) has filename: \"$fnattr\""); 
   
   if ( $filename ne $fnattr  ) {
      if ( $tag = $tag->GetFirstChild() ) {
         while($tag) {
            $fnattr = lc(($tag->getAttributes('filename') || ''));
            $main::veryverbose && i::logit("TAG digging found filename: \"$fnattr\""); 
            if ($filename ne $fnattr ) {
            	$tag = $tag->GetSibling();
            }
            else { 
                $tagfile = $tag;
                last;
            }
         }
      }
      else {
       $main::veryverbose && i::logit( "GetFirstChild error - $EMC::Centera::LastErrorDescr" );
      }
   }
   else { $tagfile = $tag; }
   if ( !$tagfile ) {
   	  my $msg2die = "Open error - \"$filename\" not found in \"$LocalPath\"($ClipId) - $EMC::Centera::LastErrorDescr";
      $main::veryverbose && i::logit( $msg2die );
      die $msg2die;
   }
#   return undef if !$tagfile; 
   
   my $Tag_chunk = undef;
   my $buffer = ''; 
   my $chunk = \$buffer;
   @{$self}{qw(ctime mtime md5 chunks size origsize compr_method )} 
                             = $tagfile->getAttributes(qw( ctime mtime md5 chunks size origsize compr_method));
   @{$self}{qw( Tag_file Tag_chunk chunk at_chunk chunkpos residual_size filepos opened pool clip filename)} 
                                  = ( $tagfile, $Tag_chunk, $chunk, 1, 0, length($$chunk), 0, 1, $pool, $cliph, $filename );
   $self->{residual_size} = $self->{size};
  @{$self}{qw(closed opened)} = (0, 1);
   $main::veryverbose and i::warnit("Storage::IN::centera Open ended - returning to caller - self: ".Data::Dumper::Dumper($self));
   return  $self;
}

sub binmode {
	return 1;
}

sub eof {
  my $self = shift; 
  return !($self->{'residual_size'} > 0 || $self->{'chunks'} > $self->{'at_chunk'}); 
}

sub tell {
  return $_[0]->{'filepos'};
}

sub seek {
	my ( $self, $pos, $w) = @_;
	return 0 unless ( ref($self) && defined($pos));
	$w = 0 unless $w;
	return 0 unless $w < 3;
	
    $self->{filepos} = ( $w == 2 ? $self->size() : $w ? $self->tell() : 0 ) + $pos;
    return 1;  
}

sub read {
	my $self = shift;
	my $len2read = $_[1];
    ($main::debug || $main::veryverbose) and i::warnit("Storage::IN::centera read called by: ".join('::',(caller())[0,2])." len2read: $len2read outbuff:".ref($_[0])." self: ".Data::Dumper::Dumper($self));
	my $buffaddr = $self->{Tag_file}->BlobReadPartial($self->{filepos}, $len2read)
	                                   || die "centera read error: ".$EMC::Centera::LastErrorDescr."\n";
	                                   
	my $bytesread = length(${$buffaddr});
	$_[0] = ${$buffaddr};
	$self->{filepos} += $bytesread;
	$self->{residual_size} -= $bytesread;
	return $bytesread;
}

sub close {
  my $self = shift;
#  $self->{clip}->Close();
#  $self->{pool}->Close();
  @{$self}{qw(closed opened)} = (1, 0);
}

sub DESTROY {
  my $self = shift; $self->close() if $self->{'opened'};
}

__PACKAGE__;
