package XReport::Storage::IN;

use strict;

use Data::Dumper;

use IO::Compress::Gzip qw($GzipError :flush);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

use IO::File;
use File::Basename;
use Digest::SHA1;

no warnings 'redefine';

use Archive::Tar::Stream;
sub Archive::Tar::Stream::ReadBlocks {
  my $Self = shift;
  my $nblocks = shift || 1;
  unless ($Self->{infh}) {
    die "Attempt to read without input filehandle";
  }
  my $bytes = 512 * $nblocks;
  my $buf;
  my $result = '';
  while ($bytes > 0) {
    my $n = $Self->{infh}->read($buf, $bytes);
    unless (defined($n) && $n) {
      delete $Self->{infh};
      return if ($bytes == 512 * $nblocks); # nothing at EOF
      die "Failed to read full block at $Self->{inpos}";
    }
    $bytes -= $n;
    #$Self->{inpos} += $n;
    $Self->{inpos} = $Self->{infh}->tell();
    $result .= $buf;
  }
  $main::veryverbose && i::warnit("TAR readblock returned ".length($result)." bytes out of ".($nblocks*512)." as requested by ".join('::', (caller())[0,2])
                           . "- TAR Pos now is $Self->{inpos} block: ".unpack('H80', $result));
  return $result;
}


use XML::Simple;
use Data::Alias;

use constant BLOCK_SIZE => (128*1024);
use constant META_DATA_SIZE => (3*1024);
use constant OFFSET_TYPE => 0;

sub _fixFTPB {
    my $self = shift;
    my $buff = shift;
    return ('', $buff, []) if length($buff) < 16000;
    my $records = [];
    my $reccount = 0;
    my $xdata = '';
    while ( $buff && length($buff) >= 3  ) {
    	my $len = unpack('xn', $buff); 
    	last if $len > (length($buff) - 3);
    	(my $flag, my $recdata, $buff) = unpack('a1 n/a a*', $buff);
        $reccount++;
        $xdata .= pack('na*', $len, $recdata);
    	push @{$records}, [ $len, $recdata ];
        $self->{cinfo}->{maxlrec_detected} = $len if $len && $len > $self->{cinfo}->{maxlrec_detected}; 
    	$buff = '' if ( $flag eq "\x40");
    }
    return ($xdata, $buff, $records);
}

sub _fixFTPC {
	my $self = shift;
	my $byteCache = shift;
    return ('', $byteCache, []) if length($byteCache) < 32000;
	my $newbuff = '';
	my $incomplete = '';
	my $pos = 0;
	my $reccount = 0;
	my $records = [];
	
  my ($inrec, $outrec, $b1, $rc); my $incntrl = "";
  my ($at, $at_max) = (0, length($byteCache)); 
  
  while( $at_max > $at ) {
	
	my $ch = substr($byteCache,$at++,1);
	$incomplete .= $ch;
    
	$b1 = unpack("C", $ch);
    # blocks begins all times with a control byte
    # meanings:
    # byte with two high order bit on ( mask 0xC0 ): 
    #      filler bytes ( type I -> 0x00, type e/a -> 0x40 ) must be repeated (control Byte & 0x3f) times
    # byte with high order bit on ( mask 0x80 ): 
    #      following byte must be repeated (control Byte & 0x3f) times
    # 0x00: 
    #      following is a flag byte: 0x80 -> end of record
    #                                0x40 -> end of file
    
    if ( $b1 == 0x00 ) {
    	last unless $at_max > $at;

		$incntrl = substr($byteCache,$at,1);
		$incomplete .= $incntrl;
		if ( $incntrl ne "\x80" and $incntrl ne "\x40" ) {
          die("UserError: INVALID FLAG BYTE AFTER CONTROL BYTE at buffer pos ".($at - 1).": "
              .unpack("H80", substr($byteCache,$at-1, 20))
              ."\nFIRST 40 Bytes of BUFFER: ".unpack("H80", $byteCache)
              );
		}
		$at++;
		# we must now process last record block
		next;
    }
    elsif ( ($b1 & 0xc0) == 0x80 ) {
      $inrec = substr($byteCache,$at,1);
	  $incomplete .= $inrec;
      $rc = $b1 & 0x3f;
  	  $outrec .= $inrec x $rc;
  	  $at++;
    }
    elsif ( ($b1 & 0xc0) == 0xc0 ) {
      $rc = $b1 & 0x3f;
      my $fillchar = ($self->{'cinfo'}->{Type} =~ /^I/i ? "\x00" : 
                      $self->{'cinfo'}->{Type} =~ /^A/i ? "\x20" : "\x40"); # Last if type EBCDIC
      $outrec .= $fillchar x $rc; 
    }
    elsif ( ($b1 & 0x80) == 0x00 ) {
    	last if $at_max < ($at + $b1);
	  	$inrec = substr($byteCache,$at,$b1);
	  	$incomplete .= $inrec;
  	  	$outrec .= $inrec;
  	  	$at += $b1;
    }
    else {
  	  die("UserError: INVALID MODEC MASK at buffer pos $at: "
  	      .unpack("H80", substr($byteCache,$at-1, 20))
  	      ."\nFIRST 40 Bytes of BUFFER: ".unpack("H80", $byteCache)
  	      );
    }
    next if $incntrl eq "";
    $newbuff .= pack("n/a*", $outrec);
	$reccount++;
	my $reclen = length($outrec);
	push @{$records}, [ $reclen, $outrec ];
	$self->{cinfo}->{maxlrec_detected} = $reclen if $reclen > $self->{cinfo}->{maxlrec_detected}; 
	$incomplete = '';
	$incntrl = '';
  }
  
  $incomplete .= substr($byteCache, $at) if $at < $at_max;
  return ($newbuff, $incomplete, $records);
}

sub _fixLPR{
	my $self = shift;
	my $buff = shift;
	my ($datarecords, $incomplete) = ([], '');
	my @lines = split /((?:\x0d\x0a|\x0c|\x0d(?!\x0a)|(?<!\x0d)\x0a))/, $buff;
	$incomplete = pop @lines if $lines[-1] !~ /(?:\x0a|x0d|\x0c)$/;
    my $records = [];
    while ( scalar(@lines) ) {
        my $recd = shift @lines;
        if ($recd !~ /^[\x0d\x0a|\x0c]/) {
            my $ll = length($recd);
            $self->{cinfo}->{maxlrec_detected} = $ll if $ll > $self->{cinfo}->{maxlrec_detected};
            $recd .= shift @lines ; 
        }
        push @{$records}, [length($recd), $recd];
    }
	
    return (join('', map { pack("n a*", @{$_}) } @{$records} ), $incomplete, $records); 
}

sub _fixSTREAM {
	my $self = shift;
	my $buff = shift;
	my $sizeleft = shift;
    die "IMAGE Format not handled" unless exists($self->{cinfo}->{FIXRecfm}) && $self->{cinfo}->{LRECL};
	
	my $lrecl = $self->{cinfo}->{LRECL};
    $self->{cinfo}->{maxlrec_detected} = $lrecl;
    
    my ($records, $incomplete, $li, $leftover) = ([], '', length($buff), '');
    $li -= ($li % $lrecl);
    
    if ( $li < length($buff) ) {
    	($buff, $incomplete) = unpack("a$li a*", $buff);
    }
    
    if ( $li > $sizeleft ) {
    	($buff, $leftover) = unpack("a$sizeleft a*", $buff)
    } 

    return ($buff, $leftover.$incomplete, [ map { [ $lrecl, $_ ] } unpack("(a$lrecl)*", $buff) ]);

}

sub _fixFIXEDLEN {
   return _fixSTREAM(@_);
}

sub _fixAFPSTREAM {
	my $self = shift;
	my $buff = shift;
	my $newbuff = '';
	my $pos = 0;
	my $reccount = 0;
	my $records = [];
	my $len = unpack('n', substr($buff, $pos+1));
	my $blen = length($buff) - 3;
	return ('', $buff, []) if $len > $blen;
	return ($buff, '', [$len]) if $len == $blen;

	while ( $blen > $len ) {
		my $outrec = substr($buff, $pos, $len + 3);
		$newbuff .= pack('n/a*', $outrec);
		$reccount++;
		push @{$records}, [ $len + 3, $outrec ];
		$self->{cinfo}->{maxlrec_detected} = $len + 3 if $self->{cinfo}->{maxlrec_detected} < $len + 3; 
		$blen -= $len;
		$pos += $len + 3;
		last if $blen < 7;
		$len = unpack('n', substr($buff, $pos+1));
		$blen -= 3;
		
	}	
	return ($newbuff, ($blen ? substr($buff, $pos) : ''), $records);
}

sub _fixPSF {
	my $self = shift;
	my $buff = shift;
    return ('', $buff, []) if length($buff) < 16000;
    my $records = [];
    my $reccount = 0;
    my $xdata = '';
    $main::veryverbose and i::logit("Storage::IN _FIXPSF processing buffer: ".length($buff)." bytes - ".unpack('H16', $buff));
    while ( $buff && length($buff) >= 2 && (my $len = unpack('n', $buff)) <= (length($buff) - 2) ) {
        (my $recdata, $buff) = unpack('n/a a*', $buff);
        $reccount++;
        $xdata .= pack('na*', $len, $recdata);
        push @{$records}, [ $len, $recdata ];
		$self->{cinfo}->{maxlrec_detected} = $len if $len && $len > $self->{cinfo}->{maxlrec_detected}; 
	}
    $main::veryverbose and i::logit("Storage::IN _FIXPSF returning xdata: ".length($xdata)." bytes - leftover: ".length($buff)." bytes - ".scalar(@$records)." records");
    return ($xdata, $buff, $records);
}

sub Cinfo {
  my $self = shift;
  $self->{cinfo} = {} unless exists($self->{cinfo});
  $self->{cinfo} = { %{$self->{cinfo}}, @_ } if scalar(@_);
  return $self->{cinfo};
}

sub packStruct {
	my $self = shift;
	my $struct = shift;
	my $allowed_len = shift;
    my $infobuff = pack('w/a*', pack('(w/a*)*', %$struct));
    my $reqlen = length($infobuff);
    die "Update requires $reqlen metadata bytes - there is room for only $allowed_len bytes " 
    																if ( $reqlen > $allowed_len );
	return $self->{infh}->write( substr( $infobuff ."\x00" x $allowed_len, 0, $allowed_len) );
}

sub unpackStruct {
	my $self= shift;
	my $data_size = shift;
	my $packed_data = '';
	while ( length($packed_data) < $data_size && !$self->{infh}->eof() ) {
		$self->{infh}->read(my $buff, $data_size);
		$packed_data .= $buff;
	}
	return { unpack('(w/a)*', unpack('w/a', $packed_data)) } unless length($packed_data) < $data_size;
	return undef;
}

sub updateFileCinfo {
	my $self = shift;
	my $newcinfo = shift;
	
    my $currpos = $self->{infh}->tell();
    
	$self->{infh}->seek($self->{cinfottr}, SEEK_SET);
	$self->packStruct($newcinfo, $self->{cinfosize})
                  || die "unable to update tar file at $self->{cinfottr}";;
	
	$self->{infh}->seek($currpos, SEEK_SET);
	return $self->{cinfosize};
}

sub Create {
  my $class = shift;
  my $datafile = shift;
#  warn "Opening to create \"$datafile\"\n";
  
  (my $innerfn = basename($datafile)) =~ s/\.[^\.]+$//;
  my $self = {
  	      blocksize => BLOCK_SIZE,
  	      cinfosize => META_DATA_SIZE,
	      datafile => $datafile,
	      from_offset => 0,
	      from_record => 0,
	      at_record => 0,
	      block_offsets => [],
	      Name => $innerfn,
		  accesstype => 'create',	      
	      Comment => '',
	      @_
	     };
  $self->{data_buffer} = 0x00 x $self->{blocksize};
  $self->{data_buffer} = '';

  die "record Parser not specified in $self->{recordparser}" unless ref($self->{recordparser}) eq 'CODE';
  $self->{infh} = IO::File->new(">$datafile.tar") ||
                   die __PACKAGE__, "open error for $datafile - $? ";
  return undef unless $self->{infh};
  $self->{infh}->binmode();
  $self->{tar} = Archive::Tar::Stream->new(outfh => $self->{infh});
  (my $cntrlfile = basename($datafile)) =~ s/DATA\.TXT\.gz$/CNTRL.TXT/i;
  $cntrlfile .= '.cinfo' if $cntrlfile eq basename($datafile);
  my $tarhdr0 = $self->{tar}->CreateHeader($self->{tar}->BlankHeader(
  							name => $cntrlfile,
  							size => $self->{cinfosize}
  							));
#  warn "writing tar hdr for \"$cntrlfile\"\n";  							
  $self->{infh}->write($tarhdr0);
  $self->{cinfottr} = $self->{infh}->tell();
  $self->{infh}->write("\x00" x $self->{cinfosize});
    
  my $tarhdr = $self->{tar}->CreateHeader($self->{tar}->BlankHeader());
  $self->{hdrttr} = $self->{infh}->tell();
#  warn "writing empty tar hdr\n";                              
  $self->{infh}->write($tarhdr);

  bless $self, $class;
  return $self;

}

sub _read_block_offsets {
	my $self = shift;
	$self->{infh}->seek(($self->{cinfo}->{block_offsets_ttr}-0), SEEK_SET);
    my $idxhdr = $self->{tar}->ReadHeader();
    $main::veryverbose && i::warnit("Storage::IN block offsets header read starting from $self->{cinfo}->{block_offsets_ttr}: ".Data::Dumper::Dumper($idxhdr));
	my $gzlen = $self->{infh}->read(my $gz_block, $idxhdr->{size});
    my $gzfh = IO::Uncompress::Gunzip->new(\$gz_block);
    my $blkno = 0;
    $main::veryverbose && i::warnit("TAR file read starting from $self->{cinfo}->{block_offsets_ttr} ready to read block offsets - gzfh: ".ref($gzfh)." gzblock_len: $gzlen");
    # offset, record, outpos, sha1_data, gzlen, datalen, blocknum  
    $self->{block_offsets} = [];
    while (<$gzfh>) {
    	chomp;
    	my $entry = [split /\t/, $_];
    	push @{$entry}, 0 if ( scalar(@{$entry}) < 6 );
    	push @{$entry}, $blkno if ( scalar(@{$entry}) < 7 );
		#if not present make calculation (difference with previous line offset)
        $self->{block_offsets}->[-1]->[5] = $entry->[0] - $self->{block_offsets}->[-1]->[0]
                if ( $blkno  && !$self->{block_offsets}->[-1]->[5] );

#        $main::veryverbose and i::warnit("fixed entry $self->{block_offsets}->[-1]->[6] of blk offset: ".join('::', @{$self->{block_offsets}->[-1]}))
#          if $blkno; 
        push @{$self->{block_offsets}}, $entry;
        $blkno++;
    	
    }
    $gzfh->close();
    $gz_block = undef;
    $main::veryverbose and i::warnit("IN Acquiring last entry blk offset ( ".scalar(@{$self->{block_offsets}})." entries): ".join('::', @{$self->{block_offsets}->[-1]}));
    if ( $blkno  && !$self->{block_offsets}->[-1]->[5] ) {
        $blkno--;
		#search block number and size from it's offset ( first column's file)
    	$self->seek($self->{block_offsets}->[-1]->[0], SEEK_SET);
#        $main::veryverbose and i::warnit("Got last block size");
		#add lacking block size 
        $self->{block_offsets}->[-1]->[5] = $self->{currblklen};
        $self->seek(0, SEEK_SET);
    }
#    $main::veryverbose and i::warnit("fixed entry $self->{block_offsets}->[-1]->[6] of blk offset: ".join('::', @{$self->{block_offsets}->[-1]}))
#          if $blkno; 
    $main::veryverbose and i::warnit("block offsets load completed BLOCKS: ".$#{$self->{block_offsets}});
	$main::veryverbose and i::warnit(Data::Dumper("loaded BLOCKS", $self->{block_offsets}));
    $self->{currblknr} = -1;
}

sub _read_gzblock {
    my $self = shift;
    my $blknr = shift;
    $self->_read_block_offsets() if !exists($self->{block_offsets});
    $main::veryverbose and i::warnit("Now trying to access block $blknr as requested by ".join('::', (caller())[0,2]). " LB:".$#{$self->{block_offsets}});
    return undef if ( $blknr > $#{$self->{block_offsets}} );
    
    if ( !exists($self->{gzhdr}) && exists($self->{cinfo}->{data_file_ttr}) ) {
      $self->{infh}->seek($self->{cinfo}->{data_file_ttr}, SEEK_SET);
      if ($self->{cinfo}->{data_file_ttr} == $self->{block_offsets}->[0]->[2]) {
      	my $gzinfo = IO::Uncompress::Gunzip->new($self->{infh})->getHeaderInfo();
      	$self->{block_offsets}->[0]->[2] = $self->{infh}->tell();
      	$self->{block_offsets}->[0]->[4] -= $self->{block_offsets}->[0]->[2] - $self->{cinfo}->{data_file_ttr};
      	$main::veryverbose and i::warnit("First blk offset fixed: "
                    . " First block: ", $self->{cinfo}->{data_file_ttr}
                    . " Data block: ", $self->{block_offsets}->[0]->[2]
                    . " block size: ", $self->{block_offsets}->[0]->[4]
                    . " GZ Header: ", Dumper($gzinfo)
                    );
        $self->{infh}->seek($self->{cinfo}->{data_file_ttr}, SEEK_SET);
      }
      
      my $hdrlen = $self->{infh}->read($self->{gzhdr}, 
                        $self->{block_offsets}->[0]->[2] - $self->{cinfo}->{data_file_ttr})
          || die "unable to read GZHDR -"
                    , " First block: ", $self->{cinfo}->{data_file_ttr}
                    , " Data block: ", $self->{block_offsets}->[0]->[2]
                    , " - $? - $!";
      $main::veryverbose and i::warnit("gzhdr initialized - LEN: ", length($self->{gzhdr}), " DUMP: ", unpack("H80", $self->{gzhdr}));
    }
    if ( !exists($self->{currblknr}) || $blknr != $self->{currblknr} || !exists($self->{currblkioh})) {
        my ( $blkoffset, $blkfirstrec, $blkoutpos, $blksha1, $gzsize, $blksize, $blknum) = 
                                                          @{$self->{block_offsets}->[$blknr]};
        $self->{infh}->seek($blkoutpos, SEEK_SET);
#       $main::veryverbose and i::warnit("INPUT File SEEK performed BLOCK: $blknr BLKSZ: $blksize CURRPOS: ".$self->{infh}->tell());
        my $gzlen = $self->{infh}->read(my $gz_block, $gzsize);
       $main::veryverbose and i::warnit("read filled ".length($gz_block)." bytes of data (returned $gzlen)");
    
        my $tempbuff = '';
        gunzip \(($self->{gzhdr} || '').$gz_block) => \$tempbuff;
        $self->{currblkdata} = $tempbuff;
        $self->{currblklen} = length($self->{currblkdata});
       $main::veryverbose and i::warnit("gzread returned $self->{currblklen} bytes of data - HEAD: "
                                       .unpack('H40',$tempbuff)." TAIL: ".unpack('H40',substr($tempbuff, -20)) );

	    $self->{currblknr} = $blknr;
	    $self->{currblkislast} = ( $blknr == $#{$self->{block_offsets}} );

	    my $blkfh = delete $self->{currblkioh};
	    $blkfh->close() if $blkfh;
	    $blkfh = undef;
	    $self->{currblkioh} = IO::String->new( $self->{currblkdata} );
	    if ( !$self->{currblkioh} ) {
	    	$main::veryverbose and i::warnit("INIT Error for IO::String - $? - $!");
	    	die "MAJOR ERROR";
	    }
	    else {
           $main::veryverbose and i::warnit("read_gzblock returning ".ref($self->{currblkioh})."block $self->{currblknr} of ".$#{$self->{block_offsets}});
        }
    }
    elsif ( exists($self->{currblkioh})) {
    	$self->{currblkioh}->pos(0);
        $main::veryverbose and i::warnit("read_gzblock rewinded ".ref($self->{currblkioh})." block $self->{currblknr} of ".$#{$self->{block_offsets}});
    }
    return  $self->{currblkioh};
}

sub _write_data {
    my $self = shift;
    my $buff_data = shift;
    $main::veryverbose and i::logit("Storage::IN _writeData called by: ".join('::',(caller())[0,2]));
    my $outpos = $self->{infh}->tell();
    if ( !scalar(@{$self->{block_offsets}}) ) {
          $self->{cinfo}->{data_file_ttr} = $outpos;
          $self->{gzfh} = IO::Compress::Gzip->new( $self->{infh}, 
                        Name => $self->{Name} . ($self->{cinfo}->{datasfx} ? ".$self->{cinfo}->{datasfx}" : ''), 
                        ($self->{Comment} ne '' ? (Comment => $self->{Comment}) : ())
                      ) or die("zip error $GzipError");
          $self->{data_written} = $self->{infh}->tell() - $outpos;
          warn "GZ Header written len: ", $self->{data_written}, "\n";
          $outpos = $self->{infh}->tell();
    }

    #my $gz_block = 0x00 x $self->{blocksize};
    my ($from_offset, $from_record, $prevhalf) = (@{$self}{qw(from_offset from_record)}, 0);
    if (scalar(@{$self->{block_offsets}}) > 1) {
        my $halfway = scalar(@{$self->{block_offsets}}) / 2;
        $prevhalf = $self->{block_offsets}->[$halfway]->[2];
    }
    my $gz_chunk = $self->{gzfh};

    my $bytes_written = $gz_chunk->write($buff_data) or die("zip error $GzipError");
    die "Data write error - unexpected written bytes ($bytes_written) - expected value is ", length($buff_data)
                                        unless $bytes_written == length($buff_data); 
    $gz_chunk->flush(Z_FULL_FLUSH);
    my $gzlen = $self->{infh}->tell() - $outpos;
    my $currblknr = scalar(@{$self->{block_offsets}});
    push @{$self->{block_offsets}}, [@{$self}{qw(from_offset from_record)}, 
                            $outpos, Digest::SHA1::sha1_hex($buff_data), $gzlen, $bytes_written, $currblknr ];
    $self->{input_data_size} += $bytes_written; 
    $self->{data_written} += $gzlen; 
    $self->{from_offset} += length($buff_data);
    return length($buff_data);
}

sub Open {
  my $self = shift;
#  my $filereq = shift;
#  my ($mode, $datafile) = ( $filereq =~ /^(\+\<|\<|\>)?([^+<>].+)$/ );
#  $mode = '<' unless $mode;
#
#
#  return $self->Create($datafile, @_) if $mode eq '>';
#  my ( $LocalPathId_IN, $LocalPath, $LocalFileName ) = @_;
  my $args = { @_ };
  $main::veryverbose and i::warnit("Storage::IN Open called by: ".join('::',(caller())[0,2])." args:".Data::Dumper::Dumper($args));
  $args->{datafile} = (split /\/\//, "$args->{LocalPath}/$args->{LocalFileName}", 2)[1];
 
  if ( !ref($self) ) {
 # 	warn "Opening with input mode: $mode \"$datafile\"\n";
  	my $class = $self;
  	$self = {
  		    blocksize => BLOCK_SIZE,
  		    cinfosize => META_DATA_SIZE,
	    	currblkislast => 0,
	 		%{$args}
  	};
 	 bless $self, $class;
  }
  my $datafile = $self->{datafile};
  my $mode = $self->{mode} || 'input';

  my ( $LocalPathId_IN, $LocalPath, $LocalFileName, $ClipId ) = @{$self}{qw(LocalPathId_IN LocalPath LocalFileName ClipId)};
  
  if (!exists($self->{infh})) {
    my $ioclass = __PACKAGE__.'::'.(split /:/, $LocalPath, 2)[0];
    my $opencode;
    eval "require $ioclass; \$opencode = $ioclass->can('Open');";
    die " require error - $@" if $@;
#  	$self->{infh} = IO::File->new("$mode$datafile");
    #$self->{infh} = &$opencode($ioclass, $LocalPathId_IN, $LocalPath, $LocalFileName, $ClipId);
	eval{$self->{infh} = &$opencode($ioclass, $LocalPathId_IN, $LocalPath, $LocalFileName, $ClipId);};
	my $errormessage= "$? - $! - $@";
  	#if ( !$self->{infh} ) {#TESTSAN
	my $errorcount=0;
  	while ( !$self->{infh} ) {
	   if(($errorcount < 10) and ($ioclass =~ /^XReport::Storage::IN::file$/i))
	   {
			my ($ptype, $fqp) = split /:\/\//, $LocalPath, 2;
			my $FileName = "$fqp/$LocalFileName";
			i::logit( "open request from ".join('::',(caller())[0,2])." ended in error for FileName \"$FileName\" - errorcount=$errorcount  - errormessage=\"$errormessage\" ");
			i::logit( "The file \"$FileName\" does not exist or it is not available.") unless (-f $FileName);
			i::logit( "The file \"$FileName\" exists.") if (-f $FileName);
			$errorcount++;
			sleep 2;
			eval{$self->{infh} = &$opencode($ioclass, $LocalPathId_IN, $LocalPath, $LocalFileName, $ClipId);};
			$errormessage= "$? - $! - $@";
			next;
	   }
	   
       #i::logit( "open request from ".join('::',(caller())[0,2])." ended in error for $mode$datafile - $? - $!");
       i::logit( "open request from ".join('::',(caller())[0,2])." ended in error for $mode$datafile - \"$errormessage\"");
       #die "MAJOR Error for infh(" . ref($self->{infh}) . ") opencode ".ref($opencode)." - $? - $!";
       die "MAJOR Error for infh(" . ref($self->{infh}) . ") opencode ".ref($opencode)." - \"$errormessage\"";
  	}
  	$self->{infh}->binmode();
    $self->{tar} = Archive::Tar::Stream->new(infh => $self->{infh});
  }
  
  if (!exists($self->{cinfo})) {
	  $self->{tar} = Archive::Tar::Stream->new(infh => $self->{infh}) if !exists($self->{tar});
	#  warn "tar: ", Dumper($self->{tar}), "\n";
	  $self->{infh}->seek(0, 0);
	  my $firsthdr = $self->{tar}->ReadHeader();
      $main::veryverbose and i::warnit("Storage::IN First TAR Header acquired: ".Data::Dumper::Dumper($firsthdr));
	  $self->{cinfottr} = $self->{infh}->tell();
#	  warn "tar: ", Dumper($self->{tar}), "\ntarhdr: ", Dumper($firsthdr), "\n";
      $self->{cinfo} = $self->unpackStruct($firsthdr->{size});
      $main::veryverbose and i::warnit("Storage::IN CINFO acquired: ".Data::Dumper::Dumper($self->{cinfo}));
#	  warn "CINFO: ", Dumper($self->{cinfo}), "\n";
	  $self->{cinfo}->{maxlrec_detected} = 400 unless exists($self->{cinfo}->{maxlrec_detected}) && $self->{cinfo}->{maxlrec_detected};
  }
  
  $self->_read_block_offsets() if !exists($self->{block_offsets});
#  $self->{data_file_ttr} = $self->{block_offsets}->[0]->[0] if !exists($self->{data_file_ttr});
  $main::veryverbose and i::warnit("Input Archive opened Handler:".ref($self)." block offsets now loaded");
  $self->seek(0, SEEK_SET);
  return $self;
}

sub tell_block {
	my $self = shift;
	return 0 unless exists($self->{currblknr});
	return $self->{currblknr};
}

sub read {
	my $self = shift;
#	warn "STORAGE::IN read handler: ", Dumper($self), "\n"; 
	my $block_size = (exists($self->{cinfo}->{data_block_size}) 
						? $self->{cinfo}->{data_block_size} : $self->{blocksize}); 
	my $localbuff = "\x00" x $block_size;
	
	$self->_read_block_offsets() if !exists($self->{block_offsets});
    $self->_read_gzblock(0) unless exists($self->{currblklen});
    my $blkfh = $self->{currblkioh};
    my $dataleft = $self->{currblklen} - $blkfh->pos();

	my $reqptr = \$_[0];
	my $reqlen = $_[1];
    $reqlen = $dataleft if ( !defined($reqlen) );

	$$reqptr = '';
	my $leftlen = $reqlen;
	while ( length($$reqptr) < $reqlen ) {
       if ( $dataleft == 0 ) {
       	        $main::veryverbose and i::warnit("Now returning undef because last blk") and return undef if $self->{currblkislast} && length($$reqptr) == 0;
       	        last if $self->{currblkislast};
                my $blknr = $self->{currblknr} + 1;

                $blkfh = $self->_read_gzblock($blknr) ||
                                  die "READ Failed attempting to read gzblock num $blknr\n";
                $dataleft = $self->{currblklen}             
        }
        my $len2read = ($leftlen < $dataleft ? $leftlen : $dataleft );
        
        my $numbytes = $blkfh->read($localbuff, $len2read);
        die "READ Failed for $len2read bytes from $dataleft bytes left of $self->{currblklen} bytes buffer"
                     if !defined($numbytes) || $numbytes != $len2read || $numbytes != length($localbuff);
        $$reqptr .= $localbuff;
        $leftlen -= $numbytes;
	    $dataleft -= $numbytes;
	}
	
#   	$main::veryverbose and i::warnit("Now returning ", length($$reqptr), " data bytes of $reqlen requested by ".join('::', (caller())[0,2]));
	return length($$reqptr);
}

sub write {
  my $self = shift;
  my $indata = join('', @_);
  $main::veryverbose && i::logit("IN Write called by: ". join('::',(caller())[0,2]). " - XferMode: $self->{cinfo}->{XferMode}");
  if ( !$indata or !length($indata) ) {
    $main::veryverbose && i::logit("IN Write No data to be written");
    return 0;
  }
  $self->{cinfo}->{maxlrec_detected} = 80 if !exists($self->{cinfo}->{maxlrec_detected});
  my $leftover = (exists($self->{leftover}) ? $self->{leftover} : '');
  if ( length($leftover.$indata) < 32000 ) {
  	$self->{leftover} .= $indata;
    $main::veryverbose && i::logit("IN Write added INDATA to leftover - size now is ".length($leftover));
  }
  else {
     (my $xdata, $self->{leftover}, my $linfos) = &{$self->{recordparser}}($self, $leftover.$indata
                                                   , $self->{blocksize} - length($self->{data_buffer}));
     if ( length($self->{data_buffer}) + length($xdata) < $self->{blocksize}) {
            $self->{at_record} += scalar(@{$linfos});
            $self->{data_buffer} .= $xdata;
     }
     else {
  	    my $buff_data = $self->{data_buffer};
        if ( $self->{cinfo}->{XferMode} == 10 ) {
           $buff_data .= $xdata;
           $self->{at_record} += scalar(@{$linfos});
           $self->{data_buffer} = '';
        }
        else {
  	      while ( scalar(@{$linfos}) && length($buff_data) <= $self->{blocksize} ) {
  		     my $larray = shift @{$linfos};
  		     $buff_data .= pack('n/a*', $larray->[1]); 
  		     $self->{at_record} += 1;
  	      }
          $self->{data_buffer} = ( scalar(@{$linfos}) ? join('', map { pack('n/a*', $_->[1]) } @{$linfos}) : '');
	
        }
        $main::veryverbose && i::logit("IN Write writing ".length($buff_data) . " bytes to output archive");
   	    $self->_write_data($buff_data);
   	    $self->{from_record} = $self->{at_record};
     }
  }
  my $dlen = length($indata);
  $main::veryverbose && i::logit("IN Write Bytes processed: $dlen - data_buffer: ".length($self->{data_buffer})." - leftover: ".length($self->{leftover}));
  return $dlen;
}

sub Close {
  my $self = shift;
  $main::veryverbose && i::logit("IN Close called by: ", join('::',(caller())[0,2]));
  if ( !exists($self->{accesstype}) || $self->{accesstype} ne 'create' ) {
	delete $self->{currblknr};
#	delete $self->{records_buffer};
#	delete $self->{block_offsets};
#	delete $self->{stream_buffer};
  	my $closerc = $self->{infh}->close();
  	delete $self->{infh};
  	return $closerc;
  }
  
  my $datafile = $self->{datafile}.'.tar'; 
  my $data = '';
  $data = $self->{data_buffer} if length($self->{data_buffer});
  $main::veryverbose && i::logit(__PACKAGE__." Closing new archive -". " data_buff: ". length($data). " leftover: ".length($self->{leftover}));
  my ($lastb, $leftover, $records) = ('', '', []);
  ($lastb, $leftover, $records) = &{$self->{recordparser}}($self, $self->{leftover}) if $self->{leftover};
  $data .= $lastb . $leftover;
  $self->_write_data($data) if length($data);
  $self->{from_record} += scalar(@{$records});
  
  if ( $self->{gzfh} ) {
    $main::veryverbose && i::logit("Closing gz data file");
    my $endpos = $self->{infh}->tell();
    $self->{gzfh}->flush(Z_FINISH);
    $self->{gzfh}->close() or die("zip error $GzipError");
    my $lastlen = $self->{infh}->tell() - $endpos;
    $self->{data_written} += $lastlen;
    $main::veryverbose && i::logit("GZ FINISH FLUSH wrote $lastlen bytes - tot len:". $self->{data_written});
  }
  $self->{data_written} = $self->{infh}->tell() - $self->{cinfo}->{data_file_ttr};
  my $blockfiller = "\x00" x (512 - ($self->{data_written} % 512));
  $main::veryverbose && i::logit("Block filler length is ". length($blockfiller));
  $self->{infh}->write($blockfiller) if length($blockfiller) && length($blockfiller) != 512;

  my $index_block = '';
  my $index_chunk = IO::Compress::Gzip->new( \$index_block,
        		Name => 'block_offsets_data', 
  		) or die("zip error $GzipError");
  # can be dumped like this pack('w/(w/a*)', map { pack('w w w H40 w', @$_) } @{$self->{block_offsets}});
  # and the restored like this [ map { [unpack('w w w H40 w', $_) ] } unpack('w/(w/a*')', $buffer ];
  while ( scalar(@{$self->{block_offsets}}) ) {
      my $entry = shift @{$self->{block_offsets}};
	  my $bytes_written = $index_chunk->write(join("\t", @{$entry})."\n") or die("zip error $GzipError");
  }
  $index_chunk->close() or die("zip error $GzipError");
#  warn "Writing now block_offset_data (".length($index_block)." bytes\n";
  my $outpos = $self->{infh}->tell();
  my $tarhdr1 = $self->{tar}->CreateHeader(
  				   $self->{tar}->BlankHeader(
  					 			 name => 'block_offsets_data.gz',
  								 size => length($index_block)
  								 ));
  $main::veryverbose && i::logit("writing tar hdr for \"block_offset\"");                              
  $self->{infh}->write($tarhdr1);

  $self->{infh}->write($index_block);
  $self->{infh}->write("\x00" x (512 - (length($index_block) % 512)));

  $self->{infh}->close(@_);
  delete $self->{infh};

  use Fcntl;
  
  $self->{infh} = IO::File->new("+<$datafile") ||
                   die "open direct error for $datafile - $? ";
  return undef unless $self->{infh};
  
  $self->{infh}->binmode();
  $self->{cinfo}->{block_offsets_ttr} = $outpos;
  $self->{cinfo}->{data_block_size} = $self->{blocksize};
  $self->{cinfo}->{input_data_size} = $self->{input_data_size};
  $self->updateFileCinfo($self->{cinfo}) ;
  my $thispos = $self->{infh}->tell();
  my $infn = basename($self->{datafile});
  if ( $self->{cinfo}->{datasfx} ) {
  	$infn =~ s/\.gz$//;
  	$infn .= '.'.$self->{cinfo}->{datasfx}.'.gz';
  }
  my $tarhdr = $self->{tar}->CreateHeader(
                  $self->{tar}->BlankHeader(
                                name => $infn,
                                size => $self->{data_written}
                                ));

  $self->{infh}->seek($self->{hdrttr}, SEEK_SET);
  $thispos = $self->{infh}->tell();
  $main::veryverbose && i::logit("writing tar hdr for \"$self->{datafile}\" - datafile");
  $self->{infh}->write($tarhdr) || die "unable to update tar file at $self->{hdrttr}";
  $main::veryverbose && i::logit("Tar file updated at $thispos (expected $self->{hdrttr} ) - file size: $self->{data_written}");
  $self->{infh}->close(@_);
  
  delete $self->{infh};
}

sub eof {
	return 1 unless exists($_[0]->{block_offsets});
	return 0 unless exists($_[0]->{currblkioh});
	return (($_[0]->{currblknr} == $#{$_[0]->{block_offsets}}) && $_[0]->{currblkioh}->eof());
}

sub seek {
	my $self = shift;
#    $main::veryverbose and i::warnit(ref($self)."::seek moving to offset $_[0] from whence $_[1] as requested by ".join('::', (caller())[0,2]));
    die "Mixed stream and record access not allowed" if exists($self->{records_buffer});	
	my $reqpos = shift;
	my $whence = shift;
#	warn "SEEK request to position $reqpos from whence $whence\n";
	$self->_read_block_offsets() if !exists($self->{block_offsets});

	# whence = SEEK_SET = begin , means search  from beginning block
	# whence = SEEK_CUR = current , means search from block given from "currblknr" parameter
	# whence = SEEK_END = eof means search  from ending block
	
	my $newpos = 0;
    my $oldblknr = $self->{currblknr} || 0;
    my $blknr = $oldblknr;
    my $currpos = ( exists($self->{currblkioh}) ? $self->{currblkioh}->pos() : 0);
	if ( $whence == SEEK_CUR ) {
		my $reqpos += ($self->{block_offsets}->[$blknr]->[0] + $currpos); 
		$blknr++ while ( $reqpos >  ( $self->{block_offsets}->[$blknr]->[0] 
											+ $self->{block_offsets}->[$blknr]->[5] ));
		$newpos = $reqpos - $self->{block_offsets}->[$blknr]->[0]; 									
#        $self->read(\my $buff, $reqpos - $self->{block_offsets}->[$blknr]->[0])
#           if $reqpos > $self->{block_offsets}->[$blknr]->[0];	
		
	}
	elsif ( $whence == SEEK_SET ) {
		$blknr = 0;
		if ( $reqpos ) {
		  $blknr++ while ( $reqpos >  ( $self->{block_offsets}->[$blknr]->[0] 
											+ $self->{block_offsets}->[$blknr]->[5] ));
          $newpos = $reqpos - $self->{block_offsets}->[$blknr]->[0];                                    
		}	
		
	}
	elsif ( $whence == SEEK_END ) {
		$blknr = scalar(@{$self->{block_offsets}});
		while ( $reqpos  ) {
			$blknr--;
			last if abs($reqpos) < $self->{block_offsets}->[$blknr]->[5];
			$reqpos = abs($reqpos) - $self->{block_offsets}->[$blknr]->[5];
			last if !$blknr; 
		}
        $newpos = $self->{block_offsets}->[$blknr]->[0] - abs($reqpos);                                    
		
	}
	if ( $newpos && $newpos == $self->{block_offsets}->[$blknr]->[5] ) {
      $blknr++;
      $newpos = 0;		
	}
	$main::veryverbose and i::warnit("seek executed with ");
	$self->_read_gzblock($blknr) if (!exists($self->{currblknr}) || $blknr != $self->{currblknr} || !exists($self->{currblkioh}));
	$self->{currblkioh}->pos($newpos);
    return $self->{block_offsets}->[$oldblknr]->[0] + $currpos;   
}

sub tell {
	my $self = shift;
#    die "Mixed stream and record access not allowed" if exists($self->{records_buffer});	
	
	if ( !exists($self->{block_offsets}) ) { $self->_read_block_offsets(); return 0; }
	
    my $fullpos;
    $self->{currblknr} = 0 if  !exists($self->{currblknr}) && exists($self->{currblkioh}); 
	$fullpos = 0 if ( !exists($self->{currblknr}) 
	            || $self->{currblknr} == -1 
	            || !exists($self->{currblkioh})
	            );
	            
	if ( !defined($fullpos) ) {
	   my $iopos = $self->{currblkioh}->pos();
	   $fullpos = $self->{block_offsets}->[$self->{currblknr}]->[0] + $iopos;
#       $main::veryverbose and i::warnit("IN tell called by ", join('::', (caller(1))[0,2]), " IOPOS: $iopos blknr: $self->{currblknr} fullpos: $fullpos"); 
    }
    else {
#       $main::veryverbose and i::warnit("IN tell called by ", join('::', (caller(1))[0,2]), " blknr: $self->{currblknr}"); 
    }
	return $fullpos;
	
}

sub gztell {
    my $self = shift;
    die "Mixed stream and record access not allowed" if exists($self->{records_buffer});    
    
    if ( !exists($self->{block_offsets}) ) { $self->_read_block_offsets(); return 0; }
    
    return 0 if $self->{currblknr} == -1;
    
    return $self->{block_offsets}->[$self->{currblknr}]->[2] + $self->{block_offsets}->[$self->{currblknr}]->[4];
    
}

sub _bsearchNum {
	 my $self = shift;
     my ($array, $val, $keypos) = @_;
     
     # $low is first element that is not too low;
     # $high is the first that is too high
     #
     my ( $low, $high ) = ( 0, $#{$array} );
     my $cur = ($low+$high)/2;
     # Keep trying as long as there are elements that might work.
     #
     while ( $low <= $high ) {
         # Try the middle element.
         do {use integer;
         $cur = ($low+$high)/2;
         };
         last if (($array->[$cur]->[$keypos] <= $val )
               && ($cur == $#{$array} || $array->[$cur+1]->[$keypos] > $val));
         if ($array->[$cur]->[$keypos] < $val  ) {
         	$main::veryverbose and i::warnit( "BSEARCH $high: el $cur\[$keypos\] ($array->[$cur]->[$keypos])LT $val try up");
         	$low  = $cur + 1;                     # too small, try higher
     	} else {
            $main::veryverbose and i::warnit( "BSEARCH $low: el $cur\[$keypos\] ($array->[$cur]->[$keypos]) GE $val try lower");
            
        	 $high = $cur;                         # not too small, try lower
     	}
     }
     return [ @{$array->[$cur]}, $low ];
}

sub _bsearch {
     my $self = shift;
     my ($array, $val, $keypos) = @_;
     
     # $low is first element that is not too low;
     # $high is the first that is too high
     #
     my ( $low, $high ) = ( 0, scalar(@{$array}) );
     my $cur = ($low+$high)/2;
     # Keep trying as long as there are elements that might work.
     #
     while ( $low < $high ) {
         # Try the middle element.
         do {
         use integer;
         $cur = ($low+$high)/2;
         };
         if ($array->[$cur]->[$keypos] lt $val) {
#            $main::veryverbose and i::warnit( "BSEARCH: el $cur\[$keypos\] ($array->[$cur]->[$keypos])LT $val try up");
            $low  = $cur + 1;                     # too small, try higher
        } else {
#            $main::veryverbose and i::warnit( "BSEARCH: el $cur\[$keypos\] ($array->[$cur]->[$keypos]) GE $val try lower");
             $high = $cur;                         # not too small, try lower
        }
     }
     return [ @{$array->[$cur]}, $low ];
}

sub bsearchPos {
	my $self = shift;
    $self->_read_block_offsets() if !exists($self->{block_offsets});
#    $main::veryverbose and i::warnit(ref($self)."::seekPos moving to offset $_[0] requested by ".join('::', (caller())[0,2]));
    my $blken = $self->_bsearchNum($self->{block_offsets}, $_[0], 0);
    return undef if $blken->[0] > $_[0] || $_[0] > ($blken->[0] + $blken->[5]);

    my $blkioh = $self->_read_gzblock($blken->[6]);
    return undef unless $blkioh && ref($blkioh);

    my $newpos = $_[0] - $blken->[0];
    $blkioh->pos($newpos) if $newpos;
}

sub bsearchLine {
	my $self = shift;
    $self->_read_block_offsets() if !exists($self->{block_offsets});
	$main::veryverbose and i::warnit("::bsearchLine passing : $_[1]");
	my $searchType = $_[1]; #(($searchType && $searchType == OFFSET_TYPE) ? $searchType = 0 : $searchType = 1);
	$main::veryverbose and i::warnit("::bsearchLine passing Type: $searchType ");
#    $main::veryverbose and i::warnit(ref($self)."::seekLine moving to line $_[0] requested by ".join('::', (caller())[0,2]));
    my $blken = $self->_bsearchNum($self->{block_offsets}, $_[0], $searchType);
    $main::veryverbose and i::warnit(ref($self)."::bsearchLine reading block \$blken->[]: ".join('::', @{$blken}));
    $main::veryverbose and i::warnit(ref($self)."::bsearchLine reading block \$blken->[6] element: ".$blken->[6]);
    my $blkioh = $self->_read_gzblock($blken->[6]);
    unless ( $blkioh && ref($blkioh) ) {
    	$main::veryverbose and i::warnit("BSearchLine error - unable to read gzblock for $_[0] (".join('::', @{$blken}).")");
    	return undef;
    }
    #my $currec = $blken->[1]; my $curroffset = $blken->[0];
	my $current = $blken->[$searchType];
    while ( $current < $_[0]  && !$self->{currblkioh}->eof() ) {
       $blkioh->read(my $llstr, 2);
       my $ll = unpack('n', $llstr);
       $blkioh->read(my $rec, $ll);
       $main::veryverbose and i::warnit("Read $current - ll: $ll data: ".length($rec));
       #$currec++
	   ($searchType ? $current++ : ($current = 2+$ll+$current));
    }

    if ( $current != $_[0] ) {
        $main::veryverbose and i::warnit("BSearchLine error - requested $_[0] - found $current - eof: ".$self->{currblkioh}->eof());
        return undef;
    }

    return $self;
}

sub POINTSHA1 {
	my $self = shift;
	$self->{sha1_offsets} = [ map { $_ } sort { $a->[3] cmp $b->[3] } @{$self->{block_offsets}}]
	   if !exists($self->{sha1_offsets});
    return $self->_bsearch($self->{sha1_offsets}, $_[0], 3);
}

sub iarLine {
	my $self = shift;
	my $newval = shift;
#    $main::veryverbose and i::warnit(ref($self)."::iarLine called with ".($newval || 'no parms')." by ".join('::', (caller())[0,2]));
	$self->{iarLine} = 0 unless exists($self->{iarLine});
	my $currline = $self->{iarLine};
    $self->{iarLine} = $newval if defined($newval);
	return $currline; 
}

sub getNextRecord {
	my $self = shift;
	my $recdata;
	my $currec = $self->iarLine();
	my $rc = $self->read(my $llstr, 2);
	if ( $rc && $rc == 2) {
	   my $recll = unpack('n', $llstr);
	   $rc = $self->read($recdata, $recll);
	   $recdata = undef if (!$rc || $rc != $recll);
	   $self->iarLine($currec + 1) if $recdata;
	}
    return $recdata;
}

sub rewindRecords {
	my $self = shift;
	delete $self->{currblknr};
	return $self->getNextRecord();
}

use IO::String;

sub _setBTpointers {
  my ($self, $start, $end, $ix, $ttrlist, $ttrpos ) = @_;
  my $btreefh = $self->{btreefh};
  
  $ttrpos = 0 unless $ttrpos; 
  my $mid = $start + ($end - $start) / 2;

  unless ($start > $end) {
    my ($greater, $lesser) =  ($self->_setBTPointers($start,   ($mid - 1), $ix, $ttrlist, $ttrpos),
				               $self->_setBTPointers(($mid+1), $end,       $ix, $ttrlist, $ttrpos));
    $btreefh->pos($ttrlist->[$ix->[$mid]]+4+(8*$ttrpos));
    print $btreefh pack("N*", @{$ttrlist}[$greater, $lesser]);
  }
  return $ix->[$mid];
}

sub _initBTStore {
	my $self = shift;
	$self->{btstore} = '';
    $self->{indexes} = [ 1 ] unless exists($self->{indexes});
    my (@list, @ttrs) = ((), ());
        
	$self->{infh}->seek($self->{cinfo}->{block_offsets_ttr}, SEEK_SET);
    my $idxhdr = $self->{tar}->ParseHeader($self->{tar}->ReadHeader());
	my $gzlen = $self->{infh}->read(my $gz_block, $idxhdr->{size});
    my $gzfh = IO::Uncompress::Gunzip->new(\$gz_block);
    while (!$gzfh->eof()) {
    	my $line = $gzfh->getline();
    	push @list, [split /\t/, $line];
    }
#    shift @list if scalar(@list);
    $gzfh->close();
    $gz_block = undef;
	
	my $ixwords = scalar(@{$self->{indexes}})*2;
	$self->{btreefh} = IO::String->new($self->{btstore}) || die __PACKAGE__, "open error - qm $? - em $! - msg $@\n";
	my $btreefh = $self->{btreefh};
	binmode $btreefh;

    foreach my $bten ( @list ) {
		push @ttrs, $btreefh->pos();
		my $storage = pack("n/a*",pack("n/N$ixwords H40 N2 w*", (map { 0 } (1..$ixwords)), $bten->[4], map { 3*1024*1024*1024 + $_ } @{$bten}[5, 6, 1,2,3]));
  		print $btreefh $storage;
    }
    $gzfh->close();
    $gz_block = undef;
    
	$btreefh->pos(0);
	my $ixmids = [];
	foreach my $ix ( @{$self->{indexes}} ) {
  		my $ttrpos = scalar(@$ixmids);
  		push @{$ixmids}, $self->_setBTpointers(0, $#list, 
  			[ map { \$_[0] } sort { $a->[$ix] <=> $b->[$ix] } @list ] , @ttrs, $ttrpos );
	}
	$btreefh->pos(0);
}

1;

