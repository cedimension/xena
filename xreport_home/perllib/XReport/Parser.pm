#######################################################################
# @(#) $Id: Parser.pm,v 1.2 2002/10/23 22:50:18 Administrator Exp $
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################

package XReport::Parser::NamedNode;

sub getAttr {
  my ($self, $attr) = (shift, shift);

  my $NamedNode = $self->{NamedNode}->getNamedItem($attr);

  return $NamedNode ? $NamedNode->getValue() : '';
}

1;

#todo: verify filtervalues, produce scarti, and or su if, liste per filiale su report completi
#      apply format to filter

package XReport::Parser;

use strict qw(vars);
use Carp; use Carp::Assert qw(assert);

use Symbol;
use File::Path;

use constant EF_PRINT => 1;
use constant EF_DATA => 2;
use constant RF_EBCDIC => 2;

use exporter;
our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, $STOP_ELAB_AFTER_PAGE);
@ISA         = qw(Exporter);
@EXPORT      = qw( );
@EXPORT_OK   = qw( );

use XReport::Util;
use XReport::INFORMAT;

my $XREPORT_HOME = $ENV{'XREPORT_HOME'};

my $perllib = "$XREPORT_HOME/userlib/perllib";

my $DEBUG = 0;

my ($tabre, %tabre) = ("", undef);

my %Languages = (
  english     =>   1,
  french      =>   2,
  german      =>   3,
  spanish     =>   4,
  portuguese  =>   5,
  dutch       =>   6,
  italian     =>   7
);

use constant LAST_ENTRY => 0;
use constant THIS_ENTRY => 1;

#######################################################################
#
#######################################################################

sub ResetUserVars {
  @$U::UserVars{@_} = ();
}

#######################################################################
#
#######################################################################

sub NewOutline {
  my ($self, $OName, $OType, $OEntries, $MarkName, $VarNames) = @_; $OEntries ||= 'ALL';

  my $FileName = $self->{jr}->getFileName('OUTLFILE', $OName);

  my $TSVFile = gensym();

  open($TSVFile, ">".$FileName)
   or
  croak("ApplicationError: OUTLINE ".$OName." OPEN ERROR $!");

  my $Outlines = $self->{Outlines};
  my $OutlineDefs = $self->{OutlineDefs};

  $Outlines->{$OName} = [ $TSVFile, $FileName, "", "" ];
  $OutlineDefs->{$OName} = [ uc($OType), uc($OEntries), $MarkName, $VarNames ];
}

sub ResetOutline {
  my ($self, $OName) = @_; my $Outline = $self->{Outlines}->{$OName};

  my ($TSVFile, $FileName) = @{$Outline}[0..1];

  open($TSVFile, ">".$FileName)
   or
  croak("ApplicationError: OUTLINE ".$OName." OPEN ERROR $!");

  $Outline->[2] = "";
}

sub NewOutlineEntry {
  my ($self, $page, $line, $OName) = @_; return if $self->{IDENTIFY_PHASE};

  my $OutlineDef = $self->{OutlineDefs}->{$OName};

  my ($OType, $OEntries, $MarkName, $VarNames) = @$OutlineDef;

  my $VarValues = join("\t", @{$U::UserVars}{@$VarNames});

  return if $VarValues eq "";

  &$logrRtn("NewOutlineEntry $OName $VarValues") if $DEBUG;

  my $Outline = $self->{Outlines}->{$OName};

  return if $VarValues eq $Outline->[2] and $OEntries ne 'ALL';

  my ($atProgr, $TSVFile) = ($self->incrValues('atProgr', 1), $Outline->[0]);

  my ($atPage, $atLine, $atOffset); my $OUTPUT = $self->{'OUTPUT'};

  if ( $MarkName eq '' ) {
    $atPage = $OUTPUT->atPage(); $atLine = ($line) ? $line->atLine() : 0; $atOffset = $page->atOffset();
  }
  else {
    my $Mark = $self->{Marks}->{$MarkName};
    #die "UserError: OUTLINE MARK NOT SET ======" if !$Mark;
    $atPage = $Mark->[0]; $atLine = $line ? $Mark->[1] : undef; $atOffset = $Mark->[2];
  }

  $self->{OUTPUT}->InsertOutlineTarget($atProgr, $OType);

  print $TSVFile "$VarValues\t$atPage\t$atOffset\t$atLine\t$atProgr\n"; $Outline->[2] = $VarValues;
}

sub CreOutlineTree {
  my ($self, $OName) = (shift, shift); my $Outline = $self->{Outlines}->{$OName}; my $OutlineTree;

  my ($TSVFile, $FileName) = @{$Outline}[0..1];

  &$logrRtn("BEGIN of CreOutlineTree for $OName");

  open($TSVFile, "<$FileName")
   or
  croak("ApplicationError: TSVFile <$FileName> INPUT OPEN ERROR $!"); local $_;

  while(<$TSVFile>) {
    my @VarValues = split(/\t/, $_); my $atProgr = pop(@VarValues);
    $OutlineTree = { '->' => [$atProgr] } if !$OutlineTree;
    my $OutlineNode = $OutlineTree;

    splice(@VarValues, -3, 3);

    for (@VarValues) {
      if ( !exists($OutlineNode->{$_}) ) {
        #todo: <--- Verificare Sequenza ?!
        $OutlineNode->{$_} = { '->' => [$atProgr] };
      }
      $OutlineNode = $OutlineNode->{$_};
    }
    push @{$OutlineNode->{'->'}}, $atProgr if ( $OutlineNode->{'->'}->[0] != $atProgr );
  }

  close($TSVFile);

  &$logrRtn("END of CreOutlineTree for $OName");

  return $OutlineTree;
}

sub NewOutlineEntries {
  my ($self, $page, $line) = (shift, shift, shift);
  for ( @_ ) {
    NewOutlineEntry($self, $page, $line, $_);
  }
}

sub PutOutlineTrees {
  my $self = shift; my $OUTPUT = $self->getValues('OUTPUT');
  my $OutlineDefs = $self->{OutlineDefs};
  my $Outlines = $self->{Outlines};

  for my $OName ( sort(keys(%$Outlines)) ) {
    my $OType = $OutlineDefs->{$OName}->[0];

    my $Outline = $Outlines->{$OName};

    my $TSVFile = $Outline->[0];

    if ( !tell($TSVFile) ) {
      close($TSVFile);
      return;
    }
    close($TSVFile);

    my $OutlineTree = $self->CreOutlineTree($OName);

    $OUTPUT->PutOutlineTree($OName, $OType, { $OName => $OutlineTree }); #unlink $Outline->[1];
  }
}

sub ResetOutlineTrees {
  my $self = shift; my $Outlines = $self->{Outlines};

  for my $OName ( sort(keys(%$Outlines)) ) {
    $self->ResetOutline($OName);
  }
}

#######################################################################
#
#######################################################################

sub NewIndex {
  my ($self, $IName, $IType, $IEntries, $MarkName, $ExcelName, $TableName, $VarNames, $Childs) = @_;

  my $FileName = $self->{jr}->getFileName('IXFILE', $IName);

  my $FieldFormats = {};

  my @t = @$VarNames; @$VarNames = (); $IEntries ||= 'ALL';

  for (@t) {
    if ($_ !~ /\./ ) {
      push @$VarNames, $_
    }
    else {
      $FieldFormats->{$VarNames->[-1]} = $_;
    }
  }

  $self->{IndexDefs}->{$IName} = [
    uc($IType), uc($IEntries), $MarkName, $VarNames, $Childs
  ];

  my $Index = {
    Name => $IName,
    FileName => $FileName,
    ExcelName => $ExcelName,
    FieldFormats => $FieldFormats
  };
  $self->{Indexes}->{$IName} = $Index;

  my $TSVFile = gensym();

  open($TSVFile, ">".$FileName)
   or
  croak("ApplicationError: INDEX ".$IName." OPEN ERROR $!");

  print
    $TSVFile "JobReportId\t", join("\t", @$VarNames),
    $IType eq "PAGE" ? "\tFromPage\tForPages\n" : "\tFromPage\tFromLine\tForLines\n"
  ;

  $Index->{TSVFile} = $TSVFile;
  $Index->{TableName} = $TableName;

  $Index->{ClIndexTable} = ($TableName ne '' and $TableName eq $self->{jr}->getValues('ClIndexTable'));
}

sub NewIndexEntry {
  my ($self, $page, $line, $IName) = @_; return if $self->{IDENTIFY_PHASE};

  #todo: add line type index
  $main::veryverbose && i::warnit("Sono entrato in IndexEntry");
  my $IndexDefs = $self->{IndexDefs};
  my $Indexes = $self->{Indexes};
  my $IndexDef = $IndexDefs->{$IName};

  my ($IType, $IEntries, $MarkName, $VarNames, $Childs) = @$IndexDef;

  my $Index = $Indexes->{$IName}; my $TSVFile = $Index->{TSVFile};

  my $VarValues = join("\t",
     map {
       $_ =~ s/(?:^ +)|(?: $)//g; #todo: if required
       $_ =~ s/\n|\r|\t/ /g;
       $_
     }
     @{$U::UserVars}{@$VarNames}
  );
  $main::veryverbose && i::warnit("Sono entrato in IndexEntry ".join(' ', @$VarNames)."");
  my ($latPage, $latLine, $lVarValues) = @{$Index}{qw(latPage latLine lVarValues)};

  return if $VarValues eq $lVarValues and $IEntries ne 'ALL';

  for (@$Childs) {
    $self->CloseIndexEntry($page, LAST_ENTRY, $line, $_);
  }

  my $JobReportId = $self->getValues('JobReportId');

  my ($atPage, $atLine); my $OUTPUT = $self->{'OUTPUT'};

  if ( $MarkName eq '' ) {
    $atPage = $OUTPUT->atPage(); $atLine = $line ? $line->atLine(2) : undef;
  }
  else {
    my $Mark = $self->{Marks}->{$MarkName};
    #die "UserError: OUTLINE MARK NOT SET ======" if !$Mark;
    $atPage = $Mark->[0]; $atLine = $line ? $Mark->[1] : undef;
  }

  if ( $lVarValues ne "" and ($MarkName eq '' or 1)) {
    if ( $Index->{ClIndexTable} ) {
      $self->BREAKABLE_PAGE();
    }
    print $TSVFile ($IType eq "PAGE" ? $atPage-$latPage : $atLine-$latLine), "\n"; $Index->{TotEntries} += 1;
    #$self->{jr}->setValues('HasIndexes', '1');
  }

  do {
    @{$Index}{qw(latPage latLine lVarValues)} = (undef, undef, undef); return;
  }
  if ($U::UserVars->{'CODICE_DOCUMENTO'} =~ /^XXXXXX\s*$/);
  $self->{jr}->setValues('HasIndexes', '1');
  print $TSVFile
   "$JobReportId\t$VarValues\t$atPage".
   ($IType eq "PAGE" ? '': "\t".$line->atLine(1)). "\t";

  @{$Index}{qw(latPage latLine lVarValues)} = ($atPage, $atLine, $VarValues);
}

sub NewIndexEntries {
  my ($self, $page, $line) = (shift, shift, shift);
  for ( @_ ) {
    NewIndexEntry($self, $page, $line, $_);
  }
}

sub CloseIndexEntry {
  my ($self, $page, $line, $tol, $IName) = @_; return if $self->{IDENTIFY_PHASE};

  my $IndexDefs = $self->{IndexDefs};
  my $Indexes = $self->{Indexes};
  my $IndexDef = $IndexDefs->{$IName};

  my ($IType, $IEntries, $MarkName, $Childs, $VarNames) = @$IndexDef;

  my $Index = $Indexes->{$IName}; my $TSVFile = $Index->{TSVFile};

  my ($latPage, $latLine, $lVarValues) = @{$Index}{qw(latPage latLine lVarValues)};

  return if $lVarValues eq "";

  for (@$Childs) {
    $self->CloseIndexEntry($page, $line, $tol, $_);
  }

  my $JobReportId = $self->getValues('JobReportId'); my $OUTPUT = $self->{'OUTPUT'};

  my ($atPage, $atLine) = ($OUTPUT->atPage(), $line ? $line->atLine(2) : $page->atLine()+1);

  if ( $lVarValues ne "" ) {
    if ( $Index->{ClIndexTable} ) {
      if ( $tol == LAST_ENTRY ) {
        $self->BREAKABLE_PAGE();
      }
      else {
        $self->{'NEXT_PAGE_IS_BREAKABLE'} = 1;
      }
    }
    print $TSVFile (($IType eq "PAGE" ? $atPage-$latPage : $atLine-$latLine)+$tol), "\n";

    $Index->{TotEntries} += 1;
  }

  @{$Index}{qw(latPage latLine lVarValues)} = (undef, undef, undef);
}

sub CloseIndexEntries {
  #todo: change to map with NewIndexEntries
  my ($self, $page, $line, $tol) = (shift, shift, shift, shift);
  for ( @_ ) {
    CloseIndexEntry($self, $page, $line, $tol, $_);
  }
}

sub emailFilesFromExtract {
  my ($INPUT, $jr, $list, $emailstor, $writeback) = @_; my ($JobReportId, $ipdf, $PageXref);

  die "^^^^^^^^^^^^^^^^^^^^^^^^^^^";
  $JobReportId = $jr->getValues('JobReportId'); $ipdf = 0; <$INPUT>;

  $PageXref = $jr->getPageXref();

  while(!eof($INPUT)) {
    my ($lid, $LowPage, $HighPage) = splice(@$PageXref,0,3); my $ToPage = 0;

    my $doc_in = XReport::PDF::DOC->Open($jr->getFileName("PDFOUT",$lid));

    do {
      my $line = <$INPUT>; chomp $line; @$list = split("\t", $line); $line = ''; $ipdf+=1;

      my ($FromPage, $ForPages) = @{$list}[-2,-1]; $ToPage = $FromPage + $ForPages - 1;

      my $EmailPdfFile = "file://$JobReportId.".substr("000000$ipdf",-7).".pdf";

      my $doc_out = XReport::PDF::DOC->Create(
        "$emailstor/$EmailPdfFile"
      );
      $doc_out->CopyPages($doc_in, $FromPage-$LowPage+1, $ForPages); $doc_out->Close();

      &$writeback($EmailPdfFile, $FromPage, $ForPages);
    }
    while($ToPage < $HighPage);

    $doc_in->Close();
  }
}

sub emailFilesFromIndex {
  my ($INPUT, $jr, $list, $email_values, $emailstor, $writeback) = @_; my ($ipdf, $FromPage, $ForPages);

  my $JobReportId = $jr->getValues('JobReportId'); $ipdf = 0; <$INPUT>;

  my $EmailExitArgs = $email_values->{EmailExitArgs};

  die "INVALID EmailExitArgs (ref() ne 'CODE') "
   if
  ref($EmailExitArgs) ne 'CODE';

  while(!eof($INPUT)) {
    my $line = <$INPUT>; chomp $line; @$list = split("\t", $line); $line = ''; $ipdf+=1;
    $FromPage += 1;

    my ($PATH_NAME, $FILE_NAME) = &$EmailExitArgs() =~ /PATH_NAME=([^,]+),FILE_NAME=([^,]+)/;

# copy file
# my $EmailPdfFile = "file://$JobReportId.".substr("000000$ipdf",-7).".pdf";
    &$writeback("file://$PATH_NAME/$FILE_NAME", $FromPage, 1);
  }
}

sub CloseIndexes {
  my $self = shift; my ($INPUT, $OUTPUT, $IndexDefs, $Indexes, $jr) = @{$self}{qw(INPUT OUTPUT IndexDefs Indexes jr)};

  my @ExcelSheets = ();

  for my $IName ( sort(keys(%$Indexes)) ) {
    my $Index = $Indexes->{$IName}; my ($TSVFile, $latPage, $latLine) = @{$Index}{qw(TSVFile latPage latLine)};

    my $IndexDef = $IndexDefs->{$IName};

    my ($IType, $IEntries, $MarkName, $VarNames, $Childs) = @$IndexDef;

    if ( $Index->{latPage} ne "" ) {
      my ($atPage, $atLine) = ($OUTPUT->atPage(), $INPUT->atLine());

      print $TSVFile ($IType eq "PAGE" ? $atPage-$latPage+1 : $atLine-$latLine+1), "\n";

      $Index->{TotEntries} += 1;
    }
    CORE::close($TSVFile);

    my $idxdef = $self->getValues('%SharedIndexes')->{$IName};

    if ( $Index->{ClIndexTable} ) {
      if (my $trVar = uc($idxdef->getAttribute('UserTimeRefVar')) ) {
        my ($INPUT, $IdxFILE) = (gensym(), $Index->{FileName});

        open($INPUT, "<$IdxFILE")
         or
        die("INFILE INPUT OPEN ERROR \"$IdxFILE\"$!"); # NO binmode($INPUT); !!!

        my @IVarNames = split("\t", <$INPUT>); my $ipos = 0;
        for (@IVarNames) {
          last if uc($_) eq $trVar; $ipos += 1;
        }
        if ( $ipos < scalar(@IVarNames) ) {
          for (1..17) {
            my $trValue = (split("\t", <$INPUT>))[$ipos]; next if $trValue eq '';
            $self->setValues(
              UserTimeRef => $trValue
            );
            $Index->{UserTimeRef} = $trValue; last;
          }
        }
        else {
          &logrRtn("WARNING: UserTimeRefVar NOT DEFINED FOR ClINDEX $IName");
        }
        close($INPUT);
      }
      $jr->setValues(ParsedDocs => $Index->{TotEntries} || 0);
    }

    $perllib = getConfValues('userlib')."/perllib";
    for my $exit (getChildsByName($idxdef, 'exit')) {
      my $perlclass = $exit->getAttribute('perlclass');
      my $ref = $exit->getAttribute('ref');

      require "$perllib/$ref";

      my $idxexit = $perlclass->new(
        $IName, $jr, $jr->getFileName('workdir'), $exit
      );

      $idxexit->Commit(); $idxexit->Close();
    }

    my $indexloadername = $XReport::cfg->{parser}->{indexLoader};
    $indexloadername = 'LoadIndexTable' unless $indexloadername && $indexloadername !~ /^\s*$/;
    my $indexloader = $jr->can($indexloadername);
    $indexloader = sub { return 1; } unless ref($indexloader) eq 'CODE';
    $Index->{'TableName'} and &$indexloader($jr, @{$Index}{qw(
      TableName FileName UserTimeRef TotEntries
    )});

    $Index->{'ExcelName'} and  push @ExcelSheets, {
      FieldFormats => $Index->{'FieldFormats'}, ExcelName => $Index->{'ExcelName'}, TsvFileName => $Index->{'FileName'}
    };

    ### test for jr->sentEmail;

    for my $email (getChildsByName($idxdef, 'email')) {
      my ($IType, $IEntries, $MarkName, $VarNames, $Childs) = @$IndexDef;

      my $vars_pos = {JobReportId => 0}; my $list_pos = 1;

      my @list = (); my $totEmails = 0;

      my $email_values = {
        Status => 15, SendNumber => 1, TotRetries => -1
      };

      for (@$VarNames, qw(FromPage ForPages)) {
        $vars_pos->{$_} = $list_pos; $list_pos += 1;
      }

      for my $field (qw(
        SmtpServer MailClass FromField FakeFromField ToField ToFakeField SubjectField BodyField
        EmailerArgs EmailerExitArgs EmailExitArgs
      )) {
        my $value = $email->getAttribute($field);
        if ( $value =~ /\$\w+/ ) {
          $value =~ s/\$(\w+)/ exists($vars_pos->{$1}) ? "\$list[$vars_pos->{$1}]" : "\$$1"/eg;
        }
        if ( $value !~ /\$list\[/ ) {
          $email_values->{$field} = $value;
        }
        else {
          $email_values->{$field} = eval("sub {\"$value\";}");
        }
      }

      my @email_fields = qw(
        StartTime EndTime SmtpServer TotBytes Status SendNumber TotRetries MailClass
        FromField FakeFromField ToField ToFakeField
        SubjectField BodyField EmailerArgs EmailerExitArgs
      );

      my ($IdxFILE, $EmailFILE) = ($Index->{FileName}, $jr->getFileName('TEMPFILE','Emailer'));

      my ($INPUT, $OUTPUT) = (gensym(), gensym());

      open($INPUT, "<$IdxFILE")
       or
      die("INFILE INPUT OPEN ERROR \"$IdxFILE\"$!"); # NO binmode($INPUT); !!!

      open($OUTPUT, ">$EmailFILE")
       or
      die("OUTFILE OUTPUT OPEN ERROR \"$EmailFILE\"$!"); # NO binmode($OUTPUT); !!!

      print $OUTPUT join("\t", @email_fields, qw(JobReportId FromPage ForPages)), "\n";

      my ($JobReportId, $JobReportName) = $jr->getValues(qw(JobReportId JobReportName));

      my $emailstor = getConfValues('emaildir')."/$JobReportId"; rmtree($emailstor);

      mkpath($emailstor) or die "MKPATH ERROR for \"$emailstor\" rc:$!";

      my $writeback = sub {
        my ($FileAttachments, $FromPage, $ForPages) = @_; my $line = ''; $totEmails += 1;
        for my $field (@email_fields) {
          my $value = $email_values->{$field};
          $line .= (ref($value) ? &{$value} : $value) . "\t";
        }
        print $OUTPUT $line, "$FileAttachments\t$JobReportId\t$FromPage\t$ForPages\n";
      };

      my @eargs = ($INPUT, $jr, \@list, $email_values, $emailstor, $writeback);

      if ($email->getAttribute('ExtractFiles')) {
        emailFilesFromExtract(@eargs)
      }
      else {
        emailFilesFromIndex(@eargs)
      }

      close($OUTPUT); close($INPUT);

      die "ENDING NOW. totEmails=$totEmails $EmailFILE\n";

      $jr->LoadEmailTable($EmailFILE, $totEmails); unlink $EmailFILE;
    }
  }

  $jr->LoadExcelSheets(\@ExcelSheets) if @ExcelSheets;

  for my $IName ( sort(keys(%$Indexes)) ) {
    my $Index = $Indexes->{$IName}; #todo: unlik if user required

    #unlink $Index->{'FileName'} if -s $Index->{'FileName'} > 10000000; ### todo: unlik if user required
  }
}

#######################################################################
#
#######################################################################

sub NewExecEntries {
  my ($self, $page, $line) = (shift, shift, shift);
  my $ExecDefs = $self->getValues('ExecDefs');
  for my $exec ( @_ ) {
    &{$ExecDefs->{$exec}}($self, $page, $line);
  }
}

#######################################################################
#
#######################################################################

sub GetPage {
  my $self = shift; my ($INPUT, $OUTPUT) = $self->getValues(qw(INPUT OUTPUT));

  my $curr_iar_line = $INPUT->iarLine();
  $curr_iar_line -= 1 if $curr_iar_line;
  $self->{curr_page_offset} = [ $OUTPUT->atPage() + 1,
                                      $curr_iar_line,
                                      $INPUT->tell() ];
#  $main::debug && i::warnit(ref($self)."::GetPage INIT Page Offset at ", ref($INPUT), " line ", $INPUT->atLine(), " ", join('::', @{$self->{curr_page_offset}}));
  $main::debug and i::warnit(__PACKAGE__." GetPage curr_page_offset: ".join('::', @{$self->{curr_page_offset}})
                            ."- caller: ".join('::', (caller())[0,2]));

  return undef if !(my $page  = $INPUT->GetPage());

#  $OUTPUT->BeginPage($page);
  if ( $self->{'NEXT_PAGE_IS_BREAKABLE'} ) {

    $self->BREAKABLE_PAGE(); $self->{'NEXT_PAGE_IS_BREAKABLE'} = 0;
  }
  $OUTPUT->BeginPage($page);

  return $page;
}

sub ReleasePage {
  my ($self, $page) = @_; my $OUTPUT = $self->getValues('OUTPUT');

  $OUTPUT->EndPage($page);

  $self->incrValues('InputLines',$page->totLines(), 'InputPages',1);
}

sub createPageOffsetsTSV {
    my $self = shift;
    my $tsvfilen =  $self->{jr}->getFileName('IXFILE', '$$PageOffsets$$');
    $self->{PageOffsetsTSV} = IO::File->new(">$tsvfilen");
    my $potsv = $self->{PageOffsetsTSV};
    print $potsv ''.join("\t", qw(from_page from_line from_offset for_lines)), "\n";
}

sub stripNamedNode {
	my $domstr = shift;
	return $domstr unless ref($domstr);
	return [ map { stripNamedNode($_) } @{$domstr} ] if ref($domstr) eq 'ARRAY';
	return { map {$_ => stripNamedNode($domstr->{$_}) } grep !/^NamedNode$/, keys %{$domstr} }
	        if ref($domstr) =~ /^(?:HASH|XReport::Parser::NamedNode)$/;
    $main::veryverbose && i::warnit("stripNamedNode Unhandled struct type: ".ref($domstr));
	return $domstr;
}


#use XML::Simple qw();
sub doElab {
  my $self = shift; my ($PageDefs, $INPUT, $OUTPUT) = $self->getValues(qw(PageDefs INPUT OUTPUT));

  my ($page, $LineDef, $ActivePageDef, $CatActivePageDef);

  $main::debug && i::warnit("INPUT PARSING STARTED !!");
  my $categories = $self->getValues('cateList');
  $main::debug && i::warnit(ref($self).'::doElab processing categories '
                            .Dumper($categories). ' over pagedefs '
                            .Dumper(stripNamedNode($PageDefs)));
#  $main::debug and do {
#  	my $pdefs = [];
#    foreach my $pdef ( @{$PageDefs} ) {
#      push @{$pdefs}, { map { $_ => $pdef->{$_} } grep !/^NamedNode$/, keys %{$pdef} };
#    }
#    i::warnit(ref($self)."::doElab processing reports using Pagedefs(".ref($PageDefs)."): ".Dumper($pdefs));
#  };

  my $pgmkexcel = $self->{jr}->getValues('PageMKExcel');


  while( $page = $self->GetPage() ) {
  
	if ($pgmkexcel && (my $rtn = $pgmkexcel->can('Print')) ) {
		&$rtn($pgmkexcel, $page);
	}
	  
	  
	  
    my $defaultPage ;
    foreach my $catid (@{$categories}){
        $U::CatUserVars->{$catid} = {} if !exists($U::CatUserVars->{$catid});
        $U::UserVars = $U::CatUserVars->{$catid};
        my $pageMatched = 0;my $contid = 'Y';
        #$main::veryverbose && i::warnit("OPSS: ".Dumper($ActivePageDef));
        for my $PageDef (@$PageDefs) {
          $PageDef->{category} = 'NULLLIST' unless ($PageDef->{category});
          $main::veryverbose && i::warnit("Cate : $catid NAME : $PageDef->{name} PAGEDEF :cate $PageDef->{category} Contid : $PageDef->{contid}");
          #if($PageDef->{name} eq "__NOMATCH__"){$defaultPage = $PageDef; next};
          if($PageDef->{category} ne $catid) {next;}
          if ( &{$PageDef->{PageTestSub}}($page) ) {
            $pageMatched = 1;
            $contid = $PageDef->{contid};
            if ( $ActivePageDef and $ActivePageDef ne $PageDef ) {
                &{$ActivePageDef->{PageEndPageTypeSub}}($self, $page, $PageDef);
            }
            $main::veryverbose && i::warnit("Sono entrato con category $catid e Pag--- $PageDef->{name}");
            $ActivePageDef = $PageDef;
            $CatActivePageDef->{$catid} = $PageDef;
            &{$PageDef->{PageExecSub}}($self,$page);
            $OUTPUT->ActivePageDefIs( $PageDef ) if $OUTPUT->can( 'ActivePageDefIs' ); last;
          }
          else {
          	$main::veryverbose && i::warnit("Current page failed testsub defined");
          }
         }
         #$CatActivePageDef->{$catid} = $self->{NoPageDef} if (!defined($CatActivePageDef->{$catid}));
         $ActivePageDef = $self->{NoPageDef} if (!defined($ActivePageDef));
         #$main::veryverbose && i::warnit("Continuosid vale : ".Dumper($ActivePageDef));
        if((!defined($CatActivePageDef->{$catid})) or (!$pageMatched && $CatActivePageDef->{$catid}->{contid} eq 'N')){
          #$main::veryverbose && i::warnit("Setting default page : ".Dumper($ActivePageDef)."\n ");
           #$ActivePageDef = $self->{NoPageDef} if (!defined($ActivePageDef));
           $CatActivePageDef->{$catid} = $self->{NoPageDef};
           &{$self->{NoPageDef}->{PageExecSub}}($self,$page);
           #$contid = 'Y';
        }
        #-----------------------------------------##
        ### test for ranges
        $self->CheckPageInclusion($page,$catid,$contid);
        #-----------------------------------------##
    }
    $main::veryverbose && i::warnit(ref($self)."::doElab now processing PageDefs ("
      . ($ActivePageDef ? 'defined' : 'undefined' )
      . ") and LineDefs(".scalar(@{$ActivePageDef->{LineDefs}}).")" );
    if ( !$ActivePageDef or !@{$ActivePageDef->{LineDefs}} ) {
          $OUTPUT->PutPage($page);
          $self->{curr_page_offset}->[3] = $INPUT->iarLine() - $self->{curr_page_offset}->[1] - 1;
          push @{$self->{page_offsets}}, [ @{$self->{curr_page_offset}} ];
          $main::debug && i::warnit("ADDED Page Offset at line ", $INPUT->atLine(), " ", join('::', @{$self->{curr_page_offset}}));
          $self->{curr_page_offset} = [];
          if ( scalar(@{$self->{page_offsets}}) >= 100 ) {
             $self->createPageOffsetsTSV() if ( !exists($self->{PageOffsetsTSV}) );
             my $potsv = $self->{PageOffsetsTSV};
             print $potsv ''.join("\n", map { join("\t", @$_) } @{$self->{page_offsets}} ), "\n";
             @{$self->{page_offsets}} = [];
          }
    }
    else {
       my ($lineList, $LineDefs, $ActiveLineDef) = ($page->lineList(), undef, undef);
       $LineDefs = $ActivePageDef->{LineDefs};
       ### exec line definitions begin -------------------##
       $main::veryverbose && i::warnit(ref($self)."::doElab now processing pageLines (".scalar(@$lineList)
                      .") with LineDefs(".scalar(@{$LineDef}).")");
       for my $line (@$lineList) {
          for my $LineDef (@$LineDefs) {
             my $lineValue = $line->AsciiValue();
             if ( &{$LineDef->{LineTestSub}}($lineValue) ) {
                if ( $ActiveLineDef and $ActiveLineDef ne $LineDef ) {
                   &{$ActiveLineDef->{LineEndLineTypeSub}}();
                }
                $ActiveLineDef = $LineDef;
                &{$LineDef->{LineExecSub}}($self, $page, $line, $lineValue); last; ## more ops
             }
          }
          $OUTPUT->PutLine($line);
       }
       &{$ActiveLineDef->{LineEndLineSub}}() if $ActiveLineDef;
       ### exec line definitions end ---------------------##
    }

    &{$ActivePageDef->{PageEndPageSub}}() if $ActivePageDef;

    $self->ReleasePage($page);
    #last if 0 and $OUTPUT->atPage()>=150;	
	
    if($main::STOP_ELAB_AFTER_PAGE)
	{ 
		&$logrRtn("FORCED STOP OF PROCESSING AFTER ".$main::STOP_ELAB_AFTER_PAGE." PAGES BECAUSE THE PARAMETER \"STOP_ELAB_AFTER_PAGE\" IS FOUND!") if ($OUTPUT->atPage() >=$main::STOP_ELAB_AFTER_PAGE);
		last if ($OUTPUT->atPage() >= $main::STOP_ELAB_AFTER_PAGE);
	}

  }
  
  	if ($pgmkexcel && (my $rtn = $pgmkexcel->can('Finalize')) ) {
		&$rtn($pgmkexcel);
		$self->{jr}->setValues('ExistTypeMap',  $pgmkexcel->{'ExistTypeMap'});
	}

  &$logrRtn("INPUT PARSING ENDED !!");
}

#######################################################################
#
#######################################################################

use constant FV_INVALID_FORMAT => -1;
use constant FV_NOT_INTABLE => -2;
use constant FV_VALID => 1;

sub VerifyFilterValues {
  my ($self, $page) = @_;

  my (
    $FilterVarsDef, $FilterVarsValues, $OUTPUT
  ) =
  @{$self}{qw(FilterVarsDef FilterVarsValues OUTPUT)};

  for my $FilterVar (keys(%$FilterVarsDef)) {
    $main::veryverbose and &$logrRtn("VerifyFilterValues UserVars--".Dumper($U::UserVars->{$FilterVar})."---");
    $U::UserVars->{$FilterVar} =~ s/^ +| +$//g if(defined($U::UserVars->{$FilterVar}));
    $main::veryverbose and &$logrRtn("VerifyFilterValues2 UserVars--".Dumper($U::UserVars->{$FilterVar})."---");
    my $FilterVarValues = $FilterVarsValues->{$FilterVar};
    my $FilterVarValue = $U::UserVars->{$FilterVar};

	my $Value = $FilterVarValue;
	if(!defined($FilterVarValue)){
		$FilterVarValue = '__UNDEF__';$Value = '__NOMATCH__' ;
	}
    next if exists($FilterVarValues->{$FilterVarValue});

    my $checkformat = $FilterVarsDef->{$FilterVar};
    if (!ref($checkformat)) {
        $main::veryverbose and &$logrRtn("VerifyFilterValues NotCheck");
		$FilterVarValues->{$FilterVarValue} = [$Value, FV_VALID];
		next;
    }
    if ( !&$checkformat($Value) ) {
        $main::veryverbose and &$logrRtn("VerifyFilterValues CheckForm");
      $FilterVarValues->{$FilterVarValue} = [$Value, FV_INVALID_FORMAT]; next;
    }

    if ($self->{jr}->FilterValue_IN_TABLE($FilterVar, $Value, $OUTPUT->atPage())) {
        $main::veryverbose and &$logrRtn("VerifyFilterValues In Table");
      $FilterVarValues->{$FilterVarValue} = [$Value, FV_VALID];
    }
    else {
      $FilterVarValues->{$FilterVarValue} = [$Value, FV_NOT_INTABLE];
    }
  }
  $main::veryverbose and &$logrRtn("Return VerifyFilterValues".Dumper($FilterVarsValues));
  return $FilterVarsValues;
}

#######################################################################
#
#######################################################################

sub CheckPageInclusion {
  my ($self, $page,$catid,$contid) = @_;
  $main::debug && i::warnit(__PACKAGE__." CheckPageInclusion for cat. $catid (contid $contid) caller: ".join('::', (caller())[0,2]));
  $main::veryverbose && i::warnit("CheckPageInclusion Self object: ". Dumper(stripNamedNode(\{%$self})));
  $main::veryverbose && i::warnit("Check Category".Dumper($catid));
  my ($FileXref, $ReportDefs, $OUTPUT, $jr) = $self->getValues(qw(FileXref ReportDefs OUTPUT jr));
  my $descr = $jr->getValues(qw(JobReportDescr));
  $main::veryverbose && i::warnit("CUTVAR 1-----".Dumper( $U::UserVars->{CUTVAR}));
  my $VerifyFilterValues = $self->VerifyFilterValues($page) if $self->{'FilterVarsDef'};

  my ($PageIncluded, $FilterVarsAcceptable) = (0, 1);

  do {
  	$PageIncluded = 1; goto UPDATE_COUNTERS;
  } if $page->overflowed();
  $main::veryverbose && i::warnit("CUTVAR 2----- ".Dumper( $U::UserVars->{CUTVAR}));
  for my $Report ( @$ReportDefs ) {
#    warn "REPORT:\n", Dumper($Report), "\n";
    my $ReportName = $Report->{name};

    $main::veryverbose && i::warnit("CheckPageIncl report: $ReportName\- FilterVar: $Report->{filterVar}\- currfilter: $Report->{currFilter}->{$catid}\-"
                                  . " CUTVAR: $U::UserVars->{CUTVAR}\- UserRef: $U::UserVars->{UserRef}");
    $descr = $Report->{descr} if (exists($Report->{descr}) && $Report->{descr} ne '');
#    $U::UserVars->{UserRef} = ( $descr && $descr ne '' ? $descr : $ReportName )
#                unless ( exists($U::UserVars->{UserRef}) && $U::UserVars->{UserRef} ne '');
    my $reportBundles = $U::UserVars->{'$$bundles$$'};
    $reportBundles = $Report->{bundles} unless ( $reportBundles && $reportBundles !~ /^\s*$/);
    $reportBundles = '' unless $reportBundles;
    my $reportHoldDays = $U::UserVars->{'$$holddays$$'};

    if ( !&{$Report->{includeIf}}() ) {
      if ( $Report->{started}->{$catid} ) {
        my ($filterVar, $currFilter) = @{$Report->{filterVar}, $Report->{currFilter}->{$catid}};
        $filterVar = '$$$$' unless $filterVar;
        $FileXref->{$catid}->EndItem($ReportName, $filterVar, $currFilter, $reportBundles)
                                                if ($filterVar and $currFilter ne "");
        $Report->{currFilter}->{$catid} = '' if $filterVar;
        $FileXref->{$catid}->EndItem($ReportName, '$$$$', '$$$$|'.( $descr && $descr ne '' ? $descr : $ReportName ), $reportBundles);
        $Report->{started}->{$catid} = 0;
      }
      next;
    }
    elsif ( !$Report->{started}->{$catid} ) {
      $FileXref->{$catid}->StartItem($page, $ReportName, '$$$$', '$$$$|'.( $descr && $descr ne '' ? $descr : $ReportName ), $reportHoldDays, $reportBundles);
      $Report->{started}->{$catid} = 1;
    }

    $PageIncluded = 1;
#    $self->CHECK_BREAKPOINT($page, $Report) if $Report->{clVars};
    if ($Report->{clVars}) {
        $self->CHECK_BREAKPOINT($page, $Report);
    }
    else {
        $self->CHECK_BREAKPOINT_ONCOUNT($page, $Report);
    }
    my $filterVar = $Report->{filterVar};
#    next if !$filterVar;
    my $currFilter = $Report->{currFilter}->{$catid};
    $main::veryverbose && i::warnit("Check P retrieving CF $currFilter");
    my $pageFilter;
    if ( $filterVar) {
         $main::veryverbose && i::warnit("Value of U::UserVars->{filterVar}".Dumper($U::UserVars->{$filterVar}));
        ($pageFilter, my $rcfv) = @{ $VerifyFilterValues->{$filterVar}->{ $U::UserVars->{$filterVar} } };
        $pageFilter = '$$$$' if(!defined($pageFilter) || !defined($U::UserVars->{$filterVar})); # added $$$$ as default when $FilterVar is not in really used ;
        $pageFilter .= "|".$U::UserVars->{UserRef};

        if ( $rcfv == FV_INVALID_FORMAT ) {
          if (!$self->{'CONTINUE_ON_FILTERVAR_ERROR'}) {
            $FileXref->{$catid}->EndItem($ReportName, $filterVar, $currFilter), $Report->{currFilter}->{$catid} = ''
              if
            $currFilter ne '';
            $FilterVarsAcceptable = 0;
          }
          next;
        }

        $FilterVarsAcceptable = 0 if $rcfv == FV_NOT_INTABLE;
    }
    else {
        # DONE: verify usability of UserRef as filterVar
        $filterVar = '$$$$';
        $pageFilter = '$$$$|'.$U::UserVars->{UserRef};
    }

    if ( $pageFilter ne "" and $pageFilter ne $currFilter ) {
      if ( $currFilter ne "" ) {
        $FileXref->{$catid}->EndItem($ReportName, $filterVar, $currFilter);
        if ( $Report->{cluster} ) {
          my $atPage = $OUTPUT->atPage();
          my $totPages = $atPage - ($Report->{'$pageFmValue'} || 1);
          &$logrRtn("NEW FILE FOR CLUSTER FilterValue $currFilter.");
          push @{$Report->{'$FileIdList'}}, [$self->NewFile(), $currFilter, $currFilter, $totPages];
          @{$Report}{qw($pageFmValue $firstFmValue $lastToValue)} = ($atPage, $pageFilter, $pageFilter);
        }
      }
      $main::veryverbose && i::warnit("Check P --Setting startItem RN : $ReportName, FV: $filterVar , PF: $pageFilter \n ");
      $FileXref->{$catid}->StartItem($page, $ReportName, $filterVar, $pageFilter, $reportHoldDays, $U::UserVars->{'$$bundles$$'});
      $Report->{currFilter}->{$catid} = $pageFilter;
      if($contid eq 'N'){
            #$main::veryverbose && i::warnit("SETTING PAGE EXEC SUB NOPAGEDEF");
            #&{$self->{NoPageDef}->{PageExecSub}}($self,$page);
            #$Report->{currFilter}->{$catid} = '$$$$|'.$U::UserVars->{UserRef};
            #redo;
      }
      @{$Report}{qw($pageFmValue $firstFmValue $lastToValue)} = (1, $pageFilter, $pageFilter)
       if
      $self->{'$clRep'} eq $Report and !exists($Report->{'$pageFmValue'});

      $self->NewOutlineEntry($page, undef, "TO.$filterVar") if !$Report->{cluster};
    }
  }

  UPDATE_COUNTERS:
  if ( $PageIncluded && $FilterVarsAcceptable ) {
    $self->incrValues('ParsedLines',$page->totLines(), 'ParsedPages',1);
  }
  else {
    $self->DiscardPage($page, "$PageIncluded/$FilterVarsAcceptable");

    $self->incrValues('DiscardedLines',$page->totLines(), 'DiscardedPages',1);
  }

  return $PageIncluded;
}

sub DiscardPage {
  my ($self, $page, $header) = @_; my $atPage = $page->atPage();

  my $discardList = $self->{'discardList'};

  if ( ($discardList->[-2] + $discardList->[-1]) == $atPage ) {
    $discardList->[-1] += 1;
  }
  else {
    push @$discardList, $atPage, 1;
  }
}


#######################################################################
#
#######################################################################

sub CHECK_BREAKPOINT {
  my ($self, $page, $r) = @_; my ($clVars, $maxsize) = @{$r}{qw(clVars maxsize)};

  my ($fmValue, $toValue) = @{$U::UserVars}{@$clVars};
  $main::veryverbose && i::warnit(__PACKAGE__." CHECK_BREAKPOINT fmval: $fmValue toval: $toValue caller: "
             .join('::', (caller())[0,2]));

  my $lastToValue = $r->{'$lastToValue'};

  if ( !defined($r->{'$firstFmValue'}) and $fmValue ne "" ) {
    $r->{'$firstFmValue'} = $fmValue;
  }

  if ($fmValue eq "" or $fmValue eq $lastToValue or $toValue eq "") {
    $r->{'$lastToValue'} = $toValue if $toValue ne ""; return;
  }

  if ( defined(my $FileId = $self->BREAKABLE_PAGE($r->{maxsize})) ) {
    my $atPage = $self->{'OUTPUT'}->atPage();
    my $totPages = $atPage - ($r->{'$pageFmValue'} || 1);
    push
      @{$r->{'$FileIdList'}},
      [$FileId, @{$r}{qw($firstFmValue $lastToValue)}, $totPages]
    ;
    @{$r}{qw($pageFmValue $firstFmValue $lastToValue)} = ($atPage, $fmValue, $toValue);
  }

  $r->{'$lastToValue'} = $toValue; @{$U::UserVars}{@$clVars} = ('', '');
}

sub CHECK_BREAKPOINT_ONCOUNT {
#TODO: xform maxsize in cfg parm
  my ($self, $page, $r) = @_; my $maxsize = ($XReport::cfg->{maxpdfpages} || 500000);
  my $atPage = $self->{'OUTPUT'}->atPage();
  $main::veryverbose && i::warnit(__PACKAGE__." CHECK_BREAKPOINT_ONCOUNT - maxsize: $maxsize atpage: $atPage caller: "
                 . join('::', (caller())[0,2]) );

  if ( defined(my $FileId = $self->BREAKABLE_PAGE($maxsize)) ) {
    my $totPages = $atPage - ($r->{'$pageFmValue'} || 1);
    push @{$r->{'$FileIdList'}}, [$FileId, '', '', $totPages];
    $r->{'$pageFmValue'} = $atPage;
    $self->{OUTPUT}->BeginPage($page);

  }

}

sub BREAKABLE_PAGE {
  my ($self, $maxsize) = (shift, shift); my $OUTPUT = $self->{'OUTPUT'};

  if ($OUTPUT->BREAKABLE_PAGE($maxsize)) {
    &$logrRtn("PAGE is BREAKABLE - Allocating a new File - maxpages: $maxsize caller: "
             .join('::', (caller())[0,2]));
    return $self->NewFile();
  }
  else {
    return undef;
  }
}

#######################################################################
#
#######################################################################

sub reExpand {
  my $re = shift;

  while ( $re =~ /(FMT\.(\w+))(?:(?:\(([^\)]*)\))*)/ ) {
    my $repl = $tabre{$2};
    if ( $3 ne "" ) {
      my $to = $3;
      $repl =~ s/\$1/$to/;
    }
    else {
    }
    $re =~ s/(FMT\.(\w+))(?:(\([^\)]*\))*)/$repl/;
  }

  return $re;
}

sub MakeList {
  my $tx = $_[0]; $tx =~ s/^\s+//; return split(/[ ,]+/, $tx);
}

sub MakeTextList {
  return join(",", map { "'$_'" } MakeList(@_) );
}

sub MakeVarList {
  return join(",", map { "'$_'" } grep(/^[^.]+$/, ref($_[0]) ? @{$_[0]} : MakeList(@_)) );
}

sub MakeMemList {
  return join(",", map { "\$$_" } ( 1 .. scalar(grep(/^[^.]+$/, MakeList(@_)))) );
}

sub MakeFormatExpr {
  my ($expr, $var, $frm, $len, $prc);
  for ( MakeList(@_) ) {
    ($frm, $len, $prc) = $_ =~ /^([a-z]+)(\d*)\.(\d*)$/i;
    if ( $frm ne "" ){
      $expr .= 'XReport::INFORMAT::'."$frm(\@{\$U::UserVars}{'$var'}, $len, $prc);";
    }
    else {
      $var = $_;
    }
  }
  return $expr;
}

sub MakeTextLineList {
  return grep( !/^\s*$/, split(/ *\n */, $_[0]));
}

sub NewLineTest {
  my ($self, $re, $rect) = (shift, shift, "");
  return '1' if (!defined($re) || $re eq '' || $re eq '1');

  my $SharedRects = $self->getValues('%SharedRects');

  if ( $re =~ /^((?:\w+)|(?:\([\d,]+\)))\.(.*)$/ ) {
    $rect = $1;
    $re = $2;
  }
  else {
    $rect = 'LINE';
  }
  $rect=uc($rect); $re = reExpand($2);

  my $rectValue = '$lineValue';

  if ( $rect ne "LINE" ) {
    if ( $rect =~ /\w+/ ) {
      ($rect = $SharedRects->{$rect}->getAttribute("coord")) =~ s/([(,])0+/$1/g;
    }
    $rect =~ s/\((.*)\)/$1/;
    $rectValue = '$page->GetLineRect($line,'.$rect.')';
  }

  return "$rectValue =~ /". $re .'/mso';
}

sub MakeAttrsExpr {
  my ($attrsExp, $list) = @_; my @attrsExp = $attrsExp =~ /([\w\-]+\s*(?:\[[^\]]+\])?)/gs;
  #i::warnit("TEST_TLE MakeAttrsExpr input".Dumper($attrsExp, @attrsExp,$list));
  my @attrs = map {$_ =~ /^([\w\-]+)/; $1} @attrsExp;

  my %attrsPos = (); my $attrPos = 0;

  for (@attrs) {
    $attrsPos{$_} = $attrPos; $attrPos += 1;
  }

  my @postExp = grep(/^[\w\-]+\s*\[/, @attrsExp); my $postExp;
  #i::warnit("TEST_TLE MakeAttrsExpr post ".Dumper(@postExp));
  for my $stmt (@postExp) {
    assert(my ($attr, $exp) = $stmt =~ /([\w\-]+)\s*\[([^\]]+)\]/);
    assert(my ($func, $args) = $exp =~ /^\s*(\w+)\s+(.*)$/ );

    assert(lc($func) =~ /^unpack$/); ## more funcs.

    if (lc($func) eq 'unpack') {
      $postExp .= " \$$list\[$attrsPos{$attr}\] = [unpack('$args',\$$list\[$attrsPos{$attr}\])];";
    }
    else {
    }
  }
  #i::warnit("TEST_TLE MakeAttrsExpr post built ".Dumper($postExp));
  return (MakeVarList(\@attrs), $postExp);
}

sub LEFT_TO {
  if ( length($_[0]) < $_[1] ) {
    $_[0] .= ' ' x ($_[1]-length($_[0]));
  }
  return $_[0];
}

sub solvePageRectRtn {
    my $self = shift;
   my $linePos = lc($self->getValues('linePos'));
   my $getpagerect = ($linePos eq 'raw' ? 'GetRawPageRect'
                      : $linePos eq 'rawcompacted' ? 'GetRawPageRect_compacted'
                      : 'GetPageRect');
   $main::veryverbose && i::warnit("solvePageRectRtn - caller: ", join('::', (caller())[0,2])
                    , " line positioning handled by \"$getpagerect\"\n");
   return $getpagerect;
}

sub parseExpression {
   my $self = shift;
  $main::debug && i::warnit(ref($self)."::parseExpression -- args ", join("::", @_));
  my ($varname, $exp, $texp, $rect) = (shift, shift, shift, shift);

   my $minLen = 0;
   my $SharedRects = $self->getValues('%SharedRects');

   my $op = '';
   my $getpagerect = $self->solvePageRectRtn();

   my $postExp = ''; my $origExp = $exp;
   if ( $exp =~ /^(\w+|\([\d,]+\))([\!\.])(.*)$/ ) {
     $rect = $1;
     $op = ($2 eq '!' ? '!' : '=');
     $exp = $3;
   }
   $rect=uc($rect);
   #i::warnit("TEST_TLE exp id ".Dumper($exp));
   if ( $rect =~ /\w+/ and exists($SharedRects->{$rect}) ) {
     ($rect = $SharedRects->{$rect}->getAttribute("coord")) =~ s/([(,])0+/$1/g;
   }
   $rect =~ s/\((.*)\)/$1/;

   my $rectValue = "\$page->$getpagerect(".$rect.')';

   $main::debug && i::warnit("now building code to handle $texp with $exp");
   if ( $texp eq "re" ) {
     $exp = "$rectValue $op~ /". reExpand($exp) .'/mso';
   }
   elsif ( $texp eq "fldsep" ) {
      $exp = "split('$exp', \$rectValue)";
   }
   elsif ( $texp eq "like" ) {
     $minLen = length($exp)+1;
     my @l = split(/(?<=\S)\s/, $exp); $exp = '';
     for (@l) {
       if ($_ !~ /^_+$/ ) {  $exp .= " x1 a".length($_);  }
       else { $exp .= " x".(1+length($_))." "; }
     }
     $exp = "unpack('$exp', $rectValue)";
   }
   elsif ( $texp eq "unpack" ) {
     my @at = $exp =~ /\@(\d+)/g;
     for (@at) {
       $minLen = $_ if $_ > $minLen;
     }
     $minLen += 1 if $minLen;
     $exp = "unpack('$exp', $rectValue)";
   }
   elsif ( $texp eq "pageAttrs" ) {
     (my $attrsList, $postExp) = MakeAttrsExpr($exp, 'tvars');
     $exp = "\$page->GetAttributes('$rect', $attrsList)";
   }
   elsif ( $texp eq "constant" ) {
     my $quote = "'";
     $quote = '"' if $origExp =~ /\'/;
     $exp = "(".$quote.$origExp.$quote.")";
   }
   elsif ( $texp eq "elabvalue" ) {
     my ($vn, $vp, $vl ) = ( $exp =~ /^([^\(]+)(?:\((\d+),(\d+)\))?/ );
     $main::veryverbose && i::warnit("parse Expr-- $vn -- $vp -- $vl");
     if ( defined($vp) ) {
        #$vp += 1;
        my $skip = $vp ? "x$vp" : "";
        $exp = "unpack('${skip}A$vl', \$self->{jr}->getValues('$vn') . ' ' x ($vl+$vp))";
     }
     else {
         #$exp = "\$self->{jr}->getValue('$exp')";
         $exp = "\$U::UserVars->{$exp}";
     }
   }

   $exp =~ s/\$rectValue/$minLen ? LEFT_TO($rectValue, $minLen) : $rectValue/e;
   $postExp .= '@tvars = map {!ref($_) ? $_ : @$_} @tvars;' if $postExp;

   return ($exp, $postExp);
}


sub NewLineGetVars {
  my ($self, $vars, $exp, $texp, $rect, $postExp) = (shift, shift, shift, shift, "LINE", '');
  $main::debug && i::warnit("building code to retrieve variables $vars - exp: $exp");
  my $varList = MakeVarList($vars);
  ######TODO !!!!! LINE management must be analized and added because parseExpression method doesn't include it
  #
#if ( $exp =~ /^(\w+|\([\d,]+\))\.(.*)$/ ) {
#    $rect = $1;
#    $exp = $2;
#  }
#  else {
#    $rect = 'LINE';
#  }
#  $rect=uc($rect);
  #
  #my $varList = MakeVarList($vars);
  #
  #if ( $rect =~ /\w+/ and exists($SharedRects->{$rect}) ) {
  #  ($rect = $SharedRects->{$rect}->getAttribute("coord")) =~ s/([(,])0+/$1/g;
  #}
  #$rect =~ s/\((.*)\)/$1/;
  #
  #my $rectValue = '$lineValue';
  #
  #if ( $rect ne "LINE" ) {
  #  if ( $rect =~ /\w+/ ) {
  #    ($rect = $SharedRects->{$rect}->getAttribute("coord")) =~ s/([(,])0+/$1/g;
  #  }
  #  $rect =~ s/\((.*)\)/$1/;
  #  $rectValue = '$page->GetLineRect($line,'.$rect.')';
  #}
  if ( $rect eq 'LINE' && $texp eq 'fldsep' ) {
  	$exp = "split('$exp', \$lineValue)";
  }
  else {
  ($exp, $postExp) = $self->parseExpression( $vars, $exp, $texp, $rect );
  }
  return
   "if ( \@tvars = $exp ) { " .
     "\@{\$U::UserVars}{$varList} = \@tvars;" .  MakeFormatExpr( $vars ) .  "\$getVars = 1;" .
   "}"
  ;
}

sub NewPageTest {
  my ($self, $re, $rect) = (shift, shift, "");

  return '1' if (!defined($re) || $re eq '' || $re eq '1');

  my $SharedRects = $self->getValues('%SharedRects');

  my $getpagerect = $self->solvePageRectRtn();

=pod
 added new form of testing
 syntax example: test="(RECT1.NE.'PIPPO').AND.(RECT1.NE.'PLUTO').AND.(RECT2.EQ.'PAPER')"
=cut

  $main::veryverbose && i::warnit("Now processing \"$re\" line positioning handled by \"$getpagerect\"");
  my @conds = split /\.(AND|OR)\./, $re;
  my $numPars = 0;
    my $testExpr = '';
  while ( scalar(@conds) ) {
    my $condition = shift @conds;
    if ( $condition =~ /^\((\w+)\.((?i:EQ|NE|GT|LT|GE|LE))\.'([^']+)'\)$/ ) {
        my @pa = ( $1, $2, $3 );
        $main::veryverbose && i::warnit("RE vars: ", join("::", @pa));
        my $rectid = $pa[0];
      die "UserError: INVALID TEST DETECTED: $re ($rectid is not a valid or undefined)"
          unless $rectid =~ /\w+/ && exists($SharedRects->{$rectid});
      (my $coord = $SharedRects->{$rectid}->getAttribute("coord")) =~ s/([(,])0+/$1/g;
      $testExpr .= "(\$page->$getpagerect($coord) ".lc($pa[1])." \"$pa[2]\")";
    }
    else {
       my ($rect, $re, $op);
       #if ( $condition =~ /^\(?(\([\d,]+\))([\.\!])(.+)\)?$/ ) {
       if ( $condition =~ /^\((\([\d,]+\))([\.\!])(.+)\)$/ ) {
          $rect = $1;
          $op = $2;
          $re = reExpand($3);
  }
       elsif ( $condition =~ /^(\w+?)([\.\!])(.*)$/ ) {
    $rect = $1;
          $op = $2;
          $re = reExpand($3);
  }
  else {
          die "RE \"$condition\" not handled\n";
  }
       if ( $rect =~ /^[^\(]\w+/ ) {
    ($rect = $SharedRects->{$rect}->getAttribute("coord")) =~ s/([(,])0+/$1/g;
  }
  $rect =~ s/\((.*)\)/$1/;
       $op = '=' unless $op eq '!';
       $testExpr .= "(\$page->$getpagerect(".$rect.') '.$op.'~ /'. $re .'/mso)';
    }
    die "UserError: INVALID BOOLEAN OPERATOR DETECTED: $conds[0] " if scalar(@conds) && $conds[0] !~ /^(?:AND|OR)$/;
    if (scalar(@conds)){
        $testExpr .= ' '.lc(shift @conds).' (' ;
        $numPars++;
    }
  }
  $testExpr.= ')' x $numPars if ($numPars);
  return $testExpr;

#  my $rest = $re;
#  if ( $re =~ /^\((\w+)\.((?i:EQ|NE|GT|LT|GE|LE))\.'([^']+)'\)(?:\.(\w+)\.(.*))?$/ ) {
#    my $testExpr = '';
#    while( $rest =~ /^\((\w+)\.(\w+)\.'([^']+)'\)(?:\.(\w+)\.(.*))?$/ ) {
#       my @pa = ( $1, $2, $3, $4, $5 );
#       warn "RE vars: ", join("::", @pa), "\n";
#        my $rectid = $pa[0];
#      die "UserError: INVALID TEST DETECTED: $re ($rectid is not a valid or undefined)"
#          unless $rectid =~ /\w+/ && exists($SharedRects->{$rectid});
#      (my $coord = $SharedRects->{$rectid}->getAttribute("coord")) =~ s/([(,])0+/$1/g;
#      $testExpr .= "(\$page->$getpagerect($coord) ".lc($pa[1])." \"$pa[2]\")";
#      $testExpr .= " ".lc($pa[3])." " if $pa[3];
#      $rest = $pa[4];
#    }
#    die "UserError: INVALID TEST DETECTED: $re " if $rest ne '';
#    return $testExpr;
#  }
#  elsif ( $re =~ /^\((\([\d,]+\))\.(.+)\)$/ ) {
#    $rect = $1;
#    $re = reExpand($2);
#  }
#  elsif ( $re =~ /^(\w+?)\.(.*)$/ ) {
#    $rect = $1;
#    $re = reExpand($2);
#  }
#  else {
#   warn "RE \"$re\" not handled\n";
#  }
#
#  if ( $rect =~ /^[^\(]\w+/ ) {
#    ($rect = $SharedRects->{$rect}->getAttribute("coord")) =~ s/([(,])0+/$1/g;
#  }
#  $rect =~ s/\((.*)\)/$1/;
#
#  return "\$page->$getpagerect(".$rect.') =~ /'. $re .'/mso';
}

sub NewPageGetVars {
  #warn "NewPageGetVars " , join("::", @_), "\n";
  my ($self, $vars, $exp, $texp, $rect, $postExp) = (shift, shift, shift, shift, '', '');
  my $varList = MakeVarList($vars);
  if ( $texp eq "exeList" ) {
    $exp = "U::$exp(\$page, $varList)";
  }
  else {
    ($exp, $postExp) = $self->parseExpression( $vars, $exp, $texp, $rect );
  }

  return
   "if ( \@tvars = $exp ) { \n"
     . $postExp . "\n"
     # . "warn \"VARS: $varList VALS(\", scalar(\@tvars), \"):\", join('::',\@tvars), \"\\n\";\n"
     . "\@{\$U::UserVars}{$varList} = \@tvars;\n" .  MakeFormatExpr( $vars ) .  "\n"
     . "\$getVars = 1;\n"
   . "}\n"
  ;
}

sub MakeExtractDefaults {
  my ($self, $vars, $defaults) = (shift, shift, shift);
  my @envars = ();
  if ($defaults =~ /\$/) {
      @envars = ($defaults =~ /\$(\w+)/g);
  }

  return (
   (scalar(@envars) ? "my (\$".join(", \$", @envars).") = \$page->getJobReportValues(qw(".join(' ', @envars)."));" : '') .
#   "my \$JobReportDescr = 'pippo';".
   "if (!\$getVars) { "
    . "  \@{\$U::UserVars}{". MakeVarList($vars)."}  = split(\"\\t\", \"$defaults\"\);"
    ."   \$getVars = 1; }"
  );
}

sub NewIncludeTest {
  my ($self, $test, $testExpr) = (shift, shift, ""); $test =~ s/\s+$//; my $rest = $test;

  if ( $test =~ /^\(/ ) {
    while( $rest =~ /^\((\w+)\.(\w+)\.('[^']+')\)(?:\.(\w+)\.(.*))?$/ ) {
      $testExpr .= "(\$U::UserVars->{'$1'} ".lc($2)." $3)";
      $testExpr .= " ".lc($4)." " if $4;
      $rest = $5;
    }
    die "UserError: INVALID TEST DETECTED: $test " if $rest ne '';
  }
  elsif ( $test =~ /^(\w+)\.(.*)$/ ) {
    $testExpr = '$_[0] =~ /'. $2 . '/mso';
  }
  elsif ( $test eq '1' ) {
    $testExpr = '1';
  }
  else {
  }

  return $testExpr;
}


#######################################################################
#
#######################################################################

sub getChildsByName {
  my ($N, $t) = (shift, shift);

  return grep {$_->getNodeName() eq $t} @{$N->getChildNodes()};
}

sub getTextOfChilds {
  my $N = shift;

  return join("\n", map {$_->getNodeValue()} @{$N->getChildNodes()});
}

sub PrintChildNodes {
  my $N = $_[0];

  &$logrRtn(".. NodeName=". $N->getNodeName. " NameAttr=". $N->getAttribute("name"). " ..") if $DEBUG;
  for (@{$N->getChildNodes()}) {
    &$logrRtn("<<". $_->getNodeName(), $_->toString(). ">>") if $DEBUG;
  }
}

sub MakeLineSubs {
  my ($self, $line) = (shift, shift);

  my $SharedOps = $self->getValues('%SharedOps');

  my $NameAttr = $line->getAttribute("name");
  my $test = $line->getAttribute("test");
  my $testExpr = $self->NewLineTest($test);

  $main::debug && i::warnit("..LINE NameAttr=$NameAttr test=$test");

  my $LineTestSub = 'sub { my $lineValue = shift; return ('. $testExpr .') }';

  my $LineExecSub =
   "sub { \n" .
   "  my (\$self, \$page, \$line, \$lineValue) = (shift, shift, shift, shift); my \$getVars;\n"
    . " \$main\:\:veryverbose && i\:\:warnit(\"processing now line Def $NameAttr called by \".join('::', (caller())[0,2]));\n"
  ;

  my $abort = $line->getAttribute("abort");
  if ( $abort ) {
    $abort =~ s/"/\\"/g;
    $LineExecSub .= "\$page->log(); die \"$abort\";";
  }

  my $Indexes = $line->getAttribute("reset_indexes");
  if ( $Indexes ) {
    my $ListLength = scalar(MakeList($Indexes));
    if ( $ListLength > 1 ) {
      $LineExecSub .= "CloseIndexEntries(\$self, \$page, \$line, LAST_ENTRY, ".MakeTextList($Indexes).");\n";
    }
    elsif ( $ListLength == 1 ) {
      $LineExecSub .= "CloseIndexEntry(\$self, \$page, \$line, LAST_ENTRY, ".MakeTextList($Indexes).");\n";
    }
  }

  my @eOps = @{$SharedOps->{MakeTextList($line->getAttribute("ops"))}};

  my @iOps = getChildsByName($line, 'op');

  my $opcnt = 0;
  for my $op (@eOps, @iOps) {
    next if !$op;
    $opcnt++;
    my $gvname = sprintf("GETVARS%02d", $opcnt);

    my $OpExpr = "";

    my $NameAttr = $op->getAttribute("name");
    my $test = $op->getAttribute("test");
    my $testExpr = $self->NewLineTest($test);
    my $defaults = $op->getAttribute("defaults");
    my $abortifmiss = $op->getAttribute("abortifmiss");

    $main::debug && i::warnit("..OP NameAttr=$NameAttr test=$testExpr");

    $OpExpr .= "if ( ". $testExpr ." ) { my \@tvars;\n";

    my $getVars = $op->getAttribute("getVars"); my ($exp, $texp);
    if ($getVars ne "") {
      $OpExpr .= "${gvname}: { \$getVars = 0;\n"
        . " \$main\:\:veryverbose && i\:\:warnit(\"processing now Op Def $NameAttr called by \".join('::', (caller())[0,2]));\n"
      	;
      for my $j ( '', 2..10 ) {
        for (qw(re like unpack fldsep)) {
          $texp=$_; last if $exp = $op->getAttribute("$texp$j");
        }
        last if $exp eq '';
        my $getVarsExpr = $self->NewLineGetVars($getVars, $exp, $texp);

        $OpExpr .= $getVarsExpr."; last ${gvname} if \$getVars;\n";
      }
      $OpExpr .= $self->MakeExtractDefaults($getVars, $defaults) if $defaults;
      ### test if todo error
      $OpExpr .=  "if (!\$getVars) {\$page->log(); \$line->log(); die(\"$abortifmiss\")}\n" if $abortifmiss;
      $OpExpr .= "}\n";
    }

    my $Execs = $op->getAttribute("exec");
    if ( $Execs ) {
      $OpExpr .= "NewExecEntries(\$self, \$page, \$line, ".MakeTextList($Execs).");\n";
    }

    if ($self->getValues('ElabFull')) {
#      my $Indexes = $op->getAttribute("indexes");
#      if ( $Indexes ) {
      if (my $Indexes = $op->getAttribute("indexes")) {
        my $ListLength = scalar(MakeList($Indexes));
        if ( $ListLength > 1 ) {
          $OpExpr .= "NewIndexEntries(\$self, \$page, \$line, ".MakeTextList($Indexes).");\n";
        }
        elsif ( $ListLength == 1 ) {
          $OpExpr .= "NewIndexEntry(\$self, \$page, \$line, ".MakeTextList($Indexes).");\n";
        }
      }
#      my $Indexes = $op->getAttribute("reset_indexes");
#      if ( $Indexes ) {
      if (my $Indexes = $op->getAttribute("reset_indexes")) {
        my $ListLength = scalar(MakeList($Indexes));
        if ( $ListLength > 1 ) {
          $OpExpr .= "CloseIndexEntries(\$self, \$page, \$line, LAST_ENTRY, ".MakeTextList($Indexes).");\n";
        }
        elsif ( $ListLength == 1 ) {
          $OpExpr .= "CloseIndexEntry(\$self, \$page, \$line, LAST_ENTRY, ".MakeTextList($Indexes).");\n";
        }
      }
    }

    if ( $self->getValues('ElabFormat') == EF_PRINT ) {
      my $Outlines = $op->getAttribute("outlines");
      if ( $Outlines ) {
        my $ListLength = scalar(MakeList($Outlines));
        if ( $ListLength > 1 ) {
          $OpExpr .= "NewOutlineEntries(\$self, \$page, \$line, ".MakeTextList($Outlines).");\n";
        }
        elsif ( $ListLength == 1 ) {
          $OpExpr .= "NewOutlineEntry(\$self, \$page, \$line, ".MakeTextList($Outlines).");\n";
        }
        else {
        }
      }
    }

    $OpExpr .= "}";

    $LineExecSub .= $OpExpr."\n";
  }

  if ($self->getValues('ElabFull')) {
    my $Indexes = $line->getAttribute("indexes");
    if ( $Indexes ) {
      my $ListLength = scalar(MakeList($Indexes));
      if ( $ListLength > 1 ) {
        $LineExecSub .= "NewIndexEntries(\$self, \$page, \$line, ".MakeTextList($Indexes).");\n";
      }
      elsif ( $ListLength == 1 ) {
        $LineExecSub .= "NewIndexEntry(\$self, \$page, \$line, ".MakeTextList($Indexes).");\n";
      }
    }
  }

  if ( $self->getValues('ElabFormat') == EF_PRINT ) {
    my $Outlines = $line->getAttribute("outlines");
    if ( $Outlines ) {
      my $ListLength = scalar(MakeList($Outlines));
      if ( $ListLength > 1 ) {
        $LineExecSub .= "NewOutlineEntries(\$self, \$page, \$line, ".MakeTextList($Outlines).");\n";
      }
      elsif ( $ListLength == 1 ) {
        $LineExecSub .= "NewOutlineEntry(\$self, \$page, \$line, ".MakeTextList($Outlines).");\n";
      }
      else {
      }
    }
  }

  $LineExecSub .=
   "  return \$getVars;\n" .
   "}"
  ;

  $main::debug && i::warnit("LineTestSub Completed - code: ".$LineTestSub);
  $LineTestSub = eval $LineTestSub; croak "UserError: $LineTestSub\n$@" if $@;

  $main::debug && i::warnit("LineExecSub Completed - code: ".$LineExecSub);
  $LineExecSub = eval $LineExecSub; croak "UserError: $LineExecSub\n$@" if $@;

  return ($LineTestSub, $LineExecSub);
}

sub setBundles {
    my $self = shift;
#    warn "setBundles: ", Dumper(\{ @_ }), "\n";
    my ($bundleshash, $bundles, $defaultcopies) = @_;
    foreach my $bn ( split / +/, $bundles ) {
        my ($name, $copies) = split /\//, $bn, 2;
        if ( defined($copies) ) { $bundleshash->{$name} = $copies; }
        elsif ( $defaultcopies && !exists($bundleshash->{$name}) ) { $bundleshash->{$name} = $defaultcopies;}
    }
#    warn "setBundles: out: ", Dumper($bundleshash), "\n";
}

sub MakePageSubs {
=Page Definitions

attributes:
name          : page identifier
test          : filter condition
abort         : die message
mark          : markname ( do CloseIndexEntries for @markname,
                            sets @markname to (atpage, 1, pageOffsest) )
reset_indexes :
fields        : {name (coordinates) [formatname {.}]}+
setvars       : {name \{var value\} }
op
indexes
outlines
endmark

bundles       : {name[/copies]}+

=cut
  my ($self, $page, $Marks, $args) = (shift, shift, shift, shift);
  my $PageDef = shift;
  # $main::veryverbose && i::warnit("MakePageSubs: PageDef b4 pagesub: ".Dumper($PageDef));

  my $SharedOps = $self->getValues('%SharedOps');

  my $NameAttr = $page->getAttribute("name");
  my $test = $page->getAttribute("test");
  my $testExpr = $self->NewPageTest($test);

  my $PageTestSub = 'sub { my $page = shift; '."\n"
   . " \$main\:\:veryverbose && i\:\:logit(\"TEST now page $NameAttr caller: \".join('::', (caller())[0,2])"
   .".' RECT: '.\$page->GetRawPageRect((2,7,2,14)));\n"
    . 'return ('. $testExpr .') }';

  my $defaultcopies = $args->{copies};
  my $PageExecSub =
   "sub { \n"
   . "  my (\$self, \$page) = (shift, shift); my \$getVars = 0;\n"
   . " \$main\:\:veryverbose && i\:\:logit(\"processing now page Def $NameAttr called by \".join('::', (caller())[0,2]));\n"
#   . "  my \$defaultcopies = $defaultcopies;\n"
    . " my \$bundleslist = {};\n"
  ;

  my $pagebundles = $page->getAttribute("bundles");
  $pagebundles = '' unless ($pagebundles && $pagebundles !~ /^\s*$/);
  $pagebundles = $args->{bundles} if ( exists($args->{bundles}) && $pagebundles eq '');
  $pagebundles = '' unless ($pagebundles && $pagebundles !~ /^\s*$/);
#  $PageExecSub .=
#   " \$U::UserVars->{'\$\$bundles\$\$'} = '$pagebundles';\n";
  $PageExecSub .= "\$self->setBundles(\$bundleslist, \"$pagebundles\", $defaultcopies);"
#   . " \@{\$bundleslist}{qw($pagebundles)} = map { '1' } qw($pagebundles);"
    . "\n" if $pagebundles ne ''
   ;

  my $pageHoldDays = $PageDef->{'holddays'};
  $pageHoldDays = 33 unless (defined($pageHoldDays) && $pageHoldDays !~ /^\s*$/);
  $PageExecSub .= " \$U::UserVars->{'\$\$holddays\$\$'} = ('HDays=$pageHoldDays');";

  my $abort = $page->getAttribute("abort");
  if ( $abort ) {
    $abort =~ s/"/\\"/g;
    $PageExecSub .= "\$page->log(); die \"$abort\";";
  }

  my $MarkName = $page->getAttribute("mark");
  if ( $MarkName ) {
    my $setMarkExpr = "
      \$self->CloseIndexEntries(\$page, undef, LAST_ENTRY, \@{\$self->{MarkedIndexes}->{$MarkName}});
      \@{\$self->{Marks}->{'$MarkName'}} = (\$OUTPUT->atPage(), 1, \$page->atOffset());
    ";
    $PageExecSub .= $setMarkExpr;
  }

  my $Indexes = $page->getAttribute("reset_indexes");
  if ( $Indexes ) {
    my $ListLength = scalar(MakeList($Indexes)); my $BeginExpr;
    if ( $ListLength > 1 ) {
      $BeginExpr .= "CloseIndexEntries(\$self, \$page, undef, LAST_ENTRY, ".MakeTextList($Indexes).");\n";
    }
    elsif ( $ListLength == 1 ) {
      $BeginExpr .= "CloseIndexEntry(\$self, \$page, undef, LAST_ENTRY, ".MakeTextList($Indexes).");\n";
    }
    $PageExecSub .= $BeginExpr;
  }

  my $PgStringsAttr = $page->getAttribute("setvars");
   if ( $PgStringsAttr ) {
    my %PgStrings = $PgStringsAttr =~ /(\w+) +\{([^\}]*?)\}/sg;
    my @vars = keys %PgStrings;

    $PageExecSub .=
      "\@{\$U::UserVars}{qw(".join(" ", @vars).")} = (\"".join("\",\"", @{PgStrings}{@vars})."\");"
      ;
  }


  my $Fields = $page->getAttribute("fields");

  if ( $Fields ) {
    my @Fields = $Fields =~ /(\w+) +\(([\d ]+[^\)]*?)\)/sg;
    my ($hvars, $formats) = ({}, '');
    while(@Fields) {
        my $varn = shift @Fields;
        my $vard = shift @Fields;
        $hvars->{$varn} = {
            coord => [ map { s/^0+//; $_ } ($vard =~ /(\d+)/sg) ],
            format =>  ($vard =~ /(\w+\.)/)[0],
        };
        my $ncoord = scalar(@{$hvars->{$varn}->{coord}});
        if ( $ncoord == 4 ) {
            $hvars->{$varn}->{coord}->[3] -= ($hvars->{$varn}->{coord}->[1] - 1);
            $hvars->{$varn}->{coord} = [@{$hvars->{$varn}->{coord}}[2,1,3]];
        }
        elsif ( $ncoord != 3 ) {
            $main::veryverbose && i::warnit("Field coord \"$hvars->{$varn}->{coord}\" for \"$varn\" are invalid - Field ignored\n");
            delete $hvars->{$varn};
        }
        $formats .= " $varn $hvars->{$varn}->{format}"
                        if exists($hvars->{$varn}) && $hvars->{$varn}->{format};

    }
    my @vars = keys %{$hvars};
    $PageExecSub .=
      "\@{\$U::UserVars}{qw(".join(" ", @vars).")} = \$page->GetPageFields(".join(",", map { @{$hvars->{$_}->{coord}} } @vars).");"
      . ($formats ne '' ? MakeFormatExpr($formats) : '')
#    . "  my \$bundleslist = []; "
    ;

#    my (@vars , @coords, $vars) = ();
#    while(@Fields) {
#      push @vars, shift @Fields; my $Fields = shift @Fields;
#      my @fldcoord = map { s/^0+//; $_ } ($Fields =~ /(\d+)/sg);
#      if ( $Fields =~ /(\w+\.)/ ) {
#        $vars .= " $vars[-1] $1";
#      }
#      if (scalar(@fldcoord) eq 3 ) {
#       push @coords, @fldcoord;
#      }
#      elsif ( scalar(@fldcoord) eq 4 ) {
#       push @coords, ($fldcoord[2], $fldcoord[1], $fldcoord[3] - $fldcoord[1]);
#      }
#      else {
#       my $fldn = pop @vars;
#       warn "Field coord \"$Fields\" for \"$fldn\" are invalid - Field ignored\n";
#      }
##      push @coords, map { s/^0+//; $_ } ($Fields =~ /(\d+)/sg);
#      if ( $Fields =~ /(\w+\.)/ ) {
#        $vars .= " $vars[-1] $1";
#      }
#    }
#    $PageExecSub .=
#      "\@{\$U::UserVars}{qw(".join(" ", @vars).")} = \$page->GetPageFields(".join(",", @coords).");" .
#       ($vars ne '' ? MakeFormatExpr($vars) : '')
#    ;
  }
  my @iVars = getChildsByName($page, 'vars');
  my $userRefSet = 0; ## mark if a userRef <vars/>  tag  exists in this page def
  for my $vardef ( @iVars ) {
    my $varname = $vardef->getAttribute("name");
    $userRefSet = 1 if $varname eq 'UserRef';
    my ($exp, $texp);
    for my $j ( '', 2..10 ) {
      for (qw(constant re like unpack pageAttrs exeList elabvalue)) {
        $texp = $_; last if $exp = $vardef->getAttribute("$texp$j");
      }
      $main::veryverbose && i::warnit("cicling$varname 1..$texp");
      last if $exp eq '';
      $main::veryverbose && i::warnit("cicling 2..$exp");
      ($exp,my $postExp) = $self->parseExpression($varname, $exp, $texp);
      $main::veryverbose && i::warnit("cicling 3..$exp");
        $PageExecSub .= "\$U::UserVars->{$varname} = $exp;";

    }
    my @conclist = getChildsByName($vardef, 'concat');
    for my $concat ( @conclist ) {
      my ($exp, $texp);
      $main::veryverbose && i::warnit("cicling concat $varname");
      for my $j ( '', 2..10 ) {
    for (qw(constant re like unpack pageAttrs exeList elabvalue)) {
          $texp = $_; last if $exp = $concat->getAttribute("$texp$j");
        }
        $main::veryverbose && i::warnit("cicling concat 2  $exp");
        last if $exp eq '';
        ($exp, my $postExp) = $self->parseExpression($varname, $exp, $texp);

        $PageExecSub .= "\$U::UserVars->{$varname} .= $exp;";
      }
	  $main::veryverbose && i::warnit("PageExecSub  [$PageExecSub]");
    }
  }
  
  my @eOps = @{$SharedOps->{MakeTextList($page->getAttribute("ops"))}};
  my @iOps = getChildsByName($page, 'op');

  my $opcnt = 0;
  my $opbundles = [];
  for my $op (@eOps, @iOps) {
    next if !$op;
=pod
=OP definitions
attributes:
name
test
defaults
abortifmisss
getVars
re.('',2..10)
like.('',2..10)
unpack.('',2..10)
fldsep.('',2..10)
pageAttrs.('',2..10)
exeList.('',2..10)
exec
indexes
outlines

=cut
    $opcnt++;
    my $gvname = sprintf("GETVARS%02d", $opcnt);
    my $OpExpr = '';

    my $NameAttr = $op->getAttribute("name");
    my $test = $op->getAttribute("test");
    my $testExpr = $self->NewPageTest($test);
    my $defaults = $op->getAttribute("defaults");
    my $abortifmiss = $op->getAttribute("abortifmiss");

    &$logrRtn("..OP NameAttr=$NameAttr test=$testExpr") if $DEBUG;
    $OpExpr .= "if ( ". $testExpr ." ) { my \@tvars;\n";

    my $getVars = $op->getAttribute("getVars");
    #warn "..OP $gvname NameAttr=$NameAttr test=$testExpr getVars=$getVars", "\n";
    my ($exp, $texp);
    if ($getVars ne "") {
      $OpExpr .= "${gvname}: { \$getVars = 0;\n";
      for my $j ( '', 2..10 ) {
        for (qw(re like unpack pageAttrs exeList)) {
          $texp = $_; last if $exp = $op->getAttribute("$texp$j");
        }
        last if $exp eq '';
        my $getVarsExpr = $self->NewPageGetVars($getVars, $exp, $texp);

        $OpExpr .= $getVarsExpr
#                   .";warn \"getVars \$getVars tvars :: \".join('::', \@tvars).\"\\n\""
                   ."; last ${gvname} if \$getVars;\n";
      }
      $OpExpr .= $self->MakeExtractDefaults($getVars, $defaults) if $defaults;
      ### test if todo error
      $OpExpr .= "if (!\$getVars) {\$page->log(); die(\"$abortifmiss\")}\n" if $abortifmiss;
      $OpExpr .= "}\n";
    }

    $OpExpr .= "if (\$getVars) {\n";
    my $opbundles = $op->getAttribute("bundles");
#    $OpExpr .= "  \@{\$bundleslist}{qw($opbundles)} = map { 1 } qw($opbundles);\n"
    $OpExpr .= "\$self->setBundles(\$bundleslist, \"$opbundles\", $defaultcopies);"
                                              . "\n" if ($opbundles && $opbundles !~ /^\s*$/);
#   . " \@{\$bundleslist}{qw($pagebundles)} = map { '1' } qw($pagebundles);"
#    . "\n" if $opbundles ne ''
   ;

    my $Execs = $op->getAttribute("exec");
    if ( $Execs ) {
      $OpExpr .= "NewExecEntries(\$self, \$page, undef, ".MakeTextList($Execs).");\n";
    }

    if ($self->getValues('ElabFull')) {
      my $Indexes = $op->getAttribute("indexes");
      if ( $Indexes ) {
        my $ListLength = scalar(MakeList($Indexes));
        if ( $ListLength > 1 ) {
          $OpExpr .= "NewIndexEntries(\$self, \$page, undef, ".MakeTextList($Indexes).");\n";
        }
        elsif ( $ListLength == 1 ) {
          $OpExpr .= "NewIndexEntry(\$self, \$page, undef, ".MakeTextList($Indexes).");\n";
        }
      }
    }

    if ( $self->getValues('ElabFormat') == EF_PRINT ) {
      my $Outlines = $op->getAttribute("outlines");
      if ( $Outlines ) {
        my $ListLength = scalar(MakeList($Outlines));
        if ( $ListLength > 1 ) {
          $OpExpr .= "NewOutlineEntries(\$self, \$page, undef, ".MakeTextList($Outlines).");\n";
        }
        elsif ( $ListLength == 1 ) {
          $OpExpr .= "NewOutlineEntry(\$self, \$page, undef, ".MakeTextList($Outlines).");\n";
        }
        else {
        }
      }
    }

    $OpExpr .= "\n}\n}"; ## close( $getVars + textexpr )
    $PageExecSub .= $OpExpr . "\n";

  }

  (my $jrdescr = $self->getValues(qw(JobReportDescr))) =~ s/\//\\\//g;
  #
  $jrdescr =~ s/\$/\\\$/g;
  $jrdescr =~ s/\^/\\\^/g; 
  $PageExecSub .= " \$U::UserVars->{UserRef} = qq/$jrdescr/;\n" unless $userRefSet;
  

  if ($self->getValues('ElabFull')) {
    my $Indexes = $page->getAttribute("indexes");
    if ( $Indexes ) {
      my $ListLength = scalar(MakeList($Indexes));
      if ( $ListLength > 1 ) {
        $PageExecSub .= "NewIndexEntries(\$self, \$page, undef, ".MakeTextList($Indexes).");\n";
      }
      elsif ( $ListLength == 1 ) {
        $PageExecSub .= "NewIndexEntry(\$self, \$page, undef, ".MakeTextList($Indexes).");\n";
      }
    }
  }

  if ( $self->getValues('ElabFormat') == EF_PRINT ) {
    my $Outlines = $page->getAttribute("outlines");
    if ( $Outlines ) {
      my $ListLength = scalar(MakeList($Outlines));
      if ( $ListLength > 1 ) {
        $PageExecSub .= "NewOutlineEntries(\$self, \$page, undef, ".MakeTextList($Outlines).");\n";
      }
      elsif ( $ListLength == 1 ) {
        $PageExecSub .= "NewOutlineEntry(\$self, \$page, undef, ".MakeTextList($Outlines).");\n";
      }
      else {
      }
    }
  }

#  my $MarkName = $page->getAttribute("endmark");
#  if ( $MarkName ) {
  if ( my $MarkName = $page->getAttribute("endmark") ) {
    $Marks->{$MarkName} = [1, 1, 0];
    my $setMarkExpr = "
      \$self->CloseIndexEntries(\$page, undef, THIS_ENTRY, \@{\$self->{MarkedIndexes}->{$MarkName}});
      \@{\$self->{Marks}->{'$MarkName'}} = (\$OUTPUT->atPage()+1, undef, \$page->atOffset());
    ";
    $PageExecSub .= $setMarkExpr;
  }

  $PageExecSub .= ''
#           . " \$U::UserVars->{UserRef} = \$page->getJobReportValues('JobReportDescr') if ( !exists(\$U::UserVars->{UserRef}) );\n"
           ;
  
  #$PageExecSub.= "\n i\:\:warnit(\"DEBUG - ANTE UserRef=\".\$U::UserVars->{'UserRef'}); " ;  
  $PageExecSub.= "\n\$U::UserVars->{'UserRef'} =~ s/^[ \t\s]+//; " if ( (getConfValues('TRIM_USERREF'))); 
  #$PageExecSub.= "\n i\:\:warnit(\"DEBUG - POST UserRef=\".\$U::UserVars->{'UserRef'}); " ;
		   
  $PageExecSub .= ''
           . " \$U::UserVars->{'\$\$bundles\$\$'} = join(' ', map { \$_.'/'.\$bundleslist->{\$_} } keys \%{\$bundleslist} )\n"
           . "                                     if ( scalar(keys \%{\$bundleslist} ) );\n"
           . "  return \$getVars;\n"
           . "}"
           ;
  $main::veryverbose && i::warnit("PageSubs Completed - pageexecsub: ".$PageExecSub
                                 ."\npagetestsub: ".$PageTestSub);
  #warn "PAGESUB: TEST -\n", $PageTestSub."\n EXEC -\n".$PageExecSub, "\n";
#  warn "Current msg: $@\n";

  my $testCode = eval $PageTestSub;
  croak "UserError: PageTestSub\n$PageTestSub\n$@" if $@;

  my $execCode = eval $PageExecSub;
  croak "UserError: PageExecSub\n$PageExecSub\n$@" if $@;

  return ($testCode, $execCode);

}


sub MakeLineDef {
  my ($self, $line, %LineDef) = (shift, shift, ()); my $LineDef = \%LineDef;

  $LineDef{name} = $line->getAttribute('name');

  $LineDef{NamedNode} = $line->getAttributes();

  @LineDef{LineTestSub, LineExecSub} = $self->MakeLineSubs($line);

  @LineDef{LineEndLineSub, LineEndLineTypeSub}
    =
  (eval 'sub {}', eval 'sub {}'); bless $LineDef, XReport::Parser::NamedNode;
}

sub MakePageDef {
  my ($self, $page, $Marks) = (shift, shift, shift);
  my $args = { @_ };
  my $PageDef = {};

  $PageDef->{name} = $page->getAttribute('name');

  my $SharedLines = $self->getValues('%SharedLines');

  $PageDef->{NamedNode} = $page->getAttributes();
  $PageDef->{category} = $page->getAttribute('category');
  my $hdattr = $page->getAttribute('holddays');
  $hdattr = undef unless (defined($hdattr) && $hdattr !~ /^\s*$/);

  my $linepos = lc($page->getAttribute('linePos'));
  if ( !$linepos ) {
     $linepos = lc($self->getValues('linePos'));
     $self->setValues(linePos => $linepos = 'expanded' ) unless $linepos;
  }
  else {
    $self->setValues(linePos => $linepos);
  }
  $PageDef->{linePos} = $linepos;

  $PageDef->{holddays} = ($hdattr ? $hdattr : $self->getValues('HoldDays'));
  $PageDef->{contid} = (defined($page->getAttribute('contid')) ?  $page->getAttribute('contid') : defined($XReport::cfg->{contid}) ? $XReport::cfg->{contid} : 'Y');
  @{$PageDef}{PageTestSub, PageExecSub} = $self->MakePageSubs($page, $Marks, $args, $PageDef);

  @{$PageDef}{PageEndPageSub, PageEndPageTypeSub} = (eval 'sub {}', eval 'sub {}');

  my (@eLines, @iLines, @LineDefs) = ();

  @eLines = split(/ +/,$page->getAttribute("lines"));
  if ( @eLines ) {
    @eLines = map { $SharedLines->{$_} } @eLines;
  }
  else {
    @eLines = ();
  }

  @iLines = getChildsByName($page, 'line');

  @LineDefs = ();

  for my $line (@eLines, @iLines) {
    next if !$line;
    push @LineDefs, $self->MakeLineDef($line);
  }

  $PageDef->{LineDefs} = \@LineDefs;
  $PageDef->{TabLineDefs} = {
    map {$_->getAttr('name'), $_} @LineDefs
  };

  bless $PageDef, XReport::Parser::NamedNode;
}

sub MakeReportDef {
  my ($self, $report, %ReportDef) = (shift, shift, ()); my $ReportDef = \%ReportDef;

  $ReportDef{name} = $report->getAttribute('name');

  $ReportDef{NamedNode} = $report->getAttributes();
  ### Each Report def has his own UserVars defined
  $ReportDef{UserVars} = {};
  my $bundles = $report->getAttribute('bundles');
  $ReportDef{bundles} = $bundles if $bundles;

  my $if = $report->getAttribute('if');
  my $includeIf =
   "sub {\n".
     "return ".$self->NewIncludeTest($if).";\n".
   "}"
  ;
  $includeIf = eval $includeIf; croak "UserError: $includeIf\n$@" if $@;

  my $filterVar = $report->getAttribute('filterVar');

  @ReportDef{qw(includeIf filterVar)} = ($includeIf, $filterVar);

  my $cluster = $report->getAttribute('cluster');
  my $clVars = $report->getAttribute('clVars');
  my $maxsize = $report->getAttribute('maxsize');

  if ( $cluster ) {
    @ReportDef{qw(cluster clVarName maxsize)} = ($cluster, $filterVar, $maxsize);
  }
  elsif ( $clVars ) {
    my @clVars = split(/\s+/, $clVars);
    push @clVars, $clVars[0], $clVars[0]
     if
    scalar(@clVars) < 3;
    $ReportDef{qw(clVars clVarName maxsize)} = ([@clVars[1,2]], $clVars[0], $maxsize);
  }

  bless $ReportDef, XReport::Parser::NamedNode;

  $self->{'$clRep'} = $ReportDef if $cluster or $clVars; return $ReportDef;
}

#######################################################################
#
#######################################################################

sub getSharedStruct($$$) {
  my ($self, $nodeType, $structName) = (shift, shift, shift);
  my $jobReportDef = $self->getValues('jobReportDef');
  my ($href, $aref) = ({}, []);
  for my $node (getChildsByName($jobReportDef, $nodeType)) {
    my $nameAttr = $node->getAttribute("name");
    $href->{$nameAttr} = $node;
    push @$aref, $node;
  }

  $self->setValues('%'.$structName, $href, '@'.$structName, $aref);
}

sub getValues {
  my $self = shift; my @r;
  for (@_) {
    push @r, $self->{$_};
  }
  return wantarray ? @r : $r[0];
}

sub setValues {
  my $self = shift;
  for (my $j=0; $j < $#_; $j+=2) {
    $self->{$_[$j]}=$_[$j+1];
  }
  return ();
}

sub incrValues {
  my $self = shift; my @r;
  for (my $j=0; $j < $#_; $j+=2) {
    push @r, $self->{$_[$j]} += $_[$j+1];
  }
  return wantarray ? @r : $r[0];
}

sub delValues {
  my $self = shift;
  for (my $j=0; $j < $#_; $j+=2) {
    delete $self->{$_[$j]};
  }
  return ();
}

#######################################################################
#
#######################################################################

sub MatchHashList {
  my ($txt, $aref) = (shift, shift);
  for (my $j=0; $j<=$#$aref; $j+=2) {
    return $aref->[$j+1] if $txt =~ /$aref->[$j]/;
  }
  return '';
}

sub loadFile {
  my $fileName = shift; my $INPUT = gensym();  my $serializedFile;
  open($INPUT, "<$fileName")
   or
  croak("UserError: LoadFile requested by ".join('::', (caller())[1,2]). " OPEN ERROR \"$fileName\" $!"); binmode($INPUT);
  read($INPUT, $serializedFile, -s $fileName); close($INPUT);
  return $serializedFile;
}

sub freeLeaks {
  my $self = shift; $self->{'OUTPUT'} = undef;
}

sub new {
  my ($className, $jr) = @_; my ($INPUT, $OUTPUT, $FileXref); $U::UserVars = {};
  $main::debug && i::warnit("Initializing new instance of $className");
  $U::CatUserVars = {};
  if ( $tabre eq "" ) {
    $tabre = $XReport::cfg->{parser}->{tabre};
    for ( split("\n", $XReport::cfg->{parser}->{tabre}) ) {
      if ( $_ =~ /^\s*(\w+) *= *(\S.*\S) *$/ ) {
        $tabre{$1} = $2;
      }
    }
    $tabre = 1;
  }

  my $self = {}; bless $self, $className; my ($parser, $dom, $jobReportDefs);

  for (qw(ElabFull ElabFormat HoldDays JobReportDescr)) {
    $self->setValues($_, $jr->getValues($_));
  }
  $main::veryverbose && i::warnit("Check HoldDays: ".$self->getValues('HoldDays'));
  require XML::DOM;
  require XReport::FileXref;

  $parser = XML::DOM::Parser->new();

  $dom = $parser->parsefile($jr->getFileName('PARSE'));

  $jobReportDefs = $dom->getElementsByTagName("jobreport");

=pod

  sets current definition to first tag "JobReport "
  then gets and sets the following data struct.

  each structure is composed by an hash and an array

=cut

  #-----------------------------------------------------------
  $self->setValues('jobReportDef', $jobReportDefs->[0]);

  $self->getSharedStruct('page', 'SharedPages');
  $self->getSharedStruct('line', 'SharedLines');
  $self->getSharedStruct('rect', 'SharedRects');
  $self->getSharedStruct('op',   'SharedOps');

  $self->getSharedStruct('outline', 'SharedOutlines');
  $self->getSharedStruct('index',   'SharedIndexes');
  $self->getSharedStruct('exec',    'SharedExecs');

  $self->getSharedStruct('identifier', 'Identifiers');
  $self->getSharedStruct('report',     'Reports');
  $self->getSharedStruct('varxlate',   'VarXlates');

  $self->getSharedStruct('line_iexec', 'line_iexecs');
  $self->getSharedStruct('page_iexec', 'page_iexecs');

  $self->getSharedStruct('lineFilter', 'lineFilters');
  #-----------------------------------------------------------

  my ($iFactory, $oFactory) = ('XReport::INPUT', 'XReport::OUTPUT');

  my $jobReportDef = $self->getValues('jobReportDef');

  
  #setting of TRIM_USERREF
  my   $xmlTRIM_USERREF = ($jobReportDef->getAttribute('TRIM_USERREF') =~ /^([Y1])$/i? 1 : 0);
  setConfValues('TRIM_USERREF', $xmlTRIM_USERREF);
  
  
  #DISABLE_CC_IN_LIST
  my   $DISABLE_CC_IN_LIST = $jobReportDef->getAttribute('DISABLE_CC_IN_LIST');
  setConfValues('DISABLE_CC_IN_LIST', $DISABLE_CC_IN_LIST) if (($DISABLE_CC_IN_LIST) and $DISABLE_CC_IN_LIST !~ /^\s*$/);
  #overwriting of afpdir
  my   $xmlafpdir = $jobReportDef->getAttribute('afpdir');
  setConfValues('afpdir', $xmlafpdir) if (($xmlafpdir) and $xmlafpdir !~ /^\s*$/);
  #setting of EXIT_ON_MISMATCH
  my   $xmlEXIT_ON_MISMATCH = ($jobReportDef->getAttribute('EXIT_ON_MISMATCH') =~ /^([YN])$/i? $1 : 'Y');
  setConfValues('EXIT_ON_MISMATCH', $xmlEXIT_ON_MISMATCH);
  #setting of FORCE_RF
  my   $xmlFORCE_RF = ($jobReportDef->getAttribute('FORCE_RF') =~ /^(ASCII|EBCDIC|AFP|VIPP|XML|POSTSCRIPT|SCS)$/? 'RF_'.$1 : 0);
  setConfValues('FORCE_RF', $xmlFORCE_RF);
  #setting of CASSOC
  my   $CASSOC = ($jobReportDef->getAttribute('CASSOC') =~ /^(CSV|XLS)$/? $1 : '0');
  $CASSOC = 2 if ($CASSOC eq 'CSV');
  $jr->setValues('ExistTypeMap', $CASSOC);
  #setting of FORCE_DISTILLER (es.FORCE_DISTILLER="Distillr")
  my   $xmlFORCE_DISTILLER = ($jobReportDef->getAttribute('FORCE_DISTILLER') =~ /^(Distillr|ghostscript)$/? $1 : 0);
  setConfValues('FORCE_DISTILLER', $xmlFORCE_DISTILLER);
  #setting of STOP_ELAB_AFTER_PAGE
  $main::STOP_ELAB_AFTER_PAGE = ($jobReportDef->getAttribute('STOP_ELAB_AFTER_PAGE') =~ /^(\d+)$/i ? $1 : undef);
  #setting of CODEPAGE
  my   $xmlCODEPAGE = ($jobReportDef->getAttribute('CODEPAGE') =~ /^[\s]*([^\s]+)[\s]*$/i ? $1 : $XReport::cfg->{CODEPAGE});
  setConfValues('CODEPAGE', $xmlCODEPAGE);
  

   my $appL = $jobReportDef->getAttribute('cateList');
  $appL = 'NULLLIST' if ((!$appL) or $appL =~ /^\s*$/);
  my $cateList = [split /\|/, $appL];

  $main::veryverbose and &$logrRtn("Retrieving ".Dumper($cateList));
  $self->setValues('cateList', $cateList);
  my $parseParametersString = $jr->getValues('parseparametersstring');
  foreach ( split /[\/;, ]/, $parseParametersString ) {

    my ( $parm, $val ) = split / ?= ?/, $_, 2;
    warn "ParseParameters: $parm = $val\n";
    $val = '' unless $val;
    $jobReportDef->setAttribute($parm, $val);
  }
  my $linePos = lc($jobReportDef->getAttribute('linePos'));
  $linePos = 'expanded' unless $linePos;
  $self->setValues('linePos', $linePos);

  if (my $oFactory_ = $jobReportDef->getAttribute("oFactory")) {
    $oFactory = $oFactory_;
  }

  $perllib = getConfValues('userlib')."/perllib";
  $main::debug && i::warnit("Loading $iFactory, $oFactory");
  eval "require $iFactory; require $oFactory";
  die $@ if $@;

  $main::debug && i::warnit("Preparing parser hooks");
  my $line_iexecs = $self->getValues('@line_iexecs');
  if ( @$line_iexecs ) {
    my $line_iexec = $line_iexecs->[0];
    my $code = ($line_iexec->getAttribute("ref"))
     ? loadFile("$perllib/".$line_iexec->getAttribute("ref"))
     : getTextOfChilds($line_iexec)
    ;
    $jr->setValues(line_iexec => eval "$code");
  }
  else {
    $jr->setValues(line_iexec => undef);
  }

=pod

 page_iexec definitions handling

=cut

  my $page_iexecs = $self->getValues('@page_iexecs');
  if ( @$page_iexecs ) {
    my $page_iexec = $page_iexecs->[0];
    my $code = ($page_iexec->getAttribute("ref"))
     ? loadFile("$perllib/".$page_iexec->getAttribute("ref"))
     : getTextOfChilds($page_iexec)
    ;
    $iFactory = eval "$code";
  }
 
  $INPUT = $iFactory->new( $jr );
  $OUTPUT = $oFactory->new( $jr, $dom );
  $INPUT->Open();

  #-----------------------------------------------------------
  my $CreOutlineDefs = ''; my $MarkedOutlines = {};

  my $SharedOutlines = $self->getValues('@SharedOutlines');
  if ( $self->getValues('ElabFormat') == EF_PRINT ) {
    for my $outline (@$SharedOutlines) {
      my $NameAttr = $outline->getAttribute("name");
      my $TypeAttr = $outline->getAttribute("type") || "PAGE";
      my $EntriesAttr = $outline->getAttribute("entries") || "FIRST";
      my $MarkName = $outline->getAttribute("mark");
      my $VarsAttr = $outline->getAttribute("vars");
      $CreOutlineDefs .=
      'NewOutline(' .
        "\$self, " .
        "'$NameAttr', " .
        "'$TypeAttr', " .
        "'$EntriesAttr', " .
        "'$MarkName', " .
        "[".MakeTextList($VarsAttr)."]" .
      ');';
      push @{$MarkedOutlines->{$MarkName}}, $NameAttr if $MarkName;
    }
  }
  $CreOutlineDefs = eval "sub {\n". $CreOutlineDefs .'}'; croak "UserError: $@" if $@;

  #-----------------------------------------------------------
  my $CreIndexDefs = ''; my $MarkedIndexes = {};

  if ($self->getValues('ElabFull')) {
    my $SharedIndexes = $self->getValues('@SharedIndexes');
    (my $manfqn = $jr->getFileName('WORKDIR').'/'.File::Basename::basename($jr->getFileName('IXFILE', '$$MANIFEST$$'))) =~ s/\//\\/g;
     my $manifest = new FileHandle(">$manfqn");
    for my $index (@$SharedIndexes) {
      my $NameAttr = $index->getAttribute("name");
      my $TypeAttr = $index->getAttribute("type") || "PAGE";
      my $EntriesAttr = $index->getAttribute("entries") || "ALL";
      my $MarkName = $index->getAttribute("mark");
      my $VarsAttr = $index->getAttribute("vars");
      my $ChildsAttr = $index->getAttribute("childs");
      my $ExcelName = $index->getAttribute("excel");
      my $TableName = $index->getAttribute("table");
      $CreIndexDefs .=
      'NewIndex(' .
        "\$self, " .
        "'$NameAttr', " .
        "'$TypeAttr', " .
        "'$EntriesAttr', " .
        "'$MarkName', " .
        "'$ExcelName', " .
        "'$TableName', " .
        "[".MakeTextList($VarsAttr)."], " .
        "[".MakeTextList($ChildsAttr)."] " .
      ');';
      push @{$MarkedIndexes->{$MarkName}}, $NameAttr if $MarkName;
      my $ixfname = File::Basename::basename($jr->getFileName('IXFILE', $TableName));
      print $manifest join("\t", $TableName, "$ixfname",1), "\n";
    }
    $manifest->close();
  }
  $CreIndexDefs = eval "sub {\n". $CreIndexDefs .'}'; croak "UserError: $@" if $@;

  #-----------------------------------------------------------
  my $ExecDefs = {};

  my $SharedExecs = $self->getValues('@SharedExecs');
  for my $exec (@$SharedExecs) {
    my $NameAttr = $exec->getAttribute("name");
    my $ref = $exec->getAttribute("ref");
    my $code = ($exec->getAttribute("ref"))
     ? loadFile("$perllib/".$exec->getAttribute("ref"))
     : getTextOfChilds($exec)
    ;
    $ExecDefs->{$NameAttr} = eval
      "package U; no strict 'vars';\n"
     ."sub {\n"
     ."  my (\$parser, \$page, \$line) = \@_;\n"
     ."  $code; \n"
     ."}"
    ;
    croak "UserError: $@" if $@;
    #&{$ExecDefs{$NameAttr}}();
  }
  $self->{ExecDefs} = $ExecDefs;

  #-----------------------------------------------------------
  %c::VarXlates = ();

  my $VarXlates = $self->getValues('@VarXlates');
  for my $varxlate (@$VarXlates) {
    my $NameAttr = $varxlate->getAttribute("name");
    my $Xlates = $c::VarXlates{$NameAttr} = {};
    for my $xlate (getChildsByName($varxlate, "xlate")) {
      my $NameAttr = $xlate->getAttribute("name");
      my $ValueAttr = $xlate->getAttribute("newvalue");
#      print "lala $NameAttr => $ValueAttr lala\n";
      $Xlates->{$NameAttr} = $ValueAttr;
    }
  }
  use Data::Dumper;
  $main::veryverbose && i::warnit("VARXLATES:\n". Dumper(\%c::VarXlates));
  my $defaultcopies = $jr->getValues('PrintCopies');
  $defaultcopies = 0 unless $defaultcopies;
  my $defaultBundles = $jobReportDef->getAttribute("bundles");
  #-----------------------------------------------------------
  my %PageDefs = ();  my $Marks = {};

  my $SharedPages = $self->getValues('@SharedPages');
  for my $page (@$SharedPages) {
    my $NameAttr = $page->getAttribute("name");

    $PageDefs{$NameAttr} = $self->MakePageDef($page, $Marks, copies => $defaultcopies,
                                         ($defaultBundles ? (bundles => $defaultBundles) : ()));
  }
  #----- Making page __NOMATCH__ as default. This page will catch all pages non-matching with other page-rules if last contId flag is Y (yes)
  my $NoMatchPage = XML::DOM::Parser
            ->new()
               ->parse("<page name=\"__NOMATCH__\" test=\"1\" setvars=\"CUTVAR \{__NOMATCH__\} UserRef \{__NOMATCH__\}\" contid=\"Y\""
                       . " />" )
              ->getElementsByTagName("page")->[ 0 ];
  #$PageDefs{$NoMatchPage->getAttribute("name")} = $self->MakePageDef($NoMatchPage, $Marks);
  my $defaultNoMatchPage = $self->MakePageDef($NoMatchPage, $Marks);
  #-----------------------------------------------------------
  my %ReportDefs = (); my $FilterVarsDef = {}; my $FilterVarsValues = {};


  my $dbReportDefs;
=pod
  my $dbReportDefs
   = $jr->getReportDefs(
     map {$_->getAttribute("name")} @{$self->getValues('@Reports')}
  );
=cut

  my $JobReportName = $jr->getValues('JobReportName');
  my $Reports = $self->getValues('@Reports');
  if ( !scalar(@$Reports)  ) {
    my $dbReportDef = $jr->getReportDefs($JobReportName)->{$JobReportName};
    warn "Report Config: ", Dumper($dbReportDef), "\n";
    my $filterVar = $dbReportDef->{'FilterVar'};
    warn "Defining missing report JobReportname: $JobReportName FilterVar: $filterVar\n";
    my $Report = XML::DOM::Parser
            ->new()
               ->parse("<report name=\"$JobReportName\" if=\"1\" filterVar=\"$filterVar\""
                       . " />" )
              ->getElementsByTagName("report")->[ 0 ]
    ;
    $self->setValues('%Reports', {$JobReportName => $Report}, '@Reports', [$Report]);
    push @$Reports, $Report;
  }
  my $FilterVarsMask = {};


  for ( @$Reports ) {
    my $ReportName = $_->getAttribute("name");
    if ( $ReportName =~ /\$JobReportName/i ) {
      $ReportName =~ s/\$JobReportName/$JobReportName/i;
      $_->setAttribute("name", $ReportName);
    }
  }

  $dbReportDefs = $jr->getReportDefs(map {$_->getAttribute("name")} @$Reports);

  if ( my $FilterVars = getConfValues('FilterVars') ) {
    if ( ref($FilterVars) eq 'ARRAY' ) {
      $FilterVars = $FilterVars->[0];
    }
    $FilterVarsMask = $FilterVars->{'FilterVar'};
  }

  for my $report (@$Reports) {
    my $NameAttr = $report->getAttribute("name"); my $expr;

    if ( !exists($dbReportDefs->{$NameAttr}) ) {
      #croak("UserError: Report $NameAttr not defined in DB");
    }
    my $dbReportDef = $dbReportDefs->{$NameAttr};
    my $ReportDef = $self->MakeReportDef($report);

    my $filterVar = $ReportDef->{filterVar};

    if ( $filterVar ne $dbReportDef->{filterVar} ) {
      #todo: verify xml definition with db definition
    }

    if ( $filterVar ne "" and not exists($FilterVarsDef->{$filterVar})) {
      if ($_ = $FilterVarsMask->{$filterVar}) {
        my ($re, $format, $len, $prc) = @{$_}{qw(re informat)}; $format .= '.' if index($format, '.') < 0;
        ($format, $len, $prc) = $format =~ /^([a-z]+)(\d*)\.(\d*)$/i;
        $expr = "sub {
          if (\$_[0] !~ /$re/ ) {
            return 0;
          }
          XReport::INFORMAT::$format(\\\$_[0], $len, $prc); return 1;
        }";
      }
      else {
        $expr = '';
      }
      $FilterVarsDef->{$filterVar} = eval($expr); $FilterVarsValues->{$filterVar} = {};
    }

    $ReportDefs{$NameAttr} = $ReportDef;
  }

  my $FilterVars = [ sort(keys(%$FilterVarsDef)) ];

  my $CreFilterOutlines = '';

  for my $filterVar (@$FilterVars) {
    my $FileName = $jr->getFileName('OUTLFILE', "TO.$filterVar");
    $CreFilterOutlines .=
    'NewOutline(' .
      "\$self, " .
      "'TO.$filterVar', " .
      "'PAGE', " .
      "'FIRST', " .  "'', " .
      "[".MakeTextList($filterVar)."]" .
    ');';
  }
  $CreFilterOutlines = eval "sub {\n". $CreFilterOutlines .'}'; croak "UserError: $@" if $@;

  #-----------------------------------------------------------
  my (%IdentifierDefs, @IdentifiersHash) = ();

  my $Identifiers = $self->getValues('@Identifiers');
  for my $identifier (@$Identifiers) {
    my $NameAttr = $identifier->getAttribute("name");
    my $pages = [MakeList($identifier->getAttribute("pages"))];
    my $reports = [MakeList($identifier->getAttribute("reports"))];

    for ($NameAttr, MakeTextLineList(getTextOfChilds($identifier))) {
      push @IdentifiersHash, (qr/$_/, $NameAttr);
    }
    push @IdentifiersHash, (qr/$NameAttr/, $NameAttr);

    my $PageDefs = [];
    for (@$pages) {
      if ( !exists($PageDefs{$_}) ) {
        croak "UserError: Identifier $NameAttr: page $_ NOT DEFINED.";
      }
      push @$PageDefs, $PageDefs{$_};
    }

    my $ReportDefs = [];
    for (@$reports) {
      if ( !exists($ReportDefs{$_}) ) {
        croak "UserError: Identifier $NameAttr: report $_ NOT DEFINED.";
      }
      push @$ReportDefs, $ReportDefs{$_};
    }

    $IdentifierDefs{$NameAttr} = [ $PageDefs, $ReportDefs ];
    #print "??++ <$NameAttr> <", @{$IdentifierDefs{$NameAttr}->[0]}, ">\n";
  }

  #-----------------------------------------------------------

  bless $self, $className;

  my @PageDefs = (); my @ReportDefs = ();

  my $lineFilters = $self->getValues('@lineFilters');
  if ( @$lineFilters ) {
    my $lineFilter = $lineFilters->[0];
    my $code = ($lineFilter->getAttribute("class"))
     ? "require " . $lineFilter->getAttribute("class") . ";"
     : ($lineFilter->getAttribute("ref"))
     ? loadFile("$perllib/".$lineFilter->getAttribute("ref"))
     : getTextOfChilds($lineFilter)
    ;
    my $FilterClass = eval "$code"; die $@ if $@;
    $FilterClass = $lineFilter->getAttribute("class") if ($FilterClass == 1 && $lineFilter->getAttribute("class"));
    $INPUT->Close(); $INPUT->setLineFilter($FilterClass, $jr, $lineFilter); $INPUT->Open();
  }

  #begin loop IDENTIFY ---------------------------------------

  $U::UserVars->{Identifier} = ''; $U::UserVars->{LogicallyDeleted} = 0;

  $self->setValues(
    'JobReportName', $jr->getValues('JobReportName'),
    'JobReportId', $jr->getValues('JobReportId'),
    'INPUT', $INPUT,
    'OUTPUT', $OUTPUT,
    'jr', $jr,
    'dbReportDefs', $dbReportDefs
  );

  my $language = $jobReportDef->getAttribute("language") || 'Italian';

#  Date::Calc::Language($Languages{lc($language)});
  Date::Calc::Language(1);

  my $pspgexit;
  $jr->setValues( PSPageExit => $pspgexit ) ;
  $jr->setValues( PageMKExcel => $pspgexit ) ;
  $perllib = getConfValues('userlib')."/perllib"; 
  for my $exitdom (getChildsByName($jobReportDef, 'exit')) {
    #next if ($exitdom->getAttribute('type') !~ /^PSPageExit$/i );
    next if ($exitdom->getAttribute('type') !~ /^(PSPageExit|PageMKExcel)$/i );
    my $type_of_pspgexit = $1;

    my $cfgclass = $exitdom->getAttribute('perlclass');
    my $ref = $exitdom->getAttribute('ref');
	
    unless ( -e "$perllib/$ref" ) {
       &$logrRtn("unable to access \"$perllib/$ref\" as requested to load by report config");
       next;
    }
    my $perlclass;
    eval { $perlclass = require "$perllib/$ref" ;};
    if ( $@ ) {
       &$logrRtn("Error trying to load \"$perllib/$ref\" - $@");
       next;
    }
    $perlclass = $cfgclass unless (defined($perlclass) && $perlclass =~ /[^\d]+/ );

    unless ( $perlclass ) {
       &$logrRtn("unspecified module class for \"$perllib/$ref\" requested by report config - skipped ");
       next;
    }

    if ( $perlclass->can('new') ) {
        $pspgexit = $perlclass->new($self, $jr->getFileName('workdir'), $exitdom);
    }
    else {
        $pspgexit = bless {
           'JobReportName' => $jr->getValues('JobReportName')
          ,'JobReportId' => $jr->getValues('JobReportId')
          ,'INPUT' => $INPUT
          ,'OUTPUT' => $OUTPUT
          ,'jr', $jr
          ,'dbReportDefs' => $dbReportDefs
          ,'workdir' => $jr->getFileName('workdir')
          ,'exitdef' => $exitdom
        }, $perlclass;
    }
    &$logrRtn("Postscript page exit set to ".ref($pspgexit)." from \"$perllib/$ref\" ");
    $jr->setValues( PSPageExit => $pspgexit )  if ($type_of_pspgexit =~ /^PSPageExit$/i );
    $jr->setValues( PageMKExcel => $pspgexit ) if ($type_of_pspgexit =~ /^PageMKExcel$/i ); 
  }
  #$jr->setValues( PSPageExit => $pspgexit );

  if ( exists($PageDefs{IDENTIFY}) ) {
    my $PageDef = $PageDefs{IDENTIFY}; $self->{IDENTIFY_PHASE} = 1;
    for ( 1..($PageDef->getAttr('maxpages') || 5) ) {
      last if ( !(my $page = $INPUT->GetPage()) );
      if ( $U::UserVars->{Identifier} eq "" ) {
        if ($PageDef->{PageTestSub}($page)) {
          last if &{$PageDef->{PageExecSub}}($self, $page);
        }
      }
    }
    $self->{IDENTIFY_PHASE} = 0; $INPUT->Close(); $INPUT->Open();
  }
  #todo: put here test for UserTimeRef and UserTimeElab

  if ( $U::UserVars->{Identifier} eq '' and exists($IdentifierDefs{DEFAULT}) ) {
    $U::UserVars->{Identifier} = 'DEFAULT';
  }

  my ($PageDefs, $ReportDefs);

  if ( scalar(@{$self->getValues('@Identifiers')}) ) {
    if ( $U::UserVars->{Identifier} eq '' ) {
      croak("UserError: Report Identifier string not FOUND.");
    }
    my $Identifier = MatchHashList($U::UserVars->{Identifier}, \@IdentifiersHash);
    if ( $Identifier eq '' ) {
      croak("UserError: Report Identifier string \"$U::UserVars->{Identifier}\" not MATCHED.");
    }
    &$logrRtn("Identifier is $Identifier");
    $PageDefs = $IdentifierDefs{$Identifier}->[0];
    $ReportDefs = $IdentifierDefs{$Identifier}->[1];
  }
  else {
    $PageDefs = []; $ReportDefs = [];
    #if ( scalar(values(%IdentifierDefs)) ) {
      for my $page (@$SharedPages) {
        my $NameAttr = $page->getAttribute("name");
        push @$PageDefs, $PageDefs{$NameAttr}
          if
        ( $NameAttr ne "IDENTIFY" );
      }
      for my $page (@$Reports) {
        my $NameAttr = $page->getAttribute("name");
        push @$ReportDefs, $ReportDefs{$NameAttr};
      }
    #}
   }
  #end loop IDENTIFY -----------------------------------------

  #at the end $PageDefs, $Reports

  #mettere parte identificativa report -> reportlist
  foreach my $category (@{$cateList}){
    $FileXref = {} unless defined($FileXref);
    $FileXref->{$category} = XReport::FileXref->new($jr, $OUTPUT,$category);

  }
  #$FileXref = XReport::FileXref->new($jr, $OUTPUT);

#  foreach my $pdef ( @{$PageDefs} ) {
#    foreach my $ldef ( @{$pdef->{LineDefs}} ) {
#  	  delete $ldef->{NamedNode};
#    }
#    delete $pdef->{NamedNode};
#  }
#
  $self->setValues(
    JobReportName=> $jr->getValues('JobReportName'),
    JobReportId => $jr->getValues('JobReportId'),
    FilterVarsDef => $FilterVarsDef,
    FilterVarsValues => $FilterVarsValues,
    ReportDefs => $ReportDefs,
    PageDefs => $PageDefs,
    NoPageDef => $defaultNoMatchPage,
    OutlineDefs => {},
    Outlines => {},
    LastOutlines => {},
    IndexDefs => {},
    Indexes => {},
    Marks => $Marks,
    MarkedOutlines => $MarkedOutlines,
    MarkedIndexes => $MarkedIndexes,
    INPUT => $INPUT,
    OUTPUT => $OUTPUT,
    FileXref => $FileXref,
    InputLines => 0, 'InputPages' => 0,
    ParsedLines => 0, 'ParsedPages' => 0,
    DiscardedLines => 0, 'DiscardedPages' => 0,
    jr => $jr,
    dbReportDefs => $dbReportDefs,
    discardList => []
  );
  $self->delValues('jobReportDef');

  &$CreOutlineDefs(); &$CreFilterOutlines(); &$CreIndexDefs();

  $OUTPUT->Open($self);

  return $self;
}

sub END_OUTPUT {
  my ($self, $Closing) = @_; my ($OUTPUT, $FileXref) = @{$self}{qw(OUTPUT FileXref)};
  my $jrdescr = $self->getValues( qw(jr) )->getValues(qw(JobReportDescr));
  for my $rdef (@{$self->{ReportDefs}}) {
    next if !$rdef->{started}; #okkio che � un hash
    my $ReportName = $rdef->{name};
    my $descr = (exists($rdef->{descr}) && $rdef->{descr} ne '' ? $rdef->{descr} : $jrdescr);


#    if ( $Closing && $rdef->{clVars} ) {
#      my $totPages = $OUTPUT->{atPage} - ($rdef->{'$pageFmValue'} || 1) + 1;
#      push
#        @{$rdef->{'$FileIdList'}},
#        [$OUTPUT->{atFile}, @{$rdef}{qw($firstFmValue $lastToValue)}, $totPages]
#      ;
#    }
#TODO Review Last Page Logic
    if ( $Closing ) {
      my $catelist = $self->getValues(qw(cateList));
      foreach my $catid (@{$catelist}){
        $FileXref->{$catid}->Closing(1);
          if ($rdef->{currFilter}->{$catid} ne "") {
        my $filterVar = $rdef->{filterVar};
        $filterVar = '$$$$' unless $filterVar;
        $FileXref->{$catid}->EndItem($rdef->{name}, $filterVar, $rdef->{currFilter}->{$catid});
    #      $FileXref->EndItem(
    #        @{$rdef}{qw(name filterVar currFilter)}
    #      );
    #        if ( $rdef->{cluster} ) {
            my $totPages = $OUTPUT->{atPage} - ($rdef->{'$pageFmValue'} || 1) + 1;
            push
            @{$rdef->{'$FileIdList'}},
            [$OUTPUT->{atFile}, @{$rdef}{qw($firstFmValue $lastToValue)}, $totPages]
            ;
    #        }
          }
          my $reportBundles = $U::CatUserVars->{$catid}->{'$$bundles$$'};
          $reportBundles = $rdef->{bundles} unless ( $reportBundles && $reportBundles !~ /^\s*$/);
          $reportBundles = '' unless $reportBundles;
          $FileXref->{$catid}->EndItem($rdef->{name}, '$$$$', '$$$$|'.( $descr && $descr ne '' ? $descr : $ReportName ), $reportBundles);
      }

    }
  }

  $self->PutOutlineTrees(); $self->ResetOutlineTrees();
}

sub NewFile {
  my $self = shift; my $OUTPUT = $self->{OUTPUT}; my $FileId;

  $self->END_OUTPUT();

  $FileId = $OUTPUT->NewFile(@_);

  #todo: restart of OutlineTrees();

  return $FileId;
}

sub DESTROY {
  $main::debug && i::warnit( __PACKAGE__ . " DESTROY Parser");
}

sub TERMINATE {
  my $self = shift; #print "PARSER TERMINATE\n";

  %{$self} = ();

  %U::UserVars = ();
  %U::LineFilter:: = ();
}

sub Close {
  my $self = shift;
  my ($jr, $INPUT, $OUTPUT) = $self->getValues(qw(jr INPUT OUTPUT));

  &$logrRtn("INPUT OUTPUT CLOSING STARTED !! INPUT: ".ref($INPUT)." OUTPUT: ".ref($OUTPUT));
  $self->END_OUTPUT(1);

  $INPUT->Close(finalize => 1);

  if(ref($INPUT) eq 'XReport::AFP::IOStream')
  {
	  my $INPUT2 = XReport::INPUT->new($jr,RF_EBCDIC);
	  &$logrRtn("INPUT2 OUTPUT CLOSING STARTED !! INPUT2: : ".ref($INPUT2));
	  $INPUT2->Close(finalize => 1);
  }
  
  $OUTPUT->Close();
  $self->CloseIndexes();
  $self->createPageOffsetsTSV() if ( !exists($self->{PageOffsetsTSV}) );
  if ( scalar(@{$self->{page_offsets}}) ) {
          my $potsv = $self->{PageOffsetsTSV};
          print $potsv ''.join("\n", map { join("\t", @$_) } @{$self->{page_offsets}} ), "\n";
          @{$self->{page_offsets}} = [];
  }
  $self->{PageOffsetsTSV}->close();

  my ($JobReportName, $JobReportId, $dbReportDefs)
                = $self->getValues('JobReportName', 'JobReportId', 'dbReportDefs');
  $perllib = getConfValues('userlib')."/perllib";
#TODO: assegnare valori per defaults
  my $userAttrs  = {
        UserTimeElab => { default => 'JobExecutionTime', null => 'NULL' },
        UserTimeRef => { default => 'XferStartTime', null => 'NULL' },
        UserRef => { default => 'JobReportDescr', null => '' },
  };
#####################################
  for ( qw(UserTimeElab UserTimeRef UserRef) ) {
    $self->{$_} = $U::UserVars->{$_} if $self->{$_} eq '';
    if ( $self->{$_} eq '' ) {
      &$logrRtn("MISSING $_ $JobReportName $JobReportId ?!");
      $self->setValues( $_, ($_ ne 'UserRef') ? 'NULL' : '' ) if $self->getValues($_) eq '';
    }
    $jr->setValues($_, $self->getValues($_));
  }

  &$logrRtn("INPUT OUTPUT CLOSING ENDED !!");

  do { $self->TERMINATE(); return; } if !$self->getValues('ElabFull');
  my $gblXrefPageList = {};
  $gblXrefPageList->{globalHoldDays} = 33;
  my $cateList = $self->getValues('cateList');
  my $alltextstrings = {};
  foreach my $catid (@$cateList){
    my $FileXref = $self->{FileXref}->{$catid};
    $gblXrefPageList = $FileXref->getXrefPageList($gblXrefPageList);
    $main::veryverbose && i::warnit("addtextstring ".Dumper($FileXref->{textstrings}));
    $alltextstrings =  { %{$alltextstrings}, %{$FileXref->{textstrings}} };
    #foreach(keys $XrefPageList
    #my @newKeys = keys %$XrefPageList;
    #while (scalar(@newKeys) ){
    #        my $userRef = shift @newKeys;
    #   my $rdef = delete $XrefPageList->{$userRef};
    #   if (!exists($gblXrefPageList->{$userRef})){
    #       $glbXrefPageList->{$userRef} = $rdef ;
    #   }else{
    #      my $listVarn = [keys %$rdef];
    #           while (scalar(@$listVarn)){
    #       my $varn = shift @$listVarn;
    #       my $varnref = delete $rdef->{$varn};
    #       if(!exists($glbXrefPageList->{$userRef}->{$varn})){
    #          $glbXrefPageList->{$userRef}->{$varn} = $varnref;
    #       }else{
    #          my $listVarvalue = [keys %$varnref];
    #              while(scalar(@$listVarvalue)){
    #           my $varv = shift @$listVarn;
    #           my $varvref = delete $varnref->{$varv};
    #           if(!exists($glbXrefPageList->{$userRef}->{$varn}->{$varv})){
    #               $glbXrefPageList->{$userRef}->{$varn}->{$varv} = $varvref;
    #
    #           }else{
    #               %globalPages = {};
    #               $glbXrefPageList->{$userRef}->{$varn}->{$varv}->{pages};
    #               while(scalar(@{$varvref->{pages}})){
    #               #####  splice @{$varvref->{pages}}, 0, 2;
    #               }
    #           }
    #          }
    #       }
    #      }
    #   }
    #}
    #$gblXrefPageList = {%gblXRefPageList, %$XrefPageList};
  }
  my $XrefPageList = $gblXrefPageList;
  $main::veryverbose && i::warnit("Parser HoldDays".Dumper($XrefPageList->{globalHoldDays}));
  my $allPages = $OUTPUT->totPages();
  my $ReportId = 0;
  my %PhysicalReports = ("1,$allPages" => [$ReportId , -1]);
  my $discardList = $self->{discardList};

  my $duped_yes = 1;
  $self->setValues('HoldDays', $XrefPageList->{globalHoldDays});
  #$jr->NewPhysicalReport($ReportId, $allPages, \"1,$allPages");
  my $textstrings = [ keys %{$alltextstrings} ];
  $main::veryverbose && i::warnit("addedtextstring ".Dumper($textstrings));
  $textstrings = $jr->addLogicalReportsTexts($textstrings) if scalar(@{$textstrings});
  my ($LRList, $PRList) = ([], [{rid => $ReportId, totp => $allPages, lop => "1,$allPages"}]);
  &$logrRtn("BEGIN XrefPageList Processing");
  for my $r (sort(keys(%$XrefPageList))) {
    my $rRef = delete $XrefPageList->{$r};
    for my $fN (sort(keys(%{$rRef}))) {
      my $fNRef = delete $rRef->{$fN};
      for my $fV (sort(keys(%{$fNRef}))) {
        my $fVRef = delete $fNRef->{$fV};
        my ($pageArray, $totPages) = ($fVRef->{pages}, 0);
        my $pageList = join(',', map{ join(',', @{$_}[0..1])} @$pageArray);
        $main::veryverbose and &$logrRtn("XrefPageList LR - R:$r FN:$fN FV:$fV PL:".Dumper($pageList));
        while ( scalar(@{$pageArray}) ) {
            my ($FromPage, $ForPages, $LastPage) = @{shift @{$pageArray}};
            $totPages += $ForPages;
        }
#        (my $filterVal = (split /\|/, $fV)[0]) =~ s/^\$+$//;
#        $fV = '' if $fV eq '$$$$';

        $main::veryverbose and &$logrRtn("XrefPageList LR - Bund ".Dumper($fVRef->{bundles})."\n R:$r FN:$fN FV:$fV PL:$pageList TOTP:$totPages");

        if ($duped_yes or !exists($PhysicalReports{$pageList}) ) {
          $ReportId += 1;
          $PhysicalReports{$pageList} = [$ReportId, 0];
#          &$logrRtn("XrefPageList Processing $r $fN $fV adding Phis.R");
#          push @{$PRList}, {rid => $ReportId, totp => $totPages, lop => "$ReportId,$totPages"};
          push @{$PRList}, {rid => $ReportId, totp => $totPages, lop => $pageList};
          #warn "XrefPageList 1 ADDing PRLIST: ", Dumper($PRList), "\n";
          $PRList = $jr->addPhysicalReports($PRList) unless scalar(@{$PRList}) < 100;
#          $jr->NewPhysicalReport($ReportId, $totPages, \$pageList) ;
        }
        else {
#          $PhysicalReports{$pageList}->[1] += 1;
        }
        #todo: multiple filtervalues
#        &$logrRtn("XrefPageList Processing $r $fN $fV adding Log.R");
        my $LRentry = {rn => $r, fn => $fN, fv => $fV, totp => $totPages, rdef => $dbReportDefs};
        @{$LRentry}{qw(rid progr)} = @{$PhysicalReports{$pageList}};
        $LRentry->{bundles} = [ keys %{$fVRef->{bundles}} ] if exists($fVRef->{bundles});
        push @{$LRList}, $LRentry;
        $LRList = $jr->addLogicalReports($LRList) unless scalar(@{$LRList}) < 100;
#        $jr->NewLogicalReport($r, $fV, $totPages, @{$PhysicalReports{$pageList}}, $dbReportDefs,
#            (exists($fVRef->{bundles}) ? [ keys %{$fVRef->{bundles}} ] : [] ) );
      }
    }

  }
  # warn "XrefPageList ADDing ENDING PRLIST: ", Dumper($PRList), "\n";
  $PRList = $jr->addPhysicalReports($PRList) if scalar(@{$PRList});
  #warn "XrefPageList ADDing LRLIST: ", Dumper($LRList), "\n";
  $LRList = $jr->addLogicalReports($LRList) if scalar(@{$LRList});

  &$logrRtn("XrefPageList Processing Ended");

=email
  email to users
  get userlists from metadata
  subject body from memory list or xml
  url as jobreportid/user=   filterValue is not user !!
  filtervalue is a mailbox or symbolic destination !!

   my @email_fields = qw(
     StartTime EndTime SmtpServer TotBytes Status SendNumber TotRetries MailClass
     FromField FakeFromField ToField ToFakeField
     SubjectField BodyField EmailerArgs EmailerExitArgs
   );

   <email
    extract_singles="1"
    MailClass="PAGAMENTI_VISAMC"
    SmtpServer="corner"
    ToField="$EMAIL_ADDR"
    FakeToField="$EMAIL_ADDR"
    FromField="merchantaccounting@aduno.ch"
    ExitArgs="NUMERO_CONTO=$NUMERO_CONTO,DEL=$DEL,DATA_PAGAMENTO=$DATA_PAGAMENTO"
  />
=cut

  if ( my $r = $self->{'$clRep'} ) {
    if ( scalar(@{$r->{'$FileIdList'}}) > 1 ) {
      for (@{$r->{'$FileIdList'}}) {
        $jr->NewFileRange(@$_);
      }
      $jr->setValues('FileRangesVar', $r->{clVarName});
    }
    else {
      $jr->setValues('FileRangesVar', '');
    }
  }

  $jr->setDiscardPageList($discardList) if @$discardList;

  my $jobReportDef = $self->getValues('jobReportDef');

  for my $exit (getChildsByName($jobReportDef, 'exit')) {
    next if ($exit->getAttribute('type') ne "post");

    my $perlclass = $exit->getAttribute('perlclass');
    my $ref = $exit->getAttribute('ref');

    require "$perllib/$ref";

    my $postexit = $perlclass->new( $jr, $jr->getFileName('workdir'), $exit );

    $postexit->Commit();
    $postexit->Close();
  }
  $main::veryverbose && i::warnit("Parser End HoldDays".Dumper($self->getValues('HoldDays')));
  for ( qw(
      InputLines InputPages
      ParsedLines ParsedPages
      DiscardedLines DiscardedPages
      UserTimeRef UserTimeElab
      UserRef HoldDays
    ) ) { $jr->setValues($_, $self->getValues($_)); }

  if ( exists($self->{PageOffsetsTSV}) ) {
       my $potsv = $self->{PageOffsetsTSV};
       print $potsv ''.join("\n", map { join("\t", @$_) } @{$self->{page_offsets}} ), "\n"
                         if scalar(@{$self->{page_offsets}});
    $potsv->close();
  }

  $self->TERMINATE();
}

#######################################################################
#
#######################################################################

1;

__END__

$Log: Parser.pm,v $
Revision 1.2  2002/10/23 22:50:18  Administrator
edit

Revision 1.4  2001/03/22 13:10:03  mpezzi
Aggiunte versioni ai file
