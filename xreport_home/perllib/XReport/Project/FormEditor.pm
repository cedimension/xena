
package XReport::Project::FormEditor;

use strict;

require XReport::PDF::AcroForm;

sub getValues {
  my $self = shift; my @r;
  for (@_) {
    die("INVALIF FIELD REQUIRED $_\n") if !exists($self->{$_}); push @r, $self->{$_};
  }
  return wantarray ? @r : $r[0];
}

sub Open {
  my ($class, $ProjectName, %args) = @_; my ($userlib, $workdir, $SrvName) = c::getValues('userlib', 'workdir', 'SrvName'); 
  i::logit(__PACKAGE__ . " Opening $ProjectName as requested by ". join('::', (caller())[0,2]));

  my $lworkdir = "$workdir/$SrvName";
  my $FormFile = "$lworkdir/localres/$ProjectName.pdf"; 
  $args{resh}->getResources(ResName => $ProjectName, ResClass => 'pdfform') || die "Unable to get pdf resource $ProjectName";
  my $ldoc = XReport::PDF::AcroForm->Open("$FormFile");

  my $self = {
    ldoc => $ldoc, lWorkInFlow => 1, ldocFieldNames => {map {$_, undef} $ldoc->FieldNames()}
  };

  bless $self, $class;
}

sub ExtractFieldValues {
  my ($self, $lPage, $lIWorkInPage) = @_; my (@lFieldNames, @lFieldValues, %lFieldValues) = ();
  my (
    $ldoc, $ldocFieldNames
  ) = 
  @{$self}{qw(ldoc ldocFieldNames)}; my $FieldsExtractor = $lPage->{'FieldsExtractor'};

  @lFieldNames = grep {exists($ldocFieldNames->{$_})} $FieldsExtractor->FieldNames(); 

  @lFieldValues = $FieldsExtractor->getValues(@lFieldNames);

  %lFieldValues = map {shift @lFieldNames, shift @lFieldValues} (
    0..$#lFieldNames
  );
 
  $ldoc->UpdateFields(\%lFieldValues);
}

sub FlushValues {
  my $self = shift;  $self->{'lProjectValues'} = {};
}

sub FlushProject {
  print "FormEditor Flushing ".ref($_[0]->{'ldoc'})."\n";
  my ($self, $docTo) = @_; my $ldoc = $self->{'ldoc'}; $ldoc->FlushToPdf($docTo); 

}

1;

