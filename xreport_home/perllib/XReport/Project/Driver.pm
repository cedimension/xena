
package XReport::Project::Driver::Unpacker;

use strict;

use Convert::IBM390 qw(:all);

require XReport::INFORMAT;
require XReport::OUTFORMAT;

sub getValues {
  my $self = shift; my ($FieldsAttrs, $line) = @{$self}{qw(FieldsAttrs line)}; shift if $_[0] eq 'DATAMAP';
  
  my @FieldNames = @_; my ($FieldName, $FieldAttrs, $lline, $FieldValue, @FieldValues);

  my $largs = ref($FieldNames[-1]) ? pop @FieldNames : {}; 
  
  for $FieldName (@FieldNames) {
    $FieldAttrs = $FieldsAttrs->{$FieldName} or die("UNABLE TO EXTRACT Field $FieldName requested by ".join('::', (caller())[1,2])." !!");
    my (
      $lbeg, $llen, $lunpackeb, $maskin, $maskout
    ) =
    @{$FieldAttrs}{qw(lbeg llen lunpackeb maskin maskout)}; 
    
    $lline = substr($line, $lbeg+1, $llen); 
    $FieldValue = unpackeb($lunpackeb, $lline);

    $maskin and $FieldValue = &$maskin($FieldValue);  
    $maskout and $FieldValue = &$maskout($FieldValue);  

    $FieldValue =~ s/\s+$//so; push @FieldValues, $FieldValue;
  }
  
  return wantarray ? @FieldValues : $FieldValues[0];
}

sub FieldNames {
  my $self = shift; my $FieldNames = $self->{'FieldNames'}; return @$FieldNames
}

sub set_line {
  my ($self, $line) = @_; $self->{'line'} = $line;
}

sub new {
  my ($class, $FieldsAttrs) = @_; my ($FieldName, $FieldAttrs);

  while ( ($FieldName, $FieldAttrs) = each %$FieldsAttrs ) {
    my (
      $lbeg, $lend, $maskin, $maskout
    ) =
    @{$FieldAttrs}{qw(lbeg lend maskin maskout)}; my $llen = $lend - $lbeg + 1; my $lunpackeb = " e$llen"; 
    if ($maskin and my ($lmaskin, @largs) = split(/\|/, $maskin, 2)) {
      if ($lmaskin =~ /^(P|Z)(\d+(?:\.\d)?)$/i) { 
        $lunpackeb = ' '.lc($1).$2; $maskin = undef;
      }
      elsif ($lmaskin = XReport::INFORMAT->can($lmaskin)) {
        $maskin = sub {
          &$lmaskin($_[0], @largs)
        };
      }
      else {
        die("INVALID MASKIN ($lmaskin) DETECTED FOR Field($FieldName)\n")
      }
    }
    if ($maskout and my ($lmaskout, @largs) = split(/\|/, $maskout, 2)) {
#      my ($lmaskout, @largs) = @$maskout;
      if ( $lmaskout =~ /^%/ ) {
        $maskout = sub { sprintf($lmaskout, $_[0]); $_[0] };
      } 
      elsif ($lmaskout = XReport::OUTFORMAT->can($lmaskout)) {
        $maskout = sub {
          &$lmaskout($_[0], @largs)
        };
      }
      else {
        die("INVALID MASKOUT ($maskout) DETECTED FOR Field($FieldName)\n")
      }
    }
    @{$FieldAttrs}{qw(lunpackeb maskin maskout)} = ($lunpackeb, $maskin, $maskout); 
  }

  bless {FieldsAttrs => $FieldsAttrs, line => undef, FieldNames => [keys(%$FieldsAttrs)]}, $class;
}

1;

package XReport::Project::Driver;

use strict;

use Symbol;

use Convert::IBM390 qw(:all);

sub setPageAttributes {
  my ($self, $lPage) = @_; my $FieldsExtractor = $self->{'FieldsExtractor'}; $lPage->{'FieldsExtractor'} = $FieldsExtractor; 

  my $lineList = $lPage->lineList(); my $llineList = @$lineList; my $line = $lineList->[0]->Value();

  die("INVALID LINELIST LENGTH($llineList) != 1 DETECTED !!") 
   if 
  $llineList != 1;
  
  $FieldsExtractor->set_line($line);
}

sub getLine {
  my $self = shift; my $INPUT = $self->{'INPUT'}; $INPUT->getLine();
}

sub getLines {
  my ($self, $olineOffsets, $olineCache, $omaxLines) = @_;
  my (
    $INPUT, $lineOffsets, $lineCache, 
    $get_acrolist, $set_acrolist 
  ) = 
  @{$self}{qw(INPUT lineOffsets lineCache get_acrolist set_acrolist)}; 
  
  goto FILL if @$lineCache >= $omaxLines or $INPUT->eof();
  
  my ($ilineOffsets, $ilineCache, $imaxLines) = ([], [], $omaxLines); 
  
  while($imaxLines>0) {
    my $offset = $INPUT->tell(); my $line = $INPUT->getLine(); my %fields;
	if ( $line eq "" ) {
	  last;
	}
    my @acrolist = grep(!/^\s*$/, unpack("(A4)*", &$get_acrolist($line)));
    
    for (@acrolist) {
      &$set_acrolist($line, $_);
	  push @$ilineOffsets, $offset; 
      push @$ilineCache, "\xf1".$line; $imaxLines -= 1;
    }
  }

  push @$lineCache, @$ilineCache; push @$lineOffsets, @$ilineOffsets;

  FILL:
  while($omaxLines > 0 and @$lineCache) {
    my ($offset, $line) = (shift @$lineOffsets, shift @$lineCache);
    push @$olineOffsets, $offset; push @$olineCache, $line;
    $omaxLines -= 1;
  }
  
  return scalar(@$olineCache); 
}

sub eof {
  my $self = shift; my $INPUT = $self->{'INPUT'}; $INPUT->eof();
}

sub Open {
  my $self = shift; my $INPUT = $self->{'INPUT'}; $INPUT->Open();
}

sub Close {
  my $self = shift; my $INPUT = $self->{'INPUT'}; $INPUT->Close();
}

sub new {
  use IO::Scalar;
  i::logit("Initializing " . __PACKAGE__ . " called by " . join('::', (caller())[0,2])); 
  my ($class, $INPUT, $jr, $lineFilter) = @_; my $OUTPUT = gensym();  my $itlines = 0;

  my $userlib = c::getValues('userlib'); 
  
  my $datamap = $lineFilter->getAttribute("datamap");  
  my $ProjectField = $lineFilter->getAttribute("ProjectField");
  my $PrinterField = $lineFilter->getAttribute("PrinterField");
  $PrinterField = '--DONTPRINT--' unless $PrinterField;
  $jr->setValues(PrinterDestField => $PrinterField);
  i::logit("PrintDestField set to $PrinterField");
#  my $dmfile = "$userlib/datamaps/$datamap";
  
#  open(MAP, "<$dmfile") || die "open failed for \"$dmfile\" - $!";

  my $workdir = c::getValues('workdir');
  my ( $timeref ) = $jr->getValues('XferStartTime');
  my $resh = new XReport::Resources( destdir => "$workdir/$main::Application->{ApplName}/localres", TimeRef => $timeref );
  my $ldatamap = '';
  my $DMAPFH = new IO::Scalar \$ldatamap;
  $resh->getResources(ResName => $datamap, ResClass => 'txtdata', OutFile => $DMAPFH) || die "Unable to get datamap resource $datamap";
  close $DMAPFH;
  
  $DMAPFH = new IO::Scalar \$ldatamap;

  my ($FieldsExtractor, $get_acrolist, $set_acrolist); 

  my $FieldAttrs = {};
  while(1) {
    my $l = <$DMAPFH>; last if !$l; chomp $l; next if $l =~ /^\s*$/; my @l = split(";", $l, 8);
    my ($lname, $lbeg, $lend, $maskin, $maskout, $descr, $llen) = @l[0,2,4,5,6,7]; 
    #$lbeg-=1; $lend-=1; 
    $llen = $lend-$lbeg+1;
    if ($ProjectField ne '' and $lname eq $ProjectField) {
      $get_acrolist = sub {
        unpackeb("e*", substr($_[0], $lbeg, $llen));
      };
      $set_acrolist = sub {
        substr($_[0], $lbeg, $llen) = "\x40" x $llen; substr($_[0], $lbeg, 4) = packeb("e4", $_[1]);
      };
    }
    @{$FieldAttrs->{$lname}}{qw(lname lbeg lend llen maskin maskout descr)} = ($lname, $lbeg, $lend, $llen, $maskin, $maskout, $descr);
  }
  close($DMAPFH);

  $FieldsExtractor = XReport::Project::Driver::Unpacker->new($FieldAttrs);

  i::logit("Datamap $datamap defines ".scalar(keys(%$FieldAttrs))." fields to ".join('::', (caller())[0,2]));

  my $self = {
    get_acrolist => $get_acrolist, set_acrolist => $set_acrolist, 
    FieldsExtractor => $FieldsExtractor,  
    INPUT => $INPUT, lineOffsets => [], lineCache => [] 
  };

  bless $self, $class;
}

__PACKAGE__;

