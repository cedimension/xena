
package XReport::Project::LineEditor::FlowPage;

use strict;

sub lineList {
  my $self = shift; return $self->{lineList};
}

sub GetPageRect {
  my ($self, $fr, $fc, $tr, $tc) = @_; my $t = ""; my $j;

  my $lineList = $self->lineList(); 

  my $lLen = scalar(@$lineList);

  for ($fr, $fc, $tr, $tc) {
    $_-- if $_ > 0;
  } 

  $fr = -$lLen if $fr < 0 && abs($fr) > $lLen; 
  $tr = $#$lineList if $tr > $#$lineList;

  return '' if $fr > $tr;

  for ( $j=$fr;$j<=$tr;$j++ ) {
	last if $j>$#$lineList;
    $t .= substr($lineList->[$j],$fc,$tc)."\n";
  }

  return $t;
}

sub GetPageRectList {
  my ($self, $fr, $fc, $tr, $tc) = @_; my @t = (); my $j;

  my $lineList = $self->lineList(); 

  my $lLen = scalar(@$lineList);

  for ($fr, $fc, $tr, $tc) {
    $_-- if $_ > 0;
  } 

  $fr = -$lLen if $fr < 0 && abs($fr) > $lLen; 
  $tr = $#$lineList if $tr > $#$lineList;

  return '' if $fr > $tr;

  for ( $j=$fr;$j<=$tr;$j++ ) {
	last if $j>$#$lineList; push @t, [substr($lineList->[$j],$fc,$tc)];
  }

  return \@t;
}

sub new {
  my ($class, $lineList) = @_;

  bless {lineList => [map {$_ =~ s/\s+$//; " ".$_ } split("\n", $lineList)]}, $class;
}

1;

#------------------------------------------------------------
package XReport::Project::LineEditor;

use strict;

use Symbol;

use Carp::Assert qw(assert);

require MIME::Base64;
require XML::DOM;
use Data::Dumper;

use IO::String;
use XReport::PDF::DOC;

my @rect_attrs = qw(
  Name PParent Parent coord BeginCol EndCol BeginRow EndRow MaskIn DescriptionField
  IWorkInPage IWorkOutPage BlockMoveObject Left Top Width Height MaskOut NameObj 
  ClassName FontName FontColor FontColorRGB FontSize FontStyle Alignment
);

sub getChildsByName {
  my ($N, $t) = (shift, shift);

  return grep {$_->getNodeName() eq $t} @{$N->getChildNodes()}; 
}

sub getValues {
  my $self = shift; my @r;
  for (@_) {
    push @r, $self->{$_};
  }
  return wantarray ? @r : $r[0];
}

sub Open {
  my ($class, $ProjectName, %args ) = @_;   my ($userlib, $workdir, $SrvName) = c::getValues(qw(userlib workdir SrvName));
  i::logit(__PACKAGE__ . " Opening $ProjectName as from ". join('::', (caller())[0,2]));
  
  my ($ProjectFileName, $lProjectFileName, $lProjectPdfFileName, $lxml);
  my $OUTPUT = gensym(); 

  my $lworkdir = "$workdir/$SrvName";
  $lworkdir = $args{workdir} if exists($args{workdir});
  i::logit("ParseProject: Parsing $ProjectName");

  if ( $args{resh} ) {
    my $XMLH = new IO::String $lxml;
    $args{resh}->getResources(ResName => $ProjectName, ResClass => 'prj', OutFile => $XMLH) || die "Unable to get resource $ProjectName";
    close $XMLH;
  }
  elsif ( $args{fileh} ) {
    my $XMLH = new IO::String $lxml;
    while ( 1 ) { $args{fileh}->read(my $buff, 4096) || last; print $XMLH $buff if length($buff); }
    close $XMLH;
  }
  elsif ( $args{filen} ) {
    my $XMLH = new IO::String $lxml;
    my $INFH = new FileHandle("<$args{filen}") || die "Unable to open $args{filen} - $?";
    binmode $INFH;
    while ( 1 ) { $INFH->read(my $buff, 4096) || last; print $XMLH $buff if length($buff); }
    close $INFH;
    close $XMLH;
  }
  elsif ( $args{scalar} ) {
    $lxml = $args{scalar};
  }
  else {
    die "Input data not defined";
  }
#  my ($INPUT, $OUTPUT) = (gensym(), gensym()); 

#  my $lworkdir = "$workdir/$SrvName";

#  $ProjectFileName = "$userlib/merge.projects/$ProjectName.prj"; 
#  open($INPUT, "<$ProjectFileName")
#   or 
#  die("INPUT OPEN ERROR for \"$ProjectFileName\" $!"); binmode($INPUT); 
  
#  read($INPUT, $lxml, -s "$ProjectFileName"); close($INPUT);
  i::logit("Processing project definition for $ProjectName(".length($lxml)." bytes)");
  my ($FlowQueue) = $lxml =~ /\<FlowQueue\><!\[CDATA\[(.*?)]]/mso;
  $lxml =~ s/\<FlowQueue\><!\[CDATA\[(.*?)]]/\<FlowQueue\><!\[CDATA\[\]\]/mso;
  my ($PDFBackgroundSource) = $lxml =~ /\<PDFBackgroundSource\><!\[CDATA\[(.*?)]]/mso;
  $lxml =~ s/\<PDFBackgroundSource\><!\[CDATA\[(.*?)]]/\<PDFBackgroundSource\><!\[CDATA\[\]\]/mso;
  my ($BackgroundFileName) = $lxml =~ /\<BackgroundFileName\><!\[CDATA\[(.*?)]]/mso;
  $lxml =~ s/\<BackgroundFileName\><!\[CDATA\[(.*?)]]/\<BackgroundFileName\><!\[CDATA\[\]\]/mso;

  for ($FlowQueue, $PDFBackgroundSource, $BackgroundFileName) {
    $_ = MIME::Base64::decode_base64($_);
  }

  my $lProjectName = $ProjectName; my $lFlowQueue = $FlowQueue;

  if ( $args{run_in_memory} ) {
    $lProjectPdfFileName = undef;
  }
  else {
    use IO::File;

  $lProjectFileName = "$lworkdir/localres/l$ProjectName.small.xml";
  open($OUTPUT, ">$lProjectFileName")
   or
  die("OUTPUT OPEN ERROR for \"$lProjectFileName\" $!"); binmode($OUTPUT);

  print $OUTPUT $lxml; close($OUTPUT);

  $lProjectPdfFileName = "$lworkdir/localres/$ProjectName.pdf";
  open($OUTPUT, ">$lProjectPdfFileName")
   or
  die("OUTPUT OPEN ERROR for \"$lProjectPdfFileName\" $!"); binmode($OUTPUT);

  print $OUTPUT $PDFBackgroundSource; close($OUTPUT);

  }
  my $dom = XML::DOM::Parser->new()->parse($lxml);

  my $LineEditor = $dom->getElementsByTagName('LineEditor-Project')->[0];

  my $Project = $LineEditor->getElementsByTagName('Project')->[0];

  my $FrameCountBackGrounds = $Project->getAttribute("FrameCountBackGround"); 
  
  my $lWorkInPages = []; my $lWorkOutPages = []; my $lProjectBackGrounds = $FrameCountBackGrounds; 
  
  my $lWorkInFlow = 0; my $lWorkOutFlow;

  for my $rect (getChildsByName($LineEditor, 'rect')) {
    my %rect_values = map {$_, $rect->getAttribute($_)} @rect_attrs;
    for (values %rect_values) {
      $_ =~ s/^\s+|\s+$//gs;
    }
    for (@rect_values{qw(BeginCol EndCol)}) {
      $_ = 2 if $_ == 1;
    }
    my $coordList = [
      @rect_values{qw(BeginRow BeginCol EndRow EndCol)}
    ];
    my $coord = join(",", @$coordList);
    for (@rect_values{qw(Left Top Width Height)}) { 
      $_ =~ s/,/./; $_ = $_/25.4*72; $_ = int($_*1000+0.005)/1000; 
    }
#    my $lIWorkInPage = $rect_values{'IWorkInPage'} += 1;
#    my $lIWorkOutPage = $rect_values{'IWorkOutPage'} += 1;
    my $lIWorkInPage = $rect_values{'IWorkOutPage'} += 1;
    my $lIWorkOutPage = $rect_values{'IWorkInPage'} += 1;
    $lWorkInFlow = $lIWorkInPage if $lIWorkInPage > $lWorkInFlow;
    $lWorkOutFlow = $lIWorkOutPage if $lIWorkOutPage > $lWorkOutFlow;
    my $FieldName = "$lIWorkInPage/$coord";
    my (
      $BeginRow, $EndRow, $Height, $FontStyle
    ) = 
    @rect_values{qw(BeginRow EndRow Height FontStyle)}; $FontStyle =~ s/^fs//; 
    my $TotLines = $EndRow-$BeginRow+1; my $Encoding = 'WinAnsiEncoding'; my $TL = $Height/$TotLines; 
    @rect_values{qw(coord coordList FieldName TotLines FontStyle Encoding TL)} = (
      $coord, $coordList, $FieldName, $TotLines, 
      $FontStyle, $Encoding, $TL
    );
    push @{$lWorkInPages->[$lIWorkInPage]}, \%rect_values;
    push @{$lWorkOutPages->[$lIWorkOutPage]}, \%rect_values;
  }
#  i::logit("LineEditor OPEN: " .($lProjectPdfFileName || ':Scalar'));
  my $ldoc = XReport::PDF::DOC->Open($lProjectPdfFileName || (':Scalar', {}, $PDFBackgroundSource) ); my $lgeometry = [];

  my $lTotPages = $ldoc->TotPages(); 
  
  die("ProjectBackGrounds=$lProjectBackGrounds <> PdfPages=$lTotPages\n") 
   if 
  $lTotPages != $lProjectBackGrounds;

  for my $lpage (1..$lTotPages) {
    my $lg = $ldoc->getPageGeometry($lpage); $lgeometry->[$lpage] = $lg; 
    my (
      $MediaBox, $CropBox, $Rotate
    ) = 
    @{$lg}{qw(MediaBox CropBox Rotate)}; $Rotate+=360 while $Rotate<0; $Rotate = $Rotate%360; 
    my $PageHeight = $CropBox->[-1] || $MediaBox->[-1]; my $PageWidth = $CropBox->[-2] || $MediaBox->[-2];
    my $FromRef = 
      $Rotate == 0 ? $PageHeight : $Rotate == 90 || $Rotate == 270 ? $PageWidth : $Rotate == 180 ?  0 : 
      die("INVALID PAGE ROTATION DETECTED ($Rotate) Page($lpage) \"$lProjectPdfFileName\"")
    ;
    my $cm =
      $Rotate ==  90 ? [ 0,  1, -1,  0,  $PageWidth, 0] :
      $Rotate == 180 ? [-1,  0,  0, -1,  0, $PageHeight] :
      $Rotate == 270 ? [ 0, -1,  1,  0,  $PageWidth, $PageHeight] : 
      undef
    ;  
    @{$lg}{qw(Rotate PageHeight PageWidth FromRef cm)} = ($Rotate, $PageHeight, $PageWidth, $FromRef, $cm); 
  }

  my $self = {
    lProjectName => $lProjectName,
    lProjectFileName => $lProjectFileName, 
    lProjectPdfFileName => $lProjectPdfFileName, 
    lProjectBackGrounds => $lProjectBackGrounds, 
    lWorkInPages => $lWorkInPages,
    lWorkOutPages => $lWorkOutPages,
    lWorkInFlow => $lWorkInFlow,
    lWorkOutFlow => $lWorkOutFlow,
    lFlowQueue => $lFlowQueue,
    lProjectValues => {},
    ldoc => $ldoc,
    lTotPages => $lTotPages, lgeometry => $lgeometry
  };
#  print Dumper($self), "\n";
  bless $self, $class;
} 

use Data::Dumper qw(Dumper);

sub ExtractFieldValues {
  my ($self, $lPage, $lIWorkInPage) = @_; my @lineList;

  my (
    $lProjectName, $lProjectValues, $lWorkInPages
  ) = 
  @{$self}{qw(lProjectName lProjectValues lWorkInPages)};

  my $lWorkInPage = $lWorkInPages->[$lIWorkInPage];

  for my $Field (@$lWorkInPage) {
    my (
      $FieldName, $coord, $coordList, $MaskIn
    ) = 
    @{$Field}{qw(FieldName coord coordList MaskIn FieldName)}; 

    $lProjectValues->{$FieldName} = $lPage->GetPageRectList(@$coordList) || [];
  }
}

sub FlushValues {
  my $self = shift;  $self->{'lProjectValues'} = {};
}

sub FlushProject {
  my ($self, $docTo) = @_;
  my (
    $lWorkOutPages, $ldoc, $lTotPages, $lgeometry, $lProjectValues
  ) = 
  $self->getValues(qw(lWorkOutPages ldoc lTotPages lgeometry lProjectValues));

  for my $ldocFromPage (1..$lTotPages) {
    $docTo->CopyPages($ldoc, $ldocFromPage, 1), next if !defined($lWorkOutPages->[$ldocFromPage]);
    my $Fields = $lWorkOutPages->[$ldocFromPage]; 
    my (
      $Rotate, $PageHeight, $PageWidth, $FromRef, $cm
    ) = 
    @{$lgeometry->[$ldocFromPage]}{qw(Rotate PageHeight PageWidth FromRef cm)}; 
    
    my (
      $objId, $objResources, $objCatalog, $objStream
    ) = 
    ($docTo->AddObj(), "", "", "0 Tc 0 Tw 100 Tz 0 Ts\n"); 
    
    my ($Field, $FontAliases, $FontAlias, $FontAliasName, $FontMetrics, $FontAscender); $FontAliases = {};

    for $Field (@$Fields) {
      my ($coord, $coordList, $MaskOut, $lIWorkInPage) = @{$Field}{qw(coord coordList MaskOut IWorkInPage)};
      my $FieldName = "$lIWorkInPage/$coord"; 
      my (
        $FontName, $FontStyle, $FontSize, $TL, $Encoding,
        $Left, $Top, $Width, $Height, $TotLines 
      ) = 
      @{$Field}{qw(FontName FontStyle FontSize TL Encoding Left Top Width Height TotLines)};

      my ($lX_1, $lY_1, $lX_2, $lY_2) = ($Left, $FromRef-$Top, $Left+$Width, $FromRef-($Top+$Height)); 

      my $lFontName = "$FontName/$FontStyle/$Encoding"; my $FontFamily = $FontName;
      if (!exists($FontAliases->{$lFontName})) {
        $FontAliases->{$lFontName} 
         = 
        $docTo->GetFontAlias($FontName, $FontFamily, $FontStyle, $Encoding);
      }
      $FontAlias = $FontAliases->{$lFontName}; 
      (
        $FontAliasName, $FontMetrics
      ) = 
      $FontAlias->getValues(qw(FontAliasName FontMetrics)); 

      $FontAscender = $FontMetrics->getValues('Ascender')/1000;
      
      my $lY_1_Td = $lY_1 - $FontSize * $FontAscender;

      my $lFieldLines = $lProjectValues->{$FieldName}; my $lFieldLine; my @FieldLines;
       
      for $lFieldLine (@$lFieldLines) {
#	@$lFieldLine = ( grep /[^\s]/, @$lFieldLine );
	if (0 && scalar(@$lFieldLine) > 1) {
	  push @FieldLines, join(" TJ 0 0 Td\n",  map { "[() ".join(" ", map { $_ =~ s/\(/\\(/msog; $_ =~ s/\)/\\)/msog; "100 ($_)" } split(//,$_) )."]" } @$lFieldLine );
	}
	elsif (1 || scalar(@$lFieldLine) == 1) {
	  push @FieldLines, join(" Tj 0 0 Td\n", map {$_ =~ s/\(/\\(/msog; $_ =~ s/\)/\\)/msog; "($_)"} @$lFieldLine);
	}
	else {
	  push @FieldLines, "() Tj 0 0 Td\n";
	}
	  
      }
      my $FieldValue = join("\n", map {$_ = ("$_ Tj T*")} @FieldLines);

      $objStream .= join('',
        "q ",  
        ($cm ? join(" ", @$cm, 'cm ') : ''),
        "$lX_1 $lY_2 $lX_2 $lY_1 re ", 
        "W ", 
        "n ", 
        "BT ",  
        "/$FontAliasName $FontSize Tf 0 g ",
        "$lX_1 $lY_1_Td Td ", 
        "$TL TL\n", 
        "$FieldValue ",
        "ET ",  
        "Q\n"
      );
    }

    my $lFontAliases = join("", map {$_->getValues("FontAliasRef")} values(%$FontAliases));  

    $objResources = 
     "<</ProcSet [/PDF /Text /ImageB /ImageC /ImageI]/Font<<$lFontAliases>>>>"
    ;

    $objCatalog = "<</Type/XObject/Subtype/Form/BBox [0 0 32767 32767]/Resources $objResources>>";

    $docTo->AddStream([$objId, $objCatalog , $objStream], 0);

    $docTo->MergePages([$ldoc, $ldocFromPage, 1], [undef, $objId]); 
  }

  return $lTotPages;
}

sub TestFlowQueue {
  my ($self, $docTo) = @_; my ($lFlowQueue, $lWorkInFlow) = @{$self}{qw(lFlowQueue lWorkInFlow)}; 

  my @PageList = map { XReport::Project::LineEditor::FlowPage->new($_) } split("\f", $lFlowQueue);

  while (@PageList) {
    for my $lIWorkInPage (1..$lWorkInFlow) {
      my $lPage = shift @PageList;
      $self->ExtractFieldValues($lPage, $lIWorkInPage); 
    }
    $self->FlushProject($docTo);
  }

}

1;

__END__

my %cValues = (
  workdir => 'c:/xreport/work',
  userlib => 'c:/xreport/userlib',
  SrvName => 'CHRONO'
);

sub c::getValues {
  return @cValues{@_}
}

my $docTo = XReport::PDF::DOC->Create('zz.pdf');

my $lProject = XReport::Project::LineEditor->Open('INSCO31P.xml');

$lProject->TestFlowQueue($docTo);


$docTo->Close();


