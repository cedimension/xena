
package XReport::CONSTANTs;

sub RF_ASCII { 1 }
sub RF_EBCDIC { 2 }
sub RF_AFP { 3 }
sub RF_VIPP { 4 }

sub XF_LPR { 1 }
sub XF_PSF { 2 }
sub XF_FTPB { 3 }
sub XF_FTPC { 4 }

sub ST_ACCEPTED { 0 }
sub ST_RECEIVING { 1 }
sub ST_RECEIVE_ERROR { 15 }
sub ST_RECEIVED { 16 }
sub ST_QUEUED { 16 }
sub ST_INPROCESS { 17 }
sub ST_COMPLETED { 18 }
sub ST_PROCERROR { 31 }

sub NT_HOST_NOT_AVAILABLE { 1 }
sub NT_FILE_NOT_AVAILABLE { 2 }
sub NT_FILE_UNKNOWN { 3 }
sub NT_IO_ERROR { 4 }

1;
