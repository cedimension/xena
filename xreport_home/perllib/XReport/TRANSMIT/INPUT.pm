package XReport::TRANSMIT::INPUT;

use strict;

use constant INMR01 => "\xc9\xd5\xd4\xd9\xf0\xf1";
use constant INMR02 => "\xc9\xd5\xd4\xd9\xf0\xf2";
use constant INMR03 => "\xc9\xd5\xd4\xd9\xf0\xf3";
use constant INMR06 => "\xc9\xd5\xd4\xd9\xf0\xf6";

use constant INMUTILN => "\x10\x28";
use constant INMRECFM => "\x00\x49";
use constant INMDSORG => "\x00\x3c";

use constant IEBCOPY_UNLOAD_HEADER => "\x00\xca\x6d\x0f";

use Symbol;
use File::Basename;
use Convert::EBCDIC;

my $translator = new Convert::EBCDIC;

sub trim {
  my $t = $_[0];
  $t =~ s/^ +//;
  $t =~ s/ +$//;

  return $t;
}

sub deblockVB {
  my ($self, $blk, $res) = (shift, shift, '');
  my ($blklen, $blkpos, $lreclen) = (length($blk), 0, 0);
  
  if ( unpack("n", $blk) != length($blk) ) {
    die("DEBLOCK ERROR: INVALID BLOCK HEADER ");
  }
  $blkpos = 4;
  while($blkpos<$blklen) {
    $lreclen = unpack("n", substr($blk,$blkpos,2)); 
	$res .= pack("n", $lreclen-4).substr($blk,$blkpos+4,$lreclen-4);
	$blkpos += $lreclen;
  }
  
  return $res;
}

sub splitTextUnits {
  my ($self, $t) = @_; my $DEBUG = $self->{DEBUG};
  my ($tlen, $tpos) = (length($t), 0); my %TextUnits = ();
  print $DEBUG "ENTRY ", unpack("H20", $t), "\n" if $DEBUG;
  while($tpos < $tlen) {
    my ($key, $kp, $k) = (unpack("a2n", substr($t,$tpos,4)), []); my $ld;
	for ($tpos+=4; $kp>0; $kp--) {
	  $ld = unpack("n", substr($t,$tpos,2));
	  push @$k, substr($t, $tpos+2, $ld);
	  $tpos += 2+$ld;
	}
	$TextUnits{$key} = $k;
  }
  for (keys(%TextUnits)) {
    print $DEBUG "EXIT ", unpack("H4",$_), " ", join(",", map {unpack("H*", $_)} @{$TextUnits{$_}}), "\n" if $DEBUG;
   #print "EXIT ", unpack("H4",$_), " ", join(",", map {unpack("H*", $_)} @{$TextUnits{$_}}), "\n";
  }
  return \%TextUnits;
}

sub IsXmitFile {
  shift if ref($_[0]); #shift first parameter if called from a blessed ref;
  my $fileName = shift; my $INPUT = gensym(); my $buff;
  return 0 if (-s $fileName) < 8;
  open($INPUT, "<$fileName"); binmode($INPUT);
  read($INPUT,$buff,8);
  close($INPUT);
  return substr($buff,2) eq INMR01;
}

sub GetXmitLine() {
  my $self  = shift; my ($INPUT, $DEBUG, $rec) = @{$self}{'INPUT', 'DEBUG', 'rec'};

  my ($rc, $h2, $len, $flags, $buff);
  
  $$rec = '';
  while(!eof($INPUT)) {
    $rc = read($INPUT, $h2, 2);
    ($len, $flags) = unpack("CC", $h2); $len -= 2;
    die("GetXmitLine: Zero Length detected at ".tell($INPUT)."?") if $len <= 0;
    $rc = read($INPUT, $buff, $len);
 
    $$rec .= $buff;
    
    if ( ($flags & 0x40) != 0 ) {
      my $rt = $flags & 0x20; 
    
	  if ( $DEBUG ) {
        if ( $rt ) {
          print $DEBUG "<".length($$rec)."> ", unpack("H*", $$rec), "\n";  
        }
        else {
          print $DEBUG "<".length($$rec)."> ", $translator->toascii(substr($$rec,0,6)), " ", unpack("H*", $$rec), "\n"; 
        }
	  }
      return $rt;
    }
  }
}

sub IEBCOPY_reload {
  my $self = shift; my $TextUnits = $self->{TextUnits}->{IEBCOPY}; my $olibrary; my $MEMBER = gensym();
  
  my ($INPUT, $OUTDIR, $OUTSUFF, $SELECT, $do_deblock, $DEBUG, $VERBOSE, $rec) 
   = 
  @{$self}{'INPUT', 'OUTDIR', 'OUTSUFF', 'SELECT', 'do_deblock', 'DEBUG', 'VERBOSE', 'rec'};

  if ( IEBCOPY_UNLOAD_HEADER ne substr($$rec,0,4) ) {
    die "INVALID IEBCOPY UNLOAD HEADER";
  }

  for (keys(%$TextUnits)) {
    print 
	  $DEBUG "IEBCOPY ", unpack("H4",$_), " ", join(",", map {unpack("H*", $_)} @{$TextUnits->{$_}}), "\n"
	if $DEBUG;
  }
  my $recfm_ = $TextUnits->{"\x00\x49"}->[0]; my $recfm = unpack("n", $recfm_);
  my $dsorg_ = $TextUnits->{"\x00\x3c"}->[0]; my $dsorg = unpack("n", $dsorg_);
  
  #verify control information

  my $olibrary = "$OUTDIR/".$translator->toascii($TextUnits->{"\x00\x02"}->[-1]);
  mkdir($olibrary); die("MKDIR ERROR \"$olibrary\" $!") if !-d $olibrary;
    
  my ($devtab, $hdrs) = unpack("x16a20n", $$rec);
  my (@Extents, %Members, %Aliases);
###mpezzi 
### da manuale: se hdrs = 0 default = 2

  $hdrs = 2 if $hdrs == 0;
  print $DEBUG unpack("H*", $devtab), "\n" if $DEBUG;
  while ( $hdrs > 1 ) {
    $self->GetXmitLine();
    my @extents = unpack("H32".("a16"x16), $$rec); shift @extents;
    for my $extent (@extents) {
      last if $extent eq "\x00"x16;
      my ($fc, $ft, $tc, $tt, $tott) = unpack("x6nnnnn", $extent);
      print $DEBUG unpack("H32", $extent), "\n" if $DEBUG;
      print $DEBUG join(", ", ($fc, $ft, $tc, $tt, $tott)), "\n" if $DEBUG;
      push @Extents, [$fc*15+$ft, $tc*15+$tt, $tott];
    }
    $hdrs--;
  }

  READDIR:
  while(1) {
    $self->GetXmitLine();
    if ( "000000000000000000080100" ne unpack("H24",$$rec) ) {
      die "INVALID DIRECTORY BLOCK";
    }
	while (length($$rec)>12) {
      my $dirblk = substr($$rec, 22, unpack("x20n", $$rec)-2);
      while ( $dirblk ) {
        my ($name, $tt, $r, $c, $cx) = unpack("a8nC2XH2", $dirblk);
		#my $el = 12 + (($c>>4)&0x07)*3 + ($c&0x0f)*2;
		my $el = 12 + ($c&0x1f)*2;
        last if $name eq "\xff"x8;
        $name = trim($translator->toascii($name));
        my $ttr = "$tt/$r";
        print $DEBUG "$name, $ttr, $c, $cx, $el\n" if $DEBUG;
		if ( ($c & 0x80) == 0 ) {
          $Members{$ttr} = $name;
		}
		else {
          $Aliases{$ttr} = $name;
		}
		$dirblk=substr($dirblk, $el);
      }
	  $$rec = substr($$rec, 276);
	}
    last READDIR if length($$rec) == 12;
  }

  READMEMBER:
  while( !eof($INPUT) ) {
    close($MEMBER); my $atrec=0;
    while(1) {
      last READMEMBER if $self->GetXmitLine() != 0;
      while ($$rec) {
        my ($f, $mbb, $ac, $at, $ar, $kl, $dl, $tt, $ttr) = unpack("H2H6nnCCn", $$rec);
        my ($ttrec, $ttext) = ($ac*15+$at, 0);
        for my $aref (@Extents) {
          if ( $ttrec>=$aref->[0] and $ttrec<=$aref->[1] ) {
            $tt = $ttext + $ttrec - $aref->[0];
            $ttr = "$tt/$ar";
            if ( $atrec == 0 ) {
              if ( exists($Members{$ttr}) ) {
                my $Member = $Members{$ttr}; my $fileName = "$olibrary/$Member";
				$fileName .= ".$OUTSUFF" if $OUTSUFF;
				if ( !$SELECT or $Member =~ /$SELECT/o ) {
                  print "MEMBER $ttr $Member SELECTED\n" if $VERBOSE;
                  print $DEBUG "MEMBER $ttr $Member FOUND\n" if $DEBUG;
                  open($MEMBER, ">$fileName") 
				    or 
				  die("MEMBER $Member OPEN ERROR \"$fileName\" $!"); binmode($MEMBER);
				}
				else {
                  print "MEMBER $ttr $Member NOT SELECTED\n" if $VERBOSE;
				}
              }
              else {
                die "MEMBER NOT FOUND for TTR [$ttr]\n", 
              }
            }
          }
		  $ttext += $aref->[2];
        }
        print $DEBUG "$f, $mbb, $ac, $at, $ar, $tt, $kl, $dl, $ttr\n" if $DEBUG;
		if ( $dl > 0 ) {
		  #test if to do deblock
		  if ($do_deblock) {
		    if ( ($recfm & 0x5000) == 0x5000 ) { # VariableBlocked
			  print $MEMBER $self->deblockVB(substr($$rec,12,$dl));
			}
			elsif ( ($recfm & 0xC000) == 0xC000 ) { # Undefined
			  print $MEMBER pack("n",$dl), substr($$rec,12,$dl);
			}
			else {
			  die "RECFM NOT MANAGED ", unpack("H*", $recfm_);
			}
		  }
		  else {
		    print $MEMBER substr($$rec,12,$dl);
		  }
          $$rec = substr($$rec,12+$dl);
		  $atrec+=1; 
		}
		else {
		  close($MEMBER); next READMEMBER;
		}
      }
    }
  }
}

sub EXPAND_LIBRARY {
  my $self = shift; $self->{TextUnits} = {}; my %args = @_; my $rec = $self->{rec};

  my ($INPUT, $OUTDIR, $OUTSUFF, $do_deblock, $DEBUG, $VERBOSE) 
   = 
  @args{'INPUT', 'OUTDIR', 'OUTSUFF', 'DEBLOCK', 'DEBUG', 'VERBOSE'}; mkdir($OUTDIR);

  if (!ref($INPUT)) {
    my $fileName = $INPUT;
    $INPUT = gensym(); 
    open($INPUT, "<$fileName") or die("XMIT INPUT Open ERROR $fileName $!"); binmode($INPUT);
  }
  
  if ($DEBUG and !ref($DEBUG)) {
    my $fileName = $DEBUG;
    $DEBUG = gensym();
    open($DEBUG, ">$fileName") or die("XMIT DEBUG Open ERROR $fileName $!"); binmode($DEBUG);
  }

  @{$self}{'INPUT', 'OUTDIR', 'OUTSUFF', 'do_deblock', 'DEBUG'} 
   = 
  ($INPUT, $OUTDIR, $OUTSUFF, $do_deblock, $DEBUG);
  
  while(1) {
    seek($INPUT, ((80-tell($INPUT)%80)%80), 1); last if eof($INPUT);
	
    my $TextUnits = $self->{TextUnits} = {};
	
    while ($self->GetXmitLine() != 0 ) {
      if ( INMR01 eq substr($$rec,0,6) ) {
  	    print $DEBUG "Text Units for INMR01\n" if $DEBUG;
        my $TextUnits_ = $self->splitTextUnits(substr($$rec,6));
  	    $TextUnits->{INMR01} = $TextUnits_;
  	  }
      if ( INMR02 eq substr($$rec,0,6) ) {
  	    print $DEBUG "Text Units for INMR02\n" if $DEBUG;
  	    print "Text Units for INMR02\n" if $DEBUG;
        my $TextUnits_ = $self->splitTextUnits(substr($$rec,10));
  	    my $INMUTILN = $translator->toascii($TextUnits_->{"\x10\x28"}->[0]);
  	    $TextUnits->{$INMUTILN} = $TextUnits_;
  	    print $DEBUG "END Text Units <", $INMUTILN, ">\n" if $DEBUG;
  	  }
    }
	
    if ( !exists($TextUnits->{INMR01}) ) {
      die "FIRST RECORD NOT INMR01.";
	}
    elsif ( !exists($TextUnits->{IEBCOPY}) ) {
      die "IEBCOPY UTIL MISSING. OTHER FORMATS NOT MANAGED.";
	}

    $self->IEBCOPY_reload();
  
    if ( INMR06 ne $$rec ) {
      die "FINAL RECORD NOT INMR06.";
    }
  }
  
  close($INPUT);
  close($DEBUG) if $DEBUG;
}

sub new {
  my $className=shift; my $self = {}; my $rec = "";
  
  $self->{rec} = \$rec;
  
  bless $self;
}

1;
