package XReport::TRANSMIT::OUTPUT;

use strict;

use Symbol;
use Convert::IBM390 qw(:all);

use constant INMBLKSZ => 0x0030; #Block size
use constant INMCREAT => 0x1022; #Creation date
use constant INMDDNAM => 0x0001; #DDNAME for the file
use constant INMDIR   => 0x000C; #Number of directory blocks
use constant INMDSNAM => 0x0002; #Name of the file
use constant INMDSORG => 0x003C; #File organization
use constant INMERRCD => 0x1027; #RECEIVE command error code
use constant INMEXPDT => 0x0022; #Expiration date
use constant INMFACK  => 0x1026; #Originator requested notification
use constant INMFFM   => 0x102D; #Filemode number
use constant INMFNODE => 0x1011; #Origin node name or node number
use constant INMFTIME => 0x1024; #Origin timestamp
use constant INMFUID  => 0x1012; #Origin user ID
use constant INMFVERS => 0x1023; #Origin version number of the data format
use constant INMLCHG  => 0x1021; #Date last changed
use constant INMLRECL => 0x0042; #Logical record length
use constant INMLREF  => 0x1020; #Date last referenced
use constant INMMEMBR => 0x0003; #Member name list
use constant INMNUMF  => 0x102F; #Number of files transmitted
use constant INMRECCT => 0x102A; #Transmitted record count
use constant INMRECFM => 0x0049; #Record format
use constant INMSIZE  => 0x102C; #File size in bytes
use constant INMTERM  => 0x0028; #Data transmitted as a message
use constant INMTNODE => 0x1001; #Target node name or node number
use constant INMTTIME => 0x1025; #Destination timestamp
use constant INMTUID  => 0x1002; #Target user ID
use constant INMUSERP => 0x1029; #User parameter string
use constant INMUTILN => 0x1028; #Name of utility program

sub CreTextUnits {
  my $t = '';
  
  while(@_) {
    my ($k, $v) = (shift, shift);
	$t .= packeb("s", $k);
	
    if( $k == INMBLKSZ ) {   #Block size;
	  $t .= packeb("sss", 1, 2, $v);
	}
    elsif( $k == INMCREAT ) {   #Creation date;
	  $t .= packeb("sse*", 1, length($v), $v);
	}
    elsif( $k == INMDDNAM ) {   #DDNAME for the file;
	  $t .= packeb("sse*", 1, length($v), $v);
	}
    elsif( $k == INMDIR   ) {   #Number of directory blocks;
	  $t .= packeb("sss", 1, 2, $v);
	}
    elsif( $k == INMDSNAM ) {   #Name of the file;
	  my @v = split(/\./, $v);
	  $t .= packeb("s", scalar(@v));
	  for (@v) {
	    $t .= packeb("se*", length($_), $_);
	  }
	}
    elsif( $k == INMDSORG ) {   #File organization;
	  $t .= packeb("sss", 1, 2, $v);
	}
    elsif( $k == INMERRCD ) {   #RECEIVE command error code;
	}
    elsif( $k == INMEXPDT ) {   #Expiration date;
	}
    elsif( $k == INMFACK  ) {   #Originator requested notification;
	}
    elsif( $k == INMFFM   ) {   #Filemode number;
	}
    elsif( $k == INMFNODE ) {   #Origin node name or node number;
	  $t .= packeb("sse*", 1, length($v), $v);
	}
    elsif( $k == INMFTIME ) {   #Origin timestamp;
	  $t .= packeb("sse*", 1, length($v), $v);
	}
    elsif( $k == INMFUID  ) {   #Origin user ID;
	  $t .= packeb("sse*", 1, length($v), $v);
	}
    elsif( $k == INMFVERS ) {   #Origin version number of the data format;
	}
    elsif( $k == INMLCHG  ) {   #Date last changed;
	}
    elsif( $k == INMLRECL ) {   #Logical record length;
	  $t .= packeb("sss", 1, 2, $v);
	}
    elsif( $k == INMLREF  ) {   #Date last referenced;
	}
    elsif( $k == INMMEMBR ) {   #Member name list;
	}
    elsif( $k == INMNUMF  ) {   #Number of files transmitted;
	  $t .= packeb("sss", 1, 2, $v);
	}
    elsif( $k == INMRECCT ) {   #Transmitted record count;
	}
    elsif( $k == INMRECFM ) {   #Record format;
	  $t .= packeb("sss", 1, 2, $v);
	}
    elsif( $k == INMSIZE  ) {   #File size in bytes;
	  $t .= packeb("ssi", 1, 2, $v);
	}
    elsif( $k == INMTERM  ) {   #Data transmitted as a message;
	}
    elsif( $k == INMTNODE ) {   #Target node name or node number;
	  $t .= packeb("sse*", 1, length($v), $v);
	}
    elsif( $k == INMTTIME ) {   #Destination timestamp;
	  $t .= packeb("sse*", 1, length($v), $v);
	}
    elsif( $k == INMTUID  ) {   #Target user ID;
	  $t .= packeb("sse*", 1, length($v), $v);
	}
    elsif( $k == INMUSERP ) {   #User parameter string;
	  $t .= packeb("sse*", 1, length($v), $v);
	}
    elsif( $k == INMUTILN ) {   #Name of utility program;
	  $t .= packeb("sse*", 1, length($v), $v);
	}
    else {
	  die "INVALID TEXT UNIT KEY \"$k\"\n";
	}
  }

  return $t;
}

sub WriteRecord {
  my $self = shift; my $OUTPUT = $self->{OUTPUT};
  my $control = shift; $control = 32 if $control; 
  while(@_) {
    my $rec = shift; my ($at, $atmax, $l) = (0, length($rec), length($rec));
    my ($first , $last) = (128, 0);
	while($l>0) {
	  if ( $l>253 ) {
	    $l = 253;
	  }
	  else {
	    $last = 64;
	  }
	  print $OUTPUT pack("CC", $l+2, $first+$last+$control), substr($rec, $at, $l);
	  $at+=$l; $l=$atmax-$at; $first=0;
	}
  }
}

sub WriteControl {
  my ($self, $t) = (shift, shift);
  
  if ( $t =~ /INMR01|INMR03|INMR06/ ) {
    $t = packeb("e*", $t).CreTextUnits(@_);
  }
  elsif ( $t eq "INMR02" ) {
    my $f = shift;
    $t = packeb("e*i", $t, $f).CreTextUnits(@_);
  }
  else {
    die "INVALID TRANSMIT CONTROL RECORD $t";
  }
  $self->WriteRecord(1, $t);
}

sub Write {
  my $self = shift;
  $self->WriteRecord(0, @_);
}

sub Close {
  my $self = shift; my $OUTPUT = $self->{OUTPUT};
  $self->WriteControl("INMR06");
  close($OUTPUT);
}

sub Create {
  my $className = shift; my %args = @_; my $OUTPUT = gensym();

  my ($TO_NODE, $TO_UID, $FROM_NODE, $FROM_UID, $FROM_TIME, $FROM_DSNAME, $toFileName, $fromFileNameList) 
    =
  @args{qw(TO_NODE TO_UID FROM_NODE FROM_UID FROM_TIME FROM_DSNAME toFileName fromFileNameList)};
  
  open($OUTPUT, ">$toFileName") 
   or die("OPEN OUTPUT ERROR \"$toFileName\" $!"); binmode($OUTPUT);
  
  my $self = {OUTPUT => $OUTPUT}; bless $self, $className;

  $self->WriteControl(
    "INMR01", 
      INMNUMF ,  1,  
	  INMTNODE,  $TO_NODE, 
	  INMTUID,   $TO_UID, 
	  INMFNODE,  $FROM_NODE,
	  INMFUID,   $FROM_UID, 
	  INMLRECL,  80,
	  INMFTIME,  $FROM_TIME
  );

  $self->WriteControl(
    "INMR02", 1,
      INMBLKSZ,  32760, 
	  INMUTILN, "INMCOPY", 
	  INMRECFM,  0x5002, 
	  INMLRECL,  32756, 
	  INMDSORG,  0x4000,
	 #INMSIZE,   $FILE_SIZE, 
	  INMDSNAM,  $FROM_DSNAME
  );

  $self->WriteControl(
    "INMR03",
	  INMRECFM, 0x5002,
	  INMLRECL, 32756, 
	  INMDSORG, 0x4000
  );

  return $self if !$fromFileNameList;
  
  for my $fromFileName (@$fromFileNameList) {
    my $INPUT = gensym();
	open($INPUT, "<$fromFileName") 
     or die("OPEN INPUT ERROR \"$fromFileName\" $!"); binmode($INPUT);
	while(!eof($INPUT)) {
      my ($l, $buf);
      read($INPUT, $l,2); 
	  $l = unpack("n", $l);
      read($INPUT,$buf, $l);
      $self->Write($buf);
    }
	close($INPUT); 
  }
  $self->Close();

  return 1;
}

1;
