
package XReport::WorkQueue;

use strict qw(vars);

use Carp;

use XReport::ENUMS qw(:ST_ENUMS); 
use XReport::DBUtil; use XReport::Util;

use constant FULL_MODE => 1;
use constant DEBUG => 1;

my %ClassFactory = (
  tbl_JobReports => 'XReport::JobREPORT',
  tbl_ctd_INBUNDLES => 'XReport::CTD::Filter'
);

sub GetClassFactory {
  return $ClassFactory{shift()};
}

sub AND_VAR_COND {
  my ($var, $AND_VAR_COND) = (shift, shift);

  if ( $AND_VAR_COND and $AND_VAR_COND ne "*" ) {
    if ($AND_VAR_COND !~ /^NULL,/ ) {
      $AND_VAR_COND = "and $var IN($AND_VAR_COND)"; 
	}
	else {
	  $AND_VAR_COND =~ s/^NULL,//;
      $AND_VAR_COND = "and ($var IS NULL or $var IN($AND_VAR_COND))"; 
	}
  }
  elsif ( $AND_VAR_COND eq "*" ) {
    $AND_VAR_COND = "";
  }
  else {
    $AND_VAR_COND = "and $var IS NULL";
  }

  return " $AND_VAR_COND ";
}

sub whereList {
  return join(" or\n", 
    map {
      my ($TableName, $TypeOfWork, $WorkClass) = @$_;

      "(".
       "ExternalTableName = '$TableName' ". 
        AND_VAR_COND('TypeOfWork', $TypeOfWork). 
        AND_VAR_COND('WorkClass', $WorkClass). 
      ")";

    }
    @{shift()}
  );
}

sub dequeue {
  my ($self, $INTERVAL) = (shift, shift); my $selectQUERY = $self->{selectQUERY}; my $dbr;
  
  GET_WORKITEM_LOOP: while( 1 ) {
	
    eval {
      GetLock('LK_INPUTQUEUE');
	
      last GET_WORKITEM_LOOP
       if 
      !($dbr = dbExecute($selectQUERY))->eof();
	
      ReleaseLock('LK_INPUTQUEUE'); 
	};

	die $@ if ($@ and $@ !~ /User *Error *:/i);
	
	sleep $INTERVAL;
  }
  
  @{$self}{qw(WorkId ExternalTableName ExternalKey TypeOfWork)}
    = 
  $dbr->GetFieldsValues(qw(WorkId ExternalTableName ExternalKey TypeOfWork)); $dbr->Close();


  my ($SrvName, $WorkId) = @{$self}{qw(SrvName WorkId)};

  dbExecute(
   "UPDATE tbl_WorkQueue Set SrvName = '$SrvName', Status = ". ST_INPROCESS ." WHERE WorkId = $WorkId "
  );

  ReleaseLock('LK_INPUTQUEUE'); 

  return @{$self}{qw(WorkId ExternalTableName ExternalKey TypeOfWork)};
}


sub getWorkItem {
  my $self = shift; my ($WorkId, $ExternalTableName, $ExternalKey, $TypeOfWork, $ExternalWorkItem);
  
  while(1) {
    ($WorkId, $ExternalTableName, $ExternalKey, $TypeOfWork) = $self->dequeue(3); 

    eval { 
      my $ClassFactory = GetClassFactory($ExternalTableName); 
      eval(
        "require $ClassFactory"
      );
      $ExternalWorkItem = $ClassFactory->Open($ExternalKey, FULL_MODE); 
    };

    if ( my $EndingMessage = $@ ) {
      &$logrRtn("ERROR CATCHED : $EndingMessage");
      $self->suspendWorkItem(31); sleep 3;

#      if ( $EndingMessage =~ /User *Error|Restartable/i ) {
#        &$logrRtn("RECOVERABLE ERROR DETECTED");
#      }
#      else {
#	    croak("UNRECOVERABLE ERROR: TERMINATING NOW !");
#      }
#
#	  $self->delWorkItem(); sleep 3;
    }
    else {
      return $self->{ExternalWorkItem} = $ExternalWorkItem;
    }
  }
  
}

sub delWorkItem {
  my $self = shift; my $WorkId = $self->{WorkId};

  dbExecute("DELETE from tbl_WorkQueue WHERE WorkId = $WorkId");

  @{$self}{qw(WorkId ExternalTableName ExternalKey TypeOfWork)} = (); 
}

sub suspendWorkItem {
  my $self = shift; my $WorkId = $self->{WorkId};

  dbExecute("UPDATE tbl_WorkQueue set SrvName = 'SUSPENDED' WHERE WorkId = $WorkId");

  @{$self}{qw(WorkId ExternalTableName ExternalKey TypeOfWork)} = (); 
}

sub new {
  my $className = shift; my ($SrvName, $selectQUERY, @selectList) = (getConfValues('SrvName'), '');

  my $GlobalWorkQueues = getConfValues('GlobalWorkQueues')->{WorkQueue};

  my $WorkQueues = $GlobalWorkQueues->{getConfValues('WorkQueues')}->{tbl};

  for (keys(%$WorkQueues)) {
    push @selectList, [$_, @{$WorkQueues->{$_}}{qw(type class)}];
  }

  $selectQUERY = 
   "SELECT * from tbl_WorkQueue 

    WHERE 
    ( 
      ".whereList(\@selectList)."
    )
    AND (
      SrvName='$SrvName' OR (SrvName IS NULL AND Status = ". ST_QUEUED .") 
    ) 

    ORDER BY Priority DESC, WorkId"
  ;
  DEBUG and &$logrRtn("SELECTING JOB WITH: $selectQUERY");

  my $self = { SrvName => $SrvName, selectQUERY => $selectQUERY };

  bless $self, $className;
}

1;
