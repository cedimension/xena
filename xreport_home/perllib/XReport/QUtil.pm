#######################################################################
# @(#) $Id: QUtil.pm,v 1.2 2002/10/23 22:50:18 Administrator Exp $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
package XReport::QUtil;

use strict;
use Carp;

use XReport;
use XReport::DBUtil;

use vars qw($VERSION @ISA @EXPORT_OK);
our ($VERSION, @ISA, @EXPORT, @EXPORT_OK);

require Exporter;

@ISA = qw(Exporter);

@EXPORT = qw(
	     $tblName $tblPkey @tblFlds
	     &RDelete 
	    );
@EXPORT_OK = qw( &ATTACH &QCreate &QUpdate &RQuery &RUpdate &QQuery );
$VERSION = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r }; 

my $tblName = "tbl_JobReports";
my $tblPkey = "JobReportId";

my @tblFlds = ( "JobReportName"
	       , "LocalFileName", "SrvName", "XferRecipient" 
	       , "JobName", "JobNumber", "XferStartTime", "TarFileNumberIN"
	       , "RemoteHostAddr", "RemoteFileName", "JobExecutionTime", "XferEndTime"
	       , "XferMode", "XferInBytes", "XferInLines", "XferInPages", "XferDaemon"
	       , "XferId", "ElabStartTime", "ElabEndTime", "InputLines", "XferOutBytes"
	       , "InputPages", "ParsedLines", "ParsedPages", "DiscardedLines"
	       , "DiscardedPages", "Status", "UserTimeRef"
	       , "UserTimeElab", "UserReportRef", "UserReportTitle", "UserRef"
	       , "LocalPathId_IN", "LocalPathId_OUT", "HoldDays", "HasIndexes", "OrigJobReportName"
	       , "XferDestArea"
	      );

sub SQL {
  my $que = shift;
  my @SQL = ( @_ );
#  print "$que (".ref($que).") ".ref($que->{'db'})."------- @SQL\n";
#  i::logit("selecting rows for SQL: $SQL[0]");# unless ($SQL[0] =~ /^SELECT/i);
  my $db = $que->{'db'};
  my $r = $db->_SQL(@SQL);
  return undef unless ($SQL[0] =~ /^SELECT/i);
  my ($selflds) = $SQL[0] =~ /^SELECT\s([\s\w,\*]+)\sFROM.*/i;
  if ($SQL[0] =~ /IDENT_CURRENT|\@\@IDENTITY/) {
    ($selflds) = ('Id');
  }
  my @reqflds = ($selflds eq '*' ? (@tblFlds) : split(/[\s,]+/, $selflds));
  if (wantarray) {
    my $result = [];
    while (!$r->eof()) {
      my $row = {};
      foreach my $coln (@reqflds) {
	next unless grep $coln, (@reqflds);
	my $varn = ($coln eq $tblPkey ? "Id" : $coln);
#	print "Select requested @reqflds\n";
	next if !$r->Fields->Item($coln);
	$row->{$varn} = $r->Fields->Item($coln)->Value();
#	print "retrieved array $coln as $varn ($row->{$varn})\n";
      }
      push @{$result}, (%{$row});
      $r->MoveNext();
    }
    $r->Close();
#    i::logit("returning row array row 1:" . join('::', %{$result->[0]}));
#    print "returning array" . @{$result} ."\n";
    return $result;
  } else {
    my $row = {}; return if $r->eof();
    foreach my $coln (@reqflds) {
#      next unless grep $coln, (@reqflds);
      my $varn = ($coln eq $tblPkey ? "Id" : $coln);
#      print "Select requested $#reqflds @reqflds\n";
      $row->{$varn} = $r->Fields->Item($coln)->Value();
#      print "retrieved array $coln as $varn ($row->{$varn})\n";
    }
    $r->Close();
#    i::logit("returning row " . join('::', %{$row}));
#    print "returning row values ", values %{$row}, " keys ", keys %{$row}, "\n";
    return $row ;
  }
  return undef;
}

sub ATTACH {
#  print join('=', @_), "\n";
  my $class = shift;
  my $prms = my $que = { @_ };
#  print join('=', %{ $prms }), "\n";

  $prms->{'db'} = new XReport::DBUtil( @_ ) unless defined($prms->{'db'});
  $que->{'db'} = $prms->{'db'};

  bless $que, $class;

  return $que;
}

sub QCreate {

  # db       => dbc
  # colname  => colvalue
  # Id       => JobReportId

  my $que = shift;
  my $prm = { @_ };
#  print join(',', @_), "\n";
  croak "No Server name specified" unless $prm->{'SrvName'};

  $que = ATTACH($que, @_ ) unless ($que ne 'XReport::QUtil');


  my $db = $que->{'db'};

  $que->{'Status'} = $CD::stAccepted unless $que->{'Status'};

  my $dbr;

  my ($fldnames, $fldvals, $sep) = ('', '', '');
  foreach my $coln (@tblFlds) {
    next unless $que->{$coln};
    $fldnames .= $sep . $coln;
    my $qt = (($que->{$coln} =~ /^(?:\d+|(?:\w+\(\)))$/) ? "" : "'");
    (my $val = $que->{$coln}) =~ s/'/''/g;
    $fldvals .= $sep . $qt . $val . $qt;
    $sep = ", ";
  }


  $main::debug && i::logit("CREATING NEW ENTRY into $tblName");
  $que->SQL("BEGIN TRANSACTION");

  $que->SQL("INSERT INTO $tblName (" . $fldnames . ") VALUES ( " . $fldvals . ")");

  $dbr = $que->SQL(
  		   "SELECT \@\@IDENTITY AS Id"
		  );
  #$dbr = $que->SQL(
#		   "SELECT $tblPkey from $tblName (NOLOCK) " .
#		   "WHERE " .
#		   "JobReportName = '".$que->{'JobReportName'}."' AND LocalFileName = '".$que->{'LocalFileName'}."'"
#		  );
#  print "Select result values = ", values %{$dbr}, " keys ", keys %{$dbr}, "\n";
  $que->{'Id'} = $dbr->{'Id'};
#  print "This ID is $que->{'Id'}\n";
  
  my ($TypeOfWork, $WorkClass, $Priority, $SrvParameters);
  
  $dbr = $que->SQL(
    "SELECT TypeOfWork, Priority, WorkClass, HoldDays from tbl_JobReportNames "
   ."WHERE JobReportName = '".$que->{'JobReportName'}."'"
  );
  $dbr = $XReport::cfg->{'daemon'}->{$que->{'SrvName'}} if !$dbr;
 
#  i::logit("Retrieved attributes for $que->{JobReportName} - que " . join('::', %$que));
#  i::logit("Retrieved attributes for $que->{JobReportName} - dbr " . join('::', %$dbr));

  $TypeOfWork = $prm->{'TypeOfWork'} || $dbr->{'TypeOfWork'} || '1';
  $Priority = $dbr->{'Priority'} || 0;
  $WorkClass = $dbr->{'WorkClass'} || 'NULL';
  $que->{'HoldDays'} = $dbr->{'HoldDays'} || 30;
  $SrvParameters = $prm->{'SrvParameters'} || '';
  my ($TargetLocalPathId_IN, $TargetLocalPathId_OUT) = map { $dbr->{$_} || '' } qw(TargetLOcalPathId_IN TargetLOcalPathId_OUT);
  
  $dbr = $que->SQL(
		   "INSERT INTO tbl_WorkQueue " .
		   "( " .
		   "TypeOfWork, WorkClass, Priority, InsertTime, " .
		   "Status, ExternalKey, ExternalTableName, SrvParameters " .
		   ") " .
		   "VALUES " .
		   "( " .
		   "'$TypeOfWork', $WorkClass, $Priority, GETDATE(), " .
		   $que->{'Status'} . ", " . 
		   "'" . $que->{'Id'} . "', " . 
		   "'" . $tblName . "', " .
		   "'$SrvParameters'".
		   ")"
		  );
#  i::logit("WQ Created for $tblName $que->{Id} - Status: $que->{Status} TypeOfWork: $TypeOfWork WorkClass: $WorkClass");

  $que->SQL("COMMIT TRANSACTION");
  i::logit("WQ Created for $tblName $que->{Id} - Status: $que->{Status} TypeOfWork: $TypeOfWork WorkClass: $WorkClass") 
#   if $main::debug
   ;

  #END TRANSACTION
  return wantarray ? ($que->{Id}, $TargetLocalPathId_IN, $TargetLocalPathId_OUT, $que) : $que->{'Id'};

}

sub RQuery {

  # db     => dbc
  # colname  => colvalue
  # Id => JobReportId

  my $que = shift;
  $que = ATTACH($que, @_ ) unless ($que ne 'Xreport::RptQue');

  my $db = $que->{'db'};

  my $dbr;
  my $wc = '';
  foreach my $varn ( keys %{ $que } ) {
    $varn = $tblPkey if ($varn eq "Id");
    next unless grep $varn, ($tblPkey, @tblFlds);
    $wc .= "AND $varn = '" . $que->{$varn} . "' ";
  }

  $dbr = $que->SQL(
		  "SELECT " .
		  join(", ", ($tblPkey, @tblFlds)) .
		  "from $tblName " .
		  "WHERE " . $wc .
		  "$tblPkey = '".$que->{'Id'}."'"
		  );
  my %r = ();
  foreach my $coln (@tblFlds) {
    my $varn = ($coln eq $tblPkey ? "Id" : $coln);
    $r{$varn} = $dbr->{$coln};
  }
  return %r;
}

sub QQuery {

  # db     => dbc
  # colname  => colvalue
  # Id => JobReportId

  my $que = shift;
  $que = ATTACH($que, @_ ) unless ($que ne 'Xreport::RptQue');

  my $db = $que->{'db'};

  my $dbr;
  my $wc = '';
  foreach my $varn ( keys %{ $que } ) {
    $varn = $tblPkey if ($varn eq "Id");
    next unless grep $varn, ($tblPkey, @tblFlds);
    $wc .= "AND $varn = '" . $que->{$varn} . "' ";
  }

  $dbr = $que->SQL(
		  "SELECT " .
		  join(", ", ($tblPkey, @tblFlds)) .
		  "from $tblName " .
		  "WHERE " . $wc .
		  "$tblPkey = '".$que->{'Id'}."'"
		  );
  my %r = ();
  foreach my $coln (@tblFlds) {
    my $varn = ($coln eq $tblPkey ? "Id" : $coln);
    $r{$varn} = $dbr->{$coln};
  }
  return %r;
}

sub RUpdate {

  # db     => dbc
  # colname  => colvalue
  # Id => JobReportId

  my $que = shift;
#  print join(',', @_), "\n";
  $que = ATTACH($que, @_ ) unless ($que ne 'XReport::QUtil');

  my $db = $que->{'db'};

  my $dbr;

  my ($setstmt, $sep) = ('', '', '');
  foreach my $coln (@tblFlds) {
    next unless $que->{$coln};
    if ($coln eq $tblPkey) {
      $que->{'Id'} = $que->{$coln};
      next;
    }
    my $qt = (($que->{$coln} =~ /^(?:\d+|(?:\w+\(\)))$/) ? "" : "'");
    $setstmt .= $sep . $coln . " = " . $qt . $que->{$coln} . $qt; 
    $sep = ", ";
  }

  $dbr = $que->SQL( 
		  "UPDATE $tblName SET ". $setstmt .
		  " WHERE $tblPkey = " . $que->{'Id'}
		 );

  return $que;

}

sub QUpdate {

  # db     => dbc
  # colname  => colvalue
  # Id => JobReportId

  my $que = shift;
#  print join(',', @_), "\n";
  $que = ATTACH($que, @_ ) unless ($que ne 'XReport::QUtil');

  my $db = $que->{'db'};

  my $dbr;

  my ($setstmt, $sep) = ('', '', '');
  foreach my $coln (@tblFlds) {
    next unless $que->{$coln};
    if ($coln eq $tblPkey) {
      $que->{'Id'} = $que->{$coln};
      next;
    }
    
    my $qt = (($que->{$coln} =~ /^(?:\d+|(?:\w+\(\)))$/) ? "" : "'");
    (my $val = $que->{$coln}) =~ s/'/''/g;
    $setstmt .= $sep . $coln . " = " . $qt . $val . $qt; 
    $sep = ", ";
  }

  $que->SQL("BEGIN TRANSACTION");

  $dbr = $que->SQL( 
		  "UPDATE $tblName SET ". $setstmt .
		  " WHERE $tblPkey = " . $que->{'Id'}
		 );

  $dbr = $que->SQL( 
		   "UPDATE tbl_WorkQueue SET Status = " . $que->{'Status'} .
		   (exists($que->{TypeOfWork}) && $que->{TypeOfWork} ? ", TypeOfWork = " . $que->{TypeOfWork} : '' ) . 
		   (exists($que->{WorkClass}) && $que->{WorkClass} ? ", WorkClass = " . $que->{WorkClass} : '' ) . 
		   " WHERE ExternalKey = " . $que->{'Id'} .
		   " AND ExternalTableName = '" . $tblName . "'"
		 );
#  i::logit("WQ Updated - Status: $que->{Status}" .
#	   (exists($que->{TypeOfWork}) && $que->{TypeOfWork} ? " TypeOfWork: " . $que->{TypeOfWork} : '' ) . 
#	   "");

  $que->SQL("COMMIT TRANSACTION");

  return $que;

}

sub RDelete {
  my $que = shift;
  my ($JobReportName, $JobReportId) = @_ ;

  my $row = $que->SQL(
	     "SELECT * from tbl_JobReports ".
	     "where JobReportName = '$JobReportName' and JobReportId = $JobReportId"
	    );

  croak "Report $JobReportName $JobReportId NOT FOUND !?" unless $row;

  my $LocalFileName = $row->{"LocalFileName"};
  
#  &$logrRtn("DELETING REPORT LINKS $JobReportId $LocalFileName");
  
  $que->SQL("BEGIN TRANSACTION");

  $que->SQL("DELETE from tbl_SubReports " .
	   "WHERE JobReportName = '$JobReportName' AND JobReportId = $JobReportId");

  $que->SQL("DELETE from tbl_FoldersSubReports " .
	   "WHERE JobReportId = $JobReportId");

  $que->SQL("DELETE tbl_JobReports WHERE JobReportId = $JobReportId");

  $que->SQL("COMMIT TRANSACTION");

}

1;
