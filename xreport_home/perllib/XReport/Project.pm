
package XReport::Project;

use strict;

my $ProjectTypeClasses = {
  LineEditor => "XReport::Project::LineEditor", FormEditor => "XReport::Project::FormEditor"
};

sub Open {
  my ($class, $ProjectName, %args) = (shift, shift, @_); my ($userlib, $workdir, $SrvName) = c::getValues(qw(userlib workdir SrvName));
  
  my ($ProjectFileName, $lProjectFileName, $lProjectPdfFileName, $lxml, $ProjectType, $ProjectClass);
  $ProjectType = $args{jr}->getValues('ProjectClass');

  my $lworkdir = "$workdir/$SrvName";
  my $timeref = $args{jr}->getValues('XferStartTime');
  my $resh = new XReport::Resources( destdir => "$lworkdir/localres", TimeRef => $timeref );
  my %callargs = ();  
  if ($ProjectType eq 'LineEditor') {
    my ($INPUT, $OUTPUT) = (gensym(), gensym()); 
    
    $resh->getResources(ResName => $ProjectName, ResClass => 'prj') || die "Unable to get resource $ProjectName";

    $ProjectFileName = "$lworkdir/localres/$ProjectName.prj"; 
    open($INPUT, "<$ProjectFileName")
      or 
      die("INPUT OPEN ERROR for \"$ProjectFileName\" $!"); binmode($INPUT); 
  
    $INPUT->read($lxml, 1024); $INPUT->close(); ($ProjectType) = $lxml =~ /^\s*<(\w+)/ms;
    open($INPUT, "<$ProjectFileName");
	$callargs{fileh} = $INPUT;
  }
  else { $callargs{resh} = $resh; }
  $ProjectClass = $ProjectTypeClasses->{$ProjectType}
   or
  die("ProjectType $ProjectType of Project $ProjectName NOT MANAGED !!");

  eval "require $ProjectClass"; 
  i::logit("Now Opening $ProjectName with $ProjectClass");
  $ProjectClass->Open($ProjectName, %callargs, @_);
}

1;

