
package XReport::FileXref;

use strict;
use Data::Dumper;
use Symbol;

sub StartItem {
  my ($self, $page) = (shift, shift); my ($FILXREF, $OUTPUT) = @{$self}{qw(FILXREF OUTPUT)};

  my $atPage = "Page=". $OUTPUT->{atPage};
  my $bundles = pop;

  $main::veryverbose && i::logit("StartItem called by ", join('::', (caller())[0,2]),
                                 " $atPage, BUNDLES: $bundles args: ", join('::', @_));
  print $FILXREF join("\t", "StartItem", @_, ( $bundles && $bundles ne '' ? ($atPage => "Bundles=$bundles") : ($atPage))),"\n";
}

sub EndItem {
  my $self = shift; my ($FILXREF, $OUTPUT) = @{$self}{qw(FILXREF OUTPUT)};
  my $atPage = "Page=". ( $OUTPUT->{atPage} - ($self->{Closing} ? 0 : 1));

  $main::veryverbose && i::logit("EndItem called by ", join('::', (caller())[0,2]), 
                                 " $atPage args: ", join('::', @_) );

  print $FILXREF join("\t", "EndItem", @_, $atPage),"\n";
}

sub getXrefPageList {
  my ($self, $XrefList, $XrefPageList) = (shift, gensym(), shift); close($self->{'FILXREF'}); 
  
  my ($LastPage, $LastItem) = ( $self->{'OUTPUT'}->{atPage}, {} );
  
  delete $self->{'FILXREF'}; delete $self->{'OUTPUT'}; 

  open($XrefList,"<".$self->{XrefFileName}) 
                or croak("ApplicationError: XrefList OPEN Error $!\n"); binmode($XrefList);

  while(<$XrefList>) {
  	my ($xrefline) = ($_ =~ /^([^\x0d\x0a]+)[\x0d\x0a]*$/m);
    my ( $rectype, $rn, $varn, $varv, @tail ) = split /\t/, $xrefline;
    #my $infos = { map { ($_ =~ /^([^\s\=]+)=(?!\=).+$/ ? (split /=/, $_) : ($_ => 1)) } @tail };
    my $infos = { map { $_ =~ /^([^\s\=]+)(?:\=([^\=]+))?$/; ($1, $2||1)} @tail };
    my ($filterval, $textstring) = split /\|/, $varv, 2;
    $main::veryverbose && i::logit("FileName: $self->{XrefFileName} Infos ".Dumper($infos)."---XRefLine: $xrefline");
#    die "DEBUG ===============\n"; 
    unless ( (exists($infos->{Page}) && $infos->{Page} =~ /^\d+/ && $rectype =~ /^(?:Start|End)Item$/ ) ) {
      die "ApplicationError: INVALID XrefList LINE: $xrefline INFO ".Dumper($infos)." HEX:", unpack('H*', $xrefline), "\n";
    }
    
    if ( $rectype =~ /^\/?Start/ ) {
      if ( !exists($XrefPageList->{$rn}->{$varn}->{$varv}) ) {
        $XrefPageList->{$rn}->{$varn}->{$varv}->{pages} = [];
      } 
      $main::veryverbose && i::logit("FileXref firstpage: $LastPage - Start: key:($rn,$varn,$varv) p: $infos->{Page},"
              ," item added: ", join("::", @{$XrefPageList->{$rn}->{$varn}->{$varv}->{pages}})
              ," b: $infos->{Bundles},"
              );
      push @{$XrefPageList->{$rn}->{$varn}->{$varv}->{pages}}, [$infos->{Page}];
      $XrefPageList->{globalHoldDays} = $infos->{HDays} unless $infos->{HDays} < $XrefPageList->{globalHoldDays};
#      $LastItem->{$rn}->{$varn}->[-1] = $LastPage
#                              if ($infos->{Page} == $LastPage && exists($LastItem->{$rn}->{$varn}));
    } 
    elsif ( $rectype =~ /^\/?End/ ) {
      my $forPages = $infos->{Page};
      $forPages += 1; # if $infos->{Page} == $LastPage;
      $forPages -= $XrefPageList->{$rn}->{$varn}->{$varv}->{pages}->[-1]->[0];

      die("ApplicationError: NUMBER OF PAGES NOT POSITIVE -"
          ," lastpage: $LastPage - End  : key:($rn,$varn,$varv) p: $infos->{Page},"
          ," NUM OF PAG: ", $forPages
          ," Pages: ", join('::', @{$XrefPageList->{$rn}->{$varn}->{$varv}->{pages}})
         ) if $forPages <= 0;
      my $lastEl = pop @{$XrefPageList->{$rn}->{$varn}->{$varv}->{pages}};	
      #push @{$lastEl}, ($forPages, $infos->{Page});
	  $main::veryverbose && i::logit("FileXref lastpage: $LastPage - End  : key:($rn,$varn,$varv) p: $infos->{Page},"
              #," item added: ", $XrefPageList->{$rn}->{$varn}->{$varv}->{pages}->[-1]
              ," item added: ", join("::", @{$lastEl}), "forP:" , $forPages
              ," Pages: ", join('::', @{$XrefPageList->{$rn}->{$varn}->{$varv}->{pages}})
              );
      my ($curp,$curforp, $currlastp) = ($lastEl->[0],$forPages, 0 + $infos->{Page});
      my $currArray = $XrefPageList->{$rn}->{$varn}->{$varv}->{pages};
      my ($firstmatch, $lastmatch, $resultp, $resultlastp, $fromdelete, $todelete) = (undef,undef,0,0,$#{$currArray}+1,0);
      for my $i (0..$#{$currArray}){
          my ($fp, $forp, $lastp) = @{$currArray->[$i]};
	      $main::veryverbose && i::logit("FileXref parameters",$curp,$curforp, $currlastp);
          if($curp>$lastp){
                $resultp = $curp;
                $resultlastp = $currlastp;
                next;
          }
          ###### caso primo elemento e secondo elemento dentro intervallo
	      ###### caso primo elemento dentro e secondo fuori intervallo
	      ###### caso primo elemento fuori intervallo e secondo dentro
	      ###### caso primo elemento dentro intervallo e secondo dentro altro intervallo
	      if ($curp >= $fp && !defined($lastmatch)){
		    $firstmatch = $i unless $firstmatch;
		        if($currlastp <= $lastp){
		            $lastmatch = $i;
                } 
	     }
         $main::veryverbose && i::logit("FileXref parameters",$firstmatch, "LAST",$lastmatch);
	     if(defined($firstmatch) && defined($lastmatch)){
           if($curp <= $currArray->[$firstmatch]->[2]){
              $resultp = $currArray->[$firstmatch]->[0];
              $fromdelete = $firstmatch;
           }else{
              $resultp =$curp;
              $fromdelete = $firstmatch+1;
           }
           if($currlastp >= $currArray->[$lastmatch]->[0]){
              $resultlastp = $currArray->[$lastmatch]->[2];
              $todelete = $lastmatch;
           }else{
              $resultlastp = $currlastp;
              $todelete = $lastmatch-1;
           }
           $main::veryverbose && i::logit("FileXref parameters","PRIMA",$resultp, "SECONDA",$resultlastp);     
           #$resultp = ($currp <= $currArray->[$firstmatch]->[2] ? $currArray[$firstmatch]->[0] : $currp);
           #$resultlastp = ($currlastp >= $currArray->[$lastmatch]->[0] ? $currArray->[$lastmatch]->[2] : $currlastp);
           last;	 
	      }
      }
      $main::veryverbose and i::logit("getXRefPL--currArray-Before".Dumper($currArray));
      #splice @{$currArray} , $fromdelete, $todelete - $fromdelete if($resultp > 0);
      splice @{$currArray} , $fromdelete, $todelete - $fromdelete +1 , [$resultp, $resultlastp- $resultp+1, $resultlastp] if($resultp > 0);
      $main::veryverbose and i::logit("getXRefPL--After--index" , "from ", $fromdelete, "to", $todelete -$fromdelete);
      $main::veryverbose and i::logit("getXRefPL--currArray-After".Dumper($currArray));
      if(scalar(@{$currArray})){
          $XrefPageList->{$rn}->{$varn}->{$varv}->{pages} = $currArray;
      }else{
          $XrefPageList->{$rn}->{$varn}->{$varv}->{pages} = [[($lastEl->[0],$forPages, 0 + $infos->{Page})]];
      }  
      $main::veryverbose && i::logit("FileXref pages:".Dumper($XrefPageList->{$rn}->{$varn}->{$varv}->{pages}));
      #die "ApplicationError: PROVA";
#      i::logit("FileXref lastpage: $LastPage - End  : key:($rn,$varn,$varv) p: $infos->{Page},"
#              ," item added: ", $XrefPageList->{$rn}->{$varn}->{$varv}->{pages}->[-1]
#              ," Pages: ", join('::', @{$XrefPageList->{$rn}->{$varn}->{$varv}->{pages}})
#              );
      $self->{textstrings}->{$textstring} = 1 if $textstring;
#      $LastItem->{$rn}->{$varn} = $XrefPageList->{$rn}->{$varn}->{$varv}->{pages};
    }
    else {
      die "ApplicationError: INVALID XrefList LINE: $xrefline HEX:", unpack('H*', $xrefline);
    }
    if ( exists($infos->{Bundles}) ) {
      foreach ( split / /, $infos->{Bundles} ) {
          $XrefPageList->{$rn}->{$varn}->{$varv}->{bundles}->{$_} = 1;      
      }
    }
  }
  $main::veryverbose && i::logit("XrefPageList: ", Dumper($XrefPageList));
  close($XrefList); $self->{totPages} = $self->{atPage};
  return $XrefPageList;
}

#TODO Review Last Page Logic
sub Closing {
	my $self = shift;
	my $Closing = shift;
	return $self->{Closing} unless defined($Closing);
	$self->{Closing} = $Closing;
}

sub new {
  my ($class, $jr, $OUTPUT, $Category) = @_; my $FileName = $jr->getFileName('FILXREF', undef, $Category); 
  
  my $FILXREF = gensym(); 
  
  open($FILXREF, ">$FileName") 
   or croak("FILXREF OPEN ERROR \"$FileName \"$!");

  bless {XrefFileName => $FileName, FILXREF => $FILXREF, OUTPUT => $OUTPUT, Closing => 0, Category=> $Category}, $class;
}

1;
