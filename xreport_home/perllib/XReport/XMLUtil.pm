#######################################################################
# @(#) $Id: XMLUtil.pm,v 1.2 2002/10/23 22:50:19 Administrator Exp $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
package XReport::XMLUtil;

use strict;

use Exporter;
our ($VERSION, @ISA, @EXPORT_OK);

$VERSION = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r }; 

@ISA = qw(Exporter);

@EXPORT_OK = qw( 
  &xmlquote &HASH2XML &SQL2XML &SQL_UNLOAD
);

my %add_objRef;

sub Variant_DateTime {
 return $_[0]->Date('yyyy/MM/dd')." ".$_[0]->Time('hh:mm:ss tt'); # 12 hours clock and AM/PM
}

sub rtrim {
  (my $t =$_[0]) =~ s/ +$//;return $t;
}

sub xmlquote {
  my $t = shift; $t =~ s/ +$//g;
  
  $t =~ s/[\x00-\x19]/ /sg;
  $t =~ s/&/&amp;/g;
  $t =~ s/</&lt;/g;
  $t =~ s/>/&gt;/g;
  $t =~ s/\"/&quot;/g;
  $t =~ s/\'/&apos;/g;
  
  $t =~ s/([\x80-\xff])/"\&\#".unpack("C",$1).";"/seg; 
  
  return $t;
}

$add_objRef{"PAGE"} = sub {
  $_[0] .= " objRef=\"".join("/", $_[1]->GetFieldsValues(qw(JobReportId FromPage ForPages)))."\"\n";
};

$add_objRef{"LINE"} = sub {
  $_[0] .= " objRef=\"".join("/", $_[1]->GetFieldsValues(qw(JobReportId FromPage FromLine ForLines)))."\"\n";
};

sub HASH2XML {
  my ($hash, @fields) = @_; @fields = keys(%$hash) if !@fields; my $xml;

  for my $field (@fields) {
    $xml .= " $field=\"".rtrim(xmlquote($hash->{$field}))."\"\n";
  }
  
  return $xml;
}

sub SQL2XML {
  my ($ele, $dbr, %args) = @_; my ($OUTPUT, $objRef, $addFields, $maxEntries) = @args{qw(OUTPUT objRef addFields maxEntries)};

  my ($fileName, $open_is_mine) = ($OUTPUT, 0);

  if ($OUTPUT ne "" and !ref($OUTPUT)) {
	my $fileName = $OUTPUT; require Symbol; $OUTPUT = Symbol::gensym();
	CORE::open($OUTPUT, ">$fileName") 
	  or 
	die("SQL2XML OPEN OUTPUT ERROR \"$fileName\" $!"); binmode($OUTPUT); $open_is_mine = 1; 
  }
  
  my ($field, $field_name, $field_type, $field_value, $xml); my ($addedFields, $totEntries) = ('', 0);
  
  my $Fields = $dbr->Fields(); my $FieldsCount = $Fields->Count();
  
  $objRef = $add_objRef{$objRef} if $objRef; 
  
  while (!$dbr->eof()) {
    $xml .= "<$ele \n"; &$objRef($xml, $dbr) if $objRef;
	
    for (my $j=0; $j < $FieldsCount; $j++) {
      $field = $Fields->Item($j); 
	
	  $field_name = $field->Name(); $field_type = $field->Type(); 
	
	  $field_value = $field->Value();
	  if (ref($field_value) && $field_type == 135) {
        $field_value = Variant_DateTime($field_value);
      }
	
      $xml .= " $field_name=\"".xmlquote($field_value)."\"\n"; 
    }

    if ($addFields and $addedFields = &$addFields($dbr)) {
      for my $field_name (keys(%$addedFields)) {
        $xml .= " $field_name=\"".xmlquote($addedFields->{$field_name})."\"\n"; 
      }
    }

    substr($xml,-1) = "/>\n"; do { print $OUTPUT $xml; $xml = ""; } if $OUTPUT; 
    
    $totEntries += 1; last if $maxEntries and $totEntries >= $maxEntries;
  }
  continue {
    $dbr->MoveNext();
  }
  
  return $xml if !$OUTPUT;
  
  CORE::close($OUTPUT) if $open_is_mine; return 1;
}

sub SQL_UNLOAD {
  my ($tbl, $ele, $dbr, $OUTPUT) = @_; my $fileName = $OUTPUT;

  if ($OUTPUT ne "" and !ref($OUTPUT)) {
	require Symbol; 
	$OUTPUT = Symbol::gensym();
	CORE::open($OUTPUT, ">$fileName") 
	  or 
	die("SQL_UNLOAD OPEN OUTPUT ERROR \"$fileName\" $!"); binmode($OUTPUT); 
  }

  print $OUTPUT "<$tbl>\n"; SQL2XML($ele,$dbr,$OUTPUT); print $OUTPUT "</$tbl>\n";
}

1;
