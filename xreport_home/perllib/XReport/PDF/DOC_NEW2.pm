#------------------------------------------------------------
package XReport::PDF::IStream;

use strict; use bytes;

use Symbol;

sub seek {
  my ($self, $pos, $whence) = @_; my $INPUT = $self->{INPUT};
  $self->{buffer} = '';
  return CORE::seek($INPUT, $pos, $whence);
}

sub tell {
  my $self = shift; my $INPUT = $self->{INPUT};
  return CORE::tell($INPUT) - length($self->{buffer});
}

sub read() {
  my $self = shift; my $INPUT = $self->{INPUT};
  if ( (my $off = length($self->{buffer})) > 0 ) {
    CORE::seek($INPUT, -$off, 1);
    $self->{buffer} = '';
  }
  return CORE::read($INPUT, $_[0], $_[1]);
}

sub getLine {
  my $self = shift; my $INPUT = $self->{INPUT};
  my $bref = \$self->{buffer};
  my $line = '';
  while(1) {
    if ( $$bref =~ /^(.*?(?:\x0d\x0a|\x0d|\x0a))/so ) {
	  $$bref = substr($$bref, length($1));
	  return $line.$1;
    }
	elsif ($INPUT->eof()) {
	  $line .= $$bref; $$bref = '';
	  return $line;
	}
	$line .= $$bref; $INPUT->read($$bref, 1024);
  }
}

sub eof {
  my $self = shift; my $INPUT = $self->{INPUT};
  return CORE::eof($INPUT);
}

sub Open {
  my $self = shift; my ($INPUT, $fileName) = @{$self}{qw(INPUT fileName)}; $self->{buffer} = '';
  open($INPUT, "<$fileName") 
   or 
  die("INPUT OPEN ERROR \"$fileName\" $!"); binmode($INPUT);
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};
  %$self = ();
  return CORE::close($INPUT);
}

sub fsize {
  my $self = shift; my $fileName = $self->{fileName};
  return -s $fileName;
}

sub new {
  my ($className, $fileName) = @_; my $self = {};
  
  $self->{fileName} = $fileName;
  $self->{INPUT} = gensym();

  bless $self, $className;
}

#------------------------------------------------------------
package XReport::PDF::SStream;

use strict; use bytes;

sub seek {
  my ($self, $pos, $whence) = @_; my $sref = $self->{Scalar};
  if ( $whence == 0 ) {
    $self->{atPos} = $pos;
  }
  elsif ( $whence == 1 ) {
    $self->{atPos} += $pos;
  }
  elsif ( $whence == 2 ) {
    $self->{atPos} = length($$sref) + $pos;
  }
}

sub tell {
  return $_[0]->{atPos};
}

sub read() {
  my $self = shift; my $sref = $self->{Scalar};
  $_[0]=substr($$sref,$self->{atPos},$_[1]); 
  $self->{atPos}+=$_[1]; 
  return $_[1];
}

sub getLine {
  my $self = shift; my $sref = $self->{Scalar}; my $line = ""; my $l = 1024;
  my $str = substr($$sref, $self->{atPos}, $l);
  while(1) {
    if ( $str =~ /^(.*?(?:\x0d\x0a|\x0d|\x0a))/so ) {
	  $self->{atPos} += length($1);
	  return $1;
    }
	elsif ($self->{atPos}+$l >= length($$sref)) {
	  return $str;
	}
	$l += 1024; $str = substr($$sref, $self->{atPos}, $l);
  }
}

sub eof {
  my $self = shift; return $self->{atPos} >= length(${$self->{Scalar}});
}

sub Open {
  my $self = shift; $self->{atPos} = 0; return 1;
}

sub Close {
  my $self = shift; $self->{atPos} = length(${$self->{Scalar}}); return 1;
}

sub fsize {
  my $self = shift; return length(${$self->{Scalar}});
}

sub new {
  my $className = shift; my $self = {};
  
  if ( ref($_[0]) eq "SCALAR" ) {
    $self->{Scalar} = $_[0];
  }
  else {
    $self->{Scalar} = \$_[0];
  }
  $self->{atPos} = 0;

  bless $self, $className;
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
}


#------------------------------------------------------------
package XReport::PDF::Lexer;

my $cs_spaces = quotemeta("\x00\x09\x0a\x0c\x0d\x20");
my $cs_delim = quotemeta("\(\)\<\>\[\]\{\}\/\%");

my $lx_space = qr/[$cs_spaces]/so;
my $lx_delim = qr/[$cs_delim]/so;
my $lx_sep = qr/[$cs_spaces$cs_delim]/so;
my $lx_regular = qr/[^$cs_spaces$cs_delim]/so;
my $lx_testprfx = qr/([$cs_spaces]*)(([^$cs_spaces]).*)/so;

my $lx_number = qr/-?(?:[0-9]+\.[0-9]*|\.[0-9]+|[0-9]+)(?=$lx_sep|(?:$\))/so;
my $lx_name = qr/\/$lx_regular+(?=$lx_sep|(?:$\))/so;
my $lx_string  = qr/\(.*?(?<!\\)\)/so;
my $lx_hexstring  = qr/\<[0-9a-fA-F$cs_spaces]*\>(?=$lx_sep|(?:$\))/so;
my $lx_objref = qr/(?:\d+)$lx_space+\d+$lx_space+R(?=$lx_sep|(?:$\))/so;
my $lx_objbeg = qr/(?:\d+)$lx_space+\d+$lx_space+obj(?=$lx_sep|(?:$\))/so;

my $lx_length = qr/\/Length[$cs_spaces]+(\d+[$cs_spaces]0[$cs_spaces]+R)|(\d+)/so;

sub NextToken {
  my $self = shift; my ($tref, $atpos) = @{$self}{qw(tref atpos)};

  my ($spaces, $str, $c) = substr($$tref, $atpos) =~ /$lx_testprfx/so;
  die "LEXER at eof <$spaces> <$c> <$str>" if $c eq "";
  my $c2 = substr($str,1,1); my $ltoken = 0;
  
  my ($tokentype, $token) = (0, '');
  
  if ( $c =~ /[0-9-.]/ ) {
    if ( $str =~ /^($lx_objref)/so ) {
	  $tokentype = "objref";
	  $token = $1;
    }
    elsif ( $str =~ /^($lx_objbeg)/so ) {
	  $tokentype = "objbeg";
	  $token = $1;
    }
    elsif ( $str =~ /^($lx_number)/so ) {
	  $tokentype = "number";
	  $token = $1;
	}
	else {
	}
  }
  
  elsif ( $c eq "(" ) {
	$tokentype = "string";
    my ($atpar, $rest) = (0, $str);
	while(1) {
	  if ( $rest =~ /^(|.*?[^\\])([()])(.*)$/so ) {
	    if ($2 eq '(') {
	      $atpar++;
	    }
	    elsif ($2 eq ')') {
	      $atpar--;
	    }
	    $token .= $1.$2; $rest = $3;
	    last if $atpar == 0;
	  }
	  else {
	    die "ERROR: UNCLOSED STRING";
	  }
	}
  }
  
  elsif ( $c eq "<" and $c2 ne "<" ) {
	$tokentype = "hexstring";
    if ( $str =~ /^($lx_hexstring)/so ) {
	  $token = $1;
	}
	else {
	  die "ERROR: INVALID HEXSTRING";
	}
  }
  elsif ( $c eq "/" ) {
	$tokentype = "name";
    if ( $str =~ /^($lx_name)/so ) {
	  $token = $1;
	}
	else {
	  die "ERROR: NAME ERROR";
	}
  }
  elsif ( $c eq "[" ) {
	$tokentype = "begarray";
    $token = $c;
  }
  elsif ( $c eq "]" ) {
	$tokentype = "endarray";
    $token = $c;
  }
  elsif ( $c eq "<" and $c2 eq "<" ) {
	$tokentype = "begdict";
    $token = $c.$c2;
  }
  elsif ( $c eq ">" and $c2 eq ">" ) {
	$tokentype = "enddict";
    $token = $c.$c2;
  }
  elsif ( $str =~ /^(true|false)(?:$lx_sep|$\)/so ) {
    $tokentype = "boolean";
	$token = $1;
  }
  elsif ( $str =~ /^(null)(?:$lx_sep|$\)/so ) {
    $tokentype = "null";
	$token = $1;
  }
  else {
    die "ERROR: UNKNOWN TOKEN at ".substr($str,$atpos);
  }

  $self->{atpos} += length($spaces) + ($ltoken || length($token));
  
  return [$tokentype, $spaces, ($tokentype ne "name") ? $token : substr($token,1)];
}

sub new {
  my ($className, $str) = (shift, shift);

  my $self = {tref => \"$str ", atpos => 0};

  bless $self, $className;
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
}

#------------------------------------------------------------
package XReport::PDF::Parser;

use strict; use bytes;

sub ExtractObjRefList {
  my @list = $_[1] =~ /(\d+)[$cs_spaces]+\d+[$cs_spaces]+R/sog;
  return @list;
}

sub NextObject {
  my ($self, $lexer) = @_; my ($str, $atobj) = ('', 0);

  while(1) {
    my ($tokentype, $spaces, $token) = @{$lexer->NextToken()};
    if ($tokentype =~ /^(?:begarray|begdict)$/) {
	  $atobj++;
	}
    if ($tokentype =~ /^(?:endarray|enddict)$/) {
	  $atobj--;
	}
	$str .= "$spaces$token";
	last if $atobj eq 0;
  }

  return $str;
}

sub ParseResourcesList {
}

sub ParseCatalog {
  my ($self, $str) = (shift, shift); my $Catalog = {};

  my $lexer = XReport::PDF::Lexer->new($str);

  while (1) {
    my $tokentype = $lexer->NextToken()->[0];
    last if $tokentype eq "begdict";
  }
  
  while(1) {
    my ($tokentype, $spaces, $token) = @{$lexer->NextToken()};
    if ( $tokentype eq "name" ) {
	  $Catalog->{$token} = $self->NextObject($lexer);
	}
	elsif ($tokentype eq "enddict") {
	  last;
	}
	else {
	  die "ERROR PARSING CATALOG";
	}
  }
  
  return $Catalog;
}

sub ParseArray {
}

sub new {
  my $className = shift; my $self = {};

  bless $self, $className;
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
}

#------------------------------------------------------------
package XReport::PDF::DOC;

use strict; use bytes;

use Symbol;

#use Time::HiRes qw(gettimeofday);
use Compress::Zlib;
use XReport::Tie::FileIArray;

use constant OBJ_OFFSET => 0;
use constant OBJ_CATALOG => 1;

my %pgsz=(
    '4a'        	=>  [ 4760  , 6716  ],
    '2a'        	=>  [ 3368  , 4760  ],
    'a0'        	=>  [ 2380  , 3368  ],
    'a1'        	=>  [ 1684  , 2380  ],
    'a2'        	=>  [ 1190  , 1684  ],
    'a3'        	=>  [ 842   , 1190  ],
    'a4'        	=>  [ 595   , 842   ],
    'a5'        	=>  [ 421   , 595   ],
    'a6'        	=>  [ 297   , 421   ],
    '4b'        	=>  [ 5656  , 8000  ],
    '2b'        	=>  [ 4000  , 5656  ],
    'b0'        	=>  [ 2828  , 4000  ],
    'b1'        	=>  [ 2000  , 2828  ],
    'b2'        	=>  [ 1414  , 2000  ],
    'b3'        	=>  [ 1000  , 1414  ],
    'b4'        	=>  [ 707   , 1000  ],
    'b5'        	=>  [ 500   , 707   ],
    'b6'        	=>  [ 353   , 500   ],
    'letter'    	=>  [ 612   , 792   ],
    'broadsheet'    =>  [ 1296  , 1584  ],
    'ledger'    	=>  [ 1224  , 792   ],
    'tabloid'   	=>  [ 792   , 1224  ],
    'legal'     	=>  [ 612   , 1008  ],
    'executive' 	=>  [ 522   , 756   ],
    '36x36'     	=>  [ 2592  , 2592  ],
);

sub setValues {
  my $self = shift;
  while(@_) {
    my ($k, $v) = (shift, shift);
	$self->{$k} = $v;
  }
}

#------------------------------------------------------ INPUT

sub getObj {
  my ($self, $objId, $recur) = @_; my ($INPUT, $TabXref, $optimizer) = @{$self}{qw(INPUT TabXref optimizer)};

  if ($objId =~ /(\d+)[$cs_spaces]+\d+[$cs_spaces]+R/so) {
    $objId = $1;
  }

  my $objref = $TabXref->[$objId]; my ($obj, $line, $t) = ([$objId], '', '');
  
  my ($offset, $catalog) = @$objref[OBJ_OFFSET, OBJ_CATALOG]; 
  
  $INPUT->seek($offset, 0); 

  if ( defined($catalog) ) {
    my ($offset, $catlen, $strlen) = @$objref[1..3]; $INPUT->seek($offset, 1);

    $INPUT->read($t, $catlen); $obj->[1] = $t;

    do { $INPUT->read($t, $strlen); $obj->[2] = $t; } if $strlen;
  }
  else {
	if ( ($line = $INPUT->getLine()) !~ /^($lx_objbeg)(.*)/so ) {
      die "INVALID OBJ READ obj=$objId at offset=$offset \n $line";
    }
    $line = $2; my $offset = length($1);
    while(1) {
	  if ($line =~ /^(.*)endobj\s*$/s) {
		$t .= $1 if $1 ne ""; last;
	  }
	  if ( $line =~ /(.*)(stream\s*(?:\x0a|\x0d))/ and $#$obj == 0) {
	    $obj->[1] = $t.$1; 
	    $t = $2.$self->getStream($obj);
	  }
	  else {
        $t .= $line;
	  }
      $line = $INPUT->getLine();
    }
    push @$obj, $t;

    @$objref[1..3] = ($offset, length($obj->[1]), length($obj->[2]));
  }

  $optimizer->optimize_obj($obj) if $optimizer; $c::cm->dispose($objref);

  #if ($#$obj > 1) {
  #  my ($stream) = $t =~ /stream\r?\n(.*)\nendstream/s;
  #  print uncompress($stream);
  #}
  #$obj->[1] =~ s/\x0d(?!\x0a)/\x0a/g;

  if ( $obj->[1] =~ /\s*\[\s*(\d+)\s+\d+\s*R\s*\]\s*$/ ) {
	die("TOO MANY RECURSIVE GETOBJ FOR OBJ $objId") 
	  if 
	$recur > 1;
	return $self->getObj($1, $recur+1);
  }

  return $obj;
}

sub getStream {
  my ($self, $streamCat) = (shift, shift); my $INPUT = $self->{INPUT}; my $stream;
  my ($objref, $length) = $streamCat =~ /$lx_length/so;
  if ( $objref ) {
    my $at = $INPUT->tell();
    $length = $self->getObj($objref)->[1];
	$INPUT->seek($at,0);
  }
  $INPUT->read($stream, $length); return $stream.$INPUT->getLine();
}

sub getPageObj {
  my ($self, $page) = (shift, shift); my $PageList = $self->{PageList};
  
  my $objId = $PageList->[$page-1];

  do {
    my $PdfPages = scalar(@{$self->{PageList}});
    die("INVALID PAGE NUMBER REQUESTED: $page (PdfPages: $PdfPages)")
  } 
  if $objId eq ''; 
 
  return $self->getObj($objId);
}

sub getPageTextLines {
  my ($self, $page) = (shift, shift); my $obj = $self->getPageObj($page); my $TextLines;

  my ($cid) = $obj->[1] =~ /\/Contents$lx_sep+(\d+)/;

  $obj = $self->getObj($cid); 
  
  if ($#$obj > 1) {
    my ($stream, $pagetxt, $parser); require XReport::PDF::PageParser;

    ($stream) = $obj->[2] =~ /stream\r?\n(.*)\sendstream/s;
    $pagetxt = uncompress($stream);
	$pagetxt =~ s/\x0d([^\x0a])/\x0d\x0a$1/g;
	
    $parser = XReport::PDF::PageParser->new();

    $TextLines = $parser->ExtractTextLines($pagetxt);
  }
  else {
    $TextLines = [];
  }
  
  return $TextLines;
}

sub getPageList {
  my ($self, $root) = (shift, shift); my (@nodes, @leafs); @nodes = ($root); 
  
  my ($MediaBox, $Rotate) = ("", "");
  
  while(my $node = shift @nodes) {
	my $cat = $self->getObj($node)->[1];	
	my ($kids) = $cat =~ /\/Kids\s*\[?\s*([^\]\/>]*)\s*\]?/mso;
	
	if ( $kids ) {
      my ($objref, $count) = $cat =~ /\/Count$lx_sep+(?:($lx_objref)|(\d+))/so;
	  if ( $objref ) {
	    my ($objId) = $objref =~ /^\s*(\d+)/;
	    $count = $self->getObj($objId)->[1];
	  }
	  my @kids = $kids =~ /(\d+) *0 *R/g;
      if ( $count != scalar(@kids) ) {
	    unshift @nodes, @kids;
	  }
	  else {
        push @leafs, @kids;
	  }
	}
	else {
      push @leafs, $node;
	}
	
	($MediaBox) = $cat =~ /\/MediaBox\s*\[\s*([^\]]*)\s*\]/mso, $MediaBox =~ s/\s{2,}/ /g if $MediaBox eq "";
    
	($Rotate) = $cat =~ /\/Rotate\s+(\d+)/mso if $Rotate eq "";
  }
  
  return (\@leafs, $MediaBox, $Rotate);
}

sub GetLastXref {
  my $self = shift; my $INPUT = $self->{INPUT}; my $str = '';
  $INPUT->seek(-50,2); $INPUT->read($str,50);
  if ( $str =~ /startxref\s+(\d+)\s+\%\%EOF/s ) {
    #print "last startxref: $1\n";
	return $1;
  }
  die "STARTXREF ERROR <$str> --- !!!";
}

sub ReadXref {
  my ($self, $startxref) = (shift, shift); my $INPUT = $self->{INPUT}; my ($txref, $trailer); local $_;
  
  $INPUT->seek($startxref,0);

  for(1..2) {
    $_ = $INPUT->getLine(); $txref .= $_;
  }
  my ($lobj, $tobjs) = $txref =~ /^xref\s+(\d+)\s+(\d+)/; 

  $INPUT->read($txref, $tobjs * 20);
  
  while(1) {
    $_ = $INPUT->getLine(); last if $_ eq ''; $txref .= $_; last if $_ =~ /^startxref/;
  }
  
  ($txref, $trailer) = $txref =~ /(.*?)(trailer.*?)startxref/s 
    or 
  die "READXREF ERROR at $startxref --- $tobjs !!!";
  
  $txref =~ s/\s+//sg; $trailer =~ s/\s+$//s; return ($lobj, $tobjs, $txref, $trailer);
}

sub creXrefAndPageList {
  my $self = shift; my ($INPUT, $iFileName, $lFileName, $Parser) = @{$self}{qw(INPUT iFileName lFileName Parser)}; 
  
  my ($lTrailer, $PagesObjId, $CatalogObjId, $CatalogStream, $Catalog, $PageList, $MediaBox, $Rotate);
  
  my $OUTPUT = gensym(); open($OUTPUT, ">$lFileName") 
   or 
  die("OUTPUT OPEN ERROR \"$lFileName\" $!"); binmode($OUTPUT); my $startxref = $self->GetLastXref(); 

  my ($tobjs, $lsize, $ioffset, $tpages, $loffset); ($lsize, $ioffset) = (4, 5+16+10); 
  
  while ($startxref) {
    my ($lobj, $tobjs, $txref, $trailer) = $self->ReadXref($startxref); $lTrailer = $trailer if $lTrailer eq '';
    
    my $lxref = "\x00" x ($tobjs * 5 * $lsize); my $lat2 = 5*$lsize; my ($loff, $lc);
    for (my $at=0, my $at2=0, my $to=$tobjs*16; $at<$to; $at+=16, $at2+=$lat2) {
      ($loff, $lc) = (substr($txref, $at, 10), substr($txref, $at+15, 1)); 
      next if $lc ne 'n';
      substr($lxref, $at2, 5) = '.' . pack("I", $loff);
	}
    seek($OUTPUT, $ioffset+$lobj*$lat2, 0); syswrite($OUTPUT, $lxref); 
    
    ($startxref) = $trailer =~ /\/Prev *(\d+)/; # prev xref .......
  }
  
  seek($OUTPUT, 0, 2); $loffset = tell($OUTPUT); print $OUTPUT '.', "\xff" x 4; seek($OUTPUT, $ioffset-26, 0); 
   $tobjs = ($loffset-$ioffset) / (5*$lsize);
  print $OUTPUT pack("a16aIaI", "\@TabXref", '.', $tobjs, '.', $loffset); close($OUTPUT); 

  tie my @TabXref,  'XReport::Tie::FileIArray', {
    FileName => $lFileName, 
    size => $tobjs, 
    lsize => $lsize, 
    ioffset => $ioffset
  }; 
  
  $self->setValues(TabXref => \@TabXref);
  
  ($CatalogObjId) = $lTrailer =~ /\/Root *(\d+)\s+/
    or
  die("UserError: INVALID PDF DOCUMENT DETECTED. ROOT CATALOG OBJ ID NOT FOUND");
  
  $Catalog = $Parser->ParseCatalog($CatalogStream = $self->getObj($CatalogObjId)->[1]);
  
  ($PagesObjId) = $Catalog->{'Pages'} =~ /(\d+)/ 
    or
  die("UserError: INVALID PDF DOCUMENT DETECTED. PAGE TREE ROOT NOT FOUND");

  ($PageList, $MediaBox, $Rotate) = $self->getPageList($PagesObjId); $tpages = @$PageList;
  
  open($OUTPUT, "+<$lFileName")
   or 
  die("OUTPUT REOPEN ERROR \"$lFileName\" $!"); binmode($OUTPUT); seek($OUTPUT, 16+10, 2); $ioffset = tell($OUTPUT);
  
  my $lPageList = "\x00" x ($tpages * 5); my $at = 0;
  for (@$PageList) {
    substr($lPageList, $at, 5) = '.'. pack("I", $_); $at += 5;
  }

  syswrite($OUTPUT, $lPageList); 
  
  seek($OUTPUT, 0, 2); $loffset = tell($OUTPUT); print $OUTPUT '.', "\xff" x 4; seek($OUTPUT, $ioffset-26, 0); 
   $tpages == ($loffset-$ioffset) / 5 
    or 
   die "TPAGES $tpages AND lPagesList (($loffset-$ioffset) / 5) MISMATCH!";
  print $OUTPUT pack("a16aIaI", "\@PageList", '.', $tpages, '.', $loffset); close($OUTPUT); 
  
  tie my @PageList,  'XReport::Tie::FileIArray', {
    FileName => $lFileName, 
    size => $tpages, 
    lsize => 1, 
    ioffset => $ioffset
  }; 
  
  $self->setValues(PageList => \@PageList);
  
  open($OUTPUT, "+<$lFileName")
   or 
  die("OUTPUT REOPEN ERROR \"$lFileName\" $!"); binmode($OUTPUT); seek($OUTPUT, 0, 2); $ioffset = tell($OUTPUT);

  print $OUTPUT 
    pack("a16na*", "\$CatalogStream", length($CatalogStream), $CatalogStream),
    pack("a16na*", "\$MediaBox", length($MediaBox), $MediaBox),
    pack("a16na*", "\$Rotate", length($Rotate), $Rotate),
  ;

  seek($OUTPUT, 0, 0); print $OUTPUT "\@XREF"; 
  seek($OUTPUT, 0, 2); print $OUTPUT "\@XREF"; 
  
  close($OUTPUT);
  
  $self->setValues(Catalog => $Catalog, MediaBox => $MediaBox, Rotate => $Rotate);
}

sub VerifyXrefFile {
  my $self = shift; my $lFileName = $self->{'lFileName'}; my $irec = '';
  
  return undef if !-e $lFileName or !-s $lFileName > 0;
  
  my $INPUT = gensym(); open($INPUT, "<$lFileName") 
   or 
  die("INPUT OPEN ERROR \"$lFileName\"$!"); binmode($INPUT);

  seek($INPUT,  0, 0); read($INPUT, $irec, 5); return 0 if $irec ne "\@XREF"; 
  seek($INPUT, -5, 2); read($INPUT, $irec, 5); return 0 if $irec ne "\@XREF"; 

  close($INPUT); return 1; 
}

sub getXrefAndPageList {
  my $self = shift; my ($INPUT, $iFileName, $Parser) = @{$self}{qw(INPUT iFileName Parser)}; my $lFileName;

  die "....................................................." if ($iFileName eq ":Scalar");

  $lFileName = $iFileName; $lFileName =~ s/\.pdf$//i; $lFileName .= '.XREF'; 
  
  $self->setValues(lFileName => $lFileName);
  
  if (!$self->VerifyXrefFile()) {
    require XReport::Locks; my $lck = XReport::Locks->new(); my $lockName = $self->{'lockName'};
    
    $lockName ||= "CreXref.$lFileName"; $lck->getLock($lockName);
    
    $self->creXrefAndPageList(), 
    $lck->relLock($lockName), 
    return
     if 
    !$self->VerifyXrefFile(); $lck->relLock($lockName); 
  }
  
  my ($CatalogObjId, $CatalogStream, $Catalog, $MediaBox, $Rotate);
  
  my ($tobjs, $tpages, $lsize, $ioffset, $loffset, $irec);
  
  my $INPUT = gensym(); open($INPUT, "<$lFileName") 
   or 
  die("INPUT OPEN ERROR \"$lFileName\"$!"); binmode($INPUT); $lsize = 4;

  $ioffset = 5 + 26; seek($INPUT, $ioffset - 26, 0); 
  
  read($INPUT, $irec, 26); ($tobjs, $loffset) = unpack("x16xIxI", $irec);

  tie my @TabXref, 'XReport::Tie::FileIArray', {
    FileName => $lFileName, size => $tobjs, 
    lsize => 4, ioffset => $ioffset
  }; 
  
  $self->setValues(TabXref => \@TabXref);

  $ioffset = $loffset + 5 + 26; seek($INPUT, $ioffset - 26, 0); 
  
  read($INPUT, $irec, 26); ($tpages, $loffset) = unpack("x16xIxI", $irec); $lsize = 1;
  
  tie my @PageList,  'XReport::Tie::FileIArray', {
    FileName => $lFileName, size => $tpages, 
    lsize => 1, ioffset => $ioffset
  }; 
  
  $self->setValues(PageList => \@PageList);
  
  $ioffset = $loffset + 5; seek($INPUT, $ioffset, 0);
  
  read($INPUT, $irec, 18); $irec = unpack("x16n", $irec); read($INPUT, $CatalogStream, $irec);
  read($INPUT, $irec, 18); $irec = unpack("x16n", $irec); read($INPUT, $MediaBox, $irec);
  read($INPUT, $irec, 18); $irec = unpack("x16n", $irec); read($INPUT, $Rotate, $irec);
  
  $Catalog = $Parser->ParseCatalog($CatalogStream);
  
  $self->setValues(Catalog => $Catalog, MediaBox => $MediaBox, Rotate => $Rotate);
}

#------------------------------------------------------ OUTPUT

sub getFromXref {
  my ($self, $doc) = (shift, shift); my $FromXref = $self->{FromXref};
  
  $FromXref->{$doc} = [[],[2]]
   if
  !exists($FromXref->{$doc}); $FromXref = $FromXref->{$doc};
  
  return wantarray ? @$FromXref : $FromXref->[0];
}

sub getNewXrefId {
  my ($self, $FromXref, $fref) = @_; 
  
  $FromXref->[$fref] = ($self->{MaxId} += 1)
   if 
  !exists($FromXref->[$fref]); return $FromXref->[$fref];
}

sub CopyObjs {
  my ($self, $doc, @objList) = @_; my ($largs, $OUTPUT, $ENCRYPT) = @{$self}{qw(largs OUTPUT ENCRYPT)};

  my $TabXref = $self->{TabXref}; my ($FromXref, $ToXref) = $self->getFromXref($doc); 

  my $ImagesSetId = $largs->{'ImagesSetId'};

  while (@objList) {
    my $obj = shift @objList; $obj = $doc->getObj($obj) if !ref($obj); 
	
	my $objId = $self->getNewXrefId($FromXref, $obj->[0]); next if $TabXref->[$objId]; 
	
	$TabXref->[$objId] = $OUTPUT->tell();

	$obj->[1] =~ s{(-?\d+)($lx_space+\d+$lx_space+R)(?=$lx_space|$lx_delim)}{
	  my ($fref, $ref, $rest) = ($1, $1 > 0 ? $FromXref->[$1] : $ToXref->[-$1-1], $2);
	  $ref = $self->getNewXrefId($FromXref, $fref) if !$ref;
	  if ( $fref > 0 and !$TabXref->[$ref] ) {
	    push @objList, $fref;
	  }
	  $ref.$rest;
	}gsoe;

    if ( $ENCRYPT ) {
      $ENCRYPT->encr_obj($objId, 0, \$obj->[1]);
       #todo: crypt using length from dictionary
       #todo: read well pdf ref 
      $ENCRYPT->encr_stream($objId, 0, \$obj->[2]);
    }

	$OUTPUT->write("$objId 0 obj");
	$OUTPUT->write("\n") if $obj->[1] !~ /^\s*[\r\n]/;	
	$OUTPUT->write($obj->[1]); $OUTPUT->write($obj->[2]); $OUTPUT->write("endobj\n");
  }
}

sub CheckMediaBox {
  my ($self, $doc) = (shift, shift);
  
  if ( $self->{MediaBox} eq "" ) {
    $self->{MediaBox} = $doc->{MediaBox}; $self->{Rotate} = $doc->{Rotate};
  }

  return $self->{MediaBox} eq $doc->{MediaBox} and $self->{Rotate} eq $doc->{Rotate}
}

sub MergeContents {
  my ($self, $doc, $pgobj) = (shift, shift, shift);

  my ($con) = $pgobj->[1] =~ /\/Contents\s*\[?\s*([^\]\/>]*)\s*\]?/mso;
  
  my @con = $con =~ /(\d+)\s+0\s+R/msog; return 0 if @con <= 1; 

  my ($iobj, $istream, $ostream) = ('','',''); my $FromXref = $self->getFromXref($doc);
  
  for (@con) {
    $iobj = $doc->getObj($_); ($istream) = $iobj->[2] =~ /stream[\r\n]+(.*)[\r\n]endstream/s;
    $ostream .= " " . uncompress($istream);
  }
  $ostream =~ s/\x0d(?!\x0a)/\x0a/g;
  
  $FromXref->[$con[0]] = $self->AddStream($ostream); 
  
  $pgobj->[1] =~ s/\/Contents\s*\[?\s*([^\]\/>]*)\s*\]?/\/Contents \[$con[0] 0 R\]/mso; return scalar(@con);
}

sub CopyPages {
  my ($self, $doc) = (shift, shift); my $largs = ref($_[-1]) ? pop @_ : undef; my $Parser = $self->{Parser}; 

  my $PageList = $self->{PageList}; my $FromXref = $self->getFromXref($doc); 

  my $equ_MEDIABOX = $self->CheckMediaBox($doc);

  my $MergeC = $largs ? $largs->{'MERGECONTENTS'} : 0;

  while (@_) {
    my ($fm, $for) = splice(@_,0,2);
    for ($fm..($fm+$for-1)) {
      my $pgobj = $doc->getPageObj($_); $self->MergeContents($doc, $pgobj) if $MergeC;
	  
	  $pgobj->[1] =~ s/(\/Parent$lx_sep+)(\d+)/$1-1/so;
	
      if (!$equ_MEDIABOX and $pgobj->[1] !~ /\/MediaBox\s/) {
        my $t = "/MediaBox [$doc->{MediaBox}] " .
         ($doc->{Rotate} ne '' ? " /Rotate $doc->{Rotate}" : '')
        ;
        $pgobj->[1] =~ s/\>\>/ $t\>\>/s;
      }
	
	  $self->CopyObjs($doc, $pgobj); push @$PageList, $FromXref->[$pgobj->[0]];
    }
  }
}

sub getPageResources {
  my ($self, $pgobj) = (shift, shift); return undef if !$pgobj; my $res;

  while ($pgobj ne '' and $pgobj->[1] !~ /\/Resources/) {
    ($pgobj) = $pgobj->[1] =~ /\/Parent\s+(\d+)/mso 
	  or 
    die("INVALID PAGE CATALOG $pgobj->[1] DETECTED.\n"); $pgobj = $self->getObj($pgobj);
  }

  ($res) = $pgobj->[1] =~ /(\/Resources\s*<<(?:[^<>]+|<<[^<>]+>>)+>>)/mso 
    or	
  ($res) = $pgobj->[1] =~ /(\/Resources\s*\[?\s*([^\]\/>]*)\s*\]?)/mso; 
  
  $res or die("UNABLE TO GET RESOURCES STRING FOR PAGE OBJ $pgobj->[0], $pgobj->[1]");
}

sub PageToXForm {
  my ($self, $doc, $pgobj) = @_; my $FromXref = $self->getFromXref($doc); my @xolist;

  my ($con) = $pgobj->[1] =~ /\/Contents\s*\[?\s*([^\]\/>]*)\s*\]?/mso;
  my ($res) = $pgobj->[1] =~ /(\/Resources\s*\[?\s*([^\]\/>]*)\s*\]?)/mso;

  $res = $doc->getPageResources($pgobj) if $res eq ''; 
	  
  for ($con =~ /(\d+)\s+0\s+R/msog) {
	my $obj = $doc->getObj($_); my ($icat) = $obj->[1] =~ /<<(.*)>>/mso; $icat =~ s/([^\s])\//$1 \//g;
	$obj->[1] = 
	  "<</Type /XObject /Subtype /Form /BBox [0 0 32767 32767] $res $icat>>"
	;
	$self->CopyObjs($doc, $obj); push @xolist, $FromXref->[$obj->[0]];
  }
  
  return @xolist;
}

sub PageToXForm_NEW {
  my ($self, $doc, $pgobj) = @_; my $FromXref = $self->getFromXref($doc); my @xolist;

  my ($res) = $pgobj->[1] =~ /(\/Resources\s*\[?\s*([^\]\/>]*)\s*\]?)/mso;

  $res = $doc->getPageResources($pgobj) if $res eq ''; 
  
  my @con = $self->MergeContents($doc, $pgobj);
	  
  for (@con) {
	my $obj = $doc->getObj($_); my ($icat) = $obj->[1] =~ /<<(.*)>>/mso; $icat =~ s/([^\s])\//$1 \//g;
	$obj->[1] = 
	  "<</Type /XObject /Subtype /Form /BBox [0 0 32767 32767] $res $icat>>"
	;
	$self->CopyObjs($doc, $obj); push @xolist, $FromXref->[$obj->[0]];
  }
  
  return @xolist;
}

sub MergePages {
  my ($self, @docList) = @_; my ($idoc, $ipg, @pgList); my @tMediaBox;

  for $idoc (0..$#docList) {
	my ($doc, @tlist) = @{$docList[$idoc]}; $ipg = 0; my $equ_MEDIABOX = $self->CheckMediaBox($doc);

    while (@tlist) {
      my ($fm, $for) = splice(@tlist,0,2); 
      for ($fm .. ($fm + $for - 1)) {
        my $pgobj = $doc->getPageObj($_); 
		if ( $idoc == 0 ) {
		  @tMediaBox = $pgobj->[1] =~ /CropBox\s*\[\s*([\d.]+)\s+([\d.]+)\s+([\d.]+)\s+([\d.]+)/mso;
          @tMediaBox = split(/\s+/, $doc->{'MediaBox'}) if !@tMediaBox;
	      $pgList[$ipg] = [[($tMediaBox[2]-$tMediaBox[0]) || 0, ($tMediaBox[3]-$tMediaBox[1]) || 0]];	  
		}
		my ($cmX, $cmY) = $pgobj->[1] =~ /CropBox\s*\[\s*([\d.]+)\s+([\d.]+)\s+/mso;
		push @{$pgList[$ipg]},	[$cmX, $cmY, [$self->PageToXForm($doc, $pgobj)]]; $ipg += 1;
      }
    }
  }

  my ($docList, $doc, $iobj, $objList, $page_stream, $iobj);
  
  for $docList (@pgList) {

	my ($meX, $meY) = @{shift @$docList}; ($objList, $page_stream, $iobj) = ('', '', 0);
	
	for $doc (@$docList) {
  	  my ($cmX, $cmY, $XForms) = @$doc; 
	  for (1..scalar(@$XForms)) {
		$iobj += 1;
	    $objList .= "/Fm$iobj $XForms->[$_-1] 0 R ";
	    $page_stream .= ($cmX || $cmY ? " q 1 0 0 1 ".(-$cmX)." ".(-$cmY)." cm " : " q") . " /Fm$iobj Do Q";
	  }
    }
	
	my $resId = $self->AddObj(
      "<<\n"
     ."/ProcSet [/PDF /Text /ImageB /ImageC /ImageI]\n"
     ."/XObject <<$objList>>\n"
     .">>\n"
    );
	
	my $stId = $self->AddStream($page_stream); 

    $self->AddPageObj(
      "<<\n"
     ."/Type /Page\n"
     ."/Parent 2 0 R\n"
     ."/Resources $resId 0 R\n"
     ."/Contents $stId 0 R\n"
	 ."/MediaBox [0 0 $meX $meY]\n"
     .">>\n"
    );
  }
}

sub AddObj {
  my ($self, $objstr, $objId) = @_; my ($OUTPUT, $TabXref, $ENCRYPT) = @{$self}{qw(OUTPUT TabXref ENCRYPT)};

  $objId = $self->{MaxId} += 1 if !$objId;
  
  if ( $objstr ) {
    $TabXref->[$objId] = $OUTPUT->tell();

    $ENCRYPT->encr_obj($objId, 0, \$objstr) 
      if 
    $ENCRYPT;

    $OUTPUT->write("$objId 0 obj\n"); $OUTPUT->write($objstr); $OUTPUT->write("endobj\n");
  }

  return $objId;
}

sub AddStream {
  my ($self, $stream, $compress) = @_; my ($OUTPUT, $TabXref, $ENCRYPT) = @{$self}{qw(OUTPUT TabXref ENCRYPT)};

  $compress = 1 if !defined($compress);

  my $objId = $self->{MaxId} += 1;
  $stream = compress($stream) 
   if
  $compress; 
  my $length = length($stream);
  
  $TabXref->[$objId] = $OUTPUT->tell();

  $ENCRYPT->encr_stream($objId, 0, \$stream) if $ENCRYPT;
  
  $OUTPUT->write("$objId 0 obj\n");
  $OUTPUT->write(
    "<<\n"
   ."/Length $length\n"
   .($compress ? "/Filter /FlateDecode\n" : "")
   .">>\n"
  );
  $OUTPUT->write("stream\n"); $OUTPUT->write("$stream\n"); $OUTPUT->write("endstream\n"); $OUTPUT->write("endobj\n"); return $objId;
}

sub AddPageObj {
  my $self = shift; my $PageList = $self->{PageList};

  my $objId = $self->AddObj(@_);
  
  push @$PageList, $objId;

  return $objId;
}

sub TotPages {
  my $self = shift; my $PageList = $self->{PageList};
  
  return(scalar(@$PageList));
}

sub WritePageTree {
  my $self = shift; my ($OUTPUT, $TabXref) = @{$self}{qw(OUTPUT TabXref)};
  
  my ($PageList, $MediaBox, $Rotate) = @{$self}{qw(PageList MediaBox Rotate)}; 

  my $Count = @$PageList;

  $TabXref->[2] = $OUTPUT->tell(); my ($i, $imax) = (0, $#$PageList);

  $OUTPUT->write(
    "2 0 obj\n"
   ."<<\n"
   ."/Type /Pages\n"
   ."/Count $Count\n"
   ."/Kids [\n"
  );
  while(1) {
    my $l = '';
    for (1..10) {
	  last if $i > $imax;
	  $l .= " ".$PageList->[$i++]." 0 R";
	}
	$OUTPUT->write(" $l\n") if $l ne ''; last if $i > $imax;
  }
  $OUTPUT->write(
    "]\n"
    .($MediaBox ? "/MediaBox [$MediaBox]\n" : "")
    .($Rotate ? "/Rotate $Rotate\n" : "")
   .">>\n"
   ."endobj\n"
  );
}

sub WriteInfo {
}

sub WriteEncrypt {
  my $self = shift; my ($OUTPUT, $TabXref, $ENCRYPT) = @{$self}{qw(OUTPUT TabXref ENCRYPT)}; return if !$ENCRYPT;

  my ($V, $R, $O, $U, $P, $length) = @{$ENCRYPT->get_encrypt_parms()}{qw(V R O U P Length)};

  my $objId = $self->{Encrypt} = $self->{MaxId} += 1;
  $TabXref->[$objId] = $OUTPUT->tell();

  $OUTPUT->write(
    "$objId 0 obj\n"
   ."<< \n"
   ."/Filter /Standard \n"
   ."/V $V \n"
   ."/R $R \n"
   ."/O ($O) \n"
   ."/U ($U) \n"
   ."/P $P \n"
   ."/Length $length \n"
   .">> \n"
   ."endobj\n"
  );
}

sub WriteCatalog {
  my $self = shift; my ($OUTPUT, $TabXref, $Outlines) = @{$self}{qw(OUTPUT TabXref Outlines)};
  
  $TabXref->[1] = $OUTPUT->tell();

  $OUTPUT->write(
    "1 0 obj\n"
   ."<<\n"
   ."/Type /Catalog\n"
   ."/Pages 2 0 R\n"
   .($Outlines 
     ? "/Outlines $Outlines 0 R\n/PageMode /UseOutlines\n"
     : ""
    )
   .">>\n"
   ."endobj\n"
  );
}

sub ExpandOutlineNode {
  my ($self, $title, $tref, $parent, $selfId, $selfPrev, $selfNext) = @_; 

  my $first = $self->AddObj(); my ($prev, $next) = (undef, $first);
  
  my ($at, $odest) = (undef, undef);

  ($odest, $tref) = @$tref if ref($tref) =~ /ARRAY/;
  
  my @titles = sort(keys(%$tref)); my $count = scalar(@titles);

  while(@titles) {
    my $title = shift @titles; my $dest = $tref->{$title}; $at = $next;
	
	if (scalar(@titles)) {
	  $next = $self->AddObj(); 
	}
	else {
	  $next = undef;
	}
	
	if (!ref($dest)) {
	  $self->AddObj(
	    "<<\n"
	   ."/Title ($title)\n"
	   ."/Dest [$dest 0 R /XYZ null null null]\n"
	   ."/Parent $selfId 0 R\n"
	   .(($prev) ? "/Prev $prev 0 R\n" : "")
	   .(($next) ? "/Next $next 0 R\n" : "")
	   .">>\n"   
	   ,
	   $at
	  );
	}
	else {
      $dest = ($self->ExpandOutlineNode($title, $dest, $selfId, $at, $prev, $next))[1];
	}
	
    $prev = $at; $odest = $dest if !$odest; 
  }
  
  my $last = $at;  

  #todo: add possibility ref points to a page different than the first underneath

  $self->AddObj( 
    "<<\n"
   .(($title ne undef) ? "/Title ($title)\n" : "")
   ."/Dest [$odest 0 R /XYZ null null null]\n"
   ."/Parent $parent 0 R\n"
   ."/Count -$count\n"
   ."/First $first 0 R\n"
   ."/Last $last 0 R\n"
   .(($selfPrev) ? "/Prev $selfPrev 0 R\n" : "")
   .(($selfNext) ? "/Next $selfNext 0 R\n" : "")
   .">>\n"
   ,
   $selfId
  );
  
  return ($selfId, $odest, $count); #add firstpage 
}

sub setOutlines {
  my ($self, $tref) = (shift, shift); my $Outlines = $self->AddObj(); my @t; my $Count;

  for (sort(keys(%$tref))) {
    push @t, [
      $self->ExpandOutlineNode($_, $tref->{$_}, $Outlines, $self->AddObj(), undef, undef)
    ];
    $Count += $t[-1]->[2];
  }

  $self->AddObj( 
    "<<\n"
   ."/Count $Count\n"
   ."/First $t[0]->[0] 0 R\n"
   ."/Last $t[-1]->[0] 0 R\n"
   .">>\n"
   ,
   $Outlines
  );
  
  $self->{Outlines} = $Outlines; 
}

sub WriteXref {
  my $self = shift; my ($OUTPUT, $TabXref, $ENCRYPT, $Info, $ID) = @{$self}{qw(OUTPUT TabXref ENCRYPT Info ID)};

  my $start = $OUTPUT->tell(); my $size = @$TabXref; my $lobjId = 0; 
  
  my ($lfree, $offset) = (0, 0);

  $OUTPUT->write( 
    "xref\n"
   ."0 $size\n"
   ."0000000000 65535 f \n"
  ); 
  for my $objId (1..($size-1)) {
    while ($objId > $lobjId+1) {
	  $offset = substr("000000000$lfree",-10);
	  $OUTPUT->write("$offset 00000 f \n");
	  $lobjId++;
	  $lfree = $lobjId;
	}
	$offset = substr("000000000".$TabXref->[$objId],-10);
	$OUTPUT->write("$offset 00000 n \n");
	$lobjId = $objId;
  }
  
  $OUTPUT->write(
    "trailer\n"
   ."<<\n"
   ."/Size $size\n"
   .($Info ? "/Info $Info 0 R\n" : "")
   .($ENCRYPT ? "/Encrypt $self->{Encrypt} 0 R\n" : "")
   ."/Root 1 0 R\n"
   .($ID ? "/ID[<".unpack("H*",$ID->[0])."><".unpack("H*",$ID->[0]).">]\n" : '')
   .">>\n"
   ."startxref\n"
   ."$start\n"
   ."\%\%EOF\n"
  );
}

sub get_iprogr {
  $_[0]->{iprogr} += 1;
}

sub SetValues {
  my $self = shift; while (@_) {$self->{shift} = shift}; return 1;
}

sub Create {
  my ($className, $OUTPUT, $ENCRYPT) = (shift, shift, shift); require FileHandle; my ($ID, @TabXref, @PageList);

  do {
    $ENCRYPT = {
      owner_pass => rand(999999999999999),
      user_pass => '',
      perms => -1852,
      encr_length => 128
    }
  }
  if uc($ENCRYPT) eq 'YES'; $ENCRYPT = '' if !ref($ENCRYPT);

  do {
    require Digest::MD5; 
    require XReport::PDF::ENCR;

    $ID = [Digest::MD5::md5(time().rand(999999999999999))]; $ID->[1] = $ID->[0]; 
    
    $ENCRYPT = XReport::PDF::ENCR->new(%$ENCRYPT, id => $ID) 
  }
  if ref($ENCRYPT);
  
  do {
    my $fileName = $OUTPUT; $OUTPUT = FileHandle->new();
    $OUTPUT->open(">$fileName") or die("PDF OUTPUT OPEN ERROR \"$fileName\" $!"); 
  }
  if !ref($OUTPUT); 

  #tie @TabXref,  'XReport::Tie::FileIArray', {lsize => 1};
  #tie @PageList,  'XReport::Tie::FileIArray', {lsize => 1};

  my $self = {
	Parser    => XReport::PDF::Parser->new(),
	MaxId     => 2,
	FromXref  => {},
	TabXref   => \@TabXref,
	PageList  => \@PageList,
    Outlines  => undef,
    iprogr    => 0,
    ID        => $ID,
    OUTPUT    => $OUTPUT,
    ENCRYPT   => $ENCRYPT
  };

  binmode($OUTPUT);

  $OUTPUT->write("\%PDF-1.3\n%\xe2\xe3\xcf\xd3\n");

  bless $self, $className;
}

sub Close {
  my $self = shift; my $TotPages = $self->TotPages();

  my ($INPUT, $OUTPUT, $optimizer) = @{$self}{qw(INPUT OUTPUT optimizer)};

  if ($OUTPUT) {
    $self->WritePageTree();
    $self->WriteInfo();
    $self->WriteEncrypt()
     if
    $self->{ENCRYPT};
    $self->WriteCatalog();
    $self->WriteXref();
	$OUTPUT->close();
	delete $self->{'OUTPUT'};
  }

  if ($INPUT) {
	$INPUT->Close();
	delete $self->{'INPUT'};
  }

  if ($optimizer) {
    $optimizer->Close();
	delete $self->{'optimizer'};
  }

  %$self = (); return $TotPages;
}

sub Open {
  my ($className, $INPUT, $largs) = (shift, shift, shift); $largs = {}; my $iFileName; 

  if ( ref($INPUT) ) {
  }
  elsif ( $INPUT ne ":Scalar" ) {
    $iFileName = $INPUT; $INPUT = XReport::PDF::IStream->new($iFileName);
    $INPUT->Open() or die("PDF INPUT OPEN ERROR \"$iFileName\" $!"); 
  }
  else {
    $iFileName = ":Scalar"; $INPUT = XReport::PDF::SStream->new(shift);
  }

  my ($lockName, $optimizer) = @{$largs}{qw(lockName optimizer)};

  do {
    require XReport::PDF::Optimizer;
    $optimizer = XReport::PDF::Optimizer->new($optimizer);
  }
  if ref($optimizer) eq 'HASH';

  my $self = {
    INPUT => $INPUT, iFileName => $iFileName, 
    largs => $largs,
    lockName => $lockName,
    optimizer => $optimizer,
	Parser => XReport::PDF::Parser->new()
  }; 

  bless $self, $className;

  $self->getXrefAndPageList();

  return $self;
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
}

__END__
#------------------------------------------------------------
package XReport::PDF::Page;

my $Parser = XReport::PDF::Parser->new(); ### todo: make it a singleton

sub addContent {
}

sub addResource {
}

sub write {
}

sub writeAsForm {
  my $self = shift; my $doc = $self->{doc}; 
}

sub read {
  my ($self, $doc, $page) = @_; 

  my $dictstr = $doc->getPageObj($page)->[1];

  $self->{'doc'} = $doc;

  my $Catalog = $Parser->ParseCatalog($dictstr);

  print "====>", join("\n", %$Catalog), "<====\n";

  $self->{'Catalog'} = $Catalog;

  my (@MediaBox) = $Catalog->{'MediaBox'} =~ /(\d+)/gso;
  my (@Contents) = $Catalog->{'Contents'} =~ /(?:\s*(\d+)\s+\d+\s+R)/gso;
  my (@Resources) = $Catalog->{'Resources'} =~ /(?:\s*(\d+)\s+\d+\s+R)/gso;

  ### todo: MediaBox Contents and Resources as lists

  print "??? $Catalog->{'MediaBox'} / ".join(",",@MediaBox)."\n";
  print "??? $Catalog->{'Contents'} / ".join(",",@Contents)."\n";
  print "??? $Catalog->{'Resources'} / ".join(",",@Resources)."\n";
}

sub new {
  my ($className, $doc) = @_; 

  my $self = bless {doc => $doc}, $className;
}

1;

__END__

todo: 

pagetree construction

inheritable entries:

Resources
MediaBox
CropBox
Rotate

at copy time get their value

add_page 

add to kids of last_parent (second level)

if kids last_parent (second level) >= 10 

create new last_parent (second level)
add it to last_parent (third level)
and so on

at write 
for inheritable
if inheritable value eq parent_value delete it



-------------------------------------------------

pagetree

node [to, objid|ref]
[from+count, objref]

