
package XReport::PDF::AcroForm;

use strict;

require XReport::PDF::ENCODING;
require XReport::PDF::DOC;

require XReport::Font::AFM;

#------------------------------------------------------------
package XReport::PDF::AcroForm::Annot;

use strict;

use Scalar::Util qw(weaken);

sub getValues {
  my $self = shift; my ($Field, $Catalog) = @{$self}{qw(Field Catalog)}; my @r;
  for (@_) {
    push @r, exists($Catalog->{$_}) ? $Catalog->{$_} : $Field->getValues($_);
  }
  return wantarray ? @r : $r[0];
}

sub setValues {
}

sub UpdateObj {
  my $self = shift; my ($ldoc, $AnnotObjId, $Catalog) = @{$self}{qw(doc AnnotObjId Catalog)}; my $FieldS = "";

  $FieldS = "<<"; for(keys(%$Catalog)) {$FieldS .= "/$_ $Catalog->{$_}"}; $FieldS .= ">>";

  $ldoc->UpdateObj($AnnotObjId, $FieldS, "");
}

sub Update_Btn {
  my ($self, $FieldValue) = @_; my ($ldoc, $Field, $AnnotObjId, $Catalog) = @{$self}{qw(doc Field AnnotObjId Catalog)};

  my ($FieldName, $FieldType) = $Field->getValues(qw(T FT));

  print("Update_Btn NOT IMPLEMENTED (FieldName=$FieldName FieldValue $FieldValue) !!\n"); 

  my $AP = $ldoc->ParseCatalog($Catalog->{'AP'}); 
  print join("\n", %$AP), "\n";
  my $N = $ldoc->ParseCatalog($AP->{'N'});
  print join("\n", %$N), "\n";

=sample
/AP<</D<</S 3715 0 R/Off 3716 0 R>>/N<</S 3712 0 R/Off 3713 0 R>>>>
/AS/Off
=cut
 
  $Catalog->{'AS'} = $Catalog->{'V'} = "/$FieldValue"; 
}

sub Update_Tx {
  my ($self, $FieldValue) = @_; my ($ldoc, $Field, $Catalog) = @{$self}{qw(doc Field Catalog)}; $FieldValue =~ s/\s+$//s;
  my ($DR, $DA, $Rect, $Q, $Ff, $MK) = $self->getValues(qw(DR DA Rect Q Ff MK)); 

  $MK = $ldoc->ParseCatalog($MK); my $MK_R = exists($MK->{'R'}) ? $MK->{'R'} : 0; 
  
  my @Rect = $Rect =~ /([\d.]+)/sg;
  my ($lX, $lY) = ($Rect[2]-$Rect[0], $Rect[3]-$Rect[1]);
  my ($FontName, $FontSize) = $DA =~ /(\/\w+)\s+([\d.]+)\s+Tf/i;

  my ($FontObjId) = $DR =~ /$FontName\s+(\d+)\s+0\s+R/s; my $FontAlias = $ldoc->GetFontAlias($FontObjId); 
  
  my $FontMetrics = $FontAlias->getValues('FontMetrics');

  my $Matrix = "[1.0 0.0 0.0 1.0 0.0 0.0]";

  if ($MK_R == 0) {
  }
  elsif ($MK_R == 90) {
    ($lX, $lY) = ($lY, $lX);
    $Matrix = "[0.0 1.0 -1.0 0.0 $lY 0.0]";
  }
  else { 
    die("ROTATE VALUE INVALID ($MK_R) Field($Field->{'FieldName'}) FieldVale($FieldValue)");
  }

  my $lX_2 = $lX - 2; my $lY_2 = $lY - 2;

  my $TL = $FontSize * 1.156; # is it correct ? how to find ?

#  my ($Ascender, $Descender) = map {$_/1000*$FontSize} $FontMetrics->getFontValues(qw(Ascender Descender)); $Descender = -$Descender;
  my ($Ascender, $Descender) = map {$_/1000*$FontSize} $FontMetrics->getValues(qw(Ascender Descender)); $Descender = -$Descender;
#  print "Processing Field Field($Field->{'FieldName'}) FieldValue($FieldValue) - Ascdr: $Ascender Descdr: $Descender\n";
  
  my  $lX_off = 1 + 1;

  if ($Q == 0) {
  }
  elsif ($Q == 1 or $Q == 2) {
    my $lwidth = $FontAlias->StringWidth($FieldValue) * $FontSize / 1000;
    $lX_off = $Q == 1 ? ($lX_2 - $lwidth)/2 : $lX_2 - $lwidth - 2;
  }
  else {
    die "ALIGNMENT PARAMETER VALUE Q ($Q) NOT MANAGED Field($Field->{'FieldName'}) FieldValue($FieldValue)\n";
  }

  my $lY_off = 1 + $Descender + ($lY_2 < $TL ? ($lY_2-$TL)/2 : 0);
  
  $DR =~ s/\s*<<(.*)>>\s*$/$1/s; $DA =~ s/\s*\((.*)\)\s*$/$1/s;
  
  my $obj_stream = <<EOF;
/Tx BMC  
q  
1 1 $lX_2 $lY_2 re  
W 
n 
BT  
$DA
$lX_off $lY_off Td 
$TL TL 
($FieldValue) Tj
ET  
Q 
EMC
EOF
  $obj_stream =~ s/\s+$//;
  my $lobj_stream = length($obj_stream);
  my $obj_catalog = <<EOF;
<<
/Length $lobj_stream
/Type/XObject
/BBox[0.0 0.0 $lX $lY] 
/Resources
<<
$DR  
/ProcSet[/PDF/Text] 
>>
/Subtype/Form
/FormType 1
/Matrix $Matrix
>>
EOF
  $obj_catalog =~ s/\s+$//; 

  my $obj_id = $ldoc->AddObj($obj_catalog, $obj_stream);

  $Catalog->{'AP'} = "<</N $obj_id 0 R>>"; $self->{'XObject'} = $obj_id; 
}

sub Update {
  my ($self, $FieldValue) = @_; my ($ldoc, $Field, $Catalog) = @{$self}{qw(doc Field Catalog)}; 

  my ($FieldName, $FieldType) = $Field->getValues(qw(T FT));

  if ($FieldType eq "/Tx") {
    $self->Update_Tx($FieldValue);
  }
  elsif ($FieldType eq "/Btn") {
    $self->Update_Btn($FieldValue);
  }
  else {
    die("AcroForm FielType($FieldType) NOT MANAGED for Field($FieldName)");
  }

  $Catalog->{Ff} |= 1 if exists($Catalog->{Ff}); $Catalog->{F} |= 64 if exists($Catalog->{F}); $self->UpdateObj();
}

sub new {
  my ($class, $ldoc, $Field, $AnnotObjId) = @_; my $AnnotObj = $ldoc->getObj($AnnotObjId); 

  my $Catalog = $ldoc->ParseCatalog($AnnotObj->[1]);
  
  my $self = {
    AnnotObjId => $AnnotObjId, AnnotObj => $AnnotObj, Catalog => $Catalog, Updated => 0, Field => $Field, doc => $ldoc
  };

  bless $self, $class;

  for (@{$self}{qw(doc Field)}) {weaken($_)} return $self; 
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
}

1;

#------------------------------------------------------------
package XReport::PDF::AcroForm::Field;

use strict;

use Scalar::Util qw(weaken);

sub getValues {
  my $self = shift; my ($ldoc, $Catalog) = @{$self}{qw(doc Catalog)}; my @r;
  for (@_) {
    push @r, exists($Catalog->{$_}) ? $Catalog->{$_} : $ldoc->getValues($_);
  }
  return wantarray ? @r : $r[0];
}

sub setValues {
}

sub UpdateObj {
  my $self = shift; my ($ldoc, $FieldObjId, $Catalog) = @{$self}{qw(doc FieldObjId Catalog)}; my $FieldS = "";

  $FieldS = "<<"; for(keys(%$Catalog)) {$FieldS .= "/$_ $Catalog->{$_}"}; $FieldS .= ">>";

  $ldoc->UpdateObj($FieldObjId, $FieldS, "");
}

sub Update {
  my ($self, $FieldValue) = @_; my ($ldoc, $Catalog, $FieldObjId, $Annots) = @{$self}{qw(doc Catalog FieldObjId Annots)}; 

  my ($AA, $V) = $self->getValues(qw(AA V));
  
  for (@$Annots) {
    $_->Update($FieldValue) if $FieldValue !~ /^\s*$/ and $V =~ /^\s*$/;
    $Catalog->{'Ff'} = $self->getValues('Ff') | 1;
  }

  $self->UpdateObj();
}

sub ReadOnly {
  my $self = shift; $self->getValues('Ff') & 1;
}

sub Updatable {
  my $self = shift; my $ro = $self->ReadOnly(); !$ro;
}

sub new {
  my ($class, $ldoc, $FieldObjId) = @_; my $FieldObj = $ldoc->getObj($FieldObjId); my @Annots; 

  my $Catalog = $ldoc->ParseCatalog($FieldObj->[1]); my $FieldName = $Catalog->{T};
  
  my @AnnotObjIds = exists($Catalog->{Kids}) ? $ldoc->ExtractObjRefList($Catalog->{Kids}) : ($FieldObjId);

  $FieldName =~ s/^\((.*)\)$/$1/go;

  my $self = {
    FieldName => $FieldName, FieldObjId => $FieldObjId, FieldObj => $FieldObj, 
    Catalog => $Catalog, Annots => \@Annots, 
    doc => $ldoc
  };

  bless $self, $class; my $Field = $self;

  for my $AnnotObjId (@AnnotObjIds) {
    push @Annots, XReport::PDF::AcroForm::Annot->new($ldoc, $Field, $AnnotObjId);   
  }

  weaken($self->{doc}); return $self;
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
}

1;

#------------------------------------------------------------
package XReport::PDF::AcroForm;

use strict;

require XReport::PDF::Fonts;

sub getObj {
  my $self = shift; my $ldoc = $self->{'doc'}; $ldoc->getObj(@_);
}

sub ParseCatalog {
  my $self = shift; my $ldoc = $self->{'doc'}; $ldoc->ParseCatalog(@_);
}

sub ExtractObjRefList {
  my $self = shift; my $ldoc = $self->{'doc'}; $ldoc->ExtractObjRefList(@_);
}

sub GetFontAlias {
  my ($self, $FontObjId) = @_; my ($ldoc, $FONTs, $FontAliases) = @{$self}{qw(doc FONTs FontAliases)}; 

  return $FontAliases->{$FontObjId} if exists($FontAliases->{$FontObjId});

  my ($FontAlias, $BaseFont, $FontFamily, $FontStyle, $Encoding);

  my $FontObj = $ldoc->ParseCatalog($FontObjId);
  
  ($BaseFont, $Encoding) = @{$FontObj}{qw(BaseFont Encoding)}; $BaseFont =~ s/^\s*\/|\s+$//gs;

  if (exists($FontAliases->{$BaseFont})) {
    $FontAlias = $FontAliases->{$BaseFont};
  }
  else {
    $FontAlias = $FontAliases->{$BaseFont} = $FONTs->GetFontAlias($BaseFont, $FontFamily, $FontStyle, $Encoding);
  } 

  $FontAliases->{$FontObjId} = $FontAlias; return $FontAlias;
}

sub getValues {
  my $self = shift; my $Catalog = $self->{AcroCatalog}; my @r;
  for (@_) {
    push @r, exists($Catalog->{$_}) ? $Catalog->{$_} : undef;
  }
  return wantarray ? @r : $r[0];
}

sub setValues {
}

sub getPageValues {
  my ($self, $pgobj) = (shift, shift); return undef if !$pgobj; my ($ldoc) = @{$self}{qw(doc)}; my @lvalues;

  while(@_) {
    my $attribute = shift; my $lcat;
    while ($pgobj ne '' and $lcat = $ldoc->ParseCatalog($pgobj->[1]) and !exists($lcat->{$attribute})) {
      ($pgobj) = $pgobj->[1] =~ /\/Parent\s+(\d+)/mso 
	    or 
      die("INVALID PAGE CATALOG $pgobj->[1] DETECTED.\n"); $pgobj = $self->getObj($pgobj);
    }
    push @lvalues, $lcat->{$attribute};
  }

  return wantarray ? @lvalues : $lvalues[0];
}

sub FieldNames {
  my $self = shift; my $Fields = $self->{'Fields'}; map {$_->{'FieldName'}} @$Fields; 
}

sub UpdateFields {
  my ($self, $FieldValues) = @_; my ($lFields, $ldoc) = @{$self}{qw(lFields doc)};

  for my $Field (@$lFields) {
    my $FieldName = $Field->{FieldName}; my $FieldValue; 
    if (exists($FieldValues->{$FieldName})) { 
      $FieldValue = $FieldValues->{$FieldName}; $Field->Update($FieldValue);
    }
    else {
      print("FieldName $FieldName is NOT ReadOnly but is not set in FieldValues !!!\n");
    }
  }
}

sub AddObj {
  my ($self, $obj_catalog, $obj_stream) = @_; my ($NextObjId, $CreatedObjects) = @{$self}{qw(NextObjId CreatedObjects)};

  $obj_stream = "stream\r\n$obj_stream\r\nendstream\r\n" if $obj_stream ne "";

  push @$CreatedObjects, [$NextObjId, $obj_catalog, $obj_stream];

  $self->{'NextObjId'} += 1; return $NextObjId;
}

sub UpdateObj {
  my ($self, $obj_id, $obj_catalog, $obj_stream) = @_; my ($UpdatedObjects) = @{$self}{qw(UpdatedObjects)};

  $obj_stream = "stream\r\n$obj_stream\r\nendstream\r\n" if $obj_stream ne "";

  push @$UpdatedObjects, [$obj_id, $obj_catalog, $obj_stream];
}

sub PrepareDocToCopy {
  my ($self, $FileName) = @_;
  my (
    $ldoc, $CreatedObjects, $UpdatedObjects
  ) = 
  @{$self}{qw( doc CreatedObjects UpdatedObjects )}; 

  for (@$UpdatedObjects, @$CreatedObjects) { $ldoc->UpdateObj($_) }; return $ldoc;
}

sub WritePdf {
  my ($self, $FileName) = @_; my ($INPUT, $OUTPUT, $obj, $obj_id, $obj_catalog, $obj_stream, $obj_offset, $buff, $rc, $startxref, $lastxref, $Trailer);
  my (
    $ldoc, $CreatedObjects, $UpdatedObjects
  ) = 
  @{$self}{qw(doc CreatedObjects UpdatedObjects)}; my $Catalog = $ldoc->{'Catalog'}; 

  @$UpdatedObjects = sort {$a->[0] <=> $b->[0]} @$UpdatedObjects;

  $INPUT = $ldoc->{'INPUT'}; $OUTPUT = IO::File->new(">$FileName"); $OUTPUT->binmode();

  $INPUT->seek(0,0);
  while(1) {
    $rc = $INPUT->read($buff, 32768); last if $buff eq ""; die "??? rc=$rc len=".length($buff)."\n" if $rc != length($buff); $OUTPUT->write($buff, $rc);
  }

  for $obj (@$UpdatedObjects, @$CreatedObjects) {
    my ($obj_id, $obj_catalog, $obj_stream) = @$obj; $obj->[3] = $OUTPUT->tell();
    $OUTPUT->print("$obj_id 0 obj\r\n$obj_catalog\r\n");
    $OUTPUT->print($obj_stream) if $obj_stream;
    $OUTPUT->print("endobj\r\n");
  }

  $startxref = $OUTPUT->tell();

  $OUTPUT->print("xref\r\n");

  for $obj (@$UpdatedObjects) {
    my ($obj_id, undef, undef, $obj_offset) = @$obj; $OUTPUT->print("$obj_id 1\r\n");
    $obj_offset = substr("0" x 9 . $obj_offset, -10); $OUTPUT->print("$obj_offset 00000 n\r\n");
  }
  $OUTPUT->print("$CreatedObjects->[0]->[0] ".scalar(@$CreatedObjects)."\r\n");
  for $obj (@$CreatedObjects) {
    my ($obj_id, undef, undef, $obj_offset) = @$obj; 
    $obj_offset = substr("0" x 9 . $obj_offset, -10); $OUTPUT->print("$obj_offset 00000 n\r\n");
  }
  $OUTPUT->print("trailer\r\n");

  $lastxref = $ldoc->GetLastXref(); (undef, $Trailer) = $ldoc->ReadXref($lastxref);

  $Trailer = $ldoc->ParseCatalog($Trailer);

  $Trailer->{'Prev'} = $lastxref;
  $Trailer->{'Size'} = $self->{'NextObjId'};

  $obj_catalog = "<<"; for(keys(%$Trailer)) {$obj_catalog .= "/$_ $Trailer->{$_}"}; $obj_catalog .= ">>";

  $OUTPUT->print("$obj_catalog\r\n");

  $OUTPUT->print("startxref\r\n$startxref\r\n%%EOF\r\n"); $OUTPUT->close();
}

sub Open {
  my $class = shift; my $ldoc = XReport::PDF::DOC->Open(@_); my (@Fields, @lFields, $Field);

  my ($Catalog, $TabXref) = @{$ldoc}{qw(Catalog TabXref)};

  my $NextObjId = scalar(@$TabXref);

  my $AcroCatalog = $ldoc->ParseCatalog($Catalog->{AcroForm});

  my @FieldObjIds = $ldoc->ExtractObjRefList($AcroCatalog->{Fields});

  my $Encoder = XReport::PDF::ENCODING->new(); 
  
  my $self = {
    Catalog => $Catalog, AcroCatalog => $AcroCatalog, Fields => \@Fields, lFields => \@lFields, NextObjId => $NextObjId, 
    Encoder => $Encoder, FontAliases => {}, EncodingTables => {}, 
    UpdatedObjects => [], CreatedObjects => [], 
    doc => $ldoc, 
    FirstNextObjId => $NextObjId
  };

  bless $self, $class; $self->{'FONTs'} = XReport::PDF::FONTs->new($self); 

  for my $FieldObjId (@FieldObjIds) {
    $Field = XReport::PDF::AcroForm::Field->new($self, $FieldObjId); 
    push @Fields, $Field; 
    push @lFields, $Field if $Field->Updatable() 
  }

  return $self;
}

sub Close {
  my $self = shift; my $ldoc = $self->{doc}; $ldoc->Close() if $ldoc;
}

sub DESTROY {
  my $self = shift; $self->Close(); #print "DESTROY ", __PACKAGE__, "\n";
}

1;
