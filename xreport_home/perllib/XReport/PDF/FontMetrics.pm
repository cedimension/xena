
package XReport::PDF::FontMetrics;
use XReport::Font::AFM;

use strict;

use Scalar::Util qw(weaken);

sub getEncodingTable {
  my ($self, $resstream) = @_; my ($doc, $Encoder, $EncodingTables) = @{$self->{'doc'}}{qw(doc Encoder EncodingTables)}; 
  
  my ($EncodingName, $EncodingDiff, $EncodingTable);

  my $res = $doc->ParseCatalog($resstream); 

  my $Encoding = $res->{'Encoding'}; 

  if (exists($EncodingTables->{$Encoding})) {
    return $EncodingTables->{$Encoding};
  }

  if (($EncodingName, $EncodingDiff) = $Encoding =~ /<<\s*\/([a-z]+)\s*(\d+)/i) {
    $EncodingDiff = $doc->ParseCatalog($EncodingDiff);
    $EncodingTable 
     = 
    $Encoder->getEncodingTable($EncodingName, $EncodingDiff->{'Differences'});
  }
  else {
    die "DECODING NOT FOUND $resstream // $Encoding //";
  }

  $EncodingTables->{$Encoding} = $EncodingTable;
}

sub StringWidth {
  my ($self, $t, $res_stream) = @_; my ($doc, $FileName, $FontMetrics) = @{$self}{qw(doc FileName FontMetrics)}; ($doc, my $WidthTables) = @{$doc}{qw(doc WidthTables)};

  my $EncodingTable = $self->getEncodingTable($res_stream);

  my $WidthTable; my $WidthTableKey = "$FileName/$EncodingTable";

  if (exists($WidthTables->{$WidthTableKey})) {
    $WidthTable 
     = 
    $WidthTables->{$WidthTableKey}
  }
  else {
    $WidthTable = $FontMetrics->getWidthTable($EncodingTable);
    $WidthTables->{$WidthTableKey} = $WidthTable;
  }

  my $at = 0; my $max_at = length($t)-1; my $tw = 0;

  while($at<=$max_at) {
    $tw += $WidthTable->[ord(substr($t,$at,1))]; $at += 1;
  }

  return $tw;
}

sub getFontValues {
  my $self = shift; my $FontMetrics = $self->{'FontMetrics'}; $FontMetrics->getFontValues(@_);
}

sub FileName {
  my $self = shift; return $self->{'FileName'};
}

sub new {
  my ($class, $doc, $FontName) = @_; my $FileName = "$ENV{'XREPORT_HOME'}/perllib/XReport/Font/afm/$FontName.afm"; 

  my $self = {
    doc => $doc, FileName => $FileName, FontMetrics => XReport::Font::AFM->new($FileName)
  };

  weaken($self->{doc}); bless $self, $class;
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
}

1;

