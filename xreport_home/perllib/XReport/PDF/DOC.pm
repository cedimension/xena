
#------------------------------------------------------------
package XReport::PDF::IStream;

use strict; use bytes;

use IO::File;

sub seek {
  my ($self, $pos, $whence) = @_; my $INPUT = $self->{INPUT}; $self->{buffer} = '';

  return $INPUT->sysseek($pos, $whence);
}

sub tell {
  my $self = shift; my $INPUT = $self->{INPUT}; return $INPUT->sysseek(0,1) - length($self->{buffer});
}

sub reset_buffer {
  my $self = shift; my $INPUT = $self->{INPUT}; my $offset = length($self->{buffer});

  $INPUT->sysseek(-$offset, 1), $self->{buffer} = '' if $offset;
}

sub read() {
  my $self = shift; my $INPUT = $self->{INPUT}; $self->reset_buffer() if $self->{buffer} ne ''; $INPUT->sysread(@_);
}

sub getLine {
  my $self = shift; my $INPUT = $self->{INPUT};
  my $bref = \$self->{buffer};
  my $line = '';
  while(1) {
    if ( $$bref =~ /^(.*?(?:\x0d\x0a|\x0d|\x0a))/so ) {
	  $$bref = substr($$bref, length($1)); 
      return $line.$1;
    }
    $line .= $$bref; $$bref = ''; !$INPUT->sysread($$bref, 1024) and return $line; 
  }
}

sub eof {
  my $self = shift; my $INPUT = $self->{INPUT}; return $INPUT->sysseek(0,1) == $self->{FileSize};
}

sub Open {
  my $self = shift; my ($INPUT, $FileName) = @{$self}{qw(INPUT FileName)}; $self->{buffer} = '';

  $INPUT->open($FileName, "r") 
   or 
  die("INPUT OPEN ERROR \"$FileName\" $!"); binmode($INPUT); 

  $self->{FileSize} = $INPUT->sysseek(0,2); $INPUT->sysseek(0,0); return $self;
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT}; %$self = (); return $INPUT->close();
}

sub fsize {
  my $self = shift; my $FileName = $self->{FileName}; return -s $FileName;
}

sub new {
  my ($className, $FileName) = @_; my $self = {};
  
  $self->{FileName} = $FileName;
  $self->{INPUT} = IO::File->new();

  bless $self, $className;
}

#------------------------------------------------------------
package XReport::PDF::SStream;

use strict; use bytes;

sub seek {
  my ($self, $pos, $whence) = @_; my $sref = $self->{Scalar};
  if ( $whence == 0 ) {
    $self->{atPos} = $pos;
  }
  elsif ( $whence == 1 ) {
    $self->{atPos} += $pos;
  }
  elsif ( $whence == 2 ) {
    $self->{atPos} = length($$sref) + $pos;
  }
}

sub tell {
  return $_[0]->{atPos};
}

sub read() {
  my $self = shift; my $sref = $self->{Scalar};
  $_[0]=substr($$sref,$self->{atPos},$_[1]); 
  $self->{atPos}+=$_[1]; 
  return $_[1];
}

sub getLine {
  my $self = shift; my $sref = $self->{Scalar}; my $line = ""; my $l = 1024;
  my $str = substr($$sref, $self->{atPos}, $l);
  while(1) {
    if ( $str =~ /^(.*?(?:\x0d\x0a|\x0d|\x0a))/so ) {
	  $self->{atPos} += length($1);
	  return $1;
    }
	elsif ($self->{atPos}+$l >= length($$sref)) {
	  return $str;
	}
	$l += 1024; $str = substr($$sref, $self->{atPos}, $l);
  }
}

sub eof {
  my $self = shift; return $self->{atPos} >= length(${$self->{Scalar}});
}

sub Open {
  my $self = shift; $self->{atPos} = 0; return 1;
}

sub Close {
  my $self = shift; $self->{atPos} = length(${$self->{Scalar}}); return 1;
}

sub fsize {
  my $self = shift; return length(${$self->{Scalar}});
}

sub new {
  my $className = shift; 
  
  my $self = {
    Scalar => ref($_[0]) eq "SCALAR" ? $_[0] : \$_[0], atPos => 0
  };

  bless $self, $className;
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
}


#------------------------------------------------------------
package XReport::PDF::Lexer;

my $cs_spaces = quotemeta("\x00\x09\x0a\x0c\x0d\x20");
my $cs_delim = quotemeta("\(\)\<\>\[\]\{\}\/\%");

my $lx_space = qr/[$cs_spaces]/so;
my $lx_delim = qr/[$cs_delim]/so;
my $lx_sep = qr/[$cs_spaces$cs_delim]/so;
my $lx_regular = qr/[^$cs_spaces$cs_delim]/so;
my $lx_testprfx = qr/([$cs_spaces]*)(([^$cs_spaces]).*)/so;

#my $lx_number = qr/-?(?:[0-9]+\.[0-9]*|\.[0-9]+|[0-9]+)(?=$lx_sep|(?:$\))/so;
#my $lx_name = qr/\/$lx_regular+(?=$lx_sep|(?:$\))/so;
#my $lx_string  = qr/\(.*?(?<!\\)\)/so;
#my $lx_hexstring  = qr/\<[0-9a-fA-F$cs_spaces]*\>(?=$lx_sep|(?:$\))/so;
#my $lx_objref = qr/(?:\d+)$lx_space+\d+$lx_space+R(?=$lx_sep|(?:$\))/so;
#my $lx_objbeg = qr/(?:\d+)$lx_space+\d+$lx_space+obj(?=$lx_sep|(?:$\))/so;

my $lx_number = qr/-?(?:[0-9]+\.[0-9]*|\.[0-9]+|[0-9]+)(?=$lx_sep)/so;
my $lx_name = qr/\/$lx_regular+(?=$lx_sep|(?:$\))/so;
my $lx_string  = qr/\(.*?(?<!\\)\)/so;
my $lx_hexstring  = qr/\<[0-9a-fA-F$cs_spaces]*\>(?=$lx_sep)/so;
my $lx_objref = qr/(?:\d+)$lx_space+\d+$lx_space+R(?=$lx_sep)/so;
my $lx_objbeg = qr/(?:\d+)$lx_space+\d+$lx_space+obj(?=$lx_sep)/so;

my $lx_length = qr/\/Length[$cs_spaces]+(?:(\d+[$cs_spaces]+0[$cs_spaces]+R)|(\d+))/so;

my $lx_ctl0 = qr/[^<>]+/so;
my $lx_ctl1 = qr/<<(?:$lx_ctl0)+>>/so;
my $lx_ctl2 = qr/<<(?:$lx_ctl0|$lx_ctl1)+>>/so;

my $lx_catalog = qr/<<(?:$lx_ctl0|$lx_ctl1|$lx_ctl2)+>>/so;

my $lx_end_trailer = qr/(?:[$cs_spaces]*(?:startxref|xref|$lx_objbeg|$lx_objref)[$cs_spaces]*)/so;

sub NextToken {
  my $self = shift; my ($tref, $atpos) = @{$self}{qw(tref atpos)};

  my ($spaces, $str, $c) = substr($$tref, $atpos) =~ /$lx_testprfx/so;
  die "LEXER at eof <$spaces> <$c> <$str>" if $c eq "";
  my $c2 = substr($str,1,1); my $ltoken = 0;
  
  my ($tokentype, $token) = (0, '');
  
  if ( $c =~ /[0-9\-\.]/ ) {
    if ( $str =~ /^($lx_objref)/so ) {
	  $tokentype = "objref";
	  $token = $1;
    }
    elsif ( $str =~ /^($lx_objbeg)/so ) {
	  $tokentype = "objbeg";
	  $token = $1;
    }
    elsif ( $str =~ /^($lx_number)/so ) {
	  $tokentype = "number";
	  $token = $1;
	}
	else {
	}
  }
  
  elsif ( $c eq "(" ) {
	$tokentype = "string";
    my ($atpar, $rest) = (0, $str);
	while(1) {
	  if ( $rest =~ /^(|.*?[^\\])([()])(.*)$/so ) {
	    if ($2 eq '(') {
	      $atpar++;
	    }
	    elsif ($2 eq ')') {
	      $atpar--;
	    }
	    $token .= $1.$2; $rest = $3;
	    last if $atpar == 0;
	  }
	  else {
	    die "ERROR: UNCLOSED STRING";
	  }
	}
  }
  
  elsif ( $c eq "<" and $c2 ne "<" ) {
	$tokentype = "hexstring";
    if ( $str =~ /^($lx_hexstring)/so ) {
	  $token = $1;
	}
	else {
	  die "ERROR: INVALID HEXSTRING";
	}
  }
  elsif ( $c eq "/" ) {
	$tokentype = "name";
    if ( $str =~ /^($lx_name)/so ) {
	  $token = $1;
	}
	else {
	  die "ERROR: NAME ERROR";
	}
  }
  elsif ( $c eq "[" ) {
	$tokentype = "begarray";
    $token = $c;
  }
  elsif ( $c eq "]" ) {
	$tokentype = "endarray";
    $token = $c;
  }
  elsif ( $c eq '<' and $c2 eq '<' ) {
	$tokentype = "begdict";
    $token = $c.$c2;
  }
  elsif ( $c eq '>' and $c2 eq '>' ) {
#  elsif ( $c eq ">" and $c2 eq ">" ) {
	$tokentype = "enddict";
    $token = $c.$c2;
  }
  elsif ( $str =~ /^(true|false)(?:$lx_sep|$\)/so ) {
    $tokentype = "boolean";
	$token = $1;
  }
  elsif ( $str =~ /^(null)(?:$lx_sep|$\)/so ) {
    $tokentype = "null";
	$token = $1;
  }
  else {
    die "ERROR: UNKNOWN TOKEN atpos=$atpos //\nstr=$str //\nsubstr=". substr($str,$atpos). " //\n";
  }

  $self->{atpos} += length($spaces) + ($ltoken || length($token));
  
  return [$tokentype, $spaces, ($tokentype ne "name") ? $token : substr($token,1)];
}

sub new {
  my ($className, $str) = (shift, shift);

  my $self = {tref => \"$str ", atpos => 0};

  bless $self, $className;
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
}

#------------------------------------------------------------
package XReport::PDF::Parser;

use strict; use bytes;

sub ExtractObjRefList {
  my @list = $_[1] =~ /(\d+)[$cs_spaces]+\d+[$cs_spaces]+R/sog;
  return @list;
}

sub NextObject {
  my ($self, $lexer) = @_; my ($str, $atobj) = ('', 0);

  while(1) {
    my ($tokentype, $spaces, $token) = @{$lexer->NextToken()};
    if ($tokentype =~ /^(?:begarray|begdict)$/) {
	  $atobj++;
	}
    if ($tokentype =~ /^(?:endarray|enddict)$/) {
	  $atobj--;
	}
	$str .= $spaces if $str ne ''; 
    $str .= ($tokentype eq 'name' ? "/" : ''). "$token"; last if $atobj == 0;
   #$str .= ($tokentype eq 'name' && $atobj > 0 ? "/" : ''). "$token"; last if $atobj eq 0;
  }
  
  return $str;
}

sub ParseResourcesList {
}

sub ParseCatalog {
  my ($self, $str) = (shift, shift); my $catalog = {};

  my $lexer = XReport::PDF::Lexer->new($str);

  while (1) {
    my $tokentype = $lexer->NextToken()->[0];
    last if $tokentype eq "begdict";
  }
  
  while(1) {
    my ($tokentype, $spaces, $token) = @{$lexer->NextToken()}; 
    if ( $tokentype eq "name" ) {
      $catalog->{$token} = $self->NextObject($lexer);
	}
	elsif ($tokentype eq "enddict") {
	  last;
	}
	else {
	  die "ERROR PARSING CATALOG";
	}
  }
  
  return $catalog;
}

sub ParseArray {
}

sub new {
  my $className = shift; my $self = {};

  bless $self, $className;
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
}

#------------------------------------------------------------
package XReport::PDF::DOC;

use strict; use bytes;

our @ISA = qw(XReport::PDF::Parser);

use Symbol;

#use Time::HiRes qw(gettimeofday);
use Digest::MD5 qw(md5);
use Compress::Zlib;
use XReport::Tie::FileIArray;

use constant OBJ_FILE_OFFSET => 0;
use constant OBJ_CAT_OFFSET => 1;
use constant OBJ_CAT_LEN => 2;
use constant OBJ_STREAM_LEN => 3;

my %pgsz=(
    '4a'        	=>  [ 4760  , 6716  ],
    '2a'        	=>  [ 3368  , 4760  ],
    'a0'        	=>  [ 2380  , 3368  ],
    'a1'        	=>  [ 1684  , 2380  ],
    'a2'        	=>  [ 1190  , 1684  ],
    'a3'        	=>  [ 842   , 1190  ],
    'a4'        	=>  [ 595   , 842   ],
    'a5'        	=>  [ 421   , 595   ],
    'a6'        	=>  [ 297   , 421   ],
    '4b'        	=>  [ 5656  , 8000  ],
    '2b'        	=>  [ 4000  , 5656  ],
    'b0'        	=>  [ 2828  , 4000  ],
    'b1'        	=>  [ 2000  , 2828  ],
    'b2'        	=>  [ 1414  , 2000  ],
    'b3'        	=>  [ 1000  , 1414  ],
    'b4'        	=>  [ 707   , 1000  ],
    'b5'        	=>  [ 500   , 707   ],
    'b6'        	=>  [ 353   , 500   ],
    'letter'    	=>  [ 612   , 792   ],
    'broadsheet'    =>  [ 1296  , 1584  ],
    'ledger'    	=>  [ 1224  , 792   ],
    'tabloid'   	=>  [ 792   , 1224  ],
    'legal'     	=>  [ 612   , 1008  ],
    'executive' 	=>  [ 522   , 756   ],
    '36x36'     	=>  [ 2592  , 2592  ],
);

sub setValues {
  my $self = shift;
  while(@_) {
    my ($k, $v) = (shift, shift);
	$self->{$k} = $v;
  }
}

#------------------------------------------------------ INPUT

sub ParseCatalog {
  my ($self, $cstream) = @_; my $Parser = $self->{Parser};
    
  $cstream = $self->getObj($cstream =~ /(\d+)/)->[1]  
   if 
  $cstream !~ /^\s*<</;
  
  $Parser->ParseCatalog($cstream);
}

sub getObjStream {
  my ($self, $streamCat) = (shift, shift); my $INPUT = $self->{INPUT}; my $stream = '';
  my ($objref, $length) = $streamCat =~ /$lx_length/so;
  if ( $objref ) {
    my $at = $INPUT->tell(); $length = $self->getObj($objref)->[1]; $INPUT->seek($at,0);
  }
  $INPUT->read($stream, $length) if $length; return $stream.$INPUT->getLine();
}

sub UpdateObj {
  my ($self, $obj) = @_; $obj->[0] = $self->{NextObjectId} += 1 if !$obj->[0];
  
  $self->{'UpdatedObjs'}->{$obj->[0]} = $obj; 
  
  return $obj->[0];
}

sub getObj {
  my ($self, $objId, $do_recurse, $recur) = @_; 
  $do_recurse = 1 if !defined($do_recurse); 
  $recur = 0 if !defined($recur); 
  $main::veryverbose && i::logit("DOC::GetObj id=$objId, dorec=$do_recurse, rec=$recur");
  
  my ($INPUT, $TabXref, $largs, $UpdatedObjs, $optimizer) 
                         = @{$self}{qw(INPUT TabXref largs UpdatedObjs optimizer)};

  if ($objId =~ /(\d+)[$cs_spaces]+\d+[$cs_spaces]+R/so) {
    $objId = $1;
  }
  return $UpdatedObjs->{$objId} if $UpdatedObjs and exists($UpdatedObjs->{$objId});

  my $objref = $TabXref->[$objId]; 
  my ($obj, $line, $t, $tlen) = ([$objId], '', '', 0);
  my ($offset, $cat_offset) = @$objref[OBJ_FILE_OFFSET, OBJ_CAT_OFFSET]; 
  die("PDF GETOBJ ERROR DETECTED. OBJECT $objId NOT DEFINED !!") if !defined($offset);
  $INPUT->seek($offset, 0); 
  my $objstream_is_ok = 0; 

  if ( defined($cat_offset) ) {
    my ($cat_len, $str_len) = @$objref[OBJ_CAT_LEN, OBJ_STREAM_LEN]; 
    $INPUT->seek( $cat_offset, 1 );
    $INPUT->read($t, $tlen = $cat_len + $str_len); 
    die("INVALID XREF DATA For OBJECT $objId\n") if length($t) != $tlen;
     
    $obj->[1] = substr($t, 0, $cat_len); $obj->[2] = substr($t, $cat_len) if $str_len;
    if ( $str_len == 0 or $obj->[2] =~ /^stream\s.*\s?endstream\s*$/s) {
      $objstream_is_ok = 1; 
    }
    else {
      $obj = [$objId]; 
      $INPUT->seek($offset, 0); 
    }
  }

  if (!$objstream_is_ok) {
	if ( ($line = $INPUT->getLine()) !~ /^($lx_objbeg)(.*)/so ) {
      die "INVALID OBJ READ obj=$objId at offset=$offset \n $line";
    }
    $line = $2; $cat_offset = length($1); $t = '';
    while(1) {
	  if ($line =~ /^(.*)endobj\s*$/s) {
		$t .= $1 if $1 ne ""; 
		last;
	  }
	  if ( $line =~ /(.*)(stream\s*(?:\x0a|\x0d))/s and $#$obj == 0) {
	    $obj->[1] = $t.$1; 
	    $t = $2.$self->getObjStream($obj->[1]);
	  }
	  else {
        $t .= $line;
	  }
      $line = $INPUT->getLine();
    }
    push @$obj, $t;

    @$objref[OBJ_CAT_OFFSET..OBJ_STREAM_LEN] = ($cat_offset, length($obj->[1]), length($obj->[2]));
    $self->{TabXref}->[$objId] = $objref;
    
  }

  if ($obj->[1] =~ /\s*\[\s*(\d+)\s+\d+\s*R\s*\]\s*$/ and $do_recurse) {
	die("TOO MANY RECURSIVE GETOBJ FOR OBJ $objId") if $recur > 1;
	return $self->getObj($1, $do_recurse, $recur+1);
  }

  $optimizer->optimize_obj($obj) if $optimizer; 
  my $modifyexit = $largs->{'modifyexit'} if ($largs && exists($largs->{'modifyexit'}));
   
  $UpdatedObjs->{$objId} = $obj if $modifyexit and &$modifyexit($obj, $largs); 
  return $obj;
}

#sub PrintObjStream {
  #if ($#$obj > 1) {
  #  my ($stream) = $t =~ /stream\r?\n(.*)\nendstream/s;
  #  print uncompress($stream);
  #}
  #$obj->[1] =~ s/\x0d(?!\x0a)/\x0a/g;
#}


sub getObjTextStream {
  my ($self, $obj, $binmode) = @_; $obj = $self->getObj($obj) if !ref($obj); my $stream;

  if ($#$obj > 1) {
   ($stream) = $obj->[2] =~ /stream\r?\n?(.*)\s?endstream/s;  
    die "obj stream is null" if $stream eq '';
    $stream = uncompress($stream) 
     if 
    $obj->[1] =~ /\/Filter\s*\/FlateDecode/s; $stream =~ s/\x0d([^\x0a])/\x0d\x0a$1/sg if !$binmode;
  }
  else {
    die("getObjTextStream INVALID OBJECT DETECTED !!");
  }
  
  return $stream;
}

#sub getPageValues {
#  my ($self, $pgobj) = (shift, shift); return undef if !$pgobj; my @lvalues;
#
#  while(@_) {
#    my $attribute = shift; my $lcat;
#    while ($pgobj ne '' and $lcat = $self->ParseCatalog($pgobj->[1]) and !exists($lcat->{$attribute})) {
#      ($pgobj) = $pgobj->[1] =~ /\/Parent\s+(\d+)/mso 
#	    or 
#      die("INVALID PAGE CATALOG $pgobj->[1] DETECTED.\n"); $pgobj = $self->getObj($pgobj);
#    }
#    push @lvalues, $lcat->{$attribute};
#  }
#
#  return wantarray ? @lvalues : $lvalues[0];
#}

sub getPageAttribute {
  my ($self, $pageObj, $attribute) = @_; my $Parser = $self->{'Parser'}; my ($obj, $objid, $lattribute);

  my $cobj = $Parser->ParseCatalog($pageObj->[1]);
  
  while(!exists($cobj->{$attribute})) {
   ($objid) = $cobj->{'Parent'} =~ /(\d+)/s or return undef;
    $obj = $self->getObj($objid);
    $cobj = $Parser->ParseCatalog($obj->[1]);
  }

  $lattribute = $cobj->{$attribute};
}

sub getPageBox {
  my $boxType = $_[-1]; my $tbox = getPageAttribute(@_) or return; 
  
  $tbox =~ /^\s*\[[\d\-. ]+\]\s*$/s and my @tbox = $tbox =~ /([^\[\] ]+)/sg; 

  die "INVALID $boxType DETECTED: $tbox" if scalar(@tbox) != 4; return \@tbox;
}

sub getPageMediaBox { getPageBox(@_, 'MediaBox'); }
sub getPageCropBox { getPageBox(@_, 'CropBox'); }

sub getPageGeometry {
  my ($self, $pageObj) = @_; $pageObj = $self->getPageObj($pageObj) if !ref($pageObj); 
  {
    MediaBox => getPageMediaBox($self,$pageObj),
    CropBox => getPageMediaBox($self,$pageObj),
    Rotate => getPageAttribute($self, $pageObj, 'Rotate'),
  };
}

sub getPageObj {
  my ($self, $page) = (shift, shift); my $PageList = $self->{PageList}; 
  
  my ($TotPages, $objId);
  
  $TotPages = scalar(@{$self->{PageList}});
  
  $objId = $PageList->[$page-1] if $page > 0 and $page <= $TotPages;

  die("INVALID PAGE NUMBER REQUESTED: $page (PdfPages: $TotPages)") if $objId eq ''; return $self->getObj($objId);
}

sub getPageTextLines {
  my ($self, $page) = (shift, shift); my $obj = $self->getPageObj($page); my $TextLines;

  my ($cid) = $obj->[1] =~ /\/Contents$lx_sep+(\d+)/;

  $obj = $self->getObj($cid); 
  
  if ($#$obj > 1) {
    my ($stream, $pagetxt, $parser); require XReport::PDF::PageParser;

    ($stream) = $obj->[2] =~ /stream\r?\n(.*)\s?endstream/s;
    $pagetxt = uncompress($stream);
	$pagetxt =~ s/\x0d([^\x0a])/\x0d\x0a$1/g;
	
    $parser = XReport::PDF::PageParser->new();

    $TextLines = $parser->ExtractTextLines($pagetxt);
  }
  else {
    $TextLines = [];
  }
  
  return $TextLines;
}

sub getPageParser {
  my ($self, $page) = (shift, shift); my $pageObj = $self->getPageObj($page);

  require XReport::PDF::PageParser;

  return XReport::PDF::PageParser->new($self, $pageObj);
}

sub digPageList {
    my ($self, $pagelist, $node, $mbox, $rotate ) = @_;
    $mbox = '' unless $mbox; $rotate = '' unless $rotate;
    my $nodeobj = $self->getObj($node)->[1];
    unless ( $mbox ) {
       ($mbox) = $nodeobj =~ /\/MediaBox\s*\[\s*([^\]]*)\s*\]/mso;
       $mbox =~ s/\s{2,}/ /g if $mbox;
    }
    ($rotate) = $nodeobj =~ /\/Rotate\s+(\d+)/mso unless $rotate;

    my ($kids) = $nodeobj =~ /\/Kids\s*\[?\s*([^\]\/>]*)\s*\]?/mso;
    if ( $nodeobj =~ /\/Type\s*\/Page[^s]/so ){
       push @{$pagelist}, $node ;
    }
    elsif ( $nodeobj =~ /\/Type\s*\/Pages/so && $kids ) {
        foreach my $kid ( ( $kids =~ /(\d+) *0 *R/g ) ) {
            ($pagelist, $mbox, $rotate) = $self->digPageList( $pagelist, $kid, $mbox, $rotate);
        }
    }
    return $pagelist, $mbox, $rotate;           
}

sub getPageList {
  my ($self, $root) = (shift, shift); my (@nodes, @leafs); @nodes = ($root); 
  
  my ($MediaBox, $Rotate) = ("", "");
  
  while(my $node = shift @nodes) {
	my $cat = $self->getObj($node)->[1];	
	my ($kids) = $cat =~ /\/Kids\s*\[?\s*([^\]\/>]*)\s*\]?/mso;
	
	if ( $kids ) {
      my ($objref, $count) = $cat =~ /\/Count$lx_sep+(?:($lx_objref)|(\d+))/so;
	  if ( $objref ) {
	    my ($objId) = $objref =~ /^\s*(\d+)/;
	    $count = $self->getObj($objId)->[1];
	  }
	  my @kids = $kids =~ /(\d+) *0 *R/g;
      if ( $count != scalar(@kids) ) {
	    unshift @nodes, @kids;
	  }
	  else {
        for my $kid (@kids) {
	      my $cat = $self->getObj($kid)->[1];
          if ($cat =~ /\/Type\s*\/Pages/) {
	        unshift @nodes, $kid;
          }
          else {
            push @leafs, $kid;    
          }
        }
	  }
	}
	else {
      push @leafs, $node;
	}
	
	($MediaBox) = $cat =~ /\/MediaBox\s*\[\s*([^\]]*)\s*\]/mso, $MediaBox =~ s/\s{2,}/ /g if $MediaBox eq "";
    
	($Rotate) = $cat =~ /\/Rotate\s+(\d+)/mso if $Rotate eq "";
  }
  
  return (\@leafs, $MediaBox, $Rotate);
}

sub GetLastXref {
  my $self = shift; my $INPUT = $self->{INPUT}; my $str = '';
  # paldovini - secondo le specifiche PDF la keyword %%EOF puo' trovarsi negli ultimi 1024 bytes
  $INPUT->seek(-1024, 2); $INPUT->read($str, 1024);
  if ( $str =~ /startxref\s+(\d+)\s+\%\%EOF/s ) {
    #print "last startxref: $1\n";
	return $1;
  }
  die "STARTXREF ERROR <$str> --- !!!";
}

sub ReadXref {
  my ($self, $startxref) = (shift, shift); my $INPUT = $self->{INPUT}; my (@lsegs, $trailer, $line);
  
  $INPUT->seek($startxref,0); $line = $INPUT->getLine();

  die("INVALID STARTXREF DETECTED $line !!") if $line !~ /^xref\s*/;

  while(1) {
    my $line = $INPUT->getLine(); next if($line =~ /^\s*$/);
    if ($line =~ /^trailer/) {
      $line =~ s/^trailer\s*//; $trailer = $line; last; 
    } 
    my ($lobj, $tobjs, $txref) = $line =~ /^(\d+)\s+(\d+)/ 
      or 
    die"READ XREF INVALID LINE DETECTED $line\n"; 
    if ($tobjs) {
      while(1) {
        $INPUT->read($txref, 1); last if $txref eq "" or $txref =~ /^\d/;
      }
      $INPUT->seek(-1,1);
      $INPUT->read($txref, $tobjs * 20); push @lsegs, [$lobj, $tobjs, $txref];
    }
  }
  while(1) {
    my $line = $INPUT->getLine(); last if $line eq ''; $trailer .= $line; last if $line =~ /^$lx_end_trailer$/so;
  }
  
  $trailer =~ /^\s*(?:<<.*>>)$lx_end_trailer$/so 
    or 
  die "READXREF ERROR at ==$trailer== !!!"; return (\@lsegs, $trailer);
}

sub open_lxHandle {
  my $self = shift; my ($lFileName, $liobuffer, $IOXREF) = @{$self}{qw(lFileName liobuffer)};
  if (!$liobuffer) {
    open($IOXREF, "+<$lFileName")
     or 
    die("INPUT/OUTPUT REOPEN ERROR \"$lFileName\" $!"); 
  }
  else {
    $IOXREF = IO::String->new($liobuffer);
  }

  binmode($IOXREF); return $IOXREF;
}

sub creXrefAndPageList {
  my $self = shift;
  my ($INPUT, $iFileName, $lFileName, $liobuffer, $Parser) 
                   = @{$self}{qw(INPUT iFileName lFileName liobuffer Parser)}; 
  
  my ($lTrailer, $PagesObjId, $CatalogObjId, $CatalogStream, $Catalog, $PageList, $MediaBox, $Rotate);
  
  my $startxref = $self->GetLastXref(); 
  my $OUTPUT;

  if (!$liobuffer) {
    unlink $lFileName or die("XREF DELETE/UNLINK ERROR for \"$lFileName\" $!") if -e $lFileName;
  
    $OUTPUT = gensym(); open($OUTPUT, "+>$lFileName") 
                    or die("OUTPUT OPEN ERROR \"$lFileName\" $!"); 
  }
  else {
    $OUTPUT = IO::String->new($liobuffer); 
  }
  binmode($OUTPUT);

  my ($tobjs, $lsize, $ioffset, $tpages, $loffset); 
  ($lsize, $ioffset) = (4, 5+16+10); 
  
  while ($startxref) {
    my ($lsegs, $trailer) = $self->ReadXref($startxref); $lTrailer = $trailer if $lTrailer eq '';
    while(@$lsegs) {
      my ($lobj, $tobjs, $txref, $trailer) = @{shift @$lsegs};
    
      my $lxref = "\x00" x ($tobjs * 5 * $lsize); 
      my $lat2 = 5*$lsize; 
      my ($loff, $lc, $llxref);
      sysseek($OUTPUT, $ioffset+$lobj*$lat2, 0); 
      sysread($OUTPUT, $llxref, length($lxref)); 
      $llxref = $llxref . $lxref if length($llxref) != length($lxref);
      for (my $at=0, my $at2=0, my $to=$tobjs*20; $at<$to; $at+=20, $at2+=$lat2) {
        ($loff, $lc) = (substr($txref, $at, 10), substr($txref, $at+17, 1)); 
        next if $lc ne 'n';
        if (substr($llxref, $at2, 1) eq "\x00") {
          substr($lxref, $at2, 5) = '.' . pack("I", $loff); 
        }
        else {
          substr($lxref, $at2, 5) =  substr($llxref, $at2, 5);
        }
	  }
      sysseek($OUTPUT, $ioffset+$lobj*$lat2, 0); 
      syswrite($OUTPUT, $lxref); 
    }
    
    ($startxref) = $trailer =~ /\/Prev\s*(\d+)/; # prev xref .......
  }
  
  seek($OUTPUT, 0, 2); 
  $loffset = tell($OUTPUT); 
  print $OUTPUT '.', "\xff" x 4; 
  seek($OUTPUT, $ioffset-26, 0); 
  $tobjs = ($loffset-$ioffset) / (5*$lsize);
  print $OUTPUT pack("a16aIaI", "\@TabXref", '.', $tobjs, '.', $loffset); close($OUTPUT); 

  tie my @TabXref,  'XReport::Tie::FileIArray', {
    FileName => $lFileName, 
    size => $tobjs, 
    lsize => $lsize, 
    ioffset => $ioffset, iobuffer => $liobuffer
  }; 
  
  $self->setValues(TabXref => \@TabXref);
  
  ($CatalogObjId) = $lTrailer =~ /\/Root *(\d+)\s+/
        or die("UserError: INVALID PDF DOCUMENT DETECTED. ROOT CATALOG OBJ ID NOT FOUND");
  
  $Catalog = $Parser->ParseCatalog($CatalogStream = $self->getObj($CatalogObjId)->[1]);
  
  ($PagesObjId) = $Catalog->{'Pages'} =~ /(\d+)/ 
    or die("UserError: INVALID PDF DOCUMENT DETECTED. PAGE TREE ROOT NOT FOUND");

#  ($PageList, $MediaBox, $Rotate) = $self->getPageList($PagesObjId); $tpages = @$PageList;
  ($PageList, $MediaBox, $Rotate) = $self->digPageList([], $PagesObjId); $tpages = @$PageList;
  
  $OUTPUT = $self->open_lxHandle(); 
  seek($OUTPUT, 16+10, 2); 
  $ioffset = tell($OUTPUT);
  
  my $lPageList = "\x00" x ($tpages * 5); 
  my $at = 0;
  for (@$PageList) {
    substr($lPageList, $at, 5) = '.'. pack("I", $_); $at += 5;
  }

  syswrite($OUTPUT, $lPageList); 
  
  seek($OUTPUT, 0, 2); $loffset = tell($OUTPUT); print $OUTPUT '.', "\xff" x 4; seek($OUTPUT, $ioffset-26, 0); 
   $tpages == ($loffset-$ioffset) / 5 
    or die "TPAGES $tpages AND lPageList (($loffset-$ioffset) / 5) MISMATCH!";
  print $OUTPUT pack("a16aIaI", "\@PageList", '.', $tpages, '.', $loffset); close($OUTPUT); 
  
  tie my @PageList,  'XReport::Tie::FileIArray', {
    FileName => $lFileName, 
    size => $tpages, 
    lsize => 1, 
    ioffset => $ioffset, iobuffer => $liobuffer
  }; 
  
  $self->setValues(PageList => \@PageList);
  
  $OUTPUT = $self->open_lxHandle(); 
  seek($OUTPUT, 0, 2); 
  $ioffset = tell($OUTPUT);

  print $OUTPUT 
    pack("a16na*", "\$CatalogStream", length($CatalogStream), $CatalogStream),
    pack("a16na*", "\$MediaBox", length($MediaBox), $MediaBox),
    pack("a16na*", "\$Rotate", length($Rotate), $Rotate),
  ;

  seek($OUTPUT, 0, 0); print $OUTPUT "\@XREF"; 
  seek($OUTPUT, 0, 2); print $OUTPUT "\@XREF"; 
  
  close($OUTPUT);
  
  $self->setValues(Catalog => $Catalog, MediaBox => $MediaBox, Rotate => $Rotate);
}

sub VerifyXrefFile {
  my $self = shift; my ($iFileName, $lFileName, $largs) = @{$self}{qw(iFileName lFileName largs)}; 
  
  return undef if !-e $lFileName or !-s $lFileName > 0;

  if (0 and !$largs->{'timexrefisok'}) {
    my @istat = stat($iFileName); my @lstat = stat($lFileName); 

    return undef if $lstat[9] <= $istat[9];
  }
  
  my $INPUT = gensym(); open($INPUT, "<$lFileName") 
   or 
  die("INPUT OPEN ERROR \"$lFileName\"$!"); binmode($INPUT); my $irec = '';

  seek($INPUT,  0, 0); read($INPUT, $irec, 5); return 0 if $irec ne "\@XREF"; 
  seek($INPUT, -5, 2); read($INPUT, $irec, 5); return 0 if $irec ne "\@XREF"; 

  close($INPUT); return 1; 
}

sub getXrefAndPageList {
  my $self = shift;
  my ($INPUT, $iFileName, $Parser) = @{$self}{qw(INPUT iFileName Parser)}; 
  my $lFileName = $iFileName; 

  $lFileName =~ s/\.pdf$//i;
  $lFileName .= '.XREF';
  $self->setValues(lFileName => $lFileName);
  
  my $liobuffer;
  
  if ($iFileName eq ":Scalar") {
    $liobuffer = " " x 4096*100; $liobuffer = ""; $liobuffer = \$liobuffer; require IO::String; 
    $self->setValues(lFileName => ":Scalar", liobuffer => $liobuffer ); 
    $self->creXrefAndPageList();
  }
  
  elsif (!$self->VerifyXrefFile()) {
    require XReport::Locks; my $lck = XReport::Locks->new(); my $lockXref = $self->{'lockXref'};
    
    $lockXref ||= "CreXref.$lFileName"; $lck->getLock($lockXref);
    
    $self->creXrefAndPageList(), 
    $lck->relLock($lockXref), 
    return
     if 
    !$self->VerifyXrefFile(); $lck->relLock($lockXref); 
  }
  
  my ($CatalogObjId, $CatalogStream, $Catalog, $MediaBox, $Rotate);
  
  my ($tobjs, $tpages, $lsize, $ioffset, $loffset, $irec);
  
  $INPUT = $self->open_lxHandle(); $lsize = 4;

  $ioffset = 5 + 26; seek($INPUT, $ioffset - 26, 0); 
  
  read($INPUT, $irec, 26); ($tobjs, $loffset) = unpack("x16xIxI", $irec);

  tie my @TabXref, 'XReport::Tie::FileIArray', {
    FileName => $lFileName, 
    size => $tobjs, 
    lsize => 4, 
    ioffset => $ioffset, iobuffer => $liobuffer
  }; 
  
  $self->setValues(TabXref => \@TabXref);

  $ioffset = $loffset + 5 + 26; seek($INPUT, $ioffset - 26, 0); 
  
  read($INPUT, $irec, 26); ($tpages, $loffset) = unpack("x16xIxI", $irec); $lsize = 1;
  
  tie my @PageList,  'XReport::Tie::FileIArray', {
    FileName => $lFileName, 
    size => $tpages, 
    lsize => 1, 
    ioffset => $ioffset, iobuffer => $liobuffer
  }; 
  
  $self->setValues(PageList => \@PageList);
  
  $ioffset = $loffset + 5; seek($INPUT, $ioffset, 0);
  
  read($INPUT, $irec, 18); $irec = unpack("x16n", $irec); read($INPUT, $CatalogStream, $irec);
  read($INPUT, $irec, 18); $irec = unpack("x16n", $irec); read($INPUT, $MediaBox, $irec);
  read($INPUT, $irec, 18); $irec = unpack("x16n", $irec); read($INPUT, $Rotate, $irec);
  
  $Catalog = $Parser->ParseCatalog($CatalogStream);
  
  $self->setValues(Catalog => $Catalog, MediaBox => $MediaBox, Rotate => $Rotate); return $self;
}

sub PrepareXrefFileLengths {
  my $self = shift; my ($TabXref, $INPUT) = @{$self}{qw(TabXref INPUT)}; 
  die "Undefined TabXref passed by ".join('::', (caller())[0,2]) unless defined($TabXref);
  my $totobjects = scalar(@$TabXref); 
  
  my ($objid, $objref, $offset, $cat_offset, $cat_len, $str_bytes, $str_len); 
  
  my (@offset, @next_offset); 

  if ($totobjects <= 1_000_000) {
    for ($objid = 0; $objid<$totobjects; $objid += 1) {
      $objref = $TabXref->[$objid]; 
      my $offset = $objref->[OBJ_FILE_OFFSET];
      push @offset, [$objid, $offset] if $offset; 
    }

    @offset = sort {$a->[1] <=> $b->[1]} @offset;

    while(@offset) {
      last if scalar(@offset) <= 1;
      $next_offset[$offset[0]->[0]] = $offset[1]->[1];
    }
    continue {
      shift @offset;
    }
  }
  
  for ($objid = 0; $objid<$totobjects; $objid += 1) {
    $objref = $TabXref->[$objid]; 
    
    $offset = $objref->[OBJ_FILE_OFFSET]; next if !$offset; 
    $cat_offset = $objref->[OBJ_CAT_OFFSET]; 
    next if defined($cat_offset);
    
    if (exists($next_offset[$objid])) {
      $INPUT->seek($offset, 0); 
      $INPUT->read(my $t, $next_offset[$objid]-$offset);

      if ($t =~ /^($lx_objbeg)(.*?)(stream\s*(?:\x0a|\x0d).*)$/so) {
        $cat_offset = length($1); 
        $cat_len = length($2); 
        $str_bytes = $3; 
        if ($str_bytes =~ /(.*?)endobj\s*$/so) {
          $str_len = length($1); 
        }
        else {
          die("INVALID OBJECT STREAM DETECTED FOR OBJECT $objref->[0] !\n");
        }
      }
      elsif ($t =~ /^($lx_objbeg)(.*?)endobj\s*$/so) {
        $cat_offset = length($1); 
        $cat_len = length($2); 
        $str_len = 0;
      }
      else {
        die "OBJECT FORMAT NOT PARSED DETECTED. ", substr($t, 0, 128), ":\n" if  $t !~ /Linearized\s+1/; 
        $self->getObj($objid); 
        next;
      };

      @$objref[OBJ_CAT_OFFSET..OBJ_STREAM_LEN] = ($cat_offset, $cat_len, $str_len); 
    }

    else {
      $self->getObj($objid);
      $objref = $self->{TabXref}->[$objid];
    }

    my ($offset, $cat_offset, $cat_len, $str_len) = @$objref[OBJ_FILE_OFFSET..OBJ_STREAM_LEN];
    $INPUT->seek($offset, 0); 

    die "Catalog offset undefined for $objid"  if !defined($cat_offset);
    my ($t, $tlen);
    $INPUT->seek( $cat_offset, 1 );
    $INPUT->read( $t, $tlen = $cat_len + $str_len ); 
    die("INVALID XREF DATA For OBJECT $objid\n") if length($t) != $tlen;
    
    my $obj; 
    $obj->[1] = substr($t, 0, $cat_len);
    $obj->[2] = substr($t, $cat_len) if $str_len;
    if ( $str_len == 0 or $obj->[2] =~ /^stream\s.*\s?endstream\s*$/s) {
      #print "lala objid $objid is valid !!!\n";
    }
    else {
      die "lala objid $objid is not valid - str_len: $str_len\n"; #stream: ".join("\n        ",unpack("(H80)*", $obj->[2]))."\n";
    }
  }

  return $self->{'lFileName'};
}
  

#------------------------------------------------------ OUTPUT

sub getFromXref {
  my ($self, $doc) = (shift, shift); my $FromXref = $self->{FromXref}; my @FromXref;
  $FromXref->{$doc} = {
    FromXref => [], PageXref => {}
  } 
  if !exists($FromXref->{$doc}); $FromXref = $FromXref->{$doc};

  @FromXref = @{$FromXref}{@_ ? @_ : qw(FromXref)};
  
  return wantarray ? @FromXref : $FromXref[0];
}

sub delFromXref {
  my ($self, $doc) = @_; my $FromXref = $self->{FromXref}; delete $FromXref->{$doc};
}
  

sub XrefId {
  my ($self, $FromXref, $obj, $doc, $CheckForAliases) = @_; my $ref = !ref($obj) ? $obj : $obj->[0];
  return $FromXref->[$ref] if exists($FromXref->[$ref]);
  return $FromXref->[$ref] = ($self->{MaxId} += 1)
   if 
  !$CheckForAliases; 
  return $FromXref->[$ref] = ($self->{MaxId} += 1)
   if 
  $obj->[1] =~ /\/Type\s*\/Page/; my $objHashes = $self->{objHashes}; 

  my $len = $obj->[2] ne "" ? length($obj->[1])."/".length($obj->[2]) : length($obj->[1]);
  my $md5 = "$len/" . ($obj->[2] ne "" ? md5(md5($obj->[1]).md5($obj->[2])) : md5($obj->[1]));
    
  if (!exists($objHashes->{$md5})) {
    $objHashes->{$md5} = $FromXref->[$ref] = ($self->{MaxId} += 1);
  }
  else {
    $FromXref->[$ref] = $objHashes->{$md5};
    #print "== $ref == $FromXref->[$ref] == ", length($obj->[1]), "//", length($obj->[2])," ==\n";
  }
    
  return $FromXref->[$ref]; 
}

sub CopyObjs {
  my ($self, $doc, @objList) = @_; my ($largs, $OUTPUT, $ENCRYPT, $TabXref) = @{$self}{qw(largs OUTPUT ENCRYPT TabXref)};
  my ($FromXref) = $self->getFromXref($doc, qw(FromXref)); my $opt = $largs->{'CheckForAliases'}; my $PageObjs = {};

  my $ObjsTo = $doc->{ObjsTo}; $ObjsTo->{$self} = $self if !exists($ObjsTo->{$self});

  for my $obj (@objList) {
    $obj = $doc->getObj($obj, 0) if !ref($obj);
    if (my ($Type) = $obj->[1] =~ /\/Type\s*\/(Page\W*)/) {
      $PageObjs->{$obj->[0]} = 1;
    }
  }
  while (@objList) {
    my $obj = shift @objList; $obj = $doc->getObj($obj, 0) if !ref($obj); my $lobjid = $obj->[0]; 
    
    next if $FromXref->[$lobjid] and $TabXref->[$FromXref->[$lobjid]];
  
    if (my ($Type) = $obj->[1] =~ /\/Type\s*\/(Page\w*)/) {
      if ($Type ne "Page") {
        die("INVALID Type($Type) DETECTED for $lobjid $obj->[1]") 
      }
      if (!exists($PageObjs->{$lobjid})) {
        $self->XrefId($FromXref, $obj, $doc); next;
      }
    }
    my $lobjcat = $obj->[1]; my $lcomplete = 1; 

    if ($opt and $lobjcat =~ /\/Resources/) {
      $self->OptimizeResources($obj, $doc, $FromXref) 
    }

	$obj->[1] =~ s{(-?\d+)($lx_space+\d+$lx_space+R)(?=$lx_space|$lx_delim)} {
	  my ($ref, $lref, $rest) = ($1, $1 > 0 ? $FromXref->[$1] : abs($1), $2);
      $lref = $self->XrefId($FromXref, $ref, $doc) if !$opt and !$lref;
	  if ( $ref > 0 and !$TabXref->[$lref] ) {
	    unshift @objList, $ref; 
	  }
      $lcomplete = 0 if $opt and !$lref; $lref.$rest;
	}gsoe; 
    if (!$lcomplete) { $obj->[1] = $lobjcat; push @objList, $obj; next; }  

    my $objId = $self->XrefId($FromXref, $obj, $doc, $opt); next if $TabXref->[$objId]; 

	$TabXref->[$objId] = $OUTPUT->tell();

    $ENCRYPT and do {
     #todo: crypt using length from dictionary #todo: read well pdf ref 
      $ENCRYPT->encr_obj($objId, 0, \$obj->[1]);
      $ENCRYPT->encr_stream($objId, 0, \$obj->[2]);
    };
	$OUTPUT->write("$objId 0 obj");
    $OUTPUT->write("\n") if $obj->[1] !~ /^\s*[\r\n]/s;
#    $obj->[1] .= "\r\n" if $obj->[1] !~ /\s*[\r\n]$/s;	
    $OUTPUT->write($obj->[1]); 
    $OUTPUT->write("\n") if $obj->[1] !~ /\s*[\r\n]$/s;
    $OUTPUT->write($obj->[2]); 
    $OUTPUT->write("endobj\n") if $obj->[2] !~ /endobj\s*$/so;
  }
}

sub EqualMediaBox {
  my ($self, $doc) = (shift, shift);
  
  if ( $self->{MediaBox} eq "" ) {
    $self->{MediaBox} = $doc->{MediaBox}; $self->{Rotate} = $doc->{Rotate};
  }

  return $self->{MediaBox} eq $doc->{MediaBox} and $self->{Rotate} eq $doc->{Rotate}
}

sub MergeStreams {
  my ($self, $doc, @con) = @_; 

  my ($iobj, $istream, $ostream) = ('','',''); 
  
  for (@con) {
    $iobj = $doc->getObj($_); ($istream) = $iobj->[2] =~ /stream[\r\n]+(.*)[\r\n]?endstream/s;
    $ostream .= " " . uncompress($istream);
  }
  
  $ostream =~ s/\x0d(?!\x0a)/\x0a/g; return $ostream;
}

sub MergeContents {
  my ($self, $doc, $pgobj) = (shift, shift, shift); my $FromXref = $self->getFromXref($doc);

  my ($con) = $pgobj->[1] =~ /\/Contents\s*\[?\s*([^\]\/>]*)\s*\]?/mso;
  
  my @con = $con =~ /(\d+)\s+0\s+R/msog; return 0 if @con <= 1; 
  
  $FromXref->[$con[0]] = $self->AddStream($self->MergeStreams($doc, @con)); 
  
  $pgobj->[1] =~ s/\/Contents\s*\[?\s*([^\]\/>]*)\s*\]?/\/Contents \[$con[0] 0 R\]/ms; return scalar(@con);
}

sub getPageTreeId {
  my ($self, $objid, $count) = @_; my $PageTree = $self->{PageTree};

  if (!@$PageTree) {
    push @$PageTree, [$self->{MaxId} += 1, [], 0];
  }
  my $pgtobj = $PageTree->[-1];

  if (@{$pgtobj->[1]} >= 10) {
    push @$PageTree, $pgtobj = [$self->{MaxId} += 1, [], 0];  
  }
    
  push @{$pgtobj->[1]}, $objid; $pgtobj->[2] += $count; return $pgtobj->[0];
}

sub CopyPages {
  my ($self, $doc) = (shift, shift); my $largs = ref($_[-1]) ? pop @_ : undef; my $Parser = $self->{Parser}; 
  my ($PageList, $TabXref) = @{$self}{qw(PageList TabXref)}; my $FromXref = $self->getFromXref($doc); 
  my $mergecontents = $largs ? $largs->{'mergecontents'} : 0; 
  pop @_ if scalar(@_) % 2 == 1;
  my $EqualMediaBox = $self->EqualMediaBox($doc);
  my $Outlines = exists($doc->{OutlinesTree}) ? 1 : 0; 
  my $PageXref = $Outlines ? $self->getFromXref($doc,qw(PageXref)) : undef; my $lcopied = 0;

  while (@_) {
    my ($fm, $for) = splice(@_,0,2); $for = (($doc->TotPages()+$for+1)-$fm+1) if $for < 0; next if $for <= 0;
    for my $lpage ($fm..($fm+$for-1)) {
      $main::veryverbose && i::logit("CopyPage begin copying page $lpage");
      my $pgobj = $doc->getPageObj($lpage); my ($pgobjId, $pgtobjId) = ($pgobj->[0], 0); 
      delete $FromXref->[$pgobjId] if $TabXref->[$FromXref->[$pgobjId]];

      $self->MergeContents($doc, $pgobj) if $mergecontents;

      $pgtobjId = $self->getPageTreeId(
        $self->XrefId($FromXref, $pgobj, $doc), 1
      );
      $pgobj->[1] =~ s/(\/Parent$lx_sep+)(\d+)/$1-$pgtobjId/so; 
	
      if (!$EqualMediaBox and $pgobj->[1] !~ /\/MediaBox/) {
        my $t = "/MediaBox [$doc->{MediaBox}] " .
         ($doc->{Rotate} ne '' ? " /Rotate $doc->{Rotate}" : '')
        ;
        $pgobj->[1] =~ s/\>\>/ $t\>\>/s;
      }

	  $self->CopyObjs($doc, $pgobj); 
      
      push @$PageList, $FromXref->[$pgobjId]; $Outlines and $PageXref->{$lpage} = scalar(@$PageList); $lcopied += 1;
    }
  }
  return $lcopied;
}

sub getPageResources {
  my ($self, $pgobj) = (shift, shift); return undef if !$pgobj; my $res;

  while ($pgobj ne '' and $pgobj->[1] !~ /\/Resources/) {
    ($pgobj) = $pgobj->[1] =~ /\/Parent\s+(\d+)/mso 
	  or 
    die("INVALID PAGE CATALOG $pgobj->[1] DETECTED.\n"); $pgobj = $self->getObj($pgobj);
  }

  ($res) = $pgobj->[1] =~ /(\/Resources\s*$lx_catalog)/mso 
    or	
  ($res) = $pgobj->[1] =~ /(\/Resources\s*\[?\s*([^\]\/>]*)\s*\]?)/mso; 
  
  $res or die("PROGRAM IS UNABLE TO GET RESOURCES STRING FOR PAGE OBJ $pgobj->[0], $pgobj->[1]");
}

sub PageToXForm {
  my ($self, $doc, $pgobj) = @_; my $FromXref = $self->getFromXref($doc); my (@con, @xolist, $res, $con);

  ($res) = $pgobj->[1] =~ /(\/Resources\s*$lx_catalog)/mso 
    or	
  ($res) = $pgobj->[1] =~ /(\/Resources\s*\[?\s*([^\]\/>]*)\s*\]?)/mso
    or
  $res = $doc->getPageResources($pgobj) if $res eq ''; 

  ($con) = $pgobj->[1] =~ /\/Contents\s*\[?\s*([^\]\/>]*)\s*\]?/mso;

  @con = $con =~ /(\d+)\s+0\s+R/msog;
  
  if (@con == 1) {
	my $obj = $doc->getObj($con[0]); my ($icat) = $obj->[1] =~ /<<(.*)>>/mso; $icat =~ s/([^\s])\//$1 \//g;
	$obj->[1] = 
	  "<</Type /XObject /Subtype /Form /BBox [0 0 32767 32767] $res $icat>>"
	;
	$self->CopyObjs($doc, $obj); push @xolist, $FromXref->[$obj->[0]];
  }
  else {
    my $ostream = compress($self->MergeStreams($doc, @con)); my $length = length($ostream);
    my $ocat = "/Length $length /Filter /FlateDecode";
	my $obj = $doc->getObj($con[0]);
    $obj->[1] = 
	  "<</Type /XObject /Subtype /Form /BBox [0 0 32767 32767] $res $ocat>>"
	;
    $obj->[2] = "stream\n$ostream\nendstream\nendobj\n";
	$self->CopyObjs($doc, $obj); push @xolist, $FromXref->[$obj->[0]];
  }
  
  return @xolist;
}

sub MergePages {
  my ($self, @docList) = @_; my ($idoc, $ipg, @pgList); my ($tCropBox, $tRotate);

  for $idoc (0..$#docList) {
	my ($doc, @tlist) = @{$docList[$idoc]}; $ipg = 0; 

    if (!defined($doc)) {
      push @{$pgList[$ipg]}, [0, 0, [@tlist]]; next:
    }

    while (@tlist) {
      my ($fm, $for) = splice(@tlist,0,2); 
      for ($fm .. ($fm + $for - 1)) {
        my $pgobj = $doc->getPageObj($_); 
		if ( $idoc == 0 ) {
		  $tCropBox = $doc->getPageCropBox($pgobj) || $doc->getPageMediaBox($pgobj);
          $tCropBox = [split(/\s+/, $doc->{'MediaBox'})] if !$tCropBox;
          $tRotate = $doc->getPageAttribute($pgobj, 'Rotate') || 0;
	      $pgList[$ipg] = [[$tCropBox->[2]-$tCropBox->[0], $tCropBox->[3]-$tCropBox->[1]]];	  
		}
		my ($cmX, $cmY) = $pgobj->[1] =~ /CropBox\s*\[\s*([\d.]+)\s+([\d.]+)\s+/mso;
		push @{$pgList[$ipg]}, [$cmX, $cmY, [$self->PageToXForm($doc, $pgobj)]]; $ipg += 1;
      }
    }
  }

  my ($docList, $doc, $iobj, $objList, $contents);
  
  for $docList (@pgList) {
	my ($meX, $meY) = @{shift @$docList}; ($objList, $contents, $iobj) = ('', '', 0);
	
	for $doc (@$docList) {
  	  my ($cmX, $cmY, $XForms) = @$doc; 
	  for (1..scalar(@$XForms)) {
		$iobj += 1;
	    $objList .= "/Fm$iobj $XForms->[$_-1] 0 R ";
	    $contents .= ($cmX || $cmY ? " q 1 0 0 1 ".(-$cmX)." ".(-$cmY)." cm " : " q") . " /Fm$iobj Do Q";
	  }
    }
    
    my $page_id =  $self->AddObj(); my $parent_id = $self->getPageTreeId($page_id, 1);
	
	my $contents_id = $self->AddStream($contents,0); 
	
	my $resources = "<</ProcSet[/PDF/Text/ImageB/ImageC/ImageI]/XObject<<$objList>>>>";

    my $page_catalog = 
      "<</Type/Page/Parent $parent_id 0 R/Resources $resources/Contents $contents_id 0 R/MediaBox [0 0 $meX $meY]/Rotate $tRotate>>"
    ;

    $self->AddPageObj($page_catalog, $page_id);
  }
}

sub AddObj {
  my ($self, $objstr, $objId) = @_; my ($OUTPUT, $TabXref, $ENCRYPT) = @{$self}{qw(OUTPUT TabXref ENCRYPT)};

  $objId = $self->{MaxId} += 1 if !$objId;
  
  if ( $objstr ) {
    $TabXref->[$objId] = $OUTPUT->tell();

    $ENCRYPT and $ENCRYPT->encr_obj($objId, 0, \$objstr);

    $OUTPUT->write("$objId 0 obj\n"); $OUTPUT->write($objstr); $OUTPUT->write("endobj\n");
  }

  return $objId;
}

sub AddStream {
  my ($self, $stream, $compress) = @_; my ($objId, $catalog); my ($OUTPUT, $TabXref, $ENCRYPT) = @{$self}{qw(OUTPUT TabXref ENCRYPT)}; 

  if (ref($stream)) {
    ($objId, $catalog, $stream) = @$stream;
  }
  else {
    $catalog = "<<>>";
  }

  $compress = 1 if !defined($compress); 

  $objId = $self->{MaxId} += 1 if !defined($objId);
  $stream = compress($stream) 
   if
  $compress; 
  $stream .= substr($stream,-1) =~ /\s+/ ? "" : "\n"; my $length = length($stream);
  
  $TabXref->[$objId] = $OUTPUT->tell();

  $ENCRYPT and $ENCRYPT->encr_stream($objId, 0, \$stream); 

  $catalog =~ s/>>\s*$//; $catalog .= "/Length $length" .($compress ? "/Filter/FlateDecode" : "") .">>\n";
  
  $OUTPUT->write("$objId 0 obj\n");
  $OUTPUT->write(
    $catalog
  );
  $OUTPUT->write("stream\n"); $OUTPUT->write($stream); $OUTPUT->write("endstream\n"); $OUTPUT->write("endobj\n"); return $objId;
}

sub AddPageObj {
  my $self = shift; my $PageList = $self->{PageList};

  my $objId = $self->AddObj(@_);
  
  push @$PageList, $objId; return $objId;
}

sub TotObjects {
  my $self = shift; my $TabXref = $self->{TabXref}; return(scalar(@$TabXref));
}

sub TotPages {
  my $self = shift; my $PageList = $self->{PageList}; 
#TODO: verify condition (taken out to sync with old)
  return 0 unless ref($PageList);
  return(scalar(@$PageList));
}

sub WritePageTree {
  my $self = shift; my ($OUTPUT, $TabXref) = @{$self}{qw(OUTPUT TabXref)};
  
  my ($PageList, $MediaBox, $Rotate) = @{$self}{qw(PageList MediaBox Rotate)}; 
  
  while(@{$self->{PageTree}} > 1) {
    my $LeafsList = $self->{PageTree}; $self->{PageTree} = []; my $lobj;
    for $lobj (@$LeafsList) {
      my ($lobjid, $lkids, $lcount) = @$lobj; $TabXref->[$lobjid] = $OUTPUT->tell(); 
      my $pgtobjid = $self->getPageTreeId($lobjid, $lcount);
      $OUTPUT->write(
        "$lobjid 0 obj\n"
       ."<<"
       ." /Type /Pages"
       ." /Count $lcount"
       ." /Parent $pgtobjid 0 R"
       ." /Kids [". join(" ", map {"$_ 0 R"} @$lkids) ."]"
       .">>\n"
       ."endobj\n"
      );
    }
  }

  my $PageTree = $self->{PageTree}->[-1];
  die "PAGETREE not found - caller:".join('::', (caller())[0,2]) unless $PageTree;
  die "PAGETREE not an array - caller:".join('::', (caller())[0,2]) unless ref($PageTree) eq 'ARRAY'; 
  my ($PageTreeId, $PageTreeKids, $PageTreeCount) = @$PageTree; my $PageListCount = @$PageList; 

  if ($PageTreeCount != $PageListCount) { 
    die("PAGE COUNT MISMATCH PageListCount=$PageListCount PageTreeCount=$PageTreeCount");
  } 

  $TabXref->[$PageTreeId] = $OUTPUT->tell(); 

  $OUTPUT->write(
    "$PageTreeId 0 obj\n"
   ."<<"
   ."/Type/Pages"
   ."/Count $PageTreeCount"
   ."/Kids[". join(" ", map {"$_ 0 R"} @$PageTreeKids) ."]"
  );
  $OUTPUT->write(
     ($MediaBox ? " /MediaBox [$MediaBox]\n" : "")
    .($Rotate ? " /Rotate $Rotate\n" : "")
   .">>\n"
   ."endobj\n"
  );
}

sub WriteInfo {
}

sub WriteEncrypt {
  my $self = shift; my ($OUTPUT, $TabXref, $ENCRYPT) = @{$self}{qw(OUTPUT TabXref ENCRYPT)}; return if !$ENCRYPT;

  my ($V, $R, $O, $U, $P, $length) = @{$ENCRYPT->get_encrypt_parms()}{qw(V R O U P Length)};

  my $objId = $self->{Encrypt} = $self->{MaxId} += 1;
  $TabXref->[$objId] = $OUTPUT->tell();

  $OUTPUT->write(
    "$objId 0 obj\n"
   ."<< \n"
   ."/Filter /Standard \n"
   ."/V $V \n"
   ."/R $R \n"
   ."/O ($O) \n"
   ."/U ($U) \n"
   ."/P $P \n"
   ."/Length $length \n"
   .">> \n"
   ."endobj\n"
  );
}

sub WriteAcroFormList {
  my $self = shift; my ($OUTPUT, $TabXref, $AcroFormList) = @{$self}{qw(OUTPUT TabXref AcroFormList)}; return if !$AcroFormList;

  my $objId = $self->{'AcroForm'} = $self->{MaxId} += 1;
  $TabXref->[$objId] = $OUTPUT->tell();
  
  my $Fields = join(" ", map {"$_ 0 R"} @$AcroFormList);
  $OUTPUT->write(
    "$objId 0 obj\n"
   ."<< \n"
   ."/Fields [$Fields]"
   .">> \n"
   ."endobj\n"
  );

}

sub WriteCatalog {
  my $self = shift; my ($OUTPUT, $TabXref, $Outlines, $PageTree, $AcroForm) = @{$self}{qw(OUTPUT TabXref Outlines PageTree AcroForm)};
  
  $TabXref->[1] = $OUTPUT->tell(); my $Pages = $PageTree->[0];

  $OUTPUT->write(
    "1 0 obj\n"
   ."<<\n"
   ."/Type /Catalog\n"
   ."/Pages $Pages->[0] 0 R\n"
   .($Outlines 
     ? "/Outlines $Outlines 0 R\n/PageMode /UseOutlines\n"
     : ""
    )
    .($AcroForm 
      ? "/AcroForm $AcroForm 0 R\n"
      : ""
    )
   .">>\n"
   ."endobj\n"
  );
}

sub ExpandOutlineNode {
  my ($self, $title, $tref, $parent, $selfId, $selfPrev, $selfNext) = @_; 

  my $first = $self->AddObj(); my ($prev, $next) = (undef, $first);
  
  my ($at, $odest) = (undef, undef);

  ($odest, $tref) = @$tref if ref($tref) =~ /ARRAY/;
  
  my @titles = sort(keys(%$tref)); my $count = scalar(@titles);

  while(@titles) {
    my $title = shift @titles; my $dest = $tref->{$title}; $at = $next;
	
	if (scalar(@titles)) {
	  $next = $self->AddObj(); 
	}
	else {
	  $next = undef;
	}
	
	if (!ref($dest)) {
	  $self->AddObj(
	    "<<\n"
	   ."/Title ($title)\n"
	   ."/Dest [$dest 0 R /XYZ null null null]\n"
	   ."/Parent $selfId 0 R\n"
	   .(($prev) ? "/Prev $prev 0 R\n" : "")
	   .(($next) ? "/Next $next 0 R\n" : "")
	   .">>\n"   
	   ,
	   $at
	  );
	}
	else {
      $dest = ($self->ExpandOutlineNode($title, $dest, $selfId, $at, $prev, $next))[1];
	}
	
    $prev = $at; $odest = $dest if !$odest; 
  }
  
  my $last = $at;  

  #todo: add possibility ref points to a page different than the first underneath

  $self->AddObj( 
    "<<\n"
   .(($title ne undef) ? "/Title ($title)\n" : "")
   ."/Dest [$odest 0 R /XYZ null null null]\n"
   ."/Parent $parent 0 R\n"
   ."/Count -$count\n"
   ."/First $first 0 R\n"
   ."/Last $last 0 R\n"
   .(($selfPrev) ? "/Prev $selfPrev 0 R\n" : "")
   .(($selfNext) ? "/Next $selfNext 0 R\n" : "")
   .">>\n"
   ,
   $selfId
  );
  
  return ($selfId, $odest, $count); #add firstpage 
}

sub setOutlines {
  my ($self, $tref) = (shift, shift); my $Outlines = $self->AddObj(); my @t; my $Count;

  for (sort(keys(%$tref))) {
    push @t, [
      $self->ExpandOutlineNode($_, $tref->{$_}, $Outlines, $self->AddObj(), undef, undef)
    ];
    $Count += $t[-1]->[2];
  }

  $self->AddObj( 
    "<<\n"
   ."/Count $Count\n"
   ."/First $t[0]->[0] 0 R\n"
   ."/Last $t[-1]->[0] 0 R\n"
   .">>\n"
   ,
   $Outlines
  );
  
  $self->{Outlines} = $Outlines; 
}

sub getAcroFormListEntry {
  my ($self) = @_; my $AcroFormList = $self->{'AcroFormList'}; push @$AcroFormList, undef; return @$AcroFormList-1; 
}

sub setAcroFormListEntry {
  my ($self, $AcroFormListEntry, $doc, $FieldsParentObjId) = @_; my $AcroFormList = $self->{'AcroFormList'};

  my ($FromXref) = $self->getFromXref($doc, qw(FromXref));
  
  $self->CopyObjs($doc, $FieldsParentObjId);

  $AcroFormList->[$AcroFormListEntry] = $FromXref->[$FieldsParentObjId]; 
}

sub WriteXref {
  my $self = shift; my ($OUTPUT, $TabXref, $ENCRYPT, $Info, $ID) = @{$self}{qw(OUTPUT TabXref ENCRYPT Info ID)};

  my $start = $OUTPUT->tell(); my $size = @$TabXref; my ($lfree, $offset) = (0, 0);

  $OUTPUT->write( 
    "xref\n"
   ."0 $size\n"
   ."0000000000 65535 f \n"
  ); 

  my ($objId, $lobjId, $objIdLast) = (1, 0, $size); 

  while($objId < $objIdLast) {
    while ($objId > $lobjId+1) {
	  $offset = substr("0000000000$lfree",-10);
	  $OUTPUT->write("$offset 00000 f \n");
	  $lobjId++;
	  $lfree = $lobjId;
	}
	$offset = substr("0000000000".$TabXref->[$objId],-10);
	$OUTPUT->write("$offset 00000 n \n");
	$lobjId = $objId;
  }
  continue {
    $objId += 1;
  }
  
  $OUTPUT->write(
    "trailer\n"
   ."<<\n"
   ."/Size $size\n"
   .($Info ? "/Info $Info 0 R\n" : "")
   .($ENCRYPT ? "/Encrypt $self->{Encrypt} 0 R\n" : "")
   ."/Root 1 0 R\n"
   .($ID ? "/ID[<".unpack("H*",$ID->[0])."><".unpack("H*",$ID->[0]).">]\n" : '')
   .">>\n"
   ."startxref\n"
   ."$start\n"
   ."\%\%EOF\n"
  );
}

sub get_iprogr {
  $_[0]->{iprogr} += 1;
}

sub SetValues {
  my $self = shift; while (@_) {$self->{shift} = shift}; return 1;
}

sub GetFontAlias {
  my $self = shift; my $FONTs = $self->{'FONTs'}; require XReport::PDF::FONTs;

  if (!$FONTs) {
    $self->{'FONTs'} = $FONTs = XReport::PDF::FONTs->new($self); 
  }

  $FONTs->GetFontAlias(@_);
}

sub Create {
  my ($className, $OUTPUT, $largs) = (shift, shift, shift); $largs = {} if !$largs; my $ENCRYPT; require IO::File; 
  
  my ($ID, @TabXref, @PageList, @PageTree); my $encrypt = $largs->{'encrypt'}; 

  (lc($encrypt) eq "yes" or ref($encrypt)) and do {
    $ENCRYPT = {
      owner_pass => rand(999999999999999),
      user_pass => '',
      #perms => -1852,
      perms => -1340,
      encr_length => 128,
     (ref($encrypt) ? %$encrypt : ())
    }
  }; 

  $ENCRYPT and do {
    require Digest::MD5; require XReport::PDF::ENCR;

    $ID = [Digest::MD5::md5(time().rand(999999999999999))]; $ID->[1] = $ID->[0]; 
    
    $ENCRYPT = XReport::PDF::ENCR->new(%$ENCRYPT, id => $ID) 
  };

  require XReport::PDF::ResOptimizer if $largs->{'CheckForAliases'};
  
  do {
    my $FileName = $OUTPUT; $OUTPUT = IO::File->new();
    $OUTPUT->open("$FileName", "w") or die("PDF OUTPUT OPEN ERROR \"$FileName\" $!"); 
  }
  if !ref($OUTPUT); 

  my $self = {
	Parser     => XReport::PDF::Parser->new(),
	MaxId      => 1,
	FromXref   => {},
    objHashes  => {},
	TabXref    => \@TabXref,
	PageList   => \@PageList,
    PageTree   => \@PageTree,
    Outlines   => undef,
    iprogr     => 0,
    largs      => $largs,
    ID         => $ID,
    OUTPUT     => $OUTPUT,
    ENCRYPT    => $ENCRYPT, AcroFormList => []
  };

  $OUTPUT->binmode(); $OUTPUT->write("\%PDF-1.3\n%\xe2\xe3\xcf\xd3\n"); bless $self, $className;
}

sub Close {
  my $self = shift; my $TotPages = $self->TotPages();
  $main::veryverbose && i::logit("DOC.pm Close - TotPages: $TotPages - caller: ".join('::', (caller())[0,2]));
  my ($INPUT, $OUTPUT, $optimizer) = @{$self}{qw(INPUT OUTPUT optimizer)};

  if ($OUTPUT) {
    $self->WritePageTree(); $self->WriteInfo();
    $self->WriteAcroFormList()
     if
    $self->{AcroFormList};
    $self->WriteEncrypt()
     if
    $self->{ENCRYPT};
    $self->WriteCatalog(); $self->WriteXref();
	$OUTPUT->close(); delete $self->{'OUTPUT'}; 
    
    my $PageList = $self->{PageList}; my %lPageList = ();

    for(@$PageList) {
      die("INVALID PAGE OBJECT DETECTD $_") if exists($lPageList{$_}); $lPageList{$_} = undef;
    }
  }

  if ($INPUT) {
    map {$_->delFromXref($self)} values(%{$self->{ObjsTo}}); 

	$INPUT->Close(); delete $self->{'INPUT'}; 
    
    my ($iFileName, $lFileName) = @{$self}{qw(iFileName lFileName)};
    for (qw(TabXref PageList)) {
#TODO: verify condition (taken out to sync with old)
      next unless ref($self->{$_}) eq 'HASH';
      untie %{$self->{$_}}; delete $self->{$_};
    }
    unlink $lFileName || print "DELETE ERROR For \"$lFileName\" $!" 
     if 
    $iFileName eq ":Scalar" or $self->{deletexref};
  }

  if ($optimizer) {
    $optimizer->Close(); delete $self->{'optimizer'};
  }

  %$self = (); return $TotPages;
}

sub Open {
  my ($className, $INPUT, $largs) = (shift, shift, shift); my $iFileName; 

  if ( ref($INPUT) ) {
  }
  elsif ( $INPUT ne ":Scalar" ) {
    $iFileName = $INPUT; $INPUT = XReport::PDF::IStream->new($iFileName);
    $INPUT->Open() or die("PDF INPUT OPEN ERROR \"$iFileName\" $!"); 
  }
  else {
    $iFileName = ":Scalar"; $INPUT = XReport::PDF::SStream->new(shift);
  }

  my (
    $lockXref, $outlines, $optimizer, $deletexref, $update
  ) = 
  @{$largs}{qw(lockXref outlines optimizer deletexref update)};

  do {
    require XReport::PDF::Optimizer;
    $optimizer = XReport::PDF::Optimizer->new($optimizer);
  }
  if ref($optimizer) eq 'HASH';

  my $self = {
    INPUT => $INPUT, iFileName => $iFileName, ObjsTo => {},
    largs => $largs,
    lockXref => $lockXref,
    optimizer => $optimizer,
    deletexref => $deletexref,
	Parser => XReport::PDF::Parser->new(), 
    FontTable => {}, FontDescriptors => {}, BaseFonts => {}
  }; 

  bless $self, $className;
  $self->getXrefAndPageList(); 

  if ($outlines) {
    require XReport::PDF::Outlines; $self->ParseOutlines();
  } 

  if ($update) {
    require XReport::PDF::DOC::UPDATE; $self->PrepareForUpdate();
    
  }
  
  return $self;
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
  my $self = shift; $self->Close() if $self->{'INPUT'};
}

1;

__END__
#------------------------------------------------------------
package XReport::PDF::Page;

my $Parser = XReport::PDF::Parser->new(); ### todo: make it a singleton

sub addContent {
}

sub addResource {
}

sub write {
}

sub writeAsForm {
  my $self = shift; my $doc = $self->{doc}; 
}

sub read {
  my ($self, $doc, $page) = @_; 

  my $dictstr = $doc->getPageObj($page)->[1];

  $self->{'doc'} = $doc;

  my $Catalog = $Parser->ParseCatalog($dictstr);

  print "====>", join("\n", %$Catalog), "<====\n";

  $self->{'Catalog'} = $Catalog;

  my (@MediaBox) = $Catalog->{'MediaBox'} =~ /(\d+)/gso;
  my (@Contents) = $Catalog->{'Contents'} =~ /(?:\s*(\d+)\s+\d+\s+R)/gso;
  my (@Resources) = $Catalog->{'Resources'} =~ /(?:\s*(\d+)\s+\d+\s+R)/gso;

  ### todo: MediaBox Contents and Resources as lists

  print "??? $Catalog->{'MediaBox'} / ".join(",",@MediaBox)."\n";
  print "??? $Catalog->{'Contents'} / ".join(",",@Contents)."\n";
  print "??? $Catalog->{'Resources'} / ".join(",",@Resources)."\n";
}

sub new {
  my ($className, $doc) = @_; 

  my $self = bless {doc => $doc}, $className;
}

1;

__END__

todo: 

pagetree construction

inheritable entries:

Resources
MediaBox
CropBox
Rotate

at copy time get their value

add_page 

add to kids of last_parent (second level)

if kids last_parent (second level) >= 10 

create new last_parent (second level)
add it to last_parent (third level)
and so on

at write 
for inheritable
if inheritable value eq parent_value delete it

-------------------------------------------------
parse outlines


=outlines
  my ($OutlinesObjId, $NamesObjId);

  if (($OutlinesObjId) = $CatalogStream =~ /\/Outlines *(\d+)\s+/) {
    print "Outlines at $1\n";
    my $obj = $self->getObj($OutlinesObjId);
    my $Outlines = $Parser->ParseCatalog($obj->[1]);
    print join("<\n", %$Outlines), "\n<<<\n";
    my ($First, $Last) = map {$_ =~ /(\d+)/; $1} @{$Outlines}{qw(First Last)};
    for ($First..$Last) {
      print "...1 $_ //";
      $obj = $self->getObj($_);
      my $Outline = $Parser->ParseCatalog($obj->[1]);
      my ($First, $Last) = map {$_ =~ /(\d+)/; $1} @{$Outline}{qw(First Last)};
      for ($First..$Last) {
        print "...2 $_ //\n";
        $obj = $self->getObj($_);
        print "$obj->[1]\n";
      }
    }
    die join("\n", %$Outlines);
  }

  if (($NamesObjId) = $CatalogStream =~ /\/Names *(\d+)\s+/) {
    print "Names at $1\n";
  }

  die "------------------\n";
=cut



-------------------------------------------------

pagetree

node [to, objid|ref]
[from+count, objref]



