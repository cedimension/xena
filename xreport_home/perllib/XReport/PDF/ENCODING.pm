
package XReport::PDF::ENCODING;

use Carp::Assert qw(assert);
use strict;

my $AdobeBaseEncodings = {
  AdobeStandardEncoding => [qw(
    undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef 
    undef undef undef undef undef undef space exclam quotedbl numbersign dollar percent ampersand quoteright parenleft parenright asterisk lus comma hyphen period 
    slash zero one two three four five six seven eight nine colon semicolon less equal greater question at A B C D E F G H I J K L M N O P Q R S T U V W X Y Z bracketleft 
    backslash bracketright asciicircum underscore quoteleft a b c d e f g h i j k l m n o p q r s t u v w x y z braceleft bar braceright asciitilde undef undef 
    undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef 
    undef undef undef undef undef undef exclamdown cent sterling fraction yen florin section currency quotesingle quotedblleft guillemotleft guilsinglleft guilsinglright 
    ifi lfl undef endash dagger daggerdbl periodcentered undef paragraph bullet quotesinglbase quotedblbase quotedblright guillemotright ellipsis perthousand undef 
    questiondown undef grave acute circumflex tilde macron breve dotaccent dieresis undef ring cedilla undef hungarumlaut ogonek caron mdash undef undef undef undef 
    undef undef undef undef undef undef undef undef undef undef undef undef AE undef ordfeminine undef undef undef undef Lslash Oslash EOE ordmasculine undef undef 
    undef undef undef ae undef undef undef dotlessi undef undef lslash oslash eoe germandbls undef undef undef undef
  )], 

  MacRomanEncoding => [qw(
    undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef 
    undef undef undef undef undef undef space exclam quotedbl numbersign dollar percent ampersand quotesingle parenleft parenright asterisk plus comma hyphen period 
    slash zero one two three four five six seven eight nine colon semicolon less equal greater question at A B C D E F G H I J K L M N O P Q R S T U V W X Y Z bracketleft 
    backslash bracketright asciicircum underscore grave a b c d e f g h i j k l m n o p q r s t u v w x y z braceleft bar braceright asciitilde undef Adieresis Aring 
    Ccedilla Eacute Ntilde Odieresis Udieresis aacute agrave acircumflex adieresis atilde aring ccedilla eacute egrave ecircumflex edieresis iacute igrave icircumflex 
    idieresis ntilde oacute ograve ocircumflex odieresis otilde uacute ugrave ucircumflex udieresis dagger degree cent sterling section bullet paragraph germandbls 
    registered copyright trademark acute dieresis undef AE Oslash undef plusminus undef undef yen mu undef undef undef undef undef ordfeminine ordmasculine undef ae 
    oslash questiondown exclamdown logicalnot undef florin undef undef guillemotleft guillemotright ellipsis undef Agrave Atilde Otilde EOE eoe endash mdash quotedblleft 
    quotedblright quoteleft quoteright divide undef ydieresis Ydieresis fraction currency guilsinglleft guilsinglright ifi lfl daggerdbl periodcentered quotesinglbase 
    quotedblbase perthousand Acircumflex Ecircumflex Aacute Edieresis Egrave Iacute Icircumflex Idieresis Igrave Oacute Ocircumflex undef Ograve Uacute Ucircumflex 
    Ugrave dotlessi circumflex tilde macron breve dotaccent ring cedilla hungarumlaut ogonek caron
  )],

  WinAnsiEncoding => [qw(
    undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef 
    undef undef undef undef undef undef space exclam quotedbl numbersign dollar percent ampersand quotesingle parenleft parenright asterisk plus comma hyphen period 
    slash zero one two three four five six seven eight nine colon semicolon less equal greater question at A B C D E F G H I J K L M N O P Q R S T U V W X Y Z bracketleft 
    backslash bracketright asciicircum underscore grave a b c d e f g h i j k l m n o p q r s t u v w x y z braceleft bar braceright asciitilde undef Euro undef 
    quotesinglbase florin quotedblbase ellipsis dagger daggerdbl circumflex perthousand Scaron guilsinglleft EOE undef Zcaron undef undef quoteleft quoteright 
    quotedblleft quotedblright bullet endash mdash tilde trademark scaron guilsinglright eoe undef zcaron Ydieresis undef exclamdown cent sterling currency yen 
    brokenbar section dieresis copyright ordfeminine guillemotleft logicalnot undef registered macron degree plusminus twosuperior threesuperior acute mu paragraph 
    periodcentered cedilla onesuperior ordmasculine guillemotright onequarter onehalf threequarters questiondown Agrave Aacute Acircumflex Atilde Adieresis Aring AE 
    Ccedilla Egrave Eacute Ecircumflex Edieresis Igrave Iacute Icircumflex Idieresis Eth Ntilde Ograve Oacute Ocircumflex Otilde Odieresis multiply Oslash Ugrave 
    Uacute Ucircumflex Udieresis Yacute Thorn germandbls agrave aacute acircumflex atilde adieresis aring ae ccedilla egrave eacute ecircumflex edieresis igrave 
    iacute icircumflex idieresis eth ntilde ograve oacute ocircumflex otilde odieresis divide oslash ugrave uacute ucircumflex udieresis yacute thorn ydieresis
  )],

  PDFDocEncoding => [qw(
    undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef undef breve caron 
    circumflex dotaccent hungarumlaut ogonek ring tilde space exclam quotedbl numbersign dollar percent ampersand quotesingle parenleft parenright asterisk plus 
    comma hyphen period slash zero one two three four five six seven eight nine colon semicolon less equal greater question at A B C D E F G H I J K L M N O P Q R S T U V 
    W X Y Z bracketleft backslash bracketright asciicircum underscore grave a b c d e f g h i j k l m n o p q r s t u v w x y z braceleft bar braceright asciitilde 
    undef bullet dagger daggerdbl ellipsis mdash endash florin fraction guilsinglleft guilsinglright minus perthousand quotedblbase quotedblleft quotedblright quoteleft 
    quoteright quotesinglbase trademark ifi lfl Lslash EOE Scaron Ydieresis Zcaron dotlessi lslash eoe scaron zcaron undef Euro exclamdown cent sterling currency yen 
    brokenbar section dieresis copyright ordfeminine guillemotleft logicalnot undef registered macron degree plusminus twosuperior threesuperior acute mu paragraph 
    periodcentered cedilla onesuperior ordmasculine guillemotright onequarter onehalf threequarters questiondown Agrave Aacute Acircumflex Atilde Adieresis Aring AE 
    Ccedilla Egrave Eacute Ecircumflex Edieresis Igrave Iacute Icircumflex Idieresis Eth Ntilde Ograve Oacute Ocircumflex Otilde Odieresis multiply Oslash Ugrave Uacute 
    Ucircumflex Udieresis Yacute Thorn germandbls agrave aacute acircumflex atilde adieresis aring ae ccedilla egrave eacute ecircumflex edieresis igrave iacute 
    icircumflex idieresis eth ntilde ograve oacute ocircumflex otilde odieresis divide oslash ugrave uacute ucircumflex udieresis yacute thorn ydieresis
  )]
};

sub getEncodingTable {
  my ($self, $EncodingName, $Differences) = @_; my ($EncodingTables) = @{$self}{qw(EncodingTables)}; my ($EncodingTable, @Differences);
  $EncodingName =~ s/^[\/\\]//;
  $EncodingTable = 
    exists($EncodingTables->{$EncodingName}) ? $EncodingTables->{$EncodingName} : 
    exists($AdobeBaseEncodings->{$EncodingName}) ? $AdobeBaseEncodings->{$EncodingName} :
  assert(0, "EncodingName $EncodingName requested by ".join("::", (caller())[0,2]). " NOT FOUND !!"); 
#  die("EncodingName $EncodingName NOT FOUND !!"); 

  $EncodingTable = [@$EncodingTable]; return $EncodingTable if !defined($Differences);

  $Differences =~ s/\[([^]]*)\]/$1/mso;

  @Differences = $Differences =~ /(\d+|[A-Z,a-z]+)/msgo; my $at = shift;

  for (@Differences) {
    if ($_ =~ /\d+/) {
      $at = $_;
    }
    else {
      $EncodingTable->[$at] = $_; $at += 1;
    }
  }

  return $EncodingTable;
}

sub new {
  my ($class) = @_; bless {EncodingTables => {}}, $class;
}

1;

__END__

my $Encoding = XReport::PDF::Encoding->new();

my $Differences = "[24/breve/caron/circumflex/dotaccent/hungarumlaut/ogonek/ring/tilde 39/quotesingle 96/grave 128/bullet/dagger/daggerdbl/ellipsis/emdash/endash/florin/fraction/guilsinglleft/guilsinglright/minus/perthousand/quotedblbase/quotedblleft/quotedblright/quoteleft/quoteright/quotesinglbase/trademark/fi/fl/Lslash/OE/Scaron/Ydieresis/Zcaron/dotlessi/lslash/oe/scaron/zcaron 160/Euro 164/currency 166/brokenbar 168/dieresis/copyright/ordfeminine 172/logicalnot/.notdef/registered/macron/degree/plusminus/twosuperior/threesuperior/acute/mu 183/periodcentered/cedilla/onesuperior/ordmasculine 188/onequarter/onehalf/threequarters 192/Agrave/Aacute/Acircumflex/Atilde/Adieresis/Aring/AE/Ccedilla/Egrave/Eacute/Ecircumflex/Edieresis/Igrave/Iacute/Icircumflex/Idieresis/Eth/Ntilde/Ograve/Oacute/Ocircumflex/Otilde/Odieresis/multiply/Oslash/Ugrave/Uacute/Ucircumflex/Udieresis/Yacute/Thorn/germandbls/agrave/aacute/acircumflex/atilde/adieresis/aring/ae/ccedilla/egrave/eacute/ecircumflex/edieresis/igrave/iacute/icircumflex/idieresis/eth/ntilde/ograve/oacute/ocircumflex/otilde/odieresis/divide/oslash/ugrave/uacute/ucircumflex/udieresis/yacute/thorn/ydieresis]";

my $EncodingTable = $Encoding->getTable("PDFDocEncoding", $Differences);
die join(",", @$EncodingTable);

__END__

#from pdf references appendix

my $pdf_appendix = <<'EOF';
CHARNAMESTDMACWINPDFCHARNAMESTDMACWINPDF
AA101101101101�AE341256306306�Aacute �347301301�Acircumflex �345302302�Adieresis �200304304�Agrave �313300300�Aring �201305305�Atilde �314303303BB102102102102CC103103103103�Ccedilla �202307307DD104104104104EE105105105105�Eacute �203311311�Ecircumflex �346312312�Edieresis �350313313�Egrave �351310310�Eth � �320320�Euro1 � �200240FF106106106106GG107107107107HH110110110110II111111111111�Iacute �352315315�Icircumflex �353316316�Idieresis �354317317�Igrave �355314314JJ112112112112KK113113113113LL114114114114LLslash350 � �225MM115115115115NN116116116116�Ntilde �204321321OO117117117117OEOE352316214226�Oacute �356323323�Ocircumflex �357324324�Odieresis �205326326�Ograve �361322322�Oslash351257330330�Otilde �315325325PP120120120120QQ121121121121RR122122122122SS123123123123�Scaron � �212227TT124124124124�Thorn � �336336UU125125125125�Uacute �362332332�Ucircumflex �363333333�Udieresis �206334334�Ugrave �364331331VV126126126126WW127127127127XX130130130130YY131131131131�Yacute � �335335�Ydieresis �331237230ZZ132132132132�Zcaron2 � �216231aa141141141141�aacute �207341341�acircumflex �211342342�acute302253264264�adieresis �212344344�ae361276346346�agrave �210340340&ampersand046046046046
CHARNAMESTDMACWINPDFCHARNAMESTDMACWINPDF
�aring �214345345^asciicircum136136136136~asciitilde176176176176*asterisk052052052052@at100100100100�atilde �213343343bb142142142142\backslash134134134134|bar174174174174{braceleft173173173173}braceright175175175175[bracketleft133133133133]bracketright135135135135?breve306371 �030�brokenbar � �246246�bullet267245225200cc143143143143?caron317377 �031�ccedilla �215347347�cedilla313374270270�cent242242242242�circumflex303366210032:colon072072072072,comma054054054054�copyright �251251251�currency250333244244dd144144144144�dagger262240206201�daggerdbl263340207202�degree �241260260�dieresis310254250250�divide �326367367$dollar044044044044?dotaccent307372 �033idotlessi365365 �232ee145145145145�eacute �216351351�ecircumflex �220352352�edieresis �221353353�egrave �2173503508eight070070070070�ellipsis274311205203�emdash320321227204�endash261320226205=equal075075075075�eth � �360360!exclam041041041041�exclamdown241301241241ff146146146146fifi256336 �2235five065065065065flfl257337 �224�florin2463042032064four064064064064/fraction244332 �207gg147147147147�germandbls373247337337`grave301140140140>greater076076076076�guillemotleft253307253253�guillemotright273310273273�guilsinglleft254334213210�guilsinglright255335233211hh150150150150?hungarumlaut315375 �034-hyphen055055055055ii151151151151�iacute �222355355�icircumflex �224356356�idieresis �225357357�igrave �223354354jj152152152152kk153153153153ll154154154154
CHARNAMESTDMACWINPDFCHARNAMESTDMACWINPDF
<less074074074074�logicalnot �302254254llslash370 � �233mm155155155155�macron305370257257-minus � � �212�mu �265265265�multiply � �327327nn1561561561569nine071071071071�ntilde �226361361#numbersign043043043043oo157157157157�oacute �227363363�ocircumflex �231364364�odieresis �232366366oeoe372317234234?ogonek316376 �035�ograve �2303623621one061061061061�onehalf � �275275�onequarter � �274274�onesuperior � �271271�ordfeminine343273252252�ordmasculine353274272272�oslash371277370370�otilde �233365365pp160160160160�paragraph266246266266(parenleft050050050050)parenright051051051051%percent045045045045.period056056056056�periodcentered264341267267�perthousand275344211213+plus053053053053�plusminus �261261261qq161161161161?question077077077077�questiondown277300277277"quotedbl042042042042�quotedblbase271343204214�quotedblleft252322223215�quotedblright272323224216�quoteleft140324221217�quoteright047325222220�quotesinglbase270342202221'quotesingle251047047047rr162162162162�registered �250256256�ring312373 �036ss163163163163�scaron � �232235�section247244247247;semicolon0730730730737seven0670670670676six066066066066/slash057057057057.space040040040040�sterling243243243243tt164164164164�thorn � �3763763three063063063063�threequarters � �276276�threesuperior � �263263�tilde304367230037�trademark �2522312222two062062062062�twosuperior � �262262uu165165165165�uacute �234372372�ucircumflex �236373373�udieresis �237374374�ugrave �235371371
CHARNAMESTDMACWINPDFCHARNAMESTDMACWINPDF
_underscore137137137137vv166166166166ww167167167167xx170170170170yy171171171171�yacute � �375375�ydieresis �330377377�yen245264245245zz172172172172�zcaron2 � �2362360zero060060060060
EOF

my @l = split("\n", $pdf_appendix);

my (@STD, @MAC, @WIN, @PDF);

@STD = @MAC = @WIN = @PDF = map {'undef'} (1..256);

for (@l) {
  next if $_ =~ /^CHAR/;
  my @lhex = $_ =~ /(.*?)([� \d+]{4,})/g;
  print substr($_,0,32), "\n";
  while(@lhex) {
    my ($names, $values) = splice(@lhex, 0, 2); $values =~ s/ //g;
    my ($char, $name) = unpack("aa*", $names);
    my ($std, $mac, $win, $pdf) = $values =~ /(�|\d{3})/g;
    print "== $char == $name = $std / $mac / $win / $pdf ==\n";
    die "88888\n" if oct($std) > 255;
    die "88888\n" if oct($mac) > 255;
    die "88888\n" if oct($win) > 255;
    die "88888\n" if oct($pdf) > 255;
    @STD[oct($std)] = $name if $std =~ /\d/;
    @MAC[oct($mac)] = $name if $mac =~ /\d/;
    @WIN[oct($win)] = $name if $win =~ /\d/;
    @PDF[oct($pdf)] = $name if $pdf =~ /\d/;
  }
}

print "STD => ", join(",", @STD), "\n";
print "MAC => ", join(",", @MAC), "\n";
print "WIN => ", join(",", @WIN), "\n";
print "PDF => ", join(",", @PDF), "\n";

print join(",", scalar(@STD), scalar(@MAC), scalar(@WIN), scalar(@PDF)), "\n";


__END__

