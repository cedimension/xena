
#------------------------------------------------------------
package XReport::PDF::PageParser;

use strict;

use Compress::Zlib;

use Math::Trig qw(acos pi);

use constant eXX  => 0;
use constant eYX  => 1;
use constant eXY  => 2;
use constant eYY  => 3;
use constant atX  => 4;
use constant atY  => 5;

my (%PAGE_OPS);

BEGIN {
  %PAGE_OPS = (
    'b'   => "(closepath, fill,stroke) Close, fill, and stroke path using nonzero winding number 4.10 152",
    'B'   => "(fill, stroke) Fill and stroke path using nonzero winding number rule 4.10 152",
    'b*'  => "(closepath, eofill, stroke) Close, fill, and stroke path using even-odd rule 4.10 152",
    'B*'  => "eofill, stroke Fill and stroke path using even-odd rule 4.10 152",
    'BDC' => "(PDF 1.2) Begin marked-content sequence with property list 8.5 480",
    'BI'  => "Begin in-line image object 4.38 260",
    'BMC' => "(PDF 1.2) Begin marked-content sequence 8.5 480",
    'BT'  => "Begin text object 5.4 286",
    'BX'  => "(PDF 1.1) Begin compatibility section 3.19 84",
    'c'   => "curveto Append curved segment to path (three control points) 4.9 149",
    'cm'  => "concat Concatenate matrix to current transformation matrix 4.7 143",
    'cs'  => "setcolorspace (PDF 1.1) Set color space for nonstroking operations 4.21 198",
    'CS'  => "setcolorspace (PDF 1.1) Set color space for stroking operations 4.21 198",
    'd'   => "setdash Set line dash pattern 4.7 143",
    'd0'  => "setcharwidth Set glyph width in Type 3 font 5.10 303",
    'd1'  => "setcachedevice Set glyph width and bounding box in Type 3 font 5.10 303",
    'Do'  => "Invoke named XObject 4.34 243",
    'DP'  => "(PDF 1.2) Define marked-content point with property list 8.5 480",
    'EI'  => "End in-line image object 4.38 260",
    'EMC' => "(PDF 1.2) End marked-content sequence 8.5 480",
    'ET'  => "End text object 5.4 286",
    'EX'  => "(PDF 1.1) End compatibility section 3.19 84",
    'f'   => "fill Fill path using nonzero winding number rule 4.10 152",
    'F'   => "fill Fill path using nonzero winding number rule (obsolete) 4.10 152",
    'f*'  => "eofill Fill path using even-odd rule 4.10 152",
    'g'   => "setgray Set gray level for nonstroking operations 4.21 199",
    'G'   => "setgray Set gray level for stroking operations 4.21 199",
    'gs'  => "(PDF 1.2) Set parameters from graphics state parameter dictionary 4.7 143",
    'h'   => "closepath Close subpath 4.9 149",
    'i'   => "setflat Set flatness tolerance 4.7 143",
    'ID'  => "Begin in-line image data 4.38 260",
    'j'   => "setlinejoin Set line join style 4.7 143",
    'J'   => "setlinecap Set line cap style 4.7 143",
    'k'   => "setcmykcolor Set CMYK color for nonstroking operations 4.21 199",
    'K'   => "setcmykcolor Set CMYK color for stroking operations 4.21 199",
    'l'   => "lineto Append straight line segment to path 4.9 149",
    'm'   => "moveto Begin new subpath 4.9 149",
    'M'   => "setmiterlimit Set miter limit 4.7 143",
    'MP'  => "(PDF 1.2) Define marked-content point 8.5 480",
    'n'   => "End path without filling or stroking 4.10 152",
    'q'   => "gsave Save graphics state 4.7 142",
    'Q'   => "grestore Restore graphics state 4.7 142",
    're'  => "Append rectangle to path 4.9 149",
    'rg'  => "setrgbcolor Set RGB color for nonstroking operations 4.21 199",
    'RG'  => "setrgbcolor Set RGB color for stroking operations 4.21 199",
    'ri'  => "Set color rendering intent 4.7 143",
    's'   => "closepath, stroke Close and stroke path 4.10 152",
    'S'   => "stroke Stroke path 4.10 152",
    'sc'  => "setcolor (PDF 1.1) Set color for nonstroking operations 4.21 198",
    'SC'  => "setcolor (PDF 1.1) Set color for stroking operations 4.21 199",
    'scn' => "setcolor (PDF 1.2) Set color for nonstroking operations (ICCBased and => special color spaces) 4.21 199",
    'SCN' => "setcolor (PDF 1.2) Set color for stroking operations (ICCBased and special  color spaces) 4.21 199",
    'sh'  => "shfill (PDF 1.3) Paint area defined by shading pattern 4.24 214",
    'T*'  => "Move to start of next text line 5.5 288",
    'Tc'  => "Set character spacing 5.2 280",
    'Td'  => "Move text position 5.5 287",
    'TD'  => "Move text position and set leading 5.5 287",
    'Tf'  => "selectfont Set text font and size 5.2 280",
    'Tj'  => "show Show text 5.6 289",
    'TJ'  => "Show text, allowing individual glyph positioning 5.6 289",
    'TL'  => "Set text leading 5.2 280",
    'Tm'  => "Set text matrix and text line matrix 5.5 288",
    'Tr'  => "Set text rendering mode 5.2 280",
    'Ts'  => "Set text rise 5.2 280",
    'Tw'  => "Set word spacing 5.2 280",
    'Tz'  => "Set horizontal text scaling 5.2 280",
    'v'   => "curveto Append curved segment to path (initial point replicated) 4.9 149",
    'w'   => "setlinewidth Set line width 4.7 143",
    'W'   => "clip Set clipping path using nonzero winding number rule 4.11 156",
    'W*'  => "eoclip Set clipping path using even-odd rule 4.11 156",
    'y'   => "curveto Append curved segment to path (final point replicated) 4.9 149",
    "'"   => "Move to next line and show text 5.6 289",
    '"'   => "Set word and character spacing, move to next line, and 5.6 289",
  );

  %PAGE_OPS = map {quotemeta($_), $PAGE_OPS{$_}} keys(%PAGE_OPS);

}

my $itm = [1,0,0,1,0,0];

my $cs_spaces = quotemeta("\x00\x09\x0a\x0c\x0d\x20");
my $cs_delim = quotemeta("\(\)\<\>\[\]\{\}\/\%");

my $lx_space = qr/[$cs_spaces]/so;
my $lx_delim = qr/[$cs_delim]/so;
my $lx_sep = qr/[$cs_spaces$cs_delim]/so;
my $lx_regular = qr/[^$cs_spaces$cs_delim]/so;
my $lx_testprfx = qr/([$cs_spaces]*)(([^$cs_spaces]).*)/so;

my $lx_number = qr/-?(?:[0-9]+\.[0-9]*|\.[0-9]+|[0-9]+)(?=$lx_sep|(?:$\))/so;
my $lx_name = qr/\/$lx_regular+/so;
#my $lx_hexstring  = qr/\<[0-9a-fA-F$cs_spaces]*\>/so;
my $lx_objref = qr/(\d+)$lx_space+\d+$lx_space+R(?=$lx_sep|(?:$\))/so;
my $lx_objbeg = qr/(\d+)$lx_space+\d+$lx_space+obj(?=$lx_sep|(?:$\))/so;

my $lx_length = qr/\/Length[$cs_spaces]+(\d+[$cs_spaces]0[$cs_spaces]+R)|(\d+)/so;

my $lx_hexstring  = qr/\<[0-9a-fA-F$cs_spaces]*\>(?=$lx_sep|(?:$\))/so;

my $lx_pdfop = eval("qr/(?:". join("|",keys(%PAGE_OPS)) .")(?=$lx_sep|(?:$\))/so");


sub skip_image {
  my $self = shift; my ($tref, $atpos) = @{$self}{qw(tref atpos)};
  
  my ($image) = substr($$tref, $atpos) =~ /(.*?\sEI)\s/s;

  my $limage = length($image);
  
  $self->{'atpos'} += length($image);
}

sub NextOp {
  my $self = shift; my ($tref, $atpos) = @{$self}{qw(tref atpos)}; 
  
  my ($c, $atmax, $token, $last, @ol);

  $atmax = length($$tref);
  
  while(1) {
    while( substr($$tref, $atpos, 1) =~ /$lx_space/ ) {
	  $atpos++;
	}
	return undef if ( $atpos >= $atmax );
	
    $c = substr($$tref, $atpos++, 1); $token = $c; 
  
    if ( $c eq "(" ) {
      my $atpar = 1;
	  while( $atpos < $atmax ) {
	    $c = substr($$tref, $atpos++, 1);
	    if ($c eq '(') {
	      $atpar++;
	    }
	    elsif ($c eq ')') {
	      $atpar--;
	    }
		elsif ($c eq "\\") {
		  $c = substr($$tref, $atpos++, 1);
		}
		$token .= $c;
	    last if $atpar == 0;
	  }
	  die "ERROR: UNCLOSED STRING $token" if $atpar != 0;
	  $token = substr($token,1,length($token)-2);
    }
    elsif ( $c eq "<" ) {
	  while( $atpos < $atmax) {
	    $c = substr($$tref, $atpos++, 1);
		$token .= $c;
		last if $c eq ">";
	  }
	  die "INVALID HEXSTRING $token" if $token =~ /^lx_hexstring$/;
	  $token =~ s/[^0-9abcdef]//ig; $token = pack("H*",$token);
    }
    elsif ( $c eq "/" ) {
	  while( $atpos < $atmax) {
	    $c = substr($$tref, $atpos, 1); last if $c =~ /$lx_sep/;
	    $token .= $c; $atpos+=1;
	  }
	  die "INVALID NAME $token" if $token =~ /^lx_regular$/;
    }
	elsif ( $c eq "[" ) {
      next;
    }
    elsif ( $c eq "]" ) {
      next;
    }
	else {
	  while( $atpos < $atmax ) {
	    $c = substr($$tref, $atpos, 1); last if $c =~ /$lx_sep/;
	    $token .= $c; $atpos+=1;
	  }
      if ( $token =~ /^($lx_number)/so ) {
      }
      elsif ( $token =~ /^(true|false)/so ) {
      }
      elsif ( $token =~ /^(null)/so ) {
      }
      elsif ( $token =~ /^($lx_pdfop)/so ) {
	    $last = 1;
      }
      else {
        die "ERROR PARSING at <$token>";
      }
	}
	push @ol, $token; last if $last;
  }
  
  $self->{atpos} = $atpos; return @ol;
}

sub getTextParser {
  my ($self, $tref) = @_; bless {tref => $tref, atpos => 0}; 
}

sub _getAngle {
  my $tm = shift; my $angle;die "???? $tm ???\n" if !ref($tm);

  if ($tm->[eYX] + $tm->[eXY] == 0) {
    $angle = $tm->[eXX] > 0 ? 0 : 180;
  } 
  elsif ($tm->[eXX] + $tm->[eYY] == 0) {
    $angle = $tm->[eYX] > 0 ? 90 : 270;
  }
  else {
    $angle = acos($tm->[eXX]/sqrt($tm->[eXX]**2+$tm->[eYX]**2))/pi*180; 
    
    $angle = 360 - $angle if $tm->[eYX] < 0;
  }
  
  return $angle;
}


sub tm_multiply {
  my ($ts, $tf) = @_; my @ts = @$ts; my @tf = @$tf; 
  
  my @tc;
   
  $tc[0] = $ts[0]*$tf[0] + $ts[1]*$tf[2]; 
  $tc[1] = $ts[0]*$tf[1] + $ts[1]*$tf[3];
  $tc[2] = $ts[2]*$tf[0] + $ts[3]*$tf[2];
  $tc[3] = $ts[2]*$tf[1] + $ts[3]*$tf[3];
  $tc[4] = $ts[4]*$tf[0] + $ts[5]*$tf[2]; 
  $tc[5] = $ts[4]*$tf[1] + $ts[5]*$tf[3]; 

  $tc[4] += $tf[4]; $tc[5] += $tf[5]; 
  
  return \@tc; 
}

sub _TextItem {
  my ($self, $textList, $cm, $tm, $Tc, $Tw, $FontObjId, $FontSize) = @_; my $doc = $self->{'doc'}; my $TextItem;
  
  my $Font = $doc->getFont($FontObjId);
  
  my ($cwidths, $cdescr) = ($Font->getWidths(), $Font->getDescr());
  
  $textList = [$textList, 0] if !ref($textList); push @$textList, 0 if @$textList % 2 == 1;
  
  my $deltaX = 0; my ($text, $tdisplay); my ($Tc_, $Tw_) = ($Tc * 1000, $Tw * 1000);  

  for (my $i=0; $i<=$#$textList; $i+=2) {
    $text = $textList->[$i]; $tdisplay .= $text; 
    for (0..length($text)-1) {
      $deltaX += $cwidths->[unpack("C", substr($text, $_, 1))] + $Tc_; 
      $deltaX += $Tw_ if substr($text, $_, 1) eq "\x20" 
    }
    die ("INVALID textList element DETECTED ($textList->[$i+1])\ntextList:", join(",", @$textList),":\n") 
     if 
    $textList->[$i+1] !~ /^[\d\.-]+/;
    #todo: test if to add a blank depending on width of i or space
    $deltaX -= $textList->[$i+1]; $tdisplay .= " " if $textList->[$i+1] < -97;
  }
  $deltaX *= $FontSize / 1000; 

  my (
    $ascent, $descent
  ) = 
  @{$cdescr}{qw(Ascent Descent)}; 
  
  $ascent *= $FontSize/1000; $descent *= $FontSize/1000;

  my @lpoints = ([0,$descent], [$deltaX,$descent], [$deltaX, $ascent], [0,$ascent]); 

  my $trm = tm_multiply($tm, $cm); my $gs = [[@$trm], $Tc, $Tw]; my $angle = _getAngle($trm);

  for (@lpoints) {
    my ($X0, $Y0) = @$_; my ($X1, $Y1); 
    $X1 = $trm->[eXX]*$X0 + $trm->[eXY]*$Y0 + $trm->[atX];
    $Y1 = $trm->[eYX]*$X0 + $trm->[eYY]*$Y0 + $trm->[atY];
    @$_ = ($X1, $Y1);
  }

  $TextItem = [$textList, $gs, $FontObjId, $FontSize, $tdisplay, $angle, \@lpoints];

  $tm->[atX] += $tm->[eXX]*$deltaX; $tm->[atY] += $tm->[eYX]*$deltaX; 
  
  return $TextItem;
}

sub _eTextItem {
  my ($self, $TextItem, $from, $length) = @_; my $doc = $self->{'doc'}; my $otextList = [];

  die "negative length detected: $length" 
   if 
  $length < 0; 

  my ($itextList, $gs, $FontObjId, $FontSize) = @$TextItem; 

  my ($trm, $Tc, $Tw) = @$gs;
  
  my $cwidths = $doc->getFont($FontObjId)->getWidths();

  my $expX = sqrt($trm->[eXX]**2+$trm->[eYX]**2) * $FontSize / 1000;

  $from /= $expX; $length /= $expX; my $to = $from + $length; my $deltaX = 0;

  for (my $i=0; $i<=$#$itextList; $i+=2) {
    my $itext = $itextList->[$i]; my $otext = ''; my $c;
    for (0..length($itext)-1) {
      $deltaX += $cwidths->[unpack("C", $c = substr($itext, $_, 1))]; 
      $otext .= $c if $deltaX >= $from;
      last if $deltaX >= $to;
    }
    $deltaX -= $itextList->[$i+1]; next if $deltaX <= $from; 
    
    push @$otextList, $otext, $itextList->[$i+1] if $deltaX >= $from; last if $deltaX >= $to;  
  }
  
  return $self->_TextItem($otextList, $itm, $trm, $Tc, $Tw, $FontObjId, $FontSize);
}

sub _ImageItem {
  my ($self, $obj, $tm) = @_; my $tdisplay = "IMAGE (PDF OBJECT ID $obj->[0])";
  
  my $angle = _getAngle($tm);

  my @lpoints = ([0,0], [1,0], [1,1], [0,1]); 

  for (@lpoints) {
    my ($X0, $Y0) = @$_; my ($X1, $Y1); 
    $X1 = $tm->[eXX]*$X0 + $tm->[eXY]*$Y0 + $tm->[atX];
    $Y1 = $tm->[eYX]*$X0 + $tm->[eYY]*$Y0 + $tm->[atY];
    @$_ = ($X1, $Y1);
  }

  return ['imageItem', [@$tm], undef, undef, $tdisplay, $angle, \@lpoints];
}

sub _eImageItem {
  my ($self, $ImageItem, $from, $length) = @_; return $ImageItem;
}

sub TextItems {
  my ($self, $PageText, $cres, $icm) = @_; my ($doc, $Parser) = @{$self}{qw(doc Parser)};
  
  my $cm = [@$icm]; my ($tm, $ltm) = ( [ @$itm ], [ @$itm ] );  
  
  my ($FontObjId, $FontSize); my ($tl, $Tc, $Tw) = (0, 0, 0); 
  
  my (@gsstack, @TextItems); push @gsstack, [[@$cm], $Tc, $Tw];

  my ($lfonts) = $cres->{'Font'} =~ /<<([^>]*)>>/so;
  my ($lxobjects) = $cres->{'XObject'} =~ /<<([^>]*)>>/so;

  my %FontTable = $lfonts =~ /(\w+)$lx_sep(\d+)/gso; 
  my %XObjectTable = $lxobjects =~ /(\w+)$lx_sep(\d+)/gso; 

  my $TextParser = $self->getTextParser($PageText); 
  
  my $textmode = 0; my $pathmode = 0; my $imagemode = 0;

  while(1) {
    my @oargs = $TextParser->NextOp(); my $op = pop @oargs; last if $op eq ""; 

    TEST_OBJECT: 
     goto TEXT_OBJECT if $textmode;
     goto PATH_OBJECT if $pathmode;
    
    PAGE_LEVEL: 
     #Special graphics state q, Q, cm 
     if ( $op eq "q" ) {
       push @gsstack, [[@$cm], $Tc, $Tw];
 	 }
     
     elsif ( $op eq "Q" ) {
       ($cm, $Tc, $Tw) = @{pop @gsstack};
 	 }
     
     elsif ( $op eq "cm" ) {
       $cm = tm_multiply([@oargs], $cm);
 	 }
          
     #Text object: BT
     elsif ( $op eq "BT" ) {
       goto TEXT_OBJECT;
     }

     #Path object: m, re
     elsif ( $op =~ /^(?:m|re)$/ ) {
       goto PATH_OBJECT;
     }
     
     #XObjects: Do
     elsif ($op eq "Do" and my $XObjectName = substr($oargs[-1], 1)) {
       my $obj = $doc->getObj($XObjectTable{$XObjectName});
       my $cobj = $Parser->ParseCatalog($obj->[1]);
       
       if ($cobj->{'Subtype'} eq "Form") {
         my $cres = $doc->getParsedCatalog($cobj->{'Resources'});
         my $PageText = $doc->getObjStream($obj);
         my $TextItems = $self->TextItems(
           \$PageText, $cres, $cm
         );
         push @TextItems, @$TextItems;
       }
       
       elsif ($cobj->{'Subtype'} eq "Image") {
         push @TextItems, $self->_ImageItem($obj, $cm);
       }
       
       else {
         die "XObject Subtype NOT MANAGED !!\n", join("\n", %$cobj), "\n";
       }
 	 }
     
     #Image object: BI
     elsif ( $op eq "BI" ) {
       goto IMAGE_OBJECT;
     }

     #General graphics state w, J, j, M, d, ri, i, gs
     elsif( $op =~ /^(?:w|J|j|M|d|ri|i|gs)$/ ) {
       goto GENERAL_GRAPHICS_STATE;
     }

     #Color CS, cs, SC, SCN, sc, scn, G, g, RG, rg, K, k    
     elsif( $op =~ /^(?:CS|cs|SC|SCN|sc|scn|G|g|RG|rg|K|k)$/ ) {
       goto COLOR;
     }

     #Shading patterns sh
     elsif ( $op eq "sh" ) {
       goto SHADING;
     }

     #Marked content MP, DP, BMC, BDC, EMC
     elsif( $op =~ /^(?: MP|DP|BMC|BDC|EMC)$/ ) {
       goto MARKED_CONTENT;
     }

     #Compatibility BX, EX
     elsif ( $op =~ /^(?:BX|EX)$/ ) {
       goto COMPATIBILITY;
     }

     else {
       #print "pagemode $op:", join(",", @oargs), "\n";
     }
    next;
    

    PATH_OBJECT:
     #Path construction m, l, c, v, y, h, re 
     if ( $op =~ /^(?:m|l|c|v|y|h|re)$/ ) {
       $pathmode = 1;
     }

     #Clipping paths W, W*
     elsif ( $op =~ /^(?:W|W*)$/ ) {
     }

     #Path painting S, s, f, F, f*, B, B*, b, b*, n
     elsif ( $op =~ /^(?:S|s|f|F|f*|B|B*|b|b*|n)$/ ) {
       $pathmode = 0;
     }

     else {
       die "pathmode $op:", join(",", @oargs), "\n";
     }
    next;
 
    
    IMAGE_OBJECT:
     #Inline images BI, ID, EI 
     if ( $op eq "BI" ) {
       $TextParser->skip_image();
 	 }
    next;


    TEXT_OBJECT:
     #-- TEXT SHOWING ------------------------------------------------------------------------- Tj, TJ, ', "
     if ( $op eq "Tj" ) {
       push @TextItems, $self->_TextItem($oargs[0], $cm, $tm, $Tc, $Tw, $FontObjId, $FontSize);
     }
     
     elsif ( $op eq "TJ" ) {
       push @TextItems, $self->_TextItem([@oargs], $cm, $tm, $Tc, $Tw, $FontObjId, $FontSize);
     }
     
     elsif ( $op eq "'" ) {
       die "op ' to be implemented";
     }
     
     elsif ( $op eq "\"" ) {
       die "op \" to be implemented";
     }
     
     #-- TEXT POSITIONING --------------------------------------------------------------------- Td, TD, Tm, T*
     elsif ( $op eq "Td" ) {
       $tm->[atX] += $oargs[0]; $tm->[atY] += $oargs[1]; 
     }
     
     elsif ( $op eq "TD" ) {
 	   $ltm->[atX] += $oargs[0]*$ltm->[eXX]; 
       $ltm->[atY] += $oargs[1]*$ltm->[eYY]; 
       $tl = $oargs[1]*$ltm->[eYY]; 
       @$tm = @$ltm;
 	 }
     
     elsif ( $op eq "Tm" ) {
       @$tm = @oargs; @$ltm = @oargs;
 	 }
     
     elsif ( $op eq "T*" ) {
       $ltm->[atY] += $tl; @$tm = @$ltm; 
 	 }
     
     #-- TEXT STATE --------------------------------------------------------------------------- Tc, Tw, Tz, TL, Tf, Tr, Ts 
     elsif ( $op eq "Tc" ) {
       $Tc = $oargs[0];
     } 
     
     elsif ( $op eq "Tw" ) {
       $Tw = $oargs[0];
     } 
     
     elsif ( $op eq "Tz" ) {
       die("Text horizontal scaling NOTMANAGED"); #todo: manage Tz
     } 
     
     elsif ( $op eq "Tf" ) {
       ($FontObjId, $FontSize) = @oargs; $FontObjId = substr($FontObjId, 1); 
       
       $FontObjId = $FontTable{$FontObjId};
 	 }
     
     elsif ( $op eq "Tr" ) {
       die "op Tr to be implemented";
     }
     
     elsif ( $op eq "Ts" ) {
       die "op Ts to be implemented";
     }

     #-- GENERAL GRAPHIC STATE ---------------------------------------------------------------- w, J, j, M, d, ri, i, gs
     elsif( $op =~ /^(?:w|J|j|M|d|ri|i|gs)$/ ) {
       goto GENERAL_GRAPHICS_STATE;
     }

     #-- COLOR -------------------------------------------------------------------------------- CS, cs, SC, SCN, sc, scn, G, g, RG, rg, K, k
     elsif( $op =~ /^(?:CS|cs|SC|SCN|sc|scn|G|g|RG|rg|K|k)$/ ) {
       goto COLOR;
     }
          
     #-- TEXT OBJECT -------------------------------------------------------------------------- BT, ET
     elsif ( $op eq "BT" ) {
       ($tm, $ltm) = ([@$itm], [@$itm]); $tl = 0; $textmode = 1; 
 	 }
 
     elsif ( $op eq "ET" ) {
       ($tm, $ltm) = ([@$itm], [@$itm]); $tl = 0; $textmode = 0;
 	 }

     else {
       die "tmode $op:", join(",", @oargs), "\n";
     }
    next;
    

    COLOR:
     #Color CS, cs, SC, SCN, sc, scn, G, g, RG, rg, K, k    
    next; 
     
    GENERAL_GRAPHICS_STATE:
     #General graphics state w, J, j, M, d, ri, i, gs
    next; 
     
    SHADING:
     #Shading patterns sh
    next;

    MARKED_CONTENT:
     #Marked content MP, DP, BMC, BDC, EMC
    next;

    COMPATIBILITY:
     #Compatibility BX, EX
    next;
  }

  return \@TextItems;
}

sub ExtractTextItems {
  my $self = shift; my ($cpage, $doc) = @{$self}{qw(cpage doc)}; 
  
  my $cres = $doc->getParsedCatalog($cpage->{'Resources'}); 
  my ($cid) = $cpage->{'Contents'} =~ /(\d+)/; 

  my $obj = $doc->getObj($cid); 
  
  my $PageText = $doc->getObjStream($obj);

  #open(OUT, ">zz.lst"); binmode(OUT); print OUT $PageText; close(OUT);
  
  $self->{'TextItems'} = $self->TextItems(\$PageText, $cres, [1,0,0,1,0,0]);  
}

sub distance {
  my ($p1,$p2) = @_; sqrt(($p2->[0]-$p1->[0])**2+($p2->[1]-$p1->[1])**2);
}

sub max {
  $_[0] >= $_[1] ? $_[0] : $_[1];
}

sub min {
  $_[0] <= $_[1] ? $_[0] : $_[1];
}

sub getTextItems {
  my ($self, $rectangle) = @_; my $TextItems = $self->{'TextItems'}; my @TextItems = ();

  return $TextItems if !$rectangle; my $mb = $self->{'MediaBox'}; 

  my ($llX, $llY, $urX, $urY) = @$rectangle; $llY = $mb->[3] - $llY; $urY = $mb->[3] - $urY;

  for my $TextItem (@$TextItems) {
    my $lpoints = $TextItem->[-1]; my $langle = $TextItem->[-2];
    
    my ($lX, $lY) = @{$lpoints->[0]}; my ($uX, $uY) = @{$lpoints->[2]};
    
    ($lX, $uX) = ($uX, $lX) if $lX > $uX; next if $uX < $llX or $lX > $urX;
    ($lY, $uY) = ($uY, $lY) if $lY > $uY; next if $uY < $llY or $lY > $urY;

    if (ref($TextItem->[0])) {
      if (($langle == 0 or $langle == 180) and ($lX < $llX or $uX > $urX)) { 
        $TextItem = $self->_eTextItem(
          $TextItem, 
          $langle == 0 ? ($llX-$lX, $urX-$llX) : ($llX-$uX, $urX-$llX)
        );
      }
      elsif (($langle == 90 or $langle == 270) and ($lY < $llY or $uY > $urY)) {
        $TextItem = $self->_eTextItem(
          $TextItem, 
          $langle == 90 ? ($llY-$lY, $urY-$llY) : ($llY-$uY, $urY-$llY)
        );
      }
      elsif ($langle % 90 != 0) {
        require Math::Geometry::Planar;  Math::Geometry::Planar->import('SegmentIntersection');

        my $ipoints = [ [$llX,$llY], [$urX,$llY], [$urX,$urY], [$llX,$urY] ];

        my $ipoly = Math::Geometry::Planar->new(); $ipoly->points($ipoints); 

        my $lseg1 = [@$lpoints[0,1]]; my $lseg2 = [@$lpoints[3,2]];

        my $cpoints1 = [@$lseg1]; my $cpoints2 = [@$lseg2];

        for (0..3) {
          my ($p1,$p2) = @$ipoints[$_, $_ < 3 ? $_+1 : 0]; my $ip;
          if ($ip = SegmentIntersection([$p1,$p2, @$lseg1])) {
            push @$cpoints1, $ip;
          }
          if ($ip = SegmentIntersection([$p1,$p2, @$lseg2])) {
            push @$cpoints2, $ip;
          }
        }

        @$cpoints1 = sort { $a->[0] != $b->[0] ? $a->[0] <=> $b->[0] : $a->[1] <=> $b->[1]; } @$cpoints1; 
        @$cpoints2 = sort { $a->[0] != $b->[0] ? $a->[0] <=> $b->[0] : $a->[1] <=> $b->[1]; } @$cpoints2; 

        my ($from, $to, $issubstring) = (distance(@$lseg1), 0, 1);
        
        for ([$lseg1, $cpoints1], [$lseg2, $cpoints2]) {
          my ($lseg, $cpoints) = @$_; my ($p1, $p2, $p3, $p4) = @$cpoints;

          if (scalar(@$cpoints) == 4) {
            $from = min($from, distance(
              $lseg->[0] < $lseg->[1] ? ($p1,$p2) : ($p3,$p4)
            )); 
            $to = max($to, distance($p2,$p3));
          }
          elsif (scalar(@$cpoints) == 3) {
            if ($ipoly->isinside($lseg->[0])) {
              $from = 0; $to = max($to, distance($p1 eq $lseg->[0] ? ($p1,$p2) : ($p2,$p3)));
            }
            else {
              $from = min($from, distance($p1 eq $lseg->[1] ? ($p1,$p2) : ($p2,$p3))); 
              $to = max($to, distance($p1 eq $lseg->[1] ? ($p2,$p3) : ($p1,$p2)));
            }
          }
          else {
            $issubstring = 0;
          }
        }
          
        $TextItem = $self->_eTextItem($TextItem, $from, $to-$from) if $issubstring;
      }
    }
    
    push @TextItems, $TextItem;
  }
  
  return \@TextItems;
}

sub getTextLines {
  my $self = shift; my $TextItems = $self->getTextItems(@_); my @TextLines;

  @TextLines = map {"$_->[-2], /$_->[-3]/"} @$TextItems;

  return \@TextLines;
}

sub new {
  my ($className, $doc, $pageObj) = @_; my $self = bless {}, $className;
  
  my $Parser = XReport::PDF::Parser->new();

  my $cpage = $Parser->ParseCatalog($pageObj->[1]);

  %$self = (
    doc => $doc, pageObj => $pageObj, Parser => $Parser,  
    
    cpage     => $cpage, 
    
    MediaBox  => $doc->getPageMediaBox($pageObj), 
    CropBox   => $doc->getPageCropBox($pageObj)
  );

  $self->ExtractTextItems(); return $self;
}

1;

=gpc way
        my $iangle = $angle; $iangle -= 180 if $iangle >= 180;
        
        my $lpoly = Math::Geometry::Planar->new(); 
        $lpoly->points($lpoints);
        my $lgpc = $lpoly->convert2gpc();
        
        my $ipoly = Math::Geometry::Planar->new(); 
        $ipoly->points($ipoints); 
        my $igpc = $ipoly->convert2gpc();
        
        my $cgpc = GpcClip('INTERSECTION', $lgpc, $igpc); 
        my $cpoly = ((Gpc2Polygons($cgpc))[0]->get_polygons(0))[0];

        for (0..scalar(@$cpoly)-1) {
          my ($p1,$p2) = @$cpoly[$_, $_ < $#$cpoly ? $_+1 : 0];
          
          my $dx = $p2->[0]-$p1->[0];
          my $dy = $p2->[1]-$p1->[1];
          
          my $l = sqrt($dx**2+$dy**2); next if $l == 0;

          my $cangle = acos($dx/$l) / pi*180; $cangle -= 180 if $cangle >= 180;
          
          print "$iangle // $cangle : $dx : $l // $p1->[0] $p1->[1] // $p2->[0] $p2->[1]\n";
        }
=cut

=comment
 remember:
  Tm = a b c d e f
  x1 = a*x0 + c*y0 + e
  y1 = b*x0 + d*y0 + f
  
  dove: X1 coordinata user reale e X0 coordinata user da stream

 Tf: /F1, 1
 Tm. 12 // 0 // 0 // 12 // 250 // 300    ==> angle 0   x1ce*x0   y1=ce*y0
 Tm. 0 // 12 // -12 // 0 // 250 // 300   ==> angle 90  x1=-ce*y0 y1=ce*x0
 Tm. -12 // 0 // 0 // -12 // 250 // 300  ==> angle 180 x1=-ce*x0 y1=-ce*y0
 Tm. 0 // -12 // 12 // 0 // 250 // 300   ==> angle 270 x1=-ce*y0 y1=ce*x0
  
 Tm. 27.0452 // 7.2464 // -8.2908 // 26.7652 // 176.7615,893.9478 (angle not multiple of 90)
 
=cut

__END__







