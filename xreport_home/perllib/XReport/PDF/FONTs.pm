
package XReport::PDF::FontAlias;

use strict;

sub getValues {
  my $self = shift; my @r; 
  for (@_) { 
    die("INVALID PDF FONTALIAS FIELD $_") if !exists($self->{$_}); push @r, $self->{$_}; 
  } 
  return wantarray ? @r : $r[0];
}

sub StringWidth {
  my ($self, $StringValue) = @_; return 0 if $StringValue eq ""; my $CharWidths = $self->{'CharWidths'}; 
  
  my $at = 0; my $max_at = length($StringValue)-1; my $tw = 0;

  while($at<=$max_at) {
    $tw += $CharWidths->[ord(substr($StringValue, $at, 1))]; $at += 1;
  }

  return $tw;
}

sub new {
  my ($class, $FontAliasName, $FontObjId, $FontMetrics, $CharWidths) = @_; my $FontAliasRef = "/$FontAliasName $FontObjId 0 R";

  my $self = {
    FontAliasName => $FontAliasName, FontAliasRef => $FontAliasRef, 
    FontMetrics => $FontMetrics, CharWidths => $CharWidths
  };

  bless $self, $class;
}

1;

package XReport::PDF::FONTs;

use strict;

use XReport::PDF::ENCODING;

my $FontMappingTable = {
  
  'Arial Bold' => ['arialbd.ttf', 'TrueType', {embed => 0}],
  'Courier New' => ['cour.ttf', 'TrueType', {embed => 0}],
  'CourierNew' => ['cour.ttf', 'TrueType', {embed => 0}],
  'Courier New Bold' => ['courbd.ttf', 'TrueType', {embed => 0}],

  'Courier' => ['standard/courier.afm', 'Type1', {embed=>0}],
  'Courier Bold' => ['standard/courier-bold.afm', 'Type1', {embed=>0}],
  'Courier Oblique' => ['standard/courier-oblique.afm', 'Type1', {embed=>0}],
  'Courier Bold Oblique' => ['standard/courier-boldoblique.afm', 'Type1', {embed=>0}],
  'Helvetica' => ['standard/helvetica.afm', 'Type1', {embed=>0}],
  'Helvetica Bold' => ['standard/helvetica-bold.afm', 'Type1', {embed=>0}],
  'Helvetica Oblique' => ['standard/helvetica-oblique.afm', 'Type1', {embed=>0}],
  'Helvetica Bold Oblique' => ['standard/helvetica-boldoblique.afm', 'Type1', {embed=>0}],
  'Symbol' => ['standard/symbol.afm', 'Type1', {embed=>0}],
  'Times Roman' => ['standard/times-roman.afm', 'Type1', {embed=>0}],
  'Times Bold' => ['standard/times-bold.afm', 'Type1', {embed=>0}],
  'Times Italic' => ['standard/times-italic.afm', 'Type1', {embed=>0}],
  'Times Bold Italic' => ['standard/times-bolditalic.afm', 'Type1', {embed=>0}],

  'Times New Roman' => ['times.ttf', 'TrueType', {embed=>0}],
  'Times New Roman Bold' => ['timesbd.ttf', 'TrueType', {embed=>0}],
  'Times New Italic' => ['timesi.ttf', 'TrueType', {embed=>0}],
  'Times New Bold Italic' => ['timesbi.ttf', 'TrueType', {embed=>0}],

};

sub _get_encoding_table {
  my ($self, $Encoding, $Font) = @_; my ($ldoc, $Encoder, $EncodingTables) = @{$self}{qw(ldoc Encoder EncodingTables)}; 

  exists($EncodingTables->{$Encoding}) and return $EncodingTables->{$Encoding}; 
  
  my ($EncodingScheme, $EncodingDiff) = ($Encoding, undef); 

  if ($Encoding =~ /^\s*(?:\d+ +0 +R|<<)/s) {
    $Encoding = $ldoc->ParseCatalog($Encoding);
    ($Encoding, $EncodingDiff) 
     = 
    @{$Encoding}{qw(Encoding Differences)}; 
    $Encoding ||= $Font->getValues('EncodingScheme');
  }

  $EncodingTables->{$EncodingScheme} = $Encoder->getEncodingTable($Encoding, $EncodingDiff);
}

use IO::String; 

sub _get_font_iostream {
  my ($self, $FullFontName) = @_;

#  exists($FontMappingTable->{$FullFontName})
#   or
#  die("FONT MAPPING DETECTED ERROR $FullFontName");
  
#  my ($FontFileName, $FontType, $largs) = @{$FontMappingTable->{$FullFontName}}; 
  
#  my $FontPath = "c:/xreport/fonts/system";
#  my $lFontFileName = "$FontPath/$FontType/$FontFileName";

  my $largs = {embed => 0};
  my $iostream = new IO::String $self->{$FullFontName} ;
  (my $lFontFileName, my $FontType) = $self->{resh}->getResources(ResName => $FullFontName, ResClass => 'fontpdf', OutFile => $iostream) ;
  die "Error getting font $FullFontName fn: $lFontFileName ft: $FontType" unless ($lFontFileName && $FontType);

  $iostream->close();
  $iostream = new IO::String $self->{$FullFontName} ;

  return ($FontType, $iostream, {%$largs, lFontFileName => $lFontFileName});


#  my $iostream = IO::File->new("$lFontFileName", "r")
#   or
#  die("FONT FILE INPUT OPEN ERROR \"$FontFileName\" $!"); $iostream->binmode(); ($FontType, $iostream, {%$largs, lFontFileName => $lFontFileName});
}

sub _get_font {
  my ($self, $FullFontName, $FontFamily, $FontStyle) = @_; $FullFontName = $FontFamily if $FullFontName eq "";

  if ($FullFontName eq $FontFamily and $FontStyle ne "") { 
    $FullFontName = "$FullFontName $FontStyle";
  }
  my ($FontType, $iostream, $largs) = $self->_get_font_iostream($FullFontName); 
  
  my $FontPackage = "XReport::Font::$FontType"; eval "require $FontPackage"; die("$@") if $@; 
  
  my $Font = $FontPackage->Open($FullFontName, $iostream) || die("INVALID FONT $FullFontName"); ($Font, $largs);
}

sub _get_font_char_widths {
  my ($self, $Font, $Encoding, $FontEncoding, $CharWidths)  = @_; my ($Encoder, $ldoc) = @{$self}{qw(Encoder ldoc)};

  my $FirstChar = -1; my $LastChar = -1; my $Widths = []; 

  my $EncodingTable = $self->_get_encoding_table($Encoding, $Font);

  for my $ienc (0..@$EncodingTable-1) {
    my $isymbol = $FontEncoding->{$EncodingTable->[$ienc]}; my $width = defined($isymbol) ? $CharWidths->[$isymbol] : 0;
    if ($_ ne 'undef' and defined($isymbol)) {
      $FirstChar = $ienc if $FirstChar == -1; $LastChar = $ienc;
    }
    push @$Widths, $width
  }

  ($FirstChar, $LastChar, $Widths);
}

sub AddFontAlias {
  my ($self, $FullFontName, $FontFamily, $FontStyle, $Encoding) = @_;  my $FontProgr = $self->{'FontProgr'} += 1;
  my (
    $Fonts, $FontDescriptors, $ldoc, $FontPrefix, $Encoder
  ) = 
  @{$self}{qw(Fonts FontDescriptors ldoc FontPrefix Encoder)};

  my $lFontAlias = "$FullFontName/$FontFamily/$FontStyle";  

  if (!exists($Fonts->{$lFontAlias})) {
    $Fonts->{$lFontAlias} = [$self->_get_font($FullFontName, $FontFamily, $FontStyle)];
  }
  my ($Font, $largs) = @{$Fonts->{$lFontAlias}}; my ($isStandardFont, $embed) = ($Font->getValues('isStandardFont'), $largs->{'embed'});

  my $lFontAlias = "$FullFontName/$FontFamily/$FontStyle/$Encoding"; my ($FontObj, $FontWidths);

  if ($isStandardFont) {
    my (
      $FontType, $PostscriptFontName, $FontEncoding, $CharWidths
    ) = 
    $Font->getValues(qw( FontType PostscriptFontName FontEncoding CharWidths));
    
    my ($FirstChar, $LastChar, $Widths) = $self->_get_font_char_widths($Font, $Encoding, $FontEncoding, $CharWidths); 
    
    $FontObj = join("\n", 
     "<<",
      "/Type/Font/Subtype/$FontType", "/BaseFont /$PostscriptFontName", "/Encoding/$Encoding", 
     ">>\n"
    );
    $FontWidths = $CharWidths;
  }
  else {
    if (!exists($FontDescriptors->{$lFontAlias})) {
      my (
       $PostscriptFontName, $FontFamily, $FontBBox, $ItalicAngle, $FontWeight, $FontStretch, $Ascender, $Descender, 
       $XHeight, $CapHeight, $isFixedPitch, $hasSerifs, $isScript, $isSymbolic, $isItalic,
       $FontType, $isStandardFont, 
      ) =
      $Font->getValues(qw(
        PostscriptFontName FontFamily FontBBox ItalicAngle FontWeight FontStretch Ascender Descender 
        XHeight CapHeight isFixedPitch hasSerifs isScript isSymbolic isItalic
        FontType isStandardFont 
      ));
  
      my $Flags = 0; 
      $Flags |= 1 if $isFixedPitch;
      $Flags |= 2 if $hasSerifs;
      $Flags |= 4 if $isSymbolic;
      $Flags |= 8 if $isScript;
      $Flags |= 32 if !$isSymbolic;
      $Flags |= 64 if $isItalic;

      my $FontDescriptorObj = join("\n",
       "<<",
        "/Type /FontDescriptor",
        "/FontName /$PostscriptFontName",
        "/FontFamily ($FontFamily)",
        "/Flags $Flags",
        "/FontBBox $FontBBox",
        "/ItalicAngle $ItalicAngle",
        "/FontWeight $FontWeight",
        "/FontStretch /$FontStretch",
        "/Ascent $Ascender",
        "/Descent $Descender",
        ($isSymbolic ? () : ("/XHeight $XHeight", "/CapHeight $CapHeight")),
       ">>\n",
      );

      my $lobjid = $ldoc->AddObj($FontDescriptorObj); $FontDescriptors->{$lFontAlias} = "$lobjid 0 R";
    }
    my $FontDescriptor = $FontDescriptors->{$lFontAlias};
    my (
      $PostscriptFontName, $FontType, $FontEncoding, $CharWidths
    ) = 
    $Font->getValues(qw(PostscriptFontName FontType FontEncoding CharWidths));
    
    my ($FirstChar, $LastChar, $CharWidths) = $self->_get_font_char_widths($Font, $Encoding, $FontEncoding, $CharWidths);

    $FontObj = join("\n",
     "<<",
      "/Type/Font/Subtype/$FontType",
      "/FontDescriptor $FontDescriptor",
      "/BaseFont /$PostscriptFontName",
      "/Encoding/$Encoding",
      "/FirstChar $FirstChar",
      "/LastChar $LastChar",
      "/Widths [".join(" ", @{$CharWidths}[$FirstChar..$LastChar])."]",
     ">>\n",
    );
    $FontWidths = $CharWidths;
  }

  XReport::PDF::FontAlias->new("$FontPrefix$FontProgr", $ldoc->AddObj($FontObj), $Font, $FontWidths);
}

sub GetFontAlias {
  my ($self, $FullFontName, $FontFamily, $FontStyle, $Encoding) = @_; my $FontAliases = $self->{'FontAliases'}; 

  my $lFontAlias = "$FullFontName/$FontFamily/$FontStyle/$Encoding";

  if (!exists($FontAliases->{$lFontAlias})) {
    $FontAliases->{$lFontAlias} 
     = 
    $self->AddFontAlias($FullFontName, $FontFamily, $FontStyle, $Encoding);
  }

  $FontAliases->{$lFontAlias}
}

use XReport::Resources;

sub new {
  my ($class, $ldoc, %largs) = @_; %largs = (FontPrefix => 'F', FontProgr => 0, %largs);
  
  my ($FontPrefix, $FontProgr) = @largs{qw(FontPrefix FontProgr)};
  my $lworkdir = c::getValues('workdir');

  my $Encoder = XReport::PDF::ENCODING->new();

  my $self = {
    Fonts => {}, FontDescriptors => {}, ldoc => $ldoc, FontAliases => {}, 
    
	      FontPrefix => $FontPrefix, FontProgr => 0, Encoder => $Encoder, EncodingTables => {},
	      resh =>  new XReport::Resources( destdir => "$lworkdir/localres", TimeRef => '2099-12-31 23:59:59' ),

  };

  bless $self, $class;
}

1;

__END__


sub getFont {
  my ($self, $objId) = (shift, shift); my $FontTable = $self->{'FontTable'};

  if (!$FontTable) {
    require XReport::PDF::FontTable; 
    $FontTable = $self->{'FontTable'} = XReport::PDF::FontTable->new($self);
  }

  return $FontTable->getFont($objId);
}


#sub getFontEntries {
#  my ($self, $FontObjId) = @_; my ($FontTable, $FontDescriptors) = @{$self}{qw(FontTable FontDescriptors)}; my ($Font, $FontDescriptor);
#
#  ($FontObjId) = $FontObjId =~ /(\d+)/; my ($BaseFont, $TagName, $FontName, $FontSubtype);
#
#  if (!exists($FontTable->{$FontObjId})) {
#    my $obj = $self->getObj($FontObjId); $FontTable->{$FontObjId} = $self->ParseCatalog($obj->[1]);
#  }
#  $Font = $FontTable->{$FontObjId}; 
#
#  $TagName = ''; $FontName = substr($Font->{'BaseFont'}, 1); $FontSubtype = substr($Font->{'Subtype'},1);
#  
#  if (exists($Font->{'FontDescriptor'})) {
#    $FontDescriptor = $Font->{'FontDescriptor'}; ($FontDescriptor) = $FontDescriptor =~ /(\d+)/;
#
#    if (!exists($FontDescriptors->{$FontDescriptor})) {
#      my $obj = $self->getObj($FontDescriptor); $FontDescriptors->{$FontDescriptor} = $self->ParseCatalog($obj->[1]);
#    }
#
#    $FontDescriptor = $FontDescriptors->{$FontDescriptor};
#  }
#
#  ($TagName, $FontName) = $FontName =~ /^([^+]+)\+(.*)/so if $FontName =~ /\+/; 
#
#  return { TagName => $TagName, FontName => $FontName, FontSubtype => $FontSubtype, FontObjId => $FontObjId, Font => $Font, FontDescriptor => $FontDescriptor };
#}
#
#sub FillFontList {
#  my ($self) = shift; my ($TabXref) = @{$self}{qw(TabXref)};
#
#  for (1..scalar(@$TabXref)-1) {
#    my $objref = $TabXref->[$_]->[0]; next if !defined($objref);
#    my $obj = $self->getObj($_); 
#    $self->getFontEntries($_) if $obj->[1] =~ /\/Type\s*\/Font\b/mso;
#  }
#}


#sub OptimizeResources {
#  my ($self, $obj, $doc, $FromXref) = @_; my ($largs, $FontTable) = @{$self}{qw(largs FontTable)};
#
#  my $resObj = $doc->getObj($doc->getPageResources($obj));
#  my $resCat = $self->ParseCatalog($resObj->[1]);
#  
#  my @FontObjIds = $resCat->{'Font'} =~ /(-?\d+)$lx_space+\d+$lx_space+R/gso;
#
#  for my $FontObjId (@FontObjIds) {
#    next if exists($FromXref->[$FontObjId]); 
#
#    my $FontObj = $doc->getObj($FontObjId); my $FontCat = $doc->ParseCatalog($FontObj->[1]); 
#    next if !exists($FontCat->{'FontDescriptor'}) or !exists($FontCat->{'FontDescriptor'});
#
#    my ($BaseFont, $FontDescriptor) = @{$FontCat}{qw(BaseFont FontDescriptor)}; 
#    my ($SubsetName, $FontName) = $BaseFont =~ /^([^+]+)\+(.*)/so; next if $SubsetName eq "";
#
#    my $FontDesObj = $doc->getObj($FontDescriptor); my $FontDesCat = $doc->ParseCatalog($FontDesObj->[1]);
#    my $FontFileType = 
#        exists($FontDesCat->{'FontFile'}) ? "FontFile" 
#      : exists($FontDesCat->{'FontFile2'}) ? "FontFile2" 
#      : exists($FontDesCat->{'FontFile3'}) ? "FontFile3" : ""
#    ;
#    next if $FontFileType eq ""; my $FontFile;
#
#    $FontFile = $doc->getObjTextStream($FontDesCat->{$FontFileType});
#    
#    $FontFile =~ s/$SubsetName//gs; my $osmd5 = md5($FontFileType.md5($FontFile));
#
#    if (exists($FontTable->{$FontName}->{$osmd5})) {
#      print "!!!!!! $SubsetName, $FontName !!!!!!\n";
#      $FromXref->[$FontObjId] = $FontTable->{$FontName}->{$osmd5};
#    }
#    else {
#      print "?????? $SubsetName, $FontName ??????\n";
#      $FontTable->{$FontName} = { $osmd5 => ($FromXref->[$FontObjId] = ($self->{MaxId} += 1)) };
#    } 
#  }
#}
