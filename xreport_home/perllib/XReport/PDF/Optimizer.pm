
package XReport::PDF::Optimizer;

use strict;

use Symbol;

use Digest::MD5 qw(md5_hex);

#------------------------------------------------------ CompressJpeg

my $lx_hdr = qr/(stream\s*(?:\x0a|\x0d)+)/;
my $lx_jpeghdr = qr/$lx_hdr(?:\xff\xd8\xff\xee)/;

my $todensity = 75;

sub ResampleJpeg {
  my ($self, $obj) = (shift, shift); my ($args, $images, $bboxes) = @{$self}{qw(args images bboxes)};

  my (
    @bbox, $length, $width, $height, $filter, $bits_pc, $prefix, $magick, $icomps, $blob, $suffix, $md5, $image, $errmsg
  );

  @bbox = $bboxes->{$obj->[0]}->[0] =~ /(\d+)/msog; return if scalar(@bbox) != 4;

  ($length) = $obj->[1] =~ /\/Length\s+(\d+)/; return if $length < 2048;

  ($width)  = $obj->[1] =~ /\/Width\s+(\d+)/;
  ($height) = $obj->[1] =~ /\/Height\s+(\d+)/;
  ($filter) = $obj->[1] =~ /\/Filter\s*\/(\w+)/;
  ($bits_pc) = $obj->[1] =~ /\/BitsPerComponent\s+(\d+)/;

  $md5 = md5_hex($obj->[2]);

  my $FILECACHE = $self->{'FILECACHE'}; 
   
  if ( exists($images->{$md5}) ) {
    ($length, $width, $height, my $blob_offset) = @{$images->{$md5}}; my $cmd5;

    seek($FILECACHE, $blob_offset, 0); read($FILECACHE, $blob, $length);

    read($FILECACHE, $cmd5, 32); 

    die("INVALID MD5 CHECK ($md5/$cmd5)") if $cmd5 ne $md5;
  }
  else {
    my $density = int(($width/$bbox[2])*72); return if $density <= $todensity; require Image::Magick; 

    if ( $filter eq "DCTDecode" ) {
     ($prefix) = $obj->[2] =~ /^$lx_jpeghdr/so; return "NOT JPEG" if $prefix eq ''; $magick = "jpeg";
      $blob = substr($obj->[2], length($prefix), $length);
      $image = Image::Magick->new(magick => 'jpg', density => $density);
    }
    elsif ( $filter eq "FlateDecode" ) {
     ($prefix) = $obj->[2] =~ /^$lx_hdr/so;  return "INVALID STREAM" if $prefix eq ''; require Compress::Zlib;  
      $blob =  Compress::Zlib::uncompress(substr($obj->[2], length($prefix), $length));
      $icomps = (((length($blob) * 8)/ $width / $height) / $bits_pc);
      $magick = $icomps == 3 ? 'rgb' : $icomps == 1 ? 'gray' : return "UNKNOWN IMAGE TYPE";
      
      $image = Image::Magick->new(magick => $magick, compression => 'none', depth => $bits_pc, size => $width."x".$height);
    }
    else {
      return "FILTER NOT MANAGED";
    }
    $suffix = substr($obj->[2], length($prefix) + $length); $obj->[2] = '';

    $errmsg = $image->BlobToImage(
      $blob
    );
    die("Image BlobToImage ERROR: $errmsg") if $errmsg ne '';

    $density = int($density/4); $density = $todensity if $density < $todensity;

    $errmsg = $image->Resample(
      density=>"$density\%",
      filter=> 'Cubic', blur => 0.9
    ) ;
    die("Image Resample ROUTINE ERROR: $errmsg ") if $errmsg ne '';

    ($blob) = $image->ImageToBlob() 
     or 
    die("Image ImageToBlob ROUTINE ERROR."); $blob = Compress::Zlib::compress($blob) if $filter eq "FlateDecode";

    my $blob = $prefix . $blob . $suffix;
    (
      $length, $width, $height
    ) = 
    (length($blob), $image->Get(qw(Width Height))); 
    
    seek($FILECACHE, 0, 2); 

    print $FILECACHE $md5, pack("NNN", $length, $width, $height), $blob, $md5; 

    $images->{$md5} = [$length, $width, $height, tell($FILECACHE) -32 - $length]; 
  }

  $obj->[1] =~ s/\/Length\s+\d+/\/Length $length/ms;
  $obj->[1] =~ s/\/Width\s+\d+/\/Width $width/ms;
  $obj->[1] =~ s/\/Height\s+\d+/\/Height $height/ms;

  # imagemagick when writing a jpeg image changes colorspace to GRAY if IsGrayImage returns true
  if ($filter eq "DCTDecode") {
    my $i = index($blob,"\xff\xd8") + 2; my $lblob = rindex($blob,"\xff\xd9"); my ($t,$hdr, $len);
    while($i < $lblob) {
      $t = substr($blob, $i, 4); ($hdr, $len) = unpack("H4n", $t);
      last if $hdr eq "ffda";
      if ("ffc" eq substr($hdr,0,3)) {
        $icomps = unpack("C", substr($blob,$i+9,1)); last;
      }
    }
    continue { 
      $i += 2 + $len;   
    }

    $obj->[1] =~ s/\/ColorSpace\s+[^\/>]+/\/ColorSpace\/DeviceGray/ms if $icomps == 1;
  }

  $obj->[2] = $prefix . $blob . $suffix; return undef;
}

sub optimize_obj {
  my ($self, $obj) = (shift, shift); my $bboxes = $self->{'bboxes'};
    
  if ($obj->[2] and $obj->[1] =~ /\/Subtype\s*\/Image/) {
    $self->ResampleJpeg($obj);
  }

  elsif ( exists($bboxes->{$obj->[0]}) ) {
    my ($iobj) = $obj->[1] =~ /\/Im\d+\s+(\d+)\s+0\s+R/mso;
    $bboxes->{$iobj} = $bboxes->{$obj->[0]};
  }

  elsif (my ($bbox) = $obj->[1] =~ /\/BBox\s*\[([^\]]+)\]/) {
    my ($res) = $obj->[1] =~ /\/Resources\s*([^\/>]+)/mso;
    my (@res) = $res =~ /(\d+)\s+0\s+R/msog;
    for (@res) {
      push @{$bboxes->{$_}}, $bbox;
    }
  }
}

sub Close {
  my $self = shift; close($self->{'FILECACHE'});

  %$self = ();
}

sub new {
  my ($ClassName, $args) = (shift, shift); my $self;

    my ($bboxes, $images, $FILECACHE) =  ({}, {}, Symbol::gensym()); 

    my ($ImagesSetId, $ImagesFileCache) = @{$args}{qw(ImagesSetId ImagesFileCache)};

    my $cacheFile = "$ImagesFileCache/$ImagesSetId.dat"; 
    
    open($FILECACHE, (-f $cacheFile ? "+<" : "+>") . $cacheFile) 
     or
    die("FILECACHE FILE OPEN ERROR \"$cacheFile\" $!"); 

    $_ = select($FILECACHE); $| = 1; select($_); binmode($FILECACHE); 

    my ($md5, $length, $width, $height, $blob);
      
    while(!eof($FILECACHE)) {
      read($FILECACHE, $blob, 44); ($md5, $length, $width, $height) = unpack("a32NNN", $blob); 

      $images->{$md5} = [$length, $width, $height, tell($FILECACHE)]; 

      seek($FILECACHE, $length+32, 1);
    }

  $self = {
    bboxes => $bboxes, images => $images, FILECACHE => $FILECACHE
  };

  bless $self, $ClassName;
}

1;
