package XReport::PDF::ENCR;

use strict;

use Digest::MD5 qw(md5);
use Crypt::CapnMidNite;

my $PADSTRING = pack("H*","28BF4E5E4E758A4164004E56FFFA01082E2E00B6D0683E802F0CA9FE6453697A");

my $cs_spaces = quotemeta("\x00\x09\x0a\x0c\x0d\x20");
my $cs_delim = quotemeta("\(\)\<\>\[\]\{\}\/\%");

my $lx_sep = qr/[$cs_spaces$cs_delim]/so;

my $lx_string_value  = qr/\((.*?(?<!\\))\)/so;
my $lx_hexstring_value  = qr/(?<!<)\<([0-9a-fA-F$cs_spaces]*)\>(?=$lx_sep|(?:$\))/so;

my %tquote = ( 
  "\n" => "n", 
  "\r" => "r", 
  "\t" => "t", 
  "\b" => "b", 
  "\f" => "f", 
  "\(" => "(", 
  "\)" => ")", 
  "\\" => "\\"
);

my %tdequote = (
  "n" => "\n",
  "r" => "\r", 
  "t" => "\t", 
  "b" => "\b", 
  "f" => "\f", 
  "(" => "\(", 
  ")" => "\)", 
  "\\" => "\\"
);
  

sub quoteps {
  my $t = $_[0]; $t =~ s/([\n\r\(\)\\])/\\$tquote{$1}/g; return $t;
}

sub dequoteps {
  my $t = $_[0]; $t =~ s/\\([nrtbf()\\])/$tdequote{$1}/g; return $t;
}

sub encr_obj {
  my $self = shift; my ($encr_key, $rev, $lbytes) = @{$self}{qw(encr_key encr_revision encr_lbytes)};

  my ($objid, $revision, $objref) = @_; 

  $lbytes += 5; $lbytes = 16 if $lbytes > 16;

  $encr_key = md5(
    $encr_key
   .substr(pack("V",$objid),0,3)
   .pack("v",$revision)
  );
  my $crypt = Crypt::CapnMidNite->new_rc4(substr($encr_key,0,$lbytes));
  
  $$objref =~ s{$lx_string_value}{"(".quoteps($crypt->encrypt(dequoteps($1))).")";}gsoe;

  $$objref =~ s{$lx_hexstring_value}{"<".unpack("H*",$crypt->encrypt(pack("H*", $1))).">";}gsoe;
}

sub encr_string {
  my $self = shift; my ($encr_key, $rev, $lbytes) = @{$self}{qw(encr_key encr_revision encr_lbytes)};

  my ($objid, $revision, $string) = @_; $string = dequoteps($string);

  $lbytes += 5; $lbytes = 16 if $lbytes > 16;
 
  $encr_key = md5(
    $encr_key
   .substr(pack("V",$objid),0,3)
   .pack("v",$revision)
  );
  my $crypt = Crypt::CapnMidNite->new_rc4(substr($encr_key,0,$lbytes));

  quoteps($crypt->encrypt($string));
}


sub encr_stream {
  my $self = shift; my ($encr_key, $rev, $lbytes) = @{$self}{qw(encr_key encr_revision encr_lbytes)};

  my ($objid, $revision, $tref) = @_; 

  $lbytes += 5; $lbytes = 16 if $lbytes > 16;

  $encr_key = md5(
    $encr_key
   .substr(pack("V",$objid),0,3)
   .pack("v",$revision)
  );
  my $crypt = Crypt::CapnMidNite->new_rc4(substr($encr_key,0,$lbytes));

  if ( substr($$tref,0,6) eq "stream" ) {
    $$tref =~ s{^(stream\r?\n)(.*)(endstream[$cs_spaces]*)$}{
      $1.$crypt->encrypt($2)."\r$3";
    }soe;
  }
  else {
    $$tref =~ $crypt->encrypt($$tref);
  }
}

sub encr_key {
  my ($self, $key) = (shift, shift);

  $key = md5(
    substr($key.$PADSTRING,0,32)
   .$self->{encr_owner} .pack("V", $self->{perms}) .$self->{id}->[0]
  );

  return substr($key,0,5) if $self->{encr_revision} eq 2;

  for (1..50) {
    $key = md5($key);
  }

  return substr($key,0,$self->{encr_lbytes});
}

sub encr_owner {
  my $self = shift; my ($rev, $lbytes) = @{$self}{qw(encr_revision encr_lbytes)};

  my $owner = substr($self->{owner_pass}.$PADSTRING,0,32);
  my $user = substr($self->{user_pass}.$PADSTRING,0,32);

  $owner = $user if $owner eq $PADSTRING;
 
  for (0..($rev eq 2 ? 0 : 50)) {
    $owner = md5($owner);
  }
  my $encr_key = substr($owner, 0, $lbytes); 

  my $owner = $user;  

  for my $lc (0..($rev eq 2 ? 0 : 19)) {
    my $lk = '';
    for (0..($lbytes-1)) {
      $lk .= pack("C", unpack("C",substr($encr_key,$_,1)) ^ $lc);
    }
    $owner = Crypt::CapnMidNite->new_rc4($lk)->encrypt($owner);
  }

  return $owner;
}

sub encr_user {
  my $self = shift; my ($encr_key, $rev, $lbytes) = @{$self}{qw(encr_key encr_revision encr_lbytes)};

  return Crypt::CapnMidNite->new_rc4($encr_key)->encrypt($PADSTRING) if $rev eq 2;

  my $user = md5($PADSTRING.$self->{id}->[0]);

  for my $lc (0..19) {
    my $lk = '';
    for (0..($lbytes-1)) {
      $lk .= pack("C", unpack("C",substr($encr_key,$_,1)) ^ $lc);
    }
    $user = Crypt::CapnMidNite->new_rc4($lk)->encrypt($user);
  }

  return $user . "\x00" x 16;
}

sub get_encrypt_parms {
  my $self = shift; my ($O, $U, $P, $Length) = @{$self}{qw(encr_owner encr_user perms encr_length)};

  return { V => 2, R => 3, O => quoteps($O), U => quoteps($U), P => $P, Length => $Length };
}

sub new {
  my $className = shift; my %args = @_; bless (my $self = {}), $className;

  for (qw(id owner_pass user_pass perms encr_length)) {
    $self->{$_} = $args{$_};
  }

  $self->{encr_length} = ($self->{encr_length} <= 40) ? 40 : 128;

  $self->{encr_revision} = ($self->{encr_length} == 40) ? 2 : 3;

  $self->{encr_lbytes} = int($self->{encr_length}/8); 

  $self->{encr_owner} = $self->encr_owner(); 
  $self->{encr_key} 
    = 
  $self->encr_key($self->{user_pass});
  $self->{encr_user} = $self->encr_user();

  return $self;
}


sub DESTROY {
 #print "DESTROY ", __PACKAGE__, "\n";
}

1;
