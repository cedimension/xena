
package XReport::PDF::REPORT;

use strict;

use XReport::PDF::DOC;


sub MAX {
  return (($_[0] >= $_[1]) ? $_[0] : $_[1]);
}

sub NewPageFomLineList {
  my ($self, $lLines) = (shift, shift) ; my ($doc, $resId) = @{$self}{qw(doc resId)};

  my ($PageWidth, $PageHeight, $FontWidth, $FontHeight, $TopLine) 
   = 
  @{$self}{qw(PageWidth PageHeight FontWidth FontHeight TopLine)};  
  
  my $stream = "BT /F1 1 Tf $FontWidth 0 0 $FontHeight 30 $TopLine Tm 0 g /GS1 gs 0 Tc 0 Tw 1 TL\n" ;
  for (@$lLines) {
    $_ =~ s/\(/\\\(/g; $_ =~ s/\)/\\\)/g;
    $stream .= "($_)Tj T*\n"
  }
  $stream .= "ET";
  
  my $stId = $doc->AddStream($stream);

  return $doc->AddPageObj(
    "<<\n"
   ."/Type /Page\n"
   ."/Parent 2 0 R\n"
   ."/Resources $resId 0 R\n"
   ."/Contents $stId 0 R\n"
   ."/MediaBox [0 0 $PageWidth $PageHeight]\n"
   .">>\n"
  );
}

sub Close {
  my $self = shift; my $doc = $self->{qw(doc)};

  $doc->Close();
}

sub new {
  my $className = shift; 

  my %args = (
    CharsPerLine => 133,
    LinesPerPage => 66,
    PageOrient => 'L',

    FontWidth => 9.0,
    FontHeight => 8.4, 
    @_
  );

  my (
    $CharsPerLine, $LinesPerPage, $PageOrient, 
    $FontWidth, $FontHeight, 
    $OUTPUT, $ENCRYPT
  )
   =
  @args{qw(CharsPerLine LinesPerPage PageOrient FontWidth FontHeight OUTPUT ENCRYPT)};
  
  my $PageWidth = int(MAX(60 + ($FontWidth*0.6)*$CharsPerLine, 841.68)+0.5); 
  my $PageHeight = int(MAX(56 + ($FontHeight*1.0)*$LinesPerPage, 595.44)+0.5);

  ($PageWidth, $PageHeight) = ($PageHeight, $PageWidth) 
   if 
  $PageOrient ne 'L';

  my $doc = XReport::PDF::DOC->Create($OUTPUT, {encrypt => $ENCRYPT});

  my $fontId = $doc->AddObj(
    "<<\n"
   ."/Type /Font\n"
   ."/Subtype /Type1\n"
   ."/Encoding /WinAnsiEncoding\n"
   ."/BaseFont /Courier\n"
   .">>\n"
  );

  my $egsId = $doc->AddObj(
    "<<\n"
   ."/Type /ExtGState\n"
   ."/SA false\n"
   ."/SM 0.02\n"
   ."/TR /Identity\n"
   .">>\n"
  );

  my $resId = $doc->AddObj(
    "<<\n"
   ."/ProcSet [/PDF /Text ]\n"
   ."/Font <<\n"
   ."/F1 $fontId 0 R\n"
   .">>\n"
   ."/ExtGState <</GS1 $egsId 0 R>>"
   .">>\n"
  );

  my $self = {
    FontWidth => $FontWidth, FontHeight => $FontHeight,
    PageWidth => $PageWidth, PageHeight => $PageHeight,

    TopMargin => 32, BottomMargin =>  24, 
    LeftMargin => 30, RightMargin => 30,  
 
    PageOrient =>  $PageOrient,

    TopLine => $PageHeight - 32, doc => $doc, resId => $resId
  };

  bless $self, $className;
}

1;
