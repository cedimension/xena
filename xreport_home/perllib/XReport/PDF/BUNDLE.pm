package XReport::PDF::BUNDLE;

use strict; use bytes;

use Text::Reform qw(form);

use XReport::PDF::DOC;
use XReport::FIGlet;
use XReport::FileCache;

my $FmDocs = {}; my $FIGlet = XReport::FIGlet->new( -f => 'banner3' );

sub add_page {
  my ($self, $lLines) = (shift, shift); my ($doc, $resId) = @{$self}{qw(doc resId)};
  
  my $stream = "BT /F1 1 Tf 9.0 0 0 8.24 30 559 Tm 0 g /GS1 gs 0 Tc 0 Tw 1 TL\n" ;
  for (@$lLines) {
    $_ =~ s/\(/\\\(/g; $_ =~ s/\)/\\\)/g;
    $stream .= "($_)Tj T*\n"
  }
  $stream .= "ET";
  
  my $stId = $doc->AddStream($stream);

  return $doc->AddPageObj(
    "<<\n"
   ."/Type /Page\n"
   ."/Parent 2 0 R\n"
   ."/Resources $resId 0 R\n"
   ."/Contents $stId 0 R\n"
   ."/MediaBox [0 0 842 600]\n"
   .">>\n"
  );
}

sub FormatHeaderPage {
  my ($lIN, $lOUT) = (shift, []); 

  push @$lOUT, "*" x 133, map( {$_ =~ s/\n$//; "*    $_". " " x (133-6-length($_)) . "*"} @$lIN );
  
  while(scalar(@$lOUT)<60) {
    push @$lOUT, "*" . " " x 131 . "*"
  }

  push @$lOUT, "*" x 133;

  return $lOUT;
}

sub Create {
  my ($className, $fileName, $kFilterTarget, $lReports) = @_;

  my $doc = XReport::PDF::DOC->Create("$fileName");

  $doc->SetValues('MediaBox', '0 0 842 600', 'Rotate', 0);

  my $fontId = $doc->AddObj(
    "<<\n"
   ."/Type /Font\n"
   ."/Subtype /Type1\n"
   ."/Encoding /WinAnsiEncoding\n"
   ."/BaseFont /Courier\n"
   .">>\n"
  );

  my $egsId = $doc->AddObj(
    "<<\n"
   ."/Type /ExtGState\n"
   ."/SA false\n"
   ."/SM 0.02\n"
   ."/TR /Identity\n"
   .">>\n"
  );

  my $resId = $doc->AddObj(
    "<<\n"
   ."/ProcSet [/PDF /Text ]\n"
   ."/Font <<\n"
   ."/F1 $fontId 0 R\n"
   .">>\n"
   ."/ExtGState <</GS1 $egsId 0 R>>"
   .">>\n"
  );
  
  my $self = { doc => $doc, resId => $resId };

  bless $self, $className; my $tref = {}; 

  my $lLines = [(
    "", "", split("\n", $FIGlet->figify( -A => $kFilterTarget, -w => 132 )), "", ""
  )];

  push @$lLines, 
   form
    "+-----------------+-------------------------------+---------+---------------------------+-----------+------------+",
    "| REPORT          | DESCRIZIONE                   |  PAGINE |  DATA TRASFERIMENTO       | NOME JOB  | NUMERO JOB |",
    "+-----------------+-------------------------------+---------+---------------------------+-----------+------------+",
    map {(
     "| <<<<<<<<<<<<<<< | <<<<<<<<<<<<<<<<<<<<<<<<<<<<< | >>>>>>> | <<<<<<<<<<<<<<<<<<<<<<<<< | <<<<<<<<< | <<<<<<<<<< |",
     , @{$_}{qw(ReportName ReportDescr TotPages XferStartTime JobName JobNumber)}
    )} @$lReports
  ;
  push @$lLines, 
    "+-----------------+-------------------------------+---------+---------------------------+-----------+------------+"
  ;
  my $mobjId = $self->add_page(FormatHeaderPage($lLines));
  
  for my $Report ( @$lReports ) {
    my $JobReportId = $Report->{JobReportId};
    
    if ( !exists($FmDocs->{$JobReportId}) ) {
      print "OPENING PDF OUT FOR JOBREPORT $JobReportId\n";
      $FmDocs->{$JobReportId} = XReport::PDF::DOC->Open( 
	    XReport::FileCache::restore($JobReportId,0)
	  );
    }
    my $FmDoc = $FmDocs->{$JobReportId}; my $ListOfPages = $Report->{ListOfPages};

    my $HeaderPage = $Report->{qw(ReportName)}; #$HeaderPage =~ s/_//g;

    $lLines = [
      "", "", split("\n", $FIGlet->figify( -A => $HeaderPage, -w => 132)), "", "",
      "-- $Report->{ReportDescr} --", "", 
      form (
       "+---------------------+-----------------------------------------+",
       map {(
        "| >>>>>>>>>>>>>>>>>>> : <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< |",
        "$_->[0]", $Report->{$_->[1]}
       )}
       (
        [ "DATA TRASFERIMENTO" => "XferStartTime" ],
        [ "DATA ELABORAZIONE" => "UserDataElab" ],
        [ "DATA RIFERIMENTO" => "UserDataRef" ],
        [ "PAGINE" => "TotPages" ],
        [ "NOME JOB" => "JobName" ],
        [ "NUMERO JOB" => "JobNumber" ],
        [ "NUMERO INTERNO JOBREPORT" => "JobReportId" ]
       )
      ),
     "+---------------------+-----------------------------------------+",
    ];
    my $pageId = $self->add_page(FormatHeaderPage($lLines)); 

	my $atPage = $doc->TotPages(); 

    my ($ReportDescr, $XferStartTime) = @{$Report}{qw(ReportDescr XferStartTime)};
    if ( !exists($tref->{$ReportDescr}) ) {
      $tref->{$ReportDescr} = {};
    }
    
    $doc->CopyPages($FmDoc, split(",", $ListOfPages)); #add even page ???
	
    $tref->{$ReportDescr}->{$XferStartTime} = $doc->getPageId($atPage+1);
  }

  for my $key ( keys(%$tref) ) {
    my @values = values(%{$tref->{$key}}); 
    $tref->{$key} = $values[0]
     if 
    scalar(@values) == 1; 
  }

  $doc->setOutlines( { MANIFEST => [$mobjId, $tref] } );
  
  $doc->Close();
}

sub reset { 
  for (values(%$FmDocs)) {
    $_->Close();
  }
  $FmDocs = {}; 
}

1;
