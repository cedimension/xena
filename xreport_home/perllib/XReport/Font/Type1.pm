
package XReport::Font::Type1;

use strict;

sub getValues {
  my $self = shift; my @r; 
  for (@_) { 
    die("INVALID TYPE1 FONT FIELD $_") if !exists($self->{$_}); push @r, $self->{$_}; 
  } 
  return wantarray ? @r : $r[0];
}

my %StandardFonts = map {$_ => 1} (
  'Helvetica', 'Helvetica Bold', 'Helvetica Oblique', 'Helvetica Bold Oblique',
  'Courier', 'Courier Bold', 'Courier Oblique', 'Courier Bold Oblique',
  'Symbol',
  'Times Roman', 'Times Bold', 'Times Italic', 'Times Bold Italic'
);

my @keys_afm = (
  'StartFontMetrics',
  'Comment UniqueID',
  'Comment Panose',
  'Comment Copyright',
  'Comment Creation Date:',
  'Comment VMusage',
  'Comment Modified',
  'FullName',
  'FontName',
  'FamilyName',
  'Weight',
  'Stretch',
  'Notice',
  'Version',
  'Characters',
  'IsFixedPitch',
  'ItalicAngle',
  'FontBBox',
  'Ascender',
  'Descender',
  'XHeight',
  'CapHeight',
  'UnderlinePosition',
  'UnderlineThickness',
  'StdHW',
  'StdVW',
  'EncodingScheme',
  'StartCharMetrics'
);

my $lx_kval = eval "qr/(". join("|", @keys_afm). ") +(.*)/i"; 

my $lx_gid  = qr/N *(\w+)/; my $lx_dim = qr/(-?\d+)/;
my $lx_bbox = qr/B *$lx_dim *$lx_dim *$lx_dim *$lx_dim/;
my $lx_char = qr/C *$lx_dim *; *WX *$lx_dim *; *$lx_gid *; *$lx_bbox *;/;

sub Open {
  my ($class, $FullFontName, $iostream) = @_; my ($FontMetrics, $CharMetrics); $CharMetrics = {};

  $FontMetrics = {
    Weight => 0,
    IsFixedPitch => 0,
    ItalicAngle => 0,
    Ascender => 0,
    Descender => 0,
    Weight => 0,
    Stretch => 0,
    XHeight => 0,
    CapHeight => 0,
    UnderlinePosition => 0,
    UnderlineThickness => 0,
    StdHW => 0, StdVW => 0,
    EncodingScheme => 'AdobeStandardEncoding',
  };
  
  while(my $l = $iostream->getline()) {
	my ($key, $val) = $l =~ /$lx_kval/ or $l =~ /license conditions/
	  or
    die("INVALID AFM LINE OR KEYWORD NOT MANAGED at line: <$l>"); next if $key eq ""; 

    $key =~ s/^Comment //; $FontMetrics->{$key} = $val;
	  
	if ( $key eq "StartCharMetrics" ) {
	  for (1..$val) {
		my $l = $iostream->getline(); 

		my ($c, $wwx, $agid, $llx, $lly, $urx, $ury) = $l =~ /$lx_char/;

		$CharMetrics->{$agid} = [$c, $wwx, $llx, $lly, $urx, $ury]; 
	  }
      last;
	}
  }

  my (
    $PostscriptFontName, $FamilyName, $FontBBox, $FontWeight, $FontStretch, 
    $ItalicAngle, $isFixedPitch, $Ascender, $Descender, $XHeight, $CapHeight, 
    $UnderlinePosition, $UnderlineThickness,
    $StdHW, $StdVW, $EncodingScheme, $Version, $Copyright
  ) =
  @{$FontMetrics}{qw(
    FontName FamilyName FontBBox Weight Stretch
    ItalicAngle IsFixedPitch Ascender Descender XHEight CapHeight
    UnderlinePosition UnderlineThickness
    StdHW StdVW EncodingScheme Version Copyright
  )};
  $FullFontName = $FontMetrics->{FullName};
  my ($llx, $lly, $urx, $ury) = $FontBBox =~ /(-?[\d+])/g;

  my $hasSerifs = 0; 
  my $isSansSerifs = 1;
  my $isOrnamental = 0; 
  my $isScript = 0;
  my $isSymbolic = 0; 
  my $isItalic = $ItalicAngle != 0;
  my $isStandardFont = exists($StandardFonts{$FullFontName});

  my $Encodings = XReport::PDF::ENCODING->new();

  my $EncodingTable = $Encodings->getEncodingTable($EncodingScheme);

  my $FirstChar = -1; my $LastChar = -1; my $CharWidths = []; my $FontEncoding = {};

  for my $ienc (0..@$EncodingTable-1) {
    my $symbol = $EncodingTable->[$ienc];
    if ($symbol ne 'undef' and exists($CharMetrics->{$symbol})) {
      $FirstChar = $ienc if $FirstChar == -1; $LastChar = $ienc;
      $FontEncoding->{$symbol} = $ienc;
    }
    push @$CharWidths, (exists($CharMetrics->{$symbol}) ? $CharMetrics->{$symbol}[1] : 0);
  }

  my $self = {
    PostscriptFontName => $PostscriptFontName,
    FullFontName => $FullFontName,
    FontFamily => $FamilyName,
    FontType => 'TrueType',
    FontWeight => $FontWeight,
    FontStretch => $FontStretch,
    Notice => '',
    Version => '',
    Characters => '',
    EncodingScheme => $EncodingScheme,

    isFixedPitch => $isFixedPitch,
    hasSerifs => $hasSerifs,
    isSansSerifs => $isSansSerifs,
    isOrnamental =>$isOrnamental,
    isScript => $isScript,
    isSymbolic => $isSymbolic,
    isItalic => $isItalic,
    isStandardFont => $isStandardFont,

    ItalicAngle => $ItalicAngle,
    llx => $llx, lly => $lly, urx => $urx, ury => $ury,
    FontBBox => "[$llx $lly $urx $ury]",
    Ascender => $Ascender,
    Descender => $Descender,
    XHeight => $XHeight,
    CapHeight => $CapHeight,
    UnderlinePosition => $UnderlinePosition,
    UnderlineThickness => $UnderlineThickness,
    
    StdHW => $StdHW, StdVW => $StdVW,

    FontEncoding => $FontEncoding, CharWidths => $CharWidths
  };

  bless $self, $class;
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
}

1;
