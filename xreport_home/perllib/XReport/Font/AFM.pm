
package XReport::Font::AFM;

use strict;

use FileHandle;

my @keys_afm = (
  'StartFontMetrics',
  'Comment UniqueID',
  'Comment Panose',
  'Comment Copyright',
  'Comment Creation Date:',
  'Comment VMusage',
  'Comment Modified',
  'FullName',
  'FontName',
  'FamilyName',
  'Weight',
  'Notice',
  'Version',
  'Characters',
  'IsFixedPitch',
  'ItalicAngle',
  'FontBBox',
  'Ascender',
  'Descender',
  'XHeight',
  'CapHeight',
  'UnderlinePosition',
  'UnderlineThickness',
  'StdHW',
  'StdVW',
  'EncodingScheme',
  'StartCharMetrics'
);

my $lx_kval = eval "qr/(". join("|", @keys_afm). ") +(.*)/i"; 

my $lx_gid  = qr/N *(\w+)/; my $lx_dim = qr/(-?\d+)/;
my $lx_bbox = qr/B *$lx_dim *$lx_dim *$lx_dim *$lx_dim/;
my $lx_char = qr/C *$lx_dim *; *WX *$lx_dim *; *$lx_gid *; *$lx_bbox *;/;

my $cps = {};

sub toString {
  my $self = shift;
  
  my $metrics = $self->{'FontMetrics'};

  for (sort(keys(%$metrics))) {
    print "$_ : $metrics->{$_}\n";
  }

  my $cmetrics = $self->{'CharMetrics'};

  for (sort(keys(%$cmetrics))) {
    print "$_ : ", join(",", @{$cmetrics->{$_}}), "\n";
  }
}

sub getFontValues {
  my $self = shift; my $FontMetrics = $self->{'FontMetrics'}; my @r;
  for (@_) {
    push @r, $FontMetrics->{$_};
  }
  return wantarray ? @r : $r[0];
}

sub ParseAfm {
  my $self = shift; my $FileName = $self->{'FileName'}; my $metrics = {};
  
  my $INPUT = FileHandle->new("<$FileName") 
     or
  die("AFM FILE INPUT OPEN ERROR \"$FileName\" $!");
  
  while(my $l = $INPUT->getline()) {
	my ($key, $val) = $l =~ /$lx_kval/ or $l =~ /license conditions/
	  or
    die("INVALID AFM LINE OR KEYWORD NOT MANAGED at line: <$l>"); next if $key eq ""; 

    $key =~ s/^Comment //; $metrics->{$key} = $val;
	  
	if ( $key eq "StartCharMetrics" ) {
	  my $cmetrics = {};
	  for (1..$val) {
		my $l = $INPUT->getline(); 

		my ($c, $wwx, $agid, $llx, $lly, $urx, $ury) = $l =~ /$lx_char/;

		$cmetrics->{$agid} = [$c, $wwx, $llx, $lly, $urx, $ury]; 
	  }
	  @{$self}{qw(FontMetrics CharMetrics)} = ($metrics, $cmetrics); last;
	}
  }

  my $cp1252s = $cps->{cp1252}; my $cmetrics = $self->{CharMetrics};

  my $cp1252w = [];

  for (0..255) {
    $cp1252w->[$_] = exists($cmetrics->{$cp1252s->[$_]})
      ? $cmetrics->{$cp1252s->[$_]}->[1]
      : $cmetrics->{'space'}->[1]
    ;
    #print "?? $cp1252->[$_] ==> $cp1252w->[$_]\n";
  } 
  
  $INPUT->close(); 

  $self->{cpw}->{cp1252} = $cp1252w;
}

sub getWidthTable {
  my ($self, $EncodingTable) = @_; my ($cmetrics) = @{$self}{qw(CharMetrics)}; my @lw;
  
  my $lspace = $cmetrics->{'space'}->[1];

  for(@$EncodingTable) {
    push @lw, (exists($cmetrics->{$_}) ? $cmetrics->{$_}->[1] : $lspace);
  }

  return \@lw;
}

sub StringWidth {
  my ($self, $t, $size) = @_; my $cp1252w = $self->{cpw}->{cp1252}; my $tw = 0;

  my $at = 0; my $max_at = length($t)-1;

  while($at<=$max_at) {
    $tw += $cp1252w->[ord(substr($t,$at,1))]; $at += 1;
  }

  $tw*$size/1000;
}

sub FileName {
  my $self = shift; return $self->{'FileName'};
}

sub new {
  my ($ClassName, $FileName, %args) = @_; $FileName =~ s/\.pfb$//i; 
  
  if (!exists($cps->{cp1252})) {
    my ($l, @cp1252s);

    open(IN, "<c:/fsf/lout/lout-3.30/maps/cp1252.LCM");
    while($l = <IN>) {
      my ($dec, $oct, $agid) = $l =~ /([^\s]+)/g; 
      $agid = "space" if $agid eq "-none-";
      #print "($dec // $oct // $agid //)\n" ;
      $cp1252s[$dec] = $agid;
    }
    close(IN);
    $cps->{cp1252} = \@cp1252s;
  }
  
  my $self = {
	FileName => $FileName, args => \%args, cpw => {} 
  }; 
  
  bless $self, $ClassName; $self->ParseAfm(); return $self;
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
}

1;

__END__
package main;

use strict;

my $afm = XReport::Font::AFM->new("c:/fsf/lout/lout-3.30/font/timesMS");

print $afm->StringWidth('aA minchia fritta', 10), "\n";
