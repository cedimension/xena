
package XReport::Font::TrueType;

use strict;

use Font::TTF::Font;

sub getValues {
  my $self = shift; my @r; 
  for (@_) { 
    die("INVALID TRUE TYPE FONT FIELD $_") if !exists($self->{$_}); push @r, $self->{$_}; 
  } 
  return wantarray ? @r : $r[0];
}

my $FontStretches = {
  1 => 'UltraCondensed', 
  2 => 'ExtraCondensed',
  3 => 'Condensed', 
  4 => 'SemiCondensed',
  5 => 'Normal', 
  6 => 'SemiExpanded', 
  7 => 'Expanded', 
  8 => 'ExtraExpanded', 
  9 => 'UltraExpanded'
};

=sFamilyClass_1
    FAMILY_CLASS_OLDSTYLE_SERIFS = 1;
    FAMILY_CLASS_TRANSITIONAL_SERIFS = 2;
    FAMILY_CLASS_MODERN_SERIFS = 3;
    FAMILY_CLASS_CLAREDON_SERIFS = 4;
    FAMILY_CLASS_SLAB_SERIFS = 5;
    FAMILY_CLASS_FREEFORM_SERIFS = 7;

    FAMILY_CLASS_SANS_SERIF = 8;
    
    FAMILY_CLASS_ORNAMENTALS = 9;
    
    FAMILY_CLASS_SCRIPTS = 10;
    
    FAMILY_CLASS_SYMBOLIC = 12;
=cut

sub Open {
  my ($class, $FullFontName, $iostream) = @_;

  my $font = Font::TTF::Font->open($iostream) or die("INPUT iostream is NOT a TTF/OTF - $!");

  my $head = $font->{'head'}; $head->read(); my $name = $font->{'name'}; $name->read();
  my $post = $font->{'post'}; $post->read(); my $STRINGS = $post->{'STRINGS'};
  my $hmtx = $font->{'hmtx'}; $hmtx->read(); my $advance = $hmtx->{'advance'};
  my $os_2 = $font->{'OS/2'}; $os_2->read();
  my $glyf = $font->{'glyf'}; $glyf->read(); my $glyphs = $font->{'loca'}{'glyphs'};

  my ($uem, $xMin, $yMin, $xMax, $yMax) = @{$head}{qw(unitsPerEm xMin yMin xMax yMax)};
  my (
    $sFamilyClass, $sTypoAscender, $sTypoDescender, 
    $usWeightClass, $usWidthClass 
  ) = 
  @{$os_2}{qw(sFamilyClass sTypoAscender sTypoDescender usWeightClass usWidthClass)};

  my ($sFamiliClass_1, $sFamiliClass_2) = (int($sFamilyClass/256), $sFamilyClass % 256);

  my $PostscriptFontName = $name->find_name(6);
  my $lFullFontName = $name->find_name(4);
  
  $lFullFontName eq $FullFontName
   or
#  die("Font Names NOT MATCHED ($FullFontName/$lFullFontName)");  

  i::logit("Font Names NOT MATCHED ($FullFontName/$lFullFontName)");  

  my $FamilyName = $name->find_name(1);
  my $ItalicAngle = $post->{'italicAngle'};
  my $underlineThickness = $post->{'underlineThickness'};
  my $underlinePosition = $post->{'underlinePosition'};
  my $FontWeight = $usWeightClass;
  my $FontStretch = $FontStretches->{$usWidthClass};
    
  my $isFixedPitch = $post->{'isFixedPitch'};
  my $hasSerifs = $sFamiliClass_1 >= 1 || $sFamiliClass_1 <= 7;
  my $isSansSerifs = $sFamiliClass_1 == 8;
  my $isOrnamental = $sFamiliClass_1 == 9;
  my $isScript = $sFamiliClass_1 == 10;
  my $isSymbolic = $sFamiliClass_1 == 12;
  my $isItalic = $ItalicAngle != 0;
  
  my (
    $llx, $lly, $urx, $ury, $Ascender, $Descender, $UnderlineThickness, $UnderlinePosition
  ) = 
  map {int($_/$uem*1000+0.5)} ($xMin, $yMin, $xMax, $yMax, $sTypoAscender, $sTypoDescender, $underlineThickness, $underlinePosition);

  my ($XHeight, $CapHeight, $StemV) = (0, 0, 0);
  if (!$isSymbolic) {
    my $H_glyph = $glyphs->[$STRINGS->{'H'}]; $H_glyph->read(); 
    my $x_glyph = $glyphs->[$STRINGS->{'x'}]; $x_glyph->read();
    my $I_glyph = $glyphs->[$STRINGS->{'I'}]; $I_glyph->read(); \
    $I_glyph->get_points(); my ($I_xlist, $I_ylist) = @{$I_glyph}{qw(x y)};
   ($XHeight, $CapHeight) = map {int($_->{'yMax'}/$uem*1000+0.5)} ($x_glyph, $H_glyph);
  }

  my ($StdHW, $StdVW) = (0, 0);

  my $self = {
    PostscriptFontName => $PostscriptFontName,
    FullFontName => $FullFontName,
    FontFamily => $FamilyName,
    FontType => 'TrueType',
    FontWeight => $FontWeight,
    FontStretch => $FontStretch,
    Notice => '',
    Version => '',
    Characters => '',

    isFixedPitch => $isFixedPitch,
    hasSerifs => $hasSerifs,
    isSansSerifs => $isSansSerifs,
    isOrnamental =>$isOrnamental,
    isScript => $isScript,
    isSymbolic => $isSymbolic,
    isItalic => $isItalic,
    isStandardFont => 0,

    ItalicAngle => $ItalicAngle,
    llx => $llx, lly => $lly, urx => $urx, ury => $ury,
    FontBBox => "[$llx $lly $urx $ury]",
    Ascender => $Ascender,
    Descender => $Descender,
    XHeight => $XHeight,
    CapHeight => $CapHeight,
    UnderlinePosition => $UnderlinePosition,
    UnderlineThickness => $UnderlineThickness,

    StdHW => $StdHW, StdVW => $StdVW,

    FontEncoding => {%$STRINGS}, CharWidths => [map {int($_/$uem*1000+0.5)} @$advance]
  };

  bless $self, $class;
}

1;

