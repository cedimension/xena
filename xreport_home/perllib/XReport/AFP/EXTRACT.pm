package XReport::AFP::EXTRACT;

use strict;

use Symbol;
use IO::File;
use Compress::Zlib;
use Convert::EBCDIC;

use XReport;
use XReport::Util;
use XReport::DBUtil;

#------------------------------------------------------------

use constant TU_EOF => 0xFFFF;
use constant TU_RESNAME => 0x1001;
use constant TU_BINSTREAM => 0x1002;
use constant TU_PSSTREAM => 0x1003;
use constant TU_USEDRESOURCES => 0x1004;

use constant UM_FULL => 1;
use constant UM_INCREMENTAL => 2;

#------------------------------------------------------------

my $tr = Convert::EBCDIC->new();

sub ReadTextUnits {
  my ($BINSTREAM, $FileOffset, $resName, @TextUnits) = @_; my @r = ();

  my %TUdata = map {$_, undef} @TextUnits;
  
  seek($BINSTREAM, $FileOffset, 0);
  
  while(1) {
    my ($k, $v, $buff); read($BINSTREAM, $buff, 2); $k=unpack("n", $buff);
	
    if( $k == TU_RESNAME ) {   #Resource Name;
	  read($BINSTREAM,$buff,6); my ($l1, $l2) = unpack("nN", $buff);
	  read($BINSTREAM, $v, $l2);
	  if ( $v ne $resName ) {
		die "AFP::EXTRACT: MISMATCH ON RESOURCE=($resName/$v) FileOffset=$FileOffset";
	  }
	}
    elsif( $k == TU_BINSTREAM ) {   #BINARY IMAGE;
	  read($BINSTREAM,$buff,14); my ($l1, $l2, $l3, $l4) = unpack("nNNN", $buff);
      if ( exists($TUdata{$k}) ) {
	    read($BINSTREAM, my $v, $l4);
		$v = uncompress($v);
		if ( $l3 ne length($v) ) {
		  die "AFP::EXTRACT: BIN LENGTH MISMATCH ($l3/".length($v).")ON RESOURCE=$resName FileOffset=$FileOffset";
		}
        $TUdata{$k} = \$v;
	  }
	  else {
	    seek($BINSTREAM, $l4, 1);
	  }
	}
    elsif( $k == TU_PSSTREAM ) {   #POSTSCRIPT IMAGE;
	  read($BINSTREAM,$buff,14); my ($l1, $l2, $l3, $l4) = unpack("nNNN", $buff);
      if ( exists($TUdata{$k}) ) {
	    read($BINSTREAM, my $v, $l4);
		$v = uncompress($v);
		if ( $l3 ne length($v) ) {
		  die "AFP::EXTRACT: PS LENGTH MISMATCH ($l3/".length($v).")ON RESOURCE=$resName FileOffset=$FileOffset";
		}
        $TUdata{$k} = \$v;
	  }
	  else {
	    seek($BINSTREAM, $l4, 1);
	  }
	}
    elsif( $k == TU_USEDRESOURCES ) {   #USED RESOURCES;
	  read($BINSTREAM,$buff,6); my ($l1, $l2) = unpack("nN", $buff);
      if ( exists($TUdata{$k}) ) {
	    read($BINSTREAM, $v, $l2);
        $TUdata{$k} = $v;
	  }
	  else {
	    seek($BINSTREAM, $l2, 1);
	  }
	}
	elsif ( $k == TU_EOF ) {
	  last;
	}
    else {
	  die "INVALID TEXT UNIT KEY \"$k\"\n";
	}
  }

  for (@TextUnits) {
    if (!defined($TUdata{$_})) {
	  die "AFP::EXTRACT: MISSING TEXTUNIT $_ for RESOURCE $resName";
	}
#    print "Textunit $_: ", (ref($TUdata{$_}) ? ${$TUdata{$_} } : $TUdata{$_}), "\n"; # mpezzi cntl file handling
    push @r, $TUdata{$_};
  }
#  print "Returning ", scalar(@r), " TextUnits\n"; # mpezzi cntl file handling
  
  return @r;
}

sub SelectResource {
  my ($resName, $TimeFrom, $TimeTo) = @_;
  
  return dbExecute(
   "SELECT TOP 1 * FROM tbl_Os390Resources a
    INNER JOIN tbl_Os390ResFiles b ON a.FileId = b.FileId
    WHERE a.ResName = '$resName' 
	 AND b.TimeRef BETWEEN '$TimeFrom' AND '$TimeTo'
	ORDER BY TimeRef DESC
   "
  );
}

sub EbcdicName {
  return $tr->toebcdic(substr($_[0]." "x8,0,8));
}

sub afp_rec {
  my ($hdr, $body) = @_; my $str;
  $str = pack("H6", $hdr) . "\x00\x00\x00" . $body;
  $str = "\x5a". pack("n", length($str)+2). $str;
  return pack("n", length($str)), $str;
}

my %res_type = (
  "G" => "\x03", # Graphics (GOCA) object 
  "B" => "\x05", # Bar Code (BCOCA) object 
  "I" => "\x06", # Image (IOCA) object 
  "C" => "\x40", # Font Character Set object 
  "T" => "\x41", # Code Page object 
  "X" => "\x42", # Coded Font object 
  "R" => "\x92", # Object Container 
  "D" => "\xA8", # Document object 
  "S" => "\xFB", # Page Segment object 
  "O" => "\xFC", # Overlay object 
  "P" => "\xFD", # Pagedef object 
  "F" => "\xFE", # Formdef object
);


sub MakeAfpResGroup {
  my ($OUTPUT, $TimeRef, @reslist) = @_; 
  my %resources;
  my $dbr; 

  if (!ref($OUTPUT)) {
    my $fileName = $OUTPUT;
    $OUTPUT = gensym(); 
    open($OUTPUT, ">$fileName") or die("EXTRACT OUTPUT Open ERROR $fileName $!"); binmode($OUTPUT);
  }
  
  print $OUTPUT afp_rec("d3a8c6", pack("H*", "ffffffffffffffff"));

  $dbr = dbExecute( "SELECT CONVERT(varchar, MAX(TimeRef), 120) AS TimeRef 
                       FROM tbl_Os390ResFiles 
                      WHERE UpdMode=1 AND TimeRef <= '$TimeRef'
    "
  );
  my $TimeFrom = $dbr->Fields->Item('TimeRef')->Value();
  
  my %FileIds=(); $OUTPUT = gensym();
  
  while(my $resName = shift @reslist) {
    $dbr = SelectResource($resName, $TimeFrom, $TimeRef);
    my $resref;
    if ($dbr->eof()) {
      i::logit(__PACKAGE__." MakeAfpResGroup REQUESTED RESOURCE $resName NOT FOUND for $TimeRef from $TimeFrom".
              " - caller: ".join('::', (caller())[0,2]));
      $dbr->Close();
      next;
    }
	my $FileId = $dbr->Fields()->Item('FileId')->Value();
  
    if ( !exists($FileIds{$FileId}) ) {
      (my $ibmzloc = XReport::Util::getConfValues('ibmzlib') ) =~ s/\\/\//g; ### mpezzi handle ibmzlib relative reloc
      my $ibmzlib_fn = $dbr->Fields()->Item('ibmzlib')->Value();
      my $ibmzlib = "$ibmzloc/$ibmzlib_fn";
      my $INPUT = gensym();
      open($INPUT, "<$ibmzlib") or die("IBMZLIB OPEN ERROR \"$ibmzlib\" $!"); 
      binmode($INPUT);
	  $FileIds{$FileId} = $INPUT;
	}
	
    my $FileOffset = $dbr->Fields()->Item("FileOffset")->Value();
    ($resref, my $UsedResources) = ReadTextUnits(
	  $FileIds{$FileId}, $FileOffset, $resName, TU_BINSTREAM, TU_USEDRESOURCES
	);

	my $EbcdicName = EbcdicName($resName);
    my $rt = $res_type{uc(substr($resName,0,1))};
	
	my $beg_res = afp_rec("d3a8ce", "$EbcdicName\x0a\x21$rt"."\x00"x7);
	my $end_res = afp_rec("d3a9ce", "$EbcdicName");
	
	print $OUTPUT $beg_res, $$resref, $end_res;
  
    my @UsedResources = map {uc($_)} split(/[, ]+/, $UsedResources);
	
    for (@UsedResources) {
      if (!exists($resources{$_})) {
        push @reslist, $_;
	    $resources{$_} = 1;
	  }
    }
  }
  
  for (keys(%FileIds)) { close($FileIds{$_}); }
  print $OUTPUT afp_rec("d3a9c6", pack("H*", "ffffffffffffffff"));
}

use File::Copy qw();
sub copyFromAfpDir {
    my $LocalResources = shift;
    (my $afpdir) = XReport::Util::getConfValues('afpdir');
    my $resName = $_[0] . ($_[0] =~ /^CNTL\./ ? '' : '.ps');
    my $outfilename = $LocalResources.'/'. $resName ;
    for my $subdir ( qw(FDEFLIB PDEFLIB OVERLIB PSEGLIB CNTLLIB 
                        fonts/userfonts fonts/charsets.outline fonts/charsets.bitmap) ) {
        next unless -e "$afpdir/$subdir";                   
        my $infilename = "$afpdir/$subdir/$resName";
        if ( -e $infilename ) {
            File::Copy::cp($infilename, $outfilename);
            my $outsize = -s $outfilename;
            unlink $outfilename unless $outsize;
            return ($outsize || undef);
        }
    } 
    i::logit(__PACKAGE__." copyFromAfpDir REQUESTED RESOURCE $resName NOT FOUND - caller: ".join('::', (caller())[0,2]));
    return undef;
}

sub ToLocalResourcesFromHere {
  my ($LocalResources, $TimeRef, $res2process) = @_; my %LocalResList; my $dbr; 
  $main::debug && i::warnit("AFP EXTRACT ToLocalResourcesFromHere called by " .join('::', (caller())[0,2])
                           ." LR: $LocalResources TRef: $TimeRef reslist: ".ref($res2process));
  
  mkdir($LocalResources) if !-d $LocalResources;
  (my $ibmzloc) = XReport::Util::getConfValues('ibmzlib'); ### mpezzi handle ibmzlib relative reloc

  $dbr = dbExecute(
    "SELECT CONVERT(varchar, MAX(TimeRef), 120) AS TimeRef 
     from tbl_Os390ResFiles 
     where UpdMode>1 AND TimeRef <= '$TimeRef'
    "
  );
  my $TimeFrom = $dbr->Fields->Item('TimeRef')->Value();
  
  my %FileIds=(); 
  my $OUTPUT = gensym();
  while(my $resName = shift @{$res2process}) {

    $dbr = SelectResource($resName, $TimeFrom, $TimeRef);
    
    if ($dbr->eof()) {
      $main::debug && i::warnit("Resource $resName not found on DB - looking into statics");
      my $copycc = copyFromAfpDir($LocalResources, $resName);
      $LocalResList{$resName} = 1 if $copycc;
      next;
    }
    my $FileId = $dbr->Fields()->Item('FileId')->Value();
    my $ibmzlib = $dbr->Fields()->Item('ibmzlib')->Value();
    
    if ( !exists($FileIds{$FileId}) ) {
      my $INPUT = new IO::File("<$ibmzloc/$ibmzlib")
                               or die("IBMZLIB OPEN ERROR \"$ibmzlib\" in \"$ibmzloc\" $!");
      $INPUT->binmode();
      $FileIds{$FileId} = $INPUT;
    }
    
    my $FileOffset = $dbr->Fields()->Item("FileOffset")->Value();
    my ($resdataref, $UsedResources) = ReadTextUnits(
						  $FileIds{$FileId}, 
						  $FileOffset, 
						  $resName, 
						  TU_PSSTREAM, 
						  TU_USEDRESOURCES
						 );

    my $outresname = $resName . ($resName =~ /^CNTL\./ ? '' : '.ps');
    $main::debug && i::warnit("Now writing $outresname from $ibmzloc/$ibmzlib to $LocalResources");
    
    open($OUTPUT, ">$LocalResources/$outresname"); binmode($OUTPUT);
    
#    open($OUTPUT, ">$LocalResources/$resName\.ps"); binmode($OUTPUT);
    print $OUTPUT $$resdataref;
    close($OUTPUT);
    
    my @UsedResources = map {uc($_)} split(/[, ]+/, $UsedResources);
    
    for (@UsedResources) {
      my $resfilename = $LocalResources.'/'. $_ . ($_ =~ /^CNTL\./ ? '' : '.ps');
      unless ( exists($LocalResList{$_}) or -e $resfilename ) {
         push @{$res2process}, $_;
         $LocalResList{$_} = 1;
         $main::debug && i::warnit("$resName requires to additionally load $_");
      }
    }
  }
  
  for (keys(%FileIds)) { close($FileIds{$_}); }
}

sub ToLocalResources {
  my ($LocalResources, $TimeRef, @resources) = @_; return if !scalar(@resources); 
  $main::debug && i::warnit("AFP EXTRACT ToLocalResources called by " .join('::', (caller())[0,2])
                           ." LR: $LocalResources TRef: $TimeRef reslist: ".join(',', @resources));

  my $AfpResUrl = getConfValues('AfpResUrl');
  unless ( ref($AfpResUrl) and ($AfpResUrl =~ /^http/) ) {
    ToLocalResourcesFromHere($LocalResources, $TimeRef, \@resources); 
    return; 
  } 
  
  require RPC::XML; require RPC::XML::Client;
  
  &$logrRtn("RPCXML: BEGIN MakePsLocalResources for $AfpResUrl ==> $LocalResources");
        
  my $cli = RPC::XML::Client->new($AfpResUrl);
  
  my $ref = $cli->send_request(
   'ToLocalRes',  
    $LocalResources, $TimeRef, \@resources
  );
  
  if ( UNIVERSAL::isa($ref, "RPC::XML::int") ) {
    my $rc = $ref->value();
    &$logrRtn("RPCXML: END MakePsLocalResources rc=$rc");
  }
  
  elsif ( UNIVERSAL::isa($ref, "RPC::XML::fault") ) {
    my ($code, $string) = ($ref->code(), $ref->string());
    die("EXTRACT AFP RESOURCES ERROR: $code/$string");
  }
  
  elsif ( !ref($ref) ) {
    die "RestartableError: RPCXML ERROR: $ref";
  }
  
  else {
    die("INVALID RPC::XML::send_request RESPONSE: $ref");
  }
}

1;


