
#------------------------------------------------------------
# UserExit to change(verify AFP trn data

package XReport::AFP::UserExit;

sub CheckTrn {
  die join(",", @_), "\n";
}

sub new {
  my ($class) = shift; bless {}, $class;
}

1;

#------------------------------------------------------------
# PostScript Output Management

package XReport::AFP::PRINTPS;

use strict;

use Carp;
use Symbol;

my ($PdefWriteMode, $PageDefDir);

sub open {
  my ($self, $PsFileName) = (shift, shift);

  die("ApplicationError: PRINTPS MISSING DIRECTORY PARAMETER")
   if 
  ($self->{directory} eq "");
  
  if ( $PsFileName eq "") {
    $PsFileName = $self->{PsFileName}; 
  }
  else {
    $PsFileName = "$self->{directory}/$PsFileName\.ps";
    $self->{PsFileName} = $PsFileName;
  }
  
  warn ref($self), " opening \"$self->{PsFileName}\"", sprintf(" as requested by %s at %s", (caller())[0,2]), "\n" if $main::debug;
  open($self->{OUTPUT}, ">$self->{PsFileName}") 
   or 
  croak("ApplicationError: OPEN Error \"$PsFileName\" $!");
#  if($PsFileName =~ /\$\$initafp/) {
#     @{$self}{qw(PsLines binary)} = ([], 0);
#     $self->add("%initafp");
#  }
}

sub PsLines {
  my ($self, $PsLines) = @_; $self->{PsLines} = $PsLines if $PsLines; return $self->{PsLines};
}

sub add {
  my $self = shift; my $PsLines = $self->{PsLines}; push @$PsLines, @_;
}

sub insert {
  my $self = shift; my ($re, $Cmds) = @_; 
  my ($PsLines, $off) = ($self->{PsLines}, 0);
  for (@$PsLines) {
    $off += 1;
    if ( $_ =~ /$re/o ) {
      splice @$PsLines, $off, 0, @$Cmds; return;
    }
  }
}


sub close {
  my $self = shift; my ($OUTPUT, $binary) = @{$self}{qw(OUTPUT binary)}; my $chain = $binary ? "" : "\n";
  warn ref($self), " closing ", ref($OUTPUT), sprintf(" as requested by %s at %s", (caller())[0,2]), "\n" if $main::debug;
  print $OUTPUT join($chain, @{$self->{PsLines}}), $chain;
  @{$self}{qw(PsLines binary)} = ([], 0);
  close($OUTPUT);
}

sub PdefWriteMode {
}

sub opendm {
  my $self = shift; my $PdefWriteMode = $self->{PdefWriteMode};
  if ( $PdefWriteMode == 2 ) {
    CORE::open(DATAMAP, ">$PageDefDir/$_[0]\.ps") 
     or 
    croak("ApplicationError: DATAMAP Open ERROR $_[0] $!\n");
  }
}

sub printdm {
  my $self = shift; my $PdefWriteMode = $self->{PdefWriteMode};
  if ( $PdefWriteMode == 2 ) {
    print DATAMAP @_;
  }
  else {
    $self->print(@_);
  }
}

sub closedm {
  my $self = shift; my $PdefWriteMode = $self->{PdefWriteMode};
  if ( $PdefWriteMode == 2 ) {
    CORE::close(DATAMAP);
  }
}

sub binarymode {
  my $self = shift; $self->{binary} = 1; binmode($self->{OUTPUT});
}

sub new {
  my ($className, $directory) = (shift, shift) ; my $self = {};
  warn "$className new ", sprintf(" called by %s@%s", (caller())[0,2]), " dir: ", ($directory || "default"), "\n" if $main::debug;  
  $self->{directory} = $directory;
  $self->{PsLines} = [];
  $self->{OUTPUT} = gensym();
  $self->{binary} = 0;
  
  bless $self, $className;
}
#
##------------------------------------------------------------
#package XReport::INPUT::RasterLine;
#
#use Carp;
#
#sub new {
#  my ($className, $line, $atLine) = @_;
#
#  my $self = [$line, $atLine];
#  
#  bless $self, $className;
#}
#
#sub AsciiValue {
#  my $self = shift;
#  return $self->[0];
#}
#
#sub Value {
#  my $self = shift;
#  return $self->[0];
#}
#
#sub atLine {
#  my $self = shift;
#  if ( defined($_[0]) ) {
#    $self->[1] = $_[0];
#  }
#  return $self->[1];
#}

#------------------------------------------------------------
package XReport::AFP::INPUTStream;

use strict;
use File::Basename;


#my (%TRIPLETS, @istack) = (); my $in_ptoca_seqs = 0; my $ps;
my @istack = (); my $in_ptoca_seqs = 0;

#my $localdir = File::Basename::dirname(__FILE__); my ($t1, $t2, $t3);
#
#if (0) {
#open(INPUT,"<$localdir/afp.triplets.txt") || die("TRIPLETS File Open Error $!");
#
#while(<INPUT>) {
#  chomp;
#  $t1 = substr($_,4,6);
#  $t2 = substr($_,14,5);
#  $t3 = substr($_,26,35);
#  $TRIPLETS{lc($t1)} = $t3;
#}
#
#open(INPUT,"<$localdir/afp.triplets2.txt") || die("TRIPLETS File Open Error $!");
#
#while(<INPUT>) {
#  chomp;
#  $t1 = substr($_,0,5);
#  $t2 = substr($_,7,6);
#  $t3 = substr($_,17);
#  $TRIPLETS{lc($t2)} = $t3;
#}
#
#open(INPUT,"<$localdir/afp.triplets3.txt") || die("TRIPLETS File Open Error $!");
##X'D38C87' � Code Page Index (CPI)  � "CPI - D38C87 - Code Page Index" in topic 6.1.2.8 �
#while(<INPUT>) {
#  chomp;
#  if (/X'([^']+)'[^"]+"(\w\w\w) - [^ ]+ - ([^"]+)"/) {
#    $t1 = $1;
#    $t2 = $2;
#    $t3 = $3;
#    $TRIPLETS{lc($t1)} = "$t3 ($t2)";
#  }
#}
#
#close(INPUT);
#}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT}; @istack = ();
  warn ref($self), " open ", ref($INPUT), sprintf(" as requested by %s at %s", (caller())[0,2]), "\n" if $main::debug;

  $INPUT->Open(@_);
}

sub Close {
  my ($self, %args) = @_; my $INPUT = $self->{INPUT};
  warn ref($self), " close ", ref($INPUT), sprintf(" as requested by %s at %s", (caller())[0,2]), "\n" if $main::debug;

  return $INPUT->Close(@_);
}

sub eof {
    print "xxxxx\n";
  my $self = shift; my $INPUT = $self->{INPUT}; $INPUT->eof();
}

sub push_back {
  my $self = shift; push @istack, [@_];
}

sub getLine {
  $_[0]->{'INPUT'}->getLine()
}

sub readAfpBlk {
  my $self = shift; my $INPUT = $self->{INPUT}; my ($c5A, $l5A, $r5A, $d5A, $t5A, $trd) = ();

  if (@istack) {
    ($t5A, $trd) = @{shift @istack}; return ($t5A, $trd);
  }
  print "INPUT readAfpBlk - EOF reached\n" if $INPUT->eof();
  return undef if $INPUT->eof();
  
  my $line = $INPUT->getLine();
  return undef if (!$line && $INPUT->eof());
  Carp::croak("UserError: FIRST CHAR NOT 5A \"".unpack("H40", $line)."\" at ". $INPUT->tell() . ' INPUT REF: ', ref($INPUT->{getLine}))
        unless $line =~ /^\x5a/; 

  ($c5A, $r5A, my $z) = unpack('a n @1 /a a*', $line);
  ($l5A, $t5A, $trd) = unpack('n H6 x3 a*', $r5A);

  #print $OUTPUT "$t5A $TRIPLETS{$t5A} ($l5A)\n";
  #print $OUTPUT ">> ", unpack("H*", $trd), "\n";
  
  if ($t5A ne "d3ee9b" and $in_ptoca_seqs) {
    die "Pending in_ptoca_seqs but no output file defined" unless $XReport::AFP::IOStream::ps;
    $XReport::AFP::IOStream::ps->add("a.terminate.ptocaseqs"); 
    $in_ptoca_seqs = 0;
  }

  #print "!!!! $t5A // $TRIPLETS{$t5A} //\n";
  $main::veryverbose && i::warnit("::readAfpBlk - AFP REC: $t5A - DATA: ", unpack('H60', $trd));

  return ($t5A, $trd);
}

sub new {
  my ($class, $INPUT) = @_; bless {INPUT => $INPUT}, $class;
}

#------------------------------------------------------------
package XReport::AFP::IOStream;

our @ISA = ('XReport::INPUT::IPage'); #GetAbsRect

use strict;
use Carp;

#use XReport::AUTOLOAD;

use Convert::EBCDIC;

require XReport::AFP::FONTs;

#------------------------------------------------------------
#Global Vars
our $ps;
our $USED_RESOURCES = {};
our $FileName;

$main::translator = Convert::EBCDIC->new();

#sub hashix {
#  my %index;
#  my $what = shift; my $haystack = shift;
#  my ($result, $ix) = ({}, 0);
#  for (split / +/, $haystack) {
#    /^$what$/ && ($result->{$ix} = $1);
#  } continue {
#    $ix++
#  }
#  print STDERR "===> $haystack ==> $what\n".Dumper($result)."\n<===\n"; # if scalar(keys %$result);
#  return $result;
#}
#
sub main::toascii {
  my $str = shift;
  return (!$str ? '' : $str =~ /[^\xff]/ ? $main::translator->toascii($str) : "x\"".unpack("H*", $str)."\"");
}

sub main::mytoascii {
    return main::translator->toascii($_[0]);
}

#sub myunpackeb {
#  my $mask = $_[0]; my $ix = 0;
#  my $xmsk = qr/([eE]|\:\w+\:)(\*|\d+)/;
#  my $strings;
#  if ($mask =~ /$xmsk/) {
#    $strings = hashix($xmsk, $mask);
#    $mask =~ s/$xmsk/a$2/g;
#  }
#  $mask =~ s/([sS])(\d?)/($1>)$2/g;
#  $mask =~ s/([cC])/a/g;
#  $mask =~ s/m3/xs>/g;
#  $mask =~ s/M3/xS>/g;
#  $mask =~ s/m2/s>/g;
#  $mask =~ s/M2/S>/g;
#  $mask =~ s/m1/c/g;
#  $mask =~ s/M1/C/g;
#  die "Invalid mask \"$mask\" for " . unpack("H32", ($_[1])) if $mask =~ /-/;
#  print "UNPACKEB: IN: ". unpack('H*', $_[1])." MASK: ".$_[0], " NEWMASK: ".$mask, "\n"; 
#  my $result = [ unpack($mask, $_[1]) ];
#  while (my ($ix, $name) = each %$strings) {
#    $name = ($name =~ /^[eE]$/ ? 'mytoascii' : $name =~ /^:(\w+)\:/);
#    print "===>mask: $mask ix: $ix name: $name\n";
#    my $parser = main->can($name);
#    $result->[$ix] = &$parser($result->[$ix]) if $parser;
#  }
##  print "UNPACKEB - FLDS: ".join('::', @$result), "\n"; 
#  return @$result;
#}

my %TRIPLETS = ();

my %ROTATE_Angle = (
  "(0000) (2d00)" => 0,
  "(8700) (0000)" => -90,
  "(5a00) (8700)" => -180,
  "(2d00) (5a00)" => -270,
);

my %ROTATE_CC = (
  "(0000) (2d00)" => 0,
  "(8700) (0000)" => 1,
  "(5a00) (8700)" => 2,
  "(2d00) (5a00)" => 3,
);

my %ROTATE_PGP2 = (
  "0000" => '00',
  "2d00" => '01',
  "5a00" => '04',
  "8700" => '05',
);

my %ccpTest = (
  1 => "eq"
);

my $trcFlag = 1;

my ($r5adata, $r5atype, $c5A, $b5A, $r5A, $fileType, $strImg, $strImgCell, @strImgCell, $ImageOr, $XEXP, $YEXP);

my ($atX, $atY, @FontTable, $FontIdx, %afp2enc, %FillArea);

my (@MCF1, @MCF2, @CFI, @MDD, @LND, %MMO, %MMC, @PMC, @PGP1, @PGP2, @CCP, @FDX, @PGD, @PTD, @CPD);

my (@IID, @IDD, $lIPD, @IPD, @BDD, @BDA, @OBP, @OBD, @MIO, @MGO, $TLE);

my ($tGDA, @GDA, @GDD);

my (@IOC, %charsetMap, @ICP, @d3ac89);

my (%codefont, @FNC, @FND, @FNM, @FNI, %FNI, %FNO);

my ($currTextOr, $reqBitsCell, $readbits);

my ($extract_text, $text_as_comment, $text_is_ascii);

my ($OverlayName, $ResourceName, $PageDefName, $CharsetName, $fraster);

my ($CodedFontName, $FirstMediumMap, $FirstDataMap, $MediumMapName, $DataMapName);

my ($Duplexing, $DuplexingPages, %MediumMapsTable);

my ($FormDef, $PageDef, $bc128, $doc_ended, $PAGE_STARTED, $PAGE_ENDED, $UserExit, $FONTs);

#------------------------------------------------------------
# Utility Routines

sub trim {
  $_[0] =~ s/^ +//;
  $_[0] =~ s/ +$//;
  return $_[0];
}

sub rtrim {
  $_[0] =~ s/\s+$//;
  return $_[0];
}

sub ltrim {
  $_[0] =~ s/^\s+//;
  return $_[0];
}

sub QuotePsString {
  my $t = shift;  $t =~ s/\s/ /sg; $t =~ s/([(\)\{\}\r\n\f\x00-\x1f])/\\$1/sg; $t;
}


sub text_as_comment {
  # my $t = shift; $t = $main::translator->toascii($t) if !$text_is_ascii; $ps->add("\%".QuotePsString($t));
}

sub trEbcdicName {
  my $c = substr($_[0],0,2);
  return $_[0] if ($_ eq "00" || $c eq "ff");
  return rtrim($main::translator->toascii(pack("H*",$_[0])));
}

sub MakeFillCmds {
  my ($ImageOr_, @cmds_before, @cmds_after) = ("(0000) (2d00)");
  for $ImageOr (keys(%FillArea)) {
    my $aref = $FillArea{$ImageOr};
    if ( $ImageOr ne $ImageOr_ ) {
      push @cmds_before, "$ImageOr a.SetMatrix";
      $ImageOr_ = $ImageOr;
    }
    push @cmds_before, @$aref;
  }
  if ( $ImageOr ne "(0000) (2d00)" ) {
    push @cmds_before, "(0000) (2d00) a.SetMatrix";
  }
  return (\@cmds_before, \@cmds_after);
}

#------------------------------------------------------------
# Extract a text image of a page

sub MergeTextItems {
  my $self = shift; return if !$self->{TextItems};

  my ($TextItems, $TextLines) = @{$self}{'TextItems', 'TextLines'};

  #print TEXTITEMS "NEWPAGE NEWPAGE\n";

  for my $TextOr (keys(%$TextItems)) {
    my $TextItems_ = $TextItems->{$TextOr};
    my $TextLines_ = $TextLines->{$TextOr} = [];
    
    @$TextItems_ = sort {
      $a->[1] != $b->[1]
        ? $a->[1] <=> $b->[1]
        : $a->[0] <=> $b->[0]
      ;
    } 
    @$TextItems_;
        
    @$TextLines_ = (); my ($lowX, $atX, $atY) = (0,0,0); my $ltext = '';
    for my $aref (@$TextItems_) {
      #print TEXTITEMS join("\t", $ROTATE_CC{$TextOr}, @$aref), "\n" if $TextOr == 0;
      if ( $aref->[1] < $atY-3 or $aref->[1] > $atY+3  ) {
        push @$TextLines_, $ltext if $ltext ne "";
        $ltext = ''; $atX = 0;
      }
      $ltext .= " "x(($aref->[0]>$atX) ? ($aref->[0]-$atX)/15 : 0) . $aref->[3]; 
      $atX = $lowX+15*length($ltext); $atY = $aref->[1];
    }
    push @$TextLines_, $ltext if $ltext ne ""; 
  }
}

#------------------------------------------------------------
# Generate the PostScript Version of an Afp image

use constant IMG_BITMAP_MMR_IBM => "01";
use constant IMG_BITMAP_UNCOMPR => "03";
use constant IMG_BITMAP_MMR_G4 => "82";
use constant IMG_BITMAP_TIFFLZW => "0d";
use constant IMG_BITMAP_JPEG => "83";

use constant POSITION_AND_TRIM => 0x10;
use constant SCALE_TO_FIT => 0x20;
use constant CENTER_AND_TRIM => 0x30;
use constant SCALE_TO_FILL => 0x60;

use constant USE_DEFLATE => 1;

my %colorspaces = (
  0 => 'DeviceGray',
  1 => 'DeviceRGB',
  4 => 'DeviceCMYK',
);

sub convert_bands_tolzw {
  my $bands = shift; require Image::Magick;
  for my $iband (0..$#$bands) {
    my $band = $bands->[$iband];
    my $img = Image::Magick->new(magick=>'jpg');
    my $rc = $img->BlobToImage($band);
    $img->Set(magick => 'GRAY', compression => 'LZW'); #compression not managed for gray
    $bands->[$iband] = $img->ImageToBlob();
  }
}

sub GetPsDataSource {
  my $tile = shift; my @data_sources = (); my $filter;  my $decodeparms ='';
  my (
    $clw, $clh, $cmethod, $bands 
  ) =
  @{$tile}{qw(clw clh cmethod bands)};

  if ($cmethod eq IMG_BITMAP_UNCOMPR) {
          #print ">>>>> BITMAP_UNCOMPR\n";
  }
  elsif ($cmethod eq IMG_BITMAP_TIFFLZW) {
          #print ">>>>> BITMAP_TIFFLZW\n";
    $filter = "/LZWDecode filter";
  }
  elsif ($cmethod eq IMG_BITMAP_JPEG) {
          #print ">>>>> BITMAP_JPEG\n";
    #convert_bands_tolzw($bands);
    #$filter = "/LZWDecode filter";
    $filter = "/DCTDecode filter";
  }
  elsif ($cmethod eq IMG_BITMAP_MMR_IBM) {
          #print ">>>>> BITMAP_MMR_IBM\n";
    require XReport::FAX::MMR;
    #eval {
      $bands->[0] = ${XReport::FAX::MMR::decode_imageMMR(
        width => $clw, height => $clh, 
        cimage => $bands->[0]
      )};
    #};
  }
  elsif ($cmethod eq IMG_BITMAP_MMR_G4) {
          #print ">>>>> BITMAP_MMR_G4\n";
    $clw += ($clw%8 ? (8-$clw%8) : 0);
	$filter = "/CCITTFaxDecode filter"; $decodeparms = "<</K -1 /Columns $clw /Rows $clh /BlackIs1 true>>";
  }
  else {
    die "llllllllllllllllllllll $cmethod";
  }

  my $cmpr = 0; my $iband = 0;
  for my $band (@$bands) {
    $iband += 1;
    my $t = unpack("H*", ($cmpr ? compress($band) : $band));
    push @data_sources,  "[0\n". join("\n", map {" <$_>"} unpack("a128"x(POSIX::ceil(length($t)/128)), $t)) ."\n]"
  }

  my $bitmap = "[\n". join("\n", @data_sources) ."\n]"; my $data_source;
  for (0..(@data_sources-1)) {
    $data_source .= " {$_ shiftstring} $decodeparms $filter"
  }
  $data_source = substr($data_source,1);
  if (@data_sources == 1) {
    $data_source = " /DataSource $data_source";
  }
  else {
    $data_source = " /MultipleDataSources true\n /DataSource [$data_source]";
  }

  return ($bitmap, $data_source);
   
      " /DataSource <". unpack("H*", $bands->[0]) .">"
     ," /DataSource currentfile /ASCIIHexDecode filter"
     ," <<"
     #,"  /K -1 /Columns $imwl /Rows $imh /BlackIs1 false"
     ," >>"
     ," /CCITTFaxDecode filter"
     , "/DataSource currentfile /ASCIIHexDecode filter /DCTDecode filter"
     ,"/MultipleDataSources true"
     ,"/DataSource [s1 s2 s3 s4]"
}

sub PutPsImages {
  my ($obx, $oby, $obw, $obh, $usx, $usy, $usw, $ush, $angle, $scale, $images) = @_;

  my ($scalex, $scaley) = ($obw/$usw, $obh/$ush);

  $angle = 0 if $angle eq '';

  $ps->add(
    "\%PRINT IMAGE"
    ,"gsave "
    ,"(0000) (2d00) a.SetMatrix"
    ,"$obx $oby translate"
    ,"$angle rotate"  
  ); 
  $ps->add("$usx $usy translate") if $usx or $usy;

  $ps->add("0 0 $obw $obh rectclip");

  $ps->add("$scalex $scaley scale") if $scale; 

  my ($clipw, $cliph) = (($usw < $obw ? $usw : $obw), ($ush < $obh ? $ush : $obh));
  

  for my $image (@$images) {
    my $tiles = $image->{'tiles'};
    for my $tile (@$tiles) {
      my (
        $clx, $cly, $clw, $clh, $denx, $deny, $relres, $colorspace, $color, 
        $idesize, $comps, $bitsxcomp, $lutid
      ) = 
      @{$tile}{qw(clx cly clw clh denx deny relres colorspace color idesize comps bitsxcomp lutid)}; my ($XEXP, $YEXP) = (2400/$denx, 2400/$deny); 

      die("idesize value ($idesize) not valid\n") if !$idesize;

      $relres = 1 if !$relres;

      my ($tox, $toy, $tow, $toh) = ($clx*$XEXP, $cly*$YEXP, $clw*$XEXP*$relres, $clh*$YEXP*$relres);
  
      my ($imageList, $datasource) = GetPsDataSource($tile);
  
      my $ismask = $idesize == 1;
  
      my $decode = "0 1 " x $comps; chomp $decode; $decode = "1 0" if $ismask;
  
      $color = '0000' if $color eq ''; 
  
      my $cs = $colorspaces{$colorspace} or die("colorspace($colorspace) not mapped !!");
      
      $ps->add(
         "gsave "
        ,"12 dict begin"
        ,"/imageList $imageList def"
        ,"/$cs setcolorspace" 
        ,($colorspace == 0 ? "($color) (01) a.stc" : "")
        ,"$tox $toy moveto"
        ,"$tow $toh scale"
        ,"<<"
        ," /ImageType 1"
        ," /Width $clw"
        ," /Height $clh"
        ," /ImageMatrix [$clw 0 0 $clh 0 0]"
        ," /BitsPerComponent $bitsxcomp"
        ,($ismask == 1 ? " /Interpolate true" : "")
        ," /Decode [$decode]"
        ,$datasource
        ,">>"
        ,($ismask ? "imagemask" : "image")
        ,"end "
        ,"grestore "
      );
    }
  }

  $ps->add("grestore");
}

#------------------------------------------------------------
# Parse Triplets Groups

sub ParseTripletGroup {
  my ($self, $triplets, $tl, $triplet, $t, @triplets) = (shift, shift, 0, ());
  
  while ($triplets ne "") {
    my $tl = unpack("C", $triplets)-1; last if $tl<=0;
    my $triplet = substr($triplets,1,$tl);
    push @triplets, $triplet;
    #print $OUTPUT $tl, " ", unpack("H*", $triplet), "\n";
    $triplets = substr($triplets,1+$tl);
    $t = substr($triplet,0,1);
  }
  return @triplets;
}

sub ParseTripletGroups {
  my ($self, $triplets, $tl, $triplet, @triplets) = (shift, shift, 0, ());
  
  while ($triplets ne "") {
    $tl = unpack("n", $triplets)-2;
    last if $tl<=0;
    $triplet = substr($triplets,2,$tl);
    push @triplets, [$self->ParseTripletGroup($triplet)];
    $triplets = substr($triplets,2+$tl);
    #print $OUTPUT $tl, " ", unpack("H*", $triplet), "\n";
  }
  return @triplets;
}

#------------------------------------------------------------
# Parse PTOCA Text blocks

sub PutPtocaSeqs {
  my ($self, $cseqs) = (shift, shift); my $lFontIdx = -1; 
  
  my ($flg, $fc, $fl, $f, $fd, $fm, $toright) = (0, 0, 0, 0, 0, "", 0);
  
  while (length($cseqs)>0) {
    if (($f % 2) == 0) {
      ($flg, $fc, $fl, $f) = unpack("CCCC", $cseqs);
      $cseqs = substr($cseqs, 2);
    }
    else {
      ($fl, $f) = unpack("CC", $cseqs);
    }
    last if $fl<2;
    
    $fd = substr($cseqs,2,$fl-2); $fm = pack("C", $f - $f % 2);
    #print $OUTPUT unpack("H".$fl*2, $cseqs), "\n";
    
    if ( $toright > 0 ) {
      if ( $fm =~ /[\xc6\xd8]/ ) {
        $toright = 0;
      }
      elsif ( $fm !~ /[\xd2\xd4\xee\xd0\xf0]/ ) {
        $ps->add("ILNI $toright mul a.rmi"); $toright = 0;
      }
    }
    
    if ($fm eq "\xda") { # Transparent Data         
      if ( $trcFlag and $fileType eq "Document" ) {
        if ( (my $trcVal = unpack("C",$fd)+1) <= 16 ) {
          if (($FontIdx = $trcVal) != $lFontIdx) {
            $ps->add("F$FontIdx a.setfont"); $lFontIdx = $FontIdx;
          }
        }
        $fd = substr($fd,1); 
      }
      next if $fd eq ''; $fd =~ s/\x00/\x40/g; #$fd = $UserExit->CheckTrn($TLE, $fd) if $UserExit;
      
      text_as_comment($fd) if $text_as_comment; $ps->add("<".unpack("H*",$fd)."> a.trn");

      if ($extract_text and my $TextItems = $self->{TextItems} ) {
        push 
          @{$TextItems->{$currTextOr}}, 
          [ $atX, $atY, $FontTable[$FontIdx], $main::translator->toascii($fd), unpack("H*", $fd) ]
        ;
      }
      #print $OUTPUT "TRN $fdHex \n" if $fdHex ne "";
    }
    elsif ($fm eq "\xd2") { # Absolute Move Baseline
      $atY = unpack('s>', $fd)*$YEXP;
      $ps->add("$atY a.amb");
      #print $OUTPUT "AMB ", join("  ", unpack('s>', $fd)), "\n";
    }
    elsif ($fm eq "\xc6") { # Absolute Move Inline
      $atX = unpack('s>', $fd)*$XEXP;
      $ps->add("$atX a.ami");
      #print $OUTPUT "AMI ", join("  ", unpack("n", $fd)), "\n";
    }
    elsif ($fm eq "\xd8") { # Begin Line
      $atX = 0; $atY += 15; #todo: better atY increment
      $ps->add("a.bln"); 
      #print $OUTPUT "BLN ", "\n";
    }
    elsif ($fm eq "\xf2") { # Begin Suppression
      $ps->add("/".unpack("C", $fd)." a.bsu");
      #print $OUTPUT "BSU ", join("  ", unpack("H2", $fd)), "\n";
    }
    elsif ($fm eq "\xe6") { # Draw B-axis Rule DBR
      my @t = unpack("(s>)2c", $fd);
      if ($t[0] > 0) {
        $t[1] += $t[2] /= 256 ;
      }
      else {
        $t[1] -= $t[2] /= 256 ;
      }
      $#t = 1;
      $t[0]*=$YEXP; $t[1]*=$XEXP;
      $ps->add(join("  ", @t)." a.dbr");
      #print $OUTPUT "DBR  ", join("  ", @t), "\n";
    }
    elsif ($fm eq "\xe4") { # Draw I-axis Rule DIR
      my @t = unpack("(s>)2c", $fd);
      if ($t[0] > 0) {
        $t[1] += $t[2] /= 256 ;
      }
      else {
        $t[1] -= $t[2] /= 256 ;
      }
      $#t = 1;
      $t[0]*=$XEXP; $t[1]*=$YEXP;
      $ps->add(join("  ", @t)." a.dir");
      #print $OUTPUT "DIR  ", join("  ", @t), "\n";
    }
    elsif ($fm eq "\xf4") { # End Suppression
      $ps->add("/".unpack("C", $fd)." a.esu");
      #print $OUTPUT "ESU  ", join("  ", unpack("H2", $fd)), "\n";
    }
    elsif ($fm eq "\xf8") { # No Operation
      #print $OUTPUT "NOP  ", join("  ", unpack("H*", $fd)), "\n";
    }
    elsif ($fm eq "\x72") { # Overstrike
      $ps->add(">> a.ovs\n");
      #print $OUTPUT "OVS  ", join("  ", unpack("H2H4", $fd)), "\n";
    }
    elsif ($fm eq "\xd4") { # Relative Move Baseline
      my $off = unpack('s>', $fd)*$YEXP;
      $ps->add("$off a.rmb\n");
      $atY += $off;
      #print $OUTPUT "RMB $off\n";
    }
    elsif ($fm eq "\xc8") { # Relative Move Inline
      my $off = unpack('s>', $fd)*$XEXP;
      $ps->add("$off a.rmi");
      $atX += $off;
      #print $OUTPUT "RMI $off\n";
    }
    elsif ($fm eq "\xee") { # Repeat String 
      my ($ll, $t) = unpack("na*", $fd); $t = "\x40" if $t eq '';

      if ( length($t) == 1 ) {
        $t = $t x $ll;
      }
      else {
        while(length($t) < $ll) {
          $t .= $t; 
        }
        $t = substr($t,0,$ll);
      }

      if ( $t !~ /^\x40+$/ ) {
        if ( $toright > 0 ) {
          $ps->add("ILNI $toright mul a.rmi"); $toright = 0;
        }
        #$t = $UserExit->CheckTrn($TLE, $t) if $UserExit;

        text_as_comment($t) if $text_as_comment; $ps->add("<".unpack("H*", $t)."> a.trn");
      }
      else {
        $toright += length($t);
      }
      #print $OUTPUT "RPS  ", join("  ", unpack("nH*", $fd)), "\n";
    }
    elsif ($fm eq "\xd0") { # Set Baseline Increment
      my $bi = unpack('s>', $fd);
      $ps->add($bi*$YEXP." a.sbi");
      #print $OUTPUT "SBI $bi\n";
    }
    elsif ($fm eq "\xf0") { # Set Coded Font Local
      $FontIdx = unpack("C", $fd); $FontIdx = 1 if $FontIdx == 255;
      if ($FontIdx != $lFontIdx and @FontTable) {
        $ps->add("F$FontIdx a.setfont"); $lFontIdx = $FontIdx;
      }
      #print $OUTPUT "SCFL ", unpack("C", $fd), "\n";
    }
    elsif ($fm eq "\x80") { # Set Extended Text Color
      my @sec = unpack("H2H2H8C4C4", $fd);
      if ( $sec[1] eq '40' ) {
        my $color = unpack("H4", pack("c2", @sec[7,8]));
        $ps->add("($color) (01) a.stc");
      }
      elsif ( $sec[1] eq '01' ) {
        $ps->add(join(" ", (map {$_/255} @sec[7..9]), "setrgbcolor"));
      }
      elsif ( $sec[1] eq '04' ) {
        $ps->add(join(" ", (map {$_/255} @sec[7..10]), "setcmykcolor"));
      }
      else {
        die join(",", @sec), "\n";
     #   $ps->add("\%>> a.sec "), join(",", @sec), "\n";
      }
      #print $OUTPUT "SEC  ", join("  ", unpack("H2H2H8n4H8", $fd)), "\n";
    }
    elsif ($fm eq "\xc2") { # Set Intercharacter Adjustement
      $ps->add(">> a.sia");
      #print $OUTPUT "SIA  ", join("  ", unpack("nH2", $fd)), "\n";
    }
    elsif ($fm eq "\xc0") { # Set Inline Margin
      $ps->add((unpack("n", $fd)*$XEXP)." a.sim");
      #print $OUTPUT "SIM  ", join("  ", unpack("H2n", $fd)), "\n";
    }
    elsif ($fm eq "\x74") { # Set Text Color
      my @stc = unpack("H4H2", $fd);
      $ps->add("($stc[0]) ($stc[1]) a.stc");
      #print $OUTPUT "STC  ", join("  ", @stc), "\n";
    }
    elsif ($fm eq "\xf6") { # Set Text Orientation
      my @sto = unpack("H4H4", $fd);
      $currTextOr = "($sto[0]) ($sto[1])";
      $ps->add("$currTextOr a.sto");
      #print $OUTPUT "STO  ", join("  ", @sto), "\n";
    }
    elsif ($fm eq "\xc4") { # Set Variable Space Character Increment
      my $svi = unpack("s>", $fd); 
      $ps->add("%SVI $svi");
      #$svi *= 0.16666666 * 4 if $svi != -1;
      $svi *= $XEXP if $svi != -1;
      $ps->add("$svi a.svi"); # 72/1440 (1 twip) /0.3 scale factor * 4 ?????
      #print $OUTPUT "SVI  ", join("  ", unpack("n", $fd)), "\n";
    }
    elsif ($fm eq "\x78") { # Temporary Baseline Movement
      $ps->add(">> a.tbm");
      #print $OUTPUT "TBM  ", join("  ", unpack("H2H2n", $fd)), "\n";
    }
    elsif ($fm eq "\x76") { # Underscore
      $ps->add(unpack("C", $fd)." a.usc");
      #print $OUTPUT "USC  ", join("  ", unpack("H2", $fd)), "\n";
    }
    else {
      $ps->add("\%>> a.??? ".unpack("H*", $fm.$fd));
      #print $OUTPUT "???  ", join("  ", unpack("H2", $fd)), "\n";
    }
  }
  continue {
    $cseqs = substr($cseqs, $fl);
  }
}

#------------------------------------------------------------
# Parse AFP structured fields


sub ExpandAfp {
  my $self = shift; my $cref = $self->can($r5atype);
  return &$cref($self, $r5adata) if $cref;
#  print "$r5atype handler not found, trying fonts\n";
#  $cref = $FONTs->can($r5atype);
#  return &$cref($FONTs) if $cref;
#  print "$r5atype handler not found in fonts - quit\n";
  return undef;
}

sub d3a8df { my $self = shift;  my $trd = $_[0]; # Begin Overlay
  $OverlayName = $ResourceName || uc(File::Basename::basename($FileName));
  $fileType="Overlay";
  $currTextOr = "(0000) (2d00)"; %FillArea = (); 
};

sub d3a9df { my $self = shift;  my $trd = $_[0]; # End Overlay
#  $ps->add(
#    ""
#   ,"}"
#   ,"ifelse"
#   ,""
#   ,"pop"
#  ) if 0;
  #begin insert FillArea commands
  if (scalar(keys(%FillArea))) {
    my ($cmds_before, $cmds_after) = MakeFillCmds();
    @$cmds_before and $ps->insert(
     qr/BEGIN INSERT FillArea COMMANDS/i, $cmds_before
    );
    @$cmds_after and $ps->add(@$cmds_after);
  }
  #end insert FillArea commands
  $OverlayName = '';
  $fileType=""; 
  @MCF1 = (); @MCF2 = (); $currTextOr=''; %FillArea = ();
};

sub d3a88a { my $self = shift;  my $trd = $_[0]; # Begin Coded Font (BCF)
  my $fn = $FileName;
  $fn = $self->{FileName} unless $fn;
  $CodedFontName = File::Basename::basename($FileName);
  $fileType="CodedFont";
};

sub d3a98a { my $self = shift;  my $trd = $_[0]; # End Coded Font (ECF)
  $ps->add( 
    "<<"
   ," /CodedFontName /$CodedFontName"
   ," /CharsetName   /$CFI[0]"
   ," /CodepageName  /$CFI[1]"
   ," /VSize  $CFI[2]"
   ," /HSize  $CFI[2]"
   ,">>"
  );
  $CodedFontName = "";
  $fileType=""; @CFI = ();
};

sub d3a688 { my $self = shift;  my $trd = $_[0]; # Medium Descriptor
 @MDD = unpack("H2H2(S>)2a3a3H2",$trd);
 $MDD[4] = ( $MDD[4] eq "\x00\x00\x00" ? undef 
           : $MDD[4] eq "\xff\xff\xff" ? 0
           : unpack('xs>', $MDD[4]));
 $MDD[5] = ( $MDD[5] eq "\x00\x00\x00" ? undef 
           : $MDD[5] eq "\xff\xff\xff" ? 0
           : unpack('xs>', $MDD[5]));
# @MDD = myunpackeb("H2H2S2M3M3H2",$trd);
  $MDD[7] = length($trd) > 13 ? [$self->ParseTripletGroup(substr($trd,13))] : [];
  #print $OUTPUT join("  ", @MDD), "\n";
};

sub d3a8cb { my $self = shift;  my $trd = $_[0]; # Begin Page Map

  my $FN = ( $FileName ? $FileName : exists($self->{FileName}) ? $self->{FileName} : undef );  
  $PdefWriteMode = 1;
#  $PageDefName = File::Basename::basename($FileName);
#  if ( 1 or -s $FileName <= 100000 ) {
#    $PdefWriteMode = 1;
#  }
#  else {
#    $PageDefDir = dirname($FileName)."/$PageDefName\.dm";
#    if ( !-e $PageDefDir ) {
#      mkdir $PageDefDir;
#    }
#    for (glob("$PageDefDir/*.ps")) {
#      unlink $_;
#    }
#    $PdefWriteMode = 2;
#  }
  $fileType="PageDef";
  $ps->add("/PdefWriteMode $PdefWriteMode def");
};

sub d3a9cb { my $self = shift;  my $trd = $_[0]; # d3a9cb End Page Map
  $PdefWriteMode = ""; $PageDefDir = ""; $FirstDataMap = "";
  $PageDefName = ""; $fileType="";
  @MCF1 = (); @MCF2 = ();
};

sub d3a8ca { my $self = shift;  my $trd = $_[0]; # Begin Data Map
  @PGD = (); @FDX = (); @CCP=(); @MCF1=(); @MCF2 = (); @LND = ();
  $DataMapName = rtrim(
    $main::translator->toascii(substr($trd,0,8))
  );
  #print $OUTPUT $main::translator->toascii($trd), "\n" if $trd ne "";
};

sub d3a8cc { my $self = shift;  my $trd = $_[0]; # Begin Medium Map
  %MMO = (); %MMC = (); @PMC = (); @PGP1 = (); @PGP2 = (); 
  $MediumMapName = rtrim(
    $main::translator->toascii(substr($trd,0,8))
  );
  #$self->ParseTripletGroup(substr($trd,8)) if length($trd) > 8;
  #print $OUTPUT $main::translator->toascii(substr($trd,0,8)), "\n" if $trd ne "";
};

sub d3b1df { my $self = shift;  my $trd = $_[0]; # Map Medium Overlay
  my ($r, $lr) = (substr($trd,4), unpack("C",$trd));
  while($r ne "") {
    my ($lid, $OverlayName) = unpack("Cx3a8", $r);
    $OverlayName = rtrim($main::translator->toascii($OverlayName));
    $USED_RESOURCES->{$OverlayName} = 1 if ($USED_RESOURCES);
    $MMO{$lid} = $OverlayName;
    $r = substr($r,$lr);
  }
  #print $OUTPUT $main::translator->toascii($trd), "\n", join(",",%MMO), "\n";
};

sub d3a788 { my $self = shift;  my $trd = $_[0]; # Medium Modification Control
  my ($mcsid, $mcs) = unpack("Cxa*", $trd); 
  my @mcs;
  while ($mcs) {
    push @mcs, [unpack("aa", $mcs)];
    $mcs = substr($mcs,2);
  }
  $MMC{$mcsid} = [@mcs];
  #print $OUTPUT $mcsid, " ", join(",", @mcs), "\n";
};

sub d3a7af { my $self = shift;  my $trd = $_[0]; # Medium Descriptor

  push @PMC, [$self->ParseTripletGroup(substr($trd,2))];
};

sub d3a9cc { my $self = shift;  my $trd = $_[0]; # End Medium Map
  if ( $FirstMediumMap eq "" ) {
    $FirstMediumMap = rtrim($MediumMapName);
    $ps->add("/FirstMediumMap ($FirstMediumMap) def\n");
  }
  
  my ($Duplexing, $SuppressIds, $Overlays, $Overlays_front, $Overlays_rear) = ();
  
  if ( exists($MMC{1}) ) {
    my ($duplexc) = grep({$_->[0] eq "\xf4"} @{$MMC{1}});
    $Duplexing = $duplexc->[1] ne "\x01";
    if ( !$Duplexing ) {
      my @mcs = grep({ $_->[0] eq "\xf2"} @{$MMC{1}});
      for (@mcs) {
        my $lid = unpack("C", $_->[1]);
        $Overlays .= " [ 0 0 ($MMO{$lid}) ]";
      }
    }
    else {
      my @mcs = grep({$_->[0] eq "\xf2"} @{$MMC{1}});
      for (@mcs) {
        my $lid = unpack("C", $_->[1]);
        $Overlays_front .= " [ 0 0 ($MMO{$lid}) ]";
      }
      @mcs = grep({$_->[0] eq "\xf2"} @{$MMC{2}});
      for (@mcs) {
        my $lid = unpack("C", $_->[1]);
        $Overlays_rear .= " [ 0 0 ($MMO{$lid}) ]";
      }
    }

    my @mcs = grep({$_->[0] eq "\xf3"} @{$MMC{1}});
    for (@mcs) {
      my $id = unpack("C",$_->[1]); $SuppressIds .= " /$id $id";
    }
    $SuppressIds &&= substr($SuppressIds,1);
  }
  
  for my $side (0..$#PMC) {
    my $PMC = $PMC[$side];
    for (@$PMC) {
      my ($triplet, $t) = ($_,substr($_,0,1));
      if ( $t eq "\x6c" ) {
#        my @t = myunpackeb("H2H2e8M3M3", "$triplet");
        my @t = unpack("H2H2a8xS>xS>", "$triplet");
        $t[2] = rtrim($main::translator->toascii($t[2]));
        $Overlays .= " [ $t[3] $t[4] ($t[2]) ]";
      }
    }
    next if !$Duplexing;
    $Overlays_front .= $Overlays if $side eq 0;
    $Overlays_rear .= $Overlays if $side eq 1; $Overlays = '';
  }
  $Overlays_front = substr($Overlays_front,1) if $Overlays_front ne "";
  $Overlays_rear = substr($Overlays_rear,1) if $Overlays_rear ne "";
  $Overlays = substr($Overlays,1) if $Overlays ne "";
  
  my ($MedOrient) = grep(/^\x68/, @{$MDD[7]});
  if ( $MedOrient ne "" ) {
    $MedOrient = unpack("xH2", $MedOrient);
  }
  else {
    $MedOrient = '00';
  }
  $ps->add(
    "/MediumMap.$MediumMapName",
   ,"<<"
   ,"/SuppressIds <<$SuppressIds>> /Duplexing ". (($Duplexing) ? "true" : "false")
  );
  if (!$Duplexing) {
    $ps->add(
      "/Overlays [ $Overlays ]"
    );
  }
  else {
    $ps->add(
      "/Overlays_front [ $Overlays_front ]"
     ,"/Overlays_rear [ $Overlays_rear ]"
    );
  }
  if ( @PGP2 ) {
    my $aref = $PGP2[0]; 
    my (
      $plx, $ply, $ppx, $ppy, $po
    ) 
    = (@MDD[4,5], @$aref[1,2,3]);
    if ( $po eq '5a00' and $plx == 0 ) {
      ($plx, $ply, $ppx, $ppy, $po) = ($ppx, $ppy, 0, 0, '0000'); 
    }
    $ps->add(
      "/FormSize [$plx $ply]"
     ,"/PagePosition [$ppx $ppy]"
     ,"/PageOrientation ($po)"
     ,">>"
     ,"def"
    );
  }
  else {
    $ps->add(
      "/FormSize [".join(" ",@MDD[4,5]). "]"
     ,"/PagePosition [". join(" ",@PGP1). "]"
     ,"/PageOrientation ($MedOrient)"
     ,">>"
     ,"def"
    );
  }
  $MediumMapsTable{$MediumMapName} = {Duplexing => $Duplexing};
  %MMO = (); %MMC = (); @PMC = (); @PGP1 = (); @PGP2 = (); 
};

sub d3a9cd { my $self = shift;  my $trd = $_[0]; # End Form Map
  @MDD=(); $FirstMediumMap = "";
};

sub d3acaf { my $self = shift;  my $trd = $_[0]; # Page Position Format-1
  @PGP1 = unpack("xs>xs>",$trd);
  #print $OUTPUT join("  ", @PGP1), "\n";
};

sub d3b1af { my $self = shift;  my $trd = $_[0]; # Page Position (Format 2)
  my ($r, $lr) = (substr($trd,1), 0); @PGP2 = ();
  while($r ne "") {
    $lr = unpack("C", $r); my $t = substr($r,0,$lr);
    my @t = unpack("H2xs>xs>H4H2H2H2",$t);
    for (0,3,4,5,6) {
      $t[$_] =~ s/\x00$//;
    }
    $r = substr($r,$lr);
    push @PGP2, [@t] if $t[4] lt "10";
    #print $OUTPUT join("  ", @t), "\n";
  }
};

sub d3a7ca { my $self = shift;  my $trd = $_[0]; # Conditional Processing Control (CCP)
  my @ccp =unpack("n2H2H2n3",$trd);
  #print $OUTPUT join("  ", @ccp), "\n";
  my ($nr, $lr, $r) = (@ccp[4,5], substr($trd,12));
  for (my $ir=0; $ir<$nr; $ir++) {
    my $t = substr($r,0,$lr);
    my @t = unpack("CCA8CA8CA*",$t);
    $t[2] = rtrim($main::translator->toascii($t[2]));
    $t[4] = rtrim($main::translator->toascii($t[4]));
    $r = substr($r,$lr);
    push @ccp, [@t];
    #print $OUTPUT join("  ", @t), "\n";
  }
  push @CCP, [@ccp];
};

sub d3a8cd { my $self = shift;  my $trd = $_[0]; # Begin Form Map
  @MDD=(); $FirstMediumMap = "";
};

sub d3aae7 { my $self = shift;  my $trd = $_[0]; # Line Descriptor Count (LNC)
};

sub d3a6e7 { my $self = shift;  my $trd = $_[0]; # Line Descriptor (LND)

  my @t = unpack("B16n2H8CCn3H16H2NnH4nCn",$trd);
  if ( length($trd) > 40 ) {
    my $t = substr($trd,40);
    push @t, [$self->ParseTripletGroup($t)];
  }
  else {
    push @t, ();
  }
  if ( !$t[13] ) {
    @t[13..16] = ( 'ff07',  0,  1,  '0000');
  }
  push @LND, [@t];
  #print $OUTPUT "[", $#LND+2, "] ", join("  ", @t), "\n";
};

#  [0011001000000000  0  0  00002d00  -1  01  2  2  74  0000000000000000  00  0  0  ff07  101  01  0000]
sub d3eeec { my $self = shift;  my $trd = $_[0]; # Fixed Data Text (FDX)
  push @FDX, unpack("H*",$trd);
};

sub d3a9ca { my $self = shift;  my $trd = $_[0]; # End Data Map (EDM)
  my ($tMCF1, $tMCF2, $tLND, $iLND, @bits, $t, $tTrp, $Fonts, $Overlays) = ();

  for $tMCF1 (@MCF1) {
    $Fonts .= "/F$tMCF1->[0] /$tMCF1->[4] 1 1 a.fdef\n";
    $USED_RESOURCES->{$tMCF1->[4]} = 1;
  }
  for $tMCF2 (@MCF2) {
    my $verts = $tMCF2->[4] || 0; my $horizs = $tMCF2->[5] || $verts;
    if ( $tMCF2->[1] ) {
      $Fonts .= "/F$tMCF2->[0] /$tMCF2->[1] $verts $horizs a.fdef\n";
      $USED_RESOURCES->{$tMCF2->[1]} = 1;
    }
    else {
      $Fonts .=
       "/F$tMCF2->[0] /$tMCF2->[2].$tMCF2->[3] $verts $horizs a.fdef\n";
       $USED_RESOURCES->{$tMCF2->[2]} = 1;
       $USED_RESOURCES->{$tMCF2->[3]} = 1;
    }
  }

  if ( $FirstDataMap eq "" ) {
    $ps->add("/FirstDataMap ($DataMapName) def");
    if ( scalar(@CCP) ) {
      $ps->add(
       "/CCPTable",
       "[[0]"
      );
      #print $OUTPUT Dumper(@CCP), "\n";
      for my $ccp (@CCP) {
        $ps->add(
         "[$ccp->[0] ".join(" ", @{$ccp}[1..6]), " [",
         (map{
            my @t = @{$_};
            " [$t[0] $t[1] ($t[2]) $t[3] ($t[4]) (".$ccpTest{$t[5]}.") cvx ".
            "<".unpack("H*",$t[6]).">] \%".$main::translator->toascii($t[6])
          } 
          @{$ccp}[7..(scalar(@$ccp)-1)]),
         "]]"
        );
      }
      $ps->add(
       "]","def"
      );
      @CCP = ();
    }
    $FirstDataMap = $DataMapName;
  }

  $ps->opendm($DataMapName);

  $ps->add(
   "/DataMap.$DataMapName",
   "<<",
   "/PageSize [", join(" ", @PGD[2..5]), "]",
   "/FixedText <". join('',@FDX). ">",
   "/TabFont {\n". $Fonts. "}",
   "/TabLND",
   "<<",
   " [0 0 0 0000 [0 1] [0 1]]"
  );
  $iLND = 0;
  for $tLND (@LND) {
    @bits = unpack("aaaaaaaaaaaaaaaa", $tLND->[0]); 
    $iLND++;
    $t = "$iLND ";
    $t .= join(" ", $tLND->[5], $tLND->[15], $tLND->[16]); #identifiers
    $t .= join(" ", " [", $bits[0], $tLND->[6], "]"); #skipping
    $t .= join(" ", " [", $bits[1], $tLND->[7], "]"); #spacing
    $t .= join(" ", " [", $bits[2], $tLND->[1], "]"); #set inline
    $t .= join(" ", " [", $bits[3], $tLND->[2], "]"); #set baseline
    $t .= join(" ", " [", $bits[4], "(F$tLND->[4])", "]"); #set font
    $t .= join(" ", " [", $bits[10], "($tLND->[13])", "]"); #set color
    $t .= join(" ", " [", $bits[5], "($tLND->[9])", "]"); #suppression
    $t .= join(" ", " [", $bits[6], $tLND->[8], "]"); #reuse record
    $t .= join(" ", " [", $bits[7], $tLND->[11], $tLND->[12], "($tLND->[3])", "]"); #text def
    $t .= join(" ", " [", $bits[11], $tLND->[14], "]"); #CCP def
    $Overlays = "";
    if ($bits[12] eq '1' ) {
      $tTrp = $tLND->[17];
      for (@$tTrp) {
        my ($triplet, $t) = ($_,substr($_,0,1));
        if ( $t eq "\x6c" ) {
#          my @t = myunpackeb("H2H2e8M3M3", $triplet);
#          $t[2] = rtrim($t[2]);
          my @t = unpack("H2H2a8xS>xS>", "$triplet");
          $t[2] = rtrim($main::translator->toascii($t[2]));
          $Overlays .= "[ $t[3] $t[4] ($t[2]) ]";
        }
      }
    }
    $t .= join(" ", "[", $bits[12], "[", $Overlays, "] ]"); #Overlays
    $ps->add(" [$t ]");
  }
  $ps->add("]",">>","def");

  $ps->closedm();

  @PGD=(); @FDX = (); @CCP=(); @MCF1=(); @MCF2 = (); @LND = ();
};

sub d3a87b { my $self = shift;  my $trd = $_[0]; # Begin IM Image (C)
  @IOC = (); @IID = (); @ICP = (); $strImg = ""; 
  $strImgCell = ''; @strImgCell = (); $readbits = 0;
  #print $OUTPUT $main::translator->toascii($trd), "\n" if $trd ne "";
};

sub d3a67b { my $self = shift;  my $trd = $_[0]; # IM Image Input Descriptor (C)
  @IID = unpack("H24H2H2n4H12n2H4H4",$trd);
  
  if ( (POSIX::ceil($IID[5]/8) * $IID[6]) < 10000000 ) {
    $strImg = "\x00" x (POSIX::ceil($IID[5]/8) * $IID[6]);
  }
  else {
    $strImg = "\x01";
  }
  $readbits = 0;  @strImgCell = ();
  #print $OUTPUT join("  ", @IID), "\n";
};

sub d3a77b { my $self = shift;  my $trd = $_[0]; # IM Image Output Control (IOC)
  @IOC = unpack("xnxnH4H4H16H4H4H4",$trd);
  #print $OUTPUT join("  ", @IOC), "\n";
};

sub d3ac7b { my $self = shift;  my $trd = $_[0]; # IM Image Cell Position (C)
  @ICP = unpack("n6",$trd);
  $ICP[4] = $IID[5] if $ICP[4] == 65535;
  $ICP[5] = $IID[6] if $ICP[5] == 65535;
  $reqBitsCell = $ICP[2] * $ICP[3]; $strImgCell = ''; $readbits = 0; 
};

sub d3ee7b { my $self = shift;  my $trd = $_[0]; # IM Image Raster Data (C)
  return if $strImg eq "\x01";

  if ( !@ICP ) {
    if ( $readbits == 0 ) {
      $strImg = '';
    }
    $strImg .= $trd; $readbits += length($trd)*8; 
  }

  else {
    $strImgCell .= $trd; $readbits += length($trd)*8; 

    if ( $#ICP >= 5 and ($ICP[2] != $ICP[4] and $ICP[3] == 8) or ($ICP[3] != $ICP[5] and $ICP[2] == 8) ) {
     
      my ($imx, $imy, $imw, $imh, $imd, $gray, $color, $x_den, $y_den) 
        = 
      (@ICP[0..1], @ICP[4..5], unpack("B*",$strImgCell), undef, undef, @IID[3,4]);

      $imd =~ s/0//g; $gray = 1 - length($imd)/($ICP[2]*$ICP[3]);
      $imx += $IOC[0]; $imy += $IOC[1];
      $color = $IID[11];

      ($imx, $imw) = ($imx * 2400/$x_den, $imw * 2400/$x_den) if $x_den;
      ($imy, $imh) = ($imy * 2400/$y_den, $imh * 2400/$y_den) if $y_den;

      if ($fileType eq "Document" or $fileType eq "Overlay") {
        my $ImageOr = "($IOC[2]) ($IOC[3])";
        $FillArea{$ImageOr} = [] if !exists($FillArea{$ImageOr});
        push @{$FillArea{$ImageOr}}, "$imx $imy $imw $imh $gray ($color) a.FillArea";
      }
      else {
        $ps->add("$imx $imy $imw $imh $gray ($color) a.FillArea");
      }
    } 

    elsif ( scalar(@ICP) > 5 && $readbits >= $reqBitsCell ) {
      push @strImgCell, [[@ICP], $strImgCell];
      
      @ICP = (); $reqBitsCell = 0; $strImgCell = ""; $readbits = 0;
    }
  }

  #print $OUTPUT "bits=$readbits\n";
};

sub FillImgFromCells {
  my ($imw, $imh) = (0, 0); require Bit::Vector; # @IID[5,6] is misleading

  for my $cell (@strImgCell) {
    my ($ICP, $strImgCell) = @$cell; 

    my (
      $Xfm, $Yfm, $Xcell, $Ycell, $Xext, $Yext
    ) = @{$ICP}[0,1,2,3,4,5]; 
    
    my $Xto = $Xfm + $Xext; $imw = $Xto if $Xto > $imw;
    my $Yto = $Yfm + $Yext; $imh = $Yto if $Yto > $imh;
  } 

  @IID[5,6] = ($imw, $imh);

  #my $bitsImg = Bit::Vector->new($imw*$imh);
  my $bitsImg;
  for my $cell (@strImgCell) {
    my ($ICP, $strImgCell) = @$cell; 

    my (
      $Xfm, $Yfm, $Xcell, $Ycell, $Xext, $Yext
    ) = 
    @{$ICP}[0,1,2,3,4,5]; my ($Xexp, $Yexp) = (POSIX::ceil($Xext/$Xcell), POSIX::ceil($Yext/$Ycell));
  
    if ( $Xexp > 1 or $Yexp > 1 ) {
      my $bitsImgCell = unpack("B*", $strImgCell);
      if ($Xexp > 1) {
        my $bits = $bitsImgCell; $bitsImgCell = '';
        for (0..POSIX::ceil($Ycell-1)) {
          $bitsImgCell .= substr($bits, $_*$Xcell, $Xcell) x $Xexp; 
        }
      }
      $bitsImgCell = substr($bitsImgCell, 0, $Xext);
      $bitsImgCell = $bitsImgCell x POSIX::ceil($Yexp) if $Yexp > 1;  
      $strImgCell = pack("B*", substr($bitsImgCell, 0, $Xext*$Yext)); 
    }

    #my $bitsImgCell = Bit::Vector->new_Hex($Xext*$Yext, unpack("H*", reverse($strImgCell)));

    #for (0..($Yext-1)) {
    #  $bitsImg->Interval_Copy($bitsImgCell, ($Yfm+$_)*$imw + $Xfm, $_*$Xext, $Xext); 
    #}
	if (($imw - $Xfm) < $Xcell) {
		$Xfm = $imw - $Xcell;
	}
	
	$bitsImg = pack('B*', ('0' x ($imw * $Yfm)) . join ('', map{("0" x $Xfm) . $_} (unpack("(B$Xcell)*", $strImgCell))));
  }
  
  #return ($imw, $imh, $bitsImg->Block_Read());
  return ($imw, $imh, $bitsImg);
}

sub d3a97b { my $self = shift;  my $trd = $_[0]; # End IM Image (C)
 return if $strImg eq "\x00" && !@strImgCell || $strImg eq "\x01";

  my ($obx, $oby, $obw, $obh) = (@IOC[0,1], @IID[5,6]);  
  my ($imw, $imh, $color) = @IID[5,6,11]; 
  my $angle = $ROTATE_Angle{"($IOC[2]) ($IOC[3])"};

  ($imw, $imh, $strImg) = FillImgFromCells() if @strImgCell; $obw = $imw; $obh = $imh;

  if ( ($imw % 8) != 0 ) { 
    my ($bits, $lbits) = (unpack("B*", $strImg), $imw + (8 - ($imw%8)));
      
    for ($strImg='', my $j=0; $j<$imh; $j++) {
      $strImg .= pack("B".$lbits, substr($bits, $j*$imw, $imw)."00000000");
    }
  }
  my ($denx, $deny) = ($IID[3] || 2400, $IID[4] || 2400);

  $obw *= 2400/$denx; $obh *= 2400/$deny;

  PutPsImages(
    $obx, $oby, $obw, $obh, 
    0, 0, $imw, $imh, $angle, 1, [{
      tiles => [{
        clx => 0, cly => 0, clw => $imw => clh => $imh, denx => $IID[3] || 2400, deny => $IID[4] || 2400, cmethod => "03", 
        colorspace => 0, color => $color, idesize => 1, comps => 1, bitsxcomp => 1, bands =>[$strImg]
      }],
    }]
  );

  @IOC = (); @IID = (); @ICP = (); $strImg = ""; 
  $strImgCell = ''; @strImgCell = (); $readbits = 0;
};

sub d3eeee { my $self = shift;  my $trd = $_[0]; # No Operation
 my $tnop = $main::translator->toascii($trd); $tnop =~ s/\s+$//;
  
  if ( $tnop =~ /XREPORTSTAT/i && (my $jr = $self->{jr}) ) {
    my ( $UserPages, $UserDocs) 
     = 
    $tnop =~ /XREPORTSTAT +(?:IN:|PAGES=) *0*(\d+), *(?:OUT:|DOCS=) *0*(\d+)/;
    
    $jr->setValues(
      UserPages => $UserPages + 0, UserDocs => $UserDocs + 0
    );
  }
  #print $OUTPUT $main::translator->toascii($trd), "\n" if $trd ne "";
};

sub d3a6af { my $self = shift;  my $trd = $_[0]; # Page Descriptor

  @PGD = unpack("H2H2(S>)2x(S>)x(S>)H6H6H*",$trd);
  
  $PGD[4] *= 2400/$PGD[2] if $PGD[2]; $PGD[5]*=2400/$PGD[3] if $PGD[3];
  
  $self->ParseTripletGroup(substr($trd,15)) if length($trd) >= 15;
  #print $OUTPUT join("  ", @PGD), "\n";
};

sub d3a8c9 { my $self = shift;  my $trd = $_[0]; # Begin Active Environment Group (14)
};

sub d3a9c9 { my $self = shift;  my $trd = $_[0]; # End Active Environment Group (14)
  #REQUIRED RESOURCES
  my ( $tMCF1, $tMCF2); @FontTable = ();
  if ( $fileType eq "Overlay" ) {
#    $ps->add(
#      "dup type /dicttype ne"
#     ,"{"
#     ,"/Form.$OverlayName"
#     ,"<<"
#     ," /FormType 1"
#    # the limiting factor is the including page
#    # as of afp manuals at PGD semantics.
#    #," /BBox [0 0 $PGD[4] $PGD[5]]" 
#     ," /BBox [0 0 32767 32767]"
#     ," /Matrix [1 0 0 1 0 0]"
#     ," /PaintProc {(OverLib/) ($OverlayName) a.execres}"
#     ,">>"
#     ,"def"
#     ,"}"
#     ,"{"
#     ,""
#    ) if 0;
    for $tMCF1 (@MCF1) {
      $FontTable[$tMCF1->[0]] = $tMCF1->[4];
      $ps->add("/F$tMCF1->[0] /$tMCF1->[4] 1 1 a.fdef");
      $USED_RESOURCES->{$tMCF1->[4]} = 1;
    }
    for $tMCF2 (@MCF2) {
      my $verts = $tMCF2->[4] || 0; my $horizs = $tMCF2->[5] || $verts;
      if ( $tMCF2->[1] ) {
        $FontTable[$tMCF2->[0]] = $tMCF2->[1];
        $ps->add("/F$tMCF2->[0] /$tMCF2->[1] $verts $horizs a.fdef");
        $USED_RESOURCES->{$tMCF2->[1]} = 1;
      }
      else {
        $FontTable[$tMCF2->[0]] = "$tMCF2->[2]/$tMCF2->[3]";
        $ps->add("/F$tMCF2->[0] /$tMCF2->[2].$tMCF2->[3] $verts $horizs a.fdef");
        $USED_RESOURCES->{$tMCF2->[2]} = 1;
        $USED_RESOURCES->{$tMCF2->[3]} = 1;
      }
    }
    $ps->add("");
    $ps->add(
      "0 0 moveto"
     ,""    
     ,"$PGD[4] $PGD[5] a.DefMatrix"
     ,"(0000) (2d00) a.SetMatrix"
     ,"F1 a.setfont"
     ,"\%BEGIN INSERT FillArea COMMANDS"
     ,"\%END INSERT FillArea COMMANDS"
     ,""
    );
  }
  elsif ( $fileType eq "Document" ) {
    $ps->add("{"); @FontTable = ();
    for $tMCF1 (@MCF1) {
      $FontTable[$tMCF1->[0]] = $tMCF1->[4];
      $ps->add(" /F$tMCF1->[0] /$tMCF1->[4] 1 1 a.fdef");
      $USED_RESOURCES->{$tMCF1->[4]} = 1;
    }
    for $tMCF2 (@MCF2) {
      my $verts = $tMCF2->[4] || 0; my $horizs = $tMCF2->[5] || $verts;
      if ( $tMCF2->[1] ) {
        $FontTable[$tMCF2->[0]] = $tMCF2->[1];
        $ps->add("/F$tMCF2->[0] /$tMCF2->[1] $verts $horizs a.fdef");
        $USED_RESOURCES->{$tMCF2->[1]} = 1;
      }
      else {
        $FontTable[$tMCF2->[0]] = "$tMCF2->[2]/$tMCF2->[3]";
        $ps->add("/F$tMCF2->[0] /$tMCF2->[2].$tMCF2->[3] $verts $horizs a.fdef");
        $USED_RESOURCES->{$tMCF2->[2]} = 1;
        $USED_RESOURCES->{$tMCF2->[3]} = 1;
      }
    }
    $ps->add(
     "}"
     ,"$PGD[4] $PGD[5] a.BeginPage", 
     ,""
     ,"(0000) (2d00) a.SetMatrix"
     ,"F1 a.setfont"
     ,"\%BEGIN INSERT FillArea COMMANDS"
     ,"\%END INSERT FillArea COMMANDS"
     ,""
    ); 
  }
  ($atX, $atY, $FontIdx) = (0, 0, 1);
};

sub d3a69b { my $self = shift;  my $trd = $_[0]; # Presentation Text Descriptor (Format 1)
  @PTD = unpack("H2H2n4H4",$trd);
  #print $OUTPUT join("  ", @PTD), "\n";
  ($XEXP, $YEXP) = (2400/$PTD[2], 2400/$PTD[3]);
};

sub d3b19b { my $self = shift;  my $trd = $_[0]; # Presentation Text Descriptor (PTD)
  @PTD = unpack("H2H2nnxnxnH4H*", $trd);
  ($XEXP, $YEXP) = (2400/$PTD[2], 2400/$PTD[2]);
  #print $OUTPUT join("  ", @PTD), "\n";
};

sub d3abd8 { my $self = shift;  my $trd = $_[0]; # Map Page Overlay
  return if !$USED_RESOURCES;
  my @tgs = $self->ParseTripletGroups($trd);
  for my $tg (@tgs) {
    for my $t (@$tg) {
      if ( $t =~ /^\x02/ ) {
        my $OverlayName = unpack("x3a*", $t);
        $OverlayName = rtrim($main::translator->toascii($OverlayName));
        $USED_RESOURCES->{$OverlayName} = 1;
      }
    }
  }
};

sub d3b15f { my $self = shift;  my $trd = $_[0]; # Map Page Segment
  return if !$USED_RESOURCES;
  my $r = substr($trd,4);
  while($r ne "") {
    my $PsegName = unpack("x4a8", $r);
    $PsegName = rtrim($main::translator->toascii($PsegName));
    $USED_RESOURCES->{$PsegName} = 1;
    $r = substr($r,12);
  }
};

sub d3b18a { my $self = shift;  my $trd = $_[0]; # Map Coded Font (MCF-1) Format 1

  my $str = $trd;
  $str = $_[0] if (!$str && $_[0]);
  my ($tl, $ts, @t) = unpack("Cx3a*",$str);
  #print $OUTPUT $t[0], "\n";
  while (length($ts) > 0 && $tl>0) {
    @t = unpack("CH2H2H2H16H16H16H4", $ts);    
    $t[4] = trEbcdicName($t[4]);
    $t[5] = trEbcdicName($t[5]);
    $t[6] = trEbcdicName($t[6]);
    $ts = substr($ts,$tl);
    $codefont{$t[4]} = "";
    push @MCF1, [@t]
    #print $OUTPUT join("  ", @t), "\n";
  }      
};

sub d3ab8a { my $self = shift;  my $trd = $_[0]; # Map Coded Font (MCF-2) Format 2

  my @tMCF2 = $self->ParseTripletGroups($trd);
  for my $tg (@tMCF2) {
    #print $OUTPUT ">>MCF-2>>>>>>>>>\n";
    #print $OUTPUT join("\n", map({unpack("H*", $_)} @$tg)), "\n";
  }
  for my $tg (@tMCF2) {
    my @mcf2;

    for my $t (@$tg) {
      if ( $t =~ /^\x24/ ) {
        $mcf2[0] = unpack("x2C", $t);
      }
      elsif ( $t =~ /^\x02\x8e/ ) {
        $mcf2[1] = rtrim($main::translator->toascii(unpack("x3a8", $t)));
      }
      elsif ( $t =~ /^\x02\x85/ ) {
        $mcf2[2] = rtrim($main::translator->toascii(unpack("x3a8", $t)));
      }
      elsif ( $t =~ /^\x02\x86/ ) {
        $mcf2[3] = rtrim($main::translator->toascii(unpack("x3a8", $t)));
      }
      elsif ( $t =~ /^\x1f/ ) {
        $mcf2[4] = unpack("x3 n", $t);
      }
      elsif ( $t =~ /^\x5d/ ) {
        $mcf2[5] = unpack("x1 n", $t);
      }
    }

    $mcf2[3] =~ s/^C[123]/C0/ if !$mcf2[1]; push @MCF2, [@mcf2];
  }
};

sub d3abea { my $self = shift;  my $trd = $_[0]; # Map Suppression 
 my ($at, $atmax) = (0, length($trd)); my $l;
  while($at < $atmax) {
    my @t = unpack("a8xC", substr($trd, $at, 10)); 
    $t[0] = $main::translator->toascii($t[0]);
    $t[0] =~ s/ +$//g;
    $l .= " /$t[1] ($t[0])";
    $at += 10;
  }
  $ps->add("/MapSuppression <<$l >> def");
};

sub d3a89b { my $self = shift;  my $trd = $_[0]; # Begin Presentation Text Object 
};

sub d3ee9b { my $self = shift;  my $trd = $_[0]; # Presentation Text Data

  if (!$in_ptoca_seqs) {
    $ps->add("a.init.ptocaseqs"); $in_ptoca_seqs = 1;
  } 
  $self->PutPtocaSeqs($trd);
};

sub d3abcc { my $self = shift;  my $trd = $_[0]; # Invoke Medium Map (IMM)
 
  my $mm = $main::translator->toascii(substr($trd,0,8)); $mm =~ s/ +$//;
  $ps->add("($mm) a.setMediumMap") if $trd ne "";
  if ( my $jr = $self->{jr} ) {
    $jr->setValues('LastMediumMap', $mm);
  }
  #print $OUTPUT "$mm\n" if $trd ne "";
};

sub d3abca { my $self = shift;  my $trd = $_[0]; # Invoke Data Map (IDM)
  my $dm = $main::translator->toascii(substr($trd,0,8));
  $dm =~ s/ +$//;
  $ps->add("($dm) a.setDataMap\n") if $trd ne "";
  #print $OUTPUT $dm, "\n" if $trd ne "";
};

sub d3afd8 { my $self = shift;  my $trd = $_[0]; # Include Page Overlay (IPO)

#  my @IPO = myunpackeb("e8m3m3H4c*",$trd);
#  $IPO[0] = rtrim($IPO[0]);
  my @IPO = unpack("a8xs>xs>H4a*",$trd);
  $IPO[0] = rtrim($main::translator->toascii($IPO[0]));
  $ps->add(
    "\%$IPO[0]  << BEGIN (INCLUDE PAGE OVERLAY)"
   , $IPO[1]*$XEXP." ".$IPO[2]*$YEXP." ($IPO[0]) a.IncOverlay"
   ,"\%$IPO[0]  << END" 
  );
  $self->ParseTripletGroup($IPO[4]) if $IPO[4] ne "";
  #print $OUTPUT join("  ", @IPO), "\n";
};

sub d3af5f { my $self = shift;  my $trd = $_[0]; # Include Page Segment (IPS)

  my @IPS = unpack("a8xs>xs>a*",$trd);
  $IPS[0] = rtrim($main::translator->toascii($IPS[0]));
  print "IPS:", join('::', map { unpack('H*', $_ ) } @IPS), "\n";
#  my @IPS = myunpackeb("e8m3m3c*",$trd);
#  print "IPS:", join('::', map { unpack('H*', $_ ) } @IPS), "\n";
#  $IPS[0] = rtrim($IPS[0]);
  $ps->add(
    "\%$IPS[0]  << BEGIN (INCLUDE PAGE SEGMENT)"
   , $IPS[1]*$XEXP." ".$IPS[2]*$YEXP." ($IPS[0]) a.IncPSegment"
   ,"\%$IPS[0]  << END" 
  );
  $self->ParseTripletGroup($IPS[3]) if $IPS[3] ne "";
  #print $OUTPUT join("  ", @IPS), "\n";
};

sub d3afc3 { my $self = shift;  my $trd = $_[0]; # Include Object (IOB)  
  
#  my @IOB = myunpackeb("e8H2H2m3m3H4H4m3m3H2",$trd); $IOB[0] = rtrim($IOB[0]);
  my @IOB = unpack("a8H2H2xs>xs>H4H4xs>xs>H2",$trd); $IOB[0] = rtrim($main::translator->toascii($IOB[0]));
  my $angle = unpack("n", pack("H4", $IOB[5])) >> 7;
  $ps->add(
    "\%$IOB[0]  << BEGIN (INCLUDE OBJECT)"
   , $IOB[3]*$XEXP." ".$IOB[4]*$YEXP." $angle ($IOB[0]) a.IncObject"
   ,"\%$IOB[0]  << END"
 );
 #print $OUTPUT join("  ", @IOB), "\n";
};

sub d3a78a { my $self = shift;  my $trd = $_[0]; # Coded Font Control (CFC)

  $self->ParseTripletGroup(substr($trd,2));
};

sub d3a8a8 { my $self = shift;  my $trd = $_[0]; # Begin Document (BDT)
 
  my $dn = $main::translator->toascii(substr($trd,0,8));
  $fileType="Document"; $doc_ended = 0; $Duplexing = 0; $DuplexingPages = 0;
  $ps->add("a.BeginDocument", "");
  if ( $FormDef ne "" ) {
    $ps->add("($FormDef) a.setFormDef", "");
    if ( my $jr = $self->{jr} ) {
      $jr->setValues('LastFormDef', $FormDef);
    }
    $FormDef = ""; $PageDef = "";
  }
  $self->ParseTripletGroup(substr($trd,10)) if length($trd)>10;
  #print $OUTPUT "$dn\n";
};

sub d3a9a8 { my $self = shift;  my $trd = $_[0]; # End Document (EDT)
  $fileType="Document"; $doc_ended = 1; 
};

sub d3a8af { my $self = shift;  my $trd = $_[0]; # Begin Page (BPG)
 $PAGE_STARTED = 1; $PAGE_ENDED = 0; my $pn;
  #reset variabili
  @MCF1 = (); %FillArea = ();
  $self->{TextItems} = {}; $self->{TextLines} = {};
  if ( length($trd) > 8 ) {
    $pn = $main::translator->toascii(substr($trd,0,8));
    $self->ParseTripletGroup(substr($trd,8)) 
  }
  #print $OUTPUT "$pn\n";
};

sub d3a9af { my $self = shift;  my $trd = $_[0]; # End Page (EPG) #todo: aggiungere fonts usati
 return if !$PAGE_STARTED;
  $self->ParseTripletGroup($trd) if $trd;
  if (scalar(keys(%FillArea))) {
    my ($cmds_before, $cmds_after) = MakeFillCmds();
    @$cmds_before and $ps->insert(
     qr/BEGIN INSERT FillArea COMMANDS/i, $cmds_before
    );
    @$cmds_after and $ps->add(@$cmds_after);
  }
  $ps->add("a.EndPage\n\n");
  $self->MergeTextItems() if $extract_text;
  @MCF1 = (); @MCF2=(); %FillArea = (); $PAGE_STARTED = 0; $PAGE_ENDED = 1;
};

sub d3a8fb { my $self = shift;  my $trd = $_[0]; # Begin Image Object (BIM)
  my $in = $main::translator->toascii(substr($trd,0,8));
  #print $OUTPUT "$in\n";
  $lIPD = 0; @IPD = (); @IDD = (); @OBP = (); @OBD = (); @MIO = ();
};

sub d3abfb { my $self = shift;  my $trd = $_[0]; # Map IO Image Object 

  for my $t ($self->ParseTripletGroup(substr($trd,2))) {
    if (substr($t,0,1) eq "\x04") {
      $MIO[0] = unpack("C", substr($t,1,1));
    }
  }
};

sub render_empty_ipd {
  ### what ?????? -------------------------------------------------------------------------- ###
  my ($obx, $oby, $imw, $imh, $gray) = ( $OBP[2]/6, $OBP[3]/6, $IDD[4]/6, $IDD[3]/6, 1.0);

  if ( length($OBD[3]) >=14 ) {
    $gray = (255 - unpack("x13C",$OBD[3]) ) / 255; 
  }
  #verifica rotazione assi (qui inversione imw e imh)
  #calcolo grayArea
  #$ps->add "$obx $oby $imw $imh $gray a.FillArea\n"; #bypass if only background ?
  ### what ?????? -------------------------------------------------------------------------- ###

  $lIPD = 0; @IPD = (); @IDD = (); @OBP = (); @OBD = (); @MIO = ();
}

sub reverse_bits {
  my $i = 0; my $imax = length($_[0]); 
  while($i < $imax) {
    substr($_[0],$i,1) = pack("B*", reverse(unpack("B*", substr($_[0],$i,1)))); $i += 1;
  }
}

sub parse_ioca_tile {
    my ($ipd, $image) = @_; 
    my (
      $denx, $deny, $idesize, $ideflags, $cmethod, $bitorder, $colorspace, $color, $comps, $lbitsxcomp, $bitsxcomp, $lutid, $tbands, $lbitsxband, $bitsxband 
    ) = 
    @{$image}{qw(denx deny idesize ideflags cmethod bitorder colorspace color comps lbitsxcomp bitsxcomp lutid tbands lbitsxband bitsxband)};

    my (
      $recalg, $tilecolor, $relres, $clx, $cly, $clw, $clh, $bands
    );

    while (@$ipd) {
      my ($code, $parameters) = splice(@IPD, 0,2); 
      #print "tile $code\n";

      if ($code eq 'b5') { # tile position
        ($clx, $cly) = unpack("NN", $parameters);
      }

      elsif ($code eq 'b6') { # tile size
        ($clw, $clh, $relres)= unpack("NNC", $parameters); $relres = 1 if !$relres;
      }

      elsif ($code eq '95') { # image encoding parameter
              #print "---------- Passo di qua ($cmethod)\n";
        ($cmethod, $recalg, $bitorder) = unpack("H2H2C", $parameters) if(!defined($cmethod));
        #print "++++++++++ Passo di qua ($cmethod)\n";
      }

      elsif ($code eq '96') { # ide size parameter
        $idesize = unpack("C", $parameters);
      }

      elsif ($code eq '97') { # lut-id parameters
        $lutid = unpack("C", $parameters); 
      }

      elsif ($code eq '98') { # band image size
        ($tbands, @$lbitsxband) = unpack("CC*", $parameters); $bitsxband = $lbitsxband->[0];
      }
 
      elsif ($code eq '9b') { # ide structure parameter
        ($ideflags, $colorspace, @$lbitsxcomp) = unpack("CCx3C*", $parameters); 
        #print "====> $colorspace kkkk\n";
        #todo: test colorspace
      }

      elsif ($code eq 'b7') { # tile set color
        @$tilecolor = unpack("H2x4C*", $parameters);
      }

      elsif ($code eq 'feb8') { # include tile
        die("include tile not managed"); # ??? manage
      }

      elsif ($code eq '9f') { # external algorithm specification
        die("external algorithm specification not managed");
      }

      elsif ($code eq 'f7') { # ioca function set identification
        die("ioca function set identification not managed");
      }

      elsif ($code eq 'fe9c') { # band image data
        my $ic = unpack("C", substr($parameters,0,1))-1; $parameters = substr($parameters,3);
        $bands->[$ic] .= ($bitorder == 0 ? $parameters : reverse_bits($parameters));
      }

      elsif ($code eq '8d') { # end tile 
        $comps = @$lbitsxcomp, $bitsxcomp = $lbitsxcomp->[0];
        return {
         clx => $clx, cly => $cly, clw => $clw, clh => $clh, 
         denx => $denx, deny => $deny, relres => $relres, 
         idesize => $idesize, 
         cmethod => $cmethod,  
         colorspace => $colorspace, color => $color, 
         bitsxband => $bitsxband, 
         lutid => $lutid, 
         tbands => $tbands, 
         comps => $comps,
         bitsxcomp => $bitsxcomp, 
         tilecolor => $tilecolor,  
         bands => $bands
        };
      }

      elsif ($code eq '8e') { # begin transparency mask
        #print "ZZZZZ $code\n";
      }

      elsif ($code eq '94') { # image size
        my ($units, $denx, $deny, $imw, $imh) = unpack("H2nnnn", $parameters);
        #todo: manage units
        #print "ZZZZZ $code\n";
      }

      elsif ($code eq 'fe92') { # transparency image data
        #print "ZZZZZ $code\n";
      }

      elsif ($code eq '8f') { # end transparency mask
        #print "ZZZZZ $code\n";
      }

      else {
        die("IOCA PARSE_SEGMENT CODE POINT ($code) NOT IDENTIFIED !!!");
      }
    }
  }

sub parse_ioca_image {
    my ($ipd, $segment) = @_; my ($imx, $imy, $imw, $imh, $denx, $deny, $color) = (0, 0, @IDD[3,4,1,2,6]); my $image = undef;

    $color = "ff00"  if $color eq ""; 

    my (
      $colorspace, $comps, $lbitsxcomp, $bitsxcomp, $lutid, 
      $cmethod, $bitorder, 
      $units, $recal, 
      $idesize, $ideflags, 
      $tbands, $lbitsxband, $bitsxband, $bands, $tiles,
    );
    $bands = []; $tiles = []; $idesize = 1; 
    
    $colorspace = 0; $lbitsxcomp = [1]; $comps = @$lbitsxcomp; $bitsxcomp = $lbitsxcomp->[0]; $lutid = 0;

    while (@$ipd) {
      my ($code, $parameters) = splice(@IPD, 0,2);
      #print "image $code\n";

      if ($code eq '94') { # image size parameter transparency
        ($units, $denx, $deny, $imw, $imh) = unpack("H2nnnn", $parameters);
        #print "+++++ ($units, $denx, $deny, $imw, $imh)\n";
        #todo: manage units
      }

      elsif ($code eq '95') { # image encoding parameter
              #print "---------- Passo di qua2 ($cmethod)\n";
        ($cmethod, $recal, $bitorder) = unpack("H2H2C", $parameters) if(!defined($cmethod));
        #print "++++++++++ Passo di qua2 ($cmethod)\n";
        #todo: check recal
      }

      elsif ($code eq '96') { # ide size parameter
        $idesize = unpack("C", $parameters);
      }

      elsif ($code eq '97') { # lut-id parameters
        $lutid = unpack("C", $parameters); 
      }

      elsif ($code eq '98') { # band image size
        ($tbands, @$lbitsxband) = unpack("CC*", $parameters); $bitsxband = $lbitsxband->[0];
      }
 
      elsif ($code eq '9b') { # ide structure parameter
        ($ideflags, $colorspace, @$lbitsxcomp) = unpack("CCx3C*", $parameters); $comps = @$lbitsxcomp; $bitsxcomp = $lbitsxcomp->[0];
        #todo: test colorspace
      }

      elsif ($code eq 'f6') { # set bilevel image color
      }

      elsif ($code eq '9f') { # external algorithm specification
        die("external algorithm specification not managed");
      }

      elsif ($code eq 'fece') { # image subsampling parameter
        die("image subsampling parameter not managed");
      }

      elsif ($code eq 'febb') { # tile toc
      }

      elsif ($code eq '8c') { # begin tile
        #todo pass image to set default values
        if (!$image) {
          $image = {
           imx => $imx, imy => $imy, imw => $imw, imh => $imh, 
           denx => $denx, deny => $deny, 
           idesize => $idesize, 
           cmethod => $cmethod,  
           colorspace => $colorspace, color => $color, 
           comps => $comps, lbitsxcomp => $lbitsxcomp, bitsxcomp => $bitsxcomp,
           lutid => $lutid, 
           tbands => $tbands, 
           lbitsxband => $lbitsxband, bitsxband => $bitsxband
          };
        }
        push @$tiles, parse_ioca_tile($ipd, $image);
      }
      
      elsif ($code eq 'fe92') { # image data elements
        $bands->[0] .= ($bitorder == 0 ? $parameters : reverse_bits($parameters));
      }

      elsif ($code eq 'fe9c') { # band image data
        my $ic = unpack("C", substr($parameters,0,1))-1; $parameters = substr($parameters,3);
        $bands->[$ic] .= ($bitorder == 0 ? $parameters : reverse_bits($parameters));
      }

      elsif ($code eq '93') { # end image content
        if (!@$tiles) {
          push @$tiles, {
           clx => $imx, cly => $imy, clw => $imw, clh => $imh, 
           denx => $denx, deny => $deny, 
           idesize => $idesize, 
           cmethod => $cmethod,  
           colorspace => $colorspace, color => $color, 
           comps => $comps, lbitsxcomp => $lbitsxcomp, bitsxcomp => $bitsxcomp,
           lutid => $lutid, 
           tbands => $tbands, bitsxband => $bitsxband, bands => $bands
         };
         $image = {
           imx => $imx, imy => $imy, imw => $imw, imh => $imh, 
           denx => $denx, deny => $deny, 
           idesize => $idesize, 
           cmethod => $cmethod,  
           colorspace => $colorspace, color => $color, 
           comps => $comps, lbitsxcomp => $lbitsxcomp, bitsxcomp => $bitsxcomp,
           lutid => $lutid, 
           tbands => $tbands, 
           lbitsxband => $lbitsxband, bitsxband => $bitsxband
          };
       }
       $image->{'tiles'} = $tiles; return $image;
      }

      else {
        die("IOCA PARSE_SEGMENT CODE POINT ($code) NOT IDENTIFIED !!!");
      }
    }
    die "!!!!!!!!!!!!!!!!!!!!!!kkkkkkkkkkkkkkkkkkkkkkk\n";
  }

sub render_ioca_images {
  my $images = shift; 

  my (
    $obx, $oby, $obw, $obh, $usx, $usy, $usw, $ush, $denx, $deny, $mio
  ) = 
  (@OBP[2,3], @OBD[0,1], 0, 0, @IDD[3,4,1,2], $MIO[0]); $mio = SCALE_TO_FIT if !$mio; 

  $usw *= 2400/$denx; $ush *= 2400/$deny;
  
  my ($obratio, $usratio) = ($obw/$obh, $usw/$ush);
  my $scale = 1;
  my $color = $IDD[6] || "ff00"; #if bilevel 
  my $angle = $ROTATE_Angle{"($OBP[4]) ($OBP[5])"} || 0;
  
  my ($image, $tiles, $tile, $bands, $band, $cmethod, $bitorder); my @l;

  if ($mio == SCALE_TO_FIT) {
    $obratio > $usratio and $obw = $obh * $usratio or $obh = $obw/$usratio; $scale = 1;
  }
  elsif ($mio == POSITION_AND_TRIM) {
    $scale = 0;
  }
  elsif ($mio == CENTER_AND_TRIM) {
    $usx = abs($obw-$usw)/2; $usy = abs($obh-$ush)/2; $scale = 0;
  }
  elsif ($mio == SCALE_TO_FILL) {
    $scale = 1;
  }
  else {
    die("IMAGE MAPPING OPTION ($mio // $MIO[0]) NOT MANAGED !!!!\n")
  }

  if ($usx or $usy) {
    for $image (@$images) {
      $tiles = $images->{'tiles'};
      for $tile (@$tiles) {
        $tile->{'clx'} += $usx; $tile->{'cly'} += $usy;
      }
    }
  }
 
  for $image (@$images) {

    my ($imw, $imh, $color, $bands) = @{$image}{qw(imw imh color bands)};
    if ( $cmethod == IMG_BITMAP_UNCOMPR ) {
      if ( ($obw != $usw || $obh != $ush) && $imw * $imh == 32 * 8 ) {
        my $imd = unpack("B*", $bands->[0]); $imd =~ s/0//g; my $gray = 1 - length($imd)/($imw*$imh); 
        if ($fileType eq "Document" or $fileType eq "Overlay") {
          my $ImageOr = "($OBP[4]) ($OBP[5])";
          $FillArea{$ImageOr} = [] if !exists($FillArea{$ImageOr});
          push @{$FillArea{$ImageOr}}, "$obx $oby $obw $obh $gray ($color) a.FillArea";
        }
        else {
          $ps->add("$obx $oby $obw $obh $gray ($color) a.FillArea");
        }
        return;
      }
    }
  }

  PutPsImages($obx, $oby, $obw, $obh, $usx, $usy, $usw, $ush, $angle, $scale, $images);
}

sub d3a9fb { my $self = shift;  my $trd = $_[0]; # End Image Object (EIM)

  return render_empty_ipd() if !@IPD; my $images = [];

  ### ------ IOCA MANAGEMENT -------------------------------------------------------------- ###
  
  #70 0 
  #91 1 ff begin image
  #fe92 2 0000
  #8c 0 begin tile
  #b5 8 0000000000000000 tile position
  #b6 9 000003700000010601 tile size
  #95 3 0d0100 image encoding
  #96 1 20 ide size ==> 4*8 = 32 = 0x20
  #98 5 0408080808 band image size
  #9b 9 000400000008080808 ide structure parameter
  #fe9c 1716
  #.........
  #8d 0 end tile
  #93 0 end image
  #71 0
  
  my $ipd = \@IPD;
  
  while (@$ipd) {
    my ($code, $parameters) = splice(@IPD, 0,2);

    if ($code eq '70') { # begin segment 
      # manage segment name/id
    }

    elsif ($code eq '91') { # begin image content
      push @$images, parse_ioca_image($ipd); 
    }

    elsif ($code eq '71') { # end segment 
    }

    else {
      die("IOCA PARSE_SEGMENT CODE POINT ($code) NOT IDENTIFIED !!!");
    }
  }

  render_ioca_images($images);

  $lIPD = 0; @IPD = (); @IDD = (); @OBP = (); @OBD = (); @MIO = ();
};


sub getBarcode4jBean {
  my ($self, $barcodeType) = splice(@_, 0, 2); my $barcode4jTable = $self->{'barcode4jTable'}; require XReport::barcode4j;
  if (!exists($barcode4jTable->{$barcodeType})) {
    $barcode4jTable->{$barcodeType} = XReport::barcode4j->new($barcodeType);
  }
  return $barcode4jTable->{$barcodeType};
}

my $lx_num = qr/-?[\d\.]+/; my $lx_bounding_box = qr/BoundingBox: +($lx_num) +($lx_num) +($lx_num) +($lx_num)/;

sub getEpsBoundingBox {
  my ($self, $epsFileName) = splice(@_, 0, 2); my $INPUT = gensym(); my $line; 

  open($INPUT, "<$epsFileName")
   or
  die("getEpsBoundingBox INPUT OPEN ERROR \"$epsFileName\" $!");
  while(1) {
    $line = <$INPUT>; last if $line eq "" or $line =~ /$lx_bounding_box/o;
  }
  close($INPUT);

  if ($line =~ /$lx_bounding_box/o) {
    return ($1, $2, $3, $4);
  }

  die "getEpsBoundingBox line($line) not matching\n";
}

sub getEpsDeltaY {
  my $self = shift; my ($llX, $llY, $urX, $urY) = $self->getEpsBoundingBox(@_); return ($urY - $llY);
}

sub d3a8eb { my $self = shift;  my $trd = $_[0]; # Begin Bar Code Object
  @BDD = (); @BDA = ();
};

sub d3a6eb { my $self = shift;  my $trd = $_[0]; # Bar Code Data Descriptor (BDD)
  @BDD = unpack("H2H2n4H4H2H2H2H4CnCn", $trd);
  #print $OUTPUT join(",", @BDD), "\n";
  #print "Bar Code Data Descriptor (BDD) => ", join(",", @BDD), "\n";
};

# Bar Code Data Descriptor (BDD) DataMatrix => 
# 00, ten inches # 
# 00, as is
# 2400, => units x unit base (ten inches)
# 2400, => units x unit base (ten inches)
# 1984,
# 2806, 
# 0000,
# 1c, datamatrix
# 00,
# ff, Local Id
# 0000, color
# 255, X-width in mils (1_inch/1000) (255 0xFF => default for device = optimal symbol) 
# 2400, height in L units
# 1,
# 250

sub d3eeeb { my $self = shift;  my $trd = $_[0]; # Bar Code Data (BDA)
  @BDA = unpack("B8nna*", $trd);
  #print $OUTPUT join(",", @BDA);
  #print "Bar Code Data (BDA) => ", join(",", @BDA), "\n";
};

sub d3a9eb { my $self = shift;  my $trd = $_[0]; # End Bar Code Object
 my $jr = $self->{'jr'}; my $localres = $jr->getFileName('localres'); my ($barcodeType, $bean); 
  
  my ($obX, $obY, $obA) = ($OBP[2], $OBP[3], $ROTATE_Angle{"($OBP[4]) ($OBP[5])"});
  
  my $bcX = $BDA[1]; my $bcY = $BDA[2];

  if ($BDD[7] eq "11") {
    do { require Barcode::Code128; $bc128 = Barcode::Code128->new(); } 
     if 
    !$bc128;
    my $bcXb = $BDD[11] * ($BDD[2]/10000); my $bcH = $BDD[12]*$BDD[13];
  
    ### verify barcode type and parameters, verify measurement units
  
    my $bc = $bc128->barcode($main::translator->toascii($BDA[3])); $bc =~ tr/ \#/01/; 
  
    my $startFill = (substr($bc,0,1) eq '1') ? 'true' : 'false';
  
    my $bcList = join(" ", map {length($_)} split(/(?<=0)(?=1)|(?<=1)(?=0)/,$bc)); 
  
    $ps->add(
      "gsave"
     ,"(0000) (2d00) a.SetMatrix "
     ," $obX $obY translate "
     ," $obA rotate "
     ," $bcX $bcY moveto "
     ," [ $bcList ] $startFill $bcXb $bcH  a.barcode"
     ,"grestore"
    );
  }
  else {
    if ($BDD[7] eq "0c") {
      $bean = $self->getBarcode4jBean($barcodeType='Int2Of5'); $bean->setMsgPosition("none"); 
    
      $BDD[11] = 13 if $BDD[11] == 255; # 13 mils recommended 

      if ($BDD[8] ne '01' and $BDD[8] ne '02') {
        print "BDD => ", join(",", @BDD), "\n"; die("INVALID CHECKDIGIT MODE ($BDD[8])\n")
      }
      $bean->setChecksumMode($BDD[8] eq '01' ? 'IGNORE' : 'ADD');

      $bean->setBarHeight($BDD[12]*$BDD[13]*254/$BDD[3]); # height in mm (254 mm in 10 inchs)
    }
    elsif ($BDD[7] eq "1c") {
      $bean = $self->getBarcode4jBean($barcodeType='DataMatrix'); @BDA = (@BDA[0..2], unpack("B8n2C4B8a*", $BDA[3])); 
  
      # Bar Code Data (BDA) => DataMatrix =>
      # 00000000, 8 bits
      # 1133, ??
      # 377, ??
      # 00000000,
      # 0, row size
      # 0, number of rows
      # 0, structured append sequence indicator
      # 0, total number of structured append symbols
      # 254,
      # 254,
      # 00000000,
      # ROIETTI CLAUDIO VIA VENETO , 1 00100 ROMA ASSEGNO N. 6.500.000.09
    
      $BDD[11] = 21 if $BDD[11] == 255; # 21 mils recommended 
    
      $bean->setMinSize($BDA[4], $BDA[5]); $bean->setMaxSize($BDA[4], $BDA[5]);
    }
    else {
      print "Bar Code Data Descriptor (BDD) => ", join(",", @BDD), "\n";
      print "Bar Code Data (BDA) => ", join(",", @BDA), "\n";
      die("BARCODE CODE \"$BDD[7]\" NOT MANAGED"); 
    }
    
    my $barcode_text = $main::translator->toascii($BDA[-1]); $self->{'lprogr'} += 1;

    my $barcode_file_name = "$localres/$barcodeType.$self->{lprogr}". md5_hex($barcode_text) .".eps";

    $bean->setModuleWidth(0.0254 * $BDD[11]); # BDD[11] = mills of inch -- 0.0254 (25.4/1000) = mm x mills 
    $bean->generateBarcode(
      $barcode_file_name, $barcode_text
    ); 
    my $deltaY = $self->getEpsDeltaY($barcode_file_name);  

    $ps->add(
      "gsave"
     ," $obX $obY translate "
     ," $obA rotate "
     ," $bcX $bcY translate "
     ," 3.33333333 -3.33333333 scale "
     ," 0 -$deltaY translate "
     ," ($barcode_file_name) run "
     ,"grestore"
    );
  }

  @BDD = (); @BDA = ();
};


sub d3a66b { my $self = shift;  my $trd = $_[0]; # Object Area Descriptor (OBD)
 my ($obw, $obh, $XEXP, $YEXP); 
  for my $t ($self->ParseTripletGroup($trd)) {
    if (substr($t,0,1) eq "\x4b") {
      my @_4b = unpack("xC2n2", $t);
      $XEXP = 2400/$_4b[2]; $_4b[0] == 1 and $XEXP /= 2.54;
      $YEXP = 2400/$_4b[3]; $_4b[1] == 1 and $YEXP /= 2.54;
    }
    elsif (substr($t,0,1) eq "\x4c") {
      ($obw, $obh) = unpack("x2xs>xs>", $t);
    }
  }
  @OBD[0,1] = ($obw*$XEXP, $obh*$YEXP);
};

sub d3ac6b { my $self = shift;  my $trd = $_[0]; # Object Area Position (OBP)
 my ($Xexp, $Yexp);

  @OBP = unpack("H2CxnxnH4H4H2xnxnH4H4H2",$trd);

  $Xexp = $PGD[2] ? 2400/$PGD[2] : 1;
  $Yexp = $PGD[3] ? 2400/$PGD[3] : 1;

  @OBP[2,3] = ($OBP[2]*$Xexp, $OBP[3]*$Yexp);
  @OBP[7,8] = ($OBP[7]*$Xexp, $OBP[8]*$Yexp);
  
  #print $OUTPUT join("  ", @OBP), "\n";
};

sub d3a6fb { my $self = shift;  my $trd = $_[0]; # Image Data Descriptor (IDD)
  @IDD = unpack("H2n4a*", $trd); my $s = $IDD[5];
  while ( $s ne '') {
    my ($i, $l) = unpack("aC", $s); die "||- IDD -||" if $l == 0; 
    if ( $i eq "\xf6") {
      $IDD[6] = unpack("H4", substr($s, 4,2));
    }
    elsif ( $i eq "\xf7") {
      $IDD[7] = unpack("C", substr($s, 3,1));
    }
    $s = substr($s,2+$l);
  }
  #print $OUTPUT join("  ", @IDD), "\n";
};

sub d3eefb { my $self = shift;  my $trd = $_[0]; # Image Picture Data (IPD)
  if ( $lIPD > 0 ) {
    $IPD[-1] .= substr($trd,0, $lIPD); 
    if ( length($trd) > $lIPD ) {
      $trd = substr($trd, $lIPD); $lIPD = 0;
    }
    else {
      $lIPD = $lIPD - length($trd); $trd = ''; 
    }
  }
  return if $trd eq '';

  my ($at, $code, $ll, $body);

  while($at < length($trd)) {
    ($code, $ll) = unpack("H2C", substr($trd,$at,2));
    if ( $code ne "fe" ) {
      $body = substr($trd,$at+2, $ll);
      $at += 2+$ll;
    }
    else {
      $code .= unpack("H2", substr($trd, $at+1, 1)); 
      $ll = unpack("n", substr($trd, $at+2, 2));
      $body = substr($trd,$at+4, $ll);
      $lIPD = $ll - length($body);
      $at += 4+$ll;
    }
    push @IPD, $code, $body;
    #print $OUTPUT "$c $ll ", unpack("H*", $body), "\n";
  }
};

sub d3a8bb { my $self = shift;  my $trd = $_[0]; # Begin Graphics Object (BGO)
  $tGDA = ''; @GDA = (); @GDD = (); @OBP = (); @OBD = ();
};

sub d3a6bb { my $self = shift;  my $trd = $_[0]; # Graphics Data Descriptor (GDD)
  my $gdd = substr($trd,0,1) eq "\xf6" ? $trd : substr($trd,9,20); my ($XEXP, $YEXP);
  
  @GDD = unpack("x2H2H2H2H2(S>)3(s>)4", $gdd);
  ($XEXP, $YEXP) = map {2400/$_} @GDD[4,5];

  @GDD[7,8] = map {$_ *= $XEXP} @GDD[7,8];
  @GDD[9,10] = map {$_ *= $YEXP} @GDD[9,10];
};

sub d3eebb { my $self = shift;  my $trd = $_[0]; # Graphics Data (GDA)
  $tGDA .= $trd;
};

sub d3a9bb { my $self = shift;  my $trd = $_[0]; # End Graphics Object (EGO)
  my ($obx, $oby, $obw, $obh, $angle) = (@OBP[2,3], @OBD[0,1], $ROTATE_Angle{"($OBP[4]) ($OBP[5])"}); my $gda = $tGDA;

  my ($gwx, $gwy, $gww, $gwh) = @GDD[7,9,8,10]; $gww -= $gwx; $gwh -= $gwy;

  my ($XEXP, $YEXP) = map {2400/$_} @GDD[4,5];

  if ( !$gww or !$gwh) {
    my $msg = "Graphic object with null dimensions (w:$gww h:$gwh) - " . join('::', @GDD);
    warn $msg, "\n";
    $ps->add("\%$msg");
    $tGDA = ''; @GDA = (); @GDD = (); @OBP = (); @OBD = ();
    return;
  }

  my ($scalex, $scaley) = ($obw*$XEXP/$gww, $obh*$YEXP/$gwh); my ($inarea, $area_bits) = (0, "");

  $ps->add(
    "save \%GDA"
   ,"$obx $oby translate"
  #,"-$gwx -$gwy translate"
   ,"$gwx $gwy translate"
   ,"$angle rotate"
   ,"[1 0 0 -1 0 $gwh] concat"
   ,"$scalex $scaley scale"
  );
  $ps->add("0 0 moveto");

  my $lwunit = 2.4/$XEXP; my $lw = $lwunit; my $pattern_symbol = 0; my $progr = 0; my $last_pattern = ''; my $last_color = '';

  my ($imw, $imh, $strImg);

  my $lFontIdx = -1; my @arc_parameters = ();

  while($gda ne "") {
    my $gseglen = 14 + unpack("n", substr($gda,8,2)); my $gseg = substr($gda, 0, $gseglen); my $at = 0; my $ll;

    while($at < $gseglen) {
      my $order = unpack("H2", substr($gseg,$at,1)); $at+=1; $ll = 0;

      if ($order eq "00") {      # No Operation               | GNOP1      | 7.10.1| 
      }
      elsif ($order eq "01") {   # Comment                    | GCOMT      | 7.10.4| 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "04") {   # Segment Characteristics    | GSGCH      | 7.10.1| 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "08") {   # Set Pattern Set            | GSPS       | 7.10.3| 
              #print "==> $order <==\n";
      }
      elsif ($order eq "0a") {   # Set Color                  | GSCOL      | 7.10.2| 
        my $color = unpack("H2", substr($gseg,$at,1));
        $last_color = "(00$color) (01) a.stc";
        $ps->add($last_color) if !$inarea;
        }
      elsif ($order eq "0c") {   # Set Mix                    | GSMX       | 7.10.3| 
        my $mix = unpack("H*", substr($gseg,$at,1));
        if ($mix ne "02") {
                #print "==> Set Mix Order: mix($mix) ne 02 !!! <==\n";
        }
      }
      elsif ($order eq "0D") {   # Set Background Mix         | GSBMX      | 7.10.1| 
              #print "==> $order <==\n";
      }
      elsif ($order eq "11") {   # Set Fractional Line Width  | GSFLW      | 7.10.2| 
        $ll = unpack("C", substr($gseg,$at,1));

        my ($l1, $l2) = unpack("CC", substr($gseg,$at+1,$ll));
        $l1 *= $lwunit; $l2*=$lwunit/256; $lw = $l1+$l2;
        $ps->add("$lw setlinewidth \% frac");

      }
      elsif ($order eq "18") {   # Set Line Type              | GSLT       | 7.10.2| 
        my $ltype = unpack("H2", substr($gseg,$at,1));
        #print "==> $order $ltype<==\n";
      }
      elsif ($order eq "19") {   # Set Line Width             | GSLW       | 7.10.2| 
        $lw = unpack("C", substr($gseg,$at,1));
        $lw *= $lwunit;
        $ps->add("$lw setlinewidth \% no frac");
      }
      elsif ($order eq "21") {   # Set Current Position       | GSCP       | 7.10.2| 
        $ll = unpack("C", substr($gseg,$at,1));
        $ps->add(join(" ", unpack("s>s>",substr($gseg,$at+1,4)), "moveto")); 
      }
      elsif ($order eq "22") {   # Set Arc Parameters         | GSAP       | 7.10.1| 
        $ll = unpack("C", substr($gseg,$at,1));
        @arc_parameters = unpack("(s>)4", substr($gseg,$at+1,8));
        #print "==> $order ", join(",", @arc_parameters) ," <==\n";
      }
      elsif ($order eq "26") {   # Set Extended Color         | GSECOL     | 7.10.2| 
        $ll = unpack("C", substr($gseg,$at,1));

        my $ecolor = unpack("H*", substr($gseg,$at+1, $ll));

        $last_color = "($ecolor) (01) a.stc"; $ps->add($last_color) if !$inarea;
      }
      elsif ($order eq "28") {   # Set Pattern Symbol         | GSPT       | 7.10.3| 
        $pattern_symbol = unpack("C", substr($gseg,$at,1));
        $ps->add("\%Set Pattern Symbol $pattern_symbol");
        #print "==> $order <==\n";
      }
      elsif ($order eq "29") {   # Set Marker Symbol          | GSMT       | 7.10.3| 
              #print "==> $order <==\n";
      }
      elsif ($order eq "33") {   # Set Character Cell         | GSCC       | 7.10.1| 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "34") {   # Set Character Angle        | GSCA       | 7.10.1| 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "35") {   # Set Character Shear        | GSCH       | 7.10.2| 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "37") {   # Set Marker Cell            | GSMC       | 7.10.3| 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "38") {   # Set Character Set          | GSCS       | 7.10.2| 
        $FontIdx = unpack("C", substr($gseg,$at,1)); $FontIdx = 1 if $FontIdx == 255;
        if ($FontIdx != $lFontIdx) {
          $ps->add("F$FontIdx a.goca.setfont"); $lFontIdx = $FontIdx;
        }
      }
      elsif ($order eq "39") {   # Set Character Precision    | GSCR       | 7.10.2| 
              #print "==> $order <==\n";
      }
      elsif ($order eq "3a") {   # Set Character Direction    | GSCD       | 7.10.2| 
              #print "==> $order <==\n";
      }
      elsif ($order eq "3b") {   # Set Marker Precision       | GSMP       | 7.10.3| 
              #print "==> $order <==\n";
      }
      elsif ($order eq "3c") {   # Set Marker Set             | GSMS       | 7.10.3| 
              #print "==> $order <==\n";
      }
      elsif ($order eq "3e") {   # End Prolog                 | GEPROL     | 7.10.7| 
      }
      elsif ($order eq "43") {   # Set Pick Identifier        | GSPIK      |   (1) | 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "60") {   # End Area                   | GEAR       | 7.10.5| 
        $ll = unpack("C", substr($gseg,$at,1)); my ($lline, $lstroke, $lfill) = ("", "", "fill");
        $ps->add("\%!! pattern_symbol = $pattern_symbol");
        $lline = $area_bits & "\x80";
        if ($lline) {
          $lstroke =  "stroke";
        }
        if ($last_pattern >= 1 and $last_pattern <= 8) {
          $ps->add((1.0 - (0.3 + $pattern_symbol/16))." a.SetColorLevel");
        }
        elsif ($last_pattern == 15) {
          $lstroke =  $lstroke; $lfill = ""; 
        }
        $ps->add("closepath gsave $lstroke grestore currentpoint moveto $lfill grestore", "\%End Area");
        $inarea = 0; $area_bits = undef; 
      }
      elsif ($order eq "68") {   # Begin Area                 | GBAR       | 7.10.1| 
        $area_bits = unpack("B*", substr($gseg,$at,1));
        $ps->add(
          "gsave currentpoint newpath moveto", "\%Begin Area $area_bits"
        );
        $inarea = 1; $last_pattern = $pattern_symbol;
      }
      elsif ($order eq "70") {   # Begin Segment              |            |   (2) | 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "71") {   # End Segment                |            |   (2) | 
        #print "==> $order <==\n";
      }
      elsif ($order eq "81") {   # Line at CP                 | GCLINE     | 7.10.1| 
        $ll = unpack("C", substr($gseg,$at,1));
        $progr += 1;
        #$ps->add("\% GCLINE Line at CP $progr", "($progr) ==");

        my @lpoints = unpack("s>" x int($ll/2), substr($gseg, $at+1, $ll)); my ($ix, $iy, $l);
        while(@lpoints) {
          ($ix, $iy) = splice(@lpoints, 0, 2);
          $l .= "$ix $iy lineto "; 
        }
        if ($inarea) {
          $ps->add($l);
        }
        else {
          $l .= " lstroke"; $ps->add("currentpoint newpath moveto $l");
        }
      }
      elsif ($order eq "82") {   # Marker at CP               | GCMRK      | 7.10.1| 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "83") {   # Character String at CP     | GCCHST     | 7.10.3| 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "85") {   # Fillet at CP               | GCFLT      | 7.10.8| 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "87") {   # Full Arc at CP             | GCFARC     | 7.10.9| 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "91") {   # Begin Image at CP          | GCBIMG     | 7.10.2| 
        $ll = unpack("C", substr($gseg,$at,1));
        ($imw, $imh) = unpack("n2", substr($gseg,$at+1+2,4)); 
        $strImg = '';
        #print "==> $order <==\n";
      }
      elsif ($order eq "92") {   # Image Data                 | GIMD       | 7.10.1| 
        $ll = unpack("C", substr($gseg,$at,1));
        $strImg = substr($gseg, $at+1, $ll) . $strImg;
        #print "==> $order $ll<==\n";
      }
      elsif ($order eq "93") {   # End Image                  | GEIMG      | 7.10.6| 
        $ll = unpack("C", substr($gseg,$at,1));
        PutPsImages(
          0, 0, $imw/$scalex, $imh/$scaley, 
          0, 0, $imw/$scalex, $imh/$scaley, 0, 1, [{
            tiles => [{
              clx => 0, cly => 0, clw => $imw => clh => $imh, denx => 2400, deny => 2400, cmethod => "03", 
              colorspace => 0, color => undef, idesize => 1, comps => 1, bitsxcomp => 1, 
              bands => [$strImg]
            }]
          }]
        );
        ($imw, $imh, $strImg) = ();
        #print "==> $order <==\n";
      }
      elsif ($order eq "a1") {   # Relative Line at CP        | GCRLINE    | 7.10.1| 
        $ll = unpack("C", substr($gseg,$at,1));
      }
      elsif ($order eq "b2") {   # Set Process Color          | GSPCOL     | 7.10.3| 
        $ll = unpack("C", substr($gseg,$at,1));
        my @sec = unpack("H2H2H8C4C4", substr($gseg,$at+1,$ll));
        if ( $sec[1] eq '40' ) {
          my $color = unpack("H4", pack("c2", @sec[7,8]));
          $last_color = "($color) (01) a.stc";
        }
        elsif ( $sec[1] eq '01' ) {
          $last_color = join(" ", (map {$_/255} @sec[7..9]), "setrgbcolor");
        }
        elsif ( $sec[1] eq '04' ) {
          $last_color = join(" ", (map {$_/255} @sec[7..10]), "setcmykcolor");
        }
        else {
          die join(",", @sec), "\n";
        }
        $ps->add($last_color) if !$inarea;
      }
      elsif ($order eq "c0") {   # Box                        | GBOX       | 7.10.3| 
        $ll = unpack("C", substr($gseg,$at,1));

        my ($lx, $ly, $ux, $uy, $dx, $dy) = unpack("(s>)4S>S>", substr($gseg, $at+3, 12)); my $l;

        if ($dx == 0 or $dy == 0) {
          my @lpoints = ($ux,$ly, $ux,$uy, $lx,$uy); $l = "$lx $ly moveto ";
          while(@lpoints) {
            my ($ix, $iy) = splice(@lpoints, 0, 2); $l .= "$ix $iy lineto ";
          }
        }
        else {
          ($lx, $ux) = ($ux, $lx) if $lx > $ux; ($ly, $uy) = ($uy, $ly) if $ly > $uy;
          my ($lx2, $ux2, $ly2, $uy2) = ($lx+$dx, $ux-$dx, $ly+$dy, $uy-$dy); 
          my @lpoints = (
            $ux2,$ly, $ux,$ly, $ux,$ly2, $ux,$uy2, $ux,$uy, $ux2,$uy, $lx2,$uy, $lx,$uy, $lx,$uy2, $lx,$ly2, $lx,$ly, $lx2,$ly
          ); 
          $l = "$lx2 $ly moveto ";
          while(@lpoints) {
            my ($ix, $iy) = splice(@lpoints, 0, 2); $l .= "$ix $iy lineto ";
            my ($ix2,$iy2, $ix3, $iy3) = splice(@lpoints, 0, 4); $l .= "$ix2 $iy2 $ix2 $iy2 $ix3 $iy3 curveto "; 
          }
        }

        $ps->add("newpath $l " . (!$inarea ? "closepath lstroke" : ""));
      }
      elsif ($order eq "c1") {   # Line                       | GLINE      | 7.10.1| 
        $ll = unpack("C", substr($gseg,$at,1));

        my @lpoints = unpack("s>" x int($ll/2), substr($gseg, $at+1, $ll)); my ($ix, $iy, $l);
        while(@lpoints) {
          ($ix, $iy) = splice(@lpoints, 0, 2);
          $l .= "$ix $iy" . (($l ne "") ? " lineto " : " moveto "); 
        }
        if ($inarea) {
          $ps->add($l);
        }
        else {
          $ps->add("newpath $l lstroke");
        }
      }
      elsif ($order eq "c2") {   # Marker                     | GMRK       | 7.10.1| 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "c3") {   # Character String           | GCHST      | 7.10.3| 
        $ll = unpack("C", substr($gseg,$at,1));

        my ($atx, $aty, $text) = (unpack("s>s>", substr($gseg, $at+1, 4)), substr($gseg, $at+1+4));
        text_as_comment($text) if $text_as_comment; 
        $ps->add("$atx $aty moveto gsave 1 $scalex div 1 $scaley div scale " . "<".unpack("H*",$text)."> a.trn grestore");
      }
      elsif ($order eq "c5") {   # Fillet                     | GFLT       | 7.10.8| 
        $ll = unpack("C", substr($gseg,$at,1));

        my @lpoints = unpack("s>" x int($ll/2), substr($gseg, $at+1, $ll)); my $l = '';
        while(@lpoints) {
          my ($ixa, $iya, $ixb, $iyb, $ixc, $iyc) = splice(@lpoints, 0, 6);
          $l .= "$ixa $iya moveto $ixb $iyb $ixb $iyb $ixc $iyc curveto";
        }
        $l .= " lstroke" if !$inarea; $ps->add($l);
      }
      elsif ($order eq "c7") {   # Full Arc                   | GFARC      | 7.10.9| 
        $ll = unpack("C", substr($gseg,$at,1));
        my ($cx, $cy) = unpack("s>s>", substr($gseg, $at+1, 4));
    my ($MH, $MFR) = unpack("CC", substr($gseg, $at+5, 2));
        my $mfact = $MH + $MFR/256;
        die "==> GFARC not orthogonal $order ", join(",", $cx, $cy, $MH, $MFR, @arc_parameters) ," <==\n" unless
      (($arc_parameters[0] * $arc_parameters[2]) + ($arc_parameters[3] * $arc_parameters[1])) == 0;
    my $l = ($arc_parameters[3] == 0 
         ? "$arc_parameters[0] " 
         : sprintf '%s %s atan rotate %s %s mul %s %s mul add sqrt ', @arc_parameters[3, 0, 0, 0, 3, 3]
        );
        $l .= $mfact . ' mul ' unless $mfact == 1;
    $l .= ($arc_parameters[2] == 0
           ? "$arc_parameters[1] "
           : sprintf '%s %s mul %s %s mul add sqrt ', @arc_parameters[2, 2, 1, 1]
              );
        $l .= ($mfact == 1 ? 'scale' : "$mfact mul scale");
        $ps->add(  
       "$cx $cy translate matrix currentmatrix $l newpath 0 0 1 0 360 arc setmatrix" . (!$inarea ? "closepath lstroke " : "")
     );
#        if ($arc_parameters[0] == $arc_parameters[1] and $arc_parameters[2] == $arc_parameters[3] and $arc_parameters[3] == 0) {
#         $ps->add(
#            "$cx $cy translate newpath 0 0 $arc_parameters[0] 0 360 arc " .  (!$inarea ? "closepath lstroke " : "")
#          );  
#        }
#        else {
#          die "==> $order ", join(",", $cx, $cy, @arc_parameters) ," <==\n";
#        }
        #print "==> $order ", join(",", $cx, $cy, @arc_parameters) ," <==\n";
      }
      elsif ($order eq "d1") {   # Begin Image                | GBIMG      | 7.10.2| 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "e1") {   # Relative Line              | GRLINE     | 7.10.1| 
        $ll = unpack("C", substr($gseg,$at,1));
        #print "==> $order <==\n";
      }
      elsif ($order eq "e3") {   # Partial Arc                | GPARC      | 7.10.1| 
        $ll = unpack("C", substr($gseg,$at,1));
        my ($lx, $ly, $cx, $cy, $mh, $mfr, $start_angle, $sweep_angle) = unpack("(s>)4CCs>x2s>x2", substr($gseg, $at+1, 18));
        my $radius = $arc_parameters[0]* ($mh > 0 ? $mh+$mfr/$mh : 1);
        my $end_angle = $start_angle + $sweep_angle; 
        $ps->add(
         $inarea 
          ? "$lx $ly moveto $cx $cy $arc_parameters[0] $start_angle $end_angle arc " 
          : "$lx $ly moveto newpath $cx $cy $arc_parameters[0] $start_angle $end_angle arc closepath lstroke " 
        );  
        #print "==> $order ($lx, $ly, $cx, $cy, $mh, $mfr, $start_angle, $sweep_angle) ", unpack("H*",substr($gseg, $at+1, 18)) ," <==\n";
      }
      else {
        die "EEEEE $order // $at // $gseglen LLLL\n";
      }
    }
    continue {
      $at += $ll + 1;
    }

    $gda = substr($gda, $gseglen);
  }

  $ps->add("restore");

  $tGDA = ''; @GDA = (); @GDD = (); @OBP = (); @OBD = ();
};

sub d3a8a7 { my $self = shift;  my $trd = $_[0]; # Begin Document Index (BDI)
  my $INPUT = $self->{INPUT}; my $line = ''; my $atLine = 0;
  print time(), " SKIPPING INDEX DATA BEGIN.\n";
  while(1) {
    $line = $INPUT->getLine(); $atLine += 1;
    last if $line eq '' || substr($line,3,3) eq "\xd3\xa9\xa7";
    print " NOW atLine $atLine\n" if $atLine % 10000 == 0;
  }
  die("UserError: END OF INDEX NOT FOUND (BROKEN FILE ????)") if $line eq '';
  print time(), " SKIPPING INDEX DATA END.\n";
};

sub d3a9c6 { my $self = shift;  my $trd = $_[0]; # End Resource group
};

sub d3a8c6 { my $self = shift;  my $trd = $_[0]; # Begin Resource group
  %MediumMapsTable = (); $Duplexing = 0; $DuplexingPages = 0;
};

sub d3a8ce { my $self = shift;  my $trd = $_[0]; # Begin Resource (BR)
 my $INPUT = $self->{'INPUT'};  
  $ResourceName = rtrim($main::translator->toascii(substr($trd,0,8)));
  $ps->open("$ResourceName");
  my $t21 = (grep(/^\x21/, $self->ParseTripletGroup(substr($trd,10))))[0];
  my $ObjType = substr($t21,1,1);
  if ( $ObjType =~ /[\x40-\x42]/ ) {
    $FONTs->TranslateFont($ResourceName);
  }
  else {
    if ( $ObjType eq "\xfd" ) { #pagedef
      $PageDef = $ResourceName;
    }
    elsif ( $ObjType eq "\xfe" ) { #formdef
      $FormDef = $ResourceName; %MediumMapsTable = (); $Duplexing = 0; $DuplexingPages = 0;
    }
  }
  #print $OUTPUT "$ResourceName\n";
};

sub d3a9ce { my $self = shift;  my $trd = $_[0]; # End Resource (BR)
  #print "END RESOURCE\n";
  $ps->close();
  $ResourceName = "";
};

sub d3a090 { my $self = shift;  my $trd = $_[0]; # Tag Logical Element (TLE)
 

  my @triplets = $self->ParseTripletGroup($trd); my ($t, $tag, $value);

  for my $t (@triplets) {
    if ( substr($t,0,1) eq "\x02" and $tag eq '') {
      $tag = $main::translator->toascii(substr($t,3)); $tag =~ s/ +$//;
    }
    ### --------------------------------------------------------- ###
    elsif ( substr($t,0,1) eq "\x36" ) {
      $value = $main::translator->toascii(substr($t,3)); $value =~ s/[\x00 ]+$//;
    }
  }
  $TLE->{$tag} = $value if $tag ne '';
};

#------------------------------------------------------------
# Low level routines

sub readAfpBlk {
  my $self = shift; my $INPUT = $self->{INPUT};

  ($r5atype, $r5adata) = ();

  return undef if $INPUT->eof();
  
  my $line = $INPUT->getLine();
  Carp::croak("UserError: FIRST CHAR NOT 5A \"".unpack("H40", $line)."\" at ". $INPUT->tell() . ' INPUT REF: ', ref($INPUT->{getLine}))
        unless $line =~ /^\x5a/; 
  my ($c5A, $r5A, my $z) = unpack('a n @1 /a a*', $line);
  my ($l5A, $t5A, $trd) = unpack('n H6 x3 a*', $r5A);

  my $b5A = $l5A - 2;
  ($r5atype, $r5adata) = ($t5A, $trd);
  #my ($c5A, $b5A, $r5A) = unpack("ana*", $line); $b5A-=2;

  #print $OUTPUT "$r5atype $TRIPLETS{$r5atype} ($b5A)\n";
  #print $OUTPUT ">> ", unpack("H*", $r5adata), "\n";
  
  #  croak("UserError: CONTROL CHAR NOT 5A \"".unpack("H2", $c5A)."\" at ". $INPUT->tell().".")
  #   if 
  #  $c5A ne "\x5a";
  #
  #  $r5atype = unpack("H6",substr($r5A,0,3));
  #  $r5adata = substr($r5A, 6, $b5A-6);
  $main::veryverbose && i::warnit(__PACKAGE__."::readAfpBlk - AFP REC: $r5atype - DATA: ", unpack('H60', $r5adata));
  

  if ($r5atype ne "d3ee9b" and $in_ptoca_seqs) {
    $ps->add("a.terminate.ptocaseqs"); $in_ptoca_seqs = 0;
  }
  return ($t5A, $trd);

  return $b5A;
}


sub ExpandResourceHandle {
    my ($self, $INPUT) = @_;
    
  #$ps = XReport::AFP::PRINTPS->new();
   $ps = XReport::AFP::PRINTPS->new(@_);  
   $FONTs = XReport::AFP::FONTs->new($INPUT, $ps, $main::translator);

  while(!$INPUT->eof()) {
    $INPUT->read(my $rechdr, 9);
    my ($c5A, $l5A, $t5A, $f5a) = unpack('anH6H6', $rechdr);
    $INPUT->read(my $trd, ($l5A - 8));
    my $cref = $self->can($r5atype);
    &$cref($self, $trd) if $cref
  }

  
  my $resused = join(", ", sort(keys(%$USED_RESOURCES))); $USED_RESOURCES = undef;
  $main::debug && i::logit("Resource $FileName refers to $resused");
  
  return ($resused, $ps->PsLines());
}

#------------------------------------------------------------
# Create resource library files

sub ExpandResourceFile {
  $FileName = shift; my ($self, $XferMode) = ({}, 2); local $_;

  $USED_RESOURCES = {};
  
  use XReport::INPUT;
  
  my $INPUT = ( $FileName =~ /\.gz$/ ) 
   ? XReport::INPUT::IZlib->new($FileName)
   : XReport::INPUT::IPlain->new($FileName)
  ;
  $INPUT = XReport::INPUT::ILine->new($INPUT, $XferMode); 

  $self->{INPUT} = $INPUT; bless $self;

  $ResourceName = File::Basename::basename($FileName);
  #$ps = XReport::AFP::PRINTPS->new();
  $ps = XReport::AFP::PRINTPS->new(@_);  
  $FONTs = XReport::AFP::FONTs->new($self, $ps, $main::translator);
  $INPUT->Open();
  
  while(!$INPUT->eof()) {
    ($r5atype, $r5adata) = $self->readAfpBlk();
    last if !$r5atype;
    $self->ExpandAfp($r5atype, $r5adata);
  }

  $INPUT->Close();
  
  my $resused = join(", ", sort(keys(%$USED_RESOURCES))); $USED_RESOURCES = undef;
  $main::debug && i::logit("Resource $FileName refers to $resused");
  return ($resused, $ps->PsLines());
}

#------------------------------------------------------------
# Interface for an Input/Output Stream

sub BuildPage {
  my $self = shift; $self->{atPage} += 1;
  
  my $page = {}; bless $page, 'XReport::AFP::IOStream';
  
  @{$page}{qw(TextLines overflowed)} = ([], 0);

  $page->{atPage} = $self->{atPage};

  if ($extract_text) {
    for my $TextOr (sort(keys(%{$self->{TextLines}}))) {
      my $aref = $self->{TextLines}->{$TextOr};
      for (@$aref) {
        $page->appendLine("$ROTATE_CC{$TextOr}.$_");
      }
    }
    #print TEXTLINES "?? -> ".$page->{atPage}."\n";
    #print TEXTLINES join("\n", map {$_->AsciiValue()} @{$page->{TextLines}}), "\n";
  }
  
  $page->{PsLines} = $ps->{PsLines}; return $page;
}

sub BuildEmptyPage {
  my $self = shift; $ps->PsLines([]);
  $ps->add(
    "{"
   ,"}"
   ,"$PGD[4] $PGD[5] a.BeginPage" 
   ,""
   ,"(0000) (2d00) a.SetMatrix"
  ); 
  $ps->add("a.EndPage\n\n"); return $self->BuildPage;
}

sub GetAttributes {
  my ($self, $elName) = (shift, shift); my @r = ();

  return unless $elName eq "TLE" and defined($TLE) and keys(%$TLE);

  for (@_) {
    return () if !exists($TLE->{$_});
    push @r, $TLE->{$_};
  }

  return wantarray ? @r : $r[0];
}

sub overflowed {
  my $self = shift; return $self->{overflowed} if !scalar(@_); $self->{overflowed} = shift;
}

sub totLines {
  my $self = shift; my $page = $self->{TextLines};
  return scalar(@$page);
}

sub atPage {
  my $self = shift;
  return $self->{atPage};
}

sub atLine {
  my $self = shift;
  return $self->{atLine};
}

sub atOffset {
  my $self = shift;
  return $self->{atOffset};
}

sub appendLine {
  my $self = shift; my $page = $self->{TextLines};
  push @$page, XReport::INPUT::RasterLine->new(@_, scalar(@$page)+1);
}

sub repLastLine {
  my $self = shift; my $page = $self->{TextLines};
  $page->[$#$page] = XReport::INPUT::RasterLine->new(@_, scalar(@$page));
}

sub lineList {
  my $self = shift;
  return $self->{TextLines};
}

sub rawlineList {
  my $self = shift;
  return $self->{TextLines};
}

sub getLine {
  return &{$_[0]->{getLine}}($_[0]);
}

sub GetPage {
  my $self = shift; my ($INPUT, $blocksList) = @$self{qw(INPUT blocksList)}; return undef if $INPUT->eof();

  $PAGE_ENDED = 0; $ps->PsLines([]); $TLE = undef; 

  while(1) {
    ($r5atype, $r5adata) = (!@$blocksList ? $INPUT->readAfpBlk() : splice(@$blocksList,0,2)); last if !$r5atype;

    $self->ExpandAfp($r5atype, $r5adata); last if $PAGE_ENDED;

    if ($r5atype eq "d3abcc") {
      my $MediumMapName = $main::translator->toascii(substr($r5adata,0,8)); $MediumMapName =~ s/ +$//;
      if ($Duplexing and $DuplexingPages % 2 == 1) { 
        $Duplexing = $MediumMapsTable{$MediumMapName}->{Duplexing}; $DuplexingPages = 0;

        push @$blocksList, ($r5atype, $r5adata); return $self->BuildEmptyPage();
      }
      $Duplexing = $MediumMapsTable{$MediumMapName}->{Duplexing}; $DuplexingPages = 0;
    }
  }

  if ($r5atype eq "d3a9af") {
    $DuplexingPages += 1 if $Duplexing; return $self->BuildPage() 
  }
  else {
    if ($Duplexing and $DuplexingPages % 2 == 1) {
      %MediumMapsTable = (); $Duplexing = 0; $DuplexingPages = 0;
      return $self->BuildEmptyPage();
    } 
    # bypass last records
  }
}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};

  $fileType = "Document";
  warn ref($self), " open ", ref($INPUT), sprintf(" as requested by %s at %s", (caller())[0,2]), "\n" if $main::debug;

  $INPUT->Open(@_);

  $self->{atPage} = 0; $doc_ended = 0;
}

sub Close {
  my ($self, %args) = @_; my $INPUT = $self->{INPUT};
  warn ref($self), " close ", ref($INPUT), sprintf(" as requested by %s at %s", (caller())[0,2]), "\n" if $main::debug;
  
  #close(TEXTITEMS); #close(TEXTLINES);
  
  if ( $args{finalize} and $fileType eq "Document" and !$doc_ended ) {
    #die("END OF DOCUMENT (field d3a9a8) NOT DETECTED");
	warn("END OF DOCUMENT (field d3a9a8) NOT DETECTED");
  }
  $doc_ended = 0;

  $ps->close(); $ps->open("\$\$initafp"); $FONTs->czmap(); $ps->close(); return $INPUT->Close(@_);
}

sub SetElabOptions {
  my ($self, %largs) = @_; $extract_text = $largs{ExtractText};
}

sub new {
  my ($className, $self) = @_; my ($jr, $INPUT) = @{$self}{qw(jr INPUT)};
#  my $self = {};
  $INPUT = XReport::AFP::INPUTStream->new($INPUT); $self->{'INPUT'} = $INPUT;

  $INPUT->Open(); $trcFlag = $jr->getValues('Trc') =~ /[1y]/i;

  $extract_text = 1; $text_as_comment = 1; 
  
  $text_is_ascii = 0;

  my $localres = $jr->getFileName('localres');
  -d $localres or mkpath($localres)
    or die("ApplicationError: UNABLE TO CREATE LOCALRES \"$localres\" DIRECTORY $!");
  
  $ps = XReport::AFP::PRINTPS->new($localres); #$UserExit = XReport::AFP::UserExit->new();

  #open(TEXTLINES, ">c:/temp/textlines.txt"); #open(TEXTITEMS, ">c:/temp/textitems.txt");

  $FONTs = XReport::AFP::FONTs->new($INPUT, $ps, $main::translator);

  @{$self}{qw(atPage lprogr barcode4jTable blocksList)} = (0, 0, {}, []); 
  $fileType = "Document"; 
  bless $self, $className;
  return $self;
}

1;



__END__

SELECT     
  j.JobReportName AS JobReportName, j.RemoteFileName AS RemoteFileName, a.*
FROM         
  dbo.tbl_IDX_PRIVATE_DEPOSITI a INNER JOIN u0xreport.dbo.tbl_JobReports j ON j.JobReportId = a.JobReportId
  
WHERE     (
  a.logicallydelete = 0
  
  AND EXISTS (
    SELECT b.jobreportid
      FROM  tbl_IDX_PRIVATE_DEPOSITI b INNER JOIN u0xreport.dbo.tbl_JobReports k ON k.JobReportId = b.JobReportId
      
    WHERE      
      b.logicallydeleted = 0
      AND b.jobreportid <> a.jobreportid 
      AND a.filiale = b.filiale AND b.deposito = a.deposito AND b.data_rif = a.data_rif
))

order by a.FILIALE, a.DEPOSITO, b.DATA_RIF


