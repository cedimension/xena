
use strict;

#------------------------------------------------------------
package XReport::AFP::Encoder;

use strict;

use Convert::IBM390 qw(packeb);

sub _5a {
  return "\x5a". pack("n", length($_[0])+2). $_[0]; 
}

sub _catll1 {
  my $tll = '';
  for (@_) {
	$tll .= pack("C", length($_)+1) . $_;
  }
  return $tll;
}

sub _catll2 {
  my $tll = '';
  for (@_) {
	$tll .= pack("n", length($_)+2) . $_;
  }
  return $tll;
}

# progressivo di sf

#------------------------------------------------------------
# resource group
#------------------------------------------------------------

my %iResourceTypes = (
  Graphics    => "\x03",
  BarCode     => "\x05",
  Image       => "\x06",
  CharSet     => "\x40",
  CodePage    => "\x41",
  CodedFont   => "\x42", 
  Container   => "\x92",
  Document    => "\xa8",
  PageSegment => "\xfb",
  PageOverlay => "\xfc",
  PageDef     => "\xfd",
  FormDef     => "\xfe" 
);

#-- begin resource group
sub brg {
  my $self = shift;
  return _5a(
	pack("H12H16", "d3a8c6000000", "ffffffffffffffff")
  );
}

#-- end resource group
sub erg {
  my $self = shift;
  return _5a(
	pack("H12H16", "d3a9c6000000", "ffffffffffffffff")
  );
}

#-- begin resource
sub br {
  my ($self, $restype, $resid) = (shift, shift, shift); 
  $restype = $iResourceTypes{$restype} 
    or
  die("UNKNOWN Resource Type DETECTED $restype FOR RESOURCE $resid");
  return _5a(
	packeb("H12E8H4", "d3a8ce000000", $resid, "0000").
    pack("Caaa*", 10, "\x21", $restype, "\x00"x7)
  );
}

#-- end resource
sub er {
  my ($self, $resName) = @_;
  return _5a(
	pack("H12a8", "d3a9ce000000", "\xff"x8)
  );
}

#-- begin overlay
sub bmo {
  my ($self, $oid) = @_; return _5a(packeb("H12E8", "d3a8df000000", $oid));
}

#-- end overlay
sub emo {
  my ($self, $oid) = @_; return _5a(pack("H12a8", "d3a9df000000", "\xff"x8));
}

#-- begin page segment
sub bps {
  my ($self, $psid) = @_; return _5a(packeb("H12E8", "d3a85f000000", $psid));
}

#-- end page segment
sub eps {
  my ($self, $ps) = @_; return _5a(pack("H12a8", "d3a95f000000", "\xff"x8));
}

#------------------------------------------------------------
# font group /to complete/ see FfbFont.pm
#------------------------------------------------------------

#-- begin font
sub bfn {
  my ($self, $fontid) = @_;
  return _5a(
	packeb("H12E8", "d3a889000000", $fontid)
  );
}

#-- font descriptor
sub fnd {
  my ($self, $aname, $weight_class, $width_class, $csgid, $ftgid) = @_;
  return _5a(
	packeb("H12E32", "d3a689000000", $aname) .
	pack(
	 "C2n6C3a15a2a10n2",
	  $weight_class, $width_class,
     #... all zero for outline font
      0,  # Maximum Vertical Size  10th of a point
      0,  # Nominal Vertical Size  10th of a point
      0,  # Minimum Vertical Size  10th of a point
      0,  # Maximum Horizontal Size 1440th (of an Inch ??)
      0,  # Nominal Horizontal Size 1440th (of an Inch ??)
      0,  # Minimum Horizontal Size 1440th (of an Inch ??) 
     #... all zero (no ISO)
      0,  # Class ISO
      0,  # SubClass ISO
      0,  # Specific Group ISO
	 #---------------------------------------
     "\x00"x15,  # Reserved
     "\x00"x2,   # Design Flags
     "\x00"x10,  # Reserved
     #... where to find ???------------
      0,  # Graphic Character Set GID --> ??
      0   # Font Typeface GID --> ??
    )
  );
}

#-- font control
sub fnc {
  my ($self, $datLen, $fnn_dc, $fnn_nc) = @_;
  return _5a(
	pack("H12", "d3a789000000") .
	pack(
	 "a6n2n2CCaa3CCa2a2Na3CNn", 
	  "\x01",   # retired (as is)
      "\x1f",   # pfb type1
      "\x00",   # reserved (as is)
      "\x00",   # FntFlags (?? as is)
      "\x02",   # 02 relative metrics as is
      "\x02",   # 02 relative metrics
       1000,    # 1000 units per Em)
       1000,    # 1000 units per Em)
       0,       # MaxBoxWd ?? as is ??)
       0,       # MaxBoxHt ?? as is ??)
       26,      # FNO repeating group length
       28,      # FNI repeating group length
      "\x00",   # PatAlign (00 1 byte align)
      "\x00"x3, # Raster Pattern Data Count -> 0 for pfb (no raster)
       16,      # FNP Repeating group length 
        0,      # FNM Repeating group length (0 No Raster Data)
      "\x00"x2, # Shape resolution (0000 no resolution provided)
      "\x00"x2, # Shape resolution (0000 no resolution provided)
       $datLen, # Outline Pattern Data Count (pfb file length+17)
	  "\x00"x3, # Reserved  
	  #---- Font Name Map (FNN) data
       12,      # FNN Repeating Group Count 
       $fnn_dc, # FNN Data Count 
       $fnn_nc  # FNN Name Count
    )
  );
}

#-- font orientation
sub fno {
  my ($self, $body) = @_;
  return _5a(
	pack("H12", "d3ae89000000") . $body
  );
}

#-- font position
sub fnp {
  my ($self, $body) = @_; return _5a( pack("H12", "d3ac89000000") . $body);
}

#-- font index
sub fni {
  my ($self, $body) = @_; return _5a( pack("H12", "d3ac89000000") . $body);
}

#-- font name map
sub fnn {
  my ($self, $body) = @_; return _5a( pack("H12", "d3ab89000000") . $body);
}
	
#-- end font
sub efn {
  my ($self, $fontid) = @_; return _5a( pack("H12a8", "d3a989000000", "\xff"x8));
}

#------------------------------------------------------------
# code page group
#------------------------------------------------------------

#-- begin code page
sub bcp {
  my ($self, $cpid) = @_;
  return _5a(
	packeb("H12E8", "d3a887000000", $cpid)
  );
}

#-- code page descriptor
sub cpd {
  my ($self, $descr, $chars) = @_;
  return _5a(
	packeb("H12E32m4m2m2", "d3a687000000", $descr, $chars, 0, 0)
  );
}

#-- code page control
sub cpc {
  my ($self, $defChar, $prtFlags, $cpFlags) = @_;
  
  $defChar ||= " "; $prtFlags ||= "\x80"; $cpFlags ||= "\x08";

  return _5a(
	pack("H12a8H10", "d3a787000000", packeb("E8", "SP010000"), "800a202008")
  );

}

#-- code page index
sub cpi {
  my ($self, $cpList) = @_;

  return _5a(
	pack("H12a*", "d38c87000000", $cpList)
  );

}

#-- end code page
sub ecp {
  my ($self, $cpname) = @_;
  return _5a(
	pack("H12a8", "d3a987000000", "\xff"x8)
  );
}

#------------------------------------------------------------
# form/medium_map group
#------------------------------------------------------------

#-- begin form map
sub bfm {
  my ($self, $formid) = @_; return _5a(packeb("H12E8", "d3a8cd000000", $formid));
}

#-- begin medium map
sub bmm {
  my ($self, $medid) = @_; return _5a(packeb("H12E8", "d3a8cc000000", $medid));
}

#-- begin form environment group (obsolete)
sub bfg {
  my ($self, $eid) = @_; return _5a(packeb("H12E8", "d3a8c5000000", $eid));
}

sub mmo {
my $t = <<EOF;
d3b1df Map Medium Overlay (22)
>> 0c00000001000000d6f1f0f5f5f4f7f0
0c length of repeating groups
000000 reserved
... repeating group
01 local identifier
000000 Reserved
d6f1f0f5f5f4f7f0 External Name
0c00000001000000O1055470
O1055470
d3b1df Map Medium Overlay (34)
>> 0c00000001000000d6f1f0f5f5f4f7f002000000d6f1f0f5f5f4f7f1
0c0000001000000O105547002000000O1055471
O1055470,O1055471
EOF
}

#-- page position
sub pgp2 {
  my ($self, @pgp2) = @_; my $pgp2 = pack("H12H2", "d3b1af000000", "01");
  
  @pgp2 = ("0000", "00", "0000", "01") if !@pgp2;
  
  while(@pgp2) {
	my ($angle, $side) = splice(@pgp2,0,2);
	$pgp2 .= packeb("m1m3m3H4H2", 10, 0, 0, $angle, $side);
  }

  return _5a($pgp2);
}


#-- medium descriptor
sub mdd {
  return _5a(
    pack("H*", "d3a688000000") .
    packeb("H2H2 m2m2 m3m3 H2", "00", "00", 14400, 14400, 1984*6, 2806*6, "00") .
    _catll1(
	  pack("H2H2", "68", "00") #rotation
    )		
  );
}

sub mcc {
  my $self = shift;
  return _5a(
	pack("H*", "d3a288000000") .
    pack("H*H*", "000100010001", "000200020002")
  );
}

sub mmc {
  my ($self, $side, $bin) = @_; $side ||= 1; $bin ||= 1; $bin = substr("0$bin", -2);
  return _5a(
	pack("H*", "d3a788000000") .
	pack("H*", 
	 ($side == 1
	  ? "01ff0e00d100e1$bin"."f100f402f8ff"
	  : "02ff0e00d100e1$bin"."f100f402f8ff"
	 )
    )
  );
}

#-- end form environment group (obsolete)
sub efg {
  my ($self, $eid) = @_; return _5a( pack("H12a8", "d3a9c5000000", "\xff"x8));
}

#-- end medium map
sub emm {
  my ($self, $medid) = @_; return _5a( pack("H12a8", "d3a9cc000000", "\xff"x8));
}

#-- end form map
sub efm {
  my ($self, $formid) = @_; return _5a(pack("H12a8", "d3a9cd000000", "\xff"x8));
}

#------------------------------------------------------------
# document group
#------------------------------------------------------------

#-- begin document environment group
sub bdg {
  my ($self, $eid) = @_; return _5a(pack("H12E8", "d3a8c4000000", $eid));
}

#-- map suppression
sub msu {
my $t = <<EOF;
d3abea Map Suppression (26)
>> d9c9c7c1f1404040ff01d9c9c7c1f1c24040ff02
RIGA1    ff 10
RIGA1B   ff 20
EOF
}

#-- end document environment group ----------
sub edg {
  my ($self, $id) = @_; return _5a(pack("H12a8", "d3a9c4000000", "\xff"x8));
}

#-- begin document ----------
sub bdt {
  my ($self, $docid) = (shift, shift); $docid ||= "AFPDOC";
  return _5a(
    packeb("H12E8H4", "d3a8a8000000", $docid, "0000") .
     #code page global identifier
    _catll1(pack("H2H4H4", "01", "ffff", "0118")) .
	 # function set specifier. required ????
    pack(
     "CH2H2H2H4H4"x4, 
      8,"21", "02", "00", "8000", "0000", 
      8,"21", "03", "00", "8000", "4000", 
      8,"21", "05", "00", "8000", "0000", 
      8,"21", "06", "00", "8000", "8000"
    ). 
	_catll1(packeb("H2e*", "65", "TEST GENERAZIONE DOCUMENTO AFP")) 
  );
}

#-- end document ----------
sub edt {
  my $self = shift; return _5a(pack("H12a8", "d3a9a8000000", "\xff"x8));
}

#-- begin page group ----------
sub bng {
  my ($self, $groupid) = (shift, shift); $groupid ||= "PGROUP";
  return _5a(
    packeb("H12E8", "d3a8ad000000", $groupid) .
    _catll1(
      packeb("H2H2H2e*", "02", "01", "00", pack("H*","d9c5d7f0f0f2f0f0f0f0f0f0f0f1")),
      packeb("H2H2H2e*", "02", "8d", "00", pack("H*", "c3d6e7f1f0f1f0f5"))
	)
  );
}

#-- tag logical element ----------
sub tle {
  my ($self, $tagKey, $tagValue) = (shift, shift, shift);
  return _5a(
    pack("H*", "d3a090000000") .
    _catll1(
	  packeb("H2H2H2e*", "02", "0b", "00", $tagKey),
      packeb("H2H4e*", "36", "0000", $tagValue),
      pack("H2H8H8", "00000000", "7fffffff")
	)
  );
}

#-- end page group
sub eng {
  my $self = shift;
  return _5a(
	pack("H12H16", "d3a8ad000000", "ffffffffffffffff")
  );
}

#-- invoke medium map
sub imm {
  my ($self, $imid) = (shift, shift);
  return _5a(
	packeb("H12E8", "d3abcc000000", $imid)
  );
}

#-- begin page
sub bgp {
  my ($self, $pageid) = (shift, shift); $pageid ||= "PAGE";
  return _5a(
	packeb("H12E8", "d3a8af000000", $pageid)
  );
}

#-- end page
sub epg {
  my $self = shift;
  return _5a(
	pack("H12H16", "d3a9af000000", "ffffffffffffffff")
  );
}
  
#-- begin active environment group
sub bae {
  my $self = shift;
  return _5a(
	pack("H12", "d3a8c9000000")
  );
}
  
#-- map coded font 2
sub mcf2 {
  my ($self, $FontLid, $FontName, $YSize, $XSize) = @_; 
  return _5a(
    pack("H*", "d3ab8a000000") . _catll2(
    _catll1(
      pack("H2H2H2a8", "02", "85", "00", packeb("E8", "T1001252")),
      pack("H2H2H2a8", "02", "86", "00", packeb("E8",  $FontName)),
      pack("H2H2", "25", "00"),
      pack("H2H4", "26", "0000"),
      pack("H2H2C", "24", "05", $FontLid),
	  pack("H2H2H2nnH2H20H2", "1f", "05", "05", $YSize, 0, "00", "00000000000000000000", "C0"), pack("H2n", "5d", $XSize)
    ))
  );
}

#-- map page overlay
sub mpo {
  my ($self, $OverlayLid, $OverlayName) = @_; 
  return _5a(
    pack("H*", "d3abd8000000") . _catll2(
    _catll1(
      pack("H2H2H2a8", "02", "84", "00", packeb("E8",  $OverlayName)),
      pack("H2H2C", "24", "02", $OverlayLid)
    ))
  );
}

#-- page descriptor
sub pgd {
  my $self = shift;
  return _5a(
    pack("H*", "d3a6af000000") .
    packeb("H2H2 m2m2 m3m3 H6", "00", "00", 14400, 14400, 1984*6, 2806*6, "000000")
  );
}

#-- presentation text descriptor
sub ptd {
  my $self = shift;
  return _5a(
    pack("H12", "d3b19b000000") . 
    packeb("H2H2 m2m2 m3m3 H4", "00", "00", 14400, 14400, 1984*6, 2806*6, "0000")
  );
}
  
#-- end active environment group
sub eae {
  my $self = shift;
  return _5a(
	pack("H12", "d3a9c9000000")
  ); 
}

#-- begin image
sub bii {
  my ($self, $imid) = @_; return _5a(packeb("H12E8", "d3a87b000000", $imid));
}

#-- image output control
sub ioc {
  my ($self, $atX, $atY) = @_;
  return _5a(
    pack("H12", "d3a77b000000") . 
    packeb("m3m3H4H4H16H4H4H4", $atX, $atY, "0000", "2d00", "0000000000000000", "03e8", "03e8", "ffff")
  );
}

#-- image input descriptor
sub iid {
  my ($self, $cX, $cY, $lX, $lY, $dX, $dY) = @_;
  return _5a(
    pack("H12", "d3a67b000000") .
	pack(
	 "H24H2H2nnnnH*nnH4H4",
     "000009600960000000000000", "00",  "00", # as is  
      $cX,   # m2 image points
      $cY,   # m2 image points
      $lX,   # m2 x image size (here cell)
      $lY,   # m2 y image size (here cell)
     "000000002d00", # as is
      $dX,   # m2 x default image size (zero for not cell)
      $dY,   # m2 y default image size (zero for not cell)  
     "0001", # as is
     "0000"  # color
    )
  );
}

#-- image cell position
sub icp {
  my ($self, $atX, $atY, $cX, $cY, $lX, $lY) = @_; 
  return _5a(pack("H12n6", "d3ac7b000000", $atX, $atY, $cX, $cY, $lX, $lY));
}

#-- image raster data
sub ird {
  my $self = shift; return _5a(pack("H12a*", "d3ee7b000000", $_[0]));
}

#-- end image
sub eii {
  my $self = shift; return _5a(pack("H12a8", "d3a97b000000", "\xff"x8));
}

#-- begin graphics
sub bgr {
  my ($self, $grid) = @_; return _5a(packeb("H12E8", "d3a8bb000000", $grid));
}

#-- begin image object
sub bim {
  my ($self, $imid) = @_; return _5a(packeb("H12E8", "d3a8fb000000", $imid));
}
 
#-- begin object environment group
sub bog {
  my ($self, $eid) = @_; return _5a(packeb("H12E8", "d3a8c7000000", $eid));
}

#-- object area descriptor
sub obd {
  my ($self, $lX, $lY) = @_;
  return _5a(
    pack("H*", "d3a66b000000") .
    _catll1(
	  pack("H2H2", "43", "01"),
      pack("H2H2H2H4H4", "4b", "00", "00", "3840", "3840"),
      packeb("H2H2M3M3", "4c", "02", $lX, $lY)
	)
  );
}

#-- object area position
sub obp {
  my ($self, $cX, $cY, $lX, $lY) = @_;
  return _5a(
    pack("H*", "d3ac6b000000") .
	packeb(
	 "m1m1m3m3H4H4H2m3m3H4H4H2",
	  1, 23,                      # area id + len (23)
	  $cX, $cY, "0000", "2d00", "00", # area position
	  $lX, $lY, "0000", "2d00", "00"  # content position
    )
  )
}

#-- map graphics object
sub mgo {
  my $self = shift;
  return _5a(
    pack("H*", "d3abbb000000") . _catll2(
	_catll1(
	  pack("H2H2", "04", "60")   # 10 position and trim 20 scale to fit
	)
  ));
}

#-- map io image object
sub mio {
  my $self = shift;
  return _5a(
    pack("H*", "d3abfb000000") . _catll2(
	_catll1(
	  pack("H2H2", "04", "20")   # scale to fit
	)
  ));
}

#-- graphics data descriptor
sub gdd {
  my ($self, $cX, $cY, $l1X, $l2X, $l1Y, $l2Y) = @_;
  return _5a(
    pack("H*", "d3a6bb000000") .
	pack("H*", "F707B0000002000100"). 
	pack(
	 "H2H2H2H2H2H2n2H4n4", 
	 "F6",                         # window specification 
	 "12",                         # section length
	 "40",                         # absolute measures
	 "00", "00", "00",             # reserved + 2 byte integers and tens of inch
	  $cX, $cY, "0000",            # (x y units x 10 inches, IMXYRES)
	  $l1X, $l2X, $l1Y, $l2Y       # (left right X) (bottom top Y) + reserved
    ) 
  );
}

#-- image data descriptor
sub idd {
  my ($self, $cX, $cY, $lX, $lY, $algExt) = @_;
  return _5a(
    pack("H*", "d3a6fb000000") .
	pack("H2n4", "00", $cX, $cY, $lX, $lY) . $algExt 
  );
}

#-- end object environment group
sub eog {
  my $self = shift; return _5a(pack("H12a8", "d3a9c7000000", "\xff"x8));
}

#-- graphics data
sub gad {
  my ($self, $gad) = @_; return _5a(packeb("H12c*", "d3eebb000000", $gad));
}

#-- image picture data io
sub ipd {
  my ($self, $cX, $cY, $lX, $lY, $cmpr, $extp) = splice(@_,0,6);
  # test image here or in the caller ????
  my ($lrest, $ipdio, $at, $lseg) = (length($_[0]), '', 0, 0);
  while($lrest > 0) {
	$lseg = ($lrest > 16672 ? 16672 : $lrest); 
	$ipdio .= _5a(
      pack("H*", "d3eefb000000") .
      pack("H4n", "fe92", $lseg) . 
	  substr($_[0], $at, $lseg) 
    );
	$at += $lseg; $lrest -= $lseg;
  }
  
  return (
    _5a(
      pack("H*", "d3eefb000000") .
  	  pack(
  	    "H4H6H4H2n4H4H4",
  	    "7000",                              # begin segment
        "9101ff",                            # begin image content (ff = IOCA as is)
        "9409", "00", $cX, $cY, $lX, $lY,    # image size parameter
  	    "9502", $cmpr."01",                  # compression
	  )
    ).
	$ipdio .
    _5a(
      pack("H*", "d3eefb000000") .
  	  pack(
        "H4H4",
        "9300", # end image content
        "7100", # end segment
	  )
    ) 
  );
}

#-- end graphics
sub egr {
  my $self = shift; 
  return _5a(pack("H12a8", "d3a9bb000000", "\xff"x8));
}

#-- end image object
sub eim {
  my $self = shift; 
  return _5a(pack("H12a8", "d3a9fb000000", "\xff"x8));
}

#-- include page segment
sub ips {
  my ($self, $ps, $atX, $atY) = @_; 
  return _5a(packeb("H12E8m3m3", "d3af5f000000", $ps, $atX, $atY));
}

#-- include page overlay
sub ipo {
  my ($self, $po, $atX, $atY, $orient) = @_; 
  return _5a(packeb("H12E8m3m3c2", "d3afd8000000", $po, $atX, $atY, "\x00"x2 ));
}

#-- begin presentation text
sub bpt {
  my ($self, $txtid) = @_; $txtid ||= "TEXTAREA"; 
  return _5a(packeb("H12E8", "d3a89b000000", $txtid));
}

#-- presentation text data
sub ptx {
  my $self = shift; 
  return _5a(pack("H12a*a2", "d3ee9b000000", $_[0], "\x02\xf8"));
}

#-- end presentation text
sub ept {
  my $self = shift; 
  return _5a(pack("H12a8", "d3a99b000000", "\xff"x8));
}


sub new {
  my $ClassName = shift; my $self = {};

  bless $self, $ClassName;
}


#-- end presentation text
sub nop {
  my ($self, $text, $TRANSPARENT) = @_; 
  return $TRANSPARENT
    ? _5a(pack("H12a*", "d3eeee000000", $text)) 
    : _5a(packeb("H12E*", "d3eeee000000", $text))
  ;
}

#------------------------------------------------------------
package XReport::AFP::Util;

use strict;

use Convert::IBM390 qw(packeb);

sub creCodePage {
  my ($cpname, $cpdescr, $charList) = @_; require XReport::AFP::CharsetMap;
  
  my $adobe2ibm = XReport::AFP::CharsetMap::adobe2ibm(); 
  my ($achar, $ichar, $charid) = ('', '', -1);
  my ($cpList, @cpList, %cpList) = (); 
  my ($echar, $enc); 

  $enc = XReport::AFP::Encoder->new();

  for $achar (@$charList) {
	if ( substr($achar,0,1) eq '/' ) {
	  $achar = substr($achar,1);
	}
	$charid += 1; next if $achar eq '.notdef';

	$ichar = $adobe2ibm->{$achar}
	 or
	die("IBM GID NOT DEFINED FOR CHAR $achar"); 
	 
	$echar = packeb("E*", $ichar); $cpList{$charid} = $echar;
  }
  
  $cpList = join("", 
	map {pack("a8C2", $cpList{$_}, 0, $_)} (@cpList = sort {$a <=> $b} keys(%cpList))
  );

  return 
    $enc->bcp($cpname) .  $enc->cpd($cpdescr, scalar(@cpList)) .

    $enc->cpc(" ", "\x80", "\x08") .  $enc->cpi($cpList) .

    $enc->ecp($cpname) 
  ;
}

#------------------------------------------------------------
package XReport::AFP::ResourceManager;

use strict;

use Symbol;

use POSIX qw(ceil);


my $tbl_RequiredResources = {
};

my $tbl_FontNameMap = {
  'ArialMT' => 'CZARIAL',
  'ArialMT-Bold' => 'CZARIALB',
  'ArialMT-Condensed' => 'CZARIALC',
  'Dax-Medium' => 'CZDXMD', 
  'Dax-Bold' => 'CZDXBD',
  'DaxCondensed-Medium' => 'CZDXCN',
};

my $ImagesPath = "c:/euriskom/clienti/unicredit.csf/eps/bilevel";

my $FontsPath = "c:/euriskom/lout.afp";

sub _addStreamResource {
  my ($self, $resType, $resName, $resStream) = @_; my ($enc, $RESFILE) = @{$self}{qw(encoder RESFILE)};

  print $RESFILE $enc->br($resType, $resName), $resStream, $enc->er($resName);
}

sub _addFileResource {
  my ($self, $resType, $resName, $resFile) = @_; my ($enc, $RESFILE) = @{$self}{qw(encoder RESFILE)};

  use XReport::FileUtil; my $resStream = XReport::FileUtil::FileToString($resFile);

  print "RESMANAGER: loading $resType, $resName, $resFile\n";

  print $RESFILE $enc->br($resType, $resName), $resStream, $enc->er($resName);
}

sub _loadImageFile {
  my $ImageFile = shift; require Image::Magick;

  my $iml = Image::Magick->new(); 

  my $message = $iml->Read($ImageFile);

  my ($rc) = $message =~ /(\d+)/;

  $rc < 400 or die("ERROR LOADING IMAGE FILE \"$ImageFile\": $message"); return $iml->[0];
}

sub _loadPageOverlay {
  my ($self, $epoName) = @_; my $INPUT = gensym(); my $ts;

  open($INPUT, "<$epoName.afp") or die "????????????"; binmode($INPUT);
  read($INPUT, $ts, -s "$epoName.afp");
  close($INPUT);

  return $ts;
}

sub _crePageSegment {
  my ($self, $iEpsName, $epsName, $lX, $lY) = @_; my $enc = $self->{'encoder'}; 

  my $blimage = $epsName; $blimage =~ s/^.*\///; $blimage =~ s/\.[^\.]*$//;

  my $bli = _loadImageFile("$ImagesPath/$blimage"."bl.tif"); 

  my ($rows, $cols, $resX, $resY) 
    = 
  $bli->Get(qw(rows columns x-resolution y-resolution)); my $cmpr = 'None';

  my $iblob = substr($bli->ImageToBlob(compression => $cmpr), 8, ceil($cols/8)*$rows);

  my $io = XReport::AFP::IOCAObject->new(); 

  $io->ImageArea(
	iname => $iEpsName, iblob => $iblob, cmpr => $cmpr,
	
	rows => $rows, columns => $cols, 
	resX => $resX*10, resY => $resY*10, 
	
	atX => 0, atY => 0, toWidth => $lX, toHeight => $lY
  );

  my $ts = $enc->bps($iEpsName). $io->getStructuredFields(). $enc->eps(); $io->Close(); return $ts;
}

sub addFont {
  my ($self, $FontName, $FontSize) = @_; my $iFontName; my $Fonts = $self->{'Fonts'};
  
  exists($tbl_FontNameMap->{$FontName})
   or
  die("FONTNAME MAPPING MISSING for $FontName."); 
  
  $iFontName = $tbl_FontNameMap->{$FontName};

  if (!exists($Fonts->{$iFontName}) ) {
    $self->_addFileResource(
	  "CharSet", 
	  "$iFontName", 
	  "$FontsPath/$iFontName"
    ); 
    $Fonts->{$iFontName} = $iFontName;
  }
  
  return $iFontName;
} 

sub addPageOverlay {
  my ($self, $epoName) = @_; my $ts; my $PageOverlays = $self->{'PageOverlays'}; my $epoKEY;

  if (exists($PageOverlays->{$epoKEY = $epoName}) ) {
	return $PageOverlays->{$epoKEY};
  }
  
  $self->_addStreamResource(
	"PageOverlay", 
	 $epoName, 
	 $self->_loadPageOverlay($epoName)
  );

  return $PageOverlays->{$epoKEY} = $epoName; 
}

sub addPageSegment {
  my ($self, $epsName, $lX, $lY) = @_; my $ts; my $PageSegments = $self->{'PageSegments'}; my $epsKEY;

  if (exists($PageSegments->{$epsKEY = "$epsName/$lX/$lY"}) ) {
	return $PageSegments->{$epsKEY};
  }
  
  my $iEpsName = "S1". substr("000000".(scalar(keys(%$PageSegments)+1)), -6);
  
  $self->_addStreamResource(
	"PageSegment", 
	 $iEpsName, 
	 $self->_crePageSegment($iEpsName, $epsName, $lX, $lY)
  );

  return $PageSegments->{$epsKEY} = $iEpsName; 
}

sub Close {
  my $self = shift; my ($enc, $RESFILE) = @{$self}{qw(encoder RESFILE)};

  print $RESFILE $enc->erg(); close($RESFILE); 
  
  %$self = ();
}

sub new {
  my ($ClassName, $FileName) = @_; my ($enc, $RESFILE) = (XReport::AFP::Encoder->new(), gensym());

  open($RESFILE, ">$FileName") 
    or
  die("RESFILE OPEN ERROR For File \"$FileName\" $!"); binmode($RESFILE);
  
  my $self = {
	encoder => $enc, RESFILE => $RESFILE, 
	PathToResources => {
	  FONT => [], OVERLAY => [], PSEG => []
    },
	Fonts => {}, PageSegments => {}
  };
  
  print $RESFILE $enc->brg(); bless $self, $ClassName;
}

#------------------------------------------------------------
package XReport::AFP::FormMap;

sub new {
}

sub getStructuredFields {

}

#------------------------------------------------------------
package XReport::AFP::TextObject;

use strict;

use Convert::IBM390 qw(packeb);

sub _cm {
  return pack("C", length($_[0])+1). $_[0];
}

sub move {
}

sub moveTo {
  my ($self, $toX, $toY) = @_; my $ts = $self->{'ts'};
  $$ts .= packeb("c2m2c2m2", "\x04\xc7", $toX, "\x04\xd3", $toY);
}

sub hmove {
}

sub hmoveTo {
}

sub vmove {
}

sub vmoveTo {
}

sub setColor {
}

sub drawHRule {
  my ($self, $atX, $atY, $lX, $lY) = @_; my $ts = $self->{'ts'};
  $$ts .= 
    packeb("c2m2c2m2", "\x04\xc7", $atX, "\x04\xd3", $atY) .
    packeb("c2m2m2c", "\x07\xe5", $lX, $lY, "\x00") 
  ;
}

sub drawVRule {
  my ($self, $atX, $atY, $lX, $lY) = @_; my $ts = $self->{'ts'};
  $$ts .= 
    packeb("c2m2c2m2", "\x04\xc7", $atX, "\x04\xd3", $atY) .
    packeb("c2m2m2c", "\x07\xe7", $lY, $lX, "\x00") 
  ;
}

sub drawBox {
}

sub addText {
  my ($self, $text) = @_; my $ts = $self->{'ts'};
  $$ts .= pack("Caa*", length($text)+2, "\xdb", $text);
}

sub addTexteb {
  my ($self, $text) = @_; my $ts = $self->{'ts'};
  $$ts .= packeb("m1ce*", length($text)+2, "\xdb", $text); return $self;
}

sub addStream {
  my ($self, $txtStream) = @_; my $ts = $self->{'ts'}; $$ts .= $txtStream; return $self;
}

sub new {
  my ($ClassName, $page) = @_; my $ts = pack("H*", "2bd306f700002d00");

  my $self = {
    page => $page, ts => \$ts, tsList => []
  };

  bless $self, $ClassName;
}

sub getStructuredFields {
  my ($self, $text) = @_; my $ts = $self->{'ts'}; 

  my $enc = XReport::AFP::Encoder->new();
  
  return $enc->bpt() . $enc->ptx($$ts). $enc->ept();
}

sub Close {
  my $self = shift; %$self = ();
}

#------------------------------------------------------------
package XReport::AFP::ImageObject;

use strict;

#0 14 3 13
#8 4 11 7
#2 12 1 15
#10 6 9 5

#00000000060606060202020200000000
#00000000606060600202020200000000

my @dimat8 = qw(
 13 11 12 15  18 20 19 16
  4  3  2  9  27 28 29 22
  5  0  1 10  26 31 30 21
  8  6  7 14  23 25 24 17
  
 18 20 19 16  13 11 12 15
 27 28 29 22   4  3  2  9
 26 31 30 21   5  0  1 10
 23 25 24 17   8  6  7 14
);

# Bayer�s ordered dither matrices (seems not good for printers)

my @odmat4 = qw(
 10  2  8  0
  6 14  4 12
  9  1 11  3
  5 13  7 15
);

my @odmat8 = qw(
 43 11 35  3 41  9 33  1
 27 59 19 51 25 57 17 49
 39  7 47 15 37  5 45 13
 23 55 31 63 21 53 29 61
 40  8 32  0 42 10 34  2
 24 56 16 48 26 58 18 50
 36  4 44 12 38  6 46 14
 20 52 28 60 22 54 30 62
);

my %cells = ();

sub make_odmat8 {
  @odmat8 = ();

  for (0..3) {
    my $i = $_*4; push @odmat8, map({$_*4+3} @odmat4[$i..$i+3]), map({$_*4+1} @odmat4[$i..$i+3]);
  }

  for (0..3) {
    my $i = $_*4; push @odmat8, map({$_*4+0} @odmat4[$i..$i+3]),  map({$_*4+2} @odmat4[$i..$i+3]);
  }
}

sub getcell {
  my $level = shift; $level = int($level*32 + 0.5); 
  
  if (!exists($cells{$level})) {
    my ($cell, $bits) = ('', '');
	for (@dimat8) {
      $bits .= ($_ >= $level ? '1' : '0'); 
    }
	for (0..7) {
	  $cell .= pack("B*", substr($bits, $_*8, 8)) x 4;
	}
	$cells{$level} = $cell;
  }

  return $cells{$level};
}

sub addColorBlock {
  my ($self, $atX, $atY, $lX, $lY, $level, $color) = @_; return if $level >= 1.0;
  
  my $ts  = $self->{'ts'}; my $enc = XReport::AFP::Encoder->new();

  my $cell = getcell($level); return if $cell =~ /^\x00+$/; 

  my $cres = 3000; my $cexp = $cres/14400; 

  map {$_ = int($_*$cexp+0.5)} ($atX, $atY, $lX, $lY);

  $$ts .= 
    $enc->bii("CBLOCK") .  
	$enc->ioc(0, 0) .
    $enc->iid($cres, $cres, 1,1,1,1) .  
	$enc->icp($atX, $atY, 32,8, $lX, $lY) .
    $enc->ird($cell) .
    $enc->eii()
  ;
}

sub new {
  my ($ClassName, $page) = @_; my $ts = '';

  my $self = {
    ts => \$ts
  };

  bless $self, $ClassName;
}

sub getStructuredFields {
  my $self = shift; my $ts = $self->{'ts'}; return $$ts;
}

sub Close {
  my $self = shift; %$self = ();
}

#------------------------------------------------------------
package XReport::AFP::IOCAObject;

use strict;

my $tbl_Compressions = {
  None => "03", Fax => "80", Group4 => "82",
};

sub ImageArea {
  my $self = shift; my $ts = $self->{'ts'}; my $enc = XReport::AFP::Encoder->new();
  
  my %args = (atX => 0, atY => 0, iname => 'IOCAIMG', @_);
		 
  my (  
    $iname, 
	$iblob, $ifilename, 
	$atX, $atY, $towidth, $toheight,
    $resX, $resY, $cols, $rows, $cmpr
  ) =	
  @args{qw(iname iblob ifilename atX atY toWidth  toHeight resX resY columns rows cmpr)};

  for ($atX, $atY, $towidth, $toheight, $towidth, $toheight) {
	$_ = int($_ + 0.5);
  }

  $resX or $resX = 3000; $resY or $resY = 3000;

  exists($tbl_Compressions->{$cmpr})
   or
  die("COMPRESSION MAPPING MISSING for $cmpr."); 
  
  $cmpr = $tbl_Compressions->{$cmpr}; my $algExt = pack("H*", "f702010a"); # funcset 10
  
  $$ts .= 
    $enc->bim($iname) . 
    $enc->bog($iname) .
	$enc->obd($towidth, $toheight) .                    # to area size
	$enc->obp($atX, $atY, 0, 0) .                       # to area position + content offset
	$enc->mio() .                                       # trim | reduce |
	$enc->idd($resX, $resY, $cols, $rows, $algExt) .    # image resolution + size in points
	$enc->eog() .
	$enc->ipd(
	  $resX, $resY, $cols, $rows, $cmpr, $iblob
    ) .                                                 # density, image size, compression, data
	$enc->eim()
  ;
}

sub new {
  my $ClassName = shift; my $ts = '';

  my $self = {
    ts => \$ts
  };

  bless $self, $ClassName;
}

sub getStructuredFields {
  my $self = shift; my $ts = $self->{'ts'}; return $$ts;
}

sub Close {
  my $self = shift; %$self = ();
}

#------------------------------------------------------------
package XReport::AFP::GraphicsObject;

use strict;

sub ImageArea {
  my $self = shift;
  
  my $enc = XReport::AFP::Encoder->new(); my $ts = $self->{'ts'}; my ($gs, $gi);
  
  $gs = 
    pack(
	  "H*", 
	  join("",
	   "2602000a", "1807", "1901", "210400000000", "810408800880",
	   "2906", "c20400000000",
	  #"c30c00000000c1c1c1c1c2c2c2c2",
	  )
	)
  ;
  $gi = 
    pack(
	 "H2H2H8H2H2nH8",
	 "70",
	 "0c",
	 "00000000",
	 "00",
	 "00",
	  length($gs),
	 "00000000"
	)
  ;

  $$ts .=
    $enc->bgr("ZZTEST01") . 
    $enc->bog("00000001") .
	$enc->obd(4800/6, 600/6) .               # area size
	$enc->obp(200, 700, 0, 0) .              # area position + content position
	$enc->mgo() .                            # trim | reduce |
	$enc->gdd(2400, 2400, 0, 1200, 0, 1200) . # image resolution + size in points
	$enc->eog() .
	$enc->gad($gi.$gs) .                     # graphics data 
	$enc->egr()
  ;
}

sub new {
  my $ClassName = shift; my $ts = '';

  my $self = {
    ts => \$ts
  };

  bless $self, $ClassName;
}

sub getStructuredFields {
  my $self = shift; my $ts = $self->{'ts'}; return $$ts;
}

sub Close {
  my $self = shift; %$self = ();
}

#------------------------------------------------------------
package XReport::AFP::BarCodeObject;

use strict;

sub new {
}

sub getStructuredFields {
}

sub Close {
  my $self = shift; %$self = ();
}

#------------------------------------------------------------
package XReport::AFP::Page;

use strict;

use constant cb_color => 0;
use constant cb_level => 1;
use constant cb_atY   => 2;
use constant cb_lY    => 3;
use constant cb_atX   => 4;
use constant cb_lX    => 5;
use constant cb_toX   => 6;
use constant cb_toY   => 7;

my $stroke = 0;

sub xjoinable {
  my ($cb_1, $cb) = @_; 
  return 
   $cb->[cb_color] == $cb_1->[cb_color] &&
   $cb->[cb_level] == $cb_1->[cb_level] &&
   $cb->[cb_atY]   == $cb_1->[cb_atY]   && 
   $cb->[cb_lY]    == $cb_1->[cb_lY]    && 
   $cb->[cb_atX]   == $cb_1->[cb_toX]
  ;
}

sub yjoinable {
  my ($cb_1, $cb) = @_; 
  return 
   $cb->[cb_color] == $cb_1->[cb_color] &&
   $cb->[cb_level] == $cb_1->[cb_level] &&
   $cb->[cb_atX]   == $cb_1->[cb_atX]   && 
   $cb->[cb_lX]    == $cb_1->[cb_lX]    && 
   $cb->[cb_atY]   == $cb_1->[cb_toY]
  ;
}

sub _creFatColorBlocks {
  my $self = shift; my ($ColorBlocks, $enc) = @{$self}{qw(FatColorBlocks encoder)};
  
  my (@cb_1, @cb, @wb_1, @wb, %wb);

  @cb_1 = sort {
    $a->[cb_color] != $b->[cb_color] ? $a->[cb_color] <=> $b->[cb_color] : 
    $a->[cb_level] != $b->[cb_level] ? $a->[cb_level] <=> $b->[cb_level] : 
	$a->[cb_atY]   != $b->[cb_atY]   ? $a->[cb_atY]   <=> $b->[cb_atY]   :   
	$a->[cb_lY]    != $b->[cb_lY]    ? $a->[cb_lY]    <=> $b->[cb_lY]    :	  
	$a->[cb_atX]  <=> $b->[cb_atX]
  }
  @$ColorBlocks; 
  
  @cb = grep {$_->[1] != 1} @cb_1; @wb = grep {$_->[1] == 1} @cb_1;
  
  @wb_1 = shift @wb if @wb; my ($wb_1, $wb);

  while (@wb) {
	$wb_1 = $wb_1[-1]; $wb = shift @wb;
	if ( xjoinable($wb_1, $wb) ) {
	  $wb_1->[cb_lX] += $wb->[cb_lX]; $wb_1->[cb_toX] += $wb->[cb_lX]; 
	}
	else {
	  push @wb_1, $wb; 
	}
  }
  for (@wb_1) {
	$wb{join("/", @$_[cb_atY,cb_lY])} = [@$_[cb_atX, cb_lX, cb_toX]];
  }
  
  @cb_1 = shift @cb if @cb; my ($cb_1, $cb);

  while (@cb) {
	$cb_1 = $cb_1[-1]; $cb = shift @cb;
	if ( xjoinable($cb_1, $cb) ) {
	  $cb_1->[cb_lX] += $cb->[cb_lX]; $cb_1->[cb_toX] += $cb->[cb_lX];
	}
	else {
	  push @cb_1, $cb; 
	}
  }

  for $cb (@cb_1) {
	my ($color, $level, $atY, $lY, $atX, $lX) = @$cb; my @etodo;
	
	my $ckey = "$atY/$lY"; my ($atX_, $lX_, $toX_) = (0,0,0);

	if (exists($wb{$ckey})) {
	   ($atX_, $lX_, $toX_) = @{$wb{$ckey}};
	}

	if ($lX_ && $atX_ > $atX && ($atX_+$lX_) < $atX+$lX) {
	  push @etodo, $atX, $atX_-$atX, $toX_, $lX-($atX_-$atX)-$lX_; 
	}
	else {
	  push @etodo, $atX, $lX;
	}
	
	my $img = $self->addImageObject();

	while (@etodo) {
      my ($atX, $lX) = splice(@etodo,0,2); 

      $img->addColorBlock($atX, $atY, $lX, $lY, $level, $color);
	}
  }
}

sub _creThinColorBlocks {
  my $self = shift; my ($ColorBlocks, $enc) = @{$self}{qw(ThinColorBlocks encoder)};

  my @cb = sort {
    $a->[cb_color] != $b->[cb_color] ? $a->[cb_color] <=> $b->[cb_color] : 
	$a->[cb_level] != $b->[cb_level] ? $a->[cb_level] <=> $b->[cb_level] : 
	$a->[cb_atY]   != $b->[cb_atY]   ? $a->[cb_atY]   <=> $b->[cb_atY]   :   
	$a->[cb_lY]    != $b->[cb_lY]    ? $a->[cb_lY]    <=> $b->[cb_lY]    :	  
	$a->[cb_atX]  <=> $b->[cb_atX]
  }
  grep {$_->[cb_lY] < 50} @$ColorBlocks;

  my @cb_1 = shift @cb if @cb; my ($cb_1, $cb, $jcb_1, $jcb);

  while (@cb) {
	$cb_1 = $cb_1[-1]; $jcb_1 = $jcb; $cb = shift @cb; $jcb = join("/", @$cb); next if $jcb eq $jcb_1;
	if ( xjoinable($cb_1, $cb) ) {
	  $cb_1->[cb_lX] += $cb->[cb_lX]; $cb_1->[cb_toX] += $cb->[cb_lX];
	}
	else {
	  push @cb_1, $cb; 
	}
  }

  my $txt = $self->addTextObject();
  
  for $cb (@cb_1) {
	my ($color, $level, $atY, $lY, $atX, $lX) = @$cb;
	
	$txt->drawHRule($atX, $atY, $lX, $lY);
  }

  my @cb = sort {
    $a->[cb_color] != $b->[cb_color] ? $a->[cb_color] <=> $b->[cb_color] : 
	$a->[cb_level] != $b->[cb_level] ? $a->[cb_level] <=> $b->[cb_level] : 
	$a->[cb_atX]   != $b->[cb_atX]   ? $a->[cb_atX]   <=> $b->[cb_atX]   : 
	$a->[cb_lX]    != $b->[cb_lX]    ? $a->[cb_lX]    <=> $b->[cb_lX]    : 
	$a->[cb_atY]  <=> $b->[cb_atY]
  }
  grep {$_->[cb_lX] < 50} @$ColorBlocks;

  my @cb_1 = shift @cb if @cb; my ($cb_1, $cb, $jcb_1, $jcb);

  while (@cb) {
	$cb_1 = $cb_1[-1]; $jcb_1 = $jcb; $cb = shift @cb; $jcb = join("/", @$cb); next if $jcb eq $jcb_1;
	if ( yjoinable($cb_1, $cb) ) {
	  $cb_1->[cb_lY] += $cb->[cb_lY]; $cb_1->[cb_toY] += $cb->[cb_lY];
	}
	else {
	  push @cb_1, $cb; 
	}
  }

  my $txt = $self->addTextObject();
  
  for $cb (@cb_1) {
	my ($color, $level, $atY, $lY, $atX, $lX) = @$cb;
	
	$txt->drawVRule($atX, $atY, $lX, $lY);
  }

}

sub _creColorBlocks {
  my $self = shift; $self->_creFatColorBlocks(); $self->_creThinColorBlocks();
}

sub tag {
}

sub addTextObject {
  my $self = shift; my $objList = $self->{qw(objList)};

  my $obj = XReport::AFP::TextObject->new($self);

  push @$objList, $obj; return $obj;
}

sub addImageObject {
  my $self = shift; my $objList = $self->{qw(objList)};

  my $obj = XReport::AFP::ImageObject->new($self);

  push @$objList, $obj; return $obj;
}

sub addPageOverlay {
  my ($self, $epo, $atX, $atY) = @_; my $OverlayMapKEY = $epo;
  
  my ($doc, $enc, $objList) = @{$self}{qw(doc encoder objList)}; 

  my $OverlayMap  = $self->{'OverlayMap'};

  if (!exists($OverlayMap->{$OverlayMapKEY})) {
	my $enc = $self->{'encoder'};
	$OverlayMap->{$OverlayMapKEY} = ( $OverlayMap->{'atOverlayLid'} += 1 );
	$self->{'ts_mpo'} .= $enc->mpo($OverlayMap->{$OverlayMapKEY}, $epo);
  }

  push @$objList, $enc->ipo($doc->_MapPageOverlay($epo), $atX, $atY);
}

sub addPageSegment {
  my ($self, $eps, $atX, $atY, $lX, $lY) = @_; 
  #print "_MapPageSegment(\"$eps\", $lX, $lY);\n";
  
  my ($doc, $enc, $objList) = @{$self}{qw(doc encoder objList)}; 

  push @$objList, $enc->ips($doc->_MapPageSegment($eps, $lX, $lY), $atX, $atY);
}

sub FillArea {
  my ($self, $atX, $atY, $lX, $lY, $level, $color) = @_; my $cbkey = "$color/$level"; my $cb;

  if ($lX > 50 and $lY > 50) {
    $cb = $self->{'FatColorBlocks'};
  }
  else {
    $cb = $self->{'ThinColorBlocks'};
  }
  
  push @$cb, [$color||1, $level, $atY, $lY, $atX, $lX, $atX+$lX, $atY+$lY]; 
}

sub StrokeArea {
  my ($self, $atX, $atY, $lX, $lY, $level, $color) = @_; 

  $self->FillArea($atX, $atY, $lX-5,  10, $level, $color); $atX += $lX;
  $self->FillArea($atX, $atY, 10, -$lY+5, $level, $color); $atY -= $lY;
  $self->FillArea($atX, $atY, -$lX+5, 10, $level, $color); $atX -= $lX;
  $self->FillArea($atX, $atY, 10,  $lY-5, $level, $color); $atY += $lY;
}

sub setFont {
  my ($self, $FontName, $YSize, $XSize) = @_; my $doc = $self->{'doc'}; $XSize ||= $YSize; 

  $FontName = $doc->_MapFontName($FontName, $YSize); 
  
  my $FontMapKEY = "$FontName/$YSize/$XSize";

  my $FontMap  = $self->{'FontMap'};

  if (!exists($FontMap->{$FontMapKEY})) {
	my $enc = $self->{'encoder'};
	$FontMap->{$FontMapKEY} = ( $FontMap->{'atFontLid'} += 1 );
	$self->{'ts_mcf2'} .= $enc->mcf2($FontMap->{$FontMapKEY}, $FontName, $YSize, $XSize);
  }

  return $FontMap->{$FontMapKEY};
}

sub new {
  my ($ClassName, $doc, $atPage) = @_; 

  my $enc = XReport::AFP::Encoder->new();
  
  my $self = {
    FatColorBlocks => [],
    ThinColorBlocks => [],
	ts_mcf2 => '',
	ts_mpo => '',
	ts_mps => '',
	encoder => $enc,
    Size => '',  
	doc => $doc,
    atPage => $atPage,	
    objList => [],
	atFontLid => 0, FontMap => {}, OverlayMap => {}
  };

  bless $self, $ClassName; 
}

sub getStructuredFields {
  my ($self, $NoHdr) = @_; 
  
  my (
    $enc, $objList,
    $ts_mcf2, $ts_mpo, $ts_mps
  ) = 
  @{$self}{qw(encoder objList ts_mcf2 ts_mpo ts_mps)};

  my $pageid = substr("0" x 8 . $self->{'atPage'}, -8);

  $self->_creColorBlocks();

  my ($ts, $ts_pgd, $ts_ae) = ('', '', '');

  for(@$objList) {
	$ts .= (ref($_) ? $_->getStructuredFields() : $_); 
  }

  $ts_pgd = $enc->pgd(1984, 2806). $enc->ptd(1984, 2806);

  $ts_ae = $enc->bae(). $ts_mcf2. $ts_mpo. $ts_mps. $ts_pgd. $enc->eae();
		
  return ($NoHdr ? $ts_ae. $ts : $enc->bgp($pageid). $ts_ae. $ts. $enc->epg());
}

sub Close() {
  my ($self, $OUTPUT) = @_; my $objList = $self->{'objectsList'};
  
  print $OUTPUT $self->getStructuredFields() if $OUTPUT; 

  map {$_->Close()} (@$objList);
  
  %$self = ();
}

#------------------------------------------------------------
package XReport::AFP::Overlay;

use strict;

sub new {
}

sub getStructuredFields {
}

#------------------------------------------------------------
package XReport::AFP::PageSegment;

use strict;

sub new {
}

sub getStructuredFields {
}

#------------------------------------------------------------
package XReport::AFP::Doc;

use strict;

use Symbol;

sub _init {
  my $self = shift; my ($rm, $enc, $OUTPUT) = @{$self}{qw(resmanager encoder OUTPUT)};

  my $formdef = 
    $enc->bfm('F1IFORM') .
    $enc->bmm('C0DUPLEX') .
    $enc->pgp2() .
    $enc->mdd() .
    $enc->mcc() .
    $enc->mmc(1,1) . 
	$enc->mmc(2,1) .
    $enc->emm('C0DUPLEX') .
#    $enc->bmm('C0BIN2') .
#    $enc->pgp2() .
#    $enc->mdd() .
#    $enc->mcc() .
#    $enc->mmc(1,2) . 
#    $enc->mmc(2,2) . 
#    $enc->emm('C0BIN2') .
	$enc->efm('F1IFORM')
  ;
  
  $rm->_addStreamResource("FormDef", "F1IFORM", $formdef);

  print $OUTPUT $enc->bdt();
}

sub _MapFontName {
  my ($self, $FontName, $FontSize) = @_; my $Fonts = $self->{'Fonts'}; 
  
  exists($Fonts->{$FontName}) and return $Fonts->{$FontName};

  my $rm = $self->{'resmanager'};
  
  return $Fonts->{$FontName} = $rm->addFont("$FontName"); 
}

sub _MapPageOverlay {
  my ($self, $epo) = @_; my $PageOverlays = $self->{'PageOverlays'}; 

  my $epoKEY = "$epo";
  
  exists($PageOverlays->{$epoKEY}) and return $PageOverlays->{$epoKEY}; 

  my $rm = $self->{'resmanager'};
  
  return $PageOverlays->{$epoKEY} = $rm->addPageOverlay($epo);
}

sub _MapPageSegment {
  my ($self, $eps, $Xexp, $Yexp) = @_; my $PageSegments = $self->{'PageSegments'}; 

  my $epsKEY = "$eps/$Xexp/$Yexp";
  
  exists($PageSegments->{$epsKEY}) and return $PageSegments->{$epsKEY}; 

  my $rm = $self->{'resmanager'};
  
  return $PageSegments->{$epsKEY} = $rm->addPageSegment($eps, $Xexp, $Yexp);
}

sub addStreamResource {
  my $self = shift; $self->{'resmanager'}->_addStreamResource(@_);
}

sub beginPageGroup {
}

sub endPageGroup {
}

sub nop {
  my $self = shift; my ($OUTPUT, $enc) = @{$self}{qw(OUTPUT encoder)};

  print $OUTPUT $enc->nop(@_);
}

sub tagList {
  my $self = shift; my ($OUTPUT, $enc) = @{$self}{qw(OUTPUT encoder)};

  while(@_) {
    print $OUTPUT $enc->tle(splice(@_,0,2));
  }
}

sub setMediumMap {
  my $self = shift; my ($OUTPUT, $enc) = @{$self}{qw(OUTPUT encoder)};

  print $OUTPUT $enc->imm(shift);
}

sub addPage {
  my $self = shift; my ($lPage, $atPage, $OUTPUT) = @{$self}{qw(lPage atPage OUTPUT)};

  $lPage->Close($OUTPUT) if $lPage; 
  
  $atPage += 1; 

  $lPage = XReport::AFP::Page->new($self, $atPage);
  
  @{$self}{qw(lPage atPage)} = ($lPage, $atPage); return $lPage;
}


sub delPage {
  my $self = shift; my ($lPage, $atPage, $OUTPUT) = @{$self}{qw(lPage atPage OUTPUT)};

  die("delPage without addPage detected") if !$lPage; $lPage->Close(); 
  
  $atPage -= 1; 
  
  @{$self}{qw(lPage atPage)} = (undef, $atPage); return 1;
}

sub Create {
  my ($ClassName, $FileName, %args) = @_; my ($rm, $enc, $OUTPUT); 

  $rm = XReport::AFP::ResourceManager->new("$FileName\.res");
  
  $enc = XReport::AFP::Encoder->new(); 

  open($OUTPUT, ">$FileName") 
    or
  die("OPEN OUTPUT ERROR For File \"$FileName\" $!"); binmode($OUTPUT);
  
  my $self = {
    PathToResources => {FONT => [], OVERLAY => [], PSEG => []},
	resmanager => $rm, encoder => $enc, 
	FileName => $FileName,
	OUTPUT => $OUTPUT, 
    Fonts => {},
    MediumMaps => {},
    PageOverlays => {},
    PageSegments => {},
    LastMediumMap => "",
	lPage => undef, atPage => 0,
  };

  bless $self, $ClassName; $self->_init(); return $self;
}

sub SavePageAsOverlay {
  my ($self, $OverlayName) = @_;  my ($enc, $lPage) = @{$self}{qw(encoder lPage)};

  my $ts = $enc->bmo($OverlayName). $lPage->getStructuredFields(1). $enc->emo();
  
  open(OUT, ">$OverlayName.afp"); binmode(OUT);
  print OUT $ts;
  close(OUT);

  die $OverlayName, " ", length($ts), "\n";
}

sub ClosePage {
  my $self = shift; my ($lPage, $OUTPUT) = @{$self}{qw(lPage OUTPUT)};

  $lPage->Close($OUTPUT) if $lPage;

  delete $self->{'lPage'};
}

sub Close {
  my ($self, $tFileName, %args) = @_; my $unlink = $args{'unlink'};
  
  my (
	$OUTPUT, $rm, $enc, $lPage, $FileName
  ) = 
  @{$self}{qw(OUTPUT resmanager encoder lPage FileName)};
  
  $lPage->Close($OUTPUT) if $lPage; 

  $rm->Close() if $rm; 
  
  print $OUTPUT $enc->edt(); close($OUTPUT);

  return 0 if $tFileName eq ''; my ($INPUT, $rec) = (gensym(), '');
  
  open($OUTPUT, ">$tFileName"); binmode($OUTPUT); 
  
  for my $inFile ("$FileName.res", "$FileName") {
    open($INPUT, "<$inFile"); binmode($INPUT);
	while(!eof($INPUT)) {
	  read($INPUT, my $rec, 32768); print $OUTPUT $rec;
	}
	close($INPUT); unlink $inFile if $unlink;
  }
  
  close($OUTPUT); return 0;
}

1;

