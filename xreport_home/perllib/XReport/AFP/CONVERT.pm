
package XReport::AFP::CONVERT;

use strict;

use Symbol;
use File::Path;
use File::Basename;
use Compress::Zlib;

use XReport::TRANSMIT::INPUT;
use XReport::Util;
use XReport::DBUtil;
use XReport::AFP::IOStream;

#------------------------------------------------------------

use constant TU_EOF => 0xFFFF;
use constant TU_RESNAME => 0x1001;
use constant TU_BINSTREAM => 0x1002;
use constant TU_PSSTREAM => 0x1003;
use constant TU_USEDRESOURCES => 0x1004;

use constant UM_FULL => 1;
use constant UM_INCREMENTAL => 2;

use constant FULL_MODE => 1;

use constant SF_NOP => "\xd3\xee\xee";

#------------------------------------------------------------

my ($AfpSubSysId, $FileId, $TimeRef, $UPD_MODE, $ibmzlib);

my ($XmitFile, $DblkDir, $VERBOSE, $BINSTREAM, $dbr, $dieonerror);

my %Os390Prefixes = (
  FDEFLIB => 'F1', PDEFLIB => 'P1', OVERLIB => 'O1',
  PSEGLIB => 'S1', IMAGLIB => 'FCB2', FORMLIB => 'F1', PAGELIB => 'P1'
);
my @to_delete = ();

my $lx_afpresource = qr/\xd3\xa8(?:\xcd|\xcb|\xdf|\x5f|\x8a|\x89)/; 

sub FileMD5Hash {
  use Digest::MD5;
  my $fileName = shift; my $INPUT = gensym(); my $buff;
  
  open($INPUT, "<$fileName"); binmode($INPUT);
  read($INPUT,$buff,-s $fileName);
  close($INPUT);
  
  return Digest::MD5::md5_hex($buff);
}

sub GetFileByteStream {
  my $fileName = shift; my $INPUT = gensym(); my $buff;
  
  open($INPUT, "<$fileName") 
    or 
  die("INPUT OPEN ERROR \"$fileName\" $!"); binmode($INPUT);
  read($INPUT, $buff, -s $fileName);
  close($INPUT);

  return \$buff;
}

sub WriteTextUnits {
  my $BINSTREAM = shift;
  
  my $offset = tell($BINSTREAM);
  
  while(@_) {
    my ($k, $v) = (shift, shift);
	print $BINSTREAM pack("n", $k);
	
    if( $k == TU_RESNAME ) {   #Resource Name;
	  print $BINSTREAM pack("nNa*", 1, length($v), $v);
	}
    elsif( $k == TU_BINSTREAM ) {   #BINARY IMAGE;
	  my $fileSize = -s $v;
	  my $cstream = compress(${GetFileByteStream($v)});
	  print $BINSTREAM pack("nNNN", 2, 4, $fileSize, length($cstream)), $cstream;
	}
    elsif( $k == TU_PSSTREAM ) {   #POSTSCRIPT IMAGE;
	  my $fileSize = -s $v;
	  my $cstream = compress(${GetFileByteStream($v)});
	  print $BINSTREAM pack("nNNN", 2, 4, $fileSize, length($cstream)), $cstream;
	}
    elsif( $k == TU_USEDRESOURCES ) {   #USED RESOURCES;
	  print $BINSTREAM pack("nNa*", 1, length($v), $v);
	}
    else {
	  die "INVALID TEXT UNIT KEY \"$k\"\n";
	}
  }
  print $BINSTREAM pack("n", TU_EOF);

  return $offset;
}

sub IsAfpResource {
  my $fileName = shift; my $INPUT = gensym(); return 0 if (-s $fileName) < 12;
  open($INPUT, "<$fileName"); binmode($INPUT);
  my ($cbuff, $c5a, $ll, $cfield);
  while(!eof($INPUT)) {
    read($INPUT,$cbuff,2+6); ($ll, $c5a, $cfield) = unpack("n a1 x2 a3", $cbuff);
    last if $c5a ne "\x5a" || $cfield ne SF_NOP; 
    seek($INPUT, $ll-6, 1);
  }
  close($INPUT);
  unless ($c5a eq "\x5a" && $cfield =~ /$lx_afpresource/o) {
    $main::debug && i::warnit("AFP CONVERT: $fileName CC: ".unpack('H*', $c5a)." Cfield: ".unpack('H*', $cfield));
    return 0;
  }
  return 1;
}

sub _RemoveDirTree {
  my $dir = shift;
  for my $fileName (glob("$dir/*")) {
    if (-f $fileName) {
      unless ( unlink $fileName ) {
	die "UNLINK $fileName ERROR $!";
      }
#      print "FILE $fileName DELETED\n";
    }
    elsif (-d $fileName) {
      i::logit("DELETING DIRECTORY $fileName.");
      _RemoveDirTree($fileName) or die "RemoveTree ERROR for \"$fileName\"";
      rmdir $fileName or die "RMDIR $fileName ERROR $!";
      i::logit("DIRECTORY \"$fileName\" CLEANED.");
    }
  }
  return 1;
}


sub CLEAN_DIRECTORY {
  my $CleanDir = shift; mkdir($CleanDir); #Create directory
  
  #Clean target directory
#  print "CleanDir=\"$CleanDir\"\n";
  die "CleanDir PARAMETER MISSING !!" if ($CleanDir eq "");
  _RemoveDirTree $CleanDir;
  rmdir $CleanDir or die "RMDIR $CleanDir ERROR $!";
  i::logit("DIRECTORY \"$CleanDir\" CLEANED");
}

sub PERFORM_Expand {
  my $tr = XReport::TRANSMIT::INPUT->new(); 

  $tr->EXPAND_LIBRARY(
    INPUT => $XmitFile,
    OUTDIR => $DblkDir,
    DEBLOCK => 1,
    DEBUG => 0,
	VERBOSE => $VERBOSE
  );
}

sub TRANSLATE_AfpFile {
  my ($fileName, $psFileName) = (shift, shift); my $OUTPUT = gensym();
  
###mpezzi  my ($UsedResources, $PsLines) = XReport::AFP::IOStream::ExpandResourceFile($fileName);
### evita di interromper per risorse non valide
  my ($UsedResources, $PsLines);
  eval { ($UsedResources, $PsLines) =  XReport::AFP::IOStream::ExpandResourceFile($fileName); };
  my $errtoshow = "error on $fileName: " . $@ if $@;
  if ( $@ ) {
  	if ( !$dieonerror ) {
  	  &$logrRtn($errtoshow);
	  return undef;
  	}
  	else {
  		die $errtoshow."\n";
  	}
  }  
    
  open($OUTPUT,">$psFileName") or die("OUTPUT OPEN ERROR \"$psFileName\" $!"); binmode($OUTPUT);
  print $OUTPUT join("\n", @$PsLines);
  close($OUTPUT); 
  
  return $UsedResources;
}

sub TRANSLATE_FcbFile {
  my ($fileName, $txtFileName) = (shift, shift); my ($INPUT, $OUTPUT) = (gensym(), gensym());
  
  open($INPUT,"<$fileName") or die("INPUT OPEN ERROR \"$fileName\" $!"); binmode($INPUT);
  open($OUTPUT,">$txtFileName") or die("OUTPUT OPEN ERROR \"$txtFileName\" $!"); binmode($OUTPUT);
  my ($lrec, $lreclen);
  while(!eof($INPUT)) {
    read($INPUT, $lreclen, 2);
    read($INPUT, $lrec, unpack("n", $lreclen));
    if (eof($INPUT)) {
      print $OUTPUT unpack("H*", $lrec), "\n";
    }
  }
  close($OUTPUT); close($INPUT); return '';
}

use Convert::EBCDIC;

sub TRANSLATE_CntlFile {
  my ($fileName, $txtFileName) = (shift, shift); my ($INPUT, $OUTPUT) = (gensym(), gensym());
  my (@UsedResources, @PsLines);
  my $translator = Convert::EBCDIC->new();
  
  open($INPUT,"<$fileName") or die("INPUT OPEN ERROR \"$fileName\" $!"); binmode($INPUT);
  open($OUTPUT,">$txtFileName") or die("OUTPUT OPEN ERROR \"$txtFileName\" $!"); binmode($OUTPUT);
  my $lline = '';
  while(!eof($INPUT)) {
    
    read($INPUT, my $lreclen, 2);
    read($INPUT, my $lrec, unpack("n", $lreclen));
    $lrec = uc($translator->toascii($lrec));
    next if $lrec =~ /^\s*$/; $lline =~ s/\s+$//;
    if ($lrec =~ /\)$/ ) {
      push @PsLines, $lline . $lrec; 
      my ($larg, $lvalue) = $lline =~ /^\s*(\w+)\((.*)\)\s*$/; 
      if ($larg =~ /FORMDEF|PAGEDEF/ ) {
	push @UsedResources, ($larg eq 'PAGEDEF' ? 'P1' : 'F1') . $lvalue;
      }
      elsif ( $larg =~ /CHARS/ ) {
	push @UsedResources, join(",", map {"X0$_"} split(/\W+/, $lvalue))
      }
      $lline = ''; 
    }
    else {
      $lline .= substr($lrec, 0, 71);
    }

    if (eof($INPUT)) {
      print $OUTPUT join("\n", @PsLines), "\n";
    }
  }
  
  close($OUTPUT); close($INPUT); return join(", ", @UsedResources);
}

sub TRANSLATE_RESOURCES {
  if ( $XmitFile ne "" ) { CLEAN_DIRECTORY($DblkDir); PERFORM_Expand(); }
  ($dieonerror) = XReport::Util::getConfValues('dieonerror'); ### mpezzi

  $BINSTREAM = gensym();
  open( $BINSTREAM, ">$ibmzlib") or die("IBMZLIB OPEN ERROR \"$ibmzlib\" $!");
  binmode($BINSTREAM);

  dbExecute( "DELETE FROM tbl_Os390Resources WHERE FileId=$FileId");
  
  for my $lib (qw(IMAGELIB FDEFLIB PDEFLIB OVERLIB PSEGLIB CNTLLIB PAGELIB FORMLIB RSCELIB)) {
	next if !-d "$DblkDir/$lib"; 
	
	my $TextDir = "$DblkDir/$lib\.txt"; mkdir($TextDir);
	
    for my $fileName (glob("$DblkDir/$lib/*")) {
      my $ResName = basename($fileName); my ($trFileName, $UsedResources) = ("", "");
      $ResName = 'CNTL.' . $ResName if $lib eq 'CNTLLIB';
      if ( $lib !~ /IMAGELIB|CNTLLIB/ && !IsAfpResource($fileName) ) {
	&$logrRtn("Not An AFP Resource - skipping $ResName");
	next;
      }
	  
      my $MD5_Hash = FileMD5Hash($fileName);
	  
	  ### todo: test for TimeRef (NOT fileId) -- done
      $dbr = dbExecute(
       "SELECT * FROM ( SELECT top 1 a.AfpSubSysId, a.ResName, a.MD5_Hash, a.FileId, b.TimeRef, a.FileOffset, b.IBMZLIB, b.UpdMode 
          FROM tbl_Os390Resources a
	     INNER JOIN tbl_Os390ResFiles b ON a.FileId = b.FileId 
          WHERE AfpSubSysId=$AfpSubSysId 
            AND ResName='$ResName' 
		  AND b.Timeref < '$TimeRef'
		ORDER BY b.TimeRef DESC, a.FileId DESC ) DERIVEDTBL
             where MD5_Hash = 0x$MD5_Hash
	   "
      );
	  ### todo: test for updatemode (full/incremental) -- done
      if ( !$dbr->eof() and $UPD_MODE == UM_INCREMENTAL ) { 
	&$logrRtn("MD5 $MD5_Hash already in DB - AFPSSID: $AfpSubSysId TIMEREF: $TimeRef - skipping $ResName");
#	    print "?? $ResName MD5=$MD5_Hash\n ALREADY THERE\n" if 0 and $VERBOSE; 
	    next; 
      }
      else {
#	    print "?? $ResName MD5=$MD5_Hash NEW\n" if $VERBOSE;
	&$logrRtn("MD5 $MD5_Hash already in DB - AFPSSID: $AfpSubSysId TIMEREF: $TimeRef - MODE REPLACE new $ResName inserted ") 
	  if $UPD_MODE != UM_INCREMENTAL;
      }
	  
      if ( $lib eq "CNTLLIB" ) {
	$trFileName = "$TextDir/$ResName";
#	print "Now processing CNTL File $trFileName\n";
        $UsedResources = TRANSLATE_CntlFile($fileName, $trFileName);
      }
      elsif ( $lib ne "IMAGELIB" ) {
    	next if !IsAfpResource($fileName); 
        if ( $ResName =~ /^[CXT]/i ) {
        }
        else {
		$trFileName = "$TextDir/$ResName\.ps";
        $UsedResources = TRANSLATE_AfpFile($fileName, $trFileName);
      }
      }
      else {
		$trFileName = "$TextDir/$ResName\.txt";
        $UsedResources = TRANSLATE_FcbFile($fileName, $trFileName);
      }
      &$logrRtn("Error in $lib - skipping $fileName") unless defined($UsedResources);
      next unless defined($UsedResources);
      
      my $FileOffset;
      if ( $ResName =~ /^[CXT]/i ) {
        &$logrRtn("Adding/Replacing $ResName");
        $FileOffset = WriteTextUnits(
           $BINSTREAM, 
           TU_RESNAME, $ResName,
           TU_BINSTREAM, $fileName,
           );
      }
      else {
      &$logrRtn("File from $lib has 0 size - skipping $ResName") unless -s $trFileName;
      unless ( -s $trFileName ) {
    	my ($workdir, $SrvName) = XReport::Util::getConfValues('workdir', 'SrvName');
	    my $afperror = "$workdir/$SrvName/$ResName.afp";
    	my $resfile = gensym();
    	open $resfile, ">$afperror"; 
    	binmode $resfile;
    	print $resfile ${GetFileByteStream($fileName)};
    	close $resfile;
    	next;
      }

      &$logrRtn("Adding/Replacing $ResName");
        $FileOffset = WriteTextUnits(
	    $BINSTREAM, 
		TU_RESNAME, $ResName,
		TU_BINSTREAM, $fileName,
		TU_PSSTREAM, $trFileName,
		TU_USEDRESOURCES, $UsedResources
	  );
      }
      $dbr = dbExecute(
       "INSERT INTO tbl_Os390Resources
        (AfpSubSysId, ResName, MD5_Hash, FileId, FileOffset) 
         VALUES 
        ($AfpSubSysId, '$ResName', 0x$MD5_Hash, $FileId, $FileOffset) 
	   "
      );
    }
  }

  close($BINSTREAM);
  
  if ( $XmitFile ne "" ) { 
    CLEAN_DIRECTORY($DblkDir); # mpezzi cntl file handling debug
  }
}

sub IMPORT_FILE {
  my %args = @_; @to_delete = ();

  ($XmitFile, $AfpSubSysId, $FileId, $UPD_MODE, $VERBOSE) =  @args{qw(XmitFile AfpSubSysId FileId UPD_MODE VERBOSE)};

  if ($XmitFile eq "" and $FileId) {
    require XReport::Util; require XReport::JobREPORT; my $jr;

	if ( ref($FileId) ) {
	  $jr = $FileId; $FileId = $jr->getValues('JobReportId');
	}
	else {
      $jr = XReport::JobREPORT->Open($FileId, FULL_MODE);
	}
	my ($TimeRef, $XferStartTime) = $jr->getValues(qw(UserTimeRef XferStartTime));
	$TimeRef = $XferStartTime unless $TimeRef;
	
	(my $ibmzloc = XReport::Util::getConfValues('ibmzlib') ) =~ s/\\/\//g;;
    my $ibmzlib_fn = $jr->getFileName("IBMZLIB");
    $ibmzlib = "$ibmzloc/$ibmzlib_fn";

    my $dbr = dbExecute(
      "SELECT FileId from tbl_Os390ResFiles where FileId = $FileId"
    );
    if ( $dbr->eof() ) {
      dbExecute(
	   "INSERT INTO tbl_Os390ResFiles 
	    (FileId, TimeRef, UpdMode, ibmzlib) 
		VALUES($FileId, '$TimeRef', $UPD_MODE, '$ibmzlib_fn')
	   "
	  );
    }
    else {
      $dbr->Close();
	  dbExecute(
	    "UPDATE tbl_Os390ResFiles 
		  set TimeRef = '$TimeRef', UpdMode=$UPD_MODE, ibmzlib='$ibmzlib_fn' 
		 where FileId = $FileId
		"
	  );
    }
	
	my ($workdir, $SrvName) = XReport::Util::getConfValues('workdir', 'SrvName');
    $XmitFile = "$workdir/$SrvName/xafprsce\.$FileId\.afp";
    $DblkDir = "$workdir/$SrvName/deblock"; mkdir $DblkDir if !-e $DblkDir;
	die "INVALID DBLKDIR $DblkDir" if !-d $DblkDir;
	
	push @to_delete, $XmitFile, $DblkDir;
	
    my $INPUT = XReport::INPUT->new($jr); $INPUT->Open();
    my $OUTPUT = gensym(); open($OUTPUT, ">$XmitFile") 
	  or 
	die("OUTPUT XMITFILE OPEN ERROR \"$XmitFile\" $!");binmode($OUTPUT);
    while(1) {
      my $line = $INPUT->getLine();
      last if $line eq "";
      if (length($line)<80) {
        $line .= "\x40"x(80-length($line));
      }
      print $OUTPUT $line;
    }
    close($OUTPUT);
    $INPUT->Close();
  }
  
  elsif ( $XmitFile ne "" and $FileId) {
    die "XREPORT::AFP::CONVERT (\$XmitFile ne '' and \$FileId) NOT YET IMPLEMENTED.";
  }

  else {
    die "XREPORT::AFP::CONVERT INVALID XmitFile($XmitFile) and FileId($FileId) PARAMETERS.";
  }

  ### todo: translate if not digit
  $AfpSubSysId = uc($AfpSubSysId);
  
  TRANSLATE_RESOURCES();

  for (@to_delete) {
	if (-f $_ and !unlink($_)) {
      i::logit("UNABLE TO DELETE FILE \"$_\" $!"); 
	}
	elsif (-d $_ and !rmdir($_)) {
      i::logit("UNABLE TO DELETE DIRECTORY \"$_\" $!"); 
	}
  }
}

1;