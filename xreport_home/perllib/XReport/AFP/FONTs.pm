
#------------------------------------------------------------
package XReport::AFP::FONTs;


use strict;

#use XReport::AUTOLOAD;

use POSIX qw(ceil);

use Convert::EBCDIC;

use XReport::AFP::GlyphsMap qw(ibm2adobe);

my $translator = Convert::EBCDIC->new();

my (%ftab, %charsetMap, %map_to, $initialized);

my %map_to = (
  normal => {
    'Helvetica' => 'Helvetica',
    'Helvetica-Bold' => 'Helvetica-Bold',
    'Helvetica 75 Bold' => 'Helvetica-Bold',
    'Symbol' => 'Symbol',
    'OCRB' => 'CZ920B',
    'Arial' => 'ArialMT',
    'ARIAL-LC' => 'ArialNarrow',
    'ARIAL' => 'ArialMT',
    'ARIAL BLACK' => 'Arial-Black',
    'Microsoft Sans Serif' => 'ArialMT',
    'HELVETICA LATIN1' => 'CZH200',
    'COURIER NEW' => 'CZ4200',
    'Courier New' => 'CZ4200',
    'PROTOTYPE' => 'CZ6200',
    'NIMBUSROMD' => 'CZN200',
    'SONORAN SANS SERIF' => 'CZH200',
    'SONORAN SERIF' => 'CZN200Sonoran'
  },
  bold => {
    'ArialMT' => 'ArialMT-Bold',
    'CZ4200' => 'CZ4400',
    'CZ5200' => 'CZ6200',
    'CZH200' => 'CZH400',
    'CZH300' => 'CZH500',
    'CZN200' => 'CZN400',
    'CZN200Sonoran' => 'CZN400Sonoran',
    'BeraSansMono-Roman' => 'BeraSansMono-Bold'
  },
  italic => {
    'ArialMT-Bold' => 'ArialMT-BoldItalic',
    'ArialMT' => 'ArialMT-Italic',
    'CZ4200' => 'CZ4300',
    'CZ5200' => 'CZ6300',
    'CZH200' => 'CZH300',
    'CZH300' => 'CZH400',
    'CZN200' => 'CZN400',
    'CZH400' => 'CZH500',
    'CZN400' => 'CZN500',
    'CZ4400' => 'CZ4500',
    'CZN200Sonoran' => 'CZN400Sonoran',
    'BeraSansMono-Roman' => 'BeraSansMono-Italic'
  }
);

sub ExpandAfp {
  my ($self, $sf) = splice(@_,0,2);
  if (my $cref = $self->can($sf)) {
    &$cref($self, @_);   
  }
  else {
    die __PACKAGE__ . " $sf  >>> not managed !!\n";
  }
}

sub trim {
  $_[0] =~ s/^ +//;
  $_[0] =~ s/ +$//;
  return $_[0];
}

sub rtrim {
  $_[0] =~ s/\s+$//;
  return $_[0];
}

sub ltrim {
  $_[0] =~ s/^\s+//;
  return $_[0];
}

sub trEbcdicName {
  my $c = substr($_[0],0,2);
  return $_[0] if ($c eq "00" || $c eq "ff");
  return rtrim($translator->toascii(pack("H*",$_[0])));
}

#------------------------------------------------------------ CODED FONTs 

sub d3a88a { # Begin Coded Font (BCF)
  my $self = shift; $self->{CFI} = [];
};

sub d38c8a { # Coded Font Index (CFI)
  my $self = shift; my ($CFI) = @{$self}{qw(CFI)};
  print "CFI Data:", unpack('H*', $_[0]), "\n";
  @$CFI = unpack("H16H16n2H8H2", $_[0] );    
  $CFI->[0] = rtrim(trEbcdicName($CFI->[0]));
  $CFI->[1] = rtrim(trEbcdicName($CFI->[1]));
  @{$self}{qw(CFI)} = [ @{$CFI} ];
  print "CFI: Charset name: $self->{CFI}->[0], Resource Name: $self->{CFI}->[1] ($CFI->[0], $CFI->[1])\n";
};

sub d3a78a { # Coded Font Control (CFC)
  my $self = shift; #ParseTripletGroup(substr($_[0],2));
};

sub d3a98a { # End Coded Font (ECF)
  my $self = shift; my ($os, $res, $CFI) = @{$self}{qw(os res CFI)};
  print "ECF: Charset name: $self->{CFI}->[0], Resource Name: $self->{CFI}->[1] ($CFI->[0], $CFI->[1])\n";
  $os->add( 
    "<<"
   ," /CodedFontName /$res" ," /CharsetName   /$CFI->[0]" ," /CodepageName  /$CFI->[1]" ," /VSize  $CFI->[2]" ," /HSize  $CFI->[2]"
   ,">>"
  );
  delete $self->{CFI};
};

#------------------------------------------------------------ CODE PAGEs

sub d3a887 { # Begin Code Page (BCP)
  my $self = shift; @$self{qw(CPD CPC)} = ([], []);
  #ParseTripletGroup(substr($_[0],8)) if length($_[0]) > 8;
};

sub d3a687 { # Code Page Descriptor (CPD)
  my $self = shift; my ($os, $tr, $CPD) = @{$self}{qw(os tr CPD)}; 
  @$CPD = unpack("a32nNn3", $_[0]);
  $CPD->[0] = $tr->toascii($CPD->[0]);
};

sub d3a787 { # Code Page Control (CPC)
  my $self = shift; my ($os, $tr, $CPC) = @{$self}{qw(os tr CPC)};
  @$CPC = unpack("a8C2NC3N", $_[0]);
  $CPC->[0] = $tr->toascii($CPC->[0]);
  print "CPC=>", join(",", @$CPC), "\n";
};

sub d38c87 { # Code Page Index (CPI)
  my $self = shift; my ($os, $tr, $CPC) = @{$self}{qw(os tr CPC)};
  my ($j, $jmax, @t, $e, %e) = (0,length($_[0]));
  for ($j=0; $j < $jmax; $j += 10) {
    @t = unpack("A8H2C",substr($_[0], $j, 10));
    $t[0] = trim($tr->toascii($t[0]));
    if ( $e = ibm2adobe($t[0]) ) {
      $e{$t[2]} = $e;
    }
    else {
      $e{$t[2]} = $t[0];
      warn "NOT EXISTS $t[0]\n";
    }
  }
  for $e (0..255) {
    if ( exists($e{$e}) ) {
      $os->add("/$e{$e}");
    }
    else {
      $os->add("/.notdef");
    }
  }
  my ($VSChar, $VSFlags) = @$CPC[4,5];
  if ($VSFlags & 8) {
    $os->add(
      "/vichar_is <". unpack("H2", pack("C",$VSChar)). "> def"
    );
  }
};

sub d3a987 { # End Code Page (ECP)
  my $self = shift; my ($os) = @{$self}{qw(os)};
  delete $self->{CPD};
};

#------------------------------------------------------------ FONTs

sub d3a889 { # Begin Font (BFN)
  my $self = shift; my ($FNN, $FNN2, $FNG) = ("", {}, "");
  @{$self}{qw(FNC FND FNI FNIe FNM FNOe FNN FNN2 FNG)} = ([], [], [], {}, [], {}, \$FNN, $FNN2, \$FNG);
  #ParseTripletGroup(substr($_[0],8)) if length($_[0]) > 8;
};

sub d3a689 { # Font Descriptor (FND)
  my $self = shift; my ($os, $tr, $res, $FND) = @{$self}{qw(os tr res FND)};
  @$FND = unpack("a32H2H2n6H2H2H2H30H4H20n2",$_[0]);
  $FND->[0] = $tr->toascii($FND->[0]);
  $FND->[0] =~ s/[\x00 ]+$//;
  #print "$res // $FND->[0] // $FND->[13]\n";
};

sub d3a789 { # Font Control (FNC)
  my $self = shift; my $FNC = $self->{FNC};
  @$FNC = unpack("H2H2H2H2H2H2n2n2H2H2H2H6H2H2H2H2n2NH6CNn",$_[0]);
  #print join(",", @$FNC), "\n";
};

sub d3a289 { # Font Patterns Map (FNM)
  my $self = shift; my ($os, $FNM) = @{$self}{qw(os FNM)}; @$FNM = ();
  for (my $p = 0; $p < length($_[0]); $p += 8) {
    my $a = [
      unpack("n2N",substr($_[0],$p,8))
    ];
    push @$FNM, $a;    
  }
};

sub d3ae89 { # Font Orientation (FNO)
  my $self = shift; my ($FNC, $FNOe) = @{$self}{qw(FNC FNOe)};
  my ($r, @g) = $_[0];
  if ( $FNC->[1] eq "01" ) {
    while (length($r) > 0) {
      @g = unpack("H8n4H2H2n5",$r); 
      $FNOe->{$g[0]} = [@g];
      $r = substr($r,26);
    }
  }
  elsif ( $FNC->[1] =~ /05|1e|1f/i) {
    while (length($r) > 0) {
      @g = unpack("H8n4H2H2n5s>",$r);
      $FNOe->{$g[0]} = [@g];
      $r = substr($r,26);
    }
  }
};

sub d3ac89 { # Font Position (FNP)
  my $self = shift; my ($os) = @{$self}{qw(os)};
  for (0..(length($_[0])/22 - 1)) {
    my $g = [unpack("(s>)5H10H2H2s>H2s>", substr($_[0], $_*22, 22))];
    $g->[10] -= 65536 if $g->[10] > 32767;
  }
};

sub d38c89 { # Font Index (FNI)
  my $self = shift; my ($os, $tr, $FNC, $FNI, $FNIe) = @{$self}{qw(os tr FNC FNI FNIe)};
  return if scalar(@$FNI);
  if ( $FNC->[1] eq "01" ) {
    for (my $p = 0; $p < length($_[0]); $p+=24) {
      my $g = [unpack("H16 (s>)3 N (s>)3",substr($_[0],$p,24))];
      $g->[0] = trim($tr->toascii(substr($_[0],$p,8)));
      push @$FNI, $g;
    }
  }
  elsif ( $FNC->[1] =~ /05|1e|1f/i) {
    for (my $p = 0; $p < length($_[0]); $p+=28) {
      my $g = [unpack("H16 (s>)3 H4 n (s>)3 H4 s>",substr($_[0],$p,28))];
      $g->[0] = trim($tr->toascii(substr($_[0],$p,8)));
#      $g->[1] -= 65536 if $g->[1] > 32767;
#      $g->[2] -= 65536 if $g->[2] > 32767;
#      $g->[3] -= 65536 if $g->[3] > 32767;
#      $g->[6] -= 65536 if $g->[6] > 32767;
#      $g->[7] -= 65536 if $g->[7] > 32767;
#      $g->[8] -= 65536 if $g->[8] > 32767;
#      $g->[10] -= 65536 if $g->[10] > 32767;
      push @$FNI, $g;
      $FNIe->{$g->[5]} = $g->[0]; 
    }
  }
  @$FNI = sort { $a->[5] <=> $b->[5] } @$FNI;
};

sub d3ab89 { # Font Name Map (FNN)
  my $self = shift; my ($os, $tr, $FNC, $FNN, $FNN2) = @{$self}{qw(os tr FNC FNN FNN2)};
  my ($lgid, $lFNN, $tgid) = @$FNC[22,23,24]; $$FNN .= $_[0]; 
  return if length($$FNN) < $lFNN;

  my ($ibm_format, $tech_format) = unpack("H2H2",$$FNN); 
  return if ($tech_format ne '03');
  for (0..($tgid-1)) {
    my ($gid, $offset) = unpack("a8N", substr($$FNN, 2+$_*$lgid, $lgid)); 
    my $l = unpack("C", substr($$FNN,$offset,1)); die "???" if $l<=1;
    my $t = substr($$FNN,$offset+1, $l-1);
    $gid = $tr->toascii($gid);
    $FNN2->{$t} = $gid;
  }
};

sub d3ee89 { # Font Patterns (FNG)
  my $self = shift; my ($FNG) = @{$self}{qw(FNG)};
  $$FNG .= $_[0];
};

sub d3a989 { # End Font (EFN)
  my $self = shift; my ($os) = @{$self}{qw(os)};
  $self->CreateFontFile();
  delete @{$self}{qw(FNC FND FNI FNIe FNM FNOe FNN FNN2 FNG)};
};

sub d3eeee { # No Operation
};

sub test_fromfont {
  my ($to, $from) = (shift, shift);
  if ($to =~ /^C0S0D/) {
    return 'BeraSansMono-Roman';
  }
  elsif ($from =~ /^(LETTER )?GOTHIC/i) {
    return 'CZ5200';
  }
  elsif ($from =~ /^DAX-LIGHT/i) {
    return 'Dax-Light';
  }
  elsif ($from =~ /^DAX/i) {
    return 'Dax';
  }
  
  else {
    print "???? $from NOT MATCHED ????\n"; return '';
  }
}

sub MapToOutlineFont { 
  return (0, "MakeFontType3");

#  my $self = shift; my ($OutlineFont, $MapTo) = (1, undef); my ($CharsetName, $FND) = @$self{qw(res FND)}; 
#  
#  $OutlineFont = 0 if $CharsetName =~ /OCR/i; my $fweight = $FND->[1] ne '07' ? 'normal' : 'bold';
#  
#  if ($OutlineFont == 1) {
#    if ( exists($charsetMap{$CharsetName}) ) {
#      $MapTo = $charsetMap{$CharsetName};
#    }
#    elsif (my $map_to = $map_to{'normal'}->{$FND->[0]} || test_fromfont($CharsetName, $FND->[0])) {
#     if ($FND->[1] eq "07" and $map_to !~ /Bold/) {
#       my $map_bold = $map_to{'bold'};
#       if (exists($map_bold->{$map_to})) {
#         $map_to = $map_bold->{$map_to};
#       }
#       else {
#         print "$map_to Bold?? \n", join("\n", %$map_bold);
#         print "$map_to // $map_bold->{$map_to} //\n";
#         $map_to .= '-Bold'; 
#       } 
#     } 
#      if (substr($FND->[13],0,1) eq "8" and $map_to !~ /Italic/) {
#        my $map_italic = $map_to{'italic'};
#        if (exists($map_italic->{$map_to})) {
#         $map_to = $map_italic->{$map_to};
#       }
#       else {
#         print "$map_to // $map_italic->{$map_to} //\n";
#          die "ssss\n";
#         die "$map_to Italic?? \n", join("\n", %$map_italic);
#         $map_to .= '-Italic'; 
#       }
#      }
#     $MapTo = $map_to;
#   }
#    elsif ( $CharsetName =~ /^C0\w{6}$/ ) {
#      my $cz = 'CZ'.substr($CharsetName,2,4); my $basedir = getConfValues('basedir'); 
#     if ( -e "$basedir/fonts/ibm.outlines/$cz\.pfb" ) {
#        $MapTo = $cz;
#     }
#     else {
#       $OutlineFont = 0;
#     }
#    }
#    else {
#     $OutlineFont = 0;
#    }
#  }
#  return ($OutlineFont, $OutlineFont ? $MapTo : "MakeFontType3");
}

#sub check_pfb_font {
#  my ($self, $pfb) = @_; my $pfa_file = "c:/temp/zz.pfa"; my $pfb_file = "c:/temp/zz.pfb"; my $FNN2 = $self->{FNN2};
#  unlink $pfa_file, $pfb_file;
#  open(OUT, ">$pfb_file"); binmode(OUT); print OUT $pfb; close(OUT);
#  system("t1disasm $pfb_file $pfa_file");
#  open(IN, "<$pfa_file") or die("$pfa_file $!"); binmode(IN);
#  read (IN, my $pfa_stream, -s $pfa_file);
#  for my $char (keys(%$FNN2)) {
#     $pfa_stream =~ s/\/$char\b/\/$FNN2->{$char}/g;
#  }
#  my ($pre, $encoding, $middle, $charstrings, $suff) = $pfa_stream =~ /^(.*?)(^\/Encoding 257 array.*?^ *def)(.*?)(\/CharStrings 257 dict dup begin.*end?)(.*)$/sm;
#  #print "kkkk ", join(",", map {length($_)} ($pfa_stream, $pre, $encoding, $middle, $charstrings, $suff));
#  my @cchars = $charstrings =~ /(\/[.\w]+)\s*\{/msg;
#  my $totcchars = @cchars;
#  $charstrings =~ s/Encoding 257 array/Encoding $totcchars array/s;
#  my %cchars = map {$_ => 1} @cchars;
#  my @echars = $encoding =~ /dup +\d+ +(\/\w+) +put/msg;
#  for my $echar (@echars) {
#    if (!exists($cchars{$echar})) {
#      $encoding =~ s/$echar/\/.notdef/s;
#    }
#  }
#  close(IN);
#  open(OUT, ">$pfa_file"); binmode(OUT);
#  print OUT $pre, $encoding, $middle, $charstrings, $suff;
#  close(OUT);
#  system("t1asm $pfa_file $pfb_file");
#  open(IN, "<$pfb_file"); binmode(IN); read(IN, $pfb, -s $pfb_file); close(IN); return $pfb;
#}

sub CreateFontFile {
  my $self = shift; my ($os, $res, $czmap, $FNC, $FND, $FNI, $FNM, $FNOe, $FNN, $FNN2, $FNG) = @{$self}{qw(os res czmap FNC FND FNI FNM FNOe FNN FNN2 FNG)};

  my ($fni, $gid, $inc, $fixed, $ascend, $descend, $bOffset, $aSpace, $bSpace, $cSpace); 
  my ($llx, $lly, $urx, $ury, $maxBo, $maxX, $maxY, $tx, $ty);
  my ($w, $h, $fm, $wh, $bits, $br, $bt, $hstr, $aref);
  my ($CharData, $Metrics, $Meight);

  my %PointsX = ();

  my @pelunits = ($FNC->[4] eq "00" ? (6,7) : (18,19)); 
  my (
    $scaleX, $scaleY
  ) = 
  map {2400/($_ || 2400)} @$FNC[@pelunits]; 
  
  my ($extX, $extY); 


  if ( $FNC->[1] eq "01" ) {
    $aref = $FNOe->{'00002d00'}; $maxBo = $aref->[1]; $maxX = $aref->[3]; $maxY = $aref->[4];
    for $fni (0..$#$FNI) {
      $aref = $FNI->[$fni];
      $maxBo = $aref->[2] if $aref->[2] > $maxBo; 
      $maxX  = $aref->[1] if $aref->[1] > $maxX; 
      $maxY  = ($aref->[2]+$aref->[3]) if ($aref->[2]+$aref->[3]) > $maxY;
    }
  }
  elsif ( $FNC->[1] eq "05" ) {
    $aref = $FNOe->{'00000000'}; $maxBo = $aref->[1]; 
    if ( $FNC->[4] eq "00" ) {
      $maxX = $aref->[2]; $maxY = $aref->[4];
    }
    elsif ( $FNC->[4] eq "02" ) {
      ($maxX, $maxY) = ($FNC->[6], $FNC->[7]);
      #$maxX = 1000; $maxY = 1000;
    }
    else {
      die "INVALID UNIT BASE FOR FONT CONTROL $FNC->[4] FOUND";
    }
  }
  elsif ( $FNC->[1] =~ /1f/i ) {
    my $ResourceName = $self->{ResourceName};
    print __PACKAGE__."::CreateFontFile - FNC1f - FNG: ".unpack('H60', $$FNG), "\n";
    my ($pfbl, $OIDLen) = unpack("Nx4n", $$FNG);
    (my $OIDDescr, my $pfb) = unpack("x10 a".($OIDLen-2)." a*", $$FNG);
    print __PACKAGE__."::CreateFontFile - FNC1f $pfbl ", length($$FNG), " OIDLEN: $OIDLen DESCR: $OIDDescr\n";
    my $pfa = '';
    while ($pfb) {
      (local $_, $pfb) = unpack("H4 a*", $pfb);
      /8003/ && last;
      print __PACKAGE__."::CreateFontFile - PFB(", length($pfb), "): ", join('::', unpack("H4 V X6 H64", $pfb)), "\n";
      (my $data, $pfb) = unpack("V/a a*", $pfb);
      $pfa .= (/8002/ ? join("\n", map { uc($_) } unpack("(H64)*", $data))."\n" :
           /8001/ ? $data :
           "% ------- invalid segment type found: $_\n"
          );
    }
    print __PACKAGE__."::CreateFontFile - found font $OIDDescr to create\n";
    my ($fname) = $pfa =~ /\/FontName \/(\S+) def/gs;
    (my $isFixedPitch) = $pfa =~ /\/isFixedPitch \/(\S+) def/gs;
    $isFixedPitch = ($isFixedPitch && $isFixedPitch eq 'true' ? 1 : 0);
#    my $NewFontName = "XR_$fname";
#    $pfa =~ s/$fname/$NewFontName/gs;
    $pfa =~ s/$fname/$ResourceName/gs;
    my $pfafn = $os->{directory}."/$ResourceName.pfa";
    print __PACKAGE__."::CreateFontFile - Opening $pfafn to create\n";
    open(PFA, ">$pfafn") || die "Unable to open $pfafn" ;
    binmode PFA;
    print PFA $pfa;
    close PFA;
#    $main::type1fonts->{"XR_$fname"} =  "\%\%BeginResource: font XR_$fname\n".$pfa.($pfa =~ /\n$/ ? '' : "\n")."\%\%EndResource: ";
    $os->add('<<'
#               ,"/MapTo /$NewFontName"
                ,"/MapTo /$ResourceName"
                ,"/isFixedPitch " . ($isFixedPitch ? 'true' : 'false')
                ,"/Meight 0.6"
                ,"/OutlineMatrix [24 0 0 -40 0 0]"
                ,"/Metrics"
                ,"<<"
                ,">>"
#                           ,"/isFixedPitch ". (($isFixedPitch) ? 'true' : 'false')
#                           ,"/Meight $Meight"
#                           ,"/OutlineMatrix [24 0 0 -40 0 0]"
                ,'>>');
    

    return;
    }
#  elsif ( $FNC->[1] =~ /1e|1f/i ) {
#    my $ldescr = unpack("x4x4n", $$FNG); my $descr = substr($$FNG, 10, $ldescr-2);
#    my $pfb = $self->check_pfb_font(substr($$FNG,8+$ldescr));
#    my $at = 0;
#
#    my ($control, $progr, $length) = unpack("H2CV", substr($pfb,0,6)); $at+=6;
#
#    my $chdr = substr($pfb,$at,$length); $at+=$length; 
#
#    my ($lczmap) = $chdr =~ /\/FontName\s*\/([^\s]+)/mso; warn ref($self), " czmap=$lczmap\n" if $main::debug;
#    
#    $os->binarymode(); $os->add($chdr);
#
#    while(1) {
#     ($control, $progr, $length) = unpack("H2CV", substr($pfb,$at,6)); $at+=6; last if $length == 0;
#      $os->add(substr($pfb,$at,$length)); $at += $length;
#    }
#
#    $czmap->{$res} = $lczmap; return;
#  }

  die("maxX error it is 0") if $maxX == 0;
  die("maxY error it is 0") if $maxY == 0;
  
  for $fni (0..$#$FNI) {
    $aref = $FNI->[$fni]; ($gid, $inc) = (@{$aref}[0..1]); $PointsX{ibm2adobe($gid)} = $inc;
  }

  my $IsFixedPitch;
  if ( $PointsX{'M'} and $PointsX{'M'} == $PointsX{'i'} ) {
    $IsFixedPitch = 1; 
    #$maxX = $PointsX{'M'}; <=== bad error !!!!!!!!!!!
  }
  else {
    $IsFixedPitch = 0;
  }

  $Meight = 0;
  
  for $fni (0..$#$FNI) {
    if ( $FNC->[1] eq "01" ) {
      $aref = $FNI->[$fni];
     ($gid, $inc, $w, $h, $fm) =  (@{$aref}[0..4]); $w+=1; $h+=1; $fm*=8;
     ($aSpace, $bSpace, $cSpace) = (@{$aref}[5..7], @{$aref}[10]);
      $PointsX{ibm2adobe($gid)} = $inc;
      $ascend = $maxBo; $descend = $h-$maxBo;
      $inc = $inc/$maxX;
      $llx = $aSpace/$maxX; $lly = -$descend/$maxY;
      $urx = ($aSpace+$bSpace)/$maxX; $ury = $ascend/$maxY; $tx = 0; $ty = $ascend - 1;
    }
    elsif ( $FNC->[1] eq "05" ) {
     #compensare per misure relative /prima o dopo ???
      $aref = $FNM->[$fni];
     ($w, $h, $fm) =  (@{$aref}[0..2]); $w+=1; $h+=1;
      $aref = $FNI->[$fni];
     ($gid, $inc, $ascend, $descend, $bOffset) = (@{$aref}[0..3, 10]);  
     ($aSpace, $bSpace, $cSpace) = (@{$aref}[6..8]); 
      $PointsX{ibm2adobe($gid)} = $inc;
      $inc = $inc/$maxX;
      $llx = $aSpace/$maxX; $lly = -$descend/$maxY; 
      if (abs($lly) > 1.5) {
        $descend = (1 - $ascend/1000)*$h; $lly = -$descend/$maxY;
      } 
      $urx = ($aSpace+$bSpace)/$maxX; $ury = $ascend/$maxY;
      $tx = -$aSpace; $ty = $ascend - 1;
    }
    for ($llx, $lly, $urx, $ury) {
      warn "$_ =============\n" if abs($_) > 1.5;
    }
    my $adobe_gid = ibm2adobe($gid);

    $br=ceil($w/8)*8; $bt = $br * $h;
    $hstr = unpack("H*", substr($$FNG,$fm,ceil($bt/8)));
    ### test for relative metrics with raster
    if ( $FNC->[4] eq "02" ) {
      $tx = $tx/1000 * $FND->[4]/3/$scaleX; $ty = $ty/1000 * $FND->[4]/3/$scaleY; 
    }
    $CharData .= " /$adobe_gid [" . "$inc $llx $lly $urx $ury $w $h $tx $ty <$hstr>]\n";
    $Metrics .= " /$adobe_gid [" . $llx*1000 . " " . $inc*1000 . "]\n";
  
    if ( $gid eq 'LM020000' and $adobe_gid eq 'M' ) {
      $Meight = ($ury-$lly)+(1/$maxY);
    }
  
    $bits = unpack("B*", substr($$FNG,$fm, ceil($bt/8))); $bits =~ tr/0/./;
  }
  
  $os->add("<<");

  my ($OutlineFont, $MapTo) = $self->MapToOutlineFont(); $os->add("/MapTo /$MapTo");
  
  $os->add("/IsFixedPitch ". (($IsFixedPitch) ? 'true' : 'false'));
  if ( $OutlineFont and $Meight != 0 ) {
    $os->add("/Meight $Meight");
  }
    
  if ( $FNC->[4] eq "00" ) {
    ($extX, $extY) = ($maxX*$scaleX, $maxY*$scaleY);
  }
  elsif ( $FNC->[4] eq "02" ) { 
    $extX = $extY = $FND->[4]/10/0.3;
  }          
  else {
    ($extX, $extY) = map {$_/10/0.3} @$FND[4,5]; #todo: what ???
  }

  if ( $OutlineFont ) {
    if (!$IsFixedPitch ) {
    $os->add("/OutlineMatrix [$extX 0 0 -$extY 0 0]"); 
      $os->add("/Metrics\n<<\n$Metrics>>"); 
    }
    else {
      if ($FNC->[4] eq "02") {
        $extY = $FND->[4]/10/0.3; $extX = $extY * $FNOe->{'00000000'}[3]/1000;
      }
      $os->add("/OutlineMatrix [$extX 0 0 -$extY 0 0]"); 
    }
  }
  else {
    $os->add( 
      "/ImageMaskMatrix [$extX 0 0 -$extY 0 0]", "/Scale [$scaleX $scaleY]", "/CharData\n<<\n$CharData>>"
    );
  }

  $os->add(">>");
}

sub TranslateFont {
  my ($self, $res) = @_; my ($is, $os) = @{$self}{qw(is os)}; $self->{res} = $res;
  die "====== ?????" if $res =~ /^\s*$/;

  while(1) {
    my ($r5atype, $r5adata) = $is->readAfpBlk(); 
    last unless $r5atype;

    $self->ExpandAfp($r5atype, $r5adata); 
    
    last if $r5atype =~ /d3a989|d3a98a|d3a987/; 
  }

  return $self;
}

sub czmap {
  my $self = shift; 
  my ($os, $czmap) = @{$self}{qw(os czmap)}; return undef if !keys(%$czmap); 
  
  my @lcz = sort {$a cmp $b} keys(%$czmap);

  $os->add("","true setglobal"); for (@lcz) { $os->add("() ($_) a.execres"); } $os->add("false setglobal", "");

  $os->add("/czmap", "<<"); for (@lcz) { $os->add(" /$_ \/$czmap->{$_}"); } $os->add(">>", "def", "");
}

sub new {
  $main::veryverbose && print "LOG_GIU",$initialized, File::Basename::dirname( __FILE__ ), "\n";
#  if (!$initialized) {
#    $main::veryverbose && print "LOG_GIU", File::Basename::dirname( __FILE__ ), "\n"; 
#    open(my $INPUT, "<". File::Basename::dirname( __FILE__ ) ."/charsetMap.txt") or die("CharsetMap.txt OPEN Error $!");
#    
#    while(<$INPUT>) {
#      next if $_ =~ /^#/;
#      if (/(\w+) .* (CZ\w{4}(?:Bold)?) /) {
#        $charsetMap{$1} = $2;
#      }
#      elsif (/(\w+) .* ((?:Nimbus|Futura|Friz|URW|Courier|(?:\w+BT)|AGara)[\w-]*) /) {
#        $charsetMap{$1} = $2;
#      } 
#      elsif (/^(\w+) .* ([\w-]+) +HELVETICA/) {
#        $charsetMap{$1} = $2;
#      }
#    }
#    
#    close($INPUT);
#  }
  my ($class, $is, $os, $tr) = @_; 
  my $self = bless { is => $is, os => $os, tr => $tr, czmap => {} }, $class;
  $main::veryverbose && print join('; ', map { "$_: ". ref($self->{$_}) . (defined($self->{$_}) ? '(OK)' : '(UNDEF)' ) } keys %$self), "\n";
  return $self;
}


__END__

