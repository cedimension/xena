  
package XReport::AFP::PfbFont;

use strict;

use FileHandle;
use File::Basename;

use Convert::IBM390 qw(:all);

use XReport::AFP::CharsetMap qw($adobe2ibm);

sub _5a {
  return "\x5a". pack("n", length($_[0])+2). $_[0]; 
}

sub MAX_1 {
  $_[0] = $_[1] if $_[1] > $_[0];
}

sub MIN_1 {
  $_[0] = $_[1] if $_[1] < $_[0];
}

my %only_chars = (
  GA010000 => 1,
  GD010000 => 1,
  GE010000 => 1,
  GF010001 => 1,
  GF020000 => 1,
  GG020000 => 1,
  GM010000 => 1,
  GO320000 => 1,
  GP010000 => 1,
  GS010000 => 1,
  GS020000 => 1,
  GT010000 => 1,
  GT620000 => 1,
  LA010000 => 1,
  LA020000 => 1,
  LA110000 => 1,
  LA120000 => 1,
  LA130000 => 1,
  LA140000 => 1,
  LA150000 => 1,
  LA160000 => 1,
  LA170000 => 1,
  LA180000 => 1,
  LA190000 => 1,
  LA200000 => 1,
  LA270000 => 1,
  LA280000 => 1,
  LA510000 => 1,
  LA520000 => 1,
  LB010000 => 1,
  LB020000 => 1,
  LC010000 => 1,
  LC020000 => 1,
  LC410000 => 1,
  LC420000 => 1,
  LD010000 => 1,
  LD020000 => 1,
  LD610000 => 1,
  LD620000 => 1,
  LD630000 => 1,
  LD640000 => 1,
  LE010000 => 1,
  LE020000 => 1,
  LE110000 => 1,
  LE120000 => 1,
  LE130000 => 1,
  LE140000 => 1,
  LE150000 => 1,
  LE160000 => 1,
  LE170000 => 1,
  LE180000 => 1,
  LF010000 => 1,
  LF020000 => 1,
  LF510000 => 1,
  LF530000 => 1,
  LF550000 => 1,
  LF570000 => 1,
  LF590000 => 1,
  LG010000 => 1,
  LG020000 => 1,
  LH010000 => 1,
  LH020000 => 1,
  LI010000 => 1,
  LI020000 => 1,
  LI110000 => 1,
  LI120000 => 1,
  LI130000 => 1,
  LI140000 => 1,
  LI150000 => 1,
  LI160000 => 1,
  LI170000 => 1,
  LI180000 => 1,
  LI510000 => 1,
  LI610000 => 1,
  LJ010000 => 1,
  LJ020000 => 1,
  LK010000 => 1,
  LK020000 => 1,
  LL010000 => 1,
  LL020000 => 1,
  LL610000 => 1,
  LL620000 => 1,
  LM010000 => 1,
  LM020000 => 1,
  LN010000 => 1,
  LN011000 => 1,
  LN020000 => 1,
  LN190000 => 1,
  LN200000 => 1,
  LO010000 => 1,
  LO020000 => 1,
  LO110000 => 1,
  LO120000 => 1,
  LO130000 => 1,
  LO140000 => 1,
  LO150000 => 1,
  LO160000 => 1,
  LO170000 => 1,
  LO180000 => 1,
  LO190000 => 1,
  LO200000 => 1,
  LO510000 => 1,
  LO520000 => 1,
  LO610000 => 1,
  LO620000 => 1,
  LP010000 => 1,
  LP020000 => 1,
  LQ010000 => 1,
  LQ020000 => 1,
  LR010000 => 1,
  LR020000 => 1,
  LS010000 => 1,
  LS020000 => 1,
  LS210000 => 1,
  LS220000 => 1,
  LS610000 => 1,
  LT010000 => 1,
  LT020000 => 1,
  LT630000 => 1,
  LT640000 => 1,
  LU010000 => 1,
  LU020000 => 1,
  LU110000 => 1,
  LU120000 => 1,
  LU130000 => 1,
  LU140000 => 1,
  LU150000 => 1,
  LU160000 => 1,
  LU170000 => 1,
  LU180000 => 1,
  LV010000 => 1,
  LV020000 => 1,
  LW010000 => 1,
  LW020000 => 1,
  LX010000 => 1,
  LX020000 => 1,
  LY010000 => 1,
  LY020000 => 1,
  LY110000 => 1,
  LY120000 => 1,
  LY170000 => 1,
  LY180000 => 1,
  LZ010000 => 1,
  LZ020000 => 1,
  LZ210000 => 1,
  LZ220000 => 1,
  ND010000 => 1,
  ND011000 => 1,
  ND020000 => 1,
  ND021000 => 1,
  ND030000 => 1,
  ND031000 => 1,
  ND040000 => 1,
  ND041000 => 1,
  ND050000 => 1,
  ND051000 => 1,
  ND060000 => 1,
  ND061000 => 1,
  ND070000 => 1,
  ND071000 => 1,
  ND080000 => 1,
  ND081000 => 1,
  ND090000 => 1,
  ND091000 => 1,
  ND100000 => 1,
  ND100008 => 1,
  ND100009 => 1,
  ND101000 => 1,
  NF010000 => 1,
  NF040000 => 1,
  NF050000 => 1,
  NF180000 => 1,
  NF190000 => 1,
  NF200000 => 1,
  NF210000 => 1,
  SA000000 => 1,
  SA001000 => 1,
  SA010000 => 1,
  SA011000 => 1,
  SA020000 => 1,
  SA030000 => 1,
  SA040000 => 1,
  SA050000 => 1,
  SA060000 => 1,
  SA070000 => 1,
  SA160000 => 1,
  SA380000 => 1,
  SA420000 => 1,
  SA440000 => 1,
  SA450000 => 1,
  SA480000 => 1,
  SA520000 => 1,
  SA530000 => 1,
  SA540000 => 1,
  SA700000 => 1,
  SA790000 => 1,
  SA800000 => 1,
  SC010000 => 1,
  SC020000 => 1,
  SC030000 => 1,
  SC040000 => 1,
  SC050000 => 1,
  SC060000 => 1,
  SC070000 => 1,
  SC120000 => 1,
  SC200000 => 1,
  SD110000 => 1,
  SD130000 => 1,
  SD150000 => 1,
  SD150100 => 1,
  SD150300 => 1,
  SD170000 => 1,
  SD190000 => 1,
  SD190100 => 1,
  SD190300 => 1,
  SD210000 => 1,
  SD230000 => 1,
  SD250000 => 1,
  SD270000 => 1,
  SD290000 => 1,
  SD310000 => 1,
  SD410000 => 1,
  SD430000 => 1,
  SD470000 => 1,
  SD630000 => 1,
  SF010000 => 1,
  SF020000 => 1,
  SF030000 => 1,
  SF040000 => 1,
  SF050000 => 1,
  SF060000 => 1,
  SF070000 => 1,
  SF080000 => 1,
  SF090000 => 1,
  SF100000 => 1,
  SF110000 => 1,
  SF140000 => 1,
  SF150000 => 1,
  SF160000 => 1,
  SF190000 => 1,
  SF200000 => 1,
  SF210000 => 1,
  SF220000 => 1,
  SF230000 => 1,
  SF240000 => 1,
  SF250000 => 1,
  SF260000 => 1,
  SF270000 => 1,
  SF280000 => 1,
  SF360000 => 1,
  SF370000 => 1,
  SF380000 => 1,
  SF390000 => 1,
  SF400000 => 1,
  SF410000 => 1,
  SF420000 => 1,
  SF430000 => 1,
  SF440000 => 1,
  SF450000 => 1,
  SF460000 => 1,
  SF470000 => 1,
  SF480000 => 1,
  SF490000 => 1,
  SF500000 => 1,
  SF510000 => 1,
  SF520000 => 1,
  SF530000 => 1,
  SF540000 => 1,
  SF570000 => 1,
  SF580000 => 1,
  SF590000 => 1,
  SF600000 => 1,
  SF610000 => 1,
  SM010000 => 1,
  SM020000 => 1,
  SM030000 => 1,
  SM040000 => 1,
  SM050000 => 1,
  SM060000 => 1,
  SM070000 => 1,
  SM080000 => 1,
  SM100000 => 1,
  SM110000 => 1,
  SM120000 => 1,
  SM130000 => 1,
  SM140000 => 1,
  SM150000 => 1,
  SM170000 => 1,
  SM180000 => 1,
  SM190000 => 1,
  SM200000 => 1,
  SM210000 => 1,
  SM240000 => 1,
  SM250000 => 1,
  SM280000 => 1,
  SM290000 => 1,
  SM300000 => 1,
  SM310000 => 1,
  SM320000 => 1,
  SM330000 => 1,
  SM340000 => 1,
  SM350000 => 1,
  SM470000 => 1,
  SM490000 => 1,
  SM520000 => 1,
  SM530000 => 1,
  SM540000 => 1,
  SM560000 => 1,
  SM570000 => 1,
  SM570001 => 1,
  SM580000 => 1,
  SM590000 => 1,
  SM600000 => 1,
  SM630000 => 1,
  SM650000 => 1,
  SM660000 => 1,
  SM680000 => 1,
  SM690000 => 1,
  SM700000 => 1,
  SM750000 => 1,
  SM750002 => 1,
  SM760000 => 1,
  SM770000 => 1,
  SM780000 => 1,
  SM790000 => 1,
  SM900000 => 1,
  SM910000 => 1,
  SM930000 => 1,
  SP010000 => 1,
  SP020000 => 1,
  SP030000 => 1,
  SP040000 => 1,
  SP050000 => 1,
  SP060000 => 1,
  SP061000 => 1,
  SP070000 => 1,
  SP071000 => 1,
  SP080000 => 1,
  SP081000 => 1,
  SP090000 => 1,
  SP100000 => 1,
  SP110000 => 1,
  SP111000 => 1,
  SP120000 => 1,
  SP120001 => 1,
  SP130000 => 1,
  SP140000 => 1,
  SP150000 => 1,
  SP160000 => 1,
  SP170000 => 1,
  SP180000 => 1,
  SP190000 => 1,
  SP200000 => 1,
  SP210000 => 1,
  SP220000 => 1,
  SP230000 => 1,
  SP260000 => 1,
  SP270000 => 1,
  SP280000 => 1,
  SP300000 => 1,
  SP310000 => 1,
  SP320000 => 1,
  SP330000 => 1,
  SS000000 => 1,
  SS010000 => 1,
  SS020000 => 1,
  SS030000 => 1,
  SS040000 => 1,
  SS050000 => 1,
  SS260000 => 1,
  SS270000 => 1,
  SS680000 => 1,
  SV040000 => 1,
  SV520000 => 1,
);

my @keys_afm = (
  'StartFontMetrics',
  'Comment UniqueID',
  'Comment Panose',
  'Comment Copyright',
  'FullName',
  'FontName',
  'FamilyName',
  'Weight',
  'Notice',
  'Version',
  'IsFixedPitch',
  'ItalicAngle',
  'FontBBox',
  'Ascender',
  'Descender',
  'XHeight',
  'CapHeight',
  'UnderlinePosition',
  'UnderlineThickness',
  'StdHW',
  'StdVW',
  'EncodingScheme',
  'StartCharMetrics'
);

my $lx_kval = eval "qr/(". join("|", @keys_afm). ") +(.*)/i"; 

my $lx_gid  = qr/N *(\w+)/; my $lx_dim = qr/(-?\d+)/;
my $lx_bbox = qr/B *$lx_dim *$lx_dim *$lx_dim *$lx_dim/;
my $lx_char = qr/C *$lx_dim *; *WX *$lx_dim *; *$lx_gid *; *$lx_bbox *;/;


sub ParseAfm {
  my $self = shift; my $FileName = $self->{'FileName'}; $FileName .= '.afm'; my $metrics = {};
  
  my $INPUT = FileHandle->new("<$FileName") 
     or
  die("AFM FILE INPUT OPEN ERROR \"$FileName\" $!");
  
  while(my $l = $INPUT->getline()) {
	my ($key, $val) = $l =~ /$lx_kval/ 
	  or
    die("INVALID AFM LINE OR KEYWORD NOT MANAGED at line: $l");

    $key =~ s/^Comment //; $metrics->{$key} = $val;
	  
	if ( $key eq "StartCharMetrics" ) {
	  my $cmetrics = {};
	  for (1..$val) {
		my $l = $INPUT->getline(); 

		my ($c, $wwx, $agid, $llx, $lly, $urx, $ury) = $l =~ /$lx_char/;

        exists($adobe2ibm->{$agid})
          or 
        do {print("$agid IBM MAPPING MISSING !!!!\n"); next };

		next if !exists($only_chars{$adobe2ibm->{$agid}});

		$cmetrics->{$agid} = [$c, $wwx, $llx, $lly, $urx, $ury] 
	  }
	  @{$self}{qw(FontMetrics CharMetrics)} = ($metrics, $cmetrics); last;
	}
  }
  
  $INPUT->close(); 
}

sub toString {
  my $self = shift;
  
  my $metrics = $self->{'FontMetrics'};

  for (sort(keys(%$metrics))) {
    print "$_ : $metrics->{$_}\n";
  }

  my $cmetrics = $self->{'CharMetrics'};

  for (sort(keys(%$cmetrics))) {
    print "$_ : ", join(",", @{$cmetrics->{$_}}), "\n";
  }
}


sub toAfp {
  my $self = shift; my ($FileName, $args) = @{$self}{qw(FileName args)}; $FileName .= '.pfb';

  my $INPUT = FileHandle->new("$FileName", "r")
    or
  die("PFB FILE INPUT OPEN ERROR \"$FileName\" $!"); 

  binmode($INPUT); $INPUT->read(my $tpfb, 16000000); $INPUT->close();
  
  my ($metrics, $cmetrics) = @{$self}{qw(FontMetrics CharMetrics)}; 

  my @charList = 
    sort 
      {$adobe2ibm->{$a} cmp $adobe2ibm->{$b}} 
    keys(%$cmetrics)
  ;

  my $totChars = scalar(@charList);

  my (
	$FontName, 
    $Weight, $ItalicAngle, $IsFixedPitch,   
    $UnderlinePosition, $UnderlineThickness, $Panose
  ) 
  = @{$metrics}{qw(FontName Weight ItalicAngle IsFixedPitch UnderlinePosition UnderlineThickness Panose)};
  
  #---------------- FONT CHARACTER SET (PREPARE) ----------------------------------------------- 

  my @c = (0,0,0,0); my $oidName = basename($FileName); 

  for (my ($i, $maxi) = (0, length($tpfb)); $i<$maxi; $i+=4) {
	my @i = unpack("C4",substr($tpfb,$i,4));
	for (0..3) {
	  $c[$_] += $i[$_]; $c[$_] %= 256;
	}
  }
  my $checksum = pack("C4", @c);
 
  my $FNG = pack(
   "Na4na*", 
    10+length($oidName)+length($tpfb), 
	$checksum, length($oidName)+2, $oidName.$tpfb
  );

 #---------------- FONT INDEX SECTION (PREPARE) -----------------------------------------------
 
  my @FNIm = ({}, {}, {}, {}); my @FNI = ('', '', '', ''); 

  for my $agid (@charList) {
	my $igid = $adobe2ibm->{$agid};

    my ($wx, $llx, $lly, $urx, $ury) = @{$cmetrics->{$agid}}[1..5];

    my (
      $cincr, 
      $aeight, $ddepth,
      $aspace, $bspace, $cspace
    ) = 
    ($wx, $ury, 0-$lly, $llx, $urx-$llx, $wx-$urx, $ury); 

    my (
      $_cincr, 
      $_aeight, $_ddepth,
      $_aspace, $_bspace, $_cspace
    ) =
    ($cincr, $aeight, $ddepth, $aspace, $bspace, $cspace); 

    $FNIm[0]->{$igid} = [$_cincr, $_aeight, $_ddepth, $_aspace, $_bspace, $_cspace, $_aeight];
	
	$FNI[0] .= packeb(
	 "E8Ss2S2sSsSs", 
	  $igid, $_cincr, $_aeight, $_ddepth, 0, 0, $_aspace, $_bspace, $_cspace, 0, $_aeight
	);

    (
      $_cincr, 
      $_aeight, $_ddepth,
      $_aspace, $_bspace, $_cspace
    ) =
    (100+$aeight+$ddepth+100, int($bspace/2), $bspace-int($bspace/2), 100, $aeight+$ddepth, 100);

    $FNIm[1]->{$igid} = [$_cincr, $_aeight, $_ddepth, $_aspace, $_bspace, $_cspace, $_aeight];
	
	$FNI[1] .= packeb(
	 "E8Ss2S2sSsSs", 
	  $igid, $_cincr, $_aeight, $_ddepth, 0, 0, $_aspace, $_bspace, $_cspace, 0, $_aeight
	);
    
    (
      $_cincr, 
      $_aeight, $_ddepth,
      $_aspace, $_bspace, $_cspace
    ) =
    ($cincr, $ddepth, $aeight, $cspace, $bspace, $aspace);

    $FNIm[2]->{$igid} = [$_cincr, $_aeight, $_ddepth, $_aspace, $_bspace, $_cspace, $_aeight];

	$FNI[2] .= packeb(
	 "E8Ss2S2sSsSs", 
	  $igid, $_cincr, $_aeight, $_ddepth, 0, 0, $_aspace, $_bspace, $_cspace, 0, $_aeight
	);

    (
      $_cincr, 
      $_aeight, $_ddepth,
      $_aspace, $_bspace, $_cspace
    ) =
    (100+$aeight+$ddepth+100, $bspace-int($bspace/2), int($bspace/2), 100, $aeight+$ddepth, 100);

    $FNIm[3]->{$igid} = [$_cincr, $_aeight, $_ddepth, $_aspace, $_bspace, $_cspace, $_aeight];

	$FNI[3] .= packeb(
	 "E8Ss2S2sSsSs", 
	  $igid, $_cincr, $_aeight, $_ddepth, 0, 0, $_aspace, $_bspace, $_cspace, 0, $_aeight
	);
  }

 #---------------- FONT NAME MAP SECTION (PREPARE) --------------------------------------------
 
  my ($FNN, $FNN2, $FNN3, $FNN3off) = ("\x02\x03", '', '', 2+$totChars*12);
 
  for my $agid (@charList) {
    my $igid = $adobe2ibm->{$agid}; my $ll = 1+length($agid);

    $FNN2 .= packeb("E8m4", $igid, $FNN3off); 
    $FNN3 .= pack("Ca*", $ll, $agid); 

    $FNN3off += $ll;
  }

  $FNN .= $FNN2; $FNN3;

 
  #---------------- FONT ORIENTATION AND POSITION (PREPARE) ------------------------------------  

  my @tbloc = qw(00000000 00002d00 00005a00 00008700); my @tblof = qw(8 20 48 60); # to do: test for kerning
  
  my ($xaeight, $cmaeight) = map{$_->[1]} @{$FNIm[0]}{@{$adobe2ibm}{qw(x M)}};  # here or there ????
  
  my ($FNO, $FNP) = ('', '');

  for my $orient (0..3) {
    my $FNI = $FNIm[$orient]; 

    my ($mxcincr, $mxaeight, $mxddepth, $mnaspace) = (-99999, -99999, -99999, 99999);

    for my $agid (@charList) {
      my ($cincr, $aeight, $ddepth, $aspace, $bspace, $cspace, $boff) = @{$FNI->{$adobe2ibm->{$agid}}};

      MAX_1($mxcincr, $cincr); MAX_1($mxaeight, $aeight); MAX_1($mxddepth, $ddepth); MIN_1($mnaspace, $aspace); 
    }

    my $mxbextent = $mxaeight + ($mxddepth > 0 ? $mxddepth : 0);

    my ($sp_cincr,$zero_cincr) = map{$_->[0]} @{$FNI}{@{$adobe2ibm}{qw(space zero)}};
   #my ($xaeight, $cmaeight) = map{$_->[1]} @{$FNI}{@{$adobe2ibm}{qw(x M)}}; ??

    my ($oCode, $oFlags) = ($tbloc[$orient], $tblof[$orient]);

    $FNO .= pack(
	 "H8n4H2H2n6",
      $oCode,       # Orientation Code
      $mxaeight,    # Maximum Baseline Offset 
      $mxcincr,     # Maximun Character Increment 
      $sp_cincr,    # Space Character Increment 
      $mxbextent,   # Maximum Baseline extent  
      $oFlags,      # Orientation Flags
      "00",         # Reserved
      1000,         # Em-Space Increment
      0,            # Reserved
      $zero_cincr,  # Figure Space Increment (number increment) -> di zero_cincr
      0,            # Nominal Character Increment as is
      1070,         # Default Baseline Increment 
      $mnaspace     # Minimum A-space --> min llx da BBOX ->aspace max_ddepth
    );  

    $FNP .= pack(
	 "H4n4H10H2H2nH2n",
     "0000",
      $xaeight,     # Lowercase Height x height
      $cmaeight,    # Cap-M Height --> M height
      $mxaeight,    # Maximum Ascender Height
      $mxddepth,    # Descender Depth 
      "00"x5,       # Reserved
      "01",         # Retired (as is) 
      "00",         # Reserved
      $UnderlineThickness, # Underscore Width (units) 
      "00",         # Underscore Width (fraction) 
      -$UnderlinePosition # Underscore Position (positive if below)
    );
  }
   

 #---------------- FONT DESCRIPTOR SECTION ----------------------------------------------------
 
  my $weight_class = $Weight ne 'Bold' ? 5 : 7;
   #X'01'   Ultralight
   #X'02'   Extralight
   #X'03'   Light
   #X'04'   Semilight
   #X'05'   Medium (normal)
   #X'06'   Semibold
   #X'07'   Bold
   #X'08'   Extrabold
   #X'09'   Ultrabold.

  my $width_class = 5;
   #X'01'   Ultracondensed
   #X'02'   Extracondensed
   #X'03'   Condensed
   #X'04'   Semicondensed
   #X'05'   Medium (normal)
   #X'06'   Semiexpanded
   #X'07'   Expanded
   #X'08'   Extraexpanded
   #X'09'   Ultraexpanded 

  my $FND = _5a(
	packeb("H12E32", "d3a689000000", $FontName) .
	pack(
	 "C2n6C3a15a2a10n2",
	  $weight_class, $width_class,
     #... all zero for outline font
      0,  # Maximum Vertical Size  10th of a point
      0,  # Nominal Vertical Size  10th of a point
      0,  # Minimum Vertical Size  10th of a point
      0,  # Maximum Horizontal Size 1440th (of an Inch ??)
      0,  # Nominal Horizontal Size 1440th (of an Inch ??)
      0,  # Minimum Horizontal Size 1440th (of an Inch ??) 
     #... all zero (no ISO)
      0,  # Class ISO
      0,  # SubClass ISO
      0,  # Specific Group ISO
	 #---------------------------------------
     "\x00"x15,  # Reserved
     "\x00"x2,   # Design Flags
     "\x00"x10,  # Reserved
     #... where to find ???------------
      0,  # Graphic Character Set GID --> ??
      0   # Font Typeface GID --> ??
    )
  );

 
  #---------------- FONT CONTROL ---------------------------------------------------------------
  
  my ($datLen, $fnn_dc, $fnn_nc) = (length($FNG), length($FNN)+length($FNN3), $totChars);

  my $FNC = _5a(
	pack("H12", "d3a789000000") .
	pack(
	 "a6n2nnCCaa3CCa1a1a2a2Na3CNn", 
	  "\x01".   # retired (as is)
      "\x1f".   # pfb type1
      "\x00".   # reserved (as is)
      "\x00".   # FntFlags (?? as is)
      "\x02".   # 02 relative metrics as is
      "\x02",   # 02 relative metrics
       1000,    # 1000 units per Em)
       1000,    # 1000 units per Em)
       0,       # MaxBoxWd ?? as is ??)
       0,       # MaxBoxHt ?? as is ??)
       26,      # FNO repeating group length
       28,      # FNI repeating group length
      "\x00",   # PatAlign (00 1 byte align)
      "\x00"x3, # Raster Pattern Data Count -> 0 for pfb (no raster)
       22,      # FNP Repeating group length 
        0,      # FNM Repeating group length (0 No Raster Data)
	  "\x00",   # Shape resolution, ten inches
	  "\x00",   # Shape resolution, ten inches
      "\x00"x2, # No resolution provided
      "\x00"x2, # No resolution provided
       $datLen, # Outline Pattern Data Count (pfb file length+17)
	  "\x00"x3, # Reserved  
	  #---- Font Name Map (FNN) data
       12,      # FNN Repeating Group Count 
       $fnn_dc, # FNN Data Count 
       $fnn_nc  # FNN Name Count
    )
  ); 

 
  #---------------- PRODUCE THE AFP STREAM -----------------------------------------------------
  my $iFontName = $args->{'iFontName'} or die("iFontName PARAMETER MISSING."); 
  
  my $BFN = _5a(packeb("H12E8", "d3a889000000", $iFontName));

  $FNO = _5a(pack("H12", "d3ae89000000") . $FNO); 
  $FNP = _5a(pack("H12", "d3ac89000000") . $FNP);
  
  for (0..3) {
	$FNI[$_] = _5a(pack("H12", "d38c89000000") . $FNI[$_]);
  }

  my $FNI = join('',@FNI);
  
  $FNN = _5a(pack("H12", "d3ab89000000").  $FNN . $FNN3);
  
  do {
    my $tFNG=$FNG; my $ltFNG=length($tFNG); my $ls=11992; $FNG='';  
    for (my $i=0; $i < $ltFNG; $i+=$ls) {
	  $FNG .= _5a(
		pack("H12", "d3ee89000000") . 
		substr($tFNG,$i,$ls)
      );
	}
  };
  
  my $EFN = _5a(packeb("H12E8", "d3a989000000", $iFontName));
  
  return $BFN. $FND. $FNC. $FNO. $FNP. $FNI. $FNN. $FNG. $EFN;
}

sub new {
  my ($ClassName, $FileName, %args) = @_; my $self;
  
  my $self = {
	FileName => $FileName, args => \%args  
  }; 
  
  bless $self, $ClassName; $self->ParseAfm(); return $self;
}

1;

__END__

#-------------------------------------------------------------------------------------------------
my $t = <<EOF;
d3a889 Begin Font (BFN) (37)
>> c3e9c8f2f0f0404011620040f9f8f1f5f2f0f0f0f0f0f0f0f0066301000000
... to font name
CZH200  

... triplet 62 00 local date and time stamp (OPTIONAL)
16 620040f9f8f1f5f2f0f0f0f0f0f0f0f006
... triplet 63 01 CRC (OPTIONAL) 
5 6301000000

... nop description ascii 
d3eeee No Operation (57)
>> 49424d2054797065205472616e73666f726d657220322e312030342f31342f313939382031313a30383a333120435a48323030
ρβ(θ`ψΑθΚ/>ΛΓ?Κ_ΑΚδ!η
		
... nop description ebcdiic
d3eeee No Operation (57)
>> c9c2d440e3a8978540e3998195a286969994859940f24bf140f0f461f1f461f1f9f9f840f1f17af0f87af3f140c3e9c8f2f0f0
IBM Type Transformer 2.1 04/14/1998 11:08:31 CZH200
EOF

  $t = <<EOF;
d3a689 Font Descriptor (FND) (86)
>> c8c5d3e5c5e3c9c3c140d3c1e3c9d5f140404040404040404040404040404040
0505000000000000000000000000000000000000000000000000000000000000000000000000000000000000
07f90900
HELVETICA LATIN1    (32 chars) ebcdiic -->  FullName ??          
05  weight class 
05  width class
... all zero for outline font
0 Maximum Vertical Size  10th of a point
0 Nominal Vertical Size  10th of a point
0 Minimum Vertical Size  10th of a point
0 Maximum Horizontal Size 1440th of an Inch ??
0 Nominal Horizontal Size 1440th of an Inch ??
0 Minimum Horizontal Size 1440th of an Inch ?? 
... all zero (no ISO)
00  Class ISO
00  SubClass ISO
00  Specific Group ISO
000000000000000000000000000000 Reserved
0000  Design Flags
00000000000000000000  Reserved
... where to find ???
2041 Graphic Character Set GID --> ?? (0)
2304 Font Typeface GID --> ?? (0)
EOF

$t = <<EOF;
d3a789 Font Control (FNC) (48)
>> 011f0000020203e803e8000000001a1c00000000160000000000000000018e4f0000000c00001cf30176
01  retired (as is)
1f  (pfb type1)
00  reserved (as is)
00  FntFlags (?? as is)
02  (02 relative metrics as is)
02  (02 relative metrics)
03e8  (1000 units per Em)
03e8  (1000 units per Em)
0  (MaxBoxWd ?? as is ??)
0  (MaxBoxHt ?? as is ??)
1a  FNO repeating group length
1c  FNI repeating group length
00  PatAlign (00 1 byte align)
000000  Raster Pattern Data Count -> 0 for pfb (no raster)
16  FNP Repeating group length 
00  FNM Repeating group length (00 No Raster Data)
00  Shape resolution (00 10inch)
00  Shape resolution (00 10inch)
0   (00 = No shape resolution)
0   (00 = No shape resolution)
101967 Outline Pattern Data Count (file length = 101950)
000000  Reserved

... FNN data
12  FNN Repeating Group Length
7411  FNN Data Count --> FNN total length
374 FNN Name Count --> number of gids (characters)
EOF

$t = <<EOF;
FontBBox -174 -299 1237 944
d3ae89 Font Orientation (FNO) (110)
00000000 Orientation 
944  Maximum Baseline Offset ury
1015  Maximun Character Increment W
278  Space Character Increment space W of space
1232  Maximum Baseline extent ury max - min ury (if ury neg)
08  Orientation Flags fni = 0 index and kernig
    20 fni=1 no kern
	48 fni=2 kern
	60 fni=3 no kern
00  Reserved
1000  Em-Space Increment
0 Reserved
556 Figure Space Increment (number increment)
0  Nominal Character Increment as is
1070 Default Baseline Increment --> ??
-174 Minimum A-space --> min llx da BBOX

00000000  944  1015  278  1232  08  00  1000  0  556  0  1070  -174
... other orientations
00002d00  550  1412  278  1101  20  00  1000  0  556  0  1070  0 ruotato 90
00005a00  288  1015  278  1232  48  00  1000  0  556  0  1070  -169
00008700  551  1412  278  1101  60  00  1000  0  556  0  1070  0 ruotato -90

00000000  944  1015  278  1232  08  00  1000  0  556  0  1070  -174
00002d00  550  1412  278  1101  20  00  1000  0  556  0  1070  0
00005a00  288  1015  278  1232  48  00  1000  0  556  0  1070  -169
00008700  551  1412  278  1101  60  00  1000  0  556  0  1070  0
EOF

$t = <<EOF;
d3ac89 Font Position (FNP) (94)
0  Reserved
400 Lowercase Height
600 Cap-M Height --> M height
944 Maximum Ascender Height --> Ascender ?
288 Maximum Descender Depth --> Descender ?
0000000000  Reserved
01 Retired (as is) 
00  Reserved
50  Underscore Width (units) --> UnderlineThickness
00  Underscore Width (fraction) as is
75  Underscore Position ???

... other orientations
0  400  600  550  551  0000000000  01  00  50  00  75
0  400  600  288  944  0000000000  01  00  50  00  75
0  400  600  551  550  0000000000  01  00  50  00  75
EOF

$t = <<EOF;
d38c89 Font Index (FNI) (10478)
GA010000  ibm gid
583  Character Increment --> wwx
539  Ascender Height --> urx
23  Descender Depth --> -lly
0000  Reserved
0  FNM Index (as is for outline fonts)
43  A-space --> llx
469  B-space --> urx-llx
71  C-space --> charincr - urx
0  Reserved
539 Baseline Offset (= Ascender Height ??)
EOF

$t = <<EOF;
... see afp.codepages.txt ..................
d3ab89 Font Name Map (FNN) (4496+2927)
02 IBM characted ID format (as is)
03 ASCII character (for pfb fonts)
GA010000 IBM gid
4490 offset to map
6 length (1+length(ascii gid))
alpha ascii gid
EOF

$t = <<EOF;
d3ee89 Font Patterns (FNG) (11998)
... pfb file
EOF

$t = <<EOF;
d3a989 End Font (EFN) (14)
>> c3e9c8f2f0f04040
EOF

#-------------------------------------------------------------------------------------------------

sub BeginFont {
$t = <<EOF;
d3a889 Begin Font (BFN) (37)
>> c3e9c8f2f0f0404011620040f9f8f1f5f2f0f0f0f0f0f0f0f0066301000000
CZH200  
16 620040f9f8f1f5f2f0f0f0f0f0f0f0f006
5 6301000000

d3eeee No Operation (57)
>> 49424d2054797065205472616e73666f726d657220322e312030342f31342f313939382031313a30383a333120435a48323030
ρβ(θ`ψΑθΚ/>ΛΓ?Κ_ΑΚδ!η
d3eeee No Operation (57)
>> c9c2d440e3a8978540e3998195a286969994859940f24bf140f0f461f1f461f1f9f9f840f1f17af0f87af3f140c3e9c8f2f0f0
IBM Type Transformer 2.1 04/14/1998 11:08:31 CZH200
EOF
}


sub FontDescriptor {
$t = <<EOF;
d3a689 Font Descriptor (FND) (86)

>> c8c5d3e5c5e3c9c3c140d3c1e3c9d5f140404040404040404040404040404040050500000000000000000000000000000000000000000000000000000000000000000000000000000000000007f90900
HELVETICA LATIN1                  
05  
05  
0  0  0  0  0  0  0000  0000  0000  0000  0000  0000  000000000000  0000  000000  000000  0  0000  07f90900

EOF
}

sub FontControl {
$t = <<EOF;
d3a789 Font Control (FNC) (48)
>> 011f0000020203e803e8000000001a1c00000000160000000000000000018e4f0000000c00001cf30176
01  
1f  
00  
00  
02  
02  
03e8  
03e8  0  0  1a  1c  00  000000  16  00  00  00  0  0  101967  000000  12  7411  374
EOF
}

sub FontOrientation {
$t = <<EOF;
d3ae89 Font Orientation (FNO) (110)
>> 0000000003b003f7011604d0080003e80000022c0000042eff5200002d00022605840116044d200003e80000022c0000042e000000005a00012003f7011604d0480003e80000022c0000042eff5700008700022705840116044d600003e80000022c0000042e0000
00000000  944  1015  278  1232  08  00  1000  0  556  0  1070  -174
00002d00  550  1412  278  1101  20  00  1000  0  556  0  1070  0
00005a00  288  1015  278  1232  48  00  1000  0  556  0  1070  -169
00008700  551  1412  278  1101  60  00  1000  0  556  0  1070  0
EOF
}


sub FontOrientation {
$t = <<EOF;
d3ac89 Font Position (FNP) (94)
>> 00000190025803b0012000000000000100003200004b0000019002580226022700000000000100003200004b000001900258012003b000000000000100003200004b0000019002580227022600000000000100003200004b
0  400  600  944  288  0000000000  01  00  50  00  75
EOF
}


sub FontIndex {
$t = <<EOF;
d38c89 Font Index (FNI) (10478)
GA010000  583  539  23  0000  0  43  469  71  0  539
EOF
}
