package XReport::AFP::Encoder;

use Convert::IBM390 qw(packeb);
use Carp;

sub mypackeb {
#  croak "packeb error\n" unless $_[0];
#  for (@_) {
#    die "packeb error\n" unless defined($_);
#  } 
  return packeb(@_);
}

sub _5a {
  return "\x5a". pack("n", length($_[0])+2). $_[0]; 
}

sub _catll1 {
  my $tll = '';
  for (@_) {
	$tll .= pack("C", length($_)+1) . $_;
  }
  return $tll;
}

sub _catll2 {
  my $tll = '';
  for (@_) {
	$tll .= pack("n", length($_)+2) . $_;
  }
  return $tll;
}

# progressivo di sf

#------------------------------------------------------------
# resource group
#------------------------------------------------------------

our %iResourceTypes = (
  Graphics    => "\x03",
  BarCode     => "\x05",
  Image       => "\x06",
  CharSet     => "\x40",
  CodePage    => "\x41",
  CodedFont   => "\x42", 
  Container   => "\x92",
  Document    => "\xa8",
  PageSegment => "\xfb",
  PageOverlay => "\xfc",
  PageDef     => "\xfd",
  FormDef     => "\xfe" 
);

sub bdt {
  my ($self, $docid) = (shift, shift); $docid ||= "AFPDOC";
  return _5a(
    mypackeb("H12E8H4", "d3a8a8000000", $docid, "0000") .
     #code page global identifier
    _catll1(pack("H2H4H4", "01", "ffff", "0118")) .
	 # function set specifier. required ????
    pack(
     "CH2H2H2H4H4"x4, 
      8,"21", "02", "00", "8000", "0000", 
      8,"21", "03", "00", "8000", "4000", 
      8,"21", "05", "00", "8000", "0000", 
      8,"21", "06", "00", "8000", "8000"
    ). 
	_catll1(mypackeb("H2e*", "65", "TEST GENERAZIONE DOCUMENTO AFP")) 
  );
}

#-- end document ----------
sub edt {
  my $self = shift; return _5a(pack("H12a8", "d3a9a8000000", "\xff"x8));
}

#-- begin page
sub bpg {
  my ($self, $pageid) = (shift, shift); $pageid ||= "PAGE";
  return _5a(
	mypackeb("H12E8", "d3a8af000000", $pageid)
  );
}

#-- page descriptor
sub pgd {
  my ($self, $sizeX, $sizeY) = (shift, shift, shift);
  return _5a(
    pack("H*", "d3a6af000000") .
    pack("H2 H2", "00", "00") .  # page unit base x, y (10 inch)
    pack("H4 H4", "0960", "0960") .   # units per base
#    pack("H2nH2n", "00", 2008, "00", 2840) . # Xsize, Ysize
    pack("H2nH2n", "00", $sizeX, "00", $sizeY) . # Xsize, Ysize
#    mypackeb("M3M3", 1984*6, 2806*6) .
    pack("H6", "00000")
  );
}

#-- end page
sub epg {
  my $self = shift;
  return _5a(
	pack("H12H16", "d3a9af000000", "ffffffffffffffff")
  );
}
  
#-- begin active environment group
sub bag {
  my $self = shift;
  return _5a(
	pack("H12", "d3a8c9000000")
  );
}
  
sub brg {
  my $self = shift;
  return _5a(
	pack("H12", "d3a8c6000000")
  );
}

#-#-- end active environment group
sub eag {
  my $self = shift;
  return _5a(
	pack("H12", "d3a9c9000000")
  ); 
}

#- end resource group
sub erg {
  my $self = shift;
  return _5a(
	pack("H12", "d3a9c6000000")
  );
}

#-- begin resource
sub br {
  my ($self, $restype, $resid) = (shift, shift, shift); 
  $restype = $iResourceTypes{$restype} 
    or
  die("UNKNOWN Resource Type DETECTED $restype FOR RESOURCE $resid");
  $resid = "RES0000" unless $resid;
  return _5a(
	mypackeb("H12E8H4", "d3a8ce000000", $resid, "0000").
    pack("Caaa*", 10, "\x21", $restype, "\x00"x7)
  );
}

#-- end resource
sub er {
#  my ($self, $resName) = @_;
#  return _5a(
#	pack("H12a8", "d3a9ce000000", "\xff"x8)
#  );
  my ($self, $eid) = @_; return _5a(mypackeb("H12E8", "d3a9ce000000", ($eid || "\xff"x8)));
}

sub bim {
  my ($self, $imid) = @_; return _5a(mypackeb("H12E8", "d3a8fb000000", ($imid || 'IM00000')));
}
 
#-- begin object environment group
sub bog {
  my ($self, $eid) = @_; return _5a(mypackeb("H12E8", "d3a8c7000000", ($eid || 'OG00000')));
}

#-- object area descriptor
sub obd {
  my ($self, $XoaUnits, $YoaUnits, $XoaSize, $YoaSize) = @_;
  return _5a(
	     pack("H*", "d3a66b000000") .
	     _catll1(
		     pack("H2H2", "43", "01"),
		     pack("H2H2H2nn", "4b", "00", "00", $XoaUnits, $YoaUnits),
		     pack("H2H2H2nH2n", "4c", "02", "00", $XoaSize, "00", $YoaSize),
		     pack("H2H2", "70", "00"),
		    )
	    );
}

#-- object area position
sub obp {
  my ($self, $cX, $cY, $lX, $lY) = @_;
  return _5a(
	     pack("H*", "d3ac6b000000") .
	     pack("CC", 1, 23) .                # area id + len (23)
	     mypackeb(
		    "M3M3H4H4H2M3M3H4H4H2",
		    $cX, $cY, "0000", "2d00", "00", # area position
		    $lX, $lY, "0000", "2d00", "01"  # content position
		   )
	    );
}

sub mio {
  my ($self, $maptype) = @_; $maptype = '20' if $maptype eq '';

=maptype
   X'10'          Position and trim
   X'20'          Scale to fit 
   X'30'          Center and trim 
   X'41'          Migration mapping option: Image 
                  point-to-pel.  See "Coexistence 
                  Parameters" in topic C.6.14 for 
                  a description.
   X'42'          Migration mapping option: Image 
                  point-to-pel with double dot.
                  See "Coexistence Parameters" in 
                  topic C.6.14 for a description. 
   X'50'          Replicate and trim 
   X'60'          Scale to fill 
   All others     Reserved 
=cut
  
  return _5a(
    pack("H*", "d3abfb000000") . _catll2(
	_catll1(
	  pack("H2H2", "04", $maptype) 
	)
  ));
}

#-- image data descriptor
sub idd {
  my ($self, $cX, $cY, $lX, $lY, $algExt) = @_;
  return _5a(
    pack("H*", "d3a6fb000000") .
	pack("H2 n4", "00", $cX, $cY, $lX, $lY) . $algExt 
  );
}

#-- end object environment group
sub eog {
  my ($self, $eid) = @_; return _5a(mypackeb("H12E8", "d3a9c7000000", ($eid || "\xff"x8)));
#  my $self = shift; return _5a(pack("H12a8", "d3a9c7000000", "\xff"x8));
}

sub iob {
  my ($self, $obId, $startX, $startY, $rotate) = @_;
  return _5a(
	     pack("H*", "d3afc3000000") 
	     . mypackeb("E8", $obId)
	     . pack(
		    "H2H2H2nH2nH8H2nH2nH2",
		    "00", "FB",                 # IOCA
		    "00",$startX, "00", $startY,             # area position
		    (
		     $rotate eq 'rightup' ? "87000000" :
		     $rotate eq 'leftup' ? "2d005a00"  :   # content orientation
		     $rotate eq 'downup' ? "5a008700"  :   # content orientation
		     "00002d00"
		    ),
		    "00", $startX, "00", $startY,             # content position
		    "01" )                # page or overlay coordinate system
	     . _catll1(
		       pack("H2H2H2H4H4", "4b", "00", "00", "0960", "0960"),
		       pack("H2H2", "70", "00"),
		      )
	    )
}

sub encodeImageContent {
  my ($self, $bands) = (shift, shift);
#  die ref($bands) . " " . scalar(@$bands) . "\n" . "=" x 70 , "\n" . unpack("H200", $bands->[0]) . "\n" . "=" x 70 , "\n"; 
  return join('',  map { _5a( pack("H*n/A*", 'd3eefb000000fe92', $_)) } unpack("(a8192)*", $bands->[0]) ) if scalar(@$bands) == 1;
  my $encoded = _5a( 
		    pack("H*", "d3eefb000000") .
		    pack(
			 "H2 C C H*",
			 "98", scalar(@$bands)+1, scalar(@$bands), "01" x scalar(@$bands) 
			)
		   );
  foreach my $bandnum ( 0..$#{$bands} ) {
    $encoded .= _5a( pack("H* C n/A*", 'd3eefb000000fe9c', $bandnum+1, $bands->[$bandnum]) );
  }
#  my $encoded = '';
#  foreach my $bandnum ( 0..$#{$bands} ) {
#    $encoded .= _5a( pack("H* n/A*", 'd3eefb000000fe92', $bands->[$bandnum]) );
#  }
  
  return $encoded;
}

#-- image picture data io
sub ipd {
  my ($self, $HRESOL, $VRESOL, $HSIZE, $VSIZE, $cmpr, $expdt) = splice(@_,0,6);
  my $bands = shift;
  # test image here or in the caller ????
#  my ($lrest, $ipdio, $at, $lseg) = (length($_[0]), '', 0, 0);
#  while($lrest > 0) {
#    $lseg = ($lrest > 16672 ? 16672 : $lrest); 
#    $ipdio .= _5a(
#		  pack("H*", "d3eefb000000") .
#		  pack("H4n", "fe92", $lseg) . 
#		  substr($_[0], $at, $lseg) 
#		 );
#    $at += $lseg; $lrest -= $lseg;
#  }
#  my $ipdio = join('',  map { _5a( pack("H*n/A*", 'd3eefb000000fe92', $_)) } unpack("(a16672)*", $_[0]) );
#  print "=" x 72, "\n";
#  print join("\n", unpack("(H72)*", $str)) , "\n";
#  die "=" x 72, "\n";
#  die ref($bands) . "\n" . "+" x 70 . "\n" . $bands->[0] . "\n" . unpack("H400", $bands->[0]) . "\n" . "+" x 70 , "\n"; 
  return (
	  _5a(
	      pack("H*", "d3eefb000000") .
	      pack(
		   "H4 H6 H4H2n4 H4H2H2H2",
		   "7000",                              # begin segment
		   "9101ff",                            # begin image content (ff = IOCA as is)
		   "9409", "00", $HRESOL, $VRESOL,  $HSIZE, $VSIZE, # image size 
		   "9503", $cmpr, "01", "00"                  # image Encoding
		  )
	     ).
#	  $ipdio .
	  $self->encodeImageContent($bands) .
	  _5a(
	      pack("H*", "d3eefb000000") .
	      pack(
		   "H4H4",
		   "9300", # end image content
		   "7100", # end segment
		  )
	     ) 
	 );
}

#-- end image object
sub eim {
  my ($self, $eid) = @_; 
  return _5a(mypackeb("H12E8", "d3a9fb000000", ($eid || "\xff"x8)));
  #  my $self = shift; 
  #  return _5a(pack("H12a8", "d3a9fb000000", "\xff"x8));
}

sub new {
  my $ClassName = shift; my $self = {};

  bless $self, $ClassName;
}


#-- end presentation text
sub nop {
  my ($self, $text, $TRANSPARENT) = @_; 
  return $TRANSPARENT
    ? _5a(pack("H12a*", "d3eeee000000", $text)) 
    : _5a(mypackeb("H12E*", "d3eeee000000", $text))
  ;
}

sub bps {
  my ($self, $psid) = @_; return _5a(mypackeb("H12E8", "d3a85f000000", ($psid || 'PS00000')));
}

#-- end page segment
sub eps {
  my ($self, $ps) = @_; return _5a(pack("H12a8", "d3a95f000000", "\xff"x8));
}

sub ips {
  my ($self, $ps, $atX, $atY) = @_; 
  return _5a(mypackeb("H12E8M3M3", "d3af5f000000", $ps, $atX, $atY));
}

1;

