
package XReport::ARCHIVE;

use File::Path;
use strict;

sub get_INPUT_ARCHIVE {
  require XReport::ARCHIVE::INPUT; return XReport::ARCHIVE::INPUT->Open(@_);
}

sub get_OUTPUT_ARCHIVE {
  require XReport::ARCHIVE::OUTPUT; return XReport::ARCHIVE::OUTPUT->Open(@_);
}

sub cre_OUTPUT_ARCHIVE {
  require XReport::ARCHIVE::OUTPUT; return XReport::ARCHIVE::OUTPUT->Create(@_);
}

sub newTargetPath {
	my $confTGT = shift;
    my $location = shift;
#    warn "Begin newTargetPath, caller: ", join('::', (caller(1))[0,2]), " req: $confTGT cfg: ", Dumper($XReport::cfg->{LocalPath}), "\n";
    
=h1 LocalPathId

we handle here the localpathIDs: for the time being we consider valid only
IDs that begins with "file://" and are accessible - At least a valid spec must be found 
otherwise we die ungracefully. 

if the request specifies an empty ID (empty or spaces) 
we check if there is any default specified, 
if none we set the ID to L1 if valid
otherwise we get the first valid ID found in config
 
=cut
	$confTGT = '' if $confTGT =~ /^\s*$/;
    my $validIDS = [ grep { $XReport::cfg->{'LocalPath'}->{$_} =~ /^file:\/\/(.+)$/ && -e $1 }
                           keys %{$XReport::cfg->{'LocalPath'}} ];
 
	my $sizeValidIDS ={};
	foreach my $i (0 .. $#{$validIDS}) { 
		my $id = $$validIDS[$i];
		(my $poolPath) = ( $XReport::cfg->{'LocalPath'}->{$id} =~ /^file:\/\/(.+)$/ );
		$poolPath =~ s/\//\\/g;
		my ($bytes) = (`dir $poolPath | find "Dir" | find "byte"` =~ /^.+?([\d\.]+) byte/);
		$bytes = "0" unless $bytes;
		$bytes =~ s/\.//g;
		$sizeValidIDS->{$id} = $bytes; 
	}
	#order the records  by size
	$validIDS = [reverse sort { scalar($sizeValidIDS->{$a}) <=> scalar($sizeValidIDS->{$b}) } keys %$sizeValidIDS];

#TESTSAN 	
#	foreach my $i (0 .. $#{$validIDS}) {
#		my $id = $$validIDS[$i];
#		warn "TESTSAN - validIDS[".$i."]:".$id." - sizeValidIDS->{".$id."}:".$sizeValidIDS->{$id}."\n";	
#	}
	
    die "no valid IDs found in xreport.cfg\n" unless scalar(@{$validIDS});                           
	if ( !$confTGT ) {
       $confTGT = $XReport::cfg->{'LocalPath'}->{default} if exists($XReport::cfg->{'LocalPath'}->{default}) ;
       die "Invalid default specified for LocalPath, fix it or remove it to get \"L1\" or first valid\n"
              if ( $confTGT && $confTGT !~ /^\$pool_/i && !exists($XReport::cfg->{'LocalPath'}->{$confTGT}) );
       $confTGT = 'L1' if ( !$confTGT && exists($XReport::cfg->{'LocalPath'}->{L1}) 
                            && $XReport::cfg->{'LocalPath'}->{L1} =~ /^file:\/\/(.+)$/ && -e $1);
       $confTGT = $validIDS->[0] unless $confTGT;
	}
    if ( $confTGT !~ /^(\$pool_.*)$/i ) { 
       die "PATH Id \"$confTGT\" specified does not exists in config\n"
                                   if (!exists($XReport::cfg->{'LocalPath'}->{$confTGT}));
       die "ID \"$confTGT\" selects \"$XReport::cfg->{'LocalPath'}->{$confTGT}\""
           . " which is not supported by newTargetPath - caller: ".join('::', (caller())[0,2])."\n"
                                       if ( $XReport::cfg->{'LocalPath'}->{$confTGT} !~ /^file:\/\// );
       die "PATH Id \"$confTGT\" specifies \"$XReport::cfg->{'LocalPath'}->{$confTGT}\""
                             ." that is not accessible - caller: ".join('::', (caller())[0,2])."\n" 
                        if ( ( $XReport::cfg->{'LocalPath'}->{$confTGT} =~ /^file:\/\/(.+)$/ && !-e $1 ) );
    }
=h1 LocalPathId Pools

If the ID specified (either via call parm or default spec ) begins with "$pool_"
we will try to make a list of valid IDs from the LocalPath attribute of config 
named as specified. 
If we find the attribute we will die ungracefully if none of the ID listed is valid
if we do not find the attribute then we make a list out of all the valid ID found in config

This logic allow us to address the problem of out of space conditions. 
From now on addig items to lists when we fill up a location will solve the out of space condition
   
Furthermore will allow to specify in the report definition a pool of specialized locations

=cut
    else {
       my $poolid = $1;
       my $pool = [];
       if (exists($XReport::cfg->{'LocalPath'}->{$poolid})) {
          push @{$pool}, grep { exists($XReport::cfg->{'LocalPath'}->{$_}) 
       	                   && $XReport::cfg->{'LocalPath'}->{$_} =~ /^file:\/\/(.+)$/ && -e $1 }
                           split / *[,;] *| +/, $XReport::cfg->{'LocalPath'}->{pool} 
                                      if (exists($XReport::cfg->{'LocalPath'}->{pool}));
           die "NO ID specified in pool is valid, fix spec or remove it to select any valid id\n" 
                                                                               if ( !scalar(@{$pool}) );
       }
       else {
          push @{$pool}, @{$validIDS} unless scalar(@{$pool});
       }
=h1 ID selection

everys ID of the pool is checked for the available space. The first ID that selects a path 
with bytes available > minfree attribute will be chosen. The minimum amount of bytes available
will be given by the minfree attribute of the LocalPath tag or the attribute with a name built up by
the path Id itself followed by "_minfree". This will allow to specify different amount for every path
defined.
Here follow a config example:   

<LocalPath default="$pool_mio" $pool_mio="ID1,ID2,ID3" 
           $pool_002="ID3,ID2,ID1" 
           minfree="51200000"
           ID3_minfree="25600000"
           >
<ID1>file://\\NASADDRES\XRSHARE1\</ID1>   
<ID2>file://\\NASADDRES\XRSHARE2\</ID2>   
<ID3>file://\\NASADDRES\XRSHARE2\</ID2>   
</LocalPath>
ReportName TargetLocalPathId_IN TargetLocalPathId_OUT
MYREP001   ID1                  ID1
MYREP002   $pool_002            $pool_002
MYREP003                                 

to stay on the safe side we should check the available space against 
the 2/3rd of the size of the work dir but for the .ps files. 
There is the risk that very large files will not fit in the min space free

have a nice day

=cut
       my $minfree = $XReport::cfg->{'LocalPath'}->{minfree};
       unless ( $minfree ) {
          $minfree = 102400000;
          warn "No free bytes threshold specified in config (minfree) - $minfree assumed\n";
       }
       my $pathsFull = [];
       while ( scalar(@{$pool}) ) {
          my $id = shift @{$pool};
          my $lclminfree = $minfree;
          $lclminfree = $XReport::cfg->{'LocalPath'}->{$id.'_minfree'} 
                  if exists($XReport::cfg->{'LocalPath'}->{$id.'_minfree'});
                  
          (my $poolPath) = ( $XReport::cfg->{'LocalPath'}->{$id} =~ /^file:\/\/(.+)$/ );
          $poolPath =~ s/\//\\/g;
          
		  my ($bytes) = scalar($sizeValidIDS->{$id}) ;
		  #my ($bytes) = (`dir $poolPath | find "Dir" | find "byte"` =~ /^.+?([\d\.]+) byte/);
          #$bytes = "0" unless $bytes;
          #$bytes =~ s/\.//g;
          if ( $bytes < $lclminfree ) {
          	 push @{$pathsFull}, $poolPath;
             die "NO path with more than $minfree bytes available on pool paths:\n\""
                             .join("\"\n\"", @{$pathsFull}, $poolPath)."\"\n" unless scalar(@{$pool});
          	 next; 
          }
          else {
             $confTGT = $id;
          	 warn "ID $confTGT for path \"$poolPath\" selected - $bytes bytes available - min allowed $minfree\n";
             last;
          }
       }
    }
	(my $TGTPATH) = ( $XReport::cfg->{'LocalPath'}->{$confTGT} =~ /^file:\/\/(.+)$/ );
	mkpath "$TGTPATH\\$location" if (!-e "$TGTPATH\\$location");
    die "Creation/Access to \"$TGTPATH/$location\" fails - E1: $! E2: $? - caller: ".join('::', (caller())[0,2])."\n"
                                                                        unless (-e "$TGTPATH\\$location");
=h1 Return Values
the sub return an hash with the following items
LocalPathId => the ID selected by the process
fqn => Full qualified name built up by the Path string of the selected ID + "/" + string passed as arg

Furthermore the sub creates the path specified as string if does not exists in the location selected
(IN & OUT included) 
=cut                                                                        
                                                                        
    return {LocalPathId => $confTGT, fqn => "$TGTPATH\\$location"};                                                                         	
}

1;
