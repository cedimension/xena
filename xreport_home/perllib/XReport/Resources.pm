package XReport::Resources;

use strict;
use Carp qw(confess);

use XReport;
use XReport::SOAP;
use LWP;

sub new {
  my $class = shift;
  my $self = { @_ };
#  use Data::Dumper;
#  die Dumper($XReport::cfg);
#  $self->{ResourceServer} = c::getValues('ResourceServer') 
  if ( exists($self->{ResourceServer} ) ) {
    @{$self->{ResourceServer}}{qw(ListenAddr ListenPort)} = split /:/, $self->{ResourceServer} 
      unless ref($self->{ResourceServer}) eq 'HASH';
  }
  else {
    my $daemoncfg = c::getValues('daemon') ||
      confess "Unable to retrieve daemons list from config";
    confess "Unable to retrieve Resource Server Location from config"
      unless exists($daemoncfg->{ResourceServer});
    
    $self->{ResourceServer} = $daemoncfg->{ResourceServer};
  }
  $self->{ResourceServer}->{ListenAddr} = 'localhost' if $self->{ResourceServer}->{ListenAddr} eq '0.0.0.0';
       
#  die Dumper($self->{ResourceServer});

  my $localres = $self->{'destdir'} || die "Destination Directory missing";
  if (!-d $localres) {
    mkdir $localres;
    confess "Unable to create \"$localres\" directory" unless -d $localres; 
  }
  
  $self->{xrhost} = join(':', @{$self->{'ResourceServer'}}{qw(ListenAddr ListenPort)});

  die "Missing reference to Resource Server" unless $self->{xrhost};
  i::logit("Initializing environment to ResourceServer at $self->{xrhost}");
  $self->{request} = LWP::UserAgent->new;
  bless $self, $class;
}

sub _postRequest {
  my ($self, $REQDATA) = (shift, shift );
  $REQDATA->{IndexName} = '_Resources' unless exists($REQDATA->{IndexName});
  my $r = $self->{request};
  my $caller = (split(/::/, (caller(1))[3]))[-1];
  my $reqstream = XReport::SOAP::buildRequest( $REQDATA );
#  print "REQSTREAM: $reqstream\n";
  my $cfgresp = $r->post( "http://$self->{xrhost}/ResourceServer/?$caller", 
			  Content_type => 'text/xml',
			  SOAPAction => "resourceserver\#$caller",
			  Content => $reqstream,
			);
  if ( !$cfgresp->is_success() ) {
    i::logit("Error response from server - ", $cfgresp->status_line);
    return undef;
#    confess "Error response from Resource server" ;
  } else {
    my $response = XReport::SOAP::parseSOAPreq( $cfgresp->content(), 'RESPONSE');
    return $response if $response;
    i::logit(" Unable to find RESPONSE element inside SOAP Envelope ");
    return undef;
  }
}
sub _chkArgs {
  my $self = shift;
  die "self reference invalid - ", ref($self), " ne ", __PACKAGE__ unless ref($self) eq __PACKAGE__ ;
  return ($self, { @_ });
}

sub listResources {
  my ($self, $args) = _chkArgs(@_);

  $args->{TimeRef} = $self->{TimeRef} if exists($self->{TimeRef}) && !exists($args->{TimeRef});
  $args->{TimeRef} = [ undef, $args->{TimeRef} ] if exists($args->{TimeRef}) && !ref($args->{TimeRef});

  $args->{ResGroup} = $self->{ResGroup} if exists($self->{ResGroup}) && !exists($args->{ResGroup});
  $args->{ResGroup} = [ undef, $args->{ResGroup} ] if exists($args->{ResGroup}) && !ref($args->{ResGroup});

  my $conds = ($args->{conds} && ref($args->{conds}) eq 'ARRAY' ? $args->{conds} 
	       : $args->{conds} ? [ { %{$args->{conds}} } ] 
	       : undef );

  if (exists($args->{TimeRef}) || exists($args->{ResGroup})) {
    $conds = [ {} ] unless $conds;
    
    $conds = [ map { $_->{XferStartTime} = $args->{TimeRef} if exists($args->{TimeRef}) && !exists($_->{XferStartTime});
		     $_->{ResGroup} = $args->{ResGroup} if exists($args->{ResGroup}) && !exists($_->{ResGroup});
		     $_
		   } @$conds ];
  }
  i::logit(sprintf('Requesting List for %s: %s - ( %s: %s - %s: %s - %s: %s)', map { ($_, (ref($args->{$_}) ? $args->{$_}->[0] : $args->{$_}) || '-ALL-') } qw(ResGroup ResName ResClass TimeRef) ));
  my $response = $self->_postRequest({
				      IndexEntries => XReport::SOAP::whereEntries( $conds ),
				     });
  return XReport::SOAP::polishEntries($response->{request}->{IndexEntries});
}

sub getResources {
  my ($self, $args) = _chkArgs(@_);
  die "Resource name missing in request by " . join('::', caller()) unless $args->{ResName};
  die "Resource class missing" unless $args->{ResClass};

  my $localres = ( $args->{OutPath} ? $args->{OutPath} :  $self->{destdir} );
  $args->{TimeRef} = $self->{TimeRef} unless $args->{TimeRef};
  $args->{TimeRef} = '2999-01-01 00:00:00.000' unless $args->{TimeRef};

  i::logit("Requesting " . sprintf('%s - ( %s: %s - %s: %s) to be stored in %s', $args->{ResName}, map { ($_, $args->{$_}) } (qw(ResClass TimeRef)), $localres));
  
  my $response = $self->_postRequest( {
				       IndexEntries => XReport::SOAP::whereEntries
				       ( [ { ResName => $args->{'ResName'},
					     ResClass => $args->{'ResClass'},
					     XferStartTime => [ undef, $args->{TimeRef}],
					   } ] )
				      } );
  
  my ($fn, $contenttype, $content, $resId) = @{$response->{request}}{qw(FileName DocumentType DocumentBody Identity)} if $response;
  my $ft = (split /[\/\-]+/, $contenttype)[-1]; 
  i::logit("Resource $args->{ResName} ($resId $ft) retrieved, content: ". length($content));
  
  my $fname = "$fn.$ft";
  $fname = $fn;
  $fname = $args->{OutFile} if $args->{OutFile};
  $fname = "$localres/$fname" unless ref($fname);
  die "Invalid file name returned" unless $fname;
  if ($fn) {
    my $fh = $fname;
    $fh = new FileHandle(">$fname") unless ref($fname);
    die "unable to open output file ".$fname unless $fh;
    binmode $fh unless ref($fh) eq 'IO::Scalar';
    XReport::SOAP::_decode_base64($content, sub { my ($fh, $buff) = (shift, shift); print $fh $buff; }, $fh);
    close $fh;
    i::logit("Resource $resId ($ft) stored into $localres as $fn");
  }
  return (wantarray ? ($fname, $ft) : $fname);
}

sub storeResource {
  my ($self, $parms) = _chkArgs(@_);
  die "Resource list missing or invalid" unless exists($parms->{resources}) && ref($parms->{resources}) eq 'ARRAY';
#  print Dumper($parms);
  my $results = [];
  foreach my $args ( @{$parms->{resources}} ) {
#    print "checking $args->{ResName}\n";
    next unless ref($args) eq 'HASH' && exists($args->{ResName}) && exists($args->{ResFileData})  && length($args->{ResFileData});
    $args->{ResGroup} = $self->{ResGroup} if exists($self->{ResGroup}) && !exists($args->{ResGroup});
    $args->{ResClass} = $self->{ResClass} if exists($self->{ResClass}) && !exists($args->{ResClass});
    next unless exists($args->{ResClass});
#    print "processing $args->{ResName}\n";

    my $attrs = [ { 
		ResName => $args->{ResName},
		ResClass => $args->{ResClass},
	       } ];
    $attrs->[0]->{ResGroup} = $args->{ResGroup} if exists($args->{ResGroup});

    my $response = $self->_postRequest
      ( {
	 IndexEntries => XReport::SOAP::whereEntries( $attrs ),
	 FileName => $args->{ResName}, DocumentType => $args->{ResClass},
	 DocumentBody => { content => join
			   ('', map { XReport::SOAP::_encode_base64($_) } 
			    unpack("(a".(57*76).")*", $args->{ResFileData} ) 
			   )  },
	});

    push @$results, @{ XReport::SOAP::polishEntries($response->{request}->{IndexEntries}) } if $response;
    
  
  }
  return $results;
}

sub execProject {
  my ($self, $args) = _chkArgs(@_);
  die "Resource data missing" unless ref($args) eq 'HASH' && exists($args->{ResFileData});
  my $localres = ( $args->{OutPath} ? $args->{OutPath} :  $self->{destdir} );
  
  my $attrs = [ { 
		} ];
  
  my $response = $self->_postRequest
    ( {
       IndexEntries => XReport::SOAP::whereEntries( $attrs ),
       FileName => $args->{ResName}, DocumentType => $args->{ResClass},
       DocumentBody => { content => join
			 ('', map { XReport::SOAP::_encode_base64($_) } 
			  unpack("(a".(57*76).")*", $args->{ResFileData} ) 
			 )  },
      });
  
  my ($fn, $contenttype, $content) = @{$response->{request}}{qw(FileName DocumentType DocumentBody)} if $response;
  my $ft = (split /[\/\-]+/, $contenttype)[-1]; 
  
  my $fname = "$fn.pdf";
#  $fname = $fn;
  $fname = $args->{OutFile} if $args->{OutFile};
  $fname = "$localres/$fname" unless ref($fname);
  die "Invalid file name returned" unless $fname;
  if ($fn) {
    my $fh = $fname;
    $fh = new FileHandle(">$fname") unless ref($fname);
    die "unable to open output file ".$fname unless $fh;
    binmode $fh unless ref($fh) eq 'IO::Scalar';
    XReport::SOAP::_decode_base64($content, sub { my ($fh, $buff) = (shift, shift); print $fh $buff; }, $fh);
    close $fh;
    i::logit("Resource stored into $localres as $fname, content: ". length($content));
  }
  return ($fname, XReport::SOAP::polishEntries($response->{request}->{IndexEntries})) if $response;
  
}

1;
