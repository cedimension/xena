
package XReport::EXTRACT;

use strict;

use constant EF_PRINT => 1;
use constant EF_DATA => 2;

our $FileImage;

sub setJobReport {
  my ($self, $JobReportId) = @_; my $jex = $self->{'jex'};

  if ( $jex ) {
    $jex->Close()
  }
  else {
    require XReport::ARCHIVE::JobExtractor;
  } 

  $jex =  XReport::ARCHIVE::JobExtractor->Open($JobReportId);

  @{$self}{qw(JobReportId jex JobReportName ElabFormat)} = (
    $JobReportId, $jex, 
    $jex->getValues(qw(JobReportName ElabFormat))
  ); 
}

sub max {
  $_[0]>=$_[1] ? $_[0] : $_[1];
}

sub min {
  $_[0]<=$_[1] ? $_[0] : $_[1];
}

sub extractFile {
  my $self = shift; $self->{'jex'}->extractFile(@_);
}

sub getFile {
  my $self = shift; $self->{'jex'}->getFile(@_);
}

sub getPageXref {
  my $self = shift; $self->{'jex'}->getPageXref(@_);
}

sub ExtractLines {
  my $self = shift; my $args = $self->{'args'}; %$args = (%$args, @_); #require XReport::EXTRACT::Lines;

  my $QUERY_TYPE = $args->{'QUERY_TYPE'}; $self->{'ExtractList'} = []; 
  
  if ( $QUERY_TYPE eq "FULLKEY" ) {
#    require XReport::EXTRACT::FULLKEY; 
    $self->ExtractLines_FULLKEY();
  }
  elsif ( $QUERY_TYPE eq "INQUIRY" ) {
    die "UserError: QUERY_TYPE INQUIRY NOT IMPLEMENTED";
  }
  elsif ( $QUERY_TYPE eq "FROMLINES" ) {
    $self->ExtractLines_FROMLINES();
  }
  else {
  }
  
  if (my $docTo = delete($self->{'docTo'})) { 
    $docTo->Close();  $docTo = undef; 
  } 

  return $self->{'ExtractList'};
}

sub ExtractPages {
  my $self = shift; my $args = $self->{'args'}; %$args = (%$args, @_);

  my $QUERY_TYPE = $args->{'QUERY_TYPE'}; $self->{'ExtractList'} = []; 
  
  if ( $QUERY_TYPE eq "FULLKEY" ) {
  #  require XReport::EXTRACT::FULLKEY;
    $self->ExtractPages_FULLKEY();
  }
  elsif ( $QUERY_TYPE eq "INQUIRY" ) {
    die "UserError: QUERY_TYPE INQUIRY NOT IMPLEMENTED";
  }
  elsif ( $QUERY_TYPE eq "FROMPAGES" ) {
    $self->ExtractPages_FROMPAGES();
  }
  elsif ( $QUERY_TYPE eq "FULLEXCEL" ) {
    $self->Extract_EXCEL();
  }
  else {
    die "ExtractPages: INVALID QUERY_TYPE $QUERY_TYPE.";
  }
  
  if (my $docTo = delete($self->{'docTo'})) { 
    #$docTo->Close(); 
	eval{$docTo->Close()};
	if($@){
		$docTo->close();
	}	
	
  } 

  return $self->{'ExtractList'};
}


sub Close {
  my ($self, $JobReportId) = @_; return if $self->{'closed'};
  i::logit(ref($self)." Closing as requested by ".join('::', (caller())[0,2]));

  my ($args, $jex) = @{$self}{qw(args jex)};

  $jex->Close() if $jex; %$args = (); %$self = (closed => 1);
}

sub new {
  my ($className, %args) = @_; my $self;

  $self = {
    args => \%args, closed => 0
  };

  bless $self, $className;
}

sub DESTROY {
 #print "DESTROY ", __PACKAGE__, "\n";
  my $self = shift; 
  i::logit(ref($self)." DESTROY requested by ".join('::', (caller())[0,2]));
  $self->Close(); 
}

# here begins EXTRACT::pages
### todo: accept a FILTERING routine
sub getFilePages {
  my ($self, $FileId, $FilePages) = @_; my ($JobReportId) = @{$self}{qw(JobReportId)};

  local *FileImage = $self->getFile($FileId, "dat");

  my $eof = length($FileImage)-4;
  if ( "\%EOF" ne substr($FileImage,$eof,4) ) {
    die "EOF NOT FOUND";
  }
  
  my $xref = unpack("N", substr($FileImage, $eof-4, 4));
  if ( "xref" ne substr($FileImage, $xref, 4) ) {
    die "XREF NOT FOUND";
  }
  my ($FirstPage, $LastPage) = unpack("NN", substr($FileImage, $xref+4, 8));

  my $PageList = [];
  
  for ( my $i=0; $i<scalar(@$FilePages); $i+=2 ) {
    my ($FromPage, $ToPage) = ($FilePages->[$i] , $FilePages->[$i+1]);

	if ( $ToPage < $FirstPage or $FromPage > $LastPage ) {
	  print 
	   "RANGE ERROR ", 
	   join("/", $JobReportId, $FileId, $FromPage, $ToPage, $FirstPage, $LastPage), "\n"
	  ;
	  next;
	}

	$FromPage = $FirstPage if $FromPage < $FirstPage;
	$ToPage = $LastPage if $ToPage > $LastPage;

    for ($FromPage..$ToPage) {
      my $FromChar  = $xref+12 + ($_-$FirstPage) * 4;
      my $ToChar = $FromChar + 4;

      $FromChar = unpack("N", substr($FileImage, $FromChar, 4));
      $ToChar = unpack("N", substr($FileImage, $ToChar, 4));

      push @$PageList , substr($FileImage, $FromChar, $ToChar - $FromChar);
	}
  }
  
  return $PageList;
}

sub getJobReportPages {
  my ($self, $JobReportPages) = @_; 

  my (
   $args, 
   $JobReportId, $ElabFormat, $docTo
  ) = 
  @{$self}{qw(args JobReportId ElabFormat docTo)}; 

  my (
    $TO_HANDLE, $TO_DIR, $REQUEST_ID, $ENCRYPT, $OPTIMIZE
  ) = 
  @{$args}{qw(TO_HANDLE TO_DIR REQUEST_ID ENCRYPT OPTIMIZE)};

  my %JobReportFilePages; my $toFile;  my $totPages = 0;

  @$JobReportPages = sort { $a->[0] <=> $b->[0] } @$JobReportPages;

  my @PageXref = $self->getPageXref();

  my ($FromPage, $ToPage, $FieldValues) = @{shift @$JobReportPages}; 
  $main::debug && i::logit("EXTRACT procesing request for pages From page $FromPage to $ToPage");
  my $at = 0;
  my ($FileId, $LowPage, $HighPage) = (0, 0, 0); my $lToPage; 
  my $firstPage = $FromPage;
  NEXTREQUEST:
  while(1) {
    while($FromPage > $HighPage and $at < @PageXref) {
      ($FileId, $LowPage, $HighPage) = @PageXref[$at..$at+2]; $at+=3;
    } 

    die("REQUESTED PAGE ($FromPage) GREATER THAN MAX AVAILABLE PAGE ($HighPage)")
     if 
    $FromPage > $HighPage;

    while($FromPage <= $HighPage) {
      $FromPage = max($FromPage,$LowPage); $lToPage = min($ToPage,$HighPage);
	  push @{$JobReportFilePages{$FileId}}, $FromPage, $lToPage ;
	  $totPages += $lToPage-$FromPage+1; 
      if ($lToPage < $ToPage) {
        $FromPage = $lToPage + 1; next NEXTREQUEST;
      }
      last NEXTREQUEST if !@$JobReportPages;
      ($FromPage, $ToPage, $FieldValues) = @{shift @$JobReportPages};
    }
  }
 
  $main::debug && i::logit("$JobReportId FirstPage=$firstPage totpages=$totPages");
  
  if ( $ElabFormat eq EF_PRINT ) {
    $toFile = $TO_HANDLE ? $TO_HANDLE : "$TO_DIR/$REQUEST_ID\.$JobReportId\.pdf"; require XReport::PDF::DOC;
	$docTo = XReport::PDF::DOC->Create($toFile, {encrypt => $ENCRYPT}) if ( !$docTo );
	
    for my $FileId ( sort({$a <=> $b} keys(%JobReportFilePages)) ) {
	  my ($FirstPage, $LastPage) = @PageXref[$FileId*3+1..$FileId*3+2]; my ($FileFrom, $docFm);  
      $main::debug && i::logit("EXTRACT: fileid: $FileId FirstPage: $FirstPage LastPage: $LastPage");
      my $JobReportName = $self->{'JobReportName'};
      
      my $doc_args = {
        lockXref => "CreXref.$JobReportName\.$JobReportId\.0.#$FileId.pdf", timexrefisok => 1
      };
      ### todo: test if JobReport is to optimize in a better way
      if ( $OPTIMIZE ) {
        if ( $JobReportName =~ /^(?:GT|LC|UL).*U6/i ) {
          $doc_args->{optimizer} = {
            ImagesSetId => $JobReportId, 
            ImagesFileCache => c::getValues(qw(ImagesFileCacheHome))
          }
        }
      }
      $FileFrom = $self->extractFile($FileId, "pdf");
      my $efh = new IO::File("<$FileFrom");
      $efh->binmode();
      $efh->read(my $header, 50);
      $efh->close();
      if ( $header && $header !~ /^\%PDF\-1\.2/ ) {
         $self->extractFile($FileId, 'XREF', 1);
      }
      else {
         i::logit("XREF extraction skipped - header: ". unpack('A8', $header || 'UNKNOWN'));
      }
      $docFm = XReport::PDF::DOC->Open($FileFrom, $doc_args);
      
	  my @pages = @{$JobReportFilePages{$FileId}};
	  while ( @pages ) {
	    my ($from, $to) = (shift @pages, shift @pages);
        $main::debug && i::logit("EXTRACT calling copyPages to copy From page $from to $to");
		$docTo->CopyPages($docFm, $from-$FirstPage+1, $to-$from+1);
	  }

      $docFm->Close();
    }

    $self->{'docTo'} = $docTo if !$self->{'docTo'};
  }
  elsif ( $ElabFormat eq EF_DATA ) {
    my $OUTPUT = $toFile = $TO_HANDLE;
    if (!$OUTPUT) { 
      $toFile = "$TO_DIR/$REQUEST_ID\.$JobReportId\.dat"; $OUTPUT = gensym();
      open($OUTPUT, ">$toFile") 
	   or 
	  die("OPEN OUTPUT ERROR \"$toFile\" $!");
    } 
    binmode($OUTPUT);
	
    for my $FileId ( sort({$a <=> $b} keys(%JobReportFilePages)) ) {
      print $OUTPUT @{$self->getFilePages($FileId, $JobReportFilePages{$FileId})};
    }
	
	close($OUTPUT);
  }
  else {
    die "INVALID ElabFormat DETECTED: $ElabFormat.";
  }

  return $toFile;
}

sub getJobReportsPages {
  my $self = shift; my ($JobReportPages, $ExtractList) = @{$self}{qw(JobReportPages ExtractList)};

  my @JobReportIds = sort({$a <=> $b} keys(%$JobReportPages)); my $toFile;

  for my $JobReportId (@JobReportIds) {
    $self->setJobReport($JobReportId);
    $toFile = $self->getJobReportPages($JobReportPages->{$JobReportId}); 
	push @$ExtractList, [$JobReportId, $toFile]; delete $JobReportPages->{$JobReportId};
  }
}

sub VerifyPageList {
  my $self = shift; my $args = $self->{'args'}; my $list = $args->{'PAGE_LIST'};

  return $list if ref($list) eq "HASH";

  return {'.' => $list} if ref($list) eq "ARRAY";

  my @list  = split(/[\s,]+/, $list); my $PageLists = {};

  for (@list) {
    my $t = $_; my @t = split(/[\W_]+/, $t); my $dbalias;
    if (@t==3) {
      $dbalias = '.'; 
    }
    elsif (@t==4) {
      $dbalias = shift @t; 
    }
    else {
      die "SSSSSSSSSSSSSSSSSSSSSSSSSSS\n";
    }
    $dbalias = '.' if $dbalias eq ''; push @{$PageLists->{$dbalias}}, @t;
  }

  #for my $dbalias (keys(%$PageLists)) {
  #  print $dbalias, join(",", @{$PageLists->{$dbalias}}), "<==\n";
  #}

  return $PageLists;
}

sub ExtractPages_FROMPAGES {
  my $self = shift; my $JobReportPages = $self->{'JobReportPages'} = {}; 

  my $PageLists = $self->VerifyPageList(); 

  for my $dbalias (keys(%$PageLists)) {
    if ($dbalias ne '.') {
      my ($dbaliases, $dbalias_list, @dbalias_list) = c::getValues(qw(dbaliases)); 
      if (
        !$dbaliases 
        or !exists($dbaliases->{$dbalias}) 
        or !($dbalias_list = $dbaliases->{$dbalias})
      ) {
        die("MISSING DBALIASes DEFINITION FOR dbalias($dbalias)\n");
      }
      @dbalias_list = $dbalias_list =~ /(\w+)/g;
      XReport::DBUtil->push_dbxr(DBNAME => $dbalias_list[0]); c::setValues(dbalias => $dbalias);
    }
    eval {
      my $list = $PageLists->{$dbalias}; my $totPages=0;
      while(@$list) {
        my ($JobReportId, $FromPage, $ForPages) = splice( @$list, 0, 3);
        if ( !exists($JobReportPages->{$JobReportId}) ) {
          $JobReportPages->{$JobReportId} = [];
        }    
        push @{$JobReportPages->{$JobReportId}}, [$FromPage, $FromPage+$ForPages-1, ""]; $totPages+=$ForPages;
      }

      i::logit("FROMPAGES dbalias=$dbalias TOTPAGES=$totPages");
  
      $self->getJobReportsPages();
    };
    my $errmsg = $@;

    if ($dbalias ne '.') {
      XReport::DBUtil->pop_dbxr(); c::setValues(dbalias => '');
    }

    die $errmsg if $errmsg ne "";
  }
}
#pages: end

sub Extract_EXCEL {
  my $self = shift; my $JobReportPages = $self->{'JobReportPages'} = {}; 
  my ($ExtractList) = @{$self}{qw(ExtractList)};
  my $PageLists = $self->VerifyPageList(); 
	
  for my $dbalias (keys(%$PageLists)) {
     if ($dbalias ne '.') {
      # my ($dbaliases, $dbalias_list, @dbalias_list) = c::getValues(qw(dbaliases)); 
      # if (
        # !$dbaliases 
        # or !exists($dbaliases->{$dbalias}) 
        # or !($dbalias_list = $dbaliases->{$dbalias})
      # ) {
        # die("MISSING DBALIASes DEFINITION FOR dbalias($dbalias)\n");
      # }
      # @dbalias_list = $dbalias_list =~ /(\w+)/g;
      # XReport::DBUtil->push_dbxr(DBNAME => $dbalias_list[0]); c::setValues(dbalias => $dbalias);
	  die "dbalias not managed";
     }
     eval {
       my $list = $PageLists->{$dbalias}; my $totPages=0;
      while(@$list) {
        my ($JobReportId, $FromPage, $ForPages) = splice(@$list, 0, 3);
        if ( !exists($JobReportPages->{$JobReportId}) ) {
          $JobReportPages->{$JobReportId} = [];
        }    
        push @{$JobReportPages->{$JobReportId}}, [$FromPage, $FromPage+$ForPages-1, ""]; $totPages+=$ForPages;
      }
	  i::logit("FROMPAGES dbalias=$dbalias TOTPAGES=$totPages");
	};
      
	  #instead of this 
	  #$self->getJobReportsPages();
	  # put directly with one JobReportid
	  my @JobReportIds = sort({$a <=> $b} keys(%$JobReportPages)); my $toFile;

	for my $JobReportId (@JobReportIds) {
		$self->setJobReport($JobReportId);
		## to do make a method to call eventually many excel formatted for each branch
		#$toFile = $self->getJobReportPages($JobReportPages->{$JobReportId}); 
		push @$ExtractList, [$JobReportId, $toFile]; delete $JobReportPages->{$JobReportId};
	}
	### until now 04-2019 only entire excel is requested - if cut by branch will be necessary one exctract file for file-id must be implemented
	my $FileFrom = $self->extractFile(0, "xlsx");
    my $efh = new IO::File("<$FileFrom");
    $efh->binmode();
	
	##TO DO 
	while(1){
		my $buff = ' ' x 131072;
		$efh->read($buff, 131072);
		last if $buff eq '';
		$self->{docTo}->write($buff);
		# if($FileFrom->size()< 0)
			# exit;
	}
    ####################
   }
}

#FULLKEY: begin
sub PrepareFullkeyQuery {
  my ($self, $dbc) = @_; 

  my (
   $INDEX_NAME, 
   $FieldNames, $FieldTypes, $FullKeyFile
  ) = 
  @{$self}{qw(INDEX_NAME FieldNames FieldTypes FullKeyFile)};

  my $dbr = $dbc->dbExecute("EXEC sp_columns tbl_IDX_$INDEX_NAME");

  if ( $dbr->eof() ) {
    die("TABLE INDEX tbl_IDX_$INDEX_NAME NOT DEFINED !");
  }

  while(!$dbr->eof()) {
    my ($COLUMN_NAME, $TYPE_NAME) 
     = 
    $dbr->GetFieldsValues(qw(COLUMN_NAME TYPE_NAME));
    if ( $COLUMN_NAME !~ /^(?:JobReportId|FromPage|ForPages)$/ ) {
      push @$FieldNames, $COLUMN_NAME;
      push @$FieldTypes, $TYPE_NAME;
	}
    $dbr->MoveNext();
  }
  $dbr->Close();

  my @FieldNames = (@$FieldNames, qw(FM_DATE TO_DATE JobReportName));
  my @FieldTypes = (@$FieldTypes, qw(datetime datetime varchar(32)));

  $dbc->dbExecute(
    "CREATE TABLE #tbl_IDX_$INDEX_NAME ("
   .  join(', ', map {"$FieldNames[$_] $FieldTypes[$_]"} (0..$#FieldNames))
   .") "
  );
  $dbc->dbExecute(
    "BULK INSERT #tbl_IDX_$INDEX_NAME FROM '$FullKeyFile'"
  );
  $dbc->dbExecute(
    "CREATE INDEX #tbl_IDX_$INDEX_NAME\_IX1 ON #tbl_IDX_$INDEX_NAME ("
   .  join(', ', @FieldNames)
   .") "
  );

}

sub ExtractLines_FULLKEY {
  die("ExtractLines_FULLKEY NOT IMPLEMENTED.");
}

sub ExtractLines_INQUIRY {
  die("ExtractLines_INQUIRY NOT IMPLEMETED.");
}

sub ExtractPages_INQUIRY {
  die("ExtractPages_INQUIRY NOT IMPLEMETED.");
}

sub ExtractPages_FULLKEY {
  my $self = shift; my ($INDEX_NAME, $FieldNames) = @{$self}{qw(INDEX_NAME FieldNames)};

  my $dbcix = dbutil->get_ix_dbc($INDEX_NAME); 
  
  my $maindb = dbutil->get_dbname(); 
  
  i::logit("PAGE QUERY FULLKEY BEGIN <<");
  
  PrepareFullkeyQuery($dbcix);
  
  my $dbr = $dbcix->dbExecute(
    "SELECT " 
    . join(', ', map {"i.$_"} (@$FieldNames, 'JobReportId', 'FromPage', 'ForPages')) 
    . ", CONVERT(varchar,j.UserTimeRef,111) As UserTimeRef \n"
    
   ."FROM #tbl_IDX_$INDEX_NAME q WITH(INDEX(#tbl_IDX_$INDEX_NAME\_IX1)) "
   
   ."INNER JOIN tbl_IDX_$INDEX_NAME i WITH(INDEX(IX_tbl_IDX_$INDEX_NAME\_UserKey)) "
   ." ON " . 
       join(' AND ', 
       map {
         ($_ !~ /[%_]/) ? "i.$_ = q.$_" : "i.$_ LIKE q.$_"
       } 
       @$FieldNames) . "\n"
   
   ."INNER JOIN $maindb.dbo.tbl_JobReports j "
   ." ON j.JobReportId = i.JobReportId  " 
   
   ."WHERE "
   ." i.JobReportId IN ( "
   ."  SELECT JobReportid FROM $maindb.dbo.tbl_JobReportsIndexTables "
   ."  where "
   ."    IndexName = '$INDEX_NAME' "
   ."    AND UserTimeRef BETWEEN q.FM_DATE AND q.TO_DATE "
   ." ) "
   ." AND j.status = 18 AND j.JobReportName LIKE q.JobReportName "  

   ."ORDER BY i.JobReportId, i.FromPage "

   ."OPTION (FORCE ORDER) "
  );

  i::logit("PAGE QUERY FULLKEY END >>");
  
  my $totPages = 0;
  
  while ( !$dbr->eof() ) {
    my ($JobReportId, $FromPage, $ForPages, $JobReportPages) 
    = $dbr->GetFieldsValues(qw(JobReportId FromPage ForPages JobReportPages)); 

    my ($FieldValue, $FieldValues) = ("", "");

	for (@$FieldNames, "UserTimeRef") {
	  $FieldValue = $dbr->GetFieldsValues($_); $FieldValues .= "\t$FieldValue";
	}
    my $ToPage = $FromPage+$ForPages-1;

    if ( !exists($JobReportPages->{$JobReportId}) ) {
      $JobReportPages->{$JobReportId} = [];
    }
    push @{$JobReportPages->{$JobReportId}}, [$FromPage, $ToPage, $FieldValues]; $totPages+=$ToPage-$FromPage+1;
    
    $dbr->MoveNext();
  }
  $dbr->Close();

  $dbcix->dbExecute("DROP TABLE #tbl_IDX_$INDEX_NAME");
  
  i::logit("FULLKEY TOTPAGES=$totPages");
  
  getJobReportsPages();
}
#FULLKEY: end

#Lines: begin
### todo: accept a FILTERING routine
sub getFileLines {
  my ($self, $FileId, $FileLines) = @_; my $JobReportId = $self->{'JobReportId'};

  local *FileImage = $self->getFile($FileId, "dat");

  my $eof = length($FileImage)-4;
  if ( "\%EOF" ne substr($FileImage,$eof,4) ) {
    die "EOF NOT FOUND";
  }
  
  my $xref = unpack("N", substr($FileImage, $eof-4, 4));
  if ( "xref" ne substr($FileImage, $xref, 4) ) {
    die "XREF NOT FOUND";
  }
  my ($FirstPage, $LastPage) = unpack("NN", substr($FileImage, $xref+4, 8));

  my $LineList = [];
  
  for ( my $i=0; $i<scalar(@$FileLines); $i+=3 ) {
    my ($FromPage, $FromLine, $ForLines) = @{$FileLines}[$i..$i+2];

	if ( $FromPage < $FirstPage or $FromPage > $LastPage ) {
	  print 
	   "RANGE ERROR ", 
	   join("/", $JobReportId, $FileId, $FromPage, $FirstPage, $LastPage), "\n"
	  ;
	  next;
	}

    my $atPos = unpack("N",substr($FileImage,$xref+12+($FromPage-$FirstPage)*4, 4)); my $ll;

    for (1..$FromLine-1) {
      $ll = unpack("n", substr($FileImage, $atPos, 2));
      $atPos += 2+$ll;
    }
    for ( ;$ForLines>0; $ForLines-- ) {
      die ("TRYING TO READ AFTER THE XREF MARK /$atPos /$xref /$ForLines") 
        if
      $atPos >= $xref; 

      $ll = unpack("n", substr($FileImage, $atPos, 2));

      push @$LineList , substr($FileImage, $atPos, 2+$ll); $atPos += 2+$ll;
	}
  }
  
  return $LineList;
}

sub getJobReportLines {
  my ($self, $JobReportLines) = @_;

  my (
    $args, 
    $JobReportId, $ElabFormat, $docTo
  ) = 
  @{$self}{qw(args JobReportId ElabFormat docTo)}; 

  my (
    $TO_DIR, $REQUEST_ID, $ENCRYPT, $OPTIMIZE
  ) = 
  @{$args}{qw(TO_DIR REQUEST_ID ENCRYPT OPTIMIZE)};

  my %JobReportFileLines; my $toFile; 

  my @PageXref = $self->getPageXref(); my $totLines = 0;

  ### modify as getJobReportPages
  for ( @$JobReportLines ) {
    my ($FromPage, $FromLine, $ForLines, $FieldValues) = @$_; #i::logit("at /$FieldValues/");
    for ($_=0; $_<scalar(@PageXref); $_+=3) {
      if ( 
	    $FromPage >= $PageXref[$_+1] and $FromPage <= $PageXref[$_+2] 
	  ) {
	    my $FileId = $PageXref[$_]; 
        if ( !exists($JobReportFileLines{"$FileId"}) ) {
          $JobReportFileLines{$FileId} = [];
        }
	    push @{$JobReportFileLines{$FileId}}, $FromPage, $FromLine, $ForLines;
      }
    }
	
	$totLines += $ForLines;
  }

  i::logit("$JobReportId totLines=$totLines");

  if ( $ElabFormat eq EF_PRINT ) {
    $toFile = "$TO_DIR/$REQUEST_ID\.$JobReportId\.dat"; my $OUTPUT = gensym(); require XReport::PDF::DOC;
	open($OUTPUT, ">$toFile") 
	 or 
	die("OPEN OUTPUT ERROR \"$toFile\" $!"); binmode($OUTPUT);
	
	for my $FileId ( sort({$a <=> $b} keys(%JobReportFileLines)) ) {
      my $docFm = XReport::PDF::DOC->Open($self->extractFile($FileId, "pdf"));
	  my $LinesList = $JobReportFileLines{$FileId};
	  while( @$LinesList ) {
	    my ($FromPage, $FromLine, $ForLines) = splice(@$LinesList,0,3);
        my @lines = @{$docFm->getPageTextLines($FromPage)}; 
		die "INVALID NUMBER PAGE LINES DETECTED. EXPECTED > $FromLine RECEIVED=".scalar(@lines)."." 
		 if
		$FromLine > scalar(@lines); my ($line, $atLine) = ('', 1);
		while(1) {
	      while( $atLine < $FromLine and @lines) {
		    shift @lines; $atLine += 1;
	      }
	      while( $ForLines > 0 ) {
	        $line = shift @lines; $ForLines -= 1; $atLine += 1;
		    if (!@lines and $ForLines > 0) {
		      @lines = @{$docFm->getPageTextLines($FromPage += 1)}; $atLine = 1;
		    }
			print $OUTPUT pack("n", length($line)+1), " ", $line; 
	      }
		  if ( @lines && @$LinesList && $FromPage == $LinesList->[0] && $atLine <= $LinesList->[1] ) {
		    ($FromPage, $FromLine, $ForLines) = splice(@$LinesList,0,3);
		  }
		  else {
			last;
		  }
        }
	  }
	}
	
	close($OUTPUT);
  }
  elsif ( $ElabFormat eq EF_DATA ) {
    $toFile = "$TO_DIR/$REQUEST_ID\.$JobReportId\.dat"; my $OUTPUT = gensym();
    open($OUTPUT, ">$toFile") 
	 or 
	die("OPEN OUTPUT ERROR \"$toFile\" $!"); binmode($OUTPUT);
	
    for my $FileId ( sort({$a <=> $b} keys(%JobReportFileLines)) ) {
      print $OUTPUT @{$self->getFileLines($FileId, $JobReportFileLines{$FileId})};
    }
	
	close($OUTPUT);
  }
  else {
    die "INVALID ElabFormat DETECTED: $ElabFormat.";
  }

  return $toFile;
}

sub getJobReportsLines {
  my $self = shift; my ($JobReportLines, $ExtractList) = @{$self}{qw(JobReportLines ExtractList)};

  my @JobReportIds = sort({$a <=> $b} keys(%$JobReportLines)); my $toFile;

  for my $JobReportId (@JobReportIds) {
    $self->setJobReport($JobReportId);
    $toFile = $self->getJobReportLines($JobReportLines->{$JobReportId}); 
	push @$ExtractList, [$JobReportId, $toFile]; delete $JobReportLines->{$JobReportId};
  }
}

sub ExtractLines_FROMLINES {
  my $self = shift; my $args = $self->{'args'}; my $JobReportLines = $self->{'JobReportLines'} = {};

  my $list = $args->{'LINE_LIST'}; my $totLines = 0; 

  while(@$list) {
    my ($JobReportId, $FromPage, $FromLine, $ForLines) = splice( @$list, 0, 4);
    if ( !exists($JobReportLines->{$JobReportId}) ) {
      $JobReportLines->{$JobReportId} = [];
    }  
    push @{$JobReportLines->{$JobReportId}}, [$FromPage, $FromLine, $ForLines, ""]; $totLines+=$ForLines;
  }
  
  i::logit("FROMLINES TOTLINES=$totLines");
  
  $self->getJobReportsLines();
}
#Lines: begin

1;

