package XReport::INFORMAT;

#use Date::Calc (qw(Language Parse_Date Decode_Date_EU2 Decode_Date_JP2 Decode_Date_US2 Days_in_Month));
use Date::Calc (qw(Language Parse_Date Decode_Date_EU2 Decode_Date_US2 Days_in_Month));

my %LiteralMonths = (
  GENNAIO => 1, FEBBRAIO => 2, MARZO => 3, APRILE => 4, MAGGIO => 5, GIUGNO => 6,
  LUGLIO => 7, AGOSTO => 8, SETTEMBRE => 9, OTTOBRE => 10, NOVEMBRE => 11, DICEMBRE => 12,

  GEN => 1, FEB => 2, MAR => 3, APR => 4, MAG => 5, GIU => 6,
  LUG => 7, AGO => 8, SET => 9, OTT => 10, NOV => 11, DIC => 12
);

sub DDMMYY {
  my $ref = $_[0]; $ref = \$ref if ref($ref) eq '';
  my @dt = Decode_Date_EU2($$ref);
  if ( @dt ) {
	$dt[1] = substr("0".$dt[1],-2);
	$dt[2] = substr("0".$dt[2],-2);
  }
  elsif ($$ref =~ /\d{6,8}/) {
    @dt = unpack("a2a2a*", $$ref);
  }
  return $$ref = join('/', @dt);
}

sub YYMMDD {
  my $ref = $_[0]; $ref = \$ref if ref($ref) eq ''; my @dt;
#  @dt = Decode_Date_US2($$ref);
#  if ( @dt ) {
#	$dt[1] = substr("0".$dt[1],-2);
#	$dt[2] = substr("0".$dt[2],-2);
#  }
  if ($$ref =~ /\d{8}/) {
    @dt = unpack("a4a2a2", $$ref);
  }
  elsif ($$ref =~ /(\d+)\D(\d+)\D(\d+)/) {
    return DDMMYY("$3/$2/$1");
  }
  return $$ref = join('/', @dt);
}

sub VARDDMMYY {
  my $ref = $_[0]; $ref = $$ref if ref($ref);
  my @dt = Decode_Date_EU2($ref);
  if ( @dt ) {
	$dt[1] = substr("0".$dt[1],-2);
	$dt[2] = substr("0".$dt[2],-2);
  }
  elsif ($ref =~ /\d{6,8}/) {
    @dt = unpack("a2a2a*", $$ref);
  }
  return join('-', @dt);
}

sub YYYYMMDD { 
  my $ref = ref($_[0]) ? $_[0] : \$_[0];
  my ($msk, @seq) = split /#/, ($_[1] || '%02d.%02d.%04d#2#1#0');
  return $$ref = sprintf($msk, (unpack("a4a2a2", $$ref))[@seq]);
}

sub DDMMYYYY { 
  my $ref = ref($_[0]) ? $_[0] : \$_[0];
  my ($msk, @seq) = split /#/, ($_[1] || '%02d.%02d.%04d#2#1#0');
  return $$ref = sprintf($msk, (unpack("a2a2a4", $$ref))[@seq]);
}

sub MMDDYY {
  my $ref = $_[0]; $ref = \$ref if ref($ref) eq ''; my @dt;
  @dt = Decode_Date_US2($$ref);
  if ( @dt ) {
	$dt[1] = substr("0".$dt[1],-2);
	$dt[2] = substr("0".$dt[2],-2);
  }
  return $$ref = join('/', @dt);
}

sub YYT {
  my $ref = $_[0]; $ref = \$ref if ref($ref) eq ''; my @dt;
  @dt = unpack("a4,a1", $$ref); $dt[1]*=3; 
  $dt[2] = Days_in_Month(@dt);
  return $$ref = join('/', @dt);
}

sub ITALIANDATE {
  my $ref = $_[0]; $ref = \$ref if ref($ref) eq ''; my @dt;
  @dt = split(/\s+/, $$ref);
  $dt[1] = $LiteralMonths{uc($dt[1])} || $LiteralMonths{uc(substr($dt[1],0,3))};
  if (length($dt[2]) == 2) {
    $dt[2] = ($dt[2] < 50 ? '20' : '19') . $dt[2];
  }		  
  $$ref = $dt[1] ? $$ref = join('/', reverse(@dt)) : ''; return $$ref;
}

sub COMMA {
  my $ref = $_[0]; $ref = \$ref if ref($ref) eq ''; 

  $$ref =~ s/^ +//; $$ref =~ s/\.//g;$$ref =~ s/\,/\./;
  
  if (substr($$ref,-1) eq '-') {
    $$ref = '-'.substr($$ref,0,length($$ref)-1)
  }

  return $$ref;
}

sub STRIP {
}

sub CHAR {
}

sub RIGHT {
  my ($ref, $l) = @_; $ref = \$ref if ref($ref) eq '';
  return $$ref = substr((" "x$l).$$ref,-$l);
}

sub ZERO {
  my ($ref, $l) = @_; $ref = \$ref if ref($ref) eq '';
  return $$ref = substr(("0"x$l).$$ref,-$l);
}

Language(7);

1;
