
package XReport::FileUtil;

use strict;

use Symbol;

sub FileToString {
  my $FileName = shift; my $INPUT = gensym(); my $t;
  open($INPUT, "<$FileName")
    or
  die("FILE INPUT OPEN ERROR \"$FileName\" $!"); binmode($INPUT);
  read($INPUT, $t, -s $FileName); close($INPUT); return $t;
}

sub StringToFile {
  my ($FileName) = $_[1]; my $OUTPUT = gensym(); 
  open($OUTPUT, ">$FileName")
    or
  die("FILE OUTPUT OPEN ERROR \"$FileName\" $!"); binmode($OUTPUT);
  print $OUTPUT $_[0]; close($OUTPUT);
}

1;
