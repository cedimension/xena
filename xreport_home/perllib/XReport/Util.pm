#######################################################################
# @(#) $Id: Util.pm,v 1.2 2002/10/23 22:50:19 Administrator Exp $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
package XReport::Util;

use strict;
use Carp;
use warnings;

use Date::Calc;
use FileHandle;

use XReport::Logger;

use Exporter;
our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS, $logger, $logrRtn, $exenam, $prId, $lckFh);

# must be all one line, for MakeMaker
$VERSION = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r }; 

@ISA = qw(Exporter);

@EXPORT = qw(
  &getConfValues &setConfValues &getConfHash &InitServer &TermServer
  $logrRtn $logger &max &check_date 
  &XRprintHASH &sayLog &GetDateTime &parsePlFile &buildRtnHash
  &XRprId $exenam $prId $lckFh 
);

@EXPORT_OK = qw( 
  &getConfValues &setConfValues &getConfHash &InitServer &TermServer
  $logrRtn $logger &max &check_date 
  &XRprintHASH &sayLog &GetDateTime &parsePlFile &buildRtnHash
  &XRprId $exenam $prId $lckFh 
);

%EXPORT_TAGS = (
  CONFIG => [qw(
    &getConfValues &setConfValues &getConfHash &InitServer &TermServer
  )],
  LOG => [qw(
    $logrRtn $logger
  )],
  DAEMON => [qw(
    &XRprintHASH &sayLog &GetDateTime &parsePlFile &buildRtnHash
    &XRprId $exenam $prId $lckFh 
  )],
  GENERIC => [qw(
    &max &check_date
  )],
);  


($exenam) = ($0 =~ /([^\/]*)\.\w+$/);
$prId = $$;
$logrRtn = sub {print time(), " $exenam $prId >", @_, "\<$prId\n";};

use XML::Simple;
use Data::Dumper;
use File::Basename;

sub XRprId {
  $prId = shift;
}

sub XRprintHASH {
  my ($href, $pfx) = @_;
  if ( ref($href) eq "HASH") {
    foreach my $var (sort keys %{ $href } ) {
	XRprintHASH($href->{$var},$pfx.'::'.$var);
    }
  } else {
    my ($varval) = ($href =~ /\W*(.*)\W*/);
#    print $pfx.' => '.$varval."\n";
  }
}

use POSIX qw(strftime);

#TODO verify: DateTime format
#sub GetDateTime {
#  return strftime('%Y-%m-%d %H:%M:%S.%U', localtime)
#}
sub GetDateTime {
  my $format = shift || ''; my @t = localtime(); local $_;

   $t[5] += 1900;
   $t[4] += 1;

   $t[0] = substr("0".$t[0],-2);
   $t[1] = substr("0".$t[1],-2);
   $t[2] = substr("0".$t[2],-2);
   $t[3] = substr("0".$t[3],-2);
   $t[4] = substr("0".$t[4],-2);

  $_ = $format;
  SWITCH: {
  
    /SQL/i and do {
      return join('/',@t[5,4,3]). " " . join(':', @t[2,1,0]);
	};

    return join('',@t[5,4,3,2,1,0]);
  }
}

sub max {
  return(($_[0]>=$_[1]) ? $_[0] : $_[1]);
}

sub check_date {
  return Date::Calc::check_date(@_);
}

sub parsePlFile {
  my $specfile = shift;
  &$logrRtn("now building code from $specfile");
  # --- build data processing subroutine ---
  open(SPEC, $specfile) or do {&$logrRtn("cannot open $specfile - $!"); return undef};
  my $plcode = "sub {\n";
  while (<SPEC>) {
    $plcode .= $_."\n";
  }
  close(SPEC);
  $plcode .= "}\n";

#  if ($debug) {
#    foreach (split /\n/, $plcode) {
#      &$logrRtn($_);
#    }
#  }

  my $subptr = eval($plcode);
  &$logrRtn($@) unless $subptr;
  return $subptr;

}

sub buildRtnHash {
  my $bindir = shift or return;

  return undef unless ( -d $bindir );
  my %rtnHash;
  for my $reffile (glob("$bindir/*")) {
    my ($plName, $plDir, $plExtension) = File::Basename::fileparse($reffile, '\.[^\.]+');
    next if $plName eq "dummy";
    $rtnHash{$plName} = parsePlFile($reffile);
  }
  return %rtnHash;
}

sub getConfHash {
  my $SrvName = $XReport::cfg->{SrvName}; my $ComputerName = uc($ENV{COMPUTERNAME}); my @r = (); 

  if ( exists($XReport::cfg->{$_}) ) {
    push @r, %{$XReport::cfg->{$_}};
  }
  if ( $ComputerName and exists($XReport::cfg->{host}->{$ComputerName}->{$_}) ) {
    push @r, %{$XReport::cfg->{host}->{$ComputerName}->{$_}};
  }
  if ( $SrvName and exists($XReport::cfg->{daemon}->{$SrvName}->{$_}) ) {
    push @r, %{$XReport::cfg->{daemon}->{$SrvName}->{$_}};
  }

  return {@r};
}

sub getConfValues {
  my $SrvName = $XReport::cfg->{SrvName}; my $ComputerName = uc($ENV{COMPUTERNAME}); my @r;
 
  ### todo: make merge for hashes

  for (@_) {
    if ( $SrvName and exists($XReport::cfg->{daemon}->{$SrvName}->{$_}) ) {
      push @r, $XReport::cfg->{daemon}->{$SrvName}->{$_};
    }
    elsif ( $ComputerName and exists($XReport::cfg->{host}->{$ComputerName}->{$_}) ) {
      push @r, $XReport::cfg->{host}->{$ComputerName}->{$_};
    }
    elsif ( exists($XReport::cfg->{$_}) ) {
      push @r, $XReport::cfg->{$_};
    }
    else {
      push @r, undef;
    }
#    print "$_ => $r[-1]\n";
  }
  return wantarray ? @r : $r[0];
}

sub setConfValues {
  while (@_) {
    my ($prmName, $prmValue) = (shift, shift);

    $XReport::cfg->{$prmName} = $prmValue;
  }
}

sub InitServer {
  my ($SrvName, $debugLevel, $daemonMode) = ('', 0, 0); my $rc; my %args = @_; 
  
  foreach my $i (0 .. $#ARGV) {
    if 	($ARGV[$i] eq "-d") { # set debugLevel 
      $debugLevel = 1;
    }
    elsif ($ARGV[$i] eq "-N") { # Server Name 
      $SrvName = $ARGV[$i+1];
    }
    elsif ($ARGV[$i] eq '-@') { # Daemon Mode 
      $daemonMode = 1;
    }
  }
  die "Server Name not specified - unable to continue" if !$SrvName;

  die "Server Name $SrvName not in config - unable to continue" 
    if 
  !exists($XReport::cfg->{daemon}->{$SrvName});

  my $workdir = getConfValues('workdir')."/$SrvName"; 
  
  mkdir $workdir;
  
  die "UNABLE TO CREATE WORKING DIRECTORY \"$workdir\"" if !-d $workdir;
  $daemonMode = 1 if $args{'isdaemon'};

  setConfValues('SrvName' => $SrvName, '$debugLevel' => $debugLevel, '$daemonMode' => $daemonMode);

  $logger = $main::logger;
  $logger = new XReport::Logger($SrvName) unless $logger; 

  $logrRtn = eval
   'sub { ' .
     '$logger->log(@_);' .
   '}'
  ;
  die "PROGRAM ERROR: logrRtn eval ERROR at InitServer $@\n" if $@;
  $logger->log("Server $SrvName Initialized: isdaemon: $args{isdaemon}; daemonMode: $daemonMode");

  require XReport::DBUtil; 

  XReport::DBUtil::GetServerLock() if !$args{'isdaemon'};

#  *i::logit = sub { $logger->log(@_); };

  *i::add_log_file = sub { $logger->AddFile(@_); };

  *i::remove_log_file = sub { $logger->RemoveFile(@_); };

  i::add_log_file('STDOUT', \*STDOUT) if !$daemonMode;

  my $logfname = getConfValues("logsdir")."/$SrvName.LOG";
#  rename $logfname, $logfname.strftime('%Y%m%d%H%M%S', localtime) if $args{'rotatelog'};
  
  i::add_log_file('ServerLog', $logfname);
}

sub TermServer {
}
=getPackageMethods
 this routine returns all the routine names defined in the package specified in the first input parm and that do not begin with "_".
 It returns an hash where the key is bthe routine name and the value is the reference to the code
=cut
sub getPackageMethods {
  my ($rclass, $types) = @_;
  $rclass = ref $rclass || $rclass;
  $types ||= '';
  my %classes_seen;
  my %methods;
  my @class = ($rclass);
  
  no strict 'refs';
  while (my $class = shift @class) {
    next if $classes_seen{$class}++;
    unshift @class, @{"${class}::ISA"} if $types eq 'all';
    # Based on methods_via() in perl5db.pl
    my $msel = $types eq 'all' ? qr/\(/ : qr/[(_]/;
    for my $method (grep {not /^$msel/ and 
			    defined &{${"${class}::"}{$_}}} 
		    keys %{"${class}::"}) {
      $methods{$method} = wantarray ? undef : $class->can($method); 
    }
  }
#  print "Methods supported by $rclass: ", join('::', keys %methods), "\n";  
  wantarray ? keys %methods : \%methods;
}


#variables for checkDate2Restart
use constant EXITCODE2RESTART => 123; 
use constant DEBUG => 0; 
my ($flagRotateSvcLog, $FORMATDATE2RESTART, $EXITCODE2RESTART);

sub checkDate2Restart
{ 
	#my $caller = uc(File::Basename::basename($0));
	my $caller = uc(File::Basename::basename((caller())[1]));
	
	if(!defined $flagRotateSvcLog)
	{
		$flagRotateSvcLog = 0;  
		$EXITCODE2RESTART = EXITCODE2RESTART;
		my $hash_LOGROTATION = getConfValues('LOGROTATION') ;
		if ( $hash_LOGROTATION =~ '^HASH')
		{
			$flagRotateSvcLog = 1 if ($hash_LOGROTATION->{'rotateSvcLogDaily'} =~ /^[ys1]$/i) ; 
			my $FREQUENCY = 'd'; 
			$FREQUENCY = $1 if(($hash_LOGROTATION->{'FREQUENCY'}) and ($hash_LOGROTATION->{'FREQUENCY'} =~ /^([dHMS])$/)); #d->each day, H->each hour, M->each minute, S->each second
			$FORMATDATE2RESTART = '%Y-%m-%d'          if($FREQUENCY eq 'd');
			$FORMATDATE2RESTART = '%Y-%m-%dT%H'       if($FREQUENCY eq 'H');
			$FORMATDATE2RESTART = '%Y-%m-%dT%H:%M'    if($FREQUENCY eq 'M'); 
			$FORMATDATE2RESTART = '%Y-%m-%dT%H:%M:%S' if($FREQUENCY eq 'S'); 
			$EXITCODE2RESTART = $1 if(($hash_LOGROTATION->{'EXITCODE2RESTART'}) and ($hash_LOGROTATION->{'EXITCODE2RESTART'} =~ /^(\d+)$/));
		} 
		i::logit("$caller:checkDate2Restart() - flagRotateSvcLog set to $flagRotateSvcLog FORMAT[$FORMATDATE2RESTART] RC[$EXITCODE2RESTART]");
	}
	return if(!$flagRotateSvcLog);
	
	my $currentDate = POSIX::strftime($FORMATDATE2RESTART, localtime);
	$main::initialDate = $currentDate if(!$main::initialDate);
	if($currentDate ne $main::initialDate)
	{
		i::logit("$caller:$currentDate ne $main::initialDate - the process will be terminated for LOG daily rotation with rc $EXITCODE2RESTART - PID $$ - ARGV(" . join(' ', @ARGV). ")");
		exit($EXITCODE2RESTART);
	}
	(DEBUG or $main::veryverbose) and i::logit("$caller:$currentDate eq $main::initialDate - $0 - PID $$");
}

#XReport::Util::logAndDie
sub logAndDie {
	my @callers = caller();
	my $caller = uc(File::Basename::basename($callers[1]));
	my $msg = shift;
	eval {i::logit("$msg requested by ".$caller."::". join('::', (@callers)[0,2]));};
	die $msg;
}
#XReport::Util::logAndCroak
sub logAndCroak {
	my @callers = caller();
	my $caller = uc(File::Basename::basename($callers[1])); 
	my $msg = shift;
	eval {i::logit("$msg requested by ".$caller."::". join('::', (@callers)[0,2]));};
	croak $msg;
}
1;

__END__

$Log: Util.pm,v $
Revision 1.2  2002/10/23 22:50:19  Administrator
edit

Revision 1.5  2001/04/10 04:12:52  mpezzi
Fixed script name parsing to support backslashes
Added load code handler
fixed child launcher to include ../perllib

Revision 1.4  2001/03/22 13:10:03  mpezzi
Aggiunte versioni ai file

