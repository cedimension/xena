package XReport::ENUMS;

# BEGIN block is necessary here so that other modules can use the constants.
BEGIN
{
require Exporter;

$VERSION = "0.11";
@ISA = qw( Exporter );

  use constant RF_ASCII => 1;
  use constant RF_EBCDIC => 2;
  use constant RF_AFP => 3;
  use constant RF_VIPP => 4;

  use constant XF_LPR => 1;
  use constant XF_ASCII => 1;
  use constant XF_PSF => 2;
  use constant XF_FTPB => 3;
  use constant XF_FTPC => 4;
  use constant XF_ASPSF => 4;
  use constant XF_SCSSTREAM => 5;
  use constant XF_AFPSTREAM => 6;
  use constant XF_MODCASTREAM => 7;
  use constant XF_SCS => 8;
  use constant XF_AS400 => 9;
  use constant XF_BINSTREAM => 10;

  use constant ST_ACCEPTED => 0;
  use constant ST_RECEIVING => 1;
  use constant ST_RECEIVED => 2;
  use constant ST_RECVERROR => 15;
  use constant ST_QUEUED => 16;
  use constant ST_INPROCESS => 17;
  use constant ST_COMPLETED => 18;
  use constant ST_PROCERROR => 31; 

  my @RF_ENUMS = qw( RF_ASCII RF_EBCDIC RF_AFP RF_VIPP );
 
  my @XF_ENUMS = qw(XF_LPR XF_ASCII XF_PSF XF_FTPB XF_FTPC XF_ASPSF XF_SCSSTREAM XF_AFPSTREAM XF_MODCASTREAM 
                    XF_SCS XF_AS400 XF_BINSTREAM);

  my @ST_ENUMS = qw(ST_ACCEPTED ST_RECEIVED ST_QUEUED ST_INPROCESS ST_COMPLETED ST_PROCERROR);

  @EXPORT_OK = ();
  %EXPORT_TAGS = ( 
   'RF_ENUMS' => \@RF_ENUMS,
   'XF_ENUMS' => \@XF_ENUMS,
   'ST_ENUMS' => \@ST_ENUMS
  );
          

  # Add all the constant names and error code names to @EXPORT_OK
  Exporter::export_ok_tags( 'RF_ENUMS', 'XF_ENUMS','ST_ENUMS' ); 
}

1;
