package XReport::Plugin;

use strict;

my %pluginsTable;

sub getPlugin {
  my $pluginStr = shift; 
  return $pluginsTable{$pluginStr} if exists($pluginsTable{$pluginStr});
  die "Plugins are not configured - insert \"plugins\" element into config\n" unless exists($XReport::cfg->{'plugins'});

  my $plugin = ( ref($XReport::cfg->{'plugins'})  eq 'ARRAY' ? $XReport::cfg->{'plugins'}->[0]
               : ref($XReport::cfg->{'plugins'}) eq 'HASH'  ? $XReport::cfg->{'plugins'}->{plugin} 
               : $XReport::cfg->{'plugins'} ); 

  die "UNABLE TO LOAD PLUGIN $pluginStr.\n" unless ($plugin and ref($plugin) eq "HASH" and exists($plugin->{$pluginStr}));
  die "PLUGIN $pluginStr Misconfigured - ref or class attributes must be specified"
     unless (exists($plugin->{$pluginStr}->{ref}) || exists($plugin->{$pluginStr}->{class}));
  my ( $ref, $class ) = @{$plugin->{$pluginStr}}{qw(ref class)};
    $pluginsTable{$pluginStr} = $ref ? require($ref) : $class;

  return $pluginsTable{$pluginStr};
}

*i::getPlugin = *getPlugin;

1;
