#######################################################################
# @(#) $Id: INPUT.pm,v 1.2 2002/10/23 22:50:18 Administrator Exp $
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
package XReport::INPUT;
use constant DEBUG => 0;
use strict;
use bytes;

use Symbol qw();

use File::Basename;
use Convert::EBCDIC;
use Carp;

use vars qw($VERSION @ISA @EXPORT_OK);

use Exporter;
our ($VERSION, @ISA, @EXPORT, @EXPORT_OK);
@ISA = qw(Exporter);
# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.
@EXPORT_OK = qw(
);

$VERSION = '0.10';

our $EBCDIC_To_ASCII_Table =
#   0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
 '\x00\x01\x02\x03\x9C\x09\x86\x7F\x97\x8D\x8E\x0B\x0C\x0D\x0E\x0F'.  # 0
 '\x10\x11\x12\x13\x9D\x85\x08\x87\x18\x19\x92\x8F\x1C\x1D\x1E\x1F'.  # 1
 '\x80\x81\x82\x83\x84\x0A\x17\x1B\x88\x89\x8A\x8B\x8C\x05\x06\x07'.  # 2
 '\x90\x91\x16\x93\x94\x95\x96\x04\x98\x99\x9A\x9B\x14\x15\x9E\x1A'.  # 3
 '\x20\xA0\xA1\xA2\xA3\xA4\xA5\xA6\xA7\xA8\xD5\x2E\x3C\x28\x2B\x7C'.  # 4
 '\x26\xA9\xAA\xAB\xAC\xAD\xAE\xAF\xB0\xB1\x21\x24\x2A\x29\x3B\x5E'.  # 5
 '\x2D\x2F\xB2\xB3\xB4\xB5\xB6\xB7\xB8\xB9\xE5\x2C\x25\x5F\x3E\x3F'.  # 6
 '\xBA\xBB\xBC\xBD\xBE\xBF\xC0\xC1\xC2\x60\x3A\x23\x40\x27\x3D\x22'.  # 7
 '\xC3\x61\x62\x63\x64\x65\x66\x67\x68\x69\xC4\xC5\xC6\xC7\xC8\xC9'.  # 8
 '\xCA\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\xCB\xCC\xCD\xCE\xCF\xD0'.  # 9
 '\xD1\x7E\x73\x74\x75\x76\x77\x78\x79\x7A\xD2\xD3\xD4\x5B\xD6\xD7'.  # A
 '\xD8\xD9\xDA\xDB\xDC\xDD\xDE\xDF\xE0\xE1\xE2\xE3\xE4\x5D\xE6\xE7'.  # B
 '\x7B\x41\x42\x43\x44\x45\x46\x47\x48\x49\xE8\xE9\xEA\xEB\xEC\xED'.  # C
 '\x7D\x4A\x4B\x4C\x4D\x4E\x4F\x50\x51\x52\xEE\xEF\xF0\xF1\xF2\xF3'.  # D
 '\x5C\x9F\x53\x54\x55\x56\x57\x58\x59\x5A\xF4\xF5\xF6\xF7\xF8\xF9'.  # E
 '\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\xFA\xFB\xFC\xFD\xFE\xFF'   # F
;

our $EBCDIC_To_ASCII_Table_IT =
#   0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
 '\x00\x01\x02\x03\xDC\x09\xC3\x7F\xCA\xB2\xD5\x0B\x0C\x0D\x0E\x0F'.  # 0
 '\x10\x11\x12\x13\xDB\xDA\x08\xC1\x18\x19\xC8\xF2\x1C\x1D\x1E\x1F'.  # 1
 '\xC4\xB3\xC0\xD9\xBF\x0A\x17\x1B\xB4\xC2\xC5\xB0\xB1\x05\x06\x07'.  # 2
 '\xCD\xBA\x16\xBC\xBB\xC9\xCC\x04\xB9\xCB\xCE\xDF\x14\x15\xFE\x1A'.  # 3
 '\x20\xFF\x83\x84\x7B\xA0\xC6\x86\x5C\xA4\xF8\x2E\x3C\x28\x2B\x21'.  # 4
 '\x26\x5D\x88\x89\x7D\xA1\x8C\x8B\x7E\xE1\x82\x24\x2A\x29\x3B\x5E'.  # 5
 '\x2D\x2F\xB6\x8E\xB7\xB5\xC7\x8F\x80\xA5\x95\x2C\x25\x5F\x3E\x3F'.  # 6
 '\x9B\x90\xD2\xD3\xD4\xD6\xD7\xD8\xDE\x97\x3A\x9C\xF5\x27\x3D\x22'.  # 7
 '\x9D\x61\x62\x63\x64\x65\x66\x67\x68\x69\xAE\xAF\xD0\xEC\xE7\xF1'.  # 8
 '\x5B\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\xA6\xA7\x91\xF7\x92\xCF'.  # 9
 '\xE6\x8D\x73\x74\x75\x76\x77\x78\x79\x7A\xAD\xA8\xD1\xED\xE8\xA9'.  # A
 '\xBD\x23\xBE\xFA\xB8\x40\xF4\xAC\xAB\xF3\xAA\x7C\xEE\xF9\xEF\x9E'.  # B
 '\x85\x41\x42\x43\x44\x45\x46\x47\x48\x49\xF0\x93\x94\xDD\xA2\xE4'.  # C
 '\x8A\x4A\x4B\x4C\x4D\x4E\x4F\x50\x51\x52\xFB\x96\x81\x60\xA3\x98'.  # D
 '\x87\xF6\x53\x54\x55\x56\x57\x58\x59\x5A\xFD\xE2\x99\xE3\xE0\xE5'.  # E
 '\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\xFC\xEA\x9A\xEB\xE9\x9F'   # F
;

our $EBCDIC_ITALIAN_To_ISO8859_1 =
#   0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
 '\x00\x01\x02\x03\x9C\x09\x86\x7F\x97\x8D\x8E\x0B\x0C\x0D\x0E\x0F'.  # 0
 '\x10\x11\x12\x13\x9D\x85\x08\x87\x18\x19\x92\x8F\x1C\x1D\x1E\x1F'.  # 1
 '\x80\x81\x82\x83\x84\x0A\x17\x1B\x88\x89\x8A\x8B\x8C\x05\x06\x07'.  # 2
 '\x90\x91\x16\x93\x94\x95\x96\x04\x98\x99\x9A\x9B\x14\x15\x9E\x1A'.  # 3
 '\x20\xA0\xE2\xE4\x7B\xE1\xE3\xE5\x5C\xF1\xB0\x2E\x3C\x28\x2B\x21'.  # 4
 '\x26\x5D\xEA\xEB\x7D\xED\xEE\xEF\x7E\xDF\xE9\x24\x2A\x29\x3B\x5E'.  # 5
 '\x2D\x2F\xC2\xC4\xC0\xC1\xC3\xC5\xC7\xD1\xF2\x2C\x25\x5F\x3E\x3F'.  # 6
 '\xF8\xC9\xCA\xCB\xC8\xCD\xCE\xCF\xCC\xF9\x3A\xA3\xA7\x27\x3D\x22'.  # 7
 '\xD8\x61\x62\x63\x64\x65\x66\x67\x68\x69\xAB\xBB\xF0\xFD\xFE\xB1'.  # 8
 '\x5B\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\xAA\xBA\xE6\xB8\xC6\xA4'.  # 9
 '\xB5\xEC\x73\x74\x75\x76\x77\x78\x79\x7A\xA1\xBF\xD0\xDD\xDE\xAE'.  # A
 '\xA2\x23\xA5\xB7\xA9\x40\xB6\xBC\xBD\xBE\xAC\x7C\xAF\xA8\xB4\xD7'.  # B
 '\xE0\x41\x42\x43\x44\x45\x46\x47\x48\x49\xAD\xF4\xF6\xA6\xF3\xF5'.  # C
 '\xE8\x4A\x4B\x4C\x4D\x4E\x4F\x50\x51\x52\xB9\xFB\xFC\x60\xFA\xFF'.  # D
 '\xE7\xF7\x53\x54\x55\x56\x57\x58\x59\x5A\xB2\xD4\xD6\xD2\xD3\xD5'.  # E
 '\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\xB3\xDB\xDC\xD9\xDA\x9F'   # F
;

our $EBCDIC_To_ASCII_Table_SIA =
#   0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
 '\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x0D\x20\x20'.  # 0
 '\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20'.  # 1
 '\x20\x20\x20\x20\x20\x0A\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20'.  # 2
 '\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20'.  # 3
 '\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2E\x3C\x28\x2B\x21'.  # 4
 '\x26\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x24\x2A\x29\x3B\x5E'.  # 5
 '\x2D\x2F\x20\x20\x20\x20\x20\x20\x20\x20\x7C\x2C\x25\x5F\x3E\x3F'.  # 6
 '\x20\x20\x20\x20\x20\x20\x20\x20\x20\x60\x3A\x23\x40\x27\x3D\x22'.  # 7
 '\x20\x61\x62\x63\x64\x65\x66\x67\x68\x69\x20\x7B\x20\x20\x20\x20'.  # 8
 '\x20\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x20\x7D\x20\x20\x20\x20'.  # 9
 '\x20\x7E\x73\x74\x75\x76\x77\x78\x79\x7A\x20\x20\x20\x5B\x20\x20'.  # A
 '\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x5D\x20\x20'.  # B
 '\x7B\x41\x42\x43\x44\x45\x46\x47\x48\x49\x20\x20\x20\x20\x20\x20'.  # C
 '\x7D\x4A\x4B\x4C\x4D\x4E\x4F\x50\x51\x52\x20\x20\x20\x20\x20\x20'.  # D
 '\x5C\x20\x53\x54\x55\x56\x57\x58\x59\x5A\x20\x20\x20\x20\x20\x20'.  # E
 '\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x20\x20\x20\x20\x20\x20'   # F
;

our $EBCDIC_To_ASCII_Table_SWIFT =
#   0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
 '\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x0D\x20\x20'.  # 0
 '\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20'.  # 1
 '\x20\x20\x20\x20\x20\x0A\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20'.  # 2
 '\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20'.  # 3
 '\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2E\x3C\x28\x2B\x21'.  # 4
 '\x26\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2A\x29\x3B\x20'.  # 5
 '\x2D\x2F\x20\x20\x20\x20\x20\x20\x20\x20\x20\x2C\x25\x5F\x3E\x3F'.  # 6
 '\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x3A\x23\x40\x27\x3D\x22'.  # 7
 '\x20\x61\x62\x63\x64\x65\x66\x67\x68\x69\x20\x20\x20\x20\x20\x20'.  # 8
 '\x20\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\x20\x20\x20\x20\x20\x20'.  # 9
 '\x20\x20\x73\x74\x75\x76\x77\x78\x79\x7A\x20\x20\x20\x5B\x20\x20'.  # A
 '\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x5D\x20\x20'.  # B
 '\x7B\x41\x42\x43\x44\x45\x46\x47\x48\x49\x20\x20\x20\x20\x20\x20'.  # C
 '\x7D\x4A\x4B\x4C\x4D\x4E\x4F\x50\x51\x52\x20\x20\x20\x20\x20\x20'.  # D
 '\x20\x20\x53\x54\x55\x56\x57\x58\x59\x5A\x20\x20\x20\x20\x20\x20'.  # E
 '\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x20\x20\x20\x20\x20\x20'   # F
;

our $EBCDIC_GERMAN273_To_ISO8859_1 = 
#   0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
'\x00\x01\x02\x03\x9C\x09\x86\x7F\x97\x8D\x8E\x0B\x0C\x0D\x0E\x0F'.  #0
'\x10\x11\x12\x13\x9D\x85\x08\x87\x18\x19\x92\x8F\x1C\x1D\x1E\x1F'.  #1
'\x80\x81\x82\x83\x84\x0A\x17\x1B\x88\x89\x8A\x8B\x8C\x05\x06\x07'.  #2
'\x90\x91\x16\x93\x94\x95\x96\x04\x98\x99\x9A\x9B\x14\x15\x9E\x1A'.  #3
'\x20\xA0\xE2\x7B\xE0\xE1\xE3\xE5\xE7\xF1\xC4\x2E\x3C\x28\x2B\x21'.  #4
'\x26\xE9\xEA\xEB\xE8\xED\xEE\xEF\xEC\x7E\xDC\x24\x2A\x29\x3B\x5E'.  #5
'\x2D\x2F\xC2\x5B\xC0\xC1\xC3\xC5\xC7\xD1\xF6\x2C\x25\x5F\x3E\x3F'.  #6
'\xF8\xC9\xCA\xCB\xC8\xCD\xCE\xCF\xCC\x60\x3A\x23\xA7\x27\x3D\x22'.  #7
'\xD8\x61\x62\x63\x64\x65\x66\x67\x68\x69\xAB\xBB\xF0\xFD\xFE\xB1'.  #8
'\xB0\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\xAA\xBA\xE6\xB8\xC6\xA4'.  #9
'\xB5\xDF\x73\x74\x75\x76\x77\x78\x79\x7A\xA1\xBF\xD0\xDD\xDE\xAE'.  #A
'\xA2\xA3\xA5\xB7\xA9\x40\xB6\xBC\xBD\xBE\xAC\x7C\xAF\xA8\xB4\xD7'.  #B
'\xE4\x41\x42\x43\x44\x45\x46\x47\x48\x49\xAD\xF4\xA6\xF2\xF3\xF5'.  #C
'\xFC\x4A\x4B\x4C\x4D\x4E\x4F\x50\x51\x52\xB9\xFB\x7D\xF9\xFA\xFF'.  #D
'\xD6\xF7\x53\x54\x55\x56\x57\x58\x59\x5A\xB2\xD4\x5C\xD2\xD3\xD5'.  #E
'\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\xB3\xDB\x5D\xD9\xDA\x9F'  #F
;

our $translator = Convert::EBCDIC->new($EBCDIC_ITALIAN_To_ISO8859_1);


use constant RF_ASCII => 1;
use constant RF_EBCDIC => 2;
use constant RF_AFP => 3;
use constant RF_VIPP => 4;
use constant RF_XML => 5;
use constant RF_POSTSCRIPT => 6;
use constant RF_SCS => 7;

#abbiamo : quante righe inserire prima, se stampare, quante righe inserire dopo
# dobbiamo mantenere dove siamo arrivati con la pagina successiva per lo skip

our %asaExecTable = (
 "\x40" =>  [  1,  1,  0  ],   # { 1 a.spaceLines a.PrintLine}
 "\xf0" =>  [  2,  1,  0  ],   # { 2 a.spaceLines a.PrintLine}
 "\x60" =>  [  3,  1,  0  ],   # { 3 a.spaceLines a.PrintLine}
 "\x4e" =>  [  0,  1,  0  ],   # { a.PrintLine }
 "\xf1" =>  [ -1,  1,  0  ],   # { 1 a.skipLines a.PrintLine }
 "\xf2" =>  [ -2,  1,  0  ],   # { 2 a.skipLines a.PrintLine }
 "\xf3" =>  [ -3,  1,  0  ],   # { 3 a.skipLines a.PrintLine }
 "\xf4" =>  [ -4,  1,  0  ],   # { 4 a.skipLines a.PrintLine }
 "\xf5" =>  [ -5,  1,  0  ],   # { 5 a.skipLines a.PrintLine }
 "\xf6" =>  [ -6,  1,  0  ],   # { 6 a.skipLines a.PrintLine }
 "\xf7" =>  [ -7,  1,  0  ],   # { 7 a.skipLines a.PrintLine }
 "\xf8" =>  [ -8,  1,  0  ],   # { 8 a.skipLines a.PrintLine }
 "\xf9" =>  [ -9,  1,  0  ],   # { 9 a.skipLines a.PrintLine }
 "\xc1" =>  [-10,  1,  0  ],   # {10 a.skipLines a.PrintLine }
 "\xc2" =>  [-11,  1,  0  ],   # {11 a.skipLines a.PrintLine }
 "\xc3" =>  [-12,  1,  0  ],   # {12 a.skipLines a.PrintLine }
);

our %machineExecTable = (
 "\x03" =>  [  0,  0,  0  ],   # {}
 "\x09" =>  [  0,  1,  1  ],   # { a.PrintLine 1 a.spaceLines }
 "\x11" =>  [  0,  1,  2  ],   # { a.PrintLine 2 a.spaceLines }
 "\x19" =>  [  0,  1,  3  ],   # { a.PrintLine 3 a.spaceLines }
 "\x01" =>  [  0,  1,  0  ],   # { a.PrintLine }
 "\x89" =>  [  0,  1, -1  ],   # { a.PrintLine 1 a.skipLines }
 "\x91" =>  [  0,  1, -2  ],   # { a.PrintLine 2 a.skipLines }
 "\x99" =>  [  0,  1, -3  ],   # { a.PrintLine 3 a.skipLines }
 "\xa1" =>  [  0,  1, -4  ],   # { a.PrintLine 4 a.skipLines }
 "\xa9" =>  [  0,  1, -5  ],   # { a.PrintLine 5 a.skipLines }
 "\xb1" =>  [  0,  1, -6  ],   # { a.PrintLine 6 a.skipLines }
 "\xb9" =>  [  0,  1, -7  ],   # { a.PrintLine 7 a.skipLines }
 "\xc1" =>  [  0,  1, -8  ],   # { a.PrintLine 8 a.skipLines }
 "\xc9" =>  [  0,  1, -9  ],   # { a.PrintLine 9 a.skipLines }
 "\xd1" =>  [  0,  1,-10  ],   # { a.PrintLine 10 a.skipLines }
 "\xd9" =>  [  0,  1,-11  ],   # { a.PrintLine 11 a.skipLines }
 "\xe1" =>  [  0,  1,-12  ],   # { a.PrintLine 12 a.skipLines }
 "\x0b" =>  [  0,  0,  1  ],   # { pop  1 a.spaceLines }
 "\x13" =>  [  0,  0,  2  ],   # { pop  2 a.spaceLines }
 "\x1b" =>  [  0,  0,  3  ],   # { pop  3 a.spaceLines }
 "\x8b" =>  [  0,  0, -1  ],   # { pop  1 a.skipLines }
 "\x93" =>  [  0,  0, -2  ],   # { pop  2 a.skipLines }
 "\x9b" =>  [  0,  0, -3  ],   # { pop  3 a.skipLines }
 "\xa3" =>  [  0,  0, -4  ],   # { pop  4 a.skipLines }
 "\xab" =>  [  0,  0, -5  ],   # { pop  5 a.skipLines }
 "\xb3" =>  [  0,  0, -6  ],   # { pop  6 a.skipLines }
 "\xbb" =>  [  0,  0, -7  ],   # { pop  7 a.skipLines }
 "\xc3" =>  [  0,  0, -8  ],   # { pop  8 a.skipLines }
 "\xcb" =>  [  0,  0, -9  ],   # { pop  9 a.skipLines }
 "\xd3" =>  [  0,  0,-10  ],   # { pop 10 a.skipLines }
 "\xdb" =>  [  0,  0,-11  ],   # { pop 11 a.skipLines }
 "\xe3" =>  [  0,  0,-12  ],   # { pop 12 a.skipLines }
);

our %ExecTable = (
  %asaExecTable, %machineExecTable,
  "\xff" =>  [  1,  1,  0  ]
);

our @fcb2st60 = (
  1,0,0,0,0,  2,0,0,0,0,  3,0,0,0,0,  4,0,0,0,0,  5,0,0,0,0,  6,0,0,0,0,
  7,0,0,0,0,  8,0,0,0,0,  10,0,0,0,0,  11,0,0,0,0,
  0,0,0,0,0,  0,12,0,9,0, 0,0,0,0,0,0  # 5 blank lines added (from top ?)
);


our $lx_idm = qr/^(?s)\x5a..\xd3\xab\xca/;
our $lx_nop = qr/^(?s)\x5a..\xd3\xee\xee/;

our $TLE = undef;
our $NOP = undef; ### mpezzi nop handling

 #todo: gestire imagelib;
use XReport::Util;


use constant XF_LPR => 1;
use constant XF_PSF => 2;
use constant XF_FTPB => 3;
use constant XF_FTPC => 4;
use constant XF_ASPSF => 5;
use constant XF_AFPSTREAM => 6;
use constant XF_MODCASTREAM => 7;
use constant XF_SCS => 8;
use constant XF_AS400 => 9;

sub LogIt { 
  my $self = shift; &$logrRtn(@_);
}

sub getJobReportValues { 
  my $self = shift;
  return undef unless exists($self->{jr}) && $self->{jr};
  die "Handler ", ref($self), "passed by ", join('::', (caller(0))[0,2]), " does not have JR"
     unless exists($self->{jr}) && $self->{jr};
  $self->{jr}->getValues(@_);
}

sub setJobReportValues {
  my $self = shift;
  $self->{jr}->setValues(@_);
}

sub ReportFormatIs {
  my ($self, $rf) = @_; my ($INPUT, $getLine, $XferMode) = @{$self}{qw(INPUT getLine XferMode)};

  #return RF_AFP if $self->getJobReportValues('FormDef') ne '';
  $main::debug && i::warnit(ref($self)." ReportFormatIs RF: $rf XF: $XferMode LFidm: $lx_idm LFnop: $lx_nop EOF: ".$INPUT->eof()
                           ." checking for not ASCII nor EBCDIC(".join(',', (RF_ASCII, RF_EBCDIC)).")");
  if ( $rf != RF_ASCII and $rf != RF_EBCDIC ) {
    return $rf;
  }

  $main::debug && i::warnit(ref($self)." ReportFormatIs checking XFERMODE for PSF, ASPSF, AFPSTREAM (".join(',', (XF_PSF, XF_ASPSF, XF_AFPSTREAM)).")");
#  return (RF_EBCDIC) if $XferMode == XF_PSF;
  return (RF_EBCDIC) if $XferMode == XF_ASPSF;
  return (RF_AFP)    if $XferMode == XF_AFPSTREAM;


  ### todo: RF_ASCII -> RF_STREAMCC  RF_EBCDIC -> RF_ASAMACC
  ### todo: introduce EC_ASCII EC_EBCDIC
  my $TestAscii = " 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  my $TestEbcdic = $translator->toebcdic($TestAscii);
  my ($isascii, $isebcdic) = (0, 0); my ($cc, $line);
  for (my $i;$i<100 and !$INPUT->eof(); $i++) {
      $line = $self->getLine($INPUT); $cc = substr($line,0,1);
      my ($a, $e) = ($line, $line);
      $isascii += eval("\$a =~ tr/$TestAscii//d");
      $isebcdic += eval("\$e =~ tr/$TestEbcdic//d");
      (DEBUG or $main::veryverbose) && i::warnit(ref($self)." ReportFormatIs checking isascii: $isascii isebcdic: $isebcdic line: ".unpack('H40', $line));
#     if ($cc eq "\x5a" ) {
      if ($cc eq "\x5a" && $line !~ /^$lx_idm/o && $line !~ /^$lx_nop/o) {
		return RF_AFP;
      }

  }
  return $rf if ($isascii == $isebcdic);
  return ($isascii>$isebcdic) ? RF_ASCII : RF_EBCDIC;
}

sub XferModeIsAS400 {
  my ($self, $INPUT) = @_; my ($XferMode, $ReportFormat, $PrinterType); require XReport::INPUT::AS400;

  $INPUT = XReport::INPUT::AS400::reader->new($INPUT, $self->{jr});
  $PrinterType = $INPUT->getValues("Printer device type");

  if ($PrinterType eq "*SCS") {
    $XferMode = XF_SCS,  $ReportFormat = RF_SCS;
  }
  else {
    die("INVALID Printer device type ($PrinterType) DETECTED");
  }

  return ($INPUT, $XferMode, $ReportFormat);
}

sub setTranslator {


  if((getConfValues('CODEPAGE') =~ /^GERMAN273$/i) or  ( ($XReport::cfg->{CODEPAGE}) and ( $XReport::cfg->{CODEPAGE} =~ /^GERMAN273$/i) ))
  {
    i::logit("CODEPAGE changed to EBCDIC_GERMAN273_To_ISO8859_1");
	$translator = Convert::EBCDIC->new($EBCDIC_GERMAN273_To_ISO8859_1); #GERMAN273
	return;
  }  
  
  my $self = shift; my $jr = $self->{jr}; my ($translate_table, $TRANSLATE) = ("", Symbol::gensym());

  $translate_table = $jr->getFileName("TRANSLATE", $translate_table || "01180333");

  open($TRANSLATE, "<$translate_table")
    or
  die("ERROR OPENING TRANSLATE_TABLE FILE \"$translate_table\" rc=$!"); binmode($TRANSLATE);

  read($TRANSLATE, $translate_table, -s $translate_table); close($TRANSLATE);

  do {
    my $chars = $translate_table; $translate_table = ''; my @chars;
    for (0..15) {
      @chars =
        map {"\\x".uc($_)} unpack("H2"x16, substr($chars, $_*16,16))
      ;
      $translate_table .= join("", @chars);
    }
  };
  $translator = Convert::EBCDIC->new($translate_table);
}

sub setValues {
  my $self = shift; while(@_) { my ($k, $v) = (shift, shift); $self->{$k} = $v; }
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};
  if ($_[0] eq 'finalize' && $_[1] ) {
#   warn ref($self)." INPUT Close final wrapper invoked INPUT: ".ref($INPUT)."\n";
    $self->{jr}->loadBlockBtree($self->getJobReportValues(qw(JobReportId)), $INPUT->{INPUT});
  }
  return $INPUT->Close(@_);
}

sub new {
  #my ($className, $jr) = @_;
  my ($className, $jr, $ForcedReportFormat) = @_;

  (DEBUG or $main::veryverbose) and i::warnit("Initializing new instance of $className");

  my $self = { jr => $jr };

  bless $self, $className;
  my ($INPUT, $l);
  my ($XferMode, $ReportFormat, $LinesPerPage, $HasCc, $Formatted, $ijrar, $pagedef)
   =
  $self->getJobReportValues(qw(XferMode ReportFormat LinesPerPage HasCc Formatted ijrar PageDef));

  my ( $ElabContext ) = $self->getJobReportValues(qw(ElabContext));
  $self->setValues(ElabContext => $ElabContext);
  unless ( defined($ijrar) && ref($ijrar) ) {
  	my $diemsg = __PACKAGE__."Input Archive handler undefined";
    i::logit($diemsg);
    die $diemsg;
  }
  $main::debug and i::warnit("Acquiring INPUT Stream handler using ".ref($ijrar));
  $INPUT = $ijrar->get_INPUT_STREAM($jr, 'data') ||
    die "get INPUT STREAM failed ijrar: ", ref($ijrar), "\n";

  my ( $charsperline ) = 400;
  my $handler = $INPUT->Open();
  if ( exists($handler->{cinfo}) ) {
    $self->setJobReportValues(cinfo => {%{$handler->{cinfo}}});
  }
  else {
    $self->setJobReportValues(cinfo => {});
  }
  if ( exists($handler->{cinfo}->{maxlrec_detected}) ) {
    $charsperline = $handler->{cinfo}->{maxlrec_detected}
            if ( $charsperline > $handler->{cinfo}->{maxlrec_detected});
  }
  $main::debug and i::warnit("INPUT ATTRIBUTES CPL: $charsperline LPP: $LinesPerPage");
  $self->setValues(MaxLineLength => $charsperline);
  $self->setJobReportValues(MaxLineLength => $charsperline);

  die "UserError: INPUT FILE STREAM IS EMPTY !!"
   if
  $INPUT->read($l, 1) == 0;

  $self->setJobReportValues('isAfp', 0, 'isFullAfp', 0);
  (
    $INPUT, $XferMode, $ReportFormat
  ) =
  $self->XferModeIsAS400($INPUT) if $XferMode == XF_AS400; $INPUT->Close(); $self->setTranslator();

  #my ($LinesPerPage, $HasCc, $Formatted) = $self->getJobReportValues(qw(LinesPerPage HasCc Formatted));
  my ($LinesPerPageNEW, $HasCcNEW, $FormattedNEW) = $self->getJobReportValues(qw(LinesPerPage HasCc Formatted));
  $LinesPerPage = $LinesPerPageNEW;
  $HasCc = $HasCcNEW;
  $Formatted = $FormattedNEW;

  $self->setValues(
    XferMode => $XferMode, ReportFormat => $ReportFormat, Formatted => $Formatted
  );
  
  $INPUT = XReport::INPUT::ILine->new($INPUT, $XferMode, $ReportFormat);
  
  #-- Verify report_format ---------------------------//

  $self = XReport::INPUT::IPage->new($self, $INPUT);
  $self->Open();

  $ReportFormat = $self->ReportFormatIs($ReportFormat);

  if ( (!$ForcedReportFormat) and (getConfValues("FORCE_RF") =~ /^(RF_)(ASCII|EBCDIC|AFP|VIPP|XML|POSTSCRIPT|SCS)$/i ) )
  {
	$ReportFormat = eval( $1.$2 );
	i::logit(__PACKAGE__."::sub new - ReportFormat forced to $ReportFormat ($2) because \"FORCE_RF\" parameter found in xml configuration.");
  }

  $ReportFormat = $ForcedReportFormat if $ForcedReportFormat;

  $self->setJobReportValues(
    "ReportFormat" => $ReportFormat
  );

  $self->Close();
  
  #---------------------------------------------------//

  $self->setValues(lpp => $LinesPerPage || 0, HasCc => $HasCc || 0);


  if ( $ReportFormat == RF_ASCII ) {
    $LinesPerPage = 0 if $pagedef =~ /^P1/;;
    $self->setValues( lpp => $LinesPerPage );
    $self = XReport::INPUT::AsciiIPage->new($self);
  }
  elsif ( $ReportFormat == RF_EBCDIC ) {
    $self = XReport::INPUT::EbcdicIPage->new($self);
  }
  elsif ( $ReportFormat == RF_AFP ) {
    $self = XReport::INPUT::AfpIPage->new($self);
  }
  elsif ( $ReportFormat == RF_VIPP ) {
    $self = XReport::INPUT::VippIPage->new($self);
  }
  elsif ( $ReportFormat == RF_XML ) {
    $self = XReport::INPUT::XMLIPage->new($self);
  }
  elsif ( $ReportFormat == RF_POSTSCRIPT ) {
    $self = XReport::INPUT::PostscriptIPage->new($self);
  }
  elsif ( $ReportFormat == RF_SCS ) {
    $self = XReport::INPUT::EbcdicIPage->new($self, RF_SCS);
  }
  else {
    die("UserError: Invalid ReportFormat: $ReportFormat\.");
  }
  $main::debug and i::warnit( "INPUT new input file handled by " . ref($self) . " INPUT Hascc= $HasCc lpp=$LinesPerPage ");
  return $self;
}

1;

#------------------------------------------------------------
package XReport::INPUT::IZlib;
use constant DEBUG => 0;

use strict; use bytes;

use Symbol;
use Compress::Zlib;

#manage using original file size for read and seek;

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};

  if ( ref($INPUT) eq 'XReport::INPUT::IZlib' ) {
    $self->Close();
  }

  $INPUT = gzopen($self->{fileName}, "rb")
   or
  die("GZOPEN INPUT ERROR for $self->{fileName} rc=$!");

  @{$self}{qw(INPUT ByteCache loffset)} = ($INPUT, "", 0); return $self;
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};

  $INPUT->gzclose();

  @{$self}{qw(INPUT ByteCache loffset)} = ($INPUT = undef, "", 0); return $self;
}

sub read {
  my $self = shift; my ($INPUT, $loffset) = @{$self}{qw(INPUT loffset)}; my $br;
#  $main::reads += 1; $main::fpos = $INPUT->gztell() - $loffset;

  if (!$loffset) {
    $br = $INPUT->gzread($_[0], $_[1]);
  }
  else {
    $_[0] = "\x00" x $_[1];
    substr($_[0], 0, $loffset)
     =
    substr($self->{byteCache}, -$loffset);
    @{$self}{qw(byteCache loffset)} = ("", 0);
    $br = $INPUT->gzread(my $buff, $_[1]-$loffset);
    substr($_[0], $loffset, $_[1]-$loffset) = $buff;
  }

  @{$self}{qw(byteCache loffset)} = ($_[0], 0), return $br+$loffset if $br >= 0;

  die("GZREAD ERROR DETECTED (rc=$br). FileName=$self->{fileName} gzerror=".$INPUT->gzerror());
}

sub tell {
  my $self = shift; my ($INPUT, $loffset) = @{$self}{qw(INPUT loffset)};

  my $bo = $INPUT->gztell() - $loffset;

  return $bo if $bo >= 0 or $bo < 0; # $bo < 0 (-1) if fileSize > 2GigaBytes

  die("GZTELL ERROR DETECTED (rc=$bo). FileName=$self->{fileName} gzerror=".$INPUT->gzerror());
}

sub seek {
  my $self = shift; my($offset, $whence) = @_; my ($INPUT, $byteCache, $loffset) = @{$self}{qw(INPUT byteCache loffset)};

  if ($offset < 0 and $whence == 1 and length($byteCache) >= abs($offset)) {
    $self->{loffset} = -$offset;
    return $INPUT->gztell() + $offset;
  }
  else {
    $self->{loffset} = 0;
  }

  my $bo = $INPUT->gzseek($_[0], $_[1]);

  return $bo if $bo >= 0;

  die("GZSEEK ERROR DETECTED (rc=$bo). FileName=$self->{fileName} gzerror=".$INPUT->gzerror());
}

sub eof {
  my $self = shift; my $INPUT = $self->{INPUT};

  return $INPUT->gzeof();
}

sub lastModTime {
  my $self = shift; (stat($self->{fileName}))[9];
}

sub new {
  my ($className, $fileName) = @_; my $self = {};

  @{$self}{qw(fileName byteCache loffset)} = ($fileName, "", 0);
  bless $self;
}

#------------------------------------------------------------
package XReport::INPUT::IPlain;
use constant DEBUG => 0;

use strict; use bytes;

use Symbol;

#manage using original file size for read and seek;

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};

  if (!$INPUT) {
    $INPUT = gensym();
  }
  else {
    $self->Close();
  }
  open($INPUT,"<",$self->{fileName}); binmode($INPUT);

  return $self->{INPUT} = $INPUT;;
}

sub Close {
  return CORE::close($_[0]->{INPUT});
}

sub read {
  return CORE::read($_[0]->{INPUT}, $_[1], $_[2]);
}

sub tell {
  return CORE::tell($_[0]->{INPUT});
}

sub eof {
  return CORE::eof($_[0]->{INPUT});
}

sub new {
  my ($className, $fileName) = @_; my $self = {};

  $self->{fileName} = $fileName;

  bless $self;
}

#------------------------------------------------------------
package XReport::INPUT::AsciiLine;
use constant DEBUG => 0;

use strict; use bytes;

sub log {
  my $self = shift; require XReport::Util;

  &{$XReport::Util::logrRtn}(
   "Begin log of Line ", $self->AsciiValue() ,"End log of Line."
  );
}

sub cleanLine(\$) {
  my $ref = shift; my ($cc, $line) = (substr($$ref,0,1), $$ref);
  if ( length($line) > 2 ) {
    substr($line,2) =~ s/ +$//;
    $$ref = $line;
  }
}

sub overwriteLine {
  my ($self, $line) = (shift, shift); return if $line =~ /^. *$/;

  my $line_0 = $self->[0]; my $ol = $self->[4];

  if (!@$ol) {
    for (1..(length($line) < length($line_0)-1 ? length($line) : length($line_0)-1)) {
      my $c =substr($line,$_,1); my $c_0 = substr($line_0,$_,1);
      if ($c ne " " and $c_0 ne " ") {
        goto DIFFERENT;
      }
    }
    $line =~ s/[\x20\x5f]/\x00/g; $self->[0] |= $line; $self->[0] =~ s/\x00/\x20/g;
  }

  DIFFERENT:
  if ($self->[0] !~ /^.\ *$/) {
    my $ol = $self->[4]; push @$ol, $self->[0] if !@$ol; push @$ol, $line;

#    $eraser =~ s/[^\x20\x5f]/\x00/g; $eraser =~ s/[^\x00]/\xff/g; $self->[0] &= $eraser;
    $line =~ s/[\x20\x5f]/\x00/g; $self->[0] |= $line; $self->[0] =~ s/\x00/\x20/g;
  }
  else {
    $self->[0] = $line;
  }
}

sub AsciiValue {
  my $self = shift;

  if ( defined($_[0]) ) {
    $self->[0] = $_[0];
  }
  return $self->[0];
}

sub Value {
  my $self = shift;

  if ( defined($_[0]) ) {
    $self->[0] = $_[0];
  }
  return $self->[0];
}

sub atLine {
  my ($self, $beg) = (shift, shift || 1);

  if ( defined($_[0]) ) {
    $self->[3] = $_[0];
  }
  return $self->[3] + ($beg == 1 ? 0 : $self->[2]);
}

sub new {
  my ($className, $page, $atLine, $line) = @_; cleanLine($line);

  my $self = [$line, "", $page->atLine(), $atLine, []];

  bless $self, $className;
}

sub AsciiValueList {
  my $self = shift; my $ol = $self->[4]; return [$self->AsciiValue()] if !@$ol;
  my @lines = map {
    my $l = $_; $l =~ s/\x00/ /g; $l
  }
  @$ol; return \@lines;
}

#------------------------------------------------------------
package XReport::INPUT::EbcdicLine;
use constant DEBUG => 0;

use strict; use bytes;

sub cleanLine(\$) {
  my $ref = shift; my ($cc, $line) = (substr($$ref,0,1), $$ref);
  if ( $cc eq "\x5a" ) {
    ### delete trailing spaces of afp records
    $line = substr($line, 0, unpack("n", substr($line,1,2))+1);
  }
  elsif ( length($line) > 2 ) {
    substr($line,2) =~ s/[\x00-\x3f]/\x40/g;
    substr($line,2) =~ s/\x40+$//;
  }
  $$ref = $line;
}

sub overwriteLine {
  my ($self, $line) = (shift, shift); return if $line =~ /^.\x40*$/; $self->[1] = "";

  my $line_0 = $self->[0]; my $ol = $self->[4];

  if (!@$ol) {
    for (1..(length($line) < length($line_0)-1 ? length($line) : length($line_0)-1)) {
      my $c =substr($line,$_,1); my $c_0 = substr($line_0,$_,1);
      if ($c ne "\x40" and $c_0 ne "\x40") {
        goto DIFFERENT;
      }
    }
    $line =~ s/[\x40\x6d]/\x00/g; $self->[0] |= $line; $self->[0] =~ s/\x00/\x40/g; return;
  }

  DIFFERENT:
  if ($self->[0] !~ /^.\x40*$/) {
    push @$ol, $self->[0] if !@$ol; push @$ol, $line;

#    $eraser =~ s/[^\x40\x6d]/\x00/g; $eraser =~ s/[^\x00]/\xff/g; $self->[0] &= $eraser;
    $line =~ s/[\x40\x6d]/\x00/g; $self->[0] |= $line; $self->[0] =~ s/\x00/\x40/g;
  }
  else {
    $self->[0] = $line;
  }
}

sub AsciiValue {
  my $self = shift;

  if ( defined($_[0]) ) {
    $self->[1] = $_[0];
  }
  if ( $self->[1] eq "" ) {
    my $line = $self->[0]; my $cc = substr($line,0,1);
    if ($cc eq "\x5a" ) {
      $self->[1] = unpack("H24", $line);
    }
    else {
      $self->[1] = $cc.$translator->toascii(substr($line,1)); $self->[1] =~ s/\x00/ /g;
    }
  }

  return $self->[1];
}

sub AsciiValueList {
  my $self = shift; my $ol = $self->[4]; return [$self->AsciiValue()] if !@$ol;
  my @lines = map {
    my $l = substr($_,0,1) . $translator->toascii(substr($_,1)); $l =~ s/\x00/ /g; $l
  }
  @$ol; return \@lines;
}

sub Value {
  my $self = shift;

  if ( defined($_[0]) ) {
    $self->[0] = $_[0];
  }
  return $self->[0];
}

sub atLine {
  my ($self, $beg) = (shift, shift || 1);
  (DEBUG or $main::veryverbose) and i::warnit(ref($self)."::atLine called with $beg by ".join('::', (caller())[0,2]));
  if ( defined($_[0]) ) {
    $self->[3] = $_[0];
  }
  return $self->[3] + ($beg == 1 ? 0 : $self->[2]);
}

sub new {
  my ($className, $page, $atLine, $line) = @_; cleanLine($line);

  my $self = [$line, "", $page->atLine(), $atLine, []];

  bless $self, $className;
}

#------------------------------------------------------------
package XReport::INPUT::RasterLine;
use constant DEBUG => 0;

use Carp;

sub new {
  my ($className, $line, $atLine) = @_;

  my $self = [$line, $atLine];

  bless $self, $className;
}
sub AsciiValue {
  my $self = shift;
  return $self->[0];
}

sub Value {
  my $self = shift;
  return $self->[0];
}

sub atLine {
  my $self = shift;
  if ( defined($_[0]) ) {
    $self->[1] = $_[0];
  }
  return $self->[1];
}



#------------------------------------------------------------
package XReport::INPUT::ILine;
use constant DEBUG => 0;

use strict; use bytes;

use constant XF_LPR => 1;
use constant XF_PSF => 2;
use constant XF_FTPB => 3;
use constant XF_FTPC => 4;
use constant XF_ASPSF => 5;
use constant XF_AFPSTREAM => 6;
use constant XF_MODCASTREAM => 7;
use constant XF_SCS => 8;
use constant XF_AS400 => 9;
use constant XF_FB80 => "A";

my $cs_asciidelim = quotemeta("\r\n\f\x0c");

my $lx_ascii = qr/([$cs_asciidelim]|[^$cs_asciidelim]+)/o;

sub getAsciiILine {
  my $self = shift; my ($INPUT, $byteCache) = @{$self}{qw(INPUT byteCache)};

  if ($INPUT->eof() and length($byteCache) == 0) {
    return undef;
  }
  my ($at, $at_max) = (0, length($byteCache));

  my ($c, $outrec) = ("", " ", 0);

  my $lastc = '';

  while( 1 ) {
    if ( ($at_max-$at) < 2048 && !$INPUT->eof() ) {
      $byteCache = substr($byteCache, $at);
      $INPUT->read($_, 2048);
      $byteCache .= $_;
      $at=0; $at_max = length($byteCache);
    }
    last if $at >= $at_max;

    ($c) = substr($byteCache, $at, 64) =~ /^($lx_ascii)/o; $at += length($c);

    if ( $c eq "\r" ) {
      if ( $outrec =~ /^.[ \x00]*$/ ) {
        substr($outrec,1) = "";
      }
    }
    elsif ( $c eq "\n" ) {
      last;
    }
    elsif ( $c eq "\f" or $c eq "\x0c") {
      if ( length($outrec) > 1 ) {
        $at--; last;
      }
      else {
        $outrec = "1";
      }
    }
    else {
      substr($outrec, 1) = "" if $lastc eq "\r";
      $outrec .= $c;
    }

    $lastc = $c;
  }
  $self->{byteCache} = substr($byteCache, $at);
  $outrec =~ tr/\x00/ /;
  return $outrec;
}

sub getModebILine {
  my $self = shift; my $INPUT = $self->{INPUT};

  my ($rc, $d3, $b1, $d2, $outrec, $cc, $data) = ();

  $rc = $INPUT->read($d3,3);
  ($b1, $d2) = unpack("an", $d3);
  $rc = $INPUT->read($outrec, $d2);

  if ( $b1 eq "\x10" ) {
    return getModebLine();
  }

  return $outrec;
}

sub getModecILine {
  my $self = shift; my ($INPUT, $byteCache) = @{$self}{qw(INPUT byteCache)};

  my ($inrec, $incntrl, $outrec, $b1, $rc); $incntrl = "";

  my ($at, $at_max) = (0, length($byteCache));


  while( 1 ) {
    if ( ($at_max-$at) < 2048 && !$INPUT->eof() ) {
      $byteCache = substr($byteCache, $at);
      $INPUT->read($_, 2048);
      $byteCache .= $_;
      $at=0; $at_max = length($byteCache);
    }
    last if $at >= $at_max;

    $b1 = substr($byteCache,$at++,1); $b1 = unpack("C", $b1);

    if ( $b1 == 0 ) {
      $incntrl = substr($byteCache,$at++,1);
      if ( $incntrl ne "\x80" and $incntrl ne "\x40" ) {
        $inrec = substr($byteCache,$at,10); $at += 10;
        die("UserError: INVALID MODEC $at CONTROL BYTE ".unpack("H*", $incntrl.$inrec));
      }
      next;
    }
    elsif ( ($b1 & 0x80) == 0 ) {
      $inrec = substr($byteCache,$at,$b1); $at += $b1;
      $outrec .= $inrec;
    }
    elsif ( ($b1 & 0xc0) == 0x80 ) {
      $inrec = substr($byteCache,$at++,1);
      $rc = $b1 & 0x3f;
      $outrec .= $inrec x $rc;
    }
    elsif ( ($b1 & 0xc0) == 0xc0 ) {
      $rc = $b1 & 0x3f;
      $outrec .= "\x40" x $rc; #FTC si comporta in modo diverso a seconda se chiamato in modo EBCDIC o BINARY
      #$outrec .= "\x00" x $rc;
    }
    else {
      $inrec = substr($byteCache,$at,10); $at += 10;
      die("UserError: INVALID MODEC MASK ".unpack("H*", $inrec));
    }
    last if $incntrl ne "";
  }
  $self->{byteCache} = substr($byteCache, $at);

  return $outrec;
}

sub getFB80Line {
  my $self = shift; my $INPUT = $self->{INPUT};

  my ($rc, $lr, $outrec) = ();

  $rc = $INPUT->read($outrec, 80);

  return $outrec;
}

sub getPsfILine {
  my $self = shift; my $INPUT = $self->{INPUT};
  (DEBUG or $main::veryverbose) and i::warnit(ref($self)." reading PSF Line using ". ref($INPUT). " caller ". join('::', (caller())[0,2]));
  my ($rc, $lr, $outrec) = ();
  my $iar_rtn = $INPUT->can('iarLine');
#  my $currline = ($iar_rtn && ref($iar_rtn) eq 'CODE' ? &$iar_rtn($INPUT) : 0);
  $rc = $INPUT->read($lr,2);
  if ($rc && $rc == 2) {
    $lr = unpack("n", $lr);
    $rc = $INPUT->read($outrec, $lr);
	(DEBUG or $main::veryverbose) && i::warnit(ref($self)." reading PSF Line lr=$lr outrec:".unpack('H80',$outrec)."using ". ref($INPUT). " caller ". join('::', (caller())[0,2]));
    if ($rc && $rc == $lr ) {
#        &$iar_rtn($INPUT, $currline + 1) if $iar_rtn && ref($iar_rtn) eq 'CODE';
    }
    else { $outrec = undef };
  }
  return $outrec;
}

sub getAfpILine {
  my $self = shift; my $INPUT = $self->{INPUT};

  my ($rc, $lr, $outrec) = ();

  $rc = $INPUT->read($lr,3);
  $rc = $INPUT->read(
    $outrec, unpack("xn", $lr)-2
  );

  return $lr.$outrec;
}

sub getASPsfILine {
  my $self = shift; my $INPUT = $self->{INPUT};

  my ($rc, $lr, $outrec) = ();

  $rc = $INPUT->read($lr,8);
  $lr = unpack("x2n", $lr);
  $rc = $INPUT->read($outrec, $lr);

  return $outrec;
}

sub getAFPStreamILine {
  my $self = shift; my $INPUT = $self->{INPUT};

  my ($rc, $c5a, $outrec) = ();

  $rc = $INPUT->read($c5a,3);
###mpezzi  return undef if $c5a eq '';
### consider also read errors
  return undef unless ($c5a and (length($c5a) == 3));

###mpezzi  die("Input Stream Error: INVALID AFP Record Header ".unpack("H*", $c5a))
#   if
#  substr($c5a,0,1) ne "\x5a";
#
#  $rc = $INPUT->read($outrec, unpack("xn", $c5a) - 2);
###
  my ($cc, $lr) = unpack("an", $c5a);
  # print ">>", unpack("H*", $c5a), "<<\n";
  die("Input Stream Error: INVALID AFP Record Header ".unpack("H*", $c5a)) if $cc ne "\x5a";
  $lr -= 2;
  $rc = $INPUT->read($outrec, $lr);

  return $c5a . $outrec;
}


sub getMODCAStreamILine {
  my $self = shift; my $INPUT = $self->{INPUT};

  my ($rc, $c5a, $outrec) = ();

  $rc = $INPUT->read($c5a,2); return undef if $c5a eq '';

  die("Input Stream Error: INVALID MODCA Record Header ".unpack("H*", $c5a))
   if
  length($c5a) != 2; $c5a = "\x5a" . $c5a;

  $rc = $INPUT->read($outrec, unpack("xn", $c5a) - 2);

  return $c5a . $outrec;
}


my $scsdelim = quotemeta("\x34\x15\x0D\x0C\x2B");
my $lx_scs = qr/([$scsdelim]|[^$scsdelim]+)/o;

sub getSCSILine {
  my $self = shift; my ($INPUT, $byteCache, $vpos, $skipLines) = @{$self}{qw(INPUT byteCache vpos skipLines)}; $vpos += 1;
  (DEBUG or $main::veryverbose) and i::warnit("getSCSILine called by " . join('::', (caller(1))[0,2]) . " - skipLines: $skipLines vpos: $vpos");
  if ($skipLines > 0) {
    @{$self}{qw(vpos skipLines)} = ($vpos, $skipLines - 1); return "\x40";
  }

  if ($INPUT->eof() and length($byteCache) == 0) {
    return undef;
  }

  my ($c, $outrec) = ("", "\x40");
  if ($self->{outrec} ne "") {
    $outrec = $self->{outrec}; $self->{outrec} = ""; substr($outrec, 0, 1) = "\x4e";
  }

  my ($at, $at_max) = (0, length($byteCache)); my $hpos = 0;

  while( 1 ) {
#    $main::buffct += 1; my $fpos = $INPUT->tell();
    if ( ($at_max-$at) < 1024 && !$INPUT->eof() ) {
      $byteCache = substr($byteCache, $at) if $at > 0;
      $INPUT->read($_, 2048);
      $byteCache .= $_;
      $at=0; $at_max = length($byteCache);
    }
    last if $at >= $at_max;

    ($c) = substr($byteCache, $at, 1024) =~ /^($lx_scs)/o; $at += length($c);

    $c =~ s/^\x00+//, $outrec .= $c, next if $c !~ /[$scsdelim]/o;
    my $currl = length($outrec);

    if ( $c eq "\x34" ) {
      my ($ppc, $ppv) = unpack("CC", substr($byteCache,$at,2)); $at += 2;
      if ($ppc == 0xC0 ) {     # Absolute Hor move
#   my $currl = length($outrec);
    (DEBUG or $main::veryverbose) and i::warnit("HMOV - reads: $main::reads fpos: $main::fpos at: $at ppv: $ppv currl: $currl vpos: $vpos");
        if ($currl < $ppv) {
           $outrec .= "\x40" x ($ppv - $currl);
        }
        elsif ($currl > $ppv) {
          $self->{outrec} = "\x40" x $ppv; $vpos -= 1; last;
        }
      }
      elsif ($ppc == 0xC4 ) { # Absolute Vert Move
#   my $currl = length($outrec);
        (DEBUG or $main::veryverbose) and i::warnit("VMOV - reads: $main::reads fpos: $main::fpos at: $at ppv: $ppv currl: $currl vpos: $vpos");
        next if $ppv == 1 && $vpos == 0;
        if ($ppv == $vpos) {
          next;
        }
        elsif ($currl > 1) {
          $at -= 3; last;
        }
        elsif ($ppv < $vpos) {
          $main::debug and i::warnit("INVALID Absolute Vert Move from=$vpos to=$ppv\n") if $ppv > 1;
          if ($at < $at_max or !$INPUT->eof()) {
             $outrec = "\xf1"; $vpos = 1;
             next if $ppv == 1;
             @{$self}{qw(vpos skipLines)} = ($vpos, (($ppv - $vpos) - 1) - 1);
             return "\xf1";
          }
          else {
             $outrec = undef; last;
          }
        }
        elsif ($ppv > $vpos) {
          $vpos = 1 if $vpos == 0;
          $self->{skipLines} = ($ppv - $vpos) - 1;
          last;
        }
      }
      elsif ($ppc == 0xC8 ) {     # Relative Hor move
         $outrec .= "\x40" x $ppv;
      }
      else {
        die "control code ??? ", unpack("H*", substr($byteCache, $at-3, 8)), " <===========\n";
      }
    }
    elsif ( $c eq "\x0D" ) {
      (my $ncc) = substr($byteCache, $at, 2) =~ /(\x00?[\x0C\x0D\x15])/;
#      $main::debug and i::warnit("CR - reads: $main::reads fpos: $main::fpos at: $at vpos: $vpos ncc: " . unpack("H*", $ncc));

      if ($ncc =~ /\x00?[\x0C\x15]/) {
         $at += length($ncc) if $ncc =~ /\x00?\x15/;
         last;
      }
      if ($currl > 1) {
        $self->{outrec} = "\x40"; $vpos -= 1; last;
      }
      $outrec = substr($outrec, 0, 1) unless substr($byteCache, $at, 2) =~ /^\x34\xc4$/;
    }
    elsif ( $c eq "\x15" ) {
      (my $ncc) = substr($byteCache, $at, 2) =~ /(\x00?\x0D)/;
#      $main::debug and i::warnit("NL - reads: $main::reads fpos: $main::fpos at: $at vpos: $vpos ncc: " . unpack("H*", $ncc));

      if ($ncc =~ /\x00?\x0D/) {
        $at += length($ncc);
      }
      last;
    }
    elsif ( $c eq "\x0C") {
#      $main::debug and i::warnit("FF - reads: $main::reads fpos: $main::fpos at: $at vpos: $vpos");
      if ( $currl > 1 ) {
         $at--; last;
      }
      elsif ($at < $at_max or !$INPUT->eof()) {
         $outrec = "\xf1"; $vpos = 1; next;
      }
      else {
        $outrec = undef; last;
      }
    }
    elsif ( $c eq "\x2B") {
      my ($cx, $ll) = unpack("aC", substr($byteCache,$at,2)); $at += 2;

      if ($cx eq "\xc1") {  #2BC1 �Set Horizontal Format (SHF)� on page 223
      }

      elsif ($cx eq "\xc2") {  #2BC2 �Set Vertical Format (SVF)� on page 227
      }

      elsif ($cx eq "\xc6") {  #2BC6 �Set Line Density (SLD)� on page 225
      }

      elsif ($cx eq "\xd1") {  #2BD10683 STO (Set Text Orientation) is not supported
      }

      elsif ($cx eq "\xd2") {  #2BD2NN29 �Set Print Density (SPD)� on page 225
      }

      elsif ($cx eq "\xd2") {  #2BD2NN48 �Page Presentation Media (PPM)� on page 219
      }

      else {
        die "2b $cx $ll ", unpack("H*", substr($byteCache, $at, $ll-1)), "\n";
      }

      $at+= $ll-1;
    }
  }

  $self->{byteCache} = substr($byteCache, $at); $self->{vpos} = $vpos; $outrec =~ tr/\x00/\x40/; return $outrec;
}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};
  $main::debug and i::warnit(ref($self)." Opening INPUT ".ref($INPUT));

  $INPUT->Open();

  @{$self}{qw(byteCache vpos skipLines)} = ("", -1, 0);
}

sub iarLine {
    my $self = shift;
#    $main::debug and i::warnit(ref($self)."::iarLine y uses ".ref($self->{INPUT}). " called by ".join('::', (caller())[0,2]));
    my $rtn = $self->{INPUT}->can('iarLine');
    return 0 unless $rtn && ref($rtn) eq 'CODE';
    return &$rtn($self->{INPUT}, @_);
}



sub getLine {
  return &{$_[0]->{getLine}}($_[0]);
}

sub getLines {
  my ($self, $lineOffsets, $lineCache, $maxLines) = @_; my ($offset, $line);
#  $main::debug and i::warnit(ref($self)."::getLines Line using ". ref($self->{INPUT}). " caller ". join('::', (caller())[0,2]));

  while($maxLines>0) {
    $offset = $self->tell(); $line = &{$self->{getLine}}($self);
    if ( $line eq "" ) {
      last;
    }
    push @$lineOffsets, $offset; push @$lineCache, $line; $maxLines -= 1;
  }
}

sub read {
  my $self = shift; my $INPUT = $self->{INPUT}; $INPUT->read(@_);
}

sub seek {
  my $self = shift; my $INPUT = $self->{INPUT}; $INPUT->seek(@_);
}

sub tell {
  return $_[0]->{INPUT}->tell() - length($_[0]->{byteCache});
}

sub eof {
  if ( !$_[0]->{INPUT}->eof() ) {
    return 0;
  }
  return( length($_[0]->{byteCache}) == 0 );
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};
  if ($_[0] eq 'finalize' && $_[1] ) {
#   warn ref($self)." ILine Close final wrapper invoked INPUT: ".ref($INPUT)."\n";
  }

  $self->{byteCache} = "";
  @{$self}{qw(byteCache vpos skipLines)} = ("", -1, 0);

#  return $self->SUPER::Close(@_);
  return $INPUT->Close();

}

sub new {
  my ($className, $INPUT, $XferMode) = @_; my $self = {}; my $getLine;

  $self->{byteCache} = "";

  if (1) {
      $getLine = \&getPsfILine;
  }
  elsif ( $XferMode == XF_LPR ) {
    $getLine = \&getAsciiILine;
  }
  elsif ( $XferMode == XF_PSF ) {
    $getLine = \&getPsfILine;
  }
  elsif ( $XferMode == XF_FTPB ) {
    $getLine = \&getModebILine;
  }
  elsif ( $XferMode == XF_FTPC ) {
    $getLine = \&getModecILine;
  }
  elsif ( $XferMode == XF_ASPSF ) {
#    $getLine = \&getSCSILine;
    $getLine = \&getASPsfILine;
  }
  elsif ( $XferMode == XF_AFPSTREAM ) {
    $getLine = \&getAFPStreamILine;
  }
  elsif ( $XferMode == XF_MODCASTREAM ) {
    $getLine = \&getMODCAStreamILine;
  }
  elsif ( $XferMode == XF_SCS ) {
    $getLine = \&getSCSILine;
  }
  elsif ( $XferMode == XF_AS400 ) {
    $getLine = \&getSCSILine;
  }
  elsif ( $XferMode eq XF_FB80 ) {
    $getLine = \&getFB80Line;
  }
  else {
    die("UserError: Invalid XferTransferMode: $XferMode\.");
  }

  @{$self}{qw(INPUT getLine XferMode vpos skipLines)} = ($INPUT, $getLine, $XferMode, -1, 0);

  bless $self, $className;
}

#------------------------------------------------------------
package XReport::INPUT::IPage; ### superclass ###
use constant DEBUG => 0;

our @ISA = ('XReport::INPUT');

use strict; use bytes;

sub log {
  my $page = shift; my $lineList = $page->lineList(); require XReport::Util;

  &{$XReport::Util::logrRtn}(
   "Begin log of Page ".$page->atPage().".", (map {$_->AsciiValue} @$lineList), "End log of Page."
  );
}

sub overflowed {
  my $self = shift;
  $main::veryverbose and i::warnit(__PACKAGE__." overflowed: $self->{overflowed} - caller: ".join('::', (caller())[0,2]). " class: ".ref($self) );
  return $self->{overflowed} if !scalar(@_);

  $self->{overflowed} = shift;
  return $self->{overflowed};
}

sub totLines {
  my $self = shift; my $lineList = $self->{lineList};
  return scalar(@$lineList);
}

sub atPage {
  my $self = shift;
  return $self->{atPage};
}

sub iarLine {
    my $self = shift;
#    $main::debug and i::warnit(ref($self)."::iarLine x uses ".ref($self->{INPUT}). " called by ".join('::', (caller())[0,2]));
    return 0 unless my $rtn = $self->{INPUT}->can('iarLine');
    return (ref($rtn) eq 'CODE' ? &$rtn($self->{INPUT}, @_) : 0);
}

sub atLine {
  my $self = shift;
#  $main::debug and i::warnit(ref($self)."::atLine INPUT: ".ref($self->{INPUT}->{INPUT}). " called by ".join('::', (caller())[0,2]));

  return $self->{atLine};
}

sub atOffset {
  my $self = shift;
  return $self->{atOffset};
}

sub rawlineList_compacted {
  my $self = shift;
  (DEBUG or $main::veryverbose) and i::warnit("executing rawcompacted\n");
   return [grep { $_->AsciiValue() !~ /^[\x40\s]\s*$/ } @{$self->{rawpage}}];
}

sub rawlineList {
  my $self = shift;
  (DEBUG or $main::veryverbose) and i::warnit("executing raw\n");
  return $self->{rawpage};
}

sub lineList {
  my $self = shift;
  (DEBUG or $main::veryverbose) and i::warnit("executing line\n");
  return $self->{lineList};
}

sub _GetPageRect {
  my $llstRtn = shift;
  my ($page, $fr, $fc, $tr, $tc) = @_; my @rectlines = (); my $j;

  my $lineList = &$llstRtn($page);
  #(DEBUG or $main::veryverbose) and i::warnit("_GetPageRect:\n", join("\n", map { $_->AsciiValue() } @{$lineList}[0]), "\n");
  #eval{i::logit("DEBUG - ".__PACKAGE__.__LINE__."::sub _GetPageRect():\n", join("\n", map { $_->AsciiValue() } @{$lineList}[0]), "\n");};

  my $lLen = scalar(@$lineList);
  
  
  #eval{my $mylen =($#$lineList <50?$#$lineList:50);my $count=0; i::logit("DEBUG - ".__PACKAGE__.__LINE__."::sub _GetPageRect():\n", join("\n", map {(++$count)."-".$_->AsciiValue()} @{$lineList}[1..$mylen]), "\n");};

  for ($fr, $fc, $tr, $tc) {
    $_-- if $_ > 0;
  }

  $fr = -$lLen if $fr < 0 && abs($fr) > $lLen;
  $tr = $#$lineList if $tr > $#$lineList;

  unless ( $fr > $tr ) {
     for ( $j=$fr;$j<=$tr;$j++ ) {
        last if $j>$#$lineList;
		#$main::page++;
		#i::logit("DEBUG - ".__PACKAGE__.__LINE__."::sub _GetPageRect() line page:$main::page extracted $j: \"".$lineList->[$j]->AsciiValue()."\" - ". unpack("H*", $lineList->[$j]));
        push @rectlines, substr($lineList->[$j]->AsciiValue(),$fc,($tc - $fc) + 1);
     }
  }
  my $t = ''.join("\n", @rectlines);
  (DEBUG or $main::veryverbose) && i::logit("Field extracted inp: \"$t\" - ". unpack("H*", $t));
  #eval {i::logit("DEBUG - ".__PACKAGE__.__LINE__."::sub _GetPageRect() Field extracted inp: \"$t\" - ". unpack("H*", $t));};
  return $t;
}

sub GetPageRect {
    return _GetPageRect($_[0]->can('lineList'), @_);
}

sub GetRawPageRect_compacted {
    return _GetPageRect($_[0]->can('rawlineList_compacted'), @_);
}

sub GetRawPageRect {
    return _GetPageRect($_[0]->can('rawlineList'), @_);
}

# verify how to use the same getrect rtn
sub GetPageRectList {
  my ($page, $fr, $fc, $tr, $tc) = @_; my @rectlines = (); my $j;

  my $lineList = $page->lineList();

  my $lLen = scalar(@$lineList);

  for ($fr, $fc, $tr, $tc) {
    $_-- if $_ > 0;
  }

  $fr = -$lLen if $fr < 0 && abs($fr) > $lLen;
  $tr = $#$lineList if $tr > $#$lineList;

  unless ( $fr > $tr ) {
     for ( $j=$fr;$j<=$tr;$j++ ) {
       last if $j>$#$lineList;
       push @rectlines, [ map {defined($_) ? substr($_,$fc,$tc)  : '' } @{$lineList->[$j]->AsciiValueList()} ];
     }
  }
  return \@rectlines;
}

sub GetAttributes {
# $main::debug and i::warnit("INPUT IPage called by " . join('::', caller(1)));
  my $page = shift; my $FieldsExtractor = $page->{'FieldsExtractor'}; $FieldsExtractor->getValues(@_);
}

sub GetPageFields {
  my $page = shift; my $lineList = $page->lineList(); my @fields;

  while(@_) {
    my ($l, $c, $ll, $field) = (shift, shift, shift, undef); $l--; $c--;
    if ($l = $lineList->[$l] and length($l=$l->AsciiValue()) > $c) {
      $field = substr($l, $c, $ll);
    }
    push @fields, $field;
  }

  return @fields;
}

sub GetLineRect {
  my ($page, $line, $fr, $fc, $tr, $tc) = @_; my $t = ""; my $j;

  my $atLine = $line->atLine();
  my $lineList = $page->lineList($page);

  $fr+=$atLine-2; $fc--; $tr+=$atLine-2; $tc--;

  for ( $j=$fr;$j<=$tr;$j++ ) {
    last if $j>$#$lineList;
    $t .= substr($lineList->[$j]->AsciiValue(),$fc,$tc)."\n";
  }

  return $t;
}

sub GetLineRectList {
}

sub setLineFilter {
  my ($self, $FilterClass) = (shift, shift); my $INPUT = $self->{INPUT};
  if ( $FilterClass ) {
    (DEBUG or $main::veryverbose) and i::warnit( "setLineFilter initializing $FilterClass" );
    $self->{lineFilter} = $self->{INPUT} = $FilterClass->new($INPUT, @_);
  }
  else {
    $self->{INPUT} = $INPUT->{INPUT}; delete $self->{lineFilter};
  }
}

sub tell {
  return $_[0]->{lineOffset};
}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};
  return $INPUT->Open(@_);
}

sub getLine {
  return $_[0]->{INPUT}->getLine();
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};
  if ($_[0] eq 'finalize' && $_[1] ) {
#   warn ref($self)." IPage Close final wrapper invoked INPUT: ".ref($INPUT)."\n";
  }
  return $self->SUPER::Close(@_);
  # return $INPUT->Close(@_);
}

sub SetElabOptions {
}

sub new {
  my ($className, $self, $INPUT) = @_;
  $main::debug and i::warnit(__PACKAGE__." initializing class ". " - caller: ".join('::', (caller())[0,2]));

  $self->{INPUT} = $INPUT;

  bless $self, $className;
}

#------------------------------------------------------------
package XReport::INPUT::AsciiIPage;
use constant DEBUG => 0;

our @ISA = ('XReport::INPUT::IPage');

use strict; use bytes;

sub NewPage {
  my $self = shift; $self->{atPage} += 1;

  my $page = {
    INPUT  => $self,
    overflowed => 0,
    atPage => $self->{atPage},
    atLine => $self->{atLine},
    lineList => [], rawpage => [],
    atOffset => $self->tell()
  };

  bless $page, 'XReport::INPUT::AsciiIPage';
}

sub appendLine {
  my $self = shift; my ($INPUT, $lineList) = @{$self}{qw(INPUT lineList)};
  (DEBUG or $main::veryverbose) and i::warnit("appenLine ASCII-Ipage\n".join(' ', @_));
  push @$lineList, XReport::INPUT::AsciiLine->new($self, scalar(@$lineList)+1, @_);
  $INPUT->{atLine} += 1;
}

sub appendRawLine {
  my $self = shift;
  push @{$self->{rawpage}}, XReport::INPUT::AsciiLine->new($self, scalar(@{$self->{rawpage}})+1, @_);
}

sub overwriteLastLine {
  my $self = shift; my $page = $self->{lineList};
  $page->[$#$page]->overwriteLine(@_);
}

sub getLine {
  my $self = shift;

  my ($lineOffsets, $lineCache)
   =
  @{$self}{qw(lineOffsets lineCache)};

  if ( !@$lineCache ) {
    my $INPUT = $self->{INPUT};
    $INPUT->getLines($lineOffsets, $lineCache , 77);
  }
  my $currpos = $self->{INPUT}->iarLine();
  $self->{INPUT}->iarLine($currpos + 1);	
  return if !scalar(@$lineCache);

  if ( wantarray ) {
    return (shift @$lineOffsets, shift @$lineCache);
  }
  else {
    shift @$lineOffsets; return shift @$lineCache;
  }
}

sub eof {
  return (!@{$_[0]->{lineCache}} and $_[0]->{INPUT}->eof() and $_[0]->{lastLine} eq "");
}

sub GetPage {
  my $self = shift; return undef if $self->eof(); my ($INPUT, $lpp) = @{$self}{qw(INPUT lpp)};

  my ($lineOffset, $cc, $line) = @{$self}{qw(lineOffset lastcc lastLine)};

  my $page = $self->NewPage();

  if ( $line ne "" ) {
    $page->appendLine($line);
    ($cc, $line) = ();
  }

  my $totLines = $page->totLines();

  while(1) {
    ($lineOffset, $line) = $self->getLine();
    $cc = substr($line,0,1);
    last if $line eq "";
    $page->appendRawLine($line);
    if ( ($cc eq "1" and $totLines > 0) or ($lpp and $totLines >= $lpp) ) { 
      last;
    }
    $page->appendLine($line); $totLines += 1; ($cc, $line) = ();
  }
  @{$self}{qw(lineOffset lastcc lastLine)} = ($lineOffset, $cc, $line);
  
  return $page;
}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};

  @{$self}{qw(lineOffset lastcc lastLine)} = (0, '', '');

  @{$self}{qw(lineOffsets lineCache)} = ([], []);

  @{$self}{qw(atPage atLine)} = (0, 0);

  $INPUT->Open(@_);
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};

  @{$self}{qw(lineOffset lastcc lastLine)} = (0, '', '');

  @{$self}{qw(lineOffsets lineCache)} = ([], []);

  @{$self}{qw(atPage atLine)} = (0, 0);

  return $self->SUPER::Close(@_);
  return  $INPUT->Close(@_);
}

sub new {
  my ($className, $self) = (shift, shift);

  bless $self, $className;

  $self->setJobReportValues('cc','asa');

  $self->Open(@_);

  return $self;
}

#------------------------------------------------------------
package XReport::INPUT::EbcdicIPage;
use constant DEBUG => 0;

our @ISA = ('XReport::INPUT::IPage');

use strict; use bytes;

sub check_nop {
  my ($self, $line) = @_;
  my $nopline = $translator->toascii(substr($line,9));
  (DEBUG or $main::veryverbose) && i::warnit(ref($self)
                           ." check_nop called by ".join('::', (caller())[0,2])
                           ." inline: ".unpack('H40', $line)
                           ." nopline: $nopline");
  return if $nopline !~ /^XRTLE\t/;
  $nopline =~ s/\s+$//; my %TLE = split("\t", substr($nopline,6));
  @{$TLE}{keys(%TLE)} = map { $_ =~ s/\s+$//; $_ } values(%TLE);
}

sub NewPage {
  my $self = shift; $self->{atPage} += 1;
  $main::debug and i::warnit(__PACKAGE__." NewPage - caller: ".join('::', (caller())[0,2]));

  my $page = {
    INPUT  => $self,
    overflowed => 0,
    atPage => $self->{atPage},
    atLine => $self->{atLine},
    lineList => [], rawpage => [],
    atOffset => $self->tell()
  };

  bless $page, 'XReport::INPUT::EbcdicIPage';
}

sub GetAttributes {
  my ($self, $elName) = (shift, shift); my @r = ();

### mpezzi nop handling
##  if ($elName eq 'NOP' and defined($NOP) and join('', @$NOP) ne '') {
##    my $re = shift;
##    print "NOP INPUT:", ($NOP ? join(":::", @$NOP) : "__undefined__"), "\n";
##    return wantarray ? $NOP : join("\x20", @$NOP) unless $re;
##    @r =  join("\x20", @$NOP) =~ /$re/mso;
##  print "GetAttributes NOP ==> ", join(':::', @r), "\n";
##  return wantarray ? @r : $r[0];
##  }
###

  return unless $elName eq "TLE" and defined($TLE) and keys(%$TLE);

  for (@_) {
    return () if !exists($TLE->{$_});
    push @r, $TLE->{$_};
  }

  return wantarray ? @r : $r[0];
}

sub appendLine {
  my $self = shift; my ($INPUT, $lineList) = @{$self}{qw(INPUT lineList)};
  (DEBUG or $main::veryverbose) and i::warnit("appendLine EbcdicIpage\n".join(' ', @_));
  push @$lineList, XReport::INPUT::EbcdicLine->new($self, scalar(@$lineList)+1, @_); $INPUT->{atLine} += 1;
}

sub appendRawLine {
  (DEBUG or $main::veryverbose) && i::warnit("DEBUG - XReport::INPUT::EbcdicIPage sub appendRawLine  ");
  my $self = shift;
  (DEBUG or $main::veryverbose) and i::warnit("appendRawLine EbcdicIpage called by ".join('::',(caller())[0,2]));
  push @{$self->{rawpage}}, XReport::INPUT::EbcdicLine->new($self, scalar(@{$self->{rawpage}})+1, @_);
}

sub overwriteLastLine {
  my $self = shift; my $lineList = $self->{lineList};
  $lineList->[$#$lineList]->overwriteLine(@_);
}

sub getLine {
  my $self = shift;

  my ($lineOffsets, $lineCache)
   =
  @{$self}{qw(lineOffsets lineCache)};

  if ( !@$lineCache ) {
    my $INPUT = $self->{INPUT};
    $INPUT->getLines($lineOffsets, $lineCache , 88);
  }

  my $currpos = $self->{INPUT}->iarLine();
  $self->{INPUT}->iarLine($currpos + 1);
  (DEBUG or $main::veryverbose) and i::warnit("Returning no line bacause empty $lineCache ") if !scalar(@$lineCache);
  return if !scalar(@$lineCache);

  if ( wantarray ) {
    return (shift @$lineOffsets, shift @$lineCache);
  }
  else {
    shift @$lineOffsets; return shift @$lineCache;
  }
}

sub getAsciiLine {
  my $self = shift; my ($cc, $line) = $self->getLine();
  return $translator->toascii($line);
}

sub GetPage { 
  my $self = shift;  return undef if $self->eof();
  my ($INPUT, $lineFilter) = @{$self}{qw(INPUT lineFilter)};
  my ($lineOffset, $lastcc, $lastLine, $lastccExec, $HasCc, $fcb)
                              = @{$self}{qw(lineOffset lastcc lastLine lastccExec HasCc fcb)};
  $main::debug and i::warnit(__PACKAGE__." GetPage  hascc: $HasCc - pagecount: ".(++$main::pagecount)." - caller: ".join('::', (caller())[0,2]));

  my ($started, $cc, $line, $preExec, $printIt, $postExec, $curPos) = ();

  my $page = $self->NewPage();
  $page->appendRawLine($lastLine) if $lastLine;

  $started = 1 if $self->{lastcc} and $self->{lastcc} ne "\x5a";

  READLINES:
  while(1) {
    if ( $lastccExec eq "" ) {
      (DEBUG or $main::veryverbose) and i::warnit(__PACKAGE__." READLINES no linexec - caller: ".join('::', (caller())[0,2]));
      ($lineOffset, $line) = $self->getLine();
      $cc = substr($line,0,1);
      last if ( $line eq "" );
	  #change for CCKE1CC
	  if($main::DISABLE_CC_IN_LIST and (($cc eq "\x5a" ) or ($cc eq "\x40" && $line =~ /^\x40\x7c\xe0\x7b/))) {
		next READLINES;
	  }
      $page->appendRawLine($line);
      if ( $cc eq "\x5a" ) {
        if ($line =~ /^$lx_idm/o) {
            (my $dm = $translator->toascii(substr($line,9,8))) =~ s/\W//g;
            my $c1 = $self->{'c1'} = $self->{'dm_c1'}->{$dm};
            (DEBUG or $main::veryverbose) && i::warnit(ref($self)." GetPage Datamap: $dm FF: $c1 HEX: ". unpack("H*", $c1));
        }
        elsif ($line =~ /^$lx_nop/o) {
            $self->check_nop($line);
        }
        else {
            i::logit(ref($self)." GetPage Unandled 5A rec detected: ". unpack("H40", $line));
        }
        next READLINES;
      }
#      print "=b4 SCS 34======> ", unpack("H8", $line ), " ", length($line), " $preExec $printIt $postExec<===========\n"  if $main::debug;
      if ($cc =~ /^\x34/) {
         if (substr($line, 1, 1) ne "\xC4") {
            $self->LogIt( "INVALID SCS control character ".unpack("H*",$cc)." FOUND") if ( (++$self->{ccErrors}) < 10 );
            $cc = "\xff"; # set cc to a default value (0x40 or 0x09)
            #   $line = "\x40".substr($line, 1);
         }
         else {
            $cc .= substr($line, 1, 2);
            my ($ppv) = unpack("C", substr($line, 2, 1));
      #   if ($ppv == 1) {
      #     ($preExec, $printIt, $postExec) = @{$ExecTable{"\x8b"}};
      #   }
      #   else {
            my $currLines = $page->totLines();
            last READLINES if $ppv == 1 and $ppv <= $currLines;
            my $lines2skip = ($currLines > $ppv ? scalar(@$fcb) : 0 ) + $ppv - $currLines;
            $line = "\x40".substr($line,3);$cc = "\x40";
            ($preExec, $printIt, $postExec) = ($lines2skip, 1, 0);
      #   $curPos = $page->totLines() ;
      #   for ($curPos = $page->totLines()+1; $curPos < $ppv; $curPos++ ) {
      #     last READLINES if ( $page->totLines() == scalar(@$fcb) );
      #     $page->appendLine("\x40");
      #   }
      #   next READLINES;
      #   }
         }
      }
      else {

		if (!$HasCc)
		{
           $line= "\x40".$line; $cc = "\x40";
		}

#?????????????????????????????????????????????????
#        if (!$HasCc and $cc eq "\x0c") {
#           $line= "\x8b".substr($line,1);
#           $cc = "\x8b";
#        }
#        elsif (!$HasCc and $cc ne "\xf1") {
#           $line= "\x40".$line; $cc = "\x40";
#        }
#?????????????????????????????????????????????????

        #$cc = "\x40" if (($HasCc) and ($cc =~ /^\xf2/));#
		if($HasCc and $main::DISABLE_CC_IN_LIST)
		{
			$cc = "\x40" if grep {($cc =~ /^\x$_/)} split (',', $main::DISABLE_CC_IN_LIST);
		}

        if ( !exists($ExecTable{$cc}) ) {
           use Carp;
#           croak "USER ERROR: Invalid Control Character ".unpack("H2",$cc)." FOUND"
#                                   if 'CDAMPARSER' == $self->{jr}->getValues('XferDaemon');
           if ( (++$self->{ccErrors}) < 10 || ($self->{ccErrors} < 100 && ($self->{ccErrors}%10) == 0) || (($self->{ccErrors}%100) == 0)) {
              $self->LogIt( "INVALID control character ".unpack("H2",$cc)." FOUND ($self->{ccErrors})")
           }
           $cc = "\xff"; # set cc to a default value (0x40 or 0x09)
           $line = "\x40".$line;
        }

        ($preExec, $printIt, $postExec) = @{$ExecTable{$cc}};
      }
    }
    else {
     (DEBUG or $main::veryverbose) and i::warnit(__PACKAGE__." GetPage b4pre line: ".unpack("H30", $lastLine)
                               . "- caller: ".join('::', (caller())[0,2]));
     ($preExec, $printIt, $postExec) = @$lastccExec;
      $lastccExec = "";
      $line = $lastLine; $cc = substr($line,0,1);
      $page->overflowed(1) if $HasCc && $cc ne "\xf1";
    }

    #do preProcessing
    if ( $preExec > 0 ) {  # space lines
      while( $preExec>0 ) {
        if ( $page->totLines == scalar(@$fcb) ) {
          last READLINES;
        }
        $page->appendLine("\x40");
        $preExec -= 1;
      }
    }
    elsif ( $preExec < 0 ) {  # skip lines
      if ( $preExec == -1 and $page->totLines() >= 1 ) {
        $preExec = 0; last READLINES;
      }
      $page->appendLine("\x40") if !$page->totLines();
      $curPos = $page->totLines()-1;
      while( $fcb->[$curPos] != -$preExec ) {
        if ( $page->totLines == scalar(@$fcb) ) {
          last READLINES;
        }
        $page->appendLine("\x40");
        $curPos += 1;
      }
      $preExec = 0;
    }
    #test for filled page

    #push line
    if ( $printIt ) {
      if ( $page->totLines() == 0 ) {
        #required (for example X89)
        $page->appendLine("\x40")
      }
      $page->overwriteLastLine($line); #overwrite
      $printIt = 0;
      ($cc, $line) = ();
    }

    #do postProcessing
    if ( $postExec > 0 ) {  # space lines
      while( $postExec>0 ) {
        if ( $page->totLines() >= scalar(@$fcb) ) {
          last READLINES;
        }
        $page->appendLine("\x40");
        $postExec -= 1;
      }
    }
    elsif ( $postExec < 0 ) {  # skip lines
      if ( $postExec == -1 and $page->totLines() > 1 ) {
        $postExec = 0; last READLINES;
      }
      $page->appendLine("\x40") if !$page->totLines();
      $curPos = $page->totLines()-1;
      while( $fcb->[$curPos] != -$postExec ) {
        if ( $page->totLines == scalar(@$fcb) ) {
          last READLINES;
        }
        $page->appendLine("\x40");
        $curPos += 1;
      }
      $postExec = 0;
    }
  }
  @{$self}{qw(lineOffset lastcc lastLine)} = ($lineOffset, $cc, $line);

  if (abs($preExec)+abs($printIt)+abs($postExec)) {
    $self->{lastccExec}  = [$preExec, $printIt, $postExec];
  }
  else {
    $self->{lastccExec} = "";
  }

  $lineFilter->setPageAttributes($page) if $lineFilter;
  return $page;
}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};
      $main::debug and i::warnit(ref($self)." Opening INPUT ".ref($INPUT));

  @{$self}{qw(lineOffset lastcc lastLine lastccExec ccErrors)} = (0, '', '', '', 0);

  my ($lpp, $fcb) = ($self->{lpp}, [@fcb2st60]);

  if ( $lpp > scalar(@$fcb) ) {
    for (1..($lpp-scalar(@$fcb))) {
      push @$fcb, 0
    }
  }
  elsif ($lpp < scalar(@$fcb)) {
    @$fcb = @{$fcb}[0..($lpp-1)]
  }

  @{$self}{qw(lineOffsets lineCache fcb atPage atLine)} = ([], [], $fcb, 0, 0);

  $INPUT->Open(@_);
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};
  i::logit("EbcdicIpage Closing CC Errors: ".($self->{ccErrors} || 0));
  if ($_[0] eq 'finalize' && $_[1] ) {
#   warn ref($self)." EbcdicIPage Close final wrapper invoked\n";
  }

  @{$self}{qw(lineOffset lastcc lastLine lastccExec ccErrors)} = (0, '', '', '', 0);

  @{$self}{qw(lineOffsets lineCache fcb atPage atLine)} = ([], [], [], 0);

  return $self->SUPER::Close(@_);
  return $INPUT->Close(@_);
}

sub eof {
  return (!@{$_[0]->{lineCache}} and $_[0]->{lastccExec} eq "" and $_[0]->{INPUT}->eof());
}

sub new {
  my ($className, $self, $scs) = (shift, shift, shift); my $INPUT = $self->{INPUT};
  $main::debug and i::warnit("$className new called - INPUT: ".ref($INPUT)." scs: ".( $scs || 'undef'). "caller ".join('::', (caller())[0,2]));
  bless $self, $className;
  
  $main::DISABLE_CC_IN_LIST = getConfValues("DISABLE_CC_IN_LIST");

  $self->Open(); my $lineOffset;

  if ($self->{HasCc} && !$scs) {
    while( !$INPUT->eof() ) {
     ($lineOffset, $self->{lastLine}) = $INPUT->getLine();
      $self->{lastcc} = substr($self->{lastLine},0,1);

      my $cc = $self->{lastcc};
      next if $cc eq "\x5a";

      if ( exists($asaExecTable{$cc}) ) {
        $self->{ExecTable} = \%asaExecTable;
        $self->setJobReportValues('cc','asa');
      }
      else {
        $self->{ExecTable} = \%machineExecTable;
        $self->setJobReportValues('cc','machine');
      }
      last;
    }
    $self->Close(); $self->Open(@_);
  }
  else {
    $self->setJobReportValues('cc','asa');
  }

  return $self;
}

#------------------------------------------------------------
package XReport::INPUT::AfpIPage;
use constant DEBUG => 0;

our @ISA = ('XReport::INPUT::EbcdicIPage');

use strict; use bytes;
use Carp;

use Symbol;
use XReport::Util;

our  $lx_idm = qr/^\x5a..\xd3\xab\xca/;
our  $lx_nop = qr/^\x5a..\xd3\xee\xee/;

our $TLE = undef;
our $NOP = undef; ### mpezzi nop handling

sub NewPage {
  my $self = shift; $self->{atPage} += 1;

  my $page = {
    INPUT  => $self,
    atPage => $self->{atPage},
    atLine => $self->{atLine},
    lineList => [], rawpage => [],
    atOffset => $self->tell()
  };

  bless $page, 'XReport::INPUT::AfpIPage';
}

sub appendLine {
  my $self = shift; my ($INPUT, $lineList) = @{$self}{qw(INPUT lineList)};
  (DEBUG or $main::veryverbose) and i::warnit("appendLine AfpIpage\n".join(' ', @_));
  push @$lineList, XReport::INPUT::EbcdicLine->new($self, scalar(@$lineList)+1, @_);
  $INPUT->{atLine} += 1;
}

sub overwriteLastLine {
  my $self = shift; my $lineList = $self->{lineList};
  $lineList->[$#$lineList]->overwriteLine(@_);
}

sub getLine {
  my $self = shift;

  my ($lineOffsets, $lineCache)
   =
  @{$self}{qw(lineOffsets lineCache)};
  
  if ( !@$lineCache ) {
    my $INPUT = $self->{INPUT};
    $INPUT->getLines($lineOffsets, $lineCache , $INPUT->{lpp} || 88);
  }

  return if !scalar(@$lineCache);

  if ( wantarray ) {
    return (shift @$lineOffsets, shift @$lineCache);
  }
  else {
    shift @$lineOffsets; return shift @$lineCache;
  }
}

sub eof {
  return (!@{$_[0]->{lineCache}} and $_[0]->{lastcc} =~ /^\x8b?$/ and $_[0]->{INPUT}->eof());
}

sub GetAttributes {
  my ($self, $elName) = (shift, shift); my @r = ();

  return unless $elName eq "TLE" and defined($TLE) and keys(%$TLE);

  for (@_) {
    return () if !exists($TLE->{$_});
    push @r, $TLE->{$_};
  }

  return wantarray ? @r : $r[0];
}

sub check_nop {
  my ($self, $line) = @_; $line = $translator->toascii(substr($line,9));
  return if $line !~ /^XRTLE\t/;
  $line =~ s/\s+$//; my %TLE = split("\t", substr($line,6));
  @{$TLE}{keys(%TLE)} = map {
    $_ =~ s/\s+$//; $_
  }
  values(%TLE);
}

sub cleanLineList {
  my ($self, $page) = @_; my $lineList = $page->lineList(); my $ll = scalar(@$lineList)-1;

  my @l5a = (0,0,1); my $toclean = 0;

  for (0..$ll) {
    my $line = $lineList->[$ll-$_]->Value(); my $cc = substr($line,0,1); my $todel = 0;
    if ($cc eq "\x5a") {
      my $sf = substr($line,3,3);
      my $ix = $sf eq "\xd3\xab\xcc" ? 0 : $sf eq "\xd3\xab\xca" ? 1 : 2 ;
      $todel = $l5a[$ix] == 1 ? 1 : 0; $l5a[$ix] = 1;
    }
    else {
      $todel = 1;
    }
    do {delete $lineList->[$ll-$_]; $toclean += 1} if $todel;
  }

  @$lineList = grep {defined($_)} @$lineList if $toclean;
}

sub GetPage_CLEAN {
  my $self = shift;  return undef if $self->eof(); my $INPUT = $self->{INPUT}; $TLE = undef;

  my (
   $lineOffset,
   $cc, $line, $c1, $Formatted
  ) =
  @{$self}{qw(lineOffset lastcc lastLine c1 Formatted)};

  my $page = $self->NewPage(); my $started = 0;

  if ( $cc ne "" and $cc ne "\x8b" ) {
    if ($line =~ /$lx_idm/o) {
      my $dm = $translator->toascii(substr($line,9,8));
      $c1 = $self->{'c1'} = $self->{'dm_c1'}->{$dm} || "\xf1";
    }
    elsif ($line =~ /$lx_nop/o) {
      $self->check_nop($line);
    }
    $page->appendLine($line);
  }

  $started = 1 if $cc ne '' and $cc ne "\x5a" and ($cc ne $c1 or length($line) > 2);

  while(1) {
    ($lineOffset, $line) = $self->getLine(); last if $line eq "";

    $cc = substr($line,0,1); substr($line,2) =~ s/\x40+$//;

    last if ( $started and ($cc eq $c1 and $started >= 1 or $cc eq "\x8b") );

    if ( $started and $cc eq "\x5a" ) {
      my $sf = substr($line,3,3);
      last if $sf eq "\xd3\xab\xcc" or $sf eq "\xd3\xab\xca";
    }

    if ($cc ne "\x5a" and ($cc ne $c1 or length($line) > 2)) {
      $self->cleanLineList($page) if !$started;
      $started += 1;
    }
    elsif ($line =~ /^$lx_idm/o) {
      my $dm = $translator->toascii(substr($line,9,8));
      $c1 = $self->{'c1'} = $self->{'dm_c1'}->{$dm} || "\xf1";
    }
    elsif ($line =~ /$lx_nop/o) {
      $self->check_nop($line);
    }

    $page->appendLine($line); ($cc, $line) = ();
  }

  @{$self}{qw(lineOffset lastcc lastLine)} = ($lineOffset, $cc, $line);

  return $page;
}


sub GetPage {
  my $self = shift;  return undef if $self->eof(); my $INPUT = $self->{INPUT}; $TLE = undef;

  my ($lineOffset, $cc, $line, $c1, $Formatted ) =
  @{$self}{qw(lineOffset lastcc lastLine c1 Formatted)};

  my $page = $self->NewPage(); my $started = 0;

  if ( $cc and $cc ne "\x8b" ) {
    if ($line =~ /$lx_idm/o) {
      my $dm = $translator->toascii(substr($line,9,8));
      $c1 = $self->{'c1'} = $self->{'dm_c1'}->{$dm};
    }
    elsif ($line =~ /$lx_nop/o) {
      $self->check_nop($line);
    }
    $page->appendLine($line);
  }

  $started = 1 if $cc and $cc ne "\x5a";

  while(1) {
    ($lineOffset, $line) = $self->getLine(); $cc = substr($line,0,1); last unless $line;

    #do { $line = ($cc="\xf1").$line; last; } if !$Formatted;
    #todo build skip page frompagedef

    #    last if ( $started and ($cc eq $c1 and $started>=1 or $cc eq "\x8b") );
    last if ( $started and ($cc eq $c1 or $cc eq "\x8b") );
    last if ( $started and $cc eq "\x5a" and $line =~ /^\x5a..\xd3\xab[\xca\xcc]/ );

    #    if ( $started and $cc eq "\x5a" ) {
    #      my $sf = substr($line,3,3);
    #      last if $sf eq "\xd3\xab\xcc" or $sf eq "\xd3\xab\xca";
    #    }
    #warn "===========> checking line ===>", unpack("H*", $line), " for idm: $lx_idm == nop: $lx_nop\n" if $main::debug;
    if ($cc ne "\x5a") {
      $started += 1
    }
    elsif ($line =~ /^$lx_idm/o) {
      my $dm = $translator->toascii(substr($line,9,8));
      $c1 = $self->{'c1'} = $self->{'dm_c1'}->{$dm};
    }
    elsif ($line =~ /$lx_nop/o) {
      $self->check_nop($line);
    }

    $page->appendLine($line); ($cc, $line) = ();
  }

  @{$self}{qw(lineOffset lastcc lastLine)} = ($lineOffset, $cc, $line);

  return $page;
}


sub Open {
  my $self= shift; $self->SUPER::Open(@_);
  $self->{'c1'} = $self->{'dm_c1'}->{$self->{'dm_1'}} || "\xf1";
}

sub Close {
  my $self= shift; $self->SUPER::Close(@_);
  $self->{'c1'} = $self->{'dm_c1'}->{$self->{'dm_1'}} || "\xf1";
}

sub new {
  (DEBUG or $main::veryverbose) && i::logit("DEBUG - ".__PACKAGE__.__LINE__."::sub new() ");
  my ($className, $self) = (shift, shift); my $INPUT = $self->{INPUT};

  bless $self, $className; $self->setJobReportValues('isAfp', 1);
  $self->setJobReportValues('isAfpCSF',0); ##   #mpezzi csf files could omit endof document

  $self->setJobReportValues(
    'LastFormDef' => '', 'LastMediumMap' => '', 'LastDataMap' => '',
  );

  $self->Open(); my $lineOffset;

  while( !$INPUT->eof() ) {
   ($lineOffset, $self->{lastLine}) = $self->getLine();
    $self->{lastcc} = substr($self->{lastLine},0,1);

    my $cc = $self->{lastcc};
	(DEBUG or $main::veryverbose) && i::logit("DEBUG - ".__PACKAGE__.__LINE__."::sub new() - ref(self):".ref($self)." - line:".unpack('H80',$self->{lastLine}) );

   if (( $self->{lastLine} =~ /^\x5a..(...)...(.{0,11})/ ) and (getConfValues("FORCE_RF") !~ /^(RF_)(AFP|ASCII|EBCDIC)$/i )) {
   #if ( $self->{lastLine} =~ /^\x5a..(...)...(.{0,11})/ ) {
     my ($fc5a, $sig5a) = ($1, $2);
	 (DEBUG or $main::veryverbose) && i::logit("DEBUG - ".__PACKAGE__.__LINE__."::sub new() fc5a:".unpack('H*',$fc5a).", sig5a:".unpack('H*',$sig5a));
### Test for afp BeginDocument or BeginResourceGroup structured fields -------- //
# \x53\x74\x72\x65\x61\x6D\x53\x65\x72\x76\x65 StreamServe signature
     next unless $fc5a =~ /^\xd3(?:\xee\xee|\xa8[\xa7\xa8\xc6])$/;
	 (DEBUG or $main::veryverbose) && i::logit("DEBUG - ".__PACKAGE__.__LINE__."::sub new() fc5a:".unpack('H*',$fc5a).", sig5a:".unpack('H*',$sig5a));
     $self->setJobReportValues('isAfpCSF',1) if $fc5a =~ /^\xd3\xee\xee$/ && $sig5a =~ /^\xc3\xe2\xc6.*$/; # CSF Ebcdic
     next if $fc5a =~ /^\xd3\xee\xee$/ && $sig5a !~ /^\x53\x74\x72\x65\x61\x6D\x53\x65\x72\x76\x65.*$/; # StreamServe ascii
	 (DEBUG or $main::veryverbose) && i::logit("DEBUG - ".__PACKAGE__.__LINE__."::sub new() fc5a:".unpack('H*',$fc5a).", sig5a:".unpack('H*',$sig5a));

      $self->setJobReportValues('isFullAfp',1);
      if ( $self->getJobReportValues('FormDef') eq '') {
        $self->setJobReportValues('FormDef', 'NULL');
      }
      $INPUT->Close();

     my $StreamParser = getConfValues('StreamParser');
     $StreamParser = 'XReport::AFP::IOStream' unless $StreamParser;

     eval "require $StreamParser";
     croak "Unable to load $StreamParser - $@" if $@;
#     warn ref($self), " new ", sprintf(" called by %s@%s", (caller())[0,2]), " StreamParser: $StreamParser INPUT:", ref($INPUT), "\n"  if $main::debug;

     return $StreamParser->new($self);
    }
    if ( exists($asaExecTable{$cc}) ) {
      $self->{ExecTable} = \%asaExecTable;
      $self->setJobReportValues('cc','asa');
     $self->{FFcode} = "\xf1";
    }
    else {
      $self->{ExecTable} = \%machineExecTable;
      $self->setJobReportValues('cc','machine');
     $self->{FFcode} = "\x8b";
    }

    last;
  }
  $self->Close(); my $jr = $self->{'jr'};
  require XReport::AFP::EXTRACT;

  my ($XferStartTime, $PageDef) = $jr->getValues(qw(XferStartTime PageDef)); return $self if $PageDef eq '';

  XReport::AFP::EXTRACT::ToLocalResources(
    my $LocalResources = $jr->getFileName('LOCALRES'),
    $XferStartTime, $PageDef
  );

  $INPUT = gensym();

  my $FileName = "$LocalResources/$PageDef\.ps";

  $FileName = getConfValues("afpdir") . "/pdeflib/$PageDef\.ps" if !-e $FileName;

  open($INPUT, "<$FileName")
    or
  die("INPUT OPEN ERROR FOR FILE \"$FileName\" $!");

  my ($dm_1, $dm, %dm_c1);

  while(<$INPUT>) {
    if ( $_ =~ /\/FirstDataMap +\((\w+)\)/ ) {
      $dm_1 = $1; last;
    }
  }
  while(<$INPUT>) {
    if ( $_ =~ /\/DataMap\.(\w+)/ ) {
      $dm = $1;
    }
    elsif ($_ =~ /^ *\[ *1 +(\d+) /) {
      $dm_c1{$dm} = $translator->toebcdic($1);
    }
  }
  close($INPUT);

  @{$self}{qw(dm_1 dm_c1)} = ($dm_1, \%dm_c1);
  $self->{'c1'} = $self->{'dm_c1'}->{$self->{'dm_1'}};

  return $self;
}

#------------------------------------------------------------
package XReport::INPUT::VippIPage;
use constant DEBUG => 0;

our @ISA = ('XReport::INPUT::IPage');

use strict; use bytes;

sub NewPage {
  my $self = shift; $self->{atPage} += 1;

  my $page = {
    INPUT  => $self,
    atPage => $self->{atPage},
    atLine => $self->{atLine},
    lineList => [], rawpage => [],
    atOffset => $self->tell()
  };

  bless $page, 'XReport::INPUT::VippIPage';
}

sub appendLine {
  my $self = shift; my ($INPUT, $lineList) = @{$self}{qw(INPUT lineList)};
  (DEBUG or $main::veryverbose) and i::warnit("appendLine VippIPage\n".join( ' ', @_));
  push @$lineList, XReport::INPUT::AsciiLine->new($self, scalar(@$lineList)+1, @_); $INPUT->{atLine} += 1;
}

sub overwriteLastLine {
  my $self = shift; my $page = $self->{lineList};
  $page->[$#$page]->overwriteLine(@_);
}

sub getLine {
  my $self = shift;

  my ($lineOffsets, $lineCache)
   =
  @{$self}{qw(lineOffsets lineCache)};

  if ( !@$lineCache ) {
    my $INPUT = $self->{INPUT};
    $INPUT->getLines($lineOffsets, $lineCache , 88);
  }

  return if !scalar(@$lineCache);

  if ( wantarray ) {
    return (shift @$lineOffsets, shift @$lineCache);
  }
  else {
    shift @$lineOffsets; return shift @$lineCache;
  }
}

sub eof {
  return (!@{$_[0]->{lineCache}} and !exists($_[0]->{lastLine}) and $_[0]->{INPUT}->eof());
}

sub GetPage {
  (DEBUG or $main::veryverbose) && i::warnit("DEBUG - ".__PACKAGE__." sub GetPage  ");
  my $self = shift;  return undef if $self->eof(); my $INPUT = $self->{INPUT};

  my ($lineOffset, $line) = ($self->{lineOffset}, '');

  my $page = $self->NewPage();

  if ( exists($self->{lastLine}) ) {
    $page->appendLine($self->{lastLine});
    delete $self->{lastLine};
  }

  while(1) {
   ($lineOffset, $line) = $self->getLine(); last if $line eq "";
    last if /^(?:C001 )|(\%\%EOF)/;
    chomp;
    $page->appendLine($line);
  }
  if  ( $line !~ /^\%\%EOF/ ) {
    chomp;
    $self->{lastLine} = $line;
  }
  else {
    my $trailer = $line;
    while(1) {
      $line = $self->getLine(); last if $line eq "";
      $trailer .= $line;
    }
    $self->{TRAILER} = $trailer;
    $self->{lastLine} = ();
  }

  $self->{lineOffset} = $lineOffset;

  return $page;
}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};

  @{$self}{qw(lineOffset lineOffsets lineCache)} = (0, [], []);

  delete $self->{lastLine};

  @{$self}{qw(atPage atLine)} = (0, 0);

  return $INPUT->Open(@_);
}

sub Close {
  my $self = shift; my $INPUT = $self->{INPUT};

  @{$self}{qw(lineOffset lineOffsets lineCache)} = (0, [], []);

  @{$self}{qw(atPage atLine)} = (0, 0);

  return $INPUT->Close(@_);
}

sub new {
  my ($className, $self) = (shift, shift);

  bless $self, $className;

  $self->Open(@_);

  return $self;
}

#------------------------------------------------------------
package XReport::INPUT::PostscriptIPage;
use constant DEBUG => 0;

our @ISA = ('XReport::INPUT::IPage');

sub new {
  my ($className, $self) = (shift, shift); my $INPUT = $self->{INPUT};

  require XReport::Postscript::IOStream;

  return XReport::Postscript::IOStream->new($self);
}

#------------------------------------------------------------
package XReport::INPUT::XMLIPage;
use constant DEBUG => 0;

our @ISA = ('XReport::INPUT::IPage');

use strict; use bytes;

use XReport::Util qw($logger $logrRtn);

use constant EL_NAME => 0;
use constant EL_ATTRS => 1;
use constant EL_STRING => 2;

sub NewPage {
  my $self = shift; $self->{atPage} += 1;
  (DEBUG or $main::veryverbose) and i::warnit(__PACKAGE__." NewPage - caller: ".join('::', (caller())[0,2]));

  my $page = {
    INPUT  => $self,
    atPage => $self->{atPage},
    overflowed => 0,   #### mpezzi overflowed was missing
    atLine => $self->{atLine},
    lineList => [], rawpage => [],
    atOffset => $self->tell()
  };

  bless $page, 'XReport::INPUT::XMLIPage';
}

#### mpezzi overflowed was missing
sub overflowed {
  (DEBUG or $main::veryverbose) and i::warnit(__PACKAGE__." overflowed - caller: ".join('::', (caller())[0,2]));
  my $self = shift; return $self->{overflowed} if !scalar(@_); $self->{overflowed} = shift;
}
###

sub appendLine {
  my $self = shift; my ($INPUT, $lineList) = @{$self}{qw(INPUT lineList)};
  (DEBUG or $main::veryverbose) and i::warnit("appendLine XMLIPage\n".join(' ', @_));
  push @$lineList, XReport::INPUT::AsciiLine->new($self, scalar(@$lineList)+1, @_); $INPUT->{atLine} += 1;
}

sub overwriteLastLine {
  my $self = shift; my $page = $self->{lineList};
  $page->[$#$page]->overwriteLine(@_);
}

sub GetAttributes {
  my ($self, $elName) = (shift, shift); my $el = $self->{el}; my @r = ();

  return undef if $elName ne $el->[EL_NAME];

  my $elAttrs = $el->[EL_ATTRS];
  for (@_) {
    $elAttrs->{$_} =~ s/ +$//;
    push @r, $elAttrs->{$_};
  }

  return wantarray ? @r : $r[0];
}

sub setElement {
  my ($self, $el) = (shift, shift); my ($elName, $elAttrs, $elString) = @$el;

  $self->{el} = $el;

  print XMLOUT $el->[EL_STRING], "\n" if $self->atPage() <= 1000;

  $self->appendLine(" ".$el->[EL_STRING]);
}

sub getLine {
#### mpezzi modificato per far funzionare lo swift
  my $self = shift; my $INPUT = $self->{INPUT};
  return (0, $INPUT->getLine());

#  my $self = shift;

#  my ($lineOffsets, $lineCache)
#   =
#  @{$self}{qw(lineOffsets lineCache)};

#  if ( !@$lineCache ) {
#    my $INPUT = $self->{INPUT};
#    $INPUT->getLines($lineOffsets, $lineCache , 77);
#  }

#  return if !scalar(@$lineCache);

#  if ( wantarray ) {
#    return (shift @$lineOffsets, shift @$lineCache);
#  }
#  else {
#    shift @$lineOffsets; return shift @$lineCache;
#  }
###
}

sub eofLines {
  return (!@{$_[0]->{lineCache}} and !exists($_[0]->{lastLine}) and $_[0]->{INPUT}->eof());
}

sub eof {
  #return ($_[0]->atPage >= 30000 or (!@{$_[0]->{elCache}} and $_[0]->eofLines()) );
  return (!@{$_[0]->{elCache}} and $_[0]->eofLines());
}

sub checkElString {
  my $self = shift;

  if (length($self->{elString}) > 100000) {
    print $self->{elString};
    die "MAXLEN DETECTED";
  }
}

sub start_handler
{
  my ($self, $parser, $elName) = (shift, shift, shift);

  if ( (my $depth = $parser->depth()) <= 1 ) {
    if ( $depth == 0 ) {
      my $elCache = $self->{elCache};
      push @$elCache, [
        $elName, {@_} , $parser->original_string()
      ];
    }
    @{$self}{qw(elAttrs elString)} = ({@_}, '');
  }

  $self->{elString} .= $parser->original_string();

  $self->checkElString();
}

sub end_handler
{
  my ($self, $parser, $elName) = @_; my $elCache = $self->{elCache};

  if ( (my $depth = $parser->depth()) <= 1 ) {
    if ( $depth == 1 ) {
      push @$elCache, [
        $elName, $self->{elAttrs} , $self->{elString} . $parser->original_string()
      ];
    }
    @{$self}{qw(elAttrs elString)} = ('', '');
  }
  else {
    $self->{elString} .= $parser->original_string();

    $self->checkElString();
  }
}

sub default_handler {
  my ($self, $parser) = (shift, shift);

  $self->{elString} .= join('', @_);

  $self->checkElString();
}

sub fill_elCache {
  my $self = shift; my ($parser, $elCache) = @{$self}{qw(parser elCache)};

  while(1) {
    my ($lineOffset, $line) = $self->getLine(); last if $line eq "";

    $parser->parse_more($line);

    last if ( scalar(@$elCache) > 15 );
  }

  return scalar(@$elCache);
}

sub GetPage {
  (DEBUG or $main::veryverbose) && i::warnit("DEBUG - ".__PACKAGE__." sub GetPage  ");
  my $self = shift;  return undef if $self->eof(); my $INPUT = $self->{INPUT};

  my ($elCache, $page)  = ($self->{elCache}, $self->NewPage());

  $self->fill_elCache() if !@$elCache;

  my $el = shift @$elCache || return undef; $page->setElement($el);

  return $page;
}

sub Open {
  my $self = shift; my $INPUT = $self->{INPUT};

  @{$self}{qw(lineOffset lineOffsets lineCache elCache)} = (0, [], [], []);

  require XML::Parser::Expat;

  my $parser = XML::Parser::ExpatNB->new(
    ProtocolEncoding => 'US-ASCII',
    NoExpand => 1
  );

  $parser->setHandlers(
    'Start'    => sub {$self->start_handler(@_)},
    'End'      => sub {$self->end_handler(@_)},
    'Default'  => sub {$self->default_handler(@_)}
  );

  @{$self}{qw(parser elString elAttrs atPage atLine)} = ($parser, '', undef, 0, 0);

  open(XMLOUT, ">c:/temp/zzxml.xml");

  my $rc = $INPUT->Open(@_);

  return $rc;
}

sub Close {
  my $self = shift; my ($INPUT, $parser) = @{$self}{qw(INPUT parser)};

  @{$self}{qw(lineOffset lineOffsets lineCache elCache)} = (0, [], [], []);

  eval {
    $parser->parse_done();
  };

  delete $self->{parser};

  @{$self}{qw(elString elAttrs atPage atLine)} = ('', undef, 0, 0);

  close(XMLOUT);

  return $INPUT->Close(@_);
}

sub new { 
  my ($className, $self) = (shift, shift);

  bless $self, $className;

  $self->Open(@_);

  return $self;
}

1;


__END__

$Log: INPUT.pm,v $
Revision 1.2  2002/10/23 22:50:18  Administrator
edit

Revision 1.3  2001/03/22 13:10:02  mpezzi
Aggiunte versioni ai file

