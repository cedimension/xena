package XReport::QUERY;

use strict;

use XReport::JobREPORT;
use XReport::EXTRACT;

use constant EF_PRINT => 1;
use constant EF_DATA => 2;

use constant FULL_MODE => 1;

sub ExtractFromIndex {
  my $self = shift; my %args = @_;

  my ($TO_DIR, $QUERY_FILE, $INDEX_NAME, $FORMAT, $PARSE_FILE, $TRANSMIT, $REQUEST_ID) 
   = 
  @args{qw(TO_DIR QUERY_FILE INDEX_NAME FORMAT PARSE_FILE TRANSMIT REQUEST_ID)};

  my $ExtractFile = ""; my %KeepFiles = ();

  for (glob("$TO_DIR/$REQUEST_ID\*")) {
    unlink("$_") or die("UNABLE TO DELETE \"$_\" $!");
  }

  my $ExtractList = XReport::EXTRACT->ExtractPages(
    TO_DIR => $TO_DIR,
    QUERY_FILE => $QUERY_FILE, 
    INDEX_NAME => $INDEX_NAME,
	REQUEST_ID => $REQUEST_ID.".XRPAGES"
  );
  
  for (@$ExtractList) {
    my ($JobReportId, $ExtractedFile) = @$_; 
	
    my $jr = XReport::JobREPORT->Open($JobReportId, FULL_MODE);
	
	my @AfpResList = $jr->getValues('FormDef', 'PageDef');

    my ($ElabFormat, $isAfp, $AfpResFile, $AfpResDir, $INPUT, $OUTPUT);

    #($ElabFormat, $isAfp) = $jr->getValues(qw(ElabFormat isAfp)); 
    $ElabFormat = $jr->getValues('ElabFormat'); $isAfp = 1; 
	
    my $AfpResFile = "$TO_DIR/$REQUEST_ID\.IBMAFPRES\.$JobReportId.dat";
	my $TransmitFile = "$TO_DIR/$REQUEST_ID\.XMIT.$JobReportId.xmi";
  
	if ( $FORMAT eq "DATA" ) {
      if ( $isAfp ) {
        require XReport::AFP::EXTRACT;
        XReport::AFP::EXTRACT::MakeAfpResGroup( $AfpResFile, @AfpResList); 
	  }
      if ( $TRANSMIT ) {
        require XReport::TRANSMIT::OUTPUT;
        my $rc = XReport::TRANSMIT::OUTPUT->Create(
		  %$TRANSMIT,
          toFileName => $TransmitFile,
	      fromFileNameList => [ $AfpResFile, $ExtractedFile ]
        );
		$KeepFiles{$TransmitFile} = 1;
	  }
	  else {
	    $KeepFiles{$ExtractedFile} = 1;
	    $KeepFiles{$AfpResFile} = 1 if $AfpResFile;
	  }
    }

    elsif ( $FORMAT eq "PRINT" ) {
	  if ( $ElabFormat == EF_PRINT ) {
	    $KeepFiles{$ExtractedFile} = 1;
	  }
	  elsif ( $ElabFormat == EF_DATA ) {
	    $jr->PRINT_EXTRACT(
		  ExtractedFile => $ExtractedFile, 
		  REQUEST_ID => $REQUEST_ID,
		  TO_DIR => $TO_DIR
		);
	  }
    }
  }

  my @DeleteFiles = glob("$TO_DIR/$REQUEST_ID\*");
  
  if ( $FORMAT eq "DATA" ) {
    @DeleteFiles = grep(!exists($KeepFiles{$_}), @DeleteFiles);
  }
  elsif ( $FORMAT eq "PRINT" ) {
    @DeleteFiles = grep($_!~/\.PDF/i, @DeleteFiles);
  }
  
  for (@DeleteFiles) {
    unlink("$_") or die("UNABLE TO DELETE \"$_\" $!");
  }

  return glob("$TO_DIR/$REQUEST_ID\*");
}

sub new {
  my $self = {
  };

  bless $self;
}

1;
