
package XReport::Multicast;

require 5.005;
use strict;
use vars qw(@ISA @EXPORT );

use IO::Socket;
use Carp 'croak';

require Exporter;
@ISA = qw(Exporter DynaLoader IO::Socket::INET);

use constant { 
  IPPROTO_IP                =>  0,
#  IP_OPTIONS                =>  1, # set/get IP options
#  IP_HDRINCL                =>  2, # header is included with data
#  IP_TOS                    =>  3, # IP type of service and preced
#  IP_TTL                    =>  4, # IP time to live
  IP_MULTICAST_IF           =>  9, # set/get IP multicast i/f
  IP_MULTICAST_TTL          => 10, # set/get IP multicast ttl
  IP_MULTICAST_LOOP         => 11, # set/get IP multicast loopback
  IP_ADD_MEMBERSHIP         => 12, # add an IP group membership
  IP_DROP_MEMBERSHIP        => 13, # drop an IP group membership
  IP_DONTFRAGMENT           => 14, # don't fragment IP datagrams    {Do not Localize}
  IP_ADD_SOURCE_MEMBERSHIP  => 15, # join IP group/source
  IP_DROP_SOURCE_MEMBERSHIP => 16, # leave IP group/source
  IP_BLOCK_SOURCE           => 17, # block IP group/source
  IP_UNBLOCK_SOURCE         => 18, # unblock IP group/source
  IP_PKTINFO                => 19, # receive packet information for ipv4
  IP_DEFAULT_MULTICAST_TTL  => 1,    # normally limit m'casts to 1 hop    {Do not Localize}
  IP_DEFAULT_MULTICAST_LOOP => 1,    # normally hear sends if a member
  IP_MAX_MEMBERSHIPS        => 20, 
  IS_IP_ADDR => qr/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/
};

@EXPORT = qw();
#	     IPPROTO_IP
#	     IP_OPTIONS
#	     IP_HDRINCL
#	     IP_TOS
#	     IP_TTL
#	     IP_MULTICAST_IF
#	     IP_MULTICAST_TTL
#	     IP_MULTICAST_LOOP
#	     IP_ADD_MEMBERSHIP
#	     IP_DROP_MEMBERSHIP
#	     IP_DONTFRAGMENT
#	     IP_ADD_SOURCE_MEMBERSHIP
#	     IP_DROP_SOURCE_MEMBERSHIP
#	     IP_BLOCK_SOURCE
#	     IP_UNBLOCK_SOURCE
#	     IP_PKTINFO
#	     IP_DEFAULT_MULTICAST_TTL
#	     IP_DEFAULT_MULTICAST_LOOP
#	     IP_MAX_MEMBERSHIPS
#	     IS_IP_ADDR
#);

sub new {
  my ($class, $args) = ( shift, { @_ });
  @{$args}{qw(Proto Reuse)} = ('udp', 1);
  $class->SUPER::new( %$args );
}

sub mcast_add {
  my $sock = shift;
  my $group = shift || croak 'usage: $sock->mcast_add($mcast_addr [,$interface])';
  $group = inet_aton($group) if $group =~ /${+IS_IP_ADDR}/; 
  my $interface = shift;
  $interface = $sock->sockaddr() unless $interface;
  $interface = inet_aton($interface) if $interface =~ /${+IS_IP_ADDR}/; 
#  $sock->setsockopt(${+IPPROTO_IP}, ${+IP_ADD_MEMBERSHIP}, pack("a4a4", $group, $interface)) or croak "mcast_add error: $!\n";
  $sock->setsockopt(0, 12, pack("a4a4", $group, $interface)) or croak "mcast_add error: $!\n";
}

1;

