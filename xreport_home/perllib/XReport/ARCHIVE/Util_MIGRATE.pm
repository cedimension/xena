
package XReport::ARCHIVE::Util;

use strict;

use Time::Local;

use XReport::PDF::DOC_TEST;
use XReport::Util;
use XReport::ARCHIVE;

sub Migrate_INPUT_TO_CENTERA {
  my ($jr, $TargetLocalPathId, %args) = @_; require XReport::ARCHIVE::Centera; 

  die "????" if $TargetLocalPathId ne "C2";

  if ( !ref($jr) ) {
    require XReport::ARCHIVE::JobREPORT; 
    $jr = XReport::ARCHIVE::JobREPORT->Open($jr)
  } 
 
  my ($JobReportId, $XferStartTime) = $jr->getValues(qw(JobReportId XferStartTime));

  my @t = split(/\D+/, $XferStartTime); $t[1] -= 1;
  my $mtime = timelocal(reverse(@t));

  my $dbr = dbExecute("
    SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId
  ");

  die("THE JobReport $JobReportId HAS ALREADY THE INPUT ARCHIVE IN CENTERA")
   if
  0 and !$dbr->eof() && $dbr->GetFieldsValues('INFILE_ClipId') ne '';
  my $c1ObjectURI = "centera://c1/". $dbr->GetFieldsValues('INFILE_ClipId');

  $dbr->Close();

  print time(), " - BEGIN of INPUT MIGRATION TO CENTERA of JobReportId $JobReportId\n";

  my $iar = XReport::ARCHIVE::get_INPUT_ARCHIVE($jr, wrapper => 1);
  my (
    $archiver, 
    $LocalFileName
  ) 
  = @{$iar}{qw(archiver LocalFileName)};

  my $pool = XReport::ARCHIVE::Centera::FPPool->new( $TargetLocalPathId );
  my $clip = $pool->Create_Clip($jr, "INPUT"); my ($clipid, $clipsize) = ();
  
  $archiver = $iar->get_INPUT_ARCHIVE();

  for my $FileName (@{$archiver->FileNames()}) {
    my $compr_method = ($FileName =~ /\.pdf$/i) ? 'stored' : 'deflate';
    print "Migrating to Centera file=$FileName compr_method=$compr_method\n";

    my $is = $archiver->LocateFile($FileName); my $ctime = $is->ctime(); $is->Open(); my $rec;
    my $os = $clip->AddStream($FileName, ctime => $ctime, mtime => $mtime, compr_method => $compr_method);

    my $ibytes = 0;
    while(1) {
      $is->read($rec, 131072); last if $rec eq '';  $ibytes += length($rec);
      $os->write($rec);
    }
    $is->Close(); $os->Close();
    my $isize = $is->uncompressedSize();
    my $osize = $os->uncompressedSize();
    if ($isize < 0) {
      for(32,33,34) {
        if (2**$_+$isize) {
          $isize = 2**$_+$isize; last;
        }
      }
    }
    print "ibytes=$ibytes isize=$isize osize=$osize\n"; 
    die "lala $ibytes/$isize/$osize" if $ibytes != $isize or $osize != $isize;
  }
  $archiver->Close(); $clipid = $clip->Write(); $clipsize = $clip->GetTotalSize(); #$clip->ListFiles();
  print "== $clipid ==\n";
  my $c2ObjectURI = "centera://c2/$clipid";
  dbExecute("
    INSERT INTO tbl_MigrateCenteraClipIdsXref 
     (PoolId, ObjectId, c1ObjectURI, c2ObjectURI) 
    VALUES(
      -$JobReportId, 1, '$c1ObjectURI', '$c2ObjectURI'
    )
  ");

  dbExecute("
    UPDATE tbl_JobReportsCenteraClipIds set INFILE_ClipId = '$clipid', INFILE_TotalSize = $clipsize where JobReportId = $JobReportId
  ");
  
  $iar->Close();

  print time(), " - END of INPUT MIGRATION TO CENTERA of JobReportId $JobReportId ClipId=$clipid\n";
}

sub Migrate_INPUT {
  Migrate_INPUT_TO_CENTERA(@_);
}

sub Migrate_OUTPUT_TO_CENTERA {
  my ($jr , $TargetLocalPathId, %args) = @_; require XReport::ARCHIVE::Centera;

  die "????" if $TargetLocalPathId ne "C2";

  if ( !ref($jr) ) {
    require XReport::ARCHIVE::JobREPORT; 
    $jr = XReport::ARCHIVE::JobREPORT->Open($jr)
  }
 
  my ($JobReportId, $ElabStartTime) = $jr->getValues(qw(JobReportId ElabStartTime));

  my @t = split(/\D+/, $ElabStartTime); $t[1] -= 1;
  my $mtime = timelocal(reverse(@t));

  my $dbr = dbExecute("
    SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId
  ");

  die("THE JobReport $JobReportId HAS ALREADY THE OUTPUT ARCHIVE IN CENTERA")
    if 
  0 and !$dbr->eof() && $dbr->GetFieldsValues('OUTFILE_ClipId') ne ''; 
  my $c1ObjectURI = "centera://c1/". $dbr->GetFieldsValues('OUTFILE_ClipId');

  $dbr->Close();

  print time(), " - BEGIN of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId\n";

  my $iar = XReport::ARCHIVE::get_OUTPUT_ARCHIVE($jr, wrapper => 1);
  my (
    $archiver, 
    $LocalFileName
  ) 
  = @{$iar}{qw(archiver LocalFileName)};
  
  my $pool = XReport::ARCHIVE::Centera::FPPool->new( $TargetLocalPathId );
  my $clip = $pool->Create_Clip($jr, "OUTPUT"); my ($clipid, $clipsize) = ();

  for my $FileName (@{$archiver->FileNames()}) {
    if ($FileName =~ /\w+\.$JobReportId\.0\.#\d+\.(?:log|XREF)/) {
      print "todel=$FileName\n"; next; 
    }
    my $compr_method = ($FileName =~ /\.pdf$/i) ? 'stored' : 'deflate'; my $PDFOUT = 0;
    print "Migrating to Centera file=$FileName compr_method=$compr_method\n";
    
    if ($FileName =~ /\.pdf$/i) {
      $PDFOUT = gensym();
      open($PDFOUT, ">", "c:/temp/centera.migrate/$FileName") or die("==??a $!"); binmode($PDFOUT);
    }

    my $is = $archiver->LocateFile($FileName); my $ctime = $is->ctime(); $is->Open(); my $rec;
    my $os = $clip->AddStream($FileName, ctime => $ctime, mtime => $mtime, compr_method => $compr_method);

    my $ibytes = 0;
    while(1) {
      $is->read($rec, 131072); last if $rec eq ''; $ibytes += length($rec); 
      $os->write($rec);
      if ($PDFOUT) {
        print $PDFOUT $rec or die("????? $!\n");
      } 
    }

    $is->Close(); $os->Close(); 
    my $isize = $is->uncompressedSize();
    my $osize = $os->uncompressedSize();
    print "ibytes=$ibytes isize=$isize osize=$osize\n"; 
    die "lala $ibytes/$isize/$osize" if $ibytes != $isize or $osize != $isize;
    next if !$PDFOUT; close($PDFOUT);
 
    die("abracadabra") if -s "c:/temp/centera.migrate/$FileName" != $is->uncompressedSize(); 

    my $doc = XReport::PDF::DOC->Open("c:/temp/centera.migrate/$FileName");
    my $XrefFileName = basename($doc->PrepareXrefFileLengths()); 
    $doc->Close();

    my $XREFIN = gensym();
    open($XREFIN, "<", "c:/temp/centera.migrate/$XrefFileName") or die("==??b $!"); binmode($XREFIN);
    my $os = $clip->AddStream($XrefFileName, mtime => $mtime, compr_method => 'deflate');

    my $ibytes = 0;
    while(1) {
      read($XREFIN, $rec, 131072); last if $rec eq ''; $ibytes += length($rec);
      $os->write($rec);
    }
    close($XREFIN); $os->Close();
    my $isize = -s "c:/temp/centera.migrate/$XrefFileName";
    my $osize = $os->uncompressedSize();
    print "ibytes=$ibytes isize=$isize osize=$osize\n"; 
    unlink "c:/temp/centera.migrate/$FileName" or die("?? delete a $!");
    unlink "c:/temp/centera.migrate/$XrefFileName" or die("?? delete b $!");
  }
  $archiver->Close(); $clipid = $clip->Write(); $clipsize = $clip->GetTotalSize(); #$clip->ListFiles();  
  print "== $clipid ==\n";
  my $c2ObjectURI = "centera://c2/$clipid";
  dbExecute("
    INSERT INTO tbl_MigrateCenteraClipIdsXref 
     (PoolId, ObjectId, c1ObjectURI, c2ObjectURI) 
    VALUES(
      -$JobReportId, 2, '$c1ObjectURI', '$c2ObjectURI'
    )
  ");

  dbExecute("
    UPDATE tbl_JobReportsCenteraClipIds set OUTFILE_ClipId = '$clipid', OUTFILE_TotalSize = $clipsize where JobReportId = $JobReportId
  ");
    
  $iar->Close();

  print time(), " - END of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId ClipId=$clipid\n";
}

sub Migrate_OUTPUT {
  Migrate_OUTPUT_TO_CENTERA(@_);
}

1;
