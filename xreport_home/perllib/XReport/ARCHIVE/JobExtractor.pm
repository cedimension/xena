
package XReport::ARCHIVE::JobExtractor;

use strict;
no warnings 'deprecated';

use XReport::ARCHIVE::JobREPORT;

sub ZERO {
  my ($t, $l) = @_; $t = "0"x$l . $t; return substr($t, -$l);
}

sub getLocalBasket {
  my $self = shift; my ($JobReportId, $lck) = @{$self}{qw(JobReportId lck)}; my ($dbalias, $FileCache, $LocalBasket);

  my $dbr = dbExecute(
    "SELECT * from tbl_JobReportsLocalFileCache where JobReportid = $JobReportId"
  ); 
  
  if ( $dbr->eof() ) {
    $lck->getLock("JobReportsFileCache_$JobReportId", 30);

    $dbr = dbExecute(
	  "SELECT * from tbl_JobReportsLocalFileCache where JobReportid = $JobReportId "
	);
	if ( $dbr->eof() ) {
      $LocalBasket = int(rand(128)) + 1;
      dbExecute(
	    "INSERT INTO tbl_JobReportsLocalFileCache (JobReportId, LocalBasket) "
	   ."VALUES ($JobReportId, $LocalBasket) "
	  );
	}
    $dbr = dbExecute(
	  "SELECT * from tbl_JobReportsLocalFileCache where JobReportid = $JobReportId "
	);

    $lck->relLock("JobReportsFileCache_$JobReportId");
  }
   
  $dbalias = c::getValues(qw(dbalias)); $FileCache = c::getValues(qw(filecache)) . ($dbalias eq '' ? '' : "/$dbalias");

  $LocalBasket = 
   "$FileCache/" . ZERO($dbr->GetFieldsValues('LocalBasket'),5)
  ;
   
  mkdir($LocalBasket) if !-e $LocalBasket; return $self->{'LocalBasket'} = $LocalBasket;
}

sub FileExtracted {
  my $LocalFileName = shift; return 0 if !-e $LocalFileName;
  
  return 1 if $LocalFileName !~ /\.pdf$/i;

  my $INPUT = gensym(); 

  open($INPUT, "<$LocalFileName")
   or
  die("OPEN INPUT ERROR For \"\" $!"); binmode($INPUT); my $eof;
  
  seek($INPUT, -10, 2); read($INPUT, $eof, 10); close($INPUT); return $eof =~ /\%EOF/s; 
}

sub ExistsFile {
  my ($self, $FileName) = @_; my $LocalBasket = $self->{'LocalBasket'}; my ($FILE, $fc) = (gensym(), '');

  my $LocalFileName = "$LocalBasket/$FileName";  
  
  return $LocalFileName 
   if 
  FileExtracted($LocalFileName);
  
  my ($jr, $ojrar) = @{$self}{qw(jr ojrar)};

  $self->{'ojrar'} = $ojrar = $jr->get_OUTPUT_ARCHIVE() if !$ojrar;

  return $ojrar->ExistsFile($FileName, $LocalFileName);
}

sub ExtractFile {
  my ($self, $FileName) = @_; my ($LocalBasket, $lck) = @{$self}{qw(LocalBasket lck)}; my ($FILE, $fc) = (gensym(), '');

  my $LocalFileName = "$LocalBasket/$FileName";  return $LocalFileName if FileExtracted($LocalFileName);

  $lck->getLock("JobReportsFileCache_$FileName", 30);

  if ( !FileExtracted($LocalFileName) ) {
    my ($jr, $ojrar) = @{$self}{qw(jr ojrar)};

    i::logit("BEGIN Extracting File $FileName to $LocalFileName");

    $self->{'ojrar'} = $ojrar = $jr->get_OUTPUT_ARCHIVE() if !$ojrar;

    $fc = $ojrar->ExtractFile($FileName, $LocalFileName); 

    i::logit("END Extracting File $FileName to $LocalFileName");
  }

  $lck->relLock("JobReportsFileCache_$FileName"); return $LocalFileName;
}

sub FileContents {
  my ($self, $FileName) = @_; my ($LocalBasket, $lck) = @{$self}{qw(LocalBasket lck)}; my ($FILE, $fc) = (gensym(), '');
  my $LocalFileName = "$LocalBasket/$FileName";  

  if (-e $LocalFileName) {
    open($FILE, "<$LocalFileName"); binmode($FILE); read($FILE, $fc, -s $LocalFileName); close($FILE);
  }
  else {
    $lck->getLock("JobReportsFileCache_$FileName", 30);

    if ( -e $LocalFileName ) {
      open($FILE, "<$LocalFileName"); binmode($FILE); read($FILE, $fc, -s $LocalFileName); close($FILE);
    }
    else {
      my ($jr, $ojrar) = @{$self}{qw(jr ojrar)};

      $self->{'ojrar'} = $ojrar = $jr->get_OUTPUT_ARCHIVE() if !$ojrar;
      i::logit("Extracting $FileName requested by ".join('::', (caller())[0,2])
                           ." - JR REF: ".ref($self->{jr})." - OJR REF: ".ref($ojrar)
                           ." OJRAR: ".Dumper($ojrar)
                           );

      my $FileNameSize = $ojrar->FileSize($FileName);
      $ojrar->ExtractFile($FileName, $LocalFileName);
      open($FILE, "<$LocalFileName"); binmode($FILE); read($FILE, $fc, -s $LocalFileName); close($FILE);
      die "ERROR: Size of $LocalFileName(".(-s $LocalFileName).") is not equal to $FileName($FileNameSize)\n" if(-s $LocalFileName != $FileNameSize);
	  #die "JE FileContents a $FileName ??? ll=", length($fc) if $FileName =~ /\.9088\.0\.#64.pdf/;
#      $fc = $ojrar->FileContents($FileName); my $OUTPUT = gensym();
	  #die "JE FileContents b $FileName ??? ll=", length($fc) if $FileName =~ /\.9088\.0\.#64.pdf/;

#      open($FILE, ">$LocalFileName"); binmode($FILE); print $FILE $fc; close($FILE);
    }

    $lck->relLock("JobReportsFileCache_$FileName");
  }

  return ($fc ne "" ? \$fc : undef);
}

sub getPageXref {
  my $self = shift; my $JobReportId = $self->{'JobReportId'}; my @r;

  my $Xrf = $self->FileContents("\$\$PageXref.$JobReportId\.txt"); 

  for(split("\n",$$Xrf)) {
    if ($_ =~ /File=(\d+) +From=(\d+) +To=(\d+)/) {
      #push @r, $1, $2, $3;
      #added in 2014/10/10 create an array structure shifted by fileid * 3 times [...fileid, From, To.....] 
      my ($fid, $from,$to) = ($1, $2, $3);
      $r[$fid*3] = $fid; $r[$fid*3+1] = $from;$r[$fid*3+2] = $to;
    }
  }

  return @r;
}

sub extractFile {
  my ($self, $FileId, $Ext, $IF_EXISTS) = @_; 

  my (
    $JobReportId, $JobReportName 
  ) = 
  @{$self}{qw(JobReportId JobReportName)};

  my $FileName = "$JobReportName\.$JobReportId.0.#$FileId\.$Ext";

  return $self->ExtractFile($FileName)
   if
  $self->ExistsFile($FileName); $FileName = "$JobReportName\.$JobReportId.$FileId.#0\.$Ext";

  return $self->ExtractFile($FileName)
   if
  $self->ExistsFile($FileName); $FileName = "$JobReportName\.$JobReportId.0.$FileId#\.$Ext";

  $IF_EXISTS and !$self->ExistsFile($FileName) and return undef; $self->ExtractFile($FileName);
}

sub getFile {
  my ($self, $FileId, $Ext, $IF_EXISTS) = @_; 

  my (
    $JobReportId, $JobReportName 
  ) = 
  @{$self}{qw(JobReportId JobReportName)}; 

  my $FileName = "$JobReportName\.$JobReportId.0.#$FileId\.$Ext";

  return $self->FileContents($FileName)
   if
  $self->ExistsFile($FileName); $FileName = "$JobReportName\.$JobReportId.$FileId.#0\.$Ext";

  $IF_EXISTS and !$self->ExistsFile($FileName) and return undef; $self->FileContents($FileName);
}

sub Close {
  my $self = shift; return if $self->{'closed'};
  i::logit(ref($self)." Closing as requested by ".join('::', (caller())[0,2]));

  for my $obj (@{$self}{qw(ojrar jr)}) {
    $obj->Close() if $obj;
  }

  %$self = (closed => 1);
}

sub getValues {
  my $self = shift; $self->{'jr'}->getValues(@_);
}

sub Open {
  my ($ClassName, $JobReportId) = @_; my ($self, $jr, $JobReportName); 
  
  $jr = XReport::ARCHIVE::JobREPORT->Open($JobReportId);
  $JobReportName = $jr->getValues('JobReportName');

  require XReport::Locks;

  $self = {
    JobReportName => $JobReportName, JobReportId => $JobReportId, 
    jr => $jr, lck => XReport::Locks->new(), closed => 0
  };

  bless $self, $ClassName; $self->getLocalBasket(); return $self;
}

sub DESTROY {
 #print "DESTROY ", __PACKAGE__, "\n";
  my $self = shift; $self->Close(); 
}

1;
