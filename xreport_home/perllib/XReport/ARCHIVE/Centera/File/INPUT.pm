
package XReport::ARCHIVE::Centera::File::INPUT;

use strict;

use Compress::Zlib;
use Digest::MD5 qw(md5_hex);

sub desiredCompressionMethod {
  my $self = shift; $self->{'desired_compr_method'} = shift;
}

sub size {
  my $self = shift; my ($Tag_file) = @{$self}{qw(Tag_file)};
  
  return $Tag_file->getAttributes('size');
}

sub uncompressedSize {
  my $self = shift; my ($Tag_file) = @{$self}{qw(Tag_file)};
  
  return $Tag_file->getAttributes('origsize');
}

sub ctime {
  my $self = shift; my ($Tag_file) = @{$self}{qw(Tag_file)};
  
  return $Tag_file->getAttributes('ctime');
}

sub mtime {
  my $self = shift; my ($Tag_file) = @{$self}{qw(Tag_file)};
  
  return $Tag_file->getAttributes('mtime');
}

sub Tag_file {
  my $self = shift; my ($Tag_file) = @{$self}{qw(Tag_file)}; return $Tag_file;
}

sub Open {
  my $self = shift; $self->Close() if $self->{'opened'}; my ($Tag_file, $Tag_chunk, $chunks, $chunk);

  $Tag_file = $self->{qw(Tag_file)}; $chunks = $Tag_file->getAttributes('chunks');
  
  if ($chunks > 1) {
    $Tag_chunk = $Tag_file->GetFirstChild(); $chunk = $self->BlobRead($Tag_chunk);
  }
  else {
    $Tag_chunk = undef; $chunk = $self->BlobRead($Tag_file);
  }

  @{$self}{qw(
    Tag_chunk 
    chunk at_chunk chunkpos
    residual_size filepos opened
  )} 
  = ($Tag_chunk, $chunk, 1, 0, length($$chunk), 0, 1); return $self;
}

sub binmode {
}

sub eof {
  my $self = shift; !($self->{'residual_size'} > 0 || $self->{'chunks'} > $self->{'at_chunk'}); 
}

sub tell {
  $_[0]->{'filepos'};
}

sub BlobRead { 
  my ($self, $tag) = (shift, shift); my $chunk = $tag->BlobRead(); my ($md5, $tmd5);

  local $_ = $self->{'compr_method'};

  SWITCH: {
    /^stored$/ && do {
      last;
    };
    /^deflate$/ && do {
      $$chunk = uncompress($$chunk); last;
    };
    die "BlobRead INVALID COMPRESSION METHOD DETECTED $_ !!";
  }

  if ( ($md5 = md5_hex($$chunk)) ne ($tmd5 = $tag->getAttributes('md5')) ) { 
	if ($XReport::ARCHIVE::Centera::File::INPUT::noCheckMd5)
	{
		warn ("BlobRead MD5 MISMATCH DETECTED $md5/$tmd5");
	}
	else
	{
		die("BlobRead MD5 MISMATCH DETECTED $md5/$tmd5");
	}
  }

  if ( $tag->getAttributes('size') > $tag->GetBlobSize() ) {
    my $tm = $tag->GetFirstChild(); my $t = $tm->getAttributes('TagName');
    die("INVALID TAG DETECTED: EXTECTED=more READ=$t.") 
     if 
    $t ne 'more';
    $$chunk .= ${$self->BlobRead($tag->GetFirstChild($tm))};
  }

  return $chunk;
}

sub read {
  my $self = shift; my ($chunk, $chunkpos, $residual_size, $filepos) = @{$self}{qw(chunk chunkpos residual_size filepos)};
  
  $_[0] = ''; my $lenreq = $_[1]; my $rest = $lenreq; my $Tag_chunk;

  while ($rest) {
    my $len = ($residual_size >= $rest) ? $rest : $residual_size; $residual_size -= $len;
    $_[0] .= substr($$chunk, $chunkpos, $len); $chunkpos += $len; 

    last if ($rest -= $len) == 0;
    
    if (
      $Tag_chunk = $self->{'Tag_chunk'} 
      and $self->{'Tag_chunk'} = $Tag_chunk = $Tag_chunk->GetSibling() 
    ) {
      @{$self}{qw(chunk at_chunk)} = (
        $chunk = $self->BlobRead($Tag_chunk), 
        $self->{'at_chunk'}+1
      );
      $chunkpos = 0; $residual_size = length($$chunk);
    }
    else {
      last;
    }
  }
  my $len = $lenreq-$rest; $filepos += $len;

  @{$self}{qw(chunkpos residual_size filepos)} = ($chunkpos, $residual_size, $filepos); return $len;
}

sub seek {
  my ($self, $to, $from) = @_;
  my (
    $filesize, $filepos, $chunkpos, $residual_size 
  ) = 
  @{$self}{qw(filesize filepos chunkpos residual_size)}; 

  if ($from != 0) {
    $to = $from == 1 ? $filepos+$to : $filesize+$to;
  }
  $to = $to < 0 ? 0 : $to > $filesize ? $filesize : $to; my $delta = $to - $filepos;

  if ($delta > 0) {
    if ($residual_size > $delta) {
      $filepos += $delta; $chunkpos += $delta; $residual_size -= $delta; 
    }
    else {
      my ($Tag_chunk, $at_chunk) = @{$self}{qw(Tag_chunk at_chunk)};

      while(($delta = $delta - $residual_size) > 0) {
        $filepos += $residual_size;
        $Tag_chunk = $Tag_chunk->GetSibling() 
         or
        die("TRYING TO SEEK PAST OF FILE FOR $delta BYTES"); $at_chunk += 1;
        $residual_size = $Tag_chunk->getAttributes('origsize'); 
      }

      $chunkpos=$residual_size+$delta; $filepos+=$chunkpos; $residual_size-=$chunkpos;

      @{$self}{qw(Tag_chunk at_chunk chunk)} = ($Tag_chunk, $at_chunk, $self->BlobRead($Tag_chunk));
    }
  }
  elsif ($delta < 0) {
    if ($chunkpos >= - $delta ) {
      $filepos += $delta; $chunkpos += $delta; $residual_size -= $delta;
    }
    else {
      my ($Tag_chunk, $at_chunk) = @{$self}{qw(Tag_chunk at_chunk)}; 

      while(($delta = $delta + $chunkpos) < 0) {
        $filepos -= $chunkpos; 
        $Tag_chunk = $Tag_chunk->GetPrevSibling() 
         or
        die("TRYING TO SEEK PREV BEGIN OF FILE FOR $delta BYTES"); $at_chunk -= 1;
        $chunkpos = $Tag_chunk->getAttributes('origsize'); 
      }
      $residual_size = $chunkpos - $delta; $chunkpos = $delta; $filepos -= $residual_size; 

      @{$self}{qw(Tag_chunk at_chunk chunk)} = ($Tag_chunk, $at_chunk, $self->BlobRead($Tag_chunk));
    }
  } 

  @{$self}{qw(filepos chunkpos residual_size)} = ($filepos, $chunkpos, $residual_size); return $filepos;
}

sub Close {
  my $self = shift; $self->{'opened'} = 0;
}

sub new {
  my ($ClassName, $Tag_file) = @_; my ($self, $filename, $compr_method, $origsize);
  (
    $filename, $compr_method, $origsize
  ) = 
  $Tag_file->getAttributes(qw(filename compr_method origsize));

  $self = {
    filename => $filename, Tag_file => $Tag_file, opened => 0,
    chunks => $Tag_file->getAttributes('chunks'),
    compr_method => $compr_method, 
    filesize => $origsize,
  };

  bless $self, $ClassName;
}

sub DESTROY {
  my $self = shift; $self->Close() if $self->{'opened'};
}

1;
