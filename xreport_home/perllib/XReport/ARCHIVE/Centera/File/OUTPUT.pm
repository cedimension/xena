
package XReport::ARCHIVE::Centera::File::OUTPUT;

use strict;

use Compress::Zlib;
use Digest::MD5 qw(md5_hex);

sub desiredCompressionMethod {
  my $self = shift; $self->{'compression'} = shift;
}

sub size {
  my $self = shift; my ($Tag_file) = @{$self}{qw(Tag_file)};
  
  return $Tag_file ? $Tag_file->getAttributes('size') : $self->{'size'};
}

sub uncompressedSize {
  my $self = shift; my ($Tag_file) = @{$self}{qw(Tag_file)};
  
  return $Tag_file ? $Tag_file->getAttributes('origsize') : $self->{'origsize'};
}

sub Tag_file {
  my $self = shift; my ($Tag_file) = @{$self}{qw(Tag_file)};
}

sub Open {
  ### load (bucket list)
  ### begin clear buffer
}

sub tell {
  ### nomore bytes and !@bucketList
}

sub prepare_chunk {
  my ($self, $chunk) = @_; my $origsize = length($$chunk);
 
  local $_ = $self->{'compr_method'};

  SWITCH: {
    /^stored$/ && do {
      last;
    };
    /^deflate$/ && do {
      $$chunk = compress($$chunk); last;
    };
    die "BlobWrite INVALID COMPRESSION METHOD DETECTED $_ !!";
  }

  return ($origsize, length($$chunk));
}

sub writechunk {
  my $self = shift; my ($md5, $size, $origsize, $Tag_chunk);

  my (
    $Tag_file, 
    $chunk, $at_chunk, 
    $atoffset, $compr_method
  ) = 
  @{$self}{qw(Tag_file chunk at_chunk atoffset compr_method)};

  $Tag_chunk = $Tag_file->Create_Child('chunk'); $at_chunk += 1; 

  $md5 = md5_hex($$chunk);

  ($origsize, $size) = $self->prepare_chunk($chunk);

  $Tag_chunk->setAttributes(
    seq => $at_chunk, offset => $atoffset, md5 => $md5, size => $size, origsize => $origsize
  );

  @{$self}{qw(at_chunk atoffset)} = ($at_chunk, $atoffset+$size); 
  
  print time()," 1 blobwrite $at_chunk $origsize $size\n";
  $Tag_chunk->BlobWrite($chunk); $Tag_chunk->Close(); $$chunk = '';
  print time()," 2 blobwrite\n";

  $Tag_file->incrAttributes( chunks => 1, size => $size, origsize => $origsize ); return $size;
}

sub write {
  my ($self, $buffer_arg) = @_; my ($chunk, $residual_size) = @{$self}{qw(chunk residual_size)};

  my $buffer = ref($buffer_arg) ? $buffer_arg : \$buffer_arg; my $wr_bytes = 0;

  if (length($$buffer) <= $residual_size) {
    $$chunk .= $$buffer; $wr_bytes = length($$buffer);
    $residual_size -= $wr_bytes;
  }
  elsif (length($$buffer) > $residual_size){
    my (
      $compr_method, 
      $max_chunk_size
    ) = 
    @{$self}{qw(compr_method max_chunk_size)}; 

    while(length($$buffer) >= $residual_size) {
      $$chunk .= substr($$buffer, 0, $residual_size); 

      $self->writechunk(); 

      $$buffer = substr($$buffer, $residual_size); $residual_size = $max_chunk_size;
    }
    $$chunk = $$buffer;
  }

  $self->{'residual_size'} = $residual_size; return $wr_bytes;
}

sub Close {
  my $self = shift;  

  my (
   $Tag_file, $chunk, 
   $at_chunk, $compr_method
  ) = 
  @{$self}{qw(Tag_file chunk at_chunk compr_method)};

  if ($$chunk ne '' && $at_chunk > 0) {
    $self->writechunk(); 
  }

  else {
    my $md5=md5_hex($$chunk); my ($origsize, $size) = $self->prepare_chunk($chunk); $Tag_file->BlobWrite($chunk); 

    $Tag_file->setAttributes(
      size => $size, md5 => $md5, origsize => $origsize, chunks => 0
    );
  }

  my (
    $size, $origsize
  ) = 
  $Tag_file->getAttributes(qw(size origsize)); 

  $Tag_file->Close(); %$self = (size => $size, origsize => $origsize);
}

sub new {
  my ($ClassName, $clip, $filename, %args) = @_; my $TopTag = $clip->GetTopTag();

  my ($mtime, $ctime, $compr_method) = @args{qw(mtime ctime compr_method)};

  $ctime ||= time(); $compr_method ||= 'deflate';

  my ($Tag_file, $chunk) = ($TopTag->Create_Child('File'), '');

  $Tag_file->setAttributes(
    filename => $filename, ctime => $ctime, mtime => $mtime, compr_method => $compr_method
  );

  my $max_chunk_size = 1024 * 1024 * 2.5;

  $chunk = ' ' x ($max_chunk_size); $chunk = ''; ### prealloc vm

  my $self = {
    Tag_file => $Tag_file, filename => $filename, 
    at_chunk => 0, atoffset => 0, compr_method => $compr_method, 
    chunk => \$chunk, max_chunk_size => $max_chunk_size, residual_size => $max_chunk_size, 
  };

  bless $self, $ClassName;
}

1;
