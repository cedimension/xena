
#------------------------------------------------------------
package XReport::ARCHIVE::Centera::FPTag;

use strict;

use EMC::Centera;
use Data::Dumper ();

#use Digest::MD5 qw(md5_hex);
use Digest::MD5::File qw( file_md5_hex );

#sub StringAttribute {
#	 return ( scalar(@_) > 2 ? EMC::Centera::FPTag::GetStringAttribute(@_) 
#                             : EMC::Centera::FPTag::SetStringAttribute(@_) );
#}
#
#sub LongAttribute {
#    return ( scalar(@_) > 2 ? EMC::Centera::FPTag::GetLongAttribute(@_) 
#                             : EMC::Centera::FPTag::SetLongAttribute(@_) );
#}
#
#my %AttrTypeTab = (
#  filename => \&StringAttribute,
#  ctime => \&LongAttribute,
#  mtime => \&LongAttribute,
#  md5 => \&StringAttribute,
#  size => \&LongAttribute,
#  origsize => \&LongAttribute,
#  chunks => \&LongAttribute,
#  seq => \&LongAttribute,
#  offset => \&LongAttribute,
#  origoffset => \&LongAttribute,
#  compr_method => \&StringAttribute,
#);
#

#our $LastError; *LastError = *EMC::Centera::FPpool::LastError;

use constant TT_STRING => 1;
use constant TT_LONG => 2;

my %AttrTypeTab = (
  filename => TT_STRING,
  ctime => TT_LONG,
  mtime => TT_LONG,
  md5 => TT_STRING,
  size => TT_LONG,
  origsize => TT_LONG,
  chunks => TT_LONG,
  seq => TT_LONG,
  offset => TT_LONG,
  origoffset => TT_LONG,
  compr_method => TT_STRING,
);

sub getAllAttributes {
}

sub getAttributes {
  my $self = shift; my $tag = $self->{'tag'}; my @valueList;

  my ($attr, $type, $value);

  for my $attr (@_) {
    $type = $AttrTypeTab{$attr}; $value = undef; 
#    die "TAG GETATTR $attr TYPE ????" unless ($attr eq 'TagName' || ($type && ($type == TT_STRING || $type == TT_LONG)));
#    my $getrtn = $tag->can( $attr eq 'Tagname' ? 'GetTagName'
#                          : $type == TT_STRING ? 'GetStringAttribute'
#                          : 'GetLongAttribute'
#                          );
    if ($type == TT_STRING) {
      eval { $value = $tag->GetStringAttribute($attr); };
      $@ = "";
    }
    elsif ($type == TT_LONG) {
      eval { $value = $tag->GetLongAttribute($attr); };
      $@ = "";
    }
    elsif ($attr eq 'TagName') {
      $value = $tag->GetTagName();
    }
    else {
      die "TAG GETATTR $attr TYPE ????";
    }
    push @valueList, $value;
  } 

  return wantarray ? @valueList : $valueList[0];
}

sub setAttributes {
  my $self = shift; my $tag = $self->{'tag'}; 

  my ($attr, $type, $value);

  while (@_) {
    ($attr, $value) = (shift, shift);
    next unless defined($value);
    $main::veryverbose && i::logit("centera setAttributes: $attr => <$value>");
    $type = $AttrTypeTab{$attr}; 
    if ($type == TT_STRING) {
      $tag->SetStringAttribute($attr, $value);
    }
    elsif ($type == TT_LONG) {
      $tag->SetLongAttribute($attr, $value);
    }
    else {
      die "TAG SETATTR $attr TYPE ????";
    }
  }
}

sub incrAttributes {
  my $self = shift; my $tag = $self->{'tag'}; 

  my ($attr, $type, $value);

  while (@_) {
    ($attr, $value) = (shift, shift); 
    $type = $AttrTypeTab{$attr}; 
    if ($type == TT_STRING) { 
      $tag->SetStringAttribute($attr, $value.$self->getAttributes($attr));
    }
    elsif ($type == TT_LONG) {
      $tag->SetLongAttribute($attr, $value+$self->getAttributes($attr));
    }
    else {
      die "TAG SETATTR $attr TYPE ????";
    }
  }
}

sub Create_Child {
  my ($self, $tagName) = @_; 
  my $tag = $self->{'tag'};

  return XReport::ARCHIVE::Centera::FPTag->new($tag->Create_Child($tagName));
}

sub GetFirstChild {
  my $self  = shift; 
  my $tag = $self->{'tag'};

  return XReport::ARCHIVE::Centera::FPTag->new($tag->GetFirstChild());
}

sub GetSibling {
  my $self  = shift; my $tag = $self->{'tag'};

  $tag = $tag->GetSibling();
  
  return XReport::ARCHIVE::Centera::FPTag->new($tag);
}

sub GetPrevSibling {
  my $self  = shift; my $tag = $self->{'tag'};

  $tag = $tag->GetPrevSibling();
  
  return XReport::ARCHIVE::Centera::FPTag->new($tag);
}

sub BlobWrite {
  my ($self, $blobref) = (shift, shift); my $tag = $self->{'tag'};

  my $bi = EMC::Centera::FPStream->CreateBufferForInput($$blobref)
    or
  die("Centera CreateBufferForInput error $EMC::Centera::LastErrorDescr");

  $tag->BlobWrite($bi, 0)
    or
  die("Centera BlobWrite error $EMC::Centera::LastErrorDescr"); $bi->Close();
}

sub BlobWritePartial {
  my ($self, $blobref, $seq_id) = (shift, shift, shift); 
  my $tag = $self->{'tag'}; 

  my $bi = EMC::Centera::FPStream->CreateBufferForInput($$blobref)
                   or die("Centera CreateBufferForInput error $EMC::Centera::LastErrorDescr"); 
  
  my $blob_size = length($$blobref);

  $main::veryverbose && i::logit("blobwrite_partial begin $seq_id $blob_size") if $seq_id % 10 == 1;
  $tag->BlobWritePartial($bi, 0, $seq_id)
                  or die("Centera BlobWritePartial error $EMC::Centera::LastErrorDescr"); $bi->Close();
  $main::veryverbose && i::logit("blobwrite_partial end $seq_id") if $seq_id % 10 == 1;
}


sub GetBlobSize {
  my $self = shift; 
  my $tag = $self->{'tag'}; 
  $tag->GetBlobSize();
}

sub BlobRead {
  my $self = shift; my $tag = $self->{'tag'}; my $blob = ' 'x$tag->GetBlobSize();

  my $bo = EMC::Centera::FPStream->CreateBufferForOutput($blob)
                          or die("Centera CreateBufferForOutput error $EMC::Centera::LastErrorDescr");

  $tag->BlobRead($bo, 0) or die("BlobRead Error $EMC::Centera::LastErrorDescr"); 
  $bo->Close(); 
  return \$blob;
}


sub BlobReadPartial {
  my ($self, $offset, $length) = @_; 
  my $tag = $self->{'tag'}; 
  my $blob = ' 'x$length;

  my $bo = EMC::Centera::FPStream->CreateBufferForOutput($blob)
                             or die("Centera CreateBufferForOutput error $EMC::Centera::LastErrorDescr");

  $tag->BlobReadPartial($bo, $offset, $length, 0) or die("BlobRead Error $EMC::Centera::LastErrorDescr"); 
  $bo->Close(); 
  return \$blob;
}

sub Close {
  my $self = shift; 
  my $tag = $self->{'tag'}; 
  $tag->Close() if $tag; 
  %$self = ();
}

sub Delete {
  my $self = shift; 
  my $tag = $self->{'tag'}; 
  $tag->Delete();
}

sub new {
  my ($ClassName, $tag) = @_; 
  return undef if !$tag; 
 
  bless {tag => $tag}, $ClassName;
}

sub DESTROY {
 #print "DESTROY ", __PACKAGE__, "\n";
  my $self = shift; $self->Close();
}

#------------------------------------------------------------
package XReport::ARCHIVE::Centera::FPClip;

use strict;

use Symbol;

use EMC::Centera;

#our $LastError; *LastError = *EMC::Centera::FPpool::LastError;


sub GetName {
  my $self = shift; $self->{'clip'}->GetName();
}


sub GetTotalSize {
  my $self = shift; $self->{'clip'}->GetTotalSize();
}

sub GetTopTag {
  my $self = shift; my $clip = $self->{'clip'}; my ($TopTag, $FirstChild);
 
  $TopTag = $clip->GetTopTag(); $FirstChild = $TopTag->GetFirstChild();
  
  if ($FirstChild->GetTagName() eq "identify") {
    $TopTag = $FirstChild->GetSibling()
  }
  
  return XReport::ARCHIVE::Centera::FPTag->new( $TopTag );
}

use File::stat ();
sub storeFile2tag {
  my $self = shift;
  my ($fpath, $tagname) = @_;
  return undef unless -e $fpath && -f $fpath;
  
  my @pthels = reverse split /[\/\\]/, $fpath;
  my $filename = join('/', reverse((reverse split /[\/\\]/, $fpath)[0..2]));
  my $sb = File::stat::stat($fpath);
  my $fileattrs = { filename => $filename
                  , compr_method => 'stored'
                  , ctime => $sb->ctime()
                  , mtime => $sb->mtime()
                  , origsize => $sb->size()
                  };
  $main::veryverbose && i::logit("Initializing centera stream to read $fpath"); 
  my $vstream = EMC::Centera::FPStream->CreateFileForInput($fpath, 'rb', 128*1024)
                       or die("Centera Stream CreateFileForInput error $EMC::Centera::LastErrorDescr"); 
                        
  #my $clip = $self->{'clip'};
  my $toptag = $self->GetTopTag();
  my $tag = $toptag->Create_Child($tagname);
  #my $tagfile = XReport::ARCHIVE::Centera::FPTag->new($tag);
  $main::veryverbose && i::logit("writing $fpath to centera TAG $tagname"); 
  $tag->{tag}->BlobWrite($vstream, 0) 
                       or die("Centera TAG $tagname BolbWrite error $EMC::Centera::LastErrorDescr");
  $vstream->Close()                      
             or die("Centera stream Close error $EMC::Centera::LastErrorDescr");
  $main::veryverbose && i::logit("setting TAG $tagname Attributes: ".join('::', %{$fileattrs})); 
  $fileattrs->{size}  = $tag->GetBlobSize();
  #2016-03-25
  #my $chunk = $tag->BlobRead(); 
  #$fileattrs->{md5}  = Digest::MD5::md5_hex($$chunk);
  $fileattrs->{md5}  = Digest::MD5::File::file_md5_hex( $fpath ); 
  #
  
  $tag->setAttributes( %{$fileattrs} );
  $tag->Close();
  $toptag->Close();

  if ( $main::veryverbose ) {
    i::logit("store file digging into tags");
    my $TopTag = $self->GetTopTag(); my $Tag_file = $TopTag->GetFirstChild();

    i::logit("satrting from first child - TAG H: ".DataDumper::Dumper($Tag_file));
    while($Tag_file) {
      i::logit("first child found - getting attributes");
      my (
       $filename, $ctime, $mtime, $md5, $chunks, $size, $origsize, $compr_method
      ) 
      = $Tag_file->getAttributes(qw(filename ctime mtime md5 chunks size origsize compr_method));

      i::logit( 
        "File filename=$filename ctime=$ctime mtime=$mtime md5=$md5 chunks=$chunks".
        " size=$size origsize=$origsize compr_method=$compr_method"
      );
      $Tag_file = $Tag_file->GetSibling();
    }
  }
  return $sb->size();
	
}

sub AddStream {
  my $self = shift; require XReport::ARCHIVE::Centera::File::OUTPUT;

  XReport::ARCHIVE::Centera::File::OUTPUT->new($self, @_);
}

sub AddBuffer {
  my ($self, $filename, $buffer) = @_; my ($clip, $mode) = @{$self}{qw(clip mode)};

  die "INVALID CLIP OPEN MODE ($mode)" if $mode ne 'OUTPUT';

  $clip->AddBuffer($filename, $buffer); 
}

sub AddFile {
  my ($self, %args) = (shift, @_); my $INPUT = gensym();
 
  my (  $filename, $origfilename ) =  @args{qw(filename origfilename)}; 
  delete @args{qw(filename origfilename)};

  open($INPUT, "<$origfilename")
                or die("AddFile OPEN INPUT ERROR \"$origfilename\" $!"); binmode($INPUT);

  require XReport::ARCHIVE::Centera::File::OUTPUT;
  my $os = XReport::ARCHIVE::Centera::File::OUTPUT->new( $self, $filename, %args );
  my $rec;

  while(!eof($INPUT)) {
    read($INPUT, $rec, 32768); $os->write($rec);
  }

  $os->Close(); close($INPUT);
}

sub ExistsFile { 
  my ($self, $filename) = @_; my $FileTab = $self->{'FileTab'}; 

  $filename = lc($filename); my $Tag_file;

  if (!($Tag_file = $FileTab->{$filename})) {
    if ( $Tag_file = $self->GetTopTag()->GetFirstChild() ) {
      while($Tag_file) {
        if ($filename eq lc($Tag_file->getAttributes('filename'))) { 
          $FileTab->{$filename} = $Tag_file;
          last;
        }
        $Tag_file = $Tag_file->GetSibling();
      }
    }
    return if !$Tag_file;
  } 

  return exists($FileTab->{$filename});
}

sub LocateFile { 
  my ($self, $filename) = @_; my $FileTab = $self->{'FileTab'}; 
#  i::logit(ref($self)." locating $filename ");
  $filename = lc($filename); my $Tag_file;

  if (!($Tag_file = $FileTab->{$filename})) {
    if ( $Tag_file = $self->GetTopTag()->GetFirstChild() ) {
      while($Tag_file) {
      	my $tfn = lc($Tag_file->getAttributes('filename'));
#      	i::logit(ref($self)." found $tfn - checking");
        if ($filename eq $tfn) { 
          $FileTab->{$filename} = $Tag_file;
          last;
        }
        $Tag_file = $Tag_file->GetSibling();
      }
    }
    return if !$Tag_file;
  } 

  require XReport::ARCHIVE::Centera::File::INPUT;

  XReport::ARCHIVE::Centera::File::INPUT->new($Tag_file);
}

sub FileSize {
  my ($self, $filename) = @_; 
  my $FileTab = $self->{'FileTab'}; 

  $filename = lc($filename); my $Tag_file;

  if (!($Tag_file = $FileTab->{$filename})) {
    if ( $Tag_file = $self->GetTopTag()->GetFirstChild() ) {
      while($Tag_file) {
        if ($filename eq lc($Tag_file->getAttributes('filename'))) { 
          $FileTab->{$filename} = $Tag_file;
          last;
        }
        $Tag_file = $Tag_file->GetSibling();
      }
    }
    return -1 if !$Tag_file;
  } 
 
  my ( $ctime, $mtime, $md5, $chunks, $size, $origsize, $compr_method ) 
                             = $Tag_file->getAttributes(qw( ctime mtime md5 chunks size origsize compr_method));
  return $origsize;
}

sub ExtractMember2FH {
    my ($self, $INPUT, $oFH) = @_;
    
    die "Centera ExtractMember2FH output is not a ref - dest: $oFH" unless ref($oFH);
    my ($isize, $osize) = ($INPUT->uncompressedSize(), 0);
    my $buff = ' ' x 131072;
    while (1) {
      $INPUT->read($buff, 131072); 
      last if $buff eq '';
      $osize += length($buff); 
      $oFH->write($buff);
    }

    die("FILE SIZE MISMATCH BETWEEN INPUT($isize) AND OUTPUT($osize)") unless $osize == $isize;
    return $osize;
}

sub HeaderFileContent {
    my ($self, $iFileName) = @_; 
	my ($INPUT, $OUTPUT);
    $INPUT = $self->LocateFile($iFileName)
         or die("FileContents $iFileName NOT Located"); 
    $INPUT->Open();
    my $FileContents;
    $OUTPUT = new IO::String($FileContents);
    $OUTPUT->binmode();
    my $osize = 0; 
    my $buff = ' ' x 512;
    while (1) {
      $INPUT->read($buff, 512); 
      last if $buff eq '';
      $osize += length($buff); 
      $OUTPUT->write($buff);
	  last;
    } 
	$INPUT->Close();
	$OUTPUT->close;
	return $FileContents;
}

use IO::File;
use IO::String;
sub ExtractFile {
  my ($self, $iFileName, $oFileName) = @_; my ($INPUT, $OUTPUT);
    $INPUT = $self->LocateFile($iFileName); 
  die("ExtractFile $iFileName NOT Located") if !$INPUT; 
    $INPUT->Open();
    $OUTPUT = IO::File->new(">". "$oFileName\.PENDING")
        or die("OPEN OUTPUT ERROR for \"$oFileName\.PENDING\" $!"); 
    $OUTPUT->binmode();
    my $osize = $self->ExtractMember2FH($INPUT, $OUTPUT);
    $INPUT->Close();
    $OUTPUT->close;

  rename("$oFileName\.PENDING", "$oFileName")
                  or die("RENAME ERROR for \"$oFileName\.PENDING\" $!"); 

  return $oFileName;
}

sub FileContents {
    my ($self, $iFileName) = @_; my ($INPUT, $OUTPUT);
    $INPUT = $self->LocateFile($iFileName)
         or die("FileContents $iFileName NOT Located"); 
    $INPUT->Open();
    my $FileContents;
    $OUTPUT = new IO::String($FileContents);
    $OUTPUT->binmode();
    my $osize = $self->ExtractMember2FH($INPUT, $OUTPUT);
  $INPUT->Close();
    $OUTPUT->close;
  return $FileContents;
}

sub ExtractTree {
  my ($self, $root, $dest) = @_; my $FileList = $self->FileList(); my $iFileName;

  for $iFileName (@$FileList) {
    $self->ExtractFile($iFileName, "$dest/$iFileName");
  }
}

sub FileList {
  my ($self, $mask) = @_; my $clip = $self->{'clip'}; return [] if !$clip->GetNumTags(); 
  my @FileList = ();

  my $TopTag = $self->GetTopTag(); my $Tag_file = $TopTag->GetFirstChild();

  while($Tag_file) {
    my $filename = $Tag_file->getAttributes(qw(filename));
    push @FileList, $filename if !$mask || $filename =~ /$mask/;
  }
  continue {    
    $Tag_file = $Tag_file->GetSibling();
  }

  return \@FileList;
}

sub FileNames {
	return FileList(@_);
}

sub ListFiles {
  my $self = shift; my $clip = $self->{'clip'}; my @FileList = ();
  $main::veryverbose && i::logit("centera listfiles for Clipid $self->{ClipId}");

  my $TopTag = $self->GetTopTag(); my $Tag_file = $TopTag->GetFirstChild();

  while($Tag_file) {
    my (
     $filename, $ctime, $mtime, $md5, $chunks, $size, $origsize, $compr_method
    ) 
    = $Tag_file->getAttributes(qw(filename ctime mtime md5 chunks size origsize compr_method));

    i::logit( 
      "File filename=$filename ctime=$ctime mtime=$mtime md5=$md5 chunks=$chunks".
      " size=$size origsize=$origsize compr_method=$compr_method"
    );

    if ($chunks > 0) {
      my $Tag_chunk = $Tag_file->GetFirstChild();

      while($Tag_chunk) {
        my (
          $tag, $seq, $md5, $offset, $size, $origsize
        ) = 
        $Tag_chunk->getAttributes(qw(TagName seq md5 offset size origsize));

        i::logit("ck tag=$tag seq=$seq md5=$md5 offset=$offset size=$size origsize=$origsize");
       
        $Tag_chunk = $Tag_chunk->GetSibling();
      }
    }
    $Tag_file = $Tag_file->GetSibling();
  }
}

sub FileTags {
  my $self = shift; my $clip = $self->{'clip'}; my @FileTags = ();

  my $TopTag = $self->GetTopTag(); my $Tag_file = $TopTag->GetFirstChild();

  while($Tag_file) {
    my (
     $filename, $ctime, $mtime, $md5,
     $chunks, $size, $origsize, $compr_method
    ) 
    = $Tag_file->getAttributes(qw(filename ctime mtime md5 chunks size origsize compr_method));

    if ($chunks > 0 and $origsize < 0) {
      my $Tag_chunk = $Tag_file->GetFirstChild(); my $origsize_chunks = 0;

      while($Tag_chunk) {
        my (
          $tag, $seq, $md5, $offset, $size, $origsize
        ) = 
        $Tag_chunk->getAttributes(qw(TagName seq md5 offset size origsize));

        $origsize_chunks += $origsize;
       
        $Tag_chunk = $Tag_chunk->GetSibling();
      }
      $origsize = $origsize_chunks;
    }

    push @FileTags, {
      filename => $filename, size => $size, origsize => $origsize, 
      chunks => $chunks, ctime => $ctime, mtime => $mtime, 
      md5 => $md5, compr_method => $compr_method, 
      Tag_file => $Tag_file, 
    };

    $Tag_file = $Tag_file->GetSibling();
  }

  return \@FileTags;
}

sub Write {
  my $self = shift; my ($clip, $profile) = @{$self}{qw(clip profile)};

  return $clip->Write() . (
    $profile ne "" ? "?profile=$profile" : ""
  );
}

sub Close {
  my $self = shift; 
  return if $self->{'closed'}; 
  $main::debug && i::logit(ref($self)." Closing as requested by ".join('::', (caller())[0,2]));
  if ( $self->{'FileTab'} && ref($self->{'FileTab'} eq 'HASH' ) ) {
  	my @tags = keys %{$self->{'FileTab'}};
  	while ( scalar(@tags) ) {
  		(delete $self->{'FileTab'}->{shift @tags})->Close();
  	}
  }

  my $clip = $self->{'clip'}; 
  $clip->Close() if $clip; 
  %$self = (closed => 1);
}

sub FillFileTab {
  my $self = shift; my $FileTab = $self->{'FileTab'}; 
  my ($Tag_file, $FileName);

  if ( $Tag_file = $self->GetTopTag()->GetFirstChild() ) {
    while($Tag_file) {
      $FileName = $Tag_file->getAttributes('filename');
      if (!exists $FileTab->{$FileName}) {
        $FileTab->{$FileName}=$Tag_file; 
      }
      $Tag_file = $Tag_file->GetSibling();
    }
  }
}

sub new {
  my ($ClassName, $clip, $filename, $mode, $profile) = @_;
  die "ref($clip) not a Centera Clip object " if (!ref($clip) || ref($clip) ne 'EMC::Centera::FPClip');
  return undef if !$clip;

  my $FileTab = {}; my $Tag_file;

  my $self = { 
    clip => $clip , filename => $filename, FileTab => $FileTab, 
    mode => $mode, profile => $profile, closed => 0
  };
 
  bless $self, $ClassName; 
  return $self;
}

sub DESTROY {
 #print "DESTROY ", __PACKAGE__, "\n";
  my $self = shift; 
  $main::debug && i::logit(ref($self)." DESTROY requested by ".join('::', (caller())[0,2]));
  $self->Close();
}

#------------------------------------------------------------
package XReport::ARCHIVE::Centera::FPPool;

use strict;

use EMC::Centera;
use XReport::Plugin ();

use constant FP_NORMAL_OPEN => 0;
use constant FP_LAZY_OPEN => 1;

#our $LastError; *LastError = *EMC::Centera::FPpool::LastError;

use Digest::SHA1 qw();
sub getCenteraPool {
	my $self = shift;
    my ($id, $cpl) = @_;
    my $id_sha1 = Digest::SHA1::sha1_hex($id);
	return undef if ( !$cpl && (
                      !(my $pools = $main::Application->{centera_pools})) );
    if (!$cpl) {
       return undef unless grep( /$id_sha1/, $pools);
       return undef unless ($cpl = $main::Application->{'centera_pool_'.$id_sha1});
    }
    else {
       if ( grep( /$id_sha1/, $pools) && (my $oldcpl = $main::Application->{'centera_pool_'.$id_sha1}) ) {
       	  $oldcpl->Close();
       	  $cpl = EMC::Centera::FPPool->Open($id);
       }
       elsif ( !grep( /$id_sha1/, $pools) ) {
       	  $main::Application->{'centera_pools'} = join("\t", (split("\t", $pools), $id_sha1));
       }
       $main::debug && i::logit("Caching Centera Pool requested by ".join('::', (caller(5))[0,2]), "$id");
       $main::Application->{'centera_pool_'.$id_sha1} = $cpl;
    }    
    return $cpl;
}

sub activePool {
    my $self = shift;
    my ($id, $cpl) = @_;
    my $id_sha1 = Digest::SHA1::sha1_hex($id);
    return undef if ( !$cpl && (
                      !(my $pools = $main::Application->{centera_pools})) );
    if (!$cpl) {
       return undef unless grep( /$id_sha1/, $pools);
       return undef unless ($cpl = $main::Application->{'centera_pool_'.$id_sha1});
    }
    else {
       if ( grep( /$id_sha1/, $pools) && (my $oldcpl = $main::Application->{'centera_pool_'.$id_sha1}) ) {
          $oldcpl->Close();
          $cpl = EMC::Centera::FPPool->Open($id);
       }
       elsif ( !grep( /$id_sha1/, $pools) ) {
          $main::Application->{'centera_pools'} = join("\t", (split("\t", $pools), $id_sha1));
       }
       $main::debug && i::logit("Caching Centera Pool requested by ".join('::', (caller(5))[0,2]), "$id");
       $main::Application->{'centera_pool_'.$id_sha1} = $cpl;
    }    
    return $cpl;
}

sub Open_Pool {
    my ($self, $profile) = @_; 
    my ($usexit, $cnodes) = @{$self}{qw(usexit cnodes)};

    my $pool_string = $cnodes . ($profile ? '?'.$self->PeaFileName($profile) : "");
    i::logit("Opening Centera Pool requested by ".join('::', (caller(4))[0,2]), "$pool_string");

   $self->{poolhandle} = EMC::Centera::FPPool->Open($pool_string) if (!$self->{poolhandle} || $self->{closed});
   if ( !$self->{poolhandle} ) {
    	my $diemsg = "UNABLE TO OPEN Centera Pool \"$pool_string\" LastError=$EMC::Centera::LastErrorDescr";
        i::logit($diemsg);
        die($diemsg); 
    }
    $self->{closed} = 0;
    $self->{poolhandle}->SetIntOption("collisionavoidance", 1); 
    
    return $self->{poolhandle};
  
}

sub Create_Clip {
  my ($self, $jr, $io) = @_; 
  my ($usexit, $pool, $clip, $tag, @l);
  my ( $JobReportId, $LocalFileName, $rdbmsdbname) = $jr->getValues(qw(JobReportId LocalFileName rdbmsdbname));
  my $largs_centera = $self->FileParameters($io, $jr);
  my ( $profile, $storage_class, $retention_period, $identify, $account ) = 
                             @{$largs_centera}{qw(profile storage_class retention_period identify account)};
  my %identify_values = ( Database => $rdbmsdbname
          , JobReportId => $JobReportId 
          , LocalFileName => $LocalFileName 
          , ($identify ? @$identify : ())
          );
 
  $identify = [ %identify_values ];
  $pool = $self->Open_Pool($profile);

  $clip = $pool->Clip_Create("XReportFile") or die ("Centera ClipCreate Error $EMC::Centera::LastErrorDescr");
  $tag = $clip->GetTopTag()->Create_Child("identify") or die ("Centera Create_Child error $EMC::Centera::LastErrorDescr");
  
  @l = @$identify;
  while(@l) {
    my ($attr, $value) = splice(@l,0,2);# $tag->SetStringAttribute($attr, $value);
    $main::veryverbose && i::logit("centera identify: $attr => <".( $value || '_EMPTY_' ).">");
    $tag->SetStringAttribute($attr, $value) if defined($value);
  } 
  if ($account) {
    $tag = $tag->Create_Child("account"); @l = @$account;
    while(@l) {
      my ($attr, $value) = splice(@l,0,2); $tag->SetStringAttribute($attr, $value);
    }
  }
  if ($storage_class) {
    my $rcl_ctx = $pool->GetRetentionClassContext();
#    my $num_classes = $rcl_ctx->GetNumClasses();
#    my $class = $rcl_ctx->GetFirstClass();
#    my $clnames = [ $class->GetName() ];
#    $num_classes--;
#    while ($num_classes) {
#    	$class = $rcl_ctx->GetNextClass();
#        push @{$clnames}, $class->GetName();
#        $num_classes--;
#    }
    
    my $rcl = $rcl_ctx->GetNamedClass( $storage_class );
    $clip->SetRetentionClass($rcl) 
     or  die ("Centera SetRetentionClass Error $EMC::Centera::LastErrorDescr"); $rcl->Close; $rcl_ctx->Close();
  }
  elsif ($retention_period) {
    die "qqqqqqq a\n";
  }
  else {
    die "qqqqqqq b\n";
  }
  $clip->GetTopTag()->Create_Child("archive") or die ("Centera Create_Child error $EMC::Centera::LastErrorDescr");
  $self->{clips} = [] unless exists $self->{clips};
  push @{$self->{clips}}, XReport::ARCHIVE::Centera::FPClip->new($clip, $LocalFileName, "OUTPUT", $profile);

  return $self->{clips}->[-1];
}

sub Open_Clip {
  my ($self, $LocalFileName, $clipId, $mode) = @_; 
  my ($pool, $clip, $profile);

  if ($clipId =~ /^(\w+)(?:\?profile=(\w+))?$/) {
    ($clipId, $profile) = ($1, $2);
  }
  $mode = uc($mode || 'INPUT');
  $main::veryverbose && i::logit("OpenClip Trying to open $LocalFileName ($clipId?profile=$profile) as $mode as requested by ".join('::', (caller())[0,2])); 

  die("INVALID OPEN MODE \"$mode\" DETECTED. ONLY INPUT OR OUTPUT ALLOWED") if ($mode ne 'INPUT' and $mode ne 'OUTPUT');
 
#  $pool = $self->Open_Pool($profile) or die ("Centera Open_Pool error for $profile - $EMC::Centera::LastErrorDescr");; 
#  $clip = $pool->Clip_Open($clipId, 1) or die ("Centera Open_Clip error for $clipId - $EMC::Centera::LastErrorDescr");
   $clip = $self->{poolhandle}->Clip_Open($clipId, 1) or die ("Centera Open_Clip error for $clipId - $EMC::Centera::LastErrorDescr");
  
  $self->{clips} = [] unless exists $self->{clips};
  push @{$self->{clips}}, XReport::ARCHIVE::Centera::FPClip->new($clip, $LocalFileName, $mode, $profile);

  return $self->{clips}->[-1];

}

sub Close {
  my $self = shift; 
  return if $self->{closed}; 
  i::logit(ref($self)." Closing as requested by ".join('::', (caller())[0,2]));
  map {$_->Close() if defined $_ } @{$self->{'clips'}} if (exists($self->{'clips'}) && scalar(@{$self->{'clips'}})); 
#  map {$_->Close()} values(%{$self->{'cpools'}}) if (exists($self->{'cpools'}) && scalar(keys(%{$self->{'cpools'}}))); 
  %$self = (closed => 1);
}

sub FileParameters {
	my $self = shift;
	if ( exists($self->{usexit}) && (my $exitrtn = $self->{usexit}->can($self->{profid}.'_FileParameters')) ) {
		return &{$exitrtn}($self->{usexit}, @_);
	}
	die "Storage class list not found in CFG\n" unless (exists($self->{StorageClassTable}) && ref($self->{StorageClassTable}) eq 'HASH' );
	my ($io, $jr) = @_;
	my ($retention_period, $identify) = $jr->getValues(qw(retention_period identify));
	my $storage_class= (grep { $self->{StorageClassTable}->{$_}->{HoldDays} == $retention_period } keys %{$self->{StorageClassTable}})[0];
	$storage_class = $self->{StorageClassDefault} unless $storage_class;
    die "Storage class list in CFG has not entry for Ret. Period = $retention_period\n" unless $storage_class;
    my ($keyn, $keyv) = split( /;/, $self->{acctprofkey}, 2 )if $self->{acctprofkey};
    my $largs_centera = {
    	profile => $self->{profid}
    	, identify => $identify
    	, ($self->{acctprofkey} ? (account => [ $keyn => $keyv, doc_class => "file" ]) : () )
    	, storage_class => $storage_class
    };
	return $largs_centera;
}

sub ObjectParameters {
    my $self = shift;
    if ( exists($self->{usexit}) && (my $exitrtn = $self->{usexit}->can($self->{profid}.'_ObjectParameters')) ) {
        return &{$exitrtn}($self->{usexit}, @_);
    }
    die "Storage class list not found in CFG\n" unless (exists($self->{StorageClassTable}) && ref($self->{StorageClassTable}) eq 'ARRAY' );
    my ($io, %largs_object) = @_;
    my ( $PoolId, $ObjectId, $ObjectRef, $ObjectType ) = @largs_object{qw(PoolId ObjectId ObjectRef ObjectType)};
    my $storage_class= (grep { $self->{StorageClassTable}->{$_}->{poolid} == $PoolId } keys %{$self->{StorageClassTable}})[0];
    die "Storage class list in CFG has not entry for PoolId = $PoolId\n" unless $storage_class;
    my $objprofile = $self->{StorageClassTable}->{$storage_class}->{profile};
    my ($keyn, $keyv) = split( /;/, $self->{acctprofkey}, 2 )if $self->{acctprofkey};

    my $largs_centera = {
         profile => $objprofile 
        , ($self->{acctprofkey} ? (account => [ $keyn => $keyv, doc_class => "file" ]) : () )
       , storage_class => $storage_class
       , retention_period => 0
       };

  return $largs_centera;
    
}

sub PeaFileName {
    my $self = shift;
    if ( exists($self->{usexit}) && (my $exitrtn = $self->{usexit}->can($self->{profid}.'_ObjectParameters')) ) {
        return &{$exitrtn}($self->{usexit}, @_);
    }
    my $peadir = $self->{peadir};
    die "Centera pea repository $peadir not found or not accessible\n" unless ( $peadir && -d $peadir );
    my $peafile = $self->{peafile};
    die "Centera pea file name not specified in cfg for profile $self->{profid}\n" unless $peadir;
    my $peafqfn = "$peadir/$peafile.pea";
    die "Centera pea file $peafqfn not found or not accessible\n" unless ( -e $peafqfn );
    return $peafqfn;
}

sub new {
  my ($class, $LocalPathId) = @_; 
  
  die "centera peafile repository (peahome) not specified in configuration or CFG not accessible\n" 
                                          unless $XReport::cfg->{centeras}->{peahome};
  my $lp = $XReport::cfg->{LocalPath}->{$LocalPathId};
  die "LocalPath ID $LocalPathId does not exists in configuration or CFG not accessible\n" unless $lp;
  my $profname = (split( /[\\\/]+/, $lp) )[1];
  my $profid = (grep(/^$profname$/i, keys %{$XReport::cfg->{centeras}->{profile}}))[0]; 
  $profid = $profname unless $profid;
  die "centera profile $profname does not exists in configuration or CFG not accessible\n" unless $profid; 
  my ($peadir, $peafile, $hostlist, $sclist, $acctprofkey, $scdefault) 
     = ($XReport::cfg->{centeras}->{peahome}, 
        @{$XReport::cfg->{centeras}->{profile}->{$profid}}{qw(peafile server StorageClass accountkeyprofile StorageClassDefault)});
          
  
  my $cnodes = join(',', map { $_->{hostaddr} } @{$hostlist} ) if ($hostlist && ref($hostlist) eq 'ARRAY');      
  my $self = {
    LocalPathId => $LocalPathId
    , profid => $profid
    , acctprofkey => $acctprofkey
    , cpools => {}
    , peadir => $XReport::cfg->{centeras}->{peahome}
    , StorageClassTable => $sclist
    , ($scdefault ? (StorageClassDefault => $scdefault) : ())
    , peafile => $peafile
    , closed => 1
  };

  my $plugin = XReport::Plugin::getPlugin("get.centera.parameters");
  if ( $plugin ) {
     $self->{usexit} = bless { centera_id => $LocalPathId
     	                     , profid => $profid
     	                     , peafile => $peafile
     	                     }, $plugin;
     if ( my $nodesexit = $self->{usexit}->can($profid.'_AccessNodes') ) {
     	$cnodes = &{$nodesexit}($self->{usexit}, $profid);
        $self->{usexit}->{access_nodes} = $cnodes;
     }
  }
  $self->{cnodes} = $cnodes;
  $main::debug && i::logit(__PACKAGE__.' initialized as requested by '.join('::', (caller())[0,2]).' - SELF: '.Data::Dumper::Dumper($self));
  
  my $poolreq = $cnodes.'?'.PeaFileName($self, $profid);
  my $id_sha1 = Digest::SHA1::sha1_hex($poolreq);
  my $pools_str = $main::Application->{centera_pools};
  my $oldps = $pools_str;
  my @pools;
  if ( !$pools_str ) {
     $main::debug && i::logit(__PACKAGE__.' setting openstrategy ');
     EMC::Centera::FPPool::SetGlobalOption("openstrategy", FP_LAZY_OPEN);
     $pools_str = $id_sha1;
  }
  elsif ($pools_str =~ /\b$id_sha1\b/ ) {
  	 my $mainself = $main::Application->{'centera_pool_'.$id_sha1};
     $self = $mainself if $mainself;
     $main::debug && i::logit(__PACKAGE__.' initializion retrieved from main as requested by '.join('::', (caller())[0,2]).' - SELF: '.Data::Dumper::Dumper($self))
          if $mainself;
  }
  else {
  	$pools_str = join("\t", (split(/\t/, $pools_str), $id_sha1)) 
  }

  my $poolhandle;
  if ( $self->{closed} ) {
     $poolhandle = EMC::Centera::FPPool->Open($poolreq);
     if ( !$poolhandle ) {
        my $diemsg = "UNABLE TO OPEN Centera Pool \"$poolreq\" LastError=$EMC::Centera::LastErrorDescr";
        i::logit($diemsg.' - '.Data::Dumper::Dumper($self));
        die($diemsg); 
     }
     $self->{poolhandle} = $poolhandle;
  }
  else {
  	  $poolhandle = $self->{poolhandle};
  	  if ( !$poolhandle || ref($poolhandle) ne 'EMC::Centera::FPPool' ) {
        my $diemsg = __PACKAGE__." contains an invalid PoolHandle ";;
        i::logit($diemsg.' - '.Data::Dumper::Dumper($self));
        die($diemsg); 
  	  }
  }
  $self->{closed} = 0;
  $poolhandle->SetIntOption("collisionavoidance", 1); 
  bless $self, $class;
 
  if ( !$oldps || $pools_str ne $oldps ) {
     $main::Application->{'centera_pool_'.$id_sha1} = $self;
     $main::debug && i::logit(ref($self)." caching $id_sha1 - ".Data::Dumper::Dumper($main::Application->{'centera_pool_'.$id_sha1}));
     $main::Application->{centera_pools} = $pools_str ;
  }
  return $self;
  return $main::Application->{'centera_pool_'.$id_sha1};  

#  my $cnodes = $usexit->AccessNodes();
#
#  my $self = {
#	LocalPathId => uc($LocalPathId), usexit => $usexit, cnodes => $cnodes, cpools => {}
#  };
#
#  bless $self, $class;
}

sub DESTROY {
 #print "DESTROY ", __PACKAGE__, "\n";
  my $self = shift; 
  $main::debug && i::logit(ref($self)." DESTROY requested by ".join('::', (caller())[0,2]));
  $self->Close();
}

1;

__END__

package U::Centera::Parameters;

use strict;

#use XReport::AUTOLOAD;
use Data::Dumper ();
use File::Basename ();

my $peafiles = File::Basename::dirname(__FILE__) . "/peafiles";

my %profiles_Xena = (
'Xena' => ['Multibanca', 'US', 'USXena'],
);

my %profiles_c2 = (
  '00400' => ['Zivnostenska Banka', 'E5', 'E5XREPORT'],
  '02008' => ['UniCredit Banca', 'C0/S0', 'S0XREPORT'],
  'ISAIN' => ['UniCredit Banca', 'C0/S0', 'S0XREPORT'],
  'EMOC0' => ['UniCredit Banca', 'C0/S0', 'S0XREPORT'],
  'CM020' => ['UniCredit Banca', 'C0/S0', 'S0XREPORT'],
  'CG020' => ['UniCredit Banca', 'C0/S0', 'S0XREPORT'],
  '02700' => ['UniCredit Bank - CZ', 'E5', 'E5XREPORT'],
  '03131' => ['UniCredit Banca Mobiliare', 'AG', 'AGXREPORT'],
  '03135' => ['UniCredito Italiano Holding', 'UI', 'UIXREPORT'],
  '03198' => ['UniCredit Banca per la Casa', 'SQ', 'SQXREPORT'],
  '03214' => ['UniCredit Xelion Banca', 'UT', 'UTXREPORT'],
  '03218' => ['Clarima', 'UR', 'URXREPORT'],
  '03223' => ['UniCredit Private Banking', 'U6', 'U6XREPORT'],
  '03226' => ['UniCredit Banca d\'Impresa', 'U3', 'U3XREPORT'],
  '03307' => ['S2 Banca S.p.A.', 'S2', 'S2XREPORT'],
  '06095' => ['Cassa di Risparmio di Bra', 'E0', 'E0XREPORT'],
  '06105' => ['Cassa di Risparmio di Carpi', 'RC', 'RCXREPORT'],
  '06170' => ['Cassa di Risparmio di Fossano', 'F0', 'F0XREPORT'],
  '06235' => ['Banca dell\'Umbria 1462', 'RU', 'RUXREPORT'],
  '06285' => ['Cassa di Risparmio di Rimini', 'S7', 'S7XREPORT'],
  '06295' => ['Cassa di Risparmio di Saluzzo', 'G0', 'G0XREPORT'],
  '06305' => ['Cassa di Risparmio di Savigliano', 'H0', 'H0XREPORT'],
  '10639' => ['UGC Banca', 'GC', 'GCXREPORT'],
  '10639' => ['Medio Venezie', 'S1', 'GCXREPORT'],
  '16369' => ['Pioneer Investments', 'A2', 'A2XREPORT'],
  '80415' => ['Clarima Germania FBE', 'CD', 'CDXREPORT'],
  '99996' => ['Cartolarizzazioni Banca per la Casa', 'CQ', 'CQXREPORT'],
  'EMOCQ' => ['Cartolarizzazioni Banca per la Casa', 'CQ', 'CQXREPORT'],
  '99998' => ['Cartolarizzazioni Banca per la Casa', 'CE', 'CEXREPORT'],
  'EMOCE' => ['Cartolarizzazioni Banca per la Casa', 'CE', 'S0XREPORT'],
  'EMOBW' => ['Cartolarizzazioni Banca per la Casa', 'BW', 'S0XREPORT'],
  '06320' => ['Cassa di Risparmio di Torino', 'A0', 'A0XREPORT'],
  '05512' => ['Banca Popolare di Cremona', 'S5', 'S5XREPORT'],
  '03161' => ['TradingLab', 'TL', 'TLXREPORT'],
  '05437' => ['Bipop Carire', 'BZ', 'BZXREPORT'],
  '01020' => ['Banco di Sicilia', 'BD', 'BDXREPORT'],
  '03002' => ['Banca di Roma', 'BR', 'BRXREPORT'],
  'XXXXX' => ['MultiBanca', 'X1', 'X1XREPORT'],
);

my %pools = (
  100001 => ['03198', 'NPU'], #UnicreditNPA
  100101 => ['03198', 'NPU'], #UnicreditNPU
  100201 => ['03198', 'NPU'], #UnicreditAdalya
  200001 => ['02008', 'DOCCLI50'], #XFORM
  400001 => ['03214', 'SAX'], #XelionSax
  400101 => ['03223', 'SAD6'], #SAD6
  500101 => ['02008', 'ARNXX'], #ARNCM
  500201 => ['80415', 'ARNXX'], #ARNCD
  500103 => ['02008', 'ARNXX'], #ARNCMFAXP
  500203 => ['02008', 'ARNXX'], #ARNCMFAXC  
  500104 => ['03135', 'GSG'], #GSG 
  500204 => ['03135', 'GSG'], #GSGCOLL
  600001 => ['XXXXX', 'GAR'], #UnicreditGAR
  600002 => ['XXXXX', 'GAR'], #UnicreditGARLET
  700001 => ['03226', 'FDBON'], #UBIFdBon
  700002 => ['03214', 'FDBON'], #XelionFdBon
  700003 => ['03307', 'FDBON'], #2SBancaFdBon
  700004 => ['02008', 'FDBON'], #RetailFdBon
  700005 => ['05437', 'FDBON'], #BZFdBon
  700006 => ['03002', 'FDBON'], #BRFdBon
  700007 => ['01020', 'FDBON'], #BDFdBon
  700008 => ['02008', 'GSG'], #BIN
  700009 => ['02008', 'GSG'], #BancaAss
  999991 => ['03002', 'FDBON'], #BRFdBonTest
);

my $lips = join(", ", map {"uscasl00$_.intranet.unicredit.it"} (1..4), map {"uscasl30$_.intranet.unicredit.it"} (1..4));

my %centeras = (
  Xena => [join(", ", map {"uscasl30$_.intranet.unicredit.it"} (1..4)), \%profiles_Xena],
  c2 => ["10.182.32.117, 10.182.32.118, 10.182.32.119, 10.182.32.120", \%profiles_c2],
);

sub c2_AccessNodes {
  my $self = shift; return $centeras{'c2'}->[0];
}

sub Xena_AccessNodes {
  my $self = shift; return $centeras{'Xena'}->[0];
  my $self = shift; 
  return join(", ", map {"uscasl00$_.intranet.unicredit.it"} (1..4), map {"uscasl30$_.intranet.unicredit.it"} (1..4));
}

sub c2_ProfileFileName {
  my ($self, $profile) = @_; my $profiles_table = $centeras{'c2'}->[1];

  if (!exists($profiles_table->{$profile})) {
    my $msg = "profilo centera $profile non in tabella";
    i::logit($msg);
    die $msg, "\n";
  }
  $profile = $profiles_table->{$profile}->[2]; return "$peafiles/ALL_${profile}.pea";
}

sub Xena_ProfileFileName {
  my ($self, $profile) = @_; my $profiles_table = $centeras{'Xena'}->[1];

  if (!exists($profiles_table->{$profile})) {
    my $msg = "profilo centera $profile non in tabella";
    i::logit($msg);
    die $msg, "\n";
  }
  $profile = $profiles_table->{'Xena'}->[2]; return "$peafiles/ALL_${profile}.pea";
}

our %Xena_StorageClasses = (
    540 => 'USXena540d', 3650 => 'USXena10', 7300 => 'USXena20', 18250 => 'USXena50',
  );

sub Xena_FileParameters {
  my ($self, $io, $jr) = @_; 
  my ( $retention_period, $identify ) = $jr->getValues(qw(retention_period identify));

  my $storage_class = $Xena_StorageClasses{$retention_period} 
   or do { my $msg = "Xena retention_period $retention_period not in defined table."; 
           i::logit($msg); 
           die $msg, "\n"; 
      };
  
  my $largs_account = [ banca => 'Xena', doc_class => "file" ];

  my $largs_centera = { 
    profile => 'Xena', 
    identify => $identify,
    account => $largs_account,
    storage_class => $storage_class, 
  };
  $main::veryverbose && i::logit("FileParameters: ", Data::Dumper::Dumper($largs_centera));
  return $largs_centera;
}

sub c2_FileParameters {
  my ($self, $io, $jr) = @_; 
  my $profiles_table = $self->{'profiles_table'}; 

  my ( $JobReportName, $JobReportId, $LocalFileName ) = $jr->getValues(qw(JobReportName JobReportId LocalFileName)); 
  my $banca = substr($JobReportName, 1,5);
  if (exists($profiles_table->{$banca})) {
  }
  elsif ($JobReportName =~ /^..00EMU6/) {
    $banca = '03223';
  }
  elsif ($JobReportName =~ /^..00EMC0/) {
    $banca = '02008';
  }
  elsif ($JobReportName eq "RENDPIONEER") {
    $banca = '16369';
  }
  elsif ($JobReportName eq "LC00EMTO0001" ) {
    my $dbr = dbExecute("SELECT * from u0xrindex.dbo.tbl_IDX_TORINO_CONTI_CORRENTI where JobReportId=$JobReportId");
    $banca = substr("00000".$dbr->getValues('ABI'),-5);
    $dbr->Close();
  }
  elsif ($JobReportName eq "LC00EMTO0002" ) {
    my $dbr = dbExecute("SELECT * from u0xrindex.dbo.tbl_IDX_TORINO_SCALARI where JobReportId=$JobReportId");
    $banca = substr("00000".$dbr->getValues('ABI'),-5);
    $dbr->Close();
  }
  elsif ($JobReportName =~ /^..00EMBL/) {
    $banca = '02008';
  }
  elsif ($JobReportName =~ /^USXena/) {
    return $self->XenaParameters($io, $jr);
  }
  else {
    my $msg = "banca(profilo) $banca non in tabella per $JobReportName";
    return $self->XenaParameters($io, $jr);
    i::logit($msg); die $msg, "\n";
  }
  
  my $profile = $banca;
  my $storage_class = "DOCCLI";
  my $retention_period = 0;
  my $largs_account = [ banca => $banca, doc_class => "file" ];
  
  my $largs_centera = {
    profile => $profile, 
    account => $largs_account,
    storage_class => $storage_class, 
    retention_period => $retention_period, 
  };

  $main::veryverbose && i::logit("FileParameters: ", Data::Dumper::Dumper($largs_centera));
  return $largs_centera;
}

sub c2_ObjectParameters {
  my ($self, $io, %largs_object) = @_; 

  my ( $PoolId, $ObjectId, $ObjectRef, $ObjectType ) = @largs_object{qw(PoolId ObjectId ObjectRef ObjectType)};

  die("POOLID $PoolId NOT CONFIGURED !!!") if !exists($pools{$PoolId});

  my ($profile, $storage_class) = @{$pools{$PoolId}};

  my $retention_period = 0;
  my $largs_account = [ banca => $profile, doc_class => "image" ];

  my $largs_centera = {
    profile => $profile, 
    account => $largs_account,
    storage_class => $storage_class, 
    retention_period => $retention_period, 
  };

  return $largs_centera;
}

sub new {
  $main::veryverbose && i::logit( "new Parameters called by ", join('::', (caller())[0,2]), " args: ", join('::', @_));
  my ($class, $centera_id) = @_; $centera_id = lc($centera_id);

  my ($access_nodes, $profiles_table) = @{$centeras{$centera_id}};

  my $self = { 
    centera_id => $centera_id, 
    access_nodes => $access_nodes, 
    profiles_table => $profiles_table 
  };
  
  bless $self, $class;
}

__PACKAGE__;
