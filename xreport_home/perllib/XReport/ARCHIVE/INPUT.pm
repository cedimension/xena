
package XReport::ARCHIVE::INPUT;

use strict;

use File::Basename;
use File::Path;
use Symbol;

use XReport::DBUtil;

our $EBCDIC_GERMAN273_To_ISO8859_1 = 
#   0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
'\x00\x01\x02\x03\x9C\x09\x86\x7F\x97\x8D\x8E\x0B\x0C\x0D\x0E\x0F'.  #0
'\x10\x11\x12\x13\x9D\x85\x08\x87\x18\x19\x92\x8F\x1C\x1D\x1E\x1F'.  #1
'\x80\x81\x82\x83\x84\x0A\x17\x1B\x88\x89\x8A\x8B\x8C\x05\x06\x07'.  #2
'\x90\x91\x16\x93\x94\x95\x96\x04\x98\x99\x9A\x9B\x14\x15\x9E\x1A'.  #3
'\x20\xA0\xE2\x7B\xE0\xE1\xE3\xE5\xE7\xF1\xC4\x2E\x3C\x28\x2B\x21'.  #4
'\x26\xE9\xEA\xEB\xE8\xED\xEE\xEF\xEC\x7E\xDC\x24\x2A\x29\x3B\x5E'.  #5
'\x2D\x2F\xC2\x5B\xC0\xC1\xC3\xC5\xC7\xD1\xF6\x2C\x25\x5F\x3E\x3F'.  #6
'\xF8\xC9\xCA\xCB\xC8\xCD\xCE\xCF\xCC\x60\x3A\x23\xA7\x27\x3D\x22'.  #7
'\xD8\x61\x62\x63\x64\x65\x66\x67\x68\x69\xAB\xBB\xF0\xFD\xFE\xB1'.  #8
'\xB0\x6A\x6B\x6C\x6D\x6E\x6F\x70\x71\x72\xAA\xBA\xE6\xB8\xC6\xA4'.  #9
'\xB5\xDF\x73\x74\x75\x76\x77\x78\x79\x7A\xA1\xBF\xD0\xDD\xDE\xAE'.  #A
'\xA2\xA3\xA5\xB7\xA9\x40\xB6\xBC\xBD\xBE\xAC\x7C\xAF\xA8\xB4\xD7'.  #B
'\xE4\x41\x42\x43\x44\x45\x46\x47\x48\x49\xAD\xF4\xA6\xF2\xF3\xF5'.  #C
'\xFC\x4A\x4B\x4C\x4D\x4E\x4F\x50\x51\x52\xB9\xFB\x7D\xF9\xFA\xFF'.  #D
'\xD6\xF7\x53\x54\x55\x56\x57\x58\x59\x5A\xB2\xD4\x5C\xD2\xD3\xD5'.  #E
'\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\xB3\xDB\x5D\xD9\xDA\x9F'  #F
;

sub get_INPUT_STREAM_TAR {
  my ($self, $jr) = (shift, shift);
  my $ClassName = ref($self);
  my @arglist = qw(LocalPathId_IN LocalPath LocalFileName TarFileNumberIN TarRecNumberIN INFile_Url INFile_TotalSize ClipId);
  my $args = { (map { $_ => $self->{$_} } @arglist), @_ };
  my ($amtype, $FileName) = split /\:\/\//, $args->{LocalPath}, 2;
  $FileName .= "/$args->{LocalFileName}";
  
  $args->{random_access} = 1 unless exists($args->{random_access});
  if ( $amtype eq 'file' && -e $FileName && !$args->{random_access} ) {
        $main::veryverbose and i::warnit("$ClassName initializing Raw Access");
        require XReport::ARCHIVE::File;
        return XReport::ARCHIVE::File->Open("<$FileName");
  }
  require XReport::Storage::IN;
  return XReport::Storage::IN->Open(%{$args});

  #my ($FileName, $INPUT); 
  my $INPUT; 
  my ( $LocalPathId_IN, $LocalPath, $LocalFileName, $TarFileNumberIN, $TarRecNumberIN,  $INFile_Url, $INFile_TotalSize ) = 
  @{$self}{qw(LocalPathId_IN LocalPath LocalFileName TarFileNumberIN TarRecNumberIN INFile_Url INFile_TotalSize)};
  my $ClipId = @{$self}{qw(ClipId)};
  
  $main::veryverbose and i::warnit("Initializing TAR handler for $self->{LocalFileName} (PATH: $LocalPath) - args: ", join('::', @_));
  if ($LocalPath =~ /^\s*file:\/\/(.*)$/i) {
    $FileName = "$1/$LocalFileName"; 
    if ( -e $FileName && exists($args->{random_access}) && $args->{random_access}) {
       $main::veryverbose and i::warnit("$ClassName initializing Direct Access");
       require XReport::Storage::IN;
       $INPUT = XReport::Storage::IN->Open($FileName, $LocalPathId_IN, $LocalPath, $LocalFileName, $ClipId )
    		                                || i::logit("Open Failed for \"$LocalFileName\" in $LocalPath - $? - $!");
    }
    elsif ( -e $FileName ) {
        $main::veryverbose and i::warnit("$ClassName initializing Raw Access");
    	require XReport::ARCHIVE::File;
    	$INPUT = XReport::ARCHIVE::File->Open("<$FileName");
    }
    else {
    	i::logit("File \"$FileName\" does not exists or is not accessible");
    }
  }
  elsif ($LocalPath =~ /^\s*centera:\/\/(.*)$/i) {
  	die "Centera access not yet implemented";
    my $ClipId = $self->{ClipId} or die("ClipId MISSING at get_INPUT_STREAM"); 
    require XReport::ARCHIVE::Centera;

    my $pool = $self->{pool} ||= XReport::ARCHIVE::Centera::FPPool->new($LocalPathId_IN);
    my $clip = $self->{clip} ||= $self->{pool}->Open_Clip($LocalFileName, $ClipId);

    my $line_iexec = $jr->getValues('line_iexec');

    $INPUT = (!$line_iexec) ? $clip->LocateFile($LocalFileName) : $line_iexec->new($clip);
  }
  else {
    die "LOCAL_PATH TYPE \"$LocalPath\" NOT MANAGED/IDENTIFIED.";
  }

  $main::veryverbose and i::warnit("TAR will be handled by ".ref($INPUT));
  return $INPUT;
}

sub get_INPUT_STREAM_GZ {
  my ($self, $jr, $stream) = @_; my ($FileName, $INPUT); 

  my (
    $LocalPathId_IN, $LocalPath, $LocalFileName, $TarFileNumberIN, $TarRecNumberIN, 
    $INFile_Url, $INFile_TotalSize,
  ) = 
  @{$self}{qw(LocalPathId_IN LocalPath LocalFileName TarFileNumberIN TarRecNumberIN INFile_Url INFile_TotalSize)};
  i::logit(ref($self)." locating input stream handler for $LocalPath($LocalPathId_IN");
  if ((defined $INFile_Url ) && ($INFile_Url =~ /^tar/)) {
    require XReport::TAR3; $INPUT = XReport::TAR->OpenInFileUrl($INFile_Url, $LocalFileName, $INFile_TotalSize);
  }
  elsif ($LocalPath =~ /^\s*file:\/\/(.*)$/i) {
    $FileName = "$1/$LocalFileName"; require XReport::INPUT;
 
    if ( -e $FileName ) {
      $INPUT = ( $FileName =~ /\.gz$/ ) 
       ? XReport::INPUT::IZlib->new($FileName)
       : XReport::INPUT::IPlain->new($FileName)
      ;
    }
    elsif ( $TarFileNumberIN != 0 ) {
      require XReport::TAR; my $tar = XReport::ARCHIVE::TAR->Open("id:$TarFileNumberIN");
	  $INPUT = $tar->getMember("block:$TarRecNumberIN");
	  $self->{tar} = $tar;
    }
    else {
      die "INPUT FILE \"$FileName\" NOT AVAILABLE !!";
    }
  }
  elsif ($LocalPath =~ /^\s*centera:\/\/(.*)$/i) {
    my $ClipId = $self->{ClipId} or die("ClipId MISSING at get_INPUT_STREAM"); require XReport::ARCHIVE::Centera;
    (my $lfn = $LocalFileName) =~ s/^\s*IN\///i;
    my $pool = $self->{pool} ||= XReport::ARCHIVE::Centera::FPPool->new($LocalPathId_IN);
    my $clip = $self->{clip} ||= $self->{pool}->Open_Clip($lfn, $ClipId);

    my $line_iexec = $jr->getValues('line_iexec');
    i::logit("Clip $ClipId opened at $LocalPath ($LocalPathId_IN) - line_iexec: $line_iexec - LocalFileName: $lfn");
    $INPUT = (!$line_iexec) ? $clip->LocateFile($lfn) : $line_iexec->new($clip);
  }
  else {
    die "LOCAL_PATH TYPE \"$LocalPath\" NOT MANAGED/IDENTIFIED.";
  }

  return $INPUT;
}

sub get_INPUT_STREAM_ZIP {
  my ($self, $jr, $stream) = @_; my ($SrvName, $workdir) = c::getValues(qw(SrvName workdir)); 
  
  my $localres = "$workdir/$SrvName/localres"; my $OUTPUT = gensym();
  
  my $iar = $self->get_INPUT_ARCHIVE(); $iar->ExtractTree('', "$localres/"); $iar->Close(); 
  
  my @FileNames = glob("$localres/*"); my $FileName = "$localres/.listFiles.INDEX"; 

  open($OUTPUT, ">$FileName") or die("OPEN OUTPUT ERROR For: \"$FileName\" $!"); 
   print $OUTPUT join("\n", ".listFiles.INDEX", @FileNames); 
  close($OUTPUT);
    
  require XREPORT::INPUT; my $INPUT = XReport::INPUT::IPlain->new($FileName); return $INPUT;
}

sub get_INPUT_STREAM {
  my $self = shift; 
  $main::veryverbose and i::warnit(ref($self)."::get_INPUT_STREAM called by ".join('::', (caller())[0,2]));
  return ( $self->{'LocalFileName'} =~ /\.gz$/i  ? $self->get_INPUT_STREAM_GZ(@_) 
         : $self->{'LocalFileName'} =~ /\.zip$/i ? $self->get_INPUT_STREAM_ZIP(@_)
         : $self->{'LocalFileName'} =~ /\.tar$/i ? $self->get_INPUT_STREAM_TAR(@_)
         : $self->get_INPUT_STREAM_ZIP(@_)
         );
}

sub get_INPUT_ARCHIVE {
  my $self = shift;
  my $pool_ref = shift;
  my ($LocalPathId_IN, $LocalPath, $LocalFileName) = @{$self}{qw(LocalPathId_IN LocalPath LocalFileName)};
  my ($lx, $iar);
  $main::veryverbose and i::warnit("Locating handler for $LocalFileName into $LocalPathId_IN");

  if (($lx) = ($LocalPath =~ /^\s*file:\/\/(.*)$/i) ) {
    die "ZIP WHAT $LocalFileName ???\n" if $LocalFileName !~ /\.zip$/i; 
    require XReport::ARCHIVE::Zip; 
    
    $iar = XReport::ARCHIVE::Zip->Open($lx, "$LocalFileName"); 
  }
  elsif ($LocalPath =~ /^\s*centera:\/\/(.*)$/i) {
    my ($LocalPathId_IN, $JobReportId) = (@{$self}{qw(LocalPathId_IN JobReportId)}); 
    my $dbr = dbExecute("
      SELECT INFILE_ClipId from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId
    ");
    my $ClipId = $dbr->GetFieldsValues('INFILE_ClipId'); $dbr->Close(); 
	#my $pool;  

    require XReport::ARCHIVE::Centera; 

    #$pool = XReport::ARCHIVE::Centera::FPPool->new( $LocalPathId_IN );
    #$iar = $pool->Open_Clip($LocalFileName, $ClipId);
    $$pool_ref = XReport::ARCHIVE::Centera::FPPool->new( $LocalPathId_IN );
    $iar = $$pool_ref->Open_Clip($LocalFileName, $ClipId);
  }
  else {
    die "LOCAL_PATH TYPE \"$LocalPath\" NOT MANAGED/IDENTIFIED.";
  }
  
  $main::veryverbose and i::warnit("INPUT local file is $LocalFileName handled by ".ref($iar));
  return $iar;
}

use XReport::BTree::INPUT;
use Encode qw(from_to);
require MIME::Base64;
=h2 Open

 parameters: JobReportHandle (new or reference to existing)
 args supported
 random_access: to use seek to position on the file
 outxlate: to specify input_code_page:output_code_page translation
           eg: CP1407:CP850 (from ebcdic to ascii)

=cut

sub Open {
  my ($ClassName, $jr, %args) = @_; my ($self, $LocalPath, $INFile_Url, $INFile_TotalSize, $ClipId);
  $main::veryverbose and i::warnit("OPEN for $ClassName called by ".join('::', (caller())[0,2])." jr: ".ref($jr));
  do {
    require XReport::ARCHIVE::JobREPORT; 
    $jr = XReport::ARCHIVE::JobREPORT->Open($jr)
  } 
  if !ref($jr);

  my (
    $LocalPathId_IN, $LocalFileName, 
    $TarFileNumberIN, $TarRecNumberIN, $JobReportId
	,$JobReportName
  ) = 
  $jr->getValues(qw(LocalPathId_IN LocalFileName TarFileNumberIN TarRecNumberIN JobReportId JobReportName));
  
  my ($HasCc, $ReportFormat);
  eval { ($HasCc , $ReportFormat) =   $jr->getValues(qw(HasCc ReportFormat)); };

  $LocalPath = c::getValues('LocalPath')->{$LocalPathId_IN ||= "L1"};

  $LocalFileName = "IN/".$jr->getFileName('INPUT');

  my $dbr = dbExecute("SELECT * from tbl_JobReportsUrls where JobReportId=$JobReportId");
  if (!$dbr->eof()) {
    ($INFile_Url, $INFile_TotalSize) = $dbr->getValues(qw(INFile_Url INFile_TotalSize));
  }
  $dbr->Close();

  if ($LocalPath =~ /^\s*centera:\/\/(.*)$/i) {
    my $dbr = dbExecute("
      SELECT INFILE_ClipId from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId
    ");
    $ClipId = $dbr->GetFieldsValues('INFILE_ClipId'); $dbr->Close();
  }
  
  $self = {
    JobReportId => $JobReportId, archiver => undef, INFile_Url => $INFile_Url, 
    INFile_TotalSize => $INFile_TotalSize, ClipId => $ClipId, 
    LocalPathId_IN => $LocalPathId_IN, LocalPath => $LocalPath, LocalFileName => $LocalFileName
	,JobReportName => $JobReportName, HasCc => $HasCc, ReportFormat => $ReportFormat
  };

  $self->{archiver} = $self if $args{wrapper};
  bless $self, $ClassName;
  
  if ( $args{random_access} && $LocalFileName =~ /\.gz.tar$/i ) {
    for ( qw(page block) ) { 
    	$self->{$_.'_index'} = XReport::BTree::INPUT->new($JobReportId, 'input_'.$_.'s'); 
    }
    $self->{blocks_input} = $self->get_INPUT_STREAM_TAR($jr, random_access => 1);
    $main::veryverbose && i::warnit(__PACKAGE__.'->Open "recexit" parm specified but not a code reference'.$args{recexit}) 
                      if ( exists($args{recexit}) && ref($args{recexit}) ne 'CODE');
    $args{recexit} = sub { return pack('n/a*', $_[1]); } unless ( exists($args{recexit}) && ref($args{recexit}) eq 'CODE') ;
    $self->{recexit} = $args{recexit};
    
    if (exists($args{outxlate})) {
    	@{$self}{qw(incodep outcodep)} = split /:/, $args{outxlate};
        $self->{pagexlator} = 
              sub { my $self = shift; 
				  my $rec = $_[0]; 
			  #$rec =~ s/\x5A/\x51/g if (($self->{incodep} =~ /^(cp37|cp1047|posix-bc)$/i) and (exists($args{change5A})) and $args{change5A}); #for the management of the char 'é'
				  #management of German/Austria code page(not managed by Encode perl module)
				  if (($self->{incodep} =~ /^(cp37|cp273|cp1141)$/i) and ($self->{outcodep} =~ /^(latin1|ISO8859-1)$/i) and ($XReport::cfg->{CODEPAGE}) and ( $XReport::cfg->{CODEPAGE} =~ /^GERMAN273$/i)){
					my $translator = Convert::EBCDIC->new($EBCDIC_GERMAN273_To_ISO8859_1); #GERMAN273
					return $translator->toascii($rec)."\n";
				  } 
				  else
				  {
					$rec =~ s/\x5A/\x51/g if (($self->{incodep} =~ /^(cp37|cp1047|posix-bc)$/i) and (exists($args{change5A})) and $args{change5A}); #for the management of the char 'é'
				  }
				  from_to($rec, $self->{incodep}, $self->{outcodep}); 
				  return $rec."\n"; 
			  };
    }
    else {
    	$self->{pagexlator} = sub { return $_[1]};
    }	
	
#    use Data::Dumper;
#    warn "blocks_input: ", Dumper($self->{blocks_input}), "\n";
    $self->{random_access} = 1;
  }

  return $self;

}

sub extract_pages {
  my ($self, $from_page, $for_pages) = (shift, shift, shift);
  my $args = { @_ };
  $main::veryverbose and i::warnit(ref($self)."::extract_pages FP: $from_page XP: $for_pages called by ".join('::', (caller())[0,2]));
  die "Unable to extract pages without random access\n" 
                   unless (exists($self->{random_access}) && $self->{random_access});
  
  my $page_iterator = $self->{page_index}->get_iterator(0, $from_page);
  if ( !$page_iterator ) {
         i::warnit("INPUT extract pages unable to find btree element for page $from_page");
         return undef;
      }
  my ( $entry_page, $from_line, $from_offset, $for_lines ) = $page_iterator->get_next_entry();
  if ( !defined($from_line) ) {
         i::warnit("INPUT extract pages unable to retrieve btree infos for page $from_page");
         return undef;
      }
   else { i::logit(ref($self)."::extract_pages found entrypage: $entry_page fromline: $from_line from_offset: $from_offset for_lines: $for_lines"); }
#  else { i::logit(ref($self)."::extract_pages found entrypage: $entry_page fromline: $from_line"); }
      
  my $inputIOH = $self->{blocks_input}->bsearchLine($from_offset,0);
  if (!$inputIOH ) {
     i::warnit("INPUT extract page unable to find page $from_page starting at line $from_line");
     return undef;
  }

  die "$entry_page != $from_page\n" if $entry_page != $from_page;
#  my ($outlist_offset, $input_line, $block_offset) = $self->{block_index}->get_entry(0, $from_offset);
  
#  $self->{blocks_input}->seek($block_offset, 0);
#  warn "extract data buff FP: $from_page FO: $from_offset BO: $block_offset OUTIO: $outlist_offset\n";
#  warn "extract data buff IL: $input_line INPIO: $inplist_offset FL: $from_line XL: $for_lines\n";
  
#  my ($page_buff, $rec, $lrec); 
  require MIME::Base64;  
  my $is_the_first_line = 1 if ($args->{changeFirstASAchar});
  my ($totpages, $totlines) = (0, 0);
  while($for_pages > 0) {
	my $page_buff;
	 
    $main::veryverbose && i::warnit(ref($self)."::extract_pages FP: $from_page XP: $for_pages"
            . " FL: $from_line XL: $for_lines TOTL: $totlines TOTP: $totpages");
    for ($page_buff = ''; $for_lines; $for_lines-- ) {
      $inputIOH->read(my $buff,2);
      my $lrec = unpack("n", $buff); 
      $inputIOH->read(my $rec, $lrec); 
	  
	  if($is_the_first_line)
	  {
		$is_the_first_line=0;
		my @hexrec = unpack("H2 H*", $rec);
		if($hexrec[0] !~ /^f1$/i)
		{
			$rec=pack("H*", "f1".$hexrec[1]);
			i::warnit("Changed the ASA char from [".$hexrec[0]."] to [f1] - OLDrec: ", $hexrec[0], $hexrec[1]." - NEWrec: ". unpack("H*", $rec));
		}  
	  }
      $page_buff .= &{$self->{recexit}}($self, &{$self->{pagexlator}}($self, $rec));
#      i::logit("extract data buff XL: $for_lines: LLHEX: ", unpack("H*", $buff), " RECLL: ", length($rec));
      $totlines++;
    }
    if($args->{tostreamer}->can('write')){
        $args->{tostreamer}->write($page_buff);
    }else{	
    $args->{tostreamer}->print($page_buff);
    } 
    $for_pages--; 
    $totpages++;
    last if $for_pages <= 0;
  }
  continue {
    ($entry_page, $from_line, $from_offset, $for_lines) = $page_iterator->get_next_entry();
    if ( !defined($from_line) ) {
       i::warnit("INPUT extract pages unable to retrieve btree infos for page $from_page");
       return undef;
    }
  }
  # warn "extract_pages $totpages pages - $totlines lines extracted\n"; 
  return ($totpages, $totlines);
}

sub locate_pages {
  my ($self, $from_page, $for_pages) = (shift, shift, shift);
  my $args = { @_ };
  $main::veryverbose && i::warnit(ref($self)."::extract_pages FP: $from_page XP: $for_pages called by ".join('::', (caller())[0,2]));
  die "Unable to extract pages without random access\n" 
                   unless (exists($self->{random_access}) && $self->{random_access});
  
  my $page_iterator = $self->{page_index}->get_iterator(0, $from_page);
  if ( !$page_iterator ) {
         i::warnit("INPUT extract pages unable to find btree element for page $from_page");
         return undef;
      }
  my ( $entry_page, $from_line, $from_offset, $for_lines ) = $page_iterator->get_next_entry();
  if ( !defined($from_line) ) {
         i::warnit("INPUT extract pages unable to retrieve btree infos for page $from_page");
         return undef;
      }
  die "$entry_page != $from_page\n" if $entry_page != $from_page;
      
  my $pagelist = [];
  while($for_pages > 0) {
  	push @{$pagelist}, [$entry_page, $from_line, $from_offset, $for_lines];
    $for_pages--; 
  }
  continue {
    ($entry_page, $from_line, $from_offset, $for_lines) = $page_iterator->get_next_entry();
     if ( !defined($from_line) ) {
         i::warnit("INPUT extract pages unable to retrieve btree infos for page $from_page");
         return undef;
      }
  }

  return $pagelist;
}

sub getPages {
    my ($self, $pagelist) = (shift, shift);
    my $args = { @_ };
    die ref($self)."::getPages No streamer defined as argument to call" 
                                              unless exists($args->{streamer});
    die ref($self)."::getPages streamer specified cannot \"print\"" 
                                              unless $args->{streamer}->can('print');
                                              
    
    my ($totpages, $totlines, $nextline) = (0, 0);
    while( scalar(@{$pagelist})) {
    	my $pentry = shift @{$pagelist};
        my ($entry_page, $from_line, $from_offset, $for_lines) = @{$pentry};
        my $page_buff;
        my $inputIOH = $self->{blocks_input}->bsearchLine($from_line);
        if (!$inputIOH ) {
            i::warnit("INPUT extract page unable to find page $entry_page at line $from_line");
            return undef;
        }
     
        $main::veryverbose and i::warnit(ref($self)."::extract_pages FP: $entry_page"
            . " FL: $from_line XL: $for_lines TOTL: $totlines TOTP: $totpages");
        for ($page_buff = ''; $for_lines; $for_lines-- ) {
            $inputIOH->read(my $buff,2);
            $page_buff .= $buff;
            my $lrec = unpack("n", $buff); 
            $inputIOH->read(my $rec, $lrec);
            $page_buff .= $rec;
            $totlines++;
        }
        $args->{tostreamer}->print(&{$self->{pagexlator}}($self, $page_buff));
        $totpages++;
    }
    # i::logit("extract_pages $totpages pages - $totlines lines extracted"); 
    return ($totpages, $totlines);
	
}

sub getFileName {
  my $self = shift; return join("/", @{$self}{qw(LocalPath LocalFileName)});
}

sub Close {
  my $self = shift; %$self = ();
}

sub DESTROY {
  my $self = shift; $self->Close();
}

1;
