
package XReport::ARCHIVE::OUTPUT; 

use strict;

use File::Basename;
use File::Path;

use XReport::Util;

sub Create {
  my ($ClassName, $jr) = (shift, shift);

  my ( $tgtLPOUT, $LocalPathId_OUT, $LocalFileName, $TarFileNumberOUT, $TarRecNumberOUT ) = 
                     $jr->getValues(qw(TargetLocalPathId_OUT LocalPathId_OUT LocalFileName TarFileNumberOUT TarRecNumberOUT));
  my $origOutId = $LocalPathId_OUT;
  $LocalPathId_OUT = $tgtLPOUT if ($tgtLPOUT && $LocalPathId_OUT ne $tgtLPOUT);                     
  if ( $LocalPathId_OUT ) {
  	my $LocalPath = getConfValues('LocalPath')->{$LocalPathId_OUT};
  	$LocalPathId_OUT = $tgtLPOUT if $LocalPath !~ /^file:/i;
  }                    
  i::logit("Serving create request for $LocalFileName into path_id $LocalPathId_OUT (tgt: $tgtLPOUT JR OutID: $origOutId)");

  # first on disk ******** ////// ********** //////// ********* //////// *********

  # ( $LocalPathId_OUT, $LocalFileName ) = $jr->getValues(qw(LocalPathId_OUT LocalFileName));

#  $LocalPathId_OUT = "L1" if $LocalPathId_OUT !~ /^(?:|L.*)$/i;
#  my $LocalPath = getConfValues('LocalPath')->{$LocalPathId_OUT || "L1"};
#  die "Unsupported Output path on $LocalPathId_OUT ($LocalPath)" unless $LocalPath =~ /file:\/\//i;
#  $LocalPath =~ s/file:\/\///i; 
#
#  $LocalFileName = "OUT/".$jr->getFileName('ZIPOUT');
#
#  require XReport::ARCHIVE::Zip;

#  return XReport::ARCHIVE::Zip->Create($LocalPath, $LocalFileName);
  my $LocalFilePath = [ split /[\/\\]/, "OUT/".$jr->getFileName('ZIPOUT') ];
  $LocalFileName = pop @{$LocalFilePath};
  $LocalPathId_OUT = '' if ( !$LocalPathId_OUT || $LocalPathId_OUT =~ /^\s*$/ );
  
  ($LocalPathId_OUT, my $LocalPath) 
    = @{XReport::ARCHIVE::newTargetPath( $LocalPathId_OUT, join("\\", @{$LocalFilePath}))}{qw(LocalPathId fqn)};
  $jr->setValues('LocalPathId_OUT', $LocalPathId_OUT);
  my $id_out = $jr->getValues(qw(LocalPathId_OUT));
  require XReport::ARCHIVE::Zip;
  my $archresult = XReport::ARCHIVE::Zip->Create($LocalPath, $LocalFileName);
  warn "ZIP Archive created - PathID: $id_out result: $archresult\n";
   
  return $archresult;
}

use XReport::ARCHIVE::Centera qw();
use XReport::ARCHIVE::Zip qw();
use XReport::ARCHIVE::JobREPORT qw();
sub Open {
  my ($ClassName, $jr, %args) = @_; my ($LocalPath, $archiver, $lx, $self);

#  require XReport::ARCHIVE::JobREPORT;

  if ( !ref($jr) ) {
  	 i::logit("ARCHIVE::OUTPUT Open calling open for $jr as requested by ".join('::', (caller())[0,2]));
     $jr = XReport::ARCHIVE::JobREPORT->Open($jr);
  }
  my (
    $JobReportId, $LocalPathId_OUT, 
    $LocalFileName, $TarFileNumberOUT, $TarRecNumberOUT
  ) = 
  $jr->getValues(qw(JobReportId LocalPathId_OUT LocalFileName TarFileNumberOUT TarRecNumberOUT));
  my $pool;
  $LocalPath = c::getValues('LocalPath')->{$LocalPathId_OUT ||= "L1"};
  (my $iface, $lx) = ($LocalPath =~ /^\s*([^:\s]+):\/\/(.*)$/);
  if ( lc($iface) eq  'file' ) {
     i::logit("ARCHIVE::OUTPUT calling getFilename in ".ref($jr));
     $LocalFileName = "OUT/".$jr->getFileName('ZIPOUT');
     $archiver = XReport::ARCHIVE::Zip->Open($lx, $LocalFileName);
  } 
  elsif ( lc($iface) eq 'centera' ) {
    my $dbr = XReport::DBUtil::dbExecute("
      SELECT OUTFILE_ClipId from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId
    ");
    my $ClipId = $dbr->GetFieldsValues('OUTFILE_ClipId'); $dbr->Close();
   
#    require XReport::ARCHIVE::Centera; 

    i::logit("OPENING CENTERA CLIP $LocalPathId_OUT/$ClipId requested by ".join('::', (caller())[0,2]));
    $main::cpool = XReport::ARCHIVE::Centera::FPPool->new( $LocalPathId_OUT ) unless $main::cpool;
    if (!$main::cpool) {
    	my $diemsg = "Centera Pool not intialized - ";
    	i::logit("$diemsg");
    	die $diemsg;
    }
    else {
        i::logit("CENTERA POOL Open completed for $LocalPathId_OUT");
    	
    }
    
    $archiver = $main::cpool->Open_Clip($LocalFileName, $ClipId);

    i::logit("CENTERA CLIP $LocalPathId_OUT/$ClipId OPENED");
    
  }

  else {
    die("INVALID LOCALPATH DETECTED \"$LocalPath\" - iface: $iface - lx: $lx");
  }

  if ( !$args{'wrapper'} ) {
    return $archiver;
  }
  else {
    $self = {
      archiver => $archiver, JobReportId => $JobReportId, 
      LocalPath => $LocalPath, LocalFileName => $LocalFileName 
    };
    return bless $self, $ClassName;
  }
}

sub getFileName {
  my $self = shift; 
  
  return join("/", @{$self}{qw(LocalPath LocalFileName)});
}

sub Close {
  my $self = shift; %$self = ();
}

sub DESTROY {
  my $self = shift; $self->Close();
}

1;
