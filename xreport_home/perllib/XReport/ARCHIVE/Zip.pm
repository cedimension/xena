
package XReport::Zip::File::INPUT;

use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

sub desiredCompressionMethod {
}

sub uncompressedSize {
  my $self = shift; $self->{'ZipMember'}->uncompressedSize();
}

sub lastModTime {
  my $self = shift; $self->{'ZipMember'}->lastModTime();
}

sub Open {
  my $self = shift; my $ZipMember = $self->{'ZipMember'};

  #$ZipMember->desiredCompressionMethod(COMPRESSION_STORED) if $_[0] == COMPRESSION_STORED;
  $ZipMember->desiredCompressionMethod(COMPRESSION_STORED) unless (defined $_[0] );
  $ZipMember->desiredCompressionMethod(COMPRESSION_STORED) if ((defined $_[0] ) && ($_[0] == COMPRESSION_STORED));
 
  $ZipMember->rewindData(); 
}

sub eof {
  my $self = shift; $self->{'ZipMember'}->readIsDone();
}

sub Close {
  my $self = shift; $self->{'ZipMember'}->endRead();
}

sub read {
  my $self = shift; my $ZipMember = $self->{'ZipMember'}; 

  my ($az_ref, $az_status) = $ZipMember->readChunk( $_[1] );

  #print ">> $$az_ref // $az_status // $_[1] <<";

  $_[0] = $$az_ref;
}

sub ExtractFile2FH {
    my ($self, $INPUT, $oFH) = @_;
    my $zrc = $INPUT->{ZipMember}->ExtractToFileHandle($oFH);
    die "Extract ended with error - ZIP rc: $zrc" if $zrc != AZ_OK;
}

#------------------------------------------------------------
package XReport::ARCHIVE::Zip;

use File::Basename;
use File::Path; 
use File::Spec;

use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

sub Create {
  my ($ClassName, $LocalPath, $FileName) = @_; $FileName = "$LocalPath/$FileName";

  my $Zip = Archive::Zip->new(); 
  i::logit("$ClassName ZIP handler initialized to store \"$FileName\" - handler: ". ref($Zip));
  
  my $self = {
    mode => 'OUTPUT', 
    FileName => $FileName, 
    Zip => $Zip
  };

  bless $self, $ClassName;
}

sub Open {
  my ($ClassName, $LocalPath, $FileName) = @_; my $az_status; $FileName = "$LocalPath/$FileName";

  my $Zip = Archive::Zip->new(); 

  $az_status = $Zip->read($FileName) 
   and 
  die("ZIP FILE OPEN ERROR: az_status=$az_status \"$FileName\"");

  my $self = {
    mode => 'INPUT', FileName => $FileName, Zip => $Zip
  };

  bless $self, $ClassName;
}

sub AddStream {
  ### open member for output ONLY if Create return member
}

sub AddBuffer {
  ### ok or ko (do not return member)
}

sub AddFile {
  my ($self, $FromFile, $ToFile) = @_;
  my ($Zip, $mode) = @{$self}{qw(Zip mode)}; 
  i::logit("Adding $ToFile to output archive using ".ref($Zip));
  
  my $az_status = $Zip->addFile($FromFile, $ToFile);
  die "Add file failed with for \"$FromFile\" to $self->{Filename} using ".ref($Zip)." - azStatus: $az_status"
                                  unless $az_status && ref($az_status) eq 'Archive::Zip::NewFileMember';
}

sub ExistsFile {
  my ($self, $FileName) = @_; my ($Zip, $mode) = @{$self}{qw(Zip mode)}; 

  die "INVALID ZIP OPEN MODE ($mode)" if $mode ne 'INPUT';
  
  return $Zip->memberNamed($FileName);
}

sub LocateFile {
  my ($self, $FileName) = @_; my ($Zip, $mode) = @{$self}{qw(Zip mode)}; 

  my $ZipMember = $Zip->memberNamed($FileName) 
   or 
  die("ZIP MEMBER NAMED \"$FileName\" NOT FOUND");
  
  my $member = {
    Zip => $Zip, ZipMember => $ZipMember
  };

  bless $member, 'XReport::Zip::File::INPUT';
}

sub ExtractFile {
  my ($self, $iFileName, $oFileName) = @_; my ($Zip, $mode) = @{$self}{qw(Zip mode)}; 

  die "INVALID CLIP OPEN MODE ($mode)" if $mode ne 'INPUT';
  
  return $Zip->extractMember($iFileName, $oFileName);
}

sub FileContents {
  my ($self, $FileName) = @_; my ($Zip, $mode) = @{$self}{qw(Zip mode)}; 
  
  my ($t, $e) = $Zip->contents($FileName); 

  die("Zip FileContents ERROR $e") if $e; return $t;
}

sub ExtractTree {
  my $self = shift; my ($Zip, $mode) = @{$self}{qw(Zip mode)};

  return $Zip->extractTree(@_);
}

sub FileList {
  my $self = shift; my ($Zip, $mode) = @{$self}{qw(Zip mode)};

  return [$Zip->memberNames()];
}

*FileNames = *FileList;

sub ListFiles { ## new
  my $self = shift; print join("\n", @{$self->FileNames(@_)}), "\n";
}

sub Close {
  my $self = shift;
  my ($Zip, $mode) = @{$self}{qw(Zip mode)};
  my $FileName = $self->{FileName};
  if ( my ($floc, $fpath) = ($FileName =~ /^((?:[A-Za-z]:|[\\\/]{2}[^\\\/]+?))([\\\/].+)$/) ) {
    $FileName = File::Spec->catfile(split /[\\\/]/, $fpath);
    my $sep = substr($FileName, 0, 1);
  	$floc =~ s/[\\\/]/$sep/g;
  	$FileName = $floc.$FileName;
  }
  i::logit(__PACKAGE__." Closing output archive \"$FileName\" - mode: $mode");
  
  mkpath(dirname($FileName)) if !-d dirname($FileName);
  
  if ($mode eq 'OUTPUT') {
  	
    my $az_status = $Zip->writeToFileNamed($FileName);
    die("ZIP WRITE ERROR WRITING ARCHIVE \"$FileName\" az_status=$az_status") if $az_status != AZ_OK;
    i::logit(__PACKAGE__." Output archive \"$FileName\" closed - az_status: $az_status");
  }
  if ($mode eq 'OVERWRITE') {
    my $az_status = $Zip->overwriteAs($FileName);
    $az_status == AZ_OK 
      ||
    die("ZIP WRITE ERROR WRITING ARCHIVE \"$FileName\" az_status=$az_status");
  }
}

sub Update {
  my ($self, $FileName, $newFile) = @_; my ($Zip, $mode) = @{$self}{qw(Zip mode)};
  die "INVALID ZIP OPEN MODE ($mode)" if $mode ne 'INPUT';
  #return $Zip->updateMember({memberOrZipName => $FileName, name => $newFile});
  return $Zip->updateMember($FileName, $newFile);

}

sub Migrate {
}

sub Delete {
}

sub FileSize {
  my ($self, $FileName) = @_; my ($Zip, $mode) = @{$self}{qw(Zip mode)}; 
  my $member = $Zip->memberNamed($FileName);
  #if($member != undef) {
  if(defined $member ) {
    return $member->uncompressedSize();
  } else {
	return -1;
  }
}

1;

__END__

my ($ZipFileName, $az_status) = ($self->getFileName("ZIPOUT"), -723);
  for (1..3) {
    $az_status = $zip->writeToFileNamed( $ZipFileName );
    last if $az_status == 0;
    &$logrRtn("RETRYING WRITE OF ZIP ARCHIVE \"$ZipFileName\" az_status=$az_status.");
  }
  die("ERROR WRITING ZIP ARCHIVE \"$ZipFileName\" az_status=$az_status") if $az_status != 0;

