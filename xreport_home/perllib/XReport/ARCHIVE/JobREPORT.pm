
package XReport::ARCHIVE::JobREPORT;

use strict;
no warnings 'deprecated';
#use XReport::DBUtil;

sub getFileName {
  my ($self, $fileType) = (shift, shift); 

  local $_ = $fileType;

  SWITCH: {
    /^INPUT$/i and do {
      return $self->getValues('LocalFileName'); 
    };

    /^ZIPOUT$/i and do {
      my ($JobReportName, $JobReportId, $dateTime, $dtYear, $dtDay) 
        = 
      $self->getValues(qw(JobReportName JobReportId $dateTime $dtYear $dtDay));

      return "$dtYear/$dtDay/$JobReportName.$dateTime.$JobReportId\.OUT.#0.zip";
    };
   
    die "ApplicationError: getFileName INVALID INPUT $_";
  }

}

sub getValues {
  my $self = shift; my @t; 

  for my $f (@_) {
    die("FieldName: $f NOT FOUND IN JobReport HASH requested by ".join('::', (caller())[0,2])) if !exists($self->{$f});
    push @t, $self->{$f}; 
  } 

  return wantarray ? @t : $t[0];
}

sub get_OUTPUT_ARCHIVE {
  my $self = shift; require XReport::ARCHIVE; 
  return XReport::ARCHIVE::get_OUTPUT_ARCHIVE($self);
}

sub get_INPUT_ARCHIVE {
  my $self = shift; require XReport::ARCHIVE; 
  return XReport::ARCHIVE::get_INPUT_ARCHIVE($self);
}

sub getExtractor {
  die("getExtractor NOT IMPLEMENTED IN PACKAGE ".__PACKAGE__);
}

sub Close {
  my $self = shift; return if $self->{'closed'};

  %$self = (closed => 1);
}

sub Open {
  my ($ClassName, $JobReportId) = @_; my ($self, $dbr, $dateTime);

  $dbr = dbExecute("
    SELECT * FROM tbl_JobReports a INNER JOIN tbl_JobReportNames b ON b.JobReportName = a.JobReportName WHERE JobReportId = $JobReportId
  ");
  die("Archive::JobREPORT JobReportId $JobReportId NOT FOUND") if $dbr->eof();

  $self = $dbr->GetFieldsHash(); $dbr->Close();

  bless $self, $ClassName; 

  ($dateTime) = $self->getValues('LocalFileName') =~ /\/(?:[^\/.]+\.)+(\d+)\.[\d#]+\.DATA\./i; 
  #($dateTime) = $self->getValues('LocalFileName') =~ /\/[^\/.]+\.(\d+)\.[\d#]+\.DATA\./i;

  @{$self}{qw($dateTime $dtYear $dtDay closed)} = ($dateTime, unpack("a4a4", $dateTime), 0);
   
  return $self;
}

sub DESTROY {
  #print "DESTROY ", __PACKAGE__, "\n";
}

1;
