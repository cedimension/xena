
package XReport::ARCHIVE::Util;

use strict;

use XReport::Util;
use XReport::ARCHIVE;

sub Migrate_INPUT_TO_CENTERA {
  my ($jr, $TargetLocalPathId, %args) = @_; require XReport::ARCHIVE::Centera; 

  if ( !ref($jr) ) {
    require XReport::ARCHIVE::JobREPORT; 
    $jr = XReport::ARCHIVE::JobREPORT->Open($jr)
  } 
 
  my $JobReportId = $jr->getValues('JobReportId');

  my $dbr = dbExecute("
    SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId
  ");

  die("THE JobReport $JobReportId HAS ALREADY THE INPUT ARCHIVE IN CENTERA")
   if
  !$dbr->eof() && $dbr->GetFieldsValues('INFILE_ClipId') ne '';

  $dbr->Close();

  print time(), " - BEGIN of INPUT MIGRATION TO CENTERA of JobReportId $JobReportId\n";

  my $iar = XReport::ARCHIVE::get_INPUT_ARCHIVE($jr, wrapper => 1);
  my (
    $archiver, 
    $LocalFileName
  ) 
  = @{$iar}{qw(archiver LocalFileName)};
  
  my $TargetLocalPath = getConfValues('LocalPath')->{$TargetLocalPathId};
  my ($PoolRef) = $TargetLocalPath =~ /^\s*centera:\[\/\/(.*)\]/;
  my ($clipid, $clipsize) = ();

  if ($LocalFileName =~ /DATA\.TXT(?:\.gz)?$/) {
    my $pool = XReport::ARCHIVE::Centera::FPPool->new($TargetLocalPathId); 
    my $clip = $pool->Create_Clip($LocalFileName);

    my $os = $clip->AddStream($LocalFileName, compr_method => 'deflate'); 

    my $is = $archiver->get_INPUT_STREAM(); $is->Open(); my $rec;

    while(1) {
      $is->read($rec, 131072); last if $rec eq ''; 
      $os->write($rec);
    }

    $is->Close(); $os->Close(); 

    $clip->ListFiles(); $clipid = $clip->Write(); $clipsize = $clip->GetTotalSize();
  }
  else {
    my $TargetLocalPath = getConfValues('LocalPath')->{$TargetLocalPathId};
    my ($PoolRef) = ($TargetLocalPath =~ /^\s*centera:\/\/\[(.*)\]/);

    my $pool = XReport::ARCHIVE::Centera::FPPool->new($PoolRef); 
    my $clip = $pool->Create_Clip($LocalFileName);

    $archiver = $iar->get_INPUT_ARCHIVE();

    for my $FileName (@{$archiver->FileNames()}) {
      my $compr_method = ($FileName =~ /\.pdf$/i) ? 'stored' : 'deflate';

      print "Migrating to Centera file=$FileName compr_method=$compr_method\n";

      my $os = $clip->AddStream($FileName, compr_method => $compr_method);

      my $is = $archiver->LocateFile($FileName); $is->Open(); my $rec;

      while(1) {
        $is->read($rec, 131072); last if $rec eq ''; 
        $os->write($rec);
      }

      $is->Close(); $os->Close(); 
    }

    $archiver->Close(); $clipid = $clip->Write(); $clipsize = $clip->GetTotalSize(); $clip->ListFiles();
  }

  $dbr = dbExecute("
    SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId
  ");

  if ($dbr->eof()) {
    dbExecute("
      INSERT into tbl_JobReportsCenteraClipIds (
        JobReportId, PoolRef, INFILE_ClipId, OUTFILE_ClipId, INFILE_TotalSize, OUTFILE_TotalSize
      ) 
      VALUES ($JobReportId, 'C1', '$clipid', '', $clipsize, NULL)
    ");
  }
  else {
    dbExecute("
      UPDATE tbl_JobReportsCenteraClipIds set INFILE_ClipId = '$clipid', INFILE_TotalSize = $clipsize where JobReportId = $JobReportId
    ");
  }
  
  if ( $args{'deleteFile'} == 1 ) {
    my $FileName = substr($iar->getFileName(), 7);
    print "DELETING FILE \"$FileName\"\n";
    unlink $FileName
      or
    die "DELETE ERROR for INPUT ARCHIVE \"$FileName\" $!"
  }
  $iar->Close();

  print time(), " - END of INPUT MIGRATION TO CENTERA of JobReportId $JobReportId ClipId=$clipid\n";
}

sub Migrate_INPUT {
  Migrate_INPUT_TO_CENTERA(@_);
}

sub Migrate_OUTPUT_TO_CENTERA {
  my ($jr , $TargetLocalPathId, %args) = @_; require XReport::ARCHIVE::Centera;

  if ( !ref($jr) ) {
    require XReport::ARCHIVE::JobREPORT; 
    $jr = XReport::ARCHIVE::JobREPORT->Open($jr)
  }

  my $JobReportId = $jr->getValues('JobReportId');

  my $dbr = dbExecute("
    SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId
  ");

  die("THE JobReport $JobReportId HAS ALREADY THE OUTPUT ARCHIVE IN CENTERA")
    if 
  !$dbr->eof() && $dbr->GetFieldsValues('OUTFILE_ClipId') ne ''; 

  $dbr->Close();

  print time(), " - BEGIN of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId\n";

  my $iar = XReport::ARCHIVE::get_OUTPUT_ARCHIVE($jr, wrapper => 1);
  my (
    $archiver, 
    $LocalFileName
  ) 
  = @{$iar}{qw(archiver LocalFileName)};
  
  my $TargetLocalPath = getConfValues('LocalPath')->{$TargetLocalPathId};
  my ($PoolRef) = $TargetLocalPath =~ /^\s*centera:\/\/\[(.*)\]/;

  my $pool = XReport::ARCHIVE::Centera::FPPool->new($TargetLocalPathId); 
  my $clip = $pool->CreateClip($LocalFileName);

  for my $FileName (@{$archiver->FileNames()}) {
    if ($FileName =~ /\w+\.$JobReportId\.0\.#\d+\.log/) {
      print "todel=$FileName\n"; next; 
    }

    my $compr_method = ($FileName =~ /\.pdf$/i) ? 'stored' : 'deflate';

    print "Migrating to Centera file=$FileName compr_method=$compr_method\n";

    my $os = $clip->AddStream($FileName, compr_method => $compr_method);

    my $is = $archiver->LocateFile($FileName); $is->Open(); my $rec;

    while(1) {
      $is->read($rec, 131072); last if $rec eq ''; 
      $os->write($rec);
    }

    $is->Close(); $os->Close(); 
  }

  $archiver->Close(); my $clipid = $clip->Write(); my $clipsize = $clip->GetTotalSize(); #$clip->ListFiles();  

  $dbr = dbExecute("
    SELECT * from tbl_JobReportsCenteraClipIds where JobReportId = $JobReportId
  ");

  if ($dbr->eof()) {
    dbExecute("
      INSERT into tbl_JobReportsCenteraClipIds (
        JobReportId, PoolRef, INFILE_ClipId, OUTFILE_ClipId, INFILE_TotalSize, OUTFILE_TotalSize
      ) 
      VALUES ($JobReportId, 'C1', '', '$clipid', NULL, $clipsize)
    ");
  }
  else {
    dbExecute("
      UPDATE tbl_JobReportsCenteraClipIds set OUTFILE_ClipId = '$clipid', OUTFILE_TotalSize = $clipsize where JobReportId = $JobReportId
    ");
  }
    
  if ( $args{'deleteFile'} == 1 ) {
    my $FileName = substr($iar->getFileName(), 7);
    print "DELETING FILE \"$FileName\"\n";
    unlink $FileName
      or
    die "DELETE ERROR for INPUT ARCHIVE \"$FileName\" $!"
  }
  $iar->Close();

  print time(), " - END of OUTPUT MIGRATION TO CENTERA of JobReportId $JobReportId ClipId=$clipid\n";
}

sub Migrate_OUTPUT {
  Migrate_OUTPUT_TO_CENTERA(@_);
}

1;
