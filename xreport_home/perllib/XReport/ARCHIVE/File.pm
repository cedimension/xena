package XReport::ARCHIVE::File;

use base IO::File;

#sub Open {
#    my $self;
#    if ( ref($_[0]) ) {
#        $self = shift;
#    }
#    else {
#        my $class = shift;
#        $self = IO::File::new($class);
#    }
#    return $self unless scalar(@_);
#    return $self->open(@_); 
#}

sub Open {
	my $self = shift;
	return $self unless scalar(@_);
	return $self->open(@_) if ref($self); 
	return $self->new(@_);
}

sub Close {
    my $self = shift;
	return $self->close(@_);
}

__PACKAGE__;
