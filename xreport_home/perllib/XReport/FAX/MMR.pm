package XReport::FAX::FAXTables;

use strict;

our ($white_table, $black_table, $mmr_table);

my %white_codes = (
  '00110101' => 0,
  '000111' => 1,
  '0111' => 2,
  '1000' => 3,
  '1011' => 4,
  '1100' => 5,
  '1110' => 6,
  '1111' => 7,
  '10011' => 8,
  '10100' => 9,
  '00111' => 10,
  '01000' => 11,
  '001000' => 12,
  '000011' => 13,
  '110100' => 14,
  '110101' => 15,
  '101010' => 16,
  '101011' => 17,
  '0100111' => 18,
  '0001100' => 19,
  '0001000' => 20,
  '0010111' => 21,
  '0000011' => 22,
  '0000100' => 23,
  '0101000' => 24,
  '0101011' => 25,
  '0010011' => 26,
  '0100100' => 27,
  '0011000' => 28,
  '00000010' => 29,
  '00000011' => 30,
  '00011010' => 31,
  '00011011' => 32,
  '00010010' => 33,
  '00010011' => 34,
  '00010100' => 35,
  '00010101' => 36,
  '00010110' => 37,
  '00010111' => 38,
  '00101000' => 39,
  '00101001' => 40,
  '00101010' => 41,
  '00101011' => 42,
  '00101100' => 43,
  '00101101' => 44,
  '00000100' => 45,
  '00000101' => 46,
  '00001010' => 47,
  '00001011' => 48,
  '01010010' => 49,
  '01010011' => 50,
  '01010100' => 51,
  '01010101' => 52,
  '00100100' => 53,
  '00100101' => 54,
  '01011000' => 55,
  '01011001' => 56,
  '01011010' => 57,
  '01011011' => 58,
  '01001010' => 59,
  '01001011' => 60,
  '00110010' => 61,
  '00110011' => 62,
  '00110100' => 63,
  '11011' => 64,
  '10010' => 128,
  '010111' => 192,
  '0110111' => 256,
  '00110110' => 320,
  '00110111' => 384,
  '01100100' => 448,
  '01100101' => 512,
  '01101000' => 576,
  '01100111' => 640,
  '011001100' => 704,
  '011001101' => 768,
  '011010010' => 832,
  '011010011' => 896,
  '011010100' => 960,
  '011010101' => 1024,
  '011010110' => 1088,
  '011010111' => 1152,
  '011011000' => 1216,
  '011011001' => 1280,
  '011011010' => 1344,
  '011011011' => 1408,
  '010011000' => 1472,
  '010011001' => 1536,
  '010011010' => 1600,
  '011000' => 1664,
  '010011011' => 1728,
  '00000001000' => 1792,
  '00000001100' => 1856,
  '00000001101' => 1920,
  '000000010010' => 1984,
  '000000010011' => 2048,
  '000000010100' => 2112,
  '000000010101' => 2176,
  '000000010110' => 2240,
  '000000010111' => 2304,
  '000000011100' => 2368,
  '000000011101' => 2432,
  '000000011110' => 2496,
  '000000011111' => 2560
);

my %black_codes = (
  '0000110111' => 0,
  '010' => 1,
  '11' => 2,
  '10' => 3,
  '011' => 4,
  '0011' => 5,
  '0010' => 6,
  '00011' => 7,
  '000101' => 8,
  '000100' => 9,
  '0000100' => 10,
  '0000101' => 11,
  '0000111' => 12,
  '00000100' => 13,
  '00000111' => 14,
  '000011000' => 15,
  '0000010111' => 16,
  '0000011000' => 17,
  '0000001000' => 18,
  '00001100111' => 19,
  '00001101000' => 20,
  '00001101100' => 21,
  '00000110111' => 22,
  '00000101000' => 23,
  '00000010111' => 24,
  '00000011000' => 25,
  '000011001010' => 26,
  '000011001011' => 27,
  '000011001100' => 28,
  '000011001101' => 29,
  '000001101000' => 30,
  '000001101001' => 31,
  '000001101010' => 32,
  '000001101011' => 33,
  '000011010010' => 34,
  '000011010011' => 35,
  '000011010100' => 36,
  '000011010101' => 37,
  '000011010110' => 38,
  '000011010111' => 39,
  '000001101100' => 40,
  '000001101101' => 41,
  '000011011010' => 42,
  '000011011011' => 43,
  '000001010100' => 44,
  '000001010101' => 45,
  '000001010110' => 46,
  '000001010111' => 47,
  '000001100100' => 48,
  '000001100101' => 49,
  '000001010010' => 50,
  '000001010011' => 51,
  '000000100100' => 52,
  '000000110111' => 53,
  '000000111000' => 54,
  '000000100111' => 55,
  '000000101000' => 56,
  '000001011000' => 57,
  '000001011001' => 58,
  '000000101011' => 59,
  '000000101100' => 60,
  '000001011010' => 61,
  '000001100110' => 62,
  '000001100111' => 63,
  '0000001111' => 64,
  '000011001000' => 128,
  '000011001001' => 192,
  '000001011011' => 256,
  '000000110011' => 320,
  '000000110100' => 384,
  '000000110101' => 448,
  '0000001101100' => 512,
  '0000001101101' => 576,
  '0000001001010' => 640,
  '0000001001011' => 704,
  '0000001001100' => 768,
  '0000001001101' => 832,
  '0000001110010' => 896,
  '0000001110011' => 960,
  '0000001110100' => 1024,
  '0000001110101' => 1088,
  '0000001110110' => 1152,
  '0000001110111' => 1216,
  '0000001010010' => 1280,
  '0000001010011' => 1344,
  '0000001010100' => 1408,
  '0000001010101' => 1472,
  '0000001011010' => 1536,
  '0000001011011' => 1600,
  '0000001100100' => 1664,
  '0000001100101' => 1728,
  '00000001000' => 1792,
  '00000001100' => 1856,
  '00000001101' => 1920,
  '000000010010' => 1984,
  '000000010011' => 2048,
  '000000010100' => 2112,
  '000000010101' => 2176,
  '000000010110' => 2240,
  '000000010111' => 2304,
  '000000011100' => 2368,
  '000000011101' => 2432,
  '000000011110' => 2496,
  '000000011111' => 2560
);

my %mmr_codes = (
  '0001' => 'P',
  '001' => 'H',
  '1' => 'V0',
  '011' => 'VR1',
  '000011' => 'VR2',
  '0000011' => 'VR3',
  '010' => 'VL1',
  '000010' => 'VL2',
  '0000010' => 'VL3',
  '0000000000011' => 'EOL_1d',
  '0000000000010' => 'EOL_2d'
);


sub decode {
  my ($tab, $src) = (shift, shift); 
  my ($l, $pos, $max_pos, $tref) = ("", @{$src}{qw(pos max_pos tref)});
  while( $pos<$max_pos ) {
    $l .= substr($$tref, $pos, 1); $pos++;
    if (exists($tab->{$l})) {
      @{$src}{qw(pos max_pos)} = ($pos, $max_pos); return $tab->{$l}; 
    }
    if (length($l) > 13) {
      @{$src}{qw(pos max_pos)} = ($pos-length($l), $max_pos); return "";
    } 
  }
}

$white_table = bless \%white_codes;
$black_table = bless \%black_codes;
$mmr_table = bless \%mmr_codes;


package XReport::FAX::BitString;

use strict;

sub new {
  my ($className, $t) = (shift, shift); $t = unpack("B*", $t);
  bless {pos => 0, max_pos => length($t), tref => \$t};
}


package XReport::FAX::MMR;

my $mrtable = $XReport::FAX::FAXTables::mmr_table;
my $wtable = $XReport::FAX::FAXTables::white_table;
my $btable = $XReport::FAX::FAXTables::black_table;

my ($pr, $ipr, $xr, $ixr);

my ($src, $imgwidth, $imgheight);


sub get_scanlineMMR {
  ($pr, $xr) = ($xr, $pr); @$xr=(); my ($ipr,$ixr) = (0,0); my $a0color = 0; 

  my ($a0, $rle, $b1) = (0,0,$pr->[$ipr++]);

  while( $a0 < $imgwidth ) {

    my $c = $mrtable->decode($src);

    TEST:
    {
      ### Pass Mode ###
      $c eq 'P' && do { 
        $b1 += $pr->[$ipr++];
        $rle += $b1 - $a0;
        $a0 = $b1;
        $b1 += $pr->[$ipr++];
        last;
      };
      ### Horizontal Mode ###
      $c eq 'H' && do { 
        my $inc;
        ### First run
        my $table1 = ($a0color) ? $btable : $wtable; 
        do { $inc=$table1->decode($src); $a0+=$inc; $rle+=$inc; } while ($inc>=64);
        $xr->[$ixr++] = $rle; $rle = 0;
        ### Second run
        my $table2 = (!$a0color) ? $btable : $wtable; 
        do { $inc=$table2->decode($src); $a0+=$inc; $rle+=$inc; } while ($inc>=64);
        $xr->[$ixr++] = $rle; $rle = 0;
        last;
      };
      ### Vertical Modes ###
      $c =~ /V0|VR3|VR2|VR1|VL3|VL2|VL1/ && do 
      {
        my $inc=$b1;
        TEST_V:
        {
          $c eq 'V0' && do {
            $inc = $b1;
            $b1 += $pr->[$ipr++];
            last;
          };
          $c eq 'VR3' && do {
            $inc = $b1+3;
            $b1 += $pr->[$ipr++];
            last;
          };
          $c eq 'VR2' && do {
            $inc = $b1+2;
            $b1 += $pr->[$ipr++];
            last;
          };
          $c eq 'VR1' && do {
            $inc = $b1+1;
            $b1 += $pr->[$ipr++];
            last;
          };
          $c eq 'VL3' && do {
            $inc = $b1-3;
            $b1 -= $pr->[--$ipr];
            last;
          };
          $c eq 'VL2' && do {
            $inc = $b1-2;
            $b1 -= $pr->[--$ipr];
            last;
          };
          $c eq 'VL1' && do {
            $inc = $b1-1;
            $b1 -= $pr->[--$ipr];
            last;
          };
        };
        $xr->[$ixr++] = $inc+$rle-$a0;
        $a0 = $inc;
        $rle = 0;
        $a0color = !$a0color;
        last;
      };
      $c eq 'EOL_1d' && do {
        ### first line in 1d mode (ibm modification)
        while( $a0 < $imgwidth ) {
          my $inc = 0;
          ### White run
          do { $inc = $wtable->decode($src); $a0+=$inc; $rle+=$inc; } while ($inc>=64);
          $xr->[$ixr++] = $rle; $rle = 0;
          ### Black run
          do { $inc=$btable->decode($src); $a0+=$inc; $rle+=$inc; } while ($inc>=64);
          $xr->[$ixr++] = $rle; $rle = 0;
        }
        last;
      };
      $c eq 'EOL_2d' && do {
        last;
      };
      default:
      do {
        die "mmr code not recognized at pos/max_pos ".join("/",@{$src}{qw(pos max_pos)});
      };
    };
    ### Next reference run ###
    for(;$b1<=$a0 && $b1<$imgwidth;$ipr+=2)
    {
      $b1 += $pr->[$ipr]+$pr->[$ipr+1];
    }
  }

  ### Final P must be followed by V0 (they say!) ###
  if ($rle > 0)
  {
    if ($mrtable->decode($src) ne 'V0')
    {
      die("invalid_mmr_data");
    }
    $xr->[$ixr++] = $rle;
  }

  @$pr = ();

  return $xr;
}

sub decode_imageMMR {
#  my %args = @_; ($imgwidth, $imgheight, $src) = @args{qw(width height cimage)}; # mpezzi image object handling
  my %args = @_; ($imgwidth, $imgheight, $src, my $blackis0) = @args{qw(width height cimage blackis0)}; #mpezzi image object handling

  $src = XReport::FAX::BitString->new($src); my $image = '';

#  $imgwidth += (8 - $imgwidth%8) if $imgwidth%8;

  $xr = [$imgwidth]; $pr = []; 

  for (1..$imgheight) {
    my $rle = get_scanlineMMR();
#    my $bl = ''; my $color = "0"; # mpezzi image object handling
    my $bl = ''; my $color = ($blackis0 ? "1" : "0"); # mpezzi image object handling
    for (@$rle) {
      $bl .= ($color ? "1" : "0") x $_; $color = !$color;
    }
    die("INVALID LINE LENGTH DETECTED expected=$imgwidth read=", length($bl)) 
     if 
    length($bl) != $imgwidth; $image .= pack("B*", $bl);
  }

  return \$image;
}
1;

__END__

sub decode_imageRLE {
  my %args = @_; ($imgwidth, $imgheight, $src) = @args{qw(width height cimage)};

  $src = XReport::FAX::BitString->new($src); my $image = '';

  #$imgwidth += (8 - $imgwidth%8) if $imgwidth%8;

  $xr = [$imgwidth]; $pr = []; 

  for (1..$imgheight) {
    my $a0 = 0;
    while( $a0 < $imgwidth ) {
      my $inc = 0; my $rle = 0;
      ### White run
      do { $inc = $wtable->decode($src); $a0+=$inc; $rle+=$inc; } while ($inc>=64);
      print "white == $rle == $a0\n";
      last if $rle == 0;
      $xr->[$ixr++] = $rle; $rle = 0;
      last if $a0 >= $imgwidth;
      ### Black run
      do { $inc=$btable->decode($src); $a0+=$inc; $rle+=$inc; } while ($inc>=64);
      print "black == $rle == $a0\n";
      last if $rle == 0;
      $xr->[$ixr++] = $rle; $rle = 0;
    }
	$src->{'pos'} += 6;
  }

  return \$image;
}

1;
