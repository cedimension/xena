package XReport::SOAP;

use Carp;
require Exporter;


##############################################################################
# Define some constants
#

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

@ISA               = qw(Exporter);
@EXPORT            = qw();
@EXPORT_OK         = qw();
$VERSION           = '1.00';

use MIME::Base64;
use XML::Simple;
use Data::Dumper; 

use POSIX qw(strftime);
use constant SOAPMSGHDR => ($XReport::cfg->{SOAPCODEPAGE} ? '<?xml version="1.0" encoding="'.$XReport::cfg->{SOAPCODEPAGE}.'"?>' : '<?xml version="1.0" encoding="UTF-8"?>')
#use constant SOAPMSGHDR => '<?xml version="1.0" encoding="UTF-8"?>'
#use constant SOAPMSGHDR => '<?xml version="1.0" encoding="ISO-8859-1"?>'
  . '<soap:Envelope '
  . 'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" '
  . 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
  .' xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
  . '<soap:Body>'
  ;

use constant SOAPMSGTAIL => '</soap:Body >'
  . '</soap:Envelope>'
  ;

sub soaphdr {
  return SOAPMSGHDR;
}

sub soaptail {
  return SOAPMSGTAIL;
}

sub xmlquote {
  my $t = shift; $t =~ s/ +$//g;

  $t =~ s/&/&amp;/g;
  $t =~ s/</&lt;/g;
  $t =~ s/>/&gt;/g;
  $t =~ s/\"/&quot;/g;
  $t =~ s/\'/&apos;/g;

  $t =~ s/([\x80-\xff])/"\&\#".unpack("C",$1).";"/eg;

  return $t;
}

sub _solveContent {
  my $struct = shift;
#  print "Struct IN: ", ref($struct), " -\n", Dumper(\$struct);
  if (ref($struct) eq 'HASH') {
    if ( !scalar(keys %$struct) || (scalar(keys %$struct) == 1 && exists($struct->{content})) ) { 
      $struct = $struct->{content} || ''; 
    }
    else {
      foreach my $k ( keys %$struct ) {
	$struct->{$k} = _solveContent($struct->{$k});
      }
    }
  }
  elsif (ref($struct) eq 'ARRAY') {
    @$struct = map { _solveContent($_) } @$struct; 
  }
  return $struct;
}

sub _XMLout { return XML::Simple::XMLout(@_); }
sub _XMLin  {
  my $string = shift; my %args = ( 
				  KeyAttr => {'Columns' => 'colname'},
				  ForceArray => [ qw(NewEntries IndexEntries Columns ValuesArray) ],
				  ContentKey => 'content',
				  @_ ,
				 );
  return XML::Simple::XMLin($string, %args); 
}
#sub _encode_base64 { print "encoding ", length($_[0]), " bytes\n", my $b64data = MIME::Base64::encode_base64($_[0], "\n"); print "returning ", length($b64data), " bytes\n"; return $b64data;}
#sub _encode_base64 { return MIME::Base64::encode_base64($_[0]); }
sub _encode_base64 {
  my $content = shift;
  my $code = shift if ( $_[0] && ref($_[0]) eq 'CODE' );
  my $buffsz = 57*76;
  my $result = '';
  if ( ref($content) ) {
    binmode $content;
    while ( !$content->eof() ) {
      my $bcnt = $content->read(my $buff, $buffsz);
      $result = ($code ? &$code(@_, XReport::SOAP::_encode_base64($buff)) : $result.XReport::SOAP::_encode_base64($buff));
    }
    close $content;
  }
  else {
    foreach my $buff ( map { MIME::Base64::encode_base64($_) } unpack("(a$buffsz)*", $content )) {
      $result = ($code ? &$code(@_, $buff) : $result.$buff);
    }
  }
  return $result;
}
#sub _decode_base64 { return MIME::Base64::decode_base64($_[0]); }
sub _decode_base64 {
  my $b64str = shift;
  my $code = shift if ( $_[0] && ref($_[0]) eq 'CODE' );
  my $result = '';
  foreach my $buff ( map { tr#A-Za-z0-9+/##cd;                   # remove non-base64 chars
			   tr#A-Za-z0-9+/# -_#;                  # convert to uuencoded format
			   unpack("u", pack("c", 32 + 0.75*length($_) ) . $_) } split /\n/, join("\n", split /\s/, $b64str ) ) {
    $result = ($code ? &$code(@_, $buff) : $result.$buff);
  }
  return $result;
}

sub _buildSOAPStruct {
  my ($DocumentData, $rtype) = (shift, shift);
  $rtype = 'RESPONSE' unless $rtype;

  $DocumentData->{comment} = 'version 1.0';
  $DocumentData->{xmlns} = 'http://cereport.org/WebService';
  my $body = delete($DocumentData->{DocumentBody}) if exists($DocumentData->{DocumentBody});
  $body = $body->{content} if (ref($body) eq 'HASH');
#  print "BODY: ", length($body), "\n";

  my $struct = XMLout($DocumentData, 
		RootName => $rtype,
		NoIndent => 1, NoSort => 1, # SuppressEmpty => 1,
		KeyAttr => {'Columns' => 'colname'},
		ContentKey => 'content',
		XMLDecl => SOAPMSGHDR ) . SOAPMSGTAIL;

  if (my $logsub = main->can('debug2log')) {
    &$logsub("BuildSoapStruct - " . ($body ? length($body)." DocumentBody bytes will be added to struct" : '' ) . " SOAP $rtype:", $struct );
  }
  return $struct unless $body;

  my ($hdr, $trailer) = ( ($struct) =~ /^(.*\>)(\<\/$rtype.*)$/s );
#  print "HDR: $hdr \nTRAILER: $trailer\n";
  return $hdr . '<DocumentBody>' . $body . '</DocumentBody>' . $trailer;

}

sub buildResponse {
  return _buildSOAPStruct($_[0], $_[1] || 'RESPONSE');
}

sub buildRequest {
  return _buildSOAPStruct($_[0], $_[1] || 'REQUEST');
}

sub xtractParm {
  my ($struct, $parm) = (shift, shift);
  my $result = undef;
  if (ref($struct) eq 'HASH') {
    foreach my $k ( keys %$struct ) {
#      $main::dolog && i::logit("checking struct '", $k, "' for '$parm' result: ", $k =~ /^$parm$/i);
      return  $struct->{$k} if $k =~ /^$parm$/i;
      $result = xtractParm($struct->{$k}, $parm);
      return $result if $result;
    }
  }
  elsif (ref($struct) eq 'ARRAY') {
    foreach my $el ( @$struct ) {
      $result = xtractParm($el, $parm);
      return $result if $result;
    }
  }
  return undef;
}

sub BUILD_HTTP_ERROR {
  return ($_[0], "<faultcode>$_[0]</faultcode>"
    . "<faultactor>".$main::Application->{ApplName}.'/'.$_[1]."</faultactor>"
      . "<faultstring>" . ($_[2] ? $_[2] : ''). "</faultstring>");
}

sub BUILD_SOAP_ERROR {
  my $blderr = __PACKAGE__->can('BUILD_HTTP_ERROR');
  my $errmsg = (SOAPMSGHDR
	  . '<soap:Fault>'
	  . (&$blderr(@_))[1]
	  . '</soap:Fault>'
	  . SOAPMSGTAIL
	 );
  $main::dolog && i::logit($_[0] ." - $errmsg");
  return $errmsg;
}

sub closeXmlStruct {
  my $struct = shift;
  my @soaptags = ( $struct =~ /(?:\<([^\/][\w\:\-]+)[^>]*?(?<!\/)>)/sg );
  my $tagsended = { '?xml' => '>', ( $struct =~ /(?:\<\/([\w\:\-]+)[^>]*?(\>))/sg ) };
  return $struct . join('', map { (exists($tagsended->{"$_"}) ?  '' : '</'."$_".'>' ) }
			reverse @soaptags);
}


sub parseSOAPreq {
  my $rawdata = shift;
  my $rtype = shift;
  my $reqdata = { request => {} };
  $rtype = '^(?:[^\:]+\:)?REQUEST' unless $rtype;
  my ($lxmlhdr, $docbody, $lxmltail) = ( $rawdata, '', '');
  my $dbhdr_re = qr/\<DocumentBody(?i:\sxsi\:type\=[^\s]+?)?\s*\>/;
  ($lxmlhdr, $docbody, $lxmltail) = ($rawdata =~ /^(.*)$dbhdr_re(.*?)\<\/DocumentBody\s*\>(.*)$/mso)
                                                                             if $rawdata =~ /$dbhdr_re/mso;
  my $stream = $lxmlhdr . $lxmltail;
  $reqdata = {request =>  xtractParm(XMLin($stream,
					      KeyAttr => {'Columns' => 'colname'},
#					      KeyAttr => [qw(IndexEntries NewEntries colname)],
					      ForceArray => [ qw(NewEntries IndexEntries Columns ValuesArray) ],
					      ContentKey => 'content',
					     ),
					qr/$rtype/),
            #soaphdrs => xtractParm(XMLin($stream, NoAttr => 1, ), qr/(?i:soap-?env):Header/i),
          soaphdrs => xtractParm(XMLin($stream, NoAttr => 1, ), qr/(?i:soap(?:-?env)?):Header/i),
        } if $stream;
  $main::dolog && i::logit("parseSOAPreq reqdata $rtype: ", $stream, Dumper($reqdata)) if $main::trace2file;
  $reqdata->{request}->{DocumentBody} = $docbody if $docbody;
#  print "REQDATA: ", Dumper($reqdata);
  main::debug2log("parseSOAPreq reqdata $rtype: ", $stream, Dumper($reqdata));
  return $reqdata;
}

sub whereEntries {
  my $array = shift;
#  $main::dolog && i::logit("filling IndexEntries from " . join('::', (caller())[0,2]). " " . ref($dbr));

  my @IndexEntries;
  $main::dolog && i::logit("whereEntries no rows in array") unless scalar(@$array);

  while ( scalar(@$array) ) {
    
    my $fields = shift @$array;
    my $IndexEntry = { };
    while ( my ($var, $val) = each %$fields ) { 
      $val = $val->{content} if (ref($val) eq 'HASH' && exists($val->{content}));
	push @{$IndexEntry->{Columns}}, { colname => $var, 
					  ( ref($val) ? (( defined($val->[0]) ? (Min => $val->[0]) : ()), 
							 ( defined($val->[1]) ? (Max => $val->[1]) : ())) 
					    : (content =>  $val || '') ) };
    }
    push @IndexEntries, $IndexEntry;
  }

  return [ @IndexEntries ];
}

sub polishEntries {
  my $indexEntries = shift;
#  return $indexEntries if ( ref($indexEntries) eq 'HASH' );
  
#  my $newie = { Columns => [] };
  foreach $ix ( 0..$#{$indexEntries} ) {
  	 my $entry = ( exists($indexEntries->[$ix]->{Columns}) ? $indexEntries->[$ix]->{Columns} : {} );
  	 $indexEntries->[$ix] = $entry;
#    foreach $key ( keys %{$indexEntries->[$ix]->{Columns}} ) {
#      $indexEntries->[$ix]->{$key} = (ref($indexEntries->[$ix]->{Columns}->{$key}) 
#				      ? $indexEntries->[$ix]->{Columns}->{$key}->{content} 
#				      : $indexEntries->[$ix]->{Columns}->{$key});
#    }
     delete $indexEntries->[$ix]->{Columns};
  }
  return $indexEntries;
}


__PACKAGE__;
