package XReport::Web::Service;

use strict;

use Carp;
use Win32::Service;
use XReport;

sub new {
	my $className = shift;
	my $serviceStatus = {
		1 => "startService",
		4 => "stopService"
	};

	bless {
		ServiceTable => getServices(),
		StatusTable => $serviceStatus
	}, $className;
}

sub getServices {
	my %serviceTable;

	foreach my $serviceName(keys(%{$XReport::cfg->{'daemon'}})) {
		my $host = ""; my $status = {};
		my $serviceDescr = $XReport::cfg->{'daemon'}->{$serviceName}->{'display'};
	
		if(exists($XReport::cfg->{'daemon'}->{$serviceName}->{'host'})) {
			my $en = $XReport::cfg->{'daemon'}->{$serviceName}->{'host'};
			$host = (ref($en) ? $en->[0] : $en);
		}

		if($host !~ /^$/) {
			my $result = Win32::Service::GetStatus($host, $serviceName, $status);
			if($result) {
				$serviceTable{$host}{$serviceName}{'Status'} = $status->{'CurrentState'};
			} else {
				$serviceTable{$host}{$serviceName}{'Status'} = -1;
			}
	
			$serviceTable{$host}{$serviceName}{'Description'} = $serviceDescr;
		}
	}

	return \%serviceTable;
}

sub getHosts {
	my $self = shift; my @hosts; my $serviceTable = $self->{'ServiceTable'};

	foreach my $host(keys(%{$serviceTable})) {
		next if($host =~ /^$/);
		push(@hosts, $host)
	}

	return \@hosts;
}

sub getServicesName {
	my $self = shift; my $host = shift; my @services; my $serviceTable = $self->{'ServiceTable'};

	foreach my $service(keys(%{$serviceTable->{$host}})) {
		push(@services, $service)
	}

	return \@services;
}

sub getServiceDetails {
	my $self = shift; my $host = shift; my $service = shift;
	my $serviceTable = $self->{'ServiceTable'};

	my $details = {
		host => $host,
		name => $service,
		descr => $serviceTable->{$host}->{$service}->{'Description'},
		status => $serviceTable->{$host}->{$service}->{'Status'}
	};

	return $details;
}

sub changeServiceState {
	my $self = shift; my ($host, $service, $op) = (shift, shift, shift);
	my $result = "";

	if(exists($self->{"StatusTable"}->{$op})) {
		my $call = "\$result = ".$self->{"StatusTable"}->{$op}."(\$host, \$service)";
		eval($call);
	}

	$self->{"ServiceTable"} = getServices();
	return $result;
}

sub startService {
	my ($host, $service) = (shift, shift);
	my $result = "";

	$result = "Start of the service $service on $host failed!" if(!Win32::Service::StartService($host, $service));
	sleep(1);
	return $result;
}

sub stopService {
	my ($host, $service) = (shift, shift);
	my $result = "";

	$result = "Stop of the service $service on $host failed!" if(!Win32::Service::StopService($host, $service));
	sleep(1);
	return $result;
}

1;
