use lib($ENV{'XREPORT_HOME'}."/perllib");

package XReport::Web::DBUtil;

use strict;

use Carp;
use Win32::OLE;

sub new {
	my ($className, $connectionStr) = (shift, shift);

	bless {
		ConnectionHandler => dbConnect($connectionStr),
		ConnectionStr => $connectionStr,
		RecordSetHandler => undef
	}, $className;
}

sub dbConnect {
	my $connStr = shift;
	my $dbc = Win32::OLE->new("ADODB.Connection");

	$dbc->{"ConnectionTimeout"} = 10;
	$dbc->{"CommandTimeout"} = 30;
	$dbc->Open($connStr); 

	return $dbc if $dbc->State() != 0;

	my $dberr; $dberr = ($dbc->Errors(0)) ? $dbc->Errors(0)->Description() : "UNKNOWN";
	croak("ERROR CONNECTING TO XREPORT DATABASE: ".$dberr);
}

sub dbExecute {
	my $self = shift; my $sqlStr = shift; my $dbc = $self->{"ConnectionHandler"};
	my $dbr = Win32::OLE->new("ADODB.Recordset");
    
	$dbr->Open($sqlStr, $dbc, 3, 3);
	if($dbc->Errors()->Count() > 0) {
		croak("$sqlStr<br>".$dbc->Errors(0)->Description()); 
	}
  
	$self->{"RecordSetHandler"} = $dbr;
	return 1;
}

sub Variant_DateTime {
	my $data = shift;

	return $data->Date('yyyy/MM/dd')." ".$data->Time('hh:mm:ss tt') if($data);
}

sub eof {
	my $self = shift;  my $dbr = $self->{"RecordSetHandler"};

	return $dbr->eof() if($dbr);
	return 1;
}

sub getFieldsCount {
	my $self = shift; my $dbr = $self->{"RecordSetHandler"};

	return $dbr->Fields->Count() if($dbr);
	return 0;
}

sub getFieldName {
	my $self = shift; my $fieldIdx = shift; my $dbr = $self->{"RecordSetHandler"};

	return $dbr->Fields($fieldIdx)->Name() if($dbr);
	return "";
}

sub getFieldValue {
	my $self = shift; my $fieldIdx = shift;
	my $dbr = $self->{"RecordSetHandler"}; my $value = "";

	if($dbr) {
		$value = $dbr->Fields($fieldIdx)->Value();
		$value = Variant_DateTime($value) if($dbr->Fields($fieldIdx)->Type() == 135);
	}

	return $value;
}

sub MoveNext {
	my $self = shift; my $dbr = $self->{"RecordSetHandler"};

	$dbr->MoveNext() if($dbr);
}

sub closeRecordSet {
	my $self = shift; my $dbr = $self->{"RecordSetHandler"};

	$dbr->Close() if($dbr);
}

1;
