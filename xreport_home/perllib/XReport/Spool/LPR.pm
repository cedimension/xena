package XReport::Spool::LPR;

use Carp;
require Exporter;


##############################################################################
# Define some constants
#

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

@ISA               = qw(Exporter);
@EXPORT            = qw();
@EXPORT_OK         = qw();
$VERSION           = '1.00';

use base 'XReport::Spool';




sub connect {

    croak 'Usage: $lp->connect()' if (@_ != 1);

    my $self = shift;

    if ($self->connected()) {
        return 1;
    }

    my $sock;

    if ($self->{StrictRFCPorts}) {
        my $port;
        for $port (721..731) {
            $sock = new IO::Socket::INET (
                PeerAddr => $self->{RemoteServer},
                PeerPort => $self->{RemotePort},
                LocalPort => $port,
                Proto => 'tcp',
                ReuseAddr => 1,
            );
            last if (defined($sock));
            last unless ($! =~ /in use|bad file number/i);
        }
        unless (defined($sock)) {
            if ($!) {
                $self->_report("Can't establish connection to remote printer ($!)");
            } else {
                $self->_report("Can't establish connection to remote printer (No local ports available)");
            }
            return undef;
        }
    } else {
        $sock = new IO::Socket::INET (
            PeerAddr => $self->{RemoteServer},
            PeerPort => $self->{RemotePort},
            Proto => 'tcp',
            ReuseAddr => 1,
        );
        unless (defined($sock)) {
            $self->_report("Can't establish connection to remote printer ($!)");
            return undef;
        }
    }
    
    $sock->autoflush(0);
    
    $self->{Socket} = $sock;
    $self->{Mode} = 1;
    
    return 1;
}

sub new_job {

    croak 'Usage: $jobkey = $lp->new_job([$jobid [, $jobhostname]])' if (@_ < 1 || @_ > 3);

    my $self = shift;
    my $jobid = shift;
    
    $jobid = $main::g_job_id unless (defined($jobid));
    
    if ($jobid !~ /^\d+$/ || $jobid > 999) {
        $self->_report("Invalid Job ID specified");
        return undef;
    }
    
    $main::g_job_id = ($jobid + 1) % 1000;

    my $jobname = shift;
    
    $jobname = hostname() unless (defined($jobname));
    
    $jobname =~ s/[\000-\040\200-\377]//g;
    
    my $jobkey = sprintf('%03d%s', $jobid, $jobname);
    
    if (exists($self->{Jobs}->{$jobkey})) {
        $self->_report("Duplicate Job ID specified");
        return undef;
    }
    
    my $user;
    
    if ($^O eq 'MSWin32') {
    	$user = getlogin();
    } else {
    	$user = scalar(getpwuid($>));
    }
    
    $self->{Jobs}->{$jobkey} = {
        JobID => $jobid,
        Jobname => $jobname,
        SentControl => 0,
        SentData => 0,
        UsedDataFileName => 0,
        ControlFileName => "cfA$jobkey",
        DataFileName => "dfA$jobkey",
        PrintingMode => '',
        DataSize => 0,
        DataSent => 0,
        CE => {
            H => hostname(),
            P => $user,
        },
    };
    
    return $jobkey;
}

sub connected {

    croak 'Usage: $lp->connected()' if (@_ != 1);

    my $self = shift;

    undef $self->{Socket} if (defined($self->{Socket}) && ! $self->{Socket}->opened());

    return defined($self->{Socket});
}

sub send_jobs {

    croak 'Usage: $lp->send_jobs($queue)' if (@_ != 2);
    my $self = shift;

    unless ($self->connected()) {
        $self->_report("Not connected");
        return undef;
    }

    unless ($self->{Mode} == 1) {
        $self->_report("Not in ROOT command mode");
        return undef;
    }

    my $queue = shift;
    
    $queue =~ s/[\000-\040\200-\377]//g;
    
    $self->{Socket}->print("\002$queue\n") or do {
        $self->_report("Error sending command ($!)");
        return undef;
    };
    
    $self->{Socket}->flush() or do {
        $self->_report("Error flushing buffer ($!)");
        return undef;
    };
    
    my $result;
    
    $result = $self->{Socket}->getc();
    
    if (length($result)) {
        $result = unpack("C", $result);
    } else {
        $self->_report("Error getting result ($!)");
        return undef;
    };
    
    if ($result != 0) {
        $self->_report("Printer reported an error ($result)");
        return undef;
    }
    
    $self->{Mode} = 2;
    
    return 1;
}

sub job_send_control_file {

    croak 'Usage: $lp->job_send_control_file($jobkey)' unless (@_ == 2);

    my $self = shift;
    
    my $jobkey = shift;
    
    unless (exists($self->{Jobs}->{$jobkey})) {
        $self->_report("Nonexistant Job Key '$jobkey'");
        return undef;
    }

    if ($self->{Jobs}->{$jobkey}->{SentControl}) {
        $self->_report("Already sent control file for '$jobkey'");
        return undef;
    }

    unless ($self->{Mode} == 2) {
        $self->_report("Not in JOB command mode");
        return undef;
    }

    my $cf = "";
    
    my $k;

    my $result;
    
    for $k (qw(C H I J M N P S T U W L 1 2 3 4 c d f g k l n o p r t v z)) {
        next unless (exists($self->{Jobs}->{$jobkey}->{CE}->{$k}));
        $cf .= $k . $self->{Jobs}->{$jobkey}->{CE}->{$k} . "\n";
    }

    $self->{Socket}->print("\002".length($cf)." ".$self->{Jobs}->{$jobkey}->{ControlFileName}."\n") or do {
        $self->_report("Error sending command ($!)");
        return undef;
    };
    
    $self->{Socket}->flush() or do {
        $self->_report("Error flushing buffer ($!)");
        return undef;
    };

    $result = $self->{Socket}->getc();
    
    if (length($result)) {
        $result = unpack("C", $result);
    } else {
        $self->_report("Error getting result ($!)");
        return undef;
    };
    
    if ($result != 0) {
        $self->_report("Printer reported an error ($result)");
        return undef;
    }

    $self->{Socket}->print("$cf\000") or do {
        $self->_report("Error sending control file ($!)");
        return undef;
    };
    
    $self->{Socket}->flush() or do {
        $self->_report("Error flushing buffer ($!)");
        return undef;
    };

    $result = $self->{Socket}->getc();
    
    if (length($result)) {
        $result = unpack("C", $result);
    } else {
        $self->_report("Error getting result ($!)");
        return undef;
    };
    
    if ($result != 0) {
        $self->_report("Printer reported an error ($result)");
        return undef;
    }

    $self->{Jobs}->{$jobkey}->{SentControl} = 1;
}

sub job_send_data {

    croak 'Usage: $lp->job_send_data($jobkey, $data [, $totalsize])' unless (@_ >= 1);
    
    my $self = shift;
    
    if ($self->{Mode} == 2) {
        croak 'JOB Mode Usage: $lp->job_send_data($jobkey, $data [, $totalsize])' unless (@_ >= 2 && @_ <= 3);
    } elsif ($self->{Mode} == 3) {
        croak 'DATA Mode Usage: $lp->job_send_data($jobkey, $data)' unless (@_ == 2);
    } else {
        $self->_report("Not in JOB or DATA command mode");
    }
    
    my $jobkey = shift;
    
    unless (exists($self->{Jobs}->{$jobkey})) {
        $self->_report("Nonexistant Job Key '$jobkey'");
        return undef;
    }

    if ($self->{Jobs}->{$jobkey}->{SentData}) {
        $self->_report("Already sent data file for '$jobkey'");
        return undef;
    }
    
    my $data = shift;

    my $totalsize = shift;

    if (defined($totalsize) && $totalsize !~ /^\d+$/) {
        $self->_report("Size argument must be numeric");
        return undef;
    }

    if ($self->{Mode} == 2) {

	if (defined($totalsize)) {
            $self->{Socket}->print("\003$totalsize ".$self->{Jobs}->{$jobkey}->{DataFileName}."\n") or do {
        	$self->_report("Error sending command ($!)");
        	return undef;
            };
	} else {
            $self->{Socket}->print("\003 ".$self->{Jobs}->{$jobkey}->{DataFileName}."\n") or do {
        	$self->_report("Error sending command ($!)");
        	return undef;
            };
        }

        $self->{Socket}->flush() or do {
            $self->_report("Error flushing buffer ($!)");
            return undef;
        };

        my $result;

        $result = $self->{Socket}->getc();

        if (defined($result) && length($result)) {
            $result = unpack("C", $result);
        } else {
            $self->_report("Error getting result ($!)");
            return undef;
        };

        if ($result != 0) {
            $self->_report("Printer reported an error ($result)");
            return undef;
        }

        $self->{Jobs}->{$jobkey}->{DataSize} = $totalsize if (defined($totalsize));
        $self->{Mode} = 3;        
        $self->{Jobs}->{$jobkey}->{UsedDataFileName} = 1;
    }
    
    if ($self->{Mode} != 3) {
        $self->_report("Can't send data in this mode");
        return undef;
    }
    
    my $job = $self->{Jobs}->{$jobkey};

    my $dsize = length($data);

    if ($job->{DataSize} > 0 && $dsize + $job->{DataSent} > $job->{DataSize}) {
        $data = substr($data, 0, $job->{DataSize} - $job->{DataSent});
    }
    
    if (length($data) > 0) {
        $self->{Socket}->print($data) or do {
            $self->_report("Error sending data ($!)");
            return undef;
        };
    }
    
    $job->{DataSent} += length($data);
    
    if ($job->{DataSent} >= $job->{DataSize}) {

        $job->{SentData} = 1;

        if ($job->{SentControl}) {
            delete $self->{Jobs}->{$jobkey};
        }
        
        $self->{Socket}->print("\000") or do {
            $self->_report("Error sending data ($!)");
            return undef;
        };
        
        $self->{Socket}->flush() or do {
            $self->_report("Error flushing buffer ($!)");
            return undef;
        };

        my $result;

        $result = $self->{Socket}->getc();

        if (length($result)) {
            $result = unpack("C", $result);
        } else {
            $self->_report("Error getting result ($!)");
            return undef;
        };

        if ($result != 0) {
            $self->_report("Printer reported an error ($result)");
            return undef;
        }
    }
    
    if ($dsize != length($data)) {
        $self->_report("Data overflow error");
        return undef;
    }
    
    return 1;
}

1;
