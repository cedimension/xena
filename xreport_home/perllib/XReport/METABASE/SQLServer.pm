#package SQL::Translator::Generator::DDL::SQLServer;
#
#sub primary_key_constraint {
#  'CONSTRAINT ' .
#    $_[0]->quote($_[1]->name || $_[1]->table->name . '_pk') .
#    ' PRIMARY KEY CLUSTERED (' .
#    join( ', ', map $_[0]->quote($_), $_[1]->fields ) .
#    ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 75)';
#}
#
#1;
#

package XReport::METABASE::SQLServer;

use strict;
use Data::Dumper;
use base 'XReport::METABASE';
#use Moo;
no warnings 'redefine';

#sub SQL::Translator::Generator::DDL::SQLServer::primary_key_constraint {
sub primary_key_constraint {
  'CONSTRAINT ' .
    $_[0]->quote($_[1]->name || $_[1]->table->name . '_pky') .
    ' PRIMARY KEY CLUSTERED (' .
    join( ', ', map $_[0]->quote($_), $_[1]->fields ) .
    ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 75)';
}
#override('SQL::Translator::Generator::DDL::SQLServer::primary_key_constraint', \&primary_key_constant);

sub new {
        my $type = shift;
        my $class = ref $type || $type;
        my $self = $class->SUPER::new(@_);
        $self->{RDBMS} = 'SQLServer';
        return $self;
}

sub preliminarySettings{
	return join("", "SET ANSI_NULLS ON\n" , "GO\n", "SET QUOTED_IDENTIFIER ON\n", "GO\n", "SET ANSI_PADDING ON\n");
}

sub ansiSetting{
	my ($self, $type) = (shift, shift);
	return "SET ANSI_PADDING $type";
}

sub _drop {
	my $xrtbl = shift;
	return join(" ", ( "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'$xrtbl')",
                       "AND type in (N'U')) DROP TABLE $xrtbl") ); 
}

sub _truncate {
    my $xrtbl = shift;
    return "TRUNCATE TABLE $xrtbl";
}

sub _rename {
	my ($old, $new) = (shift, shift);
    return "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'${old}') AND type in (N'U')) EXEC sp_rename '${old}', '${new}'";
}

sub dropTable {
    my $self = shift;
    my $args = { @_ };
#    warn 'DROPT: ', XReport::METABASE::Dumper($args), "\n";
        my $xrtbl = (exists($args->{tblpfx}) ? $args->{tblpfx} : 'tbl_');
        $xrtbl .= $args->{table}->{name};
        $xrtbl .= (exists($args->{tblsfx}) ? $args->{tblsfx} : '');
        return _drop($xrtbl)."\nGO\n";
}

sub truncateTable {
    my $self = shift;
    my $args = { @_ };
#    warn 'DROPT: ', XReport::METABASE::Dumper($args), "\n";
    my $xrtbl = (exists($args->{tblpfx}) ? $args->{tblpfx} : 'tbl_');
    $xrtbl .= $args->{table}->{name};
    $xrtbl .= (exists($args->{tblsfx}) ? $args->{tblsfx} : '');
    my @sql = ();
    push @sql, _truncate($xrtbl);
    return join("\nGO\n", @sql )."\nGO\n";
    
}

sub rotateTable {
    my $self = shift;
    my $args = { @_ };
#    warn 'DROPT: ', XReport::METABASE::Dumper($args), "\n";
    my $xrtbl = (exists($args->{tblpfx}) ? $args->{tblpfx} : 'tbl_');
    $xrtbl .= $args->{table}->{name};
    $xrtbl .= (exists($args->{tblsfx}) ? $args->{tblsfx} : '');
    
    my $dropsfx = (%{$args->{sequence}->[0]})[1];
    $dropsfx = 'LAST' unless $dropsfx;

    my $lastsfx = (%{$args->{sequence}->[-1]})[0];
    $lastsfx = 'LAST' unless $lastsfx;
    
    my @sql = ();
    push @sql, _drop("${xrtbl}_$dropsfx");
    push @sql, map { my ($from, $to) = %{$_}; _rename("${xrtbl}_$from", "${xrtbl}_$to") } @{$args->{sequence}}
                   if scalar(@{$args->{sequence}});
    push @sql, _rename("${xrtbl}", "${xrtbl}_$lastsfx");
    
    return join("\nGO\n", @sql )."\nGO\n";
       
}

sub renameTable {
    my $self = shift;
    my $args = { @_ };
    my $table = $args->{table};
       
    my $intbl = (exists($args->{inpfx}) ? $args->{inpfx} : 'tbl_');
    $intbl .= $table->{name};
    $intbl .= (exists($args->{insfx}) ? $args->{insfx} : '');
#    warn 'GENINSERT: ', XReport::METABASE::Dumper($table), "\n";

    my $xrtbl = (exists($args->{outpfx}) ? $args->{outpfx} : 'tbl_');
    $xrtbl .= $table->{name};
    $xrtbl .= (exists($args->{outsfx}) ? $args->{outsfx} : '_LAST');
#    warn 'GENINSERT: ', XReport::METABASE::Dumper($table), "\n";
    return _rename("${intbl}", "${xrtbl}") . "\nGO\n";
}

sub copyTable {
	my $self = shift;
    my $args = { @_ };
    my $table = $args->{table};
       
    my $intbl = (exists($args->{inpfx}) ? $args->{inpfx} : 'tbl_');
    $intbl .= $table->{name};
    $intbl .= (exists($args->{insfx}) ? $args->{insfx} : '');
#    warn 'GENINSERT: ', XReport::METABASE::Dumper($table), "\n";

    my $xrtbl = (exists($args->{outpfx}) ? $args->{outpfx} : 'tbl_');
    $xrtbl .= $table->{name};
    $xrtbl .= (exists($args->{outsfx}) ? $args->{outsfx} : '_temp');
#    warn 'GENINSERT: ', XReport::METABASE::Dumper($table), "\n";

    my $where = (exists($args->{where}) ? delete $args->{where} : undef);

    my ($cols) = @{$table}{qw(rowcols)};
    my $sql = "INSERT INTO $xrtbl (" . join( ',', @$cols ) . ") ";
    $sql .= " SELECT " . join( ', ', @$cols ) . " FROM $intbl"; 
    $sql .= " WHERE $where" if $where;
	return $sql;
}

sub sqlQuoteValue {
	my ( $colt, $inputv ) = @_;
	(my $colv = ( (!$inputv || $inputv eq '.') ? '' : $inputv )) =~ s/'/''/g;
	return 'NULL' if $colv eq "\$\$NULL\$\$"; 
    my $qt = ( $colv =~ /^(?i:\s*getdate|cast|convert|getutcdate)\(.+$/ ? '' 
             : $colt =~ /^(?:[^\s]*int|bit|decimal)$/i ? '' 
             : "'");
    return $qt.$colv.$qt;
}

sub genInsert {
    my $self = shift;
    my $args = { @_ };
    my $table = $args->{table};
       
    my $xrtbl = (exists($args->{tblpfx}) ? $args->{tblpfx} : 'tbl_');
    $xrtbl .= $table->{name};
    $xrtbl .= (exists($args->{tblsfx}) ? $args->{tblsfx} : '');
#    warn 'GENINSERT: ', XReport::METABASE::Dumper($table), "\n";

    my ($where, $cols, $structCols) = @{$table}{qw(keep rowcols columns)};
    my $insert = "INSERT INTO $xrtbl (" . join( ',', @$cols ) . ") ";
    my $sql = '';
    my $ic = 0;
         
    my $tblrows = ( ref($table->{rows}) eq 'ARRAY' ? delete $table->{rows} 
            :  [ map { delete($table->{rows}->{$_}) } sort keys %{$table->{rows}} ]
            );

#   warn "Start to load " . scalar( @{$tblrows} ) . " rows into $xrtbl rquester: ", join('::', (caller())[0,2]), "\n" ;
    while ( scalar @{$tblrows}  ) {

       $sql .= $insert."\n          SELECT ". join( "\nUNION ALL SELECT ",
               map { $ic += 1; my $row = $_; 
               	     join( ", ", map {  sqlQuoteValue( $structCols->{$_}->{type} => $row->{$_} ) } @$cols )  
                   } splice( @{$tblrows}, 0, 100 ) ). " \nGO\n";
    }

    i::logit("Inserted $ic rows into $xrtbl");
    return $sql;
}

sub createDB {
	my $self = shift;
    my $args = { @_ };
    my ($dbn, $datapath, $logpath, $size) = @{$args}{qw(name datapath logpath size)};
    $size = 56 unless $size;
    $logpath = $datapath unless $logpath;
    my $logsize = int(($size/3)+0.5);
    my $cSQL = <<EOSQL;
USE [master]
GO
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'$dbn')
DROP DATABASE [$dbn]
GO
USE [master]
GO
CREATE DATABASE [$dbn] ON  PRIMARY 
( NAME = N'${dbn}_data', FILENAME = N'$datapath\\${dbn}_data.mdf' ,
                             SIZE = ${size}MB , MAXSIZE = UNLIMITED, FILEGROWTH = 10MB )
 LOG ON 
( NAME = N'${dbn}_log', FILENAME = N'$logpath\\${dbn}_log.ldf' ,
                             SIZE = ${logsize}MB , MAXSIZE = UNLIMITED, FILEGROWTH = 10MB )
GO
ALTER DATABASE [$dbn] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [$dbn].[dbo].[sp_fulltext_database] \@action = 'enable'
end
GO
ALTER DATABASE [$dbn] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [$dbn] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [$dbn] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [$dbn] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [$dbn] SET ARITHABORT OFF 
GO
ALTER DATABASE [$dbn] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [$dbn] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [$dbn] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [$dbn] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [$dbn] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [$dbn] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [$dbn] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [$dbn] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [$dbn] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [$dbn] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [$dbn] SET  DISABLE_BROKER 
GO
ALTER DATABASE [$dbn] SET AUTO_UPDATE_STATISTICS_ASYNC ON 
GO
ALTER DATABASE [$dbn] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [$dbn] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [$dbn] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [$dbn] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [$dbn] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [$dbn] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [$dbn] SET  READ_WRITE 
GO
ALTER DATABASE [$dbn] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [$dbn] SET  MULTI_USER 
GO
ALTER DATABASE [$dbn] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [$dbn] SET DB_CHAINING OFF 
GO
EOSQL

}

1;
