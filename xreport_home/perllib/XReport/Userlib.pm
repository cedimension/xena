package U;

use strict;

my $user_lib =  "$ENV{'XREPORT_HOME'}/userlib/perllib";

my $loaded_modules = {};

our $AUTOLOAD;

sub _load_module_ {
  my $module = shift; $module =~ s/.*:://;

  die "USERLIB AUTOLOAD LOOP DETECTED for MODULE $module" if $loaded_modules->{$module};

  if ( -f "$user_lib/$module\.pl" ) {
    do "$user_lib/$module\.pl"; die "LOAD ERROR for $module $@" if $@;
	if ( eval('*U::'.$module.'{CODE}') =~ /CODE/ ) {
	  $loaded_modules->{$module} = 1;
	  return $module;
	}
	else {
	  die "USERLIB ERROR: MODULE $module\.pl DIDN'T CREATE $module ENTRY"; 
	}
  }
  else {
    die "USERLIB ERROR: file \"$user_lib/$module\.pl\" NOT FOUND";
  }
}

sub AUTOLOAD {
  $AUTOLOAD = _load_module_($AUTOLOAD);

  return eval("U::$AUTOLOAD(\@_)");
}

do "$user_lib/INIT.pl"; die "INIT USERLIB ERROR $@\n" if $@;

do "$user_lib/CheckUserTimes.pl"; die "INIT USERLIB ERROR $@\n" if $@;

1;
