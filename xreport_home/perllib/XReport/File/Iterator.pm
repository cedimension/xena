
package XReport::File::Iterator;

use strict;

use Symbol;

sub NextFile {
  my $self = shift; my ($INPUT, $lgrep, $ldir, $ldirList) = @{$self}{qw(INPUT lgrep ldir ldirList)}; my ($FileName, $lFileName);

  while(1) {
    while (($FileName = readdir($INPUT)) ne '') {
      next if $FileName eq "." or $FileName eq "..";
      $lFileName = "$ldir/$FileName";
      if (-f $lFileName) {
        #next if $lFileName !~ /\/SAX.*\.tiff?$/i; 
        if (!$lgrep or $FileName =~ /$lgrep/) {
          return [$lFileName, [stat(_)]];
        }
      }
      elsif (-d $lFileName) {
        push @$ldirList, $lFileName; 
      }
    }
    $self->{ldir} = $ldir = shift @$ldirList; return undef if !$ldir; 
    print "opening $ldir\n";
    closedir($INPUT); opendir($INPUT, $ldir) || die "can't opendir $ldir: $!"; return $ldir;
  }
}

sub new {
  my ($class, $lgrep, $ldirList) = splice(@_,0,3); my $INPUT = gensym(); my $ldir = ''; 

  my $self = {
    INPUT => $INPUT, lgrep => $lgrep, ldirList => $ldirList, ldir => $ldir
  };

  bless $self, $class;
}

1;

__END__
