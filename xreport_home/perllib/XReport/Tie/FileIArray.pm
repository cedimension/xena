
#------------------------------------------------------------
package XReport::Tie::FileIArrayELE;

use strict;

sub NOT_IMPLEMENTED {die __PACKAGE__, ": $_[0] NOT IMPLEMENTED"}

sub TIEARRAY { 
 my ($class, $largs) = @_; my $self = { };
 
 @{$self}{qw(
   lsize item offset ibuffer buffer writer
 )} = 
 @{$largs}{qw(lsize item offset buffer buffer writer)}; bless $self, $class;
}


sub EXISTS { 
  my ($self, $i) = @_;  substr($self->{buffer}, $i*5, 1) eq "\x00" ? undef : 1;
}

sub FETCH { 
  my ($self, $i) = @_;  
  
  my $t = substr($self->{buffer}, $i*5, 5); 
  
  substr($t,0,1) eq "\x00" ? undef : unpack("I", substr($t,1,4));
}

sub FETCHSIZE { 
  $_[0]->{'lsize'};
} 

sub STORE { 
  my ($self, $i, $j) = @_; substr($self->{buffer}, $i*5, 5) = '.' . pack("I", $j); return $j;
} 

sub DELETE { 
  my ($self, $i) = @_; substr($self->{buffer}, $i*5, 5) = "\x00" x 5; return 1;
}

sub CLEAR { 
  my $self = shift; $self->{buffer} = "\x00" x 5 x $self->{lsize}; return 1;
}

sub EXTEND { NOT_IMPLEMENTED('PUSH') } 

sub PUSH { NOT_IMPLEMENTED('PUSH') }
sub POP { NOT_IMPLEMENTED('POP') }
sub SHIFT { NOT_IMPLEMENTED('SHIFT') }
sub UNSHIFT {NOT_IMPLEMENTED('UNSHIFT') }
sub SPLICE { NOT_IMPLEMENTED('SPLICE') }

sub STORESIZE { NOT_IMPLEMENTED('STORESIZE') }

sub DESTROY {
  my $self = shift; 
  my ( 
    $ibuffer, $buffer
  ) = @{$self}{qw(ibuffer buffer)}; return if $buffer eq $ibuffer;
  
  my (
    $offset, $writer
  ) = 
  @{$self}{qw(offset writer)}; $writer->update($offset, $buffer); return undef;
}


#------------------------------------------------------------
package XReport::Tie::FileIArray;

use strict;

use IO::File;
use Scalar::Util qw(weaken);

use File::Temp qw(tempfile);  

sub NOT_IMPLEMENTED {die __PACKAGE__, ": $_[0] NOT IMPLEMENTED"}

sub TIEARRAY { 
 my ($class, $largs) = @_; my ($self, $cacheid, $isTemp, $OUTPUT) = ({}, 0, 0, IO::File->new());
 
 my ($FileName, $size, $lsize, $ioffset, $iobuffer) = @{$largs}{qw(FileName size lsize ioffset iobuffer)}; 
 
 $lsize ||= 1; $ioffset ||= 0; 

 if (!$iobuffer) {
   if ($FileName ne '') {
     $size ||= (-s $FileName) / ($lsize*5);
     $OUTPUT->open((-e $FileName ? "+<" : "+>").$FileName) 
      or 
     die("OPEN OUTPUT ERROR DETECTED \"$FileName\" $!"); $OUTPUT->binmode(); 
   }
   else {
     ($OUTPUT, $FileName) = tempfile(SUFFIX => ".$lsize.IARRAY"); $isTemp = 1;
   }
 }
 else {
   $OUTPUT = IO::String->new($iobuffer); $OUTPUT->binmode();
 }

 my ($pendings, $pendingq) = (0, {});
 
 @{$self}{qw(
   ioffset size lsize isTemp FileName OUTPUT pendings pendingq 
 )} = 
 ($ioffset, $size, $lsize, $isTemp, $FileName, $OUTPUT, $pendings, $pendingq); bless $self, $class;
}

sub lseek {
  my ($self, $to, $lref) = @_; my $OUTPUT = $self->{OUTPUT}; my $at;

  if (($at = $OUTPUT->tell()) != $to) {
    $at = $OUTPUT->seek($to,0);
  }

  return $at;
}

sub flushq {
  my $self = shift; my ($OUTPUT, $pendingq) = @{$self}{qw(OUTPUT pendingq)};

  for my $offset (sort {$a <=> $b} keys(%$pendingq)) {
    $self->lseek($offset); 
    $OUTPUT->write($pendingq->{$offset});
  }

  @{$self}{qw(pendings pendingq)} = (0, {}); 
}

sub update {
  my ($self, $offset, $buffer) = @_; my $pendingq = $self->{pendingq};
  
  if (!exists($pendingq->{$offset})) {
    $self->{pendings} += 1;
  }
  else {
    die "qqqqqqqqqq\n";
  }
  $pendingq->{$offset} = $buffer; 
  
  $self-> flushq() if $self->{pendings} >= 256; return 1;
}

sub FETCH { 
  my ($self, $i) = @_; my ($OUTPUT, $size, $lsize, $ioffset) = @{$self}{qw(OUTPUT size lsize ioffset)};
  
  $self->{'size'} = $i+1 if $i+1 > $size;

  if ($lsize > 1) {
    my $offset = $ioffset + $i*$lsize*5; $self->lseek($offset); my $pendingq = $self->{pendingq};
    
    my ($buffer, $writer) = ("", $self); weaken($writer);
    
    if (exists($pendingq->{$offset})) {
      $buffer = $pendingq->{$offset};
    } 
    else {
      $OUTPUT->read($buffer, $lsize*5); 
    }
    tie my @list, 'XReport::Tie::FileIArrayELE', {
      lsize => $lsize, item => $i, offset => $offset, buffer => $buffer, writer => $writer 
    };

	return \@list;
  }
  
  $self->lseek($ioffset + $i*5, 0); $OUTPUT->read(my $t, 5); 

  substr($t,0,1) eq "\x00" ? undef : unpack("I", substr($t,1));
}

sub FETCHSIZE { 
  my $self = shift; return $self->{'size'};
} 

sub STORE { 
  my ($self, $i, $j) = @_; my ($OUTPUT, $size, $lsize) = @{$self}{qw(OUTPUT size lsize)};
  
  $self->{'size'} = $i+1 if $i+1 > $size; 
  
  return if $lsize > 1;
  
  $self->lseek($i*5, 0); $OUTPUT->write('.'. pack("I", $j)); 
}

sub PUSH { 
  my $self = shift; my ($OUTPUT, $lsize) = @{$self}{qw(OUTPUT lsize)};
  
  die("PUSH NOT MANAGED FOR LSIZE > 1.") if $lsize > 1;

  while(@_) {
    my $j = shift; $self->{'size'} += 1; 
  
    my $i = $self->{'size'}-1;
  
    $self->lseek($i*5, 0); $OUTPUT->write('.'. pack("I", $j));
  }
}

sub STORESIZE { 
  die "??? STORESIZE $_[1]??\n";
  my $self = shift; $self->{'size'} = shift;
}   

sub EXISTS { NOT_IMPLEMENTED('EXISTS') }  
sub DELETE { NOT_IMPLEMENTED('DELETE') }  

sub CLEAR { 
  my $self = shift;$self->{'size'} = 0; 
}

sub POP { NOT_IMPLEMENTED('POP') }
sub SHIFT { NOT_IMPLEMENTED('SHIFT') }
sub UNSHIFT { NOT_IMPLEMENTED('UNSHIFT') }
sub SPLICE { NOT_IMPLEMENTED('SPLICE') }

sub EXTEND { 
  die "??? EXTEND @_??\n";
}

sub DESTROY { 
  my $self = shift; my ($OUTPUT, $FileName, $isTemp, $pendings) = @{$self}{qw(OUTPUT FileName isTemp pendings)};
  
  goto EXIT if $isTemp or !$pendings;

  $self->flushq();

  EXIT: $OUTPUT->close(); unlink $FileName if $FileName ne '' and $isTemp; 
}

1;
