package XReport::Distillr;

use strict;
use Symbol;

use Carp;
use File::Basename;
use File::Copy qw();

#use Win32::Registry;
use Win32::TieRegistry qw(KEY_READ);

use XReport::SUtil;

#######################################################################
#
#######################################################################
#use constant ERROR_DISTILLER_NOT_INSTALLED	=> 255+1;
#use constant ERROR_DISTILLER_NOT_FOUND      => 255+5;

#use constant ERROR_DISTILLER_NOT_ACTIVE     => 255+2;
#use constant ERROR_REGISTER_CLASS_EX        => 255+3;
#use constant ERROR_CREATE_WINDOW            => 255+6;
#use constant ERROR_SEND_MESSAGE             => 255+7;
#use constant ERROR_CREATE_PROCESS_FAILED    => 255+8;
#use constant ERROR_TERMINATE_PROCESS_FAILED => 255+9;
#use constant ERROR_DISTILLER_IS_TERMINATED	=> 255+10;

#use constant HKLMPathsKeys => "SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\App Paths";

BEGIN {
}

END {
}

our $VERSION = '0.10';
use File::Basename qw();

sub FileToPdf {
  my ($PSFileName, $PdfFileName) = (shift, shift);
  my ($basedir, $afpdir) = $XReport::cfg->{qw(basedir afpdir)}; 
  my $mode = ($XReport::cfg->{distiller}->{mode} or '_UNDEF_');
  
  $afpdir = "$basedir/afp" if !$afpdir;
  $basedir =~ s/\//\\/g; $afpdir =~ s/\//\\/g;
  my $origin = $basedir;
  $origin = $ENV{XREPORT_HOME} unless -e "$basedir\\bin\\xreport.ps";
  my $psdir = dirname($PSFileName);
  my @filelvls = split(/\./, basename($PdfFileName));
  pop @filelvls;pop @filelvls;pop @filelvls;
  my $jrid = pop @filelvls;
  my $jrname = join('.', @filelvls);

  (my $psdir = File::Basename::dirname($PSFileName)) =~ s/\//\\/g;
  my $lcl_joboptions = "$psdir\\$jrname.$jrid.joboptions";
  File::Copy::cp("$origin\\bin\\xreport.joboptions", $lcl_joboptions) unless -e $lcl_joboptions;
  croak("PROCESS ERROR - DISTILLER CONTROL FILE \"$lcl_joboptions\" NOT FOUND !!") unless -e $lcl_joboptions; 

  ($PdfFileName = $PSFileName) =~ s/\.\w+$/.pdf/ unless $PdfFileName;

  my $PSINPUT = gensym();
  if ( -e $PSFileName && open ($PSINPUT, ">>$PSFileName") ) {
    print $PSINPUT
    "(\$main::currentsystemparams = {\\n) print\n"
          ," currentsystemparams { \%forall\n"
	  ,"    exch dup \% name first\n"
	  ,"    dup length string cvs print ( => [qw�) print\n"
	  ,"    exch ==\n"
	  ,"    (�],\\n) print\n"
	  ,"  } forall\n"
      ,"  (vmusage => { qw�val1 ) print vmstatus 10 string cvs print \n"
      ,"  ( val2 ) print 10 string cvs print\n"
      ,"  ( val3 ) print 10 string cvs print (� },\\n) print\n"
      ,"(};\\n) print flush\n"
    ;
#      "\<3C\> print (currentsystemparams) print \<3E\> print (\\n) print\n"
#          ," statusdict { \%forall\n"
#	  ,"    exch dup \% name first\n"
#	  ,"    \<3c\>  print dup length string cvs print \<3E\> print (\\n) print\n"
#	  ,"    exch ==\n"
#	  ,"    \<3c\>  print (/) print dup length string cvs print \<3E\> print (\\n) print\n"
#	  ,"  } forall\n"
#      ,"  \<3C\> print (vmusage val1=\") print vmstatus 10 string cvs print (\" val2=\") print 10 string cvs print (\" val3=\") print 10 string cvs print (\" /) print \<3E\> print (\\n) print\n"
#      ,"\<3C\> print (/statusdict) print \<3E\> print () == flush\n"
#    ;
    close $PSINPUT;  
  }

  #my @infiles = ( @_, $PSFileName);
  my @infiles = ( @_ );
  push @infiles , $PSFileName unless $mode eq "NO_IN_PS";

  my $distreg = Win32::TieRegistry->new( 
        "LMachine\\Software\\Microsoft\\Windows\\CurrentVersion\\App Paths\\AcroDist.exe",
        { Access=>KEY_READ(), Delimiter=>"\\" });
  die("CANNOT OPEN Distiller REGISTRY ENTRY: $^E") unless $distreg;      
  my $progr = $distreg->GetValue('');      
#  $main::HKEY_LOCAL_MACHINE->Open( "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths", 
#	  my $distreg ) or die("CANNOT OPEN Distiller REGISTRY ENTRY: $^E");
#  
#  $distreg->QueryValue("AcroDist.exe", my $progr);
  
  (my $version) = $progr =~ /Acrobat +([\d\.]+)/;

  i::warnit("DISTILLER: $progr - VERSION: $version F1: $PSFileName OTHERS: ". join('::', @_));
  
#  my $dParms = ''; # '/F';  (for version 8+)
#  $dParms .= " /J \"$psdir\\$jrname.$jrid.joboptions\" /N /Q";
#  i::warnit("DISTILLER PARMS: $dParms");
#  my $DistRetCode = system("\"$progr\" $dParms /O \"$PdfFileName\" \"". join("\" \"", @infiles) ."\"");
  my @acroParms = split(/ /, ($XReport::cfg->{distiller}->{acroargs} || '')); # ('/F') for v8+
  
  push @acroParms, ( '/J', "\"$lcl_joboptions\"", '/N', '/Q'
                   , '/O', "\"$PdfFileName\""
                   , map { "\"$_\""} @infiles); 
  i::warnit("DISTILLER PARMS: ".join(' ', @acroParms)); 
  my $DistRetCode = XReport::SUtil::spawnProcessAndWait($progr, 10800000, @acroParms);

  (my $PSLOGName = $PSFileName) =~ s/\.ps$/\.log/i;
  my $PSLOG = gensym();
  if ( -e $PSLOGName && open ($PSLOG, "<$PSLOGName") ) {
	 my $stats = ''; my $stats_begin = qr/^\s*\$main::currentsystemparams\s*=\s*\{\s*$/;
	 while ( <$PSLOG> ) {
		 $stats = $_ if ($_ =~ /$stats_begin/ );
		 warn "DISTILLER LOG: $_" unless $stats;;
		 $stats .= $_ if $stats && $_ !~ /$stats_begin/;
	 }
	 if ($stats) {
	   $stats =~ s/^\s*\$main::currentsystemparams\s*=\s*(\{.+\}).*$/$1/s;
	   eval "\$main::distllr_stats = $stats";
 	 }
	 close $PSLOG;
 } 
  
  return $DistRetCode;
  
}

sub INIT_Distiller {
}

sub TERMINATE_Distiller {
}

1;
