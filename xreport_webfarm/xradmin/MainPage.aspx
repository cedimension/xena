﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainPage.aspx.cs" Inherits="XReportSearch.MainPage"
    Theme="XReportSearchTheme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
    <title>XReport Search</title>
    <link href="~/App_Themes/XReportSearchAjaxControls.css?2" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/XReportSearchStyle.css?4" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/Menu.css?4" rel="stylesheet" type="text/css" />
</head>
<body onload="resize()" onresize="resize()" onclick="resize()" onbeforeprint="resize()">
    <script language="javascript" type="text/javascript">

        function fnOnUpdateValidators() {
            var count = 0;
            for (var i = 0; i < Page_Validators.length; i++) {
                var val = Page_Validators[i];
                var ctrl = document.getElementById(val.controltovalidate);
                if (ctrl != null && ctrl.style != null) {
                    if (!val.isvalid) {
                        ctrl.style.borderColor = "Red";
                        ctrl.className = "comboboxError";
                        count++;
                    }
                    else {
                        ctrl.style.borderColor = "#518012";
                        ctrl.className = "combobox";
                    }
                }
            }
        }

        function resize() {
            var frame = document.getElementById("MainDiv");
            var page = document.getElementById("panelTable");
            var windowheight = document.documentElement.clientHeight;
            var windowwidth = document.documentElement.clientWidth;
            frame.style.height = windowheight + "px";
            if (document.getElementById("txtHidData") != null) {
                document.getElementById("txtHidData").value = windowheight;
            }
            if (document.getElementById("txtHidData2") != null) {
                document.getElementById("txtHidData2").value = windowwidth;
            }
            frame.style.height = windowheight + "px";
            if (page.style.height != null) {
                page.style.height = (windowheight - 102) + "px";
            }
        }
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"
        AsyncPostBackTimeout="600" />
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').add_showing(onshowing);
            resize();
        }

        function onshowing() {
            if ($find('ModalProgress') != null) {
                var windowwidth = document.documentElement.clientWidth;
                var windowheight = document.documentElement.clientHeight;
                $find('ModalProgress').set_X((parseInt(windowwidth) / 2) - 100);
                $find('ModalProgress').set_Y((parseInt(windowheight) / 2) - 100);
            }
        }

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

        function beginRequest() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').show();
        }

        function endRequest() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').hide();
        }

        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            if ($get('divCustomGrid') != null) {
                xPos = $get('divCustomGrid').scrollLeft;
                yPos = $get('divCustomGrid').scrollTop;
            }
        }

        function EndRequestHandler(sender, args) {
            if ($get('divCustomGrid') != null) {
                $get('divCustomGrid').scrollLeft = xPos;
                $get('divCustomGrid').scrollTop = yPos;
            }
        }

        function hideF() {
            var tab1 = document.getElementById('tab1');
            var tab2 = document.getElementById('tab2');
            var b_Tab1 = document.getElementById('b_Tab1');
            var b_Tab2 = document.getElementById('b_Tab2');
            tab1.style.display = 'none';
            tab2.style.display = 'block';
            b_Tab1.className = 'buttonMenu';
            b_Tab2.className = 'buttonMenuSelected';
            var windowheight = document.documentElement.clientHeight;
            tab2.style.height = (windowheight - 102) + "px";
        }

        function hideFQ() {
            var tab1 = document.getElementById('tab1');
            var tab2 = document.getElementById('tab2');
            var b_Tab1 = document.getElementById('b_Tab1');
            var b_Tab2 = document.getElementById('b_Tab2');
            tab2.style.display = 'none';
            tab1.style.display = 'block';
            b_Tab1.className = 'buttonMenuSelected';
            b_Tab2.className = 'buttonMenu';
        }
    </script>
    <input type="hidden" clientidmode="Static" id="txtHidData" runat="server" />
    <input type="hidden" clientidmode="Static" id="txtHidData2" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div id="MainDiv" class="main">
                <div class="headerContainer" id="headerDiv">
                    <div class="headerLeft">
                        <div class="headerTitleLeft">
                            <asp:Button Text="Online" BorderStyle="None" ID="b_Tab1" runat="server" OnClientClick="hideFQ();return false;"
                                CssClass="buttonMenuSelected" />
                            <asp:Button Text="History" BorderStyle="None" ID="b_Tab2" runat="server" OnClientClick="hideF();return false;"
                                CssClass="buttonMenu" />
                        </div>
                    </div>
                    <div class="headerRight">
                    </div>
                </div>
                <div class="headerContainerTitleInner" id="Div1">
                    <div class="headerTitleLeftInner">
                        <asp:Label ID="lb_User" runat="server" SkinID="LabelFilter" Width="500px"></asp:Label>
                        <asp:Label ID="Label6" Width="30px" runat="server"></asp:Label>
                        <asp:Label ID="lb_Ambiente" runat="server" SkinID="LabelFilter"></asp:Label>
                    </div>
                </div>
                <asp:Panel runat="server" ID="panelTable" class="page">
                    <div id="tab1" style="display: block; height: 100%; width: 100%;">
                        <iframe runat="server" id="frame" height="100%" width="100%" visible="true"></iframe>
                    </div>
                    <div id="tab2" style="display: block; height: 0%; width: 100%">
                        <iframe runat="server" id="frameH" height="100%" width="100%" visible="true"></iframe>
                    </div>
                </asp:Panel>
            </div>
            <asp:UpdateProgress ID="UpdateProgress" runat="server" DisplayAfter="0">
                <ProgressTemplate>
                    <asp:Panel ID="panelUpdateProgress" runat="server">
                        <div>
                            <img id="Img1" src="~/Images/loader.gif" alt="" runat="server" />
                        </div>
                    </asp:Panel>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <ajax:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
                BackgroundCssClass="modalProgress" PopupControlID="panelUpdateProgress" BehaviorID="ModalProgress">
            </ajax:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
