﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StartPage.aspx.cs" Inherits="XReportSearch.StartPage" Theme="XReportSearchTheme" EnableViewState="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/SearchOnline.ascx" TagPrefix="ced" TagName="search" %>
<%@ Register Src="~/SearchHistory.ascx" TagPrefix="cedHS" TagName="searchHS" %>
<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <title>XReport Search</title>
    <link href="~/App_Themes/XReportSearchAjaxControls.css?2" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/XReportSearchStyle.css?4" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/Menu.css?4" rel="stylesheet" type="text/css" />
</head>
<body onload="resize()" onresize="resize()" onclick="resize()" onunload="popup_after_unload()">
    <script  type="text/javascript">

        function popup_after_unload() {
            var url = 'Error.aspx?forceCloseSession=Y';
            //myWindowHandle = window.open(url, 'windowname', 'width=0,height=0'); 
            myWindowHandle = window.open(url, "_blank", "toolbar=no,scrollbars=no,resizable=no,top=0,left=0,width=10,height=10");
            myWindowHandle.blur();
            window.focus();
        }

        //Number of Reconnects
        //var count = 0;
        //Maximum reconnects setting
        //var max = 30;
        //function Reconnect() {
            //count++;
            //if (count < max) {
        //window.status = 'Link to Server Refreshed ' + count.toString() + ' time(s)';
        //var img = new Image(1, 1);
        //img.src = '~/SessionEnd.aspx';
        //}
        //}
        //window.setInterval('Reconnect()', 5000); //Set to length required

        function resize2() {
            var frame = document.getElementById("sp_MainDiv");
            var windowheight = document.documentElement.clientHeight;
            var windowwidth = document.documentElement.clientWidth;
            if (frame != null && frame.style.height != null) {
                frame.style.height = windowheight - 300 + "px";
            }
            if (windowwidth < 800) {
                frame.style.fontSize = "0.4em";
            }
            else if (windowwidth > 2000) {
                frame.style.fontSize = "2em";
            }
            else {
                frame.style.fontSize = "0.6em";
            }
        }

        function resize3() {
        }

        function resize() {
            var frame = document.getElementById("sp_MainDiv");
            var page = document.getElementById("sp_panelTable");
            var S1 = document.getElementById("sp_S1");
            var page2 = document.getElementById("sp_panelTable2");
            var grid = document.getElementById("divCustomGrid");
            var windowheight = document.documentElement.clientHeight;
            var windowwidth = document.documentElement.clientWidth;
            if (frame != null && frame.style.height != null) {
                frame.style.height = windowheight + "px";
            }
            if (document.getElementById("sp_txtHidData") != null) {
                document.getElementById("sp_txtHidData").value = windowheight;
            }
            if (document.getElementById("sp_txtHidData2") != null) {
                document.getElementById("sp_txtHidData2").value = windowwidth;
            }
            if (frame != null && frame.style.height != null) {
                frame.style.height = (windowheight) + "px";
            }
            if (page != null && page.style.height != null) {
                page.style.height = (windowheight) + "px";
            }
            if (page2 != null && page2.style.height != null) {
                page2.style.height = (windowheight) + "px";
            }
            if (grid != null && grid.style.height != null) {
                grid.style.height = (windowheight - 150) + "px";
            }
            if (windowwidth < 800) {
                frame.style.fontSize = "0.4em";
            }
            else if (windowwidth > 2000) {
                frame.style.fontSize = "2em";
            }
            else {
                frame.style.fontSize = "0.7em";
            }
        }

    </script>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"
            AsyncPostBackTimeout="600" />
        <script type="text/javascript">
            function pageLoad() {
                if ($find('sp_ModalProgress') != null)
                    $find('sp_ModalProgress').add_showing(onshowing);
                resize();
            }

            function onshowing() {
                if ($find('sp_ModalProgress') != null) {
                    var windowwidth = document.documentElement.clientWidth;
                    var windowheight = document.documentElement.clientHeight;
                    $find('sp_ModalProgress').set_X((parseInt(windowwidth) / 2));
                    $find('sp_ModalProgress').set_Y((parseInt(windowheight) / 2));
                }
            }

            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

            function beginRequest() {
                if ($find('sp_ModalProgress') != null)
                    $find('sp_ModalProgress').show();
            }

            function endRequest() {
                if ($find('sp_ModalProgress') != null)
                    $find('sp_ModalProgress').hide();
            }

            var xPos, yPos;
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(BeginRequestHandler);
            prm.add_endRequest(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                if ($get('divCustomGrid') != null) {
                    xPos = $get('divCustomGrid').scrollLeft;
                    yPos = $get('divCustomGrid').scrollTop;
                }
            }

            function EndRequestHandler(sender, args) {
                if ($get('divCustomGrid') != null) {
                    $get('divCustomGrid').scrollLeft = xPos;
                    $get('divCustomGrid').scrollTop = yPos;
                }
            }

            function hideF() {
                var b_Tab1 = document.getElementById('sp_b_Tab1');
                var b_Tab2 = document.getElementById('sp_b_Tab2');
                var panel = document.getElementById('sp_panelTable');
                var panelH = document.getElementById('sp_panelTable2');
                b_Tab1.className = 'buttonMenu';
                b_Tab2.className = 'buttonMenuSelected';
                panel.style.display = 'none';
                panelH.style.display = 'inline';
            }

            function hideFQ() {
                var b_Tab1 = document.getElementById('sp_b_Tab1');
                var b_Tab2 = document.getElementById('sp_b_Tab2');
                var panel = document.getElementById('sp_panelTable');
                var panelH = document.getElementById('sp_panelTable2');
                b_Tab1.className = 'buttonMenuSelected';
                b_Tab2.className = 'buttonMenu';
                panelH.style.display = 'none';
                panel.style.display = 'inline';
            }
        </script>

        <asp:UpdatePanel ID="sp_UpdatePanel1" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div id="sp_MainDiv" class="main" style="min-height: 100%">
                    <div class="headerContainer" id="sp_headerDiv">
                        <div class="headerLeft">
                            <div class="headerTitleLeft">
                                <asp:Button Text="Online" BorderStyle="None" ID="sp_b_Tab1" runat="server" OnClientClick="hideFQ();return false;"
                                    CssClass="buttonMenuSelected" CausesValidation="false" />
                                <asp:Button Text="History" BorderStyle="None" ID="sp_b_Tab2" runat="server" OnClientClick="hideF();return false;"
                                    CssClass="buttonMenu" CausesValidation="false" />
                            </div>
                        </div>
                        <div class="headerRight">
                        </div>
                    </div>
                    <div class="headerContainerTitleInner" id="sp_Div1">
                        <div class="headerTitleLeftInner">
                            <asp:Label ID="sp_lb_Ambiente" runat="server" SkinID="LabelAuto"></asp:Label>
                            <asp:Label ID="sp_Label6" Width="20px" runat="server"></asp:Label>
                            <asp:Label ID="sp_lb_User" runat="server" SkinID="LabelAuto" ></asp:Label>
                            <asp:Label ID="Label1" Width="20px" runat="server"></asp:Label>
                            <asp:Label ID="sp_lb_Profilo" runat="server" SkinID="LabelAuto"></asp:Label>
                            <asp:Label ID="Label2"  Width="20px" runat="server"></asp:Label> 
                            <asp:Label ID="sp_lb_Profilo2" runat="server" SkinID="LabelAuto"></asp:Label>
                            <asp:Label ID="Label3"  Width="20px" runat="server"></asp:Label> 
                            <asp:Label ID="sp_lb_OrganizationUnit" runat="server" SkinID="LabelAuto"></asp:Label>                            
                            <asp:Button Text="" BorderStyle="None" ID="sp_Button1" runat="server" Visible="false" />
                            <asp:Button Text="" BorderStyle="None" ID="sp_Button2" runat="server" Visible="false" />
                        </div>
                    </div>
                    <asp:Panel runat="server" ID="sp_panelTable" CssClass="page">
                        <ced:search runat="server" ID="sp_S1" EnableTheming="true"></ced:search>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="sp_panelTable2" CssClass="page">
                        <cedHS:searchHS runat="server" ID="sp_S2" EnableTheming="true"></cedHS:searchHS>
                    </asp:Panel>
                </div>
                <asp:UpdateProgress ID="sp_UpdateProgress" runat="server" DisplayAfter="0">
                    <ProgressTemplate>
                        <asp:Panel ID="sp_panelUpdateProgress" runat="server">
                            <div>
                                <img id="sp_Img1" src="~/Images/loader.gif" alt="" runat="server" />
                            </div>
                        </asp:Panel>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <ajax:ModalPopupExtender ID="sp_ModalProgress" runat="server" TargetControlID="sp_panelUpdateProgress"
                    BackgroundCssClass="modalProgress" PopupControlID="sp_panelUpdateProgress" BehaviorID="sp_ModalProgress">
                </ajax:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
        <input type="hidden" clientidmode="Static" id="sp_txtHidData" runat="server" />
        <input type="hidden" clientidmode="Static" id="sp_txtHidData2" runat="server" />
        <img id="imgSessionAlive" width="1" height="1" src="~/Images/loader.gif" alt="" runat="server" />
    </form>
</body>
</html>
