﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchOnline.ascx.cs" Inherits="XReportSearch.SearchOnline" EnableViewState="true" %>
<%@ Register Namespace="CustomControls" TagPrefix="cedim" Assembly="XReportSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<script type="text/javascript">

    function fnOnUpdateValidators() {
        var count = 0;
        for (var i = 0; i < Page_Validators.length; i++) {
            var val = Page_Validators[i];
            var ctrl = document.getElementById(val.controltovalidate);
            if (ctrl != null && ctrl.style != null) {
                if (!val.isvalid) {
                    ctrl.style.borderColor = "Red";
                    ctrl.className = "comboboxError";
                    count++;
                }
                else {
                    ctrl.style.borderColor = "#518012";
                    ctrl.className = "combobox";
                }
            }
        }
    }

    function AtLeastOneContact_ClientValidate(source, args) {
        if (document.getElementById("<%= tb_jobname.ClientID %>").value == "" &&
            document.getElementById("<%= tb_jobname2.ClientID %>").value == "" &&
                document.getElementById("<%= tb_jobnumber.ClientID %>").value == "" &&
                document.getElementById("<%= tb_name.ClientID %>").value == "") {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
    }


    function AtLeast3CharsInJobname_ClientValidate(source, args) {
        var mystrsource = document.getElementById("<%= tb_jobname.ClientID %>").value;
        mystrsource = mystrsource.replace(/[%*]/g, "");
        if ((mystrsource == "") || mystrsource.length < 3 ){
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
    }

    

    function Date_ClientValidate(source, args) {
            var smallDate = document.getElementById("<%= tb_data1.ClientID %>").value;
            var largeDate = document.getElementById("<%= tb_data2.ClientID %>").value;
            var smallDateArr = Array();
            var largeDateArr = Array();
            smallDateArr = smallDate.split('/');
            largeDateArr = largeDate.split('/');
            var smallDt = smallDateArr[0];
            var smallMt = smallDateArr[1];
            var smallYr = smallDateArr[2];
            var largeDt = largeDateArr[0];
            var largeMt = largeDateArr[1];
            var largeYr = largeDateArr[2];
            //alert(smallDt + "-" + smallMt + "-" + smallYr);
            //alert(largeDt + "-" + largeMt + "-" + largeYr);
            if (smallYr > largeYr) 
                args.IsValid = false;
            else if (smallYr == largeYr && smallMt > largeMt) 
                args.IsValid = false;
            else if (smallYr == largeYr && smallMt == largeMt && smallDt > largeDt)
                args.IsValid = false;
            else
                args.IsValid = true;
    }

    function button_click(objTextBoxID, objBtnID) {
        //alert("uffa" + objTextBoxID + "__" + objBtnID);
        if (window.event.keyCode == 13) { 
            document.getElementById(objBtnID).focus();
            document.getElementById(objBtnID).click();
            document.getElementById(objTextBoxID).focus();
        }
    }

    function mydump(arr, level) {
        var dumped_text = "";
        if (!level) level = 0;
        if (level > 2)
            return dumped_text;
        var level_padding = "";
        for (var j = 0; j < level + 1; j++) level_padding += "    ";

        if (typeof (arr) == 'object') {
            for (var item in arr) {
                var value = arr[item];

                if (typeof (value) == 'object') {
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += mydump(value, level + 1);
                } else {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }
            }
        } else {
            dumped_text = "===>" + arr + "<===(" + typeof (arr) + ")";
        }
        return dumped_text;
    }

    function getSecondValue(source, args) {
        //var dumped = mydump(source, 0);
        document.getElementById("<%= tb_name.ClientID %>").value = args["_value"];
        SomeMethodToGetAdditionalData(args.get_value(), OnSucceded, OnFailed);
        //alert("ecco" + dumped);
        //alert("ecco1" + args["_value"]);
    }
 
    function SomeMethodToGetAdditionalData(value, fun1,fun2) {
        alert("dentro" + document.getElementById("<%= tb_name.ClientID %>").value);

    }

    function OnSucceded() {
        alert("dentro Succe");

    }

    function OnFailed() {
        alert("dentro Fialed");

    }
         
    function App_Context_Key(source, e) {
        
        if (source["_contextKey"].match(/tb_jobname/i)) {
            source.set_contextKey("tb_jobname" + '|' + $get("<%=tb_name.ClientID%>").value);
        } else {
            source.set_contextKey("tb_name" + '|' + $get("<%=tb_jobname.ClientID%>").value);
        }
        
    }

</script>
<asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server" RenderMode="Inline">
    <ContentTemplate>
        <!--<div id="MainDiv" class="main" style="height:100%; overflow:scroll" >-->
        <!--<asp:Panel runat="server" ID="panelTable" Height="100%">-->
        <table class="page" style="height: 100%; min-height: 100%">
            <tr id="PageDiv2" style="height: 100%">
                <td class="pageLeft" style="height: 100%">
                    <ajax:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="pShow"
                        CollapseControlID="pHide" ExpandControlID="pHide" Collapsed="false" ImageControlID="imgArrows"
                        ExpandDirection="Horizontal" ExpandedImage="~/Images/hide.png" CollapsedImage="~/Images/show.png">
                    </ajax:CollapsiblePanelExtender>
                    <div>
                        <asp:Panel ID="pShow" runat="server" Style="height: 100%">
                            <table>
                                <tr class="data">
                                    <td></td>
                                    <td class="buttons">
                                        <asp:Button ID="b_assign" CausesValidation="true" runat="server" Text="Search" Visible="true"
                                            CssClass="buttonBig" OnClick="buttonSearch_Click" ValidationGroup="OnlineValidators" />
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td>
                                        <asp:Label ID="lb_group" runat="server" Text="Group" SkinID="LabelFilter" Visible="false"></asp:Label>
                                    </td>
                                    <td>
                                        <ajax:ComboBox ID="ddl_group" runat="server" AutoPostBack="True" RenderMode="Block"
                                            DropDownStyle="DropDown" Visible="false" CaseSensitive="False" CssClass="comboboxDoc"
                                            onkeydown="OnKeyDownCombo()" OnSelectedIndexChanged="ddl_group_SelectedIndexChanged">
                                        </ajax:ComboBox>
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td>
                                        <asp:Label ID="l_specUser" runat="server" Visible="false" Text="Spec. User" SkinID="LabelFilter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tb_specUser" Visible="false" runat="server" SkinID="TextboxSmall"
                                            TabIndex="1" OnTextChanged="tb_specUser_TextChanged"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="data">
                                <td>
                                        <asp:Label ID="lb_jobname" runat="server" Text="Job Name" SkinID="LabelFilter"></asp:Label>
                                </td>
                                    <td>
                                        <asp:TextBox ID="tb_jobname" runat="server" SkinID="TextboxSmallLeft" TabIndex="2" OnTextChanged="tb_jobname_TextChanged" ></asp:TextBox>
                                        <ajax:AutoCompleteExtender runat="server" id="ace_tb_jobname" UseContextKey="True" OnClientPopulating="App_Context_Key" ContextKey="tb_jobname" DelimiterCharacters="" TargetControlID="tb_jobname" ServiceMethod="GetInnerCompletionList2" MinimumPrefixLength="0" CompletionSetCount="20" EnableCaching="false" CompletionInterval="100" FirstRowSelected="false" CompletionListCssClass = "completionList" CompletionListHighlightedItemCssClass = "itemHighlighted" CompletionListItemCssClass = "listItem" />
                                        <asp:TextBox ID="tb_jobname2" runat="server" SkinID="TextboxSmallRight" TabIndex="3" OnTextChanged="tb_jobname2_TextChanged" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="Report Name" SkinID="LabelFilter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tb_name" runat="server" SkinID="TextboxSmall" TabIndex="4" OnTextChanged="tb_name_TextChanged"></asp:TextBox>
                                        <ajax:AutoCompleteExtender runat="server" id="ace_tb_name" UseContextKey="True" OnClientPopulating="App_Context_Key" ContextKey="tb_name" DelimiterCharacters="" TargetControlID="tb_name" ServiceMethod="GetInnerCompletionList2" MinimumPrefixLength="0" CompletionSetCount="20" EnableCaching="false" CompletionInterval="100" FirstRowSelected="false" CompletionListCssClass = "completionList" CompletionListHighlightedItemCssClass = "itemHighlighted" CompletionListItemCssClass = "listItem" />
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Job Number" SkinID="LabelFilter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tb_jobnumber" runat="server" SkinID="TextboxSmall" TabIndex="5" OnTextChanged="tb_jobnumber_TextChanged"></asp:TextBox>
                                    </td>
                                </tr> 
                                <tr class="data" id="tr_prof" runat="server">
                                    <td>
                                        <asp:Label ID="l_prof" runat="server" Text="Profile" SkinID="LabelFilter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tb_prof" runat="server" SkinID="TextboxSmall" TabIndex="6" OnTextChanged="tb_prof_TextChanged"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="Date From" SkinID="LabelFilter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tb_data1" runat="server" SkinID="TextboxDate" TabIndex="7" OnTextChanged="tb_data1_TextChanged"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tb_data1"
                                            Display="None" ValidationGroup="OnlineValidators"></asp:RequiredFieldValidator>
                                        <asp:ImageButton runat="server" ID="b_data1" ImageUrl="~/Images/calendario.png" CausesValidation="false"
                                            ImageAlign="TextTop" />
                                        <ajax:CalendarExtender ID="CalendarExtender1" runat="server" PopupPosition="BottomRight"
                                            TargetControlID="tb_data1" PopupButtonID="b_data1" CssClass="calendar" Format="dd/MM/yyyy" />
                                        <ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="tb_data1"
                                            MaskType="Date" Mask="99/99/9999" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                        </ajax:MaskedEditExtender>
                                        <ajax:MaskedEditValidator ID="MaskedEditValidator1" ControlExtender="MaskedEditExtender1"
                                            runat="server" ControlToValidate="tb_data1" IsValidEmpty="False" Display="Dynamic" ValidationGroup="OnlineValidators">
                                        </ajax:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text="Date To" SkinID="LabelFilter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tb_data2" runat="server" SkinID="TextboxDate" TabIndex="8" OnTextChanged="tb_data2_TextChanged"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_data2"
                                            Display="None" ValidationGroup="OnlineValidators"></asp:RequiredFieldValidator>
                                        <asp:ImageButton runat="server" ID="b_data2" ImageUrl="~/Images/calendario.png" CausesValidation="false"
                                            ImageAlign="TextTop" />
                                        <ajax:CalendarExtender ID="CalendarExtender2" runat="server" PopupPosition="BottomRight"
                                            TargetControlID="tb_data2" PopupButtonID="b_data2" CssClass="calendar" Format="dd/MM/yyyy" />
                                        <ajax:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="tb_data2"
                                            MaskType="Date" Mask="99/99/9999" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                        </ajax:MaskedEditExtender>
                                        <ajax:MaskedEditValidator ID="MaskedEditValidator2" ControlExtender="MaskedEditExtender2"
                                            runat="server" ControlToValidate="tb_data2" IsValidEmpty="False" Display="Dynamic" ValidationGroup="OnlineValidators">
                                        </ajax:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td colspan="2">
                                        <asp:CustomValidator ID="CustomValidator" runat="server" ErrorMessage="At least one of ReportName, JobName and JobNumber required"
                                            Display="Dynamic" ClientValidationFunction="AtLeastOneContact_ClientValidate"
                                            ForeColor="White" Font-Bold="true" ValidationGroup="OnlineValidators" />
                                        <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="At least 3 characters (* or % excluded )in the field JobName are required"
                                            Display="Dynamic" ClientValidationFunction="AtLeast3CharsInJobname_ClientValidate"
                                            ForeColor="White" Font-Bold="true" ValidationGroup="OnlineValidators" />
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Start date must be less than end date."
                                            Display="Dynamic" ClientValidationFunction="Date_ClientValidate"
                                            ForeColor="White" Font-Bold="true" ValidationGroup="OnlineValidators" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                </td>
                <td class="pageLeftCollapse" style="height: 100%">
                    <div>
                        <asp:Panel ID="pHide" runat="server">
                            <table>
                                <tr class="data">
                                    <td>
                                        <asp:Image ID="imgArrows" runat="server" Height="25px" Width="15px" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                </td>
                <td class="pageRight" id="PageRight" style="height: 100%">
                    <cedim:CeGridView ID="gv_search" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                        AllowPaging="true" PagerStyle-CssClass="pager" HeaderStyle-ForeColor="White"
                        RowStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" RowStyle-Height="25px"
                        Visible="true" OnPageIndexChanging="gv_search_PageIndexChanging" OnRowDataBound="gv_search_OnRowDataBound"
                        SkinID="Tables2" Width="100%" Height="100%" OnRowCommand="gv_search_OnRowCommand"
                        EmptyDataRowStyle-Font-Bold="true" EmptyDataText="No Data" EmptyDataRowStyle-HorizontalAlign="Center"
                        EmptyDataRowStyle-ForeColor="#518012" AllowSorting="true" OnSorting="gv_search_Sorting">
                        <Columns>
                            <asp:BoundField ItemStyle-Width="0%" HeaderStyle-Width="0%" ReadOnly="True" InsertVisible="False"
                                DataField="JobReport_ID" Visible="false" SortExpression="JobReport_ID" HeaderText="JobReport_ID"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="0%" HeaderStyle-Width="0%" ReadOnly="True" InsertVisible="False"
                                DataField="FromPage" Visible="false" HeaderText="FromPage" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField Visible="true" SortExpression="Recipient" HeaderText="Recipient">
                                <ItemStyle Width="7%" HorizontalAlign="Center"></ItemStyle>
                                <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lb_Recipient" runat="server" Visible="true" Text='<%# DataBinder.Eval(Container,"DataItem.Recipient")%>'
                                        ToolTip='<%# DataBinder.Eval(Container,"DataItem.FolderDescr")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField ItemStyle-Width="10%" HeaderStyle-Width="10%" ReadOnly="True" InsertVisible="False"
                                DataField="ReportName" Visible="true" SortExpression="ReportName" HeaderText="ReportName"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="14%" HeaderStyle-Width="14%" ReadOnly="True" InsertVisible="False"
                                DataField="JobReportDescr" Visible="true" HeaderText="JobReportDescr"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="10%" HeaderStyle-Width="10%" ReadOnly="True" InsertVisible="False"
                                DataField="JobName" Visible="true" SortExpression="JobName" HeaderText="JobName"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="0%" HeaderStyle-Width="0%" ReadOnly="True" InsertVisible="False"
                                DataField="TimeRef" Visible="false" SortExpression="TimeRef" HeaderText="TimeRef"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="0%" HeaderStyle-Width="0%" ReadOnly="True" InsertVisible="False"
                                DataField="UserTimeRef" Visible="false" SortExpression="UserTimeRef" HeaderText="UserTimeRef"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="10%" HeaderStyle-Width="10%" ReadOnly="True" InsertVisible="False"
                                DataField="UserTimeElab" Visible="true" HeaderText="UserTimeElab" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="10%" HeaderStyle-Width="10%" ReadOnly="True" InsertVisible="False"
                                DataField="XferStartTime" Visible="true" SortExpression="XferStartTime" HeaderText="XferStartTime"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                            <asp:BoundField ItemStyle-Width="6%" HeaderStyle-Width="6%" ReadOnly="True" InsertVisible="False"
                                DataField="TotPages" Visible="true" HeaderText="TotPages" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="5%" HeaderStyle-Width="5%" ReadOnly="True" InsertVisible="False"
                                DataField="JobNumber" Visible="true" HeaderText="JobNumber" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="0%" HeaderStyle-Width="0%" ReadOnly="True" InsertVisible="False"
                                DataField="ForPages" Visible="false" HeaderText="ForPages" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ReadOnly="True" InsertVisible="false" DataField="HasIndexes" Visible="false"
                                HeaderText="Index" ItemStyle-Width="0%" HeaderStyle-Width="0%" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField Visible="true" HeaderText="PDF" HeaderStyle-Width="5%">
                                <ItemStyle Width="2%"></ItemStyle>
                                <HeaderStyle Width="2%" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="b_pdf" runat="server" CausesValidation="false" CommandName="ViewFile"
                                        ImageUrl="~/Images/icona_pdf.png" ToolTip="View PDF" Width="20px" Height="20px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true" HeaderText="TXT" HeaderStyle-Width="5%" >
                                <ItemStyle Width="2%"></ItemStyle>
                                <HeaderStyle Width="2%" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="b_txt" runat="server" CausesValidation="false" CommandName="ViewFileTxt"
                                        ImageUrl="~/Images/icona_txt.png" ToolTip="View TXT" Width="20px" Height="20px"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false" HeaderText="XLS" >
                                <ItemStyle Width="2%"></ItemStyle>
                                <HeaderStyle Width="2%" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="b_xls" runat="server" CausesValidation="false" CommandName="ViewFileXls" Visible="false"
                                        ImageUrl="~/Images/icona_xls.png" ToolTip="View XLS" Width="20px" Height="20px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false" HeaderText="IDX">
                                <ItemStyle Width="3%"></ItemStyle>
                                <HeaderStyle Width="3%" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="b_index" runat="server" CausesValidation="false" CommandName="Index"
                                        ImageUrl="~/Images/Index.png" ToolTip='<%# Eval("HasIndexes")%>' Width="20px" Height="20px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="true" HeaderText="NOTE">
                                <ItemStyle Width="2%"></ItemStyle>
                                <HeaderStyle Width="2%" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="b_note" runat="server" CausesValidation="false" CommandName="Note"
                                        ImageUrl="~/Images/notes.png" ToolTip="View Notes" Width="20px" Height="20px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField Visible="false" HeaderText="CHK">
                                <ItemStyle Width="2%"></ItemStyle>
                                <HeaderStyle Width="2%" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="b_check" runat="server" CausesValidation="false" CommandName="Checked"
                                        ToolTip="Check" Enabled="false" Width="20px" Height="20px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </cedim:CeGridView>
                </td>
            </tr>
        </table>
        <!--</asp:Panel>-->
        <!--</div>-->
        <asp:Panel ID="popupW" runat="server" CssClass="popupWarning">
                <table ID="tbPopupW" style="background-color: #e5f6d1; width: 100%; height: 100%;">
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="false" OnClick="siPopup_Click"
                                ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100%; height: 100%; text-align: center">
                           <asp:Label ID="lb_Popup" runat="server" Text="" SkinID="LabelWarning"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        <asp:HiddenField ID="h2" runat="server" />
        <ajax:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="h2"
            BehaviorID="ModalPopupExtender2" PopupControlID="popupW" Drag="false" BackgroundCssClass="modal">
        </ajax:ModalPopupExtender>
        <asp:Panel ID="popupOKcanc" runat="server" CssClass="popupWarning">
            <div class="divWarning">
                <asp:Label ID="Label7" runat="server" Text="Are you sure you want to check the selected report?"
                    SkinID="LabelWarning"></asp:Label>
            </div>
            <div class="divWarning">
                <asp:Button ID="b_yes" CausesValidation="false" runat="server" Text="Yes" Visible="true"
                    CssClass="buttonBig" OnClick="b_yes_Click" />
                <asp:Button ID="b_popupCanc" CausesValidation="false" runat="server" Text="Cancel"
                    Visible="true" CssClass="buttonBig" />
            </div>
        </asp:Panel>
        <asp:HiddenField ID="h3" runat="server" />
        <ajax:ModalPopupExtender ID="ModalPopupExtender3" runat="server" TargetControlID="h3"
            CancelControlID="b_popupCanc" BehaviorID="ModalPopupExtender3" PopupControlID="popupOKcanc"
            Drag="false" BackgroundCssClass="modal">
        </ajax:ModalPopupExtender>
        <asp:Button runat="server" ID="hiddenTargetControlForModalPopup" Style="display: none" />
        <ajax:ModalPopupExtender runat="server" ID="programmaticModalPopup" BehaviorID="programmaticModalPopupBehavior"
            TargetControlID="hiddenTargetControlForModalPopup" PopupControlID="programmaticPopup"
            BackgroundCssClass="modal" PopupDragHandleControlID="programmaticPopupDragHandle"
            RepositionMode="RepositionOnWindowScroll">
        </ajax:ModalPopupExtender>
        <asp:Panel ID="programmaticPopup" runat="server" Style="display: none; text-align: center">
            <table id="myPopup" style="width: 400px; height: 400px">
                <tr>
                    <td style="text-align: right;">
                        <asp:ImageButton ID="b_close" runat="server" CausesValidation="false" OnClick="b_close_Click"
                            ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 400px; height: 100%;">
                        <iframe id="myFrame" runat="server" scrolling="yes" style="width: 400px; height: 100%; border: none; position: relative;"></iframe>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="cmdPopup" runat="server" Style="display: inline-table; text-align: center; width: 200px; height: 200px; position: relative">
            <table id="TablePopup" runat="server" style="background-color: #EFF1F4">
                <tr>
                    <td style="text-align: right;">
                        <asp:ImageButton ID="b_close2" runat="server" CausesValidation="false" OnClick="b_close2_Click"
                            ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                    </td>
                </tr>
                <tr style="background-color: #518012">
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="AVAILABLE INDEXES" SkinID="LabelLoginBig"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Button runat="server" ID="b_cmd" Style="display: none" />
        <ajax:ModalPopupExtender runat="server" ID="cmdExt" BehaviorID="cmdExtBehavior" TargetControlID="b_cmd"
            PopupControlID="cmdPopup" BackgroundCssClass="modal">
        </ajax:ModalPopupExtender>
        <asp:Panel ID="p_notes" runat="server" Style="display: inline-table; text-align: center; position: relative">
            <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <table id="Table1" runat="server" style="background-color: #EFF1F4">
                        <tr>
                            <td style="text-align: right;">
                                <asp:ImageButton ID="b_close3" runat="server" CausesValidation="false" OnClick="b_close3_Click"
                                    ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">
                                <asp:DetailsView ID="dv_notes" runat="server" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true"
                                    AutoGenerateRows="False" CssClass="gridView" SkinID="TablesView" Visible="true"
                                    AllowPaging="true" OnPreRender="dv_notes_DataBound" OnItemUpdating="dv_notes_ItemUpdating"
                                    OnPageIndexChanging="dv_notes_PageIndexChanging" OnItemDeleting="dv_notes_ItemDeleting"
                                    OnItemInserting="dv_notes_ItemInserting" PagerStyle-CssClass="pager" HeaderText="Notes"
                                    OnModeChanging="dv_notes_ModeChanging" Width="500px" EmptyDataText="NO NOTES INSERTED"
                                    EmptyDataRowStyle-Font-Bold="true">
                                    <HeaderStyle Font-Bold="True" />
                                    <Fields>
                                        <asp:TemplateField HeaderText="Job Report ID">
                                            <ItemTemplate>
                                                <asp:Label ID="tb_JRID1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.JobReport_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Label ID="tb_JRID2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.JobReport_ID") %>'></asp:Label>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Report ID">
                                            <ItemTemplate>
                                                <asp:Label ID="tb_repID1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ReportId") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Label ID="tb_repID2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ReportId") %>'></asp:Label>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Version">
                                            <ItemTemplate>
                                                <asp:Label ID="tb_ver1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.NoteId") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Label ID="tb_ver2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.NoteId") %>'></asp:Label>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Update Time">
                                            <ItemTemplate>
                                                <asp:Label ID="tb_updTime1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastUpdateTime") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Label ID="tb_updTime2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastUpdateTime") %>'></asp:Label>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Update User">
                                            <ItemTemplate>
                                                <asp:Label ID="tb_updUser1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AUTH_USER") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Label ID="tb_updUser2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AUTH_USER") %>'></asp:Label>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Text">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_text1" ReadOnly="true" Height="200px" Width="200px" TextMode="MultiLine"
                                                    runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.textString") %>'></asp:TextBox>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="tb_text2" Height="200px" Width="200px" TextMode="MultiLine" runat="server"
                                                    Text='<%# DataBinder.Eval(Container, "DataItem.textString") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <EditItemTemplate>
                                                <asp:Button ID="lnkUpdate" runat="server" CausesValidation="True" CommandName="Update"
                                                    Text="Update" ValidationGroup="g1" CssClass="buttonSmall" ></asp:Button>
                                                <asp:Button ID="lnkCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                                                    Text="Cancel" CssClass="buttonSmall"></asp:Button>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Button ID="lnkEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                                    Text="Edit" CssClass="buttonSmall"></asp:Button>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <InsertItemTemplate>
                                                <asp:Button ID="lnkInsert" runat="server" CausesValidation="True" CommandName="Insert" 
                                                    Text="Insert" ValidationGroup="g1" CssClass="buttonSmall"></asp:Button>
                                                <asp:Button ID="lnkCacel" runat="server" CausesValidation="False" CommandName="Cancel"
                                                    Text="Cancel" CssClass="buttonSmall"></asp:Button>
                                            </InsertItemTemplate>
                                            <ItemTemplate>
                                                <asp:Button ID="lnkNew" runat="server" CausesValidation="False" CommandName="New"
                                                    Text="New" CssClass="buttonSmall"></asp:Button>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:Button ID="lnkDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                    Text="Delete" CssClass="buttonSmall"></asp:Button>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button runat="server" ID="b_notes" Style="display: none" />
        <ajax:ModalPopupExtender runat="server" ID="notes_ext" BehaviorID="notes_extBehavior"
            TargetControlID="b_notes" PopupControlID="p_notes" BackgroundCssClass="modal">
        </ajax:ModalPopupExtender>
        <asp:HiddenField ID="hiddenIndex" runat="server" Value="0" />
        <asp:Panel ID="p_ConfirmDel" runat="server" CssClass="popupWarningBig">
            <div class="divWarning">
                <asp:Button ID="b_curr" CausesValidation="false" runat="server" Text="Current" Visible="true"
                    CssClass="buttonBig" OnClick="currPopup_Click" />
            </div>
            <div class="divWarning">
                <asp:Button ID="b_all" CausesValidation="false" runat="server" Text="All Versions"
                    Visible="true" CssClass="buttonBig" OnClick="allPopup_Click" />
            </div>
            <div class="divWarning">
                <asp:Button ID="b_canc" CausesValidation="false" runat="server" Text="Cancel" Visible="true"
                    CssClass="buttonBig" />
            </div>
        </asp:Panel>
        <asp:HiddenField ID="hDel" runat="server" />
        <ajax:ModalPopupExtender ID="ConfDel_ext" runat="server" TargetControlID="hDel" CancelControlID="b_canc"
            BehaviorID="ConfDel_extBehavior" PopupControlID="p_ConfirmDel" Drag="false" BackgroundCssClass="modal">
        </ajax:ModalPopupExtender>
        
        <asp:Panel ID="p_range_pages" runat="server" Style="display: inline-table; text-align: center; width:1000px; position: relative">
            <asp:UpdatePanel ID="UpdatePanel3" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <table id="Table2" runat="server" style="width:100%;background-color: #EFF1F4">
                        <tr>
                            <td style="text-align: right;">
                                <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="false" OnClick="b_close4_Click"
                                    ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">
                                
                            </td>
                        </tr>
                        <tr>
                        <td class="pageRight" id="PageRight1" style="height: 100%">
                          <cedim:CeGridView ID="rangepages_grid" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                            AllowPaging="true" PagerStyle-CssClass="pager" HeaderStyle-ForeColor="White"
                            RowStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" RowStyle-Height="30px"
                            Visible="true"  OnPageIndexChanging="rangepage_PageIndexChanging" OnRowDataBound="rangepage_OnRowDataBound"
                            SkinID="Tables2" Width="100%" Height="100%"  OnRowCommand="rangepage_OnRowCommand"
                            EmptyDataRowStyle-Font-Bold="true" EmptyDataText="No Data" EmptyDataRowStyle-HorizontalAlign="Center"
                            EmptyDataRowStyle-ForeColor="#518012"  AllowSorting="true" OnSorting="rangepage_Sorting">
                            <Columns>
                                <asp:BoundField ItemStyle-Width="30%" HeaderStyle-Width="30%" ReadOnly="True" InsertVisible="False"
                                    DataField="Range_Pages" Visible="true" SortExpression="Range_Pages" HeaderText="Range" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField ItemStyle-Width="0%" HeaderStyle-Width="0%" ReadOnly="True" InsertVisible="False"
                                    DataField="JobReportID" Visible="false"  HeaderText="JobReportID"
                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField ItemStyle-Width="0%" HeaderStyle-Width="0%" ReadOnly="True" InsertVisible="False"
                                    DataField="FromPage" Visible="false" SortExpression="FromPage" HeaderText="FromPage" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center" />
                                 <asp:BoundField ItemStyle-Width="0%" HeaderStyle-Width="0%" ReadOnly="True" InsertVisible="False"
                                    DataField="ForPages" Visible="false" HeaderText="ForPages" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField ItemStyle-Width="15%" HeaderStyle-Width="15%" ReadOnly="True" InsertVisible="False"
                                    DataField="ReportName" Visible="true" HeaderText="ReportName"
                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField ItemStyle-Width="20%" HeaderStyle-Width="20%" ReadOnly="True" InsertVisible="False"
                                    DataField="UserTimeRef" Visible="true" HeaderText="UserTimeRef"
                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField ItemStyle-Width="20%" HeaderStyle-Width="20%" ReadOnly="True" InsertVisible="False"
                                    DataField="XferStartTime" Visible="true" HeaderText="XferStartTime"
                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                                <asp:BoundField ItemStyle-Width="10%" HeaderStyle-Width="10%" ReadOnly="True" InsertVisible="False"
                                    DataField="TotPages" Visible="true" HeaderText="TotPages" ItemStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center" />
                                <asp:TemplateField Visible="true" HeaderText="PDF" HeaderStyle-Width="3%">
                                    <ItemStyle Width="3%"></ItemStyle>
                                    <HeaderStyle Width="3%" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="b_ran_pdf" runat="server" CausesValidation="false" CommandName="ViewFile"
                                            ImageUrl="~/Images/icona_pdf.png" ToolTip="View PDF" Width="20px" Height="20px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="true" HeaderText="TXT" HeaderStyle-Width="3%" >
                                    <ItemStyle Width="3%"></ItemStyle>
                                    <HeaderStyle Width="3%" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="b_ran_txt" runat="server" CausesValidation="false" CommandName="ViewFileTxt"
                                            ImageUrl="~/Images/icona_txt.png" ToolTip="View TXT" Width="20px" Height="20px"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                </Columns>
                            </cedim:CeGridView>
                        </td>
                       </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button runat="server" ID="b_range_pages" Style="display: none" />
        <ajax:ModalPopupExtender runat="server" ID="ModalPopupExtender4" BehaviorID="rangePage_extBehavior"
            TargetControlID="b_range_pages" PopupControlID="p_range_pages" BackgroundCssClass="modal">
        </ajax:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>
<input type="hidden" clientidmode="Static" id="txtHidData" runat="server" />
<input type="hidden" clientidmode="Static" id="txtHidData2" runat="server" />
