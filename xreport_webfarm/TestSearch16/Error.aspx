﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="XReportSearch.Error"
    Theme="XReportSearchTheme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
    <title>XReport Search</title>
    <link href="~/App_Themes/XReportSearchAjaxControls.css?2" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/XReportSearchStyle.css?2" rel="stylesheet" type="text/css" />
</head>
<!--<body onload="resize()" onclick="resize()" style="background-color: #e5f6d1;" onunload="popup_after_unload()">-->
<body onload="resize()" onclick="resize()" style="background-color: #e5f6d1;" >
    <script language="javascript" type="text/javascript">


        function popup_after_unload() {
            var url = 'Error.aspx?forceCloseSession=Y';
            //myWindowHandle = window.open(url, 'windowname', 'width=0,height=0'); 
            myWindowHandle = window.open(url, "_blank", "toolbar=no,scrollbars=no,resizable=no,top=0,left=0,width=10,height=10");
            myWindowHandle.blur();
            window.focus();
        }

        function resize() {
            var frame = document.getElementById("MainDiv");
            //var page = document.getElementById("PageDiv2");
            var windowheight = document.documentElement.clientHeight;
            var windowwidth = document.documentElement.clientWidth;
            //frame.style.height = windowheight + "px";
            if (document.getElementById("txtHidData") != null) {
                document.getElementById("txtHidData").value = windowheight;
            }
            if (document.getElementById("txtHidData2") != null) {
                document.getElementById("txtHidData2").value = windowwidth;
            }
            if (frame != null && frame.style.height != null) {
                frame.style.height = windowheight + "px";
            }
            //page.style.height = (windowheight - 74) + "px";
        }
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
    <script language="javascript" type="text/javascript">

        function RefreshParent() {
            window.parent.location.href = window.parent.location.href;
        }

        function realClose() {
            var win = window.open("", "_top", "", "true");
            win.opener = true;
            //win.close();
            win.realclosefunc();
        }

        window.realclosefunc = window.close;
        window.close = realClose;

    </script>
    <input type="hidden" clientidmode="Static" id="txtHidData" runat="server" />
    <input type="hidden" clientidmode="Static" id="txtHidData2" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div id="MainDiv" class="main">
                <div class="headerContainer" id="sp_headerDiv">
                        <div class="headerLeft">
                            <div class="headerTitleLeft">
                            </div>
                        </div>
                        <div class="headerRight">
                        </div>
                    </div>
                <asp:Panel runat="server" ID="panelTable" class="page">
                    <table class="page" id="PageDiv">
                        <tr style="vertical-align: top; text-align: center; height: 100%">
                            <td>
                            </td>
                        </tr>
                        <tr style="vertical-align: top; text-align: center">
                            <td>
                                <asp:Label ID="err" runat="server" SkinID="LabelErr"></asp:Label>
                            </td>
                        </tr>
                        <tr style="vertical-align: top; text-align: center">
                            <td>
                                <asp:Button ID="b_refresh" CausesValidation="false" runat="server" Text="Reload"
                                    Visible="true" CssClass="buttonBig" OnClick="b_refresh_Click" />
                            </td>
                        </tr>
                        <tr style="vertical-align: top; text-align: center">
                            <td>
                                <asp:Button ID="b_close"  CausesValidation="false" runat="server" Text="Close"
                                    Visible="true" CssClass="buttonBig"  OnClick="b_close_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
