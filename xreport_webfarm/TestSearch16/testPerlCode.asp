<SCRIPT LANGUAGE="PerlScript" RUNAT=SERVER>

###-- vim: set syn=perl: --###

use lib("$ENV{'XREPORT_HOME'}/perllib");

use strict; 

use CGI qw(:standard);
use URI::Escape; 
use Data::Dumper;

sub logIt {
  for (@_) {
    my $t = $_; $t =~ s/\n/<br\/>/sg; $main::Response->Write($t);
  }
}

sub TRIM {
  for (@_) {
    $_ =~ s/(?:^ +| +$)//g;
  }
}

my $reqmet   = $main::Request->ServerVariables('REQUEST_METHOD'    )->item();
my $qrystr   = $main::Request->ServerVariables('QUERY_STRING'      )->item();
my $thisurl  = $main::Request->ServerVariables("HTTP_URL"          )->item();
my $ctype    = $main::Request->ServerVariables("CONTENT_TYPE"      )->item();

my $inputForm = start_html("Definition Upload Facility")
              . start_multipart_form( -method => 'POST'
                                   , -action => $thisurl
                                    ) 
              . table(Tr( td(['ReqMet:', $reqmet]) )
                     ,Tr( td(['qrystr:', $qrystr]) )
                     ,Tr( td(['thisurl:', $thisurl]))
                     ,Tr( td({-colspan => 2}, textarea(-name=>'srccode',
			  			-default=>'#enter code here',
			  			-rows=>25,
			  			-columns=>120)) )
                     ,Tr( td({-colspan => 2}, submit('Action','Parse+Exec')) ) )
              . endform()
              . end_html();

return $main::Response->Write($inputForm) if $reqmet =~ /^GET$/i;

my $strVars = $main::Request->BinaryRead($main::Request->TotalBytes());
my ($boundary) = ($ctype =~ /boundary=(.+)$/s);
my ($srccode) = (grep /name="srccode"/, split(/--$boundary(?:\x0d\x0a|--)/, $strVars));
$srccode = '#'.$srccode; 
my $codeoutput = '';
eval $srccode;
my $errmsg = $@;

$main::Response->Write(start_html("")
       .hr().pre('INPUT: '. $strVars)
       .hr().pre('srccode: '. $srccode)
       .hr().h3('Errors:').br().pre($errmsg)
       .hr().h3('Output:').br().pre($codeoutput)
       .hr().end_html());

return $main::Response->Flush();

1;

</SCRIPT>
