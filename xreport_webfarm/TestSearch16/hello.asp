<!DOCTYPE html>
<html> 
<body>
<p>ASP can output plain text:</p>
<%response.write(" LOGON_USER=")%>
<%response.write(Request.ServerVariables("LOGON_USER") & "<br>")%>
<%response.write(" AUTH_USER=")%>
<%response.write(Request.ServerVariables("AUTH_USER") & "<br>")%> 
<%response.write(" APPL_PHYSICAL_PATH=")%>
<%response.write(Request.ServerVariables("APPL_PHYSICAL_PATH") & "<br>")%> 
<%response.write(" XREPORT_HOME=")%>
<%response.write(Request.ServerVariables("XREPORT_HOME") & "<br>")%> 
<%response.write(" XREPORT_SITECONF2=")%>
<%response.write(Request.ServerVariables("XREPORT_SITECONF") & "<br>")%> 
<%
dim i
dim j
j=Application.Contents.Count
For i=1 to j
  Response.Write(Application.Contents(i) & "<br>")
Next
%>

<p>
<b>You are browsing this site with:</b>
<%Response.Write(Request.ServerVariables("http_user_agent"))%>
</p>
<p>
<b>Your IP address is:</b>
<%Response.Write(Request.ServerVariables("remote_addr"))%>
</p>
<p>
<b>The DNS lookup of the IP address is:</b>
<%Response.Write(Request.ServerVariables("remote_host"))%>
</p>
<p>
<b>The method used to call the page:</b>
<%Response.Write(Request.ServerVariables("request_method"))%>
</p>
<p>
<b>The server's domain name:</b>
<%Response.Write(Request.ServerVariables("server_name"))%>
</p>
<p>
<b>The server's port:</b>
<%Response.Write(Request.ServerVariables("server_port"))%>
</p>
<p>
<b>The server's software:</b>
<%Response.Write(Request.ServerVariables("server_software"))%>
</p>
<p>
<b>LOGON_USER:</b>
<%Response.Write(Request.ServerVariables("LOGON_USER"))%>
</p>
<p>
<b>AUTH_USER:</b>
<%Response.Write(Request.ServerVariables("AUTH_USER"))%>
</p>
<p>
<b>APPL_PHYSICAL_PATH:</b>
<%Response.Write(Request.ServerVariables("APPL_PHYSICAL_PATH"))%>
</p>
<%
for each x in Request.ServerVariables
  response.write(x & "=" & Request.ServerVariables(x) &"<br>")
next
%> 
</body>
</html>