﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchHistory.ascx.cs" Inherits="XReportSearch.SearchHistory" %>
<%@ Register Namespace="CustomControls" TagPrefix="cedim" Assembly="XReportSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<script  type="text/javascript">

    function fnOnUpdateValidatorsH() {
        var count = 0;
        for (var i = 0; i < Page_Validators.length; i++) {
            var val = Page_Validators[i];
            var ctrl = document.getElementById(val.controltovalidate);
            if (ctrl != null && ctrl.style != null) {
                if (!val.isvalid) {
                    ctrl.style.borderColor = "Red";
                    ctrl.className = "comboboxError";
                    count++;
                }
                else {
                    ctrl.style.borderColor = "#518012";
                    ctrl.className = "combobox";
                }
            }
        }
    }

    function AtLeastOneContact_ClientValidateH(source, args) {
        if (document.getElementById("<%= sh_tb_jobname.ClientID %>").value == "" &&
            document.getElementById("<%= sh_tb_jobname2.ClientID %>").value == "" &&
                document.getElementById("<%= sh_tb_jobnumber.ClientID %>").value == "" &&
                document.getElementById("<%= sh_tb_name.ClientID %>").value == "") {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
    }

    function Date_ClientValidateH(source, args) {
        var smallDate = document.getElementById("<%= sh_tb_data1.ClientID %>").value;
        var largeDate = document.getElementById("<%= sh_tb_data2.ClientID %>").value;
        var smallDateArr = Array();
        var largeDateArr = Array();
        smallDateArr = smallDate.split('/');
        largeDateArr = largeDate.split('/');
        var smallDt = smallDateArr[0];
        var smallMt = smallDateArr[1];
        var smallYr = smallDateArr[2];
        var largeDt = largeDateArr[0];
        var largeMt = largeDateArr[1];
        var largeYr = largeDateArr[2];
        //alert(smallDt + "-" + smallMt + "-" + smallYr);
        //alert(largeDt + "-" + largeMt + "-" + largeYr);
        if (smallYr > largeYr)
            args.IsValid = false;
        else if (smallYr == largeYr && smallMt > largeMt)
            args.IsValid = false;
        else if (smallYr == largeYr && smallMt == largeMt && smallDt > largeDt)
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    function resizeH() {
        var windowheight = document.documentElement.clientHeight;
        var windowwidth = document.documentElement.clientWidth;
        if (document.getElementById("sp_txtHidData") != null) {
            document.getElementById("sp_txtHidData").value = windowheight;
        }
        if (document.getElementById("sp_txtHidData2") != null) {
            document.getElementById("sp_txtHidData2").value = windowwidth;
        }
    }

</script>
<asp:UpdatePanel ID="sh_UpdatePanel1" UpdateMode="Conditional" runat="server" RenderMode="Inline" OnInit="Resize" OnPreRender="Resize">
    <ContentTemplate>
        <table class="page" id="sh_PageDiv" style="height: 100%; min-height: 100%">
            <tr id="sh_PageDiv2" style="height: 100%">
                <td class="pageLeft" style="height: 100%">
                    <ajax:CollapsiblePanelExtender ID="sh_CollapsiblePanelExtender1" runat="server" TargetControlID="sh_pShow"
                        CollapseControlID="sh_pHide" ExpandControlID="sh_pHide" Collapsed="false" ImageControlID="sh_imgArrows"
                        ExpandDirection="Horizontal" ExpandedImage="~/Images/hide.png" CollapsedImage="~/Images/show.png">
                    </ajax:CollapsiblePanelExtender>
                    <div>
                        <asp:Panel ID="sh_pShow" runat="server" Style="height: 100%">
                            <table>
                                <tr class="data">
                                    <td></td>
                                    <td class="buttons">
                                        <asp:Button ID="sh_buttonSearch" CausesValidation="true" runat="server" Text="Search" Visible="true"
                                            CssClass="buttonBig" OnClick="sh_buttonSearch_Click" ValidationGroup="HistoryValidators" />
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td>
                                        <asp:Label ID="sh_l_ambiente" runat="server" Text="Environment" SkinID="LabelFilter"></asp:Label>
                                        <asp:Label ID="sh_l_specUser" runat="server" Visible="false" Text="Spec. User" SkinID="LabelFilter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="sh_tb_specUser" Visible="false" runat="server" SkinID="TextboxSmall" TabIndex="1"></asp:TextBox>
                                        <asp:Panel ID="Panel1" runat="server">

                                            <ajax:ComboBox ID="sh_ddl_ambiente" runat="server" AutoPostBack="False" RenderMode="Block"
                                                DropDownStyle="DropDown" Visible="true" CaseSensitive="False" CssClass="comboboxDoc"
                                                TabIndex="2">
                                                <asp:ListItem Text="A1" Value="A1"></asp:ListItem>
                                                <asp:ListItem Text="A2" Value="A2"></asp:ListItem>
                                                <asp:ListItem Text="A3" Value="A3"></asp:ListItem>
                                                <asp:ListItem Text="A4" Value="A4"></asp:ListItem>
                                                <asp:ListItem Text="A6" Value="A6"></asp:ListItem>
                                                <asp:ListItem Text="A7" Value="A7"></asp:ListItem>
                                                <asp:ListItem Text="A8" Value="A8"></asp:ListItem>
                                                <asp:ListItem Text="A9" Value="A9"></asp:ListItem>
                                                <asp:ListItem Text="AP" Value="AP"></asp:ListItem>
                                                <asp:ListItem Text="BD" Value="BD"></asp:ListItem>
                                                <asp:ListItem Text="BR" Value="BR"></asp:ListItem>
                                                <asp:ListItem Text="SH" Value="SH"></asp:ListItem>
                                            </ajax:ComboBox>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td>
                                        <asp:Label ID="sh_lb_jobname" runat="server" Text="Job Name" SkinID="LabelFilter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="sh_tb_jobname" runat="server" SkinID="TextboxSmallLeft" TabIndex="3"></asp:TextBox>
                                        <asp:TextBox ID="sh_tb_jobname2" runat="server" SkinID="TextboxSmallRight" TabIndex="4"></asp:TextBox>  
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td>
                                        <asp:Label ID="sh_Label2" runat="server" Text="Report Name" SkinID="LabelFilter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="sh_tb_name" runat="server" SkinID="TextboxSmall" TabIndex="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td>
                                        <asp:Label ID="sh_Label1" runat="server" Text="Job Number" SkinID="LabelFilter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="sh_tb_jobnumber" runat="server" SkinID="TextboxSmall" TabIndex="6"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td>
                                        <asp:Label ID="sh_l_prof" runat="server" Text="Profile" SkinID="LabelFilter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="sh_tb_prof" runat="server" SkinID="TextboxSmall" TabIndex="7"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td>
                                        <asp:Label ID="sh_Label3" runat="server" Text="Date From" SkinID="LabelFilter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="sh_tb_data1" runat="server" SkinID="TextboxDate" TabIndex="8"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="sh_RequiredFieldValidator1" runat="server" ControlToValidate="sh_tb_data1"
                                            Display="None" ValidationGroup="HistoryValidators"></asp:RequiredFieldValidator>
                                        <asp:ImageButton runat="server" ID="sh_b_data1" ImageUrl="~/Images/calendario.png" CausesValidation="false"
                                            ImageAlign="TextTop" />
                                        <ajax:CalendarExtender ID="sh_CalendarExtender1" runat="server" PopupPosition="BottomRight"
                                            TargetControlID="sh_tb_data1" PopupButtonID="sh_b_data1" CssClass="calendar" Format="dd/MM/yyyy" />
                                        <ajax:MaskedEditExtender ID="sh_MaskedEditExtender1" runat="server" TargetControlID="sh_tb_data1"
                                            MaskType="Date" Mask="99/99/9999" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                        </ajax:MaskedEditExtender>
                                        <ajax:MaskedEditValidator ID="sh_MaskedEditValidator1" ControlExtender="sh_MaskedEditExtender1"
                                            runat="server" ControlToValidate="sh_tb_data1" IsValidEmpty="False" Display="Dynamic" ValidationGroup="HistoryValidators">
                                        </ajax:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td>
                                        <asp:Label ID="sh_Label4" runat="server" Text="Date To" SkinID="LabelFilter"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="sh_tb_data2" runat="server" SkinID="TextboxDate" TabIndex="9"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="sh_RequiredFieldValidator2" runat="server" ControlToValidate="sh_tb_data2"
                                            Display="None" ValidationGroup="HistoryValidators"></asp:RequiredFieldValidator>
                                        <asp:ImageButton runat="server" ID="sh_b_data2" ImageUrl="~/Images/calendario.png" CausesValidation="false"
                                            ImageAlign="TextTop" />
                                        <ajax:CalendarExtender ID="sh_CalendarExtender2" runat="server" PopupPosition="BottomRight"
                                            TargetControlID="sh_tb_data2" PopupButtonID="sh_b_data2" CssClass="calendar" Format="dd/MM/yyyy" />
                                        <ajax:MaskedEditExtender ID="sh_MaskedEditExtender2" runat="server" TargetControlID="sh_tb_data2"
                                            MaskType="Date" Mask="99/99/9999" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                        </ajax:MaskedEditExtender>
                                        <ajax:MaskedEditValidator ID="sh_MaskedEditValidator2" ControlExtender="sh_MaskedEditExtender2"
                                            runat="server" ControlToValidate="sh_tb_data2" IsValidEmpty="False" Display="Dynamic" ValidationGroup="HistoryValidators">
                                        </ajax:MaskedEditValidator>
                                    </td>
                                </tr>
                                <tr class="data">
                                    <td colspan="2">
                                        <asp:CustomValidator ID="sh_CustomValidator" runat="server" ErrorMessage="At least one of ReportName, JobName and JobNumber required"
                                            Display="Dynamic" ClientValidationFunction="AtLeastOneContact_ClientValidateH"
                                            ForeColor="White" Font-Bold="true" ValidationGroup="HistoryValidators" />
                                        <asp:CustomValidator ID="sh_CustomValidator1" runat="server" ErrorMessage="Start date must be less than end date."
                                            Display="Dynamic" ClientValidationFunction="Date_ClientValidateH" ValidationGroup="HistoryValidators"
                                            ForeColor="White" Font-Bold="true" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                </td>
                <td class="pageLeftCollapse" style="height: 100%">
                    <div>
                        <asp:Panel ID="sh_pHide" runat="server">
                            <table>
                                <tr class="data">
                                    <td>
                                        <asp:Image ID="sh_imgArrows" runat="server" Height="25px" Width="15px" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </div>
                </td>
                <td class="pageRight" id="sh_PageRight" style="height: 100%">
                    <cedim:CeGridView ID="sh_gv_searchH" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                        AllowPaging="true" PagerStyle-CssClass="pager" HeaderStyle-ForeColor="White"
                        RowStyle-HorizontalAlign="Center" HeaderStyle-Height="25px" RowStyle-Height="25px"
                        Visible="true" OnPageIndexChanging="sh_gv_searchH_PageIndexChanging" OnRowDataBound="sh_gv_searchH_OnRowDataBound"
                        SkinID="Tables2" Width="100%" Height="100%" OnRowCommand="sh_gv_searchH_OnRowCommand"
                        EmptyDataRowStyle-Font-Bold="true" EmptyDataText="No Data" EmptyDataRowStyle-HorizontalAlign="Center"
                        EmptyDataRowStyle-ForeColor="#518012">
                        <Columns>
                            <asp:BoundField ItemStyle-Width="6%" HeaderStyle-Width="6%" ReadOnly="True" InsertVisible="False"
                                DataField="Recipient" Visible="true" SortExpression="Recipient" HeaderText="Recipient"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="15%" HeaderStyle-Width="15%" ReadOnly="True" InsertVisible="False"
                                DataField="ReportName" Visible="true" SortExpression="ReportName" HeaderText="ReportName"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="9%" HeaderStyle-Width="9%" ReadOnly="True" InsertVisible="False"
                                DataField="JobName" Visible="true" SortExpression="JobName" HeaderText="JobName"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="15%" HeaderStyle-Width="15%" ReadOnly="True" InsertVisible="False"
                                DataField="TimeRef" Visible="true" SortExpression="TimeRef" HeaderText="TimeRef"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="15%" HeaderStyle-Width="15%" ReadOnly="True" InsertVisible="False"
                                DataField="UserTimeRef" Visible="true" SortExpression="UserTimeRef" HeaderText="UserTimeRef"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="15%" HeaderStyle-Width="15%" ReadOnly="True" InsertVisible="False"
                                DataField="UserTimeElab" Visible="true" SortExpression="UserTimeElab" HeaderText="UserTimeElab"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="6%" HeaderStyle-Width="6%" ReadOnly="True" InsertVisible="False"
                                DataField="TotPages" Visible="true" SortExpression="TotPages" HeaderText="TotPages"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="6%" HeaderStyle-Width="6%" ReadOnly="True" InsertVisible="False"
                                DataField="JobNumber" Visible="true" SortExpression="JobNumber" HeaderText="JobNr"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ItemStyle-Width="8%" HeaderStyle-Width="8%" ReadOnly="True" InsertVisible="False"
                                DataField="DDName" Visible="true" SortExpression="DDName" HeaderText="DDName"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ReadOnly="True" InsertVisible="False" DataField="ReportEntryId" Visible="false"
                                SortExpression="ReportEntryId" HeaderText="" ItemStyle-Width="0%" HeaderStyle-Width="0%"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ReadOnly="True" InsertVisible="False" DataField="firstRBA" Visible="false"
                                SortExpression="firstRBA" HeaderText="" ItemStyle-Width="0%" HeaderStyle-Width="0%"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ReadOnly="True" InsertVisible="False" DataField="lastRBA" Visible="false"
                                SortExpression="lastRBA" HeaderText="" ItemStyle-Width="0%" HeaderStyle-Width="0%"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ReadOnly="True" InsertVisible="False" DataField="restore_id" Visible="false"
                                SortExpression="restore_id" HeaderText="" ItemStyle-Width="0%" HeaderStyle-Width="0%"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField ReadOnly="True" InsertVisible="False" DataField="totpages2" Visible="false"
                                SortExpression="totpages2" HeaderText="" ItemStyle-Width="0%" HeaderStyle-Width="0%"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField Visible="true" HeaderText="">
                                <ItemStyle Width="3%"></ItemStyle>
                                <HeaderStyle Width="3%" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="sh_b_pdf" runat="server" CausesValidation="false" CommandName="ViewFile"
                                        ImageUrl="~/Images/List.png" ToolTip="View File" Width="20px" Height="20px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField ReadOnly="True" InsertVisible="false" DataField="Indexed" Visible="false"
                                HeaderText="Index" ItemStyle-Width="0%" HeaderStyle-Width="0%" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField Visible="true" HeaderText="">
                                <ItemStyle Width="3%"></ItemStyle>
                                <HeaderStyle Width="3%" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="sh_b_index" runat="server" CausesValidation="false" CommandName="Index"
                                        ImageUrl="~/Images/Index.png" ToolTip='<%# Eval("Indexed")%>' Width="20px" Height="20px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </cedim:CeGridView>
                </td>
            </tr>
        </table>
        <asp:Panel ID="sh_popupW" runat="server" CssClass="popupWarning">
            <table id="sh_tbPopupW" style="background-color: #e5f6d1; width: 100%; height: 100%;">
                <tr>
                    <td style="text-align: right;">
                        <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="false" OnClick="sh_siPopup_Click"
                            ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%; height: 100%; text-align: center">
                        <asp:Label ID="sh_lb_Popup" runat="server" Text="" SkinID="LabelWarning"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:HiddenField ID="sh_h2" runat="server" />
        <ajax:ModalPopupExtender ID="sh_ModalPopupExtender2" runat="server" TargetControlID="sh_h2"
            BehaviorID="sh_ModalPopupExtender2" PopupControlID="sh_popupW" Drag="false" BackgroundCssClass="modal">
        </ajax:ModalPopupExtender>
        <asp:Panel ID="sh_popupOKcanc" runat="server" CssClass="popupWarning">
            <div class="divWarning">
                <asp:Label ID="sh_Label7" runat="server" Text="Are you sure you want to check the selected report?"
                    SkinID="sh_LabelWarning"></asp:Label>
            </div>
            <div class="divWarning">
                <asp:Button ID="sh_b_yes" CausesValidation="false" runat="server" Text="Yes" Visible="true"
                    CssClass="buttonBig" OnClick="sh_b_yes_Click" />
                <asp:Button ID="sh_b_popupCanc" CausesValidation="false" runat="server" Text="Cancel"
                    Visible="true" CssClass="buttonBig" />
            </div>
        </asp:Panel>
        <asp:HiddenField ID="sh_h3" runat="server" />
        <ajax:ModalPopupExtender ID="sh_ModalPopupExtender3" runat="server" TargetControlID="sh_h3"
            CancelControlID="sh_b_popupCanc" BehaviorID="sh_ModalPopupExtender3" PopupControlID="sh_popupOKcanc"
            Drag="false" BackgroundCssClass="modal">
        </ajax:ModalPopupExtender>
        <asp:Button runat="server" ID="sh_hiddenTargetControlForModalPopup" Style="display: none" />
        <ajax:ModalPopupExtender runat="server" ID="sh_programmaticModalPopup" BehaviorID="sh_programmaticModalPopupBehavior"
            TargetControlID="sh_hiddenTargetControlForModalPopup" PopupControlID="sh_programmaticPopup"
            BackgroundCssClass="modal" PopupDragHandleControlID="sh_programmaticPopupDragHandle"
            RepositionMode="RepositionOnWindowScroll">
        </ajax:ModalPopupExtender>
        <asp:Panel ID="sh_programmaticPopup" runat="server" Style="display: none; text-align: center">
            <table id="sh_myPopup" style="background-color: #e5f6d1;width: 100%; height: 100%">
                <tr>
                    <td style="text-align: right;">
                        <asp:ImageButton ID="sh_b_close" runat="server" CausesValidation="false" OnClick="sh_b_close_Click"
                            ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%; height: 100%;">
                        <iframe id="sh_myFrame" runat="server" scrolling="yes" style="width: 100%; height: 100%; border: none; position: relative;"></iframe>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="sh_cmdPopup" runat="server" Style="display: inline-table; text-align: center; width: 200px; height: 200px; position: relative">
            <table id="sh_TablePopup" runat="server" style="background-color: #EFF1F4">
                <tr>
                    <td style="text-align: right;">
                        <asp:ImageButton ID="sh_b_close2" runat="server" CausesValidation="false" OnClick="sh_b_close2_Click"
                            ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                    </td>
                </tr>
                <tr style="background-color: #518012">
                    <td>
                        <asp:Label ID="sh_Label5" runat="server" Text="AVAILABLE INDEXES" SkinID="LabelLoginBig"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Button runat="server" ID="sh_b_cmd" Style="display: none" />
        <ajax:ModalPopupExtender runat="server" ID="sh_cmdExt" BehaviorID="sh_cmdExtBehavior" TargetControlID="sh_b_cmd"
            PopupControlID="sh_cmdPopup" BackgroundCssClass="modal">
        </ajax:ModalPopupExtender>
        <asp:HiddenField ID="sh_hiddenIndex" runat="server" Value="0" />
        <asp:Panel ID="sh_p_ConfirmDel" runat="server" CssClass="popupWarningBig">
            <div class="divWarning">
                <asp:Button ID="sh_b_curr" CausesValidation="false" runat="server" Text="Current" Visible="true"
                    CssClass="buttonBig" OnClick="sh_currPopup_Click" />
            </div>
            <div class="divWarning">
                <asp:Button ID="sh_b_all" CausesValidation="false" runat="server" Text="All Versions"
                    Visible="true" CssClass="buttonBig" OnClick="sh_allPopup_Click" />
            </div>
            <div class="divWarning">
                <asp:Button ID="sh_b_canc" CausesValidation="false" runat="server" Text="Cancel" Visible="true"
                    CssClass="buttonBig" />
            </div>
        </asp:Panel>
        <asp:HiddenField ID="sh_hDel" runat="server" />
        <ajax:ModalPopupExtender ID="sh_ConfDel_ext" runat="server" TargetControlID="sh_hDel" CancelControlID="sh_b_canc"
            BehaviorID="sh_ConfDel_extBehavior" PopupControlID="sh_p_ConfirmDel" Drag="false" BackgroundCssClass="modal">
        </ajax:ModalPopupExtender>
         <input type="hidden" clientidmode="Static" id="sp_txtHidData" runat="server" />
        <input type="hidden" clientidmode="Static" id="sp_txtHidData2" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
