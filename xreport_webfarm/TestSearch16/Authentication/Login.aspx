﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="XReportSearch.Authentication.Login"
    Theme="XReportSearchTheme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <link href="~/App_Themes/XReportSearchLoginStyle.css?1" rel="stylesheet" type="text/css" />
        <link href="~/App_Themes/XReportSearchStyle.css?2" rel="stylesheet" type="text/css" />
    <title>XReportSearch</title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="body">
        <div class="logo">
            <img id="Img2" alt="" runat="server" src="~/Images/logo_cedimension.png" />
        </div>
        <div class="login">
            <asp:Button ID="butt" CausesValidation="true" runat="server" Text="Test PGE" Visible="true" CssClass="buttonBig"
                OnClick="butt_Click" />
            <div class="marginLogo">
                <asp:Label ID="lb_err" runat="server" Visible="false" SkinID="LabelLoginBig" Text=""></asp:Label>
            </div>
            <div class="summary">
                <asp:Label ID="lb_error" runat="server" Visible="false" SkinID="LabelLogin" Width="500px"></asp:Label>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
