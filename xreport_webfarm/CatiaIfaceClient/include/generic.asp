<%//@Language="JScript"%>
<%
function removeSoapHdrs(xml){
	var re = /<soap:Envelope.*<soap:Body>/;
	xml = xml.replace(re, "");
	re = /<\/soap:Body><\/soap:Envelope>/;
	xml = xml.replace(re, "");

	return xml;
}

function base64ToBin(b64_data) {
	var stream = Server.CreateObject("ADODB.Stream");
	var stream2 = Server.CreateObject("ADODB.Stream");

	stream.Open();
	stream.CharSet = "us-ascii";
	stream.Type = 2; // adTypeText
	stream.WriteText(b64_data);
	stream.Position = 0;
	
	var content = "" + stream.ReadText();

	stream2.Open();
	stream2.CharSet = "iso-8859-1";
	stream2.Type = 2; // adTypeText
	stream2.WriteText(decodeBase64(content));
	stream2.Position = 0;
	stream2.Type = 1; // adTypeBin

	var bin_data = stream2.Read();
	stream.Close();
	stream2.Close();

	return bin_data;
}

function addTypeAttribute(xml, search_str, attr_type) {
	var nodesList = xml.selectNodes(search_str);
	for(var i = 0; i < nodesList.length; i++) {
		var folderType = xml.createAttribute("type");
		folderType.nodeValue = attr_type;

		nodesList[i].setAttributeNode(folderType);
	}

	return xml;
}

%>
