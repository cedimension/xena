<%@ Language="JScript"%>
<!--#include file="include/soapclient.asp"-->
<!--#include file="include/base64.asp"-->
<!--#include file="include/generic.asp"-->
<!--#include file="include/config.asp"-->
<%
var userXml = Server.CreateObject("Microsoft.XMLDOM");

// Get parameters sent through POST
var userRequest = Request.Form("xmlObj");
var jobReportId = new String(Request.Form("JobReportId"));

// Change request parameters if JobReportId POST parameter exist
if((jobReportId != "undefined") && (jobReportId != "")) {
//if(jobReportId == "") {
	userRequest = "<request><action>getDocument</action><IndexEntries><Columns colname='JobReportId'>" + jobReportId + "</Columns></IndexEntries></request>";
}

userXml.loadXML(userRequest);

var action = userXml.selectSingleNode("/request/action").firstChild.nodeValue;
var user = Request.ServerVariables("AUTH_USER") + "";
if(user.indexOf("\\") >= 0)	{
	user = user.split("\\")[1];
}

if(action == "initData") {
	userXml.loadXML("<request><action>initData</action><IndexEntries><Columns colname=\"UserName\">" + user + "</Columns></IndexEntries></request>");
}

var sc = new SOAPClient();
//sc.setServiceUrl("http://ACCS4169:60080/CatiaIface/xrIISWebServ.asp");
sc.setServiceUrl(webSvcUrl);
sc.setWSDLUrl("CatiaIface.wsdl");
sc.setServiceNamespace("catiaiface");
if(action == "initData") {
	sc.setMethodName("getUserProfiles");
} else if(action == "loadSearchInitData") {
	sc.setMethodName("getSearchData");
} else if(action == "loadNewInitData") {
	sc.setMethodName("getNewData");
} else if(action == "searchDocuments") {
	sc.setMethodName("getDocumentList");
} else if(action == "getDocument") {
	sc.setMethodName("getDocument");
} else if(action == "newDocument") {
	sc.setMethodName("generateDocument");
	sc.addParameter("UserName", user);
} else if(action == "newAndPrintDocument") {
	sc.setMethodName("generateAndPrintDocument");
	sc.addParameter("UserName", user);
} else if(action == "printDocument") {
	sc.setMethodName("printDocument");
	sc.addParameter("UserName", user);
}

var indexEntries = userXml.selectNodes("/request/IndexEntries");

for(var i = 0; i < indexEntries.length; i++) {
	var columns = indexEntries[i].selectNodes("./Columns");
	for(var c = 0; c < columns.length; c++) {
		if(columns[c].attributes.getNamedItem("colname").nodeValue == "Issue") {
			if((columns[c].firstChild) && (columns[c].firstChild.nodeValue == "LAST")) {
				sc.setTOP("1");
				sc.setORDERBY("Issue DESC");
				continue;
			}
		}
		if(columns[c].attributes.getNamedItem("colname").nodeValue == "Group") {
			//columns[c].attributes.getNamedItem("colname").nodeValue = "[Group]";
			continue;
		}
		if(columns[c].attributes.getNamedItem("colname").nodeValue == "Status" && action == "searchDocuments") {
			continue;
		}
		if(columns[c].attributes.getNamedItem("colname").nodeValue == "Line" && action == "searchDocuments") {
			continue;
		}
//		if(columns[c].attributes.getNamedItem("colname").nodeValue == "doct" && action == "searchDocuments") {
//			continue;
//		}
		if(columns[c].firstChild) {
			sc.addParameter(columns[c].attributes.getNamedItem("colname").nodeValue, columns[c].firstChild.nodeValue);
		}
	}
}

var u = sc.call();
var result = Server.CreateObject("Microsoft.XMLDOM") ;
result.loadXML(removeSoapHdrs(sc.getRawXml()));
if(action == "getDocument") {
	var pdf64 = result.selectSingleNode("/RESPONSE/DocumentBody").firstChild.nodeValue;
	Response.ContentType = "application/pdf";
	Response.BinaryWrite(base64ToBin(pdf64));
} else if(action == "newDocument" || action == "newAndPrintDocument") {
	var message = result.createNode(1, "message", "");
	var db_node = result.selectSingleNode("/DocumentBody");
	if(db_node) {
			message.text = db_node.firstChild.nodeValue;
	} else {
			message.text = "OK";
	}
	var r_node = result.selectSingleNode("//RESPONSE");
	if(r_node) {
		r_node.appendChild(message);
	} else {
		result.loadXML("<RESPONSE><message>" + message.text + "</message></RESPONSE>");
	}

	Response.Write(result.xml);
} else {
	result = addTypeAttribute(result, "//RESPONSE/IndexEntries[Columns[@colname='FolderName']]", "folder");
	result = addTypeAttribute(result, "//RESPONSE/IndexEntries[Columns[@colname='ReportName']]", "report");

	if(action == "searchDocuments") {
		result = addTypeAttribute(result, "//RESPONSE/IndexEntries", "report");
	}

	result = addTypeAttribute(result, "//RESPONSE/IndexEntries[Columns[@colname='LINE']]", "line");
	result = addTypeAttribute(result, "//RESPONSE/IndexEntries[Columns[@colname='STATUS']]", "status");
	result = addTypeAttribute(result, "//RESPONSE/IndexEntries[Columns[@colname='DOCT']]", "doct");

	var message = result.createNode(1, "message", "");
	message.text = "OK";
	var r_node = result.selectSingleNode("//RESPONSE");
	if(r_node) {
		r_node.appendChild(message);
		if(action == "initData") {
			var labelEntries = result.createNode(1, "IndexEntries", "");
			var labelType = result.createAttribute("type");
			labelType.nodeValue = "labels";
			labelEntries.setAttributeNode(labelType);

			var col = result.createNode(1, "Columns", "");
			var colType = result.createAttribute("colname");
			colType.nodeValue = "status";
			col.setAttributeNode(colType);
			col.text = "Status";
			labelEntries.appendChild(col);

			var col = result.createNode(1, "Columns", "");
			var colType = result.createAttribute("colname");
			colType.nodeValue = "doct";
			col.setAttributeNode(colType);
			col.text = "Doc Type";
			labelEntries.appendChild(col);

			var col = result.createNode(1, "Columns", "");
			var colType = result.createAttribute("colname");
			colType.nodeValue = "line";
			col.setAttributeNode(colType);
			col.text = "Line";
			labelEntries.appendChild(col);

			var col = result.createNode(1, "Columns", "");
			var colType = result.createAttribute("colname");
			colType.nodeValue = "drawnum";
			col.setAttributeNode(colType);
			col.text = "Draw/Part Num";
			labelEntries.appendChild(col);

			var col = result.createNode(1, "Columns", "");
			var colType = result.createAttribute("colname");
			colType.nodeValue = "issue";
			col.setAttributeNode(colType);
			col.text = "Issue";
			labelEntries.appendChild(col);

			var col = result.createNode(1, "Columns", "");
			var colType = result.createAttribute("colname");
			colType.nodeValue = "sheet";
			col.setAttributeNode(colType);
			col.text = "Sheet";
			labelEntries.appendChild(col);

			var col = result.createNode(1, "Columns", "");
			var colType = result.createAttribute("colname");
			colType.nodeValue = "printer";
			col.setAttributeNode(colType);
			col.text = "Plotter";
			labelEntries.appendChild(col);

			var col = result.createNode(1, "Columns", "");
			var colType = result.createAttribute("colname");
			colType.nodeValue = "description";
			col.setAttributeNode(colType);
			col.text = "Description";
			labelEntries.appendChild(col);

			var col = result.createNode(1, "Columns", "");
			var colType = result.createAttribute("colname");
			colType.nodeValue = "date";
			col.setAttributeNode(colType);
			col.text = "Date";
			labelEntries.appendChild(col);

			r_node.appendChild(labelEntries);
		}
	} else {
		result.loadXML("<RESPONSE><message>OK</message></RESPONSE>");
	}

	Response.Write(result.xml);
}
%>
