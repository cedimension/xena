﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimePicker.ascx.cs"
    Inherits="CustomControls.TimePicker" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:TextBox runat="server" ID="txtHour" BorderColor="#518012" BorderStyle="Solid" ForeColor="#518012" font-size="11px" font-family="Verdana" Font-Bold="true" BorderWidth="2px" Height="18px" Width="40px"></asp:TextBox>
  <ajax:MaskedEditExtender ID="MaskedEditExtender3" runat="server" TargetControlID="txtHour"
                                                MaskType="Time" Mask="99:99" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                            </ajax:MaskedEditExtender>
<asp:TextBox runat="server" ID="txtMinute"></asp:TextBox>
<ajax:NumericUpDownExtender ID="txtMinute_NumericUpDownExtender" runat="server" Enabled="True"
    RefValues=":00;:15;:30;:45;" TargetControlID="txtMinute" Width="40">
</ajax:NumericUpDownExtender>
<asp:TextBox runat="server" ID="txtDayPart"></asp:TextBox>
<ajax:NumericUpDownExtender ID="txtDayPart_NumericUpDownExtender" runat="server"
    Enabled="True" RefValues="AM;PM" TargetControlID="txtDayPart" Width="70">
</ajax:NumericUpDownExtender>
