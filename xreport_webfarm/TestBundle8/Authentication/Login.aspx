﻿<%@ Page Title="XReport Bundles" Language="C#" AutoEventWireup="true"  Theme="XReportBundlesTheme" 
    CodeBehind="Login.aspx.cs" Inherits="XReportBundles.Authentication.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/App_Themes/XReportBundlesLogin.css?1" rel="stylesheet" type="text/css" />
    <title>XReport Bundles</title>
</head>
<body>
    <script language="javascript" type="text/javascript">
        function fnOnUpdateValidators() {
            for (var i = 0; i < Page_Validators.length; i++) {
                var val = Page_Validators[i];
                var ctrl = document.getElementById(val.controltovalidate);
                if (ctrl != null && ctrl.style != null) {
                    if (!val.isvalid) {
                        ctrl.style.borderColor = "Red";
                    }
                    else {
                        ctrl.style.borderColor = "#8cb547";
                    }
                }
            }
        }
    </script>
    <form id="form1" runat="server">
    <div class="body">
        <div class="logo">
            <img id="Img2" alt="" runat="server" src="~/Images/logo_cedimension.png" />
        </div>
        <div class="login">
            <div class="marginLogo">
                <img id="Img1" alt="" runat="server" src="~/Images/splash.png" />
            </div>
            <div class="marginLogin">
                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" Text="Username "
                    SkinID="LabelLogin"></asp:Label>
                <asp:TextBox ID="UserName" runat="server" AutoPostBack="false" SkinID="LoginTextbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    Display="None" ErrorMessage="Inserire Username" ToolTip="Inserire il nome utente"></asp:RequiredFieldValidator>
            </div>
            <div class="marginLogin">
                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password" Text="Password "
                    SkinID="LabelLogin"></asp:Label>
                <asp:TextBox ID="Password" runat="server" TextMode="Password" AutoPostBack="false"
                    SkinID="LoginTextbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                    ErrorMessage="Inserire Password" ToolTip="Inserire la password" Display="None"></asp:RequiredFieldValidator>
            </div>
            <div class="marginLogin">
                <asp:ImageButton ID="LoginButton" runat="server" OnClick="LoginButton_Click" CausesValidation="true"
                    ImageUrl="~/Images/bottone_login.png" />
            </div>
            <div class="summary">
                <asp:Label ID="lb_error" runat="server" Visible="false" SkinID="LabelLogin"></asp:Label>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
