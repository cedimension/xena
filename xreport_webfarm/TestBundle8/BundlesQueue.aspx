﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BundlesQueue.aspx.cs" Inherits="XReportBundles.BundlesQueue"
    Theme="XReportBundlesTheme" ResponseEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Namespace="CustomControls" TagPrefix="cedim" Assembly="XReportBundles" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>XReport Bundles</title>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" charset="UTF-8" />
    <meta http-equiv="content-type" content="text/html" charset="UTF-8" />
    <link href="~/App_Themes/XReportBundles.css?2" rel="stylesheet" type="text/css" />
    <link href="~/App_Themes/XReportBundlesControls.css?2" rel="stylesheet" type="text/css" />
</head>
<body onload="resize()" onresize="resize()" onclick="resize()" onbeforeprint="resize()">
    <script language="javascript" type="text/javascript">

        function fnOnUpdateValidators() {
            var count = 0;
            for (var i = 0; i < Page_Validators.length; i++) {
                var val = Page_Validators[i];
                var ctrl = document.getElementById(val.controltovalidate);
                if (ctrl != null && ctrl.style != null) {
                    if (!val.isvalid) {
                        ctrl.style.borderColor = "Red";
                        ctrl.className = "comboboxError";
                        count++;
                    }
                    else {
                        ctrl.style.borderColor = "#518012";
                        ctrl.className = "combobox";
                    }
                }
            }
        }

        function resize() {
            var page = document.getElementById("PageDiv");
            var pageRight = document.getElementById("PageRight");
            var grid = document.getElementById("divCustomGrid");
            var tbGrid = document.getElementById("tbOutGrid");
            var windowheight = document.documentElement.clientHeight;
            var windowwidth = document.documentElement.clientWidth;
            if (document.getElementById("txtHidData") != null) {
                document.getElementById("txtHidData").value = windowheight;
            }
            if (document.getElementById("txtHidData2") != null) {
                document.getElementById("txtHidData2").value = windowwidth;
            }
            if (page.style != null && page.style.height != null) {
                page.style.height = (windowheight) + "px";
            }
            if (windowheight > 60 && grid != null && grid.style != null && grid.style.height != null && pageRight != null && pageRight.style != null) {
                grid.style.height = (windowheight - 60) + "px";
                pageRight.style.height = (windowheight - 60) + "px";
                if (windowwidth < 800) {
                    pageRight.style.fontSize = "0.5em";
                }
                else if (windowwidth > 2000) {
                    pageRight.style.fontSize = "2em";
                }
                else {
                    pageRight.style.fontSize = "0.8em";
                }
            }
        }

        function TextChanged(controlName) {
            var tb_To;
            if (controlName.indexOf('_From') !== -1)
                tb_To = document.getElementById(controlName.replace("_From", "_To"));
            var tb_From = document.getElementById(controlName);
            if (tb_From.value.length > 0 && tb_To != null)
                tb_To.value = tb_From.value;
        }
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"
        AsyncPostBackTimeout="600" />
    <script language="javascript" type="text/javascript">
        function pageLoad() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').add_showing(onshowing);
            resize();
        }

        function onshowing() {
            if ($find('ModalProgress') != null) {
                var windowwidth = document.documentElement.clientWidth;
                var windowheight = document.documentElement.clientHeight;
                $find('ModalProgress').set_X((parseInt(windowwidth) / 2) - 100);
                $find('ModalProgress').set_Y((parseInt(windowheight) / 2) - 100);
            }
        }

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

        function beginRequest() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').show();
        }

        function endRequest() {
            if ($find('ModalProgress') != null)
                $find('ModalProgress').hide();
        }

        var xPos, yPos;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            if ($get('divCustomGrid') != null) {
                xPos = $get('divCustomGrid').scrollLeft;
                yPos = $get('divCustomGrid').scrollTop;
            }
        }
        function EndRequestHandler(sender, args) {
            if ($get('divCustomGrid') != null) {
                $get('divCustomGrid').scrollLeft = xPos;
                $get('divCustomGrid').scrollTop = yPos;
            }
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <table class="page" id="PageDiv">
                <tr id="PageDiv2">
                    <td class="pageLeft">
                        <ajax:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server" TargetControlID="pShow"
                            CollapseControlID="pHide" ExpandControlID="pHide" Collapsed="false" ImageControlID="imgArrows"
                            ExpandDirection="Horizontal" ExpandedImage="~/Images/hide.png" CollapsedImage="~/Images/show.png">
                        </ajax:CollapsiblePanelExtender>
                        <div>
                            <asp:Panel ID="pShow" runat="server">
                                <table>
                                    <tr class="data">
                                        <td>
                                        </td>
                                        <td class="buttons">
                                            <asp:Button ID="b_assign" CausesValidation="true" runat="server" Text="Search" Visible="true"
                                                CssClass="buttonBig" OnClick="buttonSearch_Click" />
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="lb_limit" runat="server" Text="Max Rows" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_limit" runat="server" SkinID="TextboxSmallLeft" Text="500" TabIndex="1"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="lb_bname" runat="server" Text="Bundle Name" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_bname" runat="server" SkinID="TextboxSmallLeft" TabIndex="2"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="lb_jn" runat="server" Text="Job Name" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_jn" runat="server" SkinID="TextboxSmallLeft" TabIndex="3"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="lb_rn" runat="server" Text="Report Name" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_rn" runat="server" SkinID="TextboxSmallLeft" TabIndex="4"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="Label3" runat="server" Text="Date From" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_data_From" runat="server" SkinID="TextboxDate" TabIndex="5"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tb_data_From"
                                                Display="None"></asp:RequiredFieldValidator>
                                            <asp:ImageButton runat="server" ID="b_data1" ImageUrl="~/Images/calendario.png" CausesValidation="false"
                                                ImageAlign="TextTop" />
                                            <ajax:CalendarExtender ID="CalendarExtender1" runat="server" PopupPosition="BottomRight"
                                                TargetControlID="tb_data_From" PopupButtonID="b_data1" CssClass="calendar" Format="dd/MM/yyyy" />
                                            <ajax:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="tb_data_From"
                                                MaskType="Date" Mask="99/99/9999" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                            </ajax:MaskedEditExtender>
                                            <ajax:MaskedEditValidator ID="MaskedEditValidator1" ControlExtender="MaskedEditExtender1"
                                                runat="server" ControlToValidate="tb_data_From" IsValidEmpty="False" Display="Dynamic">
                                            </ajax:MaskedEditValidator>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td>
                                            <asp:Label ID="Label4" runat="server" Text="Date To" SkinID="LabelFilter"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tb_data_To" runat="server" SkinID="TextboxDate" TabIndex="6"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_data_To"
                                                Display="None"></asp:RequiredFieldValidator>
                                            <asp:ImageButton runat="server" ID="b_data2" ImageUrl="~/Images/calendario.png" CausesValidation="false"
                                                ImageAlign="TextTop" />
                                            <ajax:CalendarExtender ID="CalendarExtender2" runat="server" PopupPosition="BottomRight"
                                                TargetControlID="tb_data_To" PopupButtonID="b_data2" CssClass="calendar" Format="dd/MM/yyyy" />
                                            <ajax:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="tb_data_To"
                                                MaskType="Date" Mask="99/99/9999" CultureName="it-IT" ClearMaskOnLostFocus="false">
                                            </ajax:MaskedEditExtender>
                                            <ajax:MaskedEditValidator ID="MaskedEditValidator2" ControlExtender="MaskedEditExtender2"
                                                runat="server" ControlToValidate="tb_data_To" IsValidEmpty="False" Display="Dynamic">
                                            </ajax:MaskedEditValidator>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td colspan="2">
                                            <asp:RangeValidator ID="rangeVal" ControlToValidate="tb_limit" MinimumValue="0" MaximumValue="1000"
                                                Type="Integer" ErrorMessage="Max Rows must be an integer between 0 and 1000."
                                                ForeColor="White" Font-Bold="true" runat="server" Font-Size="Small">
                                            </asp:RangeValidator>
                                        </td>
                                    </tr>
                                    <tr class="data">
                                        <td colspan="2">
                                            <asp:CustomValidator ID="cv_dates" runat="server" ErrorMessage="Start date must be less/equal than end date."
                                                Display="Dynamic" OnServerValidate="cv_dates_ServerValidate" ForeColor="White"
                                                Font-Size="Small" Font-Bold="true" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </td>
                    <td class="pageLeftCollapse">
                        <div>
                            <asp:Panel ID="pHide" runat="server">
                                <table>
                                    <tr class="data">
                                        <td>
                                            <asp:Image ID="imgArrows" runat="server" Height="25px" Width="15px" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </td>
                    <td class="pageRight" id="PageRight">
                        <cedim:CeGridView ID="gvBundlesN" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                            AllowPaging="true" PagerStyle-CssClass="pager" HeaderStyle-ForeColor="White"
                            HeaderStyle-Height="25px" RowStyle-Height="25px" Visible="true" OnPageIndexChanging="gvBundlesN_PageIndexChanging"
                            SkinID="Tables2" Width="100%" Height="100%" EmptyDataRowStyle-Font-Bold="true"
                            EmptyDataText="No Data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="#518012"
                            ShowHeader="False" OnRowCreated="gvBundlesN_RowCreated" DataKeyNames="BundleName"
                            OnRowCommand="gvBundlesN_OnRowCommand">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Panel ID="pnlBundle" runat="server">
                                            <table style="width: 100%; vertical-align: middle">
                                                <tr>
                                                    <td style="width: 20px">
                                                        <asp:Image ID="imgCollapsible" Style="margin-right: 10px; margin-left: 7px; width: 17px;
                                                            height: 17px" runat="server" />
                                                    </td>
                                                    <td style="width: 40%">
                                                        <asp:Label ID="lb_1" runat="server" Text="Bundle Name: " SkinID="LabelErr"></asp:Label>
                                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("BundleName")%>' SkinID="LabelErr"></asp:Label>
                                                    </td>
                                                    <td style="width: 40%">
                                                        <asp:Label ID="Label5" runat="server" Text="Number of entries: " SkinID="LabelErr"></asp:Label>
                                                        <asp:Label ID="Label6" runat="server" Text='<%#Eval("TOT")%>' SkinID="LabelErr"></asp:Label>
                                                    </td>
                                                    <td style="text-align: right; width: 20px">
                                                        <asp:ImageButton ID="b_delAll" runat="server" CausesValidation="false" ImageUrl="~/Images/delete.png"
                                                            Visible="true" CommandName="DeleteAll" Width="17px" ToolTip="Delete All" Height="17px" />
                                                    </td>
                                                    <td style="text-align: right; width: 20px">
                                                        <asp:ImageButton ID="b_recAll" runat="server" CausesValidation="false" ImageUrl="~/Images/group.png"
                                                            Visible="true" CommandName="RecipientAll" Width="17px" ToolTip="Recipient List"
                                                            Height="17px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlLog" runat="server" Width="95%" Style="margin-left: 60px; margin-top: 5px;
                                            margin-bottom: 5px; height: 0px; overflow: hidden;" BorderColor="#518012" BorderStyle="Solid"
                                            BorderWidth="1px">
                                            <asp:GridView AutoGenerateColumns="False" SkinID="TablesInner" ID="gvQueue" runat="server"
                                                DataKeyNames="BundleName" Width="100%" Height="100%" OnRowCommand="gvQueue_OnRowCommand"
                                                OnRowDataBound="gvQueue_OnRowDataBound">
                                                <Columns>
                                                    <asp:BoundField HeaderText="BundleName" DataField="BundleName" ItemStyle-Width="15%" />
                                                    <asp:BoundField HeaderText="JobName" DataField="JobName" ItemStyle-Width="20%" />
                                                    <asp:BoundField HeaderText="ReportName" DataField="ReportName" ItemStyle-Width="25%" />
                                                    <asp:BoundField HeaderText="ReportTimeRef" DataField="ReportTimeRef" ItemStyle-Width="25%" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="b_det" runat="server" CausesValidation="false" CommandName="Details"
                                                                ImageUrl="~/Images/details.gif" ToolTip="Details" Width="20px" Height="20px" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="b_del" runat="server" CausesValidation="false" ImageUrl="~/Images/delete.png"
                                                                Visible="true" CommandName="Del" Width="17px" ToolTip="Delete" Height="17px" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="b_rec" runat="server" CausesValidation="false" ImageUrl="~/Images/group.png"
                                                                Visible="true" CommandName="Recipient" Width="17px" ToolTip="Recipient List"
                                                                Height="17px" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <div style="clear: both">
                                                &nbsp;</div>
                                        </asp:Panel>
                                        <ajax:CollapsiblePanelExtender ID="ctlCollapsiblePanel" runat="Server" TargetControlID="pnlLog"
                                            Collapsed="True" ExpandControlID="pnlBundle" CollapseControlID="pnlBundle" AutoCollapse="False"
                                            AutoExpand="False" ImageControlID="imgCollapsible" ExpandedImage="~/Images/collapseDG.png"
                                            CollapsedImage="~/Images/expandDG.png" ExpandDirection="Vertical" ScrollContents="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </cedim:CeGridView>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="programmaticPopup" runat="server" Style="display: none; text-align: center;
                overflow: auto; height: 400px; width: 800px">
                <table id="myPopup" style="background-color: #EFF1F4;">
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="b_close" runat="server" CausesValidation="false" OnClick="b_close_Click"
                                ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DetailsView ID="dv_Bundles" runat="server" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true"
                                AutoGenerateRows="False" CssClass="gridView" SkinID="TablesView" Visible="true"
                                RowStyle-ForeColor="#518012" Width="775px" RowStyle-HorizontalAlign="Left">
                                <Fields>
                                    <asp:BoundField HeaderText="JobReportID" DataField="JobReport_ID" />
                                    <asp:BoundField HeaderText="ReportId" DataField="ReportId" />
                                    <asp:BoundField HeaderText="JobReportName" DataField="JobReportName" />
                                    <asp:BoundField HeaderText="JobNumber" DataField="JobNumber" />
                                    <asp:BoundField HeaderText="FilterValue" DataField="FilterValue" />
                                    <asp:BoundField HeaderText="TotPages" DataField="TotPages" />
                                    <asp:BoundField HeaderText="PrintCopies" DataField="PrintCopies" />
                                    <asp:BoundField HeaderText="XferDateDiff" DataField="XferDateDiff" />
                                    <asp:BoundField HeaderText="XferRecipient" DataField="XferRecipient" />
                                </Fields>
                            </asp:DetailsView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Button runat="server" ID="hiddenTargetControlForModalPopup" Style="display: none" />
            <ajax:ModalPopupExtender runat="server" ID="programmaticModalPopup" BehaviorID="programmaticModalPopupBehavior"
                TargetControlID="hiddenTargetControlForModalPopup" PopupControlID="programmaticPopup"
                BackgroundCssClass="modal" PopupDragHandleControlID="programmaticPopupDragHandle"
                RepositionMode="RepositionOnWindowScroll">
            </ajax:ModalPopupExtender>
            <asp:Panel ID="panelrec" runat="server" Style="display: none; text-align: center;
                overflow: auto; height: 450px; width: 600px">
                <table id="Table1" style="background-color: #EFF1F4;">
                    <tr>
                        <td style="text-align: right;">
                            <asp:ImageButton ID="b_closeRec" runat="server" CausesValidation="false" ImageUrl="~/Images/close.gif"
                                ToolTip="Close" Width="14px" Height="14px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gv_recOut" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                AllowPaging="false" HeaderStyle-ForeColor="White" HeaderStyle-Height="25px" RowStyle-Height="25px"
                                Visible="true" SkinID="Tables" Width="550px" EmptyDataRowStyle-Font-Bold="true"
                                EmptyDataText="No Data" EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="#518012"
                                ShowHeader="true" OnRowCreated="gv_recOut_RowCreated" DataKeyNames="BundleName, BundleCategory, BundleSkel">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <table style="width: 100%; vertical-align: middle; height: 25px">
                                                <tr style="height: 25px">
                                                    <td style="width: 20px">
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lb_1" runat="server" Text="Bundle Name" SkinID="LabelFilter"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label1" runat="server" Text="Bundle Category" SkinID="LabelFilter"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label2" runat="server" Text="Bundle Skel" SkinID="LabelFilter"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Panel ID="pnlRecOut" runat="server" Style="margin-right: 10px; height: 25px">
                                                <table style="width: 100%; vertical-align: middle">
                                                    <tr style="height: 25px">
                                                        <td style="width: 20px">
                                                            <asp:Image ID="imgCollapsible" Style="margin-right: 10px; margin-left: 7px; width: 17px;
                                                                height: 17px" runat="server" />
                                                        </td>
                                                        <td>
                                                            <span style="font-weight: bold; font-size: 14px; color: #518012;">
                                                                <%#Eval("BundleName")%>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span style="font-weight: bold; font-size: 14px; color: #518012;">
                                                                <%#Eval("BundleCategory")%>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span style="font-weight: bold; font-size: 14px; color: #518012;">
                                                                <%#Eval("BundleSkel")%>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlRecIn" runat="server" Width="90%" Style="margin-left: 10px; margin-right: 10px;
                                                margin-top: 5px; margin-bottom: 5px; height: 0px; overflow: hidden;" BorderColor="#518012"
                                                BorderStyle="Solid" BorderWidth="1px">
                                                <asp:GridView ID="gv_recipient" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                                    AllowPaging="false" HeaderStyle-ForeColor="White" HeaderStyle-Height="20px" Width="95%"
                                                    RowStyle-Height="20px" Visible="true" SkinID="Tables" EmptyDataRowStyle-Font-Bold="true"
                                                    EmptyDataRowStyle-HorizontalAlign="Center" EmptyDataRowStyle-ForeColor="#518012"
                                                    Font-Size="Small">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Recipient" DataField="RecipientName" />
                                                    </Columns>
                                                </asp:GridView>
                                                <div style="clear: both">
                                                    &nbsp;</div>
                                            </asp:Panel>
                                            <ajax:CollapsiblePanelExtender ID="ctlCollapsiblePanel" runat="Server" TargetControlID="pnlRecIn"
                                                CollapsedSize="0" Collapsed="True" ExpandControlID="pnlRecOut" CollapseControlID="pnlRecOut"
                                                AutoCollapse="False" AutoExpand="False" ImageControlID="imgCollapsible" ExpandedImage="~/Images/collapseDG.png"
                                                CollapsedImage="~/Images/expandDG.png" ExpandDirection="Vertical" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Button runat="server" ID="b_rec_h" Style="display: none" />
            <ajax:ModalPopupExtender runat="server" ID="ModalPopupExtenderRec" BehaviorID="ModalPopupExtenderRec"
                TargetControlID="b_rec_h" PopupControlID="panelrec" CancelControlID="b_closeRec"
                BackgroundCssClass="modal" RepositionMode="RepositionOnWindowScroll">
            </ajax:ModalPopupExtender>
            <asp:UpdateProgress ID="UpdateProgress" runat="server" DisplayAfter="200">
                <ProgressTemplate>
                    <asp:Panel ID="panelUpdateProgress" runat="server">
                        <div>
                            <img id="Img1" src="~/Images/loader.gif" alt="" runat="server" />
                        </div>
                    </asp:Panel>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <ajax:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
                BackgroundCssClass="modalProgress" PopupControlID="panelUpdateProgress" BehaviorID="ModalProgress">
            </ajax:ModalPopupExtender>
            <asp:Panel ID="popupOKcanc" runat="server" CssClass="popupWarning">
                <div class="divWarning">
                    <asp:Label ID="lb_w" runat="server" Text="Are you sure you want to delete the selected entry?"
                        SkinID="LabelWarning"></asp:Label>
                </div>
                <div class="divWarning">
                    <asp:Button ID="b_yes" CausesValidation="false" runat="server" Text="Yes" Visible="true"
                        CssClass="buttonBig" OnClick="b_yes_Click" />
                    <asp:Button ID="b_popupCanc" CausesValidation="false" runat="server" Text="Cancel"
                        Visible="true" CssClass="buttonBig" />
                </div>
            </asp:Panel>
            <asp:HiddenField ID="h3" runat="server" />
            <ajax:ModalPopupExtender ID="ModalPopupExtenderConf" runat="server" TargetControlID="h3"
                CancelControlID="b_popupCanc" BehaviorID="ModalPopupExtender3" PopupControlID="popupOKcanc"
                Drag="false" BackgroundCssClass="modal">
            </ajax:ModalPopupExtender>
            <asp:Panel ID="popupOK" runat="server" CssClass="popupWarning">
                <div class="divWarning">
                    <asp:Label ID="lb_warning" runat="server" SkinID="LabelWarning"></asp:Label>
                </div>
                <div class="divWarning">
                    <asp:Button ID="b_ok" CausesValidation="false" runat="server" Text="OK" Visible="true"
                        CssClass="buttonBig" />
                </div>
            </asp:Panel>
            <asp:HiddenField ID="h4" runat="server" />
            <ajax:ModalPopupExtender ID="ModalPopupExtenderOK" runat="server" TargetControlID="h4"
                CancelControlID="b_ok" BehaviorID="ModalPopupExtenderOK" PopupControlID="popupOK"
                Drag="false" BackgroundCssClass="modal">
            </ajax:ModalPopupExtender>
            <asp:Panel ID="popupError" runat="server" CssClass="popupError">
            <table id="tbpopupError" style="background-color: #e5f6d1; width: 100%; height: 100%;">
                <tr>
                    <td style="text-align: right;">
                        <asp:ImageButton ID="ImageButton1" runat="server" CausesValidation="false" OnClick="popupError_Click"
                            ImageUrl="~/Images/close.gif" ToolTip="Close" Width="14px" Height="14px" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%; height: 100%; text-align: center">
                        <asp:Label ID="lb_popupError" runat="server" Text="" SkinID="LabelWarning"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:HiddenField ID="h_popupError" runat="server" />
        <ajax:ModalPopupExtender ID="ModalPopupExtender_Error" runat="server" TargetControlID="h_popupError"
            BehaviorID="ModalPopupExtender_Error" PopupControlID="popupError" Drag="false" BackgroundCssClass="modal">
        </ajax:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
