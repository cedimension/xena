﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Download.aspx.cs" Inherits="CeReportSearch.GetDocument" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <link rel="stylesheet" href="dist/css/AdminLTE.css?4" />
    <link rel="stylesheet" href="dist/css/XReport.css?1" />
    <link rel="stylesheet" href="dist/css/skins/XReportSkins.css?1" />
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="plugins/font/font-awesome.min.css" />
    <link rel="stylesheet" href="plugins/font/ionicons.min.css" />
    

    <title>CeReport Search</title>
    <style type="text/css">
        html
        {
            height: 100%;
            overflow: hidden;
        }
        
        body
        {
            margin: 0;
            padding: 0;
            height: 100%;
        }
    </style>
</head>
<body>
    <section class="content">
      <div class="error-page" runat="server" visible="false" id="div_error">
         <h2 class="headline text-yellow"><i class="fa fa-warning text-yellow"></i></h2>
        <div class="error-content">
          <h2><b>An error occurs</b></h2>
          <p>The system could not find the document you were looking for.</p>
          <p>Please contact the administrators of CeReport.</p>
          <p runat="server" id="p_error"></p>
        </div>
      </div>
      <div class="error-page" runat="server" visible="false" id="div_ko">
         <h2 class="headline text-yellow"><i class="fa fa-warning text-yellow"></i></h2>
        <div class="error-content">
          <h2><b>No rows selected.</b></h2>
          <p>You have to select al least 1 row.</p>
        </div>
      </div>
    </section>
</body>
</html>


