﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginUnicredit.aspx.cs" Inherits="CeReportSearch.Auth.LoginUnicredit" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <title>CeReport Search</title>
    <link rel="stylesheet" href="../dist/css/AdminLTE.css" />
    <link rel="stylesheet" href="../dist/css/Login.css?2" />
</head>
<body class="hold-transition back">
    <div class="logo">
        <img id="Img2" alt="" runat="server" src="~/images/logo_cedimension.png" />
    </div>
    <div class="login-box">
        <div class="login-logo loginTitle loginBox xfont" style="font-weight:200" >
            <b>CeReport</b> Search
        </div>
        <div class="callout callout-info loginBox">
            <div>
                <asp:Label ID="lb_error" runat="server" Visible="false" CssClass="loginLabel"></asp:Label>
            </div>
            <div>
                &nbsp 
            </div>
              <div>
                &nbsp 
            </div>
            <div>
                <asp:Label ID="lb_error_det" runat="server" Visible="false" CssClass="loginLabel"></asp:Label>
            </div>
        </div>
    </div>
</body>
</html>