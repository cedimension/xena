﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="CeReportSearch.Search" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>CeReportSearch</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <link rel="stylesheet" href="dist/css/AdminLTE.css?3" />
    <link rel="stylesheet" href="dist/css/loader.css?1" />
    <link rel="stylesheet" href="dist/css/site.css" />
    <link rel="stylesheet" href="dist/css/XReport.css?1" />
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="bootstrap/css/bootstrap.responsive.min.css" />
    <link rel="stylesheet" href="plugins/font/font-awesome.min.css" />
    <link rel="stylesheet" href="plugins/font/ionicons.min.css" />
    <link rel="stylesheet" href="dist/css/skins/XReportSkins.css?1" />
</head>
<body id="MainBody" runat="server" class="hold-transition sidebar-mini" onresize="setHeight()">
    <form runat="server" id="form">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
        <div class="wrapper" style="overflow:hidden">
            <header class="main-header">
                <div class="logo">
                    <span class="logo-mini">
                        <img src="images/logo.png" alt="XR" height="40" width="40" /></span>
                    <span class="logo-lg">
                        <img src="images/logo.png" alt="XR" height="40" width="40" />
                        <label id="lb_app" runat="server" style="font-size: large; font-weight: 200" class="xfont"><b>CeReportSearch</b></label></span>
                </div>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="logoCed">
                                <img src="images/logo_cedimension.png" alt="" />
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image gavatar" runat="server" id="div_avatar" visible="false">
                        </div>
                        <div class="pull-left image" runat="server" id="div_avatar_img" visible="false">
                        </div>
                        <div class="pull-left info">
                            <p id="lb_userdescr" class="xfont" runat="server"></p>
                            <p id="lb_username" class="xfont" runat="server"></p>
                            <p id="lb_prof" class="xfont" runat="server"></p>
                        </div>
                    </div>
                    <div class="treePanel treeFont">
                        <asp:UpdatePanel ID="UpdatePanelMenu" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <input type="hidden" id="hd_tab" name="hd_tab" value="" />
                                <ul class="sidebar-menu" runat="server" id="menu" style="margin-top: 10px">
                                </ul>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </section>
            </aside>
            <div class="content-wrapper xfont" style="overflow: hidden">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <asp:UpdatePanel ID="UpdatePanelTitle" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <h5 class="box-title" runat="server" id="title" style="font-variant: small-caps; font-weight: 600"></h5>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="col-xs-5 input-group pull-right xfont" id="filterSearch" runat="server">
                            <div class="pull-right xfont">
                                <asp:UpdatePanel ID="UpdatePanelCmd" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <label id="lb_valid" runat="server" style="color: red; font-size: 14px; font-variant: small-caps; font-weight: 800" visible="false"></label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <button type="button" class="btn btn-default btn-sm btn-ced" id="b_search_CE" runat="server" onserverclick="b_search_CE_Click">
                                            <span class="fa fa-search" aria-hidden="true"></span>&nbsp;&nbsp;Search
                                        </button>
                                        <button type="button" id="b_dowload_CE" class="btn btn-default btn-sm btn-ced" runat="server" onserverclick="b_download_CE_Click">
                                            <span class="fa fa-download" aria-hidden="true"></span>&nbsp;&nbsp;Download
                                        </button>
                                          <button type="button" id="b_archive_CE" class="btn btn-default btn-sm btn-ced" runat="server" onserverclick="b_archive_CE_Click">
                                            <span class="fa fa-file-archive-o" aria-hidden="true"></span>&nbsp;&nbsp;Archive
                                        </button>
                                        <button type="button" id="b_export_CE" class="btn btn-default btn-sm btn-ced" runat="server" onserverclick="b_export_CE_click">
                                            <span class="fa fa-file-excel-o" aria-hidden="true"></span>&nbsp;&nbsp;CSV
                                        </button>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div id="collapseCE1" class="panel-collapse collapse in">
                            <div class="box-body">
                                <asp:UpdatePanel ID="UpdatePanelFilters" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="row" id="filterCE" runat="server" style="width: 99%">
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div id="collapseCE2" class="box-footer">
                            <asp:UpdatePanel ID="UpdatePanelTable" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div id="divTable" runat="server" class="box-footer tab-content content-table" style="overflow: scroll">
                                        <label id="lb_count" runat="server" style="color: dimgrey; font-size: 14px; font-variant: small-caps; font-weight: 400" visible="false"></label>
                                        <asp:GridView ID="gd_TableCE" runat="server" AutoGenerateColumns="false" Width="100%" 
                                            CssClass="table table-striped table-bordered" SelectedRowStyle-BackColor="Gray"
                                            HeaderStyle-CssClass="tableHeader" HeaderStyle-ForeColor="White"
                                            EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-HorizontalAlign="Center" AllowSorting="true"
                                            OnRowDataBound="gd_TableCE_RowDataBound" OnSorting="gd_TableCE_Sorting">
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="main-footer">
                <div class="pull-right hidden-xs xfont">
                    <b>Version</b>
                    <label id="lb_version" runat="server" style="font-weight: 500"></label>
                    <a href="Notes.html" target="_blank">Notes</a>
                </div>
                <div class="hidden-xs xfont">
                    <strong><a href="http://www.cedimension.eu/it/">CeDimension</a></strong>
                </div>
            </footer>
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="control-sidebar-home-tab">
                    </div>
                    <div class="tab-pane" id="control-sidebar-settings-tab">
                    </div>
                </div>
            </aside>
        </div>
        <asp:UpdateProgress runat="server" ID="UpdateProgress">
            <ProgressTemplate>
                <div class="loading" id="loading">
                    <img src="images/loading.gif" alt="" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="UpdatePanelMod" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="popupError" runat="server" Style="display: none;">
                    <div id="errorModal" class="modal-danger">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="b_close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">ERROR</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <label id="lb_error" runat="server"></label>
                                    </p>
                                    <p>
                                        <label id="lb_errorDet" runat="server"></label>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="h2" runat="server" />
                <ajax:ModalPopupExtender ID="ModalPopupExtenderError" runat="server" TargetControlID="h2"
                    BehaviorID="ModalPopupExtenderError" PopupControlID="popupError" Drag="false" CancelControlID="b_close"
                    BackgroundCssClass="modalBackground1">
                </ajax:ModalPopupExtender>
                <asp:Panel ID="popupSession" runat="server" Style="display: none;">
                    <div id="errorSession" class="modal-warning">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="b_close2">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">SESSION ENDED</h4>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <b>YOUR SESSION HAS EXPIRED</b>
                                        <br />
                                        Please reload the application pressing the F5 key or clicking the reload button.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="h3" runat="server" />
                <ajax:ModalPopupExtender ID="ModalPopupExtenderSession" runat="server" TargetControlID="h3"
                    BehaviorID="ModalPopupExtenderSession" PopupControlID="popupSession" Drag="false" CancelControlID="b_close2"
                    BackgroundCssClass="modalBackground1">
                </ajax:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script src="dist/js/app.min.js"></script>
    <script src="dist/js/right_bar.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <script>

        function pageLoad() {
            setHeight();
        }

        $('#form').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $(".readonly").on('keydown paste', function (e) {
            e.preventDefault();
        });

        $('#form').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $(".ce_data").inputmask("yyyy-mm-dd", { "placeholder": "yyyy-mm-dd" });

        function setHeight() {
            windowHeight = $(window).innerHeight();
            $('.wrapper').css('min-height', (windowHeight) + 'px');
            $('.wrapper').css('max-height', (windowHeight) + 'px');
            $('.content-wrapper').css('max-height', (windowHeight - $(".content-wrapper").offset().top - 40) + 'px');
            $('.content-wrapper').css('min-height', (windowHeight - $(".content-wrapper").offset().top - 40) + 'px');
            $('.content-table').css('max-height', (windowHeight - $(".content-table").offset().top - 60) + 'px');
            $('.content-table').css('min-height', (windowHeight - $(".content-table").offset().top - 60) + 'px');
            $('.main-footer').css('max-height', (windowHeight - $(".main-footer").offset().top) + 'px');
            $('.main-footer').css('min-height', (windowHeight - $(".main-footer").offset().top) + 'px');
        }

        function chkAll(chk) {
            $('#<%=gd_TableCE.ClientID%>').find("input:checkbox").each(function () {
                if (this != chk) { this.checked = chk.checked; }
            });
        }

        function setTab(value) {
            $('#hd_tab').val(value);

        }

        var parameter = Sys.WebForms.PageRequestManager.getInstance();

        parameter.add_endRequest(function () {
            $(".ce_data").inputmask("yyyy-mm-dd", { "placeholder": "yyyy-mm-dd" });
        });
    </script>

</body>
</html>
