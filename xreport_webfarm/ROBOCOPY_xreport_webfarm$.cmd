echo off
setlocal EnableDelayedExpansion
IF /I [%1]==[dryrun] (
echo.dryrun mode activated
SET only_LOG=/L
) ELSE (
echo.normal mode activated
SET only_LOG=
)
SET dirFrom=%~dp0
SET filesToExclude= /XF log_moveBackup.txt /XF log_moveBackup.txt /XF xrIISWebServ.asp /XD TestSearchC7
::set lastFolder with the name of the last folder 
SET lastFolder=xreport_webfarm$\
SET lastFolder2=\xreport_webfarm\
SET currentLastFolder=%dirFrom:~-17%
@cd C:\Windows\system32
@for /F "usebackq tokens=1,2,3,4,5,6,7 delims=-//.:, " %%a IN (`echo %%date%%-%%time%%`) do set my_datetime=%%a-%%b-%%c-%%d-%%e-%%f
@echo %my_datetime%
@IF /I NOT [%lastFolder%]==[%currentLastFolder%] (
@IF /I NOT [%lastFolder2%]==[%currentLastFolder%] (
@echo The current last folder [%currentLastFolder%] is not [%lastFolder%]
pause
exit -1
)
)
SET log=log_robocopy_
SET fileLog=LOG\%log%_%my_datetime%.txt
mkdir "%dirFrom%LOG"
@FOR /L %%G IN (1,1,8) DO (
SET dirTo=\\C0CTLPW00%%G\%lastFolder%
robocopy "%dirFrom% " "!dirTo! " /E /MAX:10485760 /XO /XD backup /XD old /XD LOG /XD .svn /XF %log%* /XF *.swp /XF *.back /XF *.old /XF *.baK /XF *TESTSAN* %filesToExclude% %only_LOG% /LOG+:%dirFrom%%fileLog%
)
@echo.---------------------------------------------------------------------- >> %dirFrom%%fileLog%
type "%dirFrom%%fileLog%" | findstr /I /c:"newer" /c:"New File" /c:"EXTRA File" /c:"Dest " >> "%dirFrom%%fileLog%"
@echo.type "%dirFrom%%fileLog%" ^| findstr /I /c:"newer" /c:"Dest " ^>^> "%dirFrom%%fileLog%"
notepad %dirFrom%%fileLog%
::max 10 MB /MAX:10485760
::cartelle backup escluse /XD backup 
::cartelle old escluse /XD old 
::cartelle .svn escluse /XD .svn
::file *.back esclusi /XF *.back /XF *.old 
::file log esclusi /XF %log%*  /XD LOG 
::/L :: List only - don't copy, timestamp or delete any files.