﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services.Protocols;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text;
using XReportW3SvcClient.XReportWebIface;


namespace XReportW3SvcClient
{
    public partial class request_proxy : System.Web.UI.Page
    {
        public const String AUTH_METHOD_NTLM = "NTLM";
        public const String AUTH_METHOD_RACF = "RACF";

        public const String REQUEST_ACTION_INIT = "init";
        public const String REQUEST_ACTION_LOGIN = "login";
        public const String REQUEST_ACTION_LOGOUT = "logout";
        public const String REQUEST_ACTION_USER_CONFIG = "getUserConfig";
        public const String REQUEST_ACTION_DOWNLOAD_CONFIG = "downloadConfig2";

        private String authenticationMethod = "";
        private String applicationName = "";
        private String authenticatedUser = "";
        private String authenticatedUserPwd = "";

        private XmlDocument requestXml;
        private String requestAction = "";

        private void getRequestParams()
        {
            String xmlIn = "";
            if (this.Request.RequestType == "POST")
            {
                if (this.Request.Form["xmlObj"] == null || (this.Request.Form["xmlObj"] as String) == "")
                {
                    this.returnError("Missing or empty request parameter(POST)");
                }
                xmlIn = this.Request.Form["xmlObj"];
            }
            else
            {
                if (this.Request.QueryString["xmlObj"] == null || (this.Request.QueryString["xmlObj"] as String) == "")
                {
                    this.returnError("Missing or empty request parameter(GET)");
                }
                xmlIn = this.Request.QueryString["xmlObj"];
            }

            this.requestXml = new XmlDocument();
            this.requestXml.LoadXml(xmlIn);
        }

        private void getConfigParams()
        {
            this.authenticationMethod = ConfigurationManager.AppSettings["authenticationMethod"];
            this.applicationName = ConfigurationManager.AppSettings["applicationName"];
        }

        public static byte[] StrToByteArray(string str)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return encoding.GetBytes(str);
        }

        private void returnError(String error)
        {
            List<String> responseList = new List<string>();
            responseList.Add("<RESPONSE><Action>");
            responseList.Add(this.requestAction);
            responseList.Add("</Action><IndexEntries><Columns colname=\"ActionMessage\">");
            responseList.Add("Unexpected error. Click details for further information\n");
			//responseList.Add(error);
            responseList.Add("</Columns></IndexEntries>");
            responseList.Add("<DocumentBody>");
            responseList.Add(Convert.ToBase64String(StrToByteArray(error)));
            responseList.Add("</DocumentBody>");
            responseList.Add("</RESPONSE>");
            string responseData = String.Join(String.Empty, responseList.ToArray());
            this.Response.Write(responseData);

            this.Response.AppendToLog("Error: " + error);

            this.Response.End();
        }

        private void returnData(String data)
        {
            this.Response.Write(data);
            this.Response.End();
        }

        private void checkAuthentication()
        {
            if (this.Session["User"] != null && (this.Session["User"] as String) != "" && this.requestAction != REQUEST_ACTION_LOGIN)
            {
                this.authenticatedUser = this.Session["User"] as String;
                this.authenticatedUserPwd = this.Session["Password"] as String;
                if (this.requestAction == REQUEST_ACTION_INIT)
                {
                    this.requestAction = REQUEST_ACTION_USER_CONFIG;
                }
            }
            else
            {
                switch (this.authenticationMethod)
                {
                    case AUTH_METHOD_NTLM:
						//this.authenticatedUser = "it\\svc-xr-adm";
                        this.authenticatedUser = this.Request.ServerVariables["AUTH_USER"];
						if(this.authenticatedUser.LastIndexOf("\\") >= 0) {
							this.authenticatedUser = this.authenticatedUser.Substring(this.authenticatedUser.LastIndexOf("\\") + 1);
						}
                        //this.authenticatedUser = System.IO.Path.GetFileName(this.authenticatedUser);
                        if (this.requestAction == REQUEST_ACTION_INIT)
                        {
                            this.requestAction = REQUEST_ACTION_USER_CONFIG;
                        }
                        break;
                    case AUTH_METHOD_RACF:
                        switch (this.requestAction)
                        {
                            case REQUEST_ACTION_INIT:
                                this.returnData("<RESPONSE><Action>REQUIRE_LOGIN</Action></RESPONSE>");
                                break;
                            case REQUEST_ACTION_LOGIN:
                                XmlNodeList nodes = this.requestXml.SelectNodes("/REQUEST/IndexEntries/Columns");
                                if (nodes.Count <= 0)
                                {
                                    this.returnError("Missing parameters into login action");
                                }

                                foreach (XmlNode node in nodes)
                                {
                                    XmlAttribute col = node.Attributes["colname"];
                                    if (col == null)
                                    {
                                        this.returnError("Missing parameters into login action");
                                    }
                                    if (col.Value == "UserName")
                                    {
                                        this.authenticatedUser = node.InnerText;
                                        this.Session["User"] = this.authenticatedUser;
                                    }
                                    if (col.Value == "Password")
                                    {
                                        this.authenticatedUserPwd = node.InnerText;
                                        this.Session["Password"] = this.authenticatedUserPwd;
                                    }
                                }

                                this.requestAction = REQUEST_ACTION_USER_CONFIG;
                                break;
                            case REQUEST_ACTION_LOGOUT:
                                this.authenticatedUser = "";
                                this.authenticatedUserPwd = "";
                                this.Session["User"] = "";
                                this.Session["Password"] = "";
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        this.returnError("AUTH_METHOD " + this.authenticationMethod + " is unknown. Please check application configuration");
                        break;
                }
            }
        }

        private void readRequestAction()
        {
            XmlNodeList nodes = this.requestXml.SelectNodes("/REQUEST/Action");

            if (nodes.Count <= 0)
            {
                this.returnError("Missing Action request parameter");
            }

            foreach (XmlNode node in nodes)
            {
                this.requestAction = node.InnerText;
                if (this.requestAction == "downloadConfig")
                {
                    this.requestAction = "downloadConfig2";
                }
                break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.getRequestParams();
            this.getConfigParams();
            this.readRequestAction();
            this.checkAuthentication();

            Encoding param_encoding = Encoding.GetEncoding("utf-8");
            Encoding utf_8 = Encoding.UTF8;
            
            if (ConfigurationManager.AppSettings["soapcodepage"] != null)
            {
                param_encoding = Encoding.GetEncoding(ConfigurationManager.AppSettings["soapcodepage"]);
            
            }

            this.Response.AppendToLog(this.authenticationMethod + ";" + this.applicationName + ";" + ConfigurationManager.AppSettings["w3SvcUrl"]);
            if (this.requestAction == REQUEST_ACTION_LOGOUT)
            {
                this.Response.Write("<RESPONSE><Action>" + this.requestAction + "</Action></RESPONSE>");
                return;
            }

            try
            {
                DocumentData document = new DocumentData();
                if (this.requestXml.SelectSingleNode("/REQUEST").Attributes["IndexName"] != null)
                {
                    document.IndexName = this.requestXml.SelectSingleNode("/REQUEST").Attributes["IndexName"].Value;
                }
                else
                {
                    document.IndexName = "";
                }

                if (this.requestXml.SelectSingleNode("/REQUEST").Attributes["DocumentType"] != null)
                {
                    document.DocumentType = this.requestXml.SelectSingleNode("/REQUEST").Attributes["DocumentType"].Value;
                }
                else
                {
                    document.DocumentType = "";
                }

                if (this.requestXml.SelectSingleNode("/REQUEST").Attributes["FileName"] != null)
                {
                    document.FileName = this.requestXml.SelectSingleNode("/REQUEST").Attributes["FileName"].Value;
                }
                else
                {
                    document.FileName = "";
                }

                if (this.requestXml.SelectSingleNode("/REQUEST").Attributes["TOP"] != null)
                {
                    document.TOP = this.requestXml.SelectSingleNode("/REQUEST").Attributes["TOP"].Value;
                }
                else
                {
                    document.TOP = "";
                }

                if (this.requestXml.SelectSingleNode("/REQUEST").Attributes["ORDERBY"] != null)
                {
                    document.ORDERBY = this.requestXml.SelectSingleNode("/REQUEST").Attributes["ORDERBY"].Value;
                }
                else
                {
                    document.ORDERBY = "";
                }

                if (this.requestXml.SelectSingleNode("/REQUEST").Attributes["DISTINCT"] != null)
                {
                    document.DISTINCT = this.requestXml.SelectSingleNode("/REQUEST").Attributes["DISTINCT"].Value;
                }
                else
                {
                    document.DISTINCT = "";
                }

                XmlNodeList nodes = this.requestXml.SelectNodes("/REQUEST/IndexEntries");

                if (nodes.Count > 0 || this.requestAction == REQUEST_ACTION_USER_CONFIG || this.requestAction == REQUEST_ACTION_INIT)
                {
                    int totEntries = nodes.Count;
                    if (this.requestAction == REQUEST_ACTION_USER_CONFIG || this.requestAction == REQUEST_ACTION_INIT)
                    {
                        totEntries += 4;
                    }

                    IndexEntry[] entries = new IndexEntry[totEntries];
                    int c = 0;
                    if (this.requestAction == REQUEST_ACTION_USER_CONFIG || this.requestAction == REQUEST_ACTION_INIT)
                    {
                        IndexEntry entry = new IndexEntry();
                        column[] cols = new column[4];

                        column col = new column();
                        col.colname = "UserName";
                        col.Value = this.authenticatedUser;
                        cols[0] = col;

                        col = new column();
                        col.colname = "AuthMethod";
                        col.Value = this.authenticationMethod;
                        cols[1] = col;

                        col = new column();
                        col.colname = "ApplName";
                        col.Value = this.applicationName;
                        cols[2] = col;

                        col = new column();
                        col.colname = "Password";
                        col.Value = this.authenticatedUserPwd;
                        cols[3] = col;

                        entry.Columns = cols;
                        entries[c] = entry;
                        c++;
                    }

                    foreach (XmlNode node in nodes)
                    {
                        IndexEntry entry = new IndexEntry();

                        if (node.Attributes["JobReportId"] != null)
                        {
                            entry.JobReportId = node.Attributes["JobReportId"].Value;
                        }
                        else
                        {
                            entry.JobReportId = "";
                        }

                        if (node.Attributes["FromPage"] != null)
                        {
                            entry.FromPage = node.Attributes["FromPage"].Value;
                        }
                        else
                        {
                            entry.FromPage = "";
                        }

                        if (node.Attributes["ForPages"] != null)
                        {
                            entry.ForPages = node.Attributes["ForPages"].Value;
                        }
                        else
                        {
                            entry.ForPages = "";
                        }

                        XmlNodeList xmlCols = node.SelectNodes("Columns");
                        if (xmlCols.Count <= 0 && this.requestAction != REQUEST_ACTION_USER_CONFIG && this.requestAction != REQUEST_ACTION_DOWNLOAD_CONFIG)
                        {
                            this.returnError("Missing parameters for request action " + this.requestAction);
                        }

                        column[] cols = new column[xmlCols.Count];
                        int i = 0;
                        foreach (XmlNode xmlCol in xmlCols)
                        {
                            column col = new column();
                            col.colname = xmlCol.Attributes["colname"].Value;
                            //byte[] b_encoding = Encoding.Convert(utf_8, param_encoding, utf_8.GetBytes(xmlCol.InnerText!=null ? xmlCol.InnerText : ""));
                            
                            //col.Value = param_encoding.GetString(b_encoding);
                            col.Value = xmlCol.InnerText;

                            if (xmlCol.Attributes["operation"] != null)
                            {
                                col.operation = xmlCol.Attributes["operation"].Value;
                            }
                            else
                            {
                                col.operation = "";
                            }

                            if (xmlCol.Attributes["Min"] != null)
                            {
                                col.Min = xmlCol.Attributes["Min"].Value;
                            }
                            else
                            {
                                col.Min = "";
                            }

                            if (xmlCol.Attributes["Max"] != null)
                            {
                                col.Max = xmlCol.Attributes["Max"].Value;
                            }
                            else
                            {
                                col.Max = "";
                            }

                            cols[i] = col;
                            i++;
                        }

                        entry.Columns = cols;
                        entries[c] = entry;
                        c++;
                    }

                    document.IndexEntries = entries;
                }
                nodes = this.requestXml.SelectNodes("/REQUEST/DocumentBody");

                if (nodes.Count > 0)
                {
                    foreach (XmlNode node in nodes)
                    {
                        document.DocumentBody = Convert.FromBase64String(node.InnerText);
                        break;
                    }
                }

                DocumentData documentResponse = null;
                //xreportwebiface soapClient = new xreportwebiface(this.authenticatedUser,ConfigurationManager.AppSettings["w3SvcUrl"]);
                XReportWebIface.xreportwebiface soapClient = new XReportWebIface.xreportwebiface(this.authenticatedUser, ConfigurationManager.AppSettings["w3SvcUrl"]);
                //soapClient.Url = ConfigurationManager.AppSettings["w3SvcUrl"];
                //soapClient.Url = "http://d3edmptv01.corp.generali.net:50080/XReportWebIface/xrIISWebServ.asp?wsdl";
                //soapClient.Timeout = 60000;
                //Authentication authHeader = new Authentication();
                //authHeader.userid = this.authenticatedUser;
                //soapClient.authenticationValue = authHeader;
                
                Type soapClientType = soapClient.GetType();
                if (ConfigurationManager.AppSettings["soapcodepage"] != null) {
                    soapClient.RequestEncoding = param_encoding;
                }
                //soapClient.RequestEncoding = System.Text.Encoding.GetEncoding("ISO-8859-1");

                MethodInfo methodToCall = soapClientType.GetMethod(this.requestAction);
                
                if (methodToCall == null)
                {
                    this.returnError("Can't call an undefined action " + this.requestAction);
                }
                
                //byte[] b_utf8 = Encoding.Convert(param_encoding, utf_8, Data.DocumentBody);
                //string[] lines = new string[] { utf_8.GetString(b_utf8) };

                DocumentData[] methodParms = new DocumentData[1];
                methodParms[0] = document;
                documentResponse = methodToCall.Invoke(soapClient, methodParms) as DocumentData;
                this.Response.ContentEncoding = param_encoding;
                if (this.requestAction == "getDocumentByPages")
                {
                    this.Response.AddHeader("Content-Type", "application/pdf");
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + documentResponse.FileName);
                    this.Response.BinaryWrite(documentResponse.DocumentBody);
                }
                else if (this.requestAction == "getJobReportPdf")
                {
                    this.Response.AddHeader("Content-Type", "application/pdf");
                    //Response.AddHeader("Content-Disposition", "attachment; filename=" + documentResponse.FileName);
                    this.Response.BinaryWrite(documentResponse.DocumentBody);
                }
                /*else if (this.requestAction == "downloadConfig2")
                {
                    this.Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + documentResponse.FileName);
                    this.Response.BinaryWrite(documentResponse.DocumentBody);
                }*/
                else if (this.requestAction == "downloadInput")
                {
                    this.Response.AddHeader("Content-Type", "application/x-gzip");
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + documentResponse.FileName);
                    this.Response.BinaryWrite(documentResponse.DocumentBody);
                }
                else if (this.requestAction == "downloadOutput")
                {
                    this.Response.AddHeader("Content-Type", "application/zip");
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + documentResponse.FileName);
                    this.Response.BinaryWrite(documentResponse.DocumentBody);
                }
                else
                {
                    List<String> responseList = new List<string>();
                    responseList.Add("<RESPONSE");
                    responseList.Add(" IndexName=\"");
                    responseList.Add(documentResponse.IndexName);
                    responseList.Add("\"");
                    responseList.Add(" DocumentType=\"");
                    responseList.Add(documentResponse.DocumentType);
                    responseList.Add("\"");
                    responseList.Add(" FileName=\"");
                    responseList.Add(documentResponse.FileName);
                    responseList.Add("\"");
                    responseList.Add(" TOP=\"");
                    responseList.Add(documentResponse.TOP);
                    responseList.Add("\"");
                    responseList.Add(" ORDERBY=\"");
                    responseList.Add(documentResponse.ORDERBY);
                    responseList.Add("\"");
                    responseList.Add(" DISTINCT=\"");
                    responseList.Add(documentResponse.DISTINCT);
                    responseList.Add("\"");
                    responseList.Add(">");
                    responseList.Add("<Action>");
                    responseList.Add(this.requestAction);
                    responseList.Add("</Action>");

                    if (documentResponse.IndexEntries != null)
                    {
                        for (int a = 0; a < documentResponse.IndexEntries.Length; a++)
                        {
                            responseList.Add("<IndexEntries");
                            responseList.Add(" JobReportId=\"");
                            responseList.Add(documentResponse.IndexEntries[a].JobReportId);
                            responseList.Add("\"");
                            responseList.Add(" FromPage=\"");
                            responseList.Add(documentResponse.IndexEntries[a].FromPage);
                            responseList.Add("\"");
                            responseList.Add(" ForPages=\"");
                            responseList.Add(documentResponse.IndexEntries[a].ForPages);
                            responseList.Add("\"");
                            responseList.Add(">");

                            if (documentResponse.IndexEntries[a].Columns != null)
                            {
                                for (int b = 0; b < documentResponse.IndexEntries[a].Columns.Length; b++)
                                {
                                    responseList.Add("<Columns");
                                    responseList.Add(" colname=\"");
                                    responseList.Add(documentResponse.IndexEntries[a].Columns[b].colname);
                                    responseList.Add("\"");
                                    responseList.Add(" Min=\"");
                                    responseList.Add(documentResponse.IndexEntries[a].Columns[b].Min);
                                    responseList.Add("\"");
                                    responseList.Add(" Max=\"");
                                    responseList.Add(documentResponse.IndexEntries[a].Columns[b].Max);
                                    responseList.Add("\"");
                                    responseList.Add(" operation=\"");
                                    responseList.Add(documentResponse.IndexEntries[a].Columns[b].operation);
                                    responseList.Add("\"");
                                    responseList.Add(">");
                                    //byte[] b_utf8 = Encoding.Convert(param_encoding, utf_8, param_encoding.GetBytes(documentResponse.IndexEntries[a].Columns[b].Value!= null ? documentResponse.IndexEntries[a].Columns[b].Value : ""));
                                    //responseList.Add(utf_8.GetString(b_utf8));
                                    responseList.Add(documentResponse.IndexEntries[a].Columns[b].Value);
                                    responseList.Add("</Columns>");
									
									if(documentResponse.IndexEntries[a].Columns[b].colname == "ActionMessage") {
										responseList.Add("<Columns");
                                    	responseList.Add(" colname=\"ProxyServer");
                                    	responseList.Add("\"");
                                    	responseList.Add(" Min=\"");
                                    	responseList.Add("\"");
                                    	responseList.Add(" Max=\"");
                                    	responseList.Add("\"");
                                    	responseList.Add(" operation=\"");
                                    	responseList.Add("\"");
                                    	responseList.Add(">");
                                    	responseList.Add(Server.MachineName);
                                    	responseList.Add("</Columns>");
									}
                                }
                            }

                            responseList.Add("</IndexEntries>");
                        }
                    }

                    if (documentResponse.DocumentBody != null)
                    {
                        responseList.Add("<DocumentBody>");
                        responseList.Add(Convert.ToBase64String(documentResponse.DocumentBody));
                        responseList.Add("</DocumentBody>");
                    }

                    responseList.Add("</RESPONSE>");

                    string responseData = String.Join(String.Empty, responseList.ToArray());
                    this.Response.Write(responseData);
                }
            }
            catch (Exception ex)
            {
                this.returnError(ex.Message + "(" + ex.InnerException + ")");
            }
        }
    }
}