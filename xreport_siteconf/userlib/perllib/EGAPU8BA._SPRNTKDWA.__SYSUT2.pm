package XReport::OUTPUT::PageMKExcel;
use constant DEBUG => 1;

=example of parsing file 
<jobreport name="$JobReportName" linePos="raw"   >
	<exit 
	    type="PageMKExcel" 
	    ref="EGAPU8BA._SPRNTKDWA.__SYSUT2.pm"   
		mainHeader="Kunden-System,Kundennummer,Name,Vorname,Geb.dat,Geschlecht,Kurzwortlaut,Personentyp,Str./Hausnr ,Plz./Ort,Dev.land,ONACE ,Rechtsform" 
		mainHeaderKeysNewRecord="1"	/>
  <page name="PAGE001" test="1" />   
</jobreport>
=cut

use Data::Dumper;
use Excel::Writer::XLSX;


sub new {
  my ($className, $parser, $workdir, $exitdom) = @_;
  #printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - className=$className - workdir=$workdir - parser=".ref($parser)." - exitdom=".ref($exitdom) );

  my $DEBUG = $exitdom->getAttribute('DEBUG'); 
  my $ExistTypeMap = $exitdom->getAttribute('ExistTypeMap');
  die "ExistTypeMap parameter is missing in parsingfile!" if(!$ExistTypeMap);
  $DEBUG  =  $DEBUG or DEBUG or $main::veryverbose;
  
  my $self = {
		   'jr' => $parser->{jr}
		  ,'ExistTypeMap' => $ExistTypeMap
		  ,'DEBUG' => $DEBUG
          ,'parser' => $parser
          ,'workdir' => $workdir
          ,'exitdef' => $exitdom
          ,'lines' => {} 
          ,'pageCount' => 0
          ,'FILEHIN' => undef
		  };
  bless $self, $className;
}
 
 
sub printOnlyInDebug{
	$self->{'DEBUG'} and i::logit("DEBUG - ".join('::',@_));
}

sub Finalize {
    my $self = shift;
	printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() - ref(self)=".ref($self).join('::', caller()))  ;
	($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  line=".scalar(keys %{$self->{'lines'}}) );
 
	my ($JobReportName, $JobReportId) = $self->{jr}->getValues(qw(JobReportName JobReportId));
	
	my $FH = self->{FILEHIN};
	if($FH)
	{
		close($FH) or die "ApplicationError: Unable to close file - $! -$^E";	
	}
	
	my $outputNumericKeys_ = $self->{'exitdef'}->getAttribute('outputNumericKeys');
	my @outputNumericKeys = split(/,/, $outputNumericKeys_); 
	
	
	my $ExcelFileNameTrue = $self->{jr}->getFileName('Excel'); $ExcelFileNameTrue =~ s/\.excel$/\.xlsx/i;	
	printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileNameTrue=".$ExcelFileNameTrue );
	my $workbook = Excel::Writer::XLSX->new($ExcelFileNameTrue); 
	my $worksheet = $workbook->add_worksheet($JobReportName);	
	$worksheet->set_first_sheet();
	$worksheet->activate();
	
	
	my $row = 0;
	my $col = 0;
	my $initial_col = 0;
	my $max_col = 0;
	
	
	for my $typeOfLines (qw(lines))
	{
		$row = 0;
		for my $nLine (1..scalar(keys %{$self->{$typeOfLines}}))
		{
			$col=$initial_col;
			for my $i (sort {$a <=> $b} keys %{$self->{$typeOfLines}->{$nLine}})
			{ 
				my $value = $self->{$typeOfLines}->{$nLine}->{$i};
				#check if the value is numeric 
				if((grep {$_ eq $i} @outputNumericKeys) and ($value =~ /^[\d\.\,\-]+$/))
				{
					$value =~ s/\.//g;
					$value =~ s/,/\./g;
					$worksheet->write_number($row, $col, $value);
				}
				else
				{
					$worksheet->write_string($row, $col, $value);
				}
				$col++;
				$max_col = $col if ($col >$max_col);
			}
			$row++;
		}
		$initial_col=1+$max_col;
	}
	  
	$workbook->close(); 
	  
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - totalValuePattern=".$self->{'exitdef'}->getAttribute ('mainPattern')); 
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - pageCount=".$self->{'pageCount'}); 
}

sub Print {
    my $self = shift;
    my $page = shift;
	$self->{pageCount}++;
	
	printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - ref(self)=".ref($self)." ref(page)=".ref($page).join('::', caller())) if ($self->{pageCount} == 1);
	my $FH = self->{FILEHIN};
	if(DEBUG)
	{ 
		if(!$FH)
		{
			$FH = Symbol::gensym();
			my $ExcelFileName = $self->{jr}->getFileName('Excel'); $ExcelFileName =~ s/\.excel$/\.txt/i;
			open($FH, ">$ExcelFileName") or die "ApplicationError: Unable to open file $ExcelFileName - $! -$^E";		
			self->{FILEHIN} = $FH;
		}
	}
	($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileName=".$ExcelFileName );
  
	
	my $outputHeader_ = $self->{'exitdef'}->getAttribute('outputHeader');
	my @outputHeader = split(/,/, $outputHeader_); 
	my $mainHeader_ = $self->{'exitdef'}->getAttribute('mainHeader');
	my @mainHeader = split(/,/, $mainHeader_);
	
	
	
	
	
	
	my $mainPattern = $self->{'exitdef'}->getAttribute('mainPattern');
	my $mainHeaderKeysNewRecord_ = $self->{'exitdef'}->getAttribute('mainHeaderKeysNewRecord');
	my @mainHeaderKeysNewRecord = split(/,/, $mainHeaderKeysNewRecord_); 

	my @ascii_lines; 
	my $line_count = 0;
	my $nLine = scalar( keys %{$self->{'lines'}}) ; #number of lines already read
	if(!$nLine)
	{
		#write header
		$nLine = 1;		
		for my $k(0..$#outputHeader)
		{  
			$self->{'lines'}->{$nLine}->{$k+1} = $outputHeader[$k];
			($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - header ".($k+1)."=".$outputHeader[$k]);
		}
	}
	for my $line(@{$page->lineList()}) {
		my $lines = $line->AsciiValueList();
		for my $ascii_line (map {substr($_,1)} @$lines)
		{
			push @ascii_lines, $ascii_line;
			(DEBUG) and print $FH "$ascii_line\n";#TESTSAN
			$line_count++;
			
			#my @tokens = map { $_ =~ /^\s*(.*?)\s*$/; $1} ($ascii_line =~ qr{$mainPattern}); 
			my @tokens = ($ascii_line =~ qr{$mainPattern}); 
			if((scalar(@tokens)) and ( length($ascii_line) > 0 ))
			{
				($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - match $i=\n".join('::',@tokens)) if( length($ascii_line) > 0 );
				for my $i(0..$#mainHeader)
				{
					my $regex = $mainHeader[$i];
					if($tokens[0] =~ qr/$regex/i)
					{
						if(grep {$_ eq ''.($i+1)} @mainHeaderKeysNewRecord)
						{
							$self->{'lines'}->{++$nLine} = {}; 
						}
						$self->{'lines'}->{$nLine}->{$i+1} = $1 if ($tokens[2] =~ /^\s*(.*?)\s*$/);
						($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() $nLine - ".($i+1)."=".$tokens[2]);
					}
				}
			}
			else
			{
				($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - not match $i=\n".$ascii_line) if( length($ascii_line) > 0 );
			} 
		}
	}
    
	return 1;
}

__PACKAGE__;


