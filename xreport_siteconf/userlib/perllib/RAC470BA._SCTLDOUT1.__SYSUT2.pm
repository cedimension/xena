package XReport::OUTPUT::PageMKExcel;
use constant DEBUG => 0;

=example of parsing file 
<jobreport name="$JobReportName" linePos="raw"   >
	<exit  
	    type="PageMKExcel" 
	    ref="EGAQR7BA._SPRNT1007.__SYSUT2.pm"  
		mainPattern="^([^;]*);([^;]*);([^;]*);([^;]*);([^;]*);([^;]*);([^;]*);([^;]*);([^;]*);([^;]*);([^;]*);([^;]*);([^;]*);([^;]*);([^;]*);([^;]*)$"
		outputKeys="3,13,14,15,16"
		outputHeader="System,Abwicklung,Währung,Saldo,Saldo" 
	/>	 
  <page name="PAGE001" test="1" />   
</jobreport>
=cut

use Data::Dumper;
use Excel::Writer::XLSX;


sub new {
  my ($className, $parser, $workdir, $exitdom) = @_;
  #printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - className=$className - workdir=$workdir - parser=".ref($parser)." - exitdom=".ref($exitdom) );
  my $DEBUG = $exitdom->getAttribute('DEBUG'); 
  my $ExistTypeMap = $exitdom->getAttribute('ExistTypeMap');
  die "ExistTypeMap parameter is missing in parsingfile!" if(!$ExistTypeMap);
  $DEBUG  =  $DEBUG or DEBUG or $main::veryverbose;
  
  my $self = {
		   'jr' => $parser->{jr}
		  ,'DEBUG' => $DEBUG
		  ,'ExistTypeMap' => $ExistTypeMap
          ,'parser' => $parser
          ,'workdir' => $workdir
          ,'exitdef' => $exitdom
          ,'mainHeader' => {}
          ,'mainPattern' => ''
          ,'lines' => {} 
          ,'pageCount' => 0
          ,'FILEHIN' => ''
          ,'out_xls' => {}  
		  };
  bless $self, $className;


  for my $node(@{$self->{'exitdef'}->getChildNodes()})
  {
  	$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - NodeName:".$node->getNodeName());
  	#if($node->getNodeName() eq 'patterns')
  	#{
  	#	my $typeOfPage = $node->getAttribute('typeOfPage'); 
  	#	$self->{patterns}->{$typeOfPage}->{output_order_keys_mapping} = $node->getAttribute('output_order_keys_mapping');
  	#	for my $node2(grep {$_->getNodeName() eq 'pattern' } @{$node->getChildNodes()})
  	#	{
  	#		$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - NodeName2:".$node2->getNodeName());
  	#		my $order = $node2->getAttribute('order');
  	#		my $regex = $node2->getAttribute('regex');
  	#		my $numberOfTokens = $node2->getAttribute('numberOfTokens');
  	#		$self->{patterns}->{$typeOfPage}->{regex}->{$order} = $regex;
  	#		$self->{patterns}->{$typeOfPage}->{numberOfTokens}->{$order} = $numberOfTokens;
  	#	}
    #
  	#}
  	if($node->getNodeName() eq 'cutvar_rules')
  	{
  		for my $node2(grep {$_->getNodeName() eq 'cutvar_rule' } @{$node->getChildNodes()})
  		{
  			$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - NodeName2:".$node2->getNodeName());
  			my $line = $node2->getAttribute('line');
  			my $regex = $node2->getAttribute('regex');
  			$self->{cutvar_rules}->{$line} = $regex;
  		}
  	}
  }
  $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - patterns:".Dumper(%{$self->{patterns}}));
  return $self;
}
 
 
sub printOnlyInDebug{
	my $self = shift;
	$self->{'DEBUG'} and i::logit("DEBUG - ".join('::',@_));
}

sub Finalize {
    my $self = shift;
	printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() - ref(self)=".ref($self).join('::', caller()))  ;
	($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  line=".scalar(keys %{$self->{'lines'}}) );
 
	if($self->{'DEBUG'})
	{
		my $FH = self->{FILEHIN};
		close($FH) or die "ApplicationError: Unable to close file - $! -$^E";
	}
	
	my ($JobReportName, $JobReportId) = $self->{jr}->getValues(qw(JobReportName JobReportId));
	
	my $ExcelFileNameTrue = $self->{jr}->getFileName('Excel'); $ExcelFileNameTrue =~ s/\.excel$/\.xlsx/i;	
	printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileNameTrue=".$ExcelFileNameTrue );
	my $workbook = Excel::Writer::XLSX->new($ExcelFileNameTrue); 
	my $worksheet = $workbook->add_worksheet($JobReportName);	
	$worksheet->set_first_sheet();
	$worksheet->activate();
	
	
	my $row = 0;
	my $col = 0;
	my $initial_col = 0;
	my $initial_row = 0;
	my $max_col = 0;
	
	my $firstTable_col_key_value  = $self->{'exitdef'}->getAttribute('firstTable_col_key_value');

	if($firstTable_col_key_value)
	{
		my @cutvar_values = @{$self->{'cutvar_values'}};
		for my $c_k_eq_v( split(/,/, $firstTable_col_key_value))
		{
			my($c, $k, $v) = ( split(/=/, $c_k_eq_v));

			my $r = 0; $c--;
			$worksheet->write_string($r, $c, $k);
			$r++;
			if($v =~ /get_cutvarkey\((\d+)\)/i)
			{
				$v = $cutvar_values[$1-1];
			}
			elsif($v =~ /get_jr_field\((.+)\)/i)
			{
				$v = $self->{'jr'}->getValues($1);
			}
			$worksheet->write_string($r, $c, $v);
		}
		$initial_row = 2;
	}
	
	
	for my $typeOfLines (qw(lines))
	{
		$row = $initial_row;
		for my $nLine (1..scalar(keys %{$self->{$typeOfLines}}))
		{
			$col=$initial_col;
			for my $value (map {$self->{$typeOfLines}->{$nLine}->{$_} } (sort {$a <=> $b} keys %{$self->{$typeOfLines}->{$nLine}}))
			{
				if($value =~ /^[\d\.\,\-]+$/)
				{
					$value =~ s/\.//g;
					$value =~ s/,/\./g;
					$worksheet->write_number($row, $col, $value);
				}
				else
				{
					$worksheet->write_string($row, $col, $value);
				}
				$col++;
				$max_col = $col if ($col >$max_col);
			}
			$row++;
		}
		$initial_col=1+$max_col;
	}
	  
	$workbook->close(); 
	  
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - totalValuePattern=".$self->{'exitdef'}->getAttribute ('mainPattern')); 
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - pageCount=".$self->{'pageCount'}); 
}

sub Print {
    my $self = shift;
    my $page = shift;
	$self->{pageCount}++;
	
	printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - ref(self)=".ref($self)." ref(page)=".ref($page).join('::', caller())) if ($self->{pageCount} == 1);
	
	
	my $FH;
	if($self->{'DEBUG'} )
	{
		$FH = self->{FILEHIN};
		if(!$FH)
		{
			$FH = Symbol::gensym();
			my $ExcelFileName = $self->{jr}->getFileName('Excel'); $ExcelFileName =~ s/\.excel$/\.txt/i;
			open($FH, ">$ExcelFileName") or die "ApplicationError: Unable to open file $ExcelFileName - $! -$^E";		
			self->{FILEHIN} = $FH;
		}
	}
	($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileName=".$ExcelFileName );
 
 
	
	my $mainPattern = $self->{'exitdef'}->getAttribute('mainPattern');
	my $outputKeys_ = $self->{'exitdef'}->getAttribute('outputKeys');
	my $outputHeader_ = $self->{'exitdef'}->getAttribute('outputHeader');
	my @outputKeys = split(/,/, $outputKeys_);
	my @outputHeader = split(/,/, $outputHeader_);
 
		

	my @ascii_lines;
	($self->{pageCount} == 1) and (!scalar(keys %{$self->{'mainHeader'}})) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - ref(mainHeader)1=".ref($self->{'mainHeader'}));

	my $line_count = 0;
	my $cutvar_not_yet_found = 1;
	
	my $nLine = scalar( keys %{$self->{'lines'}}) ; #number of lines already read
	if(!$nLine)
	{
		#write header
		for my $k(0..$#outputKeys)
		{
			my $key = $outputKeys[$k];
			my $value = $outputHeader[$k];
			$self->{'lines'}->{'1'}->{$key} = $value;
		}
		$nLine++;
	}
	for my $line(@{$page->lineList()}) {
		my $lines = $line->AsciiValueList();
		for my $ascii_line (map {substr($_,1)} @$lines)
		{
			push @ascii_lines, $ascii_line;
			$self->{'DEBUG'} and print $FH "$ascii_line\n";
			$line_count++;
			
			
			if(exists $self->{'cutvar_rules'}->{$line_count})
			{
				my $cutvar_regex = $self->{'cutvar_rules'}->{$line_count};
				my @tokens = map { $_ =~ /^\s*(.+?)\s*$/; $1} ($ascii_line =~ qr/$cutvar_regex/i);
				if(scalar(@tokens) )
				{
					if($cutvar_not_yet_found)
					{
						$cutvar_not_yet_found = 0;
						$self->{'cutvar_values'} = [];
						$self->{'cutvar'} = $tokens[0];
						$cutvar = $self->{'cutvar'};
						$self->{'cutvars'}->{$cutvar}++;
					}
					for my $j(0..$#tokens)
					{
						push @{$self->{'cutvar_values'}}, $tokens[$j];
					}
				}
			}
			
			my @tokens = map { $_ =~ /^\s*(.*?)\s*$/; $1} ($ascii_line =~ qr{$mainPattern}); 
			if((scalar(@tokens)) and ( length($ascii_line) > 0 ))
			{
				$self->{'lines'}->{++$nLine} = {}; 
				for my $j(1..$#tokens+1)
				{
					if(grep {(($_) and ($_ eq $j))} @outputKeys)
					{
						$self->{'lines'}->{$nLine}->{$j} = $tokens[$j-1];
					}
				} 
			}
			else
			{
				($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - not match $i=\n".$ascii_line) if( length($ascii_line) > 0 );
			} 
		}
	}
	
    return 1;
}

__PACKAGE__;


