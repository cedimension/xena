package XReport::OUTPUT::PageMKExcel;
use constant DEBUG => 0;

=example of parsing file 
<jobreport name="$JobReportName" linePos="raw"   >
	<exit 
	    type="PageMKExcel"
		DEBUG="0"
	    ExistTypeMap="3" 
	    ref="FDMTG3MA._SPRINTA5.__SYSUT2.pm"  
		mainHeader="^(\s*Org.Einheit\s+)(Muenzsigel\s+)(Bezeichnung\s+)(Stand\s+)(Interne\s+Schwebe\s+)(externe\s+Schwebe\s+)(Unzen\s+)(Tagesendkurs\s+)(EURO-GW\.\s*)$" 
		mainPattern="^(.{18})(\d+\s+)(.{30}\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s*)$"
		mainKeysNotEmpty="1"
		finalHeader="^(\s*Total\s+)(Muenzsigel\s+)(Stand\s+)(Interne Schwebe\s+)(externe Schwebe\s+)(EURO-GW.\s*)$" 
		finalPattern="^(.{18})(\d+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s*)$" 
		totalValuePattern="^(\s+TOTAL\s+)([\d\.\,\-]+\s*)$"		
		firstTable_col_key_value="1=Jobname=get_jr_field(ComputedJobName),2=Reportname= MÜNZEN-STANDMELDUNG CVA160-1,3=Anforderungsdatum=get_cutvarkey(2)"
																								
	/>
  <page name="PAGE001" test="1" />   
</jobreport>
=cut

use Data::Dumper;
use Excel::Writer::XLSX;


sub new {
  my ($className, $parser, $workdir, $exitdom) = @_;
  #printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - className=$className - workdir=$workdir - parser=".ref($parser)." - exitdom=".ref($exitdom) );
  my $DEBUG = $exitdom->getAttribute('DEBUG'); 
  my $ExistTypeMap = $exitdom->getAttribute('ExistTypeMap');
  die "ExistTypeMap parameter is missing in parsingfile!" if(!$ExistTypeMap);
  $DEBUG  =  $DEBUG or DEBUG or $main::veryverbose;
  
  my $self = {
		   'jr' => $parser->{jr}
		  ,'DEBUG' => $DEBUG
		  ,'ExistTypeMap' => $ExistTypeMap
          ,'parser' => $parser
          ,'workdir' => $workdir
          ,'exitdef' => $exitdom
          ,'mainHeader' => {}
          ,'mainPattern' => ''
          ,'lines' => {}
          ,'finalHeader' => {}
          ,'finalPattern' => ''
          ,'finalLines' => {}
          ,'totalLines' => {}
          ,'pageCount' => 0
          ,'FILEHIN' => ''
          ,'out_xls' => {}  
		  };
  bless $self, $className;  

  for my $node(@{$self->{'exitdef'}->getChildNodes()})
  {
  	$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - NodeName:".$node->getNodeName());
  	#if($node->getNodeName() eq 'patterns')
  	#{
  	#	for my $node2(grep {$_->getNodeName() eq 'pattern' } @{$node->getChildNodes()})
  	#	{
  	#		$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - NodeName2:".$node2->getNodeName());
  	#		my $order = $node2->getAttribute('order');
  	#		my $regex = $node2->getAttribute('regex');
  	#		my $numberOfTokens = $node2->getAttribute('numberOfTokens');
  	#		$self->{patterns}->{regex}->{$order} = $regex;
  	#		$self->{patterns}->{numberOfTokens}->{$order} = $numberOfTokens;
  	#	}
    #
  	#}
  	if($node->getNodeName() eq 'cutvar_rules')
  	{
  		for my $node2(grep {$_->getNodeName() eq 'cutvar_rule' } @{$node->getChildNodes()})
  		{
  			$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - NodeName2:".$node2->getNodeName());
  			my $line = $node2->getAttribute('line');
  			my $regex = $node2->getAttribute('regex');
  			my $numberOfTokens = $node2->getAttribute('numberOfTokens'); 
  			$self->{cutvar_rules}->{$line} = $regex;
  			$self->{cutvar_rules}->{numberOfTokens}->{$line} = $numberOfTokens;
  		}
  	} 
  }
  
  return $self;
  
}

sub writeRowInOut_xls {
	my $self = shift;
	my $sheet = shift;
	my $out_xls = $self->{out_xls};
	if(!exists $out_xls->{$sheet})
	{
		$out_xls->{$sheet} = [[ @_ ]];
	} 
	else
	{
		push @{$out_xls ->{$sheet}}, [(@_)];
	}
	#printOnlyInDebug(__PACKAGE__.__LINE__." sub writeRowInOut_xls() - Dumper(out_xls):".Dumper($out_xls));
}



sub printOnlyInDebug{
    my $self = shift;
	$self->{'DEBUG'} and i::logit("DEBUG - ".join('::',@_));
}

sub Finalize {
    my $self = shift;
	printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() - ref(self)=".ref($self).join('::', caller()))  ;
	($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  line=".scalar(keys %{$self->{'lines'}}) );
	($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  finalLines=".scalar(keys %{$self->{'finalLines'}}) );

	#my $FH = Symbol::gensym();
	#my $ExcelFileName = $self->{jr}->getFileName('Excel'); $ExcelFileName =~ s/\.excel$/\.csv/i;
	#
	#
	#($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileName=".$ExcelFileName );
    #
    #open($FH, ">>$ExcelFileName") or die "ApplicationError: Unable to open file $ExcelFileName - $!";
    #
	##for my $nLine (sort {$a <=> $b} keys %{$self->{'lines'}} )
	#for my $nLine (1..scalar(keys %{$self->{'lines'}}))
	#{
	#	print $FH "".join(';',map {$self->{'lines'}->{$nLine}->{$_} } (sort {$a <=> $b} keys %{$self->{'lines'}->{$nLine}}))."\n";
	#	($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() - line[$nLine]=".
	#	join(';',map {$self->{'lines'}->{$nLine}->{$_} } (sort {$a <=> $b} keys %{$self->{'lines'}->{$nLine}}))
	#	#join(';',map {$self->{'lines'}->{$nLine}->{$_} =~ /^\s*(.*?)\s*$/; $1} (sort {$a <=> $b} keys %{$self->{'lines'}->{$nLine}}))
	#	);
	#}
	##for my $nFinalLine  (sort {$a <=> $b} keys %{$self->{'lines'}} )
	#for my $nFinalLine (1..scalar(keys %{$self->{'finalLines'}}))
	#{
	#	print $FH "".join(';',map {$self->{'finalLines'}->{$nFinalLine}->{$_} } (sort {$a <=> $b} keys %{$self->{'finalLines'}->{$nFinalLine}}))."\n";
	#	($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() - line[$nFinalLine]=".
	#	join(';',map {$self->{'finalLines'}->{$nFinalLine}->{$_} } (sort {$a <=> $b} keys %{$self->{'finalLines'}->{$nFinalLine}}))
	#	 );
	#}
	#
	##print $FH "".$self->{'totalLines'}->{'header'}."\n".$self->{'totalLines'}->{'value'} ."\n";
	#print $FH "".$self->{'totalLines'}->{'1'}->{'1'}."\n".$self->{'totalLines'}->{'2'}->{'1'} ."\n";
    #
	#close($FH)  or die "ApplicationError: Unable to close file $ExcelFileName - $! -$^E";	
	my $FH = self->{FILEHIN}; 
	if ($self->{'DEBUG'})
	{
		close($FH)  or die "ApplicationError: Unable to close file $ExcelFileName - $! -$^E";	
	}
	
	#if($FH)
	#{
	#	close($FH ) or die "ApplicationError: Unable to close file - $! -$^E";	
	#}
	
	my ($JobReportName, $JobReportId) = $self->{jr}->getValues(qw(JobReportName JobReportId));
	
	my $ExcelFileNameTrue = $self->{jr}->getFileName('Excel'); $ExcelFileNameTrue =~ s/\.excel$/\.xlsx/i;	
	printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileNameTrue=".$ExcelFileNameTrue );
	my $workbook = Excel::Writer::XLSX->new($ExcelFileNameTrue); 
	my $worksheet = $workbook->add_worksheet($JobReportName);	
	$worksheet->set_first_sheet();
	$worksheet->activate();
	
	
	my $row = 0;
	my $col = 0;
	my $initial_col = 0;
	my $initial_row = 0;
	my $max_col = 0;
	
	
	my $firstTable_col_key_value  = $self->{'exitdef'}->getAttribute('firstTable_col_key_value'); 
	if($firstTable_col_key_value)
	{
		my @cutvar_values = @{$self->{'cutvar_values'}};
		for my $c_k_eq_v( split(/,/, $firstTable_col_key_value))
		{
			my($c, $k, $v) = ( split(/=/, $c_k_eq_v));

			my $r = 0; $c--;
			$worksheet->write_string($r, $c, $k);
			$r++;
			if($v =~ /get_cutvarkey\((\d+)\)/i)
			{
				$v = $cutvar_values[$1-1];
			}
			elsif($v =~ /get_jr_field\((.+)\)/i)
			{
				$v = $self->{'jr'}->getValues($1);
			}
			$worksheet->write_string($r, $c, $v);
		}
		$initial_row = 2;
	}
	
	
	
	for my $typeOfLines (qw(lines finalLines totalLines))
	{
		$row = $initial_row;
		for my $nLine (1..scalar(keys %{$self->{$typeOfLines}}))
		{
			$col=$initial_col;
			for my $value (map {$self->{$typeOfLines}->{$nLine}->{$_} } (sort {$a <=> $b} keys %{$self->{$typeOfLines}->{$nLine}}))
			{
				if($value =~ /^[\d\.\,\-]+$/)
				{
					$value =~ s/\.//g;
					$value =~ s/,/\./g;
					$worksheet->write_number($row, $col, $value);
				}
				else
				{
					$worksheet->write_string($row, $col, $value);
				}
				$col++;
				$max_col = $col if ($col >$max_col);
			}
			$row++;
		}
		$initial_col=1+$max_col;
	}
	  
	$workbook->close(); 
	
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - finalPattern=".$self->{'finalPattern'});
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - mainPattern=".$self->{'mainPattern'});
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - totalValuePattern=".$self->{'exitdef'}->getAttribute ('totalValuePattern'));
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - pageCount=".$self->{'pageCount'}); 
}

sub Print {
    my $self = shift;
    my $page = shift;
    my $cutvar_not_yet_found = 1;
	$self->{pageCount}++;
	
	printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - ref(self)=".ref($self)." ref(page)=".ref($page).join('::', caller())) if ($self->{pageCount} == 1);
	my $FH = self->{FILEHIN};
	if($self->{'DEBUG'})
	{
		if(!$FH)
		{
			$FH = Symbol::gensym();
			my $ExcelFileName = $self->{jr}->getFileName('Excel'); $ExcelFileName =~ s/\.excel$/\.txt/i;
			open($FH, ">$ExcelFileName") or die "ApplicationError: Unable to open file $ExcelFileName - $! -$^E";		
			self->{FILEHIN} = $FH;
		} 
	}
	($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileName=".$ExcelFileName );

	my $mainHeader = $self->{'exitdef'}->getAttribute ('mainHeader');
	my $finalHeader = $self->{'exitdef'}->getAttribute ('finalHeader');
	my $totalValuePattern = $self->{'exitdef'}->getAttribute ('totalValuePattern');
	
	
	
	
	my $mainKeysNotEmpty = $self->{'exitdef'}->getAttribute ('mainKeysNotEmpty');
	my $finalKeysNotEmpty = $self->{'exitdef'}->getAttribute ('finalKeysNotEmpty');


	my @ascii_lines;
	($self->{pageCount} == 1) and (!scalar(keys %{$self->{'mainHeader'}})) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - ref(mainHeader)1=".ref($self->{'mainHeader'}));

	my $mainHeader_line = 0;
	my $finalHeader_line = 0;
	my $totalValuePattern_line = 0;
	my $line_count = 0;
	for my $line(@{$page->lineList()}) {
		my $lines = $line->AsciiValueList();
		for my $ascii_line (map {substr($_,1)} @$lines)
		{
			push @ascii_lines, $ascii_line;
			$self->{'DEBUG'} and print $FH "$ascii_line\n";#TESTSAN
			$line_count++;
			
		    if(exists $self->{'cutvar_rules'}->{$line_count})
		    {
		    	my $cutvar_regex = $self->{'cutvar_rules'}->{$line_count};
		    	my @tokens = map { $_ =~ /^\s*(.+?)\s*$/; $1} ($ascii_line =~ qr/$cutvar_regex/i);
		    	if(scalar(@tokens) )
		    	{
		    		if($cutvar_not_yet_found)
		    		{
		    			$cutvar_not_yet_found = 0;
		    			$self->{'cutvar_values'} = [];
		    			$self->{'cutvar'} = $tokens[0];
		    			$cutvar = $self->{'cutvar'};
		    			$self->{'cutvars'}->{$cutvar}++;
		    		}
		    		for my $j(0..$#tokens)
		    		{
		    			push @{$self->{'cutvar_values'}}, $tokens[$j];
		    		}
		    	}
		    }
			
			
			
			
			
			#my @mainHeader_tokens = ($ascii_line  =~ qr{$mainHeader}) if(!$mainHeader_line );
			#my @finalHeader_tokens = ($ascii_line  =~ qr{$finalHeader}) if(!$finalHeader_line );
			#my @totalValuePattern_tokens = ($ascii_line  =~ qr{$totalValuePattern}) if(!$totalValuePattern_line );
			my @mainHeader_tokens = ($ascii_line  =~ qr/$mainHeader/i) if(!$mainHeader_line );
			my @finalHeader_tokens = ($ascii_line  =~ qr/$finalHeader/i) if(!$finalHeader_line );
			my @totalValuePattern_tokens = ($ascii_line  =~ qr/$totalValuePattern/i) if(!$totalValuePattern_line );
			
			#if((!$mainHeader_line ) and ($ascii_line  =~ qr{$mainHeader}))
			if((!$mainHeader_line ) and (scalar(@mainHeader_tokens)))
			{
				$mainHeader_line = $line_count;
				if(!scalar(keys %{$self->{'mainHeader'}}))#check if the header is already found
				{
					printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - mainHeader found in line $line_count:".length($ascii_line)."\n$ascii_line");
					for my $i(1..$#mainHeader_tokens+1)
					{
						my $val = $mainHeader_tokens[$i-1];
						printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - mainHeader_token=$val");
						$self->{'mainHeader'}->{$i}= $1 if ($val =~ /^\s*(.*?)\s*$/ );
						$self->{'mainPattern'}.="(.{".length($val)."})";
					} 
				}
			}
			#elsif((!$finalHeader_line ) and ($ascii_line  =~ qr{$finalHeader}))
			if((!$finalHeader_line ) and (scalar(@finalHeader_tokens)))
			{
				$finalHeader_line = $line_count;
				if(!scalar(keys %{$self->{'finalHeader'}}))#check if the header is already found
				{
					printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - finalHeader found in line $line_count:".length($ascii_line)."\n$ascii_line");
					#my @tokens = ($ascii_line  =~ qr{$finalHeader});
					for my $i(1..$#finalHeader_tokens+1)
					{ 
						my $val = $finalHeader_tokens[$i-1];
						printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - finalHeader_token=$val");
						$self->{'finalHeader'}->{$i}= $1 if ($val =~ /^\s*(.*?)\s*$/ );
						$self->{'finalPattern'}.="(.{".length($val)."})";
					}
				}
			}
			if((!$totalValuePattern_line ) and (scalar(@totalValuePattern_tokens)))
			{ 
				$totalValuePattern_line = $line_count; 
				#$self->{'totalLines'}->{'header'} =  $1 if ($totalValuePattern_tokens[0] =~ /^\s*(.*?)\s*$/ );#header of TOTAL
				#$self->{'totalLines'}->{'value'} = $1 if ($totalValuePattern_tokens[1] =~ /^\s*(.*?)\s*$/ ); #value of TOTAL
				$self->{'totalLines'}->{'1'}->{'1'} =  $1 if ($totalValuePattern_tokens[0] =~ /^\s*(.*?)\s*$/ );#header of TOTAL
				$self->{'totalLines'}->{'2'}->{'1'} = $1 if ($totalValuePattern_tokens[1] =~ /^\s*(.*?)\s*$/ ); #value of TOTAL
				
				#$self->writeRowInOut_xls('totalLines',$self->{'totalLines'}->{'header'});
				#$self->writeRowInOut_xls('totalLines',$self->{'totalLines'}->{'value'});
			}
		}
	}

	if($mainHeader_line) #if mainHeader is found
	{
		#($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - main header found>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " );
		my $nLine = scalar( keys %{$self->{'lines'}}) ; #number of lines already read
		if(!$nLine)
		{
			#write header
			for my $key(sort {$a <=> $b} keys %{$self->{'mainHeader'}} )
			{
				$self->{'lines'}->{'1'}->{$key} = $self->{'mainHeader'}->{$key};
			}
			#$self->writeRowInOut_xls('lines',map{$self->{'mainHeader'}->{$_}} (sort {$a <=> $b} keys %{$self->{'mainHeader'}} ));
			$nLine++;
		}

		#overwrite mainPattern from configuration
		$self->{'mainPattern'} = $1 if( $self->{'exitdef'}->getAttribute('mainPattern') =~ /(.+)/);

		#for each line of the page
		for my $i($mainHeader_line..$#ascii_lines)
		{
			last if(($finalHeader_line) and ($i >= $finalHeader_line));
			my @tokens = map { $_ =~ /^\s*(.*?)\s*$/; $1} ($ascii_lines[$i] =~ qr{$self->{'mainPattern'}});
			#if($ascii_lines[$i] =~ qr{$self->{'mainPattern'}})
			if((scalar(@tokens)) and ( length($ascii_lines[$i]) > 0 ))
			{
				$self->{'lines'}->{++$nLine} = {}; 
				for my $j(1..$#tokens+1)
				{
					#($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - tokens[".($j-1)."]=".$tokens[$j-1]);
					$self->{'lines'}->{$nLine}->{$j} = $tokens[$j-1];
					#copy value from previous line if empty
					if((grep {(($_) and ($_ eq $j))} split(/,/, $mainKeysNotEmpty)) and ($self->{'lines'}->{$nLine}->{$j} =~ /^\s*$/))
					{
						$self->{'lines'}->{$nLine}->{$j} = $self->{'lines'}->{$nLine-1}->{$j};
					}
				}
				#$self->writeRowInOut_xls('lines',  map {$self->{'lines'}->{$nLine}->{$_}} (1..$#tokens+1));
			}
			else
			{
				($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - not match $i=\n".$ascii_lines[$i]) if( length($ascii_lines[$i]) > 0 );
			}
		}

	}
	if($finalHeader_line) #if finalHeader is found
	{
		#write header
		my $nFinalLine = scalar( keys %{$self->{'finalLines'}}) ;
		if(!$nFinalLine)
		{
			for my $key(sort {$a <=> $b} keys %{$self->{'finalHeader'}} )
			{
				$self->{'finalLines'}->{'1'}->{$key} = $self->{'finalHeader'}->{$key};
			}
			#$self->writeRowInOut_xls('finalLines',map{$self->{'finalHeader'}->{$_}} (sort {$a <=> $b} keys %{$self->{'finalHeader'}} ));
			$nFinalLine++;
		}

		#overwrite finalPattern from configuration
		$self->{'finalPattern'} = $1 if( $self->{'exitdef'}->getAttribute('finalPattern') =~ /(.+)/) ;


		for my $i($finalHeader_line..$#ascii_lines)
		{
			my @tokens = map { $_ =~ /^\s*(.*?)\s*$/; $1} ($ascii_lines[$i] =~ qr{$self->{'finalPattern'}});
			if((scalar(@tokens)) and ( length($ascii_lines[$i]) > 0 ))
			{
				$self->{'finalLines'}->{++$nFinalLine} = {}; 
				for my $j(1..$#tokens+1)
				{
					$self->{'finalLines'}->{$nFinalLine}->{$j} = $tokens[$j-1];
					#copy value from previous line if empty
					if((grep {(($_) and ($_ eq $j))} split(/,/, $finalKeysNotEmpty)) and ($self->{'finalLines'}->{$nFinalLine}->{$j} =~ /^\s*$/))
					{
						$self->{'finalLines'}->{$nFinalLine}->{$j} = $self->{'finalLines'}->{$nFinalLine-1}->{$j};
					}		
					#($j == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - tokens[".($j-1)."]=".$self->{'finalLines'}->{$nFinalLine}->{$j});
				}
				#$self->writeRowInOut_xls('finalLines',  map {$self->{'finalLines'}->{$nFinalLine}->{$_}} (1..$#tokens+1));
			}
			else
			{
				($j == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - finalHeader_line not match $i=\n".$ascii_lines[$i]) if( length($ascii_lines[$i]) > 0 );
			}
		}
	}
	if((!$mainHeader_line)  and (!$finalHeader_line))
	{
		($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - main header NOT found>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " );
		for my $i(0..$#ascii_lines)
		{
			printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() -line=".$ascii_lines[$i]);
		}
	}
	
    return 1;
}

__PACKAGE__;


