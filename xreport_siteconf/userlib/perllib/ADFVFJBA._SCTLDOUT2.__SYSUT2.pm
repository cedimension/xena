package XReport::OUTPUT::PageMKExcel;
use constant DEBUG => 0;

=example of parsing file
<jobreport name="$JobReportName" linePos="raw"   >
	<exit 
	    type="PageMKExcel"  
	    ref="ADFVFJBA._SCTLDOUT2.__SYSUT2.pm"  
		_outputHeader="Betreuercode,Organisationseinheit,HINW,Kundennummer,Kundenname,Kontonummer,PROD.,WHG,ABLAUFDATUM,ABLEHNUNGSGRUND,RATING,BERECHNUNGSDATUM,AKT. RAHMEN,SALDO"
		outputHeader="Betreuercode,Abteilung,Abt.Bezeichnung,HINW,Kundennummer,Kundenname,Kontonummer,PROD.,WHG,ABLAUFDATUM,ABLEHNUNGSGRUND,RATING,BERECHNUNGSDATUM,AKT. RAHMEN,SALDO,HINWEIS,ERRECHNETER BETR.,TYPE"
		firstLineWithData="8"
		outputNumericKeys="12,14,15,17" 
		mainKeysNotEmpty=""
		type_of_page_pattern="^.{28}VORHANDENEN\sAUTOMATISCHE\sUEBERSCHREITUNGEN\s\-\s(FIX|DYNAMISCH).*$"
		output_cutvars_mapping="2=1,3=2,18=4" 
		firstTable_col_key_value="1=Jobname=ADFVFJBA,2=Reportname=KONTROLLE ÜBERSCHREITUNGEN - FIX und DYN,3=Anforderungsdatum=get_cutvarkey(3)"
		DEBUG="0"
		typeOfPage="get_cutvarkey(4)"  >
		<cutvar_rules>
			<cutvar_rule line="2" regex="^.{5}\s{2}(\d{5})\s{3}(.{72})\sDATUM\s(\d+\/\d+\/\d+)\s+SEITE\s+\d+\s*$" numberOfTokens="3" /> 
			<cutvar_rule line="3" regex="^.{73}(DYNAMISCH|FIX).*$" numberOfTokens="1" /> 
		</cutvar_rules>
		<patterns typeOfPage="DYNAMISCH" output_order_keys_mapping="1=1,2=20,3=20,4=2,5=3,6=4,7=5,8=6,9=7,10=8,11=10,12=11,13=12,14=13,15=14,16=20,17=20,18=20" >
			<pattern order="1" regex="^([^\s]{4}.)\s(.{6})\s(\d{16})\s(.{65})\s(\d{14})\s(.{2})\s{4}(.{3})\s(.*)$" numberOfTokens="8" />
			<pattern order="2" regex="^(.{5})(.{60})\s([\s\+\-\d]{6})\s(\d+\/\d+\/\d+)\s([\s\+\-\d\.\,]{22}\d[\-\s])([\s\+\-\d\.\,]{20}\d[\-\s])\s*$" numberOfTokens="6" /> 
		</patterns>
		<patterns typeOfPage="FIX"       output_order_keys_mapping="1=1,2=20,3=20,4=2,5=3,6=4,7=5,8=6,9=7,10=8,11=20,12=11,13=20,14=12,15=14,16=10,17=13,18=20" >
			<pattern order="1" regex="^([^\s]{4}.)\s(.{6})\s(\d{16})\s(.{65})\s(\d{14})\s(.{2})\s{4}(.{3})\s(.*)$" numberOfTokens="8" />
			<_pattern order="1" regex="^([^\s]{4}.)(\s{7})\s(\d{16})\s(.{65})\s(\d{14})\s(.{2})\s{4}(.{3})\s*$" numberOfTokens="7" />
			<pattern order="2" regex="^([^\s]{4}.)(.{50})\s([\s\+\-\d]{6})([\s\+\-\d\.\,]{20}\d[\s\-])([\s\+\-\d\.\,]{25}\d[\s\-])([\s\+\-\d\.\,]{19}\d[\s\-])\s*$" numberOfTokens="6" /> 
		</patterns>
	</exit>
  <page name="PAGE001" test="1" />
</jobreport>
=cut

use Data::Dumper;
use Excel::Writer::XLSX;


sub new {
	my ($className, $parser, $workdir, $exitdom) = @_;

	my $DEBUG = $exitdom->getAttribute('DEBUG'); 
	my $ExistTypeMap = $exitdom->getAttribute('ExistTypeMap');
	die "ExistTypeMap parameter is missing in parsingfile!" if(!$ExistTypeMap);
	$DEBUG  =  $DEBUG or DEBUG or $main::veryverbose;

	#printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - className=$className - workdir=$workdir - parser=".ref($parser)." - exitdom=".ref($exitdom) );
	my $self = {
		   'jr' => $parser->{jr}
		  ,'ExistTypeMap' => $ExistTypeMap
		  ,'parser' => $parser
          ,'workdir' => $workdir
		  ,'exitdef' => $exitdom
		  ,'lines' => {}
		  ,'cutvar' => '$$$$'
		  ,'pageCount' => 0
		  ,'FILEHIN' => ''
		  ,'DEBUG' => $DEBUG
		  };


	bless $self, $className;


	for my $node(@{$self->{'exitdef'}->getChildNodes()})
	{
		$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - NodeName:".$node->getNodeName());
		if($node->getNodeName() eq 'patterns')
		{
			my $typeOfPage = $node->getAttribute('typeOfPage'); 
			$self->{patterns}->{$typeOfPage}->{output_order_keys_mapping} = $node->getAttribute('output_order_keys_mapping');
			for my $node2(grep {$_->getNodeName() eq 'pattern' } @{$node->getChildNodes()})
			{
				$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - NodeName2:".$node2->getNodeName());
				my $order = $node2->getAttribute('order');
				my $regex = $node2->getAttribute('regex');
				my $numberOfTokens = $node2->getAttribute('numberOfTokens');
				$self->{patterns}->{$typeOfPage}->{regex}->{$order} = $regex;
				$self->{patterns}->{$typeOfPage}->{numberOfTokens}->{$order} = $numberOfTokens;
			}

		}
		elsif($node->getNodeName() eq 'cutvar_rules')
		{
			for my $node2(grep {$_->getNodeName() eq 'cutvar_rule' } @{$node->getChildNodes()})
			{
				$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - NodeName2:".$node2->getNodeName());
				my $line = $node2->getAttribute('line');
				my $regex = $node2->getAttribute('regex');
				$self->{cutvar_rules}->{$line} = $regex;
			}
		}
	}
	$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - patterns:".Dumper(%{$self->{patterns}}));
	return $self;
}




sub printOnlyInDebug{
	my $self = shift;
	$self->{'DEBUG'} and i::logit("DEBUG - ".join('::',@_));
}

sub Finalize {
    my $self = shift;
	($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() - ref(self)=".ref($self).join('::', caller()))  ;
	($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  line=".scalar(keys %{$self->{$cutvar}->{'lines'}}) );
	($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  finalLines=".scalar(keys %{$self->{'finalLines'}}) );

	my ($JobReportName, $JobReportDescr) = $self->{jr}->getValues(qw(JobReportName JobReportDescr));
	$self->{lines}->{output_cutvars_mapping} = $self->{'exitdef'}->getAttribute('output_cutvars_mapping');
	my $reportDescriptionInCfg  = $self->{'exitdef'}->getAttribute('reportDescriptionInCfg');
	$reportDescriptionInCfg  = $JobReportDescr if(!$reportDescriptionInCfg);
	my $firstTable_col_key_value  = $self->{'exitdef'}->getAttribute('firstTable_col_key_value');


	$self->{lines}->{outputNumericKeys} = $self->{'exitdef'}->getAttribute('outputNumericKeys');
	$self->{lines}->{outputAmountKeys} = $self->{'exitdef'}->getAttribute('outputAmountKeys');


	#if($printSplittedReports)
	if(0)
	{
		for my $cutvar (sort keys %{$self->{'cutvars'}})
		{
			my $ExcelFileNameTrue = $self->{jr}->getFileName('Excel'); $ExcelFileNameTrue =~ s/\.excel$/\.$cutvar\.xlsx/i;
			($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileNameTrue=".$ExcelFileNameTrue );
			my $workbook = Excel::Writer::XLSX->new($ExcelFileNameTrue);
			my $worksheet = $workbook->add_worksheet($JobReportName);
			$worksheet->set_first_sheet();
			$worksheet->activate();
			my $row ;
			my $col ;
			my $initial_row = 0;
			my $initial_col = 0;
			my $max_col = 0;
			if($reportDescriptionInCfg)
			{
				$worksheet->write_string(0, 0, $reportDescriptionInCfg) if (defined $reportDescriptionInCfg);
				$initial_row = 1;
			}
			for my $typeOfLines (qw(lines ))
			{
				#my @output_cutvars_mapping = split(/,/, $self->{$cutvar}->{$typeOfLines}->{output_cutvars_mapping});
				my @outputNumericKeys = split(/,/, $self->{$typeOfLines}->{outputNumericKeys});
				$row = $initial_row;
				for my $nLine (1..scalar(keys %{$self->{$cutvar}->{$typeOfLines}}))
				{
					$col=$initial_col;
					#for my $value (map {$self->{$cutvar}->{$typeOfLines}->{$nLine}->{$_} } (sort {$a <=> $b} keys %{$self->{$cutvar}->{$typeOfLines}->{$nLine}}))
					for my $i (sort {$a <=> $b} keys %{$self->{$cutvar}->{$typeOfLines}->{$nLine}})
					{
						my $value = $self->{$cutvar}->{$typeOfLines}->{$nLine}->{$i};
						#if($value =~ /^[\d\.\,\-]+$/)
						if((grep {$_ eq $i} @outputNumericKeys ) and ($value =~ /^[\d\.\,\-\+]+$/))
						{
							$value =~ s/\.//g;
							$value =~ s/,/\./g;
							$value = $2.$1 if($value =~/^(.+)([\-\+])\s*$/); 
							$value =~ s/^\++//g;
							$worksheet->write_number($row, $col, $value);
						}
						else
						{
							$worksheet->write_string($row, $col, $value)  if (defined $value);
						}
						$col++;
						$max_col = $col if ($col >$max_col);
					}
					$row++;
				}
				$initial_col=1+$max_col;
			}

			$workbook->close();
			die "Error in creation of Excel file [$ExcelFileNameTrue]!" if((!-f $ExcelFileNameTrue) or (!-s $ExcelFileNameTrue));
		}
	}
	#if($printEntireReport)
	if(1)
	{
		my $ExcelFileNameTrue = $self->{jr}->getFileName('Excel'); $ExcelFileNameTrue =~ s/\.excel$/\.xlsx/i;
		($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileNameTrue=".$ExcelFileNameTrue );
		my $workbook = Excel::Writer::XLSX->new($ExcelFileNameTrue);
		my $worksheet = $workbook->add_worksheet($JobReportName);
		$worksheet->set_first_sheet();
		$worksheet->activate();
		my $row ;
		my $col ;
		my $initial_row = 0;
		my $initial_col = 0;
		my $max_col = 0;
		#if($reportDescriptionInCfg)
		#{
		#	$worksheet->write_string(0, 0, $reportDescriptionInCfg);
		#	$initial_row = 1;
		#}
		if($firstTable_col_key_value)
		{
			my @cutvar_values = @{$self->{'cutvar_values'}};
			for my $c_k_eq_v( split(/,/, $firstTable_col_key_value))
			{
				my($c, $k, $v) = ( split(/=/, $c_k_eq_v));

				my $r = 0; $c--;
				$worksheet->write_string($r, $c, $k);
				$r++;
				if($v =~ /get_cutvarkey\((\d+)\)/i)
				{
					$v = $cutvar_values[$1-1];
				}
				elsif($v =~ /get_jr_field\((.+)\)/i)
				{
					$v = $self->{'jr'}->getValues($1);
				}
				$worksheet->write_string($r, $c, $v);
			}
			$initial_row = 2;
		}



		for my $typeOfLines (qw(lines ))
		{
			my @outputNumericKeys = split(/,/, $self->{$typeOfLines}->{outputNumericKeys});
			my @outputAmountKeys = split(/,/, $self->{$typeOfLines}->{outputAmountKeys});
			$row = $initial_row;
			for my $nLine (1..scalar(keys %{$self->{$typeOfLines}}))
			{
				$col=$initial_col;
				#for my $value (map {$self->{$typeOfLines}->{$nLine}->{$_} } (sort {$a <=> $b} keys %{$self->{$typeOfLines}->{$nLine}}))
				for my $i (sort {$a <=> $b} keys %{$self->{$typeOfLines}->{$nLine}})
				{
					my $value = $self->{$typeOfLines}->{$nLine}->{$i};
					#if($value =~ /^[\d\.\,\-]+$/)
					if((grep {$_ eq $i} @outputNumericKeys ) and ($value =~ /^[\d\.\,\-\+]+$/))
					{
						$value =~ s/\.//g;
						$value =~ s/,/\./g;
						$value = $2.$1 if($value =~/^(.+)([\-\+])\s*$/);
						$worksheet->write_number($row, $col, $value);
					}
					elsif((grep {$_ eq $i} @outputAmountKeys ) and ($value =~ /^[\d\.\,\-\+]+$/))
					{
						$value =~ s/\.//g;
						$value =~ s/,/\./g;
						$value = $2.$1 if($value =~/^(.+)([\-\+])\s*$/);
						my $format03 = $workbook->add_format();
						$format03->set_num_format( '#,##0.00' );
						#$worksheet->write( 2, 0, 1234.56, $format03 );      # 1,234.56
						$worksheet->write($row, $col, $value,$format03);
					}
					else
					{
						$worksheet->write_string($row, $col, $value)   if (defined $value);
					}
					$col++;
					$max_col = $col if ($col >$max_col);
				}
				$row++;
			}
			$initial_col=1+$max_col;
		}

		$workbook->close();
		i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - File Excel[$ExcelFileNameTrue] created with success!") if(-f $ExcelFileNameTrue);
		die "Error in creation of Excel file [$ExcelFileNameTrue]!" if((!-f $ExcelFileNameTrue) or (!-s $ExcelFileNameTrue));

	}

	#i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - mainPattern=".$self->{'exitdef'}->getAttribute('mainPattern'));
	#i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - finalPattern=".$self->{'exitdef'}->getAttribute('finalPattern'));
	#i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - totalValuePattern=".$self->{'exitdef'}->getAttribute('totalValuePattern'));
	#i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - pageCount=".$self->{'pageCount'});
	#i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - DEBUG=".$self->{'DEBUG'});
}

sub Print {
    my $self = shift;
    my $page = shift;
	$self->{pageCount}++;
	#i::logit(__PACKAGE__.__LINE__." sub Print() - Print - DEBUG=".$self->{'DEBUG'});
	($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - ref(self)=".ref($self)." ref(page)=".ref($page).join('::', caller())) if ($self->{pageCount} == 1);
	my $FH;
	if($self->{'DEBUG'})
	{
		$FH = Symbol::gensym();
		my $ExcelFileName = $self->{jr}->getFileName('Excel'); $ExcelFileName =~ s/\.excel$/\.txt/i;
		my $w_or_a = ">>";
		$w_or_a = ">" if ($self->{pageCount} == 1);
		open($FH, $w_or_a.$ExcelFileName) or die "ApplicationError: Unable to open file $ExcelFileName - $! -$^E";
	}
	($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() -  ExcelFileName=".$ExcelFileName );

	#my $mainHeader = $self->{'exitdef'}->getAttribute('mainHeader');
	my $firstLineWithData_  = ($self->{'exitdef'}->getAttribute('firstLineWithData') =~ /^(\d+)$/ ? $1 : 1); 
	my $outputHeader_ = $self->{'exitdef'}->getAttribute('outputHeader');
	my @outputHeader = split(/,/, $outputHeader_);
	my $cutvar = $self->{'cutvar'}; 
	my $mainKeysNotEmpty = $self->{'exitdef'}->getAttribute('mainKeysNotEmpty'); 
	#my $outputOrderKeys_ = $self->{'exitdef'}->getAttribute('outputOrderKeys');
	#my $output_order_keys_mapping = $self->{'exitdef'}->getAttribute('output_order_keys_mapping');
	my $output_order_keys_mapping  ;
	my $output_cutvars_mapping = $self->{'exitdef'}->getAttribute('output_cutvars_mapping'); 
	my $type_of_page_pattern = $self->{'exitdef'}->getAttribute('type_of_page_pattern'); 
	
	
	if(!exists $self->{'cutvar_rules'})
	{
		die "Error in cutvar_rules not defined in cgf.";
	}

	#($self->{pageCount} == 1) and (!scalar(keys %{$self->{'mainHeader'}})) and ($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - ref(mainHeader)1=".ref($self->{'mainHeader'}));

	my $mainHeader_line = 0;
	my $typeOfPage;

	my $cutvar_not_yet_found = 1;
	my @ascii_lines;
	for my $line(@{$page->lineList()}) {
		my $lines = $line->AsciiValueList();
		for my $ascii_line (map {substr($_,1)} @$lines)
		{
			push @ascii_lines, $ascii_line;
			$self->{'DEBUG'} and print $FH "$ascii_line\n";#TESTSAN
		}
	}
	my $line_count = 0;
	NEXTLINE:
	for my $ascii_line_idx (0..$#ascii_lines)
	{
		my $ascii_line = $ascii_lines[$ascii_line_idx];
		next if ($line_count > $ascii_line_idx);
		$line_count++;
		($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - Page{".$self->{pageCount}."} Line{$line_count} [$ascii_line]");

		if(exists $self->{'cutvar_rules'}->{$line_count})
		{
			my $cutvar_regex = $self->{'cutvar_rules'}->{$line_count};
			my @tokens = map { $_ =~ /^\s*(.+?)\s*$/; $1} ($ascii_line =~ qr/$cutvar_regex/i);
			if(scalar(@tokens) )
			{
				if($cutvar_not_yet_found)
				{
					$cutvar_not_yet_found = 0;
					$self->{'cutvar_values'} = [];
					$self->{'cutvar'} = $tokens[0];
					$cutvar = $self->{'cutvar'};
					$self->{'cutvars'}->{$cutvar}++;
				}
				for my $j(0..$#tokens)
				{
					push @{$self->{'cutvar_values'}}, $tokens[$j];
				}
			}
		}

		
		if($ascii_line =~ qr{$type_of_page_pattern})
		{
			$typeOfPage = $1;
			$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - typeOfPage=$typeOfPage");
			die "Error - typeOfPage value [$typeOfPage] does not exist in configuration file." if(!exists $self->{patterns}->{$typeOfPage});
			
			my $oldtypeOfPage = pop @{$self->{'cutvar_values'}};
			push @{$self->{'cutvar_values'}}, $typeOfPage; 
			
			$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - typeOfPage changed from [$oldtypeOfPage] to [$typeOfPage]") if($oldtypeOfPage ne $typeOfPage);
			
			
			#$self->{previous_tokens} = ();
			#$line_count+=4;
			next NEXTLINE;
		}
		next NEXTLINE if(!$typeOfPage);
		next NEXTLINE if ($line_count < $firstLineWithData_);
		next NEXTLINE if ( length($ascii_line) == 0 );
		
		my $nLine = scalar( keys %{$self->{$cutvar}->{'lines'}}) ; #number of lines already read
		my $nLine_entire = scalar( keys %{$self->{'lines'}}) ; #number of lines already read

		if(!$nLine)
		{
			#write header
			$nLine = 1;
			for my $k(0..$#outputHeader)
			{
				$self->{$cutvar}->{'lines'}->{$nLine}->{$k+1} = $outputHeader[$k];
				($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - header ".($k+1)."=".$outputHeader[$k]);
			}
		}
		if(!$nLine_entire)
		{
			#write header
			$nLine_entire = 1;
			for my $k(0..$#outputHeader)
			{
				$self->{'lines'}->{$nLine_entire}->{$k+1} = $outputHeader[$k];
			}
		}
 
		 
		
		my $mainPattern_  = $self->{patterns}->{$typeOfPage}->{regex}->{'1'};
		my $secondLinePattern_ = $self->{patterns}->{$typeOfPage}->{regex}->{'2'}; 
		($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - mainPattern_:$mainPattern_");
		($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - secondLinePattern_:$secondLinePattern_");
		if(exists $self->{patterns}->{$typeOfPage}->{output_order_keys_mapping})
		{
			$output_order_keys_mapping = $self->{patterns}->{$typeOfPage}->{output_order_keys_mapping};
		}
		
		
		my $matching_rule = 0;
		for my $order( sort {$a <=> $b} keys %{$self->{patterns}->{$typeOfPage}->{regex}} )
		{
			my $otherLinePattern = $self->{patterns}->{$typeOfPage}->{regex}->{$order};
			next if(!$otherLinePattern);
			if(scalar($ascii_line =~ qr{$otherLinePattern}))
			{
				$matching_rule = $order;
				last;
			}
		}
		if(!$matching_rule)
		{
			($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - not match $i=\n".$ascii_line);
			next NEXTLINE;
		}

		my @tokens;
		if($matching_rule eq '1')
		{
			@tokens = map { $_ = $1 if ($_ =~ /^\s*(.+?)\s*$/); $_} ($ascii_line =~ qr{$mainPattern_});
			if((exists $self->{previous_tokens}) and (scalar(@{$self->{previous_tokens}})))
			{
				printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - previous_tokens overwritten by current line because it matches with mainpattern - Page{".$self->{pageCount}."} - Line{$line_count}: [$ascii_line]");
			}
			$self->{previous_tokens} = ();
		}
		elsif((exists $self->{previous_tokens}) and (scalar @{$self->{previous_tokens}}))
		{
			@tokens = @{$self->{previous_tokens}};
			$self->{previous_tokens} = ();
			$line_count--;
			$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - Page{".$self->{pageCount}."} - \@tokens set with previous_tokens - tokens:".join('::',@tokens));
		}
		else
		{
			die "Error - sub Print() - Page{".$self->{pageCount}."} Line{$line_count} - unuspected line:".$ascii_line;
		}

		if(!scalar(@tokens))
		{
			die "Error - sub Print() - Page{".$self->{pageCount}."} Line{$line_count}: [$ascii_line] - list of tokens empty!";
			#die "scalar(\@tokens)=[".scalar(@tokens)."][".join('::',@tokens)."] length(\$ascii_line)=[".length($ascii_line)."]";
		}
		#if((scalar(@tokens)) and ( length($ascii_line) > 0 ))
		if(scalar(@tokens))
		{
			my $number_of_main_pattern_tokens = $self->{patterns}->{$typeOfPage}->{numberOfTokens}->{'1'}-1;
			my @main_pattern_tokens = @tokens[0..$number_of_main_pattern_tokens];
			
			#my @main_pattern_tokens = ();
			#push @main_pattern_tokens, @tokens;

			my $expected_number_of_tokens = $self->{patterns}->{$typeOfPage}->{numberOfTokens}->{'1'};
			for my $order( sort {$a <=> $b} grep { $_ ne '1' } keys %{$self->{patterns}->{$typeOfPage}->{regex}} )
			{
				my $otherLinePattern = $self->{patterns}->{$typeOfPage}->{regex}->{$order};
				next if(!$otherLinePattern);
				$expected_number_of_tokens+=$self->{patterns}->{$typeOfPage}->{numberOfTokens}->{$order};
				my $number_of_tokens_already_read = scalar(@tokens);
				next if($number_of_tokens_already_read >= $expected_number_of_tokens);
				($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - otherLinePattern=$otherLinePattern");
				#die "Error - No more lines to read - nlines:[$#ascii_lines]!!!!!" if( $line_count >= $#ascii_lines);
				if( $line_count > $#ascii_lines)
				{
					$self->{previous_tokens} =  [@tokens];
					$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - previous_tokens set to tokens:".join('::',@{$self->{previous_tokens}}));
					#$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - previous_tokens set to tokens:".join('::',@tokens));
					$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - \$self->{previous_tokens}:".ref($self->{previous_tokens}));
					($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - Page{".$self->{pageCount}."} Line{$line_count} previous_tokens=".join('::',@tokens));
					last NEXTLINE;
				}
				my $next_ascii_line = $ascii_lines[$line_count];
				($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - Page{".$self->{pageCount}."} Line{$line_count} ascii_line:[$ascii_line] next_ascii_line:[$next_ascii_line]");

				#check if the new line matches with the main pattern
				last if($next_ascii_line =~ qr{$mainPattern_});
				my @tokens_next_line = map { $_ =~ /^\s*(.*?)\s*$/; $1} ($next_ascii_line =~ qr{$otherLinePattern});
				die "Page{".$self->{pageCount}."} Line($line_count) ascii_line:[$ascii_line] next_ascii_line:[$next_ascii_line] nlines:[$#ascii_lines] does not match with otherLinePattern[$otherLinePattern]" if(!scalar(@tokens_next_line));
				($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - Page{".$self->{pageCount}."} Line{$line_count} tokens_next_line=".join('::',@tokens_next_line));
				$line_count++;
				push @tokens, @tokens_next_line;
			}
			for(1..100)
			{
				push @tokens, '';
			}

			$self->{$cutvar}->{'lines'}->{++$nLine} = {};

			#reorder keys
			if( $output_order_keys_mapping )
			{
				for my $k_eq_v( split(/,/, $output_order_keys_mapping))
				{
					#1=2,2=3,4=4,5=5,6=9,7=1,8=9,9=6,10=7,11=8
					#my($k, $v) = ( split(/=/, $k_eq_v));
					my($k, $operator, $v) = ( $k_eq_v =~ /^(\d+)(=|\.=)(\d+)$/);
					die "Error in $output_order_keys_mapping parameter [".$self->{$output_order_keys_mapping}."] [$k=$v] [line=$line_count]" if((!defined $v) or (!defined $k) ) ;
					if (scalar(@tokens) < ($v-1))
					{
						die "Error in applying of rule [$k_eq_v]- the token ".($v)." does not exist. check output_order_keys_mapping parameter!";
					}
					if($operator eq '=')
					{
						$self->{$cutvar}->{'lines'}->{$nLine}->{$k} = $tokens[$v-1] if (scalar(@tokens) >= ($v-1));
					}
					elsif($operator eq '.=')
					{
						$self->{$cutvar}->{'lines'}->{$nLine}->{$k}.= ' '.$tokens[$v-1] if (scalar(@tokens) >= ($v-1));
					}
					else
					{
						die("Error!!!!!!!!!!!!!!!! bad operator:$operator");
					}

					($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - $k=".$tokens[$v-1]);
				}
			}
			#
			#if( exists $self->{outputOrderKeys} )
			#{
			#	my $newi = $self->{outputOrderKeys}->{$j};
			#	#$self->{$cutvar}->{'lines'}->{$nLine}->{$newi} = $tokens[$j-1] if $newi;
			#	$self->{$cutvar}->{'lines'}->{$nLine}->{$newi} = $tokens[$j-1];
			#	($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - $newi=".$tokens[$j-1]);
			#}


				
			for my $j(1..$#tokens+1)
			{
				($self->{pageCount} == 1) and ($tokens[$j-1]) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - tokens[".($j)."]=".$tokens[$j-1]);
				if( !$output_order_keys_mapping )
				{
					$self->{$cutvar}->{'lines'}->{$nLine}->{$j} = $tokens[$j-1];
				}

			}
			if( $output_cutvars_mapping )
			{
				my @cutvar_values = @{$self->{'cutvar_values'}}; 
				#($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - cutvar_values=".join('::',@cutvar_values ));
				#die "forced ERROR";
				for my $k_eq_v( split(/,/, $output_cutvars_mapping))
				{
					my($k, $v) = ( split(/=/, $k_eq_v));
					die "Error in output_cutvars_mapping parameter. Check regex. [$k=$v] [line=$line_count][LEN=".scalar(@cutvar_values)."]" if((!defined $v) or (!defined $k) or (!scalar(@cutvar_values))) ;

					if (scalar(@cutvar_values) < ($v-1))
					{
						die "Error in applying of rule [$k_eq_v]- the token ".($v)." does not exist. output_cutvars_mapping parameter!";
					}
					$self->{$cutvar}->{'lines'}->{$nLine}->{$k} = $cutvar_values[$v-1];
				}
			}
			for my $j(1..$#tokens+1)
			{
				#copy value from previous line if empty
				if((grep {(($_) and ($_ eq $j))} split(/,/, $mainKeysNotEmpty)) and ($self->{$cutvar}->{'lines'}->{$nLine}->{$j} =~ /^\s*$/))
				{
					$self->{$cutvar}->{'lines'}->{$nLine}->{$j} = $self->{$cutvar}->{'lines'}->{$nLine-1}->{$j} if($nLine > 2);
				}
			}
			#else
			#{
			#	die "Error output_cutvars_mapping parameter not found in cfg.";
			#}
			$self->{'lines'}->{++$nLine_entire} = {};
			for my $j(keys %{$self->{$cutvar}->{'lines'}->{$nLine}})
			{
				$self->{'lines'}->{$nLine_entire}->{$j} = $self->{$cutvar}->{'lines'}->{$nLine}->{$j};
			}

			if($secondLinePattern_)
			{
				my $set_previous_tokens = 0;
				if( $line_count <= $#ascii_lines)
				{
					my $next_ascii_line = $ascii_lines[$line_count];
					($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - Page{".$self->{pageCount}."} Line{$line_count} ascii_line:[$ascii_line] next_ascii_line:[$next_ascii_line]");
					my @tokens_next_line = map { $_ =~ /^\s*(.*?)\s*$/; $1} ($next_ascii_line =~ qr{$secondLinePattern_});

					if(scalar(@tokens_next_line))
					{
						$set_previous_tokens = 1;
					}
				}
				else
				{
					$set_previous_tokens = 1;
				}
				if($set_previous_tokens)
				{
					$self->{previous_tokens} = [@main_pattern_tokens];
					$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - previous_tokens set to main_pattern_tokens:".join('::',@{$self->{previous_tokens}}));
				}
			}
		}


		#($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() -  mainHeader_tokens        =".join('::',@mainHeader_tokens       ));

	}

	if($self->{'DEBUG'})
	{
		close($FH) or die "ApplicationError: Unable to close file - $! -$^E";
	}
    return 1;
}

__PACKAGE__;


