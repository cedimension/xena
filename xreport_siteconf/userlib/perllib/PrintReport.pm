
package PrintReport;

use strict;

use XReport;
use XReport::Spool;


sub new {
  my $class = shift;
  my $self = bless {JR => shift,
		    WORKDIR => shift,
		    CFG => shift,
		    spool => XReport::Spool->new(),
		   }, $class;
  
#  i::logit( "CFGREF: ".ref($self->{CFG} ));
  $self->{duplex} = (ref($self->{CFG}) eq 'HASH' ?  $self->{CFG}->{duplex} : $self->{CFG}->getAttribute('duplex') );

  i::logit( "PrintReport Initialized" );
  return $self;
}

sub Commit {
  my $self = shift;
  my ($workdir, $jr) = @{$self}{qw(WORKDIR JR)};
  $workdir =~ s/\\/\//g;
#  my @filelist = ();
  push my @filelist, (grep /\.pdf$/i, glob("$workdir/*"));
  my ($jrid, $prtdest) = $jr->getValues(qw(JobReportId XferRecipient));
  ($prtdest, my $duplex) = split /:/, $prtdest, 2;
  my $copies = 1;
  if (!$prtdest && (my $OS400 = $jr->getValues('OS400attrs')) ) {
    $prtdest = $OS400->{'Output queue name'};
    $copies = $OS400->{'Total copies'} if $OS400->{'Total copies'} && $OS400->{'Total copies'} > 1;
  }
  $duplex = $self->{duplex} unless $duplex;

  i::logit( "PrintReport now processing PDFS in \"$workdir\" (".scalar(@filelist).") to be printed at $prtdest ($duplex)" );
  return;
  return unless $prtdest;
  # Duplex 1: no 2: Long Side 3: Short Side 
  for(@filelist) {
    next unless -f $_;
    $self->{spool}->AddFile(
			    InputFileName => $_,
			    JobReportId => $jrid,
			    PrintDest => $prtdest,
			    PrintParameters => '-c '.($copies + 0),
			    PrintDuplex => ($duplex =~ /^(1|2|3)$/              ? $1 
					    : $duplex =~ /^no$/i                ? '1' 
					    : $duplex =~ /^Long(?:\s?Side)?$/i  ? '2' 
					    : $duplex =~ /^Short(?:\s?Side)?$/i ? '3' 
					    : '2')
			   );
  }

}

sub Close {
}

__PACKAGE__;
