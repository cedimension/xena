package XReport::OUTPUT::PageMKExcel;
use constant DEBUG => 0;

=example of parsing file 
<jobreport name="$JobReportName" linePos="raw"   >
	<exit 
	    type="PageMKExcel" 
	    ref="ADFVBHBA._SCTLDOUT2.__SYSUT2.pm"  
		mainHeader="^(.*SICHERHEITEN-NR\s+STAT\sCL\s+SICHEREITENART\s+KD\.NR\.SI-GEBER\s+NAME\s+ABL\.DAT\.)\s*$" 
		mainPattern="^(.{4})\s(\d{16})\s(.{9})(.{28})(\d{16})\s(.{33})(\d+\/\d+\/\d+)\s*$"
		secondLinePattern="^(.{59})(\d{16})\s(.{33}).*$"
		outputHeader="Kd.Nr.SI-Geber,Sicherheitengeber,Kd.Nr.Kr.-Nehmer,Kreditnehmer,Sicherheiten-Nr.,Sicherheitenart,Ablauf-Datum,Betreuerschlüssel,Betreuername,Abteilung,Abt.Bezeichnung"
		outputNumericKeys="3"
		output_order_keys_mapping="1=5,2=6,3=9,4=10,5=2,6=4,7=7,8=1,9=12,10=12,11=12"  
		cutvar_rules="LINE=2;REGEX=^.{7}(\d{5})\s+(.+?)\s*DAT.*(\d+\/\d+\/\d+).*$"
		output_cutvars_mapping="10=1,11=2,12=3"
		reportDescriptionInCfg="Sicherheiten-/Versicherungsablauf in 1 Monat erreicht" 
		
		
		firstTable_col_key_value="1=Jobname=ADFVBHBA,2=Reportname=Sicherheiten-/Versicherungsablauf in 1 Monat erreicht,3=Anforderungsdatum=get_cutvarkey(3)"
		_mainKeysNotEmpty=""
	    DEBUG="1"																					
	/>
  <page name="PAGE001" test="1" />   
</jobreport>
=cut

use Data::Dumper;
use Excel::Writer::XLSX;


sub new {
	my ($className, $parser, $workdir, $exitdom) = @_;

	my $DEBUG = $exitdom->getAttribute('DEBUG'); 
	my $ExistTypeMap = $exitdom->getAttribute('ExistTypeMap');
	die "ExistTypeMap parameter is missing in parsingfile!" if(!$ExistTypeMap); 
	$DEBUG  =  $DEBUG or DEBUG or $main::veryverbose;

	#printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - className=$className - workdir=$workdir - parser=".ref($parser)." - exitdom=".ref($exitdom) );
	my $self = {
		   'jr' => $parser->{jr}
		  ,'ExistTypeMap' => $ExistTypeMap
		  ,'parser' => $parser
          ,'workdir' => $workdir
		  ,'exitdef' => $exitdom 
		  ,'lines' => {} 
		  ,'cutvar' => '$$$$' 
		  ,'pageCount' => 0 
		  ,'FILEHIN' => '' 
		  ,'DEBUG' => $DEBUG
		  };
	bless $self, $className;
}
 



sub printOnlyInDebug{
	my $self = shift;
	$self->{'DEBUG'} and i::logit("DEBUG - ".join('::',@_));
}

sub Finalize {
    my $self = shift;
	$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() - ref(self)=".ref($self).join('::', caller()))  ;
	($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  line=".scalar(keys %{$self->{$cutvar}->{'lines'}}) );
	($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  finalLines=".scalar(keys %{$self->{'finalLines'}}) );
 
	my ($JobReportName, $JobReportDescr) = $self->{jr}->getValues(qw(JobReportName JobReportDescr));
	$self->{lines}->{output_cutvars_mapping} = $self->{'exitdef'}->getAttribute('output_cutvars_mapping');
	my $reportDescriptionInCfg  = $self->{'exitdef'}->getAttribute('reportDescriptionInCfg');
	$reportDescriptionInCfg  = $JobReportDescr if(!$reportDescriptionInCfg);
	my $firstTable_col_key_value  = $self->{'exitdef'}->getAttribute('firstTable_col_key_value'); 
	
	
	$self->{lines}->{outputNumericKeys} = $self->{'exitdef'}->getAttribute('outputNumericKeys'); 
	

	#if($printSplittedReports)
	if(0)	
	{
		for my $cutvar (sort keys %{$self->{'cutvars'}})
		{
			my $ExcelFileNameTrue = $self->{jr}->getFileName('Excel'); $ExcelFileNameTrue =~ s/\.excel$/\.$cutvar\.xlsx/i;	
			$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileNameTrue=".$ExcelFileNameTrue );
			my $workbook = Excel::Writer::XLSX->new($ExcelFileNameTrue); 
			my $worksheet = $workbook->add_worksheet($JobReportName);	
			$worksheet->set_first_sheet();
			$worksheet->activate(); 
			my $row ;
			my $col ;
			my $initial_row = 0;
			my $initial_col = 0;
			my $max_col = 0; 
			if($reportDescriptionInCfg)
			{
				$worksheet->write_string(0, 0, $reportDescriptionInCfg) if (defined $reportDescriptionInCfg);
				$initial_row = 1;
			}
			for my $typeOfLines (qw(lines ))
			{
				#my @output_cutvars_mapping = split(/,/, $self->{$cutvar}->{$typeOfLines}->{output_cutvars_mapping}); 
				my @outputNumericKeys = split(/,/, $self->{$typeOfLines}->{outputNumericKeys}); 
				$row = $initial_row;
				for my $nLine (1..scalar(keys %{$self->{$cutvar}->{$typeOfLines}}))
				{
					$col=$initial_col;
					#for my $value (map {$self->{$cutvar}->{$typeOfLines}->{$nLine}->{$_} } (sort {$a <=> $b} keys %{$self->{$cutvar}->{$typeOfLines}->{$nLine}}))			
					for my $i (sort {$a <=> $b} keys %{$self->{$cutvar}->{$typeOfLines}->{$nLine}})
					{ 
						my $value = $self->{$cutvar}->{$typeOfLines}->{$nLine}->{$i}; 
						#if($value =~ /^[\d\.\,\-]+$/)
						if((grep {$_ eq $i} @outputNumericKeys ) and ($value =~ /^[\d\.\,\-]+$/)) 
						{
							$value =~ s/\.//g;
							$value =~ s/,/\./g;
							$worksheet->write_number($row, $col, $value);
						}
						else
						{
							$worksheet->write_string($row, $col, $value)  if (defined $value); 
						}
						$col++;
						$max_col = $col if ($col >$max_col);
					}
					$row++;
				}
				$initial_col=1+$max_col;
			}
			  
			$workbook->close(); 
		}
	}
	#if($printEntireReport)
	if(1)
	{
		my $ExcelFileNameTrue = $self->{jr}->getFileName('Excel'); $ExcelFileNameTrue =~ s/\.excel$/\.xlsx/i;	
		$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileNameTrue=".$ExcelFileNameTrue );
		my $workbook = Excel::Writer::XLSX->new($ExcelFileNameTrue); 
		my $worksheet = $workbook->add_worksheet($JobReportName);	
		$worksheet->set_first_sheet();
		$worksheet->activate(); 
		my $row ;
		my $col ;
		my $initial_row = 0;
		my $initial_col = 0;
		my $max_col = 0; 
		#if($reportDescriptionInCfg)
		#{
		#	$worksheet->write_string(0, 0, $reportDescriptionInCfg); 
		#	$initial_row = 1;
		#}
		if($firstTable_col_key_value)
		{  
			my @cutvar_values = @{$self->{'cutvar_values'}};
			for my $c_k_eq_v( split(/,/, $firstTable_col_key_value))
			{
				my($c, $k, $v) = ( split(/=/, $c_k_eq_v));
				
				my $r = 0; $c--;
				$worksheet->write_string($r, $c, $k); 
				$r++;
				if($v =~ /get_cutvarkey\((\d+)\)/i)
				{ 
					$v = $cutvar_values[$1-1]; 
				}
				elsif($v =~ /get_jr_field\((.+)\)/i)
				{
					$v = $self->{'jr'}->getValues($1);
				}
				$worksheet->write_string($r, $c, $v); 
			} 
			$initial_row = 2;
		}
		
		 
		
		for my $typeOfLines (qw(lines ))
		{
			my @outputNumericKeys = split(/,/, $self->{$typeOfLines}->{outputNumericKeys}); 
			$row = $initial_row;
			for my $nLine (1..scalar(keys %{$self->{$typeOfLines}}))
			{
				$col=$initial_col;
				#for my $value (map {$self->{$typeOfLines}->{$nLine}->{$_} } (sort {$a <=> $b} keys %{$self->{$typeOfLines}->{$nLine}}))			
				for my $i (sort {$a <=> $b} keys %{$self->{$typeOfLines}->{$nLine}})
				{ 
					my $value = $self->{$typeOfLines}->{$nLine}->{$i}; 
					#if($value =~ /^[\d\.\,\-]+$/)
					#if((grep {$_ eq $i} @outputNumericKeys ) and ($value =~ /^[\d\.\,\-]+$/)) 
					#{
					#	$value =~ s/\.//g;
					#	$value =~ s/,/\./g;
					#	$worksheet->write_number($row, $col, $value); 
					#}
					if((grep {$_ eq $i} @outputNumericKeys ) and ($value =~ /^[\d\.\,\-\+]+$/))
					{
						$value =~ s/\.//g;
						$value =~ s/,/\./g;
						$value = $2.$1 if($value =~/^(.+)([\-\+])\s*$/);
						$worksheet->write_number($row, $col, $value);
					}
					else
					{
						$worksheet->write_string($row, $col, $value)   if (defined $value); 
					}
					$col++;
					$max_col = $col if ($col >$max_col);
				}
				$row++;
			}
			$initial_col=1+$max_col;
		}
		  
		$workbook->close(); 
	}
	 
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - mainPattern=".$self->{'exitdef'}->getAttribute('mainPattern'));
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - finalPattern=".$self->{'exitdef'}->getAttribute('finalPattern'));
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - totalValuePattern=".$self->{'exitdef'}->getAttribute('totalValuePattern'));
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - pageCount=".$self->{'pageCount'}); 
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - DEBUG=".$self->{'DEBUG'}); 
}

sub Print {
    my $self = shift;
    my $page = shift;
	$self->{pageCount}++;
	#i::logit(__PACKAGE__.__LINE__." sub Print() - Print - DEBUG=".$self->{'DEBUG'}); 
	$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - ref(self)=".ref($self)." ref(page)=".ref($page).join('::', caller())) if ($self->{pageCount} == 1);
	my $FH;
	if($self->{'DEBUG'})
	{
		$FH = Symbol::gensym();
		my $ExcelFileName = $self->{jr}->getFileName('Excel'); $ExcelFileName =~ s/\.excel$/\.txt/i;
		my $w_or_a = ">>";
		$w_or_a = ">" if ($self->{pageCount} == 1);
		open($FH, $w_or_a.$ExcelFileName) or die "ApplicationError: Unable to open file $ExcelFileName - $! -$^E"; 
	}
	($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileName=".$ExcelFileName );

	my $mainHeader = $self->{'exitdef'}->getAttribute('mainHeader');
	my $mainPattern_  = $self->{'exitdef'}->getAttribute('mainPattern');  
	my $secondLinePattern_  = $self->{'exitdef'}->getAttribute('secondLinePattern');  
	my $thirdLinePattern_  = $self->{'exitdef'}->getAttribute('thirdLinePattern');  
	
	 
	my $outputHeader_ = $self->{'exitdef'}->getAttribute('outputHeader');
	my @outputHeader = split(/,/, $outputHeader_); 
	my $cutvar = $self->{'cutvar'};
	#my $cutvar_line = $self->{'exitdef'}->getAttribute('cutvar_line');
	#my $cutvar_regex = $self->{'exitdef'}->getAttribute('cutvar_regex');  
	
	my $mainKeysNotEmpty = $self->{'exitdef'}->getAttribute('mainKeysNotEmpty'); 

	#my $outputOrderKeys_ = $self->{'exitdef'}->getAttribute('outputOrderKeys');
	my $output_order_keys_mapping = $self->{'exitdef'}->getAttribute('output_order_keys_mapping');
	my $output_cutvars_mapping = $self->{'exitdef'}->getAttribute('output_cutvars_mapping');
	
	
	if(!exists $self->{'cutvar_rules'})
	{
		my $cutvar_rules = $self->{'exitdef'}->getAttribute('cutvar_rules');
		for my $rule (split(/_ENDOFRULE_/, $cutvar_rules))
		{
			my ($kye_line, $value) = ($rule =~ /LINE=(\d+);REGEX=(.+)$/i );
			$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - kye_line=$kye_line - value=$value"); 
			die "Error in cutvar_rules: $cutvar_rules." if($kye_line !~ /^\d+$/);
			$self->{'cutvar_rules'}->{$kye_line} = $value;
		}
		die "Error in cutvar_rules not defined in cgf." if(!$cutvar_rules);
	}
	
	
	#my @outputOrderKeys = split(/,/, $outputOrderKeys_); 
	#if(scalar (@outputOrderKeys))
	#{
	#	for my $i(1..$#outputOrderKeys+1)
	#	{
	#		$self->{outputOrderKeys}->{$outputOrderKeys[$i-1]} = $i;
	#	}
	#}

	($self->{pageCount} == 1) and (!scalar(keys %{$self->{'mainHeader'}})) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - ref(mainHeader)1=".ref($self->{'mainHeader'}));

	my $mainHeader_line = 0; 
	
	my $cutvar_not_yet_found = 1;
	my @ascii_lines;
	for my $line(@{$page->lineList()}) {
		my $lines = $line->AsciiValueList();
		for my $ascii_line (map {substr($_,1)} @$lines)
		{
			push @ascii_lines, $ascii_line;
			$self->{'DEBUG'} and print $FH "$ascii_line\n";#TESTSAN
		}
	}
	my $line_count = 0;
	for my $ascii_line_idx (0..$#ascii_lines)
	{
		my $ascii_line = $ascii_lines[$ascii_line_idx];
		next if ($line_count > $ascii_line_idx);
		$line_count++; 
		
		if(exists $self->{'cutvar_rules'}->{$line_count})
		#if($line_count == $cutvar_line) 
		{
			my $cutvar_regex = $self->{'cutvar_rules'}->{$line_count};
			my @tokens = ($ascii_line =~ qr/$cutvar_regex/i);
			if(scalar(@tokens) )
			{
				if($cutvar_not_yet_found)
				{
					$cutvar_not_yet_found = 0;
					$self->{'cutvar_values'} = [];
					$self->{'cutvar'} = $tokens[0];
					#$self->{'cutvar_line'} = $ascii_line;				
					$cutvar = $self->{'cutvar'};
					$self->{'cutvars'}->{$cutvar}++; 
				}
				push @{$self->{'cutvar_values'}}, @tokens;
				# for my $j(0..$#tokens)
				# {
					# #$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - token248=".$tokens[$j]);
					# push @{$self->{'cutvar_values'}}, $tokens[$j];
				# }
			}
			#else
			#{
			#	die "Cutvar Line [$ascii_line] does not match with regex: $cutvar_regex . Check the configuration."
			#}
		} 
	
	
		#my @mainHeader_tokens        = ($ascii_line  =~ qr/$mainHeader/i) if(!$mainHeader_line );
		my $nLine = scalar( keys %{$self->{$cutvar}->{'lines'}}) ; #number of lines already read
		
		my $nLine_entire = scalar( keys %{$self->{'lines'}}) ; #number of lines already read
		
		if(!$nLine)
		{
			#write header
			$nLine = 1;		
			for my $k(0..$#outputHeader)
			{  
				$self->{$cutvar}->{'lines'}->{$nLine}->{$k+1} = $outputHeader[$k];
				($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - header ".($k+1)."=".$outputHeader[$k]);
			}
		} 
		if(!$nLine_entire)
		{
			#write header
			$nLine_entire = 1;		
			for my $k(0..$#outputHeader)
			{  
				$self->{'lines'}->{$nLine_entire}->{$k+1} = $outputHeader[$k];
			}
		} 
		my @tokens = map { $_ =~ /^\s*(.*?)\s*$/; $1} ($ascii_line =~ qr{$mainPattern_});
		
		if((scalar(@tokens)) and ( length($ascii_line) > 0 ))
		{ 
			for my $otherLinePattern($secondLinePattern_, $thirdLinePattern_)
			{
				next if(!$otherLinePattern);
				$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - otherLinePattern=$otherLinePattern");
				my $next_ascii_line = $ascii_lines[$line_count];
				$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - next_ascii_line=\n$next_ascii_line");
				my @tokens_next_line = map { $_ =~ /^\s*(.*?)\s*$/; $1} ($next_ascii_line =~ qr{$otherLinePattern});
				die "Line($line_count) [$next_ascii_line]  [$ascii_line] does not match with otherLinePattern[$otherLinePattern]" if(!scalar(@tokens_next_line));
				$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - tokens_next_line=".join('::',@tokens_next_line));
				$line_count++;
				push @tokens, @tokens_next_line;
			} 
			push @tokens, '';
			$self->{$cutvar}->{'lines'}->{++$nLine} = {}; 
			
			#reorder keys 
			if( $output_order_keys_mapping )
			{
				for my $k_eq_v( split(/,/, $output_order_keys_mapping))
				{
					#1=2,2=3,4=4,5=5,6=9,7=1,8=9,9=6,10=7,11=8
					my($k, $v) = ( split(/=/, $k_eq_v));
					die "Error in $output_order_keys_mapping parameter [".$self->{$output_order_keys_mapping}."] [$k=$v] [line=$line_count]" if((!defined $v) or (!defined $k) ) ;
					$self->{$cutvar}->{'lines'}->{$nLine}->{$k} = $tokens[$v-1] if (scalar(@tokens) >= ($V-1)); 
					($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - $k=".$tokens[$v-1]);
				}
			}
			#
			#if( exists $self->{outputOrderKeys} )
			#{
			#	my $newi = $self->{outputOrderKeys}->{$j};
			#	#$self->{$cutvar}->{'lines'}->{$nLine}->{$newi} = $tokens[$j-1] if $newi;
			#	$self->{$cutvar}->{'lines'}->{$nLine}->{$newi} = $tokens[$j-1];
			#	($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - $newi=".$tokens[$j-1]);
			#}  
			
			for my $j(1..$#tokens+1)
			{
				($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - tokens[".($j)."]=".$tokens[$j-1]);
				if( !$output_order_keys_mapping )
				{
					$self->{$cutvar}->{'lines'}->{$nLine}->{$j} = $tokens[$j-1];
				}
				 
			}
			if( $output_cutvars_mapping )
			{
				#my @cutvar_values = ($self->{'cutvar_line'} =~ qr/$cutvar_regex/i);
				my @cutvar_values = @{$self->{'cutvar_values'}}; 
				#$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - cutvar_values=".join('::',@cutvar_values ));
				#die "forced ERROR";
				for my $k_eq_v( split(/,/, $output_cutvars_mapping))
				{
					my($k, $v) = ( split(/=/, $k_eq_v));
					die "Error in output_cutvars_mapping parameter. Check regex. [$k=$v] [line=$line_count][LEN=".scalar(@cutvar_values)."]" if((!defined $v) or (!defined $k) or (!scalar(@cutvar_values))) ;
					$self->{$cutvar}->{'lines'}->{$nLine}->{$k} = $cutvar_values[$v-1] if (scalar(@cutvar_values) >= ($v-1)); 
				} 
			}
			for my $j(1..$#tokens+1)
			{
				#copy value from previous line if empty
				if((grep {(($_) and ($_ eq $j))} split(/,/, $mainKeysNotEmpty)) and ($self->{$cutvar}->{'lines'}->{$nLine}->{$j} =~ /^\s*$/))
				{
					$self->{$cutvar}->{'lines'}->{$nLine}->{$j} = $self->{$cutvar}->{'lines'}->{$nLine-1}->{$j} if($nLine > 2);
				}  
			}
			#else
			#{
			#	die "Error output_cutvars_mapping parameter not found in cfg.";
			#}
			$self->{'lines'}->{++$nLine_entire} = {}; 
			for my $j(keys %{$self->{$cutvar}->{'lines'}->{$nLine}})
			{
				$self->{'lines'}->{$nLine_entire}->{$j} = $self->{$cutvar}->{'lines'}->{$nLine}->{$j}; 
			}
			
		}
		else
		{
			($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - not match $i=\n".$ascii_line) if( length($ascii_line) > 0 );
		}
		
		($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() -  mainHeader_tokens        =".join('::',@mainHeader_tokens       ));

	}
 
	if($self->{'DEBUG'})
	{
		close($FH) or die "ApplicationError: Unable to close file - $! -$^E";
	}
    return 1;
}

__PACKAGE__;


