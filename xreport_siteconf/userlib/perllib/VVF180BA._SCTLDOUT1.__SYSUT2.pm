package XReport::OUTPUT::PageMKExcel;
use constant DEBUG => 0;

=example of parsing file 
<jobreport name="$JobReportName" linePos="raw"   >
<exit
	    type="PageMKExcel"
		DEBUG="1"
	    ExistTypeMap="3"
	    ref="VVF180BA._SCTLDOUT1.__SYSUT2.pm"
		mainPattern="^\s(.+?)(\s+)([\d\.,]+)\s*$"
		mainHeaderKeysNewRecord="1"
		mainHeader="^\s*Filiale\s*$,^\s*TAGESKASSENSALDO\sVORTAG\s*$,^\s*TAGESKASSENSALDO\sSOLL-BESTAND\s*$,^\s*TAGESKASSENSALDO\sIST-BESTAND\s*$,^\s*POSITIVE\sDIFFERENZEN\s*$,^\s*NEGATIVE\sDIFFERENZEN\s*$"
		outputHeader="Filiale,Tageskassensaldo Vortag,Tageskassensaldo-Soll,Tageskassensaldo Ist-bestand,Positive Differenzen,Negative Differenzen"
        finalHeader="^\s*SUMME\sTAGESKASSENSALDO\sSOLL-BESTAND\s*$,^\s*SUMME\sTAGESKASSENSALDO\sIST-BESTAND\s*$,^\s*SUMME\sPOSITIVE\sDIFFERENZEN\s*$,^\s*SUMME\sNEGATIVE\sDIFFERENZEN\s*$"
		finalOutputHeader="Summe Tageskassensaldo Soll-bestand,Summe Tageskassensaldo Ist-Bestand,Summe Positive Differenzen,Summe negative Differenzen"
		outputNumericKeys="2,3,4,5,6"
		finalOutputNumericKeys="1,2,3,4" 
		finalHeaderKeysNewRecord="1"
		
		_outputNumericKeys="2,3,4,5,6"
		_finalOutputNumericKeys="1,2,3,4" 
		
		firstTable_col_key_value="1=Jobname=get_jr_field(ComputedJobName),2=Reportname=EOY KASSAABSTIMMUNG,3=Anforderungsdatum=get_cutvarkey(1)" 
		>
		<cutvar_rules>
			<cutvar_rule line="1" regex="^.*(\d+[\\\/\.]\d+[\\\/\.]\d+).*$"  numberOfTokens="1" />
		</cutvar_rules>
	</exit> 
  <page name="PAGE001" test="1" />   
</jobreport>
=cut

use Data::Dumper;
use Excel::Writer::XLSX;


sub new {
  my ($className, $parser, $workdir, $exitdom) = @_;
  #$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - className=$className - workdir=$workdir - parser=".ref($parser)." - exitdom=".ref($exitdom) );
  my $DEBUG = $exitdom->getAttribute('DEBUG'); 
  my $ExistTypeMap = $exitdom->getAttribute('ExistTypeMap');
  die "ExistTypeMap parameter is missing in parsingfile!" if(!$ExistTypeMap);
  $DEBUG  =  $DEBUG or DEBUG or $main::veryverbose;
  
  my $self = {
		   'jr' => $parser->{jr}
		  ,'DEBUG' => $DEBUG
		  ,'ExistTypeMap' => $ExistTypeMap
          ,'parser' => $parser
          ,'workdir' => $workdir
          ,'exitdef' => $exitdom
          ,'lines' => {} 
          ,'pageCount' => 0
          ,'FILEHIN' => undef
		  };
  bless $self, $className;  
  
  i::logit(__PACKAGE__.__LINE__." sub new() - DEBUG:".$self->{DEBUG});

  for my $node(@{$self->{'exitdef'}->getChildNodes()})
  {
  	$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - NodeName:".$node->getNodeName()); 
  	if($node->getNodeName() eq 'cutvar_rules')
  	{
  		for my $node2(grep {$_->getNodeName() eq 'cutvar_rule' } @{$node->getChildNodes()})
  		{
  			$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - NodeName2:".$node2->getNodeName());
  			my $line = $node2->getAttribute('line');
  			my $regex = $node2->getAttribute('regex');
  			$self->{cutvar_rules}->{$line} = $regex;
  		}
  	}
  }
  $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub new() - patterns:".Dumper(%{$self->{patterns}}));
  return $self;
}
 
 
sub printOnlyInDebug{
    my $self = shift;
	$self->{DEBUG} and i::logit("DEBUG - ".join('::',@_));
}

sub Finalize {
    my $self = shift;
	$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() - ref(self)=".ref($self).join('::', caller()))  ;
	($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  line=".scalar(keys %{$self->{lines}}) );
 
	my ($JobReportName, $JobReportId) = $self->{jr}->getValues(qw(JobReportName JobReportId));
	
	my $FH = self->{FILEHIN};
	if($FH)
	{
		close($FH) or die "ApplicationError: Unable to close file - $! -$^E";	
	}
	 
	$self->{lines}->{numericKeys} = $self->{'exitdef'}->getAttribute('outputNumericKeys');
	$self->{finalLines}->{numericKeys} = $self->{'exitdef'}->getAttribute('finalOutputNumericKeys');
	
	$self->{lines}->{amountKeys} = $self->{'exitdef'}->getAttribute('outputAmountKeys');
	$self->{finalLines}->{amountKeys} = $self->{'exitdef'}->getAttribute('finalOutputAmountKeys');
	
	
	
	my $ExcelFileNameTrue = $self->{jr}->getFileName('Excel'); $ExcelFileNameTrue =~ s/\.excel$/\.xlsx/i;	
	$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileNameTrue=".$ExcelFileNameTrue );
	my $workbook = Excel::Writer::XLSX->new($ExcelFileNameTrue); 
	my $worksheet = $workbook->add_worksheet($JobReportName);	
	$worksheet->set_first_sheet();
	$worksheet->activate();
	
	
	my $row = 0;
	my $col = 0;
	my $initial_col = 0;
	my $initial_row = 0;
	my $max_col = 0;
	
	
	my $firstTable_col_key_value  = $self->{'exitdef'}->getAttribute('firstTable_col_key_value');

	if($firstTable_col_key_value)
	{
		my @cutvar_values = @{$self->{'cutvar_values'}};
		for my $c_k_eq_v( split(/,/, $firstTable_col_key_value))
		{
			my($c, $k, $v) = ( split(/=/, $c_k_eq_v));

			my $r = 0; $c--;
			$worksheet->write_string($r, $c, $k);
			$r++;
			if($v =~ /get_cutvarkey\((\d+)\)/i)
			{
				$v = $cutvar_values[$1-1];
			}
			elsif($v =~ /get_jr_field\((.+)\)/i)
			{
				$v = $self->{'jr'}->getValues($1);
			}
			$worksheet->write_string($r, $c, $v);
		}
		$initial_row = 2;
	}
	
	for my $typeOfLines (qw(lines finalLines))
	{
		my @numericKeys = split(/,/, $self->{$typeOfLines}->{numericKeys});  
		my @amountKeys = split(/,/, $self->{$typeOfLines}->{amountKeys});  
		$row = $initial_row;
		for my $nLine (1..scalar(keys %{$self->{$typeOfLines}}))
		{
			$col=$initial_col;
			for my $i (sort {$a <=> $b} keys %{$self->{$typeOfLines}->{$nLine}})
			{ 
				my $value = $self->{$typeOfLines}->{$nLine}->{$i};
				#check if the value is numeric 
				if((grep {$_ eq $i} @numericKeys ) and ($value =~ /^[\d\.\,\-]+$/)) 
				{
					$value =~ s/\.//g;
					$value =~ s/,/\./g;
					$value = $2.$1 if($value =~/^(.+)([\-\+])\s*$/);
					$worksheet->write_number($row, $col, $value);
				}
				elsif((grep {$_ eq $i} @amountKeys ) and ($value =~ /^[\d\.\,\-\+]+$/))
				{
					$value =~ s/\.//g;
					$value =~ s/,/\./g;
					$value = $2.$1 if($value =~/^(.+)([\-\+])\s*$/);
					my $format03 = $workbook->add_format();
					$format03->set_num_format( '#,##0.00' );
					#$worksheet->write( 2, 0, 1234.56, $format03 );      # 1,234.56
					$worksheet->write($row, $col, $value,$format03);
				}
				else
				{
					$worksheet->write_string($row, $col, $value);
				}
				$col++;
				$max_col = $col if ($col >$max_col);
			}
			$row++;
		}
		$initial_col=1+$max_col;
	}
	  
	$workbook->close(); 
	  
	i::logit(__PACKAGE__.__LINE__." sub Finalize() - totalValuePattern=".$self->{'exitdef'}->getAttribute ('mainPattern')); 
	i::logit(__PACKAGE__.__LINE__." sub Finalize() - pageCount=".$self->{'pageCount'}); 
}

sub Print {
    my $self = shift;
    my $page = shift;
	$self->{pageCount}++;
	
	$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - ref(self)=".ref($self)." ref(page)=".ref($page).join('::', caller())) if ($self->{pageCount} == 1);
	my $FH = self->{FILEHIN};
	if($self->{DEBUG})
	{ 
		if(!$FH)
		{
			$FH = Symbol::gensym();
			my $ExcelFileName = $self->{jr}->getFileName('Excel'); $ExcelFileName =~ s/\.excel$/\.txt/i;
			open($FH, ">$ExcelFileName") or die "ApplicationError: Unable to open file $ExcelFileName - $! -$^E";		
			self->{FILEHIN} = $FH;
		}
	}
	($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileName=".$ExcelFileName );
  
	
	my $outputHeader_ = $self->{'exitdef'}->getAttribute('outputHeader');
	my @outputHeader = split(/,/, $outputHeader_); 
	my $mainHeader_ = $self->{'exitdef'}->getAttribute('mainHeader');
	my @mainHeader = split(/,/, $mainHeader_);
	my $mainPattern = $self->{'exitdef'}->getAttribute('mainPattern');
	my $mainHeaderKeysNewRecord_ = $self->{'exitdef'}->getAttribute('mainHeaderKeysNewRecord');
	my @mainHeaderKeysNewRecord = split(/,/, $mainHeaderKeysNewRecord_); 
	
	
	my $finalOutputHeader_ = $self->{'exitdef'}->getAttribute('finalOutputHeader');
	my @finalOutputHeader = split(/,/, $finalOutputHeader_); 
	my $finalHeader_ = $self->{'exitdef'}->getAttribute('finalHeader');
	my @finalHeader = split(/,/, $finalHeader_);
	my $finalHeaderKeysNewRecord_ = $self->{'exitdef'}->getAttribute('finalHeaderKeysNewRecord');
	my @finalHeaderKeysNewRecord = split(/,/, $finalHeaderKeysNewRecord_); 
	
	
	
	

	my @ascii_lines; 
	my $line_count = 0;
	my $nLine = scalar( keys %{$self->{lines}}) ; #number of lines already read
	if(!$nLine)
	{
		#write header
		$nLine = 1;		
		for my $k(0..$#outputHeader)
		{  
			$self->{lines}->{$nLine}->{$k+1} = $outputHeader[$k];
			($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - header ".($k+1)."=".$outputHeader[$k]);
		}
	}
	my $nfinalLine = scalar( keys %{$self->{finalLines}}) ; #number of lines already read
	if(!$nfinalLine)
	{
		#write header
		$nfinalLine++;		
		for my $k(0..$#finalOutputHeader)
		{  
			$self->{finalLines}->{$nfinalLine}->{$k+1} = $finalOutputHeader[$k];
			$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - finalOutputHeader ".($k+1)."=".$finalOutputHeader[$k]);
		}
	}
	for my $line(@{$page->lineList()}) {
		my $lines = $line->AsciiValueList();
		for my $ascii_line (map {substr($_,1)} @$lines)
		{
			push @ascii_lines, $ascii_line;
			$self->{DEBUG} and print $FH "$ascii_line\n";
			$line_count++;

			if(exists $self->{'cutvar_rules'}->{$line_count})
			{
				my $cutvar_regex = $self->{'cutvar_rules'}->{$line_count};
				my @cutvar_tokens = map { $_ =~ /^\s*(.+?)\s*$/; $1} ($ascii_line =~ qr/$cutvar_regex/i);
				if(scalar(@cutvar_tokens) )
				{
					if($cutvar_not_yet_found)
					{
						$cutvar_not_yet_found = 0;
						$self->{'cutvar_values'} = [];
						$self->{'cutvar'} = $cutvar_tokens[0];
						$cutvar = $self->{'cutvar'};
						$self->{'cutvars'}->{$cutvar}++;
					}
					for my $j(0..$#cutvar_tokens)
					{
						push @{$self->{'cutvar_values'}}, $cutvar_tokens[$j];
					}
				}
			}
			#my @tokens = map { $_ =~ /^\s*(.*?)\s*$/; $1} ($ascii_line =~ qr{$mainPattern}); 
			my @tokens = ($ascii_line =~ qr{$mainPattern}); 
			if((scalar(@tokens)) and ( length($ascii_line) > 0 ))
			{
				($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - match $i=\n".join('::',@tokens)) if( length($ascii_line) > 0 );
				for my $i(0..$#mainHeader)
				{
					my $regex = $mainHeader[$i];
					if($tokens[0] =~ qr/$regex/i)
					{
						if(grep {$_ eq ''.($i+1)} @mainHeaderKeysNewRecord)
						{
							$self->{lines}->{++$nLine} = {}; 
						}
						$self->{lines}->{$nLine}->{$i+1} = $1 if ($tokens[2] =~ /^\s*(.*?)\s*$/);
						($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() $nLine - ".($i+1)."=".$tokens[2]);
					}
				}
				for my $i(0..$#finalHeader)
				{
					my $regex = $finalHeader[$i];
					if($tokens[0] =~ qr/$regex/i)
					{
						$self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - [".$tokens[0]."] matches with regex:".$regex);
						if(grep {$_ eq ''.($i+1)} @finalHeaderKeysNewRecord)
						{
							$self->{finalLines}->{++$nfinalLine} = {}; 
						}
						$self->{finalLines}->{$nfinalLine}->{$i+1} = $1 if ($tokens[2] =~ /^\s*(.*?)\s*$/);
						($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() $nfinalLine - ".($i+1)."=".$tokens[2]);
					}
				}
			}
			else
			{
				($self->{pageCount} == 1) and $self->printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - not match $i=\n".$ascii_line) if( length($ascii_line) > 0 );
			} 
		}
	}
    
	return 1;
}

__PACKAGE__;


