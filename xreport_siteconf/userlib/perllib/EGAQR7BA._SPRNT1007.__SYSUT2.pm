package XReport::OUTPUT::PageMKExcel;
use constant DEBUG => 0;

=example of parsing file 
<jobreport name="$JobReportName" linePos="raw"   >
	<exit 
	    type="PageMKExcel" 
	    ref="FDMTG3MA._SPRINTA5.__SYSUT2.pm"  
		mainHeader="^(\s*Org.Einheit\s+)(Muenzsigel\s+)(Bezeichnung\s+)(Stand\s+)(Interne\s+Schwebe\s+)(externe\s+Schwebe\s+)(Unzen\s+)(Tagesendkurs\s+)(EURO-GW\.\s*)$" 
		mainPattern="^(.{18})(\d+\s+)(.{30}\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s*)$"
		mainKeysNotEmpty="1"
		finalHeader="^(\s*Total\s+)(Muenzsigel\s+)(Stand\s+)(Interne Schwebe\s+)(externe Schwebe\s+)(EURO-GW.\s*)$" 
		finalPattern="^(.{18})(\d+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s+)([\d\.\,\-]+\s*)$" 
		totalValuePattern="^(\s+TOTAL\s+)([\d\.\,\-]+\s*)$"																											
	/>
  <page name="PAGE001" test="1" />   
</jobreport>
=cut

use Data::Dumper;
use Excel::Writer::XLSX;


sub new {
  my ($className, $parser, $workdir, $exitdom) = @_;
  #printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - className=$className - workdir=$workdir - parser=".ref($parser)." - exitdom=".ref($exitdom) );
  
  my $DEBUG = $exitdom->getAttribute('DEBUG'); 
  my $ExistTypeMap = $exitdom->getAttribute('ExistTypeMap');
  die "ExistTypeMap parameter is missing in parsingfile!" if(!$ExistTypeMap);
  $DEBUG  =  $DEBUG or DEBUG or $main::veryverbose;
  
  
  my $self = {
		   'jr' => $parser->{jr}
		  ,'DEBUG' => $DEBUG
		  ,'ExistTypeMap' => $ExistTypeMap
          ,'parser' => $n
          ,'exitdef' => $exitdom
          ,'mainHeader' => {}
          ,'mainPattern' => ''
          ,'lines' => {} 
          ,'pageCount' => 0
          ,'FILEHIN' => ''
          ,'out_xls' => {}  
		  };
  bless $self, $className;
}
 
 
sub printOnlyInDebug{
	$self->{'DEBUG'} and i::logit("DEBUG - ".join('::',@_));
}

sub Finalize {
    my $self = shift;
	printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() - ref(self)=".ref($self).join('::', caller()))  ;
	($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  line=".scalar(keys %{$self->{'lines'}}) );
 
	my ($JobReportName, $JobReportId) = $self->{jr}->getValues(qw(JobReportName JobReportId));
	
	my $ExcelFileNameTrue = $self->{jr}->getFileName('Excel'); $ExcelFileNameTrue =~ s/\.excel$/\.xlsx/i;	
	printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileNameTrue=".$ExcelFileNameTrue );
	my $workbook = Excel::Writer::XLSX->new($ExcelFileNameTrue); 
	my $worksheet = $workbook->add_worksheet($JobReportName);	
	$worksheet->set_first_sheet();
	$worksheet->activate();
	
	
	my $row = 0;
	my $col = 0;
	my $initial_col = 0;
	my $max_col = 0;
	
	
	for my $typeOfLines (qw(lines))
	{
		$row = 0;
		for my $nLine (1..scalar(keys %{$self->{$typeOfLines}}))
		{
			$col=$initial_col;
			for my $value (map {$self->{$typeOfLines}->{$nLine}->{$_} } (sort {$a <=> $b} keys %{$self->{$typeOfLines}->{$nLine}}))
			{
				if($value =~ /^[\d\.\,\-]+$/)
				{
					$value =~ s/\.//g;
					$value =~ s/,/\./g;
					$worksheet->write_number($row, $col, $value);
				}
				else
				{
					$worksheet->write_string($row, $col, $value);
				}
				$col++;
				$max_col = $col if ($col >$max_col);
			}
			$row++;
		}
		$initial_col=1+$max_col;
	}
	  
	$workbook->close(); 
	  
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - totalValuePattern=".$self->{'exitdef'}->getAttribute ('mainPattern')); 
	i::logit(__PACKAGE__.__LINE__." sub Print() - Finalize - pageCount=".$self->{'pageCount'}); 
}

sub Print {
    my $self = shift;
    my $page = shift;
	$self->{pageCount}++;
	
	printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - ref(self)=".ref($self)." ref(page)=".ref($page).join('::', caller())) if ($self->{pageCount} == 1);
	my $FH = self->{FILEHIN};
	if(!$FH)
	{
		$FH = Symbol::gensym();
		my $ExcelFileName = $self->{jr}->getFileName('Excel'); $ExcelFileName =~ s/\.excel$/\.txt/i;
		open($FH, ">$ExcelFileName") or die "ApplicationError: Unable to open file $ExcelFileName - $! -$^E";		
		self->{FILEHIN} = $FH;
	}
	($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Finalize() -  ExcelFileName=".$ExcelFileName );
 
 
	
	my $mainPattern = $self->{'exitdef'}->getAttribute('mainPattern');
	my $outputKeys_ = $self->{'exitdef'}->getAttribute('outputKeys');
	my $outputHeader_ = $self->{'exitdef'}->getAttribute('outputHeader');
	my @outputKeys = split(/,/, $outputKeys_);
	my @outputHeader = split(/,/, $outputHeader_);
 
		

	my @ascii_lines;
	($self->{pageCount} == 1) and (!scalar(keys %{$self->{'mainHeader'}})) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - ref(mainHeader)1=".ref($self->{'mainHeader'}));

	my $line_count = 0;
	my $nLine = scalar( keys %{$self->{'lines'}}) ; #number of lines already read
	if(!$nLine)
	{
		#write header
		for my $k(0..$#outputKeys)
		{
			my $key = $outputKeys[$k];
			my $value = $outputHeader[$k];
			$self->{'lines'}->{'1'}->{$key} = $value;
		}
		$nLine++;
	}
	for my $line(@{$page->lineList()}) {
		my $lines = $line->AsciiValueList();
		for my $ascii_line (map {substr($_,1)} @$lines)
		{
			push @ascii_lines, $ascii_line;
			print $FH "$ascii_line\n";#TESTSAN
			$line_count++;
			
			my @tokens = map { $_ =~ /^\s*(.*?)\s*$/; $1} ($ascii_line =~ qr{$mainPattern}); 
			if((scalar(@tokens)) and ( length($ascii_line) > 0 ))
			{
				$self->{'lines'}->{++$nLine} = {}; 
				for my $j(1..$#tokens+1)
				{
					if(grep {(($_) and ($_ eq $j))} @outputKeys)
					{
						$self->{'lines'}->{$nLine}->{$j} = $tokens[$j-1];
					}
				} 
			}
			else
			{
				($self->{pageCount} == 1) and printOnlyInDebug(__PACKAGE__.__LINE__." sub Print() - not match $i=\n".$ascii_line) if( length($ascii_line) > 0 );
			} 
		}
	}
    return 1;
}

__PACKAGE__;


