package XReport::OUTPUT::Postscript::PageExit;

use File::Copy qw(move copy);
use File::Path qw(rmtree);

sub Print {
    my $self = shift;
    my $pslines = shift;
    my $ct = 0;
    my $lnref = (grep { $_->[1] =~ /^\%Mogliano Veneto,/ } map { [$ct++, $_ ] } @{$pslines} )[0];
    return $pslines unless ( ref($lnref) eq 'ARRAY' );
    my $begin = $lnref->[0]-23;
    my $end = $lnref->[0]-4;

    if ( $pslines->[$begin+3] =~ /^\%(?:Gent|Spett)/ ) {
       foreach my $lnum ( $begin..$begin+4 ) {
             $pslines->[$lnum] = '%'.$pslines->[$lnum].' % DELETED BY EXIT';
       }
    } 
    else {
       foreach my $lnum ( $begin..$end ) {
          if ( $pslines->[$lnum] =~ /^(\d+)\s+a\.amb\s*$/ ) {
               my $oldamb = $1;
             my $newamb = $oldamb + 40;
             $pslines->[$lnum] = "$newamb a.amb \% EXIT CHANGED FROM $oldamb";
          }
       }
    }
    return $pslines;
}

sub VVIGGST3 {
  #return 1;
  my ($self, $resourceDir, $localres, $changed) = @_;
    #1) Change logo S1LGT02A
	_change($localres, $resourceDir, $changed, "S1LGPT2A.ps");
    _change($localres, $resourceDir, $changed, "S1PGLPTA.ps");

    # 2) File F1FAFA67.ps -> change page position (must be x:0, y:87)
    #_modifyPagePosition($localres, $changed, "F1FAFA67.ps", "/PagePosition [0 87] \n");
    _change($localres, $resourceDir, $changed, "F1FAFA67.ps");

	# 3) O1OGC524: Change position of S1PGLCDA (y must be 2524) and S1FGTLI0 (y must be 2355)
    #     O1OGC519: Change position of S1PGLCDA (y must be 2527) and S1FGTLI0 (y must be 2341)
    _modifyPsegYPos($localres, $changed, "O1OGV524.ps", S1PGLPTA => 2595, S1FGTLI0 => 2387, S1LGPT2A => 120 );
    _modifyPsegYPos($localres, $changed, "O1OGV519.ps", S1PGLPTA => 2427, S1FGTLI0 => 2241, S1LGPT2A => 120);
    _modifyPsegYPos($localres, $changed, "O1OGV579.ps", S1LGPT2A => 120);
    
    # 4) Insert background in O1OGC579 -> inset line "0 0 (S1SFONDO) a.IncPSegment" before DefMatrix
    _insertBackground($localres, $resourceDir, $changed, "O1OGV579.ps", "S1SFONDO.ps", "0 -40 (S1SFONDO) a.IncPSegment");
	#die "before normal end";    
    return 1;
}

sub VVIGGST1 {
  my ($self, $resourceDir, $localres, $changed) = @_;
    #1) Change logo S1LGTLI1
    _change($localres, $resourceDir, $changed, "S1LGPT1A.ps");
    _change($localres, $resourceDir, $changed, "S1PGLPTA.ps");
    return 1;
}

sub VVIGGST4 {
  my ($self, $resourceDir, $localres, $changed) = @_;
    #1) Change logo S1LGT02A
    _change($localres, $resourceDir, $changed, "S1LGPT2A.ps");
    _change($localres, $resourceDir, $changed, "S1PGLPTA.ps");
    _change($localres, $resourceDir, $changed, "S1PIGLO2.ps");
    _change($localres, $resourceDir, $changed, "S1LGGL1A.ps");
    return 1;
}

sub VVIGGS17 {
  my ($self, $resourceDir, $localres, $changed) = @_;
    #1) Change logo S1LGT02A
    _change($localres, $resourceDir, $changed, "S1LBGV20.ps");
    _change($localres, $resourceDir, $changed, "S1PBGV10.ps");

    # 2) File F1FAFA67.ps -> change page position (must be x:0, y:87)
    #_modifyPagePosition($localres, $changed, "F1FAFA67.ps", "/PagePosition [0 87] \n");
    _change($localres, $resourceDir, $changed, "F1FAFA67.ps");

	# 3) O1OGC519: Change position of S1PGLCDA (y must be 2527) and S1FGTLI0 (y must be 2341)
    _modifyPsegYPos($localres, $changed, "O1OGB519.ps", S1PBGV10 => 2427, S1FBGLI0 => 2241, S1LBGV20 => 110);
	_modifyPsegYPos($localres, $changed, "O1OGB579.ps", S1LBGV20 => 110);
	 
	_insertBackground($localres, $resourceDir, $changed, "O1OGB579.ps", "S1SFONDO.ps", "0 -40 (S1SFONDO) a.IncPSegment");
    return 1;
}

sub VVIGGS23 {
    my ($self, $resourceDir, $localres, $changed) = @_;
    #1) Change logo S1LGT02A
    _change($localres, $resourceDir, $changed, "S1LGT02A.ps");
    
    # 2) File F1FAFA67.ps -> change page position (must be x:0, y:87)
    #_modifyPagePosition($localres, $changed, "F1FAFA67.ps", "/PagePosition [0 87] \n");
    _change($localres, $resourceDir, $changed, "F1FAFA67.ps");
    
    # 3) O1OGC524: Change position of S1PGLCDA (y must be 2524) and S1FGTLI0 (y must be 2355)
    #     O1OGC519: Change position of S1PGLCDA (y must be 2527) and S1FGTLI0 (y must be 2341)
    copy($localres."/O1OGC524.ps", $changed."/O1OGC524.ps") or die "copy failed: $!";    
    _modifyOverlay($localres, $changed, "O1OGC524.ps");
    copy($localres."/O1OGC519.ps", $changed."/O1OGC519.ps") or die "copy failed: $!";
    _modifyOverlay($localres, $changed, "O1OGC519.ps");
    copy($localres."/O1OGC579.ps", $changed."/O1OGC579.ps") or die "copy failed: $!";
    _modifyOverlay($localres, $changed, "O1OGC579.ps");
    
    # 4) Insert background in O1OGC579 -> inset line "0 0 (S1SFONDO) a.IncPSegment" before DefMatrix
    _insertBackground($localres, $resourceDir, $changed, "O1OGC579.ps", "S1SFONDO.ps", "0 -40 (S1SFONDO) a.IncPSegment");
    return 1;
}

sub BeginPage {
    my ($self, $workDir, $siteconf) = (shift, shift, shift);
    my $resourceDir = $siteconf."/resources";
    my $localres = $workDir."/localres"; 
    my $changed = $workDir."/localres/changed"; 

    if (-e $changed && -d $changed) { rmtree($changed)  or die "RMTREE failed: $!"; }
    if (!-e $changed ) { mkdir($changed)  or die "MKDIR failed: $!"; }
    
    my $jr = $self->{jr};
    my $rmtfn = $jr->getValues('RemoteFileName');
    my $report_type = uc((split(/\./, $rmtfn))[1]);
    if (my $action = $self->can($report_type)) { 
       return &{$action}($self, $resourceDir, $localres, $changed); 
    }
    die "Report \"$report_type\" Handling Routine not found";
    
    return $self->VVIGGS23($resourceDir, $localres, $changed);
}

sub _change
{
    my ($localres, $resourceDir, $changed, $object) = (shift, shift, shift, shift);
    move($localres."/".$object, $changed."/".$object) or die "Move failed for $object from $localres to $changed: $^E";
    copy($resourceDir."/".$object, $localres."/".$object) or die "Copy failed for $object from $resourceDir to $localres: $^E";    
}

sub _modifyPagePosition
{
    my ($localres, $changed, $mediumMap, $cmd) = (shift, shift, shift, shift);
    
    move($localres."/".$mediumMap, $changed."/".$mediumMap) or die "Move failed for $mediumMap from $localres to $changed: $^E";
    open my $in, '<', $changed."/".$mediumMap or die "Can't read input file $mediumMap from $changed: $^E";
    open my $out, '>', $localres."/".$mediumMap or die "Can't write output file $mediumMap to $localres: $^E";
    while( <$in> )
    {
        if ($_ =~ m/^.*PagePosition.*$/) {
            print $out $cmd;
        }
        else {
            print $out $_;
        }
    }
    close $in;
    close $out;
}

sub _modifyPsegYPos
{
    my ($localres, $changed, $overlay ) = (shift, shift, shift);
    my $psegs = { map { uc($_) } @_ };
    copy("$localres/$overlay", "$changed/$overlay") or die "copy of $overlay from $localres to $changed failed: $^E";    
    
    open my $in, '<', $changed."/".$overlay or die "Can't read input file $overlay from $changed: $^E";
    open my $out, '>', $localres."/".$overlay or die "Can't write output file $overlay to $localres: $^E";
    while( <$in> )
    {
        my $ln = $_;
        if ($ln =~ m/^.+\(([^\(\)]+)\)\s+a.IncPSegment.*$/i)
        {
            my $tgtpseg = uc($1);
			#die "in $overlay found $tgtpseg - PSEGS: ".join('::', keys %{$psegs});
            if ( exists($psegs->{$tgtpseg}) ) {
              my $newy =  $psegs->{$tgtpseg};
              my ($x, $y, $res, $cmd) = split(/ /, $_, 4);
              $ln = "$x $newy $res $cmd % YPOS changed from $y to $newy by exit\n";
            }
        }
        print $out $ln;
    }
    close $in;
    close $out;
}

sub _modifyOverlay
{
    my ($localres, $changed, $overlay) = (shift, shift, shift);
    open my $in, '<', $changed."/".$overlay or die "Can't read input file $overlay from $changed: $!";
    open my $out, '>', $localres."/".$overlay or die "Can't write output file $overlay to $localres: $!";
    while( <$in> )
    {
        #O1OGC524: Change position of S1PGLCDA (y must be 2524) and S1FGTLI0 (y must be 2355)
        #O1OGC519: Change position of S1PGLCDA (y must be 2527) and S1FGTLI0 (y must be 2341)
        if ($_ =~ m/^.*(S1PGLCDA).*a.IncPSegment.*$/)
        {
            my ($x, $y, $res, $cmd) = split / /, $_;
            if ($overlay eq "O1OGC524.ps")
            {
                print $out "$x 2524 $res $cmd";
            }
            elsif ($overlay eq "O1OGC519.ps")
            {
                print $out "$x 2527 $res $cmd";
            }
        }
        elsif ($_ =~ m/^.*(S1FGTLI0).*a.IncPSegment.*$/)
        {
            my ($x, $y, $res, $cmd) = split / /, $_;
            if ($overlay eq "O1OGC524.ps")
            {
                print $out "$x 2355 $res $cmd";
            }
            elsif ($overlay eq "O1OGC519.ps")
            {
                print $out "$x 2341 $res $cmd";
            }
        }
        elsif ($_ =~ m/^.*(S1LGT02A).*a.IncPSegment.*$/)
        {
            my ($x, $y, $res, $cmd) = split / /, $_;

            print $out "$x 120 $res $cmd";
        }
        else
        {
            print $out $_;
        }
    }
    close $in;
    close $out;
}

sub _insertBackground
{
    my ($localres, $resourceDir, $changed, $overlay, $background, $cmd) = (shift, shift, shift, shift, shift, shift);
    copy($resourceDir."/".$background, $localres."/".$background) or die "Copy failed: $!";    
    move($localres."/".$overlay, $changed."/".$overlay) or die "Move failed: $!";
    open my $in, '<', $changed."/".$overlay or die "Can't read input file: $!";
    open my $out, '>', $localres."/".$overlay or die "Can't write output file: $!";
    while( <$in> )
    {
        if ($_ =~ m/^.*a.DefMatrix.*$/)
        {
            print $out $cmd."\n";
        }
        print $out $_;
    }
    close $in;
    close $out;
}

__PACKAGE__;


