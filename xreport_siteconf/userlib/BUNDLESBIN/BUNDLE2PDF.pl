sub extractReportPages {
  my ($rinfo, $pagelist) = (shift, shift);
  my $finfo = $rinfo->{folder};
  require XReport::EXTRACT;
  require XReport::PDF::DOC;

  my $query = XReport::EXTRACT->new();
  
  (my $pdfname = 'Bundle.'.$finfo->{FolderName}.'.'.$pagelist->[0].'.pdf') =~ s/[\\\/]/_/g;
  $pdfname = $finfo->{workdir}."\\".$pdfname;
#  warn "extract finfo for $pdfname: ", Dumper($rinfo), "\n";
  my $streamer = new IO::File "> $pdfname";
  warn "Unable to open \"$pdfname\" - $!\n" unless $streamer;
  return undef unless $streamer;

  $query->{docTo} = XReport::PDF::DOC->Create($streamer) ;

  my $elist;
  eval { $elist = $query->ExtractPages(
                       QUERY_TYPE => 'FROMPAGES',
                       PAGE_LIST => \@$pagelist ,
                       FORMAT => "PRINT",
                       ENCRYPT => 'YES',
                       OPTIMIZE => 1,
                       TO_DIR => $finfo->{workdir},
                       REQUEST_ID => "Bundle.".$finfo->{FolderName}.'.'.$rinfo->{ReportName},
                       #                   #OPTIMIZE => $FormVars->{"OPTIMIZE_PDF"}
                      );
    };

  if ( scalar(@$elist) == 0 ) {
    die "chiave richiesta non accessibile in Archivio - $@";
    return undef;
  }
  
  return $pdfname;
#TODO: check if and how to close extract  
}

sub outputSetup {
    my ($self, $binfo) = (shift, shift);

    $binfo->{outfilename} = $self->{workdir}.'/'.$self->LocalFileName($binfo);
    $binfo->{xlator} = sub { return $_[0]; }; # new Convert::EBCDIC();
    $binfo->{encoding} = 'E';
    $binfo->{outmode} = 'R';

    return $self;
}

use FileHandle;
sub initBundleOutput {
    my ($self, $binfo) = (shift, shift);

    $self->outputSetup($binfo);

#todo prevedere code pages differenti (magari da config)
    $self->{bannerxlator} = sub { return $_[0]; }; # \&xlate1047;
    $binfo->{encoding} = 'E';
    $binfo->{outmode} = 'R';
    
    my $pscode = ["%!PS"
        ,"/browse-dict { { exch (key: ) print == (val: ) print == } forall } bind def"
        ,"/textheight { gsave { 100 100 moveto (HIpg) true charpath pathbbox exch pop 3 -1 roll pop exch sub } "
        ,"  stopped { pop pop currentfont /FontMatrix get 3 get } if grestore } bind def"
        ,"/beginpage {  save /sssnap exch def currentpagedevice /PageSize get aload pop /PH exch def /PW exch def 0 PH moveto } bind def"
        ,"/endpage { sssnap restore showpage } bind def"
        ,"/stringshow {"
        ,"dup type /dicttype eq not { /intext exch def 1 dict dup /text intext put  } if"
        ,"<< /fname /Helvetica-Bold /text (BANNER) /fscale 1 /align (center) /pitch 0 >>"
        ,"copy begin"
        ,"currentpagedevice /PageSize get aload pop /PH exch def /PW exch def"
        ,"fname findfont [10 0 pitch 10 0 0] makefont setfont"
        ,"currentfont text stringwidth pop (XX) stringwidth pop add PW exch div fscale mul scalefont setfont"
        ,"/ldisp align (left) eq { 20 } { PW text stringwidth pop sub 2 div } ifelse def"
        ,"ldisp 0 textheight sub rmoveto  "
        ,"text show "
        ,"text stringwidth  pop 0 exch sub ldisp sub 0 rmoveto"
        ,"end"
        ,"} bind def"
        ,"/LabeledBox { "
        ,"/bkup save def"
        ,"<< /boxwidth { PW 40 sub } /boxlabel ( ) /boxfont [ /Helvetica-Bold 12 ] /text [[( ) ( ) ( )] [( ) ( ) ( )] [( ) ( ) ( )]] /boxpos [20 0] >>"
        ,"copy begin  "
        ,"currentdict /textfont known not { /textfont boxfont def } if"
        ,"boxpos aload pop /ybpos exch def /xbpos exch def"
        ,"textfont aload pop exch findfont exch scalefont setfont"
        ,"/xchar (XX) stringwidth pop 2 div def /txtwidth boxwidth xchar 4 mul sub def /lheight textheight def /thikness lheight cvi def"
        ,"/yinc lheight 1.5 mul cvi def  /boxheight text length yinc mul def /CardH boxheight yinc add yinc 2 div add def"
        ,"/xpos xbpos xchar 2 mul add def "
        ,"/ypos ybpos boxheight add def "
        ,"xbpos 0 CardH yinc add ybpos add sub rmoveto /ip [ currentpoint ] def"
        ,"newpath ip aload pop moveto 0 CardH rlineto boxwidth 0 rlineto 0 0 CardH sub rlineto closepath .5 setlinewidth stroke "
        ,"newpath ip aload pop moveto thikness thikness rmoveto 0 CardH thikness 2 mul sub rlineto boxwidth thikness 2 mul sub 0 rlineto 0 thikness 2 mul CardH sub rlineto closepath"
        ,"2 setlinewidth stroke"
        ,"gsave"
        ,"ip aload pop /ypos exch boxheight add def /origx exch thikness xchar add add def "
        ,"text {"
        ,"aload pop /rightstring exch def /dotstring exch def /leftstring exch def "
        ,"origx ypos moveto leftstring stringwidth pop 0 rmoveto 0 setgray"
        ,"0 dotstring stringwidth pop dup 2 div txtwidth rightstring stringwidth pop sub exch sub leftstring stringwidth pop sub "
        ,"{dotstring show} for "
        ,"origx ypos moveto 0 setgray leftstring show"
        ,"origx ypos moveto txtwidth rightstring stringwidth pop sub 0 rmoveto rightstring show "
        ,"/ypos ypos yinc sub def "
        ,"} forall"
        ,"ip aload pop CardH add moveto"
        ,"0 setgray"
        ,"boxfont aload pop exch findfont exch scalefont setfont"
        ,"boxlabel show"
        ,"grestore"
        ,"end"
        ,"bkup restore"
        ,"} bind def"
     ];

    if ( exists($XReport::cfg->{gs}->{pageparms}) ) {
        push @$pscode, '<<'.$XReport::cfg->{gs}->{pageparms}.'>> setpagedevice' ;
    }

#   warn "PSCODE: ", join("\n", @$pscode), "\n";
    (my $gscmd = "\"".$XReport::cfg->{gs}->{cmd}."\" ") =~ s/\//\\/g; 
    $gscmd .= join (' ' 
        ,"-q -dBATCH -dNOPAUSE -dSAFER -dQUIET -dNoCancel"
        ,"-sDEVICE=pswrite"
        ,$XReport::cfg->{gs}->{pdfparms} 
        ,"-sOutputFile=$binfo->{outfilename}.ps"
        ,"-"
        );
    $binfo->{outfile} = new FileHandle;
    $binfo->{outfile}->open("| $gscmd") or die $!;
    foreach ( @$pscode ) {
        print $binfo->{outfile} $_, "\n";
    }
    
    return $binfo->{outfile};
}

sub buildReportIndex {
   my ($self, $info, $hdr) = (shift, shift, shift);
#   warn "bri called by ", (caller())[0,2], " INFO\n", Dumper($info), "\n";
   my $lines = [];
   push @$lines, ( ref($hdr) eq 'ARRAY' ? @$hdr : split("\n", $XReport::cfg->{ReportIndex}->{header}) ) if $hdr;
   push @$lines, map { (my $l = $_) =~ s/\&lt/\</g; $l =~ s/\&gt/\>/g; $l  } split /\n/, 
                       form( $XReport::cfg->{ReportIndex}->{form}->{template},
                            ,{ cols => [split(' ', $XReport::cfg->{ReportIndex}->{form}->{cols})] 
                               ,from => [ @$info ]
                             } ) if defined($info);
  return @$lines;
} 

sub createBanner {
   my ($self, $bannerType, $info) = (shift, shift, shift);
   my $bannerCfg = $XReport::cfg->{$bannerType};
#   i::warnit("banner $bannerType: \n", Dumper($bannerCfg));
#        , "Banner infos: ", Dumper($info), "\n"
#        ;
    my $head =
        [map { $self->formatBannerRec($_, $info) } split /\n/, $bannerCfg->{head} ]; 
#    die "---------";
    push @$head, map { $self->formatBannerRec($_, $info) } 
         ( $self->buildReportIndex(delete $info->{IndexArray}, 1 )) if exists($info->{IndexArray});
         
    my $tail =
        [map { $self->formatBannerRec($_, $info) } split /\n/, $bannerCfg->{tail} ]; 

    $info->{BannerPages} = scalar(grep /beginpage/, (@{$head},@{$tail})); 
    $info->{BannerLines} = 0;
    $info->{$bannerType.'Head'} = $head;
    $info->{$bannerType.'Tail'} = $tail;
#    die "Formatted Banner: ", Dumper(\{HEAD => $head, TAIL => $tail}), "\n";
    return $info;
}