
open(IN, "<01180333.tbl") or die("?? $!"); binmode(IN);

read(IN, $chars, 256); 

for (0..15) {
  @chars = map {uc("\\x$_")} unpack("H2H2H2H2H2H2H2H2H2H2H2H2H2H2H2H2", substr($chars, $_*16,16));
  print join("", @chars), "\n";
}
