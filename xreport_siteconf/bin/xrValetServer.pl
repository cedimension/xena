
my $cfg = $XReport::cfg->{daemon}->{$ARGV[1]};
my ($SrvAddr, $SrvPort, $msgmax) = @{$cfg}{qw(ListenAddr ListenPort msgmax)};

$SrvAddr = "0.0.0.0" unless $SrvAddr;
$SrvPort = 9888 unless $SrvPort;
$msgmax = 4096 unless $msgmax;

use IO::Socket;
use IO::Select;
use Win32API::File qw(:ALL);
use Win32::Process;
use Data::Dumper;
use File::Basename;

my ($sock, $Cli, $currSt);
i::logit("$^O $ARGV[1] Central server Started for $SrvAddr\:$SrvPort...");
#i::logit(Dumper($XReport::cfg));

my $SrvSock = new IO::Socket::INET(Listen => 5, 
				   LocalAddr => "$SrvAddr:$SrvPort",
				   Proto => 'tcp', 
				   Reuse => 1) || die "unable to listen on $SrvPort - $!\n";

i::logit("Listener now waiting on port $SrvPort") if ($SrvSock);

my $sel = new IO::Select( $SrvSock );


my $currcmd = (split /[\\\/]+/, $0)[-1];
my @cfgpath_array = split /[\\\/]+/, $XReport::cfg->{cfgdatafn};
pop @cfgpath_array ;
my $currwrk = join("\\", @cfgpath_array);

my $currdir = fileparse($XReport::cfg->{cfgdatafn});
(my $clicmd = "perl \"$XReport::cfg->{ClientCode}\" -N $main::ServerName -cfg \"$XReport::cfg->{cfgdatafn}\" -M $msgmax -FD"); 
i::logit("now entering main loop (cmd: \"$clicmd\")") if ($SrvSock);
for (my $con = 0; ; $con++) {
  $sock = $sel->can_read(15);
  next unless ($sock);
  $Cli = $SrvSock->accept || i::logit("unable to accept connection - $! - $?");
  last unless $Cli;

  my ($cliport, $iaddr) = sockaddr_in(getpeername($Cli));
  $REQ::peer = inet_ntoa($iaddr);
  i::logit("$main::servername accepted connection from " . $REQ::peer);
  Win32::Process::Create( my $ProcessObj, "$^X", "$clicmd " . FdGetOsFHandle( $Cli->fileno() ), 1, Win32::Process->NORMAL_PRIORITY_CLASS, "$currwrk" )
      || i::logit("ERROR Launching $clicmd");
  i::logit("$main::servername cmd to print terminated ");
}
i::logit("now leaving main loop: $currSt ", (($sock) ? "Cli pending" : "Cli Exhausted"));

$SrvSock->close;

