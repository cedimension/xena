#!/usr/bin/perl -w

use constant DBGDEST => '226.1.1.9:3000';
use IO::Socket::INET;

$main::servername =  $ARGV[1];
$main::version = '1.01';
sub i::logit {
 $main::xreport_dbgsock = new IO::Socket::INET(Proto=>'udp',PeerAddr=>DBGDEST, Reuse => 1) unless $main::xreport_dbgsock;  
 $main::xreport_dbgsock->send(localtime(gmtime()) ." - $^O $main::servername \>" .join('', @_), 0, DBGDEST) if $main::xreport_dbgsock;
 print @_, "\n";
}

sub logNdie {
  i::logit(@_, " - time to die");
  die @_;
}

use constant SOAPMSGHDR => '<?xml version="1.1" encoding="UTF-8"?>'
  . '<soap:Envelope '
  . 'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" '
  . 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
  .' xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
  . '<soap:Body>'
  ;

use constant SOAPMSGTAIL => '</soap:Body>'
  . '</soap:Envelope>'
  ;

sub _buildSOAPStruct {
  my ($DocumentData, $rtype) = (shift, shift);
  $DocumentData->{comment} = 'version 1.1';
  return XMLout($DocumentData, RootName => $rtype || 'RESPONSE',
		NoIndent => 1, NoSort => 1, # SuppressEmpty => 1,
		KeyAttr => {'Columns' => 'colname'},
		XMLDecl => SOAPMSGHDR) . SOAPMSGTAIL;
}


sub listPrinterPorts {
  use Win32::TieRegistry 0.20 qw(
				  TiedRef $Registry
				  ArrayValues 1  SplitMultis 1  AllowLoad 1
				  REG_SZ REG_EXPAND_SZ REG_DWORD REG_BINARY REG_MULTI_SZ
				  KEY_READ KEY_WRITE KEY_ALL_ACCESS
			       );
  my $kpath = "LMachine\\SYSTEM\\CurrentControlSet\\Control\\Print\\Monitors\\";
  my $ports = {};

  my $printMonKey = $Registry->Open($kpath
				   { Access=>KEY_READ, Delimiter=>"\\" } ) || logNdie "Can't read path \"$kpath\" reason: $^E\n";
  $printMonKey->ArrayValues(1);
  
  foreach my $subKeyName (  $printMonKey->SubKeyNames() ) {
    next if $subKeyName =~ /^(?:DynData|PerfData)$/i;
    my $subKey = $printMonKey->{$subKeyName} || next;# logNdie "Can't read subkey \"$kpath$subKeyName\\\" reason: $^E\n";
    if (my $portsKey = $subKey->{Ports}) {
      foreach my $portName ( $portsKey->SubKeyNames() ) {
	my $regPort = $portsKey->{$portName} || logNdie "Can't read \"$kpath$subKeyName\\Ports\\$portName\\\" key: $^E\n";
	$ports->{$portName} = { map { ($_ =~ /^\\?(.*)/) => ( $regPort->{$_}->[0] =~ /^0x(.*)$/ ? unpack("N", pack("H*", $1)) :  $regPort->{$_}->[0] ) } keys %{$regPort} };
      }
    }
  }
  return $ports;
}

sub getPrinterPort {
  my $name = shift;
  $main::printerports = listPrinterPorts() unless $main::printerports;
  my $properties = $main::printerports->{$name};
  return {} unless $properties;
  return $properties;
}


sub PortName {
  my ($name, $value) = (shift, shift);
  return { $name => 'N/A'  } unless $value;
  return { $name => $value } if $value =~ /(?:^\\\\|:$)/;
  my $list = {};
  my @props = qw(HostName IPAddress  HWAddress PortNumber Protocol NetworkTimeout);
  @{$list}{@props} = @{getPrinterPort($value)}{@props};
  return { $name => $value, %$list };
}

sub PaperSizesSupported { 
  my ($name, $list) = (shift, shift); 
  return { $name => 'N/A'} unless $list;
  my $decoded = { map { $_ => ( /^0$/ ? 'Unknown'
				: /^1$/ ? 'Other'
				: /^4$/ ? 'C'
				: /^5$/ ? 'D'
				: /^6$/ ? 'E'
				: /^7$/ ? 'Letter'
				: /^8$/ ? 'Legal'
				: /^11$/ ? 'NA-Number-10-Envelope'
				: /^15$/ ? 'NA-Number-9-Envelope'
				: /^21$/ ? 'A3'
				: /^22$/ ? 'A4'
				: /^23$/ ? 'A5'
				: /^49$/ ? 'JIS B0'
				: /^54$/ ? 'JIS B5'
				: /^55$/ ? 'JIS B6'
				: $_ ) } @$list };
  return {$name => join("\t", @{$decoded}{sort keys %$decoded})};
}

sub setValue {
  #  print "setting Values for $_[0]\n";
  my ($name, $value) = (shift, shift);
  return { $name => (defined($value) ? $value : 'N/A') };
}

sub listPrinters {
  use Win32::OLE('in');
  $Win32::OLE::Warn = 3;

  my $ports;
  eval {$ports = listPrinterPorts();};
  logNdie("READ REGISTRY Failed - $@\n") if $@;
  
  my @propertiesList = qw(
			   Caption
			   Comment 
			   Description 
			   DeviceID 
			   DriverName 
			   ExtendedPrinterStatus 
			   HorizontalResolution 
			   InstallDate 
			   LanguagesSupported 
			   Location 
			   MaxSizeSupported 
			   PaperSizesSupported 
			   PortName 
			   PrinterState 
			   PrinterStatus 
			   Queued 
			   ServerName 
			   Status 
			   StatusInfo 
			   SystemName 
			);

  use constant wbemFlagReturnImmediately => 0x10;
  use constant wbemFlagForwardOnly => 0x20;
  my $computer = ".";
  my $objWMIService;
  eval {$objWMIService = Win32::OLE->GetObject
    ("winmgmts:{impersonationLevel=impersonate}\\\\$computer\\root\\CIMV2"); };
  logNdie("WMI connection failed - OLE Error: $@\n") if $@;
#  logNdie("WMI connection failed - OLE Error: $@\n") unless ref($objWMIService) && ref($objWMIService) eq 'Win32::OLE';

  my $colItems;
    eval { $colItems = $objWMIService->ExecQuery
    ("SELECT * FROM Win32_Printer","WQL",wbemFlagReturnImmediately | wbemFlagForwardOnly); };
  logNdie("WMI query failed - OLE Error: $@\n") if $@;

  foreach my $objItem (in $colItems) {
    $printers->{$objItem->{Name}} = 
      { map { my $setcode = main->can($_) || \&main::setValue; 
	      %{&$setcode($_, $objItem->{$_})} } @propertiesList};
  }

#  return $printers;

  my $newentries = [];
  my $plist = [sort keys %$printers ];
  foreach my $name ( @$plist ) {
    my $infos = $printers->{$name};
    push @$newentries, {Columns => [{ colname => 'PrtName', content => $name },
				    { colname => 'PrtSrvAddress', content => "$ENV{COMPUTERNAME}\:$XReport::cfg->{daemon}->{$main::servername}->{ListenPort}" },
				   map { { colname => $_, content => $infos->{$_} } } sort keys %$infos
				  ] };
  }
  return $newentries;
}

my $currentcfg = $XReport::cfg->{daemon}->{$main::servername};
my ($xrAddr, $xrPort, $msgmax, $xrUri) = @{$currentcfg->{xrPrintIface}}{qw(ListenAddr ListenPort msgmax URIpfx)} if $currentcfg && exists $currentcfg->{xrPrintIface};
$main::xrhost = ($xrAddr || 'BAC25') . ':' . ($xrPort || 10090);
$xrUri = 'xrPrintIface/?' unless $xrUri;

i::logit("PPRINTER $main::version remote publisher server Started for $main::xrhost...\n");

use XML::Simple;

use LWP;

my $r = LWP::UserAgent->new;

my $plist = listPrinters();
#my $chkdata= XMLout($plist, NoSort => 0, SuppressEmpty => 0);

#if (!$main::chkd || $main::chkd ne $chkdata ) {  
  i::logit("PPRINTER $main::version printers config changed - printers: " . scalar(@$plist) . " notifying server $main::xrhost/$xrUri\n");

  my $lclprnts = XMLout({IndexName => '_Printers', DocumentType => 'ResourceList', IndexEntries => [ { } ], 'NewEntries' => $plist },  
			RootName => 'REQUEST',
			NoIndent => 1, NoSort => 1, SuppressEmpty => 0,
			KeyAttr => {'Columns' => 'colname'},
			XMLDecl => SOAPMSGHDR) . SOAPMSGTAIL;
  
  
  my $cfgresp = $r->post( "http://$main::xrhost/${xrUri}RegisterPrinters",
			  'Content-Type' => 'text/xml', 
			  'Content' => $lclprnts,
			  'SOAPAction' => 'RegisterPrinters'
			) if scalar(@$plist);
  # die Dumper($cfgresp);
  i::logit("PPRINTER $main::version Error response from server - ", $cfgresp->status_line) unless $cfgresp->is_success();
  # BEGIN debug Paolo
#  open(DLOG, ">C:\\printers_update.xml");
#  print DLOG $lclprnts;
#  close(DLOG);
  # END debug Paolo
  $main::chkd = $chkdata;
#}
#else {
#  print "$^O $ARGV[1] PPRINTER no changes detected\n";
#}

i::logit("PPRINTER $main::version remote publisher server ended for $main::xrhost...\n");

1;
