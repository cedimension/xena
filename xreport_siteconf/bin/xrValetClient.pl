#

use IO::Socket::INET;
use IO::Select;
use File::Temp;
use Win32API::File qw(:ALL);
use constant DBGDEST => '226.1.1.9:3000';

sub i::logit {
 $main::xreport_dbgsock = new IO::Socket::INET(Proto=>'udp',PeerAddr=>DBGDEST, Reuse => 1) unless $main::xreport_dbgsock;  
 $main::xreport_dbgsock->send(localtime(gmtime()) ." - $main::servername 1.02\>" .join('', @_), 0, DBGDEST) if $main::xreport_dbgsock;
 print @_, "\n";
}

sub logNdie {
  i::logit(@_, " - time to die");
  die @_;
}

my ($msgmax, $CliFno) = @ARGV[0,1];
my @parms = @ARGV;
while ( @parms ) {
  #  print "PARMS: ", join("::", @parms), "\n";
  $_ = shift @parms;
  ( 
   /^-N$/ ? do { $main::servername   = shift @parms; }
   : /^-FD$/ ? do { $main::CliFno   = shift @parms; }
   : /^-M$/  ? do { $main::msgmax   = shift @parms; }
   : 1
  );
}
($main::spath = $0) =~ s/[\/\\][^\\\/]+$//;

i::logit("Now Entering CLIENT $0 - PARMS: ", join('::', @ARGV));
my $Sock = new IO::Socket::INET;
binmode $Sock;

OsFHandleOpen($Sock, $main::CliFno, "rw") or logNdie("CliSock OPEN ERROR <$!>\n");
setsockopt($Sock, SOL_SOCKET, SO_RCVBUF, pack("l", $main::msgmax))   || logNdie "Setsockopt error: $!";

($REQ::port, $REQ::iaddr) = sockaddr_in(getpeername($Sock));

$REQ::peer = inet_ntoa($REQ::iaddr);

i::logit("Serving connection $con from $REQ::peer at $REQ::port - maxblks $main::msgmax");
$REQ::InEof = 0;
my $pdffh = File::Temp->new( SUFFIX => '.pdf', DIR => $ENV{TMP}, TEMPLATE => "PH".$$."XXXX", OPEN => 1, UNLINK => 1 )
        || logNdie "Unable to open temp pdf File - $!";
binmode $pdffh;
my ($pdfname, $prtlen, $prtparms, $printername) = ($pdffh->filename(), 0, '', '');

my $sel = new IO::Select( $Sock );

logNdie "SEL2 Connection timed out during CNTL PKT receive" unless $sel->can_read(15);
$Sock->recv( my $buff, 10);
my $blen = (defined($buff) ? length($buff) : 0);
logNdie "No Data received from requester at $REQ::peer" if (!defined($buff) || !$blen);
logNdie sprintf("Data stream with invalid Header detected - cmdid: %s prtname: %s buff ll: %d", 
		unpack("H16 H4", $buff), length($buff) ) unless $buff =~  /^STRM2PRT/;

(my $cmdid, my $cmdsize) = unpack("a8 n", $buff);
logNdie "SEL2 Connection timed out during receive" if (! $sel->can_read(15) );
$Sock->recv($buff, $cmdsize);
$blen = (defined($buff) ? length($buff) : 0);

(my $fsize, $printername, my $usrparms, my $buff ) = unpack("N n/a* n/a* a*", $buff);
$usrparms = { split /\t/, $usrparms };
$prtparms .= "-d $usrparms->{PrintDuplex} " if $usrparms->{PrintDuplex} && $usrparms->{PrintDuplex} > 1;
$prtparms .= "$usrparms->{PrintParameters} " if $usrparms->{PrintParameters};

my $errmsg = ( !$printername ? " Null PrtName -" : '' )
  . (!$fsize ? " Zero Length Data -" : '')
  . '';
logNdie "Stream Error: - ".$errmsg if $errmsg;
i::logit( "writing into file \"$pdfname\" to be printed to $printername");

$blen = (defined($buff) ? length($buff) : 0);
if (!$blen) {
  my $bytes2read = $fsize - $prtlen;
  $bytes2read = $main::msgmax if $bytes2read > $main::msgmax;
  logNdie "SEL2 Connection timed out during receive" if (! $sel->can_read(15) );
  $Sock->recv($buff, $main::msgmax);
  $blen = (defined($buff) ? length($buff) : 0);
}
logNdie "No Print Data received ($fsize expected) from requester at $REQ::peer" if !$blen;

while ( $blen ) {
  print $pdffh $buff;
  $prtlen += length($buff);
  last unless $prtlen < $fsize;
  my $bytes2read = $fsize - $prtlen;
  $bytes2read = $main::msgmax if $bytes2read > $main::msgmax;
  logNdie "SEL2 Connection timed out during receive loop" if (! $sel->can_read(15) );
  $Sock->recv($buff, $bytes2read);
  $blen = (defined($buff) ? length($buff) : 0);
}
logNdie "Unmatched size of Print Data - $fsize expected - $prtlen received" unless $fsize == $prtlen;
$pdffh->close();


#my $gsparms = join(' ', qw(-dNoCancel -dSAFER -dBATCH -dNOPAUSE -sDEVICE=mswinpr2 -r600), 
#		   split(/\t/, $prtparms), 
#		   "-I\"c:\\Program Files\\gs\\gs8.64\\Resource\"", 
#		   "-I\"c:\\Program Files\\gs\\fonts\"", 
#		   "-I\"c:\\Program Files\\gs\\gs8.64\\lib\"", 
#		   "-sFONTPATH=\"C:\\WINDOWS\\FONTS\"", 
#		   "-sOutputFile=\"\%printer\%$printername\"", 
#		   "\"$pdfname\""); 
#print "calling gs with parms=\"$gsparms\"\n";
#system "\"c:\\Program Files\\gs\\gs8.64\\bin\\gswin32c.exe\" $gsparms";
my $scmd = join(" ", "\"$main::spath\\pdfp.exe\"", "-p", "\"$printername\"", split(/\t/, $prtparms), 
		   "\"$pdfname\"");
my $ackmsg = pack("a8 N", 'PRTDSTRM', $prtlen);
logNdie "Connection timed out during wait to send last ACK" if !$sel->can_write(15);
my $clen = $Sock->send( $ackmsg );
die "Unable to send last ACK" unless $clen && $clen == length($ackmsg);

use Win32::Mutex;
use Win32::Process;
use Win32;

my $ProcessObj;
(my $cmdpath = "$main::spath\\pdfp.exe") =~ s/\//\\/g;
#(my $cmdpath = ".\\pdfp.exe");
logNdie("unable to access $cmdpath") unless -e $cmdpath;
my $multiplier = -s $pdfname / 1000000 + 1;

my $cmdline = join(" ", "pdfp.exe", "-p", "\"$printername\"", "-m", ($multiplier || 3), split(/\t/, $prtparms), "\"$pdfname\"");

i::logit("FILE: \"$pdfname\" SIZE: ", -s $pdfname, " EXE: $cmdpath CMD: $cmdline"); 

#system $scmd;

eval {
  my $mutex = new Win32::Mutex(0, 'xrValetClient');
  $mutex->wait(60000);
  Win32::Process::Create($ProcessObj, $cmdpath, $cmdline, 1, NORMAL_PRIORITY_CLASS, ".") 
      || logNdie(Win32::FormatMessage( Win32::GetLastError() ));
  i::logit("Waiting for \"$cmdpath\" to end"); 
  
  $ProcessObj->Suspend();
  $ProcessObj->Resume();
  $ProcessObj->Wait(INFINITE) if $ProcessObj;
  $mutex->release();
  i::logit("Process of \"$cmdpath\" terminated"); 
};

i::logit(" CMD RESULT: $@") if $@; 
  
shutdown($Sock,2);
close($Sock);
