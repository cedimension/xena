--refreshed data from source table
MERGE tbl_FoldersRules AS TARGET
USING tbl_FoldersRules_AGG AS SOURCE 
ON (TARGET.ReportGroupId = SOURCE.ReportGroupId) 
--When records are matched, update 
--the records if there is any change
WHEN MATCHED AND (TARGET.FolderName <> SOURCE.FolderName)
THEN 
UPDATE SET TARGET.FolderName = SOURCE.FolderName
--When no records are matched, insert
--the incoming records from source
--table to target table
WHEN NOT MATCHED BY TARGET THEN 
INSERT (FORBID,FolderName,ReportGroupId) 
VALUES(SOURCE.FORBID,SOURCE.FolderName,SOURCE.ReportGroupId) 
--When there is a row that exists in target table and
--same record does not exist in source table
--then delete this record from target table

--$action specifies a column of type nvarchar(10) 
--in the OUTPUT clause that returns one of three 
--values for each row: 'INSERT', 'UPDATE', or 'DELETE', 
--according to the action that was performed on that row
OUTPUT $action, 
--UPDATED.ReportGroupId as UpdRportGroupId,
--UPDATED.FolderName as UpdFolderName,
INSERTED.FolderName AS FolderNameID, 
INSERTED.ReportGroupId AS ReportGroupId;

SELECT @@ROWCOUNT;
GO




