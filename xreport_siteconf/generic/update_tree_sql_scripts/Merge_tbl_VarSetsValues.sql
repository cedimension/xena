SET ANSI_PADDING ON

GO
--refreshed data from source table
MERGE tbl_VarSetsValues AS TARGET
USING tbl_VarSetsValues_AGG AS SOURCE 
ON (TARGET.VarSetName = SOURCE.VarSetName and TARGET.VarName = SOURCE.VarName and TARGET.VarValue = SOURCE.VarValue) 
--When records are matched, update 
--the records if there is any change
-- NO NEEDS TO UPDATE ALL COLUMNS BELONGS TO KEY
--When no records are matched, insert
--the incoming records from source
--table to target table
WHEN NOT MATCHED BY TARGET THEN 
INSERT (VarSetName , VarName , VarValue ) 
VALUES(SOURCE.VarSetName,SOURCE.VarName,SOURCE.VarValue) 
--When there is a row that exists in target table and
--same record does not exist in source table
--then delete this record from target table

--$action specifies a column of type nvarchar(10) 
--in the OUTPUT clause that returns one of three 
--values for each row: 'INSERT', 'UPDATE', or 'DELETE', 
--according to the action that was performed on that row
OUTPUT $action, 

INSERTED.VarSetName AS VarSetName, 
INSERTED.VarName AS VarName,
INSERTED.VarValue AS VarValue
;

SELECT @@ROWCOUNT;
GO
SET ANSI_PADDING OFF

GO



