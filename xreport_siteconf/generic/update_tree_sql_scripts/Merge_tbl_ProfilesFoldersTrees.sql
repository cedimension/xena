--select count(*) from tbl_ProfilesFoldersTrees
--select count(*) from tbl_ProfilesFoldersTrees_AGG
--refreshed data from source table
MERGE tbl_ProfilesFoldersTrees AS TARGET
USING tbl_ProfilesFoldersTrees_AGG AS SOURCE 
ON (TARGET.ProfileName = SOURCE.ProfileName and 
TARGET.ProfileName = CASE WHEN CHARINDEX('\', REVERSE(TARGET.RootNode)) > 0 THEN SUBSTRING(TARGET.RootNode, (LEN(TARGET.RootNode)) - CHARINDEX('\', REVERSE(TARGET.RootNode))+2,LEN(TARGET.RootNode))
ELSE TARGET.PROFILENAME END
) 
--When records are matched, update 
--the records if there is any change
WHEN MATCHED AND (TARGET.RootNode <> SOURCE.RootNode)
THEN 
UPDATE SET TARGET.RootNode = SOURCE.RootNode
--When no records are matched, insert
--the incoming records from source
--table to target table
WHEN NOT MATCHED BY TARGET THEN 
INSERT  (ProfileName,RootNode)  
VALUES(SOURCE.ProfileName,SOURCE.RootNode) 
--When there is a row that exists in target table and
--same record does not exist in source table
--then delete this record from target table

--$action specifies a column of type nvarchar(10) 
--in the OUTPUT clause that returns one of three 
--values for each row: 'INSERT', 'UPDATE', or 'DELETE', 
--according to the action that was performed on that row
OUTPUT $action, 
--UPDATED.ReportGroupId as UpdRportGroupId,
--UPDATED.FolderName as UpdFolderName,
INSERTED.ProfileName AS ProfileName, 
INSERTED.RootNode AS RootNode;

SELECT @@ROWCOUNT;
GO




