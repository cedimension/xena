--select * from tbl_UserAliases_AGG
--select * from tbl_UserAliases
--refreshed data from source table
MERGE tbl_UserAliases AS TARGET
USING tbl_UserAliases_AGG AS SOURCE 
ON (TARGET.UserName = SOURCE.UserName and TARGET.UserAlias = SOURCE.UserAlias) 
--When records are matched, update 
--the records if there is any change
WHEN MATCHED AND (TARGET.UserAliasDescr <> SOURCE.UserAliasDescr)
THEN 
UPDATE SET TARGET.UserAliasDescr = SOURCE.UserAliasDescr 
--When no records are matched, insert
--the incoming records from source
--table to target table
WHEN NOT MATCHED BY TARGET THEN 
INSERT (UserAlias,UserName,UseAlias,UserAliasDescr) 
VALUES(SOURCE.UserAlias,SOURCE.UserName,SOURCE.UseAlias,SOURCE.UserAliasDescr) 
--When there is a row that exists in target table and
--same record does not exist in source table
--then delete this record from target table
WHEN NOT MATCHED BY SOURCE AND TARGET.UserAliasDescr  like 'User authorized directly%' THEN 
DELETE 
--$action specifies a column of type nvarchar(10) 
--in the OUTPUT clause that returns one of three 
--values for each row: 'INSERT', 'UPDATE', or 'DELETE', 
--according to the action that was performed on that row
OUTPUT $action, 
INSERTED.UserAlias AS UserAlias, 
INSERTED.UserName AS UserName,
DELETED.UserAlias as DeleteUserAlias,
DELETED.UserName as DeleteUserName;

SELECT @@ROWCOUNT;
GO
DELETE UA from tbl_UserAliases UA where UserName = 'BACA' and UserAlias <>  UserName;
GO



