--select count(*) from tbl_NamedReportsGroups --50421
--select count(*) from tbl_NamedReportsGroups_AGG --23710

--refreshed data from source table
MERGE [dbo].[tbl_NamedReportsGroups] AS TARGET
USING [dbo].[tbl_NamedReportsGroups_AGG] AS SOURCE 
ON (TARGET.ReportGroupId = SOURCE.ReportGroupId
and TARGET.ReportRule = SOURCE.ReportRule
and TARGET.FilterVar = SOURCE.FilterVar
and TARGET.FilterRule = SOURCE.FilterRule
and TARGET.RecipientRule = SOURCE.RecipientRule
)  
--When records are matched, update 
--the records if there is any change
--WHEN MATCHED 
--THEN 
--INSERT 'NO UPDATE TO DO' ;
--When no records are matched, insert
--the incoming records from source
--table to target table
WHEN NOT MATCHED BY TARGET THEN 
INSERT (ReportGroupId, ReportRule, FilterVar, FilterRule, RecipientRule) 
VALUES(SOURCE.ReportGroupId, SOURCE.ReportRule, SOURCE.FilterVar, SOURCE.FilterRule, SOURCE.RecipientRule) 
--When there is a row that exists in target table and
--same record does not exist in source table
--then delete this record from target table
--WHEN NOT MATCHED BY SOURCE THEN 
--DELETE
--$action specifies a column of type nvarchar(10) 
--in the OUTPUT clause that returns one of three 
--values for each row: 'INSERT', 'UPDATE', or 'DELETE', 
--according to the action that was performed on that row
OUTPUT $action,
INSERTED.ReportGroupId AS ReportGroupId,
INSERTED.ReportRule AS ReportRule, 
INSERTED.FilterRule AS FilterRule;

SELECT @@ROWCOUNT;
GO




