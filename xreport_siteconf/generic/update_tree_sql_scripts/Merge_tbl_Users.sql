--refreshed data from source table
MERGE tbl_Users AS TARGET
USING tbl_Users_AGG AS SOURCE 
ON (TARGET.UserName = SOURCE.UserName) 
--When records are matched, update 
--the records if there is any change
WHEN MATCHED AND (TARGET.UserDescr <> SOURCE.UserDescr or TARGET.EmailAddr <> SOURCE.EmailAddr )
THEN 
UPDATE SET TARGET.UserDescr = SOURCE.UserDescr, 
TARGET.EmailAddr = SOURCE.EmailAddr 
--When no records are matched, insert
--the incoming records from source
--table to target table
WHEN NOT MATCHED BY TARGET THEN 
INSERT (UserName,Password,EMailAddr,UserDescr) 
VALUES(SOURCE.UserName,SOURCE.Password,SOURCE.EMailAddr,SOURCE.UserDescr) 
--When there is a row that exists in target table and
--same record does not exist in source table
--then delete this record from target table
--WHEN NOT MATCHED BY SOURCE THEN 
--DELETE
--$action specifies a column of type nvarchar(10) 
--in the OUTPUT clause that returns one of three 
--values for each row: 'INSERT', 'UPDATE', or 'DELETE', 
--according to the action that was performed on that row
OUTPUT $action, 
INSERTED.UserName AS FolderNameID, 
INSERTED.UserDescr AS UserDescr;

SELECT @@ROWCOUNT;
GO




