SET ANSI_PADDING ON

GO
--select count(*) from tbl_Profiles
--select count(*) from tbl_Profiles_AGG
--refreshed data from source table
MERGE tbl_Profiles AS TARGET
USING tbl_Profiles_AGG AS SOURCE 
ON (TARGET.ProfileName =  SOURCE.ProfileName)
--When records are matched, update 
--the records if there is any change
--When no records are matched, insert
--the incoming records from source
--table to target table
WHEN NOT MATCHED BY TARGET THEN 
INSERT  (ProfileName, ProfileDescr,LookAtEverything)  
VALUES(SOURCE.ProfileName,SOURCE.ProfileDescr,SOURCE.LookAtEverything) 
--When there is a row that exists in target table and
--same record does not exist in source table
--then delete this record from target table

--$action specifies a column of type nvarchar(10) 
--in the OUTPUT clause that returns one of three 
--values for each row: 'INSERT', 'UPDATE', or 'DELETE', 
--according to the action that was performed on that row
OUTPUT $action, 
--UPDATED.ReportGroupId as UpdRportGroupId,
--UPDATED.FolderName as UpdFolderName,
INSERTED.ProfileName as ProfileName,
INSERTED.ProfileDescr AS ProfileDescr; 


SELECT @@ROWCOUNT;
GO

SET ANSI_PADDING OFF 
GO



