SET ANSI_PADDING ON

GO
--refreshed data from source table
MERGE tbl_Folders AS TARGET
USING tbl_Folders_AGG AS SOURCE 
ON (TARGET.FolderName = SOURCE.FolderName) 
--When records are matched, update 
--the records if there is any change
WHEN MATCHED AND (TARGET.FolderDescr <> SOURCE.FolderDescr OR TARGET.FolderAddr <> SOURCE.FolderAddr)
THEN 
UPDATE SET TARGET.FolderAddr = SOURCE.FolderAddr, 
TARGET.FolderDescr = SOURCE.FolderDescr 
--When no records are matched, insert
--the incoming records from source
--table to target table
WHEN NOT MATCHED BY TARGET THEN 
INSERT (FolderAddr,FolderDescr,FolderName,IsActive,ParentFolder,SpecialInstr) 
VALUES(SOURCE.FolderAddr,SOURCE.FolderDescr,SOURCE.FolderName,SOURCE.IsActive,SOURCE.ParentFolder,SOURCE.SpecialInstr) 
--When there is a row that exists in target table and
--same record does not exist in source table
--then delete this record from target table
WHEN NOT MATCHED BY SOURCE THEN 
DELETE
--$action specifies a column of type nvarchar(10) 
--in the OUTPUT clause that returns one of three 
--values for each row: 'INSERT', 'UPDATE', or 'DELETE', 
--according to the action that was performed on that row
OUTPUT $action, 
DELETED.FolderName AS FolderNameID, 
--DELETED.ProductName AS TargetProductName, 
--DELETED.Rate AS TargetRate, 
INSERTED.FolderName AS FolderNameID, 
INSERTED.FolderAddr AS AddressF;

SELECT @@ROWCOUNT;
GO


SET ANSI_PADDING OFF

GO

